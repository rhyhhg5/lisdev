<%
//程序名称：LAEmployeeInput.jsp
//程序功能：功能描述
//创建日期：2004-06-04 12:04:37
//创建人  ：yangtao
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LFComQueryInput.js"></SCRIPT> 
  <%@include file="LFComQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLAEmployeeGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
        <TD class= title> 
          管理机构代码
        </TD>
        <TD  class= input> 
          <Input class= common name=ManageCom >
        </TD>
      </tr> 
      <tr class=common>
        <TD  class= title>
          机构名称 
        </TD>
        <TD  class= input> 
          <Input name=Name class=common   >        
        </TD>
        <TD  class= title>
          操作人 
        </TD>
        <TD  class= input> 
          <Input name=Operator class=common  >         
        </TD>
        </tr>
        <tr class=common>       
            <TD  class= title>
          成立日期 
        </TD>
        <TD  class= input> 
          <Input name=FoundDate class='coolDatePicker' dateFormat='short' verify="成立日期|notnull&Date" > 
        </TD>
        <TD  class= title>
          机构地址
        </TD>
        <TD  class= input> 
          <Input name=Address class=common   >        
        </TD>
        </tr>
        <tr>
          <TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='code' name=EndFlag verify="停业|code:yesno&NOTNULL" ondblclick="return showCodeList('yesno',[this]);" 
                                                         onkeyup="return showCodeListKey('yesno',[this]);">
          </TD>
          <TD  class= title>
            机构邮编
          </TD>
          <TD  class= input>
            <Input class= common name=Zipcode >
          </TD>  
        </tr>
        <tr>
          <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate format='short'>
          </TD>
          <TD  class= title>
            机构电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            机构属性 
          </TD>
          <TD  class= input>
            <Input class="code" name=Attribute verify="机构属性|NOTNULL"  CodeData="0|^0|集团公司^1|总公司^2|省级分公司（一级分公司）^3|中心支公司（二级分公司）^4|支公司^5|营业部^6|营销服务部^7|代表处" 
            ondblclick="showCodeListEx('Attribute',[this],[0]);"  onkeyup="showCodeListKeyEx('Attribute',[this],[0]);">
          </TD>
          <TD  class= title>
            机构传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            机构标志 
          </TD>
          <TD  class= input>
            <Input class="code" name=Sign verify="机构标志|NOTNULL"  CodeData="0|^0|本部^1|非本部" 
            ondblclick="showCodeListEx('Sign',[this],[0]);"  onkeyup="showCodeListKeyEx('Sign',[this],[0]);">
          </TD>
          <TD  class= title>
            EMail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            统计标志 
          </TD>
          <TD  class= input>
            <Input class="code" name=calFlag verify="统计标志|NOTNULL"  CodeData="0|^0|统计^1|不统计" 
            ondblclick="showCodeListEx('calFlag',[this],[0]);"  onkeyup="showCodeListKeyEx('calFlag',[this],[0]);">
          </TD>
          <TD  class= title>
            网址
          </TD>
          <TD  class= input>
            <Input class= common name=WebAddress >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            总公司批示时间
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=ApproveDate format='short'>
          </TD>
          <TD  class= title>
            主管人员姓名
          </TD>
          <TD  class= input>
            <Input class=common name=ManagerName >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            工商批准营业时间
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=PassDate format='short'>
          </TD>
        </tr>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询" TYPE=button class=common onclick="easyQueryClick();">
        <INPUT VALUE="返回" TYPE=button class=common onclick="returnParent();"> 	
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAEmployee1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLAEmployee1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLAEmployeeGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</Div>
	<Div>
  	
  
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
