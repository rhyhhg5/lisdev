<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LFCom.jsp
//程序功能：
//创建日期：2004-06-05 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LFCom.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LFComInit.jsp"%>  
</head>
<body  onload="initForm();" >
  <form action="./LFComSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../wagecal/OperateButton.jsp"%>    
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
          <td class=titleImg>
            机构信息维护
          </td>
        </td>
      </tr> 
    </table>
    <Div  id= "divLAAgent1" style= "display: ''">
      
    <table  class= common>
      <TR  class= common>
        <TD class= title> 
          管理机构代码
        </TD>
        <TD  class= input> 
          <Input class= 'readonly' readonly name=ManageCom >
        </TD>
      </tr> 
      <tr class=common>
        <TD  class= title>
          机构名称 
        </TD>
        <TD  class= input> 
          <Input name=Name class=common   >        
        </TD>
        <TD  class= title>
          操作人 
        </TD>
        <TD  class= input> 
          <Input name=Operator class=readonly  readonly>         
        </TD>
        </tr>
        <tr class=common>       
            <TD  class= title>
          成立日期 
        </TD>
        <TD  class= input> 
          <Input name=FoundDate class='coolDatePicker' dateFormat='short' verify="成立日期|notnull&Date" > 
        </TD>
        <TD  class= title>
          机构地址
        </TD>
        <TD  class= input> 
          <Input name=Address class=common   >        
        </TD>
        </tr>
        <tr>
          <TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='code' name=EndFlag verify="停业属性|code:yesno&NOTNULL" ondblclick="return showCodeList('yesno',[this]);" 
                                                         onkeyup="return showCodeListKey('yesno',[this]);">
          </TD>
          <TD  class= title>
            机构邮编
          </TD>
          <TD  class= input>
            <Input class= common name=Zipcode >
          </TD>  
        </tr>
        <tr>
          <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate format='short'>
          </TD>
          <TD  class= title>
            机构电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            机构属性 
          </TD>
          <TD  class= input>
            <Input class="code" name=Attribute verify="机构属性|NOTNULL"  CodeData="0|^0|集团公司^1|总公司^2|省级分公司（一级分公司）^3|中心支公司（二级分公司）^4|支公司^5|营业部^6|营销服务部^7|代表处" 
            ondblclick="showCodeListEx('Attribute',[this],[0]);"  onkeyup="showCodeListKeyEx('Attribute',[this],[0]);">
          </TD>
          <TD  class= title>
            机构传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            机构标志 
          </TD>
          <TD  class= input>
            <Input class="code" name=Sign verify="机构标志|NOTNULL"  CodeData="0|^0|本部^1|非本部" 
            ondblclick="showCodeListEx('Sign',[this],[0]);"  onkeyup="showCodeListKeyEx('Sign',[this],[0]);">
          </TD>
          <TD  class= title>
            EMail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            统计标志 
          </TD>
          <TD  class= input>
            <Input class="code" name=calFlag verify="统计标志|NOTNULL"  CodeData="0|^0|统计^1|不统计" 
            ondblclick="showCodeListEx('calFlag',[this],[0]);"  onkeyup="showCodeListKeyEx('calFlag',[this],[0]);">
          </TD>
          <TD  class= title>
            网址
          </TD>
          <TD  class= input>
            <Input class= common name=WebAddress >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            总公司批示时间
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=ApproveDate format='short'>
          </TD>
          <TD  class= title>
            主管人员姓名
          </TD>
          <TD  class= input>
            <Input class=common name=ManagerName >
          </TD>
        </tr>
        <tr>
          <TD  class= title>
            工商批准营业时间
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=PassDate format='short'>
          </TD>
        </tr>
      
     </table>
    </Div>
    <input type=hidden name=BranchType value=''>
    <Input type=hidden name=Operator >

    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

        