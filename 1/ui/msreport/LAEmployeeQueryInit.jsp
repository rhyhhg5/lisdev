<%
//程序名称：LAEmployeeQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-06-04 12:04:37
//创建人  ：yangtao
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('EmployeeCode').value = "";
    fm.all('ManageCom').value = "";
    fm.all('Name').value = "";
    fm.all('Birthday').value = "";
    fm.all('IDNo').value = "";
    fm.all('PolityVisage').value = "";
    fm.all('Nationality').value = "";
    fm.all('Sex').value = "";
    fm.all('RgtAddress').value = "";
    fm.all('Degree').value = "";
    fm.all('PostTitle').value = "";
    fm.all('EmployeeDateS').value = "";
    fm.all('OutWorkDate').value = "";
    fm.all('ManageFlag').value = "";
    fm.all('EmployeeCodeState').value = "";
    fm.all('EmployeeDateE').value = "";
  }
  catch(ex) {
    alert("在LAEmployeeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLAEmployeeGrid();  
  }
  catch(re) {
    alert("LAEmployeeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LAEmployeeGrid;
function initLAEmployeeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="姓名";         		//列名
    iArray[0+1][1]="80px";         		//列名
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="管理机构";         		//列名
    iArray[1+1][1]="100px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="性别";         		//列名
    iArray[2+1][1]="50px";         		//列名
 
    iArray[3+1]=new Array();
    iArray[3+1][0]="高管标志";         		//列名
    iArray[3+1][1]="50px";         		//列名
    
    iArray[4+1]=new Array();
    iArray[4+1][0]="员工状态";         		//列名
    iArray[4+1][1]="50px";         		//列名
 
    iArray[5+1]=new Array();
    iArray[5+1][0]="入司日期";         		//列名
    iArray[5+1][1]="100px";         		//列名
 
    iArray[6+1]=new Array();
    iArray[6+1][0]="离司日期";         		//列名
    iArray[6+1][1]="100px";         		//列名
    
    iArray[7+1]=new Array();
    iArray[7+1][0]="生日";         		//列名
    iArray[7+1][1]="100px";         		//列名
 
    iArray[8+1]=new Array();
    iArray[8+1][0]="政治面貌";         		//列名
    iArray[8+1][1]="50px";         		//列名
 
    iArray[9+1]=new Array();
    iArray[9+1][0]="民族";         		//列名
    iArray[9+1][1]="50px";         		//列名
 
    iArray[10+1]=new Array();
    iArray[10+1][0]="户口所在地";         		//列名
    iArray[10+1][1]="50px";         		//列名
 
    iArray[11+1]=new Array();
    iArray[11+1][0]="学历";         		//列名
    iArray[11+1][1]="50px";         		//列名
 
    iArray[12+1]=new Array();
    iArray[12+1][0]="专业技术职称";         		//列名
    iArray[12+1][1]="50px";         		//列名
    
    iArray[13+1]=new Array();
    iArray[13+1][0]="人员代码";         		//列名
    iArray[13+1][1]="100px";         		//列名
 
    LAEmployeeGrid = new MulLineEnter( "fm" , "LAEmployeeGrid" ); 
    LAEmployeeGrid.mulLineCount = 1; 
    LAEmployeeGrid.displayTitle = 1; 
    LAEmployeeGrid.locked = 1; 
 
    LAEmployeeGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LAEmployeeGrid.mulLineCount = 0;   
    LAEmployeeGrid.displayTitle = 1;
    LAEmployeeGrid.hiddenPlus = 1;
    LAEmployeeGrid.hiddenSubtraction = 1;
    LAEmployeeGrid.canSel = 1;
    LAEmployeeGrid.canChk = 0;
    LAEmployeeGrid.selBoxEventFuncName = "showOne";
*/
    LAEmployeeGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LAEmployeeGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
