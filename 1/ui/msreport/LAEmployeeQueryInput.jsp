<%
//程序名称：LAEmployeeInput.jsp
//程序功能：功能描述
//创建日期：2004-06-04 12:04:37
//创建人  ：yangtao
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LAEmployeeQueryInput.js"></SCRIPT> 
  <%@include file="LAEmployeeQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLAEmployeeGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      人员编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EmployeeCode >
    </TD>
    <TD  class= title>
      管理机构
    </TD>
    <TD  class= input>
      <Input class="code" name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Name >
    </TD>
    <TD  class= title>
      出生日期
    </TD>
    <TD  class= input>
      <Input class='coolDatePicker' dateFormat='short' name=Birthday >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      身份证号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDNo >
    </TD>
    <TD  class= title>
      政治面貌
    </TD>
    <TD  class= input>
      <Input name=PolityVisage class="code" id="polityvisage" 
		ondblclick="return showCodeList('polityvisage',[this]);" 
		onkeyup="return showCodeListKey('polityvisage',[this]);" > 
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      民族
    </TD>
    <TD  class= input>
      <Input name=Nationality class="code" id="Nationality" ondblclick="return showCodeList('Nationality',[this]);" 			onkeyup="return showCodeListKey('Nationality',[this]);" > 
    </TD>
    <TD  class= title>
      性别
    </TD>
    <TD  class= input>
      <Input name=Sex class="code" verify="性别|code:Sex" ondblclick="return showCodeList('Sex',[this]);" 
		onkeyup="return showCodeListKey('Sex',[this]);" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      户口所在地
    </TD>
    <TD  class= input> 
          <Input name=RgtAddress class="code" ondblclick="return showCodeList('NativePlaceBak',[this]);" 
		onkeyup="return showCodeListKey('NativePlaceBak',[this]);"> 
        </TD>
    <TD  class= title>
      学历
    </TD>
    <TD  class= input>
      <Input name=Degree class="code" id="Degree" 
		ondblclick="return showCodeList('Degree',[this]);" 
		onkeyup="return showCodeListKey('Degree',[this]);"> 
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      专业技术职称
    </TD>
    <TD  class= input>
      <Input name=PostTitle class='code' 
		ondblclick="return showCodeList('posttitle',[this]);" 
		onkeyup="return showCodeListKey('posttitle',[this]);" > 
    </TD>
    <TD  class= title>
      离司日期
    </TD>
    <TD  class= input>
      <Input class='coolDatePicker' dateFormat='short' name=OutWorkDate >
    </TD>    
  </TR>
  <TR  class= common>
    <TD  class= title>
      入司起期
    </TD>
    <TD  class= input>
      <Input class='coolDatePicker' dateFormat='short' name=EmployeeDateS >
    </TD>
    <TD  class= title>
      入司止期
    </TD>
    <TD  class= input>
      <Input class='coolDatePicker' dateFormat='short' name=EmployeeDateE >
    </TD>   
  </TR>
  <TR  class= common>
    <TD  class= title>
      人员状态
    </TD>
    <TD  class= input>      
      <Input class="code" name=EmployeeCodeState  CodeData="0|^0|在职^1|离职" 
            ondblclick="showCodeListEx('EmployeeCodeState',[this],[0]);"  onkeyup="showCodeListKeyEx('EmployeeCodeState',[this],[0]);">
    </TD>
    <TD  class= title>
      高管标志
    </TD>
    <TD  class= input>
      <Input class="code" name=ManageFlag verify="高管标志|code:ManageFlag&NOTNULL"  CodeData="0|^0|普通员工^1|高级管理人员" 
            ondblclick="showCodeListEx('ManageFlag',[this],[0]);"  onkeyup="showCodeListKeyEx('ManageFlag',[this],[0]);">

    </TD>
  </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询" TYPE=button class=common onclick="easyQueryClick();">
        <INPUT VALUE="返回" TYPE=button class=common onclick="returnParent();"> 	
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAEmployee1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLAEmployee1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLAEmployeeGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
