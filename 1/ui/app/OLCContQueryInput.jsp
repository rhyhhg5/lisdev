<%
//程序名称：OLCContQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 16:01:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./OLCContQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./OLCContQueryInit.jsp"%>
  <title>总单(合同)信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./OLCContQuerySubmit.jsp" method=post name=fm target="fraSubmit">
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
  <table  class= common>
  <TR  class= common>
  <!--  <TD  class= title>
      总单投保单号码
    </TD>-->
      <Input class= common name=ProposalNo type="hidden">
    <TD  class= title>
      印刷号码
    </TD>
    <TD  class= input>
      <Input class= common name=PrtNo >
    </TD>
    <TD  class= title>
      管理机构
    </TD>
    <TD  class= input>
      <Input class= common name=ManageCom ondblclick="return showCodeList('Station',[this]);" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      销售渠道
    </TD>
    <TD  class= input>
      <Input class= common name=SaleChnl ondblclick="return showCodeList('SaleChnl',[this]);" >
    </TD>
    <TD  class= title>
      代理机构
    </TD>
    <TD  class= input>
      <Input class= common name=AgentCom ondblclick="return showCodeList('AgentCom',[this]);" >
    </TD>
    <TD  class= title>
      代理人编码
    </TD>
    <TD  class= input>
      <Input class= common name=AgentCode ondblclick="return showCodeList('AgentCode',[this]);" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      投保人客户号
    </TD>
    <TD  class= input>
      <Input class= common name=GrpNo >
    </TD>
    <TD  class= title>
      投保人名称
    </TD>
    <TD  class= input>
      <Input class= common name=Name >
    </TD>
    <TD  class= title>
      投保人类型
    </TD>
    <TD  class= input>
      <Input class= common name=ContType ondblclick="return showCodeList('ContType',[this]);" >
    </TD>
  </TR>
</table>

    </table>
          <INPUT VALUE="查询" TYPE=button onclick="submitForm();return false;"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			 总单(合同)信息结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button > 
      <INPUT VALUE="上一页" TYPE=button > 					
      <INPUT VALUE="下一页" TYPE=button > 
      <INPUT VALUE="尾页" TYPE=button > 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
