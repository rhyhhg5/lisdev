<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：
//程序功能：外包批次清单查询
//创建日期：2008-1-29 03:40下午
//创建人  ：YangYalin
//更新记录：更新人    更新日期     更新原因/内容
%>

<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	String tCurrentDate = PubFun.getCurrentDate();
%>                            

<script language="JavaScript">
  
  var tManageCom = "<%=tGI.ComCode%>";
  var tCurrentDate = "<%=tCurrentDate%>";

function initForm()
{
  try
  {
    initInputBox();
    initBatchGrid();
    initContGrid();
    
    initElementtype();
  }
  catch(ex)
  {
    alert("初始化界面错误，" + ex.message);
  }
}

//外包批次信息列表的初始化
function initBatchGrid()
{
  var iArray = new Array();
      
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=20;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="外包批次号";         		//列名
    iArray[1][1]="120px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="BPOBatchNo";

    iArray[2]=new Array();
    iArray[2][0]="发送日期";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=80;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="SendOutDate"; 

    iArray[3]=new Array();
    iArray[3][0]="发送时间";         		//列名
    iArray[3][1]="40px";            		//列宽
    iArray[3][2]=80;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="SendOutTime"; 

    iArray[4]=new Array();
    iArray[4][0]="扫描件份数";         		//列名
    iArray[4][1]="40px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[4][21]="DocNumber";

    iArray[5]=new Array();
    iArray[5][0]="接收日期";         		//列名
    iArray[5][1]="50px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[5][21]="ReceiveDate";

    iArray[6]=new Array();
    iArray[6][0]="接收时间";         		//列名
    iArray[6][1]="40px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[6][21]="ReceiveTime";

    iArray[7]=new Array();
    iArray[7][0]="返回份数";         		//列名
    iArray[7][1]="30px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[7][21]="DocNumberBack";

    iArray[8]=new Array();
    iArray[8][0]="状态代码";         		//列名
    iArray[8][1]="50px";            		//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[8][21]="State";

    iArray[9]=new Array();
    iArray[9][0]="状态";         		//列名
    iArray[9][1]="35px";            		//列宽
    iArray[9][2]=100;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[9][21]="StateName";
    
    BatchGrid = new MulLineEnter( "fm" , "BatchGrid" ); 
    //这些属性必须在loadMulLine前
    BatchGrid.mulLineCount = 0;   
    BatchGrid.displayTitle = 1;
    BatchGrid.locked = 1;
    BatchGrid.canSel = 1;
    BatchGrid.canChk = 0;
    BatchGrid.hiddenPlus=1;   
  	BatchGrid.hiddenSubtraction=1;
  	//BatchGrid.selBoxEventFuncName = "queryContInfo";
    BatchGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("初始化" + ex.message);
  }
}

//外包录入投保单信息列表
function initContGrid()
{
  var iArray = new Array();
      
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=20;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="外包批次号";         		//列名
    iArray[1][1]="120px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="BPOBatchNo";

    iArray[2]=new Array();
    iArray[2][0]="印刷号";         		//列名
    iArray[2][1]="70px";            		//列宽
    iArray[2][2]=80;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="PrtNo"; 

    iArray[3]=new Array();
    iArray[3][0]="工作流任务ID";         		//列名
    iArray[3][1]="0px";            		//列宽
    iArray[3][2]=80;            			//列最大值
    iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="MissionID"; 

    iArray[4]=new Array();
    iArray[4][0]="扫描机构";         		//列名
    iArray[4][1]="50px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[4][21]="ManageCom";
    
    iArray[5]=new Array();
    iArray[5][0]="状态";         		//列名
    iArray[5][1]="40px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[5][21]="State";

    iArray[6]=new Array();
    iArray[6][0]="错误信息";         		//列名
    iArray[6][1]="200px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[6][21]="ErrorInfo";
    
    ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
    //这些属性必须在loadMulLine前
    ContGrid.mulLineCount = 0;   
    ContGrid.displayTitle = 1;
    ContGrid.locked = 1;
    ContGrid.canSel = 0;
    ContGrid.canChk = 0;
    ContGrid.hiddenPlus=1;   
  	ContGrid.hiddenSubtraction=1;
    ContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("初始化" + ex.message);
  }
}

</script>