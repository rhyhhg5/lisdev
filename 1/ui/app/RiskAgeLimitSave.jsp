<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输入参数
	RiskAgeLimitUI tRiskAgeLimitUI = new RiskAgeLimitUI();
	
	
	String[] tGridNo = request.getParameterValues("PolGridNo"); // 得到 MulLine 中序号列的所有值
	String[] tRiskcode = request.getParameterValues("PolGrid1");// 得到第1列的所有值,也就是险种号
	String[] tMaxage = request.getParameterValues("PolGrid3");//得到第3列的所有值,也就是最大年龄限制
	String[] tMinage = request.getParameterValues("PolGrid4");// 得到第4列的所有值,也就最小年龄限制
	
	String nafterMaxage=request.getParameter("afterMaxage");
	String nafterMinage=request.getParameter("afterMinage");
	
	
	
	
	

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String aMinage="";//存原最小年龄
	String aMaxage="";//存原最大年龄
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();

	// 从 mulline 中取得的是一列的值,所以遍历取出一行的值,然后把参数放入 TransferData 容器?
	for (int i=0; i< tGridNo.length;i++){
		if("1".equals(tGridNo[i])){
		transferData.setNameAndValue("Riskcode", tRiskcode[i]);
		 aMinage=tMinage[i];
		 aMaxage=tMaxage[i];
		break;
		}
	}
	if((!"".equals(nafterMaxage)&&nafterMaxage != null) && ("".equals(nafterMinage)||nafterMinage == null)){
		
		transferData.setNameAndValue("afterMaxage",nafterMaxage);
		transferData.setNameAndValue("afterMinage", aMinage);
		
	}
	else if((!"".equals(nafterMinage)&&nafterMinage != null) && ("".equals(nafterMaxage)||nafterMaxage == null)){
		transferData.setNameAndValue("afterMinage",nafterMinage);
		transferData.setNameAndValue("afterMaxage", aMaxage);
	}
	else if((!"".equals(nafterMinage)&&nafterMinage != null) && (!"".equals(nafterMaxage)&&nafterMaxage != null)){
		transferData.setNameAndValue("afterMaxage",nafterMaxage);
		transferData.setNameAndValue("afterMinage",nafterMinage);
	}
	
	
	if (!"Fail".equals(FlagStr)) {
		// 准备向后台传输数据 VData
		VData tVData = new VData();// Vector List 实现类之一,支持线程的同步
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			tRiskAgeLimitUI.submitData(tVData, "");// 提交数据和要进行的操作
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = tRiskAgeLimitUI.mErrors;
			if (!tError.needDealError()) {
				Content = "修改成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>