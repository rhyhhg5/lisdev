<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：ScanContInit.jsp
	//程序功能：个单新契约扫描件保单录入
	//创建日期：2004-12-22 11:10:36
	//创建人  ：HYQ
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
	
%>
<script language="JavaScript">

// 输入框的初始化（单记录部分）                         

function initForm()
{
  try
  {   
        var arrResult;
        fm.all('ManageCom').value = <%=strManageCom%>;
    	if(fm.all('ManageCom').value!=null)
        {
    	    arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
            if (arrResult != null) {
                fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
    	arrResult = easyExecSql("select current date - 3 Month,current date from dual where 1 = 1 ");
        if (arrResult != null) {
            fm.all('StartDate').value = arrResult[0][0];
    	    fm.all('EndDate').value = arrResult[0][1];
        }
    	initContGrid();
   }catch(re)
  {
    alert("GetContTaxCodeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  
}

// 保单信息列表的初始化
function initContGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      var i=0;
      iArray[i]=new Array();
      iArray[i][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[i][1]="30px";            		//列宽
      iArray[i][2]=10;            			//列最大值
      iArray[i][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      i++;
      iArray[i]=new Array();
      iArray[i][0]="保单号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[i][1]="40px";            		//列宽
      iArray[i][2]=10;            			//列最大值
      iArray[i][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      i++;
      iArray[i]=new Array();
      iArray[i][0]="印刷号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[i][1]="40px";            		//列宽
      iArray[i][2]=10;            			//列最大值
      iArray[i][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      i++;  
      iArray[i]=new Array();
      iArray[i][0]="投保人姓名";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[i][1]="30px";            		//列宽
      iArray[i][2]=10;            			//列最大值
      iArray[i][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      i++;
      iArray[i]=new Array();
      iArray[i][0]="税优识别码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[i][1]="40px";            		//列宽
      iArray[i][2]=10;            			//列最大值
      iArray[i][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      i++;
      iArray[i]=new Array();
      iArray[i][0]="获取成功标识";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[i][1]="25px";            		//列宽
      iArray[i][2]=10;            			//列最大值
      iArray[i][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      i++;
      iArray[i]=new Array();
      iArray[i][0]="获取失败原因";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[i][1]="120px";            		//列宽
      iArray[i][2]=10;            			//列最大值
      iArray[i][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
      //这些属性必须在loadMulLine前
      ContGrid.mulLineCount = 3;   
      ContGrid.displayTitle = 1;
      ContGrid.locked = 1;
      ContGrid.canSel = 1;
      ContGrid.canChk = 0;
      ContGrid.hiddenSubtraction = 1;
      ContGrid.hiddenPlus = 1;
      ContGrid.loadMulLine(iArray);  
      
      
      }
      catch(ex)
      {
        alert(ex);        
      }
}
</script>
