<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolApproveSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.brieftb.ContInputAgentcomChkBL"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	            
	TransferData mTransferData = new TransferData();
  mTransferData.setNameAndValue("GrpContNo",request.getParameter("ProposalGrpContNo"));
  mTransferData.setNameAndValue("PrtNo",request.getParameter("PrtNo"));
  mTransferData.setNameAndValue("SaleChnl",request.getParameter("SaleChnl"));
  mTransferData.setNameAndValue("ManageCom",request.getParameter("ManageCom"));
  mTransferData.setNameAndValue("GrpName",request.getParameter("GrpName"));
  mTransferData.setNameAndValue("CValiDate",request.getParameter("CValiDate"));
  mTransferData.setNameAndValue("MissionID",request.getParameter("MissionID"));
  mTransferData.setNameAndValue("SubMissionID",request.getParameter("SubMissionID"));
 
	System.out.println("GrpContNo   =="+request.getParameter("ProposalGrpContNo"));
	System.out.println("PrtNo       =="+request.getParameter("PrtNo"));
	System.out.println("SaleChnl    =="+request.getParameter("SaleChnl"));
	System.out.println("ManageCom   =="+request.getParameter("ManageCom"));
	System.out.println("GrpName     =="+request.getParameter("GrpName"));
	System.out.println("CValiDate   =="+request.getParameter("CValiDate"));
	System.out.println("MissionID   =="+request.getParameter("MissionID"));
	System.out.println("SubMissionID=="+request.getParameter("SubMissionID"));
  	//接收信息
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    String tProposalGrpContNo = request.getParameter("ProposalGrpContNo");
    String flag=request.getParameter("Flag1");
     
	if( tProposalGrpContNo!= null&flag!=null )
	{    
	 System.out.println("jsp中GrpPolNo:"+tProposalGrpContNo);

	// 准备传输数据 VData
	VData tVData = new VData();
	tVData.add( mTransferData );
	tVData.add( tG );
	
	// 数据传输
	GrpTbWorkFlowUI tGrpTbWorkFlowUI= new GrpTbWorkFlowUI();
	//江苏团险校验
	boolean tJSFlag = false;
	{
		String tSaleChnl = request.getParameter("SaleChnl");
		String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
				  	+ "and code = '"+tSaleChnl+"'";
		SSRS tSSRS = new ExeSQL().execSQL(tSql);
		String tSubManageCom = request.getParameter("ManageCom").substring(0,4);
		if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(tSubManageCom)){
			ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
	    	TransferData tJSTransferData = new TransferData();
	    	tJSTransferData.setNameAndValue("ContNo",request.getParameter("GrpContNo"));
	    	VData tJSVData = new VData();
	    	tJSVData.add(tJSTransferData);
	    	if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
	    		Content = tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
	    		FlagStr = "Fail";
	    		tJSFlag = true;
	    	}
		}
	}
	if(!tJSFlag){
		if (tGrpTbWorkFlowUI.submitData( tVData, "0000002001" ) == false){
			Content = " 复核失败，原因是: " + tGrpTbWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail"){
	    	tError = tGrpTbWorkFlowUI.mErrors;
	    	if (!tError.needDealError()){                          
	    		Content = " 复核成功! ";
	    		FlagStr = "Succ";
	    	}else{
	    		Content = " 复核失败，原因是:" + tError.getFirstError();
	    		FlagStr = "Fail";
	    	}
		}
	}
  }
%>                      
<html>
<script language="javascript">
try {
        
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>")
	} catch(ex) {}
</script>
</html>
