<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：XJGDownload.jsp
//程序功能：
//创建日期：2011-11-29
//创建人  ：fatman
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="./XJGDownload.js"></SCRIPT>
	<%@include file="./XJGDownloadInit.jsp"%>
</head>

<body onload="initElementtype();initForm();">    
  <form action="./XJGDownloadSave.jsp"  name="fm" target="fraSubmit" method="post" >
    <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">新疆共保</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divCessGetData" style= "display: ''">
	    <table class= common border=0 width=100%>
	    <TR  class= common>
				<TD  class= title8>
      			管理机构
    			</TD>
    			<TD  class= input8>
      				<Input class="code" style="width:60" name=ManageCom readonly="readonly" /><input class="codename" name=ManageComName readonly="readonly" />
    			</TD>
	         
	       
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          
	        </TR>
	        
	    </table>
	    <INPUT class=cssButton  VALUE="整单查询" TYPE=button onClick="easyQueryClick();">
        
		</Div> 
        
        <br />
        
		<div style="width:200">
	    	<table class="common">
	      		<tr class="common">
	      			<td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divResult);"></td>
	      			<td class="titleImg">整单数据</td>
	      		</tr>
	    	</table>
		</div>
		<div id="divResult" style="display:''">
    		<table class="common">
        		<tr class="common">
    	  			<td text-align:left colSpan="1">
					 	<span id="spanBillDetailGrid" >
					 </span> 
				  </td>
			 	</tr>
			</table>   
  		</div>
  		<div id= "div1" align="center" style= "display: '' ">
			<input value="首  页" class="cssButton" type="button" onclick="turnPage1.firstPage();" /> 
	    	<input value="上一页" class="cssButton" type="button" onclick="turnPage1.previousPage();" /> 					
	    	<input value="下一页" class="cssButton" type="button" onclick="turnPage1.nextPage();" /> 
	    	<input value="尾  页" class="cssButton" type="button" onclick="turnPage1.lastPage();" />  
		</div>
        
		<input class=cssButton  VALUE="整单下载" TYPE=button id="btnContDownload" onClick="downLoadCont();">	
		<input class=cssButton  VALUE="明细下载" TYPE=button id="btnContDetailDownload" onClick="downLoadContDetail();">
        
        <br />
        
        <hr />

        <div>
            <font color="red">
                <ul>操作说明：
                    <li>第一步：选择时间范围后，进行查询</li>
                    <li>第二步：点击【整单下载】，可下载承保保单信息数据</li>
                    <li>第三步：点击【明细下载】，可下载承保保单所对应的被保人信息数据</li>
                </ul>
                <ul>统计口径：
                    <li>【起始日期】【终止日期】：保单签单时间</li>
                    <li>管理机构为：86650105-新疆红山营销部</li>
                </ul>
            </font>
        </div>
        
		<input type="hidden" name="OperateType" />
        
        <input type="hidden" name="qryContSQL" />
        <input type="hidden" name="qryInsuSQL" />
  		<input type="hidden" name="qrySQL" />
  </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 