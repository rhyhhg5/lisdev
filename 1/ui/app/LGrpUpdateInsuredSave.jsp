 <%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
<%
CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	
	//获得公共输入信息
	tG=(GlobalInput)session.getValue("GI");
	transact = request.getParameter("fmtransact"); 
	System.out.println("transact: " + transact);
	LCGrpSubInsuredImportSchema mLCGrpSubInsuredImportSchema = new LCGrpSubInsuredImportSchema();
	
	mLCGrpSubInsuredImportSchema.setSeqNo(request.getParameter("SeqNo"));
	mLCGrpSubInsuredImportSchema.setGrpContNo(request.getParameter("GrpContNo"));
	mLCGrpSubInsuredImportSchema.setPrtNo(request.getParameter("PrtNo"));
	mLCGrpSubInsuredImportSchema.setName(request.getParameter("Name"));     
	mLCGrpSubInsuredImportSchema.setSex(request.getParameter("Sex"));       
	mLCGrpSubInsuredImportSchema.setBirthday(request.getParameter("Birthday"));  
	mLCGrpSubInsuredImportSchema.setIDType(request.getParameter("IDType"));
	mLCGrpSubInsuredImportSchema.setIDNo(request.getParameter("IDNo"));     
	mLCGrpSubInsuredImportSchema.setIDStartDate(request.getParameter("IDStartDate"));       
	mLCGrpSubInsuredImportSchema.setIDEndDate(request.getParameter("IDEndDate"));  
	mLCGrpSubInsuredImportSchema.setOthIDType(request.getParameter("OthIDType"));
	mLCGrpSubInsuredImportSchema.setOthIDNo(request.getParameter("OthIDNo"));     
	mLCGrpSubInsuredImportSchema.setContPlanCode(request.getParameter("ContPlanCode"));       
	mLCGrpSubInsuredImportSchema.setOccupationCode(request.getParameter("OccupationCode"));  
	mLCGrpSubInsuredImportSchema.setOccupationType(request.getParameter("OccupationType"));
	mLCGrpSubInsuredImportSchema.setRetire(request.getParameter("Retire"));     
	mLCGrpSubInsuredImportSchema.setEmployeeName(request.getParameter("EmployeeName"));       
	mLCGrpSubInsuredImportSchema.setRelation(request.getParameter("Relation"));  
	mLCGrpSubInsuredImportSchema.setBankCode(request.getParameter("BankCode"));
	mLCGrpSubInsuredImportSchema.setBankAccNo(request.getParameter("BankAccNo"));     
	mLCGrpSubInsuredImportSchema.setAccName(request.getParameter("AccName"));       

	// 准备传输数据 VData
	VData tVData = new VData();  	
	tVData.add(mLCGrpSubInsuredImportSchema);
	tVData.add(tG);
	
	LGrpUpdateInsuredUI tLGrpUpdateInsuredUI = new LGrpUpdateInsuredUI();
  	if (tLGrpUpdateInsuredUI.submitData(tVData, transact) == false)
	{
		FlagStr = "Fail";
		Content = tLGrpUpdateInsuredUI.mErrors.getErrContent();
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据保存成功！";
	}  
	
	Content = PubFun.changForHTML(Content);
%>
<html>
<script language="javascript">	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>
