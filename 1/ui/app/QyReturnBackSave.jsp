<%
//程序名称：ReProposalPrintInput.jsp
//程序功能：
//创建日期：2002-11-25
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
	String tContNo = request.getParameter("mContNo");
	String tGetpolDate = request.getParameter("mGetpolDate");
	String tGetpolMan = request.getParameter("mGetpolMan");
	String tSendPolMan=request.getParameter("mSendPolMan");
	String tContType=request.getParameter("ContType");
	String strOperation = request.getParameter("fmtransact");
	String Content = "";

	GlobalInput globalInput = new GlobalInput();

	if( (GlobalInput)session.getValue("GI") == null )
	{
		Content = "网页超时或者是没有操作员信息，请重新登录";
	}
	else
	{
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
	}

	if( tContNo == null ) {
		Content = "没有输入需要的打印参数";
	}
	LCContGetPolSchema tLCContGetPolSchema=new LCContGetPolSchema();
	QyReturnBackUI tQyReturnBackUI =new QyReturnBackUI();
	tLCContGetPolSchema.setContNo(tContNo);
    tLCContGetPolSchema.setGetpolDate(tGetpolDate);
	tLCContGetPolSchema.setGetpolMan(tGetpolMan);
	tLCContGetPolSchema.setSendPolMan(tSendPolMan);
	tLCContGetPolSchema.setContType(tContType);

	VData vData = new VData();
	vData.addElement(tLCContGetPolSchema);
	vData.add(globalInput);
	
	
	try {
		if( !tQyReturnBackUI.submitData(vData, strOperation) )
		{
	   		if ( tQyReturnBackUI.mErrors.needDealError() )
	   		{
	   			Content = tQyReturnBackUI.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		Content = "保存失败，但是没有详细的原因";
			}
		}
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		Content = ex.getMessage();
	}

	String FlagStr = "";
	if(Content.equals("")) 
	{
		FlagStr = "Succ";
		Content = "保存成功";
	} 
	else 
	{
		FlagStr = "Fail";
	}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>