<%@page import="com.sinosoft.utility.*"%>
<%
	GlobalInput globalInput = (GlobalInput) session.getValue("GI");
	String strManageCom = globalInput.ManageCom;
	String strOperator = globalInput.Operator;
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{
    try
    {
        // 初始化
        fm.all('UserCode').value = '';
        fm.all('UserName').value = '';
        fm.all('ManageCom').value = '<%=strManageCom%>';
        fm.all('fmtransact').value = '';    
    }
    catch(ex)
    {
        alert("在UpUserpasswordInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
  try
  {
  	initInpBox();
	initPolGrid(); 
  }
  catch(ex)
  {
    alert("在UpUserpasswordInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var PolGrid;          //定义为全局变量，提供给displayMultiline使用

// 保单信息列表的初始化
function initPolGrid()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="用户编码";         			//列名
      iArray[1][1]="82px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="用户姓名";         			//列名
      iArray[2][1]="82px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="所属机构";         		//列名
      iArray[3][1]="72px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="用户描述";         			//列名
      iArray[4][1]="70px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="用户上级";         			//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
         
 

      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 10;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canChk = 0;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.selBoxEventFuncName ="Polinit";
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray); 
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>
