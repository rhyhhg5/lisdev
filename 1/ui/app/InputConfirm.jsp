<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.brieftb.*" %>
  <%@page import="com.sinosoft.lis.cbcheck.*" %>
<%
//输出参数
CErrors tError = null;
String tRela  = "";                
String FlagStr="";
String Content = "";
String tAction = "";
String tOperate = "";
String wFlag = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
    VData tVData = new VData();
    //工作流操作型别，根据此值检索活动ID，取出服务类执行具体业务逻辑
    wFlag = request.getParameter("WorkFlowFlag");
    TransferData mTransferData = new TransferData();
    String tContNo=request.getParameter("ProposalContNo");
    String tPrtNo=request.getParameter("PrtNo");
    String tMissionID=request.getParameter("MissionID");
    String tSubMissionID=request.getParameter("SubMissionID");
    mTransferData.setNameAndValue("ContNo", tContNo);
    mTransferData.setNameAndValue("PrtNo", tPrtNo);
    mTransferData.setNameAndValue("AppntNo", request.getParameter("AppntNo"));
    mTransferData.setNameAndValue("AppntName",request.getParameter("AppntName"));
    mTransferData.setNameAndValue("AgentCode",request.getParameter("AgentCode"));
    mTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
    mTransferData.setNameAndValue("Operator",tG.Operator);
    mTransferData.setNameAndValue("MakeDate",PubFun.getCurrentDate());
    mTransferData.setNameAndValue("MissionID",tMissionID);  
    mTransferData.setNameAndValue("SubMissionID",tSubMissionID);
    
    System.out.println(request.getParameter("PrtNo"));
    tVData.add(mTransferData);
    tVData.add(tG);
    System.out.println("wFlag="+wFlag);
    System.out.println("-------------------start workflow---------------------");
    TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
    //江苏中介机构校验
   String  tManageCom= request.getParameter("ManageCom").substring(0,4);  
   ExeSQL mExeSQL=new ExeSQL();
    boolean tFlag=false;
    if(tManageCom.equals("8632")){     
      // String  tmContNo = request.getParameter("ContNo"); 
       String  tSaleChnl = request.getParameter("SaleChnl");   
       String  tSql="select code from ldcode where codetype='JSZJsalechnl' and code='"+tSaleChnl +"' ";
       SSRS tSSRS=mExeSQL.execSQL(tSql);
        int t= tSSRS.MaxRow;
        if(tSSRS!=null&&t>0){
        for(int i =1; i<=t;i++){
             if(tSaleChnl.equals(tSSRS.GetText(1,i))){
            ContInputAgentcomChkBL tContInputAgentcomChkBL =new ContInputAgentcomChkBL();
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("ContNo",tContNo );
            VData tmVData=new VData();
            tVData.add(tTransferData);
           if(!tContInputAgentcomChkBL.submitData(tVData,"check")){
               Content = tContInputAgentcomChkBL.mErrors.getError(0).errorMessage;
    		  FlagStr = "Fail";
                  tFlag=true;
              }
             }
        }//for
        }
    } //end of managecom 
    
    boolean mixFlag= false;
    TransferData mixTransferData = new TransferData();
     mixTransferData.setNameAndValue("ContNo",tContNo );
     mixTransferData.setNameAndValue("tPrtNo",tPrtNo );
    VData mixVData=new VData();
    mixVData.add(mixTransferData);
    MixCheckComUI tMixCheckComUI = new MixCheckComUI();//调用处理类
     if( !tMixCheckComUI.submitData( mixVData, "check" ) ) {
            if(!"".equals(Content)){
                 Content +=" <br> ";
            }
		    Content += "交叉销售信息有误："+tMixCheckComUI.mErrors.getError(0).errorMessage;
		    FlagStr = "Fail";
		    mixFlag = true;
	  }
    
  if(!tFlag&&!mixFlag){   
  if( !tTbWorkFlowUI.submitData( tVData, wFlag ) ) {
      Content = " 复核确认失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
  }else {
      if("0000008001".equals(wFlag) || "0000009001".equals(wFlag))
      {
        Content = " 录入成功！";
      }
      else if("0000008002".equals(wFlag) || "0000009002".equals(wFlag))
      {
        Content = " 复核完毕！";
      }
      FlagStr = "Succ";
      if("0000001001".equals(wFlag)){
       tVData.clear();
       TransferData pTransferData = new TransferData();
       pTransferData.setNameAndValue("ContNo", tContNo);
       pTransferData.setNameAndValue("PrtNo", tPrtNo);
       pTransferData.setNameAndValue("MissionID", tMissionID);
       pTransferData.setNameAndValue("SubMissionID", tSubMissionID);
       tVData.add(pTransferData);
       tVData.add(tG);
       UWPadFeeUI tUWPadFeeUI = new UWPadFeeUI();
       tUWPadFeeUI.submitData(tVData, "Fee");
      }
      tVData.clear();
      TransferData ttTransferData = new TransferData();
      ttTransferData.setNameAndValue("ContNo", tContNo);
      ttTransferData.setNameAndValue("PrtNo", tPrtNo);
      tVData.add(ttTransferData);
      RecordFlagBL tRecordFlagBL= new RecordFlagBL();
      tRecordFlagBL.submitData(tVData, "");
  }
  
 Content = PubFun.changForHTML(Content);
  }//end of !tFlag
 System.out.println("-------------------end workflow---------------------");
 if(wFlag.equals("0000001001"))
 {
%>
		<html>
		<script language="javascript">
		parent.fraInterface.afterSubmit1("<%=FlagStr%>","<%=Content%>");
		</script>
		</html>
<%
 }
 else
 {
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
<%
}
%>