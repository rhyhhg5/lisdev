var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartSignDate = fm.StartSignDate.value;
    var tEndSignDate = fm.EndSignDate.value;

	if(dateDiff(tStartSignDate, tEndSignDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}
	var tContainsPAD="";
	if(fm.ContainsPAD.value=="" || fm.ContainsPAD.value =="0"){
		tContainsPAD="";
	}else if(fm.ContainsPAD.value == "1"){
		tContainsPAD=" and lcc.prtno like 'PD%' ";
	}else if(fm.ContainsPAD.value == "2"){
		tContainsPAD=" and lcc.prtno not like 'PD%' ";
	}

    var tStrSQL = ""
        + " select "
        + " tmp.ManageCom, tmp.SaleChnl, tmp.BranchAttr, tmp.Name, getUniteCode(tmp.AgentCode), "
        + " tmp.AgentName, tmp.ContNo, tmp.PrtNo, tmp.AppntName, tmp.AppntSex, "
        + " tmp.AppntBirthday, tmp.InsuredName, tmp.InsuredSex, "
        + " tmp.InsuredBirthday, tmp.InsuredAppAge, tmp.RiskType3, "
        + " tmp.RiskType4, tmp.RiskCode,(select riskname from lmrisk where riskcode = tmp.RiskCode), tmp.PayIntv, tmp.PayEndYear, tmp.PayEndYearFlag, "
        + " tmp.EndDate, tmp.PayToDate, tmp.InsuYear, tmp.InsuYearFlag, "
        + " tmp.Mult, tmp.Amnt, tmp.Prem, tmp.SupplementaryPrem, "
        + " tmp.PolApplyDate, tmp.CValidate, tmp.FirstTrialOperator, "
        + " tmp.InputDate, tmp.InputOperator, tmp.ApproveDate, tmp.ApproveCode, "
        + " tmp.UWCode, tmp.UWFlag, tmp.UWDate, tmp.PayMode, "
        + " tmp.FirstPayDate, tmp.SignDate, "
        + " (case when tmp.ContAppFlag is null then '银保通撤单' else tmp.ContAppFlag end) ContAppFlag "
        + " from "
        + " ("
        + " select "
        + " lcc.ManageCom, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " labg.BranchAttr, labg.Name, lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " lcc.AppntName, "
        + " CodeName('sex', lcc.AppntSex) AppntSex, "
        + " lcc.AppntBirthday, lcp.InsuredName, "
        + " CodeName('sex', lcp.InsuredSex) InsuredSex, "
        + " lcp.InsuredBirthday, lcp.InsuredAppAge, "
        + " (select CodeName('risktype3', lmra.RiskType3) from LMRiskApp lmra where lmra.RiskCode = lcp.RiskCode) RiskType3, "
        + " (select CodeName('risktype4', lmra.RiskType4) from LMRiskApp lmra where lmra.RiskCode = lcp.RiskCode) RiskType4, "
        + " lcp.RiskCode, "
        + " CodeName('payintv', char(integer(lcp.PayIntv))) PayIntv, "
        + " lcp.PayEndYear, lcp.PayEndYearFlag, "
        + " lcp.EndDate, lcp.PayToDate, "
        + " lcp.InsuYear, lcp.InsuYearFlag, "
        + " lcp.Mult, lcp.Amnt, lcp.Prem, lcp.SupplementaryPrem, "
        + " lcc.PolApplyDate, lcp.CValidate, lcc.FirstTrialOperator, "
        + " lcc.InputDate, lcc.InputOperator, lcc.ApproveDate, lcc.ApproveCode, "
        + " lcp.UWCode, "
        + " CodeName('uwflag', lcp.UWFlag) UWFlag, "
        + " lcp.UWDate, "
        + " CodeName('paymode', char(integer(lcc.PayMode))) PayMode, "
        + " lcp.FirstPayDate, lcp.SignDate, "
        + " '承保' ContAppFlag, "
        + " '' "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.AppFlag = '1' "
        + " and length(trim(lcc.ManageCom)) = 8 "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + tContainsPAD
        + " union all "
        + " select "
        + " lcc.ManageCom, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " labg.BranchAttr, labg.Name, lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " lcc.AppntName, "
        + " CodeName('sex', lcc.AppntSex) AppntSex, "
        + " lcc.AppntBirthday, lcp.InsuredName, "
        + " CodeName('sex', lcp.InsuredSex) InsuredSex, "
        + " lcp.InsuredBirthday, lcp.InsuredAppAge, "
        + " (select CodeName('risktype3', lmra.RiskType3) from LMRiskApp lmra where lmra.RiskCode = lcp.RiskCode) RiskType3, "
        + " (select CodeName('risktype4', lmra.RiskType4) from LMRiskApp lmra where lmra.RiskCode = lcp.RiskCode) RiskType4, "
        + " lcp.RiskCode, "
        + " CodeName('payintv', char(integer(lcp.PayIntv))) PayIntv, "
        + " lcp.PayEndYear, lcp.PayEndYearFlag, "
        + " lcp.EndDate, lcp.PayToDate, "
        + " lcp.InsuYear, lcp.InsuYearFlag, "
        + " lcp.Mult, lcp.Amnt, lcp.Prem, lcp.SupplementaryPrem, "
        + " lcc.PolApplyDate, lcp.CValidate, lcc.FirstTrialOperator, "
        + " lcc.InputDate, lcc.InputOperator, lcc.ApproveDate, lcc.ApproveCode, "
        + " lcp.UWCode, "
        + " CodeName('uwflag', lcp.UWFlag) UWFlag, "
        + " lcp.UWDate, "
        + " CodeName('paymode', char(integer(lcc.PayMode))) PayMode, "
        + " lcp.FirstPayDate, lcp.SignDate, "
        + " (select (case lpei.EdorType when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' end) from LPEdorItem lpei where lpei.ContNo = lcc.ContNo and lpei.EdorType in ('CT', 'WT', 'XT') order by lpei.EdorAppDate desc fetch first rows only) ContAppFlag, "
        + " '' "
        + " from LCCont lcc "
        + " inner join LBPol lcp on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.AppFlag = '1' "
        + " and length(trim(lcc.ManageCom)) = 8 "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + tContainsPAD
        + " union all "
        + " select "
        + " lcc.ManageCom, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " labg.BranchAttr, labg.Name, lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " lcc.AppntName, "
        + " CodeName('sex', lcc.AppntSex) AppntSex, "
        + " lcc.AppntBirthday, lcp.InsuredName, "
        + " CodeName('sex', lcp.InsuredSex) InsuredSex, "
        + " lcp.InsuredBirthday, lcp.InsuredAppAge, "
        + " (select CodeName('risktype3', lmra.RiskType3) from LMRiskApp lmra where lmra.RiskCode = lcp.RiskCode) RiskType3, "
        + " (select CodeName('risktype4', lmra.RiskType4) from LMRiskApp lmra where lmra.RiskCode = lcp.RiskCode) RiskType4, "
        + " lcp.RiskCode, "
        + " CodeName('payintv', char(integer(lcp.PayIntv))) PayIntv, "
        + " lcp.PayEndYear, lcp.PayEndYearFlag, "
        + " lcp.EndDate, lcp.PayToDate, "
        + " lcp.InsuYear, lcp.InsuYearFlag, "
        + " lcp.Mult, lcp.Amnt, lcp.Prem, lcp.SupplementaryPrem, "
        + " lcc.PolApplyDate, lcp.CValidate, lcc.FirstTrialOperator, "
        + " lcc.InputDate, lcc.InputOperator, lcc.ApproveDate, lcc.ApproveCode, "
        + " lcp.UWCode, "
        + " CodeName('uwflag', lcp.UWFlag) UWFlag, "
        + " lcp.UWDate, "
        + " CodeName('paymode', char(integer(lcc.PayMode))) PayMode, "
        + " lcp.FirstPayDate, lcp.SignDate, "
        + " (select (case lpei.EdorType when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' end) from LPEdorItem lpei where lpei.ContNo = lcc.ContNo and lpei.EdorType in ('CT', 'WT', 'XT') order by lpei.EdorAppDate desc fetch first rows only) ContAppFlag, "
        + " '' "
        + " from LBCont lcc "
        + " inner join LBPol lcp on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.AppFlag = '1' "
        + " and length(trim(lcc.ManageCom)) = 8 "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + tContainsPAD
        + " ) as tmp "
        + " order by tmp.ManageCom, tmp.PrtNo, tmp.ContNo "
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "ContRiskCBDetailSave.jsp";
    fm.submit();
    fm.action = oldAction;

}