var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var i = 0;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
    var tSignStartDate = fm.SignStartDate.value;
    var tSignEndDate = fm.SignEndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>12)
	{
		alert("统计期最多为12个月！");
		return false;
	}
    var tManageCom = fm.ManageCom.value;
    var tSaleChnl = fm.SaleChnl.value;    
    var tTheContType = fm.TheContType.value;
    var strSQL = "select a.managecom,a.prtno,b.CardFlag || ' - ' || (case b.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end),a.makedate,b.customgetpoldate,getUniteCode(b.agentcode),(select name from laagent where agentcode =b.agentcode),c.branchattr,c.name "
             + " , case when b.customgetpoldate is not null then  to_date(b.customgetpoldate)-to_date(a.MakeDate) else 0 end "
             + "from lccontprint a, lccont b,labranchgroup c where a.filepath like '%.xml' and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' "
             + getWherePart('b.SaleChnl','SaleChnl')
             + " and not exists (select 1 from lbcont where prtno=b.prtno ) "
             + " and not exists (select 1 from lcrnewstatelog where prtno=b.prtno) "
             + " and b.managecom like '"+tManageCom+"%' "
             //保单签发日期 >= 保单打印日期 ，先注掉
//             + " and b.signdate>='"+tStartDate+"' "
             + " and ( b.customgetpoldate>='"+tStartDate+"' or b.customgetpoldate is null) and a.otherno=b.contno and b.agentgroup = c.agentgroup "; 
    if(tSignStartDate!="" && tSignStartDate!=null && tSignStartDate!="null"){
    	strSQL += " and b.signdate>='"+tSignStartDate+"' ";
    }
    if(tSignEndDate!="" && tSignEndDate!=null && tSignEndDate!="null"){
    	strSQL += " and b.signdate<='"+tSignEndDate+"' ";
    }
    if(tTheContType!="" && tTheContType!=null && tTheContType!="null"){
    	if(tTheContType=="0" || tTheContType=="9"){
    		strSQL += " and b.cardflag='"+tTheContType+"' ";
    	}
    	if(tTheContType=="99"){
    		strSQL += " and b.cardflag not in ('0','9') ";
    	}
    }
    strSQL += "order by a.managecom,c.branchattr,b.agentcode with ur";
    
    fm.AnalysisSql.value = strSQL;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}   
	fm.submit();
}