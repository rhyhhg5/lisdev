var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartMakeDate = fm.StartMakeDate.value;
    var tEndMakeDate = fm.EndMakeDate.value;

	if(dateDiff(tStartMakeDate, tEndMakeDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}
	var tContainsPAD="";
	if(fm.ContainsPAD.value=="" || fm.ContainsPAD.value =="0"){
		tContainsPAD="";
	}else if(fm.ContainsPAD.value == "1"){
		tContainsPAD=" and lcc.prtno like 'PD%' ";
	}else if(fm.ContainsPAD.value == "2"){
		tContainsPAD=" and lcc.prtno not like 'PD%' ";
	}

    var tStrSQL = ""
        + " select "
        + " tmp.ManageCom, tmp.ManageComName, tmp.CardType, "
        + " tmp.ContNo, tmp.PrtNo, tmp.SaleChnl, getUniteCode(tmp.AgentCode), "
        + " tmp.AgentName, tmp.BranchAttr, tmp.BranchName, "
        + " tmp.AppntName, tmp.AppntBirthday, tmp.AppntSex, "
        + " tmp.IDType, tmp.IDNo, tmp.NativePlace, tmp.Marriage, "
        + " tmp.OccupationCode, tmp.OccupationType, "
        + " tmp.CValidate, tmp.CInvalidate, tmp.PolApplyDate, "
        + " tmp.InputDate, tmp.ApproveDate, tmp.UWDate, "
        + " tmp.SignDate, tmp.CustomGetPolDate, tmp.GetPolDate, "
        + " tmp.PayMode, "
        + " sum(tmp.Prem) Prem, "
        + " sum(tmp.Amnt) Amnt, "
        + " count(distinct tmp.InsuredNo) InsuPeoples, "
        + " sum(tmp.PolPrem) ContPrem, "
        + " tmp.ContState, tmp.ContAppFlag,tmp.BQDate,tmp.AgentCom,tmp.AgentComName "
        + " from "
        + " ( "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " labg.BranchAttr, labg.Name BranchName, "
        + " lcc.AppntName, lcc.AppntBirthday, lcc.AppntSex, "
        + " CodeName('idtype', lca.IDType) IDType, "
        + " lca.IDNo, "
        + " CodeName('nativeplace', lca.NativePlace) NativePlace, "
        + " CodeName('marriage', lca.Marriage) Marriage, "
        + " lca.OccupationCode, "
        + " CodeName('occupationtype', lca.OccupationType) OccupationType, "
        + " lcc.CValidate, lcc.CInvalidate, "
        + " lcc.PolApplyDate, lcc.InputDate InputDate, lcc.ApproveDate, lcc.UWDate, "
        + " lcc.SignDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.Prem, lcp.Amnt, "
        + " lcp.InsuredNo, "
        + " lcp.Prem + nvl(lcp.SupplementaryPrem, 0) PolPrem, "
        + " ("
        + " case "
        + " when lcc.AppFlag = '1' then '已承保' "
        + " when lcc.AppFlag != '1' and lcc.UWFlag not in ('0', '5', 'z','a','1','2','8') then '待收费' "
        + " when lcc.AppFlag != '1' and lcc.UWFlag ='a' then '撤单' "
        + " when lcc.AppFlag != '1' and lcc.UWFlag ='1' then '谢绝承保' "
        + " when lcc.AppFlag != '1' and lcc.UWFlag in ('2','8') then '延期承保' "
        + " when lcc.AppFlag != '1' and lcc.UWFlag in ('0', '5', 'z') and lcc.ApproveFlag = '9' then '核保中' "
        + " else '复核中' "
        + " end "
        + " ) ContState, "
        + " (case lcc.AppFlag when '1' then '承保' else '投保' end) ContAppFlag, "
        + " '' BQDate, "
        + " lcc.AgentCom, (select name from lacom where agentcom = lcc.agentcom) AgentComName, "
        + " '' nulls "
        + " from LCCont lcc "
        + " inner join LCAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LCPol lcp on lcp.ContNo = lcc.ContNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and length(trim(lcc.ManageCom)) = 8  "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + tContainsPAD
        + " union all "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " labg.BranchAttr, labg.Name BranchName, "
        + " lcc.AppntName, lcc.AppntBirthday, lcc.AppntSex, "
        + " CodeName('idtype', lca.IDType) IDType, "
        + " lca.IDNo, "
        + " CodeName('nativeplace', lca.NativePlace) NativePlace, "
        + " CodeName('marriage', lca.Marriage) Marriage, "
        + " lca.OccupationCode, "
        + " CodeName('occupationtype', lca.OccupationType) OccupationType, "
        + " lcc.CValidate, lcc.CInvalidate, "
        + " lcc.PolApplyDate, lcc.InputDate InputDate, lcc.ApproveDate, lcc.UWDate, "
        + " lcc.SignDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.Prem, lcp.Amnt, "
        + " lcp.InsuredNo, "
        + " lcp.Prem + nvl(lcp.SupplementaryPrem, 0) PolPrem, "
        + " '已承保' ContState, "
        + " (select (case lpei.EdorType when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' end) from LPEdorItem lpei where lpei.ContNo = lcc.ContNo and lpei.EdorType in ('CT', 'WT', 'XT') order by lpei.EdorAppDate desc fetch first rows only) ContAppFlag, "
        + " (select varchar(max(lpea.ConfDate)) from LPEdorItem lpei inner join LPEdorApp lpea on lpea.EdorAcceptNo = lpei.EdorNo where lpei.ContNo = lcc.ContNo and lpei.EdorType in ('CT', 'WT', 'XT')) BQDate, "
        + " lcc.AgentCom, (select name from lacom where agentcom = lcc.agentcom) AgentComName, "
        + " '' nulls "
        + " from LBCont lcc "
        + " inner join LBAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LBPol lcp on lcp.ContNo = lcc.ContNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and length(trim(lcc.ManageCom)) = 8 "
        + " and lcc.EdorNo not like 'xb%' "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + tContainsPAD
        + " ) as tmp "
        + " group by tmp.ManageCom, tmp.ManageComName, tmp.CardType, "
        + " tmp.ContNo, tmp.PrtNo, tmp.SaleChnl, tmp.AgentCode, "
        + " tmp.AgentName, tmp.BranchAttr, tmp.BranchName, "
        + " tmp.AppntName, tmp.AppntBirthday, tmp.AppntSex, "
        + " tmp.IDType, tmp.IDNo, tmp.NativePlace, tmp.Marriage, "
        + " tmp.OccupationCode, tmp.OccupationType, "
        + " tmp.CValidate, tmp.CInvalidate, tmp.PolApplyDate, "
        + " tmp.InputDate, tmp.ApproveDate, tmp.UWDate, "
        + " tmp.SignDate, tmp.CustomGetPolDate, tmp.GetPolDate, "
        + " tmp.PayMode, tmp.ContState, tmp.ContAppFlag,tmp.AgentCom,tmp.AgentComName,tmp.BQDate"
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "ContTBDetailSave.jsp";
    fm.submit();
    fm.action = oldAction;

}