<%@ page contentType="text/html;charset=GBK"%>
<%@ include file="../common/jsp/UsrCheck.jsp"%>

<%
	//程序名称：
	//程序功能：
	//创建日期：2009-11-12
	//创建人  ：GUZG
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
	<head>
		<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
		<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
		<script src="../common/javascript/Common.js"></script>
		<script src="../common/cvar/CCodeOperate.js"></script>
		<script src="../common/javascript/MulLine.js"></script>
		<script src="../common/Calendar/Calendar.js"></script>
		<script src="../common/javascript/VerifyInput.js"></script>

		<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

		<script src="ModifyGetPolDateInput.js"></script>
		<%@include file="ModifyGetPolDateInit.jsp"%>
	</head>

	<body onload="initForm();">
		<form action="" method="post" name="fm" target="fraSubmit">
			<table>
				<tr>
					<td class="titleImg">
						请输入查询条件：
					</td>
				</tr>
			</table>

			<div id="divCondCode" style="display:''">
				<table class="common">
					<tr class="common">
						<td class="title8">
							保单号
						</td>
						<td class="input8">
							<input class="common" name="ContNo" verify="保单号|notnull" elementtype="nacessary" />
						</td>
						<TD class=title>销售提奖修改</TD>
       					<td class=input>
       						<Input class="codeno" name = SaleState verify="销售提奖修改|notnull"  CodeData = "0|^0|否|^1|是"
       		 				ondblclick = "return showCodeListEx('SaleState',[this,SaleStateName],[0,1]);"
       		 				onkeyup = "return showCodeListKeyEx('SaleState',[this,SaleStateName],[0,1]);"><Input class="codename" name= SaleStateName readonly=true elementtype="nacessary">
       		 			</td>
                        <td class="title8">
							指定薪资月
						</td>
						<td class="input8">
							<input class="common" name="WageNo" />
						</td>
					</tr>
				</table>
			</div>

			<table>
				<tr>
					<td class="common">
						<input class="cssButton" type="button" value=" 查  询 " onclick="queryRelationToAppnt();" />
					</td>
					<td class="common">
						<input class="cssButton" type="button" id="btnImport" value="修  改" onclick="submitData();" />
					</td>
				</tr>
			</table>
			
			<div id="divRelation" style="display:''">
				<table class="common">
					<tr class="common">
						<td class="title8">
							修改前回执回销日期：
						</td>
						<td class="input8">
							<input  name="GetPolDate" readonly = "true"/>
						</td>
						<td CLASS="title">
						    修改后回执回销日期: 
			    		</td>
						<td CLASS="input" COLSPAN="1">
							<input class="common" name="GetPolDateNew" elementtype=nacessary>  
			    		</td>  
                        <Input type='hidden' name=ManageCom readonly >
                        <Input type='hidden' name=tempManageCom readonly >
                        <Input type='hidden' name=tempSaleChnl readonly >
                        <td class="input8">&nbsp;</td>
					</tr>
				</table>
			</div>
			
			<br />
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
