<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-15 11:48:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%  
	String tPNo = "";
	try
	{
		tPNo = request.getParameter("PNo");

	}
	catch( Exception e )
	{ 
		tPNo = "";
	}
 
//得到界面的调用位置,默认为1,表示个人保单直接录入.
// 1 -- 个人投保单直接录入
// 2 -- 集体下个人投保单录入
// 3 -- 个人投保单明细查询
// 4 -- 集体下个人投保单明细查询
// 5 -- 复核
// 6 -- 查询
// 7 -- 保全新保加人
// 8 -- 保全新增附加险
// 9 -- 无名单补名单
// 10-- 浮动费率
// 99-- 随动定制

	String tLoadFlag = "";
	try
	{
		tLoadFlag = request.getParameter( "LoadFlag" );
		//默认情况下为个人保单直接录入
		if( tLoadFlag == null || tLoadFlag.equals( "" ))
			tLoadFlag = "1";
	}
	catch( Exception e1 )
	{
		tLoadFlag = "1";
	}
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
        System.out.println("LoadFlag:" + tLoadFlag);
%>
<script>
	var	tMissionID = "<%=request.getParameter("MissionID")%>";
	var ActivityID = "<%=request.getParameter("ActivityID")%>";
	var	tSubMissionID = "<%=request.getParameter("SubMissionID")%>"; 
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var ManageCom = "<%=request.getParameter("ManageCom")%>";
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var scantype = "<%=request.getParameter("scantype")%>";		
	var type = "<%=request.getParameter("type")%>";
	//保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
	var BQFlag = "<%=request.getParameter("BQFlag")%>";
	if (BQFlag == "null") BQFlag = "0";
	var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
	if (ScanFlag == "null") ScanFlag = "1";
	//保全调用会传险种过来
	var BQRiskCode = "<%=request.getParameter("riskCode")%>";
	var PolApplyDate = "<%=request.getParameter("PolApplyDate")%>";
	var DiskInputFlag = "<%=request.getParameter("DiskInputFlag")%>";
	//添加其它模块调用处理
	var LoadFlag ="<%=tLoadFlag%>"; //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
	var tsql=" 1 and code in (select code from ldcode where codetype=#paymodeind#) ";
	var tVideoFlag = "<%=request.getParameter("VideoFlag")%>";
	var tXSFlag = "<%=request.getParameter("XSFlag")%>";		
</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <script src="../common/javascript/CommonTools.js"></script>
    
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <%@include file="ScanCheckInit.jsp"%>
  <SCRIPT src="ScanCheckInput.js"></SCRIPT>
  <SCRIPT src="ProposalAutoMove.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

<% if (request.getParameter("type") == null) { %>
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
<!--<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>-->
<% } %>

 
</head>
<body  onload="initForm();initElementtype();initPayMode();" >
  <form action="./ContSave.jsp" method=post name=fm target="fraSubmit">

   <Div  id= "divButton" style= "display: ''">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
  </DIV>
    <!-- 合同信息部分 ContPage.jsp-->
     
    <%@include file="ContPage.jsp"%> 
    <%@include file="ComplexAppnt.jsp"%> 
    <DIV id=DivLCImpart STYLE="display:'none'">
    <!-- 告知信息部分（列表） -->
    <table>
        <tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
            </td>
            <td class= titleImg>
                投保人告知信息
            </td>
        </tr>
    </table>
    
    <Div  id= "divLCImpart1" style= "display: ''">
        <table  class= common>
            <tr  class= common>
                <td text-align: left colSpan=1>
                    <span id="spanImpartGrid" >
                    </span>
                </td>
            </tr>
        </table>
    </div>
    
    </DIV>   
    <br>
    <Div  id= "divInputContButton" style= "display: ''" style="float: right">
      <INPUT class=cssButton id="Donextbutton1" name=lrwb VALUE="录入完毕" TYPE=button onclick="inputConfirm(1);">                   
      <input type =button id="RiskInfoButton1"  class=cssButton value="复核险种信息" onclick="showComplexRiskInfo();">
      <INPUT class=cssButton id="Donextbutton3" VALUE="查看被保险人信息" TYPE=button onclick="intoInsured();">   
       <INPUT class=cssButton id="Donextbutton28" VALUE="医保单被保人信息查询" TYPE=button onclick="intoInsured1();">   
    </DIV>
   <br> 
	<font size=2 color="#ff0000">
	  <b>新、旧缴费方式对照说明</b> 
		<p></p>	
		&nbsp;&nbsp;1现金--->1自缴、3转账支票--->3其它、4银行转账---->4银行转账
		<p></p>
		&nbsp;&nbsp;11银行汇款--->11银行代收、12其他银行代收代付--->12银行代收-导入。
		<p></p>
    <DIV id = "divApproveContButton" style = "display:'none'" style="float: right">
    	<table class=common>
    		<tr class=common>
    		<td class=common>
    			<%-- <td class=common align="left">
			    	<input type =button class=cssButton value="复核险种信息" onclick="showComplexRiskInfo();"> 
		    		<INPUT class=cssButton id="Donextbutton6" VALUE="查看被保险人信息" TYPE=button onclick="intoInsured();"> 
		    		<input type =button class=cssButton value="受益人信息补录" onclick="intoBnfInfo();"> 
		    	</td>
			    <td class=common>
			    <input type=hidden  name="LoadFlag" value=<%=tLoadFlag %>>
		      	<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">  
		      </td> --%>
			    <td class=common>
		      <!-- 	<INPUT class=cssButton VALUE="查看外包错误"  TYPE=button onclick="return findIssue();">   -->
		      	<INPUT class=cssButton id="Donextbutton5" VALUE="问题件录入" TYPE=button onClick="QuestInput();">
		      </td>
			    <td class=common>
                <input class="cssButton" type="button" value="问题件回销" type="button" onclick="questBack();" />
<!-- 		      	<INPUT class=cssButton VALUE=投保人校验 TYPE=hidden onclick='AppntChk();'>
 -->		      </td>
			    <td class=common align="right">
<!-- 			        <INPUT class=cssButton VALUE="录音录像调阅"  TYPE=button onclick="getAudioAndVideo();" id="AudioAndVideo" style="display: none">
			        <INPUT  type= "hidden" class= Common name=VideoFlag>   
			        <INPUT class=cssButton VALUE="疑似客户检测"  TYPE=button onclick="return customerClick();"> -->
		    		<INPUT class=cssButton id="Donextbutton4" VALUE="复查完毕" TYPE=button onclick="inputConfirm(2);">   	
		    	</td>
		    </tr>
		   <!--  <tr>
		      <td>
		    	<INPUT class=cssButton id="Donextbutton10" VALUE="体检录入" TYPE=button onClick="showHealth();">
		      	<INPUT class=cssButton id="Donextbutton11" VALUE="体检回销" TYPE=button onClick="showHealthQ();">
		      	<INPUT class=cssButton id="Donextbutton13" VALUE="税优转入保单录入" TYPE=button onClick="TaxPolInfo();">
		      	<INPUT class=cssButton id="Donextbutton12" VALUE="税优唯一性验证" TYPE=button onClick="SYCheck();">
		      </td> -->
		    </tr>
		  </table>
    </DIV>
    
    <DIV id = "divchangplan" style = "display:'none'" style="float: right">
      <!--INPUT class=cssButton id="Donextbutton5" VALUE="问题件录入" TYPE=button onClick="QuestInput();"-->
      <input type =button class=cssButton value="复核险种信息" onclick="showComplexRiskInfo();"> 
		    		<INPUT class=cssButton id="Donextbutton6" VALUE="查看被保险人信息" TYPE=button onclick="intoInsured();"> 
      <INPUT class=cssButton VALUE=投保人校验 TYPE=hidden onclick='AppntChk();'>
      <INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">     
    <INPUT class=cssButton id="Donextbutton27" VALUE="医保单被保人信息查询" TYPE=button onclick="intoInsured1();"> 
    	<!--INPUT class=cssButton id="Donextbutton6" VALUE="查看被保险人信息" TYPE=button onclick="intoInsured();"--> 
    </DIV>
    <DIV id = "divApproveModifyContButton" style = "display:'none'" style="float: right">
    		<INPUT class=cssButton id="Donextbutton5" VALUE="问题件录入" TYPE=button onClick="QuestInput();">
          <input class="cssButton" type="button" value="问题件回销" type="button" onclick="questBack();" />
    	  <INPUT class=cssButton id="Donextbutton7" VALUE="复核修改完毕" TYPE=button onclick="inputConfirm(3);"> 
    	  <INPUT class=cssButton id="Donextbutton8" VALUE="保  存"  TYPE=button onclick="submitForm();">    
    	  <INPUT class=cssButton id="Donextbutton9" VALUE="查看被保险人信息" TYPE=button onclick="intoInsured();">   
    	  
    </DIV>
    <div id="autoMoveButton" style="display: none">
	<input type="button" name="autoMoveInput" value="随动定制确定" onclick="submitAutoMove('11');" class=cssButton>
	<input type="button" name="Next" value="查看被保险人信息" onclick="location.href='ContInsuredInput.jsp?LoadFlag='+LoadFlag+'&checktype=1&prtNo='+prtNo+'&scantype='+scantype" class=cssButton>	
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="top.close();">       
        <input type=hidden id="" name="autoMoveFlag">
        <input type=hidden id="" name="autoMoveValue">   
        <input type=hidden id="" name="pagename" value="11">                         
      </div>     
    
    <Div  id= "HiddenValue" style= "display:'none'" style="float: right"> 
    	<input type=hidden id="fmAction" name="fmAction">	
			<input type=hidden id="WorkFlowFlag" name="WorkFlowFlag">
			<INPUT  type= "hidden" class= Common name= MissionID value= ""><!-- 工作流任务编码 -->
      <INPUT  type= "hidden" class= Common name= SubMissionID value= "">
    </DIV>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
