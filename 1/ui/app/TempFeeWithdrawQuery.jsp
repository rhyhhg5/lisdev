<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：TempFeeWithdrawInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="TempFeeWithdrawQuery.js"></SCRIPT>
  <%@include file="TempFeeWithdrawQueryInit.jsp"%>
  
  <title>暂缴费退费实付收据号信息 </title>
</head>

<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 暂交费信息部分 fraSubmit-->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
  		</tr>
  	</table>
  	
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        印刷号
      </TD>
      <TD  class= input>
        <Input class=common name=PrtNo >
      </TD>
      <TD class=title>
      </TD>
      <TD class=input>
      </TD>
    </TR>
    </table>
    <table align=right>
      <TR>
        <TD>
          <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="tempFeeNoQuery();">
        </TD>
      </TR>
    </table>
    <br><br>
    
    <!-- 暂缴费退费实付收据号信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJTempFee1);">
    		</td>
    		<td class= titleImg>
    			 暂缴费退费实付收据号信息 
    		</td>
    	</tr>
    </table>
	<Div  id= "divLJTempFee1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanFeeGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
	
  <Div id= "divPage" align=center style= "display: 'none' ">
  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  <table align=right>
      <TR>
        <TD>
          <INPUT VALUE="打印实付凭证" class= cssButton TYPE=button onclick="prtLJAGet();">
         </TD>
      </TR>
    </table>   										
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <form action="./TempFeeWithdrawPrintMain.jsp" method=post name=fm2 target="_blank">
    <Input type=hidden name=PrtData >
  </form>
  
</body>
</html>
