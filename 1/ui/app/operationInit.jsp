<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ScanContInit.jsp
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('otherno').value = '';   
  }
  catch(ex)
  {
    alert("在GroupUWAutoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}



function initForm()
{
  try
  {
    
   		 initInpBox();
    
    	 initGrpGrid();
    	//divLCGrp2.style.display="none";
    
    	initGrpGrid1();
    	//divLCGrp1.style.display="none";
    
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


// 保单信息列表的初始化
function initGrpGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号1";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="暂交费收据号码1";         		//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="保单号1";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="其它号码类型1";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="金额1";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="工作流任务号";         		//列名
      iArray[5][1]="0px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[6]=new Array();
      iArray[6][0]="工作流子任务号";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[7]=new Array();
      iArray[7][0]="问题件状态";         		//列名
      iArray[7][1]="0px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      GrpGrid = new MulLineEnter( "fm" , "GrpGrid" ); 
      //这些属性必须在loadMulLine前
      GrpGrid.mulLineCount = 2;   
      GrpGrid.displayTitle = 1;
      GrpGrid.locked = 1;
      GrpGrid.canSel = 1;
      GrpGrid.canChk = 0;
      GrpGrid.hiddenSubtraction = 1;
      GrpGrid.hiddenPlus = 1;
      GrpGrid.loadMulLine(iArray);  
      
      
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);        
      }
}
// 保单信息列表的初始化
function initGrpGrid1()
  {     
                             
    var iArray1 = new Array();
      
      try
      {
    	  iArray1[0]=new Array();
    	  iArray1[0][0]="序号2";         			//列名（此列为顺序号，列名无意义，而且不显示）
    	  iArray1[0][1]="30px";            		//列宽
    	  iArray1[0][2]=10;            			//列最大值
    	  iArray1[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    	  iArray1[1]=new Array();
    	  iArray1[1][0]="暂交费收据号码2";         		//列名
    	  iArray1[1][1]="120px";            		//列宽
    	  iArray1[1][2]=170;            			//列最大值
    	  iArray1[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    	  iArray1[2]=new Array();
    	  iArray1[2][0]="票据号2";         		//列名
    	  iArray1[2][1]="120px";            		//列宽
    	  iArray1[2][2]=100;            			//列最大值
    	  iArray1[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    	  iArray1[3]=new Array();
    	  iArray1[3][0]="流水号2";         		//列名
    	  iArray1[3][1]="80px";            		//列宽
    	  iArray1[3][2]=200;            			//列最大值
    	  iArray1[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    	  iArray1[4]=new Array();
    	  iArray1[4][0]="金额2";         		//列名
    	  iArray1[4][1]="100px";            		//列宽
    	  iArray1[4][2]=100;            			//列最大值
    	  iArray1[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    	  iArray1[5]=new Array();
    	  iArray1[5][0]="工作流任务号";         		//列名
    	  iArray1[5][1]="0px";            		//列宽
    	  iArray1[5][2]=200;            			//列最大值
    	  iArray1[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
    	  iArray1[6]=new Array();
    	  iArray1[6][0]="工作流子任务号";         		//列名
    	  iArray1[6][1]="0px";            		//列宽
    	  iArray1[6][2]=200;            			//列最大值
    	  iArray1[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许  3显示
    	 


    	  GrpGridClass = new MulLineEnter( "fm" , "GrpGridClass" ); 

      //这些属性必须在loadMulLine前
      GrpGridClass.mulLineCount = 2;   
      GrpGridClass.displayTitle = 1;
      GrpGridClass.locked = 1;
      GrpGridClass.canSel = 0;
      GrpGridClass.canChk = 0;
      GrpGridClass.hiddenSubtraction = 1;
      GrpGridClass.hiddenPlus = 1;
      GrpGridClass.loadMulLine(iArray1);

      
      
      //这些操作必须在loadMulLine后面
      //GrpGrid1.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>