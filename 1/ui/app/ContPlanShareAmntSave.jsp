<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ContPlanShareAmntSave.jsp
//程序功能：共用保额要素维护
//创建日期：2010-04-06
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
 //接收信息，并作校验处理。
 
 //输入参数
 LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
 LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
 
 ContPlanShareAmntUI tContPlanShareAmntUI = new ContPlanShareAmntUI();
 
 //输出参数
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "Succ";
 String Content = "";
 String transact = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 
 //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
 transact = request.getParameter("fmtransact");
 
 //从url中取出参数付给相应的schema
  String tChk[] = request.getParameterValues("InpMul10GridChk");
	String tRiskCode[] = request.getParameterValues("Mul10Grid2");	//险种编码
	String tDutyCode[] = request.getParameterValues("Mul10Grid3");	//责任编码
	String tCalFactor[] = request.getParameterValues("Mul10Grid5");	//计算要素码
	String tCalFactorValue[] = request.getParameterValues("Mul10Grid8");	//计算要素值
	String tRemark[] = request.getParameterValues("Mul10Grid9");	//特别说明
	String tRiskVersion[] = request.getParameterValues("Mul10Grid10");	//险种版本
	String tGrpPolNo[] = request.getParameterValues("Mul10Grid11");	//集体保单险种号码
	String tMainRiskCode[] = request.getParameterValues("Mul10Grid12");	//主险编码
	String tCalFactorType[] = request.getParameterValues("Mul10Grid13");	//类型
	String tPayPlanCode[] = request.getParameterValues("Mul10Grid14");
	String tGetDutyCode[] = request.getParameterValues("Mul10Grid15");
	String tInsuAccNo[] = request.getParameterValues("Mul10Grid16");
	
	
	
	String arrCount[] = request.getParameterValues("Mul10GridNo");
	int lineCount = arrCount.length; //行数

	for(int i=0;i<lineCount;i++){
		tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
		tLCContPlanDutyParamSchema.setGrpContNo(request.getParameter("mGrpContNo"));
		tLCContPlanDutyParamSchema.setContPlanCode(request.getParameter("ContPlanCode"));
		tLCContPlanDutyParamSchema.setRiskCode(tRiskCode[i]);
		tLCContPlanDutyParamSchema.setDutyCode(tDutyCode[i]);
		tLCContPlanDutyParamSchema.setCalFactor(tCalFactor[i]);
		tLCContPlanDutyParamSchema.setCalFactorValue(tCalFactorValue[i]);
		tLCContPlanDutyParamSchema.setRemark(tRemark[i]);
		tLCContPlanDutyParamSchema.setRiskVersion(tRiskVersion[i]);
		tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo[i]);
		tLCContPlanDutyParamSchema.setMainRiskCode(tMainRiskCode[i]);
		tLCContPlanDutyParamSchema.setMainRiskVersion(tRiskVersion[i]);
		tLCContPlanDutyParamSchema.setCalFactorType(tCalFactorType[i]);
		tLCContPlanDutyParamSchema.setPlanType("0");
		tLCContPlanDutyParamSchema.setPayPlanCode(tPayPlanCode[i]);
		tLCContPlanDutyParamSchema.setGetDutyCode(tGetDutyCode[i]);
		tLCContPlanDutyParamSchema.setInsuAccNo(tInsuAccNo[i]);
		tLCContPlanDutyParamSchema.setOrder(i);
		
		tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

		System.out.println("记录"+i+"放入Set！tGrpPolNo[i] is "+tCalFactor[i]);
	}
 
 try
 {
  //准备传输数据VData
  VData tVData = new VData();
  
  //传输schema
  tVData.addElement(tLCContPlanDutyParamSet);
  
  tVData.add(tG);
  tContPlanShareAmntUI.submitData(tVData,transact);
 }
 catch(Exception ex)
 {
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
  tError = tContPlanShareAmntUI.mErrors;
  if (!tError.needDealError())
  {                          
   Content = " 保存成功! ";
   FlagStr = "Success";
  }
  else                                                                           
  {
   Content = " 保存失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }

 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

