<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：BaseStatistic.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="DelErrorStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();">    
  <form action="./DelErrorStatisticDown.jsp" method=post name=fm target="f1print">
    
		<table class= common border=0 width=100%>
			<TR  class= common>
				<TD  class= title> 管理机构</TD>
				<TD  class= input><Input class= "codeno"  name=ManageCom  verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName elementtype="nacessary"></TD>
				<TD  class= title>统计起期</TD>
				<TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="扫描起期|notnull&Date" elementtype=nacessary> </TD> 
				<TD  class= title>统计止期</TD>
				<TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="扫描止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
			<TR  class= common>
				<TD  class= title> 下载类型 </TD>
				<TD  class= Input><input class=codeno name=QYType CodeData= "0|^1|新单差错件报表^2|新单删除件报表" ondblclick="return showCodeListEx('QYType',[this,QYType_ch],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('QYType',[this,QYType_ch],[0,1],null,null,null,1);"><input class=codename name=QYType_ch elementtype="nacessary"></TD>
<!-- 				<TD  class= title> 销售渠道 </TD> -->
<!-- 				<TD  class= Input><input class=codeno name=saleChnl ondblclick="return showCodeList('lcsalechnl',[this,saleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('lcsalechnl',[this,saleChnlName],[0,1],null,null,null,1);"><input class=codename name=saleChnlName></TD> -->
<!-- 				<TD  class= title id='tContType' style= "display: 'none'"> 保单类型 </TD> -->
<!-- 				<TD  class= Input id='tdContType' style= "display: 'none'"><input class=codeno name=ContType CodeData="0|^0|标准件^9|银保通^99|简易件" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);"><input class=codename name=ContTypeName ></TD> -->
			</TR>
		</table>

    <input type="hidden" name=op value=""><input name="querySql" type="hidden" />
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
  </form>
  
   <hr />

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>