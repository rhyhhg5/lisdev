var showInfo;
var turnPage = new turnPageClass();

function Print()
{	
	if(!checkData()){
		return false;
	}
	//var salechnl=fm.TheSaleChnl.value;
	//var riskcode=fm.TheRiskCode.value;
	var managecom=fm.TheManagecom.value;
	var startdate=fm.SignStartDate.value;
	var enddate=fm.SignEndDate.value;
	//var  tCodeData="(";
	var tSQL1="";
	 if(managecom == '0'){
		 managecom='86';
	  }
	tSQL1="   select lcc.contno 保单号,  "
  +"   (select fee"
  +"      from lcinsureaccfeetrace"
  +"     where contno = lcc.contno"
  +"    and moneytype = 'RP'"
  +"   order by makedate desc fetch first 1 rows only) 风险保费,"
  +"   lcc.prem 保费,"
  +"  (lcc.prem -"
  +" (select fee"
  +"   from lcinsureaccfeetrace"
  +"  where contno = lcc.contno"
  +"    and moneytype = 'RP'"
  +"  order by makedate desc fetch first 1 rows only)) 当期万能账户保费,"
  +"  (select codename from ldcode where codetype='payintv' and code=lcp.payintv fetch first 1 rows only) 缴费方式,"
  +"  lcc.managecom 所属机构,"
  +" (select name from ldcom where comcode = lcc.managecom) 机构名称,"
  +" lcc.salechnl 渠道,"
  +"  db2inst1.codename('salechnl', lcc.salechnl) 渠道名称,"
  +" (select name from laagent where agentcode = lcc.agentcode) 销售人员姓名,"
  +" lcc.agentcode 销售人员工号,"
  +" lcc.signdate 承保日期,"
  +" lcc.cvalidate 生效日期,"
  +" lcc.getpoldate 回执回销时间,"
  +" (select CompleteTime"
  +"  from ReturnVisitTable"
  +"  where RETURNVISITFLAG in ('1', '4')"
  +"   and POLICYNO = lcc.contno"
  +"  order by makedate desc fetch first 1 rows only) 回访成功时间,"
  +" (select lde.codeName from LDCode lde where codetype='stateflag' and code=lcc.stateflag) 保单状态,"
  +"  '' 保全项目,"
  +"  null 退保时间,"
  +"   lcp.insuredname 客户姓名,"
  +"   (select GrpName  from lcaddress where customerno = lcc.appntno and AddressNo = (select AddressNo from lcappnt where contno=lcc.contno)) 投保人所在公司,"
  +"   (select sum(realpay) from llclaimpolicy where polno = lcp.polno and contno =lcp.contno) 理赔金额,"
  +"  null 退保金额,"
  +" (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) 扫描时间   "
  +" from lccont lcc, lcpol lcp"
  +" where 1 = 1"
	  +" and lcc.managecom like'"+managecom+"%'"
  
  +" and lcc.conttype = '1'"
  +" and lcc.appflag = '1'"
  +" and lcc.contno = lcp.contno"
  +" and lcp.riskcode in ('122901', '122601', '123501', '123601','123602')"
//  +" and lcc.cvalidate>='2018-01-01'"
 // +" and lcc.cvalidate<='2018-01-03 '"
  +" and lcc.cvalidate>='"+startdate+"'"
  +" and lcc.cvalidate<='"+enddate+" '"
  +" and not exists (select 1 from lcrnewstatelog n where n.contno =lcc.contno and state='6')"
  +" and not exists (select 1 from lcrnewstatelog n where n.newcontno =lcc.contno and state='6')"
  +" union all"
  +" select lcc.contno 保单号,"
  +" (select fee"
  +"   from lbinsureaccfeetrace"
  +"  where contno = lcc.contno"
  +"   and moneytype = 'RP'"
  +"  order by makedate desc fetch first 1 rows only) 风险保费,"
  +" lcc.prem 保费,"
  +"  (lcc.prem -"
  +"  (select fee"
  +"   from lbinsureaccfeetrace"
  +"   where contno = lcc.contno"
  +"    and moneytype = 'RP'"
  +"  order by makedate desc fetch first 1 rows only)) 当期万能账户保费,"
  +"  (select codename from ldcode where codetype='payintv' and code=lcp.payintv fetch first 1 rows only) 缴费方式,"
  +" lcc.managecom 所属机构,"
  +" (select name from ldcom where comcode = lcc.managecom) 机构名称,"
  +" lcc.salechnl 渠道,"
  +"  db2inst1.codename('salechnl', lcc.salechnl) 渠道名称,"
  +" (select name from laagent where agentcode = lcc.agentcode) 销售人员姓名,"
  +"  lcc.agentcode 销售人员工号,"
  +"  lcc.signdate 承保日期,"
  +" lcc.cvalidate 生效日期,"
  +" lcc.getpoldate 回执回销时间,"
  +"  (select CompleteTime"
  +"  from ReturnVisitTable"
  +"  where RETURNVISITFLAG in ('1', '4')"
  +"   and POLICYNO = lcc.contno"
  +"  order by makedate desc fetch first 1 rows only) 回访成功时间,"
  +"  (select lde.codeName from LDCode lde where codetype='stateflag' and code=lcc.stateflag) 保单状态,"
  +"  (case (select lpe.edortype"
  +"    from lpedoritem lpe, lpedorapp lpa"
  +"   where lpe.edorno = lpa.EdorAcceptNo"
  +"    and lpe.contno = lcc.contno"
  +"    and lpe.edortype in ('XT', 'CT', 'WT')"
  +"    and lpa.edorstate = '0'"
  +"    and lpe.edorno = lcc.edorno)"
  +"  when 'WT' then"
  +"   '犹豫期退保'"
  +"   when 'XT' then"
  +"  '解约'"
  +"  when 'CT' then"
  +"   '解约'"
  +"  else"
  +"   ''    "
  +"   end) 保全项目,"
  +"  (select lpa.confdate"
  +"  from lpedoritem lpe, lpedorapp lpa"
  +"  where lpe.edorno = lpa.EdorAcceptNo"
  +"   and lpe.contno = lcc.contno"
  +"   and lpe.edortype in ('XT', 'CT', 'WT')"
  +"   and lpa.edorstate = '0'"
  +"   and lpe.edorno = lcc.edorno) 退保时间,"
  +"	   lcp.insuredname 客户姓名,"
  +"   	   (select GrpName  from lcaddress where customerno = lcc.appntno and AddressNo = (select AddressNo from lbappnt where contno=lcc.contno)) 投保人所在公司,"
  +"	(select sum(realpay) from llclaimpolicy where polno = lcp.polno and contno=lcp.contno) 理赔金额,"
  +"      ("
  +"  select lpe.getmoney"
  +"  from lpedoritem lpe, lpedorapp lpa"
  +"  where lpe.edorno = lpa.EdorAcceptNo"
  +"   and lpe.contno = lcc.contno"
  +"   and lpe.edortype in ('XT', 'CT', 'WT')"
  +"   and lpa.edorstate = '0'"
  +"   and lpe.edorno = lcc.edorno) 退费金额,"
  +"   (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) 扫描时间"
        
  +" from lbcont lcc,lbpol lcp"
  +" where 1 = 1"
 // +"  and lcc.managecom like'86%'"
	  +" and lcc.managecom like'"+managecom+"%'"
//  +" and lcc.managecom like'"+managecom+"%'"
  +" and lcc.conttype = '1'"
  +" and lcc.contno = lcp.contno"
  +" and lcp.riskcode in ('122901', '122601', '123501', '123601','123602')"
  +" and lcc.cvalidate>='"+startdate+"'"
  +" and lcc.cvalidate<='"+enddate+" '"
 // +" and lcc.cvalidate>='2018-01-01'"
 // +" and lcc.cvalidate<='2018-01-03'"
  +" and not exists (select 1 from lcrnewstatelog n where n.contno =lcc.contno and state='6')"
  +" and not exists (select 1 from lcrnewstatelog n where n.newcontno =lcc.contno and state='6')"
  +" and not Exists(select 1 from lccontstate where polno = lcp.polno and contno=lcp.contno and othernotype='XB') "
 // +" with ur "


	
	//tSQL1="select * from lccont where Managecom = '86110000' and  prtno  in ('16180522000', '16180522001')   "     
    fm.querySql1.value=tSQL1;  
    fm.action = "ZmkContExcelDownload.jsp";
    fm.submit();
    fm.action = "";
    fm.querySql1.value="";  
  
}


function checkData(){
	
	var signdate = fm.SignStartDate.value == "" || fm.SignEndDate.value == "";	
	//var salechnl=fm.TheSaleChnl.value;
	//var riskcode=fm.TheRiskCode.value;
	var managecom=fm.TheManagecom.value;
	if(signdate ){
		
		alert("投保日起期和投保日止期为保单申请日，必录。");
		return false;
	}
	/*if(!salechnl){
		alert("销售渠道不能为空");
		return false;
		
	}
	if(!riskcode){
		alert("险种不能为空");
		return false;
		
	}*/
	if(!managecom){
		managecom("管理机构不能为空");
		return false;
		
	}
	/*if(riskcode==0||managecom==0){
		var sql = "select 1 from Dual where date('" + fm.SignEndDate.value + "') > date('" + fm.SignStartDate.value + "') + 3 month ";
		  var rs = easyExecSql(sql);
		  if(rs)
		  {
		    if(!confirm("投保日期起止时间大于3个月！是否继续？"))
		    {
		      return false;
		    }
		  }
	}*/
	 
	  
	
	return true;
}

