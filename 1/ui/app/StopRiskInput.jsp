<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>

<html>
  <head>
    <title>My JSP 'StopRiskInput.jsp' starting page</title>
   
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<script type="text/javascript" src="StopRiskInput.js" charset="gbk"></script>
	<%@include file="StopRiskInit.jsp"%>
  </head>
  
  <body onload="initForm();">
  	<form action="StopRiskSave.jsp" method=post name=fm target="fraSubmit">
  	<input type=hidden id="fmtransact" name=fmtransact>
  		<div id="divQueryInput" , style="display:hidden">
  		
			<table>
					<td class=button align=left>
						<input type="button" class=cssButton value="查  询" id=QUERY onclick="queryClick()">
					</td>
					
					<td class=button align=left>
						<input type="button" class=cssButton value="修  改" id=UPDATE onclick="updateClick()">
						<input type="hidden" name=subCode id="subCode">
						<input type="hidden" name=subCodeType id="subCodeType">
					</td>
					
			</table>	
  			<!-- 查询结果部分 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divProjectMonth1);">
					</td>
					<td class=titleImg>
						险种信息
					</td>
					</tr>
				</table>
  		</div>
  		<!--信息（列表）-->
  		<Div id="divProjectMonth1" style="display:''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1>
						<span id="spanProjectMonth"> </span>
					</td>
				</tr>
			</table>
		</div>
		<!--分页部分-->
		<Div id="divPage" align=center style="display:'' ">
			<table>
				<tr>
					<td class=button>
						<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
						<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
						<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
						<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
					</td>
				</tr>
			</table>
		</Div>
  	</form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>
