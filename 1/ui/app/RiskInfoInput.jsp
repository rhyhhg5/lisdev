<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Verifyinput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="RiskInfoInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="RiskInfoInit.jsp"%>
    <title>险种信息查询</title>
</head>
<body onload="initForm();" >
    <form action="./RiskInfoSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 个人保单信息部分 -->
        <table class= common border=0 width=100%>
            <tr><td class= titleImg align= center>请输入险种查询条件：</td></tr>
        </table>
        <table class= common align=center>
            <TR class= common>
                <TD class= title>险种编码</TD>
                <TD class= input><input class="codeNo" name=riskCode ondblclick="return showCodeList('RiskCode',[this,riskName],[0,1],null,null,null,1);" ><input class=codename name=riskName readonly ></TD>
                <TD class= title>团/个/银代</TD>
                <TD class= input><input class="codeNo" name=riskProp CodeData="0|^G|团体险|^I|个人险^Y|银代险" ondblclick="return showCodeListEx('RiskProp',[this,riskPropName],[0,1]);" ><input class=codename name=riskPropName readonly ></TD>
                <TD class= title>主附险标记</TD>
                <TD class= input><input class="codeNo" name=subRiskFlag CodeData="0|^M|主险|^S|附险^A|两者都可以" ondblclick="return showCodeListEx('SubRiskFlag',[this,subRiskFlagName],[0,1]);" ><input class=codename name=subRiskFlagName readonly=true ></TD>
            </TR>
            <TR class= common>
                <TD class= title>保险期间标志</TD>
                <TD class= input><input class="codeNo" name=riskType5 CodeData="0|^1|一年期以内|^2|一年期^3|一年期以上(定期)^4|一年期以上(终身)" ondblclick="return showCodeListEx('RiskType5',[this,riskType5Name],[0,1]);" ><input class=codename name=riskType5Name readonly=true ></TD>
                <TD class= title>保险有效标志</TD>
                <TD class= input><input class="codeNo" name=canSale CodeData="0|^0|有效|^1|停售" ondblclick="return showCodeListEx('CanSale',[this,CanSaleName],[0,1]);" ><input class=codename name=CanSaleName readonly=true ></TD>
                <TD class= title>停办日期</TD>
                <TD class= input><input class="coolDatePicker" name=endDate ></TD>
            </TR>
        </table>
        <input type="hidden" class="common" name="querySql" >
        <input value="查询险种信息" class="cssButton" type=button onclick="easyQueryClick();">
        <input value="下载清单" class="cssButton" type=button onclick="downloadList();">
        <br>
        <table>
            <tr>
                <td class=common>
                    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskInfo);">
                </td>
                <td class= titleImg>险种列表</td>
            </tr>
        </table>
        <div id= "divRiskInfo" style= "display: ''">
            <table class= common>
                <tr class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanRiskInfoGrid" ></span>
                    </td>
                </tr>
            </table>
            <div id= "divPage" align=center style= "display: 'none' ">
                <input value="首 页" class="cssButton" type=button onclick="turnPage.firstPage();">
                <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
                <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
                <input value="尾 页" class="cssButton" type=button onclick="turnPage.lastPage();">
            </div>
        </div>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
