//ChangExtractRate.js该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass();	//使用翻页功能，必须建立为全局变量


//提交提取保费费率的修改
function save()
{  
	var tCountryCategory=fm.CountryCategory.value;
	var tEngineeringFlag=fm.EngineeringFlag.value;
	var tEngineering=fm.Engineering.value;
	var tCountry=fm.Country.value;
	var tCountryName=fm.CountryName.value;
	
	var tSql = "select 1 from lcgrpcontroad where PrtNo='" + tPrtNo + "' and inputprem <> '' " 
			 + "union select 1 from lcpol where prtno ='" + tPrtNo + "' and standprem>0 ";
	var tarr = easyExecSql(tSql);
	if(tarr){
		alert("保费计算完毕之后，不允许对一带一路产品要素进行修改！");
		return false;
	}
	if(tCountryCategory==""){
		alert("一带一路产品国家类别是必录项！");
		return false;
	}
	if(tCountry==""){
		alert("一带一路产品国家/地区名称是必录项！");
		return false;
	}
	if(tEngineeringFlag==""||tEngineeringFlag==null){
		alert("一带一路产品“被保险人在境外是否参与建筑施工项目”是必录项！");
		return false;
	}
	if(tEngineeringFlag==1&&tEngineering==""){
		alert("一带一路产品“被保险人在境外是否参与建筑施工项目”选择参加时，工程类别必录！");
		return false;
	}
	fm.PrtNo.value=tPrtNo;
	fm.fmtransact.value="INSERT";
	var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();	
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;   
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	}
	initForm();
}

function onclickEngineering(flag){
	
	if(flag==1){
		fm.all("divEngineering").style.display = "";
		fm.EngineeringFlag.value="1";
		fm.flagb.checked=false;
	}
	if(flag==2){
		fm.all("divEngineering").style.display = "none";
		fm.EngineeringFlag.value="0";
		fm.Engineering.value="";
		fm.EngineeringName.value="";
		fm.flaga.checked=false;
	}
}

/* 退出 */
function cancle(){
	top.close();
}

function initLCGrpContRoadInfo(){
	var tSQL = "select "
			+ "CountryCategory,db2inst1.codename('country',CountryCategory),"
			+ "EngineeringFlag,"
			+ "EngineeringCategory,(select codename from ldcode where codetype='engineering' and code=EngineeringCategory), "
			+ "CalculPrem, "
			+ "TotalFactor, " 
			+ "Country "
			+ "from LCGrpContRoad "
			+ "where prtno='"+tPrtNo+"'";
	var arr = easyExecSql(tSQL);
	if(arr){
		fm.CountryCategory.value = arr[0][0];
		fm.CountryName.value = arr[0][1];
		fm.EngineeringFlag.value = arr[0][2];
		fm.Engineering.value = arr[0][3];
		fm.EngineeringName.value = arr[0][4];
		fm.CalculPrem.value= arr[0][5];
		fm.TotalFactor.value= arr[0][6];
		fm.Country.value= arr[0][7];
		var tEngineeringFlag=fm.EngineeringFlag.value;
		if(tEngineeringFlag==1){
			fm.all("divEngineering").style.display = "";
			fm.flaga.checked=true;
		}else if(tEngineeringFlag==0){
			fm.all("divEngineering").style.display = "none";
			fm.flagb.checked=true;
		}
	}else{
		fm.CountryCategory.value = "";
		fm.CountryName.value = "";
		fm.Engineering.value = "";
		fm.EngineeringName.value = "";
		fm.Country.value = "";
		fm.all("divEngineering").style.display = "none";
		fm.flaga.checked=false;
		fm.flagb.checked=false;
	}
}
