<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="java.util.*"%>
	<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LcAppntContGetInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	</head>
	<body onload="initElementtype(); initBox();">
		<form action="./LcAppntContGetSave.jsp" method=post name=fm
			target="fraSubmit">
			<table class=common border=0 width=100%>
				<TR class=common>
					<TD class=title>
						管理机构</TD>
						<TD class=input>
							<Input class=codeNo name=ManageCom value="86" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName  value="总公司"   elementtype="nacessary" >
						</TD>
					<TD class=title>
						保单签单日期起期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" dateFormat="short" name="Signdate"
							verify="保单签单日期起期|date&notnull" elementtype="nacessary" />
					</TD>
					<TD class=title>
						保单签单日期止期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" dateFormat="short" name="EndDate"
							verify="保单签单日期止期|date&notnull" elementtype="nacessary" />
					</TD>
				</TR>
			</table>
			<table>
				<tr>
					<td class=button width="10%" align=left>
						<input type="hidden" class="common" name="querySql">
						<INPUT TYPE=button class=cssButton VALUE="下  载"
							onclick=submitForm();>
					</td>
				</tr>
			</table>
			<hr>
			<div>
				<font color="red">
					<ul>
						报表说明：
						<li>
							该报表是针对主险种230501、330701、330801、122601、122901的个人当前有效保单明细报表
						</li>
						<li>
							保费、保额为主附险合计金额
						</li>
						<li>
							管理机构、保单签单日期起期和保单签单日期止期为必填项
						</li>
					</ul>
					<ul>
						其他说明：
						<li>
							由于数据量较大，下载过程时间可能会较长，请您耐心等候
						</li>
					</ul> </font>
			</div>
		</form>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</body>
</html>
