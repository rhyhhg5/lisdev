<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ProposalSignLogInit.jsp
//程序功能：万能险帐户结算利率录入界面初始化
//创建日期：2007-12-12
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!--引入必要的java类  -->
<%@page import="com.sinosoft.lis.pubfun.*"%>	
<% 
  //获取传入的参数
  String tLoadFlag = request.getParameter("LoadFlag");
%>
<script language="JavaScript">

//初始化表单
function initForm()
{ 
  try 
  {
    initInpBox();
    initSignLogGrid(); 
    initElementtype();
  }
  catch(re) 
  {
    alert("在ProposalSignLogInit.jsp-->InitForm函数中发生异常:初始化界面错误!" + re.message);
  }
}

//工单列表Mulline的初始化
function initSignLogGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=3;        //是否允许输入,1表示允许，0表示不允许
    iArray[0][21]="IndexNo";        //列明
      
    iArray[1]=new Array();
    iArray[1][0]="流水号";      //列名RiskCode
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=3;             //是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="SeriNo";             //是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="合同号";      //列名RiskCode
    iArray[2][1]="80";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="ContNo";             //是否允许输入,1表示允许，0表示不允许
        
    iArray[3]=new Array();
    iArray[3][0]="保单类型";      //列名InsuAccNo
    iArray[3][1]="50";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=2;             //是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="ContType";             //是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="conttype";
        
    iArray[4]=new Array();
    iArray[4][0]="错误信息";      //列名InsuAccNo
    iArray[4][1]="200";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="ErrInfo";             //是否允许输入,1表示允许，0表示不允许
      
    SignLogGrid = new MulLineEnter("fm", "SignLogGrid"); 
	  //设置Grid属性
    SignLogGrid.mulLineCount = 0;
    SignLogGrid.displayTitle = 1;
    SignLogGrid.locked = 1;
    SignLogGrid.canSel = 1;	
    SignLogGrid.canChk = 0;
    SignLogGrid.hiddenSubtraction = 1;
    SignLogGrid.hiddenPlus = 1;
    SignLogGrid.selBoxEventFuncName = "selectOne" ;
    SignLogGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("ProposalSignLogInit.jsp-->initSignLogGrid函数中发生异常:初始化界面错误!");
  }
}
</script>
