//程序名称：
//程序功能：
//创建日期：2010-3-25
//创建人  ：GUZG	
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 与投保人的关系
 */
function queryRelationToAppnt()
{    
    var cContNo = fm.ContNo.value;
    var tStrSql = "select  relationtoappnt,(select codename from ldcode where code = a.relationtoappnt and codetype = 'relation')"
        + " from LCInsured a "
        + " where 1 = 1 "
        + " and contno = '" + cContNo + "' and managecom like '"+fm.all('ManageCom').value+"%' ";
    
    var arrResult = easyExecSql(tStrSql);
    var tRelationToAppnt = arrResult[0][1]; 
    fm.RalationToAppnt.value = tRelationToAppnt;
    
    return true;
}

/**
 * 修改与被保人的关系
 */
function submitData()
{    
    if(fm.RelationToAppntNew.value == "")
    {
        alert("修改后于投保人的关系不能为空");
        return false;
    }
    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.action = "./ModifyRelationToAppntSave.jsp";
    fm.submit();
}


function afterImportSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("修改于投保人关系失败，请尝试重新进行修改。");
    }
}