var showInfo;
//var turnpage =new turnpageClass();

function submitForm(){
	
	var tManageCom=fm.ManageCom.value;
	var tStartSignDate=fm.StartSignDate.value;
	var tEndSignDate=fm.EndSignDate.value;
	if(tManageCom==""||tManageCom==null){
		alert("请录入管理机构");	
		return false;
	}
	if(tStartSignDate==""||tStartSignDate==null){
		alert("请录入签单起期");	
		return false ;
	}
	if(tEndSignDate==""||tEndSignDate==null){
		alert("请录入签单止期");	
		return false;
	}
	
	
	if(dateDiff(tStartSignDate,tEndSignDate,"M")>12){
		alert("统计期最多为十二个月！");
		return false;
	}
	var tSaleChnl= fm.saleChn.value;
	var tpayintvCode=fm.payintvCode.value;
	var tproductType=fm.productType.value;
	var tSql=" and  lcc.SaleChnl in ('04','13')";
	var tSql2="";
	var tSql3="";
	var tSql4="";
	var tSql5="";
	var tSql6="";
	if(tSaleChnl=="00" ){
		 tSql=" and  lcc.SaleChnl in ('04','13')";
	}
	if(tSaleChnl=="04"){
		tSql=" and  lcc.salechnl ='04' ";
	}
	if(tSaleChnl=="13"){
		tSql=" and  lcc.salechnl ='13' ";
	}
	
  if(tpayintvCode=="00"){
	  
	  tSql2=" ";
  }
  if(tpayintvCode=="01"){
	  
	  tSql2="  and  lcc.payintv='0' ";
  }
  if(tpayintvCode=="02"){
	  
	  tSql2=" and  lcc.payintv <>'0' ";
  }

 if(tproductType=="00"){
	      tSql3="";
	      tSql4="";
 }

  if(tproductType=="02" ){
	      tSql3=" and  not exists (  select 1 from lcpol lp , ldcode ld where ld.codetype='highprice' and ld.comcode='I' and lp.riskcode=ld.code and lp.contno=lcc.contno)  "
	           +" and  not exists (  select 1 from lbpol lp , ldcode ld where ld.codetype='highprice' and ld.comcode='I' and lp.riskcode=ld.code and lp.contno=lcc.contno) ";
	      tSql4=" and not exists ( select 1 from lcgrppol lp , ldcode ld where ld.codetype='highprice' and ld.comcode='G' and lp.riskcode=ld.code and lp.grpcontno=lcc.grpcontno)";
	      tSql5=" and not exists ( select 1 from lbgrppol lp , ldcode ld where ld.codetype='highprice' and ld.comcode='G' and lp.riskcode=ld.code and lp.grpcontno=lcc.grpcontno)";
	}
	if(tproductType=="01" ){
	      tSql3=" and ( exists (  select 1 from lcpol lp , ldcode ld where ld.codetype='highprice' and ld.comcode='I' and lp.riskcode=ld.code and lp.contno=lcc.contno) "
	           +" or  exists (  select 1 from lbpol lp , ldcode ld where ld.codetype='highprice' and ld.comcode='I' and lp.riskcode=ld.code and lp.contno=lcc.contno)) ";
	      tSql4=" and exists ( select 1 from lcgrppol lp , ldcode ld where ld.codetype='highprice' and ld.comcode='G' and lp.riskcode=ld.code and lp.grpcontno=lcc.grpcontno)";
	      tSql5=" and exists ( select 1 from lbgrppol lp , ldcode ld where ld.codetype='highprice' and ld.comcode='G' and lp.riskcode=ld.code and lp.grpcontno=lcc.grpcontno)";
	}
	if(fm.all('outFlag').value=='0'){
		tSql6=" and lcc.cardflag not in ('9','c','d','e','f')";
		}
	if(fm.all('outFlag').value=='1'){
		tSql6=" and lcc.cardflag in ('9','c','d','e','f')";
		}
	/*if((fm.all('outFlag').value=='')){
		tSql6=" and lcc.cardflag in ('9','c','d','e','f')";
	}*/
	 var tStrSQL = ""
	        + " select "
	        + " tmp.ComCode,(select name from ldcom where comcode=tmp.comcode),"
	        + " tmp.ManageCom, tmp.ManageComName, tmp.CardType, tmp.ContNo, tmp.PrtNo, "
	        + " tmp.SaleChnl, getUniteCode(tmp.AgentCode), tmp.AgentName, tmp.BranchAttr, tmp.BranchName, "
	        + " tmp.AppntName, tmp.AppntBirthday, tmp.AppntSex, tmp.AppntIdType, "
	        + " tmp.AppntIdNo, tmp.AppntNativePlace, tmp.AppntMarriage, tmp.AppntOccupationType, "
	        + " tmp.AppntOccupationCode, tmp.CValidate, tmp.CInValidate, "
	        + " tmp.PolApplyDate, tmp.InputDate, tmp.ApproveDate, tmp.UWDate, tmp.SignDate, "
	        + " tmp.CustomGetPolDate, tmp.GetPolDate, tmp.PayMode, "
	        + " sum(tmp.Prem) AllPrem, sum(tmp.Amnt) AllAmnt,case when tmp.CardType='团单' then (select peoples2 from lcgrpcont where grpcontno=tmp.contno) else count(distinct tmp.InsuredNo) end InsuCount, "
	        + " sum(tmp.AllPolPrem) AllContPrem, "
	        + " (case when tmp.ContAppFlag is null then '银保通撤单' else tmp.ContAppFlag end) ContAppFlag,tmp.ScanFlag,tmp.ScanDate,"
	        + " tmp.RiskCode," 
	        + " (select  riskname  from  lmriskapp  where  riskcode=tmp.RiskCode)  RiskMz, "	
	        + " ( case when tmp.PayTimes ='0' then '趸交' else '期交'  end) ,  "
	        + " case when tmp.paytimes<>'0' then (Select trim(a.payendyear)||trim(a.payendyearflag) From Lcpol a Where a.Contno = tmp.Contno And a.Riskcode = tmp.Riskcode "
            + " Union Select trim(a.payendyear)||trim(a.payendyearflag) From Lcpol a Where a.Contno = Tmp.Contno And a.Riskcode = tmp.Riskcode  Fetch First 1 Rows Only) else '' end "
            + ",Case When (Tmp.agentcom Is Not Null And Trim(tmp.agentcom)<>'') Then db2inst1.codename('bankno',substr(tmp.agentcom,1,5)) Else '' End "
            + ",(select name from lacom where agentcom=Tmp.agentcom),Tmp.cardflag "
            + ",(case when Tmp.cardflag1 in ('9','c','d','e','f') then '是' else '否' end ) "
	        //+" tmp.payday "     
	        + " from "
	        + " ( "
	        + " select "
	        + " substr(lcc.ManageCom,1,4) Comcode,"
	        + " lcc.ManageCom, "
	        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
	        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
	        + " lcc.ContNo, lcc.PrtNo, "
	        + " (case when 1=(select count(1) from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl ) then CodeName('lcsalechnl', lcc.SaleChnl) " 
	        + " else CodeName('salechnl', lcc.SaleChnl) end ) SaleChnl, "
	        + " lcc.AgentCode, "
	        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
	        + " labg.BranchAttr, labg.Name BranchName, "
	        + " lca.AppntName AppntName, lca.AppntBirthday AppntBirthday, "
	        + " CodeName('sex', lca.AppntSex) AppntSex, "
	        + " CodeName('idtype', lca.IdType) AppntIdType, "
	        + " lca.IdNo AppntIdNo, "
	        + " CodeName('nativeplace', lca.NativePlace) AppntNativePlace, "
	        + " CodeName('marriage', lca.Marriage) AppntMarriage, "
	        + " CodeName('occupationtype', lca.OccupationType) AppntOccupationType, "
	        + " lca.OccupationCode AppntOccupationCode, "
	        + " lcc.CValidate, lcc.CInValidate, "
	        + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate, "
	        + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
	        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
	        + " lcp.Prem, lcp.Amnt, lcp.InsuredNo, "
	        + " (lcp.Prem + nvl(lcp.SupplementaryPrem, 0)) AllPolPrem, "
	        + " case lcc.stateflag when '1' then '承保有效' when '2' then '失效终止' when '3' then '终止' else '投保' end ContAppFlag, "
	        //+ " '承保' ContAppFlag, "
	        + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag, "
	        + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate, "
	        + "  lcc.payintv   PayTimes , "
	        +"(select  a.riskcode riskcode from lcpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' " 
	        + " union select  a.riskcode riskcode from lbpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' " 
	        + " order by riskcode fetch first 1 rows only)  Riskcode "
	       // + "(select  a.riskcode from lcpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' order by prem desc fetch first 1 rows only)  RiskCode, "
	        //+ "case when lcc.payintv<>'0' then (select trim(a.payendyear)||''||trim(a.payendyearflag) from lcpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' fetch first 1 rows only ) else '' end PayDay"
	        + ",lcc.agentcom "
	        + ",(case lcc.cardflag when '9' then '银保通' when 'c' then '网银' when 'd' then '自助终端' when 'e' then '手机银行' when 'f' then '智慧柜员机' else '银行渠道手工单' end) cardflag "
	        + ",lcc.cardflag cardflag1 "
	        + " "
	        + " from LCCont lcc "
	        + " inner join LCPol lcp on lcc.ContNo = lcp.ContNo "
	        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
	        + " inner join LCAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
	        + " where 1 = 1 "
	        + " and lcc.ContType = '1' "
	        + " and lcc.AppFlag = '1' "
	        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
	        + getWherePart("lcc.ManageCom", "ManageCom", "like")
	        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
	        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
	        + tSql
	        + tSql2
	        + tSql3
	        + tSql6
	        + " union all "
	        + " select "
	        + " substr(lcc.ManageCom,1,4) Comcode, "
	        + " lcc.ManageCom, "
	        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
	        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
	        + " lcc.ContNo, lcc.PrtNo, "
	        + " (case when 1=(select count(1) from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl ) then CodeName('lcsalechnl', lcc.SaleChnl) " 
	        + " else CodeName('salechnl', lcc.SaleChnl) end ) SaleChnl, "
	        + " lcc.AgentCode, "
	        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
	        + " labg.BranchAttr, labg.Name BranchName, "
	        + " lca.AppntName AppntName, lca.AppntBirthday AppntBirthday, "
	        + " CodeName('sex', lca.AppntSex) AppntSex, "
	        + " CodeName('idtype', lca.IdType) AppntIdType, "
	        + " lca.IdNo AppntIdNo, "
	        + " CodeName('nativeplace', lca.NativePlace) AppntNativePlace, "
	        + " CodeName('marriage', lca.Marriage) AppntMarriage, "
	        + " CodeName('occupationtype', lca.OccupationType) AppntOccupationType, "
	        + " lca.OccupationCode AppntOccupationCode, "
	        + " lcc.CValidate, lcc.CInValidate, "
	        + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate, "
	        + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
	        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
	        + " lcp.Prem, lcp.Amnt, lcp.InsuredNo, "
	        + " (lcp.Prem + nvl(lcp.SupplementaryPrem, 0)) AllPolPrem, "
	        + " case lcc.stateflag when '1' then '承保有效' when '2' then '失效终止' when '3' then '终止' else '投保' end ContAppFlag, "
	        //+ " '承保' ContAppFlag, "
	        + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag, "
	        + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate, "
	        + " lcc.payintv   PayTimes, "
	        +"(select  a.riskcode riskcode from lcpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' " 
	        + " union select  a.riskcode riskcode from lbpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' " 
	        + " order by riskcode fetch first 1 rows only)  Riskcode"
	        //+ "(select  a.riskcode from lbpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' order by prem desc fetch first 1 rows only) RiskCode ,"
	        //+ "case when lcc.payintv<>'0' then (select trim(a.payendyear)||''||trim(a.payendyearflag) from lbpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' fetch first 1 rows only ) else '' end  PayDay"
	        + ",lcc.agentcom "
	        + ",case lcc.cardflag when '9' then '银保通' when 'c' then '网银' when 'd' then '自助终端' when 'e' then '手机银行' when 'f' then '智慧柜员机' else '银行渠道手工单' end "
	        + ",lcc.cardflag cardflag1 "
	        + " "
	        + " from LCCont lcc "
	        + " inner join LBPol lcp on lcc.ContNo = lcp.ContNo "
	        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
	        + " inner join LBAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
	        + " where 1 = 1 "
	        + " and lcc.ContType = '1' "
	        + " and lcc.AppFlag = '1' "
	        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
	        + getWherePart("lcc.ManageCom", "ManageCom", "like")
	        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
	        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
	        + tSql
	        + tSql2
	        + tSql3
	        + tSql6
	        + " union all "
	        + " select "
	        + " substr(lcc.ManageCom,1,4) Comcode, "
	        + " lcc.ManageCom, "
	        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
	        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
	        + " lcc.ContNo, lcc.PrtNo, "
	        + " (case when 1=(select count(1) from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl ) then CodeName('lcsalechnl', lcc.SaleChnl) " 
	        + " else CodeName('salechnl', lcc.SaleChnl) end ) SaleChnl, "
	        + " lcc.AgentCode, "
	        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
	        + " labg.BranchAttr, labg.Name BranchName, "
	        + " lca.AppntName AppntName, lca.AppntBirthday AppntBirthday, "
	        + " CodeName('sex', lca.AppntSex) AppntSex, "
	        + " CodeName('idtype', lca.IdType) AppntIdType, "
	        + " lca.IdNo AppntIdNo, "
	        + " CodeName('nativeplace', lca.NativePlace) AppntNativePlace, "
	        + " CodeName('marriage', lca.Marriage) AppntMarriage, "
	        + " CodeName('occupationtype', lca.OccupationType) AppntOccupationType, "
	        + " lca.OccupationCode AppntOccupationCode, "
	        + " lcc.CValidate, lcc.CInValidate, "
	        + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate, "
	        + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
	        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
	        + " lcp.Prem, lcp.Amnt, lcp.InsuredNo, "
	        + " (lcp.Prem + nvl(lcp.SupplementaryPrem, 0)) AllPolPrem, "
	        + " (select (case lpei.EdorType when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' end) from LPEdorItem lpei where lpei.ContNo = lcc.ContNo and lpei.EdorType in ('CT', 'WT', 'XT') order by lpei.EdorAppDate desc fetch first rows only) ContAppFlag, "
	        + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag, "
	        + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate, "
	        + " lcc.payintv  PayTimes ,"
	        +"(select  a.riskcode riskcode from lcpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' " 
	        + " union select  a.riskcode riskcode from lbpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' " 
	        + " order by riskcode fetch first 1 rows only)  Riskcode"
	        //+ "(select  a.riskcode from lbpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' order by prem desc fetch first 1 rows only)  RiskCode ,"
	        //+ "case when lcc.payintv<>'0' then (select trim(a.payendyear)||''||trim(a.payendyearflag) from lbpol a,lmriskapp b where  a.contno=lcc.contno and a.riskcode=b.riskcode and b.subriskflag ='M' fetch first 1 rows only ) else '' end  PayDay"
	        + ",lcc.agentcom "
	        + ",case lcc.cardflag when '9' then '银保通' when 'c' then '网银' when 'd' then '自助终端' when 'e' then '手机银行' when 'f' then '智慧柜员机' else '银行渠道手工单' end "
	        + ",lcc.cardflag cardflag1 "
	        + "  "
	        + " from LBCont lcc "
	        + " inner join LBPol lcp on lcc.ContNo = lcp.ContNo "
	        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
	        + " inner join LBAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
	        + " where 1 = 1 "
	        + " and lcc.ContType = '1' "
	        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
	        + getWherePart("lcc.ManageCom", "ManageCom", "like")
	        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
	        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
	        + tSql
	        + tSql2
	        + tSql3
	        + tSql6
	        //团单
	        + " union "
	        + " select  "
            + " substr(lcc.ManageCom,1,4) Comcode, "
            + " lcc.ManageCom,  "
            + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName,  "
            + " '团单' CardType,  "
            + " lcc.grpContNo, lcc.PrtNo,  "
            + " CodeName('salechnl', lcc.SaleChnl) SaleChnl,  "
            + " lcc.AgentCode,  "
            + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName,  "
            + " labg.BranchAttr, labg.Name BranchName,  "
            + " lcc.grpname AppntName, null AppntBirthday,  "
            + " '' AppntSex,  "
            + " '' AppntIdType,  "
            + " '' AppntIdNo,  "
            + " '' AppntNativePlace,  "
            + " '' AppntMarriage,  "
            + " '' AppntOccupationType,  "
            + " '' AppntOccupationCode,"
            + " lcc.CValidate, lcc.CInValidate,  "
            + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate,  "
            + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate,  "
            + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode,  "
            + " lcc.Prem, lcc.Amnt, '1' , "
            + " lcc.Prem AllPolPrem,  "
            + " case lcc.stateflag when '1' then '承保有效' when '2' then '失效终止' when '3' then '终止' else '投保' end ContAppFlag, "
            //+ " '承保' ContAppFlag,  "
            + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag,  "
            + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate,  "
            + " lcc.payintv   PayTimes ,  "
            + " (select  a.riskcode riskcode from LCgrpPol a,lmriskapp b where  a.grpcontno=lcc.grpcontno and a.riskcode=b.riskcode and b.subriskflag ='M'   "
            + " order by riskcode fetch first 1 rows only)  Riskcode  "
            + ",lcc.agentcom "
            + ",'银行渠道手工单' "
            + ",lcc.cardflag cardflag1 "
            + " from LCgrpCont lcc  "
            + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup  "
            + " where 1 = 1  "
            + getWherePart("lcc.ManageCom", "ManageCom", "like")
	        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
	        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
	        + tSql
	        + tSql2
	        + tSql4
	        + tSql6
	        + " union "
	        + " select  "
            + " substr(lcc.ManageCom,1,4) Comcode, "
            + " lcc.ManageCom,  "
            + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName,  "
            + " '团单' CardType,  "
            + " lcc.grpContNo, lcc.PrtNo,  "
            + " CodeName('salechnl', lcc.SaleChnl) SaleChnl,  "
            + " lcc.AgentCode,  "
            + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName,  "
            + " labg.BranchAttr, labg.Name BranchName,  "
            + " lcc.grpname AppntName, null AppntBirthday,  "
            + " '' AppntSex,  "
            + " '' AppntIdType,  "
            + " '' AppntIdNo,  "
            + " '' AppntNativePlace,  "
            + " '' AppntMarriage,  "
            + " '' AppntOccupationType,  "
            + " '' AppntOccupationCode,"
            + " lcc.CValidate, lcc.CInValidate,  "
            + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate,  "
            + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate,  "
            + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode,  "
            + " lcc.Prem, lcc.Amnt, '1' , "
            + " lcc.Prem AllPolPrem,  "
            + " (select (case lpei.EdorType when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' end) from Lpgrpedoritem lpei where lpei.grpContNo = lcc.grpContNo and lpei.EdorType in ('CT', 'WT', 'XT') order by lpei.EdorAppDate desc fetch first rows only) ContAppFlag,  "
            + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag,  "
            + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate,  "
            + " lcc.payintv   PayTimes ,  "
            + " (select  a.riskcode riskcode from lbgrppol a,lmriskapp b where  a.grpcontno=lcc.grpcontno and a.riskcode=b.riskcode and b.subriskflag ='M'   "
            + " order by riskcode fetch first 1 rows only)  Riskcode  "
            + ",lcc.agentcom "
            + ",'银行渠道手工单' "
            + ",lcc.cardflag cardflag1 "
            + " from LbgrpCont lcc  "
            + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup  "
            + " where 1 = 1  "
            + getWherePart("lcc.ManageCom", "ManageCom", "like")
	        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
	        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
	        + tSql
	        + tSql2
	        + tSql5
	        + tSql6
	        + " ) as tmp "
	        + " group by tmp.Comcode,tmp.ManageCom, tmp.ManageComName, tmp.CardType, tmp.ContNo, tmp.PrtNo, "
	        + " tmp.SaleChnl, tmp.AgentCode, tmp.AgentName, tmp.BranchAttr, tmp.BranchName, "
	        + " tmp.AppntName, tmp.AppntBirthday, tmp.AppntSex, tmp.AppntIdType, "
	        + " tmp.AppntIdNo, tmp.AppntNativePlace, tmp.AppntMarriage, tmp.AppntOccupationType, "
	        + " tmp.AppntOccupationCode, tmp.CValidate, tmp.CInValidate, "
	        + " tmp.PolApplyDate, tmp.InputDate, tmp.ApproveDate, tmp.UWDate, tmp.SignDate, "
	        + " tmp.CustomGetPolDate, tmp.GetPolDate, tmp.PayMode, "
	        + " (case when tmp.ContAppFlag is null then '银保通撤单' else tmp.ContAppFlag end),tmp.ScanFlag,tmp.ScanDate, "
	        + " tmp.RiskCode," 
	        + " tmp.PayTimes,tmp.agentcom,tmp.cardflag,tmp.cardflag1 "
	        ;
	 
  fm.querySql.value = tStrSQL;
  var oldAction = fm.action;
  fm.action = "YBCBDetailSave.jsp";
  fm.submit();
  fm.action = oldAction;
}