<html>
	<%
	//Name：LLMainAskInput.jsp
	//Function：登记界面的初始化
	//Date：2005-07-23 16:49:22
	//Author：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LmriskPracticeInput.js"></SCRIPT>
		<%@include file="LmriskPracticeInputInit.jsp"%>
	</head>
	<body  onload="initForm();">
		<form action="./LmriskPracticeSave.jsp" method=post name=fm target="fraSubmit">
				<table>
					<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLMainAskInput);">
					</TD>
					<TD class= titleImg>险种查询及修改</TD>		
					<td>
		    		  <font color="#FF0000" size="2">
		    		  </font>
		    		</td>							
					</TR>
				</table>
        <Div  id= "divLLMainAskInput" style= "display: ''">
				<table class= common>
					<TR class= common8>
						<TD  class= title8>选择险种</TD>
						<TD  class= input8><Input NAME=RiskCode VALUE="" MAXLENGTH=10 CLASS=codeNo  readonly=true ondblclick="return showCodeList('grpriskcode', [this,Riskname],[0,1]);" onkeyup="return showCodeListKey('grpriskcode', [this,Riskname],[0,1]);" verify="code:riskcode" ><input class=codename name=Riskname readonly=true ></TD>
					</TR>
					<TR class= common8>
						<TD  class= input8 /> 
						<TD  class= input8 /> 
					</TR>
				</table>
						<INPUT VALUE="查  询" class= cssButton TYPE=button onclick="QueryRiskButt();">
				        <TR class= common8><TD  class= input8> <input class="common"   name=mComCode  TYPE=hidden  ></TD></TR>
				<br>
			</div>
	  <Div  id= "divLLMainAskInput4" align=center style= "display: ''">
        <Table  class= common>
            <TR  class= common>
             <TD text-align: left colSpan=1>
                 <span id="spanQueryLDRiskGrid" ></span> 
       	    </TD>
           </TR>
         </Table>					
           <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
           <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
           <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
           <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div>		
				<HR>
				<Div  id= "divLLMainAskInput5" style= "display: 'none'">
				<table class= common>
					<TR class= common8>
						<TD class=title8 style= "width: '5%'">限制保额</TD>
				    <TD class= input8><input class="common"  name=repRiskCode style= "width: '80%' " TYPE=hidden >
				                      <input class="common"  name=repCodeName style= "width: '80%'"></TD>
					</TR>
				</table>
				<INPUT VALUE="增  加" id=addbutton  disabled="disable" class= cssButton TYPE=button onclick="AddRiskButt();">
				<INPUT VALUE="修  改" id=updatebutton disabled="disable" class= cssButton TYPE=button onclick="RepRiskButt();">
				<INPUT VALUE="删  除" id=deletebutton disabled="disable" class= cssButton TYPE=button onclick="DelRiskButt();">
				<TR class= common8><TD  class= input8> <input class="common"   name=repBankCode  TYPE=hidden  ></TD></TR>
			</div>				
			<br>
			<input name= sql type=hidden class=common>
			<input name= mOperate type = hidden class=common>
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
