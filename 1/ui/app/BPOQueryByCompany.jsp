<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BPOQueryByCompany.jsp
//程序功能：查询分公司外包件数
//创建日期：2008-3-20
//创建人  ：ZhangJianBao
//更新记录：更新人    更新日期     更新原因/内容
%>
<html>
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="BPOQueryByCompany.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="BPOQueryByCompanyInit.jsp"%>
    <title>扫描录入</title>
  </head>
  <body  onload="initForm();" >
    <form action="" method=post name=fm target="fraSubmit">
      <table class="common">
        <tr class="common">
          <td class="title">管理机构</td>
          <td class="input" COLSPAN="1">
          <input class= "codeno"  name=manageCom style="width:50" ondblclick="return showCodeList('comcode',[this,manageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,manageComName],[0,1],null,null,null,1);" verify="管理机构|notnull&code:comcode" ><input class=codename style="width:100" name=manageComName elementtype=nacessary readonly></TD>
        </td>
        <td class="title">外包状态</td>
        <td class="input" COLSPAN="1">
          <input class="codeNo" name="state" CodeData="0|^01|发送^02|接收" ondblclick="showCodeListEx('stateList',[this,stateName],[0,1]);" /><input class="codename" name="stateName" />
        </td>
        <td class="title">扫描件类型</td>
        <td class="input" COLSPAN="1">
          <input class="codeNo" name="subType" CodeData="0|^TB01|标准个险^TB05|银行保单" ondblclick="showCodeListEx('subtypeList',[this,subtypeName],[0,1]);" /><input class="codename" name="subtypeName" />
        </td>
      </tr>
      <tr class="common">
        <td class="title">统计日期起</td>
        <td class="input" COLSPAN="1">
          <input NAME="startDate" class="coolDatePicker" verify="统计日期起|notnull&date" elementtype=nacessary style="width:120">
        </td>
        <td class="title">止</td>
        <td class="input" COLSPAN="3">
          <input NAME="endDate" class="coolDatePicker" verify="统计日期止|notnull&date" elementtype=nacessary style="width:120">
        </td>
      </tr>
    </table>
    <input value="查  询" class =cssButton type=button onclick="queryBPONo();">
    <input value="下  载" class =cssButton type=button onclick="queryDown();">
    <input type=hidden name="sql">
  <br></br>
  <table>
    <tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBPONoGrid);">
      </td>
      <td class= titleImg>
        分公司外包件数
      </td>
    </tr>
  </table>
  <Div  id= "divBPONoGrid" style= "display: ''" align=center>
    <table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1>
          <span id="spanBPONoGrid" >
          </span>
        </td>
      </tr>
    </table>
  </Div>
  <Div id= "divPage" align=center style= "display: '' ">
    <input value="首  页"  class =  cssButton type=button onclick="turnPage.firstPage();">
    <input value="上一页" class = cssButton type=button onclick="turnPage.previousPage();">
    <input value="下一页" class = cssButton type=button onclick="turnPage.nextPage();">
    <input value="尾  页"  class =  cssButton type=button onclick="turnPage.lastPage();">
  </Div>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
