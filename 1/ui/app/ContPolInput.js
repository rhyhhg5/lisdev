//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var arrResult;
var mDebug = "0";
var mOperate = "";
var mAction = "";
// top.fraPic.window.service.focus();
var mSwitch = parent.VD.gVSwitch;
var mShowCustomerDetail = "GROUPPOL";
var turnPage = new turnPageClass();
var cflag = "5";
var mWFlag = 0 ;
var mGrpContNo;
var LoadFlag;
var Resource;


parent.fraMain.rows = "0,0,0,0,*";
/*********************************************************************
 *  保存集体投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm() {    
	
	//校验是否是新余及其下级机构社保直销渠道团单
	//var tActivityID = "<%=request.getParameter("ActivityID")%>" ;
	if(type == "2"){
	if(!checkXinYu()){
		return false;
	}
	}
	//校验投保人地址
	if (!CheckAddress()) {
		return false;
	}
	
	BookingPayIntyGrid.delBlankLine();
	DiseaseGrid.delBlankLine();
	HistoryImpartGrid.delBlankLine();
	if(!chkDayAndMonth()) return false;
	
	// by gzh 20101118
	// 选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
	
	// by gzh 20130408 校验综合开拓
	if(!ExtendCheck())
	{
		return false;
	}
	
	if( verifyInput2() == false )
    return false;
    
    // 生效日期校验
    if(!chkValidate())
    {
        if(!confirm("生效日期早于录入日期，是否继续录入？"))
            return false;
    }
  //投保人属性为自然人的保单支持银行转账缴费方式，投保人属性为法人的保单选择银行转账缴费方式时，在保存和修改时阻断提示
    if(!checkBank())
    {
    	return false;
    } 
    
  if(fm.all('PayMode').value=="4" && fm.all('BankCode').value=="") {
    alert("请输入银行转帐的银行和该银行的帐号");
    fm.all('BankCode').focus();
    return false;
  }

  if(fm.all('MarketType').value == ""){
  	alert("市场类型不能为空,请录入市场类型");
  	return false;
  }

    if(!checkSalechnlAgentCom()){
    	return false;
    }
    /*
	 * if(fm.all('SaleChnl').value=="03" && fm.all('AgentCom').value=="") {
	 * alert("销售渠道为中介，请录入中介公司代码！"); fm.all('AgentCom').focus(); return false; }
	 */
  // 开办市县数和开办市县内容校验
// if(!checkCity(1))
// {
// return false;
// }

    // 集团交叉业务要素校验
    if(!checkCrsBussParams())
    {
        return false;
    }
    
    // by gzh 20140403 #1870
    if(!checkOrgancomCode()){
    	return false;
    }
    
    // 2016-05-09 赵庆涛 #2810
    // 统一社会信用代码”为非必录项，但必须18位，数字和英文字母组合。
// if(!checkUnifiedSocialCreditNo()){
// return false;
// }
      
    // 2016-10-24 赵庆涛 #3093
// 投保人属性为必录项，如果是法人,要求组织机构代码、统一社会信用代码至少录入其一。如果是自然人，则“联系人姓名”栏中的联系人姓名、证件类型、联系人证件号码、证件生效日期、时效日期都必须录入。
// if(!checkInsuredProperty()){
// return false;
// }
    
    var chkInsuredPropRes = chkInsuredProp();
    if(chkInsuredPropRes!=""){
    	alert(chkInsuredPropRes);
    	return false;
    }
    var chkBeneficRes = chkBeneficiaryDetail();
    if(chkBeneficRes!=""){
    	alert(chkBeneficRes);
    	return false;
    }
    
    // 2017-02-08 赵庆涛
// 社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
    if(!checkCrs_SaleChnl()){
    	return false;
    }

    // 2017-07-04 赵庆涛
// 纳税人信息录入校验
    if(!checkTaxpayer()){
    	return false;
    }

    try{
    	// add by zjd 20140902
        if(fm.ContPrintType.value!="5"){
        	//投保人属性选择为自然人时，团体基本资料中，员工总人数、在职人数、行业编码、企业类型编码调整为非必录项
        	//1|法人^2|自然人
        	var InsuredProperty = fm.InsuredProperty.value;
        	if (InsuredProperty == "1") {
        		//行业编码和企业类型编码
        		if(fm.BusinessType.value=="" || fm.GrpNature.value== ""){
            		alert("投保人属性选择为法人时,非汇交件保单的行业编码和企业类型编码不能为空！");
            		fm.BusinessType.focus();
            		return false;
            	}
        		//在职人数
        		if(fm.AppntOnWorkPeoples.value==""){
            		alert("投保人属性选择为法人时,非汇交件保单投保信息中的在职人数不能为空！");
            		fm.AppntOnWorkPeoples.focus();
            		return false;
            	}else{
            		var tAppntOnWorkPeoples=fm.AppntOnWorkPeoples.value;
            		if(isNaN(tAppntOnWorkPeoples)){
            			alert("在职人数必须是数字！");
            			fm.AppntOnWorkPeoples.focus();
                		return false;
            		}
            	}
        		//员工总人数
        		if(fm.Peoples.value==""){
            		alert("投保人属性选择为法人时,非汇交件保单的员工总人数不能为空！");
            		fm.Peoples.focus();
            		return false;
            	}else{
            		var tPeoples=fm.Peoples.value;
            		if(isNaN(tPeoples)){
            			alert("员工总人数必须是数字！");
            			fm.Peoples.focus();
                		return false;
            		}
            	}
        		
			}
        	
        	if(fm.LinkMan1.value=="" || fm.Phone1.value==""){
        		alert("非汇交件保单的联系人姓名和联系电话不能为空！");
        		fm.LinkMan1.focus();
        		return false;
        	}else{
        	    var tlinkman=fm.LinkMan1.value;
        	    var tphone=fm.Phone1.value;
        		if(tlinkman.length>10){
        			alert("联系人姓名太长，最大长度为10！");
        			fm.LinkMan1.focus();
        			return false;
        		}
        		if(tphone.length>30){
        			alert("联系电话太长，最大长度为30！");
        			fm.Phone1.focus();
        			return false;
        		}
        	}
        	
        	if(fm.AppntOnWorkPeoples.value!=""){
        		var tAppntOnWorkPeoples=fm.AppntOnWorkPeoples.value;
        		if(isNaN(tAppntOnWorkPeoples)){
        			alert("在职人数必须是数字！");
        			fm.AppntOnWorkPeoples.focus();
            		return false;
        		}
        	}
        	
        	if(fm.OnWorkPeoples.value==""){
        		alert("非汇交件保单参保信息中的在职人数不能为空！");
        		fm.OnWorkPeoples.focus();
        		return false;
        	}else{
        		var tOnWorkPeoples=fm.OnWorkPeoples.value;
        		if(isNaN(tOnWorkPeoples)){
        			alert("在职人数必须是数字！");
        			fm.OnWorkPeoples.focus();
            		return false;
        		}
        	}
        	
        	if(fm.Peoples.value!=""){
        		var tPeoples=fm.Peoples.value;
        		if(isNaN(tPeoples)){
        			alert("员工总人数必须是数字！");
        			fm.Peoples.focus();
            		return false;
        		}
        	}
        }
    }catch(e){
    	alert("错误"+e.message);
    }
    
    if(!checkPhone(fm.Phone.value)){
    	return false;
    }
    if(!checkPhone(fm.Phone1.value)){
    	return false;
    }
    if(fm.Mobile1.value!=null && fm.Mobile1.value!=""){
    	var str="";
    	str=CheckPhone(fm.Mobile1.value);
    	if(str!=""){
    		alert(str);
    		return false;
    	}
    }
    // --------------------

// 2006-03-20 闫少杰 约定缴费方式时进行的校验 --- START
    // 1 缴费期次的缴费时间必须是递增的；
    // 2 约定缴费金额的合计应该等于总保费；
    /*
	 * if(fm.all('GrpContPayIntv').value=="-1") {
	 * if(BookingPayIntyGrid.mulLineCount==0) {
	 * alert("缴费频次为约定缴费，必须录入约定的缴费时间和缴费内容！"); return false; } else { var
	 * sumSpecFee=0; //约定缴费金额的合计 var earlyDate = ""; //前一个缴费期次的日期 var laterDate =
	 * ""; //后一个缴费期次的日期 for(var indexOfSpecFee=0;indexOfSpecFee<BookingPayIntyGrid.mulLineCount;indexOfSpecFee++) {
	 * //检验录入的缴费日期是否为约定日期格式 if( isDate(
	 * BookingPayIntyGrid.getRowColData(indexOfSpecFee,1) ) ) {
	 * if(indexOfSpecFee==0) { earlyDate =
	 * BookingPayIntyGrid.getRowColData(indexOfSpecFee,1); } else { laterDate =
	 * BookingPayIntyGrid.getRowColData(indexOfSpecFee,1);
	 * if(compareDate(laterDate,earlyDate)!=1) //laterDate>earlyDate时，函数返回值为1 {
	 * alert("缴费期次的缴费时间必须是按照期次进行递增的，即第二次的缴费日期在第一次的缴费日期之后，以此类推！"); return false; }
	 * earlyDate = laterDate; //将当前行日期置为earlyDate，为下次比较做准备 } } else {
	 * alert("录入的约定缴费时间存在错误，请确认日期格式为'yyyy-mm-dd'并且日期填写正确！"); return false; }
	 * //校验录入的缴费金额是否为合法数字 if( isNumeric(
	 * BookingPayIntyGrid.getRowColData(indexOfSpecFee,2) ) ) { sumSpecFee =
	 * sumSpecFee + Number(BookingPayIntyGrid.getRowColData(indexOfSpecFee,2)); }
	 * else { alert("录入的约定缴费金额有误，请确认您录入的缴费金额！"); return false; } }
	 * //alert("约定缴费金额的合计="+sumSpecFee);
	 * //alert("保费合计="+fm.GrpContSumPrem.value);
	 * if(sumSpecFee!=fm.GrpContSumPrem.value) {
	 * alert("约定缴费金额的合计和保费合计不相等，请确认后进行修改！"); return false; } } }
	 */
// 2006-03-20 闫少杰 约定缴费方式时进行的校验 --- END

  if(dateDiff(fm.all('PolApplyDate').value, fm.all('CValiDate').value,"D") > 365) {
    alert("保单生效日期最多为投保日期后一年");
    fm.all('PolApplyDate').focus();
    return false;
  }

    if(dateDiff( fm.all('CValiDate').value,fm.all('CInValiDate').value,"D")< 0) {
    alert("保单生效日期不应在保单失效日期之前!");
    fm.all('CInValiDate').focus();
    return false;
  }
  if(dateDiff(fm.all('CValiDate').value, fm.all('HandlerDate').value,"D") > 365) {
    alert("保单生效日期最多为投保填写日期前一年");
    fm.all('HandlerDate').focus();
    return false;
  }
	if(dateDiff(fm.all('CValiDate').value, fm.all('HandlerDate').value,"D")>1)
	{
		if(!confirm("填单日期大于保单生效日期！是否继续录入？"))
		{
			return false;
		}
	}
	if(!checkInsurePeriod())
	{
	  return false;
	}
	if ( fm.all('ContPrintType').value==null || fm.all('ContPrintType').value=="") {
   alert("保单打印类型不能为空!");
   fm.all('ContPrintType').focus();
   return false;
	}
  // 校验被保人人数
  /*
	 * if(fm.all('Peoples3').value="0"){ alert("被保人数不能为0");
	 * fm.all('Peoples3').focus(); return false;
	 *  }
	 */

  // 员工总人数=在职人数+退休人数+其它人员人数
	if(fm.ContPrintType.value!="5"){
		 if ( fm.all('Peoples').value==null || fm.all('Peoples').value=="") {
			    fm.all('Peoples').value = 0
				}
				if ( fm.all('AppntOnWorkPeoples').value==null || fm.all('AppntOnWorkPeoples').value=="") {
					fm.all('AppntOnWorkPeoples').value = 0
				}
				if ( fm.all('AppntOffWorkPeoples').value==null || fm.all('AppntOffWorkPeoples').value=="") {
					fm.all('AppntOffWorkPeoples').value = 0
				}
				if ( fm.all('AppntOtherPeoples').value==null || fm.all('AppntOtherPeoples').value=="") {
					fm.all('AppntOtherPeoples').value = 0
				}
				  if (CheckDateDollar()==false)
			  {
			  	return false;
			  }
			  var intPeoples=parseInt(fm.all('Peoples').value);
			  var intAppntOnWorkPeoples=parseInt(fm.all('AppntOnWorkPeoples').value);
			  var intAppntOffWorkPeoples=parseInt(fm.all('AppntOffWorkPeoples').value);
			  var intAppntOtherPeoples=parseInt(fm.all('AppntOtherPeoples').value );

			  //投保人属性为自然人时不做校验1法人2自然人
			  if (fm.InsuredProperty.value == "1") {
				  if (intPeoples!=intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples) {
					    alert("员工总人数应该为"+(intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples));
					    fm.all('Peoples').focus();
					    return false;
					  }
			}
			 
	}
 

  if (GrpNameDif()==false) {
    alert("你修改了投保团体名称,请删除团体客户号,再保存");
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

  LCImpartInput();  // 团体告知
  if (InputMuline1()==false)
  {
     return false;
  }
  ImpartGrid.delBlankLine();
  BaseImpartDetailGrid.delBlankLine();
  HealthImpartDetailGrid.delBlankLine();
  NewHistoryImpartGrid.delBlankLine();
  if(CheckImpart()==false)
  {
  	return false;
  }
  
  
  // 业务员身份账号校验
 	var tHandlerIDNo=fm.HandlerIDNo.value;
 	if(tHandlerIDNo!=null&&tHandlerIDNo!=""){
 		var strChkIdNo=checkIdNo("0",tHandlerIDNo,"","");
 		if(strChkIdNo !=""){
 			alert("业务经办人"+strChkIdNo);
 			return false;
 		}
 	}
 	
 	  
 	// 法人身份账号校验
 		var tLegalPersonIDNo=fm.LegalPersonIDNo.value;
 		if(tLegalPersonIDNo!=null && tLegalPersonIDNo!=""){
 			var strChkIdNo=checkIdNo("0",tLegalPersonIDNo,"","");
 			if(strChkIdNo !=""){
 				alert("法人"+strChkIdNo);
 				return false;
 			}
 		}
 		
 		// 单位授权经办人身份账号校验
 		var tIDNo=fm.IDNo.value;
 		var tIDType=fm.IDType.value;
 		if(tIDNo!=null && tIDNo!=""){
 			var strChkIdNo=checkIdNo(tIDType,tIDNo,"","");
 			if(strChkIdNo !=""){
 				alert("联系人 "+strChkIdNo);
 				return false;
 			}
 		}
 		
  //投保单填写日期(HandlerDate)[不可为空] <= 业务员填写日期(AgentDate)[可为空] <= 接收日期(ReceiveDate)[不可为空] <= current date 
   if (!CheckDate()) {
	return false;
   }
  //团体基本资料中联系人姓名、证件类型、联系人证件号码、证件生效日期、失效日期为必录项,当证件失效日期为长期有效时，此时失效日期为非必录项。
   if (!CheckLinkMan()) {
	   return false;
   }
	
  if( mAction == "" ) {
    // showSubmitFrame(mDebug);
    mAction = "INSERT";
    fm.all( 'fmAction' ).value = mAction;
    fm.all( 'LoadFlag' ).value = LoadFlag;
	
    if (fm.all('ProposalGrpContNo').value != "") {
      alert("保存结果只能进行修改操作！");
      mAction = "";
    } else {
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      tAction = fm.action;
      fm.action="../app/ContPolSave.jsp"
      ChangeDecodeStr();
      fm.submit(); // 提交
    }
  }

}

/*******************************************************************************
 * 保存个人投保单的提交后的操作,服务器数据返回后执行的操作 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function afterSubmit( FlagStr, content ,RiskCode ,GrpPolNo) {
	UnChangeDecodeStr();
  showInfo.close();
  window.focus();
  LCImpartStr1 = "";
	LCImpartStr2 = "";
	LCImpartStr3 = "";
	ImpartCheck1 = new Array();
	ImpartCheck2 = new Array();
	ImpartCheck3 = new Array();
  if( FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
    content = "处理成功！";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
     fillriskgrid();
    showDiv(operateButton, "true");
    showDiv(inputButton, "false");
  }
  displayBnf();
  mAction = "";
  if(this.ScanFlag == "1") {
  	// 注掉下面一行，因为在点击修改后会把印刷号置空，
    // fm.PrtNo.value=prtNo;
    initRiskGrid();
    fillriskgrid();
  }
  // alert(mWFlag+" "+FlagStr);

  if(mWFlag == 1 && FlagStr != "Fail") {
  	top.window.close();
    window.location.href("./ContPolInput.jsp");
  }
	if(RiskCode != null && RiskCode != "")
	{
		var strSql = "select 1 from lmrisktoacc where RiskCode='"+RiskCode+"' and RiskCode != '162401' ";
		var arr = easyExecSql(strSql);
		if(arr)
		{
			if(confirm("录入险种为账户险需要定义账户信息"))
			{
				grpPubAccInput(RiskCode,GrpPolNo);
			}
			else
				{
					alert("账户型险种必须定义账户信息");
					grpPubAccInput(RiskCode,GrpPolNo);
				}
		}
		
		var strSql = "select 1 from lcgrppol a where prtno='"+prtNo+"' and RiskCode in ('163001','163002') and not exists (select 1 from LCGrpContRoad where prtno=a.prtno) ";
		var arr = easyExecSql(strSql);
		if(arr){
			if(confirm(RiskCode+"险种需要录入一带一路产品要素")){
				insertLCGrpContLoad();
			}else{
				alert(RiskCode+"险种必须录入一带一路产品要素");
				insertLCGrpContLoad();
			}
		}else{
			if(RiskCode=="163001" ||　RiskCode=="163002"){
				if(confirm(RiskCode+"险种需要录入一带一路产品要素")){
					insertLCGrpContLoad();
				}else{
					alert(RiskCode+"险种必须录入一带一路产品要素");
					insertLCGrpContLoad();
				}
			}
		}
//		if(arr||RiskCode=="163001"){
//			if(confirm("163001险种需要录入一带一路产品要素"))
//			{
//				insertLCGrpContLoad();
//			}
//			else
//				{
//					alert("163001险种必须录入一带一路产品要素");
//					insertLCGrpContLoad();
//				}
//		}
	}
  // location.reload();
}

/*******************************************************************************
 * "重置"按钮对应操作 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function resetForm() {
  try {
    initForm();
    fm.all('PrtNo').value = prtNo;
  } catch( re ) {
    alert("在GroupPolInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

/*******************************************************************************
 * "取消"按钮对应操作 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function cancelForm() {
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

/*******************************************************************************
 * 显示frmSubmit框架，用来调试 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function showSubmitFrame(cDebug) {
  if( cDebug == "1" )
    parent.fraMain.rows = "0,0,50,82,*";
  else
    parent.fraMain.rows = "0,0,0,72,*";
}

/*******************************************************************************
 * Click事件，当点击增加图片时触发该函数 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function addClick() {

  // 下面增加相应的代码
  showDiv( operateButton, "false" );
  showDiv( inputButton, "true" );

  fm.all('RiskCode').value = "";

  // 保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
  if (BQFlag=="2") {
    var strSql = "select grppolno, grpno from lcgrppol where prtno='" + prtNo + "' and riskcode in (select riskcode from lmriskapp where subriskflag='M')";
    var arrResult = easyExecSql(strSql);
    // alert(arrResult);

    mOperate = 1;
    afterQuery(arrResult);

    // strSql = "select GrpNo,GrpName,GrpAddress,Satrap from LDGrp where
	// GrpNo='" + arrResult[0][1] + "'";
    // arrResult = easyExecSql(strSql);
    // mOperate = 2;
    // afterQuery(arrResult);

    fm.all('RiskCode').value = BQRiskCode;
    fm.all('RiskCode').className = "readonly";
    fm.all('RiskCode').readOnly = true;
    fm.all('RiskCode').ondblclick = "";
  }

  fm.all('ContNo').value = "";
  fm.all('ProposalGrpContNo').value = "";
}

/*******************************************************************************
 * Click事件，当点击“查询”图片时触发该函数 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function queryClick() {
  if(this.ScanFlag == "1") {
    alert( "有扫描件录入不允许查询!" );
    return false;
  }
  if( mOperate == 0 ) {
    mOperate = 1;
    // cContNo = fm.all( 'ContNo' ).value;
    showInfo = window.open("./GroupPolQueryMain.jsp");
  }
}


/*******************************************************************************
 * Click事件，当点击“修改”图片时触发该函数 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function updateClick() {
	
	//校验是否是新余及其下级机构社保直销渠道团单
	if(type == "2" ){
		if(!checkXinYu()){
			return false;
		}
		}
	
	//校验投保人地址
	if (!CheckAddress()) {
		return false;
	}
	
    // 销售渠道和业务员的校验
	if(!checkSaleChnl())
	{
		return false;
	}
    
    // 生效日期校验
    if(!chkValidate())
    {
        if(!confirm("生效日期早于录入日期，是否继续录入？"))
            return false;
    }
    // --------------------
    
 // 业务员身份账号校验
	var tHandlerIDNo=fm.HandlerIDNo.value;
	if(tHandlerIDNo!=null&&tHandlerIDNo!=""){
		var strChkIdNo=checkIdNo("0",tHandlerIDNo,"","");
		if(strChkIdNo !=""){
			alert("业务经办人"+strChkIdNo);
			return false;
		}
	}
	
	  
	// 法人身份账号校验
		var tLegalPersonIDNo=fm.LegalPersonIDNo.value;
		if(tLegalPersonIDNo!=null && tLegalPersonIDNo!=""){
			var strChkIdNo=checkIdNo("0",tLegalPersonIDNo,"","");
			if(strChkIdNo !=""){
				alert("法人"+strChkIdNo);
				return false;
			}
		}
		
		// 单位授权经办人身份账号校验
		var tIDNo=fm.IDNo.value;
		var tIDType=fm.IDType.value;
		if(tIDNo!=null && tIDNo!=""){
			var strChkIdNo=checkIdNo(tIDType,tIDNo,"","");
			if(strChkIdNo !=""){
				alert("联系人"+strChkIdNo);
				return false;
			}
		}
	
	
	
    // 校验业务员代码与销售渠道是否匹配 by zhangyang 2011-01-06
    if(!CheckAgentCode())
    {
    	return false;
    }
    
    
    // by gzh 20101118
	// 选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
    // 集团交叉业务要素校验
    // if(!checkCrsBussParams())
    // {
        // return false;
    // }
    // --------------------
    
    // by gzh 20130408 校验综合开拓
	if(!ExtendCheck())
	{
		return false;
	}
	
	// add by zjd 20140902
	 if(fm.ContPrintType.value!="5"){
     	//投保人属性选择为自然人时，团体基本资料中，员工总人数、在职人数、行业编码、企业类型编码调整为非必录项
     	//1|法人^2|自然人
     	var InsuredProperty = fm.InsuredProperty.value;
     	if (InsuredProperty == "1") {
     		//行业编码和企业类型编码
     		if(fm.BusinessType.value=="" || fm.GrpNature.value== ""){
         		alert("投保人属性选择为法人时,非汇交件保单的行业编码和企业类型编码不能为空！");
         		fm.BusinessType.focus();
         		return false;
         	}
     		//在职人数
     		if(fm.AppntOnWorkPeoples.value==""){
         		alert("投保人属性选择为法人时,非汇交件保单投保信息中的在职人数不能为空！");
         		fm.AppntOnWorkPeoples.focus();
         		return false;
         	}else{
         		var tAppntOnWorkPeoples=fm.AppntOnWorkPeoples.value;
         		if(isNaN(tAppntOnWorkPeoples)){
         			alert("在职人数必须是数字！");
         			fm.AppntOnWorkPeoples.focus();
             		return false;
         		}
         	}
     		//员工总人数
     		if(fm.Peoples.value==""){
         		alert("投保人属性选择为法人时,非汇交件保单的员工总人数不能为空！");
         		fm.Peoples.focus();
         		return false;
         	}else{
         		var tPeoples=fm.Peoples.value;
         		if(isNaN(tPeoples)){
         			alert("员工总人数必须是数字！");
         			fm.Peoples.focus();
             		return false;
         		}
         	}
     		
			}
     	
     	if(fm.LinkMan1.value=="" || fm.Phone1.value==""){
     		alert("非汇交件保单的联系人姓名和联系电话不能为空！");
     		fm.LinkMan1.focus();
     		return false;
     	}else{
     	    var tlinkman=fm.LinkMan1.value;
     	    var tphone=fm.Phone1.value;
     		if(tlinkman.length>10){
     			alert("联系人姓名太长，最大长度为10！");
     			fm.LinkMan1.focus();
     			return false;
     		}
     		if(tphone.length>30){
     			alert("联系电话太长，最大长度为30！");
     			fm.Phone1.focus();
     			return false;
     		}
     	}
     	
     	if(fm.AppntOnWorkPeoples.value!=""){
     		var tAppntOnWorkPeoples=fm.AppntOnWorkPeoples.value;
     		if(isNaN(tAppntOnWorkPeoples)){
     			alert("在职人数必须是数字！");
     			fm.AppntOnWorkPeoples.focus();
         		return false;
     		}
     	}
     	
     	if(fm.OnWorkPeoples.value==""){
     		alert("非汇交件保单参保信息中的在职人数不能为空！");
     		fm.OnWorkPeoples.focus();
     		return false;
     	}else{
     		var tOnWorkPeoples=fm.OnWorkPeoples.value;
     		if(isNaN(tOnWorkPeoples)){
     			alert("在职人数必须是数字！");
     			fm.OnWorkPeoples.focus();
         		return false;
     		}
     	}
     	
     	if(fm.Peoples.value!=""){
     		var tPeoples=fm.Peoples.value;
     		if(isNaN(tPeoples)){
     			alert("员工总人数必须是数字！");
     			fm.Peoples.focus();
         		return false;
     		}
     	}
     }
	 
	 if(!checkPhone(fm.Phone.value)){
	    	return false;
	    }
	 if(!checkPhone(fm.Phone1.value)){
	    	return false;
	    }
	 if(fm.Mobile1.value!=null && fm.Mobile1.value!=""){
	    	var str="";
	    	str=CheckPhone(fm.Mobile1.value);
	    	if(str!=""){
	    		alert(str);
	    		return false;
	    	}
	    }
	// by gzh 20140403 #1870 后来确认，修改时，无需校验
	/*
	 * if(!checkOrgancomCode()){ return false; }
	 */
    
	// 2016-05-09 赵庆涛 #2810
	// 统一社会信用代码”为非必录项，但必须18位，数字和英文字母组合。
// if(!checkUnifiedSocialCreditNo()){
// return false;
// }
	 
	    // 2016-10-24 赵庆涛 #3093
// 投保人属性为必录项，如果是法人,要求组织机构代码、统一社会信用代码至少录入其一。如果是自然人，则“联系人姓名”栏中的联系人姓名、证件类型、联系人证件号码、证件生效日期、时效日期都必须录入。
// if(!checkInsuredProperty()){
// return false;
// }

	 var chkInsuredPropRes = chkInsuredProp();
	    if(chkInsuredPropRes!=""){
	    	alert(chkInsuredPropRes);
	    	return false;
	    }
	    
	    var chkBeneficRes = chkBeneficiaryDetail();
	    if(chkBeneficRes!=""){
	    	alert(chkBeneficRes);
	    	return false;
	    }
	 
	    // 2017-02-08 赵庆涛
	// 社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
	if(!checkCrs_SaleChnl()){
	  	return false;
	}

    // 2017-07-04 赵庆涛
// 纳税人信息录入校验
    if(!checkTaxpayer()){
    	return false;
    }

	// ChangeDecodeStr();
	BookingPayIntyGrid.delBlankLine();
	DiseaseGrid.delBlankLine();
	HistoryImpartGrid.delBlankLine();
	if( verifyInput2() == false )
    return false;
	if(!chkDayAndMonth()) return false;
	
	//投保人属性为自然人的保单支持银行转账缴费方式，投保人属性为法人的保单选择银行转账缴费方式时，在保存和修改时阻断提示
    if(!checkBank())
    {
    	return false;
    }
	
  if(fm.all('PayMode').value=="4" && fm.all('BankCode').value=="") {
    alert("请输入银行转帐的银行和该银行的帐号");
    fm.all('BankCode').focus();
    return false;
  }
  
  if(fm.all('MarketType').value == ""){
  	alert("市场类型不能为空,请录入市场类型");
  	return false;
  }
  if(!checkSalechnlAgentCom()){
    return false;
  }
  // alert(fm.all('SaleChnl').value);
 // alert(fm.all('AgentCom').value);
 /*
	 * if(fm.all('SaleChnl').value=="03" && fm.all('AgentCom').value=="") {
	 * alert("销售渠道为中介，请录入中介公司代码！"); //fm.all('AgentCom').focus(); return false; }
	 */
  // 开办市县数和开办市县内容校验
// if(!checkCity(1))
// {
// return false;
// }
// 2006-03-20 闫少杰 约定缴费方式时进行的校验 --- START
    // 1 缴费期次的缴费时间必须是递增的；
    // 2 约定缴费金额的合计应该等于总保费；
    /*
	 * if(fm.all('GrpContPayIntv').value=="-1") {
	 * if(BookingPayIntyGrid.mulLineCount==0) {
	 * alert("缴费频次为约定缴费，必须录入约定的缴费时间和缴费内容！"); return false; } else { var
	 * sumSpecFee=0; //约定缴费金额的合计 var earlyDate = ""; //前一个缴费期次的日期 var laterDate =
	 * ""; //后一个缴费期次的日期 for(var indexOfSpecFee=0;indexOfSpecFee<BookingPayIntyGrid.mulLineCount;indexOfSpecFee++) {
	 * //检验录入的缴费日期是否为约定日期格式 if( isDate(
	 * BookingPayIntyGrid.getRowColData(indexOfSpecFee,1) ) ) {
	 * if(indexOfSpecFee==0) { earlyDate =
	 * BookingPayIntyGrid.getRowColData(indexOfSpecFee,1); } else { laterDate =
	 * BookingPayIntyGrid.getRowColData(indexOfSpecFee,1);
	 * if(compareDate(laterDate,earlyDate)!=1) //laterDate>earlyDate时，函数返回值为1 {
	 * alert("缴费期次的缴费时间必须是按照期次进行递增的，即第二次的缴费日期在第一次的缴费日期之后，以此类推！"); return false; }
	 * earlyDate = laterDate; //将当前行日期置为earlyDate，为下次比较做准备 } } else {
	 * alert("录入的约定缴费时间存在格式错误，请确认日期格式为'yyyy-mm-dd'！"); return false; }
	 * //校验录入的缴费金额是否为合法数字 if( isNumeric(
	 * BookingPayIntyGrid.getRowColData(indexOfSpecFee,2) ) ) { sumSpecFee =
	 * sumSpecFee + Number(BookingPayIntyGrid.getRowColData(indexOfSpecFee,2)); }
	 * else { alert("录入的约定缴费金额有误，请确认您录入的缴费金额！"); return false; } }
	 * //alert("约定缴费金额的合计="+sumSpecFee);
	 * //alert("保费合计="+fm.GrpContSumPrem.value);
	 * if(sumSpecFee!=fm.GrpContSumPrem.value) {
	 * alert("约定缴费金额的合计和保费合计不相等，请确认后进行修改！"); return false; } } }
	 */
// 2006-03-20 闫少杰 约定缴费方式时进行的校验 --- END

    if(dateDiff( fm.all('CValiDate').value,fm.all('CInValiDate').value,"D")< 0) {
    alert("保单生效日期不应在保单失效日期之前!");
    fm.all('CInValiDate').focus();
    return false;
  }
  if(dateDiff(fm.all('PolApplyDate').value, fm.all('CValiDate').value,"D") > 365) {
    alert("保单生效日期最多为投保填写日期前一年");
    fm.all('PolApplyDate').focus();
    return false;
  }
  if(dateDiff(fm.all('CValiDate').value, fm.all('HandlerDate').value,"D") > 365) {
    alert("保单生效日期最多为投保填写日期前一年");
    fm.all('HandlerDate').focus();
    return false;
  }
  // alert(dateDiff(fm.all('CValiDate').value,
	// fm.all('HandlerDate').value,"D"));
	if(dateDiff(fm.all('CValiDate').value, fm.all('HandlerDate').value,"D")>=1)
	{
		if(!confirm("填单日期大于保单生效日期！是否继续录入？"))
		{
			return false;
		}
	}
	if(!checkInsurePeriod())
	{
	  return false;
	}
		if ( fm.all('ContPrintType').value==null || fm.all('ContPrintType').value=="") {
   alert("保单打印类型不能为空!");
   fm.all('ContPrintType').focus();
   return false;
	}
  // 员工总人数=在职人数+退休人数+其它人员人数
  if(fm.ContPrintType.value!="5"){// 非汇交件校验逻辑
	  if ( fm.all('Peoples').value==null || fm.all('Peoples').value=="") {
		    fm.all('Peoples').value = 0
			}
			if ( fm.all('AppntOnWorkPeoples').value==null || fm.all('AppntOnWorkPeoples').value=="") {
				fm.all('AppntOnWorkPeoples').value = 0
			}
			if ( fm.all('AppntOffWorkPeoples').value==null || fm.all('AppntOffWorkPeoples').value=="") {
				fm.all('AppntOffWorkPeoples').value = 0
			}
			if ( fm.all('AppntOtherPeoples').value==null || fm.all('AppntOtherPeoples').value=="") {
				fm.all('AppntOtherPeoples').value = 0
			}

			var intPeoples=parseInt(fm.all('Peoples').value);
		  var intAppntOnWorkPeoples=parseInt(fm.all('AppntOnWorkPeoples').value);
		  var intAppntOffWorkPeoples=parseInt(fm.all('AppntOffWorkPeoples').value);
		  var intAppntOtherPeoples=parseInt(fm.all('AppntOtherPeoples').value );

		//投保人属性为自然人时不做校验1法人2自然人
		  if (fm.InsuredProperty.value == "1") {
			  if (intPeoples!=intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples) {
				    alert("员工总人数应该为"+(intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples));
				    fm.all('Peoples').focus();
				    return false;
				  }
		}
  }
  

  if (GrpNameDif()==false) {
    alert("你修改了投保团体名称,请删除团体客户号,再保存");
    return false;
  }

  var tProposalGrpContNo = "";
  tProposalGrpContNo = fm.all( 'ProposalGrpContNo' ).value;

// if (fm.GrpNo.value==''&&fm.GrpAddressNo.value!='') {
// alert("客户号为空，不能有地址编码");
// return false;
// }
  if(fm.GrpNo.value!='')
  {
    arrResult = easyExecSql("select * from LCGrpCont where ProposalGrpContNo = '" + tProposalGrpContNo + "'", 1, 0);
    if(arrResult==null)
    {
      alert("没有找到团体合同信息,请检查");
      return false;
    }
    if(fm.GrpNo.value!=arrResult[0][12])
    {
       alert("团体客户号与数据库中团体合同下的客户号不符,请检查");
       return false;
    }
  }
  LCImpartInput();  // 团体告知
if (InputMuline1()==false)
  {
     return false;
  }
  ImpartGrid.delBlankLine();
  BaseImpartDetailGrid.delBlankLine();
  HealthImpartDetailGrid.delBlankLine();
  NewHistoryImpartGrid.delBlankLine();
  if (CheckImpart()==false)
  {
  	return false;
  }
  if (CheckDateDollar()==false)
  {
  	return false;
  }
  
  //投保单填写日期(HandlerDate)[不可为空] <= 业务员填写日期(AgentDate)[可为空] <= 接收日期(ReceiveDate)[不可为空] <= current date 
  if (!CheckDate()) {
	return false;
  }
  
  //团体基本资料中联系人姓名、证件类型、联系人证件号码、证件生效日期、失效日期为必录项,当证件失效日期为长期有效时，此时失效日期为非必录项。
  if (!CheckLinkMan()) {
	   return false;
  }
  
  if( tProposalGrpContNo == null || tProposalGrpContNo == "" )
    if(this.ScanFlag == "1") {
      alert( "还未录入数据,请先增加合同信息,再进行修改!" );
    } else {
      alert( "请先做投保单保存操作，再进行修改!" );
    }
  else {
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

    if( mAction == "" ) {
      // showSubmitFrame(mDebug);
      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      mAction = "UPDATE";
      fm.all( 'fmAction' ).value = mAction;
      fm.action="../app/ContPolSave.jsp"
      ChangeDecodeStr();
      fm.submit(); // 提交
    }
  }
}

/*******************************************************************************
 * Click事件，当点击“删除”图片时触发该函数 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function deleteClick() {
  var tProposalGrpContNo = "";
  tProposalGrpContNo = fm.all( 'ProposalGrpContNo' ).value;
  if( tProposalGrpContNo == null || tProposalGrpContNo == "" )
    alert( "请先做投保单保存操作，然后再进行删除操作！" );
  else {
    if (confirm("您确定要删除该团单吗？")) {
      var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

      if( mAction == "" ) {
        // showSubmitFrame(mDebug);
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        mAction = "DELETE";
        fm.all( 'fmAction' ).value = mAction;
        fm.action="../app/ContPolSave.jsp"
                  fm.submit(); // 提交
        fm.all('Remark').value="";
        ImpartClear();
      }
    }
  }
}

/*******************************************************************************
 * 显示div 参数 ： 第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示 返回值： 无
 * ********************************************************************
 */
function showDiv(cDiv,cShow) {
  if( cShow == "true" )
    cDiv.style.display = "";
  else
    cDiv.style.display = "none";
}

/*******************************************************************************
 * 当点击“进入个人信息”按钮时触发该函数 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function intoPol() {
  // 下面增加相应的代码
  tProposalGrpContNo = fm.ProposalGrpContNo.value;
  if( tProposalGrpContNo == "" ) {
    alert("您必须先录入集体信息才能进入个人信息部分。");
    return false
         }

         // 把集体信息放入内存
         mSwitch = parent.VD.gVSwitch;  // 桢容错
  putGrpPol();

  try {
    goToPic(2)
  } catch(e) {}

  try {
    parent.fraInterface.window.location = "./ProposalGrpInput.jsp?LoadFlag=" + LoadFlag + "&type=" + type;
  } catch (e) {
    parent.fraInterface.window.location = "./ProposalGrpInput.jsp?LoadFlag=2&type=" + type;
  }
}

/*******************************************************************************
 * 把集体信息放入内存 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function putGrpPol() {
  delGrpPolVar();
  addIntoGrpPol();
}

/*******************************************************************************
 * 把集体信息放入加到变量中 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function addIntoGrpPol() {
  try {
    mSwitch.addVar( "intoPolFlag", "", "GROUPPOL" );
  } catch(ex) { }
  ;
  // body信息
  try {
    mSwitch.addVar( "BODY", "", window.document.body.innerHTML );
  } catch(ex) { }
  ;
  // 集体信息
  // 由"./AutoCreatLDGrpInit.jsp"自动生成
  try {
    mSwitch.addVar('GrpNo', '', fm.all('GrpNo').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PrtNo', '', fm.all('PrtNo').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Password', '', fm.all('Password').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpName', '', fm.all('GrpName').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpAddressCode', '', fm.all('GrpAddressCode').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpAddress', '', fm.all('GrpAddress').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpZipCode', '', fm.all('GrpZipCode').value);
  } catch(ex) { }
  ;
  try {
	  mSwitch.addVar('UnifiedSocialCreditNo', '', fm.all('UnifiedSocialCreditNo').value);
  } catch(ex) { }
  ;
  try {
	  mSwitch.addVar('InsuredProperty', '', fm.all('InsuredProperty').value);
  } catch(ex) { }
  ; 
  try {
	  mSwitch.addVar('TaxpayerType', '', fm.all('TaxpayerType').value);
  } catch(ex) { }
  ;
  try {
	  mSwitch.addVar('TaxNo', '', fm.all('TaxNo').value);
  } catch(ex) { }
  ;
  try {
	  mSwitch.addVar('CustomerBankCode', '', fm.all('CustomerBankCode').value);
  } catch(ex) { }
  ;
  try {
	  mSwitch.addVar('CustomerBankAccNo', '', fm.all('CustomerBankAccNo').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BusinessType', '', fm.all('BusinessType').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpNature', '', fm.all('GrpNature').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Peoples', '', fm.all('Peoples').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('RgtMoney', '', fm.all('RgtMoney').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Asset', '', fm.all('Asset').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('NetProfitRate', '', fm.all('NetProfitRate').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('MainBussiness', '', fm.all('MainBussiness').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Corporation', '', fm.all('Corporation').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ComAera', '', fm.all('ComAera').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LinkMan1', '', fm.all('LinkMan1').value);
  } catch(ex) { }
  ;
  try {
	mSwitch.addVar('DetailAddress', '', fm.all('DetailAddress').value);
  } catch(ex) { }
  ;
  try {
	mSwitch.addVar('ProvinceID', '', fm.all('ProvinceID').value);
  } catch(ex) { }
	  ;
  try {
	mSwitch.addVar('CityID', '', fm.all('CityID').value);
  } catch(ex) { }
	  ;
  try {
    mSwitch.addVar('Department1', '', fm.all('Department1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('HeadShip1', '', fm.all('HeadShip1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone1', '', fm.all('Phone1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('E_Mail1', '', fm.all('E_Mail1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Fax1', '', fm.all('Fax1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LinkMan2', '', fm.all('LinkMan2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Department2', '', fm.all('Department2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('HeadShip2', '', fm.all('HeadShip2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone2', '', fm.all('Phone2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('E_Mail2', '', fm.all('E_Mail2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Fax2', '', fm.all('Fax2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Fax', '', fm.all('Fax').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone', '', fm.all('Phone').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetFlag', '', fm.all('GetFlag').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Satrap', '', fm.all('Satrap').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('EMail', '', fm.all('EMail').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('FoundDate', '', fm.all('FoundDate').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntOnWorkPeoples', '', fm.all('AppntOnWorkPeoples').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntOffWorkPeoples', '', fm.all('AppntOffWorkPeoples').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntOtherPeoples', '', fm.all('AppntOtherPeoples').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BankAccNo', '', fm.all('BankAccNo').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BankCode', '', fm.all('BankCode').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpGroupNo', '', fm.all('GrpGroupNo').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('State', '', fm.all('State').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BlacklistFlag', '', fm.all('BlacklistFlag').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Currency', '', fm.all('Currency').value);
  } catch(ex) { }
  ;

  try {
    mSwitch.addVar( "ContNo", "", fm.all( 'ContNo' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "ProposalGrpContNo", "", fm.all( 'ProposalGrpContNo' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "ManageCom", "", fm.all( 'ManageCom' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "SaleChnl", "", fm.all( 'SaleChnl' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "AgentCom", "", fm.all( 'AgentCom' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "AgentCode", "", fm.all( 'AgentCode' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "AgentGroup", "", fm.all( 'AgentGroup' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "AgentCode1", "", fm.all( 'AgentCode1' ).value );
  } catch(ex) { }
  ;

  try {
    mSwitch.addVar( "RiskCode", "", fm.all( 'RiskCode' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "RiskVersion", "", fm.all( 'RiskVersion' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "CValiDate", "", fm.all( 'CValiDate' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "PolApplyDate", "", fm.all( 'PolApplyDate' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag1', '', fm.all('StandbyFlag1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag2', '', fm.all('StandbyFlag2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SaleChnl', '', fm.all('SaleChnl').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SaleChnlDetail', '', fm.all('SaleChnlDetail').value);
  } catch(ex) { }
}

/*******************************************************************************
 * 把集体信息从变量中删除 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function delGrpPolVar() {
  try {
    mSwitch.deleteVar( "intoPolFlag" );
  } catch(ex) { }
  ;
  // body信息
  try {
    mSwitch.deleteVar( "BODY" );
  } catch(ex) { }
  ;
  // 集体信息
  // 由"./AutoCreatLDGrpInit.jsp"自动生成
  try {
    mSwitch.deleteVar('GrpNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PrtNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Password');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpName');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpAddressCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpAddress');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpZipCode');
  } catch(ex) { }
  ;
  try {
		mSwitch.deleteVar('UnifiedSocialCreditNo');
		} catch(ex) { }
		;
  try {
	mSwitch.deleteVar('InsuredProperty');
  } catch(ex) { }
  ;	
  try {
	mSwitch.deleteVar('TaxpayerType');
  } catch(ex) { }
  ;	
  try {
	mSwitch.deleteVar('TaxNo');
  } catch(ex) { }
  ;	  
  try {
	mSwitch.deleteVar('CustomerBankCode');
  } catch(ex) { }
  ;	
  try {
	mSwitch.deleteVar('CustomerBankAccNo');
  } catch(ex) { }
  ;	
  try {
    mSwitch.deleteVar('BusinessType');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpNature');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Peoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('RgtMoney');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Asset');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('NetProfitRate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('MainBussiness');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Corporation');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ComAera');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LinkMan1');
  } catch(ex) { }
  ;
  try {
	mSwitch.deleteVar('DetailAddress');
	} catch(ex) { }
	;
  try {
	mSwitch.deleteVar('ProvinceID');
	} catch(ex) { }
	;
  try {
	mSwitch.deleteVar('CityID');
	} catch(ex) { }
		;
  try {
    mSwitch.deleteVar('Department1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('HeadShip1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('E_Mail1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Fax1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LinkMan2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Department2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('HeadShip2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('E_Mail2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Fax2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Fax');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Satrap');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('EMail');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('FoundDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOnWorkPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOffWorkPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOtherPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BankAccNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BankCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpGroupNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('State');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BlacklistFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Currency');
  } catch(ex) { }
  ;

  mSwitch.deleteVar( "ContNo" );
  mSwitch.deleteVar( "ProposalGrpContNo" );
  mSwitch.deleteVar( "ManageCom" );
  mSwitch.deleteVar( "SaleChnl" );
  mSwitch.deleteVar( "AgentCom" );
  mSwitch.deleteVar( "AgentCode" );
  mSwitch.deleteVar( "AgentCode1" );

  mSwitch.deleteVar( "RiskCode" );
  mSwitch.deleteVar( "RiskVersion" );
  mSwitch.deleteVar( "CValiDate" );
  mSwitch.deleteVar( "SaleChnl" );
  mSwitch.deleteVar( "SaleChnlDetail" );

}

/*******************************************************************************
 * Click事件，当双击“投保单位客户号”录入框时触发该函数 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function showAppnt1() {
  if( mOperate == 0 ) {
    mOperate = 2;
    showInfo = window.open( "../sys/GroupMain.html" );
  }
}
function showAppnt() {
  if (fm.all("GrpNo").value == "" ) {
    showAppnt1();
  } else {
    arrResult = easyExecSql("select b.CustomerNo,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum,b.OrgancomCode,b.UnifiedSocialCreditNo,b.InsuredProperty,b.TaxpayerType,b.TaxNo,b.CustomerBankCode,b.CustomerBankAccNo from LDGrp b where  b.CustomerNo='" + fm.all("GrpNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到投保单位信息");
    } else {
      displayAddress(arrResult[0]);
      getaddresscodedata();
      displayInsprop(fm.InsuredProperty.value);
    }
  }
}

/*******************************************************************************
 * 查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 参数 ： 查询返回的二维数组 返回值： 无
 * ********************************************************************
 */
function afterQuery( arrQueryResult ) {
  if( arrQueryResult != null ) {
    arrResult = arrQueryResult;
    if( mOperate == 1 )	{		// 查询集体投保单
      fm.all( 'GrpContNo' ).value = arrQueryResult[0][0];
      Resource= fm.all( 'Resource' ).value ;
      mGrpContNo=fm.all( 'GrpContNo' ).value;
      LoadFlag=fm.LoadFlag.value;
      if(LoadFlag!=16){
                 arrResult = easyExecSql("select c.*,getUniteCode(agentcode) from LCGrpCont c where GrpContNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      }else if(LoadFlag==16&Resource==1){
      var StrSQlgrpcont=" select "
+" GrpContNo,ProposalGrpContNo,PrtNo,SaleChnl,ManageCom,AgentCom,AgentType,AgentCode,AgentGroup,AgentCode1,Password,Password2,AppntNo,AddressNo,Peoples2,GrpName "
+" ,BusinessType,GrpNature,RgtMoney,Asset,NetProfitRate,MainBussiness,Corporation,ComAera,Fax,Phone,GetFlag,Satrap,EMail "
+" ,FoundDate,GrpGroupNo,BankCode,BankAccNo,AccName,DisputedFlag,OutPayFlag,GetPolMode,Lang,Currency,LostTimes,PrintCount,RegetDate "
+" ,LastEdorDate,LastGetDate,LastLoanDate,SpecFlag,GrpSpec,PayMode,SignCom,SignDate,SignTime,CValiDate,PayIntv,ManageFeeRate,ExpPeoples,ExpPremium,ExpAmnt,Peoples,Mult,Prem,Amnt,SumPrem,SumPay,Dif "
+" ,Remark,StandbyFlag1,StandbyFlag2,StandbyFlag3,InputOperator,InputDate,InputTime,ApproveFlag,ApproveCode "
+" ,ApproveDate,ApproveTime,UWOperator,UWFlag,UWDate,UWTime,AppFlag,PolApplyDate,CustomGetPolDate,GetPolDate "
+" ,GetPolTime,State,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,EnterKind,AmntGrade,Peoples3,OnWorkPeoples,OffWorkPeoples,OtherPeoples,RelaPeoples,RelaMatePeoples,RelaYoungPeoples "
+" ,RelaOtherPeoples,FirstTrialOperator,FirstTrialDate,FirstTrialTime,ReceiveOperator,ReceiveDate,ReceiveTime,TempFeeNo,HandlerName,HandlerDate,HandlerPrint,AgentDate "
+" ,BusinessBigType,MarketType,ProposalType,SaleChnlDetail,ContPrintLoFlag,PremApportFlag,ContPremFeeNo,CustomerReceiptNo,CInValiDate,RoleAgentCode,AskGrpContNo,Copys,OperationManager,InfoSource,PremScope,DegreeType,CardFlag,BigProjectFlag,ContPrintType,getUniteCode(agentcode) "
+" from lbgrpcont where proposalGrpContNo ='"+arrQueryResult[0][0]+" ' "
      ;
       arrResult = easyExecSql(StrSQlgrpcont, 1, 0);
    }else if(LoadFlag==16&Resource==2){
    var StrSQlgrpcont=" select "
+" GrpContNo,ProposalGrpContNo,PrtNo,SaleChnl,ManageCom,AgentCom,AgentType,AgentCode,AgentGroup,AgentCode1,Password,Password2,AppntNo,AddressNo,Peoples2,GrpName "
+" ,BusinessType,GrpNature,RgtMoney,Asset,NetProfitRate,MainBussiness,Corporation,ComAera,Fax,Phone,GetFlag,Satrap,EMail "
+" ,FoundDate,GrpGroupNo,BankCode,BankAccNo,AccName,DisputedFlag,OutPayFlag,GetPolMode,Lang,Currency,LostTimes,PrintCount,RegetDate "
+" ,LastEdorDate,LastGetDate,LastLoanDate,SpecFlag,GrpSpec,PayMode,SignCom,SignDate,SignTime,CValiDate,PayIntv,ManageFeeRate,ExpPeoples,ExpPremium,ExpAmnt,Peoples,Mult,Prem,Amnt,SumPrem,SumPay,Dif "
+" ,Remark,StandbyFlag1,StandbyFlag2,StandbyFlag3,InputOperator,InputDate,InputTime,ApproveFlag,ApproveCode "
+" ,ApproveDate,ApproveTime,UWOperator,UWFlag,UWDate,UWTime,AppFlag,PolApplyDate,CustomGetPolDate,GetPolDate "
+" ,GetPolTime,State,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,EnterKind,AmntGrade,Peoples3,OnWorkPeoples,OffWorkPeoples,OtherPeoples,RelaPeoples,RelaMatePeoples,RelaYoungPeoples "
+" ,RelaOtherPeoples,FirstTrialOperator,FirstTrialDate,FirstTrialTime,ReceiveOperator,ReceiveDate,ReceiveTime,TempFeeNo,HandlerName,HandlerDate,HandlerPrint,AgentDate "
+" ,BusinessBigType,MarketType,ProposalType,SaleChnlDetail,ContPrintLoFlag,PremApportFlag,ContPremFeeNo,CustomerReceiptNo,CInValiDate,RoleAgentCode,AskGrpContNo,Copys,OperationManager,InfoSource,PremScope,DegreeType,CardFlag,BigProjectFlag,ContPrintType,getUniteCode(agentcode) "
+" from lcgrpcont where proposalGrpContNo ='"+arrQueryResult[0][0]+" ' "
      ;
       arrResult = easyExecSql(StrSQlgrpcont, 1, 0);
       if(!arrResult){
 var StrSQlgrpcont= " select "
+" GrpContNo,ProposalGrpContNo,PrtNo,SaleChnl,ManageCom,AgentCom,AgentType,AgentCode,AgentGroup,AgentCode1,Password,Password2,AppntNo,AddressNo,Peoples2,GrpName "
+" ,BusinessType,GrpNature,RgtMoney,Asset,NetProfitRate,MainBussiness,Corporation,ComAera,Fax,Phone,GetFlag,Satrap,EMail "
+" ,FoundDate,GrpGroupNo,BankCode,BankAccNo,AccName,DisputedFlag,OutPayFlag,GetPolMode,Lang,Currency,LostTimes,PrintCount,RegetDate "
+" ,LastEdorDate,LastGetDate,LastLoanDate,SpecFlag,GrpSpec,PayMode,SignCom,SignDate,SignTime,CValiDate,PayIntv,ManageFeeRate,ExpPeoples,ExpPremium,ExpAmnt,Peoples,Mult,Prem,Amnt,SumPrem,SumPay,Dif "
+" ,Remark,StandbyFlag1,StandbyFlag2,StandbyFlag3,InputOperator,InputDate,InputTime,ApproveFlag,ApproveCode "
+" ,ApproveDate,ApproveTime,UWOperator,UWFlag,UWDate,UWTime,AppFlag,PolApplyDate,CustomGetPolDate,GetPolDate "
+" ,GetPolTime,State,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,EnterKind,AmntGrade,Peoples3,OnWorkPeoples,OffWorkPeoples,OtherPeoples,RelaPeoples,RelaMatePeoples,RelaYoungPeoples "
+" ,RelaOtherPeoples,FirstTrialOperator,FirstTrialDate,FirstTrialTime,ReceiveOperator,ReceiveDate,ReceiveTime,TempFeeNo,HandlerName,HandlerDate,HandlerPrint,AgentDate "
+" ,BusinessBigType,MarketType,ProposalType,SaleChnlDetail,ContPrintLoFlag,PremApportFlag,ContPremFeeNo,CustomerReceiptNo,CInValiDate,RoleAgentCode,AskGrpContNo,Copys,OperationManager,InfoSource,PremScope,DegreeType,CardFlag,BigProjectFlag,ContPrintType,getUniteCode(agentcode) "
+" from lobgrpcont where proposalGrpContNo ='"+arrQueryResult[0][0]+" ' "
				;
				arrResult = easyExecSql(StrSQlgrpcont, 1, 0);
      }
    }else if(LoadFlag==16&Resource==3){
    var StrSQlgrpcont=  " select "
+" GrpContNo,ProposalGrpContNo,PrtNo,SaleChnl,ManageCom,AgentCom,AgentType,AgentCode,AgentGroup,AgentCode1,Password,Password2,AppntNo,AddressNo,Peoples2,GrpName "
+" ,BusinessType,GrpNature,RgtMoney,Asset,NetProfitRate,MainBussiness,Corporation,ComAera,Fax,Phone,GetFlag,Satrap,EMail "
+" ,FoundDate,GrpGroupNo,BankCode,BankAccNo,AccName,DisputedFlag,OutPayFlag,GetPolMode,Lang,Currency,LostTimes,PrintCount,RegetDate "
+" ,LastEdorDate,LastGetDate,LastLoanDate,SpecFlag,GrpSpec,PayMode,SignCom,SignDate,SignTime,CValiDate,PayIntv,ManageFeeRate,ExpPeoples,ExpPremium,ExpAmnt,Peoples,Mult,Prem,Amnt,SumPrem,SumPay,Dif "
+" ,Remark,StandbyFlag1,StandbyFlag2,StandbyFlag3,InputOperator,InputDate,InputTime,ApproveFlag,ApproveCode "
+" ,ApproveDate,ApproveTime,UWOperator,UWFlag,UWDate,UWTime,AppFlag,PolApplyDate,CustomGetPolDate,GetPolDate "
+" ,GetPolTime,State,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,EnterKind,AmntGrade,Peoples3,OnWorkPeoples,OffWorkPeoples,OtherPeoples,RelaPeoples,RelaMatePeoples,RelaYoungPeoples "
+" ,RelaOtherPeoples,FirstTrialOperator,FirstTrialDate,FirstTrialTime,ReceiveOperator,ReceiveDate,ReceiveTime,TempFeeNo,HandlerName,HandlerDate,HandlerPrint,AgentDate "
+" ,BusinessBigType,MarketType,ProposalType,SaleChnlDetail,ContPrintLoFlag,PremApportFlag,ContPremFeeNo,CustomerReceiptNo,CInValiDate,RoleAgentCode,AskGrpContNo,Copys,OperationManager,InfoSource,PremScope,DegreeType,CardFlag,BigProjectFlag,ContPrintType ,getUniteCode(agentcode)"
+" from lcgrpcont where proposalGrpContNo ='"+arrQueryResult[0][0]+" ' "
;
    arrResult = easyExecSql(StrSQlgrpcont, 1, 0);
      }else{
      arrResult = easyExecSql("select c.*,getUniteCode(agentcode) from LCGrpCont c where GrpContNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      }
      if (arrResult == null) {
        // alert("未查到团单信息");
      }
      else
      {
        displayLCGrpCont(arrResult[0]);
        showOneCodeName("PayMode",fm.PayModeName.name);
        showOneCodeName("GrpContPayIntv",fm.GrpContPayIntvName.name);
        var tgrpcontno=arrResult[0][0];
        fillriskgrid();
        var tSql1=" select a.CustomerNo,a.AddressNo,a.GrpAddress,a.GrpZipCode,a.LinkMan1,a.Department1,a.HeadShip1,a.Phone1,a.E_Mail1,a.Fax1,a.LinkMan2,a.Department2,a.HeadShip2,a.Phone2,a.E_Mail2,a.Fax2,a.Mobile1,a.DetailAddress,a.postalprovince,a.postalcity,a.postalcounty,a.sex,a.nativeplace,a.occupationcode,a.occupationtype from LCGrpAddress a where a.AddressNo=(select AddressNo from LCGrpAppnt  where GrpContNo = '" + tgrpcontno + "') and a.CustomerNo=(select CustomerNo from LCGrpAppnt  where GrpContNo = '" + arrResult[0][0] + "') "
		 					     +" union "
		 					     +" select a.CustomerNo,a.AddressNo,a.GrpAddress,a.GrpZipCode,a.LinkMan1,a.Department1,a.HeadShip1,a.Phone1,a.E_Mail1,a.Fax1,a.LinkMan2,a.Department2,a.HeadShip2,a.Phone2,a.E_Mail2,a.Fax2,a.Mobile1,a.DetailAddress,a.postalprovince,a.postalcity,a.postalcounty,a.sex,a.nativeplace,a.occupationcode,a.occupationtype from LcGrpAddress a where a.AddressNo=(select AddressNo from LbGrpAppnt  where GrpContNo = '" + tgrpcontno + "') and a.CustomerNo=(select CustomerNo from LbGrpAppnt  where GrpContNo = '" + arrResult[0][0] + "') "
		 					     +" union "
		 					     +" select a.CustomerNo,a.AddressNo,a.GrpAddress,a.GrpZipCode,a.LinkMan1,a.Department1,a.HeadShip1,a.Phone1,a.E_Mail1,a.Fax1,a.LinkMan2,a.Department2,a.HeadShip2,a.Phone2,a.E_Mail2,a.Fax2,a.Mobile1,a.DetailAddress,a.postalprovince,a.postalcity,a.postalcounty,a.sex,a.nativeplace,a.occupationcode,a.occupationtype from LcGrpAddress a where a.AddressNo=(select AddressNo from LobGrpAppnt  where GrpContNo = '" + tgrpcontno + "') and a.CustomerNo=(select CustomerNo from LobGrpAppnt  where GrpContNo = '" + arrResult[0][0] + "') "
		 					     ;
        arrResult = easyExecSql(tSql1, 1, 0);
        if (arrResult == null) {
          alert("未查到投保单位地址信息");

        } else {
          displayAddress1(arrResult[0]);
        }

        // var tSql2=" select
		// b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum,b.organcomcode
		// from LDGrp b where b.CustomerNo=(select CustomerNo from LCGrpAppnt
		// where GrpContNo = '" + tgrpcontno + "') "
        // +" union "
        // +" select
		// b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum,b.organcomcode
		// from LDGrp b where b.CustomerNo=(select CustomerNo from LbGrpAppnt
		// where GrpContNo = '" + tgrpcontno + "') "
        // +" union "
        // +" select
		// b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum,b.organcomcode
		// from LDGrp b where b.CustomerNo=(select CustomerNo from LobGrpAppnt
		// where GrpContNo = '" + tgrpcontno + "') "
				// ;
        var tSql2 = "" 
            + " select b.GrpName,b.BusinessType,b.GrpNature,(case when lga.Peoples is null then b.Peoples else lga.Peoples end) Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,(case when lga.OnWorkPeoples is null then b.OnWorkPeoples else lga.OnWorkPeoples end) OnWorkPeoples,(case when lga.OffWorkPeoples is null then b.OffWorkPeoples else lga.OffWorkPeoples end) OffWorkPeoples,(case when lga.OtherPeoples is null then b.OtherPeoples else lga.OtherPeoples end) OtherPeoples,b.GrpAppntNum,b.organcomcode from LDGrp b inner join LCGrpAppnt lga on lga.CustomerNo = b.CustomerNo where lga.GrpContNo = '" + tgrpcontno + "' "
            + " union "
            + " select b.GrpName,b.BusinessType,b.GrpNature,(case when lga.Peoples is null then b.Peoples else lga.Peoples end) Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,(case when lga.OnWorkPeoples is null then b.OnWorkPeoples else lga.OnWorkPeoples end) OnWorkPeoples,(case when lga.OffWorkPeoples is null then b.OffWorkPeoples else lga.OffWorkPeoples end) OffWorkPeoples,(case when lga.OtherPeoples is null then b.OtherPeoples else lga.OtherPeoples end) OtherPeoples,b.GrpAppntNum,b.organcomcode from LDGrp b inner join LBGrpAppnt lga on lga.CustomerNo = b.CustomerNo where lga.GrpContNo = '" + tgrpcontno + "' "
            + " union "
            + " select b.GrpName,b.BusinessType,b.GrpNature,(case when lga.Peoples is null then b.Peoples else lga.Peoples end) Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,(case when lga.OnWorkPeoples is null then b.OnWorkPeoples else lga.OnWorkPeoples end) OnWorkPeoples,(case when lga.OffWorkPeoples is null then b.OffWorkPeoples else lga.OffWorkPeoples end) OffWorkPeoples,(case when lga.OtherPeoples is null then b.OtherPeoples else lga.OtherPeoples end) OtherPeoples,b.GrpAppntNum,b.organcomcode from LDGrp b inner join LOBGrpAppnt lga on lga.CustomerNo = b.CustomerNo where lga.GrpContNo = '" + tgrpcontno + "' "
            ;

				arrResult = easyExecSql(tSql2,1,0);
        if (arrResult == null) {
          alert("未查到投保单位信息");

        } else {
          displayAddress2(arrResult[0]);
        }

        var tSql3=" select c.Name,c.PostalAddress,c.ZipCode,c.Phone,c.UnifiedSocialCreditNo,c.InsuredProperty,c.TaxpayerType,c.TaxNo,c.CustomerBankCode,c.CustomerBankAccNo,c.ShareholderName,c.ShareholderIDType,c.ShareholderIDNo,c.ShareholderIDStart,c.ShareholderIDEnd,c.ShareholderIDLongFlag,c.LegalPersonName1,c.LegalPersonIDType1,c.LegalPersonIDNo1,c.LegalPersonIDStart1,c.LegalPersonIDEnd1,c.LegalPersonIDLongFlag1,c.ResponsibleName,c.ResponsibleIDType,c.ResponsibleIDNo,c.ResponsibleIDStart,c.ResponsibleIDEnd,c.ResponsibleIDLongFlag from  LCGrpAppnt c where c.GrpContNo = '" + tgrpcontno + "' "
		                    +" union "
		                    +" select c.Name,c.PostalAddress,c.ZipCode,c.Phone,c.UnifiedSocialCreditNo,c.InsuredProperty,c.TaxpayerType,c.TaxNo,c.CustomerBankCode,c.CustomerBankAccNo,c.ShareholderName,c.ShareholderIDType,c.ShareholderIDNo,c.ShareholderIDStart,c.ShareholderIDEnd,c.ShareholderIDLongFlag,c.LegalPersonName1,c.LegalPersonIDType1,c.LegalPersonIDNo1,c.LegalPersonIDStart1,c.LegalPersonIDEnd1,c.LegalPersonIDLongFlag1,c.ResponsibleName,c.ResponsibleIDType,c.ResponsibleIDNo,c.ResponsibleIDStart,c.ResponsibleIDEnd,c.ResponsibleIDLongFlag from  LbGrpAppnt c where c.GrpContNo = '" + tgrpcontno + "' "
		                    +" union "
		                    +" select c.Name,c.PostalAddress,c.ZipCode,c.Phone,c.UnifiedSocialCreditNo,c.InsuredProperty,c.TaxpayerType,c.TaxNo,c.CustomerBankCode,c.CustomerBankAccNo,c.ShareholderName,c.ShareholderIDType,c.ShareholderIDNo,c.ShareholderIDStart,c.ShareholderIDEnd,c.ShareholderIDLongFlag,c.LegalPersonName1,c.LegalPersonIDType1,c.LegalPersonIDNo1,c.LegalPersonIDStart1,c.LegalPersonIDEnd1,c.LegalPersonIDLongFlag1,c.ResponsibleName,c.ResponsibleIDType,c.ResponsibleIDNo,c.ResponsibleIDStart,c.ResponsibleIDEnd,c.ResponsibleIDLongFlag from  LobGrpAppnt c where c.GrpContNo = '" + tgrpcontno + "' "
									;
					        arrResult = easyExecSql(tSql3,1,0);
        if (arrResult != null) {
          displayAddress3(arrResult[0]);
        }
      }
    }
    if( mOperate == 2 )	{		// 投保单位信息
      arrResult = easyExecSql("select b.CustomerNo,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum,b.OrgancomCode,b.UnifiedSocialCreditNo,b.InsuredProperty,b.TaxpayerType,b.TaxNo,b.CustomerBankCode,b.CustomerBankAccNo from LDGrp b where  b.CustomerNo='" + arrQueryResult[0][0] + "'", 1, 0);
      if (arrResult == null) {
        alert("未查到投保单位信息");
      } else {
        displayAddress(arrResult[0]);
      }
    }
  }
  // alert("hehe");
  if(fm.BankCode.value!=""){
	  arrResult = easyExecSql("select bankname from ldbank where bankcode='" + fm.BankCode.value + "'", 1, 0);
      if (arrResult == null) {
        alert("查询银行信息失败！");
      } else {
    	  fm.BankCodeName.value=(arrResult[0]);
      }
  }
  
  getLCImpart();   // 团体告知
  var tStrqSq1="select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LCCustomerImpart where GrpContNo='" + arrQueryResult[0][0] + "'"
  					// +" union "
  					// +" select
					// ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from
					// LbCustomerImpart where GrpContNo='" +
					// arrQueryResult[0][0] + "'"
  						+" union "
  						+" select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LobCustomerImpart where GrpContNo='" + arrQueryResult[0][0] + "'";
    turnPage.queryModal(tStrqSq1,ImpartGrid);
    turnPage.queryModal("select InsuStartYear,InsuEndYear,InsuContent,Rate,EnsureContent,Peoples,RecompensePeoples,OccurMoney,RecompenseMoney,PendingMoney,SerialNo from LCHistoryImpart where GrpContNo='"+ arrQueryResult[0][0] + "' and (insuyear<=0 or insuyear is null)",HistoryImpartGrid);
    turnPage.queryModal("select InsuYear,InsuContent,EnsureContent,Rate,Peoples,RecompensePeoples,OccurMoney,RecompenseMoney,PendingMoney,SerialNo from LCHistoryImpart where GrpContNo='"+ arrQueryResult[0][0] + "' and insuyear>0",NewHistoryImpartGrid);
    turnPage.queryModal("select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where GrpContNo='"+ arrQueryResult[0][0] + "' and (impartver='031' or (impartver='032' and impartcode='060')) order by impartver, impartcode",BaseImpartDetailGrid);
    turnPage.queryModal("select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where GrpContNo='"+ arrQueryResult[0][0] + "' and impartver='032' and impartcode<>'060' order by impartcode",HealthImpartDetailGrid);
    
    turnPage.queryModal("select OcurTime,DiseaseName,DiseasePepoles,CureMoney,Remark,SerialNo from LCDiseaseImpart where GrpContNo='"+ arrQueryResult[0][0] + "'",DiseaseGrid);
    // 客户需求服务
    if(ImpartGrid.mulLineCount<=0)
        ImpartGrid.addOne();
    if(HistoryImpartGrid.mulLineCount<=0)
        HistoryImpartGrid.addOne();
    if(DiseaseGrid.mulLineCount<=0)
        DiseaseGrid.addOne();
    
    // 修改客户服务需求查询，按 ProposalGrpContNo 查询客户服务需求
    arrResult = easyExecSql("select ProposalGrpContNo from LCGrpCont where GrpContNo = '" + arrQueryResult[0][0] + "'");
    var ProposalGrpContNo = "";
    if(arrResult)
        ProposalGrpContNo = arrResult[0][0];
    else
        ProposalGrpContNo = arrQueryResult[0][0];
    var servInfoSql = "select distinct a.ServKind,b.ServKindRemark,a.ServDetail,c.ServDetailRemark,trim(a.servkind)||'-'||trim(a.servdetail),a.servchoose,'',d.ServChooseRemark "
        + "from LCGrpServInfo a,LDServKindInfo b,LDServDetailInfo c,LDServChooseInfo d "
        + "where a.ServKind=b.ServKind and a.ServKind=c.ServKind and a.ServDetail=c.ServDetail and a.ServKind=d.ServKind "
        + "and a.ServDetail=d.ServDetail and b.KindCode=d.KindCode and a.ServChoose=d.ServChoose and GrpContNo='" 
        + ProposalGrpContNo + "' order by a.ServKind,a.ServDetail";
    turnPage.queryModal(servInfoSql, ServInfoGrid);
    if(ServInfoGrid.mulLineCount <= 0)
        initServInfoGrid();

    mOperate = 0;		// 恢复初态
    // displayBookingPay();
    getaddresscodedata();
    displayBnf();
    displayInsprop(fm.InsuredProperty.value);
}

/*******************************************************************************
 * 把查询返回的客户地址数据返回 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function displayAddress() {
  try {
    fm.all('GrpNo').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('CustomerNo').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressNo').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value= "";
  } catch(ex) { }
  ;
  try {
	fm.all('DetailAddress').value= "";
  } catch(ex) { }
  ;
  try {
	fm.all('ProvinceID').value= "";
  } catch(ex) { }
  ;
  try {
	fm.all('CityID').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value="";
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value= "";
  } catch(ex) { }
  ;
  // 以下是ldgrp表
  try {
    fm.all('GrpNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][2]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][3]);
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOnWorkPeoples').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOffWorkPeoples').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOtherPeoples').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAppNum').value= arrResult[0][17];
  } catch(ex) { }
  ;
  try {
    fm.all('OrgancomCode').value= arrResult[0][18];
  } catch(ex) { }
  ;
  try {
	    fm.all('UnifiedSocialCreditNo').value= arrResult[0][19];
	  } catch(ex) { }
	  ;
  try {
	fm.all('InsuredProperty').value= arrResult[0][20];
  } catch(ex) { }
  ;
  try {
	fm.all('TaxpayerType').value= arrResult[0][21];
  } catch(ex) { }
  ;
  try {
	fm.all('TaxNo').value= arrResult[0][22];
  } catch(ex) { }
  ;    
  try {
	fm.all('CustomerBankCode').value= arrResult[0][23];
  } catch(ex) { }
  ; 
  try {
	fm.all('CustomerBankAccNo').value= arrResult[0][24];
  } catch(ex) { }
  ; 
}
function displayAddress1() {
  try {
    fm.all('GrpNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('CustomerNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressNo').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('Mobile1').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value= arrResult[0][17];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value= arrResult[0][18];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value= arrResult[0][19];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value= arrResult[0][20];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value= arrResult[0][21];
  } catch(ex) { }
  ;
  try {
	fm.all('DetailAddress').value= arrResult[0][22];
  } catch(ex) { }
  ;
  try {
	fm.all('ProvinceID').value= arrResult[0][23];
  } catch(ex) { }
  ;
  try {
	fm.all('CityID').value= arrResult[0][24];
  } catch(ex) { }
  ;
  try {
	fm.all('CountyID').value= arrResult[0][25];
  } catch(ex) { }
  ;
}

function displayAddress2() {
  // 以下是ldgrp表
  try {
    fm.all('GrpName').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][1]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][2]);
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOnWorkPeoples').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOffWorkPeoples').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOtherPeoples').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAppNum').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('OrgancomCode').value= arrResult[0][17];
  } catch(ex) { }
  ;
}
function displayAddress3() {
  try {
    fm.all('GrpName').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
	    fm.all('UnifiedSocialCreditNo').value= arrResult[0][4];
	  } catch(ex) { }
	  ;
  try {
	  fm.all('InsuredProperty').value= arrResult[0][5];
  } catch(ex) { }
  ;	  
  try {
	fm.all('TaxpayerType').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
	fm.all('TaxNo').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
	fm.all('CustomerBankCode').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
	fm.all('CustomerBankAccNo').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
		fm.all('ShareholderName').value= arrResult[0][10];
	  } catch(ex) { }
	  ;
try {
	fm.all('ShareholderIDType').value= arrResult[0][11];
	} catch(ex) { };
	try {
	fm.all('ShareholderIDNo').value= arrResult[0][12];
	} catch(ex) { };
	try {
	fm.all('ShareholderIDStart').value= arrResult[0][13];
	} catch(ex) { };
	try {
	fm.all('ShareholderIDEnd').value= arrResult[0][14];
	} catch(ex) { };
	try {
		if(arrResult[0][15]=="Y"){
			fm.IdNoValidity2.checked= true;
			setIDLongEffFlag2();
		}
	} catch(ex) { };
	try {
	fm.all('LegalPersonName1').value= arrResult[0][16];
	} catch(ex) { };
	try {
	fm.all('LegalPersonIDType1').value= arrResult[0][17];
	} catch(ex) { };
	try {
	fm.all('LegalPersonIDNo1').value= arrResult[0][18];
	} catch(ex) { };
	try {
	fm.all('LegalPersonIDStart1').value= arrResult[0][19];
	} catch(ex) { };
	try {
	fm.all('LegalPersonIDEnd1').value= arrResult[0][20];
	} catch(ex) { };
	try {
		if(arrResult[0][21]=="Y"){
			fm.IdNoValidity1.checked= true;
			setIDLongEffFlag1();
		}
	} catch(ex) { };
	try {
	fm.all('ResponsibleName').value= arrResult[0][22];
	} catch(ex) { };
	try {
	fm.all('ResponsibleIDType').value= arrResult[0][23];
	} catch(ex) { };
	try {
	fm.all('ResponsibleIDNo').value= arrResult[0][24];
	} catch(ex) { };
	try {
	fm.all('ResponsibleIDStart').value= arrResult[0][25];
	} catch(ex) { };
	try {
	fm.all('ResponsibleIDEnd').value= arrResult[0][26];
	} catch(ex) { };
	try {
		if(arrResult[0][27]=="Y"){
			fm.IdNoValidity3.checked= true;
			setIDLongEffFlag3();
		}
	} catch(ex) { };
}
/*******************************************************************************
 * 把查询返回的客户数据显示到投保人部分 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function displayAppnt() {
  // 由"./AutoCreatLDGrpInit.jsp"自动生成
  try {
    fm.all('GrpNo').value = arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('Password').value = arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value = arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressCode').value = arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value = arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value = arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value = arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value = arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value = arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value = arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value = arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value = arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value = arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value = arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value = arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value = arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value = arrResult[0][17];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value = arrResult[0][18];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value = arrResult[0][19];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value = arrResult[0][20];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value = arrResult[0][21];
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value = arrResult[0][22];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value = arrResult[0][23];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value = arrResult[0][24];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value = arrResult[0][25];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value = arrResult[0][26];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value = arrResult[0][27];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value = arrResult[0][28];
  } catch(ex) { }
  ;
  try {
    fm.all('GetFlag').value = arrResult[0][29];
  } catch(ex) { }
  ;
  try {
    fm.all('Satrap').value = arrResult[0][30];
  } catch(ex) { }
  ;
  try {
    fm.all('EMail').value = arrResult[0][31];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value = arrResult[0][32];
  } catch(ex) { }
  ;
  try {
    fm.all('BankAccNo').value = arrResult[0][33];
  } catch(ex) { }
  ;
  try {
    fm.all('BankCode').value = arrResult[0][34];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpGroupNo').value = arrResult[0][35];
  } catch(ex) { }
  ;
  try {
    fm.all('State').value = arrResult[0][36];
  } catch(ex) { }
  ;
  try {
    fm.all('Remark').value = arrResult[0][37];
  } catch(ex) { }
  ;
  try {
    fm.all('BlacklistFlag').value = arrResult[0][38];
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value = arrResult[0][39];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value = arrResult[0][40];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value = arrResult[0][41];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value = arrResult[0][42];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value = arrResult[0][43];
  } catch(ex) { }
  ;
  try {
    fm.all('FIELDNUM').value = arrResult[0][44];
  } catch(ex) { }
  ;
  try {
    fm.all('PK').value = arrResult[0][45];
  } catch(ex) { }
  ;
  try {
    fm.all('fDate').value = arrResult[0][46];
  } catch(ex) { }
  ;
  try {
    fm.all('mErrors').value = arrResult[0][47];
  } catch(ex) { }
  ;
  try {
	fm.all('DetailAddress').value = arrResult[0][48];
  } catch(ex) { }
  ;
  try {
	fm.all('ProvinceID').value = arrResult[0][49];
  } catch(ex) { }
  ;
  try {
	fm.all('CityID').value = arrResult[0][50];
  } catch(ex) { }
  ;
}
function displayLCGrpCont() {
  try {
    fm.all('GrpContNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('ProposalGrpContNo').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('PrtNo').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('SaleChnl').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageCom').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCom').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCom1').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentComName').value= getNameByCode("Name","LACom","AgentCom",arrResult[0][5]);
  } catch(ex) { }
  ;
  try {
    fm.all('AgentComName1').value= getNameByCode("Name","LACom","AgentCom",arrResult[0][5]);
  } catch(ex) { }
  ;
  try {
    fm.all('AgentType').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentName').value= getNameByCode("Name","LAAgent","AgentCode",arrResult[0][7]);
  } catch(ex) { }
  ;
  try {
    fm.all('AgentGroup').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode1').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Password').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Password2').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntNo').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressNo').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples2').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][16]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][17];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][17]);
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][18];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][19];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][20];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][21];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][22];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][23];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][24];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][25];
  } catch(ex) { }
  ;
  try {
    fm.all('GetFlag').value= arrResult[0][26];
  } catch(ex) { }
  ;
  try {
    fm.all('Satrap').value= arrResult[0][27];
  } catch(ex) { }
  ;
  try {
    fm.all('EMail').value= arrResult[0][28];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][29];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpGroupNo').value= arrResult[0][30];
  } catch(ex) { }
  ;
  try {
    fm.all('BankCode').value= arrResult[0][31];
  } catch(ex) { }
  ;
  try {
    fm.all('BankAccNo').value= arrResult[0][32];
  } catch(ex) { }
  ;
  try {
    fm.all('AccName').value= arrResult[0][33];
  } catch(ex) { }
  ;
  try {
    fm.all('DisputedFlag ').value= arrResult[0][34];
  } catch(ex) { }
  ;
  try {
    fm.all('OutPayFlag').value= arrResult[0][35];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolMode').value= arrResult[0][36];
  } catch(ex) { }
  ;
  try {
    fm.all('Lang').value= arrResult[0][37];
  } catch(ex) { }
  ;
  try {
    fm.all('Currency').value= arrResult[0][38];
  } catch(ex) { }
  ;
  try {
    fm.all('LostTimes').value= arrResult[0][39];
  } catch(ex) { }
  ;
  try {
    fm.all('PrintCount').value= arrResult[0][40];
  } catch(ex) { }
  ;
  try {
    fm.all('RegetDate').value= arrResult[0][41];
  } catch(ex) { }
  ;
  try {
    fm.all('LastEdorDate').value= arrResult[0][42];
  } catch(ex) { }
  ;
  try {
    fm.all('LastGetDate').value= arrResult[0][43];
  } catch(ex) { }
  ;
  try {
    fm.all('LastLoanDate').value= arrResult[0][44];
  } catch(ex) { }
  ;
  try {
    fm.all('SpecFlag').value= arrResult[0][45];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpSpec').value= arrResult[0][46];
  } catch(ex) { }
  ;
  try {
    fm.all('PayMode').value= arrResult[0][47];
  } catch(ex) { }
  ;
  try {
    fm.all('PayModeName').value= arrResult[0][47];
  } catch(ex) { }
  ;
  try {
    fm.all('SignCom').value= arrResult[0][48];
  } catch(ex) { }
  ;
  try {
    fm.all('SignDate').value= arrResult[0][49];
  } catch(ex) { }
  ;
  try {
    fm.all('SignTime').value= arrResult[0][50];
  } catch(ex) { }
  ;
  try {
    fm.all('CValiDate').value= arrResult[0][51];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpContPayIntv').value= arrResult[0][52];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpContPayIntvName').value= arrResult[0][52];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageFeeRate').value= arrResult[0][53];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPeoples').value= arrResult[0][54];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPremium').value= arrResult[0][55];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpAmnt').value= arrResult[0][56];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][57];
  } catch(ex) { }
  ;
  try {
    fm.all('Mult').value= arrResult[0][58];
  } catch(ex) { }
  ;
  try {
    fm.all('Amnt').value= arrResult[0][60];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPrem').value= arrResult[0][61];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPay').value= arrResult[0][62];
  } catch(ex) { }
  ;
  try {
    fm.all('Dif').value= arrResult[0][63];
  } catch(ex) { }
  ;
  try {
    fm.all('Remark').value= arrResult[0][64];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag1').value= arrResult[0][65];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag2').value= arrResult[0][66];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag3').value= arrResult[0][67];
  } catch(ex) { }
  ;
  try {
    fm.all('InputOperator').value= arrResult[0][68];
  } catch(ex) { }
  ;
 // alert(arrResult[0][68]);
  try {
    fm.all('InputDate').value= arrResult[0][69];
  } catch(ex) { }
  ;
  try {
    fm.all('InputTime').value= arrResult[0][70];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveFlag').value= arrResult[0][71];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveCode').value= arrResult[0][72];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveDate').value= arrResult[0][73];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveTime').value= arrResult[0][74];
  } catch(ex) { }
  ;
  try {
    fm.all('UWOperator').value= arrResult[0][75];
  } catch(ex) { }
  ;
  try {
    fm.all('UWFlag').value= arrResult[0][76];
  } catch(ex) { }
  ;
  try {
    fm.all('UWDate').value= arrResult[0][77];
  } catch(ex) { }
  ;
  try {
    fm.all('UWTime').value= arrResult[0][78];
  } catch(ex) { }
  ;
  try {
    fm.all('AppFlag').value= arrResult[0][79];
  } catch(ex) { }
  ;
  try {
    fm.all('PolApplyDate').value= arrResult[0][80];
  } catch(ex) { }
  ;
  try {
    fm.all('CustomGetPolDate').value= arrResult[0][81];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolDate').value= arrResult[0][82];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolTime').value= arrResult[0][83];
  } catch(ex) { }
  ;
  try {
    fm.all('State').value= arrResult[0][84];
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value= arrResult[0][85];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value= arrResult[0][86];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value= arrResult[0][87];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value= arrResult[0][88];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value= arrResult[0][89];
  } catch(ex) { }
  ;
  try {
    fm.all('EnterKind').value= arrResult[0][90];
  } catch(ex) { }
  ;
  try {
    fm.all('AmntGrade').value= arrResult[0][91];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples3').value= arrResult[0][92];
  } catch(ex) { }
  ;
  try {
    fm.all('OnWorkPeoples').value= arrResult[0][93];
  } catch(ex) { }
  ;
  try {
    fm.all('OffWorkPeoples').value= arrResult[0][94];
  } catch(ex) { }
  ;
  try {
    fm.all('OtherPeoples').value= arrResult[0][95];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaPeoples').value= arrResult[0][96];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaMatePeoples').value= arrResult[0][97];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaYoungPeoples').value= arrResult[0][98];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaOtherPeoples').value= arrResult[0][99];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstTrialOperator').value= arrResult[0][100];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstTrialDate').value= arrResult[0][101];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstTrialTime').value= arrResult[0][102];
  } catch(ex) { }
  ;
  try {
    fm.all('ReceiveOperator').value= arrResult[0][103];
  } catch(ex) { }
  ;
  try {
    fm.all('ReceiveDate').value= arrResult[0][104];
  } catch(ex) { }
  ;
  try {
    fm.all('ReceiveTime').value= arrResult[0][105];
  } catch(ex) { }
  ;
  try {
    fm.all('TempFeeNo').value= arrResult[0][106];
  } catch(ex) { }
  ;
  try {
    fm.all('HandlerName').value= arrResult[0][107];
  } catch(ex) { }
  ;
  try {
    fm.all('HandlerDate').value= arrResult[0][108];
  } catch(ex) { }
  ;
  try {
    fm.all('HandlerPrint').value= arrResult[0][109];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentDate').value= arrResult[0][110];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessBigType').value= arrResult[0][111];
  } catch(ex) { }
  ;
  try {
    fm.all('MarketType').value= arrResult[0][112];
  } catch(ex) { }
  ;
  try {
    fm.all('SaleChnlDetail').value= arrResult[0][114];
  } catch(ex) { }
  ;
  try {
    fm.all('CInValiDate').value= arrResult[0][119];
  } catch(ex) { }
  ;
  try {
    fm.all('AskGrpContNo').value= arrResult[0][121];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpContSumPrem').value= arrResult[0][125];
  } catch(ex) { }
  ;
  try {
    fm.all('BigProjectFlag').value= arrResult[0][128];
  } catch(ex) { }
  ;
    try {
    fm.all('ContPrintType').value= arrResult[0][129];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAgentCom').value= arrResult[0][131];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAgentCode').value= arrResult[0][132];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAgentName').value= arrResult[0][133];
  } catch(ex) { }
  ;
// try
// {
// fm.all('CTCount').value= arrResult[0][134];
// } catch(ex) {}
  try
  {
    fm.all('CoInsuranceFlag').value= arrResult[0][135];
  } catch(ex) {}
// try
// {
// fm.all('CityInfo').value= arrResult[0][136];
// } catch(ex) {}
  try
  {
    fm.all('Crs_SaleChnl').value= arrResult[0][137];
  } catch(ex) {}
  try
  {
    fm.all('Crs_BussType').value= arrResult[0][138];
  } catch(ex) {}
  try
  {
    fm.all('GrpAgentIDNo').value= arrResult[0][139];
  } catch(ex) {}
  if(fm.LoadFlag.value!=16){  // add by zjd 集团统一工号
	  try {
		    fm.all('GroupAgentCode').value= arrResult[0][142];
		  } catch(ex) { };
   }else if((fm.LoadFlag.value==16 & fm.all('Resource').value==1) || (fm.LoadFlag.value==16 & fm.all('Resource').value==2) ||(fm.LoadFlag.value==16 & fm.all('Resource').value==3) ){
	   try {
		    fm.all('GroupAgentCode').value= arrResult[0][130];
		  } catch(ex) { }; 
   }else{
	   try {
		    fm.all('GroupAgentCode').value= arrResult[0][142];
		  } catch(ex) { }; 
   }
  if(arrResult[0][131] != "" && arrResult[0][132]!=""&&arrResult[0][133]!=""&&arrResult[0][137]!=""&&arrResult[0][138]!=""&&arrResult[0][139]!="")
  {
        fm.MixComFlag.checked = true;
        if(fm.MixComFlag.checked == true)
        {
            fm.all('GrpAgentComID').style.display = "";
	        fm.all('GrpAgentTitleID').style.display = "";
	        fm.all('GrpAgentTitleIDNo').style.display = "";
        }
        var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
	    var arrResult1 = easyExecSql(strSql);
		if (arrResult1 != null) {
		     fm.GrpAgentComName.value = arrResult1[0][0];
		}
		else{  
		     fm.GrpAgentComName.value = "";
		}
  }
}
function displayLCGrpPol() {
  // 由"./AutoCreatLCGrpPolInit.jsp"自动生成
  try {
    fm.all('ContNo').value = arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('ProposalGrpContNo').value = arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('PrtNo').value = arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('KindCode').value = arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('RiskCode').value = arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('RiskVersion').value = arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('SignCom').value = arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageCom').value = arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCom').value = arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentType').value = arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('SaleChnl').value = arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('Password').value = arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNo').value = arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('Password2').value = arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value = arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressCode').value = arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value = arrResult[0][17];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value = arrResult[0][18];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value = arrResult[0][19];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][19]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][20];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][20]);
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples2').value = arrResult[0][21];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value = arrResult[0][22];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value = arrResult[0][23];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value = arrResult[0][24];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value = arrResult[0][25];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value = arrResult[0][26];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value = arrResult[0][27];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value = arrResult[0][28];
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value = arrResult[0][29];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value = arrResult[0][30];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value = arrResult[0][31];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value = arrResult[0][32];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value = arrResult[0][33];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value = arrResult[0][34];
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value = arrResult[0][35];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value = arrResult[0][36];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value = arrResult[0][37];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value = arrResult[0][38];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value = arrResult[0][39];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value = arrResult[0][40];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value = arrResult[0][41];
  } catch(ex) { }
  ;
  try {
    fm.all('GetFlag').value = arrResult[0][42];
  } catch(ex) { }
  ;
  try {
    fm.all('Satrap').value = arrResult[0][43];
  } catch(ex) { }
  ;
  try {
    fm.all('EMail').value = arrResult[0][44];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value = arrResult[0][45];
  } catch(ex) { }
  ;
  try {
    fm.all('BankAccNo').value = arrResult[0][46];
  } catch(ex) { }
  ;
  try {
    fm.all('BankCode').value = arrResult[0][47];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpGroupNo').value = arrResult[0][48];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpContPayIntv').value = arrResult[0][49];
  } catch(ex) { }
  ;
  try {
    fm.all('PayMode').value = arrResult[0][50];
  } catch(ex) { }
  ;
  try {
    fm.all('CValiDate').value = arrResult[0][51];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolDate').value = arrResult[0][52];
  } catch(ex) { }
  ;
  try {
    fm.all('SignDate').value = arrResult[0][53];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstPayDate').value = arrResult[0][54];
  } catch(ex) { }
  ;
  try {
    fm.all('PayEndDate').value = arrResult[0][55];
  } catch(ex) { }
  ;
  try {
    fm.all('PaytoDate').value = arrResult[0][56];
  } catch(ex) { }
  ;
  try {
    fm.all('RegetDate').value = arrResult[0][57];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value = arrResult[0][58];
  } catch(ex) { }
  ;
  try {
    fm.all('Mult').value = arrResult[0][59];
  } catch(ex) { }
  ;
  try {
    fm.all('Prem').value = arrResult[0][60];
  } catch(ex) { }
  ;
  try {
    fm.all('Amnt').value = arrResult[0][61];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPrem').value = arrResult[0][62];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPay').value = arrResult[0][63];
  } catch(ex) { }
  ;
  try {
    fm.all('Dif').value = arrResult[0][64];
  } catch(ex) { }
  ;
  try {
    fm.all('SSFlag').value = arrResult[0][65];
  } catch(ex) { }
  ;
  try {
    fm.all('PeakLine').value = arrResult[0][66];
  } catch(ex) { }
  ;
  try {
    fm.all('GetLimit').value = arrResult[0][67];
  } catch(ex) { }
  ;
  try {
    fm.all('GetRate').value = arrResult[0][68];
  } catch(ex) { }
  ;
  try {
    fm.all('MaxMedFee').value = arrResult[0][69];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPeoples').value = arrResult[0][70];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPremium').value = arrResult[0][71];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpAmnt').value = arrResult[0][72];
  } catch(ex) { }
  ;
  try {
    fm.all('DisputedFlag').value = arrResult[0][73];
  } catch(ex) { }
  ;
  try {
    fm.all('BonusRate').value = arrResult[0][74];
  } catch(ex) { }
  ;
  try {
    fm.all('Lang').value = arrResult[0][75];
  } catch(ex) { }
  ;
  try {
    fm.all('Currency').value = arrResult[0][76];
  } catch(ex) { }
  ;
  try {
    fm.all('State').value = arrResult[0][77];
  } catch(ex) { }
  ;
  try {
    fm.all('LostTimes').value = arrResult[0][78];
  } catch(ex) { }
  ;
  try {
    fm.all('AppFlag').value = arrResult[0][79];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveCode').value = arrResult[0][80];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveDate').value = arrResult[0][81];
  } catch(ex) { }
  ;
  try {
    fm.all('UWOperator').value = arrResult[0][82];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode').value = arrResult[0][83];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentGroup').value = arrResult[0][84];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode1').value = arrResult[0][85];
  } catch(ex) { }
  ;
  try {
    fm.all('Remark').value = arrResult[0][86];
  } catch(ex) { }
  ;
  try {
    fm.all('UWFlag').value = arrResult[0][87];
  } catch(ex) { }
  ;
  try {
    fm.all('OutPayFlag').value = arrResult[0][88];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveFlag').value = arrResult[0][89];
  } catch(ex) { }
  ;
  try {
    fm.all('EmployeeRate').value = arrResult[0][90];
  } catch(ex) { }
  ;
  try {
    fm.all('FamilyRate').value = arrResult[0][91];
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value = arrResult[0][92];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value = arrResult[0][93];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value = arrResult[0][94];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value = arrResult[0][95];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value = arrResult[0][96];
  } catch(ex) { }
  ;
  try {
    fm.all('FIELDNUM').value = arrResult[0][97];
  } catch(ex) { }
  ;
  try {
    fm.all('PK').value = arrResult[0][98];
  } catch(ex) { }
  ;
  try {
    fm.all('fDate').value = arrResult[0][99];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageFeeRate').value = arrResult[0][100];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpSpec').value = arrResult[0][101];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolMode').value = arrResult[0][102];
  } catch(ex) { }
  ;
  try {
    fm.all('PolApplyDate').value = arrResult[0][103];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag1').value = arrResult[0][105];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag2').value = arrResult[0][106];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag3').value = arrResult[0][107];
  } catch(ex) { }
  ;
}




/*******************************************************************************
 * Click事件，当点击“关联暂交费信息”图片时触发该函数 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function showFee() {
  cPolNo = fm.ProposalGrpContNo.value;
  if( cPolNo == "" ) {
    alert( "您必须先查询投保单才能进入暂交费信息部分。" );
    return false
         }

         showInfo = window.open( "./ProposalFee.jsp?PolNo=" + cPolNo + "&polType=GROUP" );
}

function queryAgent()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        return ;
    }
    
    if(fm.all('GroupAgentCode').value == "")
    {
        var tSaleChnl = (fm.SaleChnl != null && fm.SaleChnl != "undefined") ? fm.SaleChnl.value : "";
        var tAgentCom = (fm.AgentCom != null && fm.AgentCom != "undefined") ? fm.AgentCom.value : "";
        
        // 交叉渠道，个险直销人员可以销售团险产品。
        var tBranchType = 2;
        if(tSaleChnl == '06')
        {
            tBranchType = 1;
        }
        // ----------------------------------
        
        // var newWindow =
		// window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&SaleChnl=" + tSaleChnl
            + "&AgentCom=" + tAgentCom
            + "&branchtype=" + tBranchType;
        // alert("tSaleChnl : " + strURL);
        var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('GroupAgentCode').value != "")
    {
        var cAgentCode = fm.AgentCode.value;  // 保单号码
        var strSql = "select getUniteCode(AgentCode),Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode + "' and ManageCom = '" + fm.all('ManageCom').value + "'";
        var arrResult = easyExecSql(strSql);
        // alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentName.value = arrResult[0][1];
            fm.AgentGroup.value = arrResult[0][2];
            alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value = "";
            alert("代码为:["+fm.all('GroupAgentCode').value+"]的业务员不存在，请确认!");
        }
    }
}

// 查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult) {
  if(arrResult!=null) {
    fm.AgentCode.value = arrResult[0][0];
    fm.AgentName.value = arrResult[0][5];
    fm.AgentGroup.value = arrResult[0][1];
    fm.GroupAgentCode.value = arrResult[0][95];
  }
}

function queryAgent2() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
  }
  if(fm.all('GroupAgentCode').value != "" && fm.all('GroupAgentCode').value.length==8 )	 {
    var cAgentCode = fm.AgentCode.value;  // 保单号码
    var strSql = "select getUniteCode(AgentCode),Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
    // alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    } else {
      fm.AgentGroup.value="";
      alert("代码为:["+fm.all('GroupAgentCode').value+"]的业务员不存在，请确认!");
    }
  }
}

function afterCodeSelect( cCodeName, Field ) {
  if(cCodeName=="GetGrpAddressNo") {
	  var strSQL="select b.AddressNo,b.GrpAddress,b.GrpZipCode,b.LinkMan1,b.Department1,b.HeadShip1,b.Phone1,b.E_Mail1,b.Fax1,b.LinkMan2,b.Department2,b.HeadShip2,b.Phone2,b.E_Mail2,b.Fax2,b.DetailAddress,b.postalprovince,b.postalcity,b.postalcounty,Sex,NativePlace,OccupationCode,OccupationType from LCGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
	    arrResult=easyExecSql(strSQL);
    try {
      fm.all('GrpAddressNo').value= arrResult[0][0];
    } catch(ex) { }
    ;
    try {
      fm.all('GrpAddress').value= arrResult[0][1];
    } catch(ex) { }
    ;
    try {
      fm.all('GrpZipCode').value= arrResult[0][2];
    } catch(ex) { }
    ;
    try {
      fm.all('LinkMan1').value= arrResult[0][3];
    } catch(ex) { }
    ;
    try {
      fm.all('Department1').value= arrResult[0][4];
    } catch(ex) { }
    ;
    try {
      fm.all('HeadShip1').value= arrResult[0][5];
    } catch(ex) { }
    ;
    try {
      fm.all('Phone1').value= arrResult[0][6];
    } catch(ex) { }
    ;
    try {
      fm.all('E_Mail1').value= arrResult[0][7];
    } catch(ex) { }
    ;
    try {
      fm.all('Fax1').value= arrResult[0][8];
    } catch(ex) { }
    ;
    try {
      fm.all('LinkMan2').value= arrResult[0][9];
    } catch(ex) { }
    ;
    try {
      fm.all('Department2').value= arrResult[0][10];
    } catch(ex) { }
    ;
    try {
      fm.all('HeadShip2').value= arrResult[0][11];
    } catch(ex) { }
    ;
    try {
      fm.all('Phone2').value= arrResult[0][12];
    } catch(ex) { }
    ;
    try {
      fm.all('E_Mail2').value= arrResult[0][13];
    } catch(ex) { }
    ;
    try {
      fm.all('Fax2').value= arrResult[0][14];
    } catch(ex) { }
    ;
    try {
      fm.all('DetailAddress').value= arrResult[0][15];
    } catch(ex) { }
      ;
    try {
      fm.all('ProvinceID').value= arrResult[0][16];
    } catch(ex) { }
      ;
    try {
      fm.all('CityID').value= arrResult[0][17];
    } catch(ex) { }
      ;
    try {
      fm.all('CountyID').value= arrResult[0][18];
    } catch(ex) { }
      ;
      try {
    	    fm.all('AppntSex').value= arrResult[0][19];
    	   } catch(ex) { }
    	   ;
    	   try {
    		fm.all('AppntNativePlace').value= arrResult[0][20];
    	   } catch(ex) { }
    	   ;
    		try {
    			fm.all('AppntOccupationCode').value= arrResult[0][21];
    		} catch(ex) { }
    		;
    		try {
    		 fm.all('AppntOccupationType').value= arrResult[0][22];
    		} catch(ex) { }
    		;
	//市的反显
	var provinceID = fm.ProvinceID.value;
	if ( provinceID != "") {
		var sql2 = "select codename from ldcode1 where codetype='city1' and code = '" + fm.CityID.value  + "'";
		var tarrResult2 = easyExecSql(sql2);
		if (tarrResult2 != null){
			fm.City.value = tarrResult2[0][0];
		}
	}
  	//县的反显
  	var cityID = fm.CityID.value;
  	if ( cityID != "") {
  		var sql2 = "select codename from ldcode1 where codetype='county1' and code = '" + fm.CountyID.value  + "'";
  		var tarrResult2 = easyExecSql(sql2);
  		if (tarrResult2 != null){
  			fm.County.value = tarrResult2[0][0];
  		}
  	}
  }


  if(cCodeName=="PayMode") {
//	  if (fm.PayMode.value == "4") {
//		 fm.all("BankCode").style.display = "";
//		 fm.all("BankCodeName").style.display = "";
//		 fm.all("BankAccNo").style.display = "";
//		 fm.all("AccName").style.display = "";
//	}
    if(Field.value!="4") {
//      fm.all("BankCode").className = "readonly";
//      fm.all("BankCode").readOnly = true;
//      fm.all("BankCode").tabIndex = -1;
      // fm.all("BankCode").ondblclick = "";
      fm.all("BankCode").style.display = "none";
      fm.all("BankCodeName").style.display = "none";

      fm.all("BankAccNo").className = "readonly";
      fm.all("BankAccNo").readOnly = true;
      fm.all("BankAccNo").tabIndex = -1;
      // fm.all("BankAccNo").ondblclick = "";
      fm.all("BankAccNo").style.display = "none";

      fm.all("AccName").className = "readonly";
      fm.all("AccName").readOnly = true;
      fm.all("AccName").tabIndex = -1;
      fm.all("AccName").style.display = "none";

    } 
    else {
      fm.all("BankCode").style.display = "";
      fm.all("BankCodeName").style.display = "";
      fm.all("BankAccNo").style.display = "";
      fm.all("AccName").style.display = "";
      fm.all("BankCode").focus();
//      fm.all("BankCode").className = "code8";
//      fm.all("BankCode").readOnly = false;
//      fm.all("BankCode").tabIndex = 0;
      // fm.all("BankCode").ondblclick = ;
      fm.all("BankCode").style.display = "";

      fm.all("BankAccNo").className = "common";
      fm.all("BankAccNo").readOnly = false;
      fm.all("BankAccNo").tabIndex = 0;

      fm.all("AccName").className = "common";
//      fm.all("AccName").readOnly = false;
      fm.all("AccName").tabIndex = 0;
    }
  }

  if(cCodeName=="SaleChnl" || cCodeName=="unitesalechnlsgrp")
  {
	  //每次选择完销售渠道后都把交叉销售的信息清除
	  fm.MixComFlag.checked = false;
	  //清除框
	  fm.all('GrpAgentComID').style.display = "none";
      fm.all('GrpAgentTitleID').style.display = "none";
      fm.all('GrpAgentTitleIDNo').style.display = "none";
      //清除对应的值
      fm.all("Crs_SaleChnl").value = "";
      fm.all("Crs_BussType").value = "";
      fm.all("Crs_BussTypeName").value = "";
      fm.all("Crs_SaleChnlName").value = "";
      fm.all("GrpAgentCom").value = "";
      fm.all("GrpAgentIDNo").value = "";
      fm.all("GrpAgentCode").value = "";
      fm.all("GrpAgentName").value = "";
      
	  
  	if(Field.value=="03")
  	{
  		fm.all("MixComFlag").style.display = "";
  		fm.all("AgentCom").style.display = "none";
	    fm.all("AgentComName").style.display = "none";
	    fm.all("AgentCom1").style.display = "";
	    fm.all("AgentComName1").style.display = "";
	    fm.all("AgentCom1").value = "";
	    fm.all("AgentComName1").value = "";
	    fm.all("AgentCom2").style.display = "none";
	    fm.all("AgentComName2").style.display = "none";
	    fm.all("AgentCom2").value = "";
	    fm.all("AgentComName2").value = "";
	    fm.all("AgentSaleCode").style.display = "";
	    fm.all("AgentSaleName").style.display = "";
	    fm.all("AgentSaleCode").value = "";
	    fm.all("AgentSaleName").value = "";
	    var tBranchType="#2";
	    var tBranchType2="02#,#04";
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
	    fm.all("AgentSaleCodeID").style.display = "";
  	}
  	else if(Field.value=="10")
  	{
  		fm.all("AgentCom").style.display = "none";
	    fm.all("AgentComName").style.display = "none";
	    fm.all("AgentCom1").style.display = "";
	    fm.all("AgentComName1").style.display = "";
	    fm.all("AgentCom1").value = "";
	    fm.all("AgentComName1").value = "";
	    fm.all("AgentCom2").style.display = "none";
	    fm.all("AgentComName2").style.display = "none";
	    fm.all("AgentCom2").value = "";
	    fm.all("AgentComName2").value = "";
	    var tBranchType="#1";
	    var tBranchType2="02#,#04";
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
  	}
    else if(Field.value == "11" || Field.value == "12"){
	  	fm.all("AgentCom").style.display = "none";
	    fm.all("AgentComName").style.display = "none";
	    fm.all("AgentCom1").style.display = "";
	    fm.all("AgentComName1").style.display = "";
	    // fm.all("GrpAgentTitleID").style.display = "";
	    var tBranchType="2";
	    var tBranchType2="";
	    if (Field.value == "11"){
	    	tBranchType2="04";	    	
	    }else if (Field.value == "12"){
	    	tBranchType2="04";
	    }
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
	  }else if(Field.value=="15")
  	{
		fm.all("MixComFlag").style.display = "";
  		fm.all("AgentCom").style.display = "none";
	    fm.all("AgentComName").style.display = "none";
	    fm.all("AgentCom1").style.display = "";
	    fm.all("AgentComName1").style.display = "";
	    fm.all("AgentCom1").value = "";
	    fm.all("AgentComName1").value = "";
	    fm.all("AgentSaleCode").style.display = "";
	    fm.all("AgentSaleName").style.display = "";
	    fm.all("AgentSaleCode").value = "";
	    fm.all("AgentSaleName").value = "";
	    var tBranchType="#5";
	    var tBranchType2="01";
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
	    fm.all("AgentSaleCodeID").style.display = "";
    }else if(Field.value=="20")
  	{
    	fm.all("MixComFlag").style.display = "none";
  		fm.all("AgentCom").style.display = "none";
	    fm.all("AgentComName").style.display = "none";
	    fm.all("AgentCom1").style.display = "";
	    fm.all("AgentComName1").style.display = "";
	    fm.all("AgentCom1").value = "";
	    fm.all("AgentComName1").value = "";
	    fm.all("AgentSaleCode").style.display = "";
	    fm.all("AgentSaleName").style.display = "";
	    fm.all("AgentSaleCode").value = "";
	    fm.all("AgentSaleName").value = "";
	    var tBranchType="6";
	    var tBranchType2="02";
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value ;
	    fm.all("AgentSaleCodeID").style.display = "";
  	}else if(Field.value=="23")
  	{
  		fm.all("MixComFlag").style.display = "";
  		fm.all("AgentCom").style.display = "none";
	    fm.all("AgentComName").style.display = "none";
	    fm.all("AgentCom1").style.display = "";
	    fm.all("AgentComName1").style.display = "";
	    fm.all("AgentCom1").value = "";
	    fm.all("AgentComName1").value = "";
	    fm.all("AgentCom2").style.display = "none";
	    fm.all("AgentComName2").style.display = "none";
	    fm.all("AgentCom2").value = "";
	    fm.all("AgentComName2").value = "";
	    fm.all("AgentSaleCode").style.display = "";
	    fm.all("AgentSaleName").style.display = "";
	    fm.all("AgentSaleCode").value = "";
	    fm.all("AgentSaleName").value = "";
	    var tBranchType="#7";
	    var tBranchType2="01";
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
	    fm.all("AgentSaleCodeID").style.display = "";
  	}else if(Field.value=="04")
  	  {
  		fm.all("AgentCom").style.display = "none";
	    fm.all("AgentComName").style.display = "none";
	    
	    fm.all("AgentCom1").style.display = "none";
	    fm.all("AgentComName1").style.display = "none";
	    fm.all("AgentCom1").value = "";
	    fm.all("AgentComName1").value = "";
	    
	    fm.all("AgentCom2").style.display = "";
	    fm.all("AgentComName2").style.display = "";
	    fm.all("AgentCom2").value = "";
	    fm.all("AgentComName2").value = "";
	    fm.all("AgentCode").value = "";
	    fm.all("AgentName").value = "";
	    
	    fm.all("AgentSaleCode").style.display = "";
	    fm.all("AgentSaleName").style.display = "";
	    fm.all("AgentSaleCode").value = "";
	    fm.all("AgentSaleName").value = "";
	    
  	  }
	  else {
  		fm.all('AgentCom').style.display="none";
  		fm.all("AgentCom").value = "";
  		fm.all('AgentComName').style.display="none";
  		fm.all("AgentCom1").style.display = "none";
  		fm.all("AgentCom1").value = "";
	    fm.all("AgentComName1").style.display = "none";
	    fm.all("AgentCom2").style.display = "none";
	    fm.all("AgentComName2").style.display = "none";
	    fm.all("AgentCom2").value = "";
	    fm.all("AgentComName2").value = "";
	    // fm.all("GrpAgentTitleID").style.display = "none";
	  	// fm.all('AgentCom').className = "readonly";
	    // fm.all('AgentCom').readOnly = true;
	    // fm.all('AgentCom').disable = true;
	    // fm.all('AgentCom').ondblclick = "";
	    // fm.all('AgentComName').className = "readonly";
	    // fm.all('AgentComName').readOnly = true;
	    // fm.all('AgentComName').ondblclick = "";
	    fm.all("AgentSaleCode").style.display = "none";
	    fm.all("AgentSaleCode").value = "";
	    fm.all("AgentSaleName").style.display = "none";
  	} 

 
  }
  if(cCodeName=="grppayintv")
  {
  	if(Field.value == -1)
  	{
  		// 为约定缴费方式
  	// divBookingPayInty.style.display='';
  	}
  else
  	{
  		divBookingPayInty.style.display='none';
  		}
  }
  if(cCodeName=="GrpPayMode")
  {

  	if(Field.value == 2||3||4)
  	{
  		// alert();
  		fm.BankCode.style.display='';

  		fm.BankCodeName.style.display='';
  		fm.BankAccNo.style.display='';
      fm.AccName.style.display='';

    	// fm.all('BankAccNo').readOnly=true;
    	// fm.AccName.className="readonly";
    	// fm.all('AccName').readOnly=false;
  	}
  if(Field.value==1)
  	{
  		fm.BankCode.style.display='none';

  		fm.BankCodeName.style.display='none';
  		fm.BankAccNo.style.display='none';
      fm.AccName.style.display='none';
  	}
  }
  
    // 改变“是否为共保保单”选择时，控制“录入共保要素信息”按钮。
    if(cCodeName == "CoInsuranceFlag")
    {
        initCoInsuranceParamInput();
    }
    // ------------------------------
    
  //反洗钱根据投保人属性，页面显示不同的录入框
    if(cCodeName == "InsuredProperty"){
    	displayInsprop(fm.InsuredProperty.value);
    }
    
    //社保渠道（16-社保直销，18-社保综拓直销，20-社保综拓中介）不得选择交叉销售业务，其他渠道（除互动渠道）保留现有规则。
    if (fm.SaleChnl.value == "16" || fm.SaleChnl.value == "18" || fm.SaleChnl.value == "20") {
    	 fm.all("MixComFlag").style.display = "none";
    	 fm.all('GrpAgentComID').style.display = "none";
         fm.all('GrpAgentTitleID').style.display = "none";
         fm.all('GrpAgentTitleIDNo').style.display = "none";
	}else{
		fm.all("MixComFlag").style.display = "";
	}
    
}

function checkMainAppender(cRiskCode) {
  if( isSubRisk( cRiskCode ) == true ) {   // 附险
    var tPolNo = getMainRiskNo(cRiskCode);   // 弹出录入附险的窗口,得到主险保单号码
    if (!checkRiskRelation(tPolNo, cRiskCode)) {
      alert("主附险包含关系错误，输入的主险号不能带这个附加险！");
      return false;
    }
  }
  return true;
}


function isSubRisk(cRiskCode) {
  var arrQueryResult = easyExecSql("select SubRiskFlag from LMRiskApp where RiskCode='" + cRiskCode + "'", 1, 0);

  if(arrQueryResult[0] == "S")    // 需要转成大写
    return true;
  if(arrQueryResult[0] == "M")
    return false;

  if (arrQueryResult[0].toUpperCase() == "A")
    if (confirm("该险种既可以是主险,又可以是附险!选择确定进入主险录入,选择取消进入附险录入"))
      return false;
    else
      return true;

  return false;
}



function checkRiskRelation(tPolNo, cRiskCode) {
  // 集体下个人投保单
  var strSql = "select RiskCode from LCGrpPol where GrpPolNo = '" + tPolNo
               + "' and RiskCode in (select Code1 from LDCode1 where Code = '" + cRiskCode + "' and codetype='grpchkappendrisk')";
  return easyQueryVer3(strSql);
}

function getMainRiskNo(cRiskCode) {
  var urlStr = "../app/MainRiskNoInput.jsp";
  var tPolNo="";

  tPolNo = window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:310px;dialogHeight:100px;center:yes;");
  return tPolNo;
}

function grpRiskInfo() {
  // alert('a');
  // var newWindow = window.open("../app/GroupRisk.jsp");
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能进入〔险种信息〕！");
  }
  delGrpVar();
  addGrpVar();
  showInfo = window.open("../app/GroupRisk.jsp");
}

function grpInsuInfo() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能〔添加被保人〕信息！");
    return false;
  }
  // alert("1111"+fm.GrpContNo.value);
  fm.GrpContNo.value=fm.ProposalGrpContNo.value;
  delGrpVar();
  addGrpVar();
  parent.fraInterface.window.location = "../app/ContInsuredInput.jsp?LoadFlag="+LoadFlag+"&ContType=2&scantype="+ scantype+"&checktype=2"+"&ScanFlag="+ScanFlag;
}

function grpInsuList() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能进入〔被保人清单〕信息界面！");
    return false;
  }

  delGrpVar();
  addGrpVar();
  
  var tSql = "select 1 from lcgrpcont where prtno='" + fm.PrtNo.value + "' and ContPrintType = '3'";
  var arrResult = easyExecSql(tSql);
  if(arrResult){
  	window.open("../app/LGrpInsuredMain.jsp?PrtNo="+fm.PrtNo.value+"&GrpContNo="+fm.GrpContNo.value+"&Resource="+Resource+"&LoadFlag="+LoadFlag);
  } else {
  	parent.fraInterface.window.location = "../app/ContGrpInsuredInput.jsp?LoadFlag="+LoadFlag+"&scantype="+scantype+"&ScanFlag="+ScanFlag+"&oldContNo="+oldContNo+"&Resource="+Resource+"&GrpContNo="+fm.GrpContNo.value;
  }
  

}
function grpRiskPlanInfo() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能进行〔保险计划制定〕！");
    return false;
  }
  // alert(Resource);
  var newWindow = window.open("../app/ContPlan.jsp?prtNo="+fm.PrtNo.value+"&GrpContNo="+fm.GrpContNo.value+"&Resource="+Resource+"&LoadFlag="+LoadFlag);
}

function focuswrap() {
  myonfocus(showInfo);
}

function delGrpVar() {
  // 删除可能留在缓存中的个人合同信息
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ProposalContNo');
  } catch(ex) { }
  ;

  // 团体合同信息
  try {
    mSwitch.deleteVar('GrpContNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ProposalGrpContNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PrtNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SaleChnl');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ManageCom');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentCom');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentType');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentGroup');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentCode1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Password');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Password2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AddressNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Peoples2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpName');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BusinessType');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpNature');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('RgtMoney');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Asset');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('NetProfitRate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('MainBussiness');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Corporation');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ComAera');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Fax');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Satrap');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('EMail');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('FoundDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOnWorkPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOffWorkPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOtherPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpGroupNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BankCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BankAccNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AccName');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('DisputedFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('OutPayFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetPolMode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Lang');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Currency');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LostTimes');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PrintCount');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('RegetDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LastEdorDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LastGetDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LastLoanDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SpecFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpSpec');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PayMode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SignCom');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SignDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SignTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('CValiDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PayIntv');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ManageFeeRate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ExpPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ExpPremium');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ExpAmnt');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Peoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Mult');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Prem');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Amnt');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SumPrem');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SumPay');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Dif');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Remark');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('StandbyFlag1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('StandbyFlag2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('StandbyFlag3');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('InputOperator');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('InputDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('InputTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ApproveFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ApproveCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ApproveDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ApproveTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('UWOperator');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('UWFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('UWDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('UWTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PolApplyDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('CustomGetPolDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetPolDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetPolTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('State');
  } catch(ex) { }
  ;
  // 集体投保人信息
  try {
    mSwitch.deleteVar('GrpNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AddressNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntGrade');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpName');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PostalAddress');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpZipCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Password');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('State');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntType');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('RelationToInsured');
  } catch(ex) { }
  ;


}

function addGrpVar() {
  try {
    mSwitch.addVar('ContNo','','');
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ProposalContNo','','');
  } catch(ex) { }
  ;
  // 集体合同信息
  try {
    mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ProposalGrpContNo','',fm.ProposalGrpContNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PrtNo','',fm.PrtNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SaleChnl','',fm.SaleChnl.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ManageCom','',fm.ManageCom.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentCom','',fm.AgentCom.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentType','',fm.AgentType.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentCode','',fm.AgentCode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentGroup','',fm.AgentGroup.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentCode1','',fm.AgentCode1.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Password','',fm.Password.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Password2','',fm.Password2.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntNo','',fm.AppntNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Addressno','',fm.AddressNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Peoples2','',fm.Peoples2.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpName','',fm.GrpName.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BusinessType','',fm.BusinessType.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpNature','',fm.GrpNature.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('RgtMoney','',fm.RgtMoney.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Asset','',fm.Asset.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('NetProfitRate','',fm.NetProfitRate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('MainBussiness','',fm.MainBussiness.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Corporation','',fm.Corporation.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ComAera','',fm.ComAera.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Fax','',fm.Fax.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone','',fm.Phone.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetFlag','',fm.GetFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Satrap','',fm.Satrap.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('EMail','',fm.EMail.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('FoundDate','',fm.FoundDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpGroupNo','',fm.GrpGroupNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BankCode','',fm.BankCode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AccName','',fm.AccName.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('DisputedFlag','',fm.DisputedFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('OutPayFlag','',fm.OutPayFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetPolMode','',fm.GetPolMode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Lang','',fm.Lang.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Currency','',fm.Currency.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LostTimes','',fm.LostTimes.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PrintCount','',fm.PrintCount.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('RegetDate','',fm.RegetDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LastEdorDate','',fm.LastEdorDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LastGetDate','',fm.LastGetDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LastLoanDate','',fm.LastLoanDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SpecFlag','',fm.SpecFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpSpec','',fm.GrpSpec.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PayMode','',fm.PayMode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SignCom','',fm.SignCom.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SignDate','',fm.SignDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SignTime','',fm.SignTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('CValiDate','',fm.CValiDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PayIntv','',fm.GrpContPayIntv.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ManageFeeRate','',fm.ManageFeeRate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ExpPeoples','',fm.ExpPeoples.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ExpPremium','',fm.ExpPremium.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ExpAmnt','',fm.ExpAmnt.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Peoples','',fm.Peoples.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Mult','',fm.Mult.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Prem','',fm.Prem.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Amnt','',fm.Amnt.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SumPrem','',fm.SumPrem.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SumPay','',fm.SumPay.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Dif','',fm.Dif.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Remark','',fm.Remark.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag1','',fm.StandbyFlag1.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag2','',fm.StandbyFlag2.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag3','',fm.StandbyFlag3.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('InputOperator','',fm.InputOperator.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('InputDate','',fm.InputDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('InputTime','',fm.InputTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ApproveFlag','',fm.ApproveFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ApproveCode','',fm.ApproveCode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ApproveDate','',fm.ApproveDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ApproveTime','',fm.ApproveTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('UWOperator','',fm.UWOperator.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('UWFlag','',fm.UWFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('UWDate','',fm.UWDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('UWTime','',fm.UWTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppFlag','',fm.AppFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PolApplyDate','',fm.PolApplyDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('CustomGetPolDate','',fm.CustomGetPolDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetPolDate','',fm.GetPolDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetPolTime','',fm.GetPolTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('State','',fm.State.value);
  } catch(ex) { }
  ;
  // 集体投保人信息

  try {
    mSwitch.addVar('GrpNo','',fm.GrpNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PrtNo','',fm.PrtNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AddressNo','',fm.AddressNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntGrade','',fm.AppntGrade.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpName','',fm.Name.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PostalAddress','',fm.PostalAddress.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ZipCode','',fm.ZipCode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone','',fm.Phone.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Password','',fm.Password.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('State','',fm.State.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntType','',fm.AppntType.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('RelationToInsured','',fm.RelationToInsured.value);
  } catch(ex) { }
  ;

}
// 投保公司的查询
function QueryOnKeyDown() {
  var keycode = event.keyCode;
  if(keycode=="13")
  {
  	QueryOnBlur();
   }
}
function QueryOnBlur(){
	 arrResult = easyExecSql("select * from LDGrp  where GrpName like '%%" + fm.all('GrpName').value + "%%'", 1, 0);
    if (arrResult == null) {
      alert("对不起,找不到该投保单位!");
      return false;
    } else {
      arrResult = easyExecSql("select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,"
                              + " b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.CustomerNo,"
                              + " b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.UnifiedSocialCreditNo,b.InsuredProperty,b.TaxpayerType,b.TaxNo,b.CustomerBankCode,b.CustomerBankAccNo from LDGrp b where"
                              + " b.GrpName like '%%" + fm.all('GrpName').value + "%%'", 1, 0);
      if (arrResult == null) {
        alert("未查到投保单位信息");
      }
      if (arrResult.length == 1) {
      	if(!confirm("系统找到一个团体客户信息,客户名称："+arrResult[0][0]+";是否录入此客户信息")){
      		return false;
      	}
        try {
          fm.all('GrpName').value= arrResult[0][0];
        } catch(ex) { }
        ;
        try {
          fm.all('BusinessType').value= arrResult[0][1];
        } catch(ex) { }
        ;
        try {
          fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][1]);
        } catch(ex) { }
        ;
        try {
          fm.all('GrpNature').value= arrResult[0][2];
        } catch(ex) { }
        ;
        try {
          fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][2]);
        } catch(ex) { }
        ;
        try {
          fm.all('Peoples').value= arrResult[0][3];
        } catch(ex) { }
        ;
        try {
          fm.all('RgtMoney').value= arrResult[0][4];
        } catch(ex) { }
        ;
        try {
          fm.all('Asset').value= arrResult[0][5];
        } catch(ex) { }
        ;
        try {
          fm.all('NetProfitRate').value= arrResult[0][6];
        } catch(ex) { }
        ;
        try {
          fm.all('MainBussiness').value= arrResult[0][7];
        } catch(ex) { }
        ;
        try {
          fm.all('Corporation').value= arrResult[0][8];
        } catch(ex) { }
        ;
        try {
          fm.all('ComAera').value= arrResult[0][9];
        } catch(ex) { }
        ;
        try {
          fm.all('Fax').value= arrResult[0][10];
        } catch(ex) { }
        ;
        try {
          fm.all('Phone').value= arrResult[0][11];
        } catch(ex) { }
        ;
        try {
          fm.all('FoundDate').value= arrResult[0][12];
        } catch(ex) { }
        ;
        try {
          fm.all('GrpNo').value= arrResult[0][13];
        } catch(ex) { }
        ;
        try {
          fm.all('AppntOnWorkPeoples').value= arrResult[0][14];
        } catch(ex) { }
        ;
        try {
          fm.all('AppntOffWorkPeoples').value= arrResult[0][15];
        } catch(ex) { }
        ;
        try {
          fm.all('AppntOtherPeoples').value= arrResult[0][16];
        } catch(ex) { }
        ;
        try {
            fm.all('UnifiedSocialCreditNo').value= arrResult[0][17];
          } catch(ex) { }
          ;
        try {
            fm.all('InsuredProperty').value= arrResult[0][18];
            displayInsprop(arrResult[0][18]);
        } catch(ex) { }
        ;
        try {
            fm.all('TaxpayerType').value= arrResult[0][19];
        } catch(ex) { }
        ;
        try {
            fm.all('TaxNo').value= arrResult[0][20];
        } catch(ex) { }
        ;          
        try {
            fm.all('CustomerBankCode').value= arrResult[0][21];
        } catch(ex) { }
        ;      
        try {
            fm.all('CustomerBankAccNo').value= arrResult[0][22];
        } catch(ex) { }
        ;      
      	var AddressarrResult=easyExecSql("select GrpAddress,GrpZipCode,LinkMan1,Phone1,Fax1,E_Mail1,DetailAddress,postalprovince,postalcity,Sex,NativePlace,OccupationCode,OccupationType from lcgrpaddress where customerno='"+fm.all('GrpNo').value+"' and addressno in (select max(addressno) from lcgrpaddress where customerno='"+fm.all('GrpNo').value+"') ",1,0);
        try {
          fm.all('GrpAddress').value= AddressarrResult[0][0];
        } catch(ex) { }
        ;				
        try {
          fm.all('GrpZipCode').value= AddressarrResult[0][1];
        } catch(ex) { }
        ;
        try {
          fm.all('LinkMan1').value= AddressarrResult[0][2];
        } catch(ex) { }
        ;
        try {
          fm.all('Phone1').value= AddressarrResult[0][3];
        } catch(ex) { }
        ;
        try {
          fm.all('Fax1').value= AddressarrResult[0][4];
        } catch(ex) { }
        ;
        try {
          fm.all('E_Mail1').value= AddressarrResult[0][5];
        } catch(ex) { }
        ;	
        try {
          fm.all('DetailAddress').value= AddressarrResult[0][6];
        } catch(ex) { }
        ;
        try {
        	fm.all('ProvinceID').value= AddressarrResult[0][7];
        } catch(ex) { }
        ;
        try {
        	fm.all('CityID').value= AddressarrResult[0][8];
        } catch(ex) { }
        ;	
        try {
            fm.all('AppntSex').value= AddressarrResult[0][9];
        } catch(ex) { }
          ;
        try {
          fm.all('AppntNativePlace').value= AddressarrResult[0][10];
        } catch(ex) { }
        ;
        try {
            fm.all('AppntOccupationCode').value= AddressarrResult[0][11];
        } catch(ex) { }
        ;
        try {
            fm.all('AppntOccupationType').value= AddressarrResult[0][12];
        } catch(ex) { }
        ;
      }
      if (arrResult.length > 1) {
        var varSrc = "&Grpname=" + fm.all('Grpname').value;
        showinfo = window.open("./FrameMainGrpAppntQuery.jsp?Interface= GrpAppntQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
      }
    }
  }

function afterAppntQuery( arrReturn ) {

  arrResult = easyExecSql("select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,"
                          + " b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.CustomerNo,"
                          + " b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.OrgancomCode,b.UnifiedSocialCreditNo,b.InsuredProperty,b.TaxpayerType,b.TaxNo,b.CustomerBankCode,b.CustomerBankAccNo from LDGrp b where"
                          + " b.Customerno='" + arrReturn[0][0] + "'", 1, 0);

  try {
    fm.all('GrpName').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][1]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][2]);
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNo').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOnWorkPeoples').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOffWorkPeoples').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOtherPeoples').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('OrgancomCode').value= arrResult[0][17];
  } catch(ex) { }
  ;
  try {
	    fm.all('UnifiedSocialCreditNo').value= arrResult[0][18];
	  } catch(ex) { }
	  ;
  try {
	fm.all('InsuredProperty').value= arrResult[0][19];
	displayInsprop(arrResult[0][19]);
  } catch(ex) { }
  ;
  try {
	fm.all('TaxpayerType').value= arrResult[0][20];
  } catch(ex) { }
  ;
  try {
	fm.all('TaxNo').value= arrResult[0][21];
  } catch(ex) { }
  ;
  try {
	fm.all('CustomerBankCode').value= arrResult[0][22];
  } catch(ex) { }
  ;
  try {
	fm.all('CustomerBankAccNo').value= arrResult[0][23];
  } catch(ex) { }
  ;
}

// 来自险种页面的函数，未必用到
function InputPolicy() {
  var newWindow = window.open("../app/NewProposal.jsp?RiskCode=111302");
}
function InputPolicyNoList() {
  var newWindow = window.open("../app/NewProposal.jsp?NoListFlag=1&RiskCode=111302");
}

// 添加一笔险种纪录
function addRecord() {
	var tContPrintType = "";
	var tContPrintTypeSQL = "select ContPrintType,ManageCom from lcgrpcont where prtno = '"+fm.PrtNo.value+"' ";
	var tContPrintTypeArr = easyExecSql(tContPrintTypeSQL);
	if(!tContPrintTypeArr){
		alert("获取保单数据失败！");
		return false;
	}else{
		tContPrintType = tContPrintTypeArr[0][0];
	}
	
	if(tContPrintType == "3"){
		var lgrpSql = "select 1 from ldriskmanage where riskcode='" + fm.RiskCode.value 
				+ "' and checktype='lgrprisk' and state='0' " 
				+ " and startdate <= current date and (enddate is null or enddate > current date) ";
		var lgrpArr = easyExecSql(lgrpSql);
		if(!lgrpArr){
			alert("大团单保单不支持该险种，请修改保单属性或险种代码");
			return false;
		}
	}
	
	if(tContPrintType == "4"){
		var grpBriefSql = "select 1 from ldcode where codetype = 'grpbriefrisk' and code ='" + fm.RiskCode.value +"' and codename = '"+tContPrintTypeArr[0][1].substring(0,4)+"' ";
		var grpBriefArr = easyExecSql(grpBriefSql);
		if(!grpBriefArr){
			alert("简易团单保单不支持该险种，请修改保单属性或险种代码");
			return false;
		}
	}
	if(tContPrintType == "5"){
		var grpBriefSql = "select code,codename,'Risk' from ldcode where codetype = 'grphjrisk' and code ='" + fm.RiskCode.value +"' ";
		var grpBriefArr = easyExecSql(grpBriefSql);
		if(!grpBriefArr){
			alert("汇交件不支持该险种，请修改保单属性或险种代码");
			return false;
		}
		//校验学平险
		var xpxSql="select 1 from lcgrppol lcg where lcg.prtno = '" + fm.PrtNo.value +"' and riskcode = '520702'";
		var xpxSqlResult = easyExecSql(xpxSql);
		if(checkRiskCodeXPX()){
			if(xpxSqlResult!=1){
				alert("在添加" + fm.RiskCode.value +"险种时，请先添加520702险种！");
				return false;
			}
		}
	}
	if(tContPrintType!="5"){
		var grpBriefSql = "select code,codename,'Risk',comcode from ldcode where codetype = 'grphjrisk' and code ='" + fm.RiskCode.value +"' ";
		var grpBriefArr = easyExecSql(grpBriefSql);
		if(grpBriefArr){
			if(grpBriefArr[0][3] != "3"){
				alert("非汇交件不支持该险种，请修改保单属性或险种代码");
				return false;
			}
		}
	}
	
	// 校验是否允许录入此险种 2017-01-13 zqt
	var tSQL1 = "select 1 from ldcode1 where codetype='limitrisk' and codename ='"+fm.SaleChnl.value+"' and code1 ='"+fm.RiskCode.value+"' ";
	var tSQL2 = "select 1 from ldcode1 where codetype='limitrisk1' and code1 ='"
			  + fm.ManageCom.value + "' and codename ='"+fm.RiskCode.value+"' "
			  + " and codealias = '" + fm.SaleChnl.value +"'";
	var arr = easyExecSql(tSQL1);
	var arr1 = easyExecSql(tSQL2);
	
	if(arr&&!arr1){
		alert("当前险种"+fm.RiskCode.value+"在销售渠道"+fm.SaleChnlName.value+"下禁止录入，请确认！");
		return false;
	}
	
	// 首先区分是套餐险种还是普通险种
	var strSql = "select 1 from LMRiskApp where RiskCode='"+fm.RiskCode.value+"'"
							+" union "
							+"select 2 from LDRiskWrap where RiskWrapCode='"+fm.RiskCode.value+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		if(arr[0][0] == "1"){
			fm.RiskType.value = "Risk";
		}else if(arr[0][0] == "2"){
			fm.RiskType.value = "Wrap";
		}
	}else{
		alert("当前险种代码或套餐代码系统中不存在，请确认是否录入正确！");
		return false;
	}
	
	// 370301险种 必须 单独承保
	var strSQL = "select 1 from lcgrppol where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(strSQL);
	if(arr){
		if(fm.RiskCode.value == '370301'){
			alert("单独承保的险种不能添加到已存在险种的保单里！");
			return; 
		}
		if(RiskGrid.getRowColData(0,1) == '370301'){
			alert("该保单存在单独承保的险种，不能继续添加险种！");
			return;
		}
	}// End 370301 险种 必须 单独承保
	// 370301险种 算费方式 为 表定费率
	if(fm.RiskCode.value == '370301' && fm.CalRule.value != '3'){
		alert("该险种的算费方式应为'约定费率'，请核实！");
		return;
	}// End 370301险种 算费方式 为 表定费率
	// 370301险种 缴费方式 为 趸缴
	if(fm.RiskCode.value == '370301' && fm.GrpContPayIntv.value != '0'){
		alert("该险种的缴费方式应为'趸缴'，请核实！");
		return;
	}// End 370301险种 缴费方式 为 趸缴

	// 162501和162401险种 必须 单独承保
	var strSQL = "select 1 from lcgrppol where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(strSQL);
	if(arr){
		if(fm.RiskCode.value == '162501'||fm.RiskCode.value == '162401'){
			alert("单独承保的险种不能添加到已存在险种的保单里！");
			return; 
		}
		if(RiskGrid.getRowColData(0,1) == '162501'||RiskGrid.getRowColData(0,1) == '162401'){
			alert("该保单存在单独承保的险种，不能继续添加险种！");
			return;
		}
	}// End 162501和162401 险种 必须 单独承保
	// 162501和162401险种 算费方式 为 表定费率和约定差异费率
	if((fm.RiskCode.value == '162501' ||fm.RiskCode.value == '162401')&& fm.CalRule.value != '0'&&fm.CalRule.value != '4'){
		alert("该险种的算费方式应为'表定费率或约定差异费率'，请核实！");
		return;
	}// End 162501和162401险种 算费方式 为 表定费率
	// 162501和162401险种 缴费方式 为 趸缴
	if((fm.RiskCode.value == '162501' ||fm.RiskCode.value == '162401')&& fm.GrpContPayIntv.value != '0'){
		alert("该险种的缴费方式应为'趸缴'，请核实！");
		return;
	}// End 162501和162401险种 缴费方式 为 趸缴
	
	// 170501险种 必须 单独承保
	var strSQL = "select 1 from lcgrppol where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(strSQL);
	if(arr){
		if(fm.RiskCode.value == '170501'){
			alert("单独承保的险种不能添加到已存在险种的保单里！");
			return; 
		}
		if(RiskGrid.getRowColData(0,1) == '170501'){
			alert("该保单存在单独承保的险种，不能继续添加险种！");
			return;
		}
	}// End 170501 险种 必须 单独承保
	// 170501险种 算费方式 为 表定费率
	if(fm.RiskCode.value == '170501' && fm.CalRule.value != '0'){
		alert("该险种的算费方式应为'表定费率'，请核实！");
		return;
	}// End 170501险种 算费方式 为 表定费率
	// 170501险种 缴费方式 为 趸缴
	if(fm.RiskCode.value == '170501' && fm.GrpContPayIntv.value != '0'){
		alert("该险种的缴费方式应为'趸缴'，请核实！");
		return;
	}// End 170501险种 缴费方式 为 趸缴
	if(fm.RiskCode.value == '122502' && fm.CalRule.value != '3'){
		alert("该险种的算费方式应为'约定费率'，请核实！");
		return;
	}// End 122502险种 算费方式 为 约定费率
	
	//健享海外险种 必须 单独承保
	var strSQL = "select 1 from lcgrppol where prtno = '"+fm.PrtNo.value+"' ";
	var arr = easyExecSql(strSQL);
	if(arr){
		if(fm.RiskCode.value == '162601'||fm.RiskCode.value == '162701'
			||fm.RiskCode.value == '162801'||fm.RiskCode.value == '162901'){
			alert("单独承保的险种不能添加到已存在险种的保单里！");
			return; 
		}
		if(RiskGrid.getRowColData(0,1) == '162601'||RiskGrid.getRowColData(0,1) == '162701'
			||RiskGrid.getRowColData(0,1) == '162801'||RiskGrid.getRowColData(0,1) == '162901'){
			alert("该保单存在单独承保的险种，不能继续添加险种！");
			return;
		}
	}//End 健享海外 险种 必须 单独承保
	//健享海外险种 算费方式 为 表定费率
	if((fm.RiskCode.value == '162601'||fm.RiskCode.value == '162701'
		||fm.RiskCode.value == '162801'||fm.RiskCode.value == '162901')&& fm.CalRule.value != '3'){
		alert("该险种的算费方式应为'约定费率'，请核实！");
		return;
	}//End 健享海外险种 算费方式 为 表定费率
	// 健享海外险种 缴费方式 为 趸缴
	if((fm.RiskCode.value == '162601'||fm.RiskCode.value == '162701'
		||fm.RiskCode.value == '162801'||fm.RiskCode.value == '162901') && fm.GrpContPayIntv.value != '0'){
		alert("该险种的缴费方式应为'趸缴'，请核实！");
		return;
	}// End 健享海外险种 缴费方式 为 趸缴
	//162701及162901生效日最高追溯30天
	if(fm.RiskCode.value == '162701'||fm.RiskCode.value == '162901'){
		var tSql = "select cvalidate from lcgrpcont where PrtNo='" + fm.PrtNo.value + "'";
		var tarr = easyExecSql(tSql);
		if(!tarr){
			alert("获取保单数据失败！");
			return false;
		}
		var ttSql = "select (current date - 30 days) from dual ";
		var ttarr = easyExecSql(ttSql);
		if(tarr[0][0]<ttarr[0][0]){
			alert("对于险种"+fm.RiskCode.value+"生效日不得早于录单日期30天以上！");
			return false;
		}
	}
	
	//一带一路产品163001
	if((fm.RiskCode.value == '163001')&& fm.CalRule.value != '3'){
		alert("该险种的算费方式应为'约定费率'，请核实！");
		return false;
	}//End 健享海外险种 算费方式 为 表定费率
	// 健享海外险种 缴费方式 为 趸缴
	if((fm.RiskCode.value == '163001') && fm.GrpContPayIntv.value != '0'){
		alert("该险种的缴费方式应为'趸缴'，请核实！");
		return false;
	}
	//一带一路产品163001
	if((fm.RiskCode.value == '561301')&& fm.CalRule.value != '3'){
		alert("该险种的算费方式应为'约定费率'，请核实！");
		return false;
	}//End 健享海外险种 算费方式 为 表定费率
	// 健享海外险种 缴费方式 为 趸缴
	if((fm.RiskCode.value == '561301') && fm.GrpContPayIntv.value != '0'){
		alert("该险种的缴费方式应为'趸缴'，请核实！");
		return false;
	}
	//163001最高
	if(fm.RiskCode.value == '163001'){
		var tSql = "select cvalidate,cinvalidate from lcgrpcont where PrtNo='" + fm.PrtNo.value + "'";
		var tarr = easyExecSql(tSql);
		if(!tarr){
			alert("获取保单数据失败！");
			return false;
		}
		var ttSql = dateDiff(tarr[0][0],tarr[0][1],"D");
		if(ttSql>364){
			alert("对于险种"+fm.RiskCode.value+"保险期间不能超过1年！");
			return false;
		}
	}	
	var strSQL = "select 1 from lcgrppol where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(strSQL);
	if(arr){
		// 163001险种 只能与 561301承保
		if((RiskGrid.getRowColData(0,1) == '163001'&& fm.RiskCode.value != '561301')
				||(RiskGrid.getRowColData(0,1) == '561301'&& fm.RiskCode.value != '163001')
				||(RiskGrid.getRowColData(0,1) != '561301'&& fm.RiskCode.value == '163001')){
			alert("163001-《一带一路境外员工团体意外医疗保险》、561301《附加一带一路境外员工团体意外伤害保险》险种不能与其他险种一起承保!");
			return; 
		}
	}
	
	//一带一路产品163002
	if((fm.RiskCode.value == '163002')&& fm.CalRule.value != '3'){
		alert("该险种的算费方式应为'约定费率'，请核实！");
		return false;
	}//End 健享海外险种 算费方式 为 表定费率
	// 健享海外险种 缴费方式 为 趸缴
	if((fm.RiskCode.value == '163002') && fm.GrpContPayIntv.value != '0'){
		alert("该险种的缴费方式应为'趸缴'，请核实！");
		return false;
	}
	//一带一路产品163002的附加险
	if((fm.RiskCode.value == '561302')&& fm.CalRule.value != '3'){
		alert("该险种的算费方式应为'约定费率'，请核实！");
		return false;
	}//End 健享海外险种 算费方式 为 表定费率
	// 健享海外险种 缴费方式 为 趸缴
	if((fm.RiskCode.value == '561302') && fm.GrpContPayIntv.value != '0'){
		alert("该险种的缴费方式应为'趸缴'，请核实！");
		return false;
	}
	//163002最高
	if(fm.RiskCode.value == '163002'){
		var tSql = "select cvalidate,cinvalidate from lcgrpcont where PrtNo='" + fm.PrtNo.value + "'";
		var tarr = easyExecSql(tSql);
		if(!tarr){
			alert("获取保单数据失败！");
			return false;
		}
		var ttSql = dateDiff(tarr[0][0],tarr[0][1],"D");
		if(ttSql>364){
			alert("对于险种"+fm.RiskCode.value+"保险期间不能超过1年！");
			return false;
		}
	}	
	var strSQL = "select 1 from lcgrppol where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(strSQL);
	if(arr){
		// 163002险种 只能与 561302承保
		if((RiskGrid.getRowColData(0,1) == '163002'&& fm.RiskCode.value != '561302')
				||(RiskGrid.getRowColData(0,1) == '561302'&& fm.RiskCode.value != '163002')
				||(RiskGrid.getRowColData(0,1) != '561302'&& fm.RiskCode.value == '163002')){
			alert("163002-《一带一路境外员工团体意外医疗保险》、561302《附加一带一路境外员工团体意外伤害保险》险种不能与其他险种一起承保!");
			return; 
		}
	}
	
	//270102险种 算费方式 为 表定费率
	if(fm.RiskCode.value == '270102' && fm.CalRule.value != '0'){
		alert("该险种的算费方式应为'表定费率'，请核实！");
		return;
	}
	//270102险种 必须 单独承保
	var strSQL = "select 1 from lcgrppol where prtno = '"+fm.PrtNo.value+"' ";
	var arr = easyExecSql(strSQL);
	if(arr){
		if(fm.RiskCode.value == '270102'){
			alert("270102-《关爱专家特定疾病团体疾病保险》险种不能与其他险种一起承保!");
			return; 
		}
		if(RiskGrid.getRowColData(0,1) == '270102'){
			alert("270102-《关爱专家特定疾病团体疾病保险》险种不能与其他险种一起承保!");
			return;
		}
	}
	
	if(!verifyInput2())
	return false;
  if(!ChkRiskVer()) return false;
  var tRiskCode = fm.all('RiskCode').value ;
  var tCalRule = fm.all('CalRule').value ;
  var ExpPeoples = fm.all('ExpPeoples').value ;
  var tGrpContNo = fm.all('GrpContNo').value ;
  fm.all( 'LoadFlag' ).value = LoadFlag ;

  if(tGrpContNo==null ||tGrpContNo=="") {
    alert("团单合同信息未保存，不容许〔添加险种〕！");
    return ;
  }

  // if(tRiskCode==null ||tRiskCode==""|| tCalRule==null|| tCalRule=="") {
  // alert("险种编码或保费计算方式录入不完整！");
  // return ;
  // }

  RiskGrid.delBlankLine();
  
  fm.all('fmAction').value="INSERT||GROUPRISK";

  var showStr="正在添加团单险种信息，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	if(fm.RiskType.value == "Risk"){
		fm.action="../app/GroupRiskSave.jsp";
	}else if(fm.RiskType.value == "Wrap"){
		fm.action="../app/GroupWrapSave.jsp";
	}
  fm.submit();
}

// 删除一笔险种纪录
function deleteRecord() {
  // 此方法得到的行数需要-1处理
	  var checkAcount = 0;
	  var WrapFlag = false;
	  var RiskFlag = false;
	  for (i=0; i<RiskGrid.mulLineCount; i++) {
	    if (RiskGrid.getChkNo(i)) {
	    	var RiskCode = RiskGrid.getRowColData(i,1);

	    	var strSql = "select 1 from LMRiskApp where RiskCode='"+RiskCode+"'"
										+" union "
										+"select 2 from LDRiskWrap where RiskWrapCode='"+RiskCode+"'";
				var arr = easyExecSql(strSql);
				if(arr){
					if(arr[0][0] == "1"){
						fm.RiskType.value = "Risk";
						RiskFlag = true;
					}else if(arr[0][0] == "2"){
						fm.RiskType.value = "Wrap";
						WrapFlag = true;
					}
				}
	    	checkAcount++;
	    }
	  }
	  if (checkAcount < 1) {
	  	alert("请至少选择一条险种信息！");
	  	return;
	  }
	  if(RiskFlag && WrapFlag){
	  	alert("险种和套餐不能同时删除，请先删除险种");
	  	return;
	  }
	  /*if(!delete520702()){
		  return false;
	  }*/
  if( confirm("是否要删除此险种信息？删除请点击“确定”，否则点“取消”。"))
  {
  RiskGrid.delBlankLine();
  var showStr="正在删除险种信息，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.all('fmAction').value="DELETE||GROUPRISK";
  if(fm.RiskType.value == "Risk"){
		fm.action="../app/GroupRiskSave.jsp";
	}else if(fm.RiskType.value == "Wrap"){
		fm.action="../app/GroupWrapSave.jsp";
	}
   fm.submit();
    }
 else
  	{
	return false;
	  }
}


/*******************************************************************************
 * 团单险种信息的的录入 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function grpFeeInput() {
  var tGrpContNo = fm.all('GrpContNo').value ;
  if(tGrpContNo==null ||tGrpContNo=="") {
    alert("团单合同信息未保存，不容许〔添加险种〕！");
    return ;
  }
  parent.fraInterface.window.location = "../app/GrpFeeInput.jsp?ProposalGrpContNo="+tGrpContNo+"&LoadFlag="+LoadFlag;
}

/*******************************************************************************
 * 初始化险种显示，包括 人数和保费的统计 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function fillriskgrid() {
  if(fm.ProposalGrpContNo.value!="") {
// var Resource= fm.all( 'Resource' ).value ;
      mGrpContNo=fm.all( 'GrpContNo' ).value;
// var LoadFlag=fm.LoadFlag.value;
      if(LoadFlag==16 && Resource==1){
    var strSql = " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
								+" code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+fm.all('GrpContNo').value+"' "
								+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
								+" (select sum(Peoples2) from lbcontplan where grpcontno='"+fm.GrpContNo.value+"' "
								+" and ContPlancode in (select distinct contplancode from lbcontplanrisk where "
								+" grpcontno='"+fm.GrpContNo.value+"' and riskcode=a.riskcode)), "
								+" a.peoples2, "
								+" to_zero((select sum(to_zero(prem)) From lbpol Where grppolno=a.Grppolno )) "
								+" From lbgrppol a,LMRiskApp b "
								+" Where  a.riskcode=b.riskcode  and  a.grpcontno='" + fm.all('GrpContNo').value +"' and a.riskCode not in (select riskcode from LCRiskWrap where grpcontno=a.grpcontno) "
								+" union "
                +" select a,b,c,d,e, "                                                                                                         
                +" to_zero(f) from (select a.RiskWrapCode a,b.RiskWrapName b,(select codename from ldcode where codetype='calrule' and "       
                +"  code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+fm.GrpContNo.value+"' "                                      
                +"  and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')) c, "                                               
                +" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"'  and ContPlancode "                                      
                +" in (select distinct contplancode from lccontplanrisk where  grpcontno='"+fm.GrpContNo.value+"' "                                        
                +" and riskcode=a.riskcode))  d,a.peoples2 e, "                                                                                
                +" (select sum(to_zero(prem)) From lbpol Where grppolno=a.Grppolno ) f from LCRiskWrap a,LDRiskWrap "                          
                +"  b  where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode "                                                       
                +" and a.Grpcontno='"+fm.GrpContNo.value+"' ) as x group by a,b,c,d,e,f  fetch first 3000 rows only  "  
                
}else if(LoadFlag==16&Resource==2){

  var strSql =  " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
+" code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+fm.all('GrpContNo').value+"' "
+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
+" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"' "
+" and ContPlancode in (select distinct contplancode from lccontplanrisk where "
+" grpcontno='"+fm.GrpContNo.value+"' and riskcode=a.riskcode)), "
+" a.peoples2, "
+" to_zero((select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno )) "
+" From lcgrppol a,LMRiskApp b "
+" Where  a.riskcode=b.riskcode   and  a.grpcontno='" + fm.all('GrpContNo').value +"' and a.riskCode not in (select riskcode from LCRiskWrap where grpcontno=a.grpcontno)"
+" union "
+" select a,b,c,d,e,to_zero(sum(f)) from ("
+" select a.RiskWrapCode a,b.RiskWrapName b,'' c,0 d,a.peoples2 e,"
+" (select to_zero(prem) From lcpol Where grppolno=a.Grppolno ) f"
+" from LCRiskWrap a,LDRiskWrap b "
+" where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode and a.Grpcontno='"+mGrpContNo+"'"
+" ) as x group by a,b,c,d,e "
+" union "
+" Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
+" code in(select CalFactorValue from LobContPlanDutyParam where grpcontno='"+fm.all('GrpContNo').value+"' "
+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
+" (select sum(Peoples2) from lobcontplan where grpcontno='"+fm.GrpContNo.value+"' "
+" and ContPlancode in (select distinct contplancode from lobcontplanrisk where "
+" GrpContNo='"+fm.GrpContNo.value+"' and riskcode=a.riskcode)), "
+" a.peoples2, "
+" to_zero((select sum(to_zero(prem)) From lobpol Where grppolno=a.Grppolno )) "
+" From lobgrppol a,LMRiskApp b "
+" Where  a.riskcode=b.riskcode   and a.GrpContNo='" + fm.all('GrpContNo').value +"' and a.riskCode not in (select riskcode from LCRiskWrap where grpcontno=a.grpcontno)"
+" union "
+" select a,b,c,d,e,f from ("
+" select a.RiskWrapCode a,b.RiskWrapName b,'' c,0 d,a.peoples2 e,"
+" (select sum(to_zero(prem)) From lobpol Where grppolno=a.Grppolno ) f"
+" from LCRiskWrap a,LDRiskWrap b "
+" where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode and a.Grpcontno='"+mGrpContNo+"'"
+" ) as x group by a,b,c,d,e,f ";

}else if(LoadFlag==16&Resource==3){
  var strSql =  " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
+" code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+fm.all('GrpContNo').value+"' "
+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
+" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"' "
+" and ContPlancode in (select distinct contplancode from lccontplanrisk where "
+" grpcontno='"+fm.GrpContNo.value+"' and riskcode=a.riskcode)), "
+" a.peoples2, "
+" (select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno ) "
+" From lcgrppol a,LMRiskApp b "
+" Where  a.riskcode=b.riskcode   and   a.grpcontno='" + fm.all('GrpContNo').value +"' and a.riskCode not in (select riskcode from LCRiskWrap where grpcontno=a.grpcontno)"
+" union "
+" select a,b,c,d,e, "                                                                                                         
+" to_zero(f) from (select a.RiskWrapCode a,b.RiskWrapName b,(select codename from ldcode where codetype='calrule' and "       
+"  code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+fm.GrpContNo.value+"' "                                      
+"  and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')) c, "                                               
+" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"'  and ContPlancode "                                      
+" in (select distinct contplancode from lccontplanrisk where  grpcontno='"+fm.GrpContNo.value+"' "                                        
+" and riskcode=a.riskcode))  d,a.peoples2 e, "                                                                                
+" (select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno ) f from LCRiskWrap a,LDRiskWrap "                          
+"  b  where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode "                                                       
+" and a.Grpcontno='"+fm.GrpContNo.value+"' ) as x group by a,b,c,d,e,f  fetch first 3000 rows only  "  

}else{
  var strSql =  " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
+" code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+fm.all('GrpContNo').value+"' "
+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
+" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"' "
+" and ContPlancode in (select distinct contplancode from lccontplanrisk where "
+" grpcontno='"+fm.GrpContNo.value+"' and riskcode=a.riskcode)), "
+" a.peoples2, "
+" to_zero((select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno )) "
+" From lcgrppol a,LMRiskApp b "
+" Where b.riskcode not in (select Riskcode from LCRiskWrap where Grpcontno=a.GrpContNo) and  a.riskcode=b.riskcode   and  a.grpcontno='" + fm.all('GrpContNo').value +"'  "
+" union "
+" select a,b,c,d,e, "                                                                                                         
+" to_zero(f) from (select a.RiskWrapCode a,b.RiskWrapName b,(select codename from ldcode where codetype='calrule' and "       
+"  code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+fm.GrpContNo.value+"' "                                      
+"  and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')) c, "                                               
+" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"'  and ContPlancode "                                      
+" in (select distinct contplancode from lccontplanrisk where  grpcontno='"+fm.GrpContNo.value+"' "                                        
+" and riskcode=a.riskcode))  d,a.peoples2 e, "                                                                                
+" (select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno ) f from LCRiskWrap a,LDRiskWrap "                          
+"  b  where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode "                                                       
+" and a.Grpcontno='"+fm.GrpContNo.value+"' ) as x group by a,b,c,d,e,f  fetch first 3000 rows only  "  

}
                 
    turnPage.queryModal(strSql,RiskGrid);
  } else {
    return false;
  }
}

function getapproveinfo() {
// alert("hehe2");
  mOperate=1;
  var approveinfo=new Array();
  approveinfo[0]=new Array();
  approveinfo[0][0]=polNo;
  afterQuery(approveinfo);
}
/*******************************************************************************
 * 选中团单问题件的录入 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function GrpQuestInput() {
  var cGrpProposalContNo = fm.GrpContNo.value;  // 团体保单号码
  if(cGrpProposalContNo==""||cGrpProposalContNo==null) {
    alert("请先选择一个团体投保单!");
    return ;
  }
  if(LoadFlag==2)
  var loadflag=4;
  else
  var	loadflag=3;
  window.open("../uw/GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+LoadFlag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&loadflag="+loadflag);


}
/*******************************************************************************
 * 选中团单问题件的查询 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function GrpQuestQuery() {
  var cGrpContNo = fm.GrpContNo.value;  // 团单投保单号码
  if(cGrpContNo==""||cGrpContNo==null) {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  window.open("../uw/GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpContNo+"&ProposalNo="+cGrpContNo+"&Flag="+LoadFlag);
}
/*******************************************************************************
 * 复核通过该团单 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function gmanuchk() {

	if(!checkSaleChnlInfo()){
    	return false;
    }
    
    if(!checkMarkettypeSalechnl()){
    	return false;
    }
    
    if(!checkRiskSalechnl()){
    	return false;
    }
    // 销售渠道和业务员的校验
	if(!checkSaleChnl())
	{
		return false;
	}
	
	// 中介机构的校验
	if(!checkAgentCom())
	{
		return false;
	}
	
    // 开办市县数和开办市县内容校验
// if(!checkCity(2))
// {
// return false;
// }
    if(!checkCoInsuranceParams())
    {
        return false;
    }
    if(!ChkPayIntv())
    {
        return false;
    }
    // 校验被保人职业类别与职业代码
    if(!checkOccTypeOccCode())
    {
    	return false;
    }
    // 校验被保人性别
    if(!CheckSex())
    {
    	return false;
    }
    
    // 校验集团交叉销售相互代理业务的销售渠道与中介机构是否符合描述
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
   
    // by gzh 20110413 若存在没有算费的被保人，则阻断。
    if(!checkAllCalPrem()){
    	return false;
    }
    // 非阻断校验，校验缴费频次与保险区间是否匹配
    if(!checkPayIntvMatchInsuYear())
    {
    	return false;
    }
    
    // 缴费频次payintv=-1时，市场类型必须是 2,4,5,6,8,10,11
    if(!checkPayIntv()){
    	return false;
    }
    
    if(!checkFirstPayPlan()){
    	return false;
    }
    
    // 校验代理机构及代理机构业务员
    if(!checkAgentComAndSaleCdoe()){
    	return false;
    }
    
    // 校验建工险
    if(!check590301()){
    	return false;
    }
    
    // 校验健享全球表定
    if(!checkB162001()){
    	return false;
    }
    
    // 反洗钱
    if(!checkFXQ()){
    	return false;
    }
    
    // 联系人联系电话对应不同投保人
    if(!checkPhone1()){
    	return false;
    }
    
    if(!checkLGrp()){
    	return false;
    }
    
    // 校验简易团单的缴费方式
    if(!checkBriefGrp()){
    	return false;
    }
    
    // 校验简易团单是否与险种匹配
    if(!checkBriefGrpRisk()){
    	return false;
    }
    
    // 商团大项目必录校验
    if(!checkBigProject()){
    	return false;
    }
    
    // 被保险人数校验
    if(!checkPeople()){
    	return false;
    }
    
    // 校验险种370301是否录入退保费率
    if(!checkRiskZTFee()){
    	return false;
    }
    
  //校验险种370301是否录入了最低保证利率
    if(!checkGuaRate()){
    	return false;
    }

    // 校验险种162501是否录入赔付顺序
    if(!checkClaimNum()){
    	return false;
    }
    
    // 校验险种250101是否与551501绑定销售
    if(!check250101()){
    	return false;
    }
    
    // 校验险种162701与162901录入生效日与录入日期对比，zqt 20170920
    if(!check162701()){
    	return false;
    }

    // zqt 20171204
    if(!check163001()){
    	return false;
    }
    if(!check163002()){
    	return false;
    }
    
    //270102
    if(!check270102()){
    	return false;
    }
    
    //150407
    if(!check150407()){
    	return false;
    }

    //投被保人黑名单校验
    if(!checkBlackName()){
    	return false;
    }
    
    // 二期剩余功能，暂时注掉
	var qurSql="select bussno from es_issuedoc where bussno like '"+fm.PrtNo.value+"%' and status='1' and stateflag='1'";
  	var arrqurSql=easyExecSql(qurSql);
  	if(arrqurSql){
  		if (!confirm("该单还有扫描修改申请待审批，确认要继续吗？"))
        {
            return;
        }
    }
    
  cProposalGrpContNo = fm.ProposalGrpContNo.value;  // 团单投保单号码
  cflag="5";

  if(MissionID == "null" || SubMissionID == "null") {
    fm.MissionID.value = mSwitch.getVar('MissionID');
    fm.SubMissionID.value = mSwitch.getVar('SubMissionID');
  } else {
    mSwitch.deleteVar("MissionID");
    mSwitch.deleteVar("SubMissionID");
    mSwitch.addVar("MissionID", "", MissionID);
    mSwitch.addVar("SubMissionID", "", SubMissionID);
    mSwitch.updateVar("MissionID", "", MissionID);
    mSwitch.updateVar("SubMissionID", "", SubMissionID);
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  }
  if( cProposalGrpContNo == null || cProposalGrpContNo == "" )
    alert("请选择团体主险投保单后，再进行复核操作");
  else {
  
    // 对约定缴费计划信息进行校验
    if(!checkPayPlan()){
    	return false;
    }
    
    // 校验结余返还
    if(!checkBalance()){
    	return false;
    }
  //对121702、121802、121902、220402、220702、121301校验浮动费率
    if(!floatFee()){
    	return false;
    }
    
    if (confirm("该操作将复核通过该保单号下的所有投保信息,确定吗？")) {
      var i = 0;
      // var
		// showStr="正在复核团体投保单,系统将会在后台运行,此页面可以关闭,复合完毕后会自动进入人工核保,团体投保单号是:"+cProposalGrpContNo;
      var showStr="正在复核团体投保单,系统将会在后台运行,此页面可以关闭,复核完毕后会自动进入人工核保！";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.action="./GroupPolApproveSave.jsp?ProposalGrpContNo1="+cProposalGrpContNo+"&Flag1=5";
      fm.submit();
    } else
      return false;
  }

}
/*******************************************************************************
 * 点击返回按钮,关闭当前页面 参数 ： 无 返回值： 无
 * ********************************************************************
 */
/**
 * function goback() { if (LoadFlag=='14'||LoadFlag=='23') {
 * top.opener.querygrp(); } else { top.opener.easyQueryClick(); } top.close(); }
 */

function goback() {
  /**
	 * if (LoadFlag=='14'||LoadFlag=='23') { top.opener.querygrp(); } else {
	 * top.opener.easyQueryClick(); }
	 */
  top.close();
}

/*******************************************************************************
 * 复核修改该团单 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function approveupdate() {

  cProposalGrpContNo = fm.ProposalGrpContNo.value;  // 团单投保单号码
  cflag="5";
  if( cProposalGrpContNo == null || cProposalGrpContNo == "" )
    alert("请选择集体主险投保单后，再进行复核修改确认操作");
  else {
    if (confirm("该操作表示所有的修改已完成,确定吗？")) {
      var i = 0;
      var showStr="正在复核修改集体投保单，请您稍候并且不要修改屏幕上的值或链接其他页面,团体投保单号是:"+cProposalGrpContNo;
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      // window.open("./GroupPolApproveSave.jsp?ProposalGrpContNo="+cProposalGrpContNo+"&Flag1=5","windows1");
      fm.action="./GrpApproveModifyMakeSure.jsp?ProposalGrpContNo="+cProposalGrpContNo+"&Flag1=5";
      fm.submit();
    } else
      return false;
    // window.close();
    // fm.submit(); //提交
  }

}


/*******************************************************************************
 * 团单分单定制 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function grpSubContInfo() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能进行〔分单定制〕！");
    return;
  }
  delGrpVar();
  addGrpVar();
  cGrpContNo = fm.all("GrpContNo").value;
  cPrtNo = fm.all("PrtNo").value;
  var newWindow = window.open("../app/SubContPolMain.jsp?GrpContNo=" + cGrpContNo + "&PrtNo=" + cPrtNo+"&LoadFlag="+LoadFlag);
}



function getdetailaddress() {
  var strSQL="select b.AddressNo,b.GrpAddress,b.GrpZipCode,b.LinkMan1,b.Department1,b.HeadShip1,b.Phone1,b.E_Mail1,b.Fax1,b.LinkMan2,b.Department2,b.HeadShip2,b.Phone2,b.E_Mail2,b.Fax2,b.DetailAddress,b.postalprovince,b.postalcity,b.sex,b.nativeplace,b.occupationcode,b.occupationtype from LCGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
  arrResult=easyExecSql(strSQL);
  try {
    fm.all('GrpAddressNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
	fm.all('DetailAddress').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
	fm.all('ProvinceID').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
	fm.all('CityID').value= arrResult[0][17];
  } catch(ex) { }
  ;
}

/*******************************************************************************
 * 团体合同信息录入完毕确认 参数 ： wFlag--各状态时调用此函数所走的分支 返回值： 无
 * ********************************************************************
 */
function GrpInputConfirm(wFlag) {

	
	
    if(!checkSaleChnlInfo()){
    	return false;
    }
    
    if(!checkMarkettypeSalechnl()){
    	return false;
    }
    
    if(!checkRiskSalechnl()){
    	return false;
    }
    
   // 校验保单类型与险种
	if(!checkRiskContPrintType()){
		return false;
	}
	
    // 销售渠道和业务员的校验
	if(!checkSaleChnl())
	{
		return false;
	}
	
	
	// 中介机构的校验
	if(!checkAgentCom())
	{
		return false;
	}
    
    if(!checkCoInsuranceParams())
    {
        return false;
    }
    
    // by gzh 20110413 若存在没有算费的被保人，则阻断。
    if(!checkAllCalPrem()){
    	return false;
    }
    
    // 校验集团交叉销售相互代理业务的销售渠道与中介机构是否符合描述
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    // 非阻断校验，校验缴费频次与保险区间是否匹配
    if(!checkPayIntvMatchInsuYear())
    {
    	return false;
    }
    
    // 缴费频次payintv=-1时，市场类型必须是 2,4,5,6,8,10,11
    if(!checkPayIntv()){
    	return false;
    }
    
    if(!checkFirstPayPlan()){
    	return false;
    }
    
    // 校验代理机构及代理机构业务员
    if(!checkAgentComAndSaleCdoe()){
    	return false;
    }
    
    // 校验建工险
    if(!check590301()){
    	return false;
    }
    
    // 校验健享全球表定
    if(!checkB162001()){
    	return false;
    }
    
    // 反洗钱
    if(!checkFXQ()){
    	return false;
    }
    
    // 联系人联系电话对应不同投保人
    if(!checkPhone1()){
    	return false;
    }
    
    // 大团单校验
    if(!checkLGrp()){
    	return false;
    }
    
    // 校验简易团单的缴费方式
    if(!checkBriefGrp()){
    	return false;
    }
    
    // 校验简易团单是否与险种匹配
    if(!checkBriefGrpRisk()){
    	return false;
    }
    
    // 校验结余返还
    if(!checkBalance()){
    	return false;
    }
    
    // 商团大项目必录校验
    if(!checkBigProject()){
    	return false;
    }
    
    // 被保险人数校验
    if(!checkPeople()){
    	return false;
    }
    
    // 校验险种370301是否录入退保费率
    if(!checkRiskZTFee()){
    	return false;
    }
    
    //校验险种370301是否录入了最低保证利率
    if(!checkGuaRate()){
    	return false;
    }
    
    // 校验险种162501是否录入赔付顺序
    if(!checkClaimNum()){
    	return false;
    }
    
    // 校验险种250101是否与551501绑定销售
    if(!check250101()){
    	return false;
    }

    // 校验险种162701与162901录入生效日与录入日期对比，zqt 20170920
    if(!check162701()){
    	return false;
    }
    
 // 校验险种163001标准保费与录入保费对比，zqt 20171129
    if(!check163001()){
    	return false;
    }
    if(!check163002()){
    	return false;
    }
    
  //270102
    if(!check270102()){
    	return false;
    }
    
    //150407
    if(!check150407()){
    	return false;
    }
    
    //by yuchunjian  date 20180730   redmine 3884
    if(!checkFXlevel()){
    	return false;
    }
    
  mWFlag = 1;
  if (wFlag ==1 ) // 录入完毕确认
  {


    var tStr= "	select * from lwmission where 1=1 "
              +" and lwmission.processid = '0000000004'"
              +" and lwmission.activityid = '0000002001'"
              +" and lwmission.missionprop1 = '"+fm.ProposalGrpContNo.value+"'";
    turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
    if (turnPage.strQueryResult) {
      alert("该团单合同已经做过保存！");
      return;
    }

    var strsql = "";
    strsql="select InsuredNo from LCInsured where PrtNo ='"+fm.PrtNo.value+"'"	;
    turnPage.strQueryResult = easyQueryVer3(strsql, 1, 0, 1);
    if (!turnPage.strQueryResult) {
      alert("请添加被保人");
      return;
    }
    
    var strsql2 = "";
    strsql2="select PolNo from LcPol where PrtNo ='"+fm.PrtNo.value+"'"	;
    turnPage.strQueryResult = easyQueryVer3(strsql2, 1, 0, 1);
    if (!turnPage.strQueryResult) {
      alert("请添加险种信息");
      return;
    }

    if(fm.all('ProposalGrpContNo').value == "") {
      alert("团单合同信息未保存,不容许您进行 [录入完毕] 确认！");
      return;
    }
    
    // 如果是特需险种，进行公共帐户是否录入的判断。
    var tTmpGrpPrtNo = fm.PrtNo == null ? "" : fm.PrtNo.value;
    if(!checkPubAccData(tTmpGrpPrtNo))
    {
        return false;
    }
    
    var strsql3="select count(1) from LCGrpIssuePol where grpcontno in (select grpcontno from lcgrpcont where PrtNo ='"+fm.PrtNo.value+"') and state<>'5' ";
    var arr20=easyExecSql(strsql3);
 // alert(arr20);
    if(arr20)
    {
    	if(arr20[0][0]!=0){
    	fm.all('MissionProp20').value='N';
// alert(fm.all('MissionProp20').value);
    	}else{
    	fm.all('MissionProp20').value='';
// alert(fm.all('MissionProp20').value);
			}
    }
// return;

    if(ScanFlag=="0") {
      fm.WorkFlowFlag.value = "0000002098";
    }
    if(ScanFlag=="1") {
      fm.WorkFlowFlag.value = "0000002099";
    }
  }
  else if (wFlag ==2)// 复核完毕确认
  {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出团单合同信息,不容许您进行 [复核完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000002002";					// 复核完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else if (wFlag ==3) {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001002";					// 复核修改完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else if(wFlag == 4) {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出合同信息,不容许您进行 [修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001021";					// 问题修改
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else
    return;
    
    // 对约定缴费计划信息进行校验
    if(!checkPayPlan()){
    	return false;
    }
  //对121702、121802、121902、220402、220702、220802校验浮动费率
    if(!floatFee()){
    	return false;
    }

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./GrpInputConfirm.jsp";
  ChangeDecodeStr();
  fm.submit(); // 提交
}

/*******************************************************************************
 * 初始化工作流MissionID 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function initMissionID() {
  if(MissionID == "null" || SubMissionID == "null") {
    MissionID = mSwitch.getVar('MissionID');
    SubMissionID = mSwitch.getVar('SubMissionID');
  } else {
    mSwitch.deleteVar("MissionID");
    mSwitch.deleteVar("SubMissionID");
    mSwitch.addVar("MissionID", "", MissionID);
    mSwitch.addVar("SubMissionID", "", SubMissionID);
    mSwitch.updateVar("MissionID", "", MissionID);
    mSwitch.updateVar("SubMissionID", "", SubMissionID);
  }
}
function getaddresscodedata() {
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  // strsql = "select AddressNo,GrpAddress from LCGrpAddress where CustomerNo
	// ='"+fm.GrpNo.value+"'";
  strsql = "select max(int(AddressNo)) from LCGrpAddress where CustomerNo ='"+fm.GrpNo.value+"'";
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      // tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" +
		// turnPage.arrDataCacheSet[i][1];
      tCodeData =turnPage.arrDataCacheSet[i][0];
    }
  }
  // alert ("tcodedata : " + tCodeData);
  // return tCodeData;
  fm.all("GrpAddressNo").value=tCodeData;
  afterCodeSelect( "GetGrpAddressNo","");
}

function grpPersonAge() {
  // showInfo =
	// window.open("./GrpPersonAgeInput.jsp?GrpContNo="+fm.GrpContNo.value+");
  var tProposalGrpContNo = "";
  tProposalGrpContNo = fm.all( 'ProposalGrpContNo' ).value;
  if( tProposalGrpContNo == null || tProposalGrpContNo == "" ) {
    alert( "必须保存合同信息才能进入〔险种信息〕！" );
    return false
         }
         delGrpVar();
  addGrpVar();

  var cGrpContNo = fm.all("GrpContNo").value;
  var	cPrtNo = fm.all("PrtNo").value;
  showInfo = window.open("GrpPersonAgeMain.jsp?GrpContNo=" + cGrpContNo + "&PrtNo=" + cPrtNo);
}

// 用于改变日期格式
function ChangDateFormate(obj,cDate) {
  if(cDate.length==8) {
    var year = cDate.substring(0,4);
    var month = cDate.substring(4,6);
    var day = cDate.substring(6,8);

    var rDate = year+"-"+month+"-"+day;
    if(isDate(rDate)) {
      return obj.value=rDate;
    }
  }
}

function GrpNameDif() {
  if(fm.all('Grpno').value!="") {
    var strSql="select * from LDGrp where Grpname='"+fm.all('Grpname').value+"' and Customerno = '"+fm.all('Grpno').value+"'";
    var arrResult = easyExecSql(strSql);
    if(!arrResult) {
      return false;
    }
  }
}
// 此函数用于无名单补名单
function grpfilllist() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能〔添加被保人〕信息！");
    return false;
  }
  // alert("1111"+fm.GrpContNo.value);
  // fm.GrpContNo.value=fm.ProposalGrpContNo.value;
  delGrpVar();
  addGrpVar();
  parent.fraInterface.window.location = "../app/ContInsuredInput.jsp?LoadFlag=9&ContType=2&scantype="+ scantype+"&checktype=2"+"&ScanFlag="+ScanFlag+"&oldContNo="+oldContNo;
}

// 对团体告知信息进行保存
var LCImpartStr1 = "";
var LCImpartStr2 = "";
var LCImpartStr3 = "";
var ImpartCheck1 = new Array();
var ImpartCheck2 = new Array();
var ImpartCheck3 = new Array();
function LCImpartInput() {
  // 告知
  ImpartGrid.clearData();
  for(var i=0;i < fm.ImpartCheck1.length;i++) {
    // 判断输入框
    if(fm.ImpartCheck1[i].type=="text") {
      if(fm.ImpartCheck1[i].value=="") {
        ImpartCheck1[i] = "N,";
      } else {
        ImpartCheck1[i] = fm.ImpartCheck1[i].value + ",";
      }
    }
    // 判断复选框
    if(fm.ImpartCheck1[i].type=="checkbox") {
      if(fm.ImpartCheck1[i].checked==true) {
        ImpartCheck1[i] = "Y,";
      } else {
        ImpartCheck1[i] = "N,";
      }
    }
    // 各字符串相加
    LCImpartStr1 += ImpartCheck1[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr1.indexOf("Y,") != -1) {
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,1,"011");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,2,"010");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,3,"投保前参保人员医疗保障形式   □社会基本医疗保险  参加年份______　　□商业医疗保险　□单位报销  □其它______");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,4,deleteLastCharWhenComma(LCImpartStr1));
  }

  // 缴费主体
  for(var i=0;i < fm.ImpartCheck2.length;i++) {
    // 判断输入框
    if(fm.ImpartCheck2[i].type=="text") {
      if(fm.ImpartCheck2[i].value=="") {
        ImpartCheck2[i] = "N,";
      } else {
        ImpartCheck2[i] = fm.ImpartCheck2[i].value + ",";
      }
    }
    // 判断复选框
    if(fm.ImpartCheck2[i].type=="checkbox") {
      if(fm.ImpartCheck2[i].checked==true) {
        ImpartCheck2[i] = "Y,";
      } else {
        ImpartCheck2[i] = "N,";
      }
    }
    // 各字符串相加
    LCImpartStr2 += ImpartCheck2[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr2.indexOf("Y,") != -1) {
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,1,"021");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,2,"010");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,3,"□ 投保人全额承担  □被保人全额承担  □双方共同承担,其中投保人承担______％，被保人承担______％");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,4,deleteLastCharWhenComma(LCImpartStr2));
  }
  // 材料渠道
  for(var i=0;i < fm.ImpartCheck3.length;i++) {
    // 判断输入框
    if(fm.ImpartCheck3[i].type=="text") {
      if(fm.ImpartCheck3[i].value=="") {
        ImpartCheck3[i] = "N,";
      } else {
        ImpartCheck3[i] = fm.ImpartCheck3[i].value + ",";
      }
    }
    // 判断复选框
    if(fm.ImpartCheck3[i].type=="checkbox") {
      if(fm.ImpartCheck3[i].checked==true) {
        ImpartCheck3[i] = "Y,";
      } else {
        ImpartCheck3[i] = "N,";
      }
    }
    // 各字符串相加
    LCImpartStr3 += ImpartCheck3[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr3.indexOf("Y,") != -1) {
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,1,"022");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,2,"001");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,3,"材料交接：被保险人清单 □电子文档 □纸质表格______页 其它资料______,______份/页");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,4,deleteLastCharWhenComma(LCImpartStr3));
  }
  ImpartGrid.delBlankLine();
}

// 若最后一个字符是逗号，删除它
function deleteLastCharWhenComma(str)
{
  if(str == null || str.length == 0)
  {
    return str;
  }
  
	if(str.charAt(str.length - 1) == ",")
	{
	  str = str.substring(0, str.length - 1);  // 去掉最后一个逗号
	}
	return str;
}

function getLCImpart() {
  var strSQL =  " select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '011' and ImpartCode = '010'"
  						 +" union "
  						 +" select ImpartParamModle from LbCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '011' and ImpartCode = '010'"
							 +" union "
  						 +" select ImpartParamModle from LobCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '011' and ImpartCode = '010'"
  							;
  var arrResult = easyExecSql(strSQL);
  if (arrResult != null) {
    var ImpartInfo1 = new Array();
    ImpartInfo1 = arrResult[0][0].split(",");

    if(ImpartInfo1[0]=="N") {
      fm.ImpartCheck1[0].checked = false;
    } else {
      fm.ImpartCheck1[0].checked = true;
    }

    if(ImpartInfo1[1]=="N") {
      fm.ImpartCheck1[1].value = "";
    } else {
      fm.ImpartCheck1[1].value = ImpartInfo1[1];
    }

    if(ImpartInfo1[2]=="N") {
      fm.ImpartCheck1[2].checked = false;
    } else {
      fm.ImpartCheck1[2].checked = true;
    }

    if(ImpartInfo1[3]=="N") {
      fm.ImpartCheck1[3].checked = false;
    } else {
      fm.ImpartCheck1[3].checked = true;
    }

    if(ImpartInfo1[4]=="N") {
      fm.ImpartCheck1[4].value = "";
    } else {
      fm.ImpartCheck1[4].value = ImpartInfo1[4];
    }
  }

  // 缴费主体

  var strSQL2 = " select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '021' and ImpartCode = '010'"
  						 +" union "
  						 +" select ImpartParamModle from LbCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '021' and ImpartCode = '010'"
							 +" union "
  						 +" select ImpartParamModle from LobCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '021' and ImpartCode = '010'"
								;
  var arrResult2 = easyExecSql(strSQL2);
  if (arrResult2 != null) {
    var ImpartInfo2 = new Array();
    ImpartInfo2 = arrResult2[0][0].split(",");

    if(ImpartInfo2[0]=="N") {
      fm.ImpartCheck2[0].checked = false;
    } else {
      fm.ImpartCheck2[0].checked = true;
    }

    if(ImpartInfo2[1]=="N") {
      fm.ImpartCheck2[1].checked = false;
    } else {
      fm.ImpartCheck2[1].checked = true;
    }

    if(ImpartInfo2[2]=="N") {
      fm.ImpartCheck2[2].checked = false;
    } else {
      fm.ImpartCheck2[2].checked = true;
    }

    if(ImpartInfo2[3]=="N") {
      fm.ImpartCheck2[3].value = "";
    } else {
      fm.ImpartCheck2[3].value = ImpartInfo2[3];
    }

    if(ImpartInfo2[4]=="N") {
      fm.ImpartCheck2[4].value = "";
    } else {
      fm.ImpartCheck2[4].value = ImpartInfo2[4];
    }
  }
  // 材料
  var strSQL3 = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '022' and ImpartCode = '001'"
							 +" union "
							 +"select ImpartParamModle from LbCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '022' and ImpartCode = '001'"
							 +" union "
							 +"select ImpartParamModle from LobCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '022' and ImpartCode = '001'"
							 ;
  var arrResult3 = easyExecSql(strSQL3);
  if (arrResult3 != null) {
    var ImpartInfo3 = new Array();
    ImpartInfo3 = arrResult3[0][0].split(",");

    if(ImpartInfo3[0]=="N") {
      fm.ImpartCheck3[0].checked = false;
    } else {
      fm.ImpartCheck3[0].checked = true;
    }

    if(ImpartInfo3[1]=="N") {
      fm.ImpartCheck3[1].checked = false;
    } else {
      fm.ImpartCheck3[1].checked = true;
    }
    if(ImpartInfo3[2]=="N") {
      fm.ImpartCheck3[2].checked = false;
    } else {
      fm.ImpartCheck3[2].value = ImpartInfo3[2];
    }
    if(ImpartInfo3[3]=="N") {
      fm.ImpartCheck3[3].value = "";
    } else {
      fm.ImpartCheck3[3].value = ImpartInfo3[3];
    }

    if(ImpartInfo3[4]=="N") {
      fm.ImpartCheck3[4].value = "";
    } else {
      fm.ImpartCheck3[4].value = ImpartInfo3[4];
    }
  }
	 
}

// 缴费主体的单选效果
function ImpartCheck2Radio1() {
  if(fm.ImpartCheck2[0].checked == true) {
    fm.ImpartCheck2[1].checked = false;
    fm.ImpartCheck2[2].checked = false;
    fm.ImpartCheck2[3].value = "";
    fm.ImpartCheck2[4].value = "";
  }
}

function ImpartCheck2Radio2() {
  if(fm.ImpartCheck2[1].checked == true) {
    fm.ImpartCheck2[0].checked = false;
    fm.ImpartCheck2[2].checked = false;
    fm.ImpartCheck2[3].value = "";
    fm.ImpartCheck2[4].value = "";
  }
}

function ImpartCheck2Radio3() {
  if(fm.ImpartCheck2[2].checked == true) {
    fm.ImpartCheck2[0].checked = false;
    fm.ImpartCheck2[1].checked = false;
    fm.ImpartCheck2[3].value = "";
    fm.ImpartCheck2[4].value = "";
  }
}

// 告知的初始化
function ImpartClear() {
  try {
    fm.ImpartCheck1[0].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck1[1].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck1[2].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck1[3].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck1[4].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[0].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[1].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[2].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[3].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[4].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[0].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[1].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[2].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[3].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[4].value="";
  } catch(ex) { }
  ;
}
function ChkMulLineCount()
{
	if(BookingPayIntyGrid.mulLineCount==7)
	{
		alert("约定缴费方式只能录入六次");
		BookingPayIntyGrid.delBlankLine();
		return ;
	}
}
function displayBookingPay()
{
	/*
	 * if(fm.GrpContPayIntv.value==-1) { divBookingPayInty.style.display=''; var
	 * strSql = "select PayToDate, PayToAmount from LCGrpSpecFee where
	 * GrpContNo='" + fm.all('GrpContNo').value + "' order by PeriodNo";
	 * turnPage.queryModal(strSql,BookingPayIntyGrid); }
	 */
}
// 添加缴费频次的校验
function ChkPayIntv()
{
	var tPayInv = fm.GrpContPayIntv.value;
	
	for (m=0; m<RiskGrid.mulLineCount; m++)
	{
	    var tRiskCode = RiskGrid.getRowColData(m,1);
	    var strSQL = "select 1 from LMRiskPayIntv where riskcode = '"+tRiskCode+"'";
	    var arrResult = easyExecSql(strSQL);
	    if(arrResult != null)
	    {
	        var strSql = "select 1 from LMRiskPayIntv where "
							+" riskcode ='"+tRiskCode+"'"
							+" and PayIntv='"+tPayInv+"'";
			  
			var arr = easyExecSql(strSql);
			if(!arr)
			{
				alert("险种"+tRiskCode+"的缴费频次不支持"+fm.GrpContPayIntvName.value+",请修改页面中的缴费频次并且保存至数据库.");
				return false;
			}
	    }
	}
	return true;
}
// 定义公共账户
function grpPubAccInput(RiskCode,GrpPolNo)
{
	var ChkCount = 0;
	var tRow;
	if(RiskCode==null)
	{
		for(var row=0;row<RiskGrid.mulLineCount;row++)
		{
			var ChkNo = RiskGrid.getChkNo(row);
			if(ChkNo)
			{
				ChkCount++;
				tRow=row;
			}
		}
		if(ChkCount<=0||ChkCount>1)
		{
			alert("请选择一条险种信息！");
			return;
		}
		RiskCode = RiskGrid.getRowColData(tRow,1);
		var strSql = "select grppolno from lcgrppol where GrpContNo='"
		+fm.all('GrpContNo').value+"' and RiskCode ='"+RiskCode+"'";
		var arr = easyExecSql(strSql);
		if(arr)
		{
			GrpPolNo=arr[0][0];
		}
	}
	if(RiskCode=="162401"){
		alert("险种162401不需要录入公共账户");
		return false;
	}
	// add by zjd 170501 险种类公共账户录入界面
	if(RiskCode=="170501"||RiskCode=="162501"){
		if(LoadFlag=="16"){
			var showInfo = window.open("./PublicAccAmntMain.jsp?GrpContNo="+fm.all('GrpContNo').value+"&RiskCode="+RiskCode+"&GrpPolNo="+GrpPolNo+"&LookFlag=1");
		}else{
			var showInfo = window.open("./PublicAccAmntMain.jsp?GrpContNo="+fm.all('GrpContNo').value+"&RiskCode="+RiskCode+"&GrpPolNo="+GrpPolNo+"&LookFlag=0");
		}
	}else{
		if(LoadFlag=="16"){
			var showInfo = window.open("./PublicAccMain.jsp?GrpContNo="+fm.all('GrpContNo').value+"&RiskCode="+RiskCode+"&GrpPolNo="+GrpPolNo+"&LookFlag=1");
		}else{
			var showInfo = window.open("./PublicAccMain.jsp?GrpContNo="+fm.all('GrpContNo').value+"&RiskCode="+RiskCode+"&GrpPolNo="+GrpPolNo+"&LookFlag=0");
		}
	}

}

// 缴费主体的共担校验
function CheckImpart()
{
  if(fm.ImpartCheck2[2].checked==true)
  {
    if(fm.ImpartCheck2[3].value=="" || fm.ImpartCheck2[4].value=="")
    {
    	alert("交费主体,双方共同承担,投被保人承担比例不能为空");
    	LCImpartStr1 = "";
			LCImpartStr2 = "";
			LCImpartStr3 = "";
			ImpartCheck1 = new Array();
			ImpartCheck2 = new Array();
			ImpartCheck3 = new Array();
      return false;
    }
   if(parseInt(fm.ImpartCheck2[3].value)==100 || parseInt(fm.ImpartCheck2[4].value)==100)
   {
   	alert("交费主体,双方共同承担,投被保人承担百分比不能一方为100%,请检查!");
   	LCImpartStr1 = "";
		LCImpartStr2 = "";
		LCImpartStr3 = "";
		ImpartCheck1 = new Array();
		ImpartCheck2 = new Array();
		ImpartCheck3 = new Array();
     return false;
   }

   if((parseInt(fm.ImpartCheck2[3].value)+parseInt(fm.ImpartCheck2[4].value))!=100)
   {
   	alert("交费主体,双方共同承担,投被保人承担百分比合计不为100%,请检查!");
   	LCImpartStr1 = "";
		LCImpartStr2 = "";
		LCImpartStr3 = "";
		ImpartCheck1 = new Array();
		ImpartCheck2 = new Array();
		ImpartCheck3 = new Array();
     return false;
   }
  }
}


function ChkRiskVer()
{
	var tRiskCode = fm.all('RiskCode').value;
	var strSql = "select enddate from LMRiskApp where "
							+" riskcode ='"+tRiskCode+"'";
	var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
      if((fm.all('CValiDate').value)>arrResult[0][0] &&  arrResult[0][0]!='')
      {
      	alert("该产品已经停售!");
      	return false;
      }
    else
    {
      return true;
    }
  }
	return true;
}
function CheckDateDollar()
{
	var linecount = DiseaseGrid.mulLineCount;
	for( var i=0; i<linecount;i++)
	{
		var iarray=DiseaseGrid.getRowColData(i,1);
		// if(!isDate(iarray))
		// {
		// alert("日期格式有误!正确格式为'yyyy-mm-dd'");
		// return false;
		// }
		iarray=DiseaseGrid.getRowColData(i,4);
		if(!isNumeric(iarray))
		{
			alert("医疗费用金额只能为数字");
			return false;
		}
		iarray=DiseaseGrid.getRowColData(i,3);
		if(!isInteger(iarray))
		{
			alert("患病人数必须为整数!");
		 	return false;
		}
	}
	var historylinecount =HistoryImpartGrid.mulLineCount;
	for( var i=0;i<historylinecount;i++)
	{
		var iarray =HistoryImpartGrid.getRowColData(i,4);
		if(!isNumeric(iarray))
		{
			alert("保险费只能为数字");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,6);
		if(!isInteger(iarray))
		{
			alert("参加人数必须为整数");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,7);
		if(!isInteger(iarray))
		{
			alert("参加人数必须为整数");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,8);
		if(!isNumeric(iarray))
		{
			alert("发生金额必须为数字!");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,9);
		if(!isNumeric(iarray))
		{
			alert("报销金额只能为数字!");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,10);
		if(!isNumeric(iarray))
		{
			alert("未决金额必须为数字!");
			return false;
		}
	}
}
function BigProjectApprove(){
    // 销售渠道和业务员的校验
	if(!checkSaleChnl())
	{
		return false;
	}
    
    if(!checkCoInsuranceParams())
    {
        return false;
    }
    
	if(fm.GrpContNo.value=="" || fm.GrpContNo.value==null){
		alert("团体合同号码为null");
		return;
	}
	// by gzh 20110413 若存在没有算费的被保人，则阻断。
    if(!checkAllCalPrem()){
    	return false;
    }
    // 非阻断校验，校验缴费频次与保险区间是否匹配
    if(!checkPayIntvMatchInsuYear())
    {
    	return false;
    }
    // 校验代理机构及代理机构业务员
    if(!checkAgentComAndSaleCdoe()){
    	return false;
    }
    
    // 校验建工险
    if(!check590301()){
    	return false;
    }
    
    // 校验健享全球表定
    if(!checkB162001()){
    	return false;
    }
    
    // 反洗钱
    if(!checkFXQ()){
    	return false;
    }
    
    // 联系人联系电话对应不同投保人
    if(!checkPhone1()){
    	return false;
    }
    
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./BigProjectApproveSave.jsp";
	fm.fmAction.value = "INSERT||BIGPROJCET";
	fm.submit();
}


// 定义建工意外要素信息
function initContractor(){

	var strSql = "select count(*) from lcgrppol where GrpContNo='"+GrpContNo+"' and riskcode in (select riskcode from LMRiskApp where risktype8='4') ";
	var arr = easyExecSql(strSql);

	if(arr){
		if(arr[0][0]<0){
			alert("保单没有建工险产品不需要定义要素信息！");
			return;
		}
	}else{
		alert("保单没有建工险产品不需要定义要素信息！");
		return;
	}
	// alert(LoadFlag);
	var showInfo = window.open("./TbGrpContractorMain.jsp?GrpContNo="+fm.all('GrpContNo').value+"&LoadFlag="+LoadFlag,"PublicAccMain");

}

/**
 * 判断特需险种是否已经录入了帐户信息。
 */
function checkPubAccData(tGrpPrtNo)
{
    var tBResult = true;
    var tStrSql = " select distinct lgp.grppolno, lgp.riskcode "
        + " from lcgrppol lgp "
        + " where prtno = '" + tGrpPrtNo + "' "
        + " and exists (select 1 from LMRisktoAcc lmrta where lmrta.riskcode = lgp.riskcode and lmrta.riskcode!='162401') "
        + " and not exists (select 1 from LCGrpFee lgf where lgf.riskcode = lgp.riskcode and lgf.grppolno = lgp.grppolno) ";
    var arr = easyExecSql(tStrSql);
    if(arr)
    {
        var tComment = "";
        for(index in arr)
        {
            if(arr[index] != "")
            {
                tComment += "[" + arr[index][1] + "]";
            }
        }
        tComment += "并未添加相应帐户信息。";
        alert(tComment);
        tBResult = false; 
    }
    return tBResult;
}

// 若保障期不为整年，则保单必须是趸交，整年判断标准如下
// 1、若起始日期和终止日期的下一天 的月、日分别相等则为整年
// 2、闰月需要特殊处理，闰月判断标准如下:
// 起始日期 终止日期 终止日期+1天 是否整年
// （闰月开始）：
// 2008-2-28 2009-2-27 2009-2-28 是 同规则1
// 2008-2-29 2009-2-28 2009-3-1 是
// 2008-3-1 2009-2-28 2009-3-1 是 同规则1
// （闰月结束）：
// 2007-2-28 2008-2-27 2008-2-28 是 同规则1
// 2007-3-1 2008-2-29 2008-3-1 是 同规则1
// 2007-3-1 2008-2-28 2008-2-29 不是 同规则1
// 所以闰月只需要处理以2月29日起始的情况
function checkInsurePeriod()
{
  // 趸交不做判断
  if(fm.GrpContPayIntv.value == "0")
  {
    return true;
  }
  
  var tCValiDate = toDate(fm.CValiDate.value);
  var tCInValiDate = toDate(fm.CInValiDate.value);
  if(tCValiDate == null || tCInValiDate == null)
  {
    alert("保险责任终起止日期格式不正确");
  }
  
  // 求终止日期的下一天
  var tCInvaliDateAddOne = easyExecSql("select date('" + fm.CInValiDate.value + "') + 1 days from dual ");
  if(tCInvaliDateAddOne == null || tCInvaliDateAddOne == "" || tCInvaliDateAddOne == "null")
  {
    alert("保险责任终止日期格式有误");
    return false;
  }
  tCInvaliDateAddOne = toDate(tCInvaliDateAddOne);
  
  // 规则1
  if(tCValiDate.getMonth() == tCInvaliDateAddOne.getMonth() && tCValiDate.getDate() == tCInvaliDateAddOne.getDate())
  {
    return true;
  }
  
  // 规则2
  if(tCValiDate.getMonth() == tCInValiDate.getMonth() && tCValiDate.getMonth() == 1
    && tCValiDate.getDate() == 29 && tCInValiDate.getDate() == 28)
  {
    return true;
  }
  
// alert("若保单保障期不为整年，则保单应该是趸交。");
  
  return true;
}

// 由字符串生成日期
function toDate(str)
{
    var pattern = /^(\d{4})(-)(0?[1-9]|1[0-2])(-)(0?[1-9]|[12][0-9]|3[01])$/g; 
    var arr = pattern.exec(str);
    if (arr == null)
    {
      return null;
    }
    var date = new Date(arr[1], arr[3]-1, arr[5]);
    return date;
}

// 销售渠道和业务员的校验
function checkSaleChnl()
{
	var saleChnl = fm.SaleChnl.value;
	var agentCode = fm.AgentCode.value;

	var check = easyExecSql("select 1 from LAAGENT a, LDCode1 b where CodeType = 'salechnl' and Code = '" 
	    + saleChnl + "' and AGENTCODE = '" + agentCode + "' AND BranchType = Code1 and BranchType2 = CodeName");
	if(!check)
	{
        // alert("销售渠道和业务员不相符!");
        // return false;
        // 去除销售渠道和业务员不相符的阻断校验，改为提示 2008-9-28
        return confirm("销售渠道和业务员不相符！确认保存？");
	}
	return true;
}

/**
 * 校验共保保单的相关要素是否齐全。
 */
function checkCoInsuranceParams()
{
    var tStrSql = ""
        + " select 1 "
        + " from LCGrpCont lgc "
        + " left join LCIGrpCont lcigc on lgc.GrpContNo = lcigc.GrpContNo "
        + " left join LCCoInsuranceParam lcip on lcip.GrpContNo = lcigc.GrpContNo "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" + mGrpContNo + "' "
        + " and lgc.CoInsuranceFlag = '1' "
        + " and lcigc.GrpContNo is null "
        + " and lcip.GrpContNo is null "
        ;
    var arr = easyExecSql(tStrSql);
    if(arr)
    {
        alert("该单为共保保单，但共保要素信息不全，请进行确认。");
        return false;
    }
    
    return true;
}

/**
 * 显示共保险种录入要素界面。
 */
function showCoInsuranceParamWin()
{
    var tUrl = ""
        + " ./CoInsuranceParamMain.jsp"
        + "?GrpContNo=" + fm.all('GrpContNo').value 
        + "&LoadFlag=" + LoadFlag
        ;
    var showInfo = window.open(tUrl, "PublicAccMain");
}

/**
 * 启用/禁用“共保要素录入”按钮。
 */
function initCoInsuranceParamInput()
{
    var flag = fm.CoInsuranceFlag.value;
    
    if(flag == "1")
    {
        fm.btnCoInsuranceParam.disabled = false;
    }
    else
    {
        fm.btnCoInsuranceParam.disabled = true;
    }
}

// 开办市县数和开办市县内容校验
function checkCity(flag)
{
// return true;//临时取消校验。
// if(fm.MarketType.value >= 2 && fm.MarketType.value <= 99)
// {
// if(fm.CTCount.value == null || fm.CTCount.value == "" || fm.CityInfo.value ==
// null || fm.CityInfo.value == "")
// {
// alert("市场类型选择2-99时，开办市县数和开办市县内容必须录入！");
// return false;
// }
// }
//    
// //保存不校验数据库值
// if(flag == 1) return true;
//    
// var citySQL = "select 1 from LCGrpCont where PrtNo = '"
// + fm.PrtNo.value + "' and MarketType between '2' and '99' "
// + "and (CTCount is null or CityInfo is null or CityInfo = '')";
// var check = easyExecSql(citySQL);
// if(check)
// {
// alert("市场类型选择2-99时，开办市县数及开办市县内容必须录入！");
// return false;
// }
    return true;
}


/**
 * 集团交叉要素校验
 */
function checkCrsBussParams()
{
    var tCrs_SaleChnl = trim(fm.Crs_SaleChnl.value);
    var tCrs_BussType = trim(fm.Crs_BussType.value);
    
    var tGrpAgentCom = trim(fm.GrpAgentCom.value);
    var tGrpAgentCode = trim(fm.GrpAgentCode.value);
    var tGrpAgentName = trim(fm.GrpAgentName.value);
    var tGrpAgentIDNo = trim(fm.GrpAgentIDNo.value);
    
    if(tCrs_SaleChnl != null && tCrs_SaleChnl != "")
    {
        if(tCrs_BussType == null || tCrs_BussType == "")
        {
            alert("选择集团交叉渠道时，集团交叉业务类型不能为空。");
            return false;
        }
        if(tGrpAgentCom == null || tGrpAgentCom == "")
        {
            alert("选择集团交叉渠道时，对方业务员机构不能为空。");
            return false;
        }
        if(tGrpAgentCode == null || tGrpAgentCode == "")
        {
            alert("选择集团交叉渠道时，对方业务员代码不能为空。");
            return false;
        }
        if(tGrpAgentName == null || tGrpAgentName == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名不能为空。");
            return false;
        }
        if(tGrpAgentIDNo == null || tGrpAgentIDNo == "")
        {
            alert("选择集团交叉渠道时，对方业务员身份证不能为空。");
            return false;
        }
    }
    else
    {
        if(tCrs_BussType != null && tCrs_BussType != "")
        {
            alert("未选择集团交叉渠道时，集团交叉业务类型不能填写。");
            return false;
        }
        if(tGrpAgentCom != null && tGrpAgentCom != "")
        {
            alert("未选择集团交叉渠道时，对方业务员机构不能为填写。");
            return false;
        }
        if(tGrpAgentCode != null && tGrpAgentCode != "")
        {
            alert("未选择集团交叉渠道时，对方业务员代码不能为填写。");
            return false;
        }
        if(tGrpAgentName != null && tGrpAgentName != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名不能为填写。");
            return false;
        }
        if(tGrpAgentIDNo != null && tGrpAgentIDNo != "")
        {
            alert("未选择集团交叉渠道时，对方业务员身份证不能为填写。");
            return false;
        }
    }
    
    return true;
}

// 显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value = "";
        fm.all('Crs_SaleChnlName').value = "";
        fm.all('Crs_BussType').value = "";
        fm.all('Crs_BussTypeName').value = "";
        fm.all('GrpAgentCom').value = "";
        fm.all('GrpAgentComName').value = "";
        fm.all('GrpAgentCode').value = "";
        fm.all('GrpAgentName').value = "";
        fm.all('GrpAgentIDNo').value = "";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}

// 根据对方机构代码带出对方机构名称
function GetGrpAgentName()
{
	var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     fm.GrpAgentComName.value = "";
	}
}  
// 根据机构代码显示机构名称
function getAgentName()
{
    var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     alert("对方机构代码有错误,请修改");
	     fm.GrpAgentCom.value = "";
	     return false;
	}
}

/**
 * 校验生效日期填写规范。 规则：生效日期 在 录入日期（判断录入当天） 之前时，返回false；否则，返回true；
 */
function chkValidate()
{
    var tValidate = fm.CValiDate.value;
    var tCurDate = getCurrentDate();
    
    var tMarketType = fm.MarketType.value;
    
    if(tMarketType == null || tMarketType == "" || tMarketType == "1" || tMarketType == "99")
    {
        if(dateDiff(tCurDate, tValidate, "D") < 0)
        {
            return false;
        }
    }
    return true;
}

// 校验被保人职业类别与职业代码
function checkOccTypeOccCode()
{
	var sql = "select 1 from lcpol where poltypeflag = '0' and prtno = '" + fm.PrtNo.value + "'";
	var result = easyExecSql(sql);
	if(result != null)
	{
		var strSql = "select distinct OccupationType,OccupationCode from lcinsured "
					+ " where prtno = '" + fm.PrtNo.value + "'";
		var arrResult = easyExecSql(strSql);
		var i = 0;
		
		if(arrResult != null) 
		{
			for(i = 0; i < arrResult.length; i++)
			{
				var OccupationType = arrResult[i][0];
				var OccupationCode = arrResult[i][1];
				
				if(OccupationType != null && OccupationType != "")
				{
					if(!CheckOccupationType(OccupationType))
					{
						return false;
					}
				}
				
				if(OccupationCode != null && OccupationCode != "")
				{
					
					if(!CheckOccupationCode(OccupationType,OccupationCode))
					{
						return false;
					}
					
				}
			}
		}
	}
	return true;
}

// 校验职业代码
function CheckOccupationCode(Type,Code)
{
	var strSql = "select 1 from ldoccupation where OccupationCode = '" + Code +"' and OccupationType = '" + Type + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别和职业代码与系统描述不一致，请查看！");
		return false
	} 
	return true;
}

// 校验职业类别
function CheckOccupationType(Type)
{
	var strSql = "select 1 from dual where '" + Type + "' in (select distinct occupationtype from ldoccupation)";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6");
		return false
	} 
	return true;
}

// 校验被保人性别
function CheckSex()
{
	var strSql = "select distinct Sex from lcinsured where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSql);
	var i = 0;
	if(arrResult != null)
	{
		for(i = 0; i < arrResult.length; i++)
		{
			var sex = arrResult[i][0];
			if(sex != "0" && sex != "1" && sex != "2")
			{
				alert("被保人的性别填写有误，请查看。\n性别必须是半角数字的0、1、2 ，规则是0-男，1-女，2-其他");
				return false;
			}
		}
	}
	return true;
}

// 执行查询交叉销售业务员代码 date 20101029 by gzh
function queryGrpAgent()
{
    if(fm.all('GrpAgentCode').value == "" && fm.all('GrpAgentIDNo').value == "" )
    {  
        var tGrpAgentCom = (fm.GrpAgentCom != null && fm.GrpAgentCom != "undefined") ? fm.GrpAgentCom.value : "";
        var tGrpAgentName = (fm.GrpAgentName != null && fm.GrpAgentName != "undefined") ? fm.GrpAgentName.value : "";
        var tGrpAgentIDNo = (fm.GrpAgentIDNo != null && fm.GrpAgentIDNo != "undefined") ? fm.GrpAgentIDNo.value : "";
        var strURL = "../sys/GrpAgentCommonQueryMain.jsp?GrpAgentCom=" + tGrpAgentCom +
                     "&GrpAgentName=" + tGrpAgentName + "&GrpAgentIDNo=" + tGrpAgentIDNo;        
        // alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCode').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Sales_Cod='" + fm.all('GrpAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            // alert("查询结果: 业务员代码:["+arrResult[0][0]+"]
			// 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.GrpAgentCode.value + "]的业务员不存在，请确认!");
            // fm.GrpAgentCode.value="";
        }
    }else if(fm.all('GrpAgentIDNo').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Id_No='" + fm.all('GrpAgentIDNo').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            // alert("查询结果: 业务员代码:["+arrResult[0][0]+"]
			// 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("身份证号码为:[" +  fm.GrpAgentIDNo.value + "]的业务员不存在，请确认!");
            // fm.GrpAgentCode.value="";
        }
    }
}


// 查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery3(arrResult)
{  
  if(arrResult!=null)
    {  	
  	fm.GrpAgentCode.value = arrResult[0][0];
  	fm.GrpAgentName.value = arrResult[0][1];
    fm.GrpAgentIDNo.value = arrResult[0][2];
  }
}


// 执行查询交叉销售对方机构代码 date 20101117 by gzh
function queryGrpAgentCom()
{
	if(fm.all('Crs_SaleChnl').value == "" || fm.all('Crs_SaleChnl').value == null)
	{
		alert("请先选择交叉销售渠道！！");
		return false;
	}
    if(fm.all('GrpAgentCom').value == "")
    {  
        var tCrs_SaleChnl =  fm.Crs_SaleChnl.value;
        var tCrs_SaleChnlName = fm.Crs_SaleChnlName.value;
        var strURL = "../sys/GrpAgentComQueryMain.jsp?Crs_SaleChnl="+tCrs_SaleChnl+"&Crs_SaleChnlName="+tCrs_SaleChnlName;        
        // alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentComQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCom').value != "")
    {	
        var strGrpSql = "select grpagentcom,Under_Orgname  from lomixcom  where  grpagentcom ='"+fm.GrpAgentCom.value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery4(arrResult);
            // alert("查询结果: 业务员代码:["+arrResult[0][0]+"]
			// 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("机构代码为:[" +  fm.GrpAgentCom.value + "]的机构不存在，请确认!");
            // fm.GrpAgentCode.value="";
        }
    }
}


// 查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery4(arrResult)
{  
  if(arrResult!=null)
    { 	
  	fm.GrpAgentCom.value = arrResult[0][0];
  	fm.GrpAgentComName.value = arrResult[0][1];
  }
}

// 选择交叉销售后对交叉销售各不为空项的校验
    // by gzh 20101118
function MixComCheck()
{    
    if(fm.MixComFlag.checked == true)
    {
    	if(fm.Crs_SaleChnl.value == "" || fm.Crs_SaleChnl.value == null)
    	{
    		alert("选择交叉销售时，交叉销售渠道不能为空，请核查！");
    		fm.all('Crs_SaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.Crs_BussType.value == "" || fm.Crs_BussType.value == null)
    	{
    		alert("选择交叉销售时，交叉销售业务类型不能为空，请核查！");
    		fm.all('Crs_BussType').focus();
    		return false;
    	}
    	if(fm.GrpAgentCom.value == "" || fm.GrpAgentCom.value == null)
    	{
    		alert("选择交叉销售时，对方机构代码不能为空，请核查！");
    		fm.all('GrpAgentCom').focus();
    		return false;
    	}
    	/**
		 * if(fm.GrpAgentComName.value == "" || fm.GrpAgentComName.value ==
		 * null) { alert("选择交叉销售时，对方机构名称不能为空，请核查！");
		 * fm.all('GrpAgentComName').focus(); return false; }
		 */
    	if(fm.GrpAgentCode.value == "" || fm.GrpAgentCode.value == null)
    	{
    		alert("选择交叉销售时，对方业务员代码不能为空，请核查！");
    		fm.all('GrpAgentCode').focus();
    		return false;
    	}
    	if(fm.GrpAgentName.value == "" || fm.GrpAgentName.value == null)
    	{
    		alert("选择交叉销售时，对方业务员姓名不能为空，请核查！");
    		fm.all('GrpAgentName').focus();
    		return false;
    	}
    	if(fm.GrpAgentIDNo.value == "" || fm.GrpAgentIDNo.value == null)
    	{
    		alert("选择交叉销售时，对方业务员身份证号码不能为空，请核查！");
    		fm.all('GrpAgentIDNo').focus();
    		return false;
    	}
      	return true;
    }else{
    	return true;
    }
}    
    // --------------------
    
// 校验业务员代码 by zhangyang 2011-01-06
function CheckAgentCode()
{
	if(fm.all('SaleChnl').value == "06")
	{
		if(fm.all('AgentCom').value != "" && fm.all('AgentCom').value != null)
		{
			fm.all('AgentCom').value = "";
		}
		
		var strSql = "select trim(Branchtype) || trim(Branchtype2) from labranchgroup where agentgroup = (select agentgroup from laagent where agentcode = '" + fm.AgentCode.value + "') ";
		var arrResult = easyExecSql(strSql);
		var i = 0;
		if(arrResult != null)
		{
			for(i = 0; i < arrResult.length; i++)
			{
				var Branchtype = arrResult[i][0];
				if(Branchtype == "202")
				{
					alert("销售渠道与业务员类型不符，个销团渠道的业务员不能是中介业务员！");
					return false;
				}
			}
		}
	}
	return true;
}

// 校验标准团险集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。 by 赵庆涛 2016-06-28
function checkCrsBussSaleChnl()
{
	if(fm.Crs_SaleChnl.value != null && fm.Crs_SaleChnl.value != "")
	{
		var strSQL = "select 1 from lcgrpcont lgc where 1 = 1"  
			+ " and lgc.Crs_SaleChnl is not null " 
			+ " and lgc.Crs_BussType = '01' " 
			+ " and not (lgc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lgc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
			+ " and lgc.PrtNo = '" + fm.PrtNo.value + "' ";
		var arrResult = easyExecSql(strSQL);
        if (arrResult != null)
        {
        	alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
        	return false;
        }
	}
	return true;
}
// ------------------------------

// by gzh 20110413 团单被保人上载后，若有错误信息，可选中修改，修改功能只修改list表记录，不再调用算费功能
// 所以需在录入完毕及复核完毕时，校验是否所有上载被保人都计算过保费，避免修改数据后，未计算保费就录入完毕的问题

function checkAllCalPrem(){
	var strSQL = "select insuredname,sex,birthday,idtype,idno from lcinsuredlist where grpcontno = '"+mGrpContNo+"' and state = '0'";  
    var arrResult = easyExecSql(strSQL);
    if (arrResult != null)
    {
    	for(var i=0;i<arrResult.length;i++){
    		var strSQL1 = "select * from lcinsured where grpcontno = '+mGrpContNo+'"
    		            + " and name = '"+arrResult[i][0]+"' "
    		            + " and sex = '"+arrResult[i][1]+"' "
    		            + " and birthday = '"+arrResult[i][2]+"' "
    		            + "and idtype = '"+arrResult[i][3]+"' "
    		            + "and idno = '"+arrResult[i][4]+"'";
    		var arrResult1 = easyExecSql(strSQL1);
    		if(arrResult1 == null){
    			alert("该保单存在未计算保费的被保人，请先进行计算保费操作！");
    			return false;
    		}
    	}
    }
    return true;
}

// 录入完毕及复核完毕时，校验缴费频次与保险区间是否匹配
function checkPayIntvMatchInsuYear(){
	var strSQL = "select payintv,CValiDate,CInValiDate from lcgrpcont where prtno = '"+fm.PrtNo.value+"' ";
	var arrResult = easyExecSql(strSQL);
	if(arrResult == null){
		alert("校验缴费频次与保险区间时，获取数据失败！");
		return false;
	}
	var CValiDates = arrResult[0][1].split("-");
	var CInValiDates = arrResult[0][2].split("-");
	// alert(CValiDates);
	// alert(CInValiDates);
	var YearDif = parseInt(CInValiDates[0],10)-parseInt(CValiDates[0],10);// 年差
	// alert(parseInt(CInValiDates[1],10));
	// alert(parseInt(CValiDates[1],10));
	// alert(parseInt(CInValiDates[1],10)-parseInt(CValiDates[1],10));
	var MonthDif = YearDif*12 + parseInt(CInValiDates[1],10)-parseInt(CValiDates[1],10);// 月差
	
	var CValiDateValue = new Date(Date.parse(arrResult[0][1].replace(/-/g,"/")));
	var number;
	var interval;
	var name;
	if(arrResult[0][0] == "1"){// 月缴
		if(MonthDif ==0){
			number = 1;
		}else{
			number = MonthDif;
		}
		interval = "M";
		name = "月";
	}else if(arrResult[0][0] == "3"){// 季缴
		number = Math.ceil(MonthDif/3);
		interval = "q";
		name = "季";
	}else if(arrResult[0][0] == "6"){// 半年缴
		number = Math.ceil(MonthDif/6);
		interval = "hy";
		name = "半年";
	}
	if(arrResult[0][0] == "12"){// 半年缴
		number = Math.ceil(MonthDif/12);
		interval = "y";
		name = "年";
	}
	if(arrResult[0][0] != "0" && arrResult[0][0] !="-1" ){
		// alert("处理"+number+name);
		var newDate = DateAdd( interval,number,CValiDateValue);
		newDate   =   DateAdd( "d",-1,CValiDateValue); // 按照录入规则，如1月1
														// 到1月31，所以应减少一天
		// alert("算后时间："+newDate.toLocaleDateString());
		var CInValiDateValue = new Date(arrResult[0][2].replace(/-/g,"/"));
		// alert("失效时间："+CInValiDateValue.toLocaleDateString());
		
		if((Date.parse(newDate)-Date.parse(CInValiDateValue))!=0){
			if(!confirm("缴费频次为"+name+"缴，但保险区间不为整"+name+"，是否继续？")){
				// alert("点了取消");
				return false;
			}else{
				// alert("到了确定");
				return true;
			}
		}
	}
	// alert("通过检验");
	return true;
}
function test(){
	var   now   =   new   Date(Date.parse(fm.CValiDate.value.replace(/-/g,   "/")));
	// var now = new Date("2011/01/01 13:04:00");
	var   newDate
	// 加1年
	newDate   =   DateAdd( "y",1,now);  
	alert("加1年："+newDate.toLocaleDateString()+newDate.toLocaleTimeString()); 
	// 加2个月.
	newDate   =   DateAdd( "M",2,now);
	alert("加2月："+newDate.toLocaleDateString()+newDate.toLocaleTimeString());
	// 加3天.
	newDate   =   DateAdd( "d",3,now);
	alert("加3天："+newDate.toLocaleDateString()+newDate.toLocaleTimeString());
	
	// 加4小时
	newDate   =   DateAdd( "h",4,now);  
	alert("加4小时："+newDate.toLocaleDateString()+newDate.toLocaleTimeString()); 
	// 加5分钟.
	newDate   =   DateAdd( "m",5,now);
	alert("加5分钟："+newDate.toLocaleDateString()+newDate.toLocaleTimeString());
	// 加6秒.
	newDate   =   DateAdd( "s",6,now);
	alert("加6秒："+newDate.toLocaleDateString()+newDate.toLocaleTimeString());
	
	// 加7个季度
	newDate   =   DateAdd( "q",7,now);  
	alert("加7个季度："+newDate.toLocaleDateString()+newDate.toLocaleTimeString()); 
	// 加8个半年.
	newDate   =   DateAdd( "hy",8,now);
	alert("加8个半年："+newDate.toLocaleDateString()+newDate.toLocaleTimeString());
	// 加9周.
	newDate   =   DateAdd( "w",9,now);
	alert("加9周："+newDate.toLocaleDateString()+newDate.toLocaleTimeString());
}

// 校验中介机构是否是共保机构
function checkAgentCom() 
{
	var tSaleChnl = fm.SaleChnl.value;
	var tAgentCom = fm.AgentCom1.value;
	
	if(tSaleChnl != null && tSaleChnl != "" && tSaleChnl == "03")
	{
		if(tAgentCom != null && tAgentCom != "")
		{
			var strSQL = "select actype from lacom where agentcom = '" + tAgentCom + "' ";
			var arrResult = easyExecSql(strSQL);
    		if(arrResult != null && arrResult == "05")
    		{
    			alert("团险中介的中介机构不能为共保的机构!");
    			return false;
    		}
		}
	}
	return true;
}

function Crs_BussTypeHelp(){
	window.open("../sys/JtjxYwlyInfo.jsp");
}

function checkPayIntv()
{
	var tSql = "select 1 from lcgrpcont where prtno = '"+fm.PrtNo.value+"' and payintv = -1 and markettype not in (select code from ldcode where codetype = 'markettypeyd') ";
	var arr = easyExecSql(tSql);
	if(arr){
		alert("缴费频次与市场类型不匹配，请核查！");
		return false;
	}
	return true;
}

function checkFirstPayPlan()
{
	var tSql = "select 1 from lcgrpcont where prtno = '"+fm.PrtNo.value+"' and payintv = -1 ";
	var arr = easyExecSql(tSql);
	if(arr){
		var tStrSql = "select contplancode, prem from lcgrppayplan where ProposalGrpContNo = '"+fm.ProposalGrpContNo.value+"' and plancode = '1' order by contplancode " ;
		var arrPayPlan = easyExecSql(tStrSql);
		if(!arrPayPlan){
			alert("该单缴费频次为约定缴费，请录入约定缴费计划信息！");
			return false;
		}
		var tPolSql = "select contplancode,sum(prem) from lcpol where prtno = '"+fm.PrtNo.value+"' group by contplancode ";
		var arrPol = easyExecSql(tPolSql);
		if(!arrPol){
			alert("获取险种信息失败！");
			return false;
		}
		for(var i=0;i<arrPayPlan.length;i++){
			for(var j=0;j<arrPol.length;j++){
				if(arrPayPlan[i][0] == arrPol[j][0] && parseFloat(arrPayPlan[i][1]) != parseFloat(arrPol[j][1]))
				{
					alert("保障计划["+arrPayPlan[i][0]+"]的首期保费["+arrPol[j][1]+"]与约定缴费计划的首期保费["+arrPayPlan[i][1]+"]不符！");
					return false;
				}
			}
		}
	}
	return true;
}

// by gzh 增加约定缴费计划首期和总保费的校验,若缴费频次非约定缴费，则不可存在约定缴费计划
function checkPayPlan()
{
  	var tPayIntvSql = "select 1 from lcgrpcont where prtno = '"+fm.PrtNo.value+"' and payintv = -1 ";
	var tPayIntvArr = easyExecSql(tPayIntvSql);
	if(tPayIntvArr){// 约定缴费
		var tPremScopeSql = "select PremScope from lcgrpcont where prtno = '"+fm.PrtNo.value+"' ";
		var tPremScopeArr = easyExecSql(tPremScopeSql);
		var sumPrem = tPremScopeArr[0][0];
		if(sumPrem==null || sumPrem =="" || sumPrem =="0"){
			alert("缴费频次为约定缴费，必须录入保费合计！");
			fm.GrpContSumPrem.focus();
			return false;
		}
		if(!checkPayAndCin()){
			return false;
		}
		var tGetPolPremSql = "select sum(prem) from lcpol where prtno = '"+fm.PrtNo.value+"' ";
		var tGetPolPremArr = easyExecSql(tGetPolPremSql);
		if(!tGetPolPremArr){
			alert("查询首期保费失败！");
      		return false;
		}
		var tGetPlanPremSql = "select sum(prem) from lcgrppayplan where ProposalGrpContNo = '"+fm.ProposalGrpContNo.value+"' ";
		var tGetPlanPremArr = easyExecSql(tGetPlanPremSql);
		if(!tGetPlanPremArr){
			alert("查询约定缴费计划总保费失败！");
      		return false;
		}
		if(parseFloat(sumPrem) != parseFloat(tGetPlanPremArr[0][0])){
			alert("“缴费资料->保费合计”与约定缴费计划中录入的总金额不符,请核对!");
      		return false;
		}
		if(!confirm("该单缴费频次为约定缴费，首期保费为["+tGetPolPremArr[0][0]+"]元，约定计划总保费（包括首期）为["+tGetPlanPremArr[0][0]+"]元，是否继续？")){
		// alert("点了取消");
		return false;
		}
    }else{
    	var tPayPlanSql = "select 1 from lcgrppayplan where ProposalGrpContNo = '"+fm.ProposalGrpContNo.value+"'";
		var tPayPlanArr = easyExecSql(tPayPlanSql);
		if(tPayPlanArr){
			var tGetPanIntvSQL = "select payintv,(select codename from ldcode where codetype = 'grppayintv' and code = char(lgc.payintv)) from lcgrpcont lgc where lgc.prtno = '"+fm.PrtNo.value+"' ";
			var tGetPanIntvArr = easyExecSql(tGetPanIntvSQL);
			if(!tGetPanIntvArr){
				alert("获取缴费频次名称失败");
				return false;
			}
			alert("缴费方式为["+tGetPanIntvArr[0][1]+"],请先删除约定缴费计划！");
			return false;
		}
    }
    return true;
}

function checkPayAndCin()
{
	var tCinSql = "select cinvalidate from lcgrpcont where prtno = '"+fm.PrtNo.value+"'";
	var tCinArr = easyExecSql(tCinSql);
	if(!tCinArr){
		alert("获取保单终止日期失败！");
		return false;
	}
	var tSql = "select plancode,paytodate from lcgrppayplan where ProposalGrpContNo = '"+fm.ProposalGrpContNo.value+"' group by plancode,paytodate ";
	var arr = easyExecSql(tSql);
	if(!arr){
		alert("获取约定缴费时间失败！");
		return false;
	}
	for(var i=0;i<arr.length;i++){
		var zw = compareDate(arr[i][1],tCinArr[0][0]);
		if(zw==1){
			alert("第"+arr[i][0]+"期的约定缴费时间["+arr[i][1]+"]晚于保单终止日期["+tCinArr[0][0]+"]，请核查！");
			return false;
		}
	}
	return true;
}

// by gzh 20120517 增加对代理销售业务员的查询
function queryAgentSaleCode()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        fm.all('ManageCom').focus();
        return ;
    }
    
    if(fm.all('AgentCom').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom').focus();
        return ;
    }
    var tAgentCom = fm.all('AgentCom').value;
    var tAgentCom1 = fm.all('AgentCom1').value;
    if(fm.all('AgentSaleCode').value == "")
    {
        var strURL = "../sys/AgentSaleCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&AgentCom=" + tAgentCom
        var newWindow = window.open(strURL, "AgentSaleCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('AgentSaleCode').value != "")
    {
        var cAgentCode = fm.AgentSaleCode.value;  // 保单号码
        var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom').value + "'";
        var arrResult = easyExecSql(strSql);
        // alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentSaleName.value = arrResult[0][1];
            // alert("查询结果: 代理销售业务员代码:["+arrResult[0][0]+"]
			// ，代理销售业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value = "";
            alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
        }
    }
}

function queryAgentSaleCode2() {

  if(fm.all('AgentSaleCode').value != ""){
  	if(fm.all('AgentCom').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom1').focus();
        return ;
    }
    var cAgentCode = fm.AgentSaleCode.value;  // 保单号码
    var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom').value + "'";
    var arrResult = easyExecSql(strSql);
    // alert(arrResult);
    if (arrResult != null) {
      fm.AgentSaleName.value = arrResult[0][1];
      // alert("查询结果: 业务员代码:["+arrResult[0][0]+"]
		// 业务员名称为:["+arrResult[0][1]+"]");
    } else {
      fm.AgentGroup.value="";
      alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
    }
  }
}

function afterQuery5(arrResult){
  if(arrResult!=null) {
    fm.AgentSaleCode.value = arrResult[0][0];
    fm.AgentSaleName.value = arrResult[0][1];
  }
}

// 北分中介时，中介机构及代理销售业务员必须填写
function checkAgentComAndSaleCdoe(){
	var strSql = "select AgentCom,AgentSaleCode from lcgrpcont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%' and salechnl = '03'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCom = arrResult[0][0];
	    var tAgentSaleCode = arrResult[0][1];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCom == null || tAgentCom ==''){
	   		alert("中介公司代码必须录入，请核查！");
	   		return false;    	
	   	}
	   	if(tAgentSaleCode == null || tAgentSaleCode ==''){
	   		alert("代理销售业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	
	   	var tSqlCode = "select ValidStart,ValidEnd from LAQualification where agentcode = '"+tAgentSaleCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tValidStart = arrCodeResult[0][0];
	   		var tValidEnd = arrCodeResult[0][1];
	   		if(tValidStart == null || tValidStart == ''){
	   			alert("代理销售业务员资格证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tValidEnd == null || tValidEnd == ''){
	   			alert("代理销售业务员资格证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
	   			alert("代理销售业务员资格证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("代理销售业务员资格证信息不完整，请核查！");
	   		return false;
	   	}
	   	
	   	var tSqlCom = "select LicenseStartDate,LicenseEndDate,EndFlag from lacom where agentcom = '"+tAgentCom+"'";
	   	var arrComResult = easyExecSql(tSqlCom);
	   	if(arrComResult){
	   		var tLicenseStartDate = arrComResult[0][0];
	   		var tLicenseEndDate = arrComResult[0][1];
	   		var tEndFlag = arrComResult[0][2];
	   		if(tLicenseStartDate == null || tLicenseStartDate == ''){
	   			alert("中介公司许可证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tLicenseEndDate == null || tLicenseEndDate == ''){
	   			alert("中介公司许可证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == null || tEndFlag == ''){
	   			alert("中介机构合作终止状态为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == 'Y'){
	   			alert("中介机构合作终止状态为失效，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tLicenseEndDate)=='1'){
	   			alert("中介公司许可证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("中介公司许可证信息不完整，请核查！");
	   		return false;
	   	}
    }
    return true;
}
function initAgentSaleCode(){
	var tSqlCode = "select AgentSaleCode from LCGrpCont where prtno='" + fm.PrtNo.value + "'";
	var arrCodeResult = easyExecSql(tSqlCode);
	if(arrCodeResult){
		if(arrCodeResult[0][0] != null && arrCodeResult[0][0] != ""){
			var tSQL = "select agentcode,name from laagenttemp where agentcode = '"+arrCodeResult[0][0]+"'";
			var arrResult = easyExecSql(tSQL);
			if(arrResult){
				fm.AgentSaleCode.value = arrResult[0][0];
    			fm.AgentSaleName.value = arrResult[0][1];
			}
		}
	}
}
// by gzh 20120517 end
// by gzh 20120627 项目制保单归属
function ProjectCont(){
	var tSqlCode = "select 1 from LCGrpCont where prtno='" + fm.PrtNo.value + "'";
	var arrCodeResult = easyExecSql(tSqlCode);
	if(!arrCodeResult){
		alert("保单未保存，不允许进行保单归属！");
		return false;
	}
	showInfo = window.open("../project/ProjectContQYMain.jsp?PrtNo="+fm.PrtNo.value);
}
// by gzh 20120910
// 建工险590301保费计算方式为按被保险人人数计算时
// 1、保险期间不可以大于1年
// 2、必须实名投保
function check590301(){
	var tSql = " select 1 from LCContPlanDutyParam "
			 + " where ProposalGrpContNo = '"+fm.ProposalGrpContNo.value+"' "
			 + " and riskcode = '590301' "
			 + " and calfactor = 'StandbyFlag1' "
			 + " and calfactorvalue = '3' ";
	var arr = easyExecSql(tSql);
	if(arr){
		// 保险期间不可以大于1年
		var tSqlGrpCont = " select Cvalidate,Cinvalidate from LCGrpCont "
			 + " where ProposalGrpContNo = '"+fm.ProposalGrpContNo.value+"' ";
		var arrGrpCont = easyExecSql(tSqlGrpCont);
		if(arrGrpCont){
			var Cvalidate = new Date(Date.parse(arrGrpCont[0][0].replace(/-/g,"/")));
			var Cinvalidate = new Date(Date.parse(arrGrpCont[0][1].replace(/-/g,"/")));
			var tempCvalidate = DateAdd("y",1,Cvalidate);
			if((Date.parse(tempCvalidate)-Date.parse(Cinvalidate)) <=0){
				alert("承保产品新版建工险590301，保费计算方式为按被保人方式计算，保险期间应小于等于1年！");
				return false;
			}
		}else{
			alert("获取保单保险期间失败！");
			return false;
		}
		// 必须实名投保
		var tSqlCont = " select 1 from lccont where prtno = '"+fm.PrtNo.value+"' and poltype = '1'";
		var arrCont = easyExecSql(tSqlCont);
		if(arrCont){
			alert("承保产品新版建工险590301，保费计算方式为按被保人方式计算，必须实名承保！");
			return false;
		}
	}
	return true;
}
// by gzh 20121031
// 健享全球表定费率必须实名承保
function checkB162001(){
	var tSQL= " select 1 from LCContPlanDutyParam where ProposalGrpContNo='"
				+ fm.ProposalGrpContNo.value + "' and contplancode='11' and riskcode='162001' " 
            	+ " and calfactor = 'CalRule' and calfactorvalue = '0'" ;
    var tArr=easyExecSql(tSQL);
	if(tArr){
		// 必须实名投保
		var tSqlCont = " select 1 from lccont where prtno = '"+fm.PrtNo.value+"' and poltype = '1'";
		var arrCont = easyExecSql(tSqlCont);
		if(arrCont){
			alert("承保产品健享全球162001，保费计算方式为表定费率，必须实名承保！");
			return false;
		}
	}
	return true;
}
function initHandlerIDNo(){
	var tSql = "select HandlerIDNo from LCGrpCont where prtno='" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.HandlerIDNo.value = arrResult[0][0];
		}
	}
}
function initLegalPerson(){
	var tSql = "select TaxNo,LegalPersonName,LegalPersonIDNo,ClaimBankCode,ClaimAccName,ClaimBankAccNo,BusinessScope,IDStartDate,IDEndDate,IDType,codename('idtype',IDType),IDNo,IDLongEffFlag from LCGrpAppnt where prtno='" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.TaxNo.value = arrResult[0][0];
		}
		if(arrResult[0][1] != null && arrResult[0][1] != "" && arrResult[0][1] != "null"){
			fm.LegalPersonName.value = arrResult[0][1];
		}
		if(arrResult[0][2] != null && arrResult[0][2] != "" && arrResult[0][2] != "null"){
			fm.LegalPersonIDNo.value = arrResult[0][2];
		}
		if(arrResult[0][3] != null && arrResult[0][3] != "" && arrResult[0][3] != "null"){
			fm.ClaimBankCode.value = arrResult[0][3];
		}
		if(arrResult[0][4] != null && arrResult[0][4] != "" && arrResult[0][4] != "null"){
			fm.ClaimAccName.value = arrResult[0][4];
		}
		if(arrResult[0][5] != null && arrResult[0][5] != "" && arrResult[0][5] != "null"){
			fm.ClaimBankAccNo.value = arrResult[0][5];
		}
		if(arrResult[0][6] != null && arrResult[0][6] != "" && arrResult[0][6] != "null"){
			fm.BusinessScope.value = arrResult[0][6];
		}
		if(arrResult[0][7] != null && arrResult[0][7] != "" && arrResult[0][7] != "null"){
			fm.IDStartDate.value = arrResult[0][7];
		}
		if(arrResult[0][8] != null && arrResult[0][8] != "" && arrResult[0][8] != "null"){
			fm.IDEndDate.value = arrResult[0][8];
		}
		if(arrResult[0][9] != null && arrResult[0][9] != "" && arrResult[0][9] != "null"){
			fm.IDType.value = arrResult[0][9];
			fm.IDTypeName.value = arrResult[0][10];
		}
		if(arrResult[0][11] != null && arrResult[0][11] != "" && arrResult[0][11] != "null"){
			fm.IDNo.value = arrResult[0][11];
		}
		if(arrResult[0][12] != null && arrResult[0][12] != "" && arrResult[0][12] != "null" && arrResult[0][12]=="Y"){
			fm.IdNoValidity.checked = true;
			fm.IDLongEffFlag.value = arrResult[0][12];
		}
	}
}

function setIDLongEffFlag1(){

	   if(fm.IdNoValidity1.checked == true)
	    {
	        fm.LegalPersonIDLongFlag1.value = "Y";
	    }else{
	        fm.LegalPersonIDLongFlag1.value = "";
	    }
	    
	}
function setIDLongEffFlag2(){

	   if(fm.IdNoValidity2.checked == true)
	    {
	        fm.ShareholderIDLongFlag.value = "Y";
	    }else{
	        fm.ShareholderIDLongFlag.value = "";
	    }
	    
	}
function setIDLongEffFlag3(){

	   if(fm.IdNoValidity3.checked == true)
	    {
	        fm.ResponsibleIDLongFlag.value = "Y";
	    }else{
	        fm.ResponsibleIDLongFlag.value = "";
	    }
	    
	}

function checkFXQ(){
	var tSql = "select prem,peoples2 from LCGrpCont where prtno='" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		var tPrem = arrResult[0][0];
		var tPeoples2 = arrResult[0][1];
		var tAvgPrem = parseFloat(arrResult[0][0])/parseFloat(arrResult[0][1])
		if(tAvgPrem>200000){
			if(!confirm("该单存在单个被保险人保险费超过20万元，存在洗钱风险，请上传相关身份信息影印件，如已上传，请点击确定，如需要补充材料请点击取消！"))
			{
				return false;
			}
		}
	}else{
		alert("获取保单保费失败！");
		return false;
	}
	return true;
}

function checkPhone1()
{
	var tSql = "select substr(lgc.managecom,1,4),lgc.GrpName,lgad.Phone1 from LCGrpCont lgc "
			 + "inner join LCGrpAppnt lga on lga.prtno = lgc.prtno "
			 + "inner join LCGrpAddress lgad on lga.Customerno = lgad.Customerno and lga.addressno = lgad.addressno "
			 + "where lga.prtno = '"+fm.PrtNo.value+"' and lgad.Phone1 is not null and lgad.Phone1 != '' ";
	var arr = easyExecSql(tSql);
	if(arr){
		var tSQL1 = "select 1 from LCGrpCont lgc "
				 + "inner join LCGrpAppnt lga on lga.prtno = lgc.prtno "
				 + "inner join LCGrpAddress lgad on lga.Customerno = lgad.Customerno and lga.addressno = lgad.addressno "
				 + "where lgc.managecom like '"+arr[0][0]+"%' and lgc.appflag = '1' and lgc.stateflag in ('1','2') "
				 + "and lgc.GrpName != '"+arr[0][1]+"' and lgad.Phone1 = '"+arr[0][2]+"' ";
		var arr1 = easyExecSql(tSQL1);
		if(arr1){
			if(!confirm("该联系人电话下存在其它投保单位，请进一步核实客户信息的真实性。详细内容，可在菜单：承保处理-团体保单-新单查询中，点击【相同电话不同投保机构查询】按钮进行查询！是否继续？"))
			{
				return false;
			}
		}
	}
	return true;;
}
// 显示或者综合开拓
function isExtend()
{
    if(fm.ExtendFlag.checked == true)
    {
        fm.all('ExtendID').style.display = "";
        fm.all('AssistSaleChnl').value ="";
        fm.all('AssistSaleChnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value ="";
    }
    if(fm.ExtendFlag.checked == false)
    {
        fm.all('AssistSaleChnl').value ="";
        fm.all('AssistSaleChnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value ="";
        fm.all('ExtendID').style.display = "none";
    }
}
// 执行查询交叉销售业务员代码 date 20101029 by gzh
function queryAssistAgent()
{
    if(fm.all('AssistSaleChnl').value == "" || fm.all('AssistSaleChnl').value == null)
    {
    	alert("请先选择协助销售渠道！");
    	return false;
    }
    if(fm.all('AssistAgentCode').value == "" )
    {  
        var tAssistSaleChnl = (fm.AssistSaleChnl != null && fm.AssistSaleChnl != "undefined") ? fm.AssistSaleChnl.value : "";
        var strURL = "../sys/AssistAgentCommonQueryMain.jsp?AssistSaleChnl=" + tAssistSaleChnl;        
        // alert(strURL);
        var newWindow = window.open(strURL, "AssistAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('AssistAgentCode').value != "")
    {	
        var strGrpSql = "select agentcode,name from LAAgent where agentcode ='" + fm.all('AssistAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery6(arrResult);
            // alert("查询结果: 业务员代码:["+arrResult[0][0]+"]
			// 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.AssistAgentCode.value + "]的业务员不存在，请确认!");
            // fm.GrpAgentCode.value="";
        }
    }
}
function afterQuery6(arrResult){
  if(arrResult!=null) {
    fm.AssistAgentCode.value = arrResult[0][0];
    fm.AssistAgentName.value = arrResult[0][1];
  }
}
function ExtendCheck()
{    
    if(fm.ExtendFlag.checked == true)
    {
    	if(fm.AssistSaleChnl.value == "" || fm.AssistSaleChnl.value == null)
    	{
    		alert("选择综合开拓时，协助销售渠道不能为空，请核查！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.AssistAgentCode.value == "" || fm.AssistAgentCode.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员代码不能为空，请核查！");
    		fm.all('AssistAgentCode').focus();
    		return false;
    	}
    	if(fm.AssistAgentName.value == "" || fm.AssistAgentName.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员姓名不能为空，请核查！");
    		fm.all('AssistAgentName').focus();



    		return false;
    	}
    	var tSQL = "select code1,codename from ldcode1 where codetype = 'salechnl' and code = '"+fm.AssistSaleChnl.value+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("销售渠道与业务员代码不匹配！");
			return false;
		}
		var tSQL1 = "select 1 from laagent where agentcode = '"+fm.AssistAgentCode.value+"' and BranchType = '"+arrResult[0][0]+"' and BranchType2 = '"+arrResult[0][1]+"'";
		var arrResult1 = easyExecSql(tSQL1);
		if(!arrResult1){
			alert("销售渠道与业务员代码不匹配，请核查！");
			return false;
		}
      	return true;
    }else{
    	return true;
    }
}
function initExtend(){
	var tSql = "select AssistSalechnl,AssistAgentCode from LCExtend where prtno='" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.ExtendFlag.checked = true;
			fm.all('ExtendID').style.display = "";
			fm.AssistSaleChnl.value = arrResult[0][0];
			fm.AssistAgentCode.value = arrResult[0][1];
			var  tSql1 = "select name from LAAgent where agentcode ='" + arrResult[0][1] + "'";
			var arrResult1 = easyExecSql(tSql1);
			fm.AssistAgentName.value = arrResult1[0][0];
		}
	}
}

function checkLGrp(){
    var tSql = "select 1 from lcgrpcont where ContPrintType = '3' and prtno='" + fm.PrtNo.value + "'";
    var arrResult = easyExecSql(tSql);
    if(arrResult){
    	tSql = "select 1 from lcgrppol where prtno='" + fm.PrtNo.value + "' " 
    			+ " and not exists (select 1 from ldriskmanage where riskcode=lcgrppol.riskcode and checktype='lgrprisk' and state='0' " 
				+ " and startdate <= current date and (enddate is null or enddate > current date)) ";
    	arrResult = easyExecSql(tSql);
    	if(arrResult){
    		alert("大团单保单不支持该险种，请修改保单属性或险种代码！");
    		return false;
    	}
    	
    	tSql = "select 1 from lcgrpcont where prtno='" + fm.PrtNo.value + "' and exists (select 1 from LCGrpFee where grpcontno=lcgrpcont.grpcontno ) "
    			+ " union all " 
    			+ " select 1 from lcgrpcont where prtno='" + fm.PrtNo.value + "' and exists (select 1 from LCGrpInterest where grpcontno=lcgrpcont.grpcontno ) ";
    	arrResult = easyExecSql(tSql);
    	if(arrResult){
    		alert("大团单保单不支持帐户型、建工型、无名单保单，请修改保单属性或删除相关险种及定义的公共账户、建工意外要素！");
    		return false;
    	}
    }
	return true;
}

function checkBriefGrp(){
	var tContPrintTypeSQL = "select payintv from lcgrpcont where prtno = '"+fm.PrtNo.value+"' and ContPrintType = '4' ";
	var tContPrintTypeArr = easyExecSql(tContPrintTypeSQL);
	if(tContPrintTypeArr){
		if(tContPrintTypeArr[0][0] != "0"){
			alert("简易团单的缴费方式必须为趸交！");
			return false;
		}
	}
	return true;
}
function checkBriefGrpRisk(){
	var tFlagSQL = "select grpcontno from lcgrpcont where prtno = '"+fm.PrtNo.value+"' and ContPrintType = '4' ";
	var tFlagSQLArr = easyExecSql(tFlagSQL);
	if(tFlagSQLArr){
		var tRiskSQL = "select 1 from lcgrppol where prtno = '"+fm.PrtNo.value+"' ";
		var tRiskArr = easyExecSql(tRiskSQL);
		if(tRiskArr){
			var tRiskFlagSQL = "select 1 from lcgrppol lgp where lgp.prtno = '"+fm.PrtNo.value+"' "
							 + " and not exists (select 1 from ldcode where codetype = 'grpbriefrisk' and code = lgp.riskcode and codename = substr(lgp.managecom,1,4) ) ";
			var tRiskFlagArr = easyExecSql(tRiskFlagSQL);
			if(tRiskFlagArr){
				alert("该单为简易团单，简易团单不支持已添加的险种，请修改保单属性或险种代码！");
				return false;
			}
			var tContPlanSQL = "select distinct contplancode,riskcode from lccontplandutyparam where grpcontno = '"+tFlagSQLArr[0][0]+"' and contplancode != '11' ";
			var tContPlanArr = easyExecSql(tContPlanSQL);
			if(tContPlanArr){
				for(var i=0;i<tContPlanArr.length;i++){
					var tContPlanCode = tContPlanArr[i][0];
					var tRiskCode = tContPlanArr[i][1];
					var tInsuPlanCodeSQL = "select distinct calfactorvalue from lccontplandutyparam where grpcontno = '"+tFlagSQLArr[0][0]+"' and contplancode = '"+tContPlanCode+"' and riskcode = '"+tRiskCode+"' and calfactor like 'InsuPlanCode%' ";
					var tInsuPlanCodeArr = easyExecSql(tInsuPlanCodeSQL);
					if(!tInsuPlanCodeArr){
						alert("该单为简易团单，保障计划"+tContPlanCode+"中,险种["+tRiskCode+"]必须选择方案编码，请修改保障计划！");
						return false;
					}
					/*
					 * if(tInsuPlanCodeArr.length>1){
					 * alert("该单为简易团单，保障计划"+tContPlanCode+"中,险种["+tRiskCode+"]对应多个方案编码，请修改保障计划！");
					 * return false; } var tInsuTableSQL = "select distinct
					 * codealias from ldcode1 where codetype = 'grpbrieinsuplan'
					 * and code = '"+tInsuPlanCodeArr[0][0]+"' "; var
					 * tInsuTableArr = easyExecSql(tInsuTableSQL);
					 * if(!tInsuTableArr || tInsuTableArr.length>1){
					 * alert("该单为简易团单，保障计划"+tContPlanCode+"中,险种["+tRiskCode+"],获取方案编码["+tInsuPlanCodeArr[0][0]+"]对应的配置失败！");
					 * return false; } var tContPlanParamSQL = "select
					 * dutycode,calfactor,calfactorvalue from
					 * lccontplandutyparam where grpcontno =
					 * '"+tFlagSQLArr[0][0]+"' and contplancode =
					 * '"+tContPlanCode+"' and riskcode = '"+tRiskCode+"' and
					 * calfactor not like 'InsuPlanCode%' and calfactor !=
					 * 'CalRule' "; var tContPlanParamArr =
					 * easyExecSql(tContPlanParamSQL); if(!tContPlanParamArr){
					 * alert("该单为简易团单，保障计划"+tContPlanCode+"中,险种["+tRiskCode+"],获取保障计划要素信息失败！");
					 * return false; } var tInsuParamSQL = "select
					 * dutycode,calfactor,calfactorvalue,factorname from
					 * "+tInsuTableArr[0][0] ; var tInsuParamArr =
					 * easyExecSql(tInsuParamSQL); if(!tInsuParamArr){
					 * alert("该单为简易团单，获取方案["+tInsuPlanCodeArr[0][0]+"]要素信息失败！");
					 * return false; } for(var m=0;m<tContPlanParamArr.length;m++){
					 * var tFlag = "0"; for(var n=0;n<tInsuParamArr.length;n++){
					 * if(tContPlanParamArr[m][0] == tInsuParamArr[n][0] &&
					 * tContPlanParamArr[m][1] == tInsuParamArr[n][1] &&
					 * tContPlanParamArr[m][2] != tInsuParamArr[n][2]){
					 * alert("保障计划["+tContPlanCode+"],险种编码["+tRiskCode+"],责任编码["+tContPlanParamArr[m][0]+"]，要素["+tInsuParamArr[n][3]+"]的要素值["+tContPlanParamArr[m][2]+"]，与对应方案["+tInsuPlanCodeArr[0][0]+"]的要素值["+tInsuParamArr[n][2]+"]不一致！");
					 * return false; } } }
					 */
				}
			}else{
				alert("获取保障计划信息失败！");
				return false;
			}
		}else{
			alert("获取险种信息失败！");
			return false;
		}
		
		var tCalRuleSQL = "select distinct CalFactorValue from lccontplandutyparam where grpcontno = '"+tFlagSQLArr[0][0]+"' and CalFactor = 'CalRule' ";
		var tCalRuleArr = easyExecSql(tCalRuleSQL);
		if(tCalRuleArr){
			if(tCalRuleArr.length >1){
				alert("该单为简易团单，保费计算方式必须为[3-约定费率]！");
				return false;
			}
			if(tCalRuleArr[0][0] != '3'){
				alert("该单为简易团单，保费计算方式必须为[3-约定费率]！");
				return false;
			}
		}else{
			alert("获取保障计划算费要素失败！");
			return false;
		}
		
		var tInsuPlanSQL = "select distinct contplancode,CalFactorValue from lccontplandutyparam where grpcontno = '"+tFlagSQLArr[0][0]+"' and CalFactor like 'InsuPlanCode%' ";
		var tInsuPlanSArr = easyExecSql(tInsuPlanSQL);
		if(tInsuPlanSArr){
			for(var i=0;i<tInsuPlanSArr.length;i++){
				var tAllInsuRiskSQL = "select code1 from ldcode1 where codetype = 'grpbrieinsuplan' and code = '"+tInsuPlanSArr[i][1]+"' ";
				var tAllInsuRiskArr = easyExecSql(tAllInsuRiskSQL);
				if(!tAllInsuRiskArr){
					alert("简易团单，获取方案对应的险种信息失败！");
					return false;
				}
				var tAllRiskSQL = "select distinct riskcode from lccontplandutyparam where grpcontno = '"+tFlagSQLArr[0][0]+"' and contplancode = '"+tInsuPlanSArr[i][0]+"' ";
				var tAllRiskArr = easyExecSql(tAllRiskSQL);
				if(!tAllRiskArr){
					alert("简易团单，获取保障计划对应的险种信息失败！");
					return false;
				}
				for(var m=0;m<tAllInsuRiskArr.length;m++){
					var tFlag = "0";
					for(n=0;n<tAllRiskArr.length;n++){
						if(tAllInsuRiskArr[m][0] == tAllRiskArr[n][0]){
							tFlag = "1";
							break;
						}
					}
					if(tFlag != "1"){
						alert("简易团单，方案编码["+tInsuPlanSArr[i][1]+"]对应的险种["+tAllInsuRiskArr[m][0]+"]在保障计划["+tInsuPlanSArr[i][0]+"]中不存在，请核查！");
						return false;
					}
				}
			}
		}else{
			alert("该单为简易团单，必须选择方案编码，请修改保障计划！");
			return false;
		}
	}
	return true;
}

function checkOrgancomCode(){
	var tOrgancomCode = fm.OrgancomCode.value;
	var tGrpName = fm.GrpName.value;
	var tGrpNo = fm.GrpNo.value;
	if(tOrgancomCode != null && tOrgancomCode != ''){
		if(tGrpNo != null && tGrpNo != ''){
			var tSQL = "select OrgancomCode from ldgrp where customerno = '"+tGrpNo+"' ";
			var arrResult = easyExecSql(tSQL);
			if(arrResult){
				if(tOrgancomCode != arrResult[0][0]){
					alert("填写的组织机构代码'"+tOrgancomCode+"'与客户号'"+tGrpNo+"'对应的组织机构代码'"+arrResult[0][0]+"'不一致！");
					return false;
				}
			}else{
				alert("根据团体客户号没有查询到对应的客户信息！");
				return false;
			}
		}else{
			var tSQL = "select 1 from ldgrp where grpname = '"+tGrpName+"' and OrgancomCode = '"+tOrgancomCode+"' ";
			var arrResult = easyExecSql(tSQL);
			if(arrResult){
				alert("该投保机构已存在客户数据，请查询客户数据或填写对应的客户号码！");
				return false;
			}
		}
	}
	return true;
}

// function checkUnifiedSocialCreditNo(){
// var tUnifiedSocialCreditNo = fm.UnifiedSocialCreditNo.value;
// var b = /^[0-9a-zA-Z]*$/g;
// if(b.test(tUnifiedSocialCreditNo)){
// return true;
// }else {
// alert("统一社会信用代码只能是数字和英文字母组合！");
// return false;
// }
// }

function checkInsuredProperty(){
	var tInsuredProperty = fm.InsuredProperty.value;
	var tOrgancomCode = fm.OrgancomCode.value;
	var tUnifiedSocialCreditNo = fm.UnifiedSocialCreditNo.value;
	var tLinkMan1 = fm.LinkMan1.value;
	var tIDType = fm.IDType.value;
	var tIDNo = fm.IDNo.value;
	var tIDStartDate = fm.IDStartDate.value;
	var tIDEndDate = fm.IDEndDate.value;
	if(tInsuredProperty  == "" ){
		alert("投保人属性不能为空！");
		return false;
	}else if(tInsuredProperty == "1"){
		if(tOrgancomCode ==""&&tUnifiedSocialCreditNo ==　""){
			alert("投保人属性是法人,组织机构代码、统一社会信用代码至少录入其一！");
			return false;
		}
	}else if(tInsuredProperty == "2"){
		if(tLinkMan1 == ""||tIDType == ""||
		   tIDNo == ""||tIDStartDate == ""||
		   (tIDEndDate == "" && fm.IdNoValidity.checked == false)){
			alert("投保人属性是自然人，则“联系人姓名”栏中的联系人姓名、证件类型、联系人证件号码、证件生效日期、失效日期都必须录入。");
			return false;
		}
	}else{
		alert("投保人属性只能为1或2！");
		return false;
	}
	return true;
}

function checkCrs_SaleChnl(){
	var tCrs_BussType=fm.Crs_BussType.value;
	var tSaleChnl=fm.SaleChnl.value;
	if((tCrs_BussType=="01"||tCrs_BussType=="02")&&(tSaleChnl=="16"||tSaleChnl=="18")){
		alert("社保直销及社保综拓渠道出单若选择交叉销售，交叉销售业务类型不能为相互代理或联合展业！");
		return false;
	}
	return true;
}

function checkTaxpayer(){
	var tTaxpayerType = fm.TaxpayerType.value;
	var tTaxNo = fm.TaxNo.value;
	var tUnifiedSocialCreditNo = fm.UnifiedSocialCreditNo.value;
	var tOrgancomCode = fm.OrgancomCode.value;
	var tCustomerBankCode = fm.CustomerBankCode.value;
	var tCustomerBankAccNo = fm.CustomerBankAccNo.value;
	var tE_Mail1 = fm.E_Mail1.value;
	if(tTaxpayerType  == "" ){
		if(tCustomerBankCode !=　""
			||tCustomerBankAccNo !=　""){
			alert("纳税人类型为空时，客户开户银行、 客户银行账户不可录入！");
			return false;
		}
	}else if(tTaxpayerType == "1"){
		if((tTaxNo =="" && tUnifiedSocialCreditNo =="" && tOrgancomCode =="")
			||tCustomerBankCode ==　""
			||tCustomerBankAccNo ==　""
			||tE_Mail1 ==　""){
			alert("当纳税人类型为“一般纳税人”时，税务登记证号码、统一社会信用代码 、组织机构代码三项必录其一，客户开户银行、 客户银行账户、电子邮箱为必录项！");
			return false;
		}
	}else if(tTaxpayerType == "2"){
		if(tTaxNo =="" && tUnifiedSocialCreditNo =="" && tOrgancomCode == ""){
			alert("当纳税人类型为“小规模纳税人”时，税务登记证号码、统一社会信用代码 、组织机构代码三项必录其一！");
			return false;
		}
	}
	if(tTaxNo != ""){
		var a = /^[0-9a-zA-Z]*$/g;
		var tlen = tTaxNo.length;
		if(!a.test(tTaxNo)){
			alert("税务登记证号码只允许数字和字母！");
			return false;
		}
		if(tlen != 15 && tlen != 18 && tlen != 20
				){
			alert("税务登记证号码允许的位数为15、18和20位");
			return false;
		}
	}
	if(tCustomerBankCode != ""){
		var tSQL = "select 1 from ldbank where bankcode = '"+tCustomerBankCode+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("请选择或输入正确的客户开户银行！");
			return false;
		}
	}
	return true;
}

// by gzh 20140507 结余返还
function initBalance(){
	var tSqlCode = "select 1 from LCGrpCont where prtno='" + fm.PrtNo.value + "'";
	var arrCodeResult = easyExecSql(tSqlCode);
	if(!arrCodeResult){
		alert("保单未保存，不允许录入社保项目要素信息！");
		return false;
	}
	if(tLoadFlag == "16"){
		showInfo = window.open("../app/BalanceMain.jsp?PrtNo="+fm.PrtNo.value+"&LookFlag=1");
	}else{
		showInfo = window.open("../app/BalanceMain.jsp?PrtNo="+fm.PrtNo.value+"&LookFlag=0");
	}
}

function checkBigProject(){
	var tMarketType = fm.MarketType.value;
	var tBigProjectFlag = fm.BigProjectFlag.value;
	var tPrtNo = fm.PrtNo.value;
	var tSQL = "select 1 from lcbigprojectcont where prtno='"+tPrtNo+"'";
	var arr = easyExecSql(tSQL);
	if(tMarketType == "1" || tMarketType == "9"){
		if(tBigProjectFlag == "1"){
			if(!arr){
				alert("印刷号为："+tPrtNo+"的保单必须选择或录入一个大项目！");
				return false;
			}
		}
	}
	return true;
}

function checkPeople(){
	var tPrtNo = fm.PrtNo.value;
	var tSqlCount="select 1 from dual where  (select a.peoples2 - b.code from lcgrpcont a,ldcode b where prtno='"
				 +fm.PrtNo.value+"' and codetype ='checkpeople')<0";
	var arrCount = easyExecSql(tSqlCount);
	if(arrCount){
		alert("被保险人数应大于等于3！");
		return false;
	}
	return true;
}

function checkRiskZTFee(){
	var tSqlRisk = "select riskcode,grpcontno from LCGrpPol where PrtNo='" + fm.PrtNo.value + "'";
	var arr = easyExecSql(tSqlRisk);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	for(var i=0;i<arr.length;i++){
		var tRiskCode = arr[i][0];
		var tGrpContNo = arr[i][1];
		if(tRiskCode == "370301"){
			var tcheckSQL = "select 1 from LCRiskZTFee where grpcontno='"+ tGrpContNo +"'";
			var arrCheck = easyExecSql(tcheckSQL);
			if(!arrCheck){
				alert("对险种" + tRiskCode + "退保费率是必录项！");
				return false;
			}
		}
	}
	return true;
}

function checkGuaRate(){
	var tSqlRisk = "select riskcode from LCGrpPol where PrtNo='" + fm.PrtNo.value + "'";
	var arr = easyExecSql(tSqlRisk);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	var tRiskCode = arr[0][0];
	if(tRiskCode == "370301"){
		var tcheckSQL = "select 1 from LDPromiseRate where Prtno='"+ fm.PrtNo.value +"'";
		var arrCheck = easyExecSql(tcheckSQL);
		if(!arrCheck){
			alert("对险种" + tRiskCode + "保证利率是必录项！");
			return false;
		}
	}
	
	return true;
}

// 校验险种162501是否录入赔付顺序
function checkClaimNum(){
	var tSqlRisk = "select riskcode,grpcontno from LCGrpPol where PrtNo='" + fm.PrtNo.value + "'";
	var arr = easyExecSql(tSqlRisk);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	for(var i=0;i<arr.length;i++){
		var tRiskCode = arr[i][0];
		var tGrpContNo = arr[i][1];
		if(tRiskCode == "162501"){
			var tcheckSQL = "select 1 from LCGrpFee where grpcontno='"+ tGrpContNo +"' and ClaimNum is not null";
			var arrCheck = easyExecSql(tcheckSQL);
			if(!arrCheck){
				alert("对险种" + tRiskCode + "赔付顺序是必录项，请在定义公共账户页面录入！");
				return false;
			}
		}
	}
	return true;
}

function check250101(){
	var tSqlRisk = "select riskcode from LCGrpPol where PrtNo='" + fm.PrtNo.value + "'";
	var arr = easyExecSql(tSqlRisk);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	
	for(var i=0;i<arr.length;i++){
		var tRiskCode = arr[i][0];
		if(tRiskCode == "250101"){
			var tSqlRisk1 = "select 1 from LCGrpPol where PrtNo='" + fm.PrtNo.value + "' and riskcode='551501'";
			var arrCheck = easyExecSql(tSqlRisk1);
			if(!arrCheck){
				alert("险种250101必须与551501绑定销售！");
				return false;
			}
		}
	}
	return true;
}


function check162701(){
	var tSqlRisk = "select riskcode from LCGrpPol where PrtNo='" + fm.PrtNo.value + "'";
	var arr = easyExecSql(tSqlRisk);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	var tSql = "select cvalidate from lcgrpcont where PrtNo='" + fm.PrtNo.value + "'";
	var tarr = easyExecSql(tSql);
	if(!tarr){
		alert("获取保单数据失败！");
		return false;
	}
	var ttSql = "select (current date - 30 days) from dual ";
	var ttarr = easyExecSql(ttSql);
	for(var i=0;i<arr.length;i++){
		var tRiskCode = arr[i][0];
		if(tRiskCode == "162701"||tRiskCode == "162901"){			
			if(tarr[0][0]<ttarr[0][0]){
				alert("对于险种"+tRiskCode+"生效日不得早于录单日期30天以上！");
				return false;
			}
		}
	}
	return true;
}

function check163001(){
	var tSqlRisk = "select 1 from LCGrpPol where PrtNo='" + fm.PrtNo.value + "' and riskcode='163001' ";
	var arr = easyExecSql(tSqlRisk);
	if(arr){
		var tSql = "select replace(TotalFactor,'%',''),TotalFactor from LCGrpcontroad where PrtNo='" + fm.PrtNo.value + "'";
		var arr1 = easyExecSql(tSql);
		if(!arr1){
			alert("获取一带一路产品要素失败！");
			return false;
		}else{
			var tTotalFactor = arr1[0][0];
			var tTotal = arr1[0][1];
			//获取浮动比例的最大值和最小值
			var tSQL = "select code,codename from ldcode where codetype ='163001'";
			var arr = easyExecSql(tSQL);
			var min=parseInt(arr[0][0]);
			var max=parseInt(arr[0][1]);
			if(tTotalFactor < min ||tTotalFactor > max){
				alert("一带一路产品的浮动比例不得小于"+min+"%且不得大于"+max+"%，当前浮动比例为:"+tTotal);
				return false;
			}
		}
	}
	return true;
}

function check163002(){
	var tSqlRisk = "select 1 from LCGrpPol where PrtNo='" + fm.PrtNo.value + "' and riskcode='163002' ";
	var arr = easyExecSql(tSqlRisk);
	if(arr){
		var tSql = "select replace(TotalFactor,'%',''),TotalFactor from LCGrpcontroad where PrtNo='" + fm.PrtNo.value + "'";
		var arr1 = easyExecSql(tSql);
		if(!arr1){
			alert("获取一带一路产品要素失败！");
			return false;
		}else{
			var tTotalFactor = arr1[0][0];
			var tTotal = arr1[0][1];
			//获取浮动比例的最大值和最小值
			var tSQL = "select code,codename from ldcode where codetype ='163002'";
			var arr = easyExecSql(tSQL);
			var min=parseInt(arr[0][0]);
			var max=parseInt(arr[0][1]);
			if(tTotalFactor < min ||tTotalFactor > max){
				alert("一带一路产品的浮动比例不得小于"+min+"%且不得大于"+max+"%，当前浮动比例为:"+tTotal);
				return false;
			}
		}
	}
	return true;
}

//by yuchunjian  date 20180730   redmine 3884
function checkFXlevel(){
	var sql1 = "select GrpName from LCGrpCont where GrpContNo='"+fm.GrpContNo.value+"' ";
	var arrResult = easyExecSql(sql1);
	if(arrResult){
		var sqlblack = "select max(HighestEvaLevel) from fx_FXQVIEW where AOGName= '"+arrResult[0][0]+"' ";
		var fxarr = easyExecSql(sqlblack);
		   if(sqlblack){
			   if(fxarr[0][0]=="3"){
				   if(!confirm(arrResult[0][0] +"，风险等级评估为高风险客户，确认要完成录入？")){
					   return false;
				   }
			   }else if(fxarr[0][0]=="2"){
				   if(!confirm(arrResult[0][0] +"，风险等级评估为中风险客户，确认要完成录入？")){
					   return false;
				   }
			   }
		   }
	}
	return true;
}

function checkBalance(){
	var tSqlCode = "select markettype from LCGrpCont where prtno='" + fm.PrtNo.value + "'";
	var arrCodeResult = easyExecSql(tSqlCode);
	if(!arrCodeResult){
		alert("获取保单市场类型失败！");
		return false;
	}
	var tSQL = "select ProjectName,BalanceTermFlag from LCGrpContSub where prtno = '"+fm.PrtNo.value+"' ";
	var arrBalanceResult = easyExecSql(tSQL);
	if(arrCodeResult[0][0] != "1" && arrCodeResult[0][0] != "9"){
		if(!arrBalanceResult){
			alert("根据保单市场类型，需录入社保项目要素信息！");
			return false;
		}
		if(arrBalanceResult[0][0]==null || arrBalanceResult[0][0]==""
		 ||arrBalanceResult[0][1]==null || arrBalanceResult[0][1]==""){
			alert("根据保单市场类型，需录入社保项目要素信息！");
			return false;
		}
	}else{
		if(arrBalanceResult){
			alert("根据保单市场类型，不需录入社保项目要素信息！");
			return false;
		}
	}
	return true;
}
// by gzh 20140507 结余返还 end

function checkSaleChnlInfo(){
	var tSQL = "select managecom,agentcom,agentcode,salechnl from lcgrpcont where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单数据失败！");
		return false;
	}
	var tManageCom = arr[0][0];
	var tAgentCom = arr[0][1];
	var tAgentCode = arr[0][2];
	var tSaleChnl = arr[0][3];
	
	var tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
	var arrCode = easyExecSql(tSQLCode);
	if(!arrCode){
		alert("业务员与管理机构不匹配！");
		return false;
	}
	if(tSaleChnl == "02" || tSaleChnl == "03"){
		var agentCodeSql = " select 1 from LAAgent a where a.AgentCode = '"+ tAgentCode+ "'" 
	                 + " and a.BranchType = '2' and a.BranchType2 in ('01','02') "
	                 + " union all "
					 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					 + " and '03' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
	    var arrAgentCode = easyExecSql(agentCodeSql);
	    if(!arrAgentCode){
	    	alert("业务员和销售渠道不匹配！");
			return false;
	    }
	}else{
		var agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
	                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
                     + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
                     + " union all "
					 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					 + " and '10' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
	    var arrAgentCode = easyExecSql(agentCodeSql);
	    if(!arrAgentCode){
	    	alert("业务员和销售渠道不匹配！");
			return false;
	    }
	}
	if(tSaleChnl == "03" || tSaleChnl == "04" || tSaleChnl == "10" || tSaleChnl == "15" || tSaleChnl == "20" || tSaleChnl == "23") {
		var tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
		var arrCom = easyExecSql(tSQLCom);
		if(!arrCom){
			alert("中介机构与管理机构不匹配！");
			return false;
		}
		var tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
		var arrComCode = easyExecSql(tSQLComCode);
		if(!arrComCode){
			alert("业务员与中介机构不匹配！");
			return false;
		}
	}
	return true;
}
function checkMarkettypeSalechnl(){
	var tSQL = "select salechnl,markettype from lcgrpcont where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单数据失败！");
		return false;
	}
	var tSaleChnl = arr[0][0];
	var tMarketType = arr[0][1];
	var tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '"+tMarketType+"' and code1 = '"+tSaleChnl+"' ";
	var arrCheck = easyExecSql(tcheckSQL);
	if(!arrCheck){
		alert("市场类型和销售渠道不匹配，请核查！");
		return false;
	}
	return true;
}
function checkRiskSalechnl(){
	var tSQL = "select salechnl,riskcode from lcgrppol where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	for(var i=0;i<arr.length;i++){
		var tSaleChnl = arr[i][0];
		var tRiskCode = arr[i][1];
		var tcheckSQL = "select codename from ldcode where codetype = 'unitesalechnlrisk' and code = '"+tRiskCode+"' ";
		var arrCheck = easyExecSql(tcheckSQL);
		if(!arrCheck){
			if(tSaleChnl == "16"){
				alert("险种"+tRiskCode+"为非社保险种，与销售渠道"+tSaleChnl+"不匹配！");
				return false;
			}
		}else{
			if(arrCheck[0][0] != "all" && arrCheck[0][0] != tSaleChnl){
				alert("险种"+tRiskCode+"与销售渠道"+tSaleChnl+"不匹配！");
				return false;
			}
		}
		// add by zjd 特需险校验赔付顺序为必录
		var triskclaimnumsql=" select riskcode from lmriskapp where risktype3='7' and riskcode='"+tRiskCode+"'  ";
	    var arrclaim = easyExecSql(triskclaimnumsql);
	    if(arrclaim){
	    	var tlcgrpfeesql=" select 1 from lcgrpfee lgf,lcgrppol lgp  where lgf.grppolno=lgp.grppolno and lgp.prtno='"+fm.PrtNo.value+"' and lgp.riskcode='"+tRiskCode+"' and ClaimNum is not null ";
	    	var arrclaimnum = easyExecSql(tlcgrpfeesql);
	    	if(!arrclaimnum){
	    		alert("险种"+tRiskCode+"为特需险，赔付顺序必录！\n 请到账户定义界面定义管理下录入赔付顺序！");
	    		return false;
	    	
	    	}
	    }
	    // add by zjd 补充工伤团体失能损失保险B款 伤残等级给付比例
	    var tgrpSQL = "select grpcontno from lcgrpcont where prtno = '"+fm.PrtNo.value+"'";
		var grparr = easyExecSql(tgrpSQL);
	    if(tRiskCode=="460301"){
	    	var tgraderatesql=" Select Distinct a.contplancode,a.Dutycode From Lccontplandutyparam a Where a.riskcode='460301' and grpcontno ='"+grparr[0][0]+"' and ContPlanCode not in ('00','11') ";
	    	var tgraderate = easyExecSql(tgraderatesql);
	    	if(tgraderate){
	    		for(var j=0;j<tgraderate.length;j++){
	    			var tratechksql=" select 1 from LCContPlanDutyDefGrade where grpcontno='"+grparr[0][0]+"' and contplancode='"+tgraderate[0][0]+"' and dutycode='"+tgraderate[0][1]+"' and GetRate > 0 ";
	    			var tratechk = easyExecSql(tratechksql);
	    			if(!tratechk){
	    				alert("保障计划"+tgraderate[0][0]+"下责任"+tgraderate[0][1]+" 未录入伤残等级对应的给付比例。\n 请到保障计划下的制定伤残等级给付比例中录入给付比例！");
	    				return false;
	    			}
	    		}
	    	}
	    }
	}
	return true;
}
function checkSalechnlAgentCom(){
	if(fm.all('SaleChnl').value=="03" || fm.all('SaleChnl').value=="04" || fm.all('SaleChnl').value=="10" || fm.all('SaleChnl').value=="15" || fm.all('SaleChnl').value=="20" || fm.all('SaleChnl').value=="23") {
		if(fm.all('AgentCom').value==null || fm.all('AgentCom').value=="" || fm.all('AgentCom').value=="null"){
			alert("销售渠道为中介，请录入中介公司代码！");
    		return false;
		}
	}
    return true;	
}


// 双击对方业务员框要显示的页面
function otherSalesInfo(){
	window.open("../sys/MixedSalesAgentMain.jsp?ManageCom="+fm.ManageCom.value);
}
// 对方业务员页面查询完“返回”按钮，接收数据
function afterQueryMIX(arrResult){
	if(arrResult!=null){
		var arr = arrResult.split("#");
		fm.GrpAgentCom.value = arr[0]; // 对方机构代码
		// fm.GrpAgentComName.value = arr[1]; //对方机构名称
		fm.GrpAgentCode.value = arr[2]; // 对方业务员代码
		fm.GrpAgentName.value = arr[3]; // 对方业务员姓名
		fm.GrpAgentIDNo.value = arr[4]; // 对方业务员身份证
		
	}else{
		alert("返回过程出现异常，请重新操作");
	}	
}
// 汇交件校验险种与团单属性类型
function checkRiskContPrintType(){
	var tSQL = "select salechnl,riskcode from lcgrppol where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	var tContPrintType = "";
	var tContPrintTypeSQL = "select ContPrintType,ManageCom from lcgrpcont where prtno = '"+fm.PrtNo.value+"' ";
	var tContPrintTypeArr = easyExecSql(tContPrintTypeSQL);
	if(!tContPrintTypeArr){
		alert("获取保单数据失败！");
		return false;
	}else{
		tContPrintType = tContPrintTypeArr[0][0];
	}
	for(var i=0;i<arr.length;i++){
		var tSaleChnl = arr[i][0];
		var tRiskCode = arr[i][1];
		var grpBriefSql = "select code,codename,'Risk',comcode from ldcode where codetype = 'grphjrisk' and code ='" + tRiskCode +"' ";
		var grpBriefArr = easyExecSql(grpBriefSql);
		if(tContPrintType == "5"){
			if(!grpBriefArr){
				alert("汇交件不支持险种"+tRiskCode+"，请修改保单属性或险种代码");
				return false;
			}
		}
		if(tContPrintType!="5"){
			if(grpBriefArr){
				if(grpBriefArr[0][3] != "3"){
					alert("非汇交件不支持险种"+tRiskCode+"，请修改保单属性或险种代码");
					return false;
				}
				
			}
		}
	}
	return true;
}

function setIDLongEffFlag(){

   if(fm.IdNoValidity.checked == true)
    {
        fm.IDLongEffFlag.value = "Y";
    }else{
        fm.IDLongEffFlag.value = "";
    }
    
}

function ChangExtrateRate()
{
	if(!checkRiskCode()){
		return false;
	}
	   delGrpVar();
	   addGrpVar(); 
	var cGrpContNo = fm.all("GrpContNo").value;
		 if (LoadFlag=="99")
	{
	    parent.fraInterface.window.location="../app/ChangeExtractRate.jsp?GrpContNo=" + cGrpContNo+ "&ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype;
	    return;
	}
	parent.fraInterface.window.location="../app/ChangeExtractRate.jsp?GrpContNo=" + cGrpContNo+ "&ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype;
}

function checkRiskCode(){
	var tSQL = "select riskcode from lcgrppol where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	for(var i=0;i<arr.length;i++){
		var tRiskCode = arr[i][0];
		if(tRiskCode !== "370301"){
			alert("不支持险种"+tRiskCode+"录入退保费率!");
			return false;
		}else{
			return true;
		}
	}
}

function insertLCGrpContLoad(){
	var tPrtNo = fm.PrtNo.value;
	var tsql="select 1 from LCGrppol where prtno='"+tPrtNo+"' and riskcode in ('163001','163002')";
	var tarr = easyExecSql(tsql);
	if(!tarr){
		alert("一带一路产品要素仅支持一带一路产品！");
		return false;
	}
	
	window.open("../app/InsertLCGrpContRoadMain.jsp?LoadFlag="+LoadFlag+"&PrtNo="+tPrtNo);
}


// 商团险大项目录入
function bigProjectInput(){
	var tPrtNo = fm.PrtNo.value;
	// window.open("../cz/BPInput.jsp?LoadFlag="+LoadFlag);
	window.open("../bigproject/BPMain.jsp?LoadFlag="+LoadFlag+"&PrtNo="+tPrtNo);
}
// 商团险大项目查询
function bigProjectQuery(){
	var tPrtNo = fm.PrtNo.value;
	window.open("../bigproject/BPMain.jsp?LoadFlag="+LoadFlag+"&PrtNo="+tPrtNo);
}

//省，市，县详细地址填完输入框失去焦点的时候自动在上面的“投保人地址（联系地址）”填充数据
function FillAddress(){
	var province = fm.Province.value;
	var city = fm.City.value;
	var county = fm.County.value;
	var detailAddress = fm.DetailAddress.value;
	if (fm.CityID.value == "000000") {
		fm.GrpAddress.value = province + detailAddress;
	}else if(fm.CountyID.value == "000000") {
		fm.GrpAddress.value = province + city + detailAddress;
	}else{
		fm.GrpAddress.value = province + city + county + detailAddress;
	}
}

//保存时校验联系人地址
function CheckAddress(){
	//校验判断联系人地址是否为空
	if (fm.Province.value=="" || fm.City.value=="" || fm.County.value=="" || fm.DetailAddress.value=="") {
		alert("团体基本资料中的省，市，县和详细地址不能为空，请核实！");
		return false;
	}
	
	//校验省和市是否与匹配
	var provinceID = fm.ProvinceID.value;
	var cityID = fm.CityID.value;
	var countyID = fm.CountyID.value;
	var strSql = "select code from ldcode1 where codetype='province1' and code = '" + provinceID + "'";
	var strSql2 = "select code1 from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
	var strSql3 = "select code from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
	var strSql4 = "select code1 from ldcode1 where codetype='county1' and code = '" + countyID  + "'";
	var arrResult = easyExecSql(strSql);
	var arrResult2 = easyExecSql(strSql2);
	var arrResult3 = easyExecSql(strSql3);
	var arrResult4 = easyExecSql(strSql4);
	
	if (provinceID=="710000" || provinceID=="810000" || provinceID=="820000"||provinceID=="990000") {
		if(cityID != "000000"){
 			alert("省和市不匹配，请核实！");
 			return false;
		}
		if(countyID != "000000"){
			alert("市和县不匹配，请核实！");
 			return false;
		}

	}else if ((provinceID=="620000"&&cityID=="620200") || (provinceID=="440000"&&(cityID=="442000"||cityID=="441900")) ) {
		if(countyID != "000000"){
			alert("市和县不匹配，请核实！");
 			return false;
		}
	}else{
 		if (arrResult[0][0] != arrResult2[0][0]) {
 			alert("省和市不匹配，请核实！");
 			return false;
 		}
 		if (arrResult3[0][0] != arrResult4[0][0]) {
 			alert("市和县不匹配，请核实！");
 			return false;
 		}
	}
	return true;
	
}
//投保人属性为自然人的保单支持银行转账缴费方式，投保人属性为法人的保单选择银行转账缴费方式时，在保存和修改时阻断提示
function checkBank(){
	var a = fm.InsuredProperty.value; //投保人属性 1 法人 2自然人
	if (fm.PayMode.value == "4") {
		fm.AccName.value = fm.GrpName.value;
	}
	if (a == 1 && fm.PayMode.value == "4") {
		alert("投保人属性为法人时，不支持银行转账缴费方式！");
		return false;
	}else if ((a == 2) && (fm.AccName.value == "" || fm.BankCode.value == "" || fm.BankAccNo.value == "") && fm.PayMode.value == "4") {
		alert("投保人属性为自然人，且缴费方式为银行转账时，开户银行、账号任何一项不能为空！");
		return false;
	}
	
	return true;
}

//当选择银行转账缴费方式时,户名自动赋值为投保人名称（汇交人名称）
function fullAccName(){
	if (fm.PayMode.value == "4") {
		fm.AccName.value = fm.GrpName.value;
	}else{
		fm.AccName.value = "";
		fm.BankCode.value = "";
		fm.BankCodeName.value = "";
		fm.BankAccNo.value = "";
	}
}

function initBankGrid(){
	fm.all("BankCode").style.display = "none";
	fm.all("BankCodeName").style.display = "none";
	fm.all("BankAccNo").style.display = "none";
	fm.all("AccName").style.display = "none";
}

//投保单填写日期(HandlerDate)[不可为空] <= 业务员填写日期(AgentDate)[可为空] <= 接收日期(ReceiveDate)[不可为空] <= current date 
function CheckDate(){
	var HandlerDate = fm.HandlerDate.value;
	var AgentDate = fm.AgentDate.value;
	var ReceiveDate = fm.ReceiveDate.value;
	var tCurDate = getCurrentDate();
	//三者都不为空
	if (HandlerDate != "" && AgentDate != "" && ReceiveDate != "") {
//		if (ReceiveDate > tCurDate) {
//			alert("接收日期应早于或等于当天!");
//			return false;
//		}
		if (dateDiff(tCurDate, ReceiveDate, "D") > 0) {
			alert("接收日期应早于或等于当天!");
			return false;
		}
		if (HandlerDate > AgentDate || HandlerDate > ReceiveDate) {
			alert("投保单填写日期应该早于或等于业务员填写日期和接收日期!");
			return false;
		}
		if (AgentDate > ReceiveDate) {
			alert("业务员填写日期应该早于或等于接收日期!");
			return false;
		}
	}
	//业务员填写日期为空
	if (HandlerDate != "" && AgentDate == "" && ReceiveDate != "") {
//		if (ReceiveDate > tCurDate) {
//			alert("接收日期应早于或等于当天!");
//			return false;
//		}
		if (dateDiff(tCurDate, ReceiveDate, "D") > 0) {
			alert("接收日期应早于或等于当天!");
			return false;
		}
		if (HandlerDate > ReceiveDate) {
			alert("业务员填写日期为空时，投保单填写日期应该早于或等于接收日期!");
			return false;
		}
	}
	return true;
}

//团体基本资料中联系人姓名、证件类型、联系人证件号码、证件生效日期、失效日期为必录项,当证件失效日期为长期有效时，此时失效日期为非必录项。
function CheckLinkMan() {
	var LinkMan1 = fm.LinkMan1.value;
	var IDType = fm.IDType.value;
	var IDNo = fm.IDNo.value;
	var IDStartDate = fm.IDStartDate.value;
	var IDEndDate = fm.IDEndDate.value;
	//不选择长期有效时都是必录项
	if (fm.IdNoValidity.checked) {
		if (LinkMan1=="" || IDType=="" || IDNo=="" || IDStartDate=="" ) {
			alert("当证件失效日期为长期有效时,团体基本资料中联系人证件失效日期可为空,但联系人姓名、证件类型、联系人证件号码、证件生效日期都不可为空！");
			return false;
		}
	}else{
		if (LinkMan1=="" || IDType=="" || IDNo=="" || IDStartDate=="" || IDEndDate=="") {
			alert("当证件失效日期为非长期有效时，团体基本资料中联系人姓名、证件类型、联系人证件号码、证件生效日期、证件失效日期都不可为空！");
			return false;
		}
	}
	return true;
}

function initshowCrs_BussTypeName(){
	var tsql = "select codename('crs_busstype',Crs_BussType) from lcgrpcont where 1=1"
			 + " and PrtNo = '" + fm.PrtNo.value + "' ";
	var tarrResult = easyExecSql(tsql);
	if (tarrResult != null){
		fm.Crs_BussTypeName.value = tarrResult[0][0];
	}
}

function checkPhone(pho){
	var str = "";
	if(pho!=null && pho!=""){
		if(pho.charAt(0)=="1"){
			str=CheckPhone(pho);
		}else{
			str=CheckFixPhone(pho);
		}
		if(str!=""){
			str="联系电话若为手机号，则必须为11位数字；若为固定电话，则仅允许包含数字、括号和“-”！";
			alert(str);
			return false;
		}else{
			return true;
		}
	}
	return true;
}

//校验黑名单
function checkBlackName(){
	//黑名单校验投保人
	var sql1 = "select name from lcgrpappnt where prtno='"+fm.PrtNo.value+"' ";
	var arrResult = easyExecSql(sql1);
	if(arrResult){
		var sqlblack = "select 1 from lcblacklist where name='"+arrResult[0][0]+"'";
		var arrResult1 = easyExecSql(sqlblack);
		if(arrResult1){
			if(!confirm("该保单投保人："+arrResult[0][0]+"，存在于黑名单中，确认要复核通过吗？")){
				return false;
			}
		}
	}
	
	//黑名单校验被保人
	var sql2 = "select name,idtype,idno from lcinsured where prtno='"+fm.PrtNo.value+"'  ";
	var arrRes = easyExecSql(sql2);
	var tInsuNames = "";
	if(arrRes){
		for(var i = 0; i < arrRes.length; i++){
			var sqlblack2  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrRes[i][0]+"' and idnotype='"+arrRes[i][1]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrRes[i][0]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrRes[i][0]+"' and type='0' ";
			var arrRes1 = easyExecSql(sqlblack2);
			if(arrRes1){
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ " , "+arrRes[i][0];
				}else{
					tInsuNames = tInsuNames + arrRes[i][0];
				}
			}
		}
	}
	if(tInsuNames != ""){
		if (!confirm("该保单被保人："+tInsuNames+",存在于黑名单中，确认要复核通过吗？")){
			return false;
		}
	}
	return true;
}


//职业类别
function getdetailwork()
{
var strSql = "select OccupationType from LDOccupation where OccupationCode='" + fm.AppntOccupationCode.value+"'";
var arrResult = easyExecSql(strSql);
if (arrResult != null) {
    fm.AppntOccupationType.value = arrResult[0][0];

  }
else{  
   fm.AppntOccupationType.value = '';
}
}


//受益所有人最多只能录入5个
function ChkBeneficiaryCount()
{
	if(BeneficiaryDetailGrid.mulLineCount>=5)
	{
//		alert("受益所有人信息最多只能录入5条");
//		BeneficiaryDetailGrid.delBlankLine();
		BeneficiaryDetailGrid.lock();
	}else{
		BeneficiaryDetailGrid.unLock();
	}
}


//如果投保人属性为自然人校验性别与身份证号是否匹配
//如果投保人属性为法人，校验股东信息，法人信息，负责人信息，若其他信息为空则姓名不能为空
function chkInsuredProp(){
	var insuredProp=fm.InsuredProperty.value;
	var strReturn="";
	if(insuredProp=="1"){
		//控股股东/实际控制人信息
		if(fm.ShareholderName.value=="" && fm.ShareholderIDType.value=="" && fm.ShareholderIDNo.value=="" && fm.ShareholderIDStart.value=="" && (fm.ShareholderIDEnd.value=="" || fm.ShareholderIDLongFlag.value == "")){
		}else{
			if(fm.ShareholderName.value=="" || trim(fm.ShareholderName.value)==""){
				strReturn = "控股股东/实际控制人姓名不能为空";
				return strReturn;
			}
			if(fm.ShareholderIDType.value=="" || trim(fm.ShareholderIDType.value)==""){
				strReturn = "控股股东/实际控制人证件类型不能为空";
				return strReturn;
			}
			if(fm.ShareholderIDNo.value==""|| trim(fm.ShareholderIDNo.value)==""){
				strReturn = "控股股东/实际控制人证件号码不能为空";
				return strReturn;
			}else{
				if(fm.ShareholderIDType.value=='0'){
					var tIdType=fm.ShareholderIDType.value;
					var tIdNo=fm.ShareholderIDNo.value;
					var strChkIdNo=checkIdNo(tIdType,tIdNo,"","");
					if(strChkIdNo != ""){
						strReturn = "控股股东/实际控制人身份证号不符合规则";
						return strReturn;
				 	}
				}else if(fm.ShareholderIDType.value!='4'){
					strReturn = "控股股东/实际控制人证件类型不符合规则";
					return strReturn;
				}
			}
			if(fm.ShareholderIDStart.value=="" || trim(fm.ShareholderIDStart.value)==""){
				strReturn = "控股股东/实际控制人证件生效日不能为空";
				return strReturn;
			}else{
				if(!isDate(fm.ShareholderIDStart.value)){
					strReturn = "控股股东/实际控制人证件生效日期不是有效的日期格式(YYYY-MM-DD)";
					return strReturn;
				}
			}
			if((fm.ShareholderIDEnd.value=="" && fm.ShareholderIDLongFlag.value == "")||(trim(fm.ShareholderIDEnd.value)=="" && fm.ShareholderIDLongFlag.value == "")){
				strReturn = "控股股东/实际控制人证件失效日不能为空";
				return strReturn;
			}else{
				if(fm.ShareholderIDEnd.value!="" && !isDate(fm.ShareholderIDEnd.value)){
					strReturn = "控股股东/实际控制人证件失效日期不是有效的日期格式(YYYY-MM-DD)";
					return strReturn;
				}
				if(fm.ShareholderIDLongFlag.value==""){
					var res = compareDate(fm.ShareholderIDStart.value,fm.ShareholderIDEnd.value);
					if(res!=2){
						strReturn = "控股股东/实际控制人证件生效日期要早于证件失效日期";
						return strReturn;
					}
				}
			}
		}
		//法人信息
		if(fm.LegalPersonName1.value=="" && fm.LegalPersonIDType1.value=="" && fm.LegalPersonIDNo1.value=="" && fm.LegalPersonIDStart1.value=="" && (fm.LegalPersonIDEnd1.value=="" || fm.LegalPersonIDLongFlag1.value == "")){
		}else{
			if(fm.LegalPersonName1.value=="" || trim(fm.LegalPersonName1.value)==""){
				strReturn = "法人姓名不能为空";
				return strReturn;
			}
			if(fm.LegalPersonIDType1.value=="" || trim(fm.LegalPersonIDType1.value)==""){
				strReturn = "法人证件类型不能为空";
				return strReturn;
			}
			if(fm.LegalPersonIDNo1.value=="" || trim(fm.LegalPersonIDNo1.value)==""){
				strReturn = "法人证件号码不能为空";
				return strReturn;
			}else{
				if(fm.LegalPersonIDType1.value=='0'){
					var tIdType=fm.LegalPersonIDType1.value;
					var tIdNo=fm.LegalPersonIDNo1.value;
					var strChkIdNo=checkIdNo(tIdType,tIdNo,"","");
					if(strChkIdNo != ""){
						strReturn = "法人身份证号不符合规则";
						return strReturn;
				 	}
				}else if(fm.LegalPersonIDType1.value!='4'){
					strReturn = "法人证件类型不符合规则";
					return strReturn;
				}
			}
			if(fm.LegalPersonIDStart1.value=="" || trim(fm.LegalPersonIDStart1.value)==""){
				strReturn = "法人证件生效日不能为空";
				return strReturn;
			}else{
				if(!isDate(fm.LegalPersonIDStart1.value)){
					strReturn = "法人证件生效日期不是有效的日期格式(YYYY-MM-DD)";
					return strReturn;
				}
			}
			if((fm.LegalPersonIDEnd1.value=="" && fm.LegalPersonIDLongFlag1.value == "")||(trim(fm.LegalPersonIDEnd1.value)=="" && fm.LegalPersonIDLongFlag1.value == "")){
				strReturn = "法人证件失效日不能为空";
				return strReturn;
			}else{
				if(fm.LegalPersonIDEnd1.value!="" && !isDate(fm.LegalPersonIDEnd1.value)){
					strReturn = "法人证件失效日期不是有效的日期格式(YYYY-MM-DD)";
					return strReturn;
				}
				if(fm.LegalPersonIDLongFlag1.value==""){
					var res = compareDate(fm.LegalPersonIDStart1.value,fm.LegalPersonIDEnd1.value);
					if(res!=2){
						strReturn = "法人证件生效日期要早于证件失效日期";
						return strReturn;
					}
				}
			}
		}
		//负责人信息
		if(fm.ResponsibleName.value=="" && fm.ResponsibleIDType.value=="" && fm.ResponsibleIDNo.value=="" && fm.ResponsibleIDStart.value=="" && (fm.ResponsibleIDEnd.value=="" || fm.ResponsibleIDLongFlag.value == "")){
		}else{
			if(fm.ResponsibleName.value=="" || trim(fm.ResponsibleName.value)==""){
				strReturn = "负责人姓名不能为空";
				return strReturn;
			}
			if(fm.ResponsibleIDType.value=="" || trim(fm.ResponsibleIDType.value)==""){
				strReturn = "负责人证件类型不能为空";
				return strReturn;
			}
			if(fm.ResponsibleIDNo.value=="" || trim(fm.ResponsibleIDNo.value)==""){
				strReturn = "负责人证件号码不能为空";
				return strReturn;
			}else{
				if(fm.ResponsibleIDType.value=='0'){
					var tIdType=fm.ResponsibleIDType.value;
					var tIdNo=fm.ResponsibleIDNo.value;
					var strChkIdNo=checkIdNo(tIdType,tIdNo,"","");
					if(strChkIdNo != ""){
						strReturn = "负责人身份证号不符合规则";
						return strReturn;
				 	}
				}else if(fm.ResponsibleIDType.value!='4'){
					strReturn = "负责人证件类型不符合规则";
					return strReturn;
				}
			}
			if(fm.ResponsibleIDStart.value=="" || trim(fm.ResponsibleIDStart.value)==""){
				strReturn = "负责人证件生效日不能为空";
				return strReturn;
			}else{
				if(!isDate(fm.ResponsibleIDStart.value)){
					strReturn = "负责人证件生效日期不是有效的日期格式(YYYY-MM-DD)";
					return strReturn;
				}
			}
			if((fm.ResponsibleIDEnd.value=="" && fm.ResponsibleIDLongFlag.value == "")||(trim(fm.ResponsibleIDEnd.value)=="" && fm.ResponsibleIDLongFlag.value == "")){
				strReturn = "负责人证件失效日不能为空";
				return strReturn;
			}else{
				if(fm.ResponsibleIDEnd.value!="" && !isDate(fm.ResponsibleIDEnd.value)){
					strReturn = "负责人证件失效日期不是有效的日期格式(YYYY-MM-DD)";
					return strReturn;
				}
				if(fm.ResponsibleIDLongFlag.value==""){
					var res = compareDate(fm.ResponsibleIDStart.value,fm.ResponsibleIDEnd.value);
					if(res!=2){
						strReturn = "负责人证件生效日期要早于证件失效日期";
						return strReturn;
					}
				}
			}
		}
		return strReturn;
	}else if(insuredProp=="2"){
		var tSex = trim(fm.AppntSex.value);
		var tAppntNativePlace = trim(fm.AppntNativePlace.value);
		var tAppntOccupationCode = trim(fm.AppntOccupationCode.value);
		var tAppntOccupationType = trim(fm.AppntOccupationType.value);
		if(tSex == ""||tAppntNativePlace == ""||
			tAppntOccupationCode == ""||tAppntOccupationType == ""){
				strReturn="投保人属性是自然人，则性别、国籍、职业代码、职业类别都必须录入。";
				return strReturn;
				}
		if(fm.IDType.value=="0"){
			strReturn = checkIdNo(fm.IDType.value,fm.IDNo.value,"",fm.AppntSex.value);
			if(strReturn!=""){
				return strReturn;
			}
		}
		
		if(fm.AppntOccupationCode.value!=""){
			var tsqlstr=" select OccupationType from LDOccupation where  OccupationCode ='"+fm.AppntOccupationCode.value+"'";
			var OccupationType = easyExecSql(tsqlstr,1,0);
			if(OccupationType==null) {
				strReturn = "职业代码"+fm.AppntOccupationCode.value+"没有定义职业类别!";
			    return strReturn;
			}
			if(OccupationType!=fm.AppntOccupationType.value){
				strReturn = "职业代码"+fm.AppntOccupationCode.value+"的职业类别与页面录入不符,应为"+OccupationType+"类职业!";
			    return strReturn;  
			}
		} 
		
		return strReturn;
		
	}
	return strReturn;
}

//校验受益所有人信息
function chkBeneficiaryDetail(){
	var rowCount = BeneficiaryDetailGrid.mulLineCount;    //行数
	var colCount = BeneficiaryDetailGrid.colCount;        //列数
	var strReturn="";
	for(var i=0;i<rowCount;i++){
		var name = BeneficiaryDetailGrid.getRowColData(i,1);
		var identity = BeneficiaryDetailGrid.getRowColData(i,2);
		var address = BeneficiaryDetailGrid.getRowColData(i,3);
		var identitytype = BeneficiaryDetailGrid.getRowColData(i,4);
		var idno = BeneficiaryDetailGrid.getRowColData(i,6);
		var idstart = BeneficiaryDetailGrid.getRowColData(i,7);
		var idend = BeneficiaryDetailGrid.getRowColData(i,8);
		if(name=="" && identity=="" && address=="" && identitytype=="" && idno=="" && idstart== "" && idend==""){
		}else{
			if(name==""){
				strReturn = "受益所有人姓名不能为空";
				return strReturn;
			}
			if(identity==""){
				strReturn = "受益所有人身份不能为空";
				return strReturn;
			}else{
				var tsqlstr=" select 1 from ldcode where  codetype ='identity' and code='"+identity+"'";
				var res = easyExecSql(tsqlstr);
				if(!res){
					strReturn="受益所有人身份不符合规则";
				}
			}
			if(address==""){
				strReturn = "受益所有人地址不能为空";
				return strReturn;
			}
			if(identitytype==""){
				strReturn = "受益所有人证件编码不能为空";
				return strReturn;
			}
			if(idno==""){
				strReturn = "受益所有人证件号码不能为空";
				return strReturn;
			}else{
				if(identitytype=='0'){
					var strChkIdNo=checkIdNo(identitytype,idno,"","");
					if(strChkIdNo != ""){
						strReturn = "受益所有人身份证号不符合规则";
						return strReturn;
				 	}
				}else if(identitytype!='4'){
					strReturn = "受益所有人证件编码不符合规则";
					return strReturn;
				}
			}
			if(idstart==""){
				strReturn = "受益所有人生效日期不能为空";
				return strReturn;
			}else{
				if(!isDate(idstart)){
					strReturn = "受益所有人生效日期不是有效的日期格式(YYYY-MM-DD)";
					return strReturn;
				}
			}
			if(idend=="" ){
				strReturn = "受益所有人失效日不能为空";
				return strReturn;
			}else{
				if(!isDate(idend)){
					strReturn = "受益所有人证件失效日期不是有效的日期格式(YYYY-MM-DD)";
					return strReturn;
				}
				var res = compareDate(idstart,idend);
				if(res!=2){
					strReturn = "受益所有人证件生效日期要早于证件失效日期";
					return strReturn;
				}
			}
		}
	}
	return strReturn;
}

function displayBnf()
{
		var strSql1 = "select bnfname,bnftype,bnfaddress,bnfidtype,(select codename from ldcode where codetype='identitytype' and code=bnfidtype),bnfidno,bnfidstartdate,bnfidenddate from lcgrpcontbnf where prtno='"+fm.PrtNo.value+"'";
		turnPage.queryModal(strSql1,BeneficiaryDetailGrid);
		ChkBeneficiaryCount();
}

function displayInsprop(prop){
	if(prop == "1"){
    	fm.all('ShareHolder1').style.display='';
		fm.all('ShareHolder2').style.display='';
		fm.all('LegalPerson1').style.display='';
		fm.all('LegalPerson2').style.display='';
		fm.all('Responsible1').style.display='';
		fm.all('Responsible2').style.display='';
		fm.all('AppInfo').style.display='none';
		fm.all('AppntOccTy1').style.display='none';
		fm.all('AppntOccTy2').style.display='none';
		fm.all('AppntSex').value="";
		fm.all('AppntSexName').value="";
		fm.all('AppntNativePlace').value="";
		fm.all('AppntNativePlaceName').value="";
		fm.all('AppntOccupationCode').value="";
		fm.all('AppntOccupationType').value="";
		var strSql = "select ShareholderName,ShareholderIDType,ShareholderIDNo,ShareholderIDStart,ShareholderIDEnd,ShareholderIDLongFlag,LegalPersonName1,LegalPersonIDType1,LegalPersonIDNo1,LegalPersonIDStart1,LegalPersonIDEnd1,LegalPersonIDLongFlag1,ResponsibleName,ResponsibleIDType,ResponsibleIDNo,ResponsibleIDStart,ResponsibleIDEnd,ResponsibleIDLongFlag from LCGrpAppnt  where GrpContNo =(select grpcontno from lcgrpcont where prtno='"+fm.PrtNo.value+"')";
	    var arrResult1 = easyExecSql(strSql);
		fm.ShareholderName.value = arrResult1[0][0];
		fm.ShareholderIDType.value = arrResult1[0][1];
		fm.ShareholderIDNo.value = arrResult1[0][2];
		fm.ShareholderIDStart.value = arrResult1[0][3];
		fm.ShareholderIDEnd.value = arrResult1[0][4];
		if(arrResult1[0][5]=="Y")
			{fm.IdNoValidity2.checked = true;setIDLongEffFlag2();}
		fm.LegalPersonName1.value = arrResult1[0][6];
		fm.LegalPersonIDType1.value = arrResult1[0][7];
		fm.LegalPersonIDNo1.value = arrResult1[0][8];
		fm.LegalPersonIDStart1.value = arrResult1[0][9];
		fm.LegalPersonIDEnd1.value = arrResult1[0][10];
		if(arrResult1[0][11]=="Y")
			{fm.IdNoValidity1.checked = true;setIDLongEffFlag1();}
		fm.ResponsibleName.value = arrResult1[0][12];
		fm.ResponsibleIDType.value = arrResult1[0][13];
		fm.ResponsibleIDNo.value = arrResult1[0][14];
		fm.ResponsibleIDStart.value = arrResult1[0][15];
		fm.ResponsibleIDEnd.value = arrResult1[0][16];
		if(arrResult1[0][17]=="Y")
			{fm.IdNoValidity3.checked = true;setIDLongEffFlag3();}
    }else if(prop == "2"){
    	fm.all('AppInfo').style.display='';
		fm.all('AppntOccTy1').style.display='';
		fm.all('AppntOccTy2').style.display='';
		fm.all('ShareHolder1').style.display='none';
		fm.all('ShareHolder2').style.display='none';
		fm.all('LegalPerson1').style.display='none';
		fm.all('LegalPerson2').style.display='none';
		fm.all('Responsible1').style.display='none';
		fm.all('Responsible2').style.display='none';
		fm.all('ShareholderName').value="";
		fm.all('ShareholderIDType').value="";
		fm.all('IDTypeName2').value="";
		fm.all('ShareholderIDNo').value="";
		fm.all('ShareholderIDStart').value="";
		fm.all('ShareholderIDEnd').value="";
		fm.all('IdNoValidity2').checked=false;
		fm.all('ShareholderIDLongFlag').value="";
		fm.all('LegalPersonName1').value="";
		fm.all('LegalPersonIDType1').value="";
		fm.all('IDTypeName1').value="";
		fm.all('LegalPersonIDNo1').value="";
		fm.all('LegalPersonIDStart1').value="";
		fm.all('LegalPersonIDEnd1').value="";
		fm.all('IdNoValidity1').checked=false;
		fm.all('LegalPersonIDLongFlag1').value="";
		fm.all('ResponsibleName').value="";
		fm.all('ResponsibleIDType').value="";
		fm.all('IDTypeName3').value="";
		fm.all('ResponsibleIDNo').value="";
		fm.all('ResponsibleIDStart').value="";
		fm.all('ResponsibleIDEnd').value="";
		fm.all('IdNoValidity3').checked=false;
		fm.all('ResponsibleIDLongFlag').value="";
//		var strSql = "select b.sex,b.nativeplace,b.occupationcode,b.occupationtype from LCGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
    }else{
    	fm.all('ShareHolder1').style.display='none';
		fm.all('ShareHolder2').style.display='none';
		fm.all('LegalPerson1').style.display='none';
		fm.all('LegalPerson2').style.display='none';
		fm.all('Responsible1').style.display='none';
		fm.all('Responsible2').style.display='none';
		fm.all('AppInfo').style.display='none';
		fm.all('AppntOccTy1').style.display='none';
		fm.all('AppntOccTy2').style.display='none';
    }
}

//新余机构团单社保直销校验
function checkXinYu(){
	var com = fm.ManageCom.value;
	var com1 = com.substr(0,6)
	if(com1=='863605' && fm.SaleChnl.value == '16'){
		alert('该机构下的团单社保直销渠道已关闭，请重新输入');
		return false;
	}
	return true;
}


//270102 保单层保险期间与保障计划中保险期间相同
function check270102(){
	var tSqlRisk = "select 1 from LCGrpPol where PrtNo='" + fm.PrtNo.value + "' and riskcode='270102' ";
	var arr = easyExecSql(tSqlRisk);
	if(arr){
		var strSQL = "select CValiDate,CInValiDate from lcgrpcont where prtno = '"+fm.PrtNo.value+"' ";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null){
			alert("校验缴费频次与保险区间时，获取数据失败！");
			return false;
		}
		var CValiDates = arrResult[0][0].split("-");
		var CInValiDates = arrResult[0][1].split("-");
//		var YearDif = parseInt(CInValiDates[0],10)-parseInt(CValiDates[0],10);// 年差
//		var MonthDif = YearDif*12 + parseInt(CInValiDates[1],10)-parseInt(CValiDates[1],10);// 月差
		var interval = 0;

	    var sYears = CValiDates[0];
	    var sMonths = CValiDates[1];
	    if(sMonths.length == 1) sMonths = "0" + sMonths;
	    var sDays = CValiDates[2];
	    if(sDays.length == 1) sDays = "0" + sDays; 

	    var eYears = CInValiDates[0];
	    var eMonths = CInValiDates[1];
	    if(eMonths.length == 1) eMonths = "0" + eMonths;
	    var eDays = CInValiDates[2];
	    if(eDays.length == 1) eDays = "0" + eDays; 

	    interval = eYears - sYears;
	    if (eMonths > sMonths){
	    	interval++;
		}else{
			if (eMonths == sMonths && eDays > sDays){
				interval++;
				if (eMonths == 02){ //如果同是2月，校验润年问题
					if ((sYears % 4) == 0 && (eYears % 4) != 0){ //如果起始年是润年，终止年不是润年
						if (eDays == 28){ //如果终止年不是润年，且2月的最后一天28日，那么减一
							interval--;
						}
					}
				}
			}
		}
	    
	    var strSQL2 = "select contplancode,calfactorvalue from lccontplandutyparam where contplancode <> '11' and calfactor ='StandByFlag1' and grpcontno='"+fm.all( 'GrpContNo' ).value+"'";
	    var arrResult2 = easyExecSql(strSQL2);
	    if(!arrResult2){
	    	alert("获取保单数据失败！");
	    	return false;
	    }
	    for(var i=0;i<arrResult2.length;i++){
	    	var strSQL3 = "select calfactorvalue from lccontplandutyparam where calfactor='StandByFlag' and grpcontno='"+fm.all( 'GrpContNo' ).value+"' and contplancode='"+arrResult2[i][0]+"'";
	    	var arrResult3 = easyExecSql(strSQL3);
	    	if(!arrResult3){
	    		alert("获取保单数据失败！");
	    		return false;
	    	}
	    	if(arrResult2[i][1] == interval && arrResult3[0][0] == "Y"){
	    	}else{
	    		alert("保险单录入保险期间与保障计划中保险期间不一致，请核实!");
	    		return false;
	    	}
	    }
	}
    return true;
    
}

//150407险种的保险期间只能为30天
function check150407(){
	var sql = "select insuyear,insuyearflag from lcpol where PrtNo='"+fm.PrtNo.value+"' and riskcode='150407'";
	var arr = easyExecSql(sql);
	if(arr){
		for(var i=0;i<arr.length;i++){
			if(arr[i][0]!=30 || arr[i][1]!="D"){
				alert("险种150407的保险期间必须等于30天！");
	    		return false;
			}
		}
	}
	return true;
}

//判断附加险是否在121702、121802、121902、220402、220702、220802 121301 中
function checkRiskCodeXPX(){
	var j=false;
	var sql = "select code from ldcode where codetype = 'studyrisk' ";
	var riskCode = easyExecSql(sql);
	for(var i = 0;i<riskCode.length;i++){
		if(fm.RiskCode.value == riskCode[i][0]){
			j=true;
			break;
		}
	}
	return j;
}

//校验浮动费率
function floatFee(){
	var sql = "select 1 from LCGrpPol where PrtNo='" + fm.PrtNo.value + "' and riskcode in (select code from ldcode where codetype = 'studyrisk' and code <> '220802' )";
	var arr = easyExecSql(sql);
	if(arr){
		var sqlStandprem="select riskcode,case when sum(standprem) is null or sum(standprem)=0 then 0 else 1 end   " +
		" from lcpol where prtno='" + fm.PrtNo.value + "' " +
		" and riskcode in (select code from ldcode where codetype = 'studyrisk' and code <> '220802') " +
		" group by riskcode";
		var sumStandprem=easyExecSql(sqlStandprem);
		var riskcodeF = new Array();
		var s=0;
		for(var i=0;i<sumStandprem.length;i++){
			if(sumStandprem[i][1]==0){
				riskcodeF[s]=sumStandprem[i][0];
				s++;
			}
		}
		if(riskcodeF.length>0){
			var riskcodeFS=riskcodeF.join("、");
			alert(" "+riskcodeFS+"保费计算失败，请检查算费要素！ ");
			return false;
		}
		
		var sql1 = "select riskcode,cast((sum(prem)*100/sum(standprem)) as DECIMAL(10,2)) " +
				" from lcpol where prtno='" + fm.PrtNo.value + "' " +
				" and riskcode in (select code from ldcode where codetype = 'studyrisk' and code <> '220802') " +
				" group by riskcode";
		var sql2 = "select codename from ldcode where codetype = 'sturiskfloatrate' order by code desc";
		var arr1 = easyExecSql(sql1);
		var arr2 = easyExecSql(sql2);
		var min = parseInt(arr2[0][0]);
		var max = parseInt(arr2[1][0]);
		if(!arr1){
			alert("获取浮动费率失败！");
			return false;
		}else{
			var j=0;
			var riskCode = new Array();
			var floatRate = new Array();
			for(var i=0;i<arr1.length;i++){
				if(arr1[i][1]>max || arr1[i][1]<min){
					riskCode[j]=arr1[i][0];
					floatRate[j]=arr1[i][1];
					j++;
				}
			}
			if(riskCode.length){
				var riskCodes =riskCode.join("、");
				var floatRates=floatRate.join("%、");
				alert(" "+riskCodes+"险种费率浮动范围为"+min+"%-"+max+"%,当前费率为"+floatRates+"%,超出允许范围，请核实 !");
				return false;
			}
		}
	}
	return true;
}
//删除520702时判断是否存在附加险121702、121802、121902、220402、220702、220802 121301
/*function delete520702(){
	var ch520702=false;
	for (var i=0; i<RiskGrid.mulLineCount; i++){
		if (RiskGrid.getChkNo(i)) {
	    	var RiskCode = RiskGrid.getRowColData(i,1);
	    	if(RiskCode=='520702'){
	    			ch520702=true;
	    			break;
	    	}
		}
	}
	if(ch520702){
		var xpxSql="select riskcode from lcgrppol lcg where lcg.prtno = '" + fm.PrtNo.value +"' and riskcode in (select code from ldcode where codetype = 'studyrisk')";
		var riskCodes = easyExecSql(xpxSql);
		if(riskCodes==null||riskCodes.length=0){
			return true;
		}else{
			if(!checkedAll520702(riskCodes)){
				alert("删除险种520702时，请先删除该险种的附加险或同该险种所有的附加险一同删除！");
				return false;
			}else{
				return true;
			}
		}
	}else{
		return ch;
	}
}
function checkedAll520702(riskCodes){
	var count=0;
	for (var i=0; i<RiskGrid.mulLineCount; i++){
		if (RiskGrid.getChkNo(i)){
	    	var RiskCode = RiskGrid.getRowColData(i,1);
	    	for(var j=0;j<riskCodes.length;j++){
	    		if(RiskCode==riskCodes[j]){
	    			cout++;
	    		}
	    	}
		}
	}
	if(count==riskCodes.length){
		return true;
	}else{
		return false;
	}
}*/