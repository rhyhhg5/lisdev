var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;

	if(dateDiff(tStartDate, tEndDate, "M") > 3)
	{
		alert("统计期最多为三个月！");
		return false;
	}

    var tStrSQL = ""
        + " select tmp.ManageCom, tmp.ManageComName, tmp.ScanOperator, "
        + " tmp.ScanOperatorName, tmp.SubType, tmp.SubTypeName, "
        + " Count(tmp.DocCode) ScanCount, "
        + " Sum(tmp.NumPages) ScanNumCount "
        + " from "
        + " ( "
        + " select "
        + " edm.DocCode, edm.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = edm.ManageCom) ManageComName, "
        + " edm.ScanOperator, "
        + " (select ldu.UserName from LDUser ldu where ldu.UserCode = edm.ScanOperator) ScanOperatorName, "
        + " edm.SubType, edf.SubTypeName, edm.NumPages, "
        + " '' "
        + " from Es_Doc_Main edm "
        + " inner join Es_Doc_Def edf on edm.SubType = edf.SubType "
        + " where 1 = 1 "
        + getWherePart("edm.ManageCom", "ManageCom", "like")
        + getWherePart("edm.MakeDate", "StartDate", ">=")
        + getWherePart("edm.MakeDate", "EndDate", "<=")
        + " ) as tmp "
        + " group by tmp.ManageCom, tmp.ManageComName, tmp.ScanOperator, "
        + " tmp.ScanOperatorName, tmp.SubType, tmp.SubTypeName "
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "ScanWorkload1DaySave.jsp";
    fm.submit();
    fm.action = oldAction;

}