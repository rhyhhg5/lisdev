//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
var type = "";
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function query()
{
    if(!verifyInput2())
    {
        return false;
    }
    
    cleanCertInsuMul();

    var tStrSql = ""
    			+ " select a.code1, "
    			+ " (select riskname from lmriskapp where riskcode = a.code1), "
    			+ " a.codename, "
    			+ " (select codename from ldcode where code = a.codename and codetype = 'unitesalechnlsgrp') "
    			+ " from ldcode1 a "
    			+ " where codetype ='limitrisk'"
    			+ getWherePart("a.code1", "RiskCode")
    			+ getWherePart("a.codename", "SaleChnl")
    			;
    
    turnPage1.pageDivName = "divCertifyListGridPage";
    turnPage1.queryModal(tStrSql, CertifyListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有信息！");
        return false;
    }
    
    return true;
}

function save()
{ 
	type = "";
	var tRiskCode =  fm.RiskCode.value;
	var tSaleChnl =  fm.SaleChnl.value;
	if(tRiskCode==""||tSaleChnl==""){
		alert("新增时险种编码和销售渠道不能为空！");
		return false;
	}

	var strSql = "select 1 from ldcode where  codetype='limintriskcode' and code = '" + tRiskCode + "' ";
	var arr = easyExecSql(strSql);
	if(!arr){
		alert("请选择正确的险种编码！");
		return false;
	}

	var strSql = "select 1 from ldcode where  codetype='unitesalechnlsgrp' and code = '" + tSaleChnl + "' ";
	var arr = easyExecSql(strSql);
	if(!arr){
		alert("新增数据所填销售渠道不存在");
		return false;
	}
	
	fm.fmtransact.value = "INSERT";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
	type = "1";
}
/**
 * 删除
 */
function deleteClick()
{
	type = "";
    var tRow = CertifyListGrid.getSelNo() -1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
	var tCode1 = CertifyListGrid.getRowColData(tRow,1);//险种编码
	var tCodeName = CertifyListGrid.getRowColData(tRow,3);
	fm.RiskCode.value = tCode1;
	fm.SaleChnl.value = tCodeName;
	fm.fmtransact.value = "DELETE";
	var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
	type = "1";
	fm.RiskCode.value = "";
	fm.SaleChnl.value = "";
}

/**
 * 查询信息
 */
function queryManagecom()
{
	initManageCom();
    var tRow = CertifyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = CertifyListGrid.getRowData(tRow);
    var triskcode = tRowDatas[0];
    var tsalechnl = tRowDatas[2];
    
    var tStrSql = ""
    			+ " select a.code1, "
    			+ " (select name from ldcom where comcode=a.code1), "
				+ " a.codename, "
				+ " (select riskname from lmriskapp where riskcode=a.codename), "
				+ " a.codealias, "
				+ " (select codename from ldcode where code = a.codealias and codetype = 'unitesalechnlsgrp') "
				+ " from ldcode1 a "
				+ " where codetype ='limitrisk1'"
				+ " and a.codename = '" + triskcode + "'"
				+ " and a.codealias = '" + tsalechnl + "'"
				;
    
    turnPage2.pageDivName = "divCertInsuListGridPage";
    turnPage2.queryModal(tStrSql, CertInsuListGrid);

    fm.all('divCertInsuListGrid').style.display = "";
}

function savem(){
	type = "";
    var tRow = CertifyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = CertifyListGrid.getRowData(tRow);
    var triskcode = tRowDatas[0];
    var tsalechnl = tRowDatas[2];  
    
    if(triskcode==""||tsalechnl==""){
        alert("请选择个险种。");
        return false;
    }
    fm.RiskCode.value=triskcode;
    fm.SaleChnl.value=tsalechnl; 
    var tmanagecom = fm.ManageCom.value;
    if(tmanagecom==""){
        alert("请选择管理机构。");
        return false;
    }
    
    var strSql = "select 1 from ldcom where Length(trim(comcode))=8 and Sign='1' and comcode ='" + tmanagecom + "' ";
	var arr = easyExecSql(strSql);
	if(!arr){
		alert("新增管理机构不存在");
		return false;
	}
    
	fm.fmtransact.value = "INSERTMANAGECOM";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
	type = "2";

}

function deletem(){
	type = "";
	var tRow = CertInsuListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    var tRowDatas = CertInsuListGrid.getRowData(tRow);
    var tmanagecom = tRowDatas[0];
    var triskcode = tRowDatas[2];
	fm.ManageCom.value = tmanagecom;
	fm.RiskCode.value = triskcode;

	fm.fmtransact.value = "DELETEMANAGECOM";
	var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
	type = "2";
	fm.ManageCom.value = "";
}

/**
 * 清除被保人信息查询列表
 */
function cleanCertInsuMul()
{
    CertInsuListGrid.clearData();
    fm.all('divCertInsuListGrid').style.display = "none";
}

/**
 * 提交后动作。
 */
function afterSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        if(type=="1"){
        	query();
        }else if (type=="2"){
        	queryManagecom();
        }
    }
}
function initManageCom(){
	fm.all("divManageCom").style.display = "";
}
