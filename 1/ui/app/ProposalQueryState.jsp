<%
//程序名称：ProposalQueryState.jsp
//程序功能：复核不通过修改
//创建日期：2002-11-23 17:06:57
//创建人  ：胡博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var mContNo = "<%=request.getParameter("ContNo")%>";
	var PolState = "<%=request.getParameter("ProState")%>";
	
	
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="ProposalQueryState.js"></SCRIPT>
  <%@include file="ProposalQueryStateInit.jsp"%>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <title>投保件状态明细</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraTitle">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>投保件状态明细</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      <!--    <TD  class= title> 投保单号码  </TD>-->
          <TD  class= title> 投保件状态 </TD>
          <TD  class= "readonly" colspan="7"><Input type=input class=readonly4  size=100  readonly=true name=PolState > </TD>
           </TR>
        <TR  class= common>
        	<TD  class= title> 印刷号 </TD>
        	<TD  class= "readonly" ><Input class=readonly readonly=true name=PrtNo > </TD> 
          <TD  class= title> 保险合同号 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=ContNo > </TD> 
          <TD  class= title> 投保人 </TD>
          <TD  class= "readonly" ><Input class= readonly readonly=true name=AppntName > </TD>
        	<TD  class= title> 管理机构 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=Managecom > </TD> 
       </TR>
       <TR  class= common>
        	<TD  class= title> 初审人员</TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=FirstTrialOperator > </TD> 
          <TD  class= title> 初审时间 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=FirstTrialDate > </TD> 
          <TD  class= title> 扫描人员 </TD>
          <TD  class= "readonly" ><Input class= readonly readonly=true name=ScanOperator > </TD>
        	<TD  class= title> 扫描时间 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=ScanMakedate > </TD> 
       </TR>
       <TR  class= common>
        	<TD  class= title> 录入人员 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=Operator > </TD> 
          <TD  class= title> 录入时间 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=MakeDate > </TD> 
          <TD  class= title> 复核人员 </TD>
          <TD  class= "readonly" ><Input class= readonly readonly=true name=ApproveCode > </TD>
        	<TD  class= title> 复核时间 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=ApproveDate > </TD> 
       </TR>
       <TR  class= common>
        	<TD  class= title> 核保人员 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=UWOperator > </TD> 
          <TD  class= title> 核保时间 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=UWDate > </TD> 
          <TD  class= title> 签单日期 </TD>
          <TD  class= "readonly" ><Input class= readonly readonly=true name=SignDate > </TD>
        	<TD  class= title> 收费时间 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=OperFeeDate > </TD> 
       </TR>
       <TR  class= common>
          <TD  class= title> 合同打印人员 </TD>
          <TD  class= "readonly" ><Input class= readonly readonly=true name=PrintOperator > </TD>
        	<TD  class= title> 合同打印时间 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=PrintDate > </TD> 
        	<TD  class= title> 回执回销人员 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=TakebackOperator> </TD> 
          <TD  class= title> 客户签收时间 </TD>
        	<TD  class= "readonly" ><Input class= readonly readonly=true name=TakebackDate> </TD> 
        </TR>
        <TR  class= common>
          <TD  class= title>回执回销时间 </TD>
          <TD  class= "readonly" ><Input class= readonly readonly=true name=TakebackMakeDate > </TD>
        </TR>
                  
    </table>
<DIV id=DivLCPol STYLE="display:''">
    <table class= common border=0 width=100%>
    	 <tr>
	        <td class= titleImg align= center>险种信息：</td>
	     </tr>
    </table>
    <table  class= common>
       <tr  class= common>
      	 <td text-align: left colSpan=1>
  					<span id="spanRiskGrid" >
  					</span>
  			 </td>
  		 </tr>
    </table>
</DIV>
<!-- 没有进行人工核保 -->
<div id = "divNoUWResult" style = "display: 'none'">
<table class= common align=center>
	<tr class = common>
		<td class =input>
			<Input class= readonly readonly=true name="NoUWResult"  value="还未进行人工核保">
		</td>
	</tr>
</table>
</div>
<!-- 进行过人工核保 -->
<div id = "divUWResult" style = "display: 'none'">
    	  <table class= common border=0 width=100%>
    	  	<tr>
			<td class= titleImg align= center>险种核保结论：</td>
	  	</tr>
	  </table>
	   		 	   	
  	  <table  class= common align=center>
    	  				<tr class = common>
      		<TD class= title>
      		 	核保结论
      		 </TD>
      		 <TD class=input>
      		 <Input class="codeNo" name=uwstate><input class=codename name=uwstateName readonly=true >
	   	     </TD>
	   	   
	   	   <TD>
	   	   </TD>
	   	   <td id=divPropValue style="display:none" class=input >
	   	   	<Input class=common name=PropValue >
	   	   </td>
	   	   </div>
	   	   <td>
	   	   </td>
      	</TR>
      </table>
          <!--存放加费或免责的信息-->
<DIV id=DivAddFee STYLE="display:'none'">
    <table class= common border=0 width=100%>
    	 <tr>
	        <td class= titleImg align= center>加费信息 : </td>
	        <td class= titleImg align= center>免责信息 : </td>
	     </tr>
    </table>
    <table  class= common>
       <tr  class= common>
      	 <td text-align: left colSpan=1>
  					<span id="spanAddFeeGrid" >
  					</span>
  			 </td>
  			 <td text-align: left colSpan=1>
  					<span id="spanSpecGrid" >
  					</span>
  			 </td>
  		 </tr>
  	</table>
</DIV>
		<div id="divDelay" style="display: 'none'">
			<table class="common">
				<TR class="common">
					<td class= title align= left>延期天数 : </td>
	  			<td class= input align= left>
	  				<INPUT VALUE="" class=common name="inputDelayDays" >
	  			</td>
	  			<td class= title align= left>延期之 : </td>
	  			<td class= input align= left>
	  				<INPUT VALUE="" class=coolDatePicker name="inputDelayDate" >
	  			</td>
	  			<td></td><td></td><td></td>
				</TR>
			</table>
		</div>
		<div id="divCancelReason" style="display: 'none'">
			<table class="common">
				<TR class="common">
					<td class= title align= left>撤销原因 : </td>
	  			<td class= input align= left>
	  				<INPUT VALUE="" class=code name="CancelReason" verify="核保结论|code:CancelReason" CodeData="0^01|变更承保不同意^02|客户主动撤销^03|问题件逾期未回复^04|其他" ondblclick="showCodeListEx('CancelReason',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('CancelReason',[this],[0],'', '', '', true);">
	  			</td>
	  			<td></td><td></td><td></td>
				</TR>
			</table>
		</div>
         <table class=common>
		
		<tr>
		      	<TD height="24"  class= titleImg>
            		核保意见
          	</TD>
          	<td></td>
          	</tr>
		<tr>
      		<TD  class= input> <textarea name="UWIdea" verify="核保结论|len<=255" cols="100%" rows="5" witdh=120% class="common"></textarea></TD>
      		<td></td>
      		</tr>
	  </table>
</div>
  </form>
</body>
</html>
