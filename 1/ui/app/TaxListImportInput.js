//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


///**
// * 提交表单
// */
//function submitForm(){
//    var i = 0;
//    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//    fm.submit(); //提交
//}
//
///**
// * 提交后操作,服务器数据返回后执行的操作
// */
//function afterSubmit(FlagStr, content){
//    window.focus();
//    showInfo.close();
//    
//    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
//    showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//    
//    if (FlagStr == "Succ"){
//        alert("Succ");
//    }
//}
//
///**
// * 申请单证导入批次号。
// */
//function applyNewBatchNo(){
//    fmImport.btnBatchNoApply.disabled = true;
//    
//    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
//    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
//    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//    
//    fmImport.action = "./CertifyBatchImportApply.jsp";
//    fmImport.submit();
//
//    fmImport.action = "";
//}
//
///**
// * 申请提交后动作。
// */
//function afterApplyNewBatchNoSubmit(FlagStr, Content, cBatchNo){
//    showInfo.close();
//    window.focus();
//    
//    if (FlagStr == "Fail" ){             
//        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
//        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
//        alert("申请批次失败，请尝试重新进行申请。");
//    }else{ 
//        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
//        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
//    }
//    
//    initBatchNoInput(cBatchNo);
//    fmImport.btnBatchNoApply.disabled = false;
//}

/**
 * 初始化批次号。
 */
function initBatchNoInput(cBatchNo){    
    if(cBatchNo != null && cBatchNo != ""){
        fmImport.BatchNo.value = cBatchNo;
    }else{
        fmImport.BatchNo.value = "";
    }
}

/**
 * 导入清单。
 */
function importTaxList(){
	
	if(!beforeImportTaxList()){
		return false;
	}
	var tBatchNo = fmImport.BatchNo.value;
	fmImport.btnImport.disabled = true;
		
	var showStr = "正在导入清单数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fmImport.action = "./TaxListImportSave.jsp?BatchNo=" + tBatchNo;
	fmImport.submit();
		
	fmImport.action = "";
	
}

/**
 * 导入清单前的检查，校验，准备
 */
function beforeImportTaxList(){
	var tBatchNo = fmImport.BatchNo.value;
	var tFileName = fmImport.FileName.value;
	//文件名不能为空
	if(tFileName==null || tFileName==""){
		alert("请选择文件！");
		return false;
	}
	//文件类型应为表格类型.xls
	if(fmImport.FileName.value.indexOf(".xls")==-1 || fmImport.FileName.value.indexOf(".xlsx")>=0){
		alert("文件类型不正确！");
		return false;
	}
	//文件名与批次号要一致
	if(tFileName.indexOf("\\")>0){
		tFileName = tFileName.substring(tFileName.lastIndexOf("\\")+1);
	}
	if(tFileName.indexOf("/")>0){
		tFileName = tFileName.substring(tFileName.lastIndexOf("/")+1);
	}
	if(tFileName.indexOf("_")>0){
		tFileName = tFileName.substring(0,tFileName.lastIndexOf("_"));
	}
	if(tFileName.indexOf(".")>0){
		tFileName = tFileName.substring(0,tFileName.lastIndexOf("."));
	}
	if(tFileName!=tBatchNo){
		alert("文件名与批次号不一致，请检查上传文件的文件名！");
		return false;
	}
	//当且仅当lsbatchinfo中的batch的值为：
	//0：待导入;1:待内检;可进行导入
	//2：待确认;3:已确认;不可进行导入
	var tSQL = "select batchstate from lsbatchinfo where batchno='"+tBatchNo+"'";
	var tArr = easyExecSql(tSQL);
	var tBatchState = "";
	if(tArr){
		tBatchState = tArr[0][0];
	}
	if(tBatchState!=null && tBatchState!="" && (tBatchState=="2" || tBatchState=="3")){
		alert("该批次已提交内检，不可再次进行导入！");
		return false;
	}
	return true;
}

/**
 * 导入清单提交后动作。
 */
function afterImportTaxList(FlagStr, Content, cBatchNo){
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" ){             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        fm.ImportErrBatchNo.value = cBatchNo;
        queryImportErrLog();
    }else{ 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        fm.ImportBatchNo.value = cBatchNo;
        queryBatchList();
        queryImportErrLog();
    }
    
    fmImport.btnImport.disabled = false;
}

//查询已导入的客户信息
function queryBatchList(){
    var tBatchNo = fm.ImportBatchNo.value;
    
    var tStrSql = "select lsi.batchno,lsi.name,lsi.sex,lsi.birthday,lsi.idtype,lsi.idno,lsi.checkflag,lsi.errormessage,"
    		    + "case lsb.batchstate when '0' then '待导入' when '1' then '待内检' when '2' then '待确认' when '3' then '已确认' end "
    			+ "from lsinsuredlist lsi, lsbatchinfo lsb "
    		    + "where lsi.batchno=lsb.batchno "
    		    + "and lsi.batchno='"+tBatchNo+"' "
    		    + "order by lsi.batchno,lsi.insuredid";

    turnPage1.pageDivName = "divImportBatchGridPage";
    turnPage1.queryModal(tStrSql, ImportBatchGrid);
    
    queryBatchInfo();

    if (!turnPage1.strQueryResult){
        fm.btnImportConfirm.disabled = true;
        fm.btnImportDelete.disabled = true;
        fm.btnImportCheck.disabled = true;
        alert("没有该批次导入信息！");
        return false;
    }
    
    return true;
}

//查询导入成功人数，内检成功人数，显示在页面的[导入清单列表]中。
function queryBatchInfo(){
    var tBatchNo = fm.ImportBatchNo.value;
    
    var tStrSql = "select lsi.batchno,count(distinct insuredid),(select count(1) from lsinsuredlist where batchno=lsi.batchno and checkflag='00') "
    			+ "from lsinsuredlist lsi "
    			+ "where lsi.batchno = '"+tBatchNo+"' "
    			+ "group by lsi.batchno";
    
    var arr = easyExecSql(tStrSql);
    if(arr){
        fm.ConfirmBatchNo.value = arr[0][0];
        fm.SumImportBatchCount.value = arr[0][1];
        fm.SumCheckSuccessCount.value = arr[0][2];
        
        fm.btnImportConfirm.disabled = false;
        fm.btnImportDelete.disabled = false;
        fm.btnImportCheck.disabled = false;
    }else{
        fm.btnImportConfirm.disabled = true;
        fm.btnImportDelete.disabled = true;
        fm.btnImportCheck.disabled = true;
        
        fm.ConfirmBatchNo.value = "";
        fm.SumImportBatchCount.value = "";
        fm.SumCheckSuccessCount.value = "";
    }
}
/**
 * 批次导入确认。
 */
function confirmImport(){
	//只有批次状态变为 2:待确认的批次才可以点击【批次导入确认】按钮
	var tBatchNo = fm.ImportBatchNo.value;
	var tSQL = "select 1 from lsbatchinfo where batchno='"+tBatchNo+"' and batchstate<>'2'";
	var arr = easyExecSql(tSQL);
	if(arr){
		alert("该批次内检尚未完成或已确认完毕，不能进行【批次导入确认】操作");
		return false;
	}
	//若批次中含有内检失败记录，不允许确认
	var tConfirmSQL = "select distinct 1 from lsinsuredlist where batchno='"+tBatchNo+"' and (checkflag='01' or checkflag is null)";
	var tArr = easyExecSql(tConfirmSQL);
	if(tArr){
		alert("该批次中包含内检失败数据，不允许进行确认！");
		return false;
	}
    fm.btnImportConfirm.disabled = true;
    fm.btnImportDelete.disabled = true;
    fm.btnImportCheck.disabled = true;
    
    var showStr = "正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.fmOperatorFlag.value = "ImportConfirm";
    fm.action = "./TaxListImportConfirmSave.jsp";
    fm.submit();

    fm.fmOperatorFlag.value = "";
    fm.action = "";
}

/**
 * 批次导入确认后动作。
 */
function afterConfirmImport(FlagStr, Content){
    showInfo.close();
    window.focus();
  //  alert("FlagStr===="+FlagStr);
    //alert("Content===="+Content);
    if (FlagStr == "Fail" ){   
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
    //    alert("urlStr=="+urlStr);
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }else{ 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    queryBatchList();
}

/**
 * 批次被保人删除
 */
function delImport(){
	//若批次状态为3-已确认，则无需进行被保人删除操作
	var tBatchNo = fm.ImportBatchNo.value;
	var tSQL3 = "select 1 from lsbatchinfo where batchno='"+tBatchNo+"' and batchstate='3'";
	var arr3 = easyExecSql(tSQL3);
	if(arr3){
		alert("该批次已确认，不能进行【批次被保人删除】操作！");
		return;
	}
	
//	//若批次状态为2-待确认，则需要检查该批次中是否有内检失败数据(checkflag='01')
//	//若没有，不允许删除
//	var tSQL1 = "select batchstate from lsbatchinfo where batchno='"+tBatchNo+"'";
//	var arr1 = easyExecSql(tSQL1);
//	var tBatchstate = "";
//	if(arr1){
//		tBatchstate = arr1[0][0];
//		if(tBatchstate == "2"){
//			var tSQL2 = "select distinct 1 from lsinsuredlist where batchno='"+tBatchNo+"' "
//			  		  + "and (checkflag='01' or checkflag is null) ";
//			var arr2 = easyExecSql(tSQL2);
//			if(!arr2){
//				alert("待确认状态下，没有内检失败数据，不允许删除！");
//				return;
//			}
//		}
//	}
	
	var count = 0;
	for(var i = 0; i < ImportBatchGrid.mulLineCount; i++ )
	{
		if( ImportBatchGrid.getChkNo(i) == true )
		{
			count ++;
			var tBatchNo = ImportBatchGrid.getRowColData(i,1);
			var tName = ImportBatchGrid.getRowColData(i,2);
			var tCheckFlag = ImportBatchGrid.getRowColData(i,7);
			var tBatchState = ImportBatchGrid.getRowColData(i,9);
			if(tCheckFlag!=null && tCheckFlag=="00"){
				alert("已经内检成功的客户，不允许删除");
				return false;
			}
		}
	}
	if(count == 0){
		alert("请选择需要删除的客户记录！");
		return;
	}
	
    fm.btnImportConfirm.disabled = true;
    fm.btnImportDelete.disabled = true;
    fm.btnImportCheck.disabled = true;
    
    var showStr = "正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.fmOperatorFlag.value = "ImportDelete";
    fm.action = "./TaxListImportConfirmSave.jsp";
    fm.submit();

    fm.fmOperatorFlag.value = "";
    fm.action = "";
}

//查询未成功导入的记录
function queryImportErrLog(){
//    var tBatchNo = fm.ImportErrBatchNo.value;
	var tBatchNo = fmImport.BatchNo.value;
    
    var tStrSql = "select batchno,cardno,errorinfo,makedate from licertifyimportlog "
    			+ "where batchno='"+tBatchNo+"' and errortype<>'00' "
    			+ "order by batchno,cardno";

    turnPage2.pageDivName = "divImportErrLogGridPage";
    turnPage2.queryModal(tStrSql, ImportErrLogGrid);

    if (!turnPage2.strQueryResult){
        alert("没有该批次导入报错信息！");
        return false;
    }else{
    	fm.ImportErrBatchNo.value = tBatchNo;
    }
    
    return true;
}

//客户查询
function queryInsured(){
	initImportErrLogGrid();
	initImportBatchGrid();
	
	var tBatchNo = fm.ImportBatchNo.value;
    
    var tStrSql = "select lsi.batchno,lsi.name,lsi.sex,lsi.birthday,lsi.idtype,lsi.idno,lsi.checkflag,lsi.errormessage,"
    		    + "case lsb.batchstate when '0' then '待导入' when '1' then '待内检' when '2' then '待确认' when '3' then '已确认' end "
    			+ "from lsinsuredlist lsi, lsbatchinfo lsb "
    		    + "where lsi.batchno=lsb.batchno "
    		    + "and lsi.batchno='"+tBatchNo+"' "
    		    + getWherePart("lsi.Name","Name")
    		    + getWherePart("lsi.IdType","IDType")
    		    + getWherePart("lsi.IdNo","IDNo")
    		    + "order by lsi.batchno,lsi.insuredid";

    turnPage1.pageDivName = "divImportBatchGridPage";
    turnPage1.queryModal(tStrSql, ImportBatchGrid);
}

/**
 * 批次中保信内检
 */
function checkImport(){
	//在发送批次信息给中保信信息平台时，首先应该有该批次待发送信息
	if(ImportBatchGrid.mulLineCount<1){
		alert("本批次尚未有待报送的客户信息，请核实！");
		return false;
	}
	//在报送数据时，将按钮置灰，防止重复点击
	fm.btnImportConfirm.disabled = true;
    fm.btnImportDelete.disabled = true;
    fm.btnImportCheck.disabled = true;
	//报送本批次的客户信息至中保信信息平台
    fm.fmOperatorFlag.value = "CallCeckeInterface";
    var tBatchNo = fm.ImportBatchNo.value;
	var showStr = "正在报送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    //转到save.jsp提取信息，做传到后台之用
    fm.action = "TaxEIPCheckSave.jsp?BatchNo="+tBatchNo;
    fm.submit();
    fm.action = "";
}

//内检后动作
function afterCheckImport(FlagStr, Content, cBatchNo){
	showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" ){             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("批次中报信内检失败，请尝试重新进行申请。");
    }else{ 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        fm.ImportBatchNo.value = "";
        fm.ConfirmBatchNo.value = "";
    }
    queryBatchList();
}

//返回
function goBack(){
    window.location.replace("./TaxImport.jsp");
}