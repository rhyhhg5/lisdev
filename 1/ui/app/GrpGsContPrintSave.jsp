<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2009-7-15
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.wiitb.*" %>>

<%
System.out.println("GrpGsContPrintSave.jsp start ...");

String FlagStr = "Fail";
String Content = "";
GlobalInput tG = (GlobalInput)session.getValue("GI");

try
{
    String tOtherNo = request.getParameter("OtherNo");
    
    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
    tLOPRTManagerSchema.setOtherNo(tOtherNo);
    
    VData tVData = new VData();
    
    String tCode = "GSL01";
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("Code", tCode);
    
    tVData.add(tLOPRTManagerSchema);
    tVData.add(tTransferData);
    
    //tG.ClientIP = request.getRemoteAddr(); //操作员的IP地址
    tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
    System.out.println("------操作员的IP地址:"+tG.ClientIP);
  
    tVData.add(tG);
    
    GrpGsContInsuListPrtBL tGrpGsContInsuListPrtBL = new GrpGsContInsuListPrtBL();
    if(!tGrpGsContInsuListPrtBL.submitData(tVData, "only"))
    {
        Content = " 打印失败，原因是: " + tGrpGsContInsuListPrtBL.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 打印成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 打印失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("GrpGsContPrintSave.jsp end ...");

%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit2("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
