//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}
//提交，保存按钮对应操作
function submitForm(type)
{
  var tSelNo = PolGrid.getSelNo() - 1;
  if("-1" == tSelNo)
  {
     alert("未有选择的受益人信息！");
     return false;
  }
//受益人信息补录中增加身份证校验   ranguo 20170830
  var i = 0;
  var checkFlag = 0;
  for (i=0; i<PolGrid.mulLineCount; i++) 
  {
  	if (PolGrid.getSelNo(i)) 
  	{
  		checkFlag = PolGrid.getSelNo();
		break;
	}
  } 
  if (checkFlag) 
  { //获取证件类型
    var IDType =PolGrid.getRowColData(checkFlag-1,5);
	//获取证件号码
	var IDNo =PolGrid.getRowColData(checkFlag-1,6);
  }
  else
  {
	alert("请先选择一条受益人信息！"); 
  }
  //Sex从反显信息中获取
  var Sex=fm.Sex.value;
  var checkResult=checkIdNo(IDType,IDNo,"",Sex);
  if(checkResult!=null && checkResult!=""){
	 alert(checkResult);
	 return false;
  }
  if( verifyInput2() == false ){
     return false;
  }
  
  if(type=='1'){
     fm.fmtransact.value="UPDATE||MAIN";
  }else{
     fm.fmtransact.value="DELETE||MAIN";
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    content = "保存成功！";
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  showBnfInfo();
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function showRiskInfo()
{
	
	var strSql = "select a.insuredname,a.riskcode,"
	+"  b.BnfType,b.Name,b.IDType,b.IDNo,b.RelationToInsured,b.BnfLot,b.BnfGrade,a.polno,b.bnfno "
	+" from lcpol a,lcbnf b where "
	+" a.contno=b.contno "
	+" and a.polno=b.polno "
	+" and a.PrtNo='"+prtNo+"'"
	+" order by a.polno ,b.bnfno ";
	turnPage.queryModal(strSql, PolGrid);

	//try { top.fraPic.service.width = top.fraPic.service.width *0.7; } catch (ex) {}
	try { goToPic(2); top.fraPic.scrollTo(0, 1300); }  catch(e) {} 
	
}

//反显受益人信息
function showBnfInfo()
{
    var tSelNo = PolGrid.getSelNo() - 1;
    if("-1" == tSelNo)
    {
       alert("未有选择的受益人信息！");
       return false;
    }
    clearBnfInfo();   
    oneRowResult = PolGrid.getRowData(tSelNo);
    fm.all('PolNo').value=oneRowResult[9];
    fm.all('BnfNo').value=oneRowResult[10];
    fm.all('BnfType').value=oneRowResult[2];
	var strSql = "select a.Name,a.sex,a.nativeplace,a.idstartdate,a.idenddate,a.phone,a.PostalAddress,a.OccupationCode, "
	            +" (select OccupationName from LDOccupation where OccupationCode=a.OccupationCode)" 
	            + " from lcbnf a where a.polno='"+oneRowResult[9]+"' and a.bnfno='"+ oneRowResult[10]+"' and a.bnftype='"+ oneRowResult[2]+"'";
	var arr = easyExecSql(strSql);
	if(arr){
	   fm.all('Name').value=arr[0][0];
       fm.all('Sex').value=arr[0][1];
       fm.all('NativePlace').value=arr[0][2];
       fm.all('IDStartDate').value=arr[0][3];
       fm.all('IDEndDate').value=arr[0][4];
       fm.all('Phone').value=arr[0][5];
       fm.all('PostalAddress').value=arr[0][6];
       fm.all('OccupationCode').value=arr[0][7];
       fm.all('OccupationName').value=arr[0][8];
	}
	showAllCodeName();	
}

//外包反馈错误信息 2007-9-27 20:21
function findIssue()
{
	window.open("./BPOIssueInput.jsp?prtNo="+prtNo,"window1");
}

function clearBnfInfo(){
       fm.all('Name').value="";
       fm.all('Sex').value="";
       fm.all('SexName').value="";
       fm.all('NativePlace').value="";
       fm.all('NativePlaceName').value="";
       fm.all('IDStartDate').value="";
       fm.all('IDEndDate').value="";
       fm.all('Phone').value="";
       fm.all('PostalAddress').value="";
       fm.all('OccupationCode').value="";
       fm.all('OccupationName').value="";

}