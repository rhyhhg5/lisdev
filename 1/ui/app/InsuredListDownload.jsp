<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = request.getParameter("prtNo") + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String querySql = request.getParameter("querySql");
    querySql = querySql.replaceAll("%25","%");
    //设置表头
    String[][] tTitle = {{"编码", "姓名", "性别", "出生日期", 
        "证件类型", "证件号码", "保费", "保障计划", "职业类别", "在职/退休"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list", "help"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
        createexcellist.setRowColOffset(row+1,0);//设置偏移
    row = createexcellist.setData(querySql,displayData);
    if(row == -1)
        errorFlag = true;
    
    //在同一Sheet内增加说明
    //createexcellist.setRowColOffset(row + 1, 0);
    //String[][] help = {{"说明："}, {"性别"}};
    //int[] displayHelp = {1};
    //createexcellist.setData(help,displayHelp);
    
    //在新Sheet内增加说明
    //String[] helpTitle = {"说明："};
    //createexcellist.setRowColOffset(2, 0);
    String[][] helpDate = {{"说明："}, 
        {"性别：0-男，1-女，2-不详"}, 
        {"证件类型：0-身份证，1-护照，2-军官证，3-工作证，4-其它"}, 
        {"职业类别：1-一类，2-二类，3-三类，4-四类，5-五类，6-六类"}, 
        {"在职/退休：1-在职，2-退休，3-其它"}};
    //createexcellist.setTitle(1, helpTitle);
    createexcellist.setData(1, helpDate);
    
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>

