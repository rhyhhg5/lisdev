<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：ContDeleteChk.jsp
	//程序功能：个单整单删除
	//创建日期：2004-12-06 11:10:36
	//创建人  ：Zhangrong
	//更新记录：  更新人    更新日期     更新原因/内容
	System.out.println("Auto-begin:");
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "成功！";
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	if (tG == null) {
		System.out.println("session has expired");
		return;
	}

	// 投保单列表
	String tContNo = request.getParameter("ContNo");
	String tPrtNo = request.getParameter("PrtNo");
	String tDeleteReason = request.getParameter("DeleteReason");
	System.out.println("tDeleteReason&&&&&&&&&&&&&&&:" + tDeleteReason);
	boolean flag = false;

	System.out.println("ready for UWCHECK ContNo: " + tContNo);

	LCContSet tLCContSet = null;
	LCContDB tLCContDB = new LCContDB();
	TransferData tTransferData = new TransferData();
	if (!StrTool.cTrim(tContNo).equals("")) {
		tLCContDB.setContNo(tContNo);
		tLCContSet = tLCContDB.query();
	} else {
		LCContSchema mLCContSchema = new LCContSchema();
		mLCContSchema.setPrtNo(tPrtNo);
		tLCContSet = new LCContSet();
		tLCContSet.add(mLCContSchema);
	}
	if (tLCContSet == null) {
		System.out.println("（投）保单号为" + tContNo + "的合同查找失败！");
		return;
	}
	//   System.out.println("（投）保单号为!!!!!!!!1！"); 
	LCContSchema tLCContSchema = tLCContSet.get(1);
	tTransferData.setNameAndValue("DeleteReason", tDeleteReason);

	try {
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tLCContSchema);
		tVData.add(tTransferData);
		tVData.add(tG);

		// 数据传输
		PadContDeleteUI tContDeleteUI = new PadContDeleteUI();
		if (tContDeleteUI.submitData(tVData, "DELETE") == false) {
			FlagStr = "Fail";
		} else {
			ExeSQL delSQL = new ExeSQL();
			String tempfeenoSQL = "select TempFeeNo from LJTempFee where otherno = '" + tPrtNo + "' with ur";
			String tempfeeno = delSQL.getOneValue(tempfeenoSQL);
			if ("".equals(tempfeeno) || tempfeeno == null) {
				System.out.println("tempfeeno为空，无需删除暂收费表。");
			} else {
				String delTempFeeData = "delete from LJTempFee where otherno = '" + tPrtNo + "'";
				delSQL.execUpdateSQL(delTempFeeData);
				String delTempFeeClass = "delete from LJTempFeeClass where TempFeeNo = '" + tempfeeno + "'";
				delSQL.execUpdateSQL(delTempFeeClass);
			}
			String missionIDSQL = "select missionid from lwmission where missionprop1 = '" + tPrtNo + "' with ur";
			String missionID = delSQL.getOneValue(missionIDSQL);
			if ("".equals(missionID) || missionID == null) {
				System.out.println("MissionID为空，无需删除工作流。");
			} else {
				String delMission = "delete from lwmission where missionid = '" + missionID + "'";
				delSQL.execUpdateSQL(delMission);
			}
			FlagStr = "Succ";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail") {
			tError = tContDeleteUI.mErrors;
			System.out.println("tError.getErrorCount:" + tError.getErrorCount());
			if (!tError.needDealError()) {
				Content = " 整单删除成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 自动核保失败，原因是:";
				int n = tError.getErrorCount();
				if (n > 0) {
					for (int i = 0; i < n; i++) {
						//tError = tErrors.getError(i);
						Content = Content.trim() + i + ". " + tError.getError(i).errorMessage.trim() + ".";
					}
				}
				FlagStr = "Fail";
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
		Content = Content.trim() + ".提示：异常终止!";
	}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
