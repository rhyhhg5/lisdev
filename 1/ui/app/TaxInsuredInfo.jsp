<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<html>
	<head>
    	<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    	<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    	<script src="../common/javascript/Common.js"></script>
    	<script src="../common/cvar/CCodeOperate.js"></script>
    	<script src="../common/javascript/MulLine.js"></script>
   	 	<script src="../common/Calendar/Calendar.js"></script>

    	<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    	<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    	<script src="TaxInsuredInfo.js"></script>
    	<%@include file="TaxInsuredInfoInit.jsp" %>
	</head>
	<body onload="initForm();">
		<form action="" method="post" name="fm" target="fraSubmit">
			<table>
            	<tr>
                	<td class="common">
                    	<img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportList);" />
                	</td>
                	<td class="titleImg">导入清单列表</td>
           	 	</tr>
        	</table>
        	<div id="divImportList">
        		<table class=common>
        			<tr class=common>
        				<td class=title>批次号</td>
        				<td class=input>
        					<input name=BatchNo />
        				</td>
        				<td class=title>导入总人数</td>
        				<td class=input>
        					<input name=ImportPeoples />
        				</td>
        			</tr>
        		</table>
        		<div id="divBatchListInfo" style="display:''">
            		<table>
            			<tr>
                			<td class="common">
                    			<img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportBatchGrid);" />
                			</td>
                			<td class="titleImg">客户查询条件</td>
            			</tr>
            		</table>
            		<table class="common">
                		<tr class="common">
                    		<td class="title">客户姓名</td>
                   			<td class="input">
                        		<input class="common" name="Name" />
                    		</td>
                    		<td class="title">证件类型</td>
                    		<td class="input">
                        		<Input class=codeNo name="IDType"  ondblclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1]);"><input class=codename name=IDTypeName readonly=true elementtype=nacessary> 
                    		</td>
                    		<td class="title">证件号码</td>
                    		<td class="input">
                        		<input class="common" name="IDNo"  />
                    		</td>
                		</tr>
           			</table>
            		<table>
            			<tr>
                			<td class="common">
                    			<input class="cssButton" type="button" value="查  询" onclick="queryInsured();" /> 	
                			</td>
            			</tr>
           			</table>
        		</div>
        		<table class="common">
                	<tr class="common">
                    	<td>
                        	<span id="spanImportBatchGrid"></span> 
                    	</td>
                	</tr>
            	</table>
            	<div id="divImportBatchGridPage" style="display: ''" align="center">
                	<input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" />
                	<input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />
                	<input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" />
                	<input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />
            	</div>
        	</div>
        	<hr />
        	<input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value="返  回" onclick="goBack();" />
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>