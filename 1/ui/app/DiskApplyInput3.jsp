<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人 ：HST
//更新记录： 更新人  更新日期   更新原因/内容
%>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<script language='javascript'>
var showInfo;
var turnPage=new turnPageClass();
var arrDataSet;
var ImportPath;
var grpcontno="<%=request.getParameter("grpcontno")%>";
var ImportState="no";
window.onbeforeunload=beforeAfterInput;
window.onunload=AfterInput;
function beforeAfterInput()
{
	if ( ImportState=="Importing" )
	{
		alert("磁盘投保尚未完成，请不要离开!");
		return false;
	}
}

function AfterInput()
{
	if ( ImportState=="Importing" )
	{
		return false;
	}
}

function submitForm()
{
	if ( ImportState=="Succ")
	{
		if ( !confirm("确定您要再次磁盘投保吗?") )
		{
			return false ;
		}
	}
	var i=0;
	getImportPath();
	ImportFile=fm.all('FileName').value;
	var prtno=getPrtNo();
	var tprtno=ImportFile;

	if ( tprtno.indexOf("\\")>0 ) tprtno=tprtno.substring(tprtno.lastIndexOf("\\")+1);
	if ( tprtno.indexOf("/")>0 ) tprtno=tprtno.substring(tprtno.lastIndexOf("/")+1);
	if ( tprtno.indexOf("_")>0) tprtno=tprtno.substring( 0,tprtno.indexOf("_"));
	if ( tprtno.indexOf(".")>0) tprtno=tprtno.substring( 0,tprtno.indexOf("."));

//	if ( prtno!=tprtno )
//	{
//		alert("文件名与印刷号不一致,请检查文件名!");
//		return ;
//	}
//	else
//	{
		document.all("info").innerText="上传文件导入中，请稍后..."
		fm.all('ImportPath').value = ImportPath;
		ImportState="Importing";
		//fm.action="./DiskApplySave.jsp?ImportPath="+ImportPath+"&BQFlag=Y&EdorType=NI";
		//fm.action="./DiskApplySave.jsp?BQFlag=Y&EdorType=NI";
		fm.submit(); //提交
//	}
}

function getImportPath ()
{
	// 书写SQL语句
	var strSQL="";
	strSQL="select SysvarValue from ldsysvar where sysvar='XmlPath'";
	turnPage.strQueryResult =easyQueryVer3(strSQL, 1, 1, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("未找到上传路径");
		return;
	}
	//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet=clearArrayElements(turnPage.arrDataCacheSet);
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet=decodeEasyQueryResult(turnPage.strQueryResult);
	ImportPath=turnPage.arrDataCacheSet[0][0];
}

function getPrtNo ()
{
	// 书写SQL语句
	var strSQL="";
	strSQL="select PrtNo from lcgrpcont where grpcontno='"+ grpcontno +"'";
	turnPage.strQueryResult =easyQueryVer3(strSQL, 1, 1, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查找到保单:" + grpcontno);
		return;
	}
	//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet=clearArrayElements(turnPage.arrDataCacheSet);
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet=decodeEasyQueryResult(turnPage.strQueryResult);
	return turnPage.arrDataCacheSet[0][0];
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Result )
{
	document.all("info").innerText=content;
	ImportState=FlagStr ;
}
</script>
</head>
<body>
	<form action="./DiskApplySave.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
		<input type=hidden name="Flag" value="BQ">
		<input type=hidden name="ImportPath">
		<Input type="hidden"　width="100%" name="insuredimport" value="1">
		<br><br>
		<table class=common>
			<TR>
				<TD width='15%' style="font:9pt">文件名</TD>
				<TD width='85%'>
					<Input type="file" width="100%" name=FileName>
					<INPUT VALUE="导  入" class="cssButton" TYPE=button onclick="submitForm();">
				</TD>
			</TR>
			<TR>
				<TD colspan=2>
				</TD>
			</TR>
			<input type=hidden name=ImportFile>
			
		</table>
		<table class=common>
			<TR>
				<TD id="info" width='100%' style="font:10pt"></TD>
			</TR>
		</table>
	</form>
</body>