<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
	<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<title>契约保单责任保额变更</title>
		<script src="QYModifyAmntInput.js"></script>
		<%@include file="QYModifyAmntInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./QYModifyAmntSave.jsp" method="post" name="fm" target="fraTitle">
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输入查询条件：
					</td>
				</tr>
			</table>

			<table class=common border=0 width=100%>
				<tr class=common>
					<td class=title>管理机构</td>
					<td class=input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true ></td>
					<td class=title>印 刷 号</td>
					<td class=input><input class=common name=PrtNo /></td>
					<td class=title>合 同 号</td>
					<td class=input><input class=common name=ContNo /></td>
				</tr>
				<tr class=common>
					<td class=title>保单类型</td>
					<td class=input><Input class=codeno name=ContType value="2" CodeData="0|^1|个单^2|团单" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" ><input class=codename name=ContTypeName value="个单" readonly=true ></td>
					<td class=title>保障计划</td>
					<td class=input><Input class= common name=ContPlanCode /></td>
					<td class=title>保额类型</td>
					<td class=input><Input class=codeno name=AmntType value="1" CodeData="0|^1|保额^2|限额" ondblclick="return showCodeListEx('AmntType',[this,AmntTypeName],[0,1],null,null,null,1);" ><input class=codename name=AmntTypeName value="保额" readonly=true ></td>
				</tr>
				<tr class=common>
					<td class=title>险种</td>
					<td class=input><Input class= common name=RiskCode /></td>
					<td class=title>责任</td>
					<td class=input><Input class= common name=DutyCode /></td>
					<td class=title></td>
					<td class=input></td>
				</tr>
			</table>
			<INPUT VALUE="查询保单" class="cssButton" TYPE=button onclick="easyQueryClick();">
			
			<table>
				<tr>
					<td class=common>
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
					</td>
					<td class= titleImg>保单信息</td>
				</tr>
			</table>
			<div id= "divLCPol1" style= "display: ''">
				<table class= common>
					<TR class= common>
						<td text-align: left colSpan=1>
							<span id="spanPolGrid" ></span>
						</td>
					</tr>
				</table>
	
				<table align=center>
					<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
					<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
					<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
					<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
				 </table>
			</div>
			<br />
			<table class=common border=0 width=100%>
				<tr class=common>
					<td class=title>修改后责任保额</td>
					<td class=input><input class=common name=ModifyAmnt /></td>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
				</tr>
			</table>
			<INPUT VALUE="修改保额" class="cssButton" TYPE=button onclick="modifyAmnt();">
			<input type=hidden id="fmtransact" name="fmtransact">
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
