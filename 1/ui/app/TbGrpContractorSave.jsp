<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TbGrpConractorSave.jsp
//程序功能：
//创建日期：2006-5-17 17:12
//创建人  ：yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
<%
	GlobalInput tG=(GlobalInput)session.getValue("GI");
	LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
	String tNum[] = request.getParameterValues("ConractorGridNo");
	String tCalFactor[] = request.getParameterValues("ConractorGrid1");
	String tCalFactorValue[] = request.getParameterValues("ConractorGrid4");
	String tCalFactorType[] = request.getParameterValues("ConractorGrid5");
	String tOperate = request.getParameter("fmtransact");
	String FlagStr ="";
	CErrors tError = null;
	String Content ="";
	int multline = 0;
	if (tNum != null) multline = tNum.length;
	for(int i=0 ; i<multline ; i++){
		LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
		tLCContPlanDutyParamSchema.setGrpContNo(request.getParameter("GrpContNo"));
		tLCContPlanDutyParamSchema.setCalFactor(tCalFactor[i]);
		tLCContPlanDutyParamSchema.setCalFactorType(tCalFactorType[i]);
		tLCContPlanDutyParamSchema.setCalFactorValue(tCalFactorValue[i]);
		tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
	}
	VData tVData = new VData();
	tVData.add(tLCContPlanDutyParamSet);
	tVData.add(tG);
	TbGrpContractorUI tTbGrpContractorUI = new TbGrpContractorUI();
  try
  {
  	tTbGrpContractorUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tTbGrpContractorUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>