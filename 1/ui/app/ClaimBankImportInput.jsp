<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="ClaimBankImportInput.js"></script>
    <%@include file="ClaimBankImportInputInit.jsp" %>
</head>

<body onload="initForm();">
    <form action="" method="post" name="fmImport" target="fraSubmit" enctype="multipart/form-data">

        <table class="common">
            <tr class="common">
                <td class="title">批次号</td>
                <td class="input">
                    <input class="readonly" name="BatchNo" readonly="readonly" />
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        
        <hr />
        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">批次导入</td>
            </tr>
        </table>
        
        <div id="divFileImport" name="divFileImport" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">批次文件</td>
                    <td class="common">
                        <input class="cssfile" name="FileName" type="file" />
                    </td>
                </tr>
            </table>
            <input class="cssButton" type="button" id="btnImport" name="btnImport" value="批次导入" onclick="this.disabled=true; setEnable('btnImport');importBatchList();"  />
        </div>
        <input type="hidden" id="MissionId" name="MissionId" />
        <input type="hidden" id="SubMissionId" name="SubMissionId" />
        <input type="hidden" id="ProcessId" name="ProcessId" />
        <input type="hidden" id="ActivityId" name="ActivityId" />
        <input type="hidden" id="ActivityStatus" name="ActivityStatus" /> 
        <input type="hidden" class="common" name="querySql" >  
        <hr />
    </form>

	<form action="" method="post" name="fm" target="fraSubmit">
		<div id="divImportLog" style="display:'none'">        
	        <table>
	            <tr>
	                <td class="common">
	                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportCondCode);" />
	                </td>
	                <td class="titleImg">批次导入日志</td>
	            </tr>
	        </table>
        </div>
        <div id="divImportCondCode" style="display:'none'">
            <table class="common">
                <tr class="common">
                    <td class="title">导入批次号</td>
                    <td class="input">
                        <input class="common" name="ImportBatchNo" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="common">
                    	
                        <input class="cssButton" type="button" value="查  询" onclick="queryImportLog();" />
                    </td>
                    <td>
                    	<input type="button" class="cssButton" value="下载清单" onclick="downloadList();">
                    </td>
                </tr>
            </table>
        </div>
		<div>
            <!-- Mulline Code -->
            <table>
                <tr>
                    <td class="common">
                        <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportLogGrid);" />
                    </td>
                    <td class="titleImg">导入批次日志</td>
                </tr>
            </table>
            <div align="center" id="divImportLogGrid" style="display: ''">
                <table class="common">
                    <tr class="common">
                        <td>
                            <span id="spanImportLogGrid"></span> 
                        </td>
                    </tr>
                </table>
                
                <div id="divImportLogGridPage" style="display: ''" align="center">
                    <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                    <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                    
                    <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                    <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                   
                </div>
            </div>
            <!-- Mulline Code End -->
        </div>
        
        <hr />
        
        <br />
        <!--input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value="返  回" onclick="goBack();" -->
        
        <input type="hidden" id="fmOperatorFlag" name="fmOperatorFlag" />
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
