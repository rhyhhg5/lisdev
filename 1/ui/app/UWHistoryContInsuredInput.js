//               该文件中包含客户端需要处理的函数和事件
var mOperate = "";
var showInfo1;
var mDebug="0";
var cflag = "0";  //问题件操作位置
var turnPage = new turnPageClass();
var arrResult;
var Resource;
var approvefalg;
//存放添加动作执行的次数
var addAction = 0;
//暂交费总金额
var sumTempFee = 0.0;
//暂交费信息中交费金额累计
var tempFee = 0.0;
//暂交费分类信息中交费金额累计
var tempClassFee = 0.0;
//单击确定后，该变量置为真，单击添加一笔时，检验该值是否为真，为真继续，然后再将该变量置假
var confirmFlag=false;
//
var arrCardRisk;
//window.onfocus=focuswrap;
var mSwitch = parent.VD.gVSwitch;
//工作流flag
var mWFlag = 0;
//使得从该窗口弹出的窗口能够聚焦
function focuswrap() {
  myonfocus(showInfo1);
}

var contState = "C";
function initContState(){
	var sql = "select 1 from lccont where proposalcontno='" 
			+ ContNo 
			+ "' union all "
			+ "select 2 from lbcont where proposalcontno='" 
			+ ContNo 
			+ "' and edorno not like 'xb%'";
	var rs = easyExecSql(sql);
	if(rs){
		if(rs[0][0] == "2") {
			// 退保保单
			contState="B";
		} else if(rs[0][0] == "1") {
			contState="C";
		}
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterQuery( FlagStr, content ) {
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
}

//取消按钮对应操作
function cancelForm() {
}

//提交前的校验、计算
function beforeSubmit() {
  //添加操作

}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
  if(cDebug=="1") {
    parent.fraMain.rows = "0,0,50,82,*";
  } else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

//问题件录入
function QuestInput() {

 cContNo = fm.ContNo.value;  //保单号码
    if(cContNo == "") {
      alert("尚无该投保单，请先保存!");
    } else {
      window.open("../uw/QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+ LoadFlag+"&AppFlag=2","window1");
    }
  var cGrpProposalContNo = fm.GrpContNo.value;  //团体保单号码
}
//问题件查询
function QuestQuery() {
  cContNo = fm.all("ContNo").value;  //保单号码
  if(LoadFlag=="2"||LoadFlag=="4"||LoadFlag=="13") {
    if(mSwitch.getVar( "ProposalGrpContNo" )==""||mSwitch.getVar( "ProposalGrpContNo" )==null) {
      alert("请先选择一个团体主险投保单!");
      return ;
    } else {
      window.open("./GrpQuestQueryMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+LoadFlag);
    }
  } else {
    if(cContNo == "") {
      alert("尚无该投保单，请先保存!");
    } else {
      window.open("../uw/QuestQueryMain.jsp?ContNo="+cContNo+"&Flag="+LoadFlag,"window1");
    }
  }
}
//Click事件，当点击增加图片时触发该函数
function addClick() {
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick() {
	ChangeDecodeStr();
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick() {
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick() {
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow) {
}

/*********************************************************************
 *  进入险种信息录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoRiskInfo() {
  if(fm.InsuredNo.value==""||fm.ContNo.value=="") {
    alert("请先添加，选择被保人");
    return false;
  }
  if(Resource==1|Resource==2|Resource==3){
  	alert("保单明细不能进入险种明细！");
  	return false;
  }
  //mSwitch =parent.VD.gVSwitch;
  delInsuredVar();
  addInsuredVar();

  try {
    mSwitch.addVar('SelPonNo','',fm.SelPolNo.value);
  } catch(ex) {}
  ; //选择险种单进入险种界面带出已保存的信息

  if ((/*(LoadFlag=='5'||*/LoadFlag=='6'||LoadFlag=='16')&&(mSwitch.getVar( "PolNo" ) == null || mSwitch.getVar( "PolNo" ) == "")) {
    alert("请先选择被保险人险种信息！");
    return;
  }
  try {
    mSwitch.addVar('SelPolNo','',fm.SelPolNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ContNo','',fm.ContNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.updateVar('ContNo','',fm.ContNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('mainRiskPolNo');
  } catch(ex) {}
  ;
  parent.fraInterface.window.location = "./ProposalInput.jsp?LoadFlag=" + LoadFlag+"&prtNo="+prtNo + "&ContType="+ContType+"&ScanFlag="+ScanFlag+"&scantype=" + scantype+ "&MissionID=" + MissionID+ "&SubMissionID=" + SubMissionID+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&SaleChnl="+SaleChnl+"&SaleChnlDetail="+SaleChnlDetail+"&oldContNo="+oldContNo+"&ContNo="+ContNo+"&InsuredNo="+fm.InsuredNo.value;

}

/*********************************************************************
 *  删除缓存中被保险人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delInsuredVar() {
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('InsuredNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('PrtNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('GrpContNo');
  } catch(ex) {}
  ;
  //   try{mSwitch.deleteVar('AppntNo');}catch(ex){};
  //   try{mSwitch.deleteVar('ManageCom');}catch(ex){};
  try {
    mSwitch.deleteVar('ExecuteCom');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('FamilyType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('RelationToMainInsure');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('RelationToAppnt');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('AddressNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('SequenceNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Name');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Sex');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Birthday');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('IDType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('IDNo');
  } catch(ex) {}
  ;
  try {
	    mSwitch.deleteVar('InsuIDStartDate');
	  } catch(ex) {}
	  ;
  try {
		 mSwitch.deleteVar('InsuIDEndDate');
	   } catch(ex) {}
	  ;	  
	  
  try {
    mSwitch.deleteVar('RgtAddress');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Marriage');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('MarriageDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Health');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Stature');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Avoirdupois');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Degree');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('CreditGrade');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('BankCode');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('BankAccNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('AccName');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('JoinCompanyDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('StartWorkDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Position');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Salary');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('OccupationType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('OccupationCode');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('WorkType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('PluralityType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('SmokeFlag');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ContPlanCode');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Operator');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('MakeDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('MakeTime');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ModifyDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ModifyTime');
  } catch(ex) {}
  ;
}

/*********************************************************************
 *  将被保险人信息加入到缓存中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addInsuredVar() {
  try {
    mSwitch.addVar('ContNo','',fm.ContNo.value);
  } catch(ex) {}
  ;
  //alert("ContNo:"+fm.ContNo.value);
  try {
    mSwitch.addVar('InsuredNo','',fm.InsuredNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('PrtNo','',fm.PrtNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);
  } catch(ex) {}
  ;
  //   try{mSwitch.addVar('AppntNo','',fm.AppntNo.value);}catch(ex){};
  //   try{mSwitch.addVar('ManageCom','',fm.ManageCom.value);}catch(ex){};
  try {
    mSwitch.addVar('ExecuteCom','',fm.ExecuteCom.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('FamilyType','',fm.FamilyType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('RelationToMainInsure','',fm.RelationToMainInsure.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('RelationToAppnt','',fm.RelationToAppnt.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('AddressNo','',fm.AddressNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('SequenceNo','',fm.SequenceNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Name','',fm.Name.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Sex','',fm.Sex.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Birthday','',fm.Birthday.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('IDType','',fm.IDType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('IDNo','',fm.IDNo.value);
  } catch(ex) {}
  ;
  try {
	    mSwitch.addVar('InsuIDStartDate','',fm.InsuIDStartDate.value);
	  } catch(ex) {}
	  ;
  try {
		    mSwitch.addVar('InsuIDEndDate','',fm.InsuIDEndDate.value);
		  } catch(ex) {}
		  ;
  try {
    mSwitch.addVar('RgtAddress','',fm.RgtAddress.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Marriage','',fm.Marriage.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('MarriageDate','',fm.MarriageDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Health','',fm.Health.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Stature','',fm.Stature.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Avoirdupois','',fm.Avoirdupois.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Degree','',fm.Degree.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('CreditGrade','',fm.CreditGrade.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('BankCode','',fm.BankCode.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('AccName','',fm.AccName.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('JoinCompanyDate','',fm.JoinCompanyDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('StartWorkDate','',fm.StartWorkDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Position','',fm.Position.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Salary','',fm.Salary.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('OccupationType','',fm.OccupationType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('OccupationCode','',fm.OccupationCode.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('WorkType','',fm.WorkType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('PluralityType','',fm.PluralityType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('SmokeFlag','',fm.SmokeFlag.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ContPlanCode','',fm.ContPlanCode.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Operator','',fm.Operator.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('MakeDate','',fm.MakeDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('MakeTime','',fm.MakeTime.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ModifyDate','',fm.ModifyDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ModifyTime','',fm.ModifyTime.value);
  } catch(ex) {}
  ;
}

/*********************************************************************
 *  添加被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addRecord() {

  if(LoadFlag==1||LoadFlag==3||LoadFlag==5||LoadFlag==6) {
    var tPrtNo=fm.all("PrtNo").value;
    var sqlstr="select SequenceNo from LCInsured where PrtNo='"+tPrtNo+"'";
    arrResult=easyExecSql(sqlstr,1,0);
    if(arrResult!=null) {
      for(var sequencenocout=0; sequencenocout<arrResult.length;sequencenocout++ ) {
        if(fm.SequenceNo.value==arrResult[sequencenocout][0]) {
          alert("已经存在该客户内部号码");
          fm.SequenceNo.focus();
          return false;
        }
      }
    }
  }


  if(LoadFlag==1) {
    if(fm.SequenceNo.value==""||fm.SequenceNo.value>3) {
      alert("只能添加三个被保险人！");
      fm.SequenceNo.focus();
      return false;
    }
    if(fm.SequenceNo.value=="") {
      fm.SequenceNo.focus();
      alert("内部客户号码为空！");
      return false;
    }
    if(fm.Marriage.value=="") {
      fm.Marriage.focus();
      alert("请填写婚姻状况！");
      return false;
    }
    if(fm.RelationToMainInsured.value=="") {
      fm.RelationToMainInsured.focus();
      alert("请填写与第一被保险人关系！");
      return false;
    }
    if(fm.RelationToAppnt.value==""&&fm.SequenceNo.value=="1") {
      fm.RelationToAppnt.focus();
      alert("请填写与投保人关系！！");
      return false;
    }
    if ( fm.SamePersonFlag.checked==true&&fm.RelationToAppnt.value!="00")
    {
    	fm.RelationToAppnt.focus();
    	alert("投保人为被保险人本人时，与投保人关系矛盾，请检查！");
      return false;
    }
    if ( fm.SequenceNo.value=="1"&&fm.RelationToMainInsured.value!="00")
    {
    	fm.RelationToMainInsured.focus();
    	alert("客户内部号码与第一被保险人关系矛盾，请检查！");
      return false;
    }

  }
  if(!checksex())
    return false;
  if (fm.all('PolTypeFlag').value==0) {
    if( verifyInput2() == false )
      return false;
  }
  if(fm.all('IDType').value=="0") {
    var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
    if (strChkIdNo != "") {
      alert(strChkIdNo);
      return false;
    }
  }
  if(fm.all('IDType').value=="5") {
	    var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
	    if (strChkIdNo != "") {
	      alert(strChkIdNo);
	      return false;
	    }
	  }
  //if(!checkself()) return false;
  if(!checkrelation())
    return false;
  if(ImpartGrid.checkValue2(ImpartGrid.name,ImpartGrid)== false)
    return false;
  if(ImpartDetailGrid.checkValue2(ImpartDetailGrid.name,ImpartDetailGrid)== false)
    return false;

  //InputMuline1();  //保障状况告知;
  if (InputMuline1()==false)
  {
     return false;
  }
  //by gzh  校验被保人告知19条，当选“是”时，保额不能为空。
  if(!ImpartCheck119()){
  		return false;
  }
  
  ImpartGrid.delBlankLine();
  ImpartDetailGrid.delBlankLine();

//  if (fm.InsuredNo.value==''&&fm.AddressNo.value!='') {
//    alert("被保险人客户号为空，不能有地址号码");
//    return false;
//  }
  if(LoadFlag==9) {
    if(!checkifnoname())
      return false;
    if(!checkenough())
      return false;
  }
  //getImpartInfo();
  fm.all('ContType').value=ContType;
  fm.all( 'BQFlag' ).value = BQFlag;
  fm.all('fmAction').value="INSERT||CONTINSURED";
  ChangeDecodeStr();
  fm.action="./ContInsuredSave.jsp?oldContNo="+oldContNo;
  fm.submit();
}

/*********************************************************************
 *  修改被选中的被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function modifyRecord() {
  if (fm.all('PolTypeFlag').value==0 && LoadFlag!=24) {
    if( verifyInput2() == false )
      return false;
  }

  // if(!checkself()) return false;
  if(!checksex())
    return false;
  if (fm.Name.value=='') {
    alert("请选中需要修改的客户！")
    return false;
  }
  //alert("SelNo:"+InsuredGrid.getSelNo());
  if (InsuredGrid.mulLineCount==0) {
    alert("该被保险人还没有保存，无法进行修改！");
    return false;
  }
//  if (fm.InsuredNo.value==''&&fm.AddressNo.value!='') {
//    alert("被保险人客户号为空，不能有地址号码");
//    return false;
//  }
  var strSql = "select ContType from lccont where contno ='"+fm.ProposalContNo.value+"'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
  	if(arr[0][0]==1)
  	{
  		if(fm.SequenceNo.value == null || fm.SequenceNo.value == "")
  		{
  			alert("请选择是第几个被保险人!");
  			return false;
  		}
  		var insuredCount = InsuredGrid.mulLineCount;
  		var checkCount = fm.SequenceNo.value;
  		//认为如果mulline中的客户数大于等于SequenceNo 才可以修改，否则应该是保存
  		if(insuredCount<checkCount)
  		{
  			alert("请先保存客户信息才能修改！");
  			return false;
  		}
  	}
  }
  if(ImpartGrid.checkValue2(ImpartGrid.name,ImpartGrid)== false)
    return false;
  if(ImpartDetailGrid.checkValue2(ImpartDetailGrid.name,ImpartDetailGrid)== false)
    return false;

	initArray();
  //InputMuline1();  //保障状况告知;
  if (InputMuline1()==false)
  {
     return false;
  }
  //by gzh  校验被保人告知19条，当选“是”时，保额不能为空。
  if(!ImpartCheck119()){
  		return false;
  }
//增加身份证的校验
  if(BirthdaySexAppntIDNo()==false){
  	return false;
  }
  ImpartGrid.delBlankLine();
  ImpartDetailGrid.delBlankLine();
  //getImpartInfo();
  fm.all('ContType').value=ContType;
  fm.all('fmAction').value="UPDATE||CONTINSURED";
  if(LoadFlag=="24"){
		var strSQL = "select 1 from lobgrpcont where PrtNo='"+fm.PrtNo.value+"'";
		var arr = easyExecSql(strSQL);
	  	if(!arr){
		  	if(checkCanModify()){
		  		fm.action="./ContInsuredSave.jsp?LoadFlag=2";
		  		ChangeDecodeStr();
		  		fm.submit();
				}
			}else{
				ChangeDecodeStr();
				fm.submit();
			}
	}else{
		ChangeDecodeStr();
	  fm.submit();
	}
}
/*********************************************************************
 *  删除被选中的被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteRecord() {
  if (fm.InsuredNo.value=='') {
    alert("请选中需要删除的客户！")
    return false;
  }
  if (InsuredGrid.mulLineCount==0) {
    alert("该被保险人还没有保存，无法进行删除！");
    return false;
  }
//  if (fm.InsuredNo.value==''&&fm.AddressNo.value!='') {
//    alert("被保险人客户号为空，不能有地址号码");
//    return false;
//  }
  if(!confirm("是否要删除此被保险人？删除请点击“确定”，否则点“取消”。"))
  {
  	return;
  }
  //getImpartInfo();
  fm.all('ContType').value=ContType;
  fm.all('fmAction').value="DELETE||CONTINSURED";
  fm.submit();
}
/*********************************************************************
 *  返回上一页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function returnparent() {
  var backstr=fm.all("ContNo").value;

  mSwitch.addVar("PolNo", "", backstr);
  mSwitch.updateVar("PolNo", "", backstr);
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) {}
  ;

  location.href="UWHistoryContInput.jsp?ScanFlag=1&LoadFlag="+ LoadFlag + "&prtNo="+fm.all("PrtNo").value+"&scantype="+scantype+"&ContNo="+ContNo+"&ActivityID="+mSwitch.getVar("ActivityID");
}
/*********************************************************************
 *  进入险种计划界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function grpRiskPlanInfo() {
  var newWindow = window.open("../app/GroupRiskPlan.jsp");
}
/*********************************************************************
 *  代码选择后触发时间
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) {

  if(cCodeName=="AddressNo") {
    getdetailaddress()
  }

  try {
    if(cCodeName == "Relation") {
      if(fm.all('RelationToAppnt').value=="00") {
        fm.all('SamePersonFlag').checked=true;
        displayissameperson();
        isSamePerson();
      }
    }
    //如果是无名单
    if( cCodeName == "PolTypeFlag") {
      DivGrpNoname.style.display='';
      if (Field.value!='0') {
        fm.all('InsuredPeoples').readOnly=false;
        fm.all('InsuredAppAge').readOnly=false;
      } else {
        fm.all('InsuredPeoples').readOnly=true;
        fm.all('InsuredAppAge').readOnly=true;
      }
      if(Field.value=='1') {
        FillNoName();
      }
    }
    if( cCodeName == "ImpartCode") {
    }
    if( cCodeName == "SequenceNo") {
      if(Field.value=="1"&&fm.SamePersonFlag.checked==false) {
        emptyInsured();
        param="121";
        fm.pagename.value="121";
        fm.InsuredSequencename.value="第一被保险人资料";
        fm.RelationToMainInsured.value="00";
      }
      if(Field.value=="2"&&fm.SamePersonFlag.checked==false) {
        if(InsuredGrid.mulLineCount==0) {
          alert("请先添加第一被保人");
          fm.SequenceNo.value="1";
          return false;
        }
        emptyInsured();
        noneedhome();
        param="122";
        fm.pagename.value="122";
        fm.InsuredSequencename.value="第二被保险人资料";
      }
      if(Field.value=="3"&&fm.SamePersonFlag.checked==false) {
        if(InsuredGrid.mulLineCount==0) {
          alert("请先添加第一被保人");
          Field.value="1";
          return false;
        }
        if(InsuredGrid.mulLineCount==1) {
          alert("请先添加第二被保人");
          Field.value="1";
          return false;
        }
        emptyInsured();
        noneedhome();
        param="123";
        fm.pagename.value="123";
        fm.InsuredSequencename.value="第三被保险人资料";
      }
      if (scantype== "scan") {
        setFocus();
      }
      initArray();
    }
    if( cCodeName == "CheckPostalAddress") {
    var PostalAddressSQL="select PostalAddress from lcaddress where customerno in(select appntno from lccont where prtno ='"+fm.all( 'PrtNo' ).value+"')  and AddressNo =(select char(max(int(AddressNo))) from LCAddress where customerno in(select appntno from lccont where prtno='"+fm.all( 'PrtNo' ).value+"'))";
    var sqlstr=easyExecSql(PostalAddressSQL,1,0);
    if(sqlstr){
			//alert();
    	if(fm.CheckPostalAddress.value=="1") {
    		fm.all('PostalAddress').value=sqlstr;
    		fm.all('ZipCode').value="";
    		}else if(fm.CheckPostalAddress.value=="2"){
    		fm.all('PostalAddress').value="";
    		fm.all('ZipCode').value="";    		
    		}
  		}else{
    	fm.all('PostalAddress').value="";
    	fm.all('ZipCode').value="";    
    	}  
    }
    if(cCodeName=="OccupationCode") {
      //alert();
      var t = Field.value;
      var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+t+"'";
      //alert(strSql);
      var arr = easyExecSql(strSql);
      if( arr ) {
        fm.OccupationType.value = arr[0][0];
      }
    }
    if(cCodeName=="AccountNo") {
    	if(Field.value=="1") {
		    if(mSwitch.getVar("AppntBankAccNo"))
		      fm.all('BankAccNo').value=mSwitch.getVar("AppntBankAccNo");
		    if(mSwitch.getVar("AppntBankCode"))
		      fm.all('BankCode').value=mSwitch.getVar("AppntBankCode");
		    if(mSwitch.getVar("AppntAccName"))
		      fm.all('AccName').value=mSwitch.getVar("AppntAccName");
		   }
	  }
  } catch(ex) {}

}
/*********************************************************************
 *  显示家庭单下被保险人的信息
 *  返回值：  无
 *********************************************************************
 */
function getInsuredInfo() {
  var ContNo=fm.all("ContNo").value;
  if(ContNo!=null&&ContNo!="") {
    var strSQL ="select InsuredNo,Name,Sex,Birthday,RelationToMainInsured,SequenceNo from L" + contState + "Insured where ContNo='"+ContNo+"' ";
    turnPage.queryModal(strSQL,InsuredGrid);					
    if(InsuredGrid.mulLineCount > 0)
    {
        InsuredGrid.radioBoxSel(1);
        getInsuredDetail();
    }
  }
}

/*********************************************************************
 *  获得个单合同的被保人信息
 *  返回值：  无
 *********************************************************************
 */
function getProposalInsuredInfo() {
  var tContNo=fm.all("ContNo").value;
  Resource=fm.Resource.value;
  if(Resource==2){
  var StrSQL="select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate  from L" + contState + "Insured where ContNo='"+tContNo+"' "                                              
 						+" union "
						+"select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate  from LobInsured where ContNo='"+tContNo+"' "                                               						
 	;
 	}else if(Resource==1){
  var StrSQL="select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate  from LbInsured where ContNo='"+tContNo+"' "                                              
 	;
 	}else if(Resource==3){
  var StrSQL="select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate  from L" + contState + "Insured where ContNo='"+tContNo+"' "                                              
 	;
 	}else{
  var StrSQL="select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate  from L" + contState + "Insured where ContNo='"+tContNo+"' "                                              
 	; 	
 	}				
  arrResult=easyExecSql(StrSQL,1,0);

	
  if(arrResult==null) {
    //alert("未得到被投保人信息");
    return;
  } else {
    DisplayInsured();//该合同下的被投保人信息
    var tCustomerNo = arrResult[0][2];		// 得到投保人客户号
    var tAddressNo = arrResult[0][10]; 		// 得到投保人地址号
    arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo='"+tCustomerNo+"'",1,0);
    if(arrResult==null) {
      //alert("未得到用户信息");
      //return;
    } else {
      //displayAppnt();       //显示投保人详细内容
      emptyUndefined();
      fm.AddressNo.value=tAddressNo;
      getdetailaddress();//显示投保人地址详细内容
    }
  }
  getInsuredPolInfo();
  getImpartInfo();
  getImpartDetailInfo();
}

/*********************************************************************
 *  MulLine的RadioBox点击事件，获得被保人详细信息，填入被保人信息表
 *  返回值：  无
 *********************************************************************
 */
function getInsuredDetail(parm1,parm2) {
    var oneRowResult = null;
    if(parm1 == null || parm1 == "" || parm1 == "undefined")
    {
        var tSelNo = InsuredGrid.getSelNo() - 1;

        if("-1" == tSelNo)
        {
            alert("未有选择的被保人信息！");
            return false;
        }
        
        oneRowResult = InsuredGrid.getRowData(tSelNo);
    }
    
    if(oneRowResult != null)
    {
        fm.SequenceNo.value = oneRowResult[5];
    }
    else
    {
        fm.SequenceNo.value=fm.all(parm1).all('InsuredGrid6').value;
    }
    
  if(fm.SequenceNo.value=="1") {
    fm.pagename.value="121";
    fm.InsuredSequencename.value="第一被保险人资料";
  }
  if(fm.SequenceNo.value=="2") {
    fm.pagename.value="122";
    fm.InsuredSequencename.value="第二被保险人资料";
  }
  if(fm.SequenceNo.value=="3") {
    fm.pagename.value="123";
    fm.InsuredSequencename.value="第三被保险人资料";
  }
  if (scantype== "scan") {
    setFocus();
  }
	var InsuredNo = null;
	if(oneRowResult != null)
    {
        InsuredNo = oneRowResult[0];
    }
    else
    {
        InsuredNo = fm.all(parm1).all('InsuredGrid1').value;
    }
  var ContNo = fm.ContNo.value;
  //被保人详细信息
  var strSQL ="select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo='"+InsuredNo+"'";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null) {
    displayAppnt();
  }
  strSQL ="select GRPCONTNO ,CONTNO ,INSUREDNO ,PRTNO ,APPNTNO ,MANAGECOM ,EXECUTECOM ,FAMILYID ,RELATIONTOMAININSURED ,RELATIONTOAPPNT ,ADDRESSNO ,SEQUENCENO ,NAME ,SEX ,BIRTHDAY ,IDTYPE ,IDNO ,NATIVEPLACE ,NATIONALITY ,RGTADDRESS ,MARRIAGE ,MARRIAGEDATE ,HEALTH ,STATURE ,AVOIRDUPOIS ,DEGREE ,CREDITGRADE ,BANKCODE ,BANKACCNO ,ACCNAME ,JOINCOMPANYDATE ,STARTWORKDATE ,POSITION ,SALARY ,OCCUPATIONTYPE ,OCCUPATIONCODE ,WORKTYPE ,PLURALITYTYPE ,SMOKEFLAG ,CONTPLANCODE ,OPERATOR ,INSUREDSTAT ,MAKEDATE ,MAKETIME ,MODIFYDATE ,MODIFYTIME ,UWFLAG ,UWCODE ,UWDATE ,UWTIME ,BMI ,INSUREDPEOPLES ,CONTPLANCOUNT ,DISKIMPORTNO ,OTHIDTYPE ,OTHIDNO ,ENGLISHNAME ,GRPINSUREDPHONE ,IDSTARTDATE ,IDENDDATE " +
  		" from L" + contState +"Insured where ContNo = '"+ContNo+"' and InsuredNo='"+InsuredNo+"'";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null) {
    DisplayInsured();
  }

  var tAddressNo = arrResult[0][10]; 		// 得到被保人地址号
  fm.AddressNo.value=tAddressNo;
  getdetailaddress();
  getInsuredPolInfo();

  getImpartInfo();

  getImpartDetailInfo();

  initArray();


}
/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayCustomer() {
  try {
    fm.all('Nationality').value= arrResult[0][8];
  } catch(ex) {}
  ;

}
/*********************************************************************
 *  把查询返回的客户地址数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayAddress() {
  try {
    fm.all('PostalAddress').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= arrResult[0][14];
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= arrResult[0][16];
  } catch(ex) {}
  ;
  try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){};
  try {
    fm.all('GrpPhone').value= arrResult[0][12];
  } catch(ex) {}
  ;
  try {
    fm.all('CompanyAddress').value= arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][11];
  } catch(ex) {}
  ;
}
/*********************************************************************
 *  显示被保人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayInsured() {
  try {
    fm.all('GrpContNo').value=arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('ContNo').value=arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('InsuredNo').value=arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('PrtNo').value=arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('AppntNo').value=arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('ManageCom').value=arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('ExecuteCom').value=arrResult[0][6];
  } catch(ex) {}
  ;
  try {
    fm.all('FamilyID').value=arrResult[0][7];
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToMainInsured').value=arrResult[0][8];
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToAppnt').value=arrResult[0][9];
  } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value=arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('SequenceNo').value=arrResult[0][11];
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value=arrResult[0][12];
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value=arrResult[0][13];fm.all('SexName').value=easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'sex' and Code='"+arrResult[0][13]+"'");
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value=arrResult[0][14];
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value=arrResult[0][15];
  } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value=arrResult[0][16];
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value=arrResult[0][17];
    var arr = easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'nativeplace' and Code='"+arrResult[0][17]+"'");
    if(arr)
    fm.all('NativePlaceName').value=arr;
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value=arrResult[0][18];
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value=arrResult[0][19];
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value=arrResult[0][20];fm.all('MarriageName').value=easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'marriage' and Code='"+arrResult[0][20]+"'");
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value=arrResult[0][21];
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value=arrResult[0][22];
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value=arrResult[0][23];
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value=arrResult[0][24];
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value=arrResult[0][25];
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value=arrResult[0][26];
  } catch(ex) {}
  ;
  try {
    fm.all('BankCode').value=arrResult[0][27];
  } catch(ex) {}
  ;
  try {
    fm.all('BankAccNo').value=arrResult[0][28];
  } catch(ex) {}
  ;
  try {
    fm.all('AccName').value=arrResult[0][29];
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value=arrResult[0][30];
  } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value=arrResult[0][31];
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value=arrResult[0][32];
  } catch(ex) {}
  ;
  try {
  if(arrResult[0][33]=='-1'){
	try{fm.all('Salary').value=""; }catch(ex){};               
	}else{
	try{fm.all('Salary').value= arrResult[0][33]; }catch(ex){};
	}
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value=arrResult[0][34];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value=arrResult[0][35];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][35]);
  } catch(ex) { }
  ;
  try {
    fm.all('WorkType').value=arrResult[0][36];
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value=arrResult[0][37];
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value=arrResult[0][38];
  } catch(ex) {}
  ;
  try {
    fm.all('ContPlanCode').value=arrResult[0][39];
  } catch(ex) {}
  ;
  try {
    fm.all('Operator').value=arrResult[0][40];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value=arrResult[0][41];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value=arrResult[0][42];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value=arrResult[0][43];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value=arrResult[0][44];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value=arrResult[0][44];
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDNo').value=arrResult[0][55];
  } catch(ex) {}
  ;
  try {
    fm.all('EnglishName').value=arrResult[0][56];
  } catch(ex) {}
  ;
  try {
	    fm.all('InsuIDStartDate').value=arrResult[0][58];
	  } catch(ex) {}
	  ;
  try {
		    fm.all('InsuIDEndDate').value=arrResult[0][59];
		  } catch(ex) {}
		  ;
}
function displayissameperson() {
  try {
    fm.all('InsuredNo').value= mSwitch.getVar( "AppntNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value= mSwitch.getVar( "AppntName" );
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value= mSwitch.getVar( "AppntSex" );
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value= mSwitch.getVar( "AppntBirthday" );
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value= mSwitch.getVar( "AppntIDType" );
  } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value= mSwitch.getVar( "AppntIDNo" );
  } catch(ex) {}
  ;
  try {
	    fm.all('InsuIDStartDate').value= mSwitch.getVar( "AppIDStartDate" );
	  } catch(ex) {}
	  ;
	  try {
	    fm.all('InsuIDEndDate').value= mSwitch.getVar( "AppIDEndDate" );
	  } catch(ex) {}
	  ;
  try {
    fm.all('Password').value= mSwitch.getVar( "AppntPassword" );
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value= mSwitch.getVar( "AppntNativePlace" );
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value= mSwitch.getVar( "AppntNationality" );
  } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value= mSwitch.getVar( "AddressNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value= mSwitch.getVar( "AppntRgtAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value= mSwitch.getVar( "AppntMarriage" );
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value= mSwitch.getVar( "AppntMarriageDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value= mSwitch.getVar( "AppntHealth" );
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value= mSwitch.getVar( "AppntStature" );
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value= mSwitch.getVar( "AppntAvoirdupois" );
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value= mSwitch.getVar( "AppntDegree" );
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value= mSwitch.getVar( "AppntDegreeCreditGrade" );
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDType').value= mSwitch.getVar( "AppntOthIDType" );
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDNo').value= mSwitch.getVar( "AppntOthIDNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('ICNo').value= mSwitch.getVar( "AppntICNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpNo').value= mSwitch.getVar( "AppntGrpNo" );
  } catch(ex) {}
  ;
  try {
    fm.all( 'JoinCompanyDate' ).value = mSwitch.getVar( "JoinCompanyDate" );
    if(fm.all( 'JoinCompanyDate' ).value=="false") {
      fm.all( 'JoinCompanyDate' ).value="";
    }
  } catch(ex) { }
  ;
  try {
    fm.all('StartWorkDate').value= mSwitch.getVar( "AppntStartWorkDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= mSwitch.getVar( "AppntPosition" );
  } catch(ex) {}
  ;
  try {
    fm.all('Salary').value= mSwitch.getVar( "AppntSalary" );
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= mSwitch.getVar( "AppntOccupationType" );
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value= mSwitch.getVar( "AppntOccupationCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('WorkType').value= mSwitch.getVar( "AppntWorkType" );
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value= mSwitch.getVar( "AppntPluralityType" );
  } catch(ex) {}
  ;
  try {
    fm.all('DeathDate').value= mSwitch.getVar( "AppntDeathDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value= mSwitch.getVar( "AppntSmokeFlag" );
  } catch(ex) {}
  ;
  try {
    fm.all('BlacklistFlag').value= mSwitch.getVar( "AppntBlacklistFlag" );
  } catch(ex) {}
  ;
  try {
    fm.all('Proterty').value= mSwitch.getVar( "AppntProterty" );
  } catch(ex) {}
  ;
  try {
    fm.all('Remark').value= mSwitch.getVar( "AppntRemark" );
  } catch(ex) {}
  ;
  try {
    fm.all('State').value= mSwitch.getVar( "AppntState" );
  } catch(ex) {}
  ;
  try {
    fm.all('Operator').value= mSwitch.getVar( "AppntOperator" );
  } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value= mSwitch.getVar( "AppntMakeDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value= mSwitch.getVar( "AppntMakeTime" );
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value= mSwitch.getVar( "AppntModifyDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value= mSwitch.getVar( "AppntModifyTime" );
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value= mSwitch.getVar( "AppntPostalAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value= mSwitch.getVar( "AppntPostalAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= mSwitch.getVar( "AppntZipCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= mSwitch.getVar( "AppntPhone" );
  } catch(ex) {}
  ;
  try {
    fm.all('Fax').value= mSwitch.getVar( "AppntFax" );
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= mSwitch.getVar( "AppntMobile" );
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= mSwitch.getVar( "AppntEMail" );
  } catch(ex) {}
  ;
  try {
  fm.all('GrpName').value= mSwitch.getVar( "AppntGrpName" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpPhone').value= mSwitch.getVar( "AppntGrpPhone" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value= mSwitch.getVar( "CompanyAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= mSwitch.getVar( "AppntGrpZipCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpFax').value= mSwitch.getVar( "AppntGrpFax" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= mSwitch.getVar( "AppntHomeAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomePhone').value= mSwitch.getVar( "AppntHomePhone" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= mSwitch.getVar( "AppntHomeZipCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeFax').value= mSwitch.getVar( "AppntHomeFax" );
  } catch(ex) {}
  ;
}
/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartInfo() {
  initImpartGrid();
  var InsuredNo=fm.all("InsuredNo").value;
  var ContNo=fm.all("ContNo").value;
  //告知信息初始化
  if(InsuredNo!=null&&InsuredNo!="") {
    var strSQL ="select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from L" + contState + "CustomerImpart where CustomerNo='"+InsuredNo+"' and ProposalContNo='"+fm.ProposalContNo.value+"' and CustomerNoType='I'";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      clearImpart();
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = ImpartGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
  clearImpart();    //清空
  getImpartbox();   //保障状况告知※健康告知
}
/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartDetailInfo() {
  initImpartDetailGrid();
  var InsuredNo=fm.all("InsuredNo").value;
  var ContNo=fm.all("ContNo").value;
  //告知信息初始化
  if(InsuredNo!=null&&InsuredNo!="") {
    var strSQL ="select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from L" + contState + "CustomerImpartDetail where CustomerNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and CustomerNoType='I'";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = ImpartDetailGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
}
/*********************************************************************
 *  获得被保人险种信息，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getInsuredPolInfo() {
  initPolGrid();
  var InsuredNo=fm.all("InsuredNo").value;
  var ContNo=fm.all("ContNo").value;
  //险种信息初始化
  if(InsuredNo!=null&&InsuredNo!="") {
    var strSQL ="select PolNo,RiskCode,Prem,Amnt,SupplementaryPrem from LCPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
    					 +" union "
    					 +"select PolNo,RiskCode,Prem,Amnt,SupplementaryPrem from LobPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
    					 +" union "
    					 +"select PolNo,RiskCode,Prem,Amnt,SupplementaryPrem from LbPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
    					 ;
    if(ContType==1){
    	strSQL ="select PolNo,RiskCode,Prem,Amnt,SupplementaryPrem from L" + contState + "Pol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
    					 ;
    	}
    //alert(strSQL);
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
}
/*********************************************************************
 *  MulLine的RadioBox点击事件，获得被保人险种详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolDetail(parm1,parm2) {
  var PolNo=fm.all(parm1).all('PolGrid1').value
            try {
              mSwitch.deleteVar('PolNo')
            } catch(ex) {}
            ;
  try {
    mSwitch.addVar('PolNo','',PolNo);
  } catch(ex) {}
  ;
  fm.SelPolNo.value=PolNo;
}
/*********************************************************************
 *  根据家庭单类型，隐藏界面控件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function choosetype() {
  if(fm.FamilyType.value=="1")
    divTempFeeInput.style.display="";
  if(fm.FamilyType.value=="0")
    divTempFeeInput.style.display="none";
}
/*********************************************************************
 *  校验被保险人与主被保险人关系
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function checkself() {
  if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value=="") {
    fm.RelationToMainInsured.value="00";
    return true;
  } else if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value!="00") {
    alert("个人单中'与第一被保险人关系'只能是'本人'");
    fm.RelationToMainInsured.value="00";
    return false;
  } else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value==""&&InsuredGrid.mulLineCount==0) {
    fm.RelationToMainInsured.value="00";
    return true;
  } else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value!="00"&&InsuredGrid.mulLineCount==0) {
    alert("家庭单中第一位被保险人的'与第一被保险人关系'只能是'本人'");
    fm.RelationToMainInsured.value="00";
    return false;
  } else
    return true;
}
/*********************************************************************
 *  校验保险人
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function checkrelation() {
  if(LoadFlag==2||LoadFlag==7) {
    if (fm.all('ContNo').value != "") {
      alert("团单的个单不能有多被保险人");
      return false;
    } else
      return true;
  } else {
    if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="0") {
      var strSQL="select * from LCInsured where contno='"+fm.all('ContNo').value +"'";
      turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
      if(turnPage.strQueryResult) {
        alert("个单不能有多被保险人");
        return false;
      } else
        return true;
    }
    //else if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="1"&&InsuredGrid.mulLineCount>0&&fm.RelationToMainInsured.value=="00")
    //{
    //    alert("家庭单只能有一个第一被保险人");
    //    return false;
    //}
    else if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="1"&&fm.RelationToAppnt.value=="00") {
      var strSql="select * from LCInsured where contno='"+fm.all('ContNo').value +"' and RelationToAppnt='00' ";
      turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);
      if(turnPage.strQueryResult) {
        alert("投保人已经是该保单下的被保险人");
        return false;
      } else
        return true;
    } else
      return true;
  }
  //select count(*) from ldinsured

}
/*********************************************************************
 *  投保人与被保人相同选择框事件
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function isSamePerson() {
  //对应未选同一人，又打钩的情况
  if ( fm.SamePersonFlag.checked==true&&fm.RelationToAppnt.value!="00") {
    fm.all('DivLCInsured').style.display = "none";
    divLCInsuredPerson.style.display = "none";
    divSalary.style.display = "none";
    fm.SamePersonFlag.checked = true;
    fm.RelationToAppnt.value="00"
                             displayissameperson();
  }
  //对应是同一人，又打钩的情况
  else if (fm.SamePersonFlag.checked == true) {
    fm.all('DivLCInsured').style.display = "none";
    divLCInsuredPerson.style.display = "none";
    divSalary.style.display = "none";
    displayissameperson();
  }
  //对应不选同一人的情况
  else if (fm.SamePersonFlag.checked == false) {
    fm.all('DivLCInsured').style.display = "";
    emptyInsured();
//    try {
//      fm.all('Name').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Sex').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Birthday').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('IDType').value= "0";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('IDNo').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Password').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('NativePlace').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Nationality').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('RgtAddress').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Marriage').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('MarriageDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Health').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Stature').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Avoirdupois').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Degree').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('CreditGrade').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OthIDType').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OthIDNo').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('ICNo').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpNo').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('JoinCompanyDate').value= "";
//    } catch(ex) {}
//    try {
//      fm.all('StartWorkDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Position').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Salary').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OccupationType').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OccupationCode').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OccupationName').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('WorkType').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('PluralityType').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('DeathDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('SmokeFlag').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('BlacklistFlag').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Proterty').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Remark').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('State').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Operator').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('MakeDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('MakeTime').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('ModifyDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('ModifyTime').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('PostalAddress').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('PostalAddress').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('ZipCode').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Phone').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Mobile').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('EMail').value="";
//    } catch(ex) {}
//    ;
//    try {
//   fm.all('GrpName').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpPhone').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpAddress').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpZipCode').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('HomeAddress').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('HomeZipCode').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('HomePhone').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('HomeFax').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpFax').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Fax').value= "";
//    } catch(ex) {}
//    ;

  }
}
/*********************************************************************
 *  投保人客户号查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryInsuredNo() {
  if (fm.all("InsuredNo").value == "") {
    showAppnt1();
  } else {
    arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + fm.all("InsuredNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到投保人信息");
      displayAppnt(new Array());
      emptyUndefined();
    } else {
      displayAppnt(arrResult[0]);
      getaddresscodedata();
    }
  }
}
/*********************************************************************
 *  投保人查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt1() {
  if( mOperate == 0 ) {
    mOperate = 2;
    showInfo = window.open( "../sys/LDPersonQueryNew.html" );
  }
}
/*********************************************************************
 *  显示投保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt() {
  try {
    fm.all('InsuredNo').value= arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value= arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('IDTypeName').value= getNameFromLDCode("IDType",arrResult[0][4]);
  } catch(ex) { }
  ;
  try {
    fm.all('IDNo').value= arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('Password').value= arrResult[0][6];
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value= arrResult[0][7];
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value= arrResult[0][8];
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value= arrResult[0][9];
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value= arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value= arrResult[0][11];
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value= arrResult[0][12];
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value= arrResult[0][13];
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value= arrResult[0][14];
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value= arrResult[0][15];
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value= arrResult[0][16];
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDType').value= arrResult[0][17];
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDNo').value= arrResult[0][18];
  } catch(ex) {}
  ;
  try {
    fm.all('ICNo').value= arrResult[0][19];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpNo').value= arrResult[0][20];
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value= arrResult[0][21];
  } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value= arrResult[0][22];
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= arrResult[0][23];
  } catch(ex) {}
  ;
  try {
	if(arrResult[0][24]=='-1'){
	try{fm.all('Salary').value=""; }catch(ex){};               
	}else{
	try{fm.all('Salary').value= arrResult[0][24]; }catch(ex){};
	}
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= arrResult[0][25];
  } catch(ex) {}
  ;
  //alert( arrResult[0][25]);
  try {
    fm.all('OccupationCode').value= arrResult[0][26];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][26]);
  } catch(ex) { }
  ;
  try {
    fm.all('WorkType').value= arrResult[0][27];
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value= arrResult[0][28];
  } catch(ex) {}
  ;
  try {
    fm.all('DeathDate').value= arrResult[0][29];
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value= arrResult[0][30];
  } catch(ex) {}
  ;
  try {
    fm.all('BlacklistFlag').value= arrResult[0][31];
  } catch(ex) {}
  ;
  try {
    fm.all('Proterty').value= arrResult[0][32];
  } catch(ex) {}
  ;
  try {
    fm.all('Remark').value= arrResult[0][33];
  } catch(ex) {}
  ;
  try {
    fm.all('State').value= arrResult[0][34];
  } catch(ex) {}
  ;
  try {
    fm.all('Operator').value= arrResult[0][35];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value= arrResult[0][36];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value= arrResult[0][37];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value= arrResult[0][38];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value= arrResult[0][39];
  } catch(ex) {}
  ;
  try {
 fm.all('GrpName').value= arrResult[0][41];
  } catch(ex) {}
  ;
  //地址显示部分的变动
  try {
    fm.all('AddressNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value=  "";
  } catch(ex) {}
  ;
 //try{fm.all('GrpName').value= arrResult[0][46];}catch(ex){};
  try {
    fm.all('GrpPhone').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomePhone').value=  "";
  } catch(ex) {}
  ;
}
/*********************************************************************
 *  查询返回后触发
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {

  //alert("here:" + arrQueryResult + "\n" + mOperate);
  if( arrQueryResult != null ) {
    arrResult = arrQueryResult;

    if( mOperate == 1 ) {		// 查询投保单
      fm.all( 'ContNo' ).value = arrQueryResult[0][0];

      arrResult = easyExecSql("select ProposalContNo,PrtNo,ManageCom,SaleChnl,AgentCode,AgentGroup,AgentCode1,AgentCom,AgentType,Remark from LCCont where LCCont = '" + arrQueryResult[0][0] + "'", 1, 0);

      if (arrResult == null) {
        alert("未查到投保单信息");
      } else {
        displayLCContPol(arrResult[0]);
      }
    }

    if( mOperate == 2 ) {		// 投保人信息
      //arrResult = easyExecSql("select a.*,b.AddressNo,b.PostalAddress,b.ZipCode,b.HomePhone,b.Mobile,b.EMail,a.GrpNo,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode from LDPerson a,LCAddress b where 1=1 and a.CustomerNo=b.CustomerNo and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      if (arrResult == null) {
        alert("未查到投保人信息");
      } else {
        displayAppnt(arrResult[0]);
        getaddresscodedata();
      }
    }
  }
  mOperate = 0;		// 恢复初态
}
/*********************************************************************
 *  查询职业类别
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getdetailwork() {
  var strSql = "select OccupationType from LDOccupation where OccupationCode='" + fm.OccupationCode.value+"'";
  var arrResult = easyExecSql(strSql);
  //alert(arrResult[0][0]);
  if (arrResult != null) {
    //alert(fm.OccupationType.value);
    fm.OccupationType.value = arrResult[0][0];
  }
}
/*获得个人单信息，写入页面控件
function getProposalInsuredInfo(){
  var ContNo = fm.ContNo.value;
  //被保人详细信息
  var strSQL ="select * from ldperson where CustomerNo in (select InsuredNo from LCInsured where ContNo='"+ContNo+"')";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null){
  	DisplayCustomer();
  }

  strSQL ="select * from LCInsured where ContNo = '"+ContNo+"'";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null){
    DisplayInsured();
  }else{
    return;
  }

  var tAddressNo = arrResult[0][10]; 		// 得到被保人地址号
  var InsuredNo=arrResult[0][2];
  var strSQL="select * from LCAddress where AddressNo='"+tAddressNo+"' and CustomerNo='"+InsuredNo+"'";
  arrResult=easyExecSql(strSQL);
    if(arrResult!=null){
  	DisplayAddress();
    }

    getInsuredPolInfo();

}*/


/*********************************************************************
 *  把合同所有信息录入结束确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function inputConfirm(wFlag) {
    if(!checkSaleChnl())
    {
        return false;
    }
    //险种校验
    if(!checkRiskInfo())
    {
        return false;
    }
    
    //银保渠道与险种校验
    if(!checkSaleChnlRisk())
    {
        return false;
    }
    //校验小于18周岁的被保人是否选择了第19项告知
    if(!checkGZ19()){
    	return false;
    }
    //当19项告知选择“是”时，19项告知保额内容不能为空，且必须是数字
	if(!checkGZ19Prem()){
		return false;
	}         
    
    //保单保险期间的校验
    if(!checkInsuYear())
    {
    	return false;
    }
    
    if (wFlag ==1 ) //录入完毕确认
    {
        var tStr= "	select * from lwmission where 1=1 and lwmission.missionprop1 = '"+fm.ContNo.value+"'";
        turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
        if (turnPage.strQueryResult) {
            alert("该合同已经做过保存！");
            return;
        }
        fm.AppntNo.value = AppntNo;
        fm.AppntName.value = AppntName;
        fm.WorkFlowFlag.value = "7999999999";
    }
    else if (wFlag ==2)//复核完毕确认
    {
    	//增加身份证的校验
        if(BirthdaySexAppntIDNo()==false){
        	return false;
        }
        //增加Polapplydate不能为空的校验
        var arrResult1 = easyExecSql("select polapplydate from LCCont where prtNo='"+fm.PrtNo.value+"' " );
        if(arrResult1 == null){
        	alert("投保日期为空,不能通过复核!");
        	return false;
        }
        if(fm.all('ProposalContNo').value == "")
        {
            alert("未查询出合同信息,不容许您进行 [复核完毕] 确认！");
            return;
        }

        // 复核完毕时，对先收费的保单，对保费到帐情况进行校验。
        if(!checkEnterAccOfTempFee())
        {
            return false;
        }
        // ---------------------------
        
        //黑名单校验 20120503  gzh
        if(!checkBlackName()){
        	return false;
        }
        
        if(!checkAppnt(wFlag)){
        	return false;
        }
     //校验投保人账户信息
       if(!checkBank()){
        	return false;
        }
        fm.WorkFlowFlag.value = "0000001001";					//复核完毕
        fm.MissionID.value = MissionID;
        fm.SubMissionID.value = SubMissionID;
        approvefalg="2";
        
        //阳光宝贝只有五家机构（北京、上海、江苏、辽宁、深圳）可以销售
        //var arrResult = easyExecSql("select ManageCom from LCPol where prtNo='"+fm.PrtNo.value+"' " 
        //                          + " and riskcode in('320106','120706') fetch first 1 rows only");
        //if(arrResult){
        //	if(arrResult[0][0].substr(0,4) != "8611"&&arrResult[0][0].substr(0,4) != "8621"&&arrResult[0][0].substr(0,4) != "8631"&&arrResult[0][0].substr(0,4) != "8632"&&arrResult[0][0].substr(0,4) != "8695"){
        //		alert("该管理机构"+arrResult[0][0]+"没有销售阳光宝贝的权限!!");
        //		return false;
        //	}
        //}
    }
    else if (wFlag ==3)
    {
        if(fm.all('ProposalContNo').value == "") {
            alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
            return;
        }
        fm.WorkFlowFlag.value = "0000001002";
        fm.MissionID.value = tMissionID;
        fm.SubMissionID.value = tSubMissionID;
    }
    else
        return;

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./InputConfirm.jsp";
    fm.submit(); //提交
}
/*********************************************************************
 *  查询被保险人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getdetailaddress() {
  var strSQL="select b.AddressNo,b.PostalAddress,b.ZipCode,b.Phone,b.Mobile,b.EMail,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode,b.HomeAddress,b.HomeZipCode,b.HomePhone,b.GrpName from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.InsuredNo.value+"'";
  arrResult=easyExecSql(strSQL);
  try {
    fm.all('AddressNo').value= arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value= arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpPhone').value= arrResult[0][6];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value= arrResult[0][7];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][8];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= arrResult[0][9];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('HomePhone').value= arrResult[0][11];
  } catch(ex) {}
  ;
  try {

 fm.all('GrpName').value= arrResult[0][12];
  } catch(ex) {}
  ; //已经在 LDPerson 里里面查询出来了
}

/*********************************************************************
 *  查询保险计划
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getContPlanCode(tProposalGrpContNo) {
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select ContPlanCode,ContPlanName from LCContPlan where ContPlanCode<>'00' and ContPlanCode<>'11' and ProposalGrpContNo='"+tProposalGrpContNo+"'";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
    divContPlan.style.display="";
  } else {
    //alert("保险计划没查到");
    divContPlan.style.display="none";
  }
  //alert ("tcodedata : " + tCodeData);
  return tCodeData;
}

/*********************************************************************
 *  查询处理机构
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getExecuteCom(tProposalGrpContNo) {
  //alert("1");
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select ExecuteCom,Name from LCGeneral a,LDCom b where a.GrpContNo='"+tProposalGrpContNo+"' and a.ExecuteCom=b.ComCode";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
    divExecuteCom.style.display="";
  } else {
    divExecuteCom.style.display="none";
  }
  //alert ("tcodedata : " + tCodeData);

  return tCodeData;
}

function emptyInsured() {

  try {
    fm.all('InsuredNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('ExecuteCom').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('FamilyID').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToMainInsured').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToAppnt').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value= "";
  } catch(ex) {}
  ;
//  try
//  {
//  	fm.all('SequenceNo').value= "";
//  }
//  catch(ex)
//  {}
//  ;
  try {
    fm.all('Name').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('SexName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value= "0";
  } catch(ex) {}
  ;
  try {
    fm.all('IDTypeName').value= "身份证";
  } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlaceName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('BankCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('BankAccNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('AccName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Salary').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('WorkType').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('ContPlanCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomePhone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeFax').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpFax').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Fax').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('CheckPostalAddress').value= "";
  } catch(ex) {}
  ;
  emptyAddress();
  ImpartGrid.clearData();
  ImpartGrid.addOne();
  ImpartDetailGrid.clearData();
  //ImpartDetailGrid.addOne();
  clearImpart();
  initArray();
}

/*********************************************************************
 *  清空客户地址数据
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function emptyAddress() {
  try {
    fm.all('PostalAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= "";
  } catch(ex) {}
  ;
  //try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){};
  try{
  	fm.all('GrpName').value="";
  }catch(ex){}
  ;
  try {
    fm.all('GrpPhone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= "";
  } catch(ex) {}
  ;
}
/*********************************************************************
 *  根据身份证号取得出生日期和性别
 *  参数  ：  身份证号
 *  返回值：  无
 *********************************************************************
 */

function getBirthdaySexByIDNo(iIdNo) {
  if(fm.all('IDType').value=="0") {
    fm.all('Birthday').value=getBirthdatByIdNo(iIdNo);
    fm.all('Sex').value=getSexByIDNo(iIdNo);
  }
  if(fm.all('IDType').value=="5") {
	    fm.all('Birthday').value=getBirthdatByIdNo(iIdNo);
	    fm.all('Sex').value=getSexByIDNo(iIdNo);
	  }
}
/*********************************************************************
 *  合同信息录入完毕确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpInputConfirm(wFlag) {
  mWFlag = 1;
  if (wFlag ==1 ) //录入完毕确认
  {
    var tStr= "	select * from lwmission where 1=1 "
              +" and lwmission.processid = '0000000004'"
              +" and lwmission.activityid = '0000002001'"
              +" and lwmission.missionprop1 = '"+fm.ProposalGrpContNo.value+"'";
    turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
    if (turnPage.strQueryResult) {
      alert("该团单合同已经做过保存！");
      return;
    }
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("团单合同信息未保存,不容许您进行 [录入完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "6999999999";			//录入完毕
  } else if (wFlag ==2)//复核完毕确认
  {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出团单合同信息,不容许您进行 [复核完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000002002";					//复核完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else if (wFlag ==3) {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001002";					//复核修改完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else if(wFlag == 4) {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出合同信息,不容许您进行 [修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001021";					//问题修改
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else
    return;

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./GrpInputConfirm.jsp";
  fm.submit(); //提交
}
function getaddresscodedata() {
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  //strsql = "select AddressNo,PostalAddress from LCAddress where CustomerNo ='"+fm.InsuredNo.value+"'";
  strsql = "select max(int(AddressNo)) from LCAddress where CustomerNo ='"+fm.InsuredNo.value+"'";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData =turnPage.arrDataCacheSet[i][0];
    }
  }
  //alert ("tcodedata : " + tCodeData);
  //return tCodeData;
  //fm.all("AddressNo").CodeData=tCodeData;
  fm.all("AddressNo").value=tCodeData;
  afterCodeSelect("AddressNo","");
}

function getImpartCode(parm1,parm2) {
  //alert("hehe:"+fm.all(parm1).all('ImpartGrid1').value);
  var impartVer=fm.all(parm1).all('ImpartGrid1').value;
  window.open("../app/ImpartCodeSel.jsp?ImpartVer="+impartVer);
}
function checkidtype() {
  if(fm.IDType.value=="") {
    alert("请先选择证件类型！");
    fm.IDNo.value="";
  }
}
function getallinfo() {
  if(fm.Name.value!=""&&fm.IDType.value!=""&&fm.IDNo.value!="") {
    strSQL = "select a.CustomerNo, a.Name, a.Sex, a.Birthday, a.IDType, a.IDNo from LDPerson a where 1=1 "
             +"  and Name='"+fm.Name.value
             +"' and IDType='"+fm.IDType.value
             +"' and IDNo='"+fm.IDNo.value
             +"' order by a.CustomerNo";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (turnPage.strQueryResult != "") {
      mOperate = 2;
      //alert(mOperate);
      //window.open("../sys/LDPersonQueryAll.html?Name="+fm.Name.value+"&IDType="+fm.IDType.value+"&IDNo="+fm.IDNo.value,"newwindow","height=10,width=1090,top=180,left=180, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no,status=no");
    } else
      return;
  }
}
function DelRiskInfo() {
  if(fm.InsuredNo.value=="") {
    alert("请先选择被保人");
    return false;
  }
  var tSel =PolGrid.getSelNo();
  if( tSel == 0 || tSel == null ) {
    alert("该客户没有险种或者您忘记选择了？");
    return false;
  }
  if(!confirm("是否要删除该险种？删除请点击“确定”，否则点“取消”。"))
  {
  	return;
  }
  var tRow = PolGrid.getSelNo() - 1;
  var tpolno=PolGrid.getRowColData(tRow,1)
             fm.all('fmAction').value="DELETE||INSUREDRISK";
  fm.action="./DelIsuredRisk.jsp?polno="+tpolno;
  fm.submit(); //提交

}
function InsuredChk() {
  var tSel =InsuredGrid.getSelNo();
  if( tSel == 0 || tSel == null ) {
    alert("请先选择被保险人！");
    return false;
  }
  var tRow = InsuredGrid.getSelNo() - 1;
  var tInsuredNo=InsuredGrid.getRowColData(tRow,1);
  var tInsuredName=InsuredGrid.getRowColData(tRow,2);
  var tInsuredSex=InsuredGrid.getRowColData(tRow,3);
  var tBirthday=InsuredGrid.getRowColData(tRow,4);
  var sqlstr="select *from ldperson where Name='"+tInsuredName+"' and Sex='"+tInsuredSex+"' and Birthday='"+tBirthday+"' and CustomerNo<>'"+tInsuredNo+"'";
  arrResult = easyExecSql(sqlstr,1,0);
  if(arrResult==null) {
    alert("没有与该被保人相似的客户,无需校验");
    return false;
  }

  window.open("../uw/InsuredChkMain.jsp?ProposalNo1="+fm.ContNo.value+"&InsuredNo="+tInsuredNo+"&Flag=I","window1");
}
function FillPostalAddress() {
    var PostalAddressSQL="select PostalAddress from lcaddress where customerno in(select appntno from lccont where prtno ='"+fm.all( 'PrtNo' ).value+"')  and AddressNo =(select char(max(int(AddressNo))) from LCAddress where customerno in(select appntno from lccont where prtno='"+fm.all( 'PrtNo' ).value+"'))";
    var sqlstr=easyExecSql(PostalAddressSQL,1,0);
    if(sqlstr){
			//alert();
    	if(fm.CheckPostalAddress.value=="1") {
    		fm.all('PostalAddress').value=sqlstr;
    		fm.all('ZipCode').value="";
    		}else if(fm.CheckPostalAddress.value=="2"){
    		fm.all('PostalAddress').value="";
    		fm.all('ZipCode').value="";    		
    		}
  		}else{
    	fm.all('PostalAddress').value="";
    	fm.all('ZipCode').value="";    
    	} 
}
function checksex() {
  var malearray= new Array("02","04","06","08","09","14","16","19","22");
  var femalearray= new Array("01","05","07","10","11","15","17","20","23");
  if(fm.Sex.value=="0") {
    for(var relationmcount=0;relationmcount<femalearray.length-1;relationmcount++) {
      if(fm.RelationToAppnt.value==femalearray[relationmcount]) {
        alert("被保人性别与'与投保人关系'中的关系身份不符");
        return false;
      }
      if(fm.RelationToMainInsured.value==femalearray[relationmcount]) {
        alert("被保人性别与'与第一被保人关系'中的关系身份不符");
        return false;
      }
    }
  }
  if(fm.Sex.value=="1") {
    for(var relationfcount=0;relationfcount<malearray.length;relationfcount++) {
      if(fm.RelationToAppnt.value==malearray[relationfcount]) {
        alert("被保人性别与'与投保人关系'中的关系身份不符");
        return false;
      }
      if(fm.RelationToMainInsured.value==malearray[relationfcount]) {
        alert("被保人性别与'与第一被保人关系'中的关系身份不符");
        return false;
      }
    }
  }
  //alert(fm.OccupationCode.value);
  if(fm.OccupationCode.value!=""){
  var tsqlstr=" select OccupationType from LDOccupation where  OccupationCode ='"+fm.OccupationCode.value+"'";
   var OccupationType = easyExecSql(tsqlstr,1,0);
  if(OccupationType==null) {
    alert("职业代码"+fm.OccupationCode.value+"没有定义职业类别!");
    return false;
  }
  if(OccupationType!=fm.OccupationType.value){
    alert("职业代码"+fm.OccupationCode.value+"的职业类别与页面录入不符,应为"+OccupationType+"类职业!");
    return false;  
  }
 } 
  return true;
}
function getdetailaccount() {
  if(fm.AccountNo.value=="1") {
    if(mSwitch.getVar("AppntBankAccNo"))
      fm.all('BankAccNo').value=mSwitch.getVar("AppntBankAccNo");
    if(mSwitch.getVar("AppntBankCode"))
      fm.all('BankCode').value=mSwitch.getVar("AppntBankCode");
      fm.all('bankName').value = easyExecSql("select codename from ldcode where codetype='bank' and code='"+mSwitch.getVar("AppntBankCode")+"'");
    if(mSwitch.getVar("AppntAccName"))
      fm.all('AccName').value=mSwitch.getVar("AppntAccName");
  }
  if(fm.AccountNo.value=="2") {
    fm.all('BankAccNo').value="";
    fm.all('BankCode').value="";
    fm.all('AccName').value="";
  }

}
function AutoMoveForNext() {
  if(fm.AutoMovePerson.value=="定制第二被保险人") {
    //emptyFormElements();
    param="122";
    fm.pagename.value="122";
    fm.AutoMovePerson.value="定制第三被保险人";
    return false;
  }
  if(fm.AutoMovePerson.value=="定制第三被保险人") {
    //emptyFormElements();
    param="123";
    fm.pagename.value="123";
    fm.AutoMovePerson.value="定制第一被保险人";
    return false;
  }
  if(fm.AutoMovePerson.value=="定制第一被保险人") {
    //emptyFormElements();
    param="121";
    fm.pagename.value="121";
    fm.AutoMovePerson.value="定制第二被保险人";
    return false;
  }
}
function noneedhome() {
  var insuredno="";
  if(InsuredGrid.mulLineCount>=1) {
    for(var personcount=0;personcount< InsuredGrid.mulLineCount;personcount++) {
      if(InsuredGrid.getRowColData(personcount,5)=="00") {
        insuredno=InsuredGrid.getRowColData(personcount,1);
        break;
      }
    }
    var strhomea="select HomeAddress,HomeZipCode,HomePhone from lcaddress where customerno='"+insuredno+"' and addressno=(select addressno from lcinsured where contno='"+fm.ContNo.value+"' and insuredno='"+insuredno+"')";
    arrResult=easyExecSql(strhomea,1,0);
    try {
      fm.all('HomeAddress').value= arrResult[0][0];
    } catch(ex) {}
    ;
    try {
      fm.all('HomeZipCode').value= arrResult[0][1];
    } catch(ex) {}
    ;
    try {
      fm.all('HomePhone').value= arrResult[0][2];
    } catch(ex) {}
    ;
  }
}
function getdetail() {
  var strSql = "select BankCode,AccName from LCAccount where BankAccNo='" + fm.BankAccNo.value+"'";
  arrResult = easyExecSql(strSql);
  if (arrResult != null) {
    fm.BankCode.value = arrResult[0][0];
    fm.AccName.value = arrResult[0][1];
  }

}
//用于无名单时填充被保险人信息
function FillNoName() {
  fm.IDType.value='4';
  DivLCInsured.style.display='none';
  DivGrpNoname.style.display='';
}

//用于无名单校验
function checkifnoname() {
  var strSql = "select ContPlanCode from LCInsured where ContNo='" + oldContNo+"' and Name='无名单'";
  arrResult = easyExecSql(strSql);
  if(fm.ContPlanCode.value!=""&&fm.ContPlanCode.value!=arrResult[0][0]) {
    alert("所选计划与原无名单所选不一致！");
    return false;
  } else {
    return true;
  }
}
//用于无名单校验
function checkenough() {
  var strSql1 = " select * from lcpol where appflag='4' and MasterPolNo in (select  proposalNo from lcpol where Contno='" + oldContNo+"')";
  var arrResult1 = easyExecSql(strSql1);
  var strSql2 = " select * from lccont where appflag<>'1' and GrpContNo = (select  GrpContNo from lcCont where Contno='" + oldContNo+"')";
  var arrResult2 = easyExecSql(strSql2);
  if(arrResult1!=null||arrResult2!=null) {
    alert("尚有补名单未签单,请先签单再补其他名单!");
    return false;
  } else {
    return true;
  }
}
//险种复合
function showComplexRiskInfo()
{
	window.open("./ComplexRiskMain.jsp?ContNo="+ContNo+"&prtNo="+fm.PrtNo.value,"", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}
//校验是否重新生成新的合同
function checkCanModify(){
	var strSql = "select 1 from LCInsured where name='"+fm.Name.value+"' and EnglishName='"+fm.EnglishName.value+"' and OthIDNo='"+fm.OthIDNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		return false;
	}else{
		return true;
	}
}
function afterSubmit1( FlagStr, content )
{
	UnChangeDecodeStr();
	showInfo.close();
	window.focus();
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "新单复核成功！";
		cMissionID = fm.MissionID.value;
		strSQL = "select 1 from lwmission where 1=1 "
				 + " and missionid = '"+ cMissionID +"'"
				 + " and activityid = '0000001100' "
				 + " and processid = '0000000003'"
				 ;	 

	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  	//判断是否查询成功
  	if (turnPage.strQueryResult) {
			content = content + "但需提交人工核保！";
  	}
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		top.opener.queryCont();
	}
	mAction = ""; 
	top.close();
}




//外包反馈错误信息 2007-9-28 11:47
function findIssue()
{
	window.open("./BPOIssueInput.jsp?prtNo="+prtNo,"window1");
}

//销售渠道和业务员的校验
function checkSaleChnl()
{
    //业务员和销售渠道的校验
    var agentCodeSql = "select 1 from LAAgent a, LDCode1 b, LCCont c "
        + "where c.PrtNo = '" + fm.PrtNo.value + "' "
        + "and a.AgentState < '06' and b.CodeType = 'salechnl' "
        + "and a.AgentCode = c.AgentCode and b.Code = c.SaleChnl "
        + "and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName " 
        + "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '10' = '" + fm.SaleChnl.value + "' and a.BranchType2 = '04'  "
        + "with ur";
    var arr = easyExecSql(agentCodeSql);
    if(!arr){
        alert("业务员和销售渠道不相符！");
        return false;
    }
    return true;
}

/**
 * 校验所填入缴费凭证号的，到帐情况
 */
function checkEnterAccOfTempFee()
{
    var tContNo = fm.ContNo.value;
    var tResult = easyExecSql("select PayMode, TempFeeNo from LCCont lcc where lcc.ContNo = '" + tContNo + "'");
    if(!tResult)
    {
        alert('未查询出该保单对应缴费相关信息。');
        return false;
    }
    
    var tPayMode = tResult[0][0];
    var tTempFeeNo = tResult[0][1];
    
    // 只对缴费方式为：1-现金；11-银行代收，进行校验。
    if(tPayMode != "1" && tPayMode != "11" && tPayMode != "12")
        return true;

    // 暂缴号为空或不填，不进行校验。
    if(tTempFeeNo == null || tTempFeeNo == "")
        return true;
        
    var tStrSql = "select nvl(sum(PayMoney), 0) "
       + " from LJTempFeeClass ljtfc "
       + " where ljtfc.EnterAccDate is not null and ljtfc.ConfMakeDate is not null "
       + " and ConfDate is null "
       + " and ljtfc.TempFeeNo = '" + tTempFeeNo + "' ";
       
    tResult = easyExecSql(tStrSql);
    if(!tResult)
    {
        alert('未查询出该暂缴号对应保费相关信息。');
        return false;
    }
    var tEnterAccMoney = tResult[0][0];
    
    tResult = easyExecSql("select nvl(sum(Prem), 0) from LCPol lcp where lcp.ContNo = '" + tContNo + "' ");
    if(!tResult)
    {
        alert('未查询出该保单对应保费相关信息。');
        return false;
    }
    var tContPrem = tResult[0][0];
    
    var tComment = "该单为先收费保单:\n"
        + "缴费方式为：" + tPayMode + "\n"
        + "暂缴收据号为：" + tTempFeeNo + "\n"
        + "目前到帐保费为：" + tEnterAccMoney + "\n"
        + "该单系统保费为：" + tContPrem;
    return confirm(tComment);
}

//险种的校验
function checkRiskInfo()
{
    //该分公司险种是否停售校验
    var riskEnd = "select 1 from LCPol a, LMRiskApp b, LDCode c where a.PrtNo = '" 
        + fm.PrtNo.value + "' and b.RiskType4 = '4' and c.CodeType = 'UniversalSale' "
        + "and a.RiskCode = b.RiskCode and c.Code = substr(a.ManageCom, 1, 4) ";
    var result = easyExecSql(riskEnd);
    if(result)
    {
        alert("该分公司万能险已停售！");
        return false;
    }
    
    //保险期间单位为空的校验
    var riskInsu = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and (InsuYearFlag is null or InsuYearFlag = '')";
    result = easyExecSql(riskInsu);
    if(result)
    {
        alert("保险期间单位为空，请修改！");
        return false;
    }
    
    //个人防癌套餐的校验
    var risk230501And331001 = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and RiskCode in ('230501', '331001')";
    result = easyExecSql(risk230501And331001);
    if(result)
    {
        var riskInfoSql = "select 1 from LCPol where PrtNo = '" + fm.PrtNo.value + "' "
            + "and RiskCode in ('230501', '331001') "
            + "and not (InsuYear in (10, 15, 20) and InsuYearFlag = 'Y' "
            + "and PayEndYear = InsuYear and PayEndYearFlag = InsuYearFlag "
            + "and PayIntv = 12)";
        result = easyExecSql(riskInfoSql);
        if(result)
        {
            alert("请检查缴费频次、保险期间和缴费期间！\n(1).保险期间必须为10年、15年、20年。"
                + "\n(2).缴费年期必须与保险期间一致。\n(3).缴费频次必须为年缴。");
            return false;
        }
    }
    return true;
}


/**
 * 校验销售渠道与产品关联
 */
function checkSaleChnlRisk()
{   
    var tPrtNo = fm.PrtNo.value;
    var saleChannel = null;
    
    var arr = easyExecSql("select salechnl from lccont where prtno = '" + tPrtNo + "' ");
    if(arr)
    {
        saleChannel = arr[0][0];
    }
    
    if(saleChannel != null && saleChannel == "13")
    {
        var tStrSql = " select 1 "
            + " from LCPol lcp "
            + " left join LDCode1 ldc1 on lcp.SaleChnl = ldc1.Code and lcp.RiskCode = ldc1.code1 and ldc1.codetype = 'checksalechnlrisk' "
            + " where 1 = 1 "
            + " and ldc1.CodeType is null "
            + " and lcp.PrtNo = '" + tPrtNo + "' "
            ;
        var result = easyExecSql(tStrSql);
        
        if(result)
        {
            alert("该单为银代直销保单，但存在非银代直销产品，请核实。");
            return false;
        }
    }
    
    return true;
}


function BirthdaySexAppntIDNo()
{
	if(fm.all('IDType').value=="0")
	{
		if(fm.all('Birthday').value!=getBirthdatByIdNo(fm.all('IDNo').value) || fm.all('Sex').value!=getSexByIDNo(fm.all('IDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
	
	if(fm.all('IDType').value=="5")
	{
		if(fm.all('Birthday').value!=getBirthdatByIdNo(fm.all('IDNo').value) || fm.all('Sex').value!=getSexByIDNo(fm.all('IDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
}
//by gzh 20110402 录入完毕及复核完毕时，校验被保人告知第19项是否正确填写（即存在小于18周岁的未成年人，但还没选择被保人告知第19项）
function checkGZ19(){
	var strSql = "select insuredno,insuredappage from lcpol where prtno = '"+prtNo+"' and insuredappage<18";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		for(var i=0;i<arr.length;i++){
			var CustomerimpartSql = "select * from l" + contState + "customerimpart where prtno = '"+prtNo+"' and customerno = '"+arr[i][0]+"' and impartver = '001' and impartcode='250'";
			var arrCustomer = easyExecSql(CustomerimpartSql);
			if(arrCustomer==null){
				alert("被保人（"+arr[i][0]+"）年龄为"+arr[i][1]+"岁，请填写被保人告知第19项！");
				return false;
			}
		}
	}
	return true;
}

//by gzh 20110614 录入完毕及复核完毕时，校验被保人告知第19项是否正确填写（选择“是”时，保额必须填写）
function checkGZ19Prem(){
	var CustomerimpartSql = "select impartparammodle from l" + contState + "customerimpart where prtno = '"+prtNo+"' and impartver = '001' and impartcode='250'";
	var arrCustomer = easyExecSql(CustomerimpartSql);
	if(arrCustomer !=null){
		for(var i=0;i<arrCustomer.length;i++){
			var impartparammodle = arrCustomer[i][0].split(",");
			if(impartparammodle[0] == "Y" && (!isNumeric(impartparammodle[1]))){
				alert("被保人告知第19项,当选择“是”时，保额不能为空且必须是数字！");
				return false;
			}
		}
	}
	return true;
}

//校验保单的保险期间是否为负数     by zhangyang  2011-06-09
function checkInsuYear()
{
	var strSQL = "select Years from lcpol where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		for(var i = 0; i < arrResult.length; i++)
		{
			var tYears = arrResult[i][0];
			if(tYears != null && tYears!= '' && tYears < 0)
			{
				alert("保单的保险期间有误，请确认保单的保险期间录入值是否正确！");
				return false;
			}
		}
	}
	return true;
}

//校验黑名单
function checkBlackName(){
	var strSQL = "select appntname from lccont where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		for(var i = 0; i < arrResult.length; i++)
		{
			var tAppntName = arrResult[i][0];
			var strSQL1 = "select 1 from LCBlackList where type = '0' and name = '"+tAppntName+"'";
			var arrResult1 = easyExecSql(strSQL1);
			if(arrResult1 != null)
			{
				if (!confirm("该保单投保人姓名："+arrResult[i][0]+",存在于黑名单中，确认要复核通过吗？"))
				{
					return false;
				}
			}
		}
	}
	var strSQL2 = "select name from lcinsured where prtno = '" + fm.PrtNo.value + "'";
	var arrResult2 = easyExecSql(strSQL2);
	var tInsuNames = "";
	if(arrResult2 != null){
		for(var i = 0; i < arrResult2.length; i++)
		{
			var tName = arrResult2[i][0];
			var strSQL3 = "select 1 from LCBlackList where type = '0' and name = '"+tName+"'";
			var arrResult3 = easyExecSql(strSQL3);
			
			if(arrResult3 != null)
			{
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ ","+tName;
				}else{
					tInsuNames = tInsuNames + tName;
				}
			}
		}	
	}
	if(tInsuNames != ""){
		if (!confirm("该保单被保人姓名："+tInsuNames+",存在于黑名单中，确认要复核通过吗？"))
		{
			return false;
		}
	}
	return true;
}

function checkAppnt(wFlag){
	var tSql = "select cc.prtno,cad.Customerno,cc.Managecom,cad.Mobile,cad.Phone,cad.homephone,ca.idno from lccont cc inner join lcappnt ca on ca.appntno=cc.appntno and ca.contno=cc.contno " +
				"inner join lcaddress cad on cad.customerno=ca.appntno and cad.addressno=ca.addressno where cc.prtno='" + fm.PrtNo.value + "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		// 印刷号
		var prtno = appntResult[0][0];
		// 投保人客户号
		var customerno = appntResult[0][1];
		// 管理机构
		var managecom = appntResult[0][2];
		// 移动电话
		var mobile = appntResult[0][3];
		// 联系电话
		var phone = appntResult[0][4];
		// 家庭电话
		var homephone = appntResult[0][5];
		// 证件号
		var id = appntResult[0][6];
		
		// 共有校验
		if(!isNull(mobile)) {
			if(!isInteger(mobile) || mobile.length != 11){
				alert("投保人移动电话需为11位数字，请核查！");
		   		return false;
			}
		}
		
		// 机构特殊校验  --需要在数据库中配置 codetype = 字段名 + check

		// 暂时统一对投保人进行校验，若需要单独校验，可以调整为按字段进行校验
		// 若配置为空则会对全部机构进行校验
		if(manageCheck(managecom.substring(0, 4),"appnt")){
			
			if(isNull(id)){
				alert("投保人证件号码不能为空，请核查！");
				return false;
			}
			
			if(isNull(mobile) && isNull(phone) && isNull(homephone)){
				alert("投保人联系电话、住宅电话、移动电话不能同时为空，请核查！");
		   		return false;
			}
		}
		
		if(manageCheck(managecom.substring(0, 4),"phone")){

			if(!isNull(phone)){
				if(phone.length < 7){
					alert("投保人联系电话不能少于7位，请核查！");
			   		return false;
				}
			}
			
			// 复核时，对于3个以上重复号码，强制下发问题件
			if(wFlag == 2 && (!isNull(mobile) || !isNull(phone))){
				// 由于重复号码校验速度很慢，因此先对问题件进行判断
				
				// 空代表没有下发问题件 1代表已下发问题件未回销 2代表已回销
				// order by doc 为避免多次下发问题件联系电话问题件 只要有没有回销的 不让通过
				var issueSql = "Select (Case When (Select Docid From Es_Doc_Main Where Doccode = Ci.Prtseq And Subtype = 'TB22') Is Null Then 1 Else 2 End) doc "
						+ "From Lcissuepol Ci Where Errfieldname = '联系电话' And Questionobj Like '投保人%' And Contno = (Select Contno From Lccont Where Prtno = '" + prtno + "') " 
						+ "order by doc";
				var issueResult = easyExecSql(issueSql);
				
				// 页面提示信息
				var alterInf = "";
				if(issueResult == null){
					alterInf = "请下发投保人联系电话问题件";
				} else if(issueResult[0][0] == "1"){
					alterInf = "已下发的联系电话问题件尚未回销，请确认";
				}
				
				if(issueResult == null || issueResult[0][0] == "1"){
					if(!isNull(phone)){
						var phoneSql = "select count(distinct customerno) from lcaddress where phone='" + phone + "' and Customerno<>'" + customerno + "' ";
						var result = easyExecSql(phoneSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人联系电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
				
					if(!isNull(mobile)){
						var mobilSql = "select count(distinct customerno) from lcaddress where mobile='" + mobile + "' and Customerno<>'" + customerno + "' ";
						var result = easyExecSql(mobilSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人移动电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
				}
			}
		}
		
		if(manageCheck(managecom.substring(0, 4),"agent")){
				// 业务员手机号码校验
				if(!isNull(phone)){
					var mobilSql = "select agentcode from laagent where phone='" + phone + "' with ur";
					var result = easyExecSql(mobilSql);
					if(result){
						alert("该投保人联系电话与本方业务员" + result[0][0] + "联系号码相同，请核查！");
						return false;
					}
				}
				
				if(!isNull(mobile)){
					var phoneSql = "select agentcode from laagent where mobile='" + mobile + "' with ur";
					var result = easyExecSql(phoneSql);
					if(result){
						alert("该投保人移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
						return false;
					}
				}
			}
	}
	return true;
}

// 判断机构是否需要进行校验
// managecom 机构
// checkflag 校验项
function manageCheck(managecom, checkflag){
	var checkSql = "select (case (select count(1) from ldcode where codetype = '" + checkflag + "check') " +  
			"when 0 then 1 else (select distinct 1 from ldcode where codetype = '" + checkflag + "check' and code = '" + managecom + "') end ) " + 
			"from dual";
	var result = easyExecSql(checkSql);
	if(result){
		if(result[0][0] == "1"){
			// 该机构需要进行校验
			return true;
		}
	}
	// 该机构不需要进行校验
	return false;
}

// 判断是否为空
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}
//校验投保人银行账户信息
function checkBank()
{
    var strSql= easyExecSql("select PayMode,ExPayMode,BankCode, Accname,BankAccNo from LCCont where prtno = '" + fm.PrtNo.value + "'");
 	if(strSql) 
 	{          
		if(strSql[0][0]== "4"||strSql[0][1]=="4")
		{
			if(strSql[0][2] == null ||strSql[0][2] == '')
			{   
				alert("银行代码为空，若修改以后请点击修改按钮！");
				return false;
			}      
			if(strSql[0][3] == null ||strSql[0][3] == '')
			{
				alert("户名为空，若修改以后请点击修改按钮！");
				return false;
			} 
			if(strSql[0][4] == null ||strSql[0][4] == '')
			{
				alert("账号为空，若修改以后请点击修改按钮！");
				return false;
			 }     
		}
	}else{
			alert("未查到该保单信息，请核对数据！")
			return false;	
	}
    return true;
}
