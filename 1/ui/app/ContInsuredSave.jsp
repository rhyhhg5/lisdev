<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LDPersonSave.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LCContSchema tLCContSchema = new LCContSchema();
  LDPersonSchema tLDPersonSchema   = new LDPersonSchema();
  LCInsuredDB tOLDLCInsuredDB=new LCInsuredDB();
  LCAddressSchema tLCAddressSchema = new LCAddressSchema();
  LCInsuredSchema tmLCInsuredSchema =new LCInsuredSchema();
  LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
  LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = new LCCustomerImpartDetailSet();
  TransferData tTransferData = new TransferData(); 
  ContInsuredUI tContInsuredUI   = new ContInsuredUI();
  String LoadFlag =  request.getParameter("LoadFlag");
  LCApproveSchema tLCApproveSchema = new LCApproveSchema(); 
  //输出参数
  String FlagStr = "";
  String Content = "";
   
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  System.out.println("tGI"+tGI);
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
  CErrors tError = null;
  String tBmCert = "";
  System.out.println("aaaa");
  //后面要执行的动作：添加，修改，删除
  String fmAction=request.getParameter("fmAction");
    System.out.println("fmAction:"+fmAction); 

/*        
  String tLimit="";
  String CustomerNo="";
*/ 
        
        tLCContSchema.setGrpContNo(request.getParameter("GrpContNo"));  
        tLCContSchema.setContNo(request.getParameter("ContNo")); 
        tLCContSchema.setPrtNo(request.getParameter("PrtNo"));
        tLCContSchema.setManageCom(request.getParameter("ManageCom"));
        //tLCContSchema.setPolType(request.getParameter("PolTypeFlag"));//无名单标记
        //tLCContSchema.setPeoples(request.getParameter("InsuredAppAge"));//被保险人年龄
        
        tmLCInsuredSchema.setInsuredNo(request.getParameter("InsuredNo"));
        tmLCInsuredSchema.setRelationToMainInsured(request.getParameter("RelationToMainInsured"));
        tmLCInsuredSchema.setRelationToAppnt(request.getParameter("RelationToAppnt"));
        tmLCInsuredSchema.setContNo(request.getParameter("ContNo"));
        tmLCInsuredSchema.setGrpContNo(request.getParameter("GrpContNo"));
        tmLCInsuredSchema.setContPlanCode(request.getParameter("ContPlanCode"));
        tmLCInsuredSchema.setExecuteCom(request.getParameter("ExecuteCom"));
        tmLCInsuredSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
        tmLCInsuredSchema.setInsuredPeoples(request.getParameter("InsuredPeoples"));
        tmLCInsuredSchema.setSalary(request.getParameter("Salary"));
        tmLCInsuredSchema.setBankCode(request.getParameter("BankCode"));
        tmLCInsuredSchema.setBankAccNo(request.getParameter("BankAccNo"));                
        tmLCInsuredSchema.setAccName(request.getParameter("AccName"));
        tmLCInsuredSchema.setOthIDNo(request.getParameter("OthIDNo"));
        tmLCInsuredSchema.setEnglishName(request.getParameter("EnglishName"));
        tmLCInsuredSchema.setOccupationType(request.getParameter("OccupationType"));
        tmLCInsuredSchema.setOccupationCode(request.getParameter("OccupationCode"));
        tmLCInsuredSchema.setPosition(request.getParameter("Position"));
        tmLCInsuredSchema.setIDStartDate(request.getParameter("InsuIDStartDate"));
        tmLCInsuredSchema.setIDEndDate(request.getParameter("InsuIDEndDate"));
		//modify by zxs 
		tmLCInsuredSchema.setAuthorization(request.getParameter("InsuredAuth"));
        
        tLDPersonSchema.setCustomerNo(request.getParameter("InsuredNo"));
        tLDPersonSchema.setName(request.getParameter("Name"));
        tLDPersonSchema.setSex(request.getParameter("Sex"));      
        tLDPersonSchema.setBirthday(request.getParameter("Birthday"));
        tLDPersonSchema.setIDType(request.getParameter("IDType"));
        tLDPersonSchema.setIDNo(request.getParameter("IDNo"));
        tLDPersonSchema.setPassword(request.getParameter("Password"));
        tLDPersonSchema.setNativePlace(request.getParameter("NativePlace"));
        tLDPersonSchema.setNationality(request.getParameter("Nationality"));
        tLDPersonSchema.setRgtAddress(request.getParameter("RgtAddress"));
        tLDPersonSchema.setMarriage(request.getParameter("Marriage"));
        tLDPersonSchema.setMarriageDate(request.getParameter("MarriageDate"));
        tLDPersonSchema.setHealth(request.getParameter("Health"));
        tLDPersonSchema.setStature(request.getParameter("Stature"));
        tLDPersonSchema.setAvoirdupois(request.getParameter("Avoirdupois"));
        tLDPersonSchema.setDegree(request.getParameter("Degree"));
        tLDPersonSchema.setCreditGrade(request.getParameter("CreditGrade"));
        tLDPersonSchema.setOthIDType(request.getParameter("OthIDType"));
        tLDPersonSchema.setOthIDNo(request.getParameter("OthIDNo"));
        tLDPersonSchema.setICNo(request.getParameter("ICNo"));
        tLDPersonSchema.setGrpNo(request.getParameter("GrpNo"));
        tLDPersonSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
        tLDPersonSchema.setStartWorkDate(request.getParameter("StartWorkDate"));
        tLDPersonSchema.setPosition(request.getParameter("Position"));
        tLDPersonSchema.setSalary(request.getParameter("Salary"));
        tLDPersonSchema.setOccupationType(request.getParameter("OccupationType"));
        tLDPersonSchema.setOccupationCode(request.getParameter("OccupationCode"));
        tLDPersonSchema.setWorkType(request.getParameter("WorkType"));
        tLDPersonSchema.setPluralityType(request.getParameter("PluralityType"));
        tLDPersonSchema.setDeathDate(request.getParameter("DeathDate"));
        tLDPersonSchema.setSmokeFlag(request.getParameter("SmokeFlag"));
        tLDPersonSchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));
        tLDPersonSchema.setProterty(request.getParameter("Proterty"));
        tLDPersonSchema.setRemark(request.getParameter("Remark"));
        tLDPersonSchema.setState(request.getParameter("State"));
 				tLDPersonSchema.setGrpName(request.getParameter("GrpName"));
 				tLDPersonSchema.setEnglishName(request.getParameter("EnglishName"));
 				tLDPersonSchema.setNativeCity(request.getParameter("NativeCity"));
					//modify by zxs 
 				tLDPersonSchema.setAuthorization(request.getParameter("InsuredAuth"));
 									
 				tLCAddressSchema.setCustomerNo(request.getParameter("InsuredNo"));
        //tLCAddressSchema.setAddressNo(request.getParameter("AddressNo"));  
        tLCAddressSchema.setPostalAddress(request.getParameter("PostalAddress"));
        tLCAddressSchema.setZipCode(request.getParameter("ZipCode"));
        tLCAddressSchema.setPhone(request.getParameter("Phone"));
        tLCAddressSchema.setFax(request.getParameter("Fax"));          
        tLCAddressSchema.setMobile(request.getParameter("Mobile"));        
        tLCAddressSchema.setEMail(request.getParameter("EMail"));
        
        tLCAddressSchema.setHomeAddress(request.getParameter("HomeAddress"));        
        tLCAddressSchema.setHomeZipCode(request.getParameter("HomeZipCode"));
        tLCAddressSchema.setHomePhone(request.getParameter("HomePhone"));
        tLCAddressSchema.setHomeFax(request.getParameter("HomeFax"));  
        tLCAddressSchema.setGrpName(request.getParameter("GrpName")); 
        tLCAddressSchema.setPostalCity(request.getParameter("City"));
        tLCAddressSchema.setPostalCommunity(request.getParameter("PostalCommunity"));
        tLCAddressSchema.setPostalCounty(request.getParameter("County"));
        tLCAddressSchema.setPostalProvince(request.getParameter("Province"));
        tLCAddressSchema.setPostalStreet(request.getParameter("PostalStreet"));
        tLCAddressSchema.setHomeCode(request.getParameter("HomeCode"));
        tLCAddressSchema.setHomeNumber(request.getParameter("HomeNumber"));
        System.out.println("-------------美丽----------"+request.getParameter("PostalProvince"));  
                   
        tLCAddressSchema.setCompanyPhone(request.getParameter("GrpPhone"));
        tLCAddressSchema.setCompanyAddress(request.getParameter("GrpAddress"));
        tLCAddressSchema.setCompanyZipCode(request.getParameter("GrpZipCode"));
        tLCAddressSchema.setCompanyFax(request.getParameter("GrpFax"));        
        String SavePolType="";
        String BQFlag=request.getParameter("BQFlag");
        if(BQFlag==null) SavePolType="0";
        else if(BQFlag.equals("")) SavePolType="0";
        else  SavePolType=BQFlag;  
        tTransferData.setNameAndValue("SavePolType",SavePolType); //保全保存标记，默认为0，标识非保全  
        tTransferData.setNameAndValue("ContNo",request.getParameter("ContNo")); 
        tTransferData.setNameAndValue("FamilyType",request.getParameter("FamilyType"));//家庭单标记 
        tTransferData.setNameAndValue("ContType",request.getParameter("ContType"));//团单，个人单标记
        tTransferData.setNameAndValue("PolTypeFlag",request.getParameter("PolTypeFlag"));//无名单标记
        tTransferData.setNameAndValue("InsuredPeoples",request.getParameter("InsuredPeoples"));//被保险人人数
        tTransferData.setNameAndValue("InsuredAppAge",request.getParameter("InsuredAppAge"));//被保险人年龄
        tTransferData.setNameAndValue("SequenceNo",request.getParameter("SequenceNo"));//
        tTransferData.setNameAndValue("OldContNo",request.getParameter("oldContNo"));
        tTransferData.setNameAndValue("LoadFlag",request.getParameter("LoadFlag"));
            //modify by zxs 
        tTransferData.setNameAndValue("InsuredAuth",request.getParameter("InsuredAuth"));      
        System.out.println("ContType"+request.getParameter("ContType"));
        if (fmAction.equals("UPDATE||CONTINSURED")||fmAction.equals("DELETE||CONTINSURED"))
        {
            String tRadio[] = request.getParameterValues("InpInsuredGridSel");
            String tInsuredNo[] = request.getParameterValues("InsuredGrid1");
            System.out.println("**************2"+tRadio);
            System.out.println("**************1"+tInsuredNo);
            
            if (tRadio!=null)
            {
                for (int index=0; index< tRadio.length;index++)
                {
                System.out.println("**************3");
                
                    if(tRadio[index].equals("1"))
                    {
                      tOLDLCInsuredDB.setContNo(request.getParameter("ContNo"));
                      tOLDLCInsuredDB.setGrpContNo(request.getParameter("GrpContNo"));
                      tOLDLCInsuredDB.setInsuredNo(tInsuredNo[index]);
                    }
                    if (tOLDLCInsuredDB.getInsuredNo()==null)
                    {
                      tOLDLCInsuredDB.setContNo(request.getParameter("ContNo"));
                      tOLDLCInsuredDB.setGrpContNo(request.getParameter("GrpContNo"));
                      tOLDLCInsuredDB.setInsuredNo(tInsuredNo[0]);
                    }
                }
            }
            else
            {
                System.out.println("**************4");
                tOLDLCInsuredDB.setContNo(request.getParameter("ContNo"));
                tOLDLCInsuredDB.setGrpContNo(request.getParameter("GrpContNo"));
                System.out.println("tInsuredNo"+tInsuredNo[0]);
                tOLDLCInsuredDB.setInsuredNo(tInsuredNo[0]);
            }
        }
        
        String tImpartNum[] = request.getParameterValues("ImpartGridNo");
		String tImpartVer[] = request.getParameterValues("ImpartGrid1");            //告知版别
		String tImpartCode[] = request.getParameterValues("ImpartGrid2");           //告知编码
		String tImpartContent[] = request.getParameterValues("ImpartGrid3");        //告知内容
		String tImpartParamModle[] = request.getParameterValues("ImpartGrid4");        //填写内容
		//String tImpartCustomerNoType[] = request.getParameterValues("ImpartGrid5"); //告知客户类型
		//String tImpartCustomerNo[] = request.getParameterValues("ImpartGrid6");     //告知客户号码
			
			int ImpartCount = 0;
			if (tImpartNum != null) ImpartCount = tImpartNum.length;
	        
			for (int i = 0; i < ImpartCount; i++)	{
	            LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
				
                tLCCustomerImpartSchema.setProposalContNo(request.getParameter("ContNo"));
				//tLCCustomerImpartSchema.setCustomerNo(tLDPersonSchema.getCustomerNo());
				System.out.println("&&&&"+StrTool.unicodeToGBK(tImpartCode[i])
						+"&&&&&"+StrTool.unicodeToGBK(tImpartContent[i])+"&&&&&&"+
						StrTool.unicodeToGBK(tImpartParamModle[i])+"&&&&"+StrTool.unicodeToGBK(tImpartVer[i]));
				tLCCustomerImpartSchema.setCustomerNoType("I");
				tLCCustomerImpartSchema.setImpartCode(StrTool.unicodeToGBK(tImpartCode[i]));
				tLCCustomerImpartSchema.setImpartContent(StrTool.unicodeToGBK(tImpartContent[i]));
				tLCCustomerImpartSchema.setImpartParamModle(StrTool.unicodeToGBK(tImpartParamModle[i]));
				tLCCustomerImpartSchema.setImpartVer(StrTool.unicodeToGBK(tImpartVer[i])) ;
				tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
			}
			
                 System.out.println("end set schema 告知信息..."+ImpartCount);
                        String tImpartDetailNum[] = request.getParameterValues("ImpartDetailGridNo");
	              	String tImpartDetailVer[] = request.getParameterValues("ImpartDetailGrid1");            //告知版别
	              	String tImpartDetailCode[] = request.getParameterValues("ImpartDetailGrid2");           //告知编码
	              	String tImpartDetailContent[] = request.getParameterValues("ImpartDetailGrid3");        //告知内容
	              	String tImpartDetailDiseaseContent[] = request.getParameterValues("ImpartDetailGrid4");  
	              	String tImpartDetailStartDate[] = request.getParameterValues("ImpartDetailGrid5");      
	              	String tImpartDetailEndDate[] = request.getParameterValues("ImpartDetailGrid6");
		        String tImpartDetailProver[] = request.getParameterValues("ImpartDetailGrid7");    
		        String tImpartDetailCurrCondition[] = request.getParameterValues("ImpartDetailGrid8");  
		        String tImpartDetailIsProved[] = request.getParameterValues("ImpartDetailGrid9");  		        		        		
			int ImpartDetailCount = 0;
			if (tImpartDetailNum != null) ImpartDetailCount = tImpartDetailNum.length;
	        
			for (int i = 0; i < ImpartDetailCount; i++)	{
	                        LCCustomerImpartDetailSchema tLCCustomerImpartDetailSchema = new LCCustomerImpartDetailSchema();						
				tLCCustomerImpartDetailSchema.setCustomerNoType("I");
				tLCCustomerImpartDetailSchema.setImpartVer(tImpartDetailVer[i]) ;				
				tLCCustomerImpartDetailSchema.setImpartCode(tImpartDetailCode[i]);
				tLCCustomerImpartDetailSchema.setImpartDetailContent(tImpartDetailContent[i]);
				tLCCustomerImpartDetailSchema.setDiseaseContent(tImpartDetailDiseaseContent[i]);
				tLCCustomerImpartDetailSchema.setStartDate(tImpartDetailStartDate[i]);
				tLCCustomerImpartDetailSchema.setEndDate(tImpartDetailEndDate[i]);
				tLCCustomerImpartDetailSchema.setProver(tImpartDetailProver[i]);
				tLCCustomerImpartDetailSchema.setCurrCondition(tImpartDetailCurrCondition[i]);								
				tLCCustomerImpartDetailSchema.setIsProved(tImpartDetailIsProved[i]);					
				tLCCustomerImpartDetailSet.add(tLCCustomerImpartDetailSchema);
			}
			
             System.out.println("end set schema 告知明细信息..."+ImpartDetailCount);  
             //复录被保人
            VData ttVData = new VData();
            if(LoadFlag.equals("5")){
                tLCApproveSchema.setPrtNo(request.getParameter("PrtNo"));
                tLCApproveSchema.setCustomerType("01");
                tLCApproveSchema.setContType("1");       
                ttVData.add(tGI);
                ttVData.add(tLCApproveSchema); 
                ttVData.add(tOLDLCInsuredDB);
	        }           
        try
         {
            // 准备传输数据 VData
            System.out.println("tLDPersonSchema2"+tLDPersonSchema);
             VData tVData = new VData();
             tVData.add(tLCContSchema);
             tVData.add(tLDPersonSchema);
             tVData.add(tLCCustomerImpartSet);
             tVData.add(tLCCustomerImpartDetailSet);             
             tVData.add(tmLCInsuredSchema);
             tVData.add(tLCAddressSchema);
             tVData.add(tOLDLCInsuredDB);
             
             System.out.println("sadf12312"+tOLDLCInsuredDB.getInsuredNo());
             
             tVData.add(tTransferData);
             tVData.add(tGI);
             
             //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
            if ( tContInsuredUI.submitData(tVData,fmAction))
            {
                if(LoadFlag.equals("5")){
                    VData tVData1 = new VData();
		    	    tVData1 = tContInsuredUI.getResult();
                    ttVData.add((LCInsuredSchema)tVData1.getObjectByObjectName("LCInsuredSchema",0));		   
		            ApproveContUI tApproveContUI=new ApproveContUI();
		            tApproveContUI.submitData(ttVData, fmAction);
		        }
    		    if (fmAction.equals("INSERT||CONTINSURED"))
		        {
		    	    System.out.println("------return");
			        	
		    	    tVData.clear();
		    	    tVData = tContInsuredUI.getResult();
		    	    System.out.println("-----size:"+tVData.size());
		    	    
		    	    LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema(); 
			        mLCInsuredSchema=(LCInsuredSchema)tVData.getObjectByObjectName("LCInsuredSchema",0);
			
			        System.out.println("test");
			
			        if( mLCInsuredSchema.getInsuredNo() == null ) 
			        {
				            System.out.println("null");
			        }
			
			        String strCustomerNo = mLCInsuredSchema.getInsuredNo(); 
			        String strContNo =  mLCInsuredSchema.getContNo();
			        String strAddressNo= mLCInsuredSchema.getAddressNo();
			        System.out.println("jsp"+strCustomerNo);
			        String RelationToMainInsured=mLCInsuredSchema.getRelationToMainInsured();
			        if(RelationToMainInsured==null)
			        {
			          RelationToMainInsured="";
			        }
			        if( strCustomerNo==null)
			        {
			        	strCustomerNo = "123";
			        	System.out.println("null");
			        }
			
			        // System.out.println("-----:"+mLAAgentSchema.getAgentCode());
		            %>
		            <SCRIPT language="javascript">
		                parent.fraInterface.fm.all("InsuredNo").value="<%=strCustomerNo%>";
		            	parent.fraInterface.InsuredGrid.addOne("InsuredGrid");    	            	 
                        parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,1,"<%=StrTool.unicodeToGBK(mLCInsuredSchema.getInsuredNo())%>");
                        parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,2,"<%=StrTool.unicodeToGBK(mLCInsuredSchema.getName())%>");
                        parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,3,"<%=mLCInsuredSchema.getSex()%>");
                        parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,4,"<%=mLCInsuredSchema.getBirthday()%>");                     
                        parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,5,"<%=RelationToMainInsured%>");   
                        parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,6,"<%=mLCInsuredSchema.getSequenceNo()%>");                                     
                        parent.fraInterface.fm.all("ContNo").value="<%=strContNo%>"; 
                        parent.fraInterface.fm.all("AddressNo").value="<%=strAddressNo%>"; 
                        
		            </SCRIPT>
		            <%
		        }
		        if (fmAction.equals("UPDATE||CONTINSURED"))
		        {
		    	    System.out.println("------return");
			        	
		    	    tVData.clear();
		    	    tVData = tContInsuredUI.getResult();
		    	    System.out.println("-----size:"+tVData.size());
		    	    
		    	    LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema(); 
			        mLCInsuredSchema=(LCInsuredSchema)tVData.getObjectByObjectName("LCInsuredSchema",0);
			
			        String strCustomerNo = mLCInsuredSchema.getInsuredNo(); 
			        String strContNo =  mLCInsuredSchema.getContNo();
			        String strAddressNo= mLCInsuredSchema.getAddressNo();
			        System.out.println("jsp"+strAddressNo);
		            %>
		            <SCRIPT language="javascript">
		                parent.fraInterface.fm.all("InsuredNo").value="<%=strCustomerNo%>";
		                parent.fraInterface.fm.all("AddressNo").value="<%=strAddressNo%>";  
		                if (parent.fraInterface.fm.all("FamilyType").value=="1"){  
		                if(parent.fraInterface.InsuredGrid.getSelNo()==0)
		                {
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,1,"<%=mLCInsuredSchema.getInsuredNo()%>");              
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,2,"<%=StrTool.unicodeToGBK(mLCInsuredSchema.getName())%>");                   
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,3,"<%=mLCInsuredSchema.getSex()%>");                    
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,4,"<%=mLCInsuredSchema.getBirthday()%>");               
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,5,"<%=mLCInsuredSchema.getRelationToMainInsured()%>");  
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.mulLineCount-1,6,"<%=mLCInsuredSchema.getSequenceNo()%>");             
		                }
		                else
		                {
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.getSelNo()-1,1,"<%=mLCInsuredSchema.getInsuredNo()%>");
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.getSelNo()-1,2,"<%=StrTool.unicodeToGBK(mLCInsuredSchema.getName())%>");
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.getSelNo()-1,3,"<%=mLCInsuredSchema.getSex()%>");
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.getSelNo()-1,4,"<%=mLCInsuredSchema.getBirthday()%>");
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.getSelNo()-1,5,"<%=mLCInsuredSchema.getRelationToMainInsured()%>");
                                  parent.fraInterface.InsuredGrid.setRowColData(parent.fraInterface.InsuredGrid.getSelNo()-1,6,"<%=mLCInsuredSchema.getSequenceNo()%>");   
                              }                       
                        }                                               
		            </SCRIPT>
		            <%
		        }
		        if (fmAction.equals("DELETE||CONTINSURED"))
		        {
		    	    
		            %>
		            <SCRIPT language="javascript">
		                parent.fraInterface.fm.all("InsuredNo").value=""; 
		                if (parent.fraInterface.fm.all("FamilyType").value=="1"){  
                            //删除被选中的客户
                            parent.fraInterface.InsuredGrid.delRadioTrueLine("InsuredGrid");
                    }
                    else {
                        parent.fraInterface.InsuredGrid.clearData();
                    }
                    if (parent.fraInterface.fm.all("ContType").value=="2"&&parent.fraInterface.fm.all("RelationToMainInsured").value=="00"){
                       parent.fraInterface.fm.all("ContNo").value="";
                       parent.fraInterface.fm.all("ProposalContNo").value="";
                    }
                    parent.fraInterface.PolGrid.clearData();
                    parent.fraInterface.emptyInsured();
                    
		            </SCRIPT>
		            <%
		        }
	        }
    }
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
  

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tContInsuredUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println("FlagStr:"+FlagStr+"Content:"+Content);
  
}//页面有效区
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

