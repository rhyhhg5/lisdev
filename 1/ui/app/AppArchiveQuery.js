/** 
 * 程序名称：LGWorkBoxInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-02-23 09:59:56
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
window.onfocus=myonfocus;


//提交后操作,服务器数据返回后执行的操作
// 查询按钮
function easyQuery(){		
  var strSql ="";    
  var manageCom=fm.all("ManageCom").value;
  if(fm.all("ManageCom").value==""&&fm.all("applicant").value==""&&fm.all("printNo").value==""&&fm.all("scanDate").value==""&&fm.all("type").value==""){
    alert("请至少填写一个查询条件!");
    return false;
  }
  
  //如果没有填写投保人客户号或者印刷号，则归档号区间和扫描起止日期至少要填写一个，并且扫描起止日期之间不能大于3个月
  // by zhangyang 2011-06-03
  if(!checkMessage())
  {
  	return false;
  }
  
	if((fm.all("startArchiveNo").value != null && fm.all("startArchiveNo").value != '')
	 		&& (fm.all("endArchiveNo").value == null || fm.all("endArchiveNo").value == ''))
	{
		fm.all("endArchiveNo").value = fm.all("startArchiveNo").value;
	}
	if((fm.all("endArchiveNo").value != null && fm.all("endArchiveNo").value != '')
	 		&& (fm.all("startArchiveNo").value == null || fm.all("startArchiveNo").value == ''))
	{
		fm.all("startArchiveNo").value = fm.all("endArchiveNo").value;
	}
	if(fm.all("startArchiveNo").value > fm.all("endArchiveNo").value)
	{
		alert("起始号码不得大于终止号码！");
		return ;
	}
	//add by hyy
  //var saleChnlCode = fm.all("SaleChnlCode").value ;
  //var agentComBank = fm.all("AgentComBank").value ;
  
  //window.alert(saleChnlCode) ;
  //window.alert(agentComBank) ;
  var appntPart = "";
  var appntNo = fm.all("applicant").value != null ? fm.all("applicant").value : "";
  var tSaleChnlCode = fm.all("SaleChnlCode").value != null ? fm.all("SaleChnlCode").value : "";
  var tAgentComBank = fm.all("AgentComBank").value != null ? fm.all("AgentComBank").value : "";
  if(appntNo != "" || tSaleChnlCode != "" ||tAgentComBank != "")
  {
    var tTmpSqlAppntNo = appntNo != "" ? " and AppntNo = '" + appntNo + "' " : "";
    var tTmpSqlSaleChnlCode = tSaleChnlCode != "" ? " and SaleChnl = '" + tSaleChnlCode + "' " : "";
    var tTmpSqlAgentComBank = tAgentComBank != "" ? " and AgentCom = '" + tAgentComBank + "' " : "";
    
    appntPart = "  and exists (select 1 from LCCont where  PrtNo = m.DocCode "
                            + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + "union select 1 from LBCont where PrtNo = m.DocCode "
                             + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + "union select 1 from LCGrpCont where PrtNo = m.DocCode "
                             + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + "union select 1 from LBGrpCont where PrtNo = m.DocCode " 
                             + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + " ) ";
  }
  
  var subtype = "";
  if(fm.type.value == 'TB01')
  {
     subtype = " and subtype in ('TB01','TB10','TB27','TB28','TB29') ";
  }
  if(fm.type.value == 'TB02')
  {
     subtype = " and subtype in ('TB02', 'TB08', 'TB09') ";
  }
  if(fm.type.value == 'TB04')
  {
     subtype = " and subtype in ('TB04','TB06','TB24')";
  }
  strSql="select m.ArchiveNo,m.DocCode,'','',"
          + "(case m.SubType when 'TB01' then '个单' when 'TB02' then '团单' when 'TB08' then '团单' when 'TB09' then '团单' when 'TB04' then '银保' when 'TB24' then '银保' when 'TB06' then '银保' when 'TB10' then '个单' when 'TB27' then '个单' when 'TB28' then '个单'  when 'TB29' then '个单' end),'',m.MakeDate"+
          " ,NumPages ,'','' " +
           " from ES_DOC_Main m "+
           " where m.ManageCom like '"+manageCom+"%'"+
           " and (m.SubType='TB01' or m.SubType='TB02' or m.SubType='TB08' or m.SubType='TB09' or m.SubType='TB04' or m.SubType='TB06' or m.SubType='TB24' or m.SubType='TB10' or m.SubType='TB27' or m.SubType='TB28'or m.SubType='TB29') "
           + getWherePart("DocCode", "printNo")
           + getWherePart("MakeDate", "scanDate", ">=")
           + getWherePart("MakeDate", "scanEndDate", "<=")
//         + getWherePart("SubType", "type")
		   + getWherePart("m.ArchiveNo", "startArchiveNo", ">=")//2007-10-23 
		   + getWherePart("m.ArchiveNo", "endArchiveNo", "<=")//2007-10-23 
		   + subtype
           + appntPart
           + " order by m.ArchiveNo desc with ur";
  turnPage2.queryModal(strSql,LarryBoxGrid1);
  addMul();
}

function addMul(){
  var sql=""
    for(i = 0; i < LarryBoxGrid1.mulLineCount; i++){        
        if(LarryBoxGrid1.getRowColData(i,5)=="个单" || LarryBoxGrid1.getRowColData(i,5)=="银保")
          sql="select ContNo,AppntName,CodeName('stateflag', StateFlag),SaleChnl,AgentCom "+
                  "from LCCont "+
                  "where PrtNo='"+LarryBoxGrid1.getRowColData(i,2)+"' "+
                  "union "+
                  "select ContNo,AppntName,CodeName('stateflag', StateFlag),SaleChnl,AgentCom "+
                  "from LBCont "+
                  "where PrtNo='"+LarryBoxGrid1.getRowColData(i,2)+"' ";
        else
          sql="select GrpContNo,GrpName,CodeName('stateflag', StateFlag),SaleChnl,AgentCom "+
                  "from LCGrpCont "+
                  "where PrtNo='"+LarryBoxGrid1.getRowColData(i,2)+"' "+
                  "union "+
                  "select GrpContNo,'',CodeName('stateflag', StateFlag),SaleChnl,AgentCom "+
                  "from LBGrpCont "+
                  "where PrtNo='"+LarryBoxGrid1.getRowColData(i,2)+"' ";
        var rs = easyExecSql(sql);
	    if(rs){
	      LarryBoxGrid1.setRowColDataByName(i, "billName", rs[0][0]);
	      LarryBoxGrid1.setRowColDataByName(i, "appent", rs[0][1]);
	      LarryBoxGrid1.setRowColDataByName(i, "billState", rs[0][2]);
          LarryBoxGrid1.setRowColDataByName(i, "saleChnl", rs[0][3]);
          LarryBoxGrid1.setRowColDataByName(i, "agentCom", rs[0][4]);
	    }
   }
}



//归档号打印
function printList()
{
	if (LarryBoxGrid1.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}	
	if((fm.all("startArchiveNo").value != null && fm.all("startArchiveNo").value != '')
	 		&& (fm.all("endArchiveNo").value == null || fm.all("endArchiveNo").value == ''))
	{
		fm.all("endArchiveNo").value = fm.all("startArchiveNo").value;
	}
	if((fm.all("endArchiveNo").value != null && fm.all("endArchiveNo").value != '')
	 		&& (fm.all("startArchiveNo").value == null || fm.all("startArchiveNo").value == ''))
	{
		fm.all("startArchiveNo").value = fm.all("endArchiveNo").value;
	}
	if(fm.all("startArchiveNo").value > fm.all("endArchiveNo").value)
	{
		alert("起始号码不得大于终止号码！");
		return ;
	}
	if(!checkNum())//校验打印条数    zhangjianbao    2007-11-8  modify（2014-11-14） by zjd 原 300条修改为 10000条打印由vts改成excel。
	{
		alert("打印条数大于10000，请缩小打印范围！");
		return ;
	}
	
  var sql = "";
  var manageCom=fm.all("ManageCom").value;
  
  var appntPart = "";
  //var appntNo = fm.all("applicant").value;
  var appntNo = fm.all("applicant").value != null ? fm.all("applicant").value : "";
  var tSaleChnlCode = fm.all("SaleChnlCode").value != null ? fm.all("SaleChnlCode").value : "";
  var tAgentComBank = fm.all("AgentComBank").value != null ? fm.all("AgentComBank").value : "";
  var subtype = "";
  if(fm.type.value == 'TB01')
  {
     subtype = " and subtype in ('TB01','TB10','TB27','TB28','TB29') ";
  }
  if(fm.type.value == 'TB02')
  {
     subtype = " and subtype in ('TB02', 'TB08', 'TB09') ";
  }
  if(fm.type.value == 'TB04')
  {
     subtype = " and subtype in ('TB04','TB06','TB24')";
  }

  
  if((appntNo != "" || tSaleChnlCode != "" ||tAgentComBank != "") && (fm.all("type").value == "TB01" || fm.all("type").value == "TB04" || fm.all("type").value == "TB24"))
  {
    //appntPart = " and exists (select 1 from LCCont where  PrtNo = m.DocCode and AppntNo = '" + appntNo + "' "
    //                         + "union select 1 from LBCont where PrtNo = m.DocCode and AppntNo = '" + appntNo + "' ";
    var tTmpSqlAppntNo = appntNo != "" ? " and AppntNo = '" + appntNo + "' " : "";
    var tTmpSqlSaleChnlCode = tSaleChnlCode != "" ? " and SaleChnl = '" + tSaleChnlCode + "' " : "";
    var tTmpSqlAgentComBank = tAgentComBank != "" ? " and AgentCom = '" + tAgentComBank + "' " : "";
    
    appntPart = "  and exists (select 1 from LCCont where  PrtNo = m.DocCode "
                            + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + "union select 1 from LBCont where PrtNo = m.DocCode "
                             + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + " ) ";
    
  } 
  else if((appntNo != "" || tSaleChnlCode != "" ||tAgentComBank != "") && fm.all("type").value == "TB02")
  {
    //appntPart = " and exists (select 1 from LCGrpCont where PrtNo = m.DocCode and AppntNo = '" + appntNo + "' "
    //                         + "union select 1 from LBGrpCont where PrtNo = m.DocCode and AppntNo = '" + appntNo + "') ";
    var tTmpSqlAppntNo = appntNo != "" ? " and AppntNo = '" + appntNo + "' " : "";
    var tTmpSqlSaleChnlCode = tSaleChnlCode != "" ? " and SaleChnl = '" + tSaleChnlCode + "' " : "";
    var tTmpSqlAgentComBank = tAgentComBank != "" ? " and AgentCom = '" + tAgentComBank + "' " : "";
    
    appntPart = "  and exists ( select 1 from LCGrpCont where PrtNo = m.DocCode "
                             + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + "union select 1 from LBGrpCont where PrtNo = m.DocCode " 
                             + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + " ) ";
  }
  
	//组合 SQL 			修改：m.NumPages 为 ''  2007-10-23
	if(fm.all("type").value == "TB01" || fm.all("type").value == "TB04" || fm.all("type").value == "TB24")
	{
	  sql = "select m.ArchiveNo,c.managecom,nvl((select name from ldcom where comcode=c.managecom),''),( CASE m.SubType WHEN 'TB01' THEN '个单' WHEN 'TB02' THEN '团单' WHEN 'TB08' THEN			'团单'		WHEN 'TB09' THEN			'团单'		WHEN 'TB04' THEN			'银保'		WHEN 'TB24' THEN			'银保'		WHEN 'TB06' THEN			'银保'		WHEN 'TB10' THEN			'个单'		WHEN 'TB27' THEN			'个单'		WHEN 'TB28' THEN			'个单'		WHEN 'TB29' THEN			'个单'		END	), m.DocCode,c.contno,c.appntname,c.CVALIDATE,c.CINVALIDATE,CodeName('stateflag', c.StateFlag)"
		  	+ ",CodeName('salechnl', c.salechnl), a.name, (select sum(numpages) from ES_DOC_Main where doccode like c.prtno||'%'and c.salechnl <> '02' and c.salechnl<>'03' ) "
					+ "from laagent a, ES_DOC_Main m, lccont c  "
					+ "where m.ManageCom like '" + manageCom + "%' " 
						+ getWherePart("m.MakeDate", "scanDate", ">=") 
						+ getWherePart("m.MakeDate", "scanEndDate", "<=") 
						+ getWherePart("DocCode", "printNo")
						//+ getWherePart("SubType", "type")
						+ getWherePart("m.ArchiveNo", "startArchiveNo", ">=")//2007-10-23 
						+ getWherePart("m.ArchiveNo", "endArchiveNo", "<=")//2007-10-23 
                        + getWherePart("c.SaleChnl", "SaleChnlCode")
                        + getWherePart("c.AgentCom", "AgentComBank")
                        + subtype
						+ " and c.prtno = m.DocCode "
						+ "and c.conttype = '1'"
						+ "and a.agentcode = c.agentcode "
						+ " union "
						+" select m.ArchiveNo,c.managecom,nvl((select name from ldcom where comcode=c.managecom),''),( CASE m.SubType WHEN 'TB01' THEN '个单' WHEN 'TB02' THEN '团单' WHEN 'TB08' THEN			'团单'		WHEN 'TB09' THEN			'团单'		WHEN 'TB04' THEN			'银保'		WHEN 'TB24' THEN			'银保'		WHEN 'TB06' THEN			'银保'		WHEN 'TB10' THEN			'个单'		WHEN 'TB27' THEN			'个单'		WHEN 'TB28' THEN			'个单'		WHEN 'TB29' THEN			'个单'		END	), m.DocCode,c.contno,c.appntname,c.CVALIDATE,c.CINVALIDATE,CodeName('stateflag', c.StateFlag)"
					  	+ ",CodeName('salechnl', c.salechnl), a.name, (select sum(numpages) from ES_DOC_Main where doccode like c.prtno||'%'and c.salechnl <> '02' and c.salechnl<>'03' ) "
						+ "from laagent a, ES_DOC_Main m, lbcont c "
						+ "where m.ManageCom like '" + manageCom + "%' "
						+ getWherePart("m.MakeDate", "scanDate", ">=")
						+ getWherePart("m.MakeDate", "scanEndDate", "<=")
						+ getWherePart("DocCode", "printNo")
						//+ getWherePart("SubType", "type")
						+ getWherePart("m.ArchiveNo", "startArchiveNo", ">=")//2007-10-23 
						+ getWherePart("m.ArchiveNo", "endArchiveNo", "<=")//2007-10-23 
                        + getWherePart("c.SaleChnl", "SaleChnlCode")
                        + getWherePart("c.AgentCom", "AgentComBank")
                        + subtype
						+ " and c.prtno = m.DocCode "
						+ "and c.conttype = '1'"
						+ "and a.agentcode = c.agentcode "
						+ " union "
						+ "select m.ArchiveNo, '','','', m.DocCode,'','',INPUTSTARTDATE,INPUTENDDATE,'','','',( SELECT  sum(NUMPAGES) FROM ES_DOC_Main WHERE doccode LIKE m.doccode || '%' )"
						+ "from ES_DOC_Main m "
						+ "where m.ManageCom like '" + manageCom + "%' "
						+ getWherePart("MakeDate", "scanDate", ">=")
						+ getWherePart("MakeDate", "scanEndDate", "<=")
						+ getWherePart("DocCode", "printNo")
						//+ getWherePart("SubType", "type")
						+ getWherePart("m.ArchiveNo", "startArchiveNo", ">=")//2007-10-23 
						+ getWherePart("m.ArchiveNo", "endArchiveNo", "<=")//2007-10-23 
						+ subtype
						+ appntPart
						//+ " and m.DocCode not in (select prtno from lccont union select prtno from lbcont) "
                        + " and not exists (select 1 from lccont lcc where m.DocCode = lcc.prtno union all select 1 from lbcont lbc where m.DocCode = lbc.prtno ) "
						+ " order by ArchiveNo"
    ;
	}
	else if(fm.all("type").value == "TB02")
	{
	  sql =  "select m.ArchiveNo,c.managecom,nvl((select name from ldcom where comcode=c.managecom),''),( CASE m.SubType WHEN 'TB01' THEN '个单' WHEN 'TB02' THEN '团单' WHEN 'TB08' THEN			'团单'		WHEN 'TB09' THEN			'团单'		WHEN 'TB04' THEN			'银保'		WHEN 'TB24' THEN			'银保'		WHEN 'TB06' THEN			'银保'		WHEN 'TB10' THEN			'个单'		WHEN 'TB27' THEN			'个单'		WHEN 'TB28' THEN			'个单'		WHEN 'TB29' THEN			'个单'		END	), m.DocCode,c.grpcontno,c.grpname,c.CVALIDATE,c.CINVALIDATE,CodeName('stateflag', c.StateFlag)"
		  	+ ",CodeName('salechnl', c.salechnl), a.name, (select sum(numpages) from ES_DOC_Main where doccode like c.prtno||'%'and c.salechnl <> '02' and c.salechnl<>'03' ) "
					+ "from laagent a, ES_DOC_Main m, lcgrpcont c "
					+ "where m.ManageCom like '" + manageCom + "%' " 
						+ getWherePart("m.MakeDate", "scanDate", ">=") 
						+ getWherePart("m.MakeDate", "scanEndDate", "<=") 
						+ getWherePart("DocCode", "printNo")
						//+ getWherePart("SubType", "type")
                        + subtype
						+ getWherePart("m.ArchiveNo", "startArchiveNo", ">=")//2007-10-23 
						+ getWherePart("m.ArchiveNo", "endArchiveNo", "<=")//2007-10-23 
                        + getWherePart("c.SaleChnl", "SaleChnlCode")
                        + getWherePart("c.AgentCom", "AgentComBank")
						+ " and c.prtno = m.DocCode "
						+ "and a.agentcode = c.agentcode "
					+ " union "
					+"select m.ArchiveNo,c.managecom,nvl((select name from ldcom where comcode=c.managecom),''),( CASE m.SubType WHEN 'TB01' THEN '个单' WHEN 'TB02' THEN '团单' WHEN 'TB08' THEN			'团单'		WHEN 'TB09' THEN			'团单'		WHEN 'TB04' THEN			'银保'		WHEN 'TB24' THEN			'银保'		WHEN 'TB06' THEN			'银保'		WHEN 'TB10' THEN			'个单'		WHEN 'TB27' THEN			'个单'		WHEN 'TB28' THEN			'个单'		WHEN 'TB29' THEN			'个单'		END	), m.DocCode,c.grpcontno,c.grpname,c.CVALIDATE,c.CINVALIDATE,CodeName('stateflag', c.StateFlag)"
				  	+ ",CodeName('salechnl', c.salechnl), a.name, (select sum(numpages) from ES_DOC_Main where doccode like c.prtno||'%'and c.salechnl <> '02' and c.salechnl<>'03' ) "
					+ "from laagent a, ES_DOC_Main m, lbgrpcont c "
					+ "where m.ManageCom like '" + manageCom + "%' "
						+ getWherePart("m.MakeDate", "scanDate", ">=")
						+ getWherePart("m.MakeDate", "scanEndDate", "<=")
						+ getWherePart("m.ArchiveNo", "startArchiveNo", ">=")//2007-10-23 
						+ getWherePart("m.ArchiveNo", "endArchiveNo", "<=")//2007-10-23 
						+ getWherePart("DocCode", "printNo")
						//+ getWherePart("SubType", "type")
                        + subtype
                        + getWherePart("c.SaleChnl", "SaleChnlCode")
                        + getWherePart("c.AgentCom", "AgentComBank")
						+ " and c.prtno = m.DocCode "
						+ "and a.agentcode = c.agentcode "
					+ " union "
					+ "select m.ArchiveNo, '','','', m.DocCode,'','',INPUTSTARTDATE,INPUTENDDATE,'','','',( SELECT  sum(NUMPAGES) FROM ES_DOC_Main WHERE doccode LIKE m.doccode || '%' )"
					+ "from ES_DOC_Main m "
					+ "where m.ManageCom like '" + manageCom + "%' "
						+ getWherePart("MakeDate", "scanDate", ">=")
						+ getWherePart("MakeDate", "scanEndDate", "<=")
						+ getWherePart("DocCode", "printNo")
						//+ getWherePart("SubType", "type")
                        + subtype
						+ getWherePart("m.ArchiveNo", "startArchiveNo", ">=")//2007-10-23 
						+ getWherePart("m.ArchiveNo", "endArchiveNo", "<=")//2007-10-23 
						+ appntPart
						//+ " and m.DocCode not in (select prtno from lcgrpcont union select prtno from lbgrpcont) "
                        + " and not exists (select 1 from lcgrpcont lgc where m.DocCode = lgc.prtno union all select 1 from lbgrpcont lbgc where m.DocCode = lbgc.prtno) "
						+ " order by ArchiveNo"
    ;
	}
	else
	{
		alert("请选择保单类型！");
		return false;
	}
	//alert(sql.replace(new RegExp("%","g"), '$'));return false;
  fm.QuerySql.value = sql;
	fm.action = "AppArchiveQueryPrint.jsp";
	fm.target = "_blank";//弹出新窗口
	fm.submit();
}

//校验打印条数    zhangjianbao    2007-11-8
function checkNum()
{
  var strSql = "";
  var manageCom=fm.all("ManageCom").value;
  var appntPart = "";
  //var appntNo = fm.all("applicant").value;
  var appntNo = fm.all("applicant").value != null ? fm.all("applicant").value : "";
  var tSaleChnlCode = fm.all("SaleChnlCode").value != null ? fm.all("SaleChnlCode").value : "";
  var tAgentComBank = fm.all("AgentComBank").value != null ? fm.all("AgentComBank").value : "";
  
  if(appntNo != "" || tSaleChnlCode != "" || tAgentComBank != "")
  {
    //appntPart = "  and exists (select 1 from LCCont where  PrtNo = m.DocCode and AppntNo = '" + appntNo + "' "
    //                         + "union select 1 from LBCont where PrtNo = m.DocCode and AppntNo = '" + appntNo + "' "
    //                         + "union select 1 from LCGrpCont where PrtNo = m.DocCode and AppntNo = '" + appntNo + "' "
    //                         + "union select 1 from LBGrpCont where PrtNo = m.DocCode and AppntNo = '" + appntNo + "') ";
    var tTmpSqlAppntNo = appntNo != "" ? " and AppntNo = '" + appntNo + "' " : "";
    var tTmpSqlSaleChnlCode = tSaleChnlCode != "" ? " and SaleChnl = '" + tSaleChnlCode + "' " : "";
    var tTmpSqlAgentComBank = tAgentComBank != "" ? " and AgentCom = '" + tAgentComBank + "' " : "";
        appntPart = "  and exists (select 1 from LCCont where  PrtNo = m.DocCode "
                            + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + "union select 1 from LBCont where PrtNo = m.DocCode "
                             + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + "union select 1 from LCGrpCont where PrtNo = m.DocCode "
                             + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + "union select 1 from LBGrpCont where PrtNo = m.DocCode " 
                             + tTmpSqlAppntNo + tTmpSqlSaleChnlCode + tTmpSqlAgentComBank
                             + " ) ";
  }
  
  var subtype = "";
  if(fm.type.value == 'TB01')
  {
     subtype = " and subtype in ('TB01','TB10','TB27','TB29','TB28') ";
  }
  if(fm.type.value == 'TB02')
  {
     subtype = " and subtype in ('TB02', 'TB08', 'TB09') ";
  }
  if(fm.type.value == 'TB04')
  {
     subtype = " and subtype in ('TB04','TB06','TB24')";
  }
  strSql="select count(*), min(m.ArchiveNo), max(m.ArchiveNo) from ES_DOC_Main m "
  				+ " where m.ManageCom like '"+manageCom+"%'"
  				+ " and (m.SubType='TB01' or m.SubType='TB02' or m.SubType='TB08' or m.SubType='TB09' or m.SubType='TB04' or m.SubType='TB06' or m.SubType='TB24' or m.SubType='TB10' or m.SubType='TB27' or m.SubType='TB29' or m.subtype='TB28') "
  				+ getWherePart("DocCode", "printNo")
          		+ getWherePart("MakeDate", "scanDate", ">=")
          		+ getWherePart("MakeDate", "scanEndDate", "<=")
  				//+ getWherePart("SubType", "type")
  				+ getWherePart("m.ArchiveNo", "startArchiveNo", ">=")
  				+ getWherePart("m.ArchiveNo", "endArchiveNo", "<=")
  				+ subtype
  				+ appntPart
                + " with ur ";
           
	var result = easyExecSql(strSql);
	if(!result)
  	{
		alert('未查询出合同保费相关信息。');
		return false;
  	}
  	//alert(result[0][0] + "****" + result[0][1] + "****" + result[0][2]);
	if(result[0][0] > 300)
	{
		return false;
	}
  	fm.minArchiveNo.value = result[0][1];  	
  	fm.maxArchiveNo.value = result[0][2];  	
	return true;
}



//add by hyy

/**
 * 下拉框，选定后触发事件。
 */
function afterCodeSelect(tCodeName, tObj)
{
    if (tCodeName == "lcsalechnl")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    showInfo=window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
    //alert(showInfo.fm);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
    }
}

/**
 * 归档号维护
 */
function maintainArchiveNo()
{
  fm.action = "MaintainArchiveNo.jsp";
  fm.target = "_blank";//弹出新窗口
  fm.submit();
}

/**
 * 如果没有填写投保人客户号或者印刷号，则归档号区间和扫描起止日期至少要填写一个，并且扫描起止日期之间不能大于3个月
 * by zhangyang 2011-06-03
 */
function checkMessage()
{
	//只有投保人客户号或者印刷号都不填写才进行校验
	if((fm.all("applicant").value == null || fm.all("applicant").value == "") 
  		&& (fm.all("printNo").value == null || fm.all("printNo").value == ""))
	{
		//归档号区间和扫描起止日期至少要填写一个
		if((fm.all("startArchiveNo").value == null || fm.all("startArchiveNo").value == "")
			&& (fm.all("endArchiveNo").value == null || fm.all("endArchiveNo").value == "")
			&& (fm.all("scanDate").value == null || fm.all("scanDate").value == "")
			&& (fm.all("scanEndDate").value == null || fm.all("scanEndDate").value == ""))
		{
			alert("由于没有填写投保人客户号或者印刷号，所以归档号区间和扫描起止日期至少要填写一个!");
			return false;
		}
		
		//只填扫描日期起
		if((fm.all("scanDate").value == null || fm.all("scanDate").value == "")
			&& (fm.all("scanEndDate").value != null && fm.all("scanEndDate").value != ""))
		{
			alert("请选择扫描日期起！");
			return false;
		}
		
		//只填扫描日期止
		if((fm.all("scanDate").value != null && fm.all("scanDate").value != "")
			&& (fm.all("scanEndDate").value == null || fm.all("scanEndDate").value == ""))
		{
			alert("请选择扫描日期止！");
			return false;
		}
		
		//扫描日期区间不能大于3个月
		if(fm.all("scanDate").value != null && fm.all("scanDate").value != "" 
			&& fm.all("scanEndDate").value != null && fm.all("scanEndDate").value != "")
		{
			var tStartDate = fm.all("scanDate").value;
			var tEndDate = fm.all("scanEndDate").value;
			if(dateDiff(tStartDate,tEndDate,"M")>3)
			{
				alert("统计期最多为三个月！");
				return false;
			}
		}
	}
	return true;
}
