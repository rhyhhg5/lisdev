<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    

<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
  
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox(); 
    queryClick();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initGrpContGrid();
	divPage.style.display="";                 
    fm.all('ManageCom').value = ManageCom; 
    var tMSQL = "select name from ldcom where comcode = '"+ManageCom+"'";
	var strMResult = easyExecSql(tMSQL);
	if(strMResult){
		fm.all('ManageComName').value = strMResult[0][0];
	}
    fm.all('PrtNo').value = tPrtNo;  
}

var GrpContGrid
function initGrpContGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="印刷号码";         	  //列名
    iArray[1][1]="60px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="客户号码";         	
    iArray[2][1]="60px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="投保单位名称";         	  //列名
    iArray[3][1]="120px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="签单时间";      //列名
    iArray[4][1]="60px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="录入人员";      //列名
    iArray[5][1]="60px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
     
    GrpContGrid = new MulLineEnter("fm", "GrpContGrid"); 
    //设置Grid属性
    GrpContGrid.mulLineCount = 0;
    GrpContGrid.displayTitle = 1;
    GrpContGrid.locked = 1;
    GrpContGrid.canSel = 0;
    GrpContGrid.canChk = 0;
    GrpContGrid.hiddenSubtraction = 1;
    GrpContGrid.hiddenPlus = 1;
   
    GrpContGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
