var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;

window.onfocus = myonfocus;

// 查询按钮
function easyQueryClick()
{
	initPolGrid();
	if(!verifyInput2())
		return false;
	
	if(fm.PrtNo.value == "" && fm.ContNo.value == "")
	{
		alert("印刷号和保单号至少录入一个！");
		return false;
	}
	if(fm.ContType.value==""){
		alert("保单类型不能为空！");
		return false;
	}
	
	
	// 书写SQL语句
    var strSQL = "";
    if(fm.ContType.value == 1)
    {
    	strSQL = "select lcc.Contno,lcc.Prtno,lcc.salechnl||(select CodeName from ldcode where codetype = 'salechnl' and Code = lcc.SaleChnl),lcc.crs_salechnl||(select CodeName from ldcode where codetype = 'crs_salechnl' and Code = lcc.crs_SaleChnl ),lcc.crs_busstype||(select CodeName from ldcode where codetype = 'crs_busstype' and Code = lcc.crs_busstype ),lcc.managecom||(select Name from ldcom where comcode=lcc.managecom)," 
    	+ "lcc.ContType "
    	+ "from lccont lcc "
    	+ "where 1 = 1 "
    	+ " and lcc.ContType = '1' "
    	+ getWherePart('lcc.ContNo','ContNo')
        + getWherePart('lcc.PrtNo','PrtNo')
        + getWherePart('lcc.ManageCom','ManageCom','like')       
    }
    else if(fm.ContType.value == 2)
    {
    	strSQL = "select lgc.GrpContno,lgc.Prtno,lgc.salechnl||(select CodeName from ldcode where codetype = 'salechnl' and Code = lgc.SaleChnl),lgc.crs_salechnl||(select CodeName from ldcode where codetype = 'crs_salechnl' and Code = lgc.crs_SaleChnl )  ,lgc.crs_busstype||(select CodeName from ldcode where codetype = 'crs_busstype' and Code = lgc.crs_busstype ),lgc.managecom||(select Name from ldcom where comcode=lgc.managecom),2 as conttype  " 
        + "from lcgrpcont lgc "
    	+ "where 1 = 1 "    	
    	+ getWherePart('lgc.GrpContNo','ContNo')
        + getWherePart('lgc.PrtNo','PrtNo')
        + getWherePart('lgc.ManageCom','ManageCom','like')      
    }else{
    	alert("保单类型输入错误");
    	return false;
    }
    turnPage.pageLineNum = 10;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 10);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    fm.querySql.value = strSQL;
}

//维护销售渠道与业务员
function modify()
{
	var i = 0;
	var checkFlag = 0;
	var action = "modify"
	
	for(i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getSelNo(i))
		{
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	
	if(checkFlag == 0)
	{
		alert("请先选择一条保单信息！");
		return false;
	}
	
	var contno = PolGrid.getRowColData(checkFlag - 1, 1);
	var prtno = PolGrid.getRowColData(checkFlag - 1, 2);
	var conttype = PolGrid.getRowColData(checkFlag - 1, 7);
	
	var tStrURL = "./Crsnance.jsp?ContNo=" + contno + "&PrtNo=" + prtno + "&ContType=" + conttype + "&Action=" + action;
	window.location = tStrURL;
}
