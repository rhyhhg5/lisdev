<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2005-12-19 16:16
//创建人  ：yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
CErrors tError = null;
String FlagStr = "Fail";
String Content = "";
GlobalInput tG = (GlobalInput) session.getValue("GI");
String tMissionID = request.getParameter("MissionID");
String tActivityID = request.getParameter("ActivityID");
String tContNo = request.getParameter("ContNo");
String tSubMissionID = request.getParameter("SubMissionID");
String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
String sOutXmlPath = application.getRealPath("");	//xml文件输出路径
System.out.println("数据路径szTemplatePath +"+szTemplatePath);
System.out.println("数据路径sOutXmlPath +"+sOutXmlPath);
TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("ContNo", tContNo);
tTransferData.setNameAndValue("MissionID", tMissionID);
tTransferData.setNameAndValue("SubMissionID", tSubMissionID);
tTransferData.setNameAndValue("ActivityID", tActivityID);
tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );
try
{
  VData tVData = new VData();
  tVData.add(tTransferData);
  tVData.add(tG);
  TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
  if (tTbWorkFlowUI.submitData(tVData, tActivityID) == false)
  {
    int n = tTbWorkFlowUI.mErrors.getErrorCount();
    for (int i = 0; i < n; i++)
      Content = " 人工核保失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tTbWorkFlowUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 人工核保操作成功!";
      FlagStr = "Succ";
    }
    else
    {
      FlagStr = "Fail";
    }
  }
}
catch (Exception e)
{
  e.printStackTrace();
  Content = Content.trim() + ".提示：异常终止!";
}
%>
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
