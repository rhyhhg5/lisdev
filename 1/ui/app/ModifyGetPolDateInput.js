//程序名称：
//程序功能：
//创建日期：2010-3-25
//创建人  ：GUZG	
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 与投保人的关系
 */
function queryRelationToAppnt()
{    
    var cContNo = fm.ContNo.value;
    var tStrSql = "select  customgetpoldate,managecom,salechnl "
        + " from LCCont a "
        + " where 1 = 1 "
        + " and contno = '" + cContNo + "' and managecom like '"+fm.all('ManageCom').value+"%' ";
    
    var arrResult = easyExecSql(tStrSql);
    if(arrResult != null){
        var tGetPolDate = arrResult[0][0];
        fm.all('tempManageCom').value= arrResult[0][1];
        fm.all('tempSaleChnl').value= arrResult[0][2];
    	fm.GetPolDate.value = tGetPolDate;
    }
    
    
    return true;
}

/**
 * 修改与被保人的关系
 */
function submitData()
{    
   if(fm.GetPolDateNew.value == "" || fm.GetPolDateNew.value==null)
    {   
        alert("修改后回执回销日期不能为空");
        return false;
    }
	if(!verifyInput()){
		return false;
	}
	var tSaleState=fm.all('SaleState').value;
	if(tSaleState=='1'){
		if(fm.all('WageNo').value==null || fm.all('WageNo').value==''){
			alert("要修改销售提奖，请指定薪资月");
			return false;
		}
		if(fm.all('tempSaleChnl').value==null || fm.all('tempSaleChnl').value==''){
			alert("要修改销售提奖，没有得到该保单的销售渠道信息");
			return false;
		}
		var tStrSql = "select  state "
        + " from Lawage a "
        + " where 1 = 1 "
        + " and managecom = '" + fm.all('tempManageCom').value + "' and indexcalno='"
        +fm.all('WageNo').value+"' and branchtype=(select code1 from ldcode1 where codetype='salechnl' and code='"+fm.all('tempSaleChnl').value+"')";
    
    	var arrResult = easyExecSql(tStrSql);
    	if(arrResult != null && arrResult[0][0]>'0'){
        	alert("该机构该年月的薪资已经确认，请重新指定薪资月");
        	return false;
    	}
	}
	
    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.action = "./ModifyGetPolDateSave.jsp";
    fm.submit();
}


function afterImportSubmit( FlagStr, content )
{
	showInfo.close();
	window.focus();
	mAction = ""; 

	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		showDiv(operateButton, "true"); 
		showDiv(inputButton, "false");
		top.close();
	}
}