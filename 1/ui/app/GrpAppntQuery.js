//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage=new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPersonGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
//    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//选择页面上查询的字段对应于"select *"中的位置
function getSelArray()
{
	var arrSel = new Array();
	
	arrSel[0] = 0;
	arrSel[1] = 2;
	arrSel[2] = 3;
	arrSel[3] = 4;
	arrSel[4] = 16;
	arrSel[5] = 18;

	return arrSel;
}

function returnParent()
{
	var arrReturn = new Array();
	var tSel = PersonGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterAppntQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PersonGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = PersonGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}


function afterQuery()
{
    var tStrCustomerNoCont = "";
    if(CustomerNo != null && CustomerNo != "" && CustomerNo != "null")
    {
        tStrCustomerNoCont = " and ldg.CustomerNo = '" + CustomerNo + "' ";
    }
    
    var tStrSql = ""
        + " select "
        + " ldg.Customerno, ldg.Grpname, ldg.BusinessType,ldg.organcomcode "
        + " from LDGrp ldg "
        + " where 1 = 1 "
        + " and ldg.Grpname like '%" + Grpname + "%' "
        + tStrCustomerNoCont
        ;

	// var strSQL="select Customerno,Grpname,BusinessType from LDGrp where Grpname like '%%" + Grpname + "%%' and CustomerNo like '%%" + CustomerNo + "%%' ";
	turnPage.queryModal(tStrSql, PersonGrid);
}
