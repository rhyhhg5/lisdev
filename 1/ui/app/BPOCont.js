//程序功能：
//创建日期：2007-10-19 18:00
//创建人  ：YangYalin
//更新记录：更新人    更新日期     更新原因/内容

var showInfo;

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var turnPage5 = new turnPageClass();
var turnPage6 = new turnPageClass();
var turnPage7 = new turnPageClass();
var turnPage8 = new turnPageClass();

var tDefaultStartDate;

//----------------------------------初始化区域-------------------------------

//初始化隐藏空间
function initHiddenInputBox()
{
  fm.BPOBatchNo.value = tBPOBatchNo;
  fm.MissionID.value = tMissionID;
  fm.PrtNoOld.value = tPrtNo;
  fm.PrtNo.value = tPrtNo;
  
  var sql = "select ContID "
          + "from BPOLCPol "
          + "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
          + "   and PrtNo = '" + fm.PrtNoOld.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.ContID.value = rs[0][0];
  }
  
  fm.all("IDFirstTrialDateTitle").style.display = "none";
  fm.all("IDFirstTrialDate").style.display = "none";
  
  var sql = "select SubType from ES_DOC_Main "
          + " where DocCode = '" + fm.PrtNoOld.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.SubType.value = rs[0][0];
  }
  else
  {
    alert("没有扫描件，不能进行业务处理！");
    return ;
  }
	
	if("TB03" == fm.SubType.value)
	{
	  tIntlFlag = "1";
	  tCardFlag = "7";
	  tWrapType = "9"
	}
	else if("TB04" == fm.SubType.value)
	{
	  tIntlFlag = "0";
	  tCardFlag = "8";
	  tWrapType = "8"
	}
	else if("TB05" == fm.SubType.value)
	{
	  tIntlFlag = "0";
	  tCardFlag = "6";
	  tWrapType = "6"
	}
}

/*********************************************************************
 *  查询保单管理信息
 *  描述:
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryContManageInfo()
{
  var sql = "select a.ManageCom, a.ReceiveDate, a.PolApplyDate, a.FirstTrialOperator, "
          + "   '', a.SaleChnl, a.AgentCode, a.CValiDate, a.AgentCom "
          + ",a.Crs_SaleChnl,a.Crs_BussType,a.GrpAgentCom ,a.GrpAgentCode,a.GrpAgentName,a.GrpAgentIDNo "
          + "from BPOLCPol a "
          + "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
          + "   and a.ContID = '" + fm.ContID.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    //由于目前同意保单的以上险种信息相同，所以可以取第一行
    fm.ManageCom.value = rs[0][0];
    fm.ReceiveDate.value = rs[0][1];
    fm.PolApplyDate.value = rs[0][2];
    fm.FirstTrialOperator.value = rs[0][3];
    fm.SaleChnl.value = rs[0][5];
    fm.GroupAgentCode.value = rs[0][6];
    fm.CValiDate.value = rs[0][7];
    fm.AgentCom.value = rs[0][8];
    fm.AgentCom1.value = rs[0][8];
    fm.Crs_SaleChnl.value = rs[0][9];
    fm.Crs_BussType.value = rs[0][10];
    fm.GrpAgentCom.value = rs[0][11]; 
    fm.GrpAgentCode.value = rs[0][12];
    fm.GrpAgentName.value = rs[0][13];    
    fm.GrpAgentIDNo.value = rs[0][14];
    fm.AgentCode.value=easyExecSql(" select AgentCode from laagent where GroupAgentCode='"+rs[0][6]+"'");
    if(rs[0][9] != "" && rs[0][10] != "" &&rs[0][11]!=""&&rs[0][12]!=""&&rs[0][13]!=""&&rs[0][14]!=""){
        fm.MixComFlag.checked = true;
        if(fm.MixComFlag.checked == true)
        {
            fm.all('GrpAgentComID').style.display = "";
	        fm.all('GrpAgentTitleID').style.display = "";
	        fm.all('GrpAgentTitleIDNo').style.display = "";
        }
        var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
		var arrResult1 = easyExecSql(strSql);
		if (arrResult1 != null) {
		     fm.GrpAgentComName.value = arrResult1[0][0];
		}
		else{  
		     fm.GrpAgentComName.value = "";
	    }
    }
    
  }
}

//查询导入错误日志
function queryImportLog()
{
  var sql = "select a.ErrorInfo "
            + "from LCGrpImportLog a "
            + "where a.PrtNo = '" + fm.PrtNo.value + "' "
            + "order by a.MakeDate desc, a.MakeTime desc ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.BPOImportLog.value = rs[0][0];
  }
}

/*********************************************************************
 *  查询投保人信息
 *  描述:
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryAppntInfo()
{
	var sql = "select a.Name, a.Birthday, a.Sex, a.Marriage, "
	        + "   a.IDType, a.IDNo, a.NativePlace, "
	        + "   a.GrpName, a.Position, a.OccupationCode, a.OccupationType, "
	        + "   a.Salary, "
	        + "   a.PostalAddress, a.ZipCode, "
	        + "   a.Phone, a.Mobile, a.EMail, a.AppntID, a.PremScope,a.DueFeeMsgFlag," 
	        +"	  a.IDStartDate,a.IDEndDate,a.ExiSpec,a.HomePhone,a.nativecity "
	        + "from BPOLCAppnt a "
	        + "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
	        + "   and ContID = '" + fm.ContID.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.AppntName.value = rs[0][0];
    fm.AppntBirthday.value = rs[0][1];
    fm.AppntSex.value = rs[0][2];
    fm.AppntMarriage.value = rs[0][3];
    fm.AppntIDType.value = rs[0][4];
    fm.AppntIDNo.value = rs[0][5];
    fm.AppntNativePlace.value = rs[0][6];
    fm.AppntGrpName.value = rs[0][7];
    fm.AppntPosition.value = rs[0][8];
    fm.AppntOccupationCode.value = rs[0][9];
    fm.AppntOccupationType.value = rs[0][10];
    fm.AppntSalary.value = rs[0][11];
    fm.AppntPostalAddress.value = rs[0][12];
    fm.AppntZipCode.value = rs[0][13];
    fm.AppntPhone.value = rs[0][14];
    fm.AppntMobile.value = rs[0][15];
    fm.AppntEMail.value = rs[0][16];
    fm.AppntID.value = rs[0][17];
    fm.PremScope.value = rs[0][18];
    fm.DueFeeMsgFlag.value = rs[0][19];
    fm.AppIDStartDate.value = rs[0][20];
    fm.AppIDEndDate.value = rs[0][21];
    fm.ExiSpec.value = rs[0][22];
    var rel=rs[0][12].split("$",-1);
    if(rel.length==5){
        fm.appnt_PostalProvince.value=rel[0];
        fm.appnt_PostalCity.value=rel[1];
        fm.appnt_PostalCounty.value=rel[2];
        fm.appnt_PostalStreet.value=rel[3];
        fm.appnt_PostalCommunity.value=rel[4];
    }
    var rel=rs[0][23].split("-",-1);
    if(rel.length==2){
        fm.AppntHomeCode.value=rel[0];
        fm.AppntHomeNumber.value=rel[1];
    }
  }
  fm.AppntNativeCity.value = rs[0][24];
  setCodeName("AppntNativeCityName", "nativecity", rs[0][24]);
  controlNativeCity("");
  return true;
}

/*********************************************************************
 *  查询被保人信息
 *  描述:
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryInsuredListInfo()
{
	var sql = "select a.ContID, a.InsuredID, a.Name, a.Birthday, CodeName('sex', a.Sex), "
	        + "   a.IDType, codeName('idtype', a.IDType), a.IDNO "
	        + "from BPOLCInsured a "
	        + "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
	        + "   and ContID = '" + fm.ContID.value + "' "
             + " order by int(a.InsuredID) "
            ;
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, InsuredGrid);
		
	if(InsuredGrid.mulLineCount > 0)
	{
	  var row = InsuredGrid.getSelNo();
	  if(row > 0)
		{
		  InsuredGrid.radioBoxSel(row);
		}
		else
		{
		  InsuredGrid.radioBoxSel(1);//第一被保险人默认选中
		}
		getOneInsuredInfo();
	}
  
  return true;
}

/*********************************************************************
 *  查询选中被保人信息
 *  描述:
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getOneInsuredInfo()
{
  getInsuredBasicInfo();
  getBnfInfo();
  if(tWrapType != '6')//银行保险不需要初始化险种信息和告知信息
  {
		getPolListInfo();
  	getImpartInfo();
  }
  showAllCodeName();
  
  return true;
}

//得到被保人基本信息
function getInsuredBasicInfo()
{
  var tInsuredID = InsuredGrid.getRowColDataByName(InsuredGrid.getSelNo() - 1, "InsuredID");

	var sql = "select a.Name, a.Birthday, a.Sex, a.Marriage, "
	        + "   a.IDType, a.IDNo, a.NativePlace, "
	        + "   a.GrpName, a.Position, a.OccupationCode, a.OccupationType, "
	        + "   a.Salary, "
	        + "   a.PostalAddress, a.ZipCode, "
	        + "   a.Phone, a.Mobile, a.EMail, "
	        + "   a.ContID, a.InsuredID, "
	        + "   b.RelationToAppnt, b.RelationToMainInsured,a.IDStartDate,a.IDEndDate,b.ExPayMode,a.HomePhone,a.nativecity "
	        + "from BPOLCInsured a "
            + "left join BPOLCPol b on a.BPOBatchNo = b.BPOBatchNo and a.ContId = b.ContId and a.InsuredID = b.InsuredID "
	        + "where a.BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
	        + "   and a.ContID = '" + fm.ContID.value + "' "
	        + "   and a.InsuredID = '" + tInsuredID + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    //由于同意被保人的每个险种的与投保人被保人关系后相同，此处取第一行
    fm.BPOInsuredName.value = rs[0][0];
    fm.BPOInsuredBirthday.value = rs[0][1];
    fm.BPOInsuredSex.value = rs[0][2];
    fm.BPOInsuredMarriage.value = rs[0][3];
    fm.BPOInsuredIDType.value = rs[0][4];
    fm.BPOInsuredIDNo.value = rs[0][5];
    fm.BPOInsuredNativePlace.value = rs[0][6];
    fm.BPOInsuredGrpName.value = rs[0][7];
    fm.BPOInsuredPosition.value = rs[0][8];
    fm.BPOInsuredOccupationCode.value = rs[0][9];
    fm.BPOInsuredOccupationType.value = rs[0][10];
    fm.BPOInsuredSalary.value = rs[0][11];
    fm.BPOInsuredPostalAddress.value = rs[0][12];
    fm.BPOInsuredZipCode.value = rs[0][13];
    fm.BPOInsuredPhone.value = rs[0][14];
    fm.BPOInsuredMobile.value = rs[0][15];
    fm.BPOInsuredEMail.value = rs[0][16];
    fm.BPOInsuredContID.value = rs[0][17];
    fm.BPOInsuredID.value = rs[0][18];
    fm.RelationToAppnt.value = rs[0][19];
    fm.RelationToMainInsured.value = rs[0][20];
    fm.InsuIDStartDate.value = rs[0][21];
    fm.InsuIDEndDate.value = rs[0][22];
    fm.ExPayMode.value = rs[0][23];
    fm.BPOInsuredHomePhone.value = rs[0][24];
    fm.BPOInsuredNativeCity.value = rs[0][25];
    setCodeName("BPOInsuredNativeCityName", "nativecity", rs[0][25]);
  }
  controlNativeCity1("");
}

//查询险种信息
function getPolListInfo()
{
  var sql = "select a.MainPolID, a.ContID, a.InsuredID, a.PolID, "
          + "   (select RiskName from LMRisk where RiskCode = a.RiskCode), a.RiskCode, "
          + "   (case when (select min(CalMode) from LMDuty where DutyCode in(select DutyCode from LMRiskDuty where RiskCode = a.RiskCode)) = 'O' then a.Mult else a.Amnt end), "
          + "   a.InsuYear, a.InsuYearFlag, a.PayEndYear, "
          + "   a.PayEndYearFlag, a.PayIntv, '', a.Prem, a.SupplementaryPrem "
	        + "from BPOLCPol a "
	        + "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
	        + "   and ContID = '" + fm.ContID.value + "' "
	        + "   and InsuredID = '" + fm.BPOInsuredID.value + "' ";
	turnPage3.pageDivName = "divPage3";
	turnPage3.queryModal(sql, PolGrid);
  
  return true;
}

//查询受益人信息
function getBnfInfo()
{
  var sql = "select a.ContID, a.InsuredID, "
          + "   case when a.BnfType is null then '1' else a.BnfType end, "
          + "   '', Name, a.sex, '', a.BirthDay, a.IDType, codeName('idtype', a.IDType), "
          + "   a.IDNo, a.RelationToInsured, CodeName('relation', a.RelationToInsured), "
          + "   a.BnfLot, a.BnfGrade, CodeName('occupationtype', a.BnfGrade) "
	        + "from BPOLCBnf a "
	        + "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
	        + "   and ContID = '" + fm.ContID.value + "' "
	        + "   and InsuredID = '" + fm.BPOInsuredID.value + "' ";
	turnPage4.pageDivName = "divPage4";
	turnPage4.queryModal(sql, BnfGrid);
  
  return true;
}

//查询缴费方式
function queryFeeInfo()
{
  var sql = "select max(PayMode) from BPOLCPol "
          + "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
          + "   and ContID = '" + fm.ContID.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.PayMode.value = rs[0][0];
  }
  
  sql = "select a.BankCode, a.AccName, a.BankAccNo, a.Remark "
      + "from BPOLCAppnt a "
      + "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
      + "   and ContID = '" + fm.ContID.value + "' ";
  rs = easyExecSql(sql);
  if(rs)
  {
    fm.BPOBankCode.value = rs[0][0];
    fm.BPOAccName.value = rs[0][1];
    fm.BPOBankAccNo.value = rs[0][2];
    fm.Remark.value = rs[0][3];
  }
}

/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartInfo() 
{
  clearImpart();
  getImpartbox();   //保障状况告知※健康告知
  getImpartDetailInfo();
}

/*********************************************************************
 *  查询告知明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartDetailInfo() 
{
  initImpartDetailGrid();

  var sql = "select ImpartVer, ImpartCode, "
          + "   (select ImpartContent from LDImpart where ImpartVer = a.ImpartVer and ImpartCode = a.ImpartCode), "
          + "   DiseaseContent "
          + "from BPOLCImpart a "
          + "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
          + "   and ContID = '" + fm.BPOInsuredContID.value + "' "
          + "   and CustomerNo='" + fm.BPOInsuredID.value + "' "
          + "   and  CustomerNoType = 'I' "
          + "   and DiseaseContent is not null "
          + "   and DiseaseContent != '' ";
  turnPage6.pageLineNum = 50;
  turnPage6.pageDivName = "divPage6";
  turnPage6.queryModal(sql, ImpartDetailGrid);
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmitBPO(FlagStr, content) 
{
	try
	{
	  showInfo.close();
	}catch(ex)
	{
	  alert(ex.message);
	}
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
	  queryInsuredListInfo();
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterImportSubmitBPO(FlagStr, content) 
{
	try
	{
		fm.all('import').disabled = false;//生成保单按钮可见    2008-1-4
	  showInfo.close();
	}catch(ex)
	{
	  alert(ex.message);
	}
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		top.opener.focus();
		top.opener.queryContInfo();
		top.close();
	}
}


//----------------------------------事件响应区域-------------------------------

function checkidtype()
{
	if(fm.AppntIDType.value=="")
	{
		alert("请选择证件的类型");
		fm.AppntIDNo.value="";
  }
} 

/*********************************************************************
 *  根据身份证号取得出生日期和性别
 *  参数  ：  身份证号
 *  返回值：  无
 *********************************************************************
 */

function getBirthdaySexByIDNo(iIdNo)
{
	if(fm.all('AppntIDType').value=="0")
	{
		fm.all('AppntBirthday').value=getBirthdatByIdNo(iIdNo);
		fm.all('AppntSex').value=getSexByIDNo(iIdNo);
	}	
}  

function isPhone(s)
{
	var patrn= /^[0-9]+(-|)[0-9]+$/g;
	return patrn.test(s);

}

//保存保单管理信息
function saveManageInfo()
{
	if(fm.AppntPhone.value != null && fm.AppntPhone.value != '' && !isPhone(fm.AppntPhone.value))
	{
		alert("联系电话格式不正确！联系电话应为数字或数字和’-‘的组合！");
		return false;
	}
	if(fm.AppntMobile.value != null && fm.AppntMobile.value != '' && !isPhone(fm.AppntMobile.value))
	{
		alert("移动电话格式应为数字或数字和 '-' 的组合！");
		return false;
	}
	if(isNaN(fm.PremScope.value))
	{
		alert("整单保费必须是数字！");
		return false;
	}
	
	if(!checkNative()){
	    return false;
	}
	
	// by gzh 对邮编扩充后对邮编进行校验
	var tAppntZipCode = fm.AppntZipCode.value;
	if(tAppntZipCode !=null && tAppntZipCode !=''){
		if(isNaN(tAppntZipCode))
		{
			alert("邮政编码必须是数字！");
			return false;
		}
		if(tAppntZipCode.length>6){
			alert("邮政编码最长为6位！");
			return false;
		}
	}
	//end  by gzh
	
	if(fm.all('AppntIDType').value=="0") 
	  {
	    var strChkIdNo = chkIdNo(trim(fm.all('AppntIDNo').value),trim(fm.all('AppntBirthday').value),trim(fm.all('AppntSex').value));
	    if (strChkIdNo != "") 
	    {
	      alert(strChkIdNo);
	      return false;
	    }
	  }
	if(!MixComCheck())
	{	  
	  return false;
	}
	fm.AppntPostalAddress.value=trim(fm.appnt_PostalProvince.value)
	                           +"$"+ trim(fm.appnt_PostalCity.value)
	                           +"$"+ trim(fm.appnt_PostalCounty.value)
	                           +"$"+ trim(fm.appnt_PostalStreet.value)
	                           +"$"+ trim(fm.appnt_PostalCommunity.value);
	  if(fm.all('AppntIDType').value=="5") 
	  {
	    var strChkIdNo = chkIdNo(trim(fm.all('AppntIDNo').value),trim(fm.all('AppntBirthday').value),trim(fm.all('AppntSex').value));
	    if (strChkIdNo != "") 
	    {
	      alert(strChkIdNo);
	      return false;
	    }
	  }
	fm.AppntHomePhone.value=trim(fm.AppntHomeCode.value)
	                        +"-"+ trim(fm.AppntHomeNumber.value);
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
			
  
  fm.action = "BPOContSave.jsp";
  fm.submit();
}

//保存被保人信息
function saveInsuredInfo()
{
  if(!checkInsuredBasicInfo())
  {
    return false;
  }
  if (InputMuline1()==false)
  {
     return false;
  }
  
  if(!checkNativei()){
	  return false;
  }
  
	if(fm.BPOInsuredPhone.value != null && fm.BPOInsuredPhone.value != '' && !isPhone(fm.BPOInsuredPhone.value))
	{
		alert("联系电话格式不正确！联系电话应为数字或数字和 '-' 的组合！");
		return false;
	}
	if(fm.BPOInsuredMobile.value != null && fm.BPOInsuredMobile.value != '' && !isPhone(fm.BPOInsuredMobile.value))
	{
		alert("移动电话格式不正确！移动电话应为数字或数字和 '-' 的组合！");
		return false;
	}
	//by gzh  校验被保人告知19条，当选“是”时，保额不能为空。20110315
  //if(!ImpartCheck1191()){
  		//return false;
  //}
  ImpartGrid.delBlankLine();
  ImpartDetailGrid.delBlankLine();
  
  // by gzh 对邮编扩充后对邮编进行校验
	var tBPOInsuredZipCode = fm.BPOInsuredZipCode.value;
	if(tBPOInsuredZipCode !=null && tBPOInsuredZipCode !=''){
		if(isNaN(tBPOInsuredZipCode))
		{
			alert("邮政编码必须是数字！");
			return false;
		}
		if(tBPOInsuredZipCode.length>6){
			alert("邮政编码最长为6位！");
			return false;
		}
	}
	//end  by gzh
  
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
  
  fm.action = "BPOInsuredSave.jsp";
  fm.submit();
}

//导入保单
function importCont()
{
  fm.all('import').disabled=true;//生成保单按钮不可见    2008-1-4
  var sql = "select count(1) from (select 1 from lccont where prtno = '" + fm.PrtNoOld.value + "' "
  		+ "union all select 1 from lbcont where prtno = '" + fm.PrtNoOld.value + "') as temp";
  var rs = easyExecSql(sql);
  if(rs)
  {
    if(rs[0][0] > 0)
    {
    	alert("该保单已生成！");
    	return ;
    }
  }
  
  //添加对保险期间单位为空的校验  2008-4-28
  sql = "select InsuYearFlag from BPOLCPol where PrtNo = '" + fm.PrtNoOld.value 
      + "' and InsuYear != '0' and (InsuYearFlag = '' or InsuYearFlag is null)";
  rs = easyExecSql(sql);
  if(rs)
  {
    alert("存在保险期间单位为空的险种，请检查险种信息！");
    fm.all('import').disabled = false;
    return;
  }
  
  //by gzh  校验被保人告知19条，当选“是”时，保额不能为空。20110315
  //if(!ImpartCheck1191()){
  		//return false;
  //}
  
  //by gzh 校验邮编位数
  var tSql = "select zipcode from bpolcappnt "
  			+ "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
	        + " and ContID = '" + fm.ContID.value + "' ";
  rs = easyExecSql(tSql);
  if(!rs)
  {
    alert("查询投保人信息失败！");
    fm.all('import').disabled = false;
    return;
  }else{
  
  	if(rs[0][0] != null && rs[0][0] != '')
    {
    	if(isNaN(rs[0][0]))
		{
			alert("投保人邮政编码必须是数字！");
			fm.all('import').disabled = false;
			return false;
		}
		if(rs[0][0].length>6)
		{
			alert("投保人邮编最长6位！");
    		fm.all('import').disabled = false;
    		return false;
		}
    }
  }
  
  var tSql = "select insuredid,zipcode from bpolcinsured "
  			+ "where BPOBatchNo = '" + fm.BPOBatchNo.value + "' "
	        + " and ContID = '" + fm.ContID.value + "' ";
  rs = easyExecSql(tSql);
  if(!rs)
  {
    alert("查询被保人信息失败！");
    fm.all('import').disabled = false;
    return;
  }else{
    var error = "";
  	for(var i = 0; i < rs.length; i++){
  		if(rs[i][1] != null && rs[i][1] != '')
    	{
	    	if(isNaN(rs[i][1])){
	    		error +="被保人[contid:"+rs[i][0]+"]的邮编必须为数字！\n";
	    	}
	    	if(rs[i][1].length>6){
	    		error +="被保人[contid:"+rs[i][0]+"]的邮编长度不应超过6位！\n";
	    	}
    	}
  	}
  	if(error!=null && error !=''){
  		alert(error);
  		fm.all('import').disabled = false;
    	return false;
  	}
  }        
  
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
  
  fm.action = "BPOContImportSave.jsp";
  fm.submit();
}

//调用查看外包错误信息界面
function viewBPOIssue()
{
  window.open("./BPOIssueInput.jsp?prtNo=" + fm.PrtNoOld.value,"BPOIssue");
}

//------------------------------------校验区------------------------------------

//校验被保人基本信息是否合法
function checkInsuredBasicInfo()
{
  var row = InsuredGrid.getSelNo() - 1;
  if(row < 0)
  {
    alert("请选择被保人");
    return false;
  }
  
  //if(fm.BPOInsuredMarriage.value=="") 
  //{
  //  fm.BPOInsuredMarriage.focus();
  //  alert("请填写婚姻状况！");
  //  return false;
  //}
  if(fm.RelationToMainInsured.value=="") 
  {
    fm.RelationToMainInsured.focus();
    alert("请填写与第一被保险人关系！");
    return false;
  }
  if(fm.RelationToAppnt.value=="") 
  {
    fm.RelationToAppnt.focus();
    alert("请填写与投保人关系！！");
    return false;
  }
  
  //校验身份证的函数chkIdNo的生日需要传入yyyy-mm-dd类型的日期，所以需要将yyyy-mm-dd的格式进行转换
  var birthday = trim(fm.all('BPOInsuredBirthday').value);
  if(!(isDate(birthday) || isDateN(birthday)) || trim(birthday).length < 8)
  {
    alert("出生日期格式必须是yyyy-mm-dd或yyyymmdd。");
    return false;
  }
  if(birthday.length == 8)
  {
    fm.BPOInsuredBirthday.value = birthday.substring(0, 4) + "-" 
      + birthday.substring(4, 6) + "-" + birthday.substring(6, 8);
  }
  
  if(fm.all('BPOInsuredIDType').value=="0") 
  {
    var strChkIdNo = chkIdNo(trim(fm.all('BPOInsuredIDNo').value),trim(fm.all('BPOInsuredBirthday').value),trim(fm.all('BPOInsuredSex').value));
    if (strChkIdNo != "") 
    {
      alert(strChkIdNo);
      return false;
    }
  }
  
  if(fm.all('BPOInsuredIDType').value=="5") 
  {
    var strChkIdNo = chkIdNo(trim(fm.all('BPOInsuredIDNo').value),trim(fm.all('BPOInsuredBirthday').value),trim(fm.all('BPOInsuredSex').value));
    if (strChkIdNo != "") 
    {
      alert(strChkIdNo);
      return false;
    }
  }
  
  ImpartGrid.delBlankLine();
  ImpartDetailGrid.delBlankLine();
  
  return true;
}

//银行保险的初始化
function initDiv()
{
  if("TB05" == fm.SubType.value)
  {
		fm.all("divNation").style.display = '';
		fm.all("divNationGrid").style.display = '';
		fm.all("divWrap").style.display = '';
		fm.all("divWrapGrid").style.display = '';
		fm.all("divWrap").style.display = '';
		fm.all("divRiskWrapID").style.display = '';
		fm.all("DivImpart3").style.display = 'none';
		fm.all("divLCImpart3").style.display = 'none';
		fm.all("DivLCPol").style.display = 'none';
		fm.all("divPolGrid").style.display = 'none';
        
        hiddenInputOfSingleCont();
	  
		sql = "select NationNo, ChineseName from BPOLCNation where BPOBatchNo='" 
				+ fm.BPOBatchNo.value + "' and InsuredId = '" + fm.BPOInsuredID.value + "'";
		turnPage7.queryModal(sql, NationGrid);
		sql = "select RiskWrapCode, WrapName from LDWrap where WrapType='" + tWrapType + "'";
		turnPage8.queryModal(sql, WrapGrid);
		
		var RiskWrapCodes = queryWrap();
		for(var i = 0; i < WrapGrid.mulLineCount; i++)
		{
		  for(var j = 0; j < RiskWrapCodes[0].length; j++)
		  {
		    if(RiskWrapCodes[0][j] == WrapGrid.getRowColDataByName(i, "RiskWrapCode"))
			  {
			    WrapGrid.checkBoxSel(i + 1);
			  }
			}
		}
		queryWrapDetail();
  }
}

function queryWrap()
{
	var sql = "select distinct RiskWrapCode from BPOLCRiskDutyWrap where BPOBatchNo='" 
				+ fm.BPOBatchNo.value + "' and PrtNo = '" + fm.PrtNo.value + "'";
	var rs = easyExecSql(sql);
	
	return rs;
}

//查询套餐明细
function queryWrapDetail()
{
  var riskWrapCodes = "";  //已选择的套餐信息
  
  for(var i = 0; i < WrapGrid.mulLineCount; i++)
  {
    if(WrapGrid.getChkNo(i))
    {
      riskWrapCodes = riskWrapCodes 
        + "'" + WrapGrid.getRowColDataByName(i, "RiskWrapCode") + "',";
    }
  }
  
  if(riskWrapCodes == "")
  {
    alert("请选择套餐");
    return false;
  }
  riskWrapCodes = riskWrapCodes.substring(0, riskWrapCodes.length - 1);
  
  initRiskWrapGrid(riskWrapCodes);
  
  var sql = strSqlMain;
  
  sql = sql 
    + "  and a.RiskWrapCode in (" + riskWrapCodes + ") "
    + "order by a.RiskWrapCode, RiskCode, DutyCode ";
  
	turnPage3.pageDivName = "divPage3";
	turnPage3.pageLineNum = 100;
	turnPage3.queryModal(sql, RiskWrapGrid);
	
	//套餐责任信息
	var sql = "select distinct RiskWrapCode, RiskCode, DutyCode "
            + "from BPOLCRiskDutyWrap "
            + "where PrtNo = '" + fm.PrtNo.value + "' "
            + "   and InsuredId = '" + fm.BPOInsuredID.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    //选中被保人已录入的套餐责任信息
    for(var i = 0; i < rs.length; i++)
    {
      for(var j = 0; j < RiskWrapGrid.mulLineCount; j++)
      {
        var tGridRiskWrapCode = RiskWrapGrid.getRowColDataByName(j, "RiskWrapCode");
        var tGridRiskCode = RiskWrapGrid.getRowColDataByName(j, "RiskCode");
        var tGridDutyCode = RiskWrapGrid.getRowColDataByName(j, "DutyCode");
        if(rs[i][0] == tGridRiskWrapCode
            && (rs[i][1] == tGridRiskCode || "000000" == tGridRiskCode)
            && (rs[i][2] == tGridDutyCode || "000000" == tGridDutyCode))  //若不需要显示到险种和责任，那么只需要比较套餐即可
        {
          RiskWrapGrid.checkBoxSel(j+1);
        }
      }
    }
  }
  
  //将"null"显示为空字符创""
  for(var i = 0 ; i<RiskWrapGrid.mulLineCount ; i++)
  {
		for(var m = 1 ; m<RiskWrapGrid.colCount ; m++)
		{
			if(RiskWrapGrid.getRowColData(i,m)=="null" || RiskWrapGrid.getRowColData(i,m)=="0")
			{
				RiskWrapGrid.setRowColData(i,m,"");
			}
		}
	}
}

/**
 * 隐藏简易件（银保）业务不需要的录入框。
 */
function hiddenInputOfSingleCont()
{
    // 婚姻
    fm.all("AppntMarriageTitle").style.display = "none";
    fm.all("AppntMarriageInput").style.display = "none";
    fm.all("BPOInsuredMarriageTitle").style.display = "none";
    fm.all("BPOInsuredMarriageInput").style.display = "none";
    // 国籍
    fm.all("AppntNativePlaceTitle").style.display = "";
    fm.all("AppntNativePlaceInput").style.display = "";
    fm.all("BPOInsuredNativePlaceTitle").style.display = "";
    fm.all("BPOInsuredNativePlaceInput").style.display = "";
    // 年收入
    fm.all("AppntSalaryTitle").style.display = "none";
    fm.all("AppntSalaryInput").style.display = "none";
    fm.all("BPOInsuredSalaryTitle").style.display = "none";
    fm.all("BPOInsuredSalaryInput").style.display = "none";
}
//校验当告知19选择“是”时，保额必须填写。
function ImpartCheck1191() {
  var age = getAge(fm.all('BPOInsuredBirthday').value);
  if(age<18 && (fm.Detail119[0].checked==false) && (fm.Detail119[1].checked==false)){
  	alert("被保人年龄为"+age+"岁,请选择第19项告知！");
  	return false;
  }
  if(fm.Detail119[0].checked == true) {
    if(fm.Detail119[2].value == "" ||fm.Detail119[2].value == null){
    	alert("请填写被保人告知19条保额信息！");
    	fm.Detail119[2].focus();
    	return false;
    }
    if(isNaN(fm.Detail119[2].value)){
    	alert("被保人告知19条保额必须为数字！");
    	fm.Detail119[2].focus();
    	return false;
    }
    if(fm.Detail119[2].value<0){
    	alert("被保人告知19条保额必须大于0！");
    	fm.Detail119[2].focus();
    	return false;
    }
  }
  return true;
}

function controlNativeCity1(displayFlag)
{
  if(fm.BPOInsuredNativePlace.value=="OS"){
    fm.all("NativePlaces").style.display = displayFlag;
	fm.all("NativeCitys").style.display = displayFlag;
	fm.all("NativeCityTitles").innerHTML="国家";
	
  }else if(fm.BPOInsuredNativePlace.value=="HK"){
    fm.all("NativePlaces").style.display = displayFlag;
	fm.all("NativeCitys").style.display = displayFlag;
	fm.all("NativeCityTitles").innerHTML="地区";
  }else{
    fm.all("NativePlaces").style.display = "none";
	fm.all("NativeCitys").style.display = "none";
  }
}

function checkNativei(){
   if(fm.BPOInsuredNativePlace.value=="OS"||fm.BPOInsuredNativePlace.value=="HK"){
      if(fm.BPOInsuredNativeCity.value==""||fm.BPOInsuredNativeCity.value==null){
         alert("被保人所属国家/地区未录入！");
         return false;
      }
   }
   return true;
}

//为代码赋汉字
function setCodeName(fieldName, codeType, code)
{
  var sql = "select CodeName('" + codeType + "', '" + code + "') from dual";
  var rs = easyExecSql(sql)
  if(rs)
  {
    fm.all(fieldName).value = rs[0][0];
  }
}