<%@page import="com.sinosoft.utility.*"%>
<script language="JavaScript">
  
// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        
    }
    catch(ex)
    {
        alert("RiskdutyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initRiskdutyGrid();
    }
    catch(re)
    {
        alert("RiskdutyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var RiskdutyGrid;

// 保单信息列表的初始化
function initRiskdutyGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="险种编码";
        iArray[1][1]="50px";
        iArray[1][2]=50;
        iArray[1][3]=0;
        iArray[1][21]="riskCode";

        iArray[2]=new Array();
        iArray[2][0]="险种名称";
        iArray[2][1]="160px";
        iArray[2][2]=80;
        iArray[2][3]=0;
        iArray[2][21]="riskName";

        iArray[3]=new Array();
        iArray[3][0]="责任编码";
        iArray[3][1]="60px";
        iArray[3][2]=80;
        iArray[3][3]=0;
        iArray[3][21]="dutycode";

        iArray[4]=new Array();
        iArray[4][0]="责任名称";
        iArray[4][1]="160px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        iArray[4][21]="dutyname";

        

        RiskdutyGrid = new MulLineEnter( "fm" , "RiskdutyGrid" );
        //这些属性必须在loadMulLine前
        RiskdutyGrid.mulLineCount = 0;
        RiskdutyGrid.displayTitle = 1;
        RiskdutyGrid.hiddenPlus = 1;
        RiskdutyGrid.hiddenSubtraction = 1;
        RiskdutyGrid.canSel = 0;
        RiskdutyGrid.canChk = 0;
        RiskdutyGrid.loadMulLine(iArray);
        RiskdutyGrid.selBoxEventFuncName = "RiskdutyDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>