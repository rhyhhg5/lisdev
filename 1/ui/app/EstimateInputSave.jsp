<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String CurrentDate = PubFun.getCurrentDate();
	String CurrentTime = PubFun.getCurrentTime();
	
	//输入参数
	EstimateUI mEstimateUI = new EstimateUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	
	String mGrpContNo= request.getParameter("GrpContNo");

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");

	if(mGrpContNo == null || "".equals(mGrpContNo))
	{
		FlagStr = "Fail";
		Content = "获取保单号码失败！";
	}
	System.out.println("保单号:" + mGrpContNo);
	
	if(!"Fail".equals(FlagStr)){
		LCEstimateSchema tLCEstimateSchema = new LCEstimateSchema();
		tLCEstimateSchema.setGrpContNo(request.getParameter("GrpContNo"));
		tLCEstimateSchema.setProjectName(request.getParameter("ProjectName"));
		tLCEstimateSchema.setProjectNo(request.getParameter("ProjectNo"));
		tLCEstimateSchema.setEstimateRate(request.getParameter("EstimateRate"));
		tLCEstimateSchema.setEstimateBalanceRate(request.getParameter("EstimateBalanceRate"));
		tLCEstimateSchema.setContEstimatePrem(request.getParameter("ContEstimatePrem"));
		tLCEstimateSchema.setBackEstimatePrem(request.getParameter("BackEstimatePrem"));
		tLCEstimateSchema.setReason(request.getParameter("Reason"));
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.addElement(tLCEstimateSchema);
		try {
			mEstimateUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = mEstimateUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>