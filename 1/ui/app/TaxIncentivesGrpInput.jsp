<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<script>
	var mType = "<%=request.getParameter("type")%>";
	var pGrpNo = "<%=request.getParameter("GrpNo")%>";
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<script src="../common/javascript/CommonTools.js"></script>
		<SCRIPT src="TaxIncentivesGrpInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="TaxIncentivesGrpInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./TaxIncentivesGrpSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divGroupPol1);">
					</td>
					<td class=titleImg>
						单位基本信息：
					</td>
				</tr>
			</table>

			<Div id="divGroupPol1" style="display: ''">
				<table class=common>
					<TR class=common>
						<TD class=title8>
							团体编号
						</TD>
						<TD class=input8>
							<Input name=GrpNo class="common" readonly>
						</TD>
						<TD class=title8>
							税务登记证号
						</TD>
						<TD class=input8>
							<Input name=TaxRegistration class="common">
						</TD>
						<TD class=title8>
							社会信用代码
						</TD>
						<TD class=input8>
							<Input class=common8 name=OrgancomCode>
						</TD>
					</TR>
					<TR class=common>
						<TD class=title8>
							单位名称
						</TD>
						<TD class=input8 colspan=4>
							<Input name=GrpName class= common3 elementtype=nacessary verify="单位名称|notnull">
						</TD>
						<td class=input8></td><!-- 此行无实际用途，仅为页面table中每一行的背景色能相同长度，看起来整齐用 -->
					</TR>
				</table>
			</Div>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divGroupPol2);">
					</td>
					<td class=titleImg>
						团体详细信息
					</td>
				</tr>
			</table>

			<Div id="divGroupPol2" style="display: ''">
				<table class=common>
					<TR>
						<TD class=title8>
							单位地址
						</TD>
						<TD class=input8 colspan=4>
							<Input class= common3 name=GrpAddress>
						</TD>
						<td class=input8></td><!-- 此行无实际用途，仅为页面table中每一行的背景色能相同长度，看起来整齐用 -->
					</TR>
					<TR>
						<TD class=title8>
							单位性质
						</TD>
						<TD class=input8>
							<Input class=codeNo name=GrpNature
								ondblclick="showCodeList('GrpNature',[this,GrpNatureName],[0,1]);"
								onkeyup="showCodeListKey('GrpNature',[this,GrpNatureName],[0,1]);"><Input class=common8 name=GrpNatureName readonly>
						</TD>
						<TD class=title8>
							在职员工总数
						</TD>
						<TD class=input8>
							<Input class=common8 name=Peoples>
						</TD>
						<TD class=title8>
							邮政编码
						</TD>
						<TD class=input8>
							<Input class=common8 name=GrpZipCode
								verify="邮政编码|notnull&zipcode">
						</TD>
					</TR>
					<TR>
						<TD class=title8>
							行业类别
						</TD>
						<TD class=input8>
							<Input class=codeNo name=BusinessType
								ondblclick="showCodeList('BusinessType',[this,BusinessTypeName,BusinessBigType],[0,1,2],null,null,null,1,300);"
								onkeyup="showCodeListKey('BusinessType',[this,BusinessTypeName,BusinessBigType],[0,1,2],null,null,null,1,300);"><Input class="common8" name=BusinessTypeName readonly>
						
					 	    <Input class="common8" type="hidden" name=BusinessBigType  >
						</TD>
						<TD class=title8>
							单位法人代表
						</TD>
						<TD class=input8>
							<Input class=common8 name=LegalPersonName>
						</TD>
						<TD class=title8>
							单位电话号码
						</TD>
						<TD class=input8>
							<Input class=common8 name=Phone>
						</TD>

					</TR>
					<TR class=common>
						<TD class=title8>
							业务经办人姓名
						</TD>
						<TD class=input8>
							<Input name=HandlerName class="common" verify="经办人签名|len<=20">
						</TD>
						<TD class=title8>
							所在部门
						</TD>
						<TD class=input8>
							<Input class=common8 name=Department>
						</TD>
						<TD class=title8>
							联系电话
						</TD>
						<TD class=input8>
							<Input class=common8 name=HandlerPhone>
						</TD>

					</TR>
					<TR class=common>
						<TD class=title8>
							开户银行
						</TD>
						<TD class=input8>
							<Input class=codeNo name=BankCode verify="开户银行|code:bank&len<=24"
								ondblclick="return showCodeList('Bank',[this,BankCodeName],[0,1],null,null,null,1);"
								onkeyup="return showCodeListKey('Bank',[this,BankCodeName],[0,1],null,null,null,1);"><input class=codename name=BankCodeName readonly=true>
						</TD>
						<TD class=title8>
							银行账户名
						</TD>
						<TD class=input8>
							<Input name=BankAccName class="common">
						</TD>
						<TD class=title8>
							银行帐号
						</TD>
						<TD class=input8>
							<Input name=BankAccNo class="common">
						</TD>
					</TR>
				</table>
				<br>
				<INPUT VALUE="保  存" class = cssButton TYPE=button onclick="AddInput();"  id="AddSave" style="display: none"> 
                <INPUT VALUE="修  改" class = cssButton TYPE=button onclick="UpdateInput();" id="UpdateSave" style="display: none">  
				<INPUT VALUE="返  回" class = cssButton TYPE=button onclick="goBack();" id="GoBack">
				<input type=hidden name=transact>
		</form>

		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>

	</body>
</html>
