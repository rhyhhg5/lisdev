<%
//程序名称：ParticularStatisticSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
ReportUI
</title>
<head>
</head>
<body>
<%

//String FileName = "GroupProduct_Income";
//String StartDate = "2005-01-01";
//String EndDate = "2005-01-11";
//String ManageCom = "86";
String flag = "0";
String FlagStr = "";
String Content = "";
String OperatorManagecom = "";
GlobalInput tG = new GlobalInput(); 
tG=(GlobalInput)session.getValue("GI");



//设置模板名称

String FileName ="UWResultStatistic";
String ManageCom = request.getParameter("ManageCom");
System.out.println(ManageCom);
String StartDate = request.getParameter("StartDate");
System.out.println(StartDate);
String EndDate = request.getParameter("EndDate");
System.out.println(EndDate);
String RiskCode = request.getParameter("RiskCode");
String UWUserCode2 = request.getParameter("UWUserCode");
System.out.println(RiskCode);
OperatorManagecom=tG.ManageCom;
System.out.println(OperatorManagecom);
String managecom1 ="";
String managecom2 ="";
String RiskCode1 ="";
String RiskCode2 ="";
String UWdate ="";
String UWdate1 ="";
String UWUserCode ="";
String UWUserCode1 ="";
String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");

if(sd.compareTo(ed) > 0)
{
flag = "1";
FlagStr = "Fail";
Content = "操作失败，原因是:统计止期比统计统计起期早";
}
if(ManageCom.length()==8)
	 {
			managecom1="length('?ManageCom?')";
			managecom2="length('?ManageCom?')";
			System.out.println(managecom1);
			System.out.println(managecom2);
		}
	else {
			managecom1="length('?ManageCom?')*2";
			managecom2="length('?ManageCom?')";
			System.out.println(managecom1);
			System.out.println(managecom2);
		}
	if(!RiskCode.equals(""))
	{
	RiskCode1=" and riskcode='?RiskCode?'";
	RiskCode2=" and b.riskcode='?RiskCode?'";
	System.out.println("RiskCode1");
	}else{
	RiskCode1=" ";
	RiskCode2=" ";	
	}
	if(!UWUserCode2.equals("")){
		UWUserCode=" and uwcode='?UWUserCode2?' ";
		UWUserCode1=" and b.uwcode='?UWUserCode2?' ";
	   System.out.println("UWUserCode "+UWUserCode);	
		System.out.println("UWUserCode1 "+UWUserCode1);	
	}else{
	 UWUserCode=" ";   
	UWUserCode1=" ";	
	}
	if(!StartDate.equals("")&!EndDate.equals(""))
	{
		UWdate=" and Uwdate between '?StartDate?' and '?EndDate?' ";
		UWdate1=" and b.UWdate between '?StartDate?' and '?EndDate?'";
		System.out.println(UWdate);	
	}
JRptList t_Rpt = new JRptList();
String tOutFileName = "";
if(flag.equals("0"))
{
t_Rpt.m_NeedProcess = false;
t_Rpt.m_Need_Preview = false;
t_Rpt.mNeedExcel = true;

StartDate = AgentPubFun.formatDate(StartDate, "yyyy-MM-dd");
EndDate = AgentPubFun.formatDate(EndDate, "yyyy-MM-dd");
String CurrentDate = PubFun.getCurrentDate();
CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");

t_Rpt.AddVar("StartDate", StartDate);
t_Rpt.AddVar("EndDate", EndDate);
t_Rpt.AddVar("ManageCom",ManageCom);
t_Rpt.AddVar("RiskCode",RiskCode);
t_Rpt.AddVar("managecom1",managecom1);
t_Rpt.AddVar("managecom2",managecom2);
t_Rpt.AddVar("ManageComName", ReportPubFun.getMngName(OperatorManagecom));
t_Rpt.AddVar("UWdate", UWdate);
t_Rpt.AddVar("UWdate1", UWdate1);
t_Rpt.AddVar("RiskCode1", RiskCode1);
t_Rpt.AddVar("RiskCode2", RiskCode2);
t_Rpt.AddVar("UWUserCode", UWUserCode);
t_Rpt.AddVar("UWUserCode1", UWUserCode1);
t_Rpt.AddVar("UWUserCode2", UWUserCode2);


String YYMMDD = "";
YYMMDD = StartDate.substring(0, StartDate.indexOf("-")) + "年"
       + StartDate.substring(StartDate.indexOf("-") + 1,StartDate.lastIndexOf("-")) + "月"
       + StartDate.substring(StartDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("StartDateN", YYMMDD);
YYMMDD = "";
YYMMDD = EndDate.substring(0, EndDate.indexOf("-")) + "年"
       + EndDate.substring(EndDate.indexOf("-") + 1,EndDate.lastIndexOf("-")) + "月"
       + EndDate.substring(EndDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("EndDateN", YYMMDD);
YYMMDD = "";
YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
       + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
       + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("MakeDate", YYMMDD);

t_Rpt.Prt_RptList(pageContext,FileName);
tOutFileName = t_Rpt.mOutWebReportURL;
String strVFFileName = 
FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
String strRealPath = 
application.getRealPath("/web/Generated").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName; 
System.out.println("strVFPathName : "+ strVFPathName); 
System.out.println("=======Finshed in JSP==========="); 
System.out.println(tOutFileName);

response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+
"&RealPath="+strVFPathName);
}
%>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >