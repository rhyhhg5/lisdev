<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>   
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>     
<%  
	String tPNo = "";
	try
	{
		tPNo = request.getParameter("PNo");
	}
	catch( Exception e )
	{ 
		tPNo = "";
	}
 
//得                到界面的调用位置,默认为1,表示个人保单直接录入.
// 1 --                 个人投保单直接录入
// 2 -- 集体下个人投保单录入
// 3 -- 个人投保单明细查询
// 4 -- 集体下个人投保单明细查询
// 5 -- 复核
// 6 -- 查询
// 7 -- 保全新保加人
// 8 -- 保全新增附加险
// 9 -- 无名单补名单
// 10-- 浮动费率
// 99-- 随动定制

	String tLoadFlag = "";
	try
	{
		tLoadFlag = request.getParameter( "LoadFlag" );
		//默认情况下为个人保单直接录入
		if( tLoadFlag == null || tLoadFlag.equals( "" ))
			tLoadFlag = "1";
	}
	catch( Exception e1 )
	{
		tLoadFlag = "1";
	}
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
System.out.println("LoadFlag:" + tLoadFlag);
%>
<head>
<script>
	var LoadFlag = "<%=tLoadFlag%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置.
	var ApproveFlag ="";
	if(LoadFlag=="5")
	{
		LoadFlag=1;
		ApproveFlag = "5";
	}
	if(LoadFlag=="25")
	{
		LoadFlag=1;
		ApproveFlag = "25";
	}
	var BQFlag = "<%=request.getParameter("BQFlag")%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
	var prtNo = "<%=request.getParameter("prtNo")%>"
	var InsuredNo = "<%=request.getParameter("InsuredNo")%>"
	var ManageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
	if (ScanFlag == "null") ScanFlag = "1";	
	var checktype = "<%=request.getParameter("checktype")%>";	
	var scantype = "<%=request.getParameter("scantype")%>";	
	var mainnoh = "";
	var ContType = "<%=request.getParameter("ContType")%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
	if (ContType == "null")
	    ContType=1;
	var EdorType = "<%=request.getParameter("EdorType")%>";	
	var param="";	   
	var MissionID = "<%=request.getParameter("MissionID")%>";
	var SubMissionID = "<%=request.getParameter("SubMissionID")%>";	 
	var SaleChnlDetail = "<%=request.getParameter("SaleChnlDetail")%>";	 
	var oldContNo	= "<%=request.getParameter("oldContNo")%>";
</script>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	
	<SCRIPT src="ProposalInput.js"></SCRIPT>  
	<SCRIPT src="SpecDealByRisk.js"></SCRIPT>
	
	<%@include file="ProposalInit.jsp"%>
	<SCRIPT src="ProposalAutoMove.js"></SCRIPT> 
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
        <%if(request.getParameter("scantype")!=null&&request.getParameter("scantype").equals("scan")){%>	
	<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
        <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
        <%}%> 	
</head>

<body onload="initForm();">
  <form action="./ProposalSave.jsp" method=post name=fm target="fraSubmit">
    <Div  id= "divButton" style= "display: ''">
    <%@include file="../common/jsp/ProposalOperateButton.jsp"%>
    </DIV>

    <Div  id= "divInputContButton" style= "display: 'none'" style="float: left"> 	
    <INPUT class=cssButton id="riskbutton0" VALUE="问题件录入" TYPE=button onClick="QuestInput();">
      <INPUT class=cssButton id="riskbutton1" VALUE="上一步" TYPE=button onclick="returnparent();">        
      <INPUT class=cssButton id="riskbutton2" VALUE="录入完毕"  TYPE=button onclick="inputConfirm(1);">      
      <!--INPUT class=cssButton id="riskbutton3" VALUE="强制解除锁定" TYPE=button onclick="unLockTable();"-->        	
    </DIV>
    <DIV id = "divApproveContButton" style = "display:'none'" style="float: left">
    <INPUT class=cssButton id="riskbutton4" VALUE="问题件录入" TYPE=button onClick="QuestInput();">
      <INPUT class=cssButton id="riskbutton5" VALUE="上一步" TYPE=button onclick="returnparent();"> 
    	<INPUT class=cssButton id="riskbutton6" VALUE="复核完毕" TYPE=button onclick="inputConfirm(2);">
    	<!--INPUT class=cssButton id="riskbutton7" VALUE="强制解除锁定" TYPE=button onclick="unLockTable();"--> 
    </DIV>
    <DIV id = "divApproveModifyContButton" style = "display:'none'" style="float: right">
      <INPUT class=cssButton id="riskbutton5" VALUE="上一步" TYPE=button onclick="returnparent();">
       <INPUT class=cssButton id="riskbutton4" VALUE="问题件录入" TYPE=button onClick="QuestInput();"> 
    	<INPUT class=cssButton id="riskbutton6" VALUE="复核修改完毕" TYPE=button onclick="inputConfirm(3);">
    	<!--INPUT class=cssButton id="riskbutton7" VALUE="强制解除锁定" TYPE=button onclick="unLockTable();"-->     	 
    </DIV>    
    <Div  id= "divRiskCode0">
    <table class=common>
       <tr class=common>
          <TD  class= title>
            险种代码
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskCode ondblclick="showCodeListEx('RiskCode',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('RiskCode',[this],[1],'', '', '', true);">
          </TD>
       </tr>
    </table>
    </Div>

<!-- 隐藏信息 -->
    <Div  id= "divALL0" style= "display: 'none'">
    <!-- 连带被保人信息部分（列表） -->
	<Div  id= "divLCInsured0" style= "display: 'none'">
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured2);">
    		</td>
    		<td class= titleImg>
    			 连带被保人信息
    		</td>
    	</tr>
      </table>
	  <Div  id= "divLCInsured2" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanSubInsuredGrid" >
					</span> 
				</td>
			</tr>
		</table>
	  </div>
	</div>
    <!-- 受益人信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCBnf1);">
    		</td>
    		<td class= titleImg>
    			 受益人信息<INPUT class=cssButton id="riskbutton5" VALUE="上一步" TYPE=button onclick="returnparent();">
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCBnf1" style= "display: ''" >
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanBnfGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
    <!-- 告知信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
    		</td>
    		<td class= titleImg>
    			 告知信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCImpart1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanImpartGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
    <!-- 特约信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCSpec1);">
    		</td>
    		<td class= titleImg>
    			 特约信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCSpec1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanSpecGrid">
					</span> 
				</td>
			</tr>
		</table>
		<Div id= "divHideButton" style= "display: ''">
  			<input type="button" class=cssButton name= "back" value= "上一步">
  		</Div>
		<Div id= "divHideButton" style= "display: ''">
  			<input type="button" name= "quest" value= "问题件录入">
  		</Div>

	</div>

    <!--可以选择的责任部分，该部分始终隐藏-->
	<Div  id= "divDutyGrid" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanDutyGrid" >
					</span> 
				</td>
			</tr>
		</table>
		<!--确定是否需要责任信息-->
	</div>
	

	<Div  id= "divPremGrid1" style= "display: ''">
		<table  class= common>
			<tr  class= common>
			<td text-align: left colSpan=1>
				<span id="spanPremGrid" >
				</span>
			</td>
			</tr> 
		</table> 
	</div>
	
	


</DIV>	
		<input type=hidden id="inpNeedDutyGrid" name="inpNeedDutyGrid" value="0">
		<input type=hidden id="inpNeedPremGrid" name="inpNeedPremGrid" value="0">		
		<input type=hidden id="fmAction" name="fmAction">
		<input type=hidden id="SaleChnl" name="SaleChnl">
		<input type=hidden id="SaleChnlDetail" name="SaleChnlDetail">
		<input  type= "hidden" class= Common name= SelPolNo value= "">
		<input type=hidden id="ContType" name="ContType" value="">
		            <input type=hidden name=BQFlag>  		
  </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <span id="spanApprove"  style="display: none; position:relative; slategray"></span>
</body>

</html>



