//TempFeeWithdrawInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//暂交费号码查询按钮
function tempFeeNoQuery(afterQuery) {
  var prtNo = fm.PrtNo.value;

  if (prtNo != "") { 
    var strSql = "select ActuGetNo, OtherNo, OtherNoType, PayMode, SumGetMoney, StartGetDate  from ljaget where  exists (select 1 from ljtempfee where tempfeeno =ljaget.otherno and otherno='"+prtNo+"')"     
               + " and ConfDate is null";
  //alert(strSql);
             
  turnPage.queryModal(strSql, FeeGrid);
  }
  else {
    alert("请输入印刷号!"); 
  }
}

//打印实付凭证
function prtLJAGet() {
  if (FeeGrid.getSelNo() == 0) {
    alert("请先选择一行，再进行打印！");
  } 
  else {
    strSql = "select ActuGetNo,OtherNo,OtherNoType,SumGetMoney,ShouldDate,ConfDate,Operator,ManageCom,getUniteCode(AgentCode) from ljaget where ActuGetNo='" + FeeGrid.getRowColData(FeeGrid.getSelNo()-1, 1) + "'";
    fm2.PrtData.value = easyQueryVer3(strSql);
    
    //判断是否银行在途
    var arrPrtData = decodeEasyQueryResult(fm2.PrtData.value);
    if (arrPrtData[0][23] == "1") {
      alert("该实付数据在送银行途中，不允许打印！");
      return;
    }
    
    fm2.submit();
  }
}
