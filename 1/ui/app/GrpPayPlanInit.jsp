<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpPayPlanInit.jsp
//程序功能：
//创建日期：2011-12-22
//创建人  ：gzh
//更新记录：  更新人 更新日期    更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
%> 
<!--用户校验类-->
<SCRIPT src="GrpPayPlanInput.js"></SCRIPT>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox(){
  try{
	// 保单查询条件
  }
  catch(ex)
  {
    alert("GrpPayPlanInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox(){
  try{
  }
  catch(ex)
  {
    alert("GrpPayPlanInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm(){
  try{
    fm.ProposalGrpContNo.value = ProposalGrpContNo;
    initGrpPayPlanGrid();
    initGrpPayPlanDetailGrid();
    if(Flag == "sys"){
    	fm.submitButton.style.display = "none";
    	fm.delButton.style.display = "none";
    	fm.submitButtonDetail.style.display = "none";
    	GrpPayPlanGrid.hiddenPlus =1;
     	GrpPayPlanGrid.hiddenSubtraction = 1;
    }
    getGrpPayPlan();
  }
  catch(re)
  {
    alert("GrpPayPlanInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGrpPayPlanGrid(){
	var iArray = new Array();
	try{
	iArray[0]=new Array();
	iArray[0][0]="序号";
	iArray[0][1]="30px";
	iArray[0][2]=100;
	iArray[0][3]=0;
	//alert("not here");
    iArray[1]= new Array();
	iArray[1][0]="计划编码";
	iArray[1][1]="200px";
	iArray[1][2]=100;
	iArray[1][3]=0;
	
	iArray[2]=new Array();
	iArray[2][0]="约定缴费时间";
	iArray[2][1]="250px";
	iArray[2][2]=100;
	iArray[2][3]=1;
	
	iArray[3]=new Array();
	iArray[3][0]="空白填充列1";
	iArray[3][1]="250px";
	iArray[3][2]=100;
	iArray[3][3]=3;
	
	iArray[4]=new Array();
	iArray[4][0]="空白填充列2";
	iArray[4][1]="250px";
	iArray[4][2]=100;
	iArray[4][3]=3;
	
	iArray[5]=new Array();
	iArray[5][0]="空白填充列3";
	iArray[5][1]="250px";
	iArray[5][2]=100;
	iArray[5][3]=3;
	
	iArray[6]=new Array();
	iArray[6][0]="空白填充列4";
	iArray[6][1]="250px";
	iArray[6][2]=100;
	iArray[6][3]=3;
	
  	GrpPayPlanGrid = new MulLineEnter( "fm" , "GrpPayPlanGrid" );
     //这些属性必须在loadMulLine前
     GrpPayPlanGrid.mulLineCount = 0;
     GrpPayPlanGrid.displayTitle = 1;
     GrpPayPlanGrid.hiddenPlus =0;
     GrpPayPlanGrid.hiddenSubtraction = 0;
     GrpPayPlanGrid.canSel=1;

     //GrpPayPlanGrid.addEventFuncName = "addClick";
     //GrpPayPlanGrid.delEventFuncName = "delTestClick"; //该属性不好用
     GrpPayPlanGrid.selBoxEventFuncName = "ShowGrpPayPlanDetail"; 
     GrpPayPlanGrid.loadMulLine(iArray);
	}
	catch(ex) {
      alert(ex);
    }
}

function initGrpPayPlanDetailGrid(){
	
	var iArray = new Array();
	try{
	iArray[0]=new Array();
	iArray[0][0]="序号";
	iArray[0][1]="30px";
	iArray[0][2]=100;
	iArray[0][3]=0;
	
	iArray[1]= new Array();
	iArray[1][0]="计划编码";
	iArray[1][1]="120px";
	iArray[1][2]=100;
	iArray[1][3]=3;
	
	iArray[2]=new Array();
	iArray[2][0]="约定缴费时间";
	iArray[2][1]="200px";
	iArray[2][2]=100;
	iArray[2][3]=3;
	
    iArray[3]= new Array();
	iArray[3][0]="保障计划编码";
	iArray[3][1]="200px";
	iArray[3][2]=100;
	iArray[3][3]=0;
	
	iArray[4]=new Array();
	iArray[4][0]="约定缴费金额";
	iArray[4][1]="200px";
	iArray[4][2]=100;
	iArray[4][3]=1;
	
	iArray[5]=new Array();
	iArray[5][0]="空白填充列3";
	iArray[5][1]="250px";
	iArray[5][2]=100;
	iArray[5][3]=3;
	
	iArray[6]=new Array();
	iArray[6][0]="空白填充列4";
	iArray[6][1]="250px";
	iArray[6][2]=100;
	iArray[6][3]=3;
	
	     GrpPayPlanDetailGrid = new MulLineEnter( "fm" , "GrpPayPlanDetailGrid" );
      //这些属性必须在loadMulLine前
      GrpPayPlanDetailGrid.mulLineCount = 0;
      GrpPayPlanDetailGrid.displayTitle = 1;
      GrpPayPlanDetailGrid.hiddenPlus = 1;
      GrpPayPlanDetailGrid.hiddenSubtraction = 1;
      //GrpPayPlanDetailGrid.canSel=1;
      //GrpPayPlanDetailGrid.selBoxEventFuncName = "ShowGrpPayPlan"; 
      GrpPayPlanDetailGrid.loadMulLine(iArray);
	}
	catch(ex) {
      alert(ex);
    }
}
</script>