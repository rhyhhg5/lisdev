<%@page contentType="text/html;charset=GBK"%>
<jsp:directive.page import="com.sinosoft.lis.db.LCInsuredDB"/>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：GroupPolInput.jsp
	//程序功能：
	//创建日期：2002-08-15 11:48:43
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//输出参数
	String FlagStr = "";
	String Content = "";
	String operator = request.getParameter("fmtransact");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	if (tG == null) {
		System.out.println("页面失效,请重新登陆");
		FlagStr = "Fail";
		Content = "页面失效,请重新登陆";
	} else //页面有效
	{
		//输入参数
		VData tVData = new VData();
		LCBnfSchema tLCBnfSchema = new LCBnfSchema();
		tLCBnfSchema.setContNo(request.getParameter("ContNo"));
		tLCBnfSchema.setPolNo(request.getParameter("PolNo"));
		tLCBnfSchema.setBnfNo(request.getParameter("BnfNo"));
		tLCBnfSchema.setBnfType(request.getParameter("BnfType"));
		tLCBnfSchema.setSex(request.getParameter("Sex"));
		tLCBnfSchema.setNativePlace(request.getParameter("NativePlace"));
		tLCBnfSchema.setOccupationCode(request.getParameter("OccupationCode"));
		tLCBnfSchema.setPhone(request.getParameter("Phone"));
		tLCBnfSchema.setPostalAddress(request.getParameter("PostalAddress"));
		tLCBnfSchema.setIDStartDate(request.getParameter("IDStartDate"));
		tLCBnfSchema.setIDEndDate(request.getParameter("IDEndDate"));
		tVData.add(tG);
		tVData.add(tLCBnfSchema); 

	    ComplexAddBnfBL tComplexAddBnfBL = new ComplexAddBnfBL();
		if(!tComplexAddBnfBL.submitData(tVData, operator)){
		    Content=tComplexAddBnfBL.mErrors.getContent();
		    FlagStr="Fail";
		}else{
		    Content = " 保存成功! ";
		    FlagStr = "Succ";
		}

		System.out.println("Content:" + Content);
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
