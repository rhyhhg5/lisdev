<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：交叉销售补提批处理
	//程序功能：
	//创建日期：2016-12-26
	//创建人  ：于坤
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
	<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<title>交叉销售补提</title>
		<script src="CrsBTInput.js"></script>
		<%@include file="CrsBTInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="./CrsBTSave.jsp" method="post" name="fm" target="fraTitle">
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输选择日期范围:
					</td>
				</tr>
			</table>

			<table class=common border=0 width=100%>
				<tr class="common">
					<td class="title">起始日期</td>
					<td class="input"><input class=coolDatePicker name=CValidateStart></td>
					<td class="title">截止日期</td>
					<td class="input"><input class=coolDatePicker name=CValidateEnd></td>
					<td class="title"></td>
					<td class="input">
				</tr>
			</table>
			<INPUT VALUE=" 重 提 " class="cssButton" TYPE=button onclick="submitForm();">
			<br />
			<div>	
			<input type=hidden id="fmtransact" name="fmtransact">
			</div>
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
