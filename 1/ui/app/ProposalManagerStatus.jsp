<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：ProposalManagerStatus.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="ProposalManagerStatus.js"></SCRIPT>
  <%@include file="ProposalManagerStatusInit.jsp"%>
  
  <title>更改投保单录入状态</title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  Operator = "<%=tGlobalInput.Operator%>";
</script>

<body  onload="initForm();" >
  <form action="./ProposalManagerStatusSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 更改投保单录入状态 fraSubmit-->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
  		</tr>
  	</table>
  	
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        投保单印刷号
      </TD>
      <TD  class= input>
        <Input class= common name=DOC_CODE >
      </TD>
      <TD  class= title>
        操作员名称
      </TD>
      <TD  class= input>
        <Input class= common name=Operator >
      </TD>
      <TD  class= title>
        录入日期
      </TD>
      <TD  class= input>
        <Input class= common name=InputStartDate >
      </TD>    
    </TR>
    </table>
    <input type=hidden name=PrtNo value="">
    
    <INPUT VALUE="查 询" class= common TYPE=button onclick="easyQueryClick();">
    <br><br>
    
    <!-- 更改投保单录入状态 -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJTempFee1);">
    		</td>
    		<td class= titleImg>
    			 已录入完成投保单列表 <INPUT VALUE="修改录入状态" class= common TYPE=button onclick="submitForm();">
    		</td>
    	</tr>
    </table>
	<Div  id= "divLJPol1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanPolGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
	
  <Div id= "divPage" align=center style= "display: 'none' ">
  <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
  <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
  <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
  <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
            										
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
