<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		String GrpContNo = request.getParameter("GrpContNo");
		String LoadFlag = request.getParameter("LoadFlag");
		String Resource = request.getParameter("Resource");
		String PrtNo = request.getParameter("PrtNo");
	%>
	<head>
		<script>
			var GrpContNo="<%=GrpContNo%>";
			var PrtNo="<%=PrtNo%>"
			var LoadFlag="<%=LoadFlag%>";
 			var Resource="<%=Resource%>";
 		</script>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

		<SCRIPT src="LGrpInsuredInput.js"></SCRIPT>
		<%@include file="LGrpInsuredInit.jsp"%>
		<title>大团单被保人导入</title>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;">
					</td>
					<td class=titleImg>
						合同信息
					</td>
				</tr>
			</table>
			<table class=common align=center>
				<TR class=common>
					<TD class=title>
						印刷号
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=PrtNo>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=ManageCom>
					</TD>
					<TD class=title>
						销售渠道
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=SaleChnl>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						投保单位名称
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=GrpName>
					</TD>
					<TD class=title>
						责任生效日期
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=CValiDate>
					</TD>
					<TD class=title>
						责任终止日期
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=CInValiDate>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						实保人数
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=Peoples>
					</TD>
					<TD class=title>
						总保费
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=Prem>
					</TD>
				</TR>
			</table>
			<br>
			<hr>
			<table>
				<tr class=common style="height:30px">
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"">
					</td>
					<td class=titleImg>
						被保人导入情况
					</td>
					<td style="width:100px">
						<center>
							<INPUT VALUE="刷  新" class=cssbutton TYPE=button
								onclick="refreshInsured();">
						</center>
					</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<TD class=title>
						已导入人数
					</TD>
					<TD class=input>
						<Input class='common' name=InputInsured readonly>
					</TD>
					<TD class=title>
						导入失败人数
					</TD>
					<TD class=input>
						<Input class='common' name=InputErrorInsured readonly>
					</TD>
				</tr>
				<tr class=common>
					<TD class=title>
						已校验人数
					</TD>
					<TD class=input>
						<Input class='common' name=CheckInsured readonly>
					</TD>
					<TD class=title>
						校验失败人数
					</TD>
					<TD class=input>
						<Input class='common' name=CheckErrorInsured readonly>
					</TD>
					<TD class=title>
						已算费人数
					</TD>
					<TD class=input>
						<Input class='common' name=CalculateInsured readonly>
					</TD>
					<TD class=title>
						算费失败人数
					</TD>
					<TD class=input>
						<Input class='common' name=CalculateErrorInsured readonly>
					</TD>
				</tr>
			</table>
			<br>
			<Div id="divPersonInsured" style="display: ''">
				<Table class=common>
					<TR class=common>
						<TD text-align: left colSpan=1>
							<span id="spanContPlanPeoplesGrid"></span>
						</TD>
					</TR>
				</Table>
			</Div>
			<br>
			<hr>
			<br>
			<table>
				<tr>
					<td>
						<input class="button" type="button" value="导入被保险人" onclick="openInputInsured();">
						<input class="button" type="button" value="校验被保险人" onclick="checkDeal();">
						<input class="button" type="button" value="计算保费" onclick="calDeal();">
						<input class="button" type="button" value="错误信息查询" onclick="openQueryInsured();">
						<input class="button" type="button" value="被保人信息调整" onclick="openUpdateInsured();">
						<input class="button" type="button" value="完  成" onclick="NoNameCont();">
						<input class="button" type="button" value="关  闭" onclick="goBack();">
					</td>
				</tr>
			</table>
			<br>
			<font color="red"><strong>在点击【校验被保人】、【计算保费】按钮后，该页面可以关闭，数据处理会在后台进行。</strong></font>
			<br>
			<font color="red"><strong>在保单全部算费完毕后，请点击【完成】按钮。</strong></font>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
