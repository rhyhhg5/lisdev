<%
//程序名称：ReProposalPrintInput.jsp
//程序功能：
//创建日期：2002-11-25
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
	System.out.println("=========进入save=========");
	String tContType = request.getParameter("ContType");
	String tContNo = request.getParameter("ContNo");
	String tCrs_SaleChnl = request.getParameter("Crs_SaleChnl");
	String tCrs_BussType=request.getParameter("Crs_BussType");
	String tGrpagentcom=request.getParameter("Grpagentcom");
	String tGrpagentcomName = request.getParameter("GrpagentcomName");
	String tGrpagentcode=request.getParameter("Grpagentcode");
	String tGrpagentname=request.getParameter("Grpagentname");
	String tGrpagentidno = request.getParameter("Grpagentidno");
	String strOperation = request.getParameter("fmtransact");
	String Content = "";

	GlobalInput globalInput = new GlobalInput();
     globalInput = (GlobalInput) session.getValue("GI");

	CRSModifyInputUI tCRSModifyInputUI =new CRSModifyInputUI();
	TransferData transferData = new TransferData();
	
			transferData.setNameAndValue("ContType",tContType );
			transferData.setNameAndValue("ContNo", tContNo);
			transferData.setNameAndValue("Crs_SaleChnl", tCrs_SaleChnl);
			transferData.setNameAndValue("Crs_BussType", tCrs_BussType);
			transferData.setNameAndValue("Grpagentcom", tGrpagentcom);
			transferData.setNameAndValue("GrpagentcomName", tGrpagentcomName);
			transferData.setNameAndValue("Grpagentcode", tGrpagentcode);
			transferData.setNameAndValue("Grpagentname", tGrpagentname);
			transferData.setNameAndValue("Grpagentidno", tGrpagentidno);
            transferData.setNameAndValue("strOperation", strOperation);
  
	VData vData = new VData();
	vData.add(transferData);
	vData.add(globalInput);
	
	try {
		if( !tCRSModifyInputUI.submitData(vData,strOperation ) )
		{
	   		if ( tCRSModifyInputUI.mErrors.needDealError() )
	   		{
	   			Content = tCRSModifyInputUI.mErrors.getFirstError();
		 	}
		  	else
		  	{
		  		Content = "保存失败，但是没有详细的原因";
			}
		}
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		Content = ex.getMessage();
	}

	String FlagStr = "";
	if(Content.equals("")) 
	{
		FlagStr = "Succ";
		Content = "保存成功";
	} 
	else 
	{
		FlagStr = "Fail";
	}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
