<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	CError tError = null;
	String Content = "";
	String FlagStr="";
	String tAction = request.getParameter("Action");
	String tFinancialAction = request.getParameter("FinancialAction");
	String tContType = request.getParameter("ContType");
	String tPrtNo = request.getParameter("PrtNo");
	String tContNo = request.getParameter("ContNo");
	String tSaleChnl = request.getParameter("mSaleChnl");
	String tAgentCom = request.getParameter("mAgentCom");
	String tAgentCode = request.getParameter("mAgentCode");
	String tAgentGroup = request.getParameter("mAgentGroup");
	
	System.out.println("动作为：" + tAction);
	System.out.println("保单类型为：" + tContType);
	System.out.println("保单印刷号：" + tPrtNo);
	System.out.println("保单号：" + tContNo);
	System.out.println("要改的销售渠道：" + tSaleChnl);
	System.out.println("要改的中介机构代码：" + tAgentCom);
	System.out.println("要改的业务员代码：" + tAgentCode);
	System.out.println("要改的业务员团队代码：" + tAgentGroup);
	
	VData tVData = new VData();
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");
	tVData.add(tGlobalInput);
	
	TransferData tTransferData = new TransferData(); 
	tTransferData.setNameAndValue("tAction", tAction);
	tTransferData.setNameAndValue("tFinancialAction", tFinancialAction);
	tTransferData.setNameAndValue("tContType", tContType);
	tTransferData.setNameAndValue("tPrtNo", tPrtNo);
	tTransferData.setNameAndValue("tContNo", tContNo);
	tTransferData.setNameAndValue("tSaleChnl", tSaleChnl);
	tTransferData.setNameAndValue("tAgentCom", tAgentCom);
	tTransferData.setNameAndValue("tAgentCode", tAgentCode);
	tTransferData.setNameAndValue("tAgentGroup", tAgentGroup);
	
	tVData.addElement(tTransferData);
	
	QYMaintenanceBL tQYMaintenanceBL = new QYMaintenanceBL();
	System.out.println("before submit");
	if(!tQYMaintenanceBL.submitData(tVData,tAction))
	{
		Content = " 保存失败，原因是: " + tQYMaintenanceBL.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	
	System.out.println("Content:"+Content);

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>