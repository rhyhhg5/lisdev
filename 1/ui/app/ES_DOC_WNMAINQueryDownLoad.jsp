<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ES_DOC_MAINQueryDownLoad.jsp
//程序功能：
//创建日期：2007-9-23 19:37
//创建人  ：zhousp
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>

<%
  String FlagStr = "";
  String Content = "";
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
   String mContNo= request.getParameter("mContNo");
  //String fileName = System.currentTimeMillis() + ".xls";
  
  String tOutXmlPath = application.getRealPath("printdata/data") + "/";
  System.out.println("OutXmlPath:" + tOutXmlPath);
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
  tTransferData.setNameAndValue("ContNo",mContNo);
  
  
	try
	{
    VData vData = new VData();
  	vData.add(tG);
  	vData.add(tTransferData);
  	
    OmnipotenceProposalDownloadUI tOmnipotenceProposalDownloadUI = new OmnipotenceProposalDownloadUI();
		if (!tOmnipotenceProposalDownloadUI.submitData(vData, ""))
		{
			Content = "客户卡下载失败，原因是:" + tOmnipotenceProposalDownloadUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
	  else
	  {
	 	  String FileName =tOutXmlPath +mContNo+".zip";
	 	  File file = new File(FileName);
	 	  
	    response.reset();
      response.setContentType("application/octet-stream"); 
      response.setHeader("Content-Disposition","attachment; filename="+mContNo+".zip"+"");
      response.setContentLength((int) file.length());
     System.out.println("怎么回事？？？？？:" );        
      byte[] buffer = new byte[4096];
      BufferedOutputStream output = null;
      BufferedInputStream input = null;    
      //写缓冲区
      try 
      {
        output = new BufferedOutputStream(response.getOutputStream());
        input = new BufferedInputStream(new FileInputStream(file));
        
        int len = 0;
        while((len = input.read(buffer)) >0)
        {
          output.write(buffer,0,len);
        }
        input.close();
        output.close();
      }
      catch (Exception e) 
      {
        e.printStackTrace();
      } // maybe user cancelled download
      finally 
      {
          if (input != null) input.close();
          if (output != null) output.close();
          file.delete();
      }
	  }
	}
	catch(Exception ex)
	{
	  ex.printStackTrace();
	}finally {
  	out.clear();
		out = pageContext.pushBody();
  }
  
  if (!FlagStr.equals("Fail"))
  {
  	Content = "";
  	FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>
