<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolPrintInit.jsp
//程序功能：
//创建日期：2002-11-26
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*" %>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox(){
  try{
    fm.reset();
  }
  catch(ex)
  {
    alert("在GroupPolPrintInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
		initGrpContGrid();
		fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
  }
  catch(re)
  {
    alert("GroupPolPrintInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpContGrid(){
	var iArray = new Array();

	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="保险合同号";
		iArray[1][1]="100px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="印刷号";
		iArray[2][1]="100px";            	
		iArray[2][2]=100;       
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="团单保费";
		iArray[3][1]="65px";
		iArray[3][2]=200;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="投保人名称";
		iArray[4][1]="300px";
		iArray[4][2]=200;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="生效日期";
		iArray[5][1]="70px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="打印次数";
		iArray[6][1]="0px";
		iArray[6][2]=100;
		iArray[6][3]=3;

		iArray[7]=new Array();
		iArray[7][0]="被保险人打印";
		iArray[7][1]="77px";
		iArray[7][2]=100;
		iArray[7][3]=2;
		iArray[7][10]="printInsureDetail";
		iArray[7][11]= "0|^0|不打印|^1|打印";
        iArray[7][12]="7|8";
        iArray[7][13]="1|0";

		iArray[8]=new Array();
		iArray[8][0]="被保险人打印标记";
		iArray[8][1]="0px";
		iArray[8][2]=100;
		iArray[8][3]=3;
		iArray[8][21]="printInsureDetail";

		iArray[9]=new Array();
		iArray[9][0]="保单打印";
		iArray[9][1]="55px";
		iArray[9][2]=100;
		iArray[9][3]=2;
		iArray[9][10]="contPrintFlag";
		iArray[9][11]= "0|^0|不打印|^1|打印";
        iArray[9][12]="9|10";
        iArray[9][13]="1|0";

		iArray[10]=new Array();
		iArray[10][0]="保单打印标记";
		iArray[10][1]="0px";
		iArray[10][2]=100;
		iArray[10][3]=3;
		iArray[10][21]="contPrintFlag";

		GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" );
		//这些属性必须在loadMulLine前
		GrpContGrid.mulLineCount = 0;
		GrpContGrid.displayTitle = 1;
		GrpContGrid.hiddenPlus = 1;
		GrpContGrid.hiddenSubtraction = 1;
		GrpContGrid.canSel = 0;
		GrpContGrid.canChk = 1;
		GrpContGrid.locked = 1;
		GrpContGrid.canChk = 1;
		GrpContGrid.loadMulLine(iArray);

		//这些操作必须在loadMulLine后面
		//GrpContGrid.setRowColData(1,1,"asdf");
	}
	catch(ex){
		alert(ex);
	}
}
</script>