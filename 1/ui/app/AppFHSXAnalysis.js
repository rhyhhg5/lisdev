var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var i = 0;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
    var tSignStartDate = fm.SignStartDate.value;
    var tSignEndDate = fm.SignEndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>12)
	{
		alert("统计期最多为12个月！");
		return false;
	}
    var tManageCom = fm.ManageCom.value; 
    var tSaleChnl = fm.SaleChnl.value; 
    var tTheContType = fm.TheContType.value;
    var strSQL = "select a.managecom,a.prtno,a.CardFlag || ' - ' || (case a.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end),a.approvedate,a.makedate,getUniteCode(a.agentcode),(select name from laagent where agentcode =a.agentcode), "
             + "b.branchattr,b.name,case when a.approvedate is not null then  to_date(a.approvedate)-to_date(a.makedate) else 0 end  "
             + " from lccont a,labranchgroup b where a.conttype='1' "
             + getWherePart('a.SaleChnl','SaleChnl')
             + " and not exists (select 1 from lbcont where prtno=a.prtno ) "
             + " and not exists (select 1 from lcrnewstatelog where prtno=a.prtno) "
             + "and a.managecom like '"+tManageCom+"%' and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' and a.agentgroup = b.agentgroup " ;
    if(tSignStartDate!="" && tSignStartDate!=null && tSignStartDate!="null"){
    	strSQL += " and a.signdate>='"+tSignStartDate+"' ";
    }
    if(tSignEndDate!="" && tSignEndDate!=null && tSignEndDate!="null"){
    	strSQL += " and a.signdate<='"+tSignEndDate+"' ";
    }
    if(tTheContType!="" && tTheContType!=null && tTheContType!="null"){
    	if(tTheContType=="0" || tTheContType=="9"){
    		strSQL += " and a.cardflag='"+tTheContType+"' ";
    	}
    	if(tTheContType=="99"){
    		strSQL += " and a.cardflag not in ('0','9') ";
    	}
    }
    strSQL += "order by a.managecom,b.branchattr,a.agentcode with ur";
    
    fm.AnalysisSql.value = strSQL; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}  
	fm.submit();
}