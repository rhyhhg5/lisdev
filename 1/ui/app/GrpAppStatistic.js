var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
	var tStartDate = fm.StartDate.value;
	var tEndDate = fm.EndDate.value;
	var tMngCom = fm.ManageCom.value;
	var tQYType = fm.QYType.value;
	var tStrSQL = "";
	if(dateDiff(tStartDate,tEndDate,"M")>12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}
	
	if(dateDiff(tStartDate,tEndDate,"M")<1){
      alert("统计止期比统计统计起期早");
      return false;
    }
    
   if(tQYType=="1"){
      tStrSQL =" select A , B , C , D , E , F , G , H , I , J , K , L , M , N , O , P , Q , R , S , T , U , to_zero ( V1 )+to_zero ( V2 ) , W , X , Y , Z , getUniteCode(A1) , A2 "
               +" from "
               +" ( "
               +" select a.grpcontno A , a.prtno B , case a.appflag when '1' then '承保' else '投保' end C , "
               +" b.GrpName D , b.Peoples2 E , a.riskcode F , b.polapplydate G , a.cvalidate H , b.ReceiveDate I ,  "
               +" b.FirstTrialDate J , "
               +" b.FirstTrialOperator K , " 
               +" ( select makedate from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB02' ) L ,   "
               +" ( select ScanOperator from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB02' ) M , " 
               +" b.InputDate N , b.InputOperator O , b.ApproveDate P , b.ApproveCode Q , 0 R , b.UWOperator S ,  "
               +" case B.UWFlag when '9' then '正常承保' when '4' then '通融承保' when '8' then '核保通知书待回复' when '1' then '拒保' when '6' then '待上级核保' when 'z' then '核保订正' end T ,  "
               +" b.uwdate U , ( select sum ( prem ) from lbPOL where conttype='2' and grpcontno=B.grpcontno and riskcode=a.riskcode ) V1 , "
               +" ( select sum ( prem ) from lcPOL where conttype='2' and grpcontno=B.grpcontno and riskcode=a.riskcode ) V2 , "
               +" a.FirstPayDate W , b.signdate X , b.CustomGetPolDate Y , c.name Z , b.agentcode A1 , b.payintv A2 from lcgrppol a , lcgrpcont b , LDCOM C "
               +" where b.prtno not in ( select prtno from lbgrpcont where edorno like 'xb%' ) and a.grpcontno=b.grpcontno AND C.comcode=b.managecom AND b.makedate between  "
               +" '"+tStartDate+"' and '"+tEndDate+"' and substr ( b.ManageCom , 1 , length ( '"+tMngCom+"' ) ) "
               +" ='"+tMngCom+"' "
               +" )as x order by A ";
    }else{
    	
     	tStrSQL ="select A , B , C , D , E , F , G , H , I , J , K , L , M , N , O , P , Q , R , S , T , U , V , W , X , Y , Z , getUniteCode(A1) , '' "
        +" from "
        +" ( "
        +" select a.grpcontno A , a.prtno B , '退保' C , "
        +" b.GrpName D , b.Peoples2 E , a.riskcode F , b.polapplydate G , a.cvalidate H , b.ReceiveDate I , "
        +" b.FirstTrialDate J , "
        +" b.FirstTrialOperator K , " 
        +" ( select makedate from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB02' ) L , "
        +" ( select ScanOperator from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB02' ) M , " 
        +" b.InputDate N , b.InputOperator O , b.ApproveDate P , b.ApproveCode Q , 0 R , b.UWOperator S , "
        +" case B.UWFlag when '9' then '正常承保' when '4' then '通融承保' when '8' then '核保通知书待回复' when '1' then '拒保' when '6' then '待上级核保' when 'z' then '核保订正' end T ,  "
        +" b.uwdate U , value ( ( select sum ( prem ) from lbPOL where conttype='2' and grpcontno=B.grpcontno and riskcode=a.riskcode ) , 0 ) V , "
        +" a.FirstPayDate W , b.signdate X , b.CustomGetPolDate Y , c.name Z , b.agentcode A1 from lbgrppol a , lbgrpcont b , LDCOM C "
        +" where b.edorno not like 'xb%' and a.grpcontno=b.grpcontno AND C.comcode=b.managecom AND b.makedate between  "
        +" '"+tStartDate+"' and '"+tEndDate+"' and substr ( b.ManageCom , 1 , length ( '"+tMngCom+"' ) )='"+tMngCom+"' "
        +" )as x  "
        +" union "
        +" select A , B , C , D , E , F , G , H , I , J , K , L , M , N , O , P , Q , R , S , T , U , V , W , X , Y , Z , getUniteCode(A1) , '' "
        +" from "
        +" (  "
        +" select a.grpcontno A , a.prtno B , '退保' C ,  "
        +" b.GrpName D , b.Peoples2 E , a.riskcode F , b.polapplydate G , a.cvalidate H , b.ReceiveDate I ,  "
        +" b.FirstTrialDate J ,  "
        +" b.FirstTrialOperator K ,  "
        +" ( select makedate from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB02' ) L , "
        +" ( select ScanOperator from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB02' ) M , " 
        +" b.InputDate N , b.InputOperator O , b.ApproveDate P , b.ApproveCode Q , 0 R , b.UWOperator S , "
        +" case B.UWFlag when '9' then '正常承保' when '4' then '通融承保' when '8' then '核保通知书待回复' when '1' then '拒保' when '6' then '待上级核保' when 'z' then '核保订正' end T , "
        +" b.uwdate U , value ( ( select sum ( prem ) from lbPOL where conttype='2' and grpcontno=B.grpcontno and riskcode=a.riskcode ) , 0 ) V , "
        +" a.FirstPayDate W , b.signdate X , b.CustomGetPolDate Y , c.name Z , b.agentcode A1 from lbgrppol a , lcgrpcont b , LDCOM C "
        +" where a.edorno not like 'xb%' and a.grpcontno=b.grpcontno AND C.comcode=b.managecom AND b.makedate between "
        +" '"+tStartDate+"' and '"+tEndDate+"'  and substr ( b.ManageCom , 1 , length ( '"+tMngCom+"' ) )='"+tMngCom+"' "
        +" )as x "
        +" order by A ";      	
        
    }
    
     fm.querySql.value = tStrSQL;
     fm.action="GrpAppStatisticSubDown.jsp";
     fm.submit();
//    var i = 0;
//    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//    //showSubmitFrame(mDebug); 
//	//fm.fmtransact.value = "PRINT";
//	fm.target = "f1print";
//	fm.all('op').value = '';
//	fm.submit();
//	showInfo.close();

}