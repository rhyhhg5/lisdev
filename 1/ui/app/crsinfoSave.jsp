<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	CError tError = null;
	String Content = "";
	String FlagStr="";	
	
	String tedorno=request.getParameter("edorno");
	String taction = request.getParameter("action");
	String tContNo = request.getParameter("ContNo");
	String tSaleChnl = request.getParameter("SaleChnl");
	String tCrsSaleChnl = request.getParameter("CrsSaleChnl");	
	String tCrsBussType = request.getParameter("CrsBussType");	
	String tGrpAgentCode = request.getParameter("GrpAgentCode");
	String tGrpAgentName = request.getParameter("GrpAgentName");
	String tGrpAgentIdNo = request.getParameter("GrpAgentIdNo");
	String tGrpAgentCom = request.getParameter("GrpAgentCom");
	String tAgentCode=request.getParameter("AgentCode");
	String tAgentGroup=request.getParameter("AgentGroup");
	String tAgentCom=request.getParameter("AgentCom");
	
	System.out.println("保单号：" + tContNo);
	System.out.println("要改的销售渠道：" + tSaleChnl);
	System.out.println("要改的交叉销售渠道：" + tCrsSaleChnl);
	System.out.println("要改的交叉销售业务类型：" + tCrsBussType);
	System.out.println("要改的对方业务员代码：" + tGrpAgentCode);
	System.out.println("要改的对方业务员名字：" + tGrpAgentName);
	System.out.println("要改的对方业务员证件号：" + tGrpAgentIdNo);
	System.out.println("要改的对方机构代码：" + tGrpAgentCom);
	System.out.println("要改的机构代码：" + tAgentCom);
	
	
	
	VData tVData = new VData();
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");
	tVData.add(tGlobalInput);
	
	TransferData tTransferData = new TransferData(); 
	tTransferData.setNameAndValue("tAgentCom", tAgentCom);
	tTransferData.setNameAndValue("tAgentGroup", tAgentGroup);
	tTransferData.setNameAndValue("tAgentCode", tAgentCode);	
	tTransferData.setNameAndValue("tContNo", tContNo);
	tTransferData.setNameAndValue("tSaleChnl", tSaleChnl);
	tTransferData.setNameAndValue("tCrsSaleChnl", tCrsSaleChnl);
	tTransferData.setNameAndValue("tCrsBussType", tCrsBussType);
	tTransferData.setNameAndValue("tGrpAgentCode", tGrpAgentCode);
	tTransferData.setNameAndValue("tGrpAgentName", tGrpAgentName);
	tTransferData.setNameAndValue("tGrpAgentIdNo", tGrpAgentIdNo);	
	tTransferData.setNameAndValue("tGrpAgentCom", tGrpAgentCom);
	tTransferData.setNameAndValue("tEdorNo", tedorno);
	
	tVData.addElement(tTransferData);
	
	CrsInfoBL tCrsnanceBL = new CrsInfoBL();
	System.out.println("before submit");
	if(!tCrsnanceBL.submitData(tVData,taction))
	{
		Content = " 处理失败，原因是: " + tCrsnanceBL.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		if("delete".equals(taction)){
			Content = " 删除成功! ";
			FlagStr = "Succ";
		}else{
			Content = " 修改成功! ";
			FlagStr = "Succ";
		}
		
	}
	
	System.out.println("Content:"+Content);

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>