<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BaseStatisticSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
boolean errorFlag = false;

//获得session中的人员信息

GlobalInput tG = (GlobalInput)session.getValue("GI");

//生成文件名
Calendar cal = new GregorianCalendar();
String min=String.valueOf(cal.get(Calendar.MINUTE));
String sec=String.valueOf(cal.get(Calendar.SECOND));
String type= request.getParameter("QYType"); 
String downLoadFileName = "";
String mStartDate=request.getParameter("MakeDate");
String filePath = application.getRealPath("temp");

String querySql = request.getParameter("querySql");

querySql = querySql.replaceAll("%25","%");

	//设置表头
	String[][] tTitle = new String[2][];

	downLoadFileName = "每日个险核保工作量统计表"+tG.Operator+"_"+ min + sec + ".xls";
	tTitle[0] = new String[] { "制表人：" , tG.Operator, "",
			"统计时间：" +mStartDate};
    tTitle[1] = new String[] {"机构代码","机构名称","核保审核完毕件数","尚未审核完毕件数"};
    

String tOutXmlPath = filePath +File.separator+ downLoadFileName;
System.out.println("OutXmlPath:" + downLoadFileName);
 
	//表头的显示属性
int []displayTitle = {1,2,3,4};
  
//数据的显示属性
int []displayData = {1,2,3,4};

CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
createexcellist.createExcelFile();
String[] sheetName ={"list"};
createexcellist.addSheet(sheetName);
int row = createexcellist.setData(tTitle,displayTitle);

if(row ==-1) errorFlag = true;
createexcellist.setRowColOffset(row+1,0);//设置偏移
if(createexcellist.setData(querySql,displayData)==-1)
{
	errorFlag = true;
	System.out.println(errorFlag);
}
if(!errorFlag)
//写文件到磁盘
try{
   createexcellist.write(tOutXmlPath);
}catch(Exception e)
{
	errorFlag = true;
	System.out.println(e);
}
//返回客户端
if(!errorFlag)
	downLoadFile(response,filePath,downLoadFileName);
out.clear();
	out = pageContext.pushBody();
%>
