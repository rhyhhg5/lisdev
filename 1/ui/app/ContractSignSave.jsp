<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalSignSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  
  tG=(GlobalInput)session.getValue("GI");
  
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	LCPolSet tLCPolSet = new LCPolSet();

	String tProposalNo[] = request.getParameterValues("PolGrid1");
	String tChk[] = request.getParameterValues("InpPolGridChk");
	boolean flag = false;
	int proposalCount = tProposalNo.length;
	int ProposalCount = 10;	

	for (int i = 0; i < proposalCount; i++)
	{
		if (tProposalNo[i] != null && tChk[i].equals("1"))
		{
System.out.println("ProposalNo:"+i+":"+tProposalNo[i]);
	  		LCPolSchema tLCPolSchema = new LCPolSchema();
	
		    tLCPolSchema.setPolNo( tProposalNo[i] );
	    
		    tLCPolSet.add( tLCPolSchema );
		    flag = true;
		}
	}
	
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCPolSet );
		
		// 数据传输
		ProposalSignUI tProposalSignUI   = new ProposalSignUI();
		if (tProposalSignUI.submitData(tVData,"INSERT") == false)
		{
int n = tProposalSignUI.mErrors.getErrorCount();
for (int i = 0; i < n; i++)
System.out.println("Error: "+tProposalSignUI.mErrors.getError(i).errorMessage);
			Content = " 查询失败，原因是: " + tProposalSignUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tProposalSignUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 查询成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 查询失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
		}
	}  
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.initPolGrid();
	parent.fraInterface.fm.submit();
</script>
</html>
