<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：tbGrpParticularStatisticSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
boolean errorFlag = false;

//获得session中的人员信息

GlobalInput tG = (GlobalInput)session.getValue("GI");

//生成文件名
Calendar cal = new GregorianCalendar();
String min=String.valueOf(cal.get(Calendar.MINUTE));
String sec=String.valueOf(cal.get(Calendar.SECOND));
String type= request.getParameter("QYType"); 
String downLoadFileName = "";
String mStartDate=request.getParameter("StartDate");
String mEndDate=request.getParameter("EndDate");
String filePath = application.getRealPath("temp");

String querySql = request.getParameter("querySql");

querySql = querySql.replaceAll("%25","%");

	//设置表头
	String[][] tTitle = new String[2][];

if(type.equals("1")){
	downLoadFileName = "团单投保状况明细统计报表_"+tG.Operator+"_"+ min + sec + ".xls";
	tTitle[0] = new String[] { "制表人：" , tG.Operator, "",
			"统计时间：" +mStartDate+"至"+mEndDate,"","","",""};
    tTitle[1] = new String[] {"机构代码","机构名称","险种","投保件数","投保客户数","投保人数","投保金额",""};
    
}else if(type.equals("2")){
	downLoadFileName = "团单核保状况明细统计报表_"+tG.Operator+"_"+ min + sec + ".xls";	
	tTitle[0] = new String[] { "制表人：" , tG.Operator, "",
			"统计时间：" +mStartDate+"至"+mEndDate,"","","","",""};
    tTitle[1] = new String[] {"机构代码","机构名称","险种","契撤件数","契撤保费","核保通过待签单件数","核保通过待签单人数","核保通过待签单保费"};
}else{
    downLoadFileName = "团单承保状况明细统计报表_"+tG.Operator+"_"+ min + sec + ".xls";
    tTitle[0] = new String[] { "制表人：" , tG.Operator, "",
			"统计时间：" +mStartDate+"至"+mEndDate,"","","","" };
    tTitle[1] = new String[] {"机构代码","机构名称","险种","承保件数","承保客户数","承保人数","承保保费",""};
}

String tOutXmlPath = filePath +File.separator+ downLoadFileName;
System.out.println("OutXmlPath:" + downLoadFileName);
 
	//表头的显示属性
int []displayTitle = {1,2,3,4,5,6,7,8};
  
//数据的显示属性
int []displayData = {1,2,3,4,5,6,7,8};

CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
createexcellist.createExcelFile();
String[] sheetName ={"list"};
createexcellist.addSheet(sheetName);
int row = createexcellist.setData(tTitle,displayTitle);

if(row ==-1) errorFlag = true;
createexcellist.setRowColOffset(row+1,0);//设置偏移
if(createexcellist.setData(querySql,displayData)==-1)
{
	errorFlag = true;
	System.out.println(errorFlag);
}
if(!errorFlag)
//写文件到磁盘
try{
   createexcellist.write(tOutXmlPath);
}catch(Exception e)
{
	errorFlag = true;
	System.out.println(e);
}
//返回客户端
if(!errorFlag)
	downLoadFile(response,filePath,downLoadFileName);
out.clear();
	out = pageContext.pushBody();
%>
