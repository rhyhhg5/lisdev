<%
//Name:ReportInit.jsp
//function：
//author:Xx
//Date:2005-08-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode; 
System.out.println(strManageCom);
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.all('mComCode').value = '<%=strManageCom%>';
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();
    initQueryLDRiskGrid();

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initQueryLDRiskGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		    //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		    //列最大值
      iArray[0][3]=0;              		    //是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";   		        //列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            		    //列最大值
      iArray[1][3]=0;              		    //是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="险种名称";		        //列名
      iArray[2][1]="150px";                 //列宽
      iArray[2][2]=60;            		    //列最大值
      iArray[2][3]=0;              		    //是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="限制保额";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=200;            	        //列最大值
      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="是否存在最大保额";         //列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=200;            	        //列最大值
      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许


      QueryLDRiskGrid = new MulLineEnter( "fm" , "QueryLDRiskGrid" ); 
      //这些属性必须在loadMulLine前
      QueryLDRiskGrid.mulLineCount = 3;   
      QueryLDRiskGrid.displayTitle = 1;
      QueryLDRiskGrid.hiddenPlus = 1;
      QueryLDRiskGrid.hiddenSubtraction = 1;
      QueryLDRiskGrid.canSel = 1;
      QueryLDRiskGrid.loadMulLine(iArray);  
      QueryLDRiskGrid.selBoxEventFuncName = "ShowDetail";

      }
      catch(ex)
      {
        alert(ex);
      }
}

 </script>