<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
    tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var strManageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Verifyinput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="GrpWaitFeeInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="GrpWaitFeeInputInit.jsp"%>
    <title>待收费保单下载</title>
</head>
<body onload="initForm();" >
    <form action="" method=post name=fm target="fraSubmit">
        <!-- 个人保单信息部分 -->
        <table class= common border=0 width=100%>
            <tr><td class= titleImg align= center>请输入保单打印查询条件：</td></tr>
        </table>
        <table class= common align=center>
            <TR class= common>
                <TD class= title>管理机构</TD>
                <TD class= input><input class="codeNo" name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><input class=codename name=ManageComName readonly ></TD>
                <TD class= title>印刷号</TD>
                <TD class= input><input class="common" name=PrtNo ></TD>
            </TR>
            <TR  class= common> 
	          <TD  class= title>
	            录单起始日期
	          </TD>          
	          <TD  class= input>
	          <Input class="coolDatePicker"  name=InputDataNo1 >
	          </TD>        
	          <TD  class= title>
	            录单终止日期
	          </TD>
	          <TD  class= input>
	            <Input class="coolDatePicker" name=InputDataNo2 >
	          </TD>          
	      	</TR>
        </table>
        <br>
        <input type="hidden" class="common" name="querySql" >
        <input value="查询信息" class="cssButton" type=button onclick="easyQueryClick();">
        <input value="下载清单" class="cssButton" type=button onclick="download();">
        <br>
        <br>
        <table>
            <tr>
                <td class=common>
                    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContPrintDaily);">
                </td>
                <td class= titleImg>待交费保单清单</td>
            </tr>
        </table>
        <div id= "divContPrintDaily" style= "display: ''">
            <table class= common>
                <tr class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanContPrintDailyGrid" ></span>
                    </td>
                </tr>
            </table>
            <div id= "divPage" align=center style= "display: 'none' ">
                <input value="首 页" class="cssButton" type=button onclick="turnPage.firstPage();">
                <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
                <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
                <input value="尾 页" class="cssButton" type=button onclick="turnPage.lastPage();">
            </div>
        </div>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
