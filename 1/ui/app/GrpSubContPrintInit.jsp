<%
//程序名称：
//程序功能：
//创建日期：2009-7-15
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initGrpContGrid();
        initGrpSubContGrid();
        
        fm.all("ManageCom").value = <%=strManageCom%>;

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initGrpContGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="合同号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="印刷号";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="投保单位";
        iArray[3][1]="250px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="销售渠道";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="业务员代码";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="生效日期";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="管理机构";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="被保险人人数";
        iArray[8][1]="0px";
        iArray[8][2]=100;
        iArray[8][3]=0;        


        GrpContGrid = new MulLineEnter("fm", "GrpContGrid"); 

        GrpContGrid.mulLineCount = 0;   
        GrpContGrid.displayTitle = 1;
        GrpContGrid.canSel = 1;
        GrpContGrid.hiddenSubtraction = 1;
        GrpContGrid.hiddenPlus = 1;
        GrpContGrid.canChk = 0;
        GrpContGrid.selBoxEventFuncName = "initGrpSubConf";
        GrpContGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化GrpContGrid时出错：" + ex);
    }
}

function initGrpSubContGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="分单合同号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="生效日期";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="客户号";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="被保人姓名";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="性别";
        iArray[5][1]="60px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="证件类型";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="证件号";
        iArray[7][1]="120px";
        iArray[7][2]=100;
        iArray[7][3]=0;


        GrpSubContGrid = new MulLineEnter("fm", "GrpSubContGrid"); 

        GrpSubContGrid.mulLineCount = 0;   
        GrpSubContGrid.displayTitle = 1;
        GrpSubContGrid.canSel = 0;
        GrpSubContGrid.hiddenSubtraction = 1;
        GrpSubContGrid.hiddenPlus = 1;
        GrpSubContGrid.canChk = 1;
        GrpSubContGrid.selBoxEventFuncName = "";
        GrpSubContGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化GrpSubContGrid时出错：" + ex);
    }
}
</script>

