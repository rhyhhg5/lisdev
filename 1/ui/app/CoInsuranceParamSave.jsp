<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：TbGrpConractorSave.jsp
//程序功能：
//创建日期：2006-5-17 17:12
//创建人  ：yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
    System.out.println("start CoInsuranceParamSave.jsp...");
    
    CErrors tError = null;
    String FlagStr = "";
    String Content = "";
    
	GlobalInput tG=(GlobalInput)session.getValue("GI");
    
    // 获取保单关键信息。
    String tPrtNo = request.getParameter("PrtNo");
    String tGrpContNo = request.getParameter("GrpContNo");
    String tProposalGrpContNo = request.getParameter("ProposalGrpContNo");
    // --------------------------
    
    // 获取Mutline数据。
    String tRows[] = request.getParameterValues("CoInsuranceParamGridNo");
    String tCIAgentComCodes[] = request.getParameterValues("CoInsuranceParamGrid1");
    String tCIAgentComNames[] = request.getParameterValues("CoInsuranceParamGrid2");
    String tCIRates[] = request.getParameterValues("CoInsuranceParamGrid3");
    // --------------------------
    
    LCCoInsuranceParamSet tLCCoInsuranceParamSet = new LCCoInsuranceParamSet();
    
    for(int i = 0; i < tRows.length; i++)
    {
        LCCoInsuranceParamSchema tTmpLCCoInsuranceParamSchema = new LCCoInsuranceParamSchema();
        
        tTmpLCCoInsuranceParamSchema.setGrpContNo(tGrpContNo);
        tTmpLCCoInsuranceParamSchema.setProposalGrpContNo(tProposalGrpContNo);
        tTmpLCCoInsuranceParamSchema.setPrtNo(tPrtNo);
        
        tTmpLCCoInsuranceParamSchema.setAgentCom(tCIAgentComCodes[i]);
        tTmpLCCoInsuranceParamSchema.setAgentComName(tCIAgentComNames[i]);
        tTmpLCCoInsuranceParamSchema.setRate(tCIRates[i]);
        
        tLCCoInsuranceParamSet.add(tTmpLCCoInsuranceParamSchema);
    }
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
    
    VData tVData = new VData();
    tVData.add(tG);
    tVData.add(tLCCoInsuranceParamSet);
    tVData.add(tTransferData);
    
    String tOperate = request.getParameter("fmtransact");
    
    CoInsuranceGrpContUL tCoInsuranceGrpContUL = new CoInsuranceGrpContUL();
    
    try
    {        
        if (!tCoInsuranceGrpContUL.submitData(tVData, tOperate))
        {
            Content = " 处理失败! 原因是: " + tCoInsuranceGrpContUL.mErrors.getLastError();
            FlagStr = "Fail";
        }
        else
        {
            Content = " 处理成功！";
            FlagStr = "Succ";
        }
    }
    catch(Exception ex)
    {
        Content = "处理失败，原因是:" + ex.toString();
        FlagStr = "Fail";
    }
    System.out.println("end CoInsuranceParamSave.jsp...");

%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>