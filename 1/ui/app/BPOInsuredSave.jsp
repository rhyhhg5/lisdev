<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：外包录入错误数据修改被保人保存页面
//创建日期：2007-10-25 20:40
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.utility.TransferData"%>
  <%@page import="com.sinosoft.lis.brieftb.*" %>
<%
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
	//输出参数            
	String FlagStr="";
	String Content = "";
	
	String tBPOBatchNo = request.getParameter("BPOBatchNo");
	String tContID = request.getParameter("ContID");
	String tAppntID = request.getParameter("AppntID");
	String tPrtNo = request.getParameter("PrtNo");
	String tInsuredID = request.getParameter("BPOInsuredID");
	
  BPOLCInsuredSchema tBPOLCInsuredSchema = null;
      
  //修改投保人合同信息
  BPOLCInsuredDB tBPOLCInsuredDB = new BPOLCInsuredDB();
  tBPOLCInsuredDB.setBPOBatchNo(tBPOBatchNo);
  tBPOLCInsuredDB.setInsuredID(tInsuredID);
  if(tBPOLCInsuredDB.getInfo())
  {
    tBPOLCInsuredSchema = tBPOLCInsuredDB.getSchema();
    tBPOLCInsuredSchema.setInsuredID(request.getParameter("BPOInsuredID"));
    tBPOLCInsuredSchema.setName(request.getParameter("BPOInsuredName"));
    tBPOLCInsuredSchema.setBirthday(request.getParameter("BPOInsuredBirthday"));
    tBPOLCInsuredSchema.setSex(request.getParameter("BPOInsuredSex"));
    tBPOLCInsuredSchema.setMarriage(request.getParameter("BPOInsuredMarriage"));
    tBPOLCInsuredSchema.setIDType(request.getParameter("BPOInsuredIDType"));
    tBPOLCInsuredSchema.setIDNo(request.getParameter("BPOInsuredIDNo"));
    tBPOLCInsuredSchema.setNativePlace(request.getParameter("BPOInsuredNativePlace"));
    tBPOLCInsuredSchema.setGrpName(request.getParameter("BPOInsuredGrpName"));
    tBPOLCInsuredSchema.setPosition(request.getParameter("BPOInsuredPosition"));
    tBPOLCInsuredSchema.setOccupationCode(request.getParameter("BPOInsuredOccupationCode"));
    tBPOLCInsuredSchema.setOccupationType(request.getParameter("BPOInsuredOccupationType"));
    tBPOLCInsuredSchema.setSalary(request.getParameter("BPOInsuredSalary"));
    tBPOLCInsuredSchema.setPostalAddress(request.getParameter("BPOInsuredPostalAddress"));
    tBPOLCInsuredSchema.setZipCode(request.getParameter("BPOInsuredZipCode"));
    tBPOLCInsuredSchema.setPhone(request.getParameter("BPOInsuredPhone"));
    tBPOLCInsuredSchema.setMobile(request.getParameter("BPOInsuredMobile"));
    tBPOLCInsuredSchema.setEMail(request.getParameter("BPOInsuredEMail"));
    tBPOLCInsuredSchema.setIDStartDate(request.getParameter("InsuIDStartDate"));
    tBPOLCInsuredSchema.setIDEndDate(request.getParameter("InsuIDEndDate"));
    tBPOLCInsuredSchema.setHomePhone(request.getParameter("BPOInsuredHomePhone"));
    tBPOLCInsuredSchema.setNativeCity(request.getParameter("BPOInsuredNativeCity"));
  }
  
  //处理套餐信息
  BPOLCRiskDutyWrapSet mBPOLCRiskDutyWrapSet = new BPOLCRiskDutyWrapSet();
  String tWrapChk[] = request.getParameterValues("InpRiskWrapGridChk");
  int tWrapCol = Integer.parseInt(StrTool.cTrim(request.getParameter("RiskWrapCol")));
  String tRiskWrapCode[] = request.getParameterValues("RiskWrapGrid1");
  String tWrapCode = request.getParameter("RiskWrapGrid1");
  String CalFactorValue;
  if(tWrapChk != null){
  	int k = 0;
	  for (int index = 0; index < tWrapChk.length; index++) {
	    if (tWrapChk[index].equals("1")) {
	      for (int i = 3; i < tWrapCol; i++) {
	        if (StrTool.cTrim(request.getParameterValues("RiskWrapGrid" + i)[index]).equals("Factor")) {
	          CalFactorValue = request.getParameterValues("RiskWrapGrid" + (i + 2))[index];
	          
	          BPOLCRiskDutyWrapSchema tBPOLCRiskDutyWrapSchema = new BPOLCRiskDutyWrapSchema();
				    tBPOLCRiskDutyWrapSchema.setBPOBatchNo(tBPOBatchNo);
				    tBPOLCRiskDutyWrapSchema.setContID(tContID);
				    tBPOLCRiskDutyWrapSchema.setInsuredID(tInsuredID);
				    tBPOLCRiskDutyWrapSchema.setSequenceNo(k);
				    tBPOLCRiskDutyWrapSchema.setPrtNo(tPrtNo);
	          tBPOLCRiskDutyWrapSchema.setRiskWrapCode(tRiskWrapCode[index]);
	          tBPOLCRiskDutyWrapSchema.setRiskCode("000000");
	          tBPOLCRiskDutyWrapSchema.setDutyCode("000000");
	          tBPOLCRiskDutyWrapSchema.setCalFactor(request.getParameterValues("RiskWrapGrid" + (i + 1))[index]);
	          tBPOLCRiskDutyWrapSchema.setCalFactorValue(CalFactorValue);
	          if(i == 11 && (CalFactorValue == null ||CalFactorValue.equals("")))
	          {
	          	tBPOLCRiskDutyWrapSchema.setCalFactorValue("Y");
	          }
	          tBPOLCRiskDutyWrapSchema.setOperator(tG.Operator);
	          mBPOLCRiskDutyWrapSet.add(tBPOLCRiskDutyWrapSchema);
	          k++;
	          System.out.println(i + "****" + tBPOLCRiskDutyWrapSchema.getRiskWrapCode() + "****"
	          									+ tBPOLCRiskDutyWrapSchema.getCalFactor() + "****"
	          									+ tBPOLCRiskDutyWrapSchema.getCalFactorValue());
	        }
	      }
	    }
	  }
  }
  
  //抵达国家
	BPOLCNationSet tBPOLCNationSet = new BPOLCNationSet();
  String[] tNationNo = request.getParameterValues("NationGrid1");//国家代码
  String[] tNationName = request.getParameterValues("NationGrid2");
  int MulCount = 0;
  if (tNationNo != null)    MulCount = tNationNo.length;
  for (int n = 0; n < MulCount; n++) {
    BPOLCNationSchema tBPOLCNationSchema = new BPOLCNationSchema();
    tBPOLCNationSchema.setBPOBatchNo(tBPOBatchNo);
    tBPOLCNationSchema.setContID(tContID);
    tBPOLCNationSchema.setInsuredID(tInsuredID);
    tBPOLCNationSchema.setSequenceNo(n);
    tBPOLCNationSchema.setPrtNo(tPrtNo);
    tBPOLCNationSchema.setNationNo(tNationNo[n]);
    tBPOLCNationSchema.setChineseName(tNationName[n]);
    tBPOLCNationSet.add(tBPOLCNationSchema);
    //System.out.println(tBPOLCNationSchema.getNationNo());
  }
  
  //险种信息
  String tRelationToAppnt = request.getParameter("RelationToAppnt");
  String tRelationToMainInsured = request.getParameter("RelationToMainInsured");
  String[] tMainPolIDs = request.getParameterValues("PolGrid1");
  String[] tInsuredIDs = request.getParameterValues("PolGrid3");
  String[] tPolIDs = request.getParameterValues("PolGrid4");
  String[] tRiskCodes = request.getParameterValues("PolGrid6");
  String[] tAmntMults = request.getParameterValues("PolGrid7");
  String[] tInsuYears = request.getParameterValues("PolGrid8");
  String[] tInsuYearFlags = request.getParameterValues("PolGrid9");
  String[] tPayEndYears = request.getParameterValues("PolGrid10");
  String[] tPayEndYearFlags = request.getParameterValues("PolGrid11");
  String[] tPayIntvs = request.getParameterValues("PolGrid12");
  String[] tPrems = request.getParameterValues("PolGrid14");
  String[] tSupplementaryPrems = request.getParameterValues("PolGrid15");
  BPOLCPolSet tBPOLCPolSet = new BPOLCPolSet();
  if(tRiskCodes != null && tRiskCodes.length > 0)
  {
    for(int i = 0; i < tRiskCodes.length; i++)
    {
      BPOLCPolSchema tBPOLCPolSchema = new BPOLCPolSchema();
      tBPOLCPolSchema.setBPOBatchNo(tBPOBatchNo);
      tBPOLCPolSchema.setContID(tContID);
      tBPOLCPolSchema.setInsuredID(tInsuredIDs[i]);
      tBPOLCPolSchema.setMainPolID(tMainPolIDs[i]);
      //tBPOLCPolSchema.setMainPolID(tMainPolIDs[i]);
      tBPOLCPolSchema.setPolID(tPolIDs[i]);
      tBPOLCPolSchema.setAppntID(tAppntID);
      tBPOLCPolSchema.setRiskCode(tRiskCodes[i]);
      tBPOLCPolSchema.setInsuYear(tInsuYears[i]);
      tBPOLCPolSchema.setInsuYearFlag(tInsuYearFlags[i]);
      tBPOLCPolSchema.setPayEndYear(tPayEndYears[i]);
      tBPOLCPolSchema.setPayEndYearFlag(tPayEndYearFlags[i]);
      tBPOLCPolSchema.setPayIntv(tPayIntvs[i]);
      tBPOLCPolSchema.setPrem(tPrems[i]);
      tBPOLCPolSchema.setSupplementaryPrem(tSupplementaryPrems[i]);
      tBPOLCPolSchema.setRelationToAppnt(tRelationToAppnt);
      tBPOLCPolSchema.setRelationToMainInsured(tRelationToMainInsured);
      tBPOLCPolSchema.setPrtNo(request.getParameter("PrtNoOld"));
      tBPOLCPolSchema.setPolApplyDate(request.getParameter("PolApplyDate"));
      tBPOLCPolSchema.setCValiDate(request.getParameter("CValiDate"));
      tBPOLCPolSchema.setFirstTrialOperator(request.getParameter("FirstTrialOperator"));
      tBPOLCPolSchema.setReceiveDate(request.getParameter("ReceiveDate"));
      tBPOLCPolSchema.setPayMode(request.getParameter("PayMode"));
      tBPOLCPolSchema.setManageCom(request.getParameter("ManageCom"));
      tBPOLCPolSchema.setSaleChnl(request.getParameter("SaleChnl"));
      tBPOLCPolSchema.setAgentCode(request.getParameter("GroupAgentCode"));
      tBPOLCPolSchema.setExPayMode(request.getParameter("ExPayMode"));
      
	// 获取业务员代码对应的业务员姓名
    String tAgentName = "";
    if(request.getParameter("AgentCode") != null)
    {
        String tSql = "select Name from LAAgent where AgentCode = '" + request.getParameter("AgentCode") + "'";
        tAgentName = new ExeSQL().getOneValue(tSql);
    }
    tBPOLCPolSchema.setAgentName(tAgentName);
	// -----------------------------
      
      String sql = "select min(CalMode) from LMDuty "
                 + "where DutyCode in"
                 + "    (select DutyCode from LMRiskDuty "
                 + "    where RiskCode = '" + tRiskCodes[i] + "') ";
      String tCalMode = new ExeSQL().getOneValue(sql);
      if("O".equals(tCalMode))
      {
        tBPOLCPolSchema.setMult(tAmntMults[i]);
      }
      else
      {
        tBPOLCPolSchema.setAmnt(tAmntMults[i]);
      }
      
      tBPOLCPolSet.add(tBPOLCPolSchema);
    }
  }
  
  //客户告知
	//String tImpartVer[] = request.getParameterValues("ImpartGrid1");            //告知版别
	String tImpartCode[] = request.getParameterValues("ImpartGrid2");           //告知编码
	//String tImpartContent[] = request.getParameterValues("ImpartGrid3");        //告知内容
	String tImpartParamModle[] = request.getParameterValues("ImpartGrid4");    //告知客户号码
  BPOLCImpartSet tBPOLCImpartSet = new BPOLCImpartSet();
	if(tImpartCode != null && tImpartCode.length > 0)
  {
  	for (int i = 0; i < tImpartCode.length; i++)	
  	{
  	  BPOLCImpartSchema tBPOLCImpartSchema = new BPOLCImpartSchema();
  	  tBPOLCImpartSchema.setBPOBatchNo(tBPOBatchNo);
  	  tBPOLCImpartSchema.setContID(tContID);
  	  tBPOLCImpartSchema.setCustomerNoType("I");
  	  tBPOLCImpartSchema.setCustomerNo(tInsuredID);
  		//tBPOLCImpartSchema.setImpartVer(tImpartVer[i]);
  		tBPOLCImpartSchema.setImpartVer("001");
  		tBPOLCImpartSchema.setImpartCode(tImpartCode[i]);
  		tBPOLCImpartSchema.setImpartParamModle(tImpartParamModle[i]);
  		//tBPOLCImpartSchema.setDiseaseContent(tImpartContent[i]);
  		tBPOLCImpartSet.add(tBPOLCImpartSchema);
  	}
  }
  
  //客户告知明细
	//String tImpartDetailVer[] = request.getParameterValues("ImpartDetailGrid1");            //告知版别
	String tImpartDetailCode[] = request.getParameterValues("ImpartDetailGrid2");           //告知编码
	//String tImpartDetailContent[] = request.getParameterValues("ImpartDetailGrid3");        //告知内容
	String tImpartDetailDiseaseContent[] = request.getParameterValues("ImpartDetailGrid4");    //告知客户号码
  BPOLCImpartSet tBPOLCImpartSetDetail = new BPOLCImpartSet();
	if(tImpartDetailCode != null && tImpartDetailCode.length > 0)
  {
  	for (int i = 0; i < tImpartDetailCode.length; i++)	
  	{
  	  BPOLCImpartSchema schema = new BPOLCImpartSchema();
  	  schema.setBPOBatchNo(tBPOBatchNo);
  	  schema.setContID(tContID);
  	  schema.setCustomerNoType("I");
  	  schema.setCustomerNo(tInsuredID);
  		//schema.setImpartVer(tImpartDetailVer[i]);
  		schema.setImpartVer("001");
  		schema.setImpartCode(tImpartDetailCode[i]);
  		//schema.setImpartParamModle(tImpartDetailParamModle[i]);
  		schema.setDiseaseContent(tImpartDetailDiseaseContent[i]);
  		tBPOLCImpartSetDetail.add(schema);
  	}
  }
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("BPOLCImpartSetDetail", tBPOLCImpartSetDetail);
  
  //受益人
	String tInsuredIDBnfs[] = request.getParameterValues("BnfGrid2"); 
	String tBnfTypes[] = request.getParameterValues("BnfGrid3"); 
	String tNames[] = request.getParameterValues("BnfGrid5");
	String tSexs[] = request.getParameterValues("BnfGrid6"); 
	String tBirthdays[] = request.getParameterValues("BnfGrid8"); 
	String tIDTypes[] = request.getParameterValues("BnfGrid9");
	String tIDNos[] = request.getParameterValues("BnfGrid11");
	String tRelationToInsureds[] = request.getParameterValues("BnfGrid12");
	String tBnfLots[] = request.getParameterValues("BnfGrid14");
	String tBnfGrades[] = request.getParameterValues("BnfGrid15");
  BPOLCBnfSet tBPOLCBnfSet = new BPOLCBnfSet();
	if(tBnfTypes != null && tBnfTypes.length > 0)
  {
  	for (int i = 0; i < tBnfTypes.length; i++)	
  	{
  	  BPOLCBnfSchema tBPOLCBnfSchema = new BPOLCBnfSchema();
  	  tBPOLCBnfSchema.setBPOBatchNo(tBPOBatchNo);
  	  tBPOLCBnfSchema.setContID(tContID);
  	  tBPOLCBnfSchema.setInsuredID(tInsuredIDBnfs[i]);
  		tBPOLCBnfSchema.setBnfType(tBnfTypes[i]) ;
  		tBPOLCBnfSchema.setName(tNames[i]);
  		tBPOLCBnfSchema.setSex(tSexs[i]);
  		tBPOLCBnfSchema.setBirthday(tBirthdays[i]);
  		tBPOLCBnfSchema.setIDType(tIDTypes[i]);
  		tBPOLCBnfSchema.setIDNo(tIDNos[i]);
  		tBPOLCBnfSchema.setRelationToInsured(tRelationToInsureds[i]);
  		tBPOLCBnfSchema.setBnfLot(tBnfLots[i]);
  		tBPOLCBnfSchema.setBnfGrade(tBnfGrades[i]);
			tBPOLCBnfSchema.setPrtNo(tPrtNo);
  		tBPOLCBnfSet.add(tBPOLCBnfSchema);
  	}
  }
  
    // 综合开拓信息
    String tAssistSaleChnl = request.getParameter("AssistSaleChnl");
  	String tAssistAgentCode = request.getParameter("AssistAgentCode");
	LCExtendSchema tLCExtendSchema = new LCExtendSchema();
	tLCExtendSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCExtendSchema.setAssistSalechnl(tAssistSaleChnl);
	tLCExtendSchema.setAssistAgentCode(tAssistAgentCode);
  
    tTransferData.setNameAndValue("SubType", request.getParameter("SubType"));
	VData tVData = new VData();
	tVData.add(tBPOLCInsuredSchema);   
	tVData.add(tBPOLCNationSet);  
	tVData.add(mBPOLCRiskDutyWrapSet);  
	tVData.add(tBPOLCPolSet);  
	tVData.add(tBPOLCImpartSet);  
	tVData.add(tBPOLCBnfSet);   
	tVData.add(tLCExtendSchema);
	tVData.add(tTransferData);                                       
	tVData.add(tG);
BPOInsuredUI tBPOInsuredUI = new BPOInsuredUI();   
if(!tBPOInsuredUI.submitData( tVData, SysConst.UPDATE))                       
	{                                                                               
		Content = " 保存失败，原因是: " + tBPOInsuredUI.mErrors.getErrContent();
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
  Content = PubFun.changForHTML(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmitBPO("<%=FlagStr%>","<%=Content%>");
</script>
</html>

