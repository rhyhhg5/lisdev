<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput) session.getValue("GI");
	String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">
function initForm()
{
  try
  {
	initBox();
	initSetGrid(); 
  }
  catch(re)
  {
    alert("在GrpContAgentsaleUPInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initBox()
{
    try
    {
        // 初始化
        fm.all("PrtNo").value = '';
        fm.all("ContNo").value = '';
        fm.all("ManageCom").value = '<%=strManageCom%>';
        if(fm.all("ManageCom").value==86)
        {
            fm.all("ManageCom").readOnly=false;
        }
        else
        {
            fm.all("ManageCom").readOnly==true;
        }
        if(fm.all("ManageCom").value!==null)
        {
            var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all("ManageCom").value+"'");
            //显示代码选择中文
            if (arrResult != null) 
            {
                fm.all("ManageComName").value=arrResult[0][0];
            }
        }
    }
    catch(ex)
    {
        alert("在GrpContAgentsaleUPInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

  //保单信息列表的初始化
 function initSetGrid(){   
 var iArray = new Array();
   
 try
 {
	 iArray[0]=new Array();
	 iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	 iArray[0][1]="30px";            		//列宽
	 iArray[0][2]=10;            			//列最大值

	 iArray[1]=new Array();
	 iArray[1][0]="集体合同号码";         	//列名
	 iArray[1][1]="40px";            		//列宽
	 iArray[1][2]=100;            			//列最大值

	 iArray[2]=new Array();
	 iArray[2][0]="印刷号";         			//列名
	 iArray[2][1]="82px";            		//列宽
	 iArray[2][2]=100;            			//列最大值
   
	 iArray[3]=new Array();
	 iArray[3][0]="销售渠道 ";         	    //列名
	 iArray[3][1]="70px";            		//列宽
	 iArray[3][2]=100;            			//列最大值
   
	 iArray[4]=new Array();
	 iArray[4][0]="代理机构";         		//列名
	 iArray[4][1]="60px";            		//列宽
	 iArray[4][2]=100;            			//列最大值      
   
	 iArray[5]=new Array();
	 iArray[5][0]="代理销售业务员编码";		//列名
	 iArray[5][1]="70px";                   //列宽
	 iArray[5][2]=100;            			//列最大值
   
     SetGrid = new MulLineEnter( "fm" , "SetGrid" ); 
     SetGrid.mulLineCount = 10;   
     SetGrid.displayTitle = 1;
     SetGrid.locked = 1;
     SetGrid.canChk = 0;
     SetGrid.canSel = 1;
     SetGrid.hiddenPlus = 1;
     SetGrid.hiddenSubtraction = 1;
     SetGrid.loadMulLine(iArray); 
 }
 catch(ex)
 {
     alert(ex);
 }
} 
  
</script>
