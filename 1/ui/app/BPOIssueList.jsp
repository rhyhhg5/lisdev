
<table>
	<tr>
    	<td class=common>
	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBPOIssue);">
		</td>
		<td class= titleImg>
			 外包反馈错误信息
		</td>
	</tr>
</table>
<Div  id= "divBPOIssue" style= "display: ''" align=center>
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanIssueGrid" >
				</span> 
		  	</td>
		</tr>
	</table>
</Div>
<Div id= "divPage" align=center style= "display: 'none' ">
<INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPageX.firstPage();"> 
<INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPageX.previousPage();"> 					
<INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPageX.nextPage();"> 
<INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPageX.lastPage();"> 
</Div>


<script language="JavaScript">               

var turnPageX = new turnPageClass();

// 错误信息列表的初始化
function initIssueGrid()
{                        
  var iArray = new Array();
    
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=20;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="印刷号";         		//列名
    iArray[1][1]="70px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="对象";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=80;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="名称";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=80;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许


    iArray[4]=new Array();
    iArray[4][0]="错误项";         		//列名
    iArray[4][1]="100px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[5]=new Array();
    iArray[5][0]="错误类型";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=120;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

    iArray[6]=new Array();                                                  
    iArray[6][0]="原内容";         		//列名                        
    iArray[6][1]="120px";            		//列宽                                
    iArray[6][2]=200;            			//列最大值                            
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

    iArray[7]=new Array();                                                  
    iArray[7][0]="备注";         		//列名                        
    iArray[7][1]="200px";            		//列宽                                
    iArray[7][2]=200;            			//列最大值                            
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

    IssueGrid = new MulLineEnter( "fm" , "IssueGrid" ); 
    //这些属性必须在loadMulLine前
    IssueGrid.mulLineCount = 0;   
    IssueGrid.displayTitle = 1;
    IssueGrid.locked = 1;
    IssueGrid.canSel = 0;
    IssueGrid.canChk = 0;
    IssueGrid.hiddenPlus=1;   
  	IssueGrid.hiddenSubtraction=1;
    IssueGrid.loadMulLine(iArray);  
    
    
    //这些操作必须在loadMulLine后面
    //IssueGrid.setRowColData(1,1,"asdf");
  }
  catch(ex)
  {
    alert(ex.message);
  }
  queryIssueList();
}

function queryIssueList()
{
  var strSQL = "select distinct b.PrtNo, '投保人', "
            + "   (select Name from BPOLCAppnt where BPOBatchNo = a.BPOBatchNo and ContID = a.ContID), "
            + "   CodeName('bpodatafield', a.IssueField), CodeName('bpoissuetype', a.IssueType), "
            + "   a.OriginalInfo, a.Remark "
            + "from BPOIssue a, BPOLCPol b "
            + "where a.BPOBatchNo = b.BPOBatchNo "
            + "   and a.ContID = b.ContID "
            + "   and b.PrtNo = '" + fm.PrtNo.value + "' "
            + "   and a.IssueObject = '01' "
            + "union all "
            + "select distinct b.PrtNo, '被保人', "
            + "   (select Name from BPOLCInsured where BPOBatchNo = a.BPOBatchNo and ContID = a.ContID and InsuredID = a.IssueObjectID), "
            + "   CodeName('bpodatafield', a.IssueField), CodeName('bpoissuetype', a.IssueType), "
            + "   a.OriginalInfo, a.Remark "
            + "from BPOIssue a, BPOLCPol b "
            + "where a.BPOBatchNo = b.BPOBatchNo "
            + "   and a.ContID = b.ContID "
            + "   and b.PrtNo = '" + fm.PrtNo.value + "' "
            + "   and a.IssueObject = '02' ";
	
	turnPageX.queryModal(strSQL, IssueGrid);
}

</script>

