<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：AppUWSXAnalysis.jsp
//程序功能：F1报表生成
//创建日期：2007-09-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="AppUWSXAnalysis.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./AppUWSXAnalysisSave.jsp" method=post name=fm target="fraSubmit">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input><Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>
          <TD  class= title>销售渠道</TD>
	        <TD  class= input><Input class=codeNo name=SaleChnl ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary></TD>          
        </TR>
       <TR  class= common>
		  <TD  class= title>保单自核提交日期起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>保单自核提交日期止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD>        	
      </TR>
      <TR class= common>
      	<TD class=title>保单签发起期</TD>
      	<TD class=input>
      		<input name=SignStartDate class=coolDatePicker dateFormat=short verify="保单签发起期|Date">
      	</TD>
      	<TD class=title>保单签发止期</TD>
      	<TD class=input>
      		<input name=SignEndDate class=coolDatePicker dateFormat=short verify="保单签发止期|Date">
      	</TD>
      </TR>
    </table>
	  <input name= "AnalysisSql" type=hidden>
    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
	<hr>
	
	    <div>
        <font color="red">
            <ul>查询字段说明：
                <li>统计起止期：保单自核提交日期（即复核完毕日期）的起止期。</li>                
            </ul>
            <ul>统计结果说明：
                <li>自核提交日期：保单复核完毕提交自核时间</li>
                <li>核保完成日期：保单核保完毕时间</li>
                <li>核保时效：保单复核完毕至核保完毕时间的差值</li>
                <li>备注：该报表仅统计个险标准平台，其他平台不统计</li>
            </ul>
        </font>
    </div>
	
	
	
<%--	<table>--%>
<%--	<TR>--%>
<%--		<TD><IMG src="../common/images/butExpand.gif"--%>
<%--			style="cursor:hand;" OnClick="showPage(this,divCare1);"></TD>--%>
<%--		<TD class=titleImg>查询字段说明：</TD>--%>
<%--	</TR>--%>
<%--    </table>--%>
<%--    <Div id="divCare1" style="display: ''">--%>
<%--    <tr class="common">--%>
<%--	   <td class="title">统计起止期：保单自核提交日期（即复核完毕日期）的起止期</td>--%>
<%--    </tr>--%>
<%--    <br>--%>
<%--    </Div>--%>
<%--<br><br>--%>
<%--	<table>--%>
<%--	<TR>--%>
<%--		<TD><IMG src="../common/images/butExpand.gif"--%>
<%--			style="cursor:hand;" OnClick="showPage(this,divCare2);"></TD>--%>
<%--		<TD class=titleImg>统计结果说明：</TD>--%>
<%--	</TR>--%>
<%--    </table>--%>
<%--    <Div id="divCare2" style="display: ''">--%>
<%--    <tr class="common"><td class="title">自核提交日期：保单复核完毕提交自核时间</td></tr><br>--%>
<%--    <tr class="common"><td class="title">核保完成日期：保单人工核保完毕时间</td></tr><br>--%>
<%--    <tr class="common"><td class="title">核保时效：保单复核完毕至人工核保完毕时间的差值</td></tr><br>--%>
<%--    <tr class="common"><td class="title">备注：该报表仅统计个险标准平台，其他平台不统计</td></tr><br>--%>
<%--    <br>--%>
<%--    </Div> 		--%>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 