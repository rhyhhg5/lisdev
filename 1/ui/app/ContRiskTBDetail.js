var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartMakeDate = fm.StartMakeDate.value;
    var tEndMakeDate = fm.EndMakeDate.value;

	if(dateDiff(tStartMakeDate, tEndMakeDate, "M") > 3)
	{
		alert("统计期最多为三个月！");
		return false;
	}
	var tContainsPAD="";
	if(fm.ContainsPAD.value=="" || fm.ContainsPAD.value =="0"){
		tContainsPAD="";
	}else if(fm.ContainsPAD.value == "1"){
		tContainsPAD=" and lcc.prtno like 'PD%' ";
	}else if(fm.ContainsPAD.value == "2"){
		tContainsPAD=" and lcc.prtno not like 'PD%' ";
	}

    var tStrSQL = ""
        + " select "
        + " tmp.ManageCom, tmp.SaleChnl, tmp.BranchAttr, tmp.Name, getUniteCode(tmp.AgentCode), "
        + " tmp.AgentName, tmp.ContNo, tmp.PrtNo, tmp.AppntName, tmp.AppntSex, "
        + " tmp.AppntBirthday, tmp.AppntMobile, tmp.AppntPhone, "
        + " tmp.InsuredName, tmp.InsuredSex, "
        + " tmp.InsuredBirthday, tmp.InsuredAppAge, tmp.RiskType3, "
        + " tmp.RiskType4, tmp.RiskCode, tmp.RiskName, tmp.PayIntv, tmp.PayEndYear, tmp.PayEndYearFlag, "
        + " tmp.EndDate, tmp.PayEndDate, tmp.InsuYear, tmp.InsuYearFlag, "
        + " tmp.Mult, tmp.Amnt, tmp.Prem, tmp.SupplementaryPrem, "
        + " tmp.PolApplyDate, tmp.CValidate, tmp.FirstTrialOperator, "
        + " tmp.InputDate, tmp.InputOperator, tmp.ApproveDate, tmp.ApproveCode, "
        + " tmp.UWCode, tmp.UWFlag, tmp.UWDate, tmp.PayMode, "
        + " tmp.FirstPayDate, tmp.SignDate, tmp.ContAppFlag ,tmp.docmakedate"
        + " from "
        + " ("
        + " select "
        + " lcc.ManageCom, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " labg.BranchAttr, labg.Name, lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " lcc.AppntName, "
        + " CodeName('sex', lcc.AppntSex) AppntSex, "
        + " lcc.AppntBirthday, "
        + " (select lcar.Mobile from LCAddress lcar inner join LCAppnt lca on lca.AppntNo = lcar.CustomerNo and lca.AddressNo = lcar.AddressNo where lca.ContNo = lcc.ContNo) AppntMobile, "
        + " (select lcar.Phone from LCAddress lcar inner join LCAppnt lca on lca.AppntNo = lcar.CustomerNo and lca.AddressNo = lcar.AddressNo where lca.ContNo = lcc.ContNo) AppntPhone, "
        + " lcp.InsuredName, "
        + " CodeName('sex', lcp.InsuredSex) InsuredSex, "
        + " lcp.InsuredBirthday, lcp.InsuredAppAge, "
        + " CodeName('risktype3', lmra.RiskType3) RiskType3, "
        + " CodeName('risktype4', lmra.RiskType4) RiskType4, "
        + " lcp.RiskCode, lmra.RiskName, "
        + " CodeName('payintv', lcp.PayIntv) PayIntv, "
        + " lcp.PayEndYear, lcp.PayEndYearFlag, "
        + " lcp.EndDate, lcp.PayEndDate, lcp.InsuYear, lcp.InsuYearFlag, "
        + " lcp.Mult, lcp.Amnt, lcp.Prem, lcp.SupplementaryPrem, "
        + " lcc.PolApplyDate, lcp.CValidate, lcc.FirstTrialOperator, "
        + " lcc.InputDate, lcc.InputOperator, lcc.ApproveDate, lcc.ApproveCode, "
        + " lcp.UWCode, "
        + " CodeName('uwflag', lcp.UWFlag) UWFlag, "
        + " lcp.UWDate, "
        + " CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.FirstPayDate, lcp.SignDate, "
        + " (case lcc.AppFlag when '1' then '承保' else '投保' end) ContAppFlag, "
        + " (select es.makedate from es_doc_main es where es.doccode = lcc.prtno order by es.makedate desc fetch first 1 rows only) docmakedate, "
        + " '' "
        + " from LCPol lcp "
        + " inner join LCCont lcc on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
        + " where 1 = 1 "
        + " and lcp.ContType = '1' "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + tContainsPAD
        + " union all "
        + " select "
        + " lcc.ManageCom, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " labg.BranchAttr, labg.Name, lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " lcc.AppntName, "
        + " CodeName('sex', lcc.AppntSex) AppntSex, "
        + " lcc.AppntBirthday, "
        + " (select lcar.Mobile from LCAddress lcar inner join LCAppnt lca on lca.AppntNo = lcar.CustomerNo and lca.AddressNo = lcar.AddressNo where lca.ContNo = lcc.ContNo) AppntMobile, "
        + " (select lcar.Phone from LCAddress lcar inner join LCAppnt lca on lca.AppntNo = lcar.CustomerNo and lca.AddressNo = lcar.AddressNo where lca.ContNo = lcc.ContNo) AppntPhone, "
        + " lcp.InsuredName, "
        + " CodeName('sex', lcp.InsuredSex) InsuredSex, "
        + " lcp.InsuredBirthday, lcp.InsuredAppAge, "
        + " CodeName('risktype3', lmra.RiskType3) RiskType3, "
        + " CodeName('risktype4', lmra.RiskType4) RiskType4, "
        + " lcp.RiskCode, lmra.RiskName, "
        + " CodeName('payintv', lcp.PayIntv) PayIntv, "
        + " lcp.PayEndYear, lcp.PayEndYearFlag, "
        + " lcp.EndDate, lcp.PayEndDate, lcp.InsuYear, lcp.InsuYearFlag, "
        + " lcp.Mult, lcp.Amnt, lcp.Prem, lcp.SupplementaryPrem, "
        + " lcc.PolApplyDate, lcp.CValidate, lcc.FirstTrialOperator, "
        + " lcc.InputDate, lcc.InputOperator, lcc.ApproveDate, lcc.ApproveCode, "
        + " lcp.UWCode, "
        + " CodeName('uwflag', lcp.UWFlag) UWFlag, "
        + " lcp.UWDate, "
        + " CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.FirstPayDate, lcp.SignDate, "
        + " '退保' ContAppFlag, "
        + " (select esa.makedate from es_doc_main esa where esa.doccode = lcc.prtno order by esa.makedate desc fetch first 1 rows only) docmakedate, "
        + " '' "
        + " from LBPol lcp "
        + " inner join LCCont lcc on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
        + " where 1 = 1 "
        + " and lcp.ContType = '1' "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + tContainsPAD
        + " union all "
        + " select "
        + " lcc.ManageCom, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " labg.BranchAttr, labg.Name, lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " lcc.AppntName, "
        + " CodeName('sex', lcc.AppntSex) AppntSex, "
        + " lcc.AppntBirthday, "
        + " (select lcar.Mobile from LCAddress lcar inner join LBAppnt lca on lca.AppntNo = lcar.CustomerNo and lca.AddressNo = lcar.AddressNo where lca.ContNo = lcc.ContNo) AppntMobile, "
        + " (select lcar.Phone from LCAddress lcar inner join LBAppnt lca on lca.AppntNo = lcar.CustomerNo and lca.AddressNo = lcar.AddressNo where lca.ContNo = lcc.ContNo) AppntPhone, "
        + " lcp.InsuredName, "
        + " CodeName('sex', lcp.InsuredSex) InsuredSex, "
        + " lcp.InsuredBirthday, lcp.InsuredAppAge, "
        + " CodeName('risktype3', lmra.RiskType3) RiskType3, "
        + " CodeName('risktype4', lmra.RiskType4) RiskType4, "
        + " lcp.RiskCode, lmra.RiskName, "
        + " CodeName('payintv', lcp.PayIntv) PayIntv, "
        + " lcp.PayEndYear, lcp.PayEndYearFlag, "
        + " lcp.EndDate, lcp.PayEndDate, lcp.InsuYear, lcp.InsuYearFlag, "
        + " lcp.Mult, lcp.Amnt, lcp.Prem, lcp.SupplementaryPrem, "
        + " lcc.PolApplyDate, lcp.CValidate, lcc.FirstTrialOperator, "
        + " lcc.InputDate, lcc.InputOperator, lcc.ApproveDate, lcc.ApproveCode, "
        + " lcp.UWCode, "
        + " CodeName('uwflag', lcp.UWFlag) UWFlag, "
        + " lcp.UWDate, "
        + " CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.FirstPayDate, lcp.SignDate, "
        + " (select (case lpei.EdorType when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' end) from LPEdorItem lpei where lpei.ContNo = lcc.ContNo and lpei.EdorType in ('CT', 'WT', 'XT') order by lpei.EdorAppDate desc fetch first rows only) ContAppFlag, "
        + " (select esb.makedate from es_doc_main esb where esb.doccode = lcc.prtno order by esb.makedate desc fetch first 1 rows only) docmakedate, "
        + " '' "
        + " from LBPol lcp "
        + " inner join LBCont lcc on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
        + " where 1 = 1 "
        + " and lcp.ContType = '1' "
        + " and lcc.EdorNo not like 'xb%' "
        + " and lcp.EdorNo not like 'xb%' "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + tContainsPAD
        + " ) as tmp "
        + " order by tmp.ManageCom, tmp.PrtNo, tmp.ContNo "
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "ContRiskTBDetailSave.jsp";
    fm.submit();
    fm.action = oldAction;

}