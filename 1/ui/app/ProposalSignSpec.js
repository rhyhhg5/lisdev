//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var arrDataSet;

//提交，保存按钮对应操作
function signPol()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.all("signbutton").disabled=true;
  //showSubmitFrame(mDebug);
  fm.submit(); //提交 
}


//缴费催办查询__暂时注销
function UrgPayQuery()
{ 
  var checkedRowNum = 0 ;
  var rowNum = PolGrid. mulLineCount ; 
  var count = -1;
  for ( var i = 0 ; i< rowNum ; i++ )
  { 
  	if( checkedRowNum > 1)
      break;
  	if(PolGrid.getChkNo(i)) {
  	   checkedRowNum = checkedRowNum + 1;
       count = i;
      }
  }  
  if(checkedRowNum == 1)
  {
  	var cProposalNo = PolGrid.getRowColData(count,1,PolGrid);//投保单号
  	//alert(cProposalNo);
  	window.open("../uw/OutTimeQueryMain.jsp?ProposalNo1="+cProposalNo);
  }
  if(checkedRowNum == 2 || checkedRowNum == 0)
  {			
	alert("请只选择一条投保单进行缴费催办查询!");  	
  }
  
}

//发缴费催办通知书_暂时注销,前台及后台程序均已实现.只是报表未描述.待民生提出需求后在启用
function SendUrgPay()
{
  var checkedRowNum = 0 ;
  var rowNum = PolGrid. mulLineCount ; 
  var count = -1;
  for ( var i = 0 ; i< rowNum ; i++ )
  { 
  	if( checkedRowNum > 1)
      break;
  	if(PolGrid.getChkNo(i)) {
  	   checkedRowNum = checkedRowNum + 1;
       count = i;
      }
  } 
  if(checkedRowNum == 1)
  {
  	var cProposalNo = PolGrid.getRowColData(count,1,PolGrid);//投保单号
        cOtherNoType="00"; //个人投保单主险号码类型
        cCode="15";        //缴费催办通知书单据类型  
   if (cProposalNo != ""){
	  	showModalDialog("../uw/UWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");	  
	  }
  }
  if(checkedRowNum == 2 || checkedRowNum == 0)
  {			
	alert("请只选择一条投保单进行发缴费催办通知书!");  	
  }
  
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.all("signbutton").disabled=false;
  if (FlagStr == "Fail" )
  {   
	//if(content.length>1480)
	//{
	  //content="后台反馈的提示信息过多，已超过最大显示范围！请减少提交纪录数量(建议18条数据以下)";	
	//} 
	 alert(content);
	//var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ; 
    //showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:750px;dialogHeight:450px");   
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:800px;dialogHeight:450px");   
  }
  else
  { 
   alert(content);
    //执行下一步操作
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{
  initPolGrid();
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	// 书写SQL语句
  var strSql = "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName,UWDate from LCPol  where 1=1 "
				 + " and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+comcode+"','Ph')=1 "
				 + "and AppFlag='0' "						// 投保状态				
				 + "and ApproveFlag='9' "					// 已经复核
				 + "and UWFlag in ('9','4') "			    // 核保通过
				 + "and GrpPolNo='00000000000000000000' "	// 个人投保单
				 + "and ContNo='00000000000000000000' "		// 个人投保单
				 + getWherePart( 'ProposalNo' )
				 + getWherePart( 'ManageCom','ManageCom', 'like' )
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'PrtNo' )
				 + "and PrtNo in (select PrtNo from lcpol where  1=1 " 			
				 + "and ApproveFlag='9' "					// 已经复核
				 + "and UWFlag in ('9','4') "			    // 核保通过
				 + "and GrpPolNo='00000000000000000000' "	// 个人投保单
				 + "and ContNo='00000000000000000000' "		// 个人投保单
				 + getWherePart( 'ManageCom','ManageCom', 'like' )
				 + getWherePart( 'AgentCode' )
				 + " and PolNo=MainPolNo )"	 
				 //+ " and PolNo=MainPolNo "  //主险
				 + " and ManageCom like '" + comcode + "%%'"
				 + " Order by UWDate";

	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	showInfo.close();
    alert("未查询到满足条件的数据！");
     return false;
  }
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  turnPage.pageLineNum = 20 ;
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql 
  
  
  //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,MAXSCREENLINES);
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  showInfo.close();
  //showCodeName();
}

function easyQueryClick2()
{
  initPolGrid();
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	// 书写SQL语句
  var strSql = "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName,UWDate from LCPol where 1=1 "
				 + " and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+comcode+"','Ph')=1 "
				 + "and AppFlag='0' "						// 投保状态				
				 + "and ApproveFlag='9' "					// 已经复核
				 + "and UWFlag in ('9','4') "			    // 核保通过
				 + "and GrpPolNo='00000000000000000000' "	// 个人投保单
				 + "and ContNo='00000000000000000000' "		// 个人投保单
				 + getWherePart( 'ProposalNo' )
				 + getWherePart( 'ManageCom','ManageCom', 'like' )
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'PrtNo' )
				 + "and PrtNo in (select PrtNo from lcpol where  1=1 " 			
				 + "and ApproveFlag='9' "					// 已经复核
				 + "and UWFlag in ('9','4') "			    // 核保通过
				 + "and GrpPolNo='00000000000000000000' "	// 个人投保单
				 + "and ContNo='00000000000000000000' "		// 个人投保单
				 + getWherePart( 'ManageCom','ManageCom', 'like' )
				 + getWherePart( 'AgentCode' )
				 + " and PolNo=MainPolNo )"	 
				 //+ " and PolNo=MainPolNo "  //主险
				 + " and ManageCom like '" + comcode + "%%'"
				 + " Order by UWDate";

//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	showInfo.close();
    alert("未查询到满足条件的数据！");
     return false;
  }
  
  //设置查询起始位置
  //turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  turnPage.pageLineNum = 20 ;
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql 
  
  
  //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,MAXSCREENLINES);
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  showInfo.close();
  //showCodeName();
}

