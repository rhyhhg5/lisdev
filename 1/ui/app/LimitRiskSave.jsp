<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>


<!--用户校验类-->
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.lis.schema.*"%>
  
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%--
  <%@page import="com.sinosoft.lis.config.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  --%>
<% 
	
	String strOperation = request.getParameter("fmtransact");//操作類型
	System.out.println("操作类型是-----------------"+strOperation);
	
	String content = "";
	String FlagStr = "";
	String mDescType = "";
	LDCode1Schema tLDCode1Schema   = new LDCode1Schema();
	LimitRiskUI tLimitRiskUI = new LimitRiskUI();
	CErrors tError = null;
	
	if(strOperation.equals("INSERT"))
  	{		
		tLDCode1Schema.setCodeType("limitrisk");
  		tLDCode1Schema.setCode1(request.getParameter("RiskCode"));//险种编码
		tLDCode1Schema.setCodeName(request.getParameter("SaleChnl"));//险种名称
		tLDCode1Schema.setCodeAlias("");
		tLDCode1Schema.setComCode("");
		tLDCode1Schema.setRiskWrapPlanName("");
    	mDescType = "新增";
  	}
  	if(strOperation.equals("INSERTMANAGECOM"))
  	{	
		tLDCode1Schema.setCodeType("limitrisk1");
  		tLDCode1Schema.setCode1(request.getParameter("ManageCom"));//险种编码
		tLDCode1Schema.setCodeName(request.getParameter("RiskCode"));//险种名称
		tLDCode1Schema.setCodeAlias(request.getParameter("SaleChnl"));
		tLDCode1Schema.setComCode("");
		tLDCode1Schema.setRiskWrapPlanName("");
    	mDescType = "新增管理机构";
  	}
  	if(strOperation.equals("DELETE"))
  	{	
  	   tLDCode1Schema.setCode1(request.getParameter("RiskCode"));//险种编码
	   tLDCode1Schema.setCodeName(request.getParameter("SaleChnl"));//险种名称
	   tLDCode1Schema.setCodeType("limitrisk");
   	   mDescType = "删除";
 	}
  	if(strOperation.equals("DELETEMANAGECOM"))
  	{	
  	   tLDCode1Schema.setCode1(request.getParameter("ManageCom"));//险种编码
	   tLDCode1Schema.setCodeName(request.getParameter("RiskCode"));//险种名称
	   tLDCode1Schema.setCodeAlias(request.getParameter("SaleChnl"));//险种名称	   
	   tLDCode1Schema.setCodeType("limitrisk1");
   	   mDescType = "删除管理机构";
 	}
 	VData tVData = new VData();
 	LDCode1Set tLDCode1Set = new LDCode1Set();
 	tLDCode1Set.add(tLDCode1Schema);
 	
	try{
		tVData.addElement(strOperation);
		tVData.addElement(tLDCode1Set);
		tLimitRiskUI.submitData(tVData,strOperation);
	}catch(Exception ex){
		content = mDescType+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	System.out.println("end"+mDescType);

  	if (FlagStr=="")
  	{
    	tError = tLimitRiskUI.mErrors;
    	
    if (!tError.needDealError())
    {
      	content = mDescType+"成功";
    	FlagStr = "Succ";
    }
    else
    {
    	content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	System.out.println("失败，原因是:"+content);
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=content%>");
</script>
</html>