<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
System.out.println("TaxEIPCheckSave.jsp Begin ...");

GlobalInput tG = (GlobalInput)session.getValue("GI");
String FlagStr = "Fail";
String Content = "";
String tBatchNo = request.getParameter("BatchNo");
String tOperate = request.getParameter("fmOperatorFlag");
//只有批次号不为空才想后台传送数据
if(tBatchNo!=null){
	try{
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BatchNo",tBatchNo);
		tVData.add(tG);
		tVData.add(tTransferData);
		
		TaxEIPCheckUI tTaxEIPCheckUI = new TaxEIPCheckUI();
		if(!tTaxEIPCheckUI.submitData(tVData,tOperate)){
			Content = "数据处理失败，原因是："+tTaxEIPCheckUI.mErrors.getLastError();
			FlagStr = "Fail";
		}else{
			Content = "数据处理成功！";
			FlagStr = "Succ";
		}
	}catch(Exception e){
		Content = "出现异常，处理失败！";
		FlagStr = "Succ";
		e.printStackTrace();
	}
}else{
	FlagStr = "Fail";
	Content = "没有获得前台传来的批次号信息！";
}


System.out.println("TaxEIPCheckSave.jsp End ...");
%>

<html>
	<script language="javascript">
		parent.fraInterface.afterConfirmImport("<%=FlagStr%>", "<%=Content%>", "<%=tBatchNo%>");
	</script>
</html>