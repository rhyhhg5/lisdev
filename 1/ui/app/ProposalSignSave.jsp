<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ProposalSignSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Succ";
	String Content = "";

	try
	{
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
		String tActivityID = request.getParameter("ActivityID");
	  
	  	//接收信息
	  	// 投保单列表
		LCPolSet tLCPolSet = new LCPolSet();
	
		String tProposalNo[] = request.getParameterValues("PolGrid1");
		String tPtrNo[] = request.getParameterValues("PolGrid2");
		String tChk[] = request.getParameterValues("InpPolGridChk");
		String tMissionID[] = request.getParameterValues("PolGrid6");   
		String tSubMissionID[] = request.getParameterValues("PolGrid7");         
		boolean flag = false;
		int proposalCount = tProposalNo.length;
	
		for (int i = 0; i < proposalCount; i++)
		{
		  LCContSet tLCContSet = new LCContSet();
		  TransferData tTransferData = new TransferData();
		  
		 	flag = false;
			if (tProposalNo[i] != null && tChk[i].equals("1"))
			{
				System.out.println("select contno:" +  tProposalNo[i]);
			 	LCContSchema tLCContSchema = new LCContSchema ();
			 	tLCContSchema.setContNo( tProposalNo[i] );
			 	tLCContSchema.setPrtNo( tPtrNo[i] );
	   		tLCContSet.add( tLCContSchema );
	   		
	   		tTransferData.setNameAndValue("MissionID",tMissionID[i] );		   		
     		tTransferData.setNameAndValue("SubMissionID",tSubMissionID[i] );
     		System.out.println("subMissionId:" +  tSubMissionID[i]);
   			//tTransferData.setNameAndValue("LCContSet",tLCContSet );
     
		    flag = true;
			}
		
		
	  	if (flag == true)
	  	{
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add( tG );
			tVData.add( tLCContSet );
			tVData.add( tTransferData );
			
		
			TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
			if ( tTbWorkFlowUI.submitData( tVData, tActivityID)== false )
			{
				System.out.println("Content>>>>>>0:" + Content );
				Content = "投保单签单失败 " ;
				FlagStr = "Fail";
			}
			
			int n = tTbWorkFlowUI.mErrors.getErrorCount();
			System.out.println("flag==="+FlagStr);
			if( n == 0 && FlagStr.equals("Succ"))
			{    
		              
		    	Content = "签单成功！";
			   	FlagStr = "Succ";			   	
			   	
		    }
		    else
		    {
		       
				String strErr = "";
				for (int t = 0; t < n; t++)
				{
					strErr += (t+1) + ": " + tTbWorkFlowUI.mErrors.getError(t).errorMessage + "; ";
				}
				if ( FlagStr.equals("Succ"))
				{
					Content = "部分投保单签单失败，原因是: " + strErr;
					FlagStr = "Fail";
				}
				else
				{
				  Content +="可能有如下原因:"+ strErr;
				}
				
				
			} // end of if
		} // end of if

		}  //end for contset 
	} // end of try
	catch ( Exception e1 )
	{
	 e1.printStackTrace();
		Content = "投保单签单失败，原因是: " + e1.toString();
		FlagStr = "Fail";
	}
%>                      
<html>
<script language="javascript">
    
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");


</script>
</html>
