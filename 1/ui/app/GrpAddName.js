//               该文件中包含客户端需要处理的函数和事件
var showInfo;

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function easyQueryClick() {
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSql = "select PrtNo, GrpContNo,ContNo, Peoples,Prem, Amnt from LCCont where  appflag='1' and poltype='1' "
    		+ getWherePart( 'PrtNo' )
		+ getWherePart( 'GrpContNo' )
		+ getWherePart( 'ContNo' );			 
  //alert(strSql);
	turnPage.queryModal(strSql, PolGrid);
	
}

//var prtNo = "";
function addNameClick() { 
  if (PolGrid.getSelNo()!=0) { 
  	var PrtNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1); 	
  	var GrpContNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2); 	
  	var ContNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3); 	
    sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    window.open("./GrpAddNameGroupPolInputMain.jsp?LoadFlag=9&PrtNo="+PrtNo+"&polNo="+GrpContNo+"&oldContNo="+ContNo, "", sFeatures);        
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
  
  //initQuery();
}
function addNameInClick() { 
  if (PolGrid.getSelNo()!=0) { 
  	var PrtNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1); 	
  	var GrpContNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2); 	
  	var ContNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3); 	
  	/*
  	if(!checkenough(ContNo))
  	{
  		return false;
  	}
  	*/
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  	
	fm.action = "./GrpAddNameSave.jsp?LoadFlag=9&PrtNo="+PrtNo+"&GrpContNo="+GrpContNo+"&ContNo="+ContNo;
	fm.submit();      
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
  
  //initQuery();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  { 
		content = "处理成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}
function checkenough(ContNo)
//这个校验只校验了数量，没有校验“质量“，暂时如此
{
    var strSql1 = " select count(*) from lcpol where appflag='4' and MasterPolNo in (select  proposalNo from lcpol where Contno='" + ContNo+"')";    
    var arrResult1 = easyExecSql(strSql1);
    var strSql2 = " select count(*) from lcpol where  Contno='" + ContNo+"'";    
    var arrResult2 = easyExecSql(strSql2);    
    if(arrResult1[0][0]!=arrResult2[0][0])
    {
    	alert("补名单的险种与原无名单所录险种有差异,不能签单,请确认!");
    	return false;
    }
    else
    {
    	return true;
    }
    	
}

