//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var QueryResult="";
var QueryCount = 0;
var mulLineCount = 0;
var QueryWhere="";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

window.onfocus=myonfocus;

function addClick()
{
	var row=0;	
	row = GrpPayPlanGrid.mulLineCount-1;	
	GrpPayPlanGrid.setRowColData(row,1,''+(parseInt(row)+1));
}

function getGrpPayPlan()
{
	var tSql = "select distinct '第'||plancode||'期',paytodate from lcgrppayplan where proposalgrpcontno = '"+ProposalGrpContNo+"' ";
	turnPage.pageLineNum = 100;
	turnPage.queryModal(tSql, GrpPayPlanGrid);
}


function ShowGrpPayPlanDetail()
{
	
	var tPlanCode = GrpPayPlanGrid.getSelNo();
	//alert(tPlanCode);
    var tStrSql = "select plancode,paytodate,contplancode, prem from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"' and plancode = '"+tPlanCode+"' order by contplancode " ;
    //prompt('',tStrSql);
    turnPage1.pageLineNum = 50;
    turnPage1.pageDivName = "divGrpPayPlanGrid";
    turnPage1.queryModal(tStrSql, GrpPayPlanDetailGrid);

    if (!turnPage1.strQueryResult)
    {
        alert("没有获取该单的约定缴费计划信息！");
        return false;
    }
    //fm.querySql.value = tStrSql;
    return true;
}

function Submit()
{
	if(!beforeSub()){
    	return false;
    }
    fm.all('fmtransact').value = "INSERT";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}

function beforeSub()
{
	var tPaySQL = "select 1 from lcgrppayplan where ProposalGrpContNo = '"+ProposalGrpContNo+"' ";
	var PayArr = easyExecSql(tPaySQL);
	if(PayArr)
	{
		alert("该单已录入约定缴费计划，不可再次保存！");
		return false;	
	}
	var tSQL = "select prtno from lcgrpcont where ProposalGrpContNo = '"+ProposalGrpContNo+"' ";
	var arr = easyExecSql(tSQL);
	if(!arr)
	{
		alert("获取印刷号失败！");
		return false;
	}
	var tSpecSQL = "select 1 from lcgrppol where prtno = '"+arr[0][0]+"' and riskcode in (select riskcode from lmriskapp where risktype3 = '7' or risktype4 = '4')  ";
	var tSpecArr = easyExecSql(tSpecSQL);
	if(tSpecArr)
	{
		alert("含有特需险或万能险种，不可添加约定缴费计划！");
		return false;
	}
	var lineCount = 0;
	lineCount = GrpPayPlanGrid.mulLineCount;
	if(lineCount==0){
		alert("请填写缴费计划！");
		return false;
	}
	for(var i=0;i<lineCount;i++){
		if(GrpPayPlanGrid.getRowColData(i,2) == ""){
			alert("第'"+(i+1)+"'行，约定缴费时间为空！");
           return false;
		}
		if(i<(lineCount-1)){
			var zw = compareDate(GrpPayPlanGrid.getRowColData(i,2),GrpPayPlanGrid.getRowColData(i+1,2));
			if(zw != 2){
				alert("第"+(i+2)+"期的约定缴费时间应晚于第"+(i+1)+"期的约定缴费时间!");
				return false;
			}
		}
	}
	return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		getGrpPayPlan();
		initGrpPayPlanDetailGrid();
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		getGrpPayPlan();
		initGrpPayPlanDetailGrid();
		//如果提交成功，则根据已有条件重新查询需要处理的数据
	}
}

function DelGrpPayPlan()
{
	if(!confirm("删除全部缴费计划，是否继续？"))
	{
		return false;
	}
	fm.all('fmtransact').value = "DELETE";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}

function SubmitDetail()
{
	if(!beforeSubDetail()){
    	return false;
    }
    fm.all('fmtransact').value = "INSERTDetail";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}

function beforeSubDetail()
{
	var lineCount = 0;
	lineCount = GrpPayPlanDetailGrid.mulLineCount;
	if(lineCount==0){
		alert("请填写缴费计划明细！");
		return false;
	}
	for(var i=0;i<lineCount;i++){
		if(isNaN(GrpPayPlanDetailGrid.getRowColData(i,4))){
			alert("第'"+(i+1)+"'行，约定缴费金额必须为数字！");
           return false;
		}		
	}
	return true;
}
