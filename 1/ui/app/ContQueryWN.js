//程序名称：ContQueryWN.js
//程序功能： 
//创建日期：2007-11-09
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
	{
		try 
		{
			showInfo.focus();
		}
		catch (ex) 
		{
			showInfo = null;
		}
	}
}

function easyQueryClick() 
{ 	
  if(!verifyInput2())
  {
      return false;
  }
  if(!checkDate())
  {
    return false;
  }
  
  var condition = "";
  //为避免 SQLSTATE 58004 错误，修改状态查询的 exists 为 in
  if(fm.State.value == "1")
  {
    condition = " and (b.PrtNo is null or b.PrtNo in (select MissionProp1 from LWMission where MissionProp1 = b.PrtNo and ActivityID = '0000009001')) ";
  }
  else if(fm.State.value == "2")
  {
    condition = " and b.PrtNo in (select MissionProp2 from LWMission where MissionProp2 = b.PrtNo and ActivityID = '0000009002') ";
  }
  else if(fm.State.value == "3")
  {
    condition = " and b.PrtNo in (select MissionProp2 from LWMission where MissionProp2 = b.PrtNo and ActivityID = '0000009004') and not exists(select 1 from LJTempFee where OtherNo = b.PrtNo and ConfMakeDate is not null) ";
  }
  else if(fm.State.value == "4")
  {
    condition = " and b.PrtNo in (select MissionProp2 from LWMission where MissionProp2 = b.PrtNo and ActivityID = '0000009004') and exists(select 1 from LJTempFee where OtherNo = b.PrtNo and ConfMakeDate is not null) ";
  }
  else if(fm.State.value == "5")
  {
    condition = " and b.PrtNo in (select MissionProp2 from LBMission where MissionProp2 = b.PrtNo and ActivityID = '0000009004') ";
  }
  
	var strSql = "select a.DocCode, b.ContNo, (select Name from LDCom where ComCode = a.ManageCom), "
            + "    CodeName('lcsalechnl', b.SaleChnl), "
            + "    b.AppntName, b.CValiDate, "
            + "    case when b.PrtNo is null then '待录入' "
            + "      when exists (select 1 from LWMission where MissionProp1 = b.PrtNo and ActivityID = '0000009001') then '待录入' "
            + "      when exists (select 1 from LWMission where MissionProp2 = b.PrtNo and ActivityID = '0000009002') then '待复核' "
            + "      when exists (select 1 from LWMission where MissionProp2 = b.PrtNo and ActivityID = '0000009004') and not exists(select 1 from LJTempFee where OtherNo = b.PrtNo and ConfMakeDate is not null) then '待收费' "
            + "      when exists (select 1 from LWMission where MissionProp2 = b.PrtNo and ActivityID = '0000009004') and exists(select 1 from LJTempFee where OtherNo = b.PrtNo and ConfMakeDate is not null) then '待签单' "
            + "      when exists (select 1 from LBMission where MissionProp2 = b.PrtNo and ActivityID = '0000009004') then '已签单' "
            + "    end, "
            + "    varchar(b.Prem), "
            + "    (select varchar(sum(InsuAccBala)) from LCInsureAcc where ContNo = b.ContNo) "
            + "from ES_Doc_Main a left join LCCont b "
            + "on a.DocCode = b.PrtNo "
            + "where a.SubType = 'TB04' "
            + "    and (b.ContType is null or b.ContType = '1') "
            + "    and a.ManageCom like '" + fm.ManageCom.value + "%' "
            + getWherePart('a.DocCode', 'PrtNo','=')
            + getWherePart('b.ContNo', 'ContNo','=')
            + getWherePart('b.AgentCom', 'AgentCom')
            + getWherePart('getUniteCode(b.AgentCode)', 'AgentCode')
            + getWherePart('b.SaleChnl', 'SaleChnl')
            + getWherePart('b.AppntName', 'AppntName')
            + getWherePart('b.CValiDate', 'CValiDate')
            + getWherePart('a.MakeDate', 'CValiDateStart','>=')
            + getWherePart('a.MakeDate', 'CValiDateEnd','<=')
            + condition
            + "order by a.DocCode ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, LCContGrid);
	return true;
}

function queryClick()
{
  if(!easyQueryClick())
      return false;
  if(LCContGrid.mulLineCount == 0)
  {
      alert("没有查询到数据");
      return false;
  }
}

function checkDate()
{
  var sql = "select 1 from Dual where date('" + fm.CValiDateEnd.value + "') > date('" + fm.CValiDateStart.value + "') + 3 month ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    if(!confirm("生效日期起止时间大于3个月！是否继续？"))
    {
      return false;
    }
  }
  
  return true;
}

//保单下载
function downloadCont()
{
    if(LCContGrid.mulLineCount == 0)
    {
        alert("没有需要下载的数据");
        return false;
    }
    fm.Sql.value = turnPage2.strQuerySql;
 	fm.action = "ContQueryWNDownLoad.jsp";
    fm.submit();
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
	cDiv.style.display="";
	}
	else
	{
	cDiv.style.display="none";  
	}
}
