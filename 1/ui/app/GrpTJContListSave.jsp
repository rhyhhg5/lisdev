<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContTBDetailSave.jsp
//程序功能：
//创建日期：2009-07-27
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);

  String querySql = request.getParameter("querySql");
  
  querySql = querySql.replaceAll("%25","%");
  
	//设置表头
	String[][] tTitle = {{"印刷号","投保单位名称","保单号","生效日期","终止日期","保险区间","保险区间单位","被保险人姓名","性别","身份证号","出生日期","所属保障计划","险种代码","单险种保费","人均总保费","理赔金转帐银行","户名","帐号"}};
	//表头的显示属性
	int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
    
  //数据的显示属性
  int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
   
  //生成文件
  CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
  createexcellist.createExcelFile();
  String[] sheetName ={"list"};
  createexcellist.addSheet(sheetName);
  int row = createexcellist.setData(tTitle,displayTitle);
  
  if(row ==-1) errorFlag = true;
  createexcellist.setRowColOffset(row+1,0);//设置偏移
  if(createexcellist.setData(querySql,displayData)==-1)
  {
  	errorFlag = true;
  	System.out.println(errorFlag);
  }
  if(!errorFlag)
  //写文件到磁盘
  try{
     createexcellist.write(tOutXmlPath);
  }catch(Exception e)
  {
  	errorFlag = true;
  	System.out.println(e);
  }
  //返回客户端
  if(!errorFlag)
  	downLoadFile(response,filePath,downLoadFileName);
  out.clear();
	out = pageContext.pushBody();
%>

