var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;

//查询税优团体客户资料
function QueryGrpInfoClick(){
	var tSQL = "select grpno,grpname,organcomcode,taxregistration,leagleperson,grpaddress "
			 + "from lsgrp "
			 + "where 1=1 "
			 + getWherePart('GrpNo','GrpNo')
			 + getWherePart('GrpName','GrpName')
			 + getWherePart('TaxRegistration','TaxRegistration')
			 + getWherePart('OrganComcode','OrganComcode')
			 + " order by grpno";
	turnPage.queryModal(tSQL,GrpInfoGrid);
}
//选中某一条团体客户记录时，将团体编号显示在'导入批次查询'块中的'团体编号'控件中
function initGrpNo(){
	var tSel = GrpInfoGrid.getSelNo();
	fm.GroupNo.value = GrpInfoGrid.getRowColData(tSel-1,1);
}
//为团体客户申请一个批次号
function Apply(){
	var checkFlag = 0;
	for(var i=0; i<GrpInfoGrid.mulLineCount; i++){
		if(GrpInfoGrid.getSelNo(i)){
			checkFlag = GrpInfoGrid.getSelNo();
			break;
		}
	}
	if(checkFlag){
		var tGrpNo = GrpInfoGrid.getRowColData(checkFlag-1 , 1);
		if(tGrpNo==null || tGrpNo=="" || tGrpNo=="null"){
			alert("请先选择一条客户信息！");
			return false;
		}
		fm.ApplyButton.disabled = true;//申请当中，按钮设置为不可用
		
		var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		
		fm.action = "./TaxApplySave.jsp";
		fm.submit();
		
		fm.action = "";
	}else{
		alert("请先选择客户信息！");
		return false;
	}
}
//提交申请之后的处理函数
function afterSubmit(FlagStr,Content,tBatchNo){
	showInfo.close();//关掉弹窗
	fm.ApplyButton.disabled = false;//返回申请的结果后，按钮设置为可用
	if( FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//反显
		var tSQL = "select batchno,applydate,batchstate,managecom,operator from lsbatchinfo where batchno='"+tBatchNo+"' and grpno='"+fm.GroupNo.value+"'";
		var tArr = easyExecSql(tSQL);
		if(tArr){
			fm.ApplyDate.value = tArr[0][1];
			fm.BatchNo.value = tArr[0][0];
			fm.ManageCom.value = tArr[0][3];
			fm.BatchState.value = tArr[0][2];
		}
		showAllCodeName();
		QueryBatchClick();
	}
}
//查询已申请的批次信息
function QueryBatchClick(){
	if(fm.GroupNo.value==null || fm.GroupNo.value=="" || fm.GroupNo.value=="null"){
		alert("请先选择一条客户记录！");
		return false;
	}
	var tSQL = "select grpno,batchno,applydate,case batchstate when '0' then '待导入' when '1' then '待内检' when '2' then '待确认' when '3' then '已确认' end,managecom,operator "
			 + "from lsbatchinfo "
			 + "where 1=1 "
			 + getWherePart("GrpNo","GroupNo")
			 + getWherePart("ManageCom","ManageCom","like")
			 + getWherePart("BatchNo","BatchNo")
			 + getWherePart("BatchState","BatchState")
			 + getWherePart("ApplyDate","ApplyDate")
			 + " order by grpno,batchno";
	turnPage1.queryModal(tSQL,BatchDetailGrid);
	if(BatchDetailGrid.mulLineCount<=0){
		alert("没有查询到相关数据");
	}
}
//批次导入
function batchImport(){
	var tRow = BatchDetailGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0){
        alert("请选择一条记录。");
        return false;
    }
    var tRowDatas = BatchDetailGrid.getRowData(tRow);
    var tBatch = tRowDatas[1];
    var tBatchState = tRowDatas[3];
    //已确认的批次只能查看导入的被保人清单
    if(tBatchState == "已确认"){
    	alert("批次："+tBatch+"为已确认状态，仅能查看导入的被保人清单,点击确定后将弹出批次被保人详情查看页面！");
    	window.open("./TaxInsuredInfoMain.jsp?BatchNo="+tBatch);
    }else{
    	window.location = "./TaxListImportInput.jsp?BatchNo="+tBatch;
    }
}
//批次被保人详情
function importInsuredDetail(){
	var tRow = BatchDetailGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0){
        alert("请选择一条记录。");
        return false;
    }
    var tRowDatas = BatchDetailGrid.getRowData(tRow);
    var tBatch = tRowDatas[1];
	window.open("./TaxInsuredInfoMain.jsp?BatchNo="+tBatch);
}
