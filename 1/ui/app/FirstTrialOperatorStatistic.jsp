<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：FirstTrialOperatorStatistic
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="FirstTrialOperatorStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form  method=post name=fm target="fraTitle">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
					<TD  class= title> 管理机构</TD>
					<TD  class= input><Input class= "codeno"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
        <TR  class= common>

					<TD  class= title>
					类型
					</TD>
					<TD  class= Input>
					<input class=codeno name=QYType verify="|len<=20" CodeData= "0|^个险投保书|1^团险询价投保书|2^团险投保书|3^全部|4"  ondblclick="return showCodeListEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);" onkeyup="return showCodeListKeyEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);"><input class=codename name=QYType_ch></TD>    
		         
			    <TD  class= title>初审人员</TD>
			    <TD  class= input> <Input class=common name=FirstTrialOperator> </TD>          
  
		    </TR>
		    </table>
    <input type="hidden" name=op value="">
    <INPUT VALUE="" TYPE=hidden  name=querySql>
    <INPUT VALUE="" TYPE=hidden  name=totalSql>
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		    <hr>
	<br>
			<TD  class= title>说明:填单问题比例=（问题件条目数+新单删除件数）/新单录入完成件数×100％</TD>	
  </br>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
