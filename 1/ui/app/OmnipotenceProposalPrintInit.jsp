<%
//程序名称：ProposalPrintInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
  
  var tIntlFlag = "<%=request.getParameter("IntlFlag")%>";
  var tSaleChnlCode = ("1" == tIntlFlag ? "salechnlall" : "LCSaleChnl");
  
// 输入框的初始化（单记录部分）
function initInpBox()
{
	try
	{
		fm.reset();
		
		if("1" == tIntlFlag)
		{
		  fm.all("PrintStateTRID").style.display = "";
		  fm.all("downloadCustomerCardButton").style.display = "";
		  fm.all("downloadCustomerCardButtonExcel").style.display = "";
		}
	}
	catch(ex)
	{
		alert("在ProposalPrintInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm()
{
    try
    {
        initInpBox();
        initContGrid();
        fm.all('ManageCom').value = <%=strManageCom%>;
        if(fm.all('ManageCom').value==86){
            fm.all('ManageCom').readOnly=false;
        }
        else{
            fm.all('ManageCom').readOnly=true;
        }
        if(fm.all('ManageCom').value!=null)
        {
            var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('ManageComName').value=arrResult[0][0];
            }
        }
        //初始化签单日期
        var dateSql = "select (Current Date - 1 month), (Current Date + 1 month) from dual";
        var arr = easyExecSql(dateSql);
        if(arr){
            fm.all('startSignDate').value = arr[0][0];
            fm.all('endSignDate').value = arr[0][1];
        }
    }
    catch(re)
    {
        alert("ProposalPrintInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var ContGrid;

// 保单信息列表的初始化
function initContGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="保险合同号";
		iArray[1][1]="120px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		iArray[1][21]="ContNo";

		iArray[2]=new Array();
		iArray[2][0]="印刷号";
		iArray[2][1]="100px";           
		iArray[2][2]=100;            	
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="保费";
		iArray[3][1]="80px";
		iArray[3][2]=200;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="投保人名称";
		iArray[4][1]="200px";
		iArray[4][2]=200;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="生效日期";
		iArray[5][1]="80px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="打印次数";
		iArray[6][1]="0px";
		iArray[6][2]=100;
		iArray[6][3]=3;

		iArray[7]=new Array();
		iArray[7][0]="打印状态";
		iArray[7][1]="50px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="是否打印";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=2;
		iArray[8][10]="contPrintFlag";
		iArray[8][11]= "0|^0|不打印|^1|打印";
		iArray[8][12]="8|9";
		iArray[8][13]="1|0";

		iArray[9]=new Array();
		iArray[9][0]="是否打印标记";
		iArray[9][1]="60px";
		iArray[9][2]=100;
		iArray[9][3]=3;
		iArray[9][21]="contPrintFlag";

		ContGrid = new MulLineEnter( "fm" , "ContGrid" );
		//这些属性必须在loadMulLine前
		ContGrid.mulLineCount = 0;
		ContGrid.displayTitle = 1;
		ContGrid.hiddenPlus = 1;
		ContGrid.hiddenSubtraction = 1;
		ContGrid.canSel = 0;
		ContGrid.canChk = 1;
		ContGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>