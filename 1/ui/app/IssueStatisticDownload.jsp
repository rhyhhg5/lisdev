<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProjectReportQueryDownLoad.jsp
//程序功能：项目制管理-->项目综合-->项目信息查询报表
//创建日期：2012-06-27
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    String tIssuePolType = request.getParameter("IssuePolType");
    String tFileName = "";
    String tSQL = "select codename from ldcode where codetype = 'issuepoltype' and code = '"+tIssuePolType+"' ";
    tFileName = new ExeSQL().getOneValue(tSQL);
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = tFileName+"_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    String tCurrentDate = PubFun.getCurrentDate();
    String mStartDate=request.getParameter("StartDate");
    String mEndDate=request.getParameter("EndDate");
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String querySql = request.getParameter("querySql");
    querySql = querySql.replaceAll("%25","%");
    //设置表头
    String[][] tTitle = new String[2][];
    tTitle[0] = new String[] { "制表人:",tG.Operator,"制表时间:",tCurrentDate,"统计时间：", mStartDate+"至"+mEndDate};
    tTitle[1] = new String[] {"机构代码", "机构名称", "类型", "件数", "比例", "回复时效"};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
        createexcellist.setRowColOffset(row,0);//设置偏移
    if(createexcellist.setData(querySql,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>
