var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartMakeDate = fm.StartMakeDate.value;
    var tEndMakeDate = fm.EndMakeDate.value;

	if(dateDiff(tStartMakeDate, tEndMakeDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}

    var tStrSQL = ""
        + " select "
        + " tmpInfo.ManageCom, tmpInfo.ManageComName, tmpInfo.SaleChnl, "
        + " tmpInfo.ContTBCount, tmpInfo.ContTBInsuCount, "
        + " tmpInfo.AllPrem, tmpInfo.AllSupplementaryPrem, "
        + " Div((tmpInfo.AllPrem + tmpInfo.AllSupplementaryPrem), tmpInfo.ContTBCount) AvgTBPrem, "
        + " tmpInfo.AllAmnt "
        + " from "
        + " ( "
        + " select "
        + " tmp.ManageCom, tmp.ManageComName, "
        + " tmp.SaleChnl, "
        + " count(distinct tmp.ContNo) ContTBCount, "
        + " count(distinct tmp.InsuredNo) ContTBInsuCount, "
        + " sum(tmp.Prem) AllPrem, "
        + " sum(tmp.SupplementaryPrem) AllSupplementaryPrem, "
        + " sum(tmp.Amnt) AllAmnt "
        + " from    "
        + " ( "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " lcp.ContNo, lcp.InsuredNo, "
        + " lcp.Prem, nvl(lcp.SupplementaryPrem, 0) SupplementaryPrem, lcp.Amnt, "
        + " '' "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcp.ContNo = lcc.ContNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + " ) as tmp "
        + " group by tmp.ManageCom, tmp.ManageComName, tmp.SaleChnl "
        + " ) as tmpInfo "
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "ContTBSave.jsp";
    fm.submit();
    fm.action = oldAction;

}