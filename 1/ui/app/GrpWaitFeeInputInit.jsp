<%@page import="com.sinosoft.utility.*"%>
<script language="JavaScript">
  
// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        fm.all('ManageCom').value = strManageCom;
        if(fm.all('ManageCom').value != null)
        {
            var arrResult = easyExecSql("select Name from LDCom where ComCode = '" + strManageCom + "'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('manageComName').value=arrResult[0][0];
            }
        }
    }
    catch(ex)
    {
        alert("ContPrintDailyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initContPrintDailyGrid();
    }
    catch(re)
    {
        alert("ContPrintDailyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var ContPrintDailyGrid;

// 保单信息列表的初始化
function initContPrintDailyGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="交费类型";
        iArray[1][1]="60px";
        iArray[1][2]=60;
        iArray[1][3]=0;

        iArray[2]=new Array();
        iArray[2][0]="业务号码";
        iArray[2][1]="90px";
        iArray[2][2]=80;
        iArray[2][3]=0;

        iArray[3]=new Array();
        iArray[3][0]="缴费通知书号/暂收据号";
        iArray[3][1]="90px";
        iArray[3][2]=80;
        iArray[3][3]=0;

        iArray[4]=new Array();
        iArray[4][0]="交费方式";
        iArray[4][1]="60px";
        iArray[4][2]=60;
        iArray[4][3]=0;

        iArray[5]=new Array();
        iArray[5][0]="交费金额";
        iArray[5][1]="90px";
        iArray[5][2]=70;
        iArray[5][3]=0;

        iArray[6]=new Array();
        iArray[6][0]="投保单位名称";
        iArray[6][1]="120px";
        iArray[6][2]=70;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="保单生效日期";
        iArray[7][1]="60px";
        iArray[7][2]=60;
        iArray[7][3]=0;

        iArray[8]=new Array();
        iArray[8][0]="业务员代码";
        iArray[8][1]="60px";
        iArray[8][2]=70;
        iArray[8][3]=0;

        iArray[9]=new Array();
        iArray[9][0]="业务员姓名";
        iArray[9][1]="60px";
        iArray[9][2]=70;
        iArray[9][3]=0;

        ContPrintDailyGrid = new MulLineEnter( "fm" , "ContPrintDailyGrid" );
        //这些属性必须在loadMulLine前
        ContPrintDailyGrid.mulLineCount = 0;
        ContPrintDailyGrid.displayTitle = 1;
        ContPrintDailyGrid.hiddenPlus = 1;
        ContPrintDailyGrid.hiddenSubtraction = 1;
        ContPrintDailyGrid.canSel = 0;
        ContPrintDailyGrid.canChk = 0;
        ContPrintDailyGrid.loadMulLine(iArray);
        ContPrintDailyGrid.selBoxEventFuncName = "ContPrintDailyDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>