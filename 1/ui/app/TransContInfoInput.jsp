<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
String tPrtNo = request.getParameter( "PrtNo" );
String tAddFlag = request.getParameter( "AddFlag" );
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
        <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
        <script src="../common/javascript/CommonTools.js"></script>
		<SCRIPT src="TransContInfoInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	</head>
	<body onload="initForm();initElementtype()" >
		<form action="./TransContInfoSave.jsp" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;">
					</td>
					<td class=titleImg>
						转入信息
					</td>
				</tr>
			</table>
			
			<table class=common>
				<tr CLASS="common">
					<td CLASS="title">
						印刷号
					</td>
					<td CLASS="input" COLSPAN="1">
						<input name="PrtNo" CLASS="common" readonly=true value=<%=tPrtNo %>  elementtype=nacessary>
					</td>
					<td CLASS="title">
						管理机构
					</td>
					<td CLASS="input" COLSPAN="1">
						<input name="ManageComName" CLASS="common" readonly=true  elementtype=nacessary>
					</td>
					<td CLASS="title"></td>
					<td CLASS="input" >
					    <input name="ManageCom" CLASS="common" readonly=true style="display: 'none'">
					</td>
				</tr>
				<tr CLASS="common" >
				<td CLASS="title">
						原保险公司
					</td>
					<td CLASS="input" COLSPAN="1">
						<Input class="codeNo" name="CompanyNo"
							verify="转入保险公司|NOTNULL&code:transcompany"
							ondblclick="return showCodeList('transcompany',[this,CompanyName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('transcompany',[this,CompanyName],[0,1],null,null,null,1);"><input class=codename name=CompanyName readonly=true elementtype=nacessary>
					</td>
					<td CLASS="title">
						原保单号
					</td>
					<td CLASS="input" COLSPAN="1">
						<input name="TransGrpContNo" CLASS="common" verify="转入保单号|notnull" elementtype=nacessary>
					</td>
					<td CLASS="title">申请日期 
    		        </td>
			        <td CLASS="input" COLSPAN="1">
			        <input NAME="ApplyDate" VALUE  class="coolDatePicker"  verify="申请日期|date&notnull" elementtype=nacessary>
    		        </td>
				</tr>
				<tr CLASS="common"  style="display: 'none'">
					<td CLASS="title" style="display: 'none'">
						转入保单类型
					</td>
					<td CLASS="input" COLSPAN="1" style="display: 'none'">
						<input class="codeNo" name="ContType" value="1"
							CodeData="0|^1|个单^2|团单"
							verify="转入保单类型|NOTNULL"
							ondblclick="return showCodeListEx('conttype',[this,ContTypeName],[0,1]);"
							onkeyup="showCodeListKeyEx('conttype',[this,ContTypeName],[0,1]);"
							onfocus="changeConttype()"><Input class=codename name=ContTypeName value="" elementtype=nacessary>
					</td>
					<td CLASS="title" id="TransContNoTitle" style="display: 'none'">
						转入分单号
					</td>
					<td CLASS="input" COLSPAN="1" style="display: 'none'" id="TransContNoInput">
						<input name="TransContNo" CLASS="common"  elementtype=nacessary>
					</td>
					<td CLASS="title" style="display: 'none'">
						业务员编码
					</td>
					<td CLASS="input" COLSPAN="1" style="display: 'none'">
						<input name="AgentCode" CLASS="common" >
						<input name="Str" CLASS="common" >
						<input name="AddFlag" CLASS="common" value=<%=tAddFlag %> >
					</td>
				</tr>
				<tr CLASS="common">
					<td CLASS="title">
						转入联系人
					</td>
					<td CLASS="input" COLSPAN="1">
						<input name="Name" CLASS="common" verify="联系人|notnull" elementtype=nacessary readonly=true>
					</td>
					<td CLASS="title">
						联系人电话
					</td>
					<td CLASS="input" COLSPAN="1">
						<input name="Phone" CLASS="common" verify="联系人电话|NUM&notnull" elementtype=nacessary>
					</td>
					<td CLASS="title">
						联系人email
					</td>
					<td CLASS="input" COLSPAN="1">
						<input name="Email" CLASS="common" verify="联系人email|Email&notnull" elementtype=nacessary>
					</td>
				</tr>
				<tr CLASS="common">
					<td CLASS="title">
						本方银行编码
					</td>
					<td CLASS="input" COLSPAN="1">
						<Input NAME=BankName CodeData="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('getbankcode1',[this,BankCode],[0,1],null,fm.ManageCom.value,'ManageCom',null,'50%');" onkeyup="return showCodeListKey('getbankcode1',[this,BankCode],[0,1]);" verify="银行代码|notnull" elementtype=nacessary><input class=codename name=BankCode readonly=true TYPE=hidden>
					</td>
					<td CLASS="title">
						本方银行账户名
					</td>
					<td CLASS="input" COLSPAN="1">
						<input name="AccName" CLASS="common" verify="本方银行账户名|notnull" elementtype=nacessary>
					</td>
					<td CLASS="title">
						本方银行帐号
					</td>
					<td CLASS="input" COLSPAN="1"><input NAME="AccNo" VALUE MAXLENGTH="20" CLASS="code"
							ondblclick="return showCodeList('getbankaccno',[this],[0],null,fm.Str.value,'1');"
							onkeyup="return showCodeListKey('getbankaccno',[this],[0],null,fm.Str.value,'1');"
							verify="银行账号|notnull" elementtype=nacessary>
					</td>
				</tr>
				<tr CLASS="common" style="display: 'none'" id="ApplyID1">
					<td CLASS="title">
						申请状态
					</td>
					<td CLASS="input" COLSPAN="1">
						<input CLASS="common" name="AppState" readonly>
					</td>
					<td CLASS="title">
						预计终止日期
					</td>
					<td CLASS="input" COLSPAN="1">
						<input CLASS="common" name="ExpectedDate" readonly>
					</td>
					<td CLASS="title">
						无法转出原因
					</td>
					<td CLASS="input" COLSPAN="1">
					    <input CLASS="common" name="RejectReson" readonly>
					</td>
				</tr>
			</table>
			<div align="left" style="display: ''"  id="AddID">
				<br />
				<input type="hidden" name="methodName" value="" />
				<INPUT VALUE="添  加" class=cssButton TYPE=button
					onclick="insertCode();">
				<INPUT VALUE="删  除" class=cssButton TYPE=button
					onclick="deleteCode();">
				<INPUT VALUE="修  改" class=cssButton TYPE=button
					onclick="updateCode();">
				<INPUT VALUE="退  出" class=cssButton TYPE=button 
					onclick="CloseCode();">		
			</div>
			<div align="left" style="display: 'none'" id="ApplyID2">
				<br />
				<INPUT VALUE="申  请" class=cssButton TYPE=button name="TransApp"
					onclick="Apply();">
				<INPUT VALUE="退  出" class=cssButton TYPE=button 
					onclick="CloseCode();">	
			</div>	
		</form>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</body>
</html>
