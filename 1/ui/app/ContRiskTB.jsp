<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：
//程序功能：
//创建日期：2009-07-30
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="ContRiskTB.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();">    
<form action="" method=post name=fm target="fraSubmit">

    <table class="common">
        <tr class="common">
            <td class="title">管理机构</td>
            <td class="input">
                <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
            </td>
            <td class="title">投保日期起</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="StartMakeDate" verify="投保日期|date&notnull" elementtype="nacessary" />
            </td>
            <td class="title">投保日期止</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="EndMakeDate" verify="投保日期|date&notnull" elementtype="nacessary" />
            </td>
        </tr>
        <tr class="common">
            <td class="title">险种分类1</td>
            <td class="input">
                <input class="codeNo" name="RiskType3" verify="险种分类1" ondblclick="return showCodeList('risktype3',[this,RiskType3Name],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('risktype3',[this,RiskType3Name],[0,1],null,null,null,1);" /><input class="codename" name="RiskType3Name" readonly="readonly" />
            </td>
            <td class="title">险种分类2</td>
            <td class="input">
                <input class="codeNo" name="RiskType4" verify="险种分类2" ondblclick="return showCodeList('risktype4',[this,RiskType4Name],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('risktype4',[this,RiskType4Name],[0,1],null,null,null,1);" /><input class="codename" name="RiskType4Name" readonly="readonly" />
            </td>
            <td class="title">是否移动行销平台出单</td>
            <td class="input">
            	<Input class=codeno name=ContainsPAD CodeData='0|^0|全部^1|是^2|否'  ondblclick="return showCodeListEx('ContainsPAD',[this,ContainsPADName],[0,1]);" onkeyup="return showCodeListKeyEx('ContainsPAD',[this,ContainsPADName],[0,1]);" value = "0"><Input class=codename name="ContainsPADName" readonly=true value = "全部">
			</td>
        </tr>               
    </table>
    
    <br />
    
    <input value="下  载" class="cssButton" type="button" onclick="submitForm();" />
    
    <hr />
    
    <div>
        <font color="red">
            <ul>报表说明：
                <li>本报表是对险种投保保费，件数进行统计。</li>
                <li>本报表不包括：犹豫期撤保/退保和投保撤单的情况。</li>
                <li>投保日期查询口径：这里指保单录入日期。</li>
            </ul>
            <ul>名词解释：
                <li>合同号码：没有保单签发之前以13开头的，都是临时合同号码。</li>
                <li>投保件数：一个险种算一件。相同合同号只统计一次。</li>
                <li>投保首年追加保费：主要针对万能险种。</li>
                <li>被保人数：每个险种有多少人进行购买。相同客户号只统计一次。</li>
                <li>投保险种件均保费：（投保保费 + 首年追加）/ 投保件数。</li>
            </ul>
            <ul>统计条件：
                <li>管理机构、投保日期段、销售渠道。</li>
                <li>投保日期段间隔不超过十二个月。</li>
            </ul>
        </font>
    </div>
    
    <input name="querySql" type="hidden" />
    <input type="hidden" name=op value="" />

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 