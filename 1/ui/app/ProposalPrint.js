//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();
var PolNo;
var manageCom;

// 提交，保存按钮对应操作
function submitForm() {
	var i = 0;
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	// showSubmitFrame(mDebug);
	initContGrid();

	fm.submit(); // 提交
}

// 提交，保存按钮对应操作
function printPol() {
	var i = 0;
	var flag = 0;
	var row = 0; // 用于记录被选中的行数
	// 判定是否有选择打印数据
	for (i = 0; i < ContGrid.mulLineCount; i++) {
		if (ContGrid.getChkNo(i) == true) {
			flag = 1;
			row = i; // 被选中的行数为i
			/**
			 * 进行影像件校验
			 */
			// 开始校验未存在影像件的pad保单
			var PrtNo = ContGrid.getRowColDataByName(row, "PrtNo");
			var tContNo = ContGrid.getRowColDataByName(row, "ContNo");
			var isPadFlag = PrtNo.substring(0, 2);
			// 1、判断被选中保单是否为PAD出单
			if (isPadFlag == "PD") {
				/**
				 * 新增问题件校验20180702
				 */
				var checkSQL = "select ScanCheckFlag from lccontsub where prtno = '"
						+ PrtNo
						+ "' with ur";
				var result = easyExecSql(checkSQL);
				if ("1" != result[0][0]) {
					alert("此保单尚未完成影像件复查，无法打印！");
					return false;
				}
				// 2、判断该PAD保单是否存在影像件
				var checkSql = "select * from es_doc_main where doccode = '"
						+ PrtNo + "' and Subtype='TB28' with ur";
				turnPage.strQueryResult = easyQueryVer3(checkSql);
				if (!turnPage.strQueryResult) {
					alert("印刷号为：" + PrtNo + "的保单不存在PAD影像件，不能打印！");
					return false;
				}
			}
			// break;
		}
	}
	// 如果没有打印数据，提示用户选择
	if (flag == 0) {
		alert("请先选择一条记录，再打印保单");
		return false;
	}

	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	// disable打印按钮，防止用户重复提交
	fm.all("printButton").disabled = true;

	// 如果是国际业务,则执行简易险平台的打印方式

	var tContNo = "";
	if (tIntlFlag == "1") {
		var count = 0;
		for (var i = 0; i < ContGrid.mulLineCount; i++) {
			if (ContGrid.getChkNo(i)) {
				count++;
			}
			if (count > 1) {
				alert("一次不能打印多个保单");
				return false;
			}
			tContNo = ContGrid.getRowColData(i, 1);
		}

		fm.action = "../briefapp/BriefSingleContPrintSave.jsp?IntlFlag="
				+ tIntlFlag + "&mCardFlag=X&mContNo=" + tContNo;
	}

	fm.submit();
}

function getQueryResult() {
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	// alert("111" + tRow);
	if (tRow == 0 || tRow == null || arrDataSet == null)
		// {alert("111");
		return arrSelected;
	// }
	// alert("222");
	arrSelected = new Array();
	// 设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow - 1];

	return arrSelected;
}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	// 无论打印结果如何，都重新激活打印按钮
	window.focus();
	try {
		showInfo.close();
	} catch (ex) {
	}

	fm.all("printButton").disabled = false;
	if (FlagStr == "Fail") {
		// 如果打印失败，展现错误信息
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}

}

// 重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
	try {
		initForm();
	} catch (re) {
		alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

// 提交前的校验、计算
function beforeSubmit() {
	// 添加操作
}

// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

function returnParent() {
	tPolNo = "00000120020110000050";
	top.location.href = "./ProposalQueryDetail.jsp?PolNo=" + tPolNo;
}

// 查询按钮
function easyQueryClick() {
	// 初始化表格
	initContGrid();
	if (!verifyInput2())
		return false;

	if (fm.PrtNo.value == "" && fm.ContNo.value == ""
			&& fm.startUWDate.value == "") {
		alert("印刷号、保单号和签单起始日期至少录入一个！");
		return false;
	}
	if (fm.PrtNo.value != "" || fm.ContNo.value != "") {
		fm.startUWDate.value = "";
		fm.endUWDate.value = "";
	}

	var strManageComWhere = " ";
	if (fm.BranchGroup.value != '') {
		strManageComWhere += " AND EXISTS"
				+ " ( SELECT AgentGroup FROM LABranchGroup WHERE AgentGroup=A.AgentGroup and BranchAttr LIKE '"
				+ fm.BranchGroup.value + "%%') ";
	}

	var tContType = "";
	if (fm.CardFlag.value == "") {
		tContType = " and ((CardFlag = '0') or (CardFlag = '2' and not exists (select 1 from LCRiskDutyWrap lcrdw inner join LDCode1 ldc1 on ldc1.Code1 = lcrdw.RiskWrapCode where lcrdw.ContNo = LCCont.ContNo and ldc1.Code = '2' and ldc1.codetype = 'bcprint'  and ldc1.othersign='1')) or (CardFlag = '8' and exists (select 1 from LDBusinessRule where ComCode = substr(ManageCom, 1, 4) and RuleType = '01' and BussType = 'TB04' and State = '1'))) ";
	} else if (fm.CardFlag.value == "0") {
		tContType = " and (CardFlag = '0')";
	} else if (fm.CardFlag.value == "8") {
		tContType = " and CardFlag = '8' and exists(select 1 from LDBusinessRule where ComCode = substr(ManageCom, 1, 4) and RuleType = '01' and BussType = 'TB04' and State = '1') ";
	} else if (fm.CardFlag.value == "2") {
		tContType = " and CardFlag = '2' and not exists  (select 1 from LCRiskDutyWrap lcrdw inner join LDCode1 ldc1 on ldc1.Code1 = lcrdw.RiskWrapCode where lcrdw.ContNo = LCCont.ContNo and ldc1.Code = '2' and ldc1.codetype = 'bcprint'  and ldc1.othersign='1') ";
	}

	var tPrintCountCondition = " and (PrintCount < 1 or PrintCount is null ) ";
	var tPrintState = "";

	// 若为国际业务
	if (tIntlFlag == "1") {
		tContType = " and IntlFlag = '1' "
		tPrintCountCondition = "";

		if ("1" == fm.PrintState.value) {
			tPrintState = "   and PrintCount > 0 ";
		} else {
			tPrintState = "   and PrintCount = 0 ";
		}
	}

	// 去掉了几个查询的筛选条件，有待以后考察是否需要保留
	var strSQL = "";
	strSQL = "select ContNo,PrtNo,Prem,AppntName,CValiDate,PrintCount, "
			+ "case when PrintCount > 0 then '已打印' else '未打印' end, CodeName('lcsalechnl', SaleChnl) "
			+ "from LCCont where ContType = '1' and GrpContNo = '00000000000000000000' "
			// + " and
			// VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+comcode+"','Pj')=1 "
			+ "and (CardFlag in ('0', '7', '8','2') or CardFlag is null) "
			+ "and AppFlag in ( '1','9') "
			+ "and UWFlag not in ('a','1','8') "
			//+ tPrintCountCondition
			+ tContType
			+ tPrintState
			// + " and EXISTS (select riskcode from LMRiskApp where NotPrintPol
			// = '0' and
			// riskcode=A.riskcode )"
			//zxs
			+ " and not exists (select 1 from LCPol a, LMRiskApp b where a.ContNo = LCCont.ContNo and a.RiskCode = b.RiskCode and b.RiskType4 = '4' and a.riskcode not in ('332301','334801') ) "
			+ getWherePart('ContNo')
			+ getWherePart('PrtNo')
			+ getWherePart('getUniteCode(agentcode)', 'AgentCode')
			+ getWherePart('SaleChnl')
			+ getWherePart('ManageCom', 'ManageCom', 'like')
			+ getWherePart('UWDate', 'startUWDate', '>=')
			+ getWherePart('UWDate', 'endUWDate', '<=')
			+ strManageComWhere
			// + "AND NOT EXISTS ( SELECT PolNo FROM LCPol WHERE A.PrtNo = PrtNo
			// AND AppFlag
			// <> '1' ) "
			// 不查询未进行过补打或遗失补发的电子保单 modofy by lxs 2016-11-16
			+ "and not exists  (select 1 from lccontsub where prtno = lccont.prtno  and (printtype = '1' and lccont.losttimes < 1 )and (printtype = '1' and lccont.printcount != -1))"
			+ " and exists (select 1 from LDCode1 where CodeType = 'printchannel' and Code = substr(ManageCom, 1, 4) and Code1 = ContType and CodeName = '"
			+ printChannelType + "') " + "and ManageCom like '" + comcode
			+ "%%' " + "order by ManageCom,AgentGroup,AgentCode " + "with ur ";

	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("未查询到满足条件的数据！");
		return false;
	}

	// 设置查询起始位置
	turnPage.pageIndex = 0;
	// 在查询结果数组中取出符合页面显示大小设置的数组
	turnPage.pageLineNum = 30;
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	// 设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = ContGrid;
	// 保存SQL语句
	turnPage.strQuerySql = strSQL;
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function queryBranch() {
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

// 查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult) {
	if (arrResult != null) {
		fm.BranchGroup.value = arrResult[0][3];
	}
}

// 下载客户卡
function downloadCustomerCard(suffix) {
	var count = 0;
	for (var i = 0; i < ContGrid.mulLineCount; i++) {
		if (ContGrid.getChkNo(i)) {
			count++;
		}
	}
	if (count == 0) {
		alert("请选择保单");
		return false;
	}

	var fmAction = fm.action;
	fm.action = "../intlapp/DownloadCustCardSave.jsp?Suffix=" + suffix;
	fm.submit();
}