<%@page import="com.sinosoft.lis.db.LCGrpContDB"%>
<%@page import="com.sinosoft.lis.db.LCContDB"%>
<%@page import="com.sinosoft.lis.db.CrsInfoListDB"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	CError tError = null;
	String Content = "";
	String FlagStr="";
	String sql="";
	String tPrtNo="";
	String tContNo="";
	String tContType="";
	String tSaleChnl="";
	String tAgentCode="";
	String tAgentCom="";
	String tAgentGroup="";
	String tCrsSaleChnl="";
	String tCrsBussType="";
	String tGrpAgentCom="";
	String tGrpAgentIdNo="";
	String tGrpAgentName="";
	String tGrpAgentCode="";
	int max=0;
	
	String tAction = "";	
	String tFinancialAction = request.getParameter("FinancialAction");
	System.out.println(tFinancialAction);
	CrsInfoListDB tcrsInfoDb=new CrsInfoListDB();	
	LCContDB tlcContDB=new LCContDB();
	LCContSet tlcContSet=new LCContSet();
	LCContSchema tLcContSchema=new LCContSchema();
	LCGrpContDB tLcGrpContDB=new LCGrpContDB();
	LCGrpContSet tLcGrpContSet=new LCGrpContSet();
	LCGrpContSchema tlGrpContSchema=new LCGrpContSchema();
	ExeSQL eSql=new ExeSQL();
	SSRS ssrs=new SSRS();
	
	VData tmData = new VData();
	ssrs=eSql.execSQL("select count(*) from crsinfolist where stateflag='4'");
	String num=ssrs.GetText(1, 1);
	System.out.println(num);
	if("0".equals(num)){
		
	if("doPositive".equals(tFinancialAction)){
		//财务冲正	
		sql="select * from crsinfolist where stateflag='2' and state in ('2','3')";
		
		
	}else if("doNegative".equals(tFinancialAction)){
		//财务冲负
		sql="select * from crsinfolist where stateflag='0' and state in ('2','3')";
		
	}else{
		//更新
		sql="select * from crsinfolist where (stateflag='1' and state in ('2','3')"
		+"and not exists (select 1 from ljapay where incomeno=crsinfolist.contno and int(SumActuPayMoney) <0 and confdate=current date))or (stateflag='0' and state = '1')";
	}
	System.out.println(sql);
	CrsInfoListSet tInfoListSet=tcrsInfoDb.executeQuery(sql);
	CrsInfoListSchema tCrsInfoListSchema=new CrsInfoListSchema();
	if(tInfoListSet.size()==0){
		Content = " 没有要处理的数据! ";
		FlagStr = "Succ";
	}
	if(tInfoListSet.size()<=200){
		max=tInfoListSet.size();
	}else{
		max=200;
	}
	//读取excel数据进行处理，并从lccont中取得所需其他数据
	
	for(int i=1;i<=max;i++){
		tCrsInfoListSchema=tInfoListSet.get(i);
		String state=tCrsInfoListSchema.getState();
		System.out.println("State:"+state);
		tContType=tCrsInfoListSchema.getContType();
		tContNo=tCrsInfoListSchema.getContNo();
		if("2".equals(state)||"3".equals(state)){
			tSaleChnl=tCrsInfoListSchema.getSaleChnl();
		}		
		if("1".equals(tContType)){
			tlcContDB.setContNo(tContNo);
			if(!tlcContDB.getInfo()){
				MMap tMMap = new MMap();
				CrsInfoListSchema mCrsInfoListSchema=new CrsInfoListSchema();
				mCrsInfoListSchema=tCrsInfoListSchema;
				mCrsInfoListSchema.setOther(mCrsInfoListSchema.getStateFlag());
				mCrsInfoListSchema.setStateFlag("4");
				mCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
				mCrsInfoListSchema.setModifyTime(PubFun.getCurrentTime());
				
				String reason="请核实合同号";
				String[] rs=reason.split("，");
				mCrsInfoListSchema.setFalseReason(rs[0]);
				tMMap.put(mCrsInfoListSchema, "UPDATE");
				tmData.clear();
				tmData.add(tMMap);
				PubSubmit tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(tmData, "UPDATE")) {	
					
				}
				continue;
			}
			tlcContSet=tlcContDB.query();
			tLcContSchema=tlcContSet.get(1);
			tPrtNo=tLcContSchema.getPrtNo();
			
			if("".equals(tCrsInfoListSchema.getSaleChnl())||tCrsInfoListSchema.getSaleChnl()==null){
				tSaleChnl=tLcContSchema.getSaleChnl();
			}else{
				tSaleChnl=tCrsInfoListSchema.getSaleChnl();;
			}
			if("2".equals(state)){
				
				if("".equals(tCrsInfoListSchema.getAgentCode())||tCrsInfoListSchema.getAgentCode()==null){
					tAgentCode=tLcContSchema.getAgentCode();
				}else{
					tAgentCode=tCrsInfoListSchema.getAgentCode();
				}
				if("".equals(tCrsInfoListSchema.getAgentCom())||tCrsInfoListSchema.getAgentCom()==null){
					tAgentCom=tLcContSchema.getAgentCom();
				}else{
					tAgentCom=tCrsInfoListSchema.getAgentCom();
				}
				if("".equals(tCrsInfoListSchema.getAgentGroup())||tCrsInfoListSchema.getAgentGroup()==null){
					tAgentGroup=tLcContSchema.getAgentGroup();
				}else{
					tAgentGroup=tCrsInfoListSchema.getAgentGroup();
				}
				tAction="Both";
				System.out.println(tAgentCode+","+tAgentCom+","+tAgentGroup);
			}else if("3".equals(state)){
				tAgentCode=tCrsInfoListSchema.getAgentCode();
				tAgentCom=tCrsInfoListSchema.getAgentCom();
				tAgentGroup=tCrsInfoListSchema.getAgentGroup();
				tAction="Both";
			}else if("1".equals(state)){
				tAgentCode=tLcContSchema.getAgentCode();
				tAgentCom=tLcContSchema.getAgentCom();
				tAgentGroup=tLcContSchema.getAgentGroup();
				tSaleChnl=tLcContSchema.getSaleChnl();
				tAction="skip";
			}
			
		}else if("2".equals(tContType)){
			tLcGrpContDB.setGrpContNo(tContNo);
			if(!tLcGrpContDB.getInfo()){
				MMap tMMap = new MMap();
				CrsInfoListSchema mCrsInfoListSchema=new CrsInfoListSchema();
				mCrsInfoListSchema=tCrsInfoListSchema;
				mCrsInfoListSchema.setOther(mCrsInfoListSchema.getStateFlag());
				mCrsInfoListSchema.setStateFlag("4");
				mCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
				mCrsInfoListSchema.setModifyTime(PubFun.getCurrentTime());
				
				String reason="请核实合同号";
				String[] rs=reason.split("，");
				mCrsInfoListSchema.setFalseReason(rs[0]);
				tMMap.put(mCrsInfoListSchema, "UPDATE");
				tmData.clear();
				tmData.add(tMMap);	
				PubSubmit tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(tmData, "UPDATE")) {	
					
				}
				continue;
			}
			tLcGrpContSet=tLcGrpContDB.query();
			tlGrpContSchema=tLcGrpContSet.get(1);
			tPrtNo=tlGrpContSchema.getPrtNo();
			
			if("".equals(tCrsInfoListSchema.getSaleChnl())||tCrsInfoListSchema.getSaleChnl()==null){
				tSaleChnl=tlGrpContSchema.getSaleChnl();
			}else{
				tSaleChnl=tCrsInfoListSchema.getSaleChnl();;
			}
			if("2".equals(state)){
				
				if("".equals(tCrsInfoListSchema.getAgentCode())||tCrsInfoListSchema.getAgentCode()==null){
					tAgentCode=tlGrpContSchema.getAgentCode();
				}else{
					tAgentCode=tCrsInfoListSchema.getAgentCode();
				}
				if("".equals(tCrsInfoListSchema.getAgentCom())||tCrsInfoListSchema.getAgentCom()==null){
					tAgentCom=tlGrpContSchema.getAgentCom();
				}else{
					tAgentCom=tCrsInfoListSchema.getAgentCom();
				}
				
				if("".equals(tCrsInfoListSchema.getAgentGroup())||tCrsInfoListSchema.getAgentGroup()==null){
					tAgentGroup=tlGrpContSchema.getAgentGroup();
				}else{
					tAgentGroup=tCrsInfoListSchema.getAgentGroup();
				}
				System.out.println(tAgentCode+","+tAgentCom+","+tAgentGroup);
				tAction="Both";
			}else if("3".equals(state)){
				tAgentCode=tCrsInfoListSchema.getAgentCode();
				tAgentCom=tCrsInfoListSchema.getAgentCom();
				tAgentGroup=tCrsInfoListSchema.getAgentGroup();
				tAction="Both";
			}else if("1".equals(state)){
				tAgentCode=tlGrpContSchema.getAgentCode();
				tAgentCom=tlGrpContSchema.getAgentCom();
				tAgentGroup=tlGrpContSchema.getAgentGroup();
				tSaleChnl=tlGrpContSchema.getSaleChnl();
				tAction="skip";
			}
			
		}
		VData tVData = new VData();
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput = (GlobalInput)session.getValue("GI");
		tVData.add(tGlobalInput);
		
		TransferData tTransferData = new TransferData(); 
		tTransferData.setNameAndValue("tAction", tAction);	
		tTransferData.setNameAndValue("tFinancialAction", tFinancialAction);
		tTransferData.setNameAndValue("tContType", tContType);
		tTransferData.setNameAndValue("tPrtNo", tPrtNo);
		tTransferData.setNameAndValue("tContNo", tContNo);
		tTransferData.setNameAndValue("tSaleChnl", tSaleChnl);
		tTransferData.setNameAndValue("tAgentCom", tAgentCom);
		tTransferData.setNameAndValue("tAgentCode", tAgentCode);
		tTransferData.setNameAndValue("tAgentGroup", tAgentGroup);
		
		tVData.addElement(tTransferData);
		
		QYMaintenanceBL tQYMaintenanceBL = new QYMaintenanceBL();
		System.out.println("before submit");
		if(!tQYMaintenanceBL.submitData(tVData,tAction))
		{
			
			MMap tMMap = new MMap();
			CrsInfoListSchema mCrsInfoListSchema=new CrsInfoListSchema();
			mCrsInfoListSchema=tCrsInfoListSchema;
			mCrsInfoListSchema.setOther(mCrsInfoListSchema.getStateFlag());
			mCrsInfoListSchema.setStateFlag("4");
			mCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
			mCrsInfoListSchema.setModifyTime(PubFun.getCurrentTime());
			
			String reason=tQYMaintenanceBL.mErrors.getError(0).errorMessage;
			String[] rs=reason.split("，");
			mCrsInfoListSchema.setFalseReason(rs[0]);
			tMMap.put(mCrsInfoListSchema, "UPDATE");
			tmData.clear();
			tmData.add(tMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tmData, "UPDATE")) {	
				
			}
			
		}
		else
		{
			Content = " 保存成功! ";
			FlagStr = "Succ";		
			if("doNegative".equals(tFinancialAction)){			
				MMap tMMap = new MMap();
				CrsInfoListSchema mCrsInfoListSchema=new CrsInfoListSchema();
				mCrsInfoListSchema=tCrsInfoListSchema;
				mCrsInfoListSchema.setStateFlag("1");	
				mCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
				mCrsInfoListSchema.setModifyTime(PubFun.getCurrentTime());
				tMMap.put(mCrsInfoListSchema, "UPDATE");
				tmData.clear();
				tmData.add(tMMap);		
				PubSubmit tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(tmData, "UPDATE")) {	
					
				}
				
			
			}else if("doPositive".equals(tFinancialAction)){
				MMap tMMap1 = new MMap();
				CrsInfoListSchema mCrsInfoListSchema=new CrsInfoListSchema();
				mCrsInfoListSchema=tCrsInfoListSchema;
				mCrsInfoListSchema.setStateFlag("3");
				mCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
				mCrsInfoListSchema.setModifyTime(PubFun.getCurrentTime());
				tMMap1.put(mCrsInfoListSchema, "UPDATE");
				System.out.println("66666666");
				System.out.println(mCrsInfoListSchema.getEdorNo());	
				tmData.clear();
				tmData.add(tMMap1);			
				PubSubmit tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(tmData, "UPDATE")) {	
					
				}
				
			}
			else if("None".equals(tFinancialAction)){
				VData tvData = new VData();
				tvData.add(tGlobalInput);
				TransferData mTransferData = new TransferData();
				tCrsSaleChnl=tCrsInfoListSchema.getCrs_SaleChnl();
				tCrsBussType=tCrsInfoListSchema.getCrs_BussType();
				tGrpAgentCode=tCrsInfoListSchema.getGrpagentCode();
				tGrpAgentCom=tCrsInfoListSchema.getGrpagentCom();
				tGrpAgentIdNo=tCrsInfoListSchema.getGrpagentIdNo();
				tGrpAgentName=tCrsInfoListSchema.getGrpagentName();
				mTransferData.setNameAndValue("tAction", tAction);
				mTransferData.setNameAndValue("tFinancialAction", tFinancialAction);
				mTransferData.setNameAndValue("tContType", tContType);
				mTransferData.setNameAndValue("tPrtNo", tPrtNo);
				mTransferData.setNameAndValue("tContNo", tContNo);
				mTransferData.setNameAndValue("tSaleChnl", tSaleChnl);
				mTransferData.setNameAndValue("tCrsSaleChnl", tCrsSaleChnl);
				mTransferData.setNameAndValue("tCrsBussType", tCrsBussType);
				mTransferData.setNameAndValue("tGrpAgentCode", tGrpAgentCode);
				mTransferData.setNameAndValue("tGrpAgentName", tGrpAgentName);
				mTransferData.setNameAndValue("tGrpAgentIdNo", tGrpAgentIdNo);	
				mTransferData.setNameAndValue("tGrpAgentCom", tGrpAgentCom);
				tvData.addElement(mTransferData);
				CrsnanceBL tCrsnanceBL = new CrsnanceBL();
				System.out.println("before submit");
				if(!tCrsnanceBL.submitData(tvData,"modify"))
				{
					
					MMap tMMap = new MMap();
					CrsInfoListSchema mCrsInfoListSchema=new CrsInfoListSchema();
					mCrsInfoListSchema=tCrsInfoListSchema;
					mCrsInfoListSchema.setOther(mCrsInfoListSchema.getStateFlag());
					mCrsInfoListSchema.setStateFlag("4");
					mCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
					mCrsInfoListSchema.setModifyTime(PubFun.getCurrentTime());
					String reason=tCrsnanceBL.mErrors.getError(0).errorMessage;
					String[] rs=reason.split("，");
					mCrsInfoListSchema.setFalseReason(rs[0]);
					tMMap.put(mCrsInfoListSchema, "UPDATE");
					tmData.clear();
					tmData.add(tMMap);		
					PubSubmit tPubSubmit = new PubSubmit();
					if (!tPubSubmit.submitData(tmData, "UPDATE")) {	
						
					}
				}
				else
				{
					Content = " 保存成功! ";
					FlagStr = "Succ";
					if(!tCrsnanceBL.submitData(tVData,"insert"))
					{
						
						MMap tMMap = new MMap();
						CrsInfoListSchema mCrsInfoListSchema=new CrsInfoListSchema();
						mCrsInfoListSchema=tCrsInfoListSchema;
						mCrsInfoListSchema.setOther(mCrsInfoListSchema.getStateFlag());
						mCrsInfoListSchema.setStateFlag("4");
						mCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
						mCrsInfoListSchema.setModifyTime(PubFun.getCurrentTime());
						String reason=tCrsnanceBL.mErrors.getError(0).errorMessage;
						String[] rs=reason.split("，");
						mCrsInfoListSchema.setFalseReason(rs[0]);
						tMMap.put(mCrsInfoListSchema, "UPDATE");
						tmData.clear();
						tmData.add(tMMap);	
						PubSubmit tPubSubmit = new PubSubmit();
						if (!tPubSubmit.submitData(tmData, "UPDATE")) {	
							
						}
					}
					else
					{
						Content = " 保存成功! ";
						FlagStr = "Succ";
						MMap tMMap2 = new MMap();
						CrsInfoListSchema mCrsInfoListSchema=new CrsInfoListSchema();
						mCrsInfoListSchema=tCrsInfoListSchema;
						if("1".equals(mCrsInfoListSchema.getState())){
							mCrsInfoListSchema.setStateFlag("3");
						}else{
							mCrsInfoListSchema.setStateFlag("2");
						}	
						mCrsInfoListSchema.setModifyDate(PubFun.getCurrentDate());
						mCrsInfoListSchema.setModifyTime(PubFun.getCurrentTime());
						tMMap2.put(mCrsInfoListSchema, "UPDATE");
						tmData.clear();
						tmData.add(tMMap2);
						PubSubmit tPubSubmit = new PubSubmit();
						if (!tPubSubmit.submitData(tmData, "UPDATE")) {	
							
						}
					}
				}
				
			}
			
		}
		
	}
	ssrs=eSql.execSQL("select count(*) from crsinfolist where stateflag='4'");
	num=ssrs.GetText(1, 1);
	if("0".equals(num)){
		
	}else{
		Content = " 存在问题件,请处理问题件";
		FlagStr = "Fail";
	}
	
	}else{
		Content = " 请先处理问题件，再继续更新";
		FlagStr = "Fail";
	}
	
	
	
	System.out.println("Content:"+Content);

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>