<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：ParticularStatistic.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="ParticularStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./ParticularStatisticSub.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
					<TD  class= title> 管理机构</TD>
					<TD  class= input><Input class= "codeno"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
       </TR>
			<TR  class= common>
				<TD  class= title> 险种编码 </TD>
				<TD  class= input><Input class= "codeno"  name=RiskCode  readonly=true elementtype=nacessary ondblclick=" getRisk(this); return showCodeList('RiskCode',[this,RiskName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RiskCode',[this,RiskName],[0,1],null,null,null,1);" ><Input class=codename  name=RiskName></TD>
        <TD  class= title> 销售渠道 </TD>
				<TD  class= Input><input class=codeno name=saleChnl ondblclick="return showCodeList('lcsalechnl',[this,saleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('lcsalechnl',[this,saleChnlName],[0,1],null,null,null,1);"><input class=codename name=saleChnlName></TD>
				<TD  class= title id='tdAgentComName' style= "display: 'none'"> 销售网点 </TD>
				<TD  class= Input id='tdAgentCom' style= "display: 'none'"><input class=code name=agentCom ondblclick="return queryAgentComBank();"></TD>
			</TR>
    </table>

    <input type="hidden" name=op value=""><input type="hidden" name=querySql >
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>




