<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：GroupUW.jsp
//程序功能：集体新单复核
//创建日期：2002-08-13 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  
	//String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	String tPrtNo = "";
	String tGrpProposalNo = "";
	tPrtNo = request.getParameter("PrtNo");
	tGrpProposalNo = request.getParameter("GrpProposalNo");
	
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var PrtNo = "<%=tPrtNo%>";
	var GrpMainProposalNo = "<%=tGrpProposalNo%>";
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="GroupPolApproveDetail.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GroupPolApproveDetailInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./GroupUWCho.jsp" method=post name=fmQuery target="fraSubmit">
    <TR  class= common>
<!--   <TD  class= titles>
        集体投保单号码-->
        <Input class= common name=GrpProposalNo type="hidden">
        <Input type = "hidden" class= common name="GrpMainProposalNo" value="">
        <INPUT type= "hidden" name= "Operator" value= "">
    </TR>       
  
    <table>
    <tr>
    	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);"></td>
        <td class= titleImg> 集体投保单查询结果 </td>
   </tr>
   </table>
    
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanGrpGrid" onclick= "getGrpPolGridCho();"></span> 
  			  	</td>
  			</tr>
    	</table>   
    	 	
    <!-- 集体投保单下个人单查询结果部分（列表） -->
    <Div id = "divPerson" style = "display: ''">
    <hr  SIZE=12 NOSHADE=true ></hr>
    <table>
    	<tr>
        	<td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);"></td>
    		<td class= titleImg>个人投保单查询结果</td>
    	</tr>
    </table>
   
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" onclick= "getPolGridCho();">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" Class=Common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" Class=Common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" Class=Common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" Class=Common TYPE=button onclick="getLastPage();">
      <INPUT TYPE="hidden" name= "PolTypeHide" value= "">
      <INPUT TYPE="hidden" name= "PrtNoHide"   value= "">
  	</div>
  </form>
  <form  action="./GroupPolApproveSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 核保 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>团体单下个人投保单信息：</td>
		</tr>
	</table>
    <table  class= common border=0 width=100%>
      	<TR  class= common>
       <!--   <TD  class= title> 投保单号码</TD>-->
				<Input class="readonly" readonly name=ProposalNo type="hidden">          
          <TD  class= title>险种编码</TD>
          <TD  class= input><Input class="readonly" readonly name=RiskCode ></TD>
          
          <TD  class= title>险种版本</TD>
          <TD  class= input><Input class="readonly" readonly name=RiskVersion ></TD>
        </TR>
        <TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input><Input class="readonly" readonly name=ManageCom ></TD>
          
          <TD  class= title>投保人客户号</TD>
          <TD  class= input><Input class="readonly" readonly name=AppntNo ></TD>
          
          <TD  class= title> 投保人姓名</TD>
          <TD  class= input><Input class="readonly" readonly name=AppntName ></TD>
        </TR>
        <TR  class= common>
          <TD  class= title>被保人客户号</TD>
          <TD  class= input><Input class="readonly" readonly name=InsuredNo ></TD>
          
          <TD  class= title>被保人姓名</TD>
          <TD  class= input><Input class="readonly" readonly name=InsuredName ></TD>
          
          <TD  class= title>被保人性别</TD>
          <TD  class= input><Input class="readonly" readonly name=InsuredSex ></TD>
        </TR>
        <TR  class= common>
          <TD  class= title>份数</TD>
          <TD  class= input><Input class="readonly" readonly name=Mult ></TD>
          
          <TD  class= title>保费</TD>
          <TD  class= input><Input class="readonly" readonly name=Prem ></TD>
          
          <TD  class= title>保额</TD>
          <TD  class= input><Input class="readonly" readonly name=Amnt ></TD>
        </TR>
	</table>
	<table class= common border=0 width=100% >
          <INPUT VALUE = "个单保单明细信息" Class = Common TYPE = button onclick = "showPolDetail();"> 
          <!--INPUT VALUE = "个单问题件查询" Class = Common type = button onclick = "QuestQuery();"-- >
          <!INPUT VALUE = "个单问题件录入" Class = Common type = button onclick = "QuestInput();"-->
    </table>    
    <table  class= common border=0 width=100%>
      	  <input type="hidden" name= "PolNoHide" value= "">
      	  <input type="hidden" name= "GrpProposalNo" value= "">
      	  <hr SIZE=12 NOSHADE=true></hr>
   </table>	  
  </Div>
          <INPUT VALUE="团体扫描件查询"    Class=Common TYPE=button onclick="ScanQuery2();"> 
          <INPUT VALUE="团体问题件查询"    Class=Common TYPE=button onclick="GrpQuestQuery();">
          <INPUT VALUE="团体问题件录入"    Class=Common TYPE=button onclick="GrpQuestInput();">
          
    <!-- 集体单新单复核结论 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>团体单新单复核结论：</td>
	    </tr>
    </table>
          <INPUT VALUE="团体单新单复核通过" Class=Common TYPE=button onclick="gmanuchk();">
          <INPUT VALUE="返回" Class=Common TYPE=button onclick="goback();">
          <input type="hidden" name= "PrtNoHide" value= "">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
