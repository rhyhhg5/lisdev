<%@page import="com.sinosoft.utility.*"%>
<script language="JavaScript">
  
// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        
    }
    catch(ex)
    {
        alert("RiskInfoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initRiskInfoGrid();
    }
    catch(re)
    {
        alert("RiskInfoInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var RiskInfoGrid;

// 保单信息列表的初始化
function initRiskInfoGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="险种编码";
        iArray[1][1]="50px";
        iArray[1][2]=50;
        iArray[1][3]=0;
        iArray[1][21]="riskCode";

        iArray[2]=new Array();
        iArray[2][0]="险种名称";
        iArray[2][1]="160px";
        iArray[2][2]=80;
        iArray[2][3]=0;
        iArray[2][21]="riskName";

        iArray[3]=new Array();
        iArray[3][0]="团/个/中介";
        iArray[3][1]="60px";
        iArray[3][2]=80;
        iArray[3][3]=0;
        iArray[3][21]="riskProp";

        iArray[4]=new Array();
        iArray[4][0]="主附险标记";
        iArray[4][1]="60px";
        iArray[4][2]=60;
        iArray[4][3]=0;
        iArray[4][21]="subRiskFlag";

        iArray[5]=new Array();
        iArray[5][0]="保险期间标志";
        iArray[5][1]="70px";
        iArray[5][2]=70;
        iArray[5][3]=0;
        iArray[5][21]="riskType5";

        iArray[6]=new Array();
        iArray[6][0]="开办日期";
        iArray[6][1]="65px";
        iArray[6][2]=70;
        iArray[6][3]=0;
        iArray[6][21]="startDate";

        iArray[7]=new Array();
        iArray[7][0]="停办日期";
        iArray[7][1]="65px";
        iArray[7][2]=80;
        iArray[7][3]=0;
        iArray[7][21]="endDate";

        iArray[8]=new Array();
        iArray[8][0]="最小被保人年龄";
        iArray[8][1]="80px";
        iArray[8][2]=80;
        iArray[8][3]=0;
        iArray[8][21]="minInsuredAge";

        iArray[9]=new Array();
        iArray[9][0]="最大被保人年龄";
        iArray[9][1]="80px";
        iArray[9][2]=80;
        iArray[9][3]=0;
        iArray[9][21]="maxInsuredAge";

        RiskInfoGrid = new MulLineEnter( "fm" , "RiskInfoGrid" );
        //这些属性必须在loadMulLine前
        RiskInfoGrid.mulLineCount = 0;
        RiskInfoGrid.displayTitle = 1;
        RiskInfoGrid.hiddenPlus = 1;
        RiskInfoGrid.hiddenSubtraction = 1;
        RiskInfoGrid.canSel = 0;
        RiskInfoGrid.canChk = 0;
        RiskInfoGrid.loadMulLine(iArray);
        RiskInfoGrid.selBoxEventFuncName = "RiskInfoDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>