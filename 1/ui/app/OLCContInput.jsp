<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:22:25
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="OLCContInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="OLCContInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./OLCContSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 合同信息部分 -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOLCCont1);">
    		</td>
    		<td class= titleImg>
    			 总单(合同)信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divOLCCont1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
     <!--    <TD  class= title>
            总单投保单号码
          </TD>-->
            <Input class= common name=ProposalNo type="hidden">
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='code' name=ManageCom ondblclick="return showCodeList('station',[this]);"  onkeyup="return showCodeListKey('station',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            销售渠道
          </TD>
          <TD  class= input>
            <Input class='code' name=SaleChnl ondblclick="return showCodeList('SaleChnl',[this]);"  onkeyup="return showCodeListKey('SaleChnl',[this]);">
          </TD>
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input class='code' name=AgentCom ondblclick="return showCodeList('AgentCom',[this]);"  onkeyup="return showCodeListKey('AgentCom',[this]);">
          </TD>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class='code' name=AgentCode ondblclick="return showCodeList('AgentCode',[this]);"  onkeyup="return showCodeListKey('AgentCode',[this]);">
          </TD>
        </TR>
      </table>
    </Div>
    <!-- 投保人信息部分 -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContract2);">
    		</td>
    		<td class= titleImg>
    			 投保人信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divContract2" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            投保人客户号
          </TD>
          <TD  class= input>
            <Input class= common name=GrpNo >
          </TD>
          <TD  class= title>
            投保人名称
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Name >
          </TD>
          <TD  class= title>
            投保人类型
          </TD>
          <TD  class= input>
            <Input class='code' name=ContType ondblclick="return showCodeList('ContType',[this]);" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系人
          </TD>
          <TD  class= input>
            <Input class= common name=LinkMan >
          </TD>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone >
          </TD>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= common name=Address >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name=ZipCode >
          </TD>
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
        </TR>
      </table>
    </Div>
    <input type=hidden name=hideOperate value=''>
      <INPUT VALUE="进入集体信息" TYPE=button onclick="intoGrpPol()"> 
      <INPUT VALUE="进入个人信息" TYPE=button onclick="intoPol()"> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
