<%@page import="com.sinosoft.utility.*"%>
<%
    GlobalInput globalInput = (GlobalInput)session.getValue("GI");
    String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
  
// 输入框的初始化（单记录部分）
function initInpBox()
{
	try
	{
        fm.manageCom.value = "";
        fm.manageComName.value = "";
        fm.contType.value = "";
        fm.contTypeName.value = "";
        fm.printChannel.value = "";
        fm.printChannelName.value = "";
	}
	catch(ex)
	{
		alert("PrintChannelInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm()
{
	try
	{
        initInpBox();
        initPrintChannelGrid();
        fm.all('manageComQuery').value = "<%=strManageCom%>";
        if(fm.all('manageComQuery').value=="86"){
            fm.all('manageComQuery').readOnly=false;
    	}
        else
        {
    	    fm.all('manageComQuery').readOnly=true;
    	}
    	if(fm.all('manageComQuery').value!=null)
        {
    	    var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('manageComQuery').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('manageComNameQuery').value=arrResult[0][0];
            } 
    	}
	}
	catch(re)
	{
		alert("PrintChannelInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

//定义为全局变量，提供给displayMultiline使用
var PrintChannelGrid;

// 保单信息列表的初始化
function initPrintChannelGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="公司编码";
		iArray[1][1]="100px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		iArray[1][21]="manageCom";

		iArray[2]=new Array();
		iArray[2][0]="公司名称";
		iArray[2][1]="100px";
		iArray[2][2]=100;
		iArray[2][3]=0;
		iArray[2][21]="manageComName";

		iArray[3]=new Array();
		iArray[3][0]="保单类型";
		iArray[3][1]="100px";
		iArray[3][2]=100;
		iArray[3][3]=3;
		iArray[3][21]="contType";

		iArray[4]=new Array();
		iArray[4][0]="保单类型";
		iArray[4][1]="100px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		iArray[4][21]="contTypeName";

		iArray[5]=new Array();
		iArray[5][0]="打印渠道";
		iArray[5][1]="80px";
		iArray[5][2]=200;
		iArray[5][3]=3;
		iArray[5][21]="printChannel";

		iArray[6]=new Array();
		iArray[6][0]="打印渠道";
		iArray[6][1]="80px";
		iArray[6][2]=200;
		iArray[6][3]=0;
		iArray[6][21]="printChannelName";

		PrintChannelGrid = new MulLineEnter( "fm" , "PrintChannelGrid" );
		//这些属性必须在loadMulLine前
		PrintChannelGrid.mulLineCount = 0;
		PrintChannelGrid.displayTitle = 1;
		PrintChannelGrid.hiddenPlus = 1;
		PrintChannelGrid.hiddenSubtraction = 1;
		PrintChannelGrid.canSel = 1;
		PrintChannelGrid.canChk = 0;
		PrintChannelGrid.loadMulLine(iArray);
		PrintChannelGrid.selBoxEventFuncName = "printChannelDetail";
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>