<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：EasyScanErrorListInit.jsp
//程序功能：外包异常保单修改状态页面
//创建日期：2008-6-4
//创建人  ：ZhangJianbao
//更新记录：更新人    更新日期     更新原因/内容
%>

<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	String tCurrentDate = PubFun.getCurrentDate();
    String tStrSubType = (String)request.getParameter("SubType");
%>                            

<script language="JavaScript">
  
var tManageCom = "<%=tGI.ComCode%>";
var tCurrentDate = "<%=tCurrentDate%>";
var tSubType = "<%=tStrSubType%>";

function initForm()
{
    try
    {
        initInputBox();
        initContGrid();
        initElementtype();
    }
    catch(ex)
    {
        alert("初始化界面错误，" + ex.message);
    }
}

// 错误信息列表的初始化
function initContGrid()
{
    var iArray = new Array();
        
    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=20;            			//列最大值
        iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="外包批次号";         		//列名
        iArray[1][1]="150px";            		//列宽
        iArray[1][2]=100;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[1][21]="BatchNo";
        
        iArray[2]=new Array();
        iArray[2][0]="印刷号";         		//列名
        iArray[2][1]="70px";            		//列宽
        iArray[2][2]=80;            			//列最大值
        iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[2][21]="BussNo"; 
        
        iArray[3]=new Array();
        iArray[3][0]="工作流任务ID";         		//列名
        iArray[3][1]="0px";            		//列宽
        iArray[3][2]=80;            			//列最大值
        iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        iArray[3][21]="MissionID"; 
        
        iArray[4]=new Array();
        iArray[4][0]="扫描机构";         		//列名
        iArray[4][1]="50px";            		//列宽
        iArray[4][2]=100;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[4][21]="ManageCom";
        
        iArray[5]=new Array();
        iArray[5][0]="外包批次状态";         		//列名
        iArray[5][1]="80px";            		//列宽
        iArray[5][2]=100;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[5][21]="BatchState";
        
        iArray[6]=new Array();
        iArray[6][0]="保单状态";         		//列名
        iArray[6][1]="80px";            		//列宽
        iArray[6][2]=100;            			//列最大值
        iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[6][21]="MissionState";
        
        ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
        //这些属性必须在loadMulLine前
        ContGrid.mulLineCount = 0;   
        ContGrid.displayTitle = 1;
        ContGrid.locked = 1;
        ContGrid.canSel = 1;
        ContGrid.canChk = 0;
        ContGrid.hiddenPlus=1;   
        ContGrid.hiddenSubtraction=1;
        ContGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化" + ex.message);
    }
}

</script>