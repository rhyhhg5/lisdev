<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：ContQueryWN.jsp
//程序功能：万能险查询面
//创建日期：2007-11-9
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!—页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <SCRIPT src="ContQueryWN.js"></SCRIPT>
  <%@include file="ContQueryWNInit.jsp"%>
  <title>万能险查询</title>
</head>
<body  onload="initForm();" >
<!--通过initForm方法给页面赋初始值-->
  <form action="ContQueryWNDownload.jsp" method=post name=fm target="fraSubmit">
    <table class= common border=0 width=100%>
      <tr>
        <td class= titleImg align= center>请输入查询条件：</td>
      </tr>
    </table>
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>印刷号码</td>
        <td  class= input><input class= common name=PrtNo  verify="印刷号码|int"></td>
        <td  class= title>合同号码</td>
        <td  class= input><input class= common name=ContNo  verify="合同号码|int"></td>
        <td  class= title>管理机构</td>
        <td  class= input><input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype="nacessary"></td>
      </tr>
      <tr  class= common>
        <td  class= title>销售渠道</td>
        <td  class= input><input class="codeNo"  name=SaleChnl ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true></td>
        <td class= title>投保人姓名</td>
        <td  class= input><input class=common name="AppntName" verify="投保人|len<=20"></td>
        <td class= title>生效日期</td>
        <td  class= input><input class=coolDatePicker3 name="CValiDate" verify="生效日期|date"  style="width:130"></td>
      </tr>
      <tr  class= common>
        <td  class= title>状态</td>
        <td  class= input>
          <Input class=codeNo name="State" readonly= true value CodeData="0|^1|待录入^2|待复核^3|待收费^4|待签单^5|已签单" ondblclick="return showCodeListEx('contstate',[this,StateName],[0,1]);" onkeyup="return showCodeListKeyEx('contstate',[this,StateName],[0,1]);"><input class=codename name=StateName readonly=true>
        </td>
        <td class= title>扫描日期起</td>
        <td  class= input><input class=coolDatePicker3 name="CValiDateStart" verify="扫描日期起|date&notnull" elementtype="nacessary"  style="width:130"></td>
        <td class= title>扫描日期止</td>
        <td  class= input><input class=coolDatePicker3 name="CValiDateEnd" verify="扫描日期止|date&notnull"  elementtype="nacessary"  style="width:130"></td>
      </tr>
      <tr  class= common>
        <td class= title>代理机构</td>
        <td  class= input><input class=common name="AgentCom" ></td>
        <td class= title>代理人</td>
        <td  class= input><input class=common name="AgentCode" ></td>
      </tr>
    </table>
    <input class=cssButton value="查  询" type=button onclick="queryClick();">
	<input class=cssButton value="重  置" type=button onclick="initInpBox();">
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCCont);"></td>
	  <td class=titleImg>查询结果</td>
	</tr>
  </table>
  <!-- 信息（列表） -->
  <div id="divLCCont" style="display:''">
	<table class=common>
      <tr class=common>
	    <td text-align:left colSpan=1>
	      <span id="spanLCContGrid">
	      </span>
	    </td>
	  </tr>
	</table>
  </div>

  <div id="divPage2" align=center style="display: 'none' ">
	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </div>
  <input class=cssButton value="下  载" type=button onclick="downloadCont();" name="DownloadButton">
  <input type=hidden id="fmtransact" name="fmtransact">
  <Input type=hidden name=Sql>
</form>
<!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
