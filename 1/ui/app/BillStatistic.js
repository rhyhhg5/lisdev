var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    
	var tStartDate = fm.StartDate.value;
	var tEndDate = fm.EndDate.value;
	var tMngCom = fm.ManageCom.value;
	if(dateDiff(tStartDate,tEndDate,"M")>3)
	{
		alert("统计期最多为三个月！");
		return false;
	}
	
    if(dateDiff(tStartDate,tEndDate,"M")<1){
      alert("统计止期比统计统计起期早");
      return false;
    }
	
    var tStrSQL = ""
        +" select ( select codename from ldcode where code=a.code and codetype='prttype' ) , " 
        +" c.showname , a.PrtSeq , b.FirstTrialOperator , getUniteCode(b.agentcode) , a.makedate , "
        +" b.UWDate "
        +" from "
        +" Loprtmanager a , lccont b , ldcom c "
        +" where c.comcode=b.managecom and a.otherno=b.ProposalContNo and b.conttype='1' and b.UWflag='9' and a.code<>'07' "
        +" and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and "
        +" substr ( b.ManageCom , 1 , length ( '"+tMngCom+"' ) )='"+tMngCom+"' "
        +" union "
        +" select ( select codename from ldcode where code=a.code and codetype='prttype' ) , "
        +" c.showname , a.PrtSeq , b.FirstTrialOperator , getUniteCode(b.agentcode) , a.makedate , "
        +" ( select makedate from es_doc_main where DocCode=a.prtseq and b. UWflag<>'9' ) "
        +" from "
        +" Loprtmanager a , lccont b , ldcom c "
        +" where c.comcode=b.managecom and a.otherno=b.ProposalContNo and b.conttype='1' and a.code<>'07' and b. UWflag<>'9' "
        +" and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and "
        +" substr ( b.ManageCom , 1 , length ( '"+tMngCom+"' ) )='"+tMngCom+"' "
        +" union "
        +" select ( select codename from ldcode where code=a.code and codetype='prttype' ) , "
        +" c.showname , a.PrtSeq , b.FirstTrialOperator , getUniteCode(b.agentcode) , a.makedate , "
        +" b.UWDate "
        +" from  "
        +" Loprtmanager a , lbcont b , ldcom c "
        +" where c.comcode=b.managecom and a.otherno=b.ProposalContNo and b.conttype='1' and b.UWflag='9' and a.code<>'07' "
        +" and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and "
        +" substr ( b.ManageCom , 1 , length ( '"+tMngCom+"' ) )='"+tMngCom+"' "
        +" union "
        +" select ( select codename from ldcode where code=a.code and codetype='prttype' ) , "
        +" c.showname , a.PrtSeq , b.FirstTrialOperator , getUniteCode(b.agentcode) , a.makedate , "
        +" ( select makedate from es_doc_main where DocCode=a.prtseq and b. UWflag<>'9' ) " 
        +" from  "
        +" Loprtmanager a , lbcont b , ldcom c  "
        +" where c.comcode=b.managecom and a.otherno=b.ProposalContNo and b.conttype='1' and a.code<>'07' and b. UWflag<>'9' "
        +" and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and "
        +" substr ( b.ManageCom , 1 , length ( '"+tMngCom+"' ) )='"+tMngCom+"' "
        +" union  "
        +" select ( select codename from ldcode where code=a.code and codetype='prttype' ) , "
        +" c.showname , a.PrtSeq , b.FirstTrialOperator , getUniteCode(b.agentcode) , a.makedate , "
        +" b.UWDate "
        +" from "
        +" Loprtmanager a , lobcont b , ldcom c  "
        +" where c.comcode=b.managecom and a.otherno=b.ProposalContNo and b.conttype='1' and b.UWflag='9' and a.code<>'07' "
        +" and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and "
        +" substr ( b.ManageCom , 1 , length ( '"+tMngCom+"' ) )='"+tMngCom+"' "
        +" union "
        +" select ( select codename from ldcode where code=a.code and codetype='prttype' ) , "
        +" c.showname , a.PrtSeq , b.FirstTrialOperator , getUniteCode(b.agentcode) , a.makedate , "
        +" ( select makedate from es_doc_main where DocCode=a.prtseq and b. UWflag<>'9' ) "
        +" from "
        +" Loprtmanager a , lobcont b , ldcom c "
        +" where c.comcode=b.managecom and a.otherno=b.ProposalContNo and b.conttype='1' and a.code<>'07' and b. UWflag<>'9' "
        +" and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and "
        +" substr ( b.ManageCom , 1 , length ( '"+tMngCom+"' ) )='"+tMngCom+"' "
        +" with ur ";
        
        fm.querySql.value = tStrSQL;

         var oldAction = fm.action;
         fm.action = "BillStatisticSubDown.jsp";
         fm.submit();
         fm.action = oldAction;
         
//  var i = 0;
//  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//  //showSubmitFrame(mDebug); 
//	//fm.fmtransact.value = "PRINT";
//	fm.target = "f1print";
//	fm.all('op').value = '';
//	fm.submit();
//	showInfo.close();
         
 
}

