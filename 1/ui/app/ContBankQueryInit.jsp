<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContDeleteInit.jsp
//程序功能：个单整单删除
//创建日期：2004-12-06 11:10:36
//创建人  ：Zhangrong
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
%>                            

<script language="JavaScript">

function initForm()
{
  try
  { 	
    initGrid();
  }
  catch(re)
  {
    alert("ContUWInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="投保书号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="合同号";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="机构";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;  
      
      iArray[4]=new Array();
      iArray[4][0]="渠道";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="投保人";         		//列名
      iArray[5][1]="70px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
 
      iArray[6]=new Array();
      iArray[6][0]="联系地址";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="邮编";         		//列名
      iArray[7][1]="0px";            		//列宽
      iArray[7][2]=0;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="联系电话";         		//列名
      iArray[8][1]="0px";            		//列宽
      iArray[8][2]=0;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="生效日";          //列名
      iArray[9][1]="80px";  
      iArray[9][2]=100;              //列名
      iArray[9][3]=0;                //列名

      iArray[10]=new Array();
      iArray[10][0]="录入日期";                 //列名
      iArray[10][1]="80px";
      iArray[10][2]=100;                //列名
      iArray[10][3]=0;                //列名  
      
      iArray[11]=new Array();
      iArray[11][0]="状态";                 //列名
      iArray[11][1]="60px";
      iArray[11][2]=100;                //列名
      iArray[11][3]=0;                //列名  
      
      iArray[12]=new Array();
      iArray[12][0]="保费";                 //列名
      iArray[12][1]="60px";
      iArray[12][2]=100;                //列名
      iArray[12][3]=0;                //列名  
      
      iArray[13]=new Array();
      iArray[13][0]="网点代码";         		//列名
      iArray[13][1]="0px";            		//列宽
      iArray[13][2]=0;            			//列最大值
      iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="网点";         		//列名
      iArray[14][1]="0px";            		//列宽
      iArray[14][2]=0;            			//列最大值
      iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[15]=new Array();
      iArray[15][0]="业务员号码";         		//列名
      iArray[15][1]="0px";            		//列宽
      iArray[15][2]=0;            			//列最大值
      iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[16]=new Array();
      iArray[16][0]="业务员";         		//列名
      iArray[16][1]="0px";            		//列宽
      iArray[16][2]=0;            			//列最大值
      iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许,3表示隐藏      			

      Grid = new MulLineEnter( "fm" , "Grid" ); 
      //这些属性必须在loadMulLine前
      Grid.mulLineCount = 10;   
      Grid.displayTitle = 1;
      Grid.locked = 1;
      //Grid.canSel = 1;
      Grid.hiddenPlus = 1;
      Grid.hiddenSubtraction = 1;
      Grid.loadMulLine(iArray);
      
      //这些操作必须在loadMulLine后面
      //Grid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>