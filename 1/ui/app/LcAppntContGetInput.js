var showInfo;
// 查询
function submitForm() {

	var Signdate = fm.Signdate.value;
	var EndDate = fm.EndDate.value;
	var ManageCom=fm.ManageCom.value;
	if (Signdate == "" || EndDate == "") {
		alert("请选择签单日期的起始和终止日期");
		return false;
	}
	if (dateDiff(Signdate, EndDate, "M") > 12) {
		alert("统计期最多为12个月！");
		return false;
	}

	var strSQL=" Select Comcode 省级机构代码, Comname 省级机构名称,"
		+" Managecom 管理机构代码,Managecomname 管理机构名称,"
		+" Salechnl 渠道名称,Contno 保单号,"
		+" Mainriskcode 险种代码,"
		+" (Case"
		+" When Mainriskcode In ('230501') Then '康健无忧个人防癌疾病保险'"
		+" When Mainriskcode In ('330701') Then '健康人生个人护理保险（万能型，A款）'"
		+" When Mainriskcode In ('330801') Then '健康宝个人护理保险（万能型）'"
		+" When Mainriskcode In ('122601') Then '个人税收优惠型健康保险（万能型）A款'"
		+" Else '爱健康个人税收优惠型健康保险（万能型）A款' End ) 险种名称,"
		+" Sum(Amnt) 保额,Sum(Prem) 保费,Cvalidate 保单生效日,Signdate 保单签单日,"
		+" Appntno 投保人客户号,Appntname 投保人姓名,Appntsex 投保人性别,Age 年龄,"
		+" Idtype 投保人证件类型,Idno 投保人证件号,Mobile 联系电话,Postaladdress 联系地址,"
		+" Payintv 缴费频次,Icdcode Icd编码 ,Disresult Icd名称 "
		+" From (Select Substr(Lc.Managecom, 1, 4) Comcode,"
		+" (Select Name From Ldcom Where Comcode = Substr(Lc.Managecom, 1, 4)) Comname,"
		+" Lc.Managecom Managecom,"
		+" (Select Name From Ldcom Where Comcode = Lc.Managecom) Managecomname,"
		+" (Select Codename From ldcode where codetype='lcsalechnl' and code = Lc.Salechnl ) Salechnl,"
		+" Lc.Contno Contno,"
		+" Lp.Amnt Amnt,"
		+" Lp.Prem Prem,"
		+" Lc.Cvalidate Cvalidate,"
		+" Lc.Signdate Signdate,"
		+" Lca.Appntno Appntno,"
		+" Lca.Appntname Appntname,"
		+" (Select Codename From ldcode where codetype='sex' and code = Lca.Appntsex ) Appntsex,"
		+" (Year(Current Date) - Year(Lca.Appntbirthday)) Age,"
		+" (Select Codename From ldcode where codetype='idtype' and code = Lca.Idtype ) Idtype,"
		+" Lca.Idno Idno,"
		+" (Select Mobile From Lcaddress Where Customerno = Lca.Appntno And Addressno = Lca.Addressno) Mobile,"
		+" (Select Postaladdress From Lcaddress Where Customerno = Lca.Appntno And Addressno = Lca.Addressno) Postaladdress,"
		+" (Select Codename From ldcode where codetype='payintv' and code = Lp.Payintv ) Payintv,"
		+" (Select Disresult From Lcdiseaseresult Where Contno = Lp.Contno) Disresult,"
		+" (Select Icdcode From Lcdiseaseresult Where Contno = Lp.Contno) Icdcode,"
		+" (Select Codename From ldcode where codetype='stateflag' and code = Lc.Stateflag ) Stateflag,"
		+" (Case"
		+" When Riskcode In ( '331001', '230501') Then '230501'"
		+" When Riskcode In ('530501', '330801') Then '330801'"
		+" When Riskcode In ('530401', '330701') Then '330701'"
		+" Else  Riskcode  End ) Mainriskcode"
		+" From Lccont Lc"
		+" Inner Join Lcappnt Lca On Lc.Contno = Lca.Contno"
		+" Inner Join Lcpol Lp On Lc.Contno = Lp.Contno"
		+" Where 1 = 1 "
		+ getWherePart('Lp.Signdate', 'Signdate', '>=')
	    + getWherePart('Lp.Signdate', 'EndDate', '<=')
	    + getWherePart('lc.ManageCom', 'ManageCom', 'like')
		+" And Lp.Appflag = '1' "
		+" And Lp.stateflag = '1' "
		+" And Lp.Riskcode In ( '331001', '230501', '530501', '330801', '530401', '330701', '122601', '122901')"
		+" And Lp.Conttype = '1'"
		+"  ) Temp"
		+" Group By "
		+" Comcode,Comname,Managecom,Managecomname,Salechnl,Contno,Mainriskcode,Cvalidate,Signdate,Appntno,Appntname,Appntsex,Idtype,Idno,Mobile,Age,Payintv,Postaladdress,Disresult,Icdcode With Ur"
		
	fm.querySql.value = strSQL;
	fm.submit();

}