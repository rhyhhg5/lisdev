<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BPOIssueInit.jsp
//程序功能：用于查看外包反馈错误信息
//创建日期：2007-9-27 18:22
//创建人  ：xiaoxin
//更新记录：更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>                            

<script language="JavaScript">               

function initForm()
{
  try
  {
    initIssueGrid();
    easyQueryClick();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

// 错误信息列表的初始化
function initIssueGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=20;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="70px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="对象";         		//列名
      iArray[2][1]="50px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="名称";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="错误项";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();
      iArray[5][0]="错误类型";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=120;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[6]=new Array();                                                  
      iArray[6][0]="原内容";         		//列名                        
      iArray[6][1]="120px";            		//列宽                                
      iArray[6][2]=200;            			//列最大值                            
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[7]=new Array();                                                  
      iArray[7][0]="备注";         		//列名                        
      iArray[7][1]="200px";            		//列宽                                
      iArray[7][2]=200;            			//列最大值                            
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      IssueGrid = new MulLineEnter( "fm" , "IssueGrid" ); 
      //这些属性必须在loadMulLine前
      IssueGrid.mulLineCount = 10;   
      IssueGrid.displayTitle = 1;
      IssueGrid.locked = 1;
      IssueGrid.canSel = 0;
      IssueGrid.canChk = 0;
      IssueGrid.hiddenPlus=1;   
    	IssueGrid.hiddenSubtraction=1;
      IssueGrid.loadMulLine(iArray);  
      
      
      //这些操作必须在loadMulLine后面
      //IssueGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>