<html>
	<%
		//程序名称：修改保额
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：潘彬
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="LcpolAmntModifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LcpolAmntModifyInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./LcpolAmntModifySave.jsp" method=post name=fm target="fraSubmit">
			<table class="common">
				<tr class="common">
					<td class="title">
						保单号码
					</td>
					<td CLASS="input" >
						<input CLASS="common" name="GrpContNo" elementtype=nacessary />
					</td>
					<td class="title">
						被保人客户号码
					</td>
					<td CLASS="input" >
						<input class="common" name="InsuredNo" elementtype=nacessary />
					</td>
					<td class="title">
						险种编码
					</td>
					<td CLASS="input" >
						<input class="common" name="RiskCode" elementtype=nacessary />
					</td>
				</tr>
			</table>
			<input value="查  询" class=cssButton type=button onclick="return EasyQuery()">

			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1>
						<span id="spanLcpolAmntModifyGrid"> </span>
					</td>
				</tr>
			</table>
			<Div id="divPage" align=center style="display: 'none' ">
				<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
				<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
				<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
				<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
			</Div>
			<tr class="common">
				<td class="title">
					修改后保额
				</td>
				<td>
					<input class="common" name="Amnt" />
				</td>
				<td>
					<input value="修  改" name="ModifyButton" class="cssButton" type="button" onclick="updateClick()">
				</td>
			</tr>
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden id="fmAction" name="fmAction">
			<input type=hidden id="polno1" name="polno1">
		</form>

		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
