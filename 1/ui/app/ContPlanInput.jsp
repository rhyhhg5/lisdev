<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
String GrpContNo = request.getParameter("GrpContNo");
String LoadFlag = request.getParameter("LoadFlag");
String Resource = request.getParameter("Resource");
%>
<head>
<script>
GrpContNo="<%=GrpContNo%>";
LoadFlag="<%=LoadFlag%>";
 var Resource="<%=Resource%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="ContPlan.js"></SCRIPT>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
<SCRIPT src="ProposalAutoMove.js"></SCRIPT>
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ContPlanInit.jsp"%>
<title>团体保障计划定制 </title>

</head>
<body onload="initForm();">
	<form method=post name=fm target="fraSubmit" action="ContPlanSave.jsp">
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>合同信息</td>
			</tr>
		</table>
		
		<table  class= common align=center>
			<TR  class= common>
				<!--<TD  class= title>集体合同号</TD>
				<TD  class= input></TD>-->
					<Input class=readonly readonly name=GrpContNo value="<%=GrpContNo%>" type=hidden>
					<Input type=hidden name=ProposalGrpContNo>
					<input type=hidden name=mOperate>
					<input type=hidden name=LoadFrame value="<%=request.getParameter("LoadFrame")%>">
				
				<TD  class= title>管理机构</TD>
				<TD  class= input>
					<Input class=readonly readonly name=ManageCom>
				</TD>
				<TD  class= title>团体客户号</TD>
				<TD  class= input>
					<Input class= readonly readonly name=AppntNo>
				</TD>

				
				<TD  class= title>投保单位名称</TD>
				<TD  class= input>
					<Input class= readonly readonly name=GrpName>
				</TD>
			</TR>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>已存在保障计划</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanCodeGrid" ></span>
				</td>
			</tr>
		</table>
	
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>保障计划定制</td>
			</tr>
		</table>
		<table  class= common align=center>
			<TR  class= common>
				<TD  class= title>保障计划</TD>
				<TD  class= input>
					<Input name=ContPlanCodeName CLASS="code" type="hidden">
					<Input class="code" name=ContPlanCode readonly maxlength="2" verify="保障计划|notnull" onchange="ChangePlan();" CodeData="0|^1|A^2|B^3|C^4|D^5|E^6|F^7|G^8|H^9|I^10|J^11|K^12|L^13|M^14|N^15|O^16|P^17|Q^18|R^19|S^20|T^21|U^22|V^23|W^24|X^25|Y^26|Z" ondblclick="showCodeListEx('ContPlanCode',[this,ContPlanCodeName],[1,0],null,null,null,1);" onkeyup="showCodeListEx('ContPlanCode',[this,ContPlanCodeName],[1,0],null,null,null,1);">
				</TD>
				<TD  class= title>人员类别</TD>
				<TD  class= input>
					<input class=common name=ContPlanName verify="人员类别|notnull&len<=100">
				</TD>
				<TD  class= title id="AgeTitle" style="display: 'none'">年龄区间</TD>
				<TD  class= input id="AgeInput" style="display: 'none'">
        			<Input class=codeno name=AgeRange readonly=true CodeData="0|^0|(0~18周岁)^1|(19~64周岁)^2|(65~74周岁 )" ondblclick="return showCodeListEx('AgeRange',[this,AgeRangeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('AgeRange',[this,AgeRangeName],[0,1],null,null,null,1);"><input class=codename name=AgeRangeName readonly=true > 		     		
				</TD>
			</tr>
			<tr class=common>
				<TD  class= title>参保人数</TD>
				<TD  class= input>
					<Input class=common name=Peoples3 verify="参保人数|notnull&int">
				</TD> 		
				<TD  class= title>应保人数</TD>
				<TD  class= input>
					<Input class=common name=Peoples2 verify="应保人数|notnull&int">
				</TD> 			
					<Input type=hidden name=PlanSql>
			</TR>
			<TR>
				

				<TD id=divriskcodename class= title>险种代码</TD>
				<TD id=divriskcode  class= input>
					<Input class=codeNo name=RiskCode readonly=true ondblclick="return showCodeList('GrpRisk',[this,RiskCodeName,RiskFlag],[0,1,3],null,fm.GrpContNo.value,'b.GrpContNo',1);" onkeyup="return showCodeListKey('GrpRisk',[this,RiskCodeName,RiskFlag],[0,1,3],null,fm.GrpContNo.value,'b.GrpContNo');"><Input class=codename name="RiskCodeName" readonly=true>
					<input type=hidden name=RiskFlag>
				</TD>
				<TD id=divmainriskname style="display:none" class=title>主险代码</TD>
				<TD id=divmainrisk style="display:none" class=input>
					<Input class=codeno maxlength=6 name=MainRiskCode ondblclick="return showCodeList('GrpMainRisk',[this,MainRiskCodeName],[0,1],null,fm.GrpContNo.value,'b.GrpContNo',1);" onkeyup="return showCodeListKey('GrpMainRisk',[this,MainRiskCodeName],[0,1],null,fm.GrpContNo.value,'b.GrpContNo');"><Input class=codename name="MainRiskCodeName" readonly=true>
				</TD>
				<TD id=divcontplanname style="display:none" class=title>保险套餐</TD>
				<TD id=divcontplan style="display:none" class=input>
					<Input class=codeno maxlength=6 name=RiskPlan ondblclick="return showCodeList('RiskPlan',[this,RiskPlanName],[0,1],null,'','',1);" onkeyup="return showCodeListKey('RiskPlan',[this,RiskPlanName],[0,1],null,'','');"><Input class=codename name="RiskPlanName" readonly=true>
				</TD>
				<TD  class= title id="SumPremTitle">总保费</TD>
				<TD  class= input id="SumPremInput">
					<Input class=common name=SumPrem verify="总保费|NUM" onblur="QueryOnBlur()">
				</TD>
				<TD  class= title id="PlanTitle" style="display: 'none'">险种计划</TD>
				<TD  class= input id="PlanInput" style="display: 'none'">
        			<Input class=codeno name=Plan readonly=true CodeData="0|^1|计划一^2|计划二" ondblclick="return showCodeListEx('Plan',[this,PlanName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Plan',[this,PlanName],[0,1],null,null,null,1);"><input class=codename name=PlanName readonly=true > 		     		
				</TD>
				<TD  class= title id="MultTitle" style="display: 'none'">档次</TD>
				<TD  class= input id="MultInput" style="display: 'none'">
					<Input class=common name=Mult verify="档次|NUM">
				</TD>
				<TD  class= title  id="AccPremTitle" style="display: 'none'" >个人账户保费</TD>
				<TD  class= input  id="AccPremInput" style="display: 'none'">
					<Input class=common name=AccPrem verify="个人账户保费|NUM">
				</TD>
				<td  class= title  id="QCPremTitle" style="display: 'none'" >全残护理个人保费</td>
				<td  class= input  id="QCPremInput" style="display: 'none'">
					<Input class=common name=QCPrem verify="全残护理个人保费|NUM" />
				</td>
				<TD  class= title  id="RiskAmntTitle" style="display: 'none'" >险种保额</TD>
				<TD  class= input  id="RiskAmntInput" style="display: 'none'">
					<Input class=common name=RiskAmnt verify="险种保额|NUM">
				</TD>
				<TD  class= title  id="RiskCotysTitle" style="display: 'none'" >份数</TD>
				<TD  class= input  id="RiskCotysInput" style="display: 'none'">
					<Input class=common name=RiskCotys verify="份数|NUM" onkeyup="inputCopys()">
				</TD>
			</TR>
			<!-- add by zjd 险种460301 使用 20150325 -->
			<tr class=common style="display: 'none'" id="BusinessTypeTR">
			    <TD id=businesstype1 class= title>行业类别</TD>
				<TD id=businesstype2  class= input>
					<Input class=codeNo name=BusinessType  ondblclick="return showCodeList('contplanbustype',[this,BusinessTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('contplanbustype',[this,BusinessTypeName],[0,1],null,null,null,1);"><Input class=codename name="BusinessTypeName" readonly=true>
				</TD>
			</tr>
			<TR class=common style="display: 'none'" id="AreaCode">
				<TD  class= title>方案编码</TD>
				<TD  class= input>
					<Input class= "codeno"  name=InsuPlanCode  ondblclick="return showCodeList('InsuPlanCode',[this,InsuPlanCodeName],[0,1],null,fm.JXQQB.value,'1',1);" onkeyup="return showCodeListKey('InsuPlanCode',[this,AgentGroupName],[0,1],null,fm.JXQQB.value,'1',1);" ><Input class=codename  name=InsuPlanCodeName readonly>
				</TD> 		
				<TD  class= title>保障地区</TD>
				<TD  class= input>
					<Input class=common name=InsuArea>
				</TD>
				<TD  id = 'SpecialGetRate' class= title>特别医院给付比例</TD>
				<TD  id = 'SpecialGetRateName' class= input>
					<Input class=common name=SpecialGetRate >
				</TD>
			</TR>
			<tr class=common style="display: 'none'" id="AreaCode1">
				<TD  class= title  id="RiskYearAmntTitle" style="display: 'none'" >年度限额</TD>
				<TD  class= input  id="RiskYearAmntInput" style="display: 'none'">
					<Input class=common name=YearAmnt verify="年度限额|NUM">
				</TD> 			
				<TD  class= title>特别医院给付比例</TD>
				<TD  class= input>
					<Input class=common name=SpecialGetRate1 >
				</TD>
			</tr>
			<TR class=common style="display: 'none'" id="AreaCode160306">
				<TD  class= title>方案编码</TD>
				<TD  class= input>
					<Input class= "codeno"  name=InsuPlanCode160306  ondblclick="return showCodeList('InsuPlanCode160306',[this,InsuPlanCodeName1],[0,1],null,null,null,null);" onkeyup="return showCodeListKey('InsuPlanCode160306',[this,InsuPlanCodeName1],[0,1],null,null,null,null);" ><Input class=codename  name=InsuPlanCodeName1 readonly>
				</TD> 		
			</TR>
			<TR class=common style="display: 'none'" id="AreaCodeQDWZ">
				<TD  class= title>方案编码</TD>
				<TD  class= input>
					<Input class= "codeno"  name=InsuPlanCodeQDWZ  ondblclick="return showCodeList('InsuPlanCodeQDWZ',[this,InsuPlanCodeName2],[0,1],null,fm.all('ManageCom').value.substr(0,4),'ComCode',1);" onkeyup="return showCodeListKey('InsuPlanCodeQDWZ',[this,InsuPlanCodeName2],[0,1],null,fm.all('ManageCom').value.substr(0,4),'ComCode',1);" ><Input class=codename  name=InsuPlanCodeName2 readonly>
				</TD> 		
			</TR>
		</table>
		<Div id= "divRiskPlanQuery" style= "display: ''" align= left> 
		<Input VALUE="险种责任查询" class=cssButton id=PlanDuty name=PlanDuty TYPE=button onclick="QueryDutyClick();" disabled="disabled">
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanDutyGrid" ></span>
				</td>
			</tr>
		</table>
			</div>
		<Input VALUE="定义保障计划要素" class=cssButton id=PlanCalFactor name=PlanCalFactor TYPE=button onclick="AddContClick();" disabled="disabled">
		<!--INPUT VALUE="定制管理费" class="cssButton"  TYPE=button onclick="ShowManageFee();"-->
		<!--table class=common>
			<TR  class= common>
				<TD>
					<Input VALUE="添加险种到保障计划" class=cssButton  TYPE=button onclick="AddContClick();">
				</TD>
			</TR>
			<TR  class= common>
				<TD  class= title>保障计划类型</TD>
				<TD  class= input>
					<Input class="code" name=ContPlanType ondblclick="showCodeList('contplantype',[this]);" onkeyup="showCodeListKey('contplantype',[this]);">
				</TD>
			</TR>
		</table-->
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>保障计划详细信息</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanGrid" style="display: ''"></span>
				</td>
			</tr>
		</table>
		<div id="divManageFee" style="display: 'none'">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanManageFeeGrid" ></span>
					</td>
				</tr>
			</table>
		</div>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanRiskPremGrid" style="display: ''"></span>
				</td>
			</tr>
		</table>
		<div id="divManageFee" style="display: 'none'">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanContPlanPremGrid" ></span>
					</td>
				</tr>
			</table>
	</div>
	<div id= "divRisk163001" style= "display: 'none'">
		<table>
		 <TR>
	          <TD colspan=6>
	          	<span style="font-weight:normal;color:red;font-size:12px">
	          	注释：163001险种的责任（医疗费用）的要素（限额）只能为万元的整数倍，最低1万最高50万，责任（医疗费用）的要素（免赔额）只允许录入0、100、500、1000，录入不符合要求将无法计算保费。          	
	          	</span>
	          </TD>
	        </TR>  
		</table>
	</div>
	<div id= "divRisk163002" style= "display: 'none'">
		<table>
		 <TR>
	          <TD colspan=6>
	          	<span style="font-weight:normal;color:red;font-size:12px">
	          	注释：163002险种的责任（医疗费用）的要素（限额）只能为万元的整数倍，最低1万最高50万，责任（医疗费用）的要素（免赔额）只允许录入0、100、500、1000，录入不符合要求将无法计算保费。          	
	          	</span>
	          </TD>
	        </TR>  
		</table>
	</div>
    <Div id= "divRiskPlanSave" style= "display: ''" align= right> 
      <hr>
    	<INPUT VALUE="保障计划查询" class="cssButton"  style= "display: 'none'" TYPE=button onclick="">
    	<INPUT VALUE="保障计划保存" class="cssButton"   TYPE=button onclick="submitForm();">
    	<INPUT VALUE="保障计划修改" class="cssButton"   TYPE=button onclick="UptContClick();">
    	<!--INPUT VALUE="测试beforeSubmit2" class="cssButton"   TYPE=button onclick="beforeSubmit2();"-->
    	<INPUT VALUE="保障计划删除" class="cssButton"   TYPE=button onclick="DelContClick();">
    	<hr>
    </Div>
		<Div  id= "divRiskPlanRela" style= "display: ''" align= left> 
  		<INPUT VALUE="上一步" class="cssButton"  TYPE=button onclick="returnparent();">
  		<INPUT VALUE="伤残给付比例查询" class="cssButton"  TYPE=button onclick="displayDisRate();">
  		<!--INPUT VALUE="保障计划要约录入" class="cssButton"  TYPE=button onclick="nextstep();"-->
  		<INPUT VALUE="制订约定缴费计划" class="cssButton"   TYPE=button onclick="PlanCodeClick();">
        <INPUT VALUE="制订被保人共用保额" class="cssButton" name="ShareAmnt" TYPE=button onclick="ShareAmntClick();">
        <INPUT VALUE="制订伤残等级给付比例" class="cssButton" name="GradRate" TYPE=button onclick="GradRateClick();">
		</Div>
		<div id= "divRiskPlanRela2" style= "display: none " align= left>
		<br>
		<INPUT VALUE="制订被保人共用保额" class="cssButton" name="ShareAmnt" TYPE=button onclick="ShareAmntClick();" >
		<INPUT VALUE="查询伤残等级给付比例" class="cssButton" name="GradRate" TYPE=button onclick="GradRateClick();">
		</div>
    <div id="div1" style="display : 'none'">2005-5-13 14:29
      <TD  class= title>计划类别</TD>
        <TD  class= input>
        <Input class=codeno name=RiskPlan1 CodeData='0|^0|非固定计划^1|保险套餐' value=0 ondblclick="return showCodeListEx('RiskPlan1',[this,RiskPlan1Name],[0,1]);" onkeyup="return showCodeListKeyEx('RiskPlan1',[this,RiskPlan1Name],[0,1]);"><Input class=codename name="RiskPlan1Name" readonly=true value="非固定计划">
      </TD>
    </div>
	<INPUT  type=hidden name="Resource" value=""> 
	<INPUT  type=hidden name="JXQQB" value="0"> 
	<INPUT  type=hidden name="JXQQY" value="0">   
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>