<%
//程序名称：ProposalApproveInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
	String tActivityID = request.getParameter("ActivityID");	
	String tProcessID = request.getParameter("ProcessID");
	String tSubType = request.getParameter("SubType");
%>                            

<script language="JavaScript">

var tActivityID;
var tProcessID;
var tSubType;
  
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  tActivityID = "<%=tActivityID%>";
  if(tActivityID == null || tActivityID == "" || tActivityID == "null")
  {
    tActivityID = "0000001001";
  }
  
  tProcessID = "<%=tProcessID%>";
  if(tProcessID == null || tProcessID == "" || tProcessID == "null")
  {
    tProcessID = "0000000003";
  }
  
  tSubType = "<%=tSubType%>";
  if(tSubType == null || tSubType == "" || tSubType == "null")
  {
    tSubType = "TB01";
  }
  try
  {                                   
	// 保单查询条件
    fm.all('ProposalNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	  initPolGrid();

        initPendingContGrid();

	  fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="投保单号";         		//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="印刷号";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="投保人";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="录入员";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="录入日期";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[6]=new Array();
      iArray[6][0]="工作流任务号";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
              
      iArray[7]=new Array();
      iArray[7][0]="工作流子任务号";         		//列名
      iArray[7][1]="0px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
             
      iArray[8]=new Array();
      iArray[8][0]="机构名称";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[9]=new Array();
      iArray[9][0]="扫描日期";         		//列名
      iArray[9][1]="150px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="外包错误反馈";         		//列名
      iArray[10][1]="80";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[10][21]="HaseErrorName";
      
      iArray[11]=new Array();
      iArray[11][0]="状态";
      iArray[11][1]="80";
      iArray[11][2]=100;
      iArray[11][3]=0;
      
      iArray[12]=new Array();
      iArray[12][0]="销售渠道";
      iArray[12][1]="60";
      iArray[12][2]=60;
      iArray[12][3]=0;
      iArray[12][3]="SaleChnl";
      
      iArray[13]=new Array();
      iArray[13][0]="是否银行在途";
      iArray[13][1]="80";
      iArray[13][2]=80;
      iArray[13][3]=0;
      iArray[13][3]="BankOnTheWayFlag";
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


// 被保人信息列表的初始化
function initSubInsuredGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="连带被保人客户号";    	//列名
      iArray[1][1]="180px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][7]="showSubInsured";
      iArray[1][8]="['SubInsured']";        //是否使用自己编写的函数 

      iArray[2]=new Array();
      iArray[2][0]="姓名";         			//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="性别";         			//列名
      iArray[3][1]="140px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="出生日期";         		//列名
      iArray[4][1]="140px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="与被保人关系";         		//列名
      iArray[5][1]="160px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][4]="Relation";

      SubInsuredGrid = new MulLineEnter( "fm" , "SubInsuredGrid" ); 
      //这些属性必须在loadMulLine前
      SubInsuredGrid.mulLineCount = 2;   
      SubInsuredGrid.displayTitle = 1;
      //SubInsuredGrid.tableWidth = 200;
      SubInsuredGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 受益人信息列表的初始化
function initBnfGrid() {                               
  var iArray = new Array();

  try {
    iArray[0]=new Array();
    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";		//列宽
    iArray[0][2]=10;			//列最大值
    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许
  
    iArray[1]=new Array();
    iArray[1][0]="受益人类别"; 		//列名
    iArray[1][1]="80px";		//列宽
    iArray[1][2]=40;			//列最大值
    iArray[1][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="BnfType";
    iArray[1][9]="受益人类别|notnull&code:BnfType";
   
    iArray[2]=new Array();
    iArray[2][0]="受益人姓名"; 	//列名
    iArray[2][1]="80px";		//列宽
    iArray[2][2]=40;			//列最大值
    iArray[2][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[2][9]="受益人姓名|len<=20";//校验
   
    iArray[3]=new Array();
    iArray[3][0]="性别"; 	//列名
    iArray[3][1]="30px";		//列宽
    iArray[3][2]=100;			//列最大值
    iArray[3][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="sex";
    iArray[3][9]="性别|code:sex";//校验
  
    iArray[4]=new Array();
    iArray[4][0]="证件类型"; 		//列名
    iArray[4][1]="60px";		//列宽
    iArray[4][2]=80;			//列最大值
    iArray[4][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[4][4]="IDType";
    iArray[4][9]="证件类型|code:IDType";
  
    iArray[5]=new Array();
    iArray[5][0]="证件号码"; 		//列名
    iArray[5][1]="150px";		//列宽
    iArray[5][2]=80;			//列最大值
    iArray[5][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[5][9]="证件号码|len<=20";
  
    iArray[6]=new Array();
    iArray[6][0]="出生日期"; 		//列名
    iArray[6][1]="80px";		//列宽
    iArray[6][2]=80;			//列最大值
    iArray[6][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[6][9]="出生日期|date";
  
  
    iArray[7]=new Array();
    iArray[7][0]="与被保人关系"; 	//列名
    iArray[7][1]="90px";		//列宽
    iArray[7][2]=100;			//列最大值
    iArray[7][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[7][4]="Relation";
    iArray[7][9]="与被保人关系|code:Relation";
  
    iArray[8]=new Array();
    iArray[8][0]="受益比例"; 		//列名
    iArray[8][1]="60px";		//列宽
    iArray[8][2]=80;			//列最大值
    iArray[8][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[8][9]="受益比例|num&len<=10";
  
    iArray[9]=new Array();
    iArray[9][0]="受益顺序"; 		//列名
    iArray[9][1]="60px";		//列宽
    iArray[9][2]=80;			//列最大值
    iArray[9][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[9][4]="OccupationType";
    iArray[9][9]="受益顺序|code:OccupationType";
  
    iArray[10]=new Array();
    iArray[10][0]="联系地址"; 		//列名
    iArray[10][1]="400px";		//列宽
    iArray[10][2]=240;			//列最大值
    iArray[10][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[10][9]="通讯地址|len<=80";
  
    iArray[11]=new Array();
    iArray[11][0]="邮编"; 		//列名
    iArray[11][1]="60px";		//列宽
    iArray[11][2]=80;			//列最大值
    iArray[11][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[11][9]="邮编|zipcode";
  
    iArray[12]=new Array();
    iArray[12][0]="电话"; 		//列名
    iArray[12][1]="100px";		//列宽
    iArray[12][2]=80;			//列最大值
    iArray[12][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[12][9]="电话|len<=18";
  
    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" ); 
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 2; 
    BnfGrid.displayTitle = 1;
    BnfGrid.loadMulLine(iArray); 
  
    //这些操作必须在loadMulLine后面

  } catch(ex) {
    alert(ex);
  }
}

// 告知信息列表的初始化
function initImpartGrid() {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="告知客户类型";         		//列名
      iArray[1][1]="90px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="CustomerType";
      iArray[1][9]="告知客户类型|len<=18";

      iArray[2]=new Array();
      iArray[2][0]="告知编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ImpartCode";
      iArray[2][9]="告知编码|len<=4";

      iArray[3]=new Array();
      iArray[3][0]="告知版别";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][4]="ImpartVer";
      iArray[3][9]="告知版别|len<=5";

      iArray[4]=new Array();
      iArray[4][0]="告知内容";         		//列名
      iArray[4][1]="360px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="填写内容";         		//列名
      iArray[5][1]="200px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][9]="填写内容|len<=200";

      ImpartGrid = new MulLineEnter( "fm" , "ImpartGrid" ); 
      //这些属性必须在loadMulLine前
      ImpartGrid.mulLineCount = 2;   
      ImpartGrid.displayTitle = 1;
      //ImpartGrid.tableWidth   ="500px";
      ImpartGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ImpartGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 特别约定信息列表的初始化
function initSpecGrid() {                               
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="特约类型";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="SpecType";
      iArray[1][9]="特约类型|len<=5";

      iArray[2]=new Array();
      iArray[2][0]="特约编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="SpecCode";
      iArray[2][9]="特约编码|len<=5";

      iArray[3]=new Array();
      iArray[3][0]="特约内容";         		//列名
      iArray[3][1]="540px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="特约内容|len<=255";
      
      SpecGrid = new MulLineEnter( "fm" , "SpecGrid" );
      //这些属性必须在loadMulLine前
      SpecGrid.mulLineCount = 2;   
      SpecGrid.displayTitle = 1;
      SpecGrid.loadMulLine(iArray);           
      //这些操作必须在loadMulLine后面
      //SpecGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

//责任列表
function initDutyGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="责任编码";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="责任名称";         			//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="保费";         			//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保额";         			//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="交费年期";         			//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="领取年期";         			//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
 
      DutyGrid = new MulLineEnter( "fm" , "DutyGrid" ); 
      //这些属性必须在loadMulLine前
      DutyGrid.mulLineCount = 0;   
      DutyGrid.displayTitle = 1;
      DutyGrid.canChk = 1;
      DutyGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //DutyGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


function emptyForm() {
	//emptyFormElements();     //清空页面所有输入框，在COMMON。JS中实现
   
	initSubInsuredGrid();
	initBnfGrid();
	initImpartGrid();
	initSpecGrid();
	initDutyGrid();
	
	//SubInsuredGrid.clearData();
	//BnfGrid.clearData();
	//ImpartGrid.clearData();
	//SpecGrid.clearData();
	//DutyGrid.clearData();
	spanDutyGrid.innerHTML="";
}


/**
 * 待回复保单列表
 */
function initPendingContGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=10;
        iArray[0][3]=0;
        
        iArray[1]=new Array();
        iArray[1][0]="投保单号";
        iArray[1][1]="150px";
        iArray[1][2]=100;
        iArray[1][3]=3;
        
        iArray[2]=new Array();
        iArray[2][0]="印刷号";
        iArray[2][1]="120px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="投保人";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="录入员";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="录入日期";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="工作流任务号";
        iArray[6][1]="0px";
        iArray[6][2]=200;
        iArray[6][3]=3;
              
        iArray[7]=new Array();
        iArray[7][0]="工作流子任务号";
        iArray[7][1]="0px";
        iArray[7][2]=200;
        iArray[7][3]=3;
             
        iArray[8]=new Array();
        iArray[8][0]="机构名称";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        
        iArray[9]=new Array();
        iArray[9][0]="扫描日期";
        iArray[9][1]="150px";
        iArray[9][2]=100;
        iArray[9][3]=0;
        
        iArray[10]=new Array();
        iArray[10][0]="外包错误反馈";
        iArray[10][1]="80";
        iArray[10][2]=100;
        iArray[10][3]=0;
        iArray[10][21]="HaseErrorName";
        
        iArray[11]=new Array();
        iArray[11][0]="状态";
        iArray[11][1]="80";
        iArray[11][2]=100;
        iArray[11][3]=0;
        
        iArray[12]=new Array();
        iArray[12][0]="业务员代码";
        iArray[12][1]="80";
        iArray[12][2]=100;
        iArray[12][3]=0;
        
        iArray[13]=new Array();
        iArray[13][0]="业务员";
        iArray[13][1]="80";
        iArray[13][2]=100;
        iArray[13][3]=0;
        
        iArray[14]=new Array();
        iArray[14][0]="销售机构代码";
        iArray[14][1]="80";
        iArray[14][2]=100;
        iArray[14][3]=0;
        
        iArray[15]=new Array();
        iArray[15][0]="销售机构";
        iArray[15][1]="80";
        iArray[15][2]=100;
        iArray[15][3]=0;
        
        iArray[16]=new Array();
        iArray[16][0]="销售渠道";
        iArray[16][1]="60";
        iArray[16][2]=60;
        iArray[16][3]=0;
        iArray[16][3]="SaleChnl";
        
        PendingContGrid = new MulLineEnter("fm", "PendingContGrid");
        PendingContGrid.mulLineCount = 0;
        PendingContGrid.displayTitle = 1;
        PendingContGrid.locked = 1;
        PendingContGrid.canSel = 1;
        PendingContGrid.hiddenPlus = 1;
        PendingContGrid.hiddenSubtraction = 1;
        PendingContGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>