<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>

<script language="JavaScript">
function initForm() {
	try {
	    initInpBox(); 
	    initAppntGrid();
	    //initProjectInfo();	  
	    if(tGrpcontno!=""){
        fm.GrpContNo.value=tGrpcontno;
        }else{
        alert("团体保单号传输错误！");
        return;
        }
	    
	    if(tflag!="" && tflag!=null && tflag!="null" ){
	        fm.all("updateApp").style.display="none";
	     }else{
	    	 fm.all("updateApp").style.display="";
	     }
        
	} catch (re) {
		alert(re.message);
	}
}
function initInpBox() {}

var AppntGrid;
function initAppntGrid() {
	var iArray = new Array();
	try {
		iArray[0] = new Array();
		iArray[0][0] = "序号"; //列名
		iArray[0][1] = "30px"; //列名    
		iArray[0][3] = 0; //列名
		//列名

		iArray[1] = new Array();
		iArray[1][0] = "投保人姓名"; //列名
		iArray[1][1] = "80px"; //列宽
		iArray[1][2] = 200; //列最大值
		iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许 

		iArray[2] = new Array();
		iArray[2][0] = "投保人性别";
		iArray[2][1] = "80px";
		iArray[2][2] = 200;
		iArray[2][3] = 0;

		iArray[3] = new Array();
		iArray[3][0] = "投保人出生日期"; //列名
		iArray[3][1] = "80px"; //列宽
		iArray[3][2] = 200; //列最大值
		iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许
		
		iArray[4] = new Array();
		iArray[4][0] = "投保人证件类型"; //列名
		iArray[4][1] = "80px"; //列宽
		iArray[4][2] = 200; //列最大值
		iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许  

		iArray[5] = new Array();
		iArray[5][0] = "投保人证件号码"; //列名
		iArray[5][1] = "80px"; //列宽
		iArray[5][2] = 200; //列最大值
		iArray[5][3] = 0; //是否允许输入,1表示允许，0表示不允许  

		iArray[6] = new Array();
		iArray[6][0] = "被保人姓名"; //列名
		iArray[6][1] = "80px"; //列宽
		iArray[6][2] = 200; //列最大值
		iArray[6][3] = 0; //是否允许输入,1表示允许，0表示不允许

		iArray[7] = new Array();
		iArray[7][0] = "被保人性别"; //列名
		iArray[7][1] = "80px"; //列宽
		iArray[7][2] = 200; //列最大值
		iArray[7][3] = 0; //是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
	    iArray[8][0]="被保人出生日期";              //列名
	    iArray[8][1]="80px";            	//列宽
	    iArray[8][2]=200;            			//列最大值
	    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[9]=new Array();
	    iArray[9][0]="被保人证件类型";              //列名
	    iArray[9][1]="80px";            	//列宽
	    iArray[9][2]=200;            			//列最大值
	    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[10]=new Array();
	    iArray[10][0]="被保人证件号码";              //列名
	    iArray[10][1]="80px";            	//列宽
	    iArray[10][2]=200;            			//列最大值
	    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[11]=new Array();
        iArray[11][0]="被保人序号";              //列名
        iArray[11][1]="80px";               //列宽
        iArray[11][2]=200;                      //列最大值
        iArray[11][3]=3;                        //是否允许输入,1表示允许，0表示不允许
        
        iArray[12]=new Array();
        iArray[12][0]="性别";              //列名
        iArray[12][1]="80px";               //列宽
        iArray[12][2]=200;                      //列最大值
        iArray[12][3]=3;                        //是否允许输入,1表示允许，0表示不允许
        
        iArray[13]=new Array();
        iArray[13][0]="证件类型";              //列名
        iArray[13][1]="80px";               //列宽
        iArray[13][2]=200;                      //列最大值
        iArray[13][3]=3;                        //是否允许输入,1表示允许，0表示不允许
	    
		AppntGrid = new MulLineEnter("fm", "AppntGrid");
		//设置Grid属性
		AppntGrid.mulLineCount = 1;
		AppntGrid.displayTitle = 1;
		AppntGrid.locked = 1;
		AppntGrid.canSel = 1;
		AppntGrid.canChk = 0;
		AppntGrid.hiddenSubtraction = 1;
		AppntGrid.hiddenPlus = 1;
        AppntGrid.selBoxEventFuncName ="reportDetailClick";
		/** 
		 ProjectUW.hiddenPlus=0;
		 ProjectUW.hiddenSubtraction=0;
		 */
		AppntGrid.loadMulLine(iArray);
	} catch (ex) {
		alert(ex);
	}
}
</script>
