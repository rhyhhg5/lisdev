var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    alert(content); 
  }
  else
  { 
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	var PrtNo = fm.PrtNo.value;
	var header = PrtNo.substr(0,2);
	k++;
	var ManageCom=fm.ManageCom.value;
	var InputDate=fm.MakeDate.value;
	var SaleChnl=fm.SaleChnl.value;
	var InputDateSql=" and a.inputdate='"+InputDate+"'";
	var SaleChnlSql=" and a.SaleChnl='"+SaleChnl+"'";
	var PrtnoSql = " and a.prtno  like '%"+PrtNo+"%'"
	if(header == "PD"){
		var strSQL = "";
		strSQL = "select a.ContNo,a.ProposalContNo,a.PrtNo,a.ManageCom,getUniteCode(a.Agentcode),a.AppntName,CodeName('unitesalechnl', a.SaleChnl),a.inputdate "
				+" from LCCont a left join lccontsub b on a.prtno=b.prtno where "+k+" = "+k
				+" and a.signdate is not null and a.conttype = '1' and b.prtno is not null and (a.printcount = '0' or a.printcount = '-1') and (b.ScanCheckFlag <> '1' Or b.ScanCheckFlag is null)  and a.managecom like '"+ManageCom+"%' and a.prtno like 'PD%'";
		if(SaleChnl != null && SaleChnl !=""){
			strSQL=strSQL+SaleChnlSql;
		}
		if(InputDate != null && InputDate !=""){
			strSQL=strSQL+InputDateSql;
		}
		if(PrtNo != null && PrtNo !=""){
			strSQL=strSQL+PrtnoSql;
		}

		  //查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	}else{
		var strSQL = "";
		strSQL = "select a.ContNo,a.ProposalContNo,a.PrtNo,a.ManageCom,getUniteCode(a.Agentcode),a.AppntName,CodeName('unitesalechnl', a.SaleChnl),a.inputdate "
				+" from LCCont a left join lccontsub b on a.prtno=b.prtno where "+k+" = "+k
				+" and  a.signdate is not null and a.conttype = '1' and b.prtno is not null and (a.printcount = '0' or a.printcount = '-1') and (b.ScanCheckFlag <> '1' Or b.ScanCheckFlag is null)  and a.managecom like '"+ManageCom+"%' and a.prtno like 'PD%'";
		if(SaleChnl != null && SaleChnl !=""){
			strSQL=strSQL+SaleChnlSql;
		}
		if(InputDate != null && InputDate !=""){
			strSQL=strSQL+InputDateSql;
		}
		if(PrtNo != null && PrtNo !=""){
			strSQL=strSQL+PrtnoSql;
		}
		  //查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	}
	// 书写SQL语句
//	k++;
//	var strSQL = "";
//	strSQL = "select ContNo,ProposalContNo,PrtNo,ManageCom,getUniteCode(Agentcode),AppntName from LCCont where "+k+" = "+k
//				 + " and conttype = '1' and prtno like '%" + PrtNo + "%' and signdate is not null and printcount = '0' ";
//
//	  //查询SQL，返回结果字符串
//  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("查询失败！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		alert("result:"+arrResult);
	} // end of if
}

function getStatus()
{
//  var i = 0;
//  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  //showSubmitFrame(1);
//  fm.submit(); //提交
	var i = 0;
	var checkFlag = 0;
	var state = "0";
	var transprtno="";
	for (i = 0; i < PolGrid.mulLineCount; i++) {
		if (PolGrid.getSelNo(i)) {
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		transprtno = PolGrid.getRowColData(checkFlag - 1, 3);
		if(transprtno == "" || transprtno == null){
			alert("请先选择一条保单信息！");
		}
		window.open("./ScanXInputMain.jsp?prtNo="+transprtno+"&LoadFlag=5","", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");    
		initPolGrid();
	}else{
		alert("请先选择一条保单信息！");
		return false;
	}
//  if(transprtno == ""&&transprtno == null){
//	  alert("请选择一个保单后，再进行复核操作！");
//	  return false;
//  }
//  alert("1");
//  window.open("./ScanXInputMain.jsp?prtNo="+transprtno+"&LoadFlag=5","", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");    
//  initPolGrid();
//  transprtno = "";
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,*,0,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}