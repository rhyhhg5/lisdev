<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：SchedulePage.jsp
//程序功能：信息显示页面
//创建日期：2005-05-10
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//             yangming   2005-05-10    修改
%>

  <%@page import="com.sinosoft.utility.*"%>

<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<html>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="LCDiskApplyInput.js"></SCRIPT>
  <title>信息反馈</title>
  
  <%--页面样式--%>
  <link rel="stylesheet" type="text/css" href="../css/Project.css">
<%
  String SUCCESS    = "S";  //成功
  String FAILURE    = "F";  //失败
  String COMMON     = "C";  //一般信息

  String Picture    = request.getParameter("picture");
  String Content    = StrTool.unicodeToGBK(request.getParameter("content"));
  String BatchNo    = request.getParameter("BatchNo");
  /*****************added by dingzhong for time out check*********************/
    
  boolean bIsOutTime = false;
  if (session == null) {
      bIsOutTime = true;
  } else  {
      GlobalInput tG1 = (GlobalInput)session.getValue("GI");
      if (tG1 == null) {
          bIsOutTime = true;
      } else {
          String  userCode = tG1.Operator;
    	  String comCode =tG1.ComCode;
    	  String manageCom = tG1.ManageCom;
	    			
	  if ((userCode.length()==0) || (userCode.compareTo("")==0)||
     	  (comCode.length()==0) || (comCode.compareTo("")==0) ||
     	  (manageCom.length()==0) || (manageCom.compareTo("") == 0)) {
     	      bIsOutTime = true;
     	  }
     }
  }	  
         
  if (bIsOutTime) {
      Content = "页面超时，请重新登录.";
  }   
  /*********************************************************************/    
  
  String strPicture ="";                               
%>

</head>
<body class="interface">
  <h1><center>系统信息</center></h1>
  <br>

<%
  if(Picture==null)
    Picture = COMMON;

  if(Picture.equalsIgnoreCase(SUCCESS))
    strPicture ="success.gif";
  else if (Picture.equalsIgnoreCase(FAILURE))
    strPicture ="failure.gif";
  else
    strPicture ="common.gif";
%>

<table>
  <td>
    <img src='../images/<%=strPicture%>'>
  </td>
  <td class="common" id=Content>
    <%=Content%>
  </td>
  <TD  id="info"  style="font:10pt">
</table>

<table >
  <center>
  <table width=80%><tr><td align=left>
    <!--<table id=lpc bgcolor=blue><tr><td>&nbsp;</td></tr></table>-->
    
    </td></tr>
  </table>
  </center>
</table>

<center><input type=button class=common id=butSubmit value="显示查询进度" onclick="ShowSchedule()" tabIndex=0></center>
<%if (Content.indexOf("正在") == -1) {%>
<center><input type=button class=common id=butSubmit value="确 定" onclick="window.close()" tabIndex=0>
<%}%>
<script language=JavaScript>

ini = new Date().getTime();
var pc = 0;
//load();

function load() {
  pc += 1;
  lpc.style.width = pc + "%";
  time = setTimeout("load()",30);
  if (pc > 100) { 
    pc=0;
  }
}

function loaded() {
  fim = new Date().getTime();
  dif = fim - ini;
  ld.style.display = 'none';
  body.style.backgroundColor = 'silver';
  q.innerHTML = dif/1000;
  page.style.display = '';
}

function Show() {
  if (txt.style.display == "none") { txt.style.display = "" }
  else { txt.style.display = "none" }
}
// event.onAnyDoubt = "../js.htm";

function ShowSchedule()
{
	Content.style.display='none';
	var BatchNo = "<%=BatchNo%>";
	var strSql="select errorinfo,contid from LCGRPIMPORTLOG where BATCHNO='"+BatchNo+"'";
	alert();
	var arr = easyExecSql(strSql);
	alert();
	var contNo = "";
	var xmlNo = "";
	if(arr)
	{
		var kind=arr[0][1];
		if(kind=="C")
		{
			contNo=arr[0][0];
			xmlNo =arr[1][0];
		}
	else
		{
			contNo=arr[1][0];
			xmlNo =arr[0][0];
		}
	}
	document.all("info").innerText="系统中将有"+contNo+"个合同需要导入系统";
}

try {
  window.butSubmit.focus();
} 
catch(e) {}

</script>

</body>
</html>