<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
String tContNo = "";
try
{
	tContNo = request.getParameter( "ContNo" );

	//默认情况下为集体投保单
	if( tContNo == null || tContNo.equals( "" ))
		tContNo = "00000000000000000000";
}
catch( Exception e1 )
{
	tContNo = "00000000000000000000";
}
%>
<script>
var contNo = "<%=tContNo%>";  //个人单的查询条件.
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="ReGroupPolPrint.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ReGroupPolPrintInit.jsp"%>
<title>重打团单 </title>
</head>
<body  onload="initForm();" >
	<form action="./ReGroupPolPrintSave.jsp" method=post name=fm target="fraSubmit">
		<!-- 保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr>
				<td class= titleImg align= center>请输入查询条件：</td>
			</tr>
		</table>
		<table  class= common align=center>
			<TR  class= common>
				<TD  class= title>保险合同号</TD>
				<TD  class= input>
					<Input class= common name=GrpContNo verify="保险合同号|int&len=10">
				</TD>
				<TD  class= title>印刷号码</TD>
				<TD  class= input>
					<Input class= common name=PrtNo verify="印刷号码|len=11">
				</TD>
				<TD  class= title>管理机构</TD>
				<TD  class= input>
        <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
				</TD>
			</TR>
			<!--TD  class= title>
			险种版本
			</TD>
			<TD  class= input>
			<Input class="code" name=RiskVersion ondblclick="return showCodeList('RiskVersion',[this]);" onkeyup="return showCodeListKey('RiskVersion',[this]);">
			</TD>
			</TR>
			<TR  class= common>
				<TD  class= title>险种编码</TD>
				<TD  class= input>
					<Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);">
			</TD-->
			<TR class="common">
				<TD  class= title>业务员代码</TD>
				<TD  class= input>
           <Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCodet" ondblclick="return showCodeList('AgentCodet',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4]);showAllCodeName();" onkeyup="return showCodeListKey('AgentCodet',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
				</TD>
				<TD  class= title>业务员组别</TD>
				<TD  class= input>
				<Input class=codeNo name=AgentGroup readonly=true verify="业务员组别|code:branchcode" ondblclick="return showCodeList('branchcode',[this,AgentGroupName],[0,1]);" onkeyup="return showCodeListKey('branchcode',[this,AgentGroupName],[0,1]);"><input class=codename name=AgentGroupName readonly=true >
				</TD>
				<TD  class= title>投保人姓名</TD>
				<TD  class= input>
					<Input class= common name=GrpName  verify="投保人姓名|len<=20">
				</TD>
			</TR>
		</table>
		<INPUT VALUE="查询保单" TYPE="button" onclick="easyQueryClick();" class="cssButton">
	</form>
	<form action="./ReGroupPolPrintSave.jsp" method=post name=fmSave target="fraSubmit">
		<input value="提交申请" type="button" onclick="printGroupPol();" class="cssButton">
		<input VALUE="yes" name = "" type="checkbox" onclick="dataConfirm(this);">重新生成打印数据
		<table>
			<tr>
				<td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);"></td>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<Div  id= "divLCPol1" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanGrpPolGrid" ></span>
					</td>
				</tr>
			</table>
			<INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class="cssButton">
			<INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class="cssButton">
			<INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class="cssButton">
			<INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class="cssButton">
		</div>
		<input type=hidden id="fmtransact" name="fmtransact">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
