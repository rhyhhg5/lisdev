//该文件中包含客户端需要处理的函数和事件
//程序功能：契约保单发送及下载规则设置
//创建人：杨亚林
//创建时间：2008-3-13 11:39上午

var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();


//================================================初始化区域================================================

function initInpBox()
{
  fm.ManageCom.value = tManageCom;
  
  //显示机构中文名
  var sql = "select name "
          + "from LDCom "
          + "where ComCode = '" + fm.all('ManageCom').value + "' ";
	var arrResult = easyExecSql(sql);   
  if (arrResult)
  {
    fm.all('ManageComName').value=arrResult[0][0];
  }
}


//================================================事件响应区域================================================

//查询已配置的规则
function queryRule()
{
	var sql = "select RuleType, RuleName, BussType, BussName, ComCode, "
	        + "   (select Name from LDCom where ComCode = a.ComCode), RuleValue "
	        + "from LDBusinessRule a "
	        + "where ComCode like '" + tManageCom + "%' "
	        + (fm.ManageCom.value == "" ? "" : "   and ComCode like '" + fm.ManageCom.value + "%' ")
	        + "order by BussType, ComCode ";
  
	turnPage2.pageLineNum = 50;
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, BusinessRuleGrid);
}

//提交，保存按钮对应操作
function submitForm()
{
  if(!checkData())
  {
    return false;
  }
  
  if(!verifyInput2())
  {
    return false;
  }
  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  fm.submit(); //提交
}

function afterSubmit( FlagStr, content )
{
	//无论打印结果如何，都重新激活打印按钮
	showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail" )
	{
		//如果打印失败，展现错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
}

//================================================校验区域================================================

function checkData()
{
  if(BusinessRuleGrid.mulLineCount == 0)
  {
    if(!confirm("将删除所有规则类型为" + fm.RuleType.value + " 业务类型为" + fm.BussType.value + "的规则"))
    {
      return false;
    }
  }
  
  return true;
}