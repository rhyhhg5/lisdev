
<%
	//程序名称：LdSpecialProductsInput.jsp
	//程序功能：特殊产品定义（特殊产品打印时，指定医院和推荐意见合并统一显示，所以需配置特殊产品）
	//创建日期：201-07-07 12:00:16
	//创建人  ：郭忠华
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="LdSpecialProductsInput.js"></SCRIPT>
		<%@include file="LdSpecialProductsInit.jsp"%>
	</head>

	<%
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
	%>
	<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
	<body onload="initForm(); initElementtype();">
		<form action="./LdSpecialProductsSave.jsp" method=post name=fm
			target="fraSubmit">
			<%@include file="../common/jsp/OperateButton.jsp"%>
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLDWorkTime1);">
					</td>
					<td class=titleImg>
						特殊产品设定信息
					</td>
				</tr>
			</table>
			<Div id="divLDWorkTime1" style="display: ''">
				<table class=common align='center'>
					<TR class=common>
						<TD class=title8>
							产品代码
						</TD>
						<TD class=input8>
							<Input class=codeno name=SpecRiskCode verify="险种编码|notnull&code:SpecRiskCode"
								ondblclick="return showCodeList('SpecRiskCode',[this,RiskCodeName],[0,1],null,null,null,1);"
								onkeyup="return showCodeListKey('SpecRiskCode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true
								elementtype=nacessary>
						</TD>
						<TD class=title>
							显示名称
						</TD>
						<TD class=input>
							<Input class='common' name=RiskShowName elementtype=nacessary
								verify="显示名称|notnull">
						</TD>
					</TR>
				</table>
			</Div>
			<INPUT VALUE="重 置" class=cssButton TYPE=button onclick="initForm();">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divSpecialGrid);">
					</td>
					<td class=titleImg>
						特殊产品列表：
					</td>
				</tr>
			</table>
			<Div id="divSpecialGrid" style="display: ''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanSpecialGrid"> </span>
						</td>
					</tr>
				</table>
				<INPUT VALUE="首  页" class=cssButton TYPE=button
					onclick="getFirstPage();">
				<INPUT VALUE="上一页" class=cssButton TYPE=button
					onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class=cssButton TYPE=button
					onclick="getNextPage();">
				<INPUT VALUE="尾  页" class=cssButton TYPE=button
					onclick="getLastPage();">
			</div>
			
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden name="operator">
			<input type=hidden name="SpecRiskCode1">
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>


