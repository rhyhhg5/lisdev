<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：
//程序功能：
//创建日期：2009-07-28
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="ContRiskHBDetail.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();">    
<form action="" method=post name=fm target="fraSubmit">

    <table class="common">
        <tr class="common">
            <td class="title">管理机构</td>
            <td class="input">
                <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
            </td>
            <td class="title">投保日期起</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="StartMakeDate" verify="投保日期|date&notnull" elementtype="nacessary" />
            </td>
            <td class="title">投保日期止</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="EndMakeDate" verify="投保日期|date&notnull" elementtype="nacessary" />
            </td>
        </tr>             
    </table>
    
    <br />
    
    <input value="下  载" class="cssButton" type="button" onclick="submitForm();" />
    
    <hr />
    
    <div>
        <font color="red">
            <ul>报表说明：
                <li>本报表主要是按照投保时间（以保单录入时间为准)进行统计，投保件的各种状态情况。</li>
                <li>此报表包括：犹豫期退保，退保的情况</li>                
                <li>投保日期查询口径：这里指保单录入日期。</li>
            </ul>
            <ul>名词解释：
                <li>契撤件数：指投保申请单在投保审核过程中，客户提出撤单的件数。</li>
                <li>拒保件数：指该险种核保师拒保核保结论的件数。</li>
                <li>标准体承保件数：按照标准体核保的险种件数。</li>
                <li>条件承保件数：非标准体承保险种的件数。</li>
                <li>总投保件数：总投保件数（包括退保/犹豫期撤保）= 标准体承保件数 + 条件承保件数 + 拒保件数 + 契撤件数 + 延期件数</li>
            </ul>
            <ul>统计条件：
                <li>管理机构、投保日期段。</li>
                <li>投保日期段间隔不超过十二个月。</li>
            </ul>
        </font>
    </div>
    
    <input name="querySql" type="hidden" />
    <input type="hidden" name=op value="" />

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 