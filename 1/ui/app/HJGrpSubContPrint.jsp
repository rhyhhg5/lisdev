<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2009-07-15
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="HJGrpSubContPrint.js"></script>
    <%@include file="HJGrpSubContPrintInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">查询条件：</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title8">生效日期起</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="CValidateStart" verify="申请日期|date" />
                    </td>
                    <td class="title8">生效日期止</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="CValidateEnd" verify="申请日期|date" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title8">团单合同号</td>
                    <td class="input8">
                        <input class="common" name="GrpContNo" />
                    </td>
                    <td class="title8">团单印刷号</td>
                    <td class="input8">
                        <input class="common" name="PrtNo" />
                    </td>
                    <td class="title8">业务员代码</td>
                    <td class="input8">
                        <input class="common" name="AgentCode" />
                    </td>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryGrpCont();" /> 	
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divGrpContGrid);" />
                </td>
                <td class="titleImg">团单清单列表</td>
            </tr>
        </table>
        <div id="divGrpContGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanGrpContGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divGrpContGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>
        
        <br />
        
        <table>
            <tr>
                <td class="titleImg">个人凭证查询条件：</td>
            </tr>
        </table>
        
        <div id="divGrpSubCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">团单合同号</td>
                    <td class="input8">
                        <input class="common" name="GS_GrpContNo" readonly="readonly" />
                    </td>
                    <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="common" name="GS_ManageCom" readonly="readonly" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title">分单凭证是否打印</td>
        			<td class="input" COLSPAN="1">
                        <input class="codeNo" name="PrintCount" CodeData="0|^0|否^1|是" ondblclick="showCodeListEx('PrintCount',[this,PrintCountName],[0,1]);" /><input class="codename" name="PrintCountName" />
                    </td>
                    <td class="title">告知书是否打印</td>
        			<td class="input" COLSPAN="1">
                        <input class="codeNo" name="PrintCountG" CodeData="0|^0|否^1|是" ondblclick="showCodeListEx('PrintCount',[this,PrintCountNameG],[0,1]);" /><input class="codename" name="PrintCountNameG" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title8">学生姓名</td>
                    <td class="input8">
                        <input class="common" name="GS_insuerdname" />
                    </td>                
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryGrpSubCont();" />    
                </td>
            </tr>
            
        </table>
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divGrpSubContGrid);" />
                </td>
                <td class="titleImg">个人凭证清单列表</td>
            </tr>
        </table>
        <div id="divGrpSubContGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanGrpSubContGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divGrpSubContGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                     
            </div>
        </div>
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnSubContPrint" value="分单凭证打印" onclick="printGrpSubPDFBatch();" />
                    <input class="cssButton" type="button" id="btnSubNoticePrint" value="告知书打印" onclick="printGrpSubNoticePDFBatch();" />   
                </td>
            </tr>
        </table>
        
        <br />
        
        <hr />
        
        <div>
            <font color="red">
                <ul>
                    <li>只可查询到已经打印过的团单</li>
                </ul>
            </font>
        </div>
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
