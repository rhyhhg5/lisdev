<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PubAccInputSave.jsp
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
  LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
  LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
  LDPromiseRateSet tLDPromiseRateSet = new LDPromiseRateSet();
			
  GrpPubAccUI tGrpPubAccUI   = new GrpPubAccUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String tGrpContNo = request.getParameter("GrpContNo");
  String tGrpPolNo = request.getParameter("GrpPolNo");
  String tPrtNo = request.getParameter("PrtNo");
  String tGuaRate = request.getParameter("GuaRate");
  String tOperator = request.getParameter("Operator");
  String tFlag = request.getParameter("flag");
  //实际是存储在职/退休 但是应数据被保险人状态，因此用词字段存储账户类型
  String tPublicAccType = request.getParameter("PublicAccType");
  //如果是公共账户的话存储公共账户名称
	String tInsuredName = request.getParameter("InsuredName");
	//账户金额
	String tPublicAcc = request.getParameter("Prem");
	//险种代码
	String tRiskCode = request.getParameter("RiskCode");
	//处理要素信息
	String[] tCalFactor = request.getParameterValues("PublicAccGrid1");
	String[] tCalFactorValue = request.getParameterValues("PublicAccGrid4");
	String[] tCalFactorType = request.getParameterValues("PublicAccGrid5");
	String tInsuAccNo = request.getParameter("InsuAccNo");

	LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
	tLCInsuredListSchema.setGrpContNo(tGrpContNo);
	tLCInsuredListSchema.setInsuredName(tInsuredName);
	tLCInsuredListSchema.setEmployeeName(tInsuredName);
	tLCInsuredListSchema.setRelation("00");
	tLCInsuredListSchema.setRiskCode(tRiskCode);
	tLCInsuredListSchema.setPublicAcc(tPublicAcc);
	tLCInsuredListSchema.setPublicAccType(tPublicAccType);
	tLCInsuredListSet.add(tLCInsuredListSchema);
	//add by zqt 是否进账 2016-05-26
	String houston=request.getParameter("Houston");
	String entrustM=request.getParameter("EntrustMoney");
	String entrustMoney;
	if(entrustM==null||entrustM.equals("")){
			entrustMoney=null;
		}else{
			int entrustM1=Integer.parseInt(entrustM);
			entrustMoney=entrustM1+"";
		}
	//add by zjd 赔付顺序 2015-01-12
	String claimnum=request.getParameter("ClaimNum");
	
	if(tCalFactor!=null)
	{
		int n = tCalFactor.length;
		for(int m = 0 ; m<n ; m++)
		{
			LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
			tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
			tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
			tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
			
			tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
			tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
			tLCContPlanDutyParamSchema.setContPlanCode("11");
			tLCContPlanDutyParamSchema.setDutyCode("000000");
			tLCContPlanDutyParamSchema.setCalFactor(tCalFactor[m]);
			tLCContPlanDutyParamSchema.setCalFactorType(tCalFactorType[m]);
			tLCContPlanDutyParamSchema.setCalFactorValue(tCalFactorValue[m]);
			tLCContPlanDutyParamSchema.setPlanType("0");
			tLCContPlanDutyParamSchema.setPayPlanCode("000000");
			tLCContPlanDutyParamSchema.setGetDutyCode("000000");
			tLCContPlanDutyParamSchema.setInsuAccNo(tInsuAccNo);
			tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
		}
	}
	
	//处理管理费信息
	String[] cGrpPolNo       = request.getParameterValues("ManageFeeGrid1");
	String[] cGrpContNo      = request.getParameterValues("ManageFeeGrid2");
	String[] cRiskCode       = request.getParameterValues("ManageFeeGrid3");
	String[] cFeeCode        = request.getParameterValues("ManageFeeGrid4");
	String[] cInsuAccNo      = request.getParameterValues("ManageFeeGrid5");
	String[] cPayPlanCode    = request.getParameterValues("ManageFeeGrid6");
	String[] cPayInsuAccName = request.getParameterValues("ManageFeeGrid7");
	String[] cFeeCalMode     = request.getParameterValues("ManageFeeGrid8");
	String[] cFeeCalModeType = request.getParameterValues("ManageFeeGrid9");
	String[] cFeeCalCode   = request.getParameterValues("ManageFeeGrid10");
	String[] cFeeValue     = request.getParameterValues("ManageFeeGrid12");
	String[] cCompareValue = request.getParameterValues("ManageFeeGrid13");
	String[] cFeePeriod    = request.getParameterValues("ManageFeeGrid14");
	String[] cMaxTime      = request.getParameterValues("ManageFeeGrid15");
	String[] cDefaultFlag  = request.getParameterValues("ManageFeeGrid16");
	String[] cOperator     = request.getParameterValues("ManageFeeGrid17");
	String[] cMakeDate     = request.getParameterValues("ManageFeeGrid18");
	String[] cMakeTime     = request.getParameterValues("ManageFeeGrid19");
	String[] cModifyDate   = request.getParameterValues("ManageFeeGrid20");
	String[] cModifyTime   = request.getParameterValues("ManageFeeGrid21");
	
	//处理个人管理费信息
	String[] cIndGrpPolNo       = request.getParameterValues("IndManageFeeGrid1");
	String[] cIndGrpContNo      = request.getParameterValues("IndManageFeeGrid2");
	String[] cIndRiskCode       = request.getParameterValues("IndManageFeeGrid3");
	String[] cIndFeeCode        = request.getParameterValues("IndManageFeeGrid4");
	String[] cIndInsuAccNo      = request.getParameterValues("IndManageFeeGrid5");
	String[] cIndPayPlanCode    = request.getParameterValues("IndManageFeeGrid6");
	String[] cIndPayInsuAccName = request.getParameterValues("IndManageFeeGrid7");
	String[] cIndFeeCalMode     = request.getParameterValues("IndManageFeeGrid8");
	String[] cIndFeeCalModeType = request.getParameterValues("IndManageFeeGrid9");
	String[] cIndFeeCalCode   = request.getParameterValues("IndManageFeeGrid10");
	String[] cIndFeeValue     = request.getParameterValues("IndManageFeeGrid12");
	String[] cIndCompareValue = request.getParameterValues("IndManageFeeGrid13");
	String[] cIndFeePeriod    = request.getParameterValues("IndManageFeeGrid14");
	String[] cIndMaxTime      = request.getParameterValues("IndManageFeeGrid15");
	String[] cIndDefaultFlag  = request.getParameterValues("IndManageFeeGrid16");
	String[] cIndOperator     = request.getParameterValues("IndManageFeeGrid17");
	String[] cIndMakeDate     = request.getParameterValues("IndManageFeeGrid18");
	String[] cIndMakeTime     = request.getParameterValues("IndManageFeeGrid19");
	String[] cIndModifyDate   = request.getParameterValues("IndManageFeeGrid20");
	String[] cIndModifyTime   = request.getParameterValues("IndManageFeeGrid21");
	
	if(cGrpPolNo!=null)
	{
		int mulline = cGrpPolNo.length;
		System.out.println("@@ : "+mulline);
		for(int i=0 ; i<mulline ; i++)
		{
			LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
			tLCGrpFeeSchema.setGrpPolNo(cGrpPolNo[i]);             
			tLCGrpFeeSchema.setGrpContNo(cGrpContNo[i]);     
			tLCGrpFeeSchema.setRiskCode(cRiskCode[i]);            
			tLCGrpFeeSchema.setFeeCode(cFeeCode[i]);           
			tLCGrpFeeSchema.setInsuAccNo(cInsuAccNo[i]);          
			tLCGrpFeeSchema.setPayPlanCode(cPayPlanCode[i]);      
			tLCGrpFeeSchema.setPayInsuAccName(cPayInsuAccName[i]); 
			tLCGrpFeeSchema.setFeeCalMode(cFeeCalMode[i]);         
			tLCGrpFeeSchema.setFeeCalModeType(cFeeCalModeType[i]); 
			tLCGrpFeeSchema.setFeeCalCode(cFeeCalCode[i]);         
			tLCGrpFeeSchema.setFeeValue(cFeeValue[i]);          
			tLCGrpFeeSchema.setCompareValue(cCompareValue[i]);     
			tLCGrpFeeSchema.setFeePeriod(cFeePeriod[i]);          
			tLCGrpFeeSchema.setMaxTime(cMaxTime[i]);              
			tLCGrpFeeSchema.setDefaultFlag(cDefaultFlag[i]);   
			tLCGrpFeeSchema.setOperator(cOperator[i]);       
			tLCGrpFeeSchema.setMakeDate(cMakeDate[i]);          
			tLCGrpFeeSchema.setMakeTime(cMakeTime[i]);           
			tLCGrpFeeSchema.setModifyDate(cModifyDate[i]);      
			tLCGrpFeeSchema.setModifyTime(cModifyTime[i]); 
			tLCGrpFeeSchema.setHouston(houston);
			tLCGrpFeeSchema.setEntrustMoney(entrustMoney);
			tLCGrpFeeSchema.setClaimNum(claimnum);
			tLCGrpFeeSet.add(tLCGrpFeeSchema);
		}
	}
	
	if(cIndGrpPolNo!=null)
	{
		int mulline = cIndGrpPolNo.length;
		System.out.println("@@ : "+mulline);
		for(int i=0 ; i<mulline ; i++)
		{
			LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
			tLCGrpFeeSchema.setGrpPolNo(cIndGrpPolNo[i]);             
			tLCGrpFeeSchema.setGrpContNo(cIndGrpContNo[i]);     
			tLCGrpFeeSchema.setRiskCode(cIndRiskCode[i]);            
			tLCGrpFeeSchema.setFeeCode(cIndFeeCode[i]);           
			tLCGrpFeeSchema.setInsuAccNo(cIndInsuAccNo[i]);          
			tLCGrpFeeSchema.setPayPlanCode(cIndPayPlanCode[i]);      
			tLCGrpFeeSchema.setPayInsuAccName(cIndPayInsuAccName[i]); 
			tLCGrpFeeSchema.setFeeCalMode(cIndFeeCalMode[i]);         
			tLCGrpFeeSchema.setFeeCalModeType(cIndFeeCalModeType[i]); 
			tLCGrpFeeSchema.setFeeCalCode(cIndFeeCalCode[i]);         
			tLCGrpFeeSchema.setFeeValue(cIndFeeValue[i]);          
			tLCGrpFeeSchema.setCompareValue(cIndCompareValue[i]);     
			tLCGrpFeeSchema.setFeePeriod(cIndFeePeriod[i]);          
			tLCGrpFeeSchema.setMaxTime(cIndMaxTime[i]);              
			tLCGrpFeeSchema.setDefaultFlag(cIndDefaultFlag[i]);   
			tLCGrpFeeSchema.setOperator(cIndOperator[i]);       
			tLCGrpFeeSchema.setMakeDate(cIndMakeDate[i]);          
			tLCGrpFeeSchema.setMakeTime(cIndMakeTime[i]);           
			tLCGrpFeeSchema.setModifyDate(cIndModifyDate[i]);      
			tLCGrpFeeSchema.setModifyTime(cIndModifyTime[i]);
			tLCGrpFeeSchema.setHouston(houston);
			tLCGrpFeeSchema.setEntrustMoney(entrustMoney);
			tLCGrpFeeSchema.setClaimNum(claimnum);
			tLCGrpFeeSet.add(tLCGrpFeeSchema);
		}
	}
	String[] dGrpPolNo       = request.getParameterValues("RiskInterestGrid1");
	String[] dGrpContNo      = request.getParameterValues("RiskInterestGrid2");
	String[] dRiskCode       = request.getParameterValues("RiskInterestGrid3");
	String[] dInterestCode   = request.getParameterValues("RiskInterestGrid4");
	String[] dInsuAccNo      = request.getParameterValues("RiskInterestGrid5");
	String[] dInterestType = request.getParameterValues("RiskInterestGrid7");
	String[] dDefaultCalType     = request.getParameterValues("RiskInterestGrid8");
	String[] dDefaultRate = request.getParameterValues("RiskInterestGrid9");
	LCGrpInterestSet tLCGrpInterestSet = new LCGrpInterestSet();
	if(dGrpPolNo!=null)
	{
		int mulline = dGrpPolNo.length;
		System.out.println("@@ : "+mulline);
		for(int i=0 ; i<mulline ; i++)
		{
			LCGrpInterestSchema tLCGrpInterestSchema = new LCGrpInterestSchema();
			tLCGrpInterestSchema.setGrpPolNo(dGrpPolNo[i]);             
			tLCGrpInterestSchema.setGrpContNo(dGrpContNo[i]);     
			tLCGrpInterestSchema.setRiskCode(dRiskCode[i]);            
			tLCGrpInterestSchema.setInterestCode(dInterestCode[i]);           
			tLCGrpInterestSchema.setInsuAccNo(dInsuAccNo[i]);               
			tLCGrpInterestSchema.setInterestType(dInterestType[i]); 
			tLCGrpInterestSchema.setDefaultRate(dDefaultRate[i]);            
			tLCGrpInterestSet.add(tLCGrpInterestSchema);
		}
	}
	if(tPrtNo!=null){
		LDPromiseRateSchema tLDPromiseRateSchema = new LDPromiseRateSchema();
		tLDPromiseRateSchema.setRiskCode("370301");
		tLDPromiseRateSchema.setRate(tGuaRate);
		tLDPromiseRateSchema.setRateType("C");
		tLDPromiseRateSchema.setRateIntv("Y");
		tLDPromiseRateSchema.setStartDate("20000101");
		tLDPromiseRateSchema.setEndDate("29991231");
		tLDPromiseRateSchema.setOperator(tOperator);
		tLDPromiseRateSchema.setPrtNo(tPrtNo);
		tLDPromiseRateSet.add(tLDPromiseRateSchema);
	}
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	TransferData tTransferData = new TransferData();
  	tTransferData.setNameAndValue("GrpContNo",tGrpContNo);
  	tTransferData.setNameAndValue("RiskCode",tRiskCode);
  	tTransferData.setNameAndValue("GrpPolNo",tGrpPolNo);
  	tTransferData.setNameAndValue("InsuAccNo",tInsuAccNo);
  	tTransferData.setNameAndValue("ClaimNum",claimnum);
  	tTransferData.setNameAndValue("Flag",tFlag);
  	tVData.add(tTransferData);
		tVData.add(tLCInsuredListSet);
		tVData.add(tLCContPlanDutyParamSet);
		tVData.add(tLCGrpFeeSet);
		tVData.add(tLCGrpInterestSet);
		tVData.add(tLDPromiseRateSet);
  	tVData.add(tG);
    tGrpPubAccUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tGrpPubAccUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
