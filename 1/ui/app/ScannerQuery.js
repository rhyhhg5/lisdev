
//程序名称：ES_DOC_MAINQuery.js
//程序功能：扫描单证下载的主页面JS
//创建日期：2007-09-25
//创建人  ：DongJianbin
//更新记录:  更新人 shaoax   更新日期 2007-11-20    更新原因/内容 添加扫描件删除函数


//               该文件中包含客户端需要处理的函数和事件

var arrDataSet 
var tArr;
var deleflag = 0;
var turnPage = new turnPageClass();

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    window.focus();
    showInfo.close();
    
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
    showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
 	if (FlagStr == "Succ")
	{
        queryScanner();
	}
}

function clearInpBox()
{
    fm.all('ManageCom').value = '' 	;
    fm.all('StartDate').value = ''	;
    fm.all('EndDate').value = ''	;    
    fm.all('ScanOperator').value = ''	;
    fm.all('SubType').value = 'TB01'	;
    fm.all('SubTypeName').value = '家庭投保书';
    fm.all('DocCode').value = ''	;
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在EsPicDownload.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

function easyqueryClick()
{
	initCodeGrid();
	var strSQL = "select docid,doccode,subtype,numpages,managecom,makedate,scanoperator, CodeName('esmainstate', state) state "
					+ " from ES_DOC_MAIN where 1=1 "
					+ getMngCom()
					+ getStartDate()
					+ getEndDate()					
					+ getWherePart( 'ScanOperator' )
					+ getWherePart( 'DocCode' )
					+ getWherePart("SubType")
					+ getWherePart("State")
					;	
	//alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  if (!turnPage.strQueryResult) {
    alert("未查询到满足条件的数据！");
     return false;
  }	
  turnPage.queryModal(strSQL,CodeGrid);
}

function queryScanner()
{
    var tStrSql = " select edm.DocId, edm.DocCode, "
        + " edm.SubType, edm.NumPages, "
        + " edm.ManageCom, edm.MakeDate, edm.ScanOperator, "
        + " edm.BussType "
        + " from Es_Doc_Main edm "
        + " where 1 = 1 "
        + " and edm.ManageCom like '" + fm.ManageCom.value + "%' "
        + getStartDate()
        + getEndDate()                  
        + getWherePart("edm.ScanOperator", "ScanOperator")
        + getWherePart("edm.DocCode", "DocCode")
        + getWherePart("edm.SubType", "SubType")
        ;
    turnPage.queryModal(tStrSql, ScannerGrid);
}

function DownloadPic()
{
//	 var checkFlag="";
//  for (i=0; i<CodeGrid.mulLineCount; i++)
//  {
//    if (CodeGrid.getSelNo(i))
//    {
//      checkFlag = CodeGrid.getSelNo();
//      break;
//    }
//  }
	//如果没有打印数据，提示用户选择
//	if( checkFlag == 0 )
//	{
//		alert("请先选择一条记录，再下载单证");
//		return false;
//	}
//	else {
    fm.action="./ES_DOC_MAINQueryDownLoad.jsp";
    fm.EXESql.value = turnPage.strQuerySql;
    fm.submit();
//  } 
}
function getStartDate()
{
	var ssd=fm.StartDate.value;
	var sDate="";
	if(ssd=="")
	{
		sDate= "";
	}
  else
	{
	sDate= "and makedate>='"+ssd+"' ";
	}
	return sDate;
}
function getEndDate()
{
	var sed=fm.EndDate.value;
	var eDate="";
	if(sed=="")
	{
		eDate= "";
	}
else
	{
		eDate= "and makedate<='"+sed+"' ";
	}
	return eDate;
}
function getMngCom()
{
	var tManageCom=fm.ManageCom.value;
	var cManageCom="";
  if (tManageCom=="")
  {
    cManageCom="";
  }
  else
  {
    cManageCom=" and ManageCom like '"+tManageCom+"%' ";  
  }
  return cManageCom;
}

/**
 * 扫描件删除操作
 */
function delScanner()
{
	var row = ScannerGrid.getSelNo() - 1;
	if(row == null || row < 0)
	{
		alert("请选择一条记录。");
		return false;
	}
	
	//查询要删除的扫描件信息
	var tResult = ScannerGrid.getRowData(row);

	var docId = tResult[0];
	fm.DocId.value = docId;
	
	if (confirm("您确实想删除该记录吗?"))
	{ 
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

		fm.action = "ScannerQuerySave.jsp";
		fm.submit();
	}
}

function viewPic()
{    
    var tRow = ScannerGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tDocID = ScannerGrid.getRowColData(tRow, 1);
    var tDocCode = ScannerGrid.getRowColData(tRow, 2);
    var tBussTpye = ScannerGrid.getRowColData(tRow, 8);
    var tSubTpye = ScannerGrid.getRowColData(tRow, 3);

    var tStrUrl = ""
        + "../easyscan/QCManageInputMainShow.jsp"
        + "?EASYWAY=1"
        + "&DocID=" + tDocID
        + "&DocCode=" + tDocCode
        + "&BussTpye=" + tBussTpye
        + "&SubTpye=" + tSubTpye
        ;

    window.open(tStrUrl);        
}

