<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolSignSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
   <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	String Priview = "PREVIEW";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	TransferData tTransferData = new TransferData();
  
  	//接收信息
  	// 投保单列表
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	String tGrpContNo[] = request.getParameterValues("GrpGrid1");
	String tRadio[] = request.getParameterValues("InpGrpGridSel");
	String tMissionID[] = request.getParameterValues("GrpGrid7");   
	String tSubMissionID[] = request.getParameterValues("GrpGrid8");
	String workType = request.getParameter("workType");
	String tempGrpContNo = "";
	
	boolean flag = false;
	int grouppolCount = tGrpContNo.length;
	for (int i = 0; i < grouppolCount; i++)
	{
		if( tGrpContNo[i] != null && tRadio[i].equals( "1" ))
		{
            System.out.println("GrpContNo:"+i+":"+tGrpContNo[i]);
            tempGrpContNo = tGrpContNo[i];
		    tLCGrpContSchema.setGrpContNo( tGrpContNo[i] );
		    if("PREVIEW".equals(workType))
		    {
		      tLCGrpContSchema.setAppFlag("9");
		    }
		    tTransferData.setNameAndValue("GrpContNo",tGrpContNo[i] );
		    tTransferData.setNameAndValue("MissionID",tMissionID[i] );
	     	tTransferData.setNameAndValue("SubMissionID",tSubMissionID[i] );
		    flag = true;
			break;
		}
	}

// 	String tLockSQL = " select prtno from lcgrpcont where proposalgrpcontno = '"+tempGrpContNo+"' ";
// 	String PrtNo = new ExeSQL().getOneValue(tLockSQL);
// 	if(PrtNo != null && !"".equals(PrtNo)){
// 		String tempSQL = " select 1 from ljtempfee where otherno = '"+PrtNo+"' or otherno = '"+tempGrpContNo+"'";
// 		String temp = new ExeSQL().getOneValue(tempSQL);
// 		if(temp != null && !"".equals(temp)){
// 			String insertSQL = "INSERT INTO lcurgeverifylog VALUES ('"+PrtNo+"','2',NULL,NULL,NULL,'5','5',"
// 					 + "'it001','5',current date,current time,current date,current time,NULL)";
// 			System.out.println("提交sql:"+insertSQL);
// 			new ExeSQL().execUpdateSQL(insertSQL);
// 		}
		
// 	}
	
  	if (flag == true)
  	{
        String tActivityId = "0000002006";
        
        // 如果是大数据提交，tActivityId 为 "0000002007";
        String tSubmitType = request.getParameter("SubmitType");
        if(tSubmitType != null && tSubmitType.equals("1"))
        {
            tActivityId = "0000002007";
        }
        // ---------------------------------------
        
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCGrpContSchema );
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
		//LCGrpContSignBL tLCGrpContSignBL   = new LCGrpContSignBL();
		
		//boolean bl = tLCGrpContSignBL.submitData( tVData, "INSERT" );
		//int n = tLCGrpContSignBL.mErrors.getErrorCount();
        GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
		boolean bl= tTbWorkFlowUI.submitData( tVData, tActivityId);
		int n = tTbWorkFlowUI.mErrors.getErrorCount();
		if( n == 0 ) 
	    { 
		    if ( bl )
		    {
	    		if(!"PREVIEW".equals(workType)){
	    			String tSQL = " select prtno from lcgrpcont where proposalgrpcontno = '"+tempGrpContNo+"' "
	    					+ " and substr(managecom,1,4) in ( select code from ldcode where codetype = 'qysendmessgem' ) ";
		    		String tPrtNo = new ExeSQL().getOneValue(tSQL);
		    		if(tPrtNo != null && !"".equals(tPrtNo)){
		    			VData tSMSVData = new VData();
		    			TransferData tSMSTransferData = new TransferData();
		    			tSMSTransferData.setNameAndValue("PrtNo",tPrtNo );
		    			tSMSTransferData.setNameAndValue("ContTypeFlag","2" );
		    			tSMSVData.add(tSMSTransferData);
		    			tSMSVData.add( tG );
	    				QySendMessgeMBL tQySendMessgeMBL = new QySendMessgeMBL();
	    				if(tQySendMessgeMBL.submitData(tSMSVData,"QYMSG")){
	    					System.out.println(tPrtNo+"签单后即时发送短信成功！");
	    				}else{
	    					System.out.println(tPrtNo+"签单后即时发送短信失败！");
	    				}
		    		}
	    		}
	    		Content = " 签单成功! ";
		   		FlagStr = "Succ";
			    if("PREVIEW".equals(workType))
			    {
	    		  Content = " 预打保单成功! ";
			    }
	    	}
	    	else
	    	{
	    	   Content = "集体投保单签单失败";
	    	   FlagStr ="Fail" ;
			    if("PREVIEW".equals(workType))
			    {
	    		  Content = " 预打保单失败! ";
			    }
	    	}
	    }
	    else
	    {
	         
	    	String strErr = "";
			for (int i = 0; i < n; i++)
			{
				strErr += (i+1) + ": " + tTbWorkFlowUI.mErrors.getError(i).errorMessage + "; ";
				System.out.println(tTbWorkFlowUI.mErrors.getError(i).errorMessage );
			}
			 if ( bl )
			 {
	    		Content = " 部分签单成功,但是有如下信息: " +strErr;
		   		FlagStr = "Succ";
			    if("PREVIEW".equals(workType))
			    {
	    		  Content = " 预打保单成功! ";
			    }
			    if(!"PREVIEW".equals(workType)){
	    			String tSQL = " select prtno from lcgrpcont where proposalgrpcontno = '"+tGrpContNo[0]+"' "
	    					+ " and substr(managecom,1,4) in ( select code from ldcode where codetype = 'qysendmessgem' ) ";
		    		String tPrtNo = new ExeSQL().getOneValue(tSQL);
		    		if(tPrtNo != null && !"".equals(tPrtNo)){
		    			VData tSMSVData = new VData();
		    			TransferData tSMSTransferData = new TransferData();
		    			tSMSTransferData.setNameAndValue("PrtNo",tPrtNo );
		    			tSMSTransferData.setNameAndValue("ContTypeFlag","2" );
		    			tSMSVData.add(tSMSTransferData);
		    			tSMSVData.add( tG );
	    				QySendMessgeMBL tQySendMessgeMBL = new QySendMessgeMBL();
	    				if(tQySendMessgeMBL.submitData(tSMSVData,"QYMSG")){
	    					System.out.println(tPrtNo+"签单后即时发送短信成功！");
	    				}else{
	    					System.out.println(tPrtNo+"签单后即时发送短信失败！");
	    				}
		    		}
	    		}
	    	}
	    	else
	    	{
		 		  Content = "集体投保单签单失败，原因是: " + strErr;
				  FlagStr = "Fail";
			    if("PREVIEW".equals(workType))
			    {
	    		  Content = " 预打保单失败! ";
			    }
			}
		} // end of if
	} // end of if
	
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
