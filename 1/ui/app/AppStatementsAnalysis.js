var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var i = 0;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>12)
	{
		alert("统计期最多为12个月！");
		return false;
	}
    var tManageCom = fm.ManageCom.value;
    var tSaleChnl = fm.SaleChnl.value;
    if (tSaleChnl==""){
        fm.op.vale ="";      
  	}else{
        fm.op.vale ="and a.salechnl = '"+tSaleChnl+"'";    		
  	}
    var strSQL = "select t.A,t.B,t.L,getUniteCode(t.C),t.D,t.E,t.F,t.G,t.H,t.I,t.J,t.K from (select a.managecom A,a.prtno B,a.ContNo L,a.agentcode C,(select name from laagent where agentcode =a.agentcode) D, " 
             + "b.branchattr E,b.name F, "
             + " db2inst1.codename('lcsalechnl',a.salechnl) G,a.appntname H, "
             + "a.prem I,a.signdate J,'新单' K from lccont a,labranchgroup b where a.agentgroup = b.agentgroup and a.conttype='1' "
             + ""+fm.op.vale+" and a.managecom like '"+tManageCom+"%' and a.signdate between '"+tStartDate+"' and '"+tEndDate+"'  and  "
             + "a.prtno in ( select distinct(prtno) from lcpol where contno = a.contno and signdate between '"+tStartDate+"' and '"+tEndDate+"' "
             + "and conttype='1' and renewcount=0 and managecom like '"+tManageCom+"%') "
             + "union "
             + "select a.managecom A,a.prtno B,a.ContNo L,a.agentcode C,(select name from laagent where agentcode =a.agentcode) D, "
             + "b.branchattr E,b.name F, "
             + " db2inst1.codename('lcsalechnl',a.salechnl) G,a.appntname H, "
             + "a.prem I,a.signdate J,'退保' K from lbcont a,labranchgroup b where a.agentgroup = b.agentgroup and a.conttype='1' "
             + ""+fm.op.vale+" and a.managecom like '"+tManageCom+"%' and a.signdate between "
             + "'"+tStartDate+"' and '"+tEndDate+"'  and  a.prtno in ( select distinct(prtno) from lcpol "
             + "where signdate between '"+tStartDate+"' and '"+tEndDate+"'  and conttype='1' and renewcount=0 "
             + "and managecom like '"+tManageCom+"%') ) as t order by t.K,t.A,t.E,t.C,t.G with ur "
 
    fm.AnalysisSql.value = strSQL; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}
	fm.submit();
}