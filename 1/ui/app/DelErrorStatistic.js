var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm() {	
//	if(verifyInput() == false) {
//		return false;
//	}
	var tMngCom = fm.ManageCom.value;
	var tStartDate = fm.StartDate.value;
	var tEndDate = fm.EndDate.value;
	var type = fm.QYType.value;
	if(tMngCom == "" || tMngCom == "null" || tMngCom == null) {
		alert("管理机构不能为空！");
		return ;
	}
	if (tStartDate == null || tStartDate == "null" || tStartDate =="") {
		alert("统计起期不能为空！");
		return ;
	}
    if (tEndDate == null || tEndDate == "null" || tEndDate =="") {
    	alert("统计止期不能为空！");
    	return ;
    }
	if(type==null || type=="" || type=="null") {
		alert("下载类型不能为空！");
		return ;
	}
	if(dateDiff(tStartDate,tEndDate,"M")>3) {
	    alert("统计期最多为三个月！");
	    return ;
	}
	var tStrSQL = "";
	if(dateDiff(tStartDate,tEndDate,"M")<1){
	      alert("统计止期比统计起期早");
	      return ;
	}

//    var length=tMngCom.split("").length;
//    var managecom="";
//    if(length==8){
//        managecom="length('"+tMngCom+"')";
//    }else{
//        managecom="length('"+tMngCom+"')*2";
//    }	 
    if(type=="1"){
    	tStrSQL = "select substr(bpo.managecom, 1, 4), "+
    	"       (select name from ldcom where comcode = substr(bpo.managecom, 1, 4)), "+
    	"       bpo.managecom, "+
    	"       (select name from ldcom where comcode = bpo.managecom), "+
    	"       bpo.bussno, "+
    	"       lcc.appntname, "+
    	"       Codename('lcsalechnl', Lcc.Salechnl), "+
    	"       getUniteCode(lcc.agentcode), "+
    	"       (select name from laagent where agentcode = lcc.agentcode), "+
    	"       (select BranchAttr "+
    	"          from LABranchGroup "+
    	"         where AgentGroup = lcc.AgentGroup), "+
    	"       (select name from LABranchGroup where AgentGroup = lcc.AgentGroup), "+
    	"       es.MakeDate, "+
    	"       es.ScanOperator, "+
    	"       lcc.FirstTrialOperator, "+
    	"       batchno, "+
    	"       '已录入', "+
    	"       case "+
    	"         when (select count(1) "+
    	"                 from LCGrpImportLog "+
    	"                where prtno = bpo.bussno "+
    	"                  and batchno = bpo.batchno "+
    	"                  and errorstate = '1') > 0 then "+
    	"          '是' "+
    	"         else "+
    	"          '否' "+
    	"       end, "+
    	"       bpo.makedate "+
    	"  from bpomissionstate bpo "+
    	" inner join es_doc_main es "+
    	"    on es.doccode = bpo.bussno "+
    	" inner join lccont lcc "+
    	"    on lcc.prtno = bpo.bussno "+
    	" where 1 = 1 "+
    	"   and bpo.makedate between '"+tStartDate+"' and '"+tEndDate+"' "+
    	"   and bpo.managecom like '"+tMngCom+"%' "+
    	"   and bpo.state = '04' "+
    	"   and subtype in ('TB01', 'TB29') "+
    	"union all "+
    	"select substr(bpo.managecom, 1, 4), "+
    	"       (select name from ldcom where comcode = substr(bpo.managecom, 1, 4)), "+
    	"       bpo.managecom, "+
    	"       (select name from ldcom where comcode = bpo.managecom), "+
    	"       bpo.bussno, "+
    	"       lcc.appntname, "+
    	"       Codename('lcsalechnl', Lcc.Salechnl), "+
    	"       getUniteCode(lcc.agentcode), "+
    	"       (select name from laagent where agentcode = lcc.agentcode), "+
    	"       (select BranchAttr "+
    	"          from LABranchGroup "+
    	"         where AgentGroup = lcc.AgentGroup), "+
    	"       (select name from LABranchGroup where AgentGroup = lcc.AgentGroup), "+
    	"       es.MakeDate, "+
    	"       es.ScanOperator, "+
    	"       lcc.FirstTrialOperator, "+
    	"       batchno, "+
    	"       '已录入', "+
    	"       case "+
    	"         when (select count(1) "+
    	"                 from LCGrpImportLog "+
    	"                where prtno = bpo.bussno "+
    	"                  and batchno = bpo.batchno "+
    	"                  and errorstate = '1') > 0 then "+
    	"          '是' "+
    	"         else "+
    	"          '否' "+
    	"       end, "+
    	"       bpo.makedate "+
    	"  from bpomissionstate bpo "+
    	" inner join es_doc_main es "+
    	"    on es.doccode = bpo.bussno "+
    	" inner join lbcont lcc "+
    	"    on lcc.prtno = bpo.bussno "+
    	" where 1 = 1 "+
    	"   and bpo.makedate between '"+tStartDate+"' and '"+tEndDate+"' "+
    	"   and bpo.managecom like '"+tMngCom+"%' "+
    	"   and bpo.state = '04' "+
    	"   and subtype in ('TB01', 'TB29') "+
    	"   and edorno not like 'xb%' "+
    	"union all "+
    	"select substr(bpo.managecom, 1, 4), "+
    	"       (select name from ldcom where comcode = substr(bpo.managecom, 1, 4)), "+
    	"       bpo.managecom, "+
    	"       (select name from ldcom where comcode = bpo.managecom), "+
    	"       bpo.bussno, "+
    	"       '', "+
    	"       '', "+
    	"       '', "+
    	"       '', "+
    	"       '', "+
    	"       '', "+
    	"       null, "+
    	"       '', "+
    	"       '', "+
    	"       batchno, "+
    	"       case "+
    	"         when bpo.state = '05' then "+
    	"          '不录入' "+
    	"         else "+
    	"          '已删除' "+
    	"       end, "+
    	"       case "+
    	"         when (select count(1) "+
    	"                 from LCGrpImportLog "+
    	"                where prtno = bpo.bussno "+
    	"                  and batchno = bpo.batchno "+
    	"                  and errorstate = '1') > 0 then "+
    	"          '是' "+
    	"         else "+
    	"          '否' "+
    	"       end, "+
    	"       bpo.makedate "+
    	"  from bpomissionstate bpo "+
    	" where 1 = 1 "+
    	"   and bpo.makedate between '"+tStartDate+"' and '"+tEndDate+"' "+
    	"   and bpo.managecom like '"+tMngCom+"%' "+
    	"   and bpo.state in ('05', '06') ";

    }else if(type=="2"){
    	tStrSQL = "select substr(lc.managecom, 1, 4), "+
    	"       (select name from ldcom where comcode = substr(lc.managecom, 1, 4)), "+
    	"       lc.managecom, "+
    	"       (select name from ldcom where comcode = lc.managecom), "+
    	"       lc.CardFlag || ' - ' || (case lc.CardFlag "+
    	"         when '0' then "+
    	"          '标准件' "+
    	"         when '9' then "+
    	"          '银保通' "+
    	"         else "+
    	"          '简易件' "+
    	"       end), "+
    	"       lc.prtno, "+
    	"       lc.appntname, "+
    	"       Codename('lcsalechnl', Lc.Salechnl), "+
    	"       getUniteCode(lc.agentcode), "+
    	"       (select name from laagent where agentcode = lc.agentcode), "+
    	"       (select BranchAttr "+
    	"          from LABranchGroup "+
    	"         where AgentGroup = lc.AgentGroup), "+
    	"       (select name from LABranchGroup where AgentGroup = lc.AgentGroup), "+
    	"       (select makedate from es_doc_main where doccode = lc.prtno fetch first 1 row only), "+
    	"       (select ScanOperator from es_doc_main where doccode = lc.prtno fetch first 1 row only), "+
    	"       lc.FirstTrialOperator, "+
    	"       case "+
    	"         when lc.appflag = '1' then "+
    	"          '承保' "+
    	"         when uwflag = 'a' then "+
    	"          '撤销' "+
    	"         when uwflag = '1' then "+
    	"          '谢绝承保' "+
    	"         else "+
    	"          '投保' "+
    	"       end "+
    	"  from lccont lc "+
    	" where 1 = 1 "+
    	"   and cardflag in ('0', '1', '2', '3', '5', '6', '8') "+
    	"   and conttype = '1' "+
    	"   and managecom like '"+tMngCom+"%' "+
    	"   and (case "+
    	"         when InputDate is not null then "+
    	"          InputDate "+
    	"         else "+
    	"          lc.makedate "+
    	"       end) between '"+tStartDate+"' and '"+tEndDate+"' "+
    	"union all "+
    	"select substr(lc.managecom, 1, 4), "+
    	"       (select name from ldcom where comcode = substr(lc.managecom, 1, 4)), "+
    	"       lc.managecom, "+
    	"       (select name from ldcom where comcode = lc.managecom), "+
    	"       lc.CardFlag || ' - ' || (case lc.CardFlag "+
    	"         when '0' then "+
    	"          '标准件' "+
    	"         when '9' then "+
    	"          '银保通' "+
    	"         else "+
    	"          '简易件' "+
    	"       end), "+
    	"       lc.prtno, "+
    	"       lc.appntname, "+
    	"       Codename('lcsalechnl', Lc.Salechnl), "+
    	"       getUniteCode(lc.agentcode), "+
    	"       (select name from laagent where agentcode = lc.agentcode), "+
    	"       (select BranchAttr "+
    	"          from LABranchGroup "+
    	"         where AgentGroup = lc.AgentGroup), "+
    	"       (select name from LABranchGroup where AgentGroup = lc.AgentGroup), "+
    	"       (select makedate from es_doc_main where doccode = lc.prtno fetch first 1 row only), "+
    	"       (select ScanOperator from es_doc_main where doccode = lc.prtno fetch first 1 row only), "+
    	"       lc.FirstTrialOperator, "+
    	"       '退保' "+
    	"  from lbcont lc "+
    	" where 1 = 1 "+
    	"   and cardflag in ('0', '1', '2', '3', '5', '6', '8') "+
    	"   and conttype = '1' "+
    	"   and managecom like '"+tMngCom+"%' "+
    	"   and edorno not like 'xb%' "+
    	"   and inputdate between '"+tStartDate+"' and '"+tEndDate+"' "+
    	"union all "+
    	"select substr(lc.managecom, 1, 4), "+
    	"       (select name from ldcom where comcode = substr(lc.managecom, 1, 4)), "+
    	"       lc.managecom, "+
    	"       (select name from ldcom where comcode = lc.managecom), "+
    	"       lc.CardFlag || ' - ' || (case lc.CardFlag "+
    	"         when '0' then "+
    	"          '标准件' "+
    	"         when '9' then "+
    	"          '银保通' "+
    	"         else "+
    	"          '简易件' "+
    	"       end), "+
    	"       lc.prtno, "+
    	"       lc.appntname, "+
    	"       Codename('lcsalechnl', Lc.Salechnl), "+
    	"       getUniteCode(lc.agentcode), "+
    	"       (select name from laagent where agentcode = lc.agentcode), "+
    	"       (select BranchAttr "+
    	"          from LABranchGroup "+
    	"         where AgentGroup = lc.AgentGroup), "+
    	"       (select name from LABranchGroup where AgentGroup = lc.AgentGroup), "+
    	"       null, "+
    	"       (select ScanOperator from es_doc_main where doccode = lc.prtno fetch first 1 row only), "+
    	"       '', "+
    	"       '删除' "+
    	"  from lobcont lc "+
    	" where 1 = 1 "+
    	"   and cardflag in ('0', '1', '2', '3', '5', '6', '8') "+
    	"   and conttype = '1' "+
    	"   and managecom like '"+tMngCom+"%' "+
    	"   and (case "+
    	"         when InputDate is not null then "+
    	"          InputDate "+
    	"         else "+
    	"          lc.makedate "+
    	"       end) between '"+tStartDate+"' and '"+tEndDate+"' ";

    }
    fm.querySql.value = tStrSQL;
    fm.action="DelErrorStatisticDown.jsp";
    fm.submit();
}


//下拉框选择后执行
//function afterCodeSelect(cName, Filed)
//{	
//
//  if(cName=='QYType')
//  {
//  	if(fm.QYType.value == '1')
//  	{
//  		fm.ContType.value = '';
//  		fm.ContTypeName.value = '';
//    	fm.all("tContType").style.display = 'none';
//    	fm.all("tdContType").style.display = 'none';
//  	}
//  	else
//  	{
//  		fm.ContType.value = '';
//  		fm.ContTypeName.value = '';
//    	fm.all("tContType").style.display = '';
//    	fm.all("tdContType").style.display = '';
//  	}
//	}
//}

/**
 * 查询银行网点
 */
//function queryAgentComBank()
//{
//    var tTmpUrl = "../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01";
//    showInfo=window.open(tTmpUrl);
//}

/**
 * 查询结构返现
 */
//function afterQuery(arrQueryResult)
//{
//    if(arrQueryResult)
//    {
//        fm.agentCom.value = arrQueryResult[0][0];
//    }
//}


	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
