<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
	//输入参数
	UpdateRemarkUI tUpdateRemarkUI = new UpdateRemarkUI();

	String mAfterUpdateRemark = request.getParameter("AfterUpdateRemark");
	String action = request.getParameter("action");//action为save表示保存为update表示更新为delete表示删除
	String tRadio[] = request.getParameterValues("InpPolGridSel");
	String tPrtNo[] = request.getParameterValues("PolGrid3");
	String tContType[] = request.getParameterValues("PolGrid6");

	Date nowModifyDate = new Date();
	//String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	String nowModifyTime = Long.toString(nowModifyDate.getTime());
	
	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	String strInput = "";
	
	String uSql = "";
	
	TransferData transferData = new TransferData();
	
	for (int i=0; i< tRadio.length;i++){
		if("1".equals(tRadio[i])){
			transferData.setNameAndValue("PrtNo", tPrtNo[i]);
			transferData.setNameAndValue("ContType", tContType[i]);
			transferData.setNameAndValue("AfterUpdateRemark", mAfterUpdateRemark);
			break;
		}
	}
	
	//传递数据库操作的参数
	transferData.setNameAndValue("action", action);
	
	if(!"Fail".equals(FlagStr)){
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			tUpdateRemarkUI.submitData(tVData, "");
		} catch (Exception ex) {
			if("save".equals(action)||"update".equals(action)){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";	
			}else if("delete".equals(action)){
				Content = "删除失败，原因是:" + ex.toString();
				FlagStr = "Fail";					
			}
		}			


		if (!FlagStr.equals("Fail")) {
			tError = tUpdateRemarkUI.mErrors;
			if (!tError.needDealError()) {
				if("save".equals(action)||"update".equals(action)){
					Content = " 保存成功! ";
					FlagStr = "Succ";
				}else if("delete".equals(action)){
					Content = " 删除成功! ";
					FlagStr = "Succ";
				}
			} else {
				if("save".equals(action)||"update".equals(action)){
					Content = " 保存失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
				}else if("delete".equals(action)){
					Content = " 删除失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
				}
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

