<%
//程序名称：EasyScanQuery.jsp
//程序功能：扫描件显示
//创建日期：2002-09-28 17:06:57
//创建人  ：胡博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>

<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<%@include file="../common/EasyScanQuery/EasyScanQueryKernel.jsp"%>

</head>

<%
String ptrNo = request.getParameter("PrtNo");
//ptrNo = "004";

String[] arrPic = easyScanQueryKernel(ptrNo);
System.out.println(arrPic[0] + "\n" + arrPic[1]);
%>

<body border="0">
<center id="centerPic"></center>
<img border="0" id="service" src="">
</body>
</html>

<script>
try {
  var arrPic   = decodeEasyQueryResult('<%=arrPic[0]%>', 1);
  var arrHost  = decodeEasyQueryResult('<%=arrPic[1]%>', 1);
  var picNum   = arrPic.length;
  var arrPicName = new Array();

  for (var i=0; i<picNum; i++) {
    if (arrPic[i][6] == "1") {
      //localhost:8900/ui/用于测试，以后会使用arrPic[i][3]中的值，但必须与arrHost[0]匹配
      arrPicName[i] = "http://localhost:8900/ui/" + arrPic[i][5] + arrPic[i][4] + ".jpg";
    } else {
      arrPicName[i] = "该图片扫描失败或上传失败！"
    }
  }

  window.top.fraInterface.pic_name = arrPicName;
  
  //document.write("http://localhost:8900/ui/" + arrPic[picIndex][5] + arrPic[picIndex][4] + ".jpg");
  window.service.src = arrPicName[0];
  window.centerPic.innerHTML = arrPicName[0];
} catch(ex) { 
alert("EasyScanQuery.jsp:" + ex); 
}
</script>

