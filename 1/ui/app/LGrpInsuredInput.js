var turnPage = new turnPageClass(); 

function initContInfo() {
	var contQuerySql = "select (select name from ldcom where comcode=gc.managecom),(select distinct codename from ldcode where codetype in ('salechnl','lcsalechnl') and code=gc.salechnl),gc.grpname,gc.cvalidate,gc.cinvalidate from lcgrpcont gc where prtno='" + PrtNo + "' with ur";
	var arrResult = easyExecSql(contQuerySql);
	if(arrResult){
		fm.PrtNo.value = PrtNo;
		fm.ManageCom.value = arrResult[0][0];
		fm.SaleChnl.value = arrResult[0][1];
		fm.GrpName.value = arrResult[0][2];
		fm.CValiDate.value = arrResult[0][3];
		fm.CInValiDate.value = arrResult[0][4];
		refreshInsured();
	} else {
		alert("初始化保单信息异常！");
		return false;
	}
}
function refreshInsured() {
	var insuredQuerySql = "select ContPlanCode,(select contplanname from lccontplan where contplancode=x.ContPlanCode and grpcontno='" + GrpContNo + "'),count(1),0,sum(sumcheck),sum(sumcheckerror),sum(sumcal),sum(sumcalerror) from (select ContPlanCode,case DataChkFlag when '01' then 1 else 0 end sumcheck, case DataChkFlag when '02' then 1 else 0 end sumcheckerror,case CalState when '01' then 1 else 0 end sumcal,case CalState when '03' then 1 else 0 end sumcalerror from LCGrpSubInsuredImport where prtno='" +  PrtNo + "') as x group by ContPlanCode with ur";
	turnPage.queryModal(insuredQuerySql, ContPlanPeoplesGrid);
	
	var sumInput = 0;
	var sumInputError = 0;
	var sumCheck = 0;
	var sumCheckError = 0;
	var sumCalculate = 0;
	var sumCalculateError = 0;
	
	var rowNum = ContPlanPeoplesGrid.mulLineCount;
	
	for(var row = 0; row < rowNum; row++){
		sumInput += parseInt(ContPlanPeoplesGrid.getRowColData(row, 3));
		sumInputError += parseInt(ContPlanPeoplesGrid.getRowColData(row, 4));
		sumCheck += parseInt(ContPlanPeoplesGrid.getRowColData(row, 5));
		sumCheckError += parseInt(ContPlanPeoplesGrid.getRowColData(row, 6));
		sumCalculate += parseInt(ContPlanPeoplesGrid.getRowColData(row, 7));
		sumCalculateError += parseInt(ContPlanPeoplesGrid.getRowColData(row, 8));
	}
	
	fm.InputInsured.value = sumInput;
	fm.InputErrorInsured.value = sumInputError;
	fm.CheckInsured.value = sumCheck;
	fm.CheckErrorInsured.value = sumCheckError;
	fm.CalculateInsured.value = sumCalculate;
	fm.CalculateErrorInsured.value = sumCalculateError;
	
	var premQuery = "select prem from lcgrpcont where prtno='" + PrtNo + "'";
	var arrResult = easyExecSql(premQuery);
	if(arrResult){
		fm.Peoples.value = sumCalculate;
		fm.Prem.value = arrResult[0][0];
	}
	
}
function openInputInsured() {
	window.open("LGrpInputInsuredMain.jsp?PrtNo=" + PrtNo + "&GrpContNo=" + GrpContNo + "&Resource=" + Resource + "&LoadFlag="+LoadFlag);
}

function openQueryInsured() {
	window.open("LGrpQueryInsuredMain.jsp?PrtNo=" + PrtNo + "&GrpContNo=" + GrpContNo + "&Resource=" + Resource + "&LoadFlag="+LoadFlag);
}

function openUpdateInsured() {
	window.open("LGrpUpdateInsuredMain.jsp?PrtNo=" + PrtNo + "&GrpContNo=" + GrpContNo + "&Resource=" + Resource + "&LoadFlag="+LoadFlag);
}

function calDeal() {
	var showStr="正在处理数据……";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "LGrpCalSave.jsp?PrtNo=" + PrtNo + "&GrpContNo=" + GrpContNo;
	fm.submit();
}

function checkDeal(){
	var showStr="正在处理数据……";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "LGrpCheckSave.jsp?PrtNo=" + PrtNo + "&GrpContNo=" + GrpContNo;
	fm.submit();
}

function NoNameCont()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		
	fm.action = "./LGrpNoNameContSave.jsp?GrpContNo="+GrpContNo;	
	fm.submit();
}

function goBack(){
	top.close();
}

function afterSubmit(flagStr, content) {

	refreshInsured();
	showInfo.close();
	
	var urlStr = "";
	if (flagStr == "Fail") {             
		urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	} else { 
    	urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
   		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}