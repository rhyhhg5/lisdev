<%
//程序名称：LdSpecialProductsInit.jsp
//程序功能：特殊产品定义（特殊产品打印时，指定医院和推荐意见合并统一显示，所以需配置特殊产品）
	//创建日期：201-07-07 12:00:16
	//创建人  ：郭忠华
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
function initDiv(){
	fm.operator.value=operator;;
    fm.SpecRiskCode.value="";
    fm.SpecRiskCode1.value="";
    fm.RiskCodeName.value="";
    fm.RiskShowName.value="";
}
function initForm()
{
  try
  {
    initSpecialGrid();
    initDiv()
  }
  catch(ex)
  {
    alert("在LDWorkTimeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }   
}
function initSpecialGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
      iArray[1]=new Array();
      iArray[1][0]="产品编码";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="产品名称";         			//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=50;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="显示名称";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
                           
      SpecialGrid = new MulLineEnter( "fm" , "SpecialGrid" );                       
      SpecialGrid.mulLineCount = 0;
      SpecialGrid.displayTitle = 1;
      SpecialGrid.locked = 1;
      SpecialGrid.canSel = 1;
      SpecialGrid.hiddenPlus = 1;
      SpecialGrid.hiddenSubtraction = 1;

      SpecialGrid.loadMulLine(iArray); 
          
      SpecialGrid.selBoxEventFuncName = "SpecialProductsShow";      

      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>