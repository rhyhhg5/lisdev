<%
//程序名称：LCInuredListInput.jsp
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">                                  
function initForm()
{
  try
  { 
  initContPlanPeoplesGrid();
    initDiskErrQueryGrid();

  }
  catch(re)
  {
    alert("LCInuredListInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initDiskErrQueryGrid()
{
    var iArray = new Array();
      
    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）             
      iArray[0][1]="30px";         			//列宽                                                     
      iArray[0][2]=10;          			//列最大值                                                 
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[1]=new Array();                                                                                       
      iArray[1][0]="团体保单号";    			//列名                                                     
      iArray[1][1]="150px";            			//列宽                                                     
      iArray[1][2]=100;            			//列最大值                                                 
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许5                    
                                                                                                                   
      iArray[2]=new Array();                                                                                       
      iArray[2][0]="批次号";         		//列名                                                     
      iArray[2][1]="150px";            			//列宽                                                     
      iArray[2][2]=100;            			//列最大值                                                 
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许 0                     

      iArray[3]=new Array();                                                                                       
      iArray[3][0]="合同ID";         		//列名                                                     
      iArray[3][1]="100px";            			//列宽                                                     
      iArray[3][2]=100;            			//列最大值                                                 
      iArray[3][3]=3;             			//是否允许输入,1表示允许，0表示不允许1                    
         
      iArray[4]=new Array();                                                                                       
      iArray[4][0]="被保人ID";         		//列名                                                     
      iArray[4][1]="100px";            			//列宽                                                     
      iArray[4][2]=100;            			//列最大值                                                 
      iArray[4][3]=0;             			//是否允许输入,1表示允许，0表示不允许1                    
                                                                                                                          
      iArray[5]=new Array();                                                                                       
      iArray[5][0]="被保人姓名";         		//列名                                                     
      iArray[5][1]="100px";            			//列宽                                                     
      iArray[5][2]=100;            			//列最大值                                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用6     
                                                                                                                   
      iArray[6]=new Array();                                                                                       
      iArray[6][0]="错误信息";         		//列名                                                     
      iArray[6][1]="700px";            			//列宽                                                     
      iArray[6][2]=100;            			//列最大值                                                 
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用3
                                                                                                           
      DiskErrQueryGrid = new MulLineEnter( "fm" , "DiskErrQueryGrid" ); 
      //这些属性必须在loadMulLine前
      DiskErrQueryGrid.mulLineCount = 0;   
      DiskErrQueryGrid.displayTitle = 1;
	  	DiskErrQueryGrid.hiddenPlus = 1;
      DiskErrQueryGrid.hiddenSubtraction = 1;
      DiskErrQueryGrid.canSel = 1;
      DiskErrQueryGrid.selBoxEventFuncName = "showList";
      DiskErrQueryGrid.loadMulLine(iArray);  

      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initContPlanPeoplesGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";         			//列宽
        iArray[0][2]=10;          			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="保障计划";    			//列名
        iArray[1][1]="20px";            			//列宽
        iArray[1][2]=20;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许5

        iArray[2]=new Array();
        iArray[2][0]="人员类别";         		//列名
        iArray[2][1]="30px";            			//列宽
        iArray[2][2]=20;            			//列最大值
        iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0

        iArray[3]=new Array();
        iArray[3][0]="导入被保险人数";         		//列名
        iArray[3][1]="20px";            			//列宽
        iArray[3][2]=20;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0

        ContPlanPeoplesGrid = new MulLineEnter("fm", "ContPlanPeoplesGrid");
        //这些属性必须在loadMulLine前
        ContPlanPeoplesGrid.mulLineCount = 0;
        ContPlanPeoplesGrid.displayTitle = 1;
        ContPlanPeoplesGrid.hiddenPlus = 1;
        ContPlanPeoplesGrid.hiddenSubtraction = 1;
        ContPlanPeoplesGrid.canSel = 0;
        ContPlanPeoplesGrid.canChk = 0;
        ContPlanPeoplesGrid.loadMulLine(iArray);
 
    }
    catch(ex)
    {
        alert(ex);
    }
}
</script>
