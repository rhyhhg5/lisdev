<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
	//个人下个人

	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>

	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="ScanRecheckInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ScanRecheckInit.jsp"%>
<title>保单状态查询</title>
</head>
<body onload="initForm();">
	<form method=post name=fm target="fraSubmit"
		action="./PolStatusChk.jsp">
		<!-- 保单信息部分 -->
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>
		<table class=common align=center>
			<TR class=common>
				<!--<TD  class= title>
            投保单号码
          </TD>
          <TD  class= input>-->
				<Input class=common name=ProposalContNo type="hidden">
				<TD class=title>印刷号</TD>
				<TD class=input><Input class=common name=PrtNo></TD>
				<TD class=title>管理机构</TD>
				<TD class=input><Input class="codeNo"  name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>
			</TR>
			<TR class=common>
			<TD class= title>录入日期</TD>
          	<TD class= input>
          	<Input class="coolDatePicker" name=MakeDate verify="录入日期|date">
          	</TD>
			<td CLASS="title">销售渠道 </td>
			<td CLASS="input" COLSPAN="1">
			<input name="SaleChnl" VALUE MAXLENGTH="2"  CLASS=codeNo ondblclick="return showCodeList('unitesalechnl',[this,SaleChnlName],[0,1],null,'1','1',1);" onkeyup="return showCodeListKey('unitesalechnl',[this,SaleChnlName],[0,1],null,'1','1',1);" verify="销售渠道|code:unitesalechnl&notnull"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>   
    		</td> 
			</TR>
		</table>
		<INPUT VALUE="查  询" class=CssButton TYPE=button
			onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divLCPol1);"></td>
				<td class=titleImg>保单信息</td>
			</tr>
		</table>
		<Div id="divLCPol1" style="display: ''" align=center>
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1><span id="spanPolGrid">
					</span></td>
				</tr>
			</table>
			<INPUT VALUE="首  页" class=CssButton TYPE=button
				onclick="getFirstPage();"> <INPUT VALUE="上一页"
				class=CssButton TYPE=button onclick="getPreviousPage();"> <INPUT
				VALUE="下一页" class=CssButton TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class=CssButton TYPE=button
				onclick="getLastPage();">
		</div>
		<p>
			<INPUT VALUE="影像复查" class=CssButton TYPE=button
				onclick="getStatus();">
		</p>
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
