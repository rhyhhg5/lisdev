<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：ScannerQuerySave.jsp
//程序功能：投保单扫描件删除功能
//创建日期：2008-08-13
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@ page import="com.sinosoft.utility.*" %>
<%@ page import="com.sinosoft.lis.pubfun.*" %>
<%@ page import="com.sinosoft.lis.tb.*" %>

<%
System.out.println("begin ScannerQuerySave.jsp...");

//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";

GlobalInput tG = (GlobalInput)session.getValue("GI");

String tDocId = request.getParameter("DocId");
System.out.println("docId : " + tDocId);

TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("DocId", tDocId);

ScanDeleUI tScanDeleUI = new ScanDeleUI();
try
{
    VData tVData = new VData();
    tVData.add(tG);
    tVData.add(tTransferData);
    
    if (!tScanDeleUI.submitData(tVData, null))
    {
        Content = " 删除失败! 原因是: " + tScanDeleUI.mErrors.getLastError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 删除成功！";
        FlagStr = "Succ";
    }
}
catch(Exception ex)
{
    Content = "删除失败，原因是:" + ex.toString();
    FlagStr = "Fail";
}
System.out.println("end ScannerQuerySave.jsp...");

%>
<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
