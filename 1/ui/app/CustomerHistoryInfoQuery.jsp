<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="CustomerHistoryInfoQuery.js"></script>
    <%@include file="CustomerHistoryInfoQueryInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">团体客户号</td>
                    <td class="input8">
                        <input class="common" name="GrpCustomerNo" verify="团体客户号|notnull" />
                    </td>
                    <td class="title8">&nbsp;</td>
                    <td class="input8">&nbsp;</td>
                    <td class="title8">&nbsp;</td>
                    <td class="input8">&nbsp;</td>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryGrpCustomerInfo();" /> 	
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divGrpCustomerGrid);" />
                </td>
                <td class="titleImg">查询结果列表</td>
            </tr>
        </table>
        <div id="divGrpCustomerGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanGrpCustomerGrid"></span> 
                    </td>
                </tr>
            </table>
        </div>

        <div id="divGrpCustomerGridPage" style="display: ''" align="center">
            <input type="button" class="cssButton" value="首  页" onclick="getFirstPage();" /> 
            <input type="button" class="cssButton" value="上一页" onclick="getPreviousPage();" /> 					
            <input type="button" class="cssButton" value="下一页" onclick="getNextPage();" /> 
            <input type="button" class="cssButton" value="尾  页" onclick="getLastPage();" /> 					
        </div>
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnDelMission" value="查看既往信息" onclick="queryHistoryInfo();" />   
                </td>
            </tr>
        </table>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
