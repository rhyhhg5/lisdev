<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：
//程序功能：查询分公司外包件数
//创建日期：2008-3-20
//创建人  ：ZhangJianBao
//更新记录：更新人    更新日期     更新原因/内容
%>

<%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
%>

<script language="JavaScript">

  var tManageCom = "<%=tGI.ComCode%>";
  var tCurrentDate = "<%=tCurrentDate%>";

  function initForm()
  {
    try
    {
      initInputBox();
      initBPONoGrid();
      initElementtype();
    }
    catch(ex)
    {
      alert("初始化界面错误，" + ex.message);
    }
  }

  // 错误信息列表的初始化
  function initBPONoGrid()
  {
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=20;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="公司代码";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="manageCom";

      iArray[2]=new Array();
      iArray[2][0]="公司名称";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="comName";

      iArray[3]=new Array();
      iArray[3][0]="外包件数";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="BPONo";

      BPONoGrid = new MulLineEnter("fm" , "BPONoGrid" );
      //这些属性必须在loadMulLine前
      BPONoGrid.mulLineCount = 0;
      BPONoGrid.displayTitle = 1;
      BPONoGrid.locked = 1;
      BPONoGrid.canSel = 1;
      BPONoGrid.canChk = 0;
      BPONoGrid.hiddenPlus=1;
      BPONoGrid.hiddenSubtraction=1;
      BPONoGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert("初始化" + ex.message);
    }
  }

</script>