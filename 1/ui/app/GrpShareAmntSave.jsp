 <%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
<%
    CErrors tError = null;
	String GrpContNo  = "";   
    String ContPlanCode  = "";             
	String FlagStr = "";
	String Content = "";
	String tOperator = "";
    String prtNo="";
    String SharePlanCode="";
    String SharePlanName="";
    String AmntType="";
    String ShareAmnt="";
    String GetRate="";
    String OutDutyAmnt="";
	GlobalInput tG = new GlobalInput(); 
	
    TransferData mTransferData = new TransferData();
	//获得公共输入信息
	tG=(GlobalInput)session.getValue("GI");
	tOperator = request.getParameter("fmtransact");
    GrpContNo = request.getParameter("GrpContNo");
    ContPlanCode = request.getParameter("ContPlanCode");
    prtNo = request.getParameter("prtNo");
    
    SharePlanCode=request.getParameter("SharePlanCode");
    SharePlanName=request.getParameter("SharePlanName");
    AmntType=request.getParameter("AmntType");
    ShareAmnt=request.getParameter("ShareAmnt");
    GetRate=request.getParameter("GetRate");
    OutDutyAmnt=request.getParameter("OutDutyAmnt");
    
	System.out.println("tOperator: " + tOperator);

  //校验处理
  //内容待填充
	try {
		 LCGrpShareAmntSet mLCGrpShareAmntSet = new LCGrpShareAmntSet();
         LCGrpRiskShareAmntSet  mLCGrpRiskShareAmntSet=new LCGrpRiskShareAmntSet();
		 if("INSERT".equals(tOperator))////新增共用计划
		 {
		 	String[] tPlanCodes= request.getParameterValues("GrpShareAmntGrid1");
		 	String[] tPlanNames= request.getParameterValues("GrpShareAmntGrid2");
            String[] tPlanAmnts= request.getParameterValues("GrpShareAmntGrid3");
		 	for(int i=0;i<tPlanCodes.length;i++){
			 	LCGrpShareAmntSchema tLCGrpShareAmntSchema = new LCGrpShareAmntSchema();
                if("".equals(tPlanCodes[i])){//只保存新增的共用计划
                tLCGrpShareAmntSchema.setPrtNo(prtNo);
                tLCGrpShareAmntSchema.setGrpContNo(GrpContNo);
                tLCGrpShareAmntSchema.setContPlanCode(ContPlanCode);
                tLCGrpShareAmntSchema.setSharePlanName(tPlanNames[i]);
                tLCGrpShareAmntSchema.setShareAmnt(tPlanAmnts[i]);
                tLCGrpShareAmntSchema.setOperator(tG.Operator);
                tLCGrpShareAmntSchema.setMakeDate(PubFun.getCurrentDate());
                tLCGrpShareAmntSchema.setMakeTime(PubFun.getCurrentTime());
                tLCGrpShareAmntSchema.setModifyDate(PubFun.getCurrentDate());
                tLCGrpShareAmntSchema.setModifyTime(PubFun.getCurrentTime());
                mLCGrpShareAmntSet.add(tLCGrpShareAmntSchema);
                }
			 	
		 	}
		 }else if("INSERTDetail".equals(tOperator)){
            String mChk[] = request.getParameterValues("InpGrpShareAmntRiskGridChk");
		 	String[] tRiskCodes= request.getParameterValues("GrpShareAmntRiskGrid1");
            System.out.println("险种编码:"+tRiskCodes[0]);
		 	for(int i=0;i<tRiskCodes.length;i++){
                if(mChk[i].equals("1")){
               LCGrpRiskShareAmntSchema tLCGrpRiskShareAmntSchema = new LCGrpRiskShareAmntSchema();
                tLCGrpRiskShareAmntSchema.setPrtNo(prtNo);
                tLCGrpRiskShareAmntSchema.setGrpContNo(GrpContNo);
                tLCGrpRiskShareAmntSchema.setContPlanCode(ContPlanCode);
                tLCGrpRiskShareAmntSchema.setSharePlanCode(SharePlanCode);
                tLCGrpRiskShareAmntSchema.setRiskcode(tRiskCodes[i]);
                tLCGrpRiskShareAmntSchema.setAmntType(AmntType);
                tLCGrpRiskShareAmntSchema.setShareAmnt(ShareAmnt);
                tLCGrpRiskShareAmntSchema.setGetRate(GetRate);
                tLCGrpRiskShareAmntSchema.setOutDutyAmnt(OutDutyAmnt);
                tLCGrpRiskShareAmntSchema.setOperator(tG.Operator);
                tLCGrpRiskShareAmntSchema.setMakeDate(PubFun.getCurrentDate());
                tLCGrpRiskShareAmntSchema.setMakeTime(PubFun.getCurrentTime());
                tLCGrpRiskShareAmntSchema.setModifyDate(PubFun.getCurrentDate());
                tLCGrpRiskShareAmntSchema.setModifyTime(PubFun.getCurrentTime());
                
                mLCGrpRiskShareAmntSet.add(tLCGrpRiskShareAmntSchema);
                }
			 	
		 	}
		 }else if("DELETE".equals(tOperator)){
         
         mTransferData.setNameAndValue("prtNo", prtNo);
         mTransferData.setNameAndValue("ContPlanCode",ContPlanCode);
         mTransferData.setNameAndValue("SharePlanCode",SharePlanCode);
         }		 
		
		
		 // 准备传输数据 VData
		 VData vData = new VData();
		
		 vData.addElement(tG);
		 vData.addElement(mLCGrpShareAmntSet);
         vData.addElement(mLCGrpRiskShareAmntSet);
		 vData.add(mTransferData);
		 
		 // 数据传输
		 GrpShareAmntUI tGrpShareAmntUI = new GrpShareAmntUI();
		
		 if (!tGrpShareAmntUI.submitData(vData, tOperator)) {
		   Content = " 保存失败，原因是: " + tGrpShareAmntUI.mErrors.getFirstError();
		   FlagStr = "Fail";
		 } else {
		 	Content = " 保存成功 ";
		 	FlagStr = "Succ";
		 }
	} catch(Exception ex) {
		ex.printStackTrace( );
   		Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   		FlagStr = "Fail";
	}
	
%>
<html>
<script language="javascript">	
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
