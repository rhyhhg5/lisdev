//程序名称：ScanContInput.js
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{
  if(!verifyInput2())
    return false;
  var ApplyDate = fm.InputDate.value;
  if(strCurDay!==ApplyDate)
  {
    alert("申请日期不是当天！");
    return false;
  }
  if( verifyInput2() == false )
    return false;
  cPrtNo = fm.PrtNo.value;
  cManageCom = fm.ManageCom.value;
  if(cPrtNo == "")
  {
    alert("请录入印刷号！");
    return;
  }
  if(cManageCom == "")
  {
    alert("请录入管理机构！");
    return;
  }
  //if(isNumeric(cPrtNo)==false)
  //{
  //  alert("印刷号应为数字");
  //  return false;
  //}
  if(type=='2')//对于集体保单的申请
  {
    if (GrpbeforeSubmit() == false)
    {
      alert("已存在该印刷号，请选择其他值!");
      return false;
    }

  }
  else //对于个人保单的申请
  {
    if (beforeSubmit() == false)
    {
      alert("已存在该印刷号，请选择其他值!");
      return false;
    }
  }
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  easyQueryClick();
}

/*********************************************************************
 *  执行新契约扫描的EasyQuery
 *  描述:查询显示对象是扫描件.显示条件:扫描件已上载成功
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 var queryflag=false;
function easyQueryClick()
{
  // 初始化表格

  if((fm.PrtNoQ.value=="" || fm.PrtNoQ.value==null) && (fm.ContNoQ.value=="" || fm.ContNoQ.value==null)){
  alert("打印号和保单号必须录入一个");
  return false;
  }
  if(!verifyInput2())
    return false;
  // 书写SQL语句
  var strSQL = "";
  initGrpGrid();
    strSQL = "select prtno,contno,paymode,bankcode,accname,bankaccno,'个单' from lccont where 1 = 1 "
             + getWherePart('PrtNo','PrtNoQ')
             + getWherePart('contno','ContNoQ')
             + getWherePart('conttype','ContTypeQ')
             + " order by prtno "
             ;
    turnPage.queryModal(strSQL,GrpGrid);
    queryflag=true;
    return true;
 
  //turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  ////判断是否查询成功
  //if (!turnPage.strQueryResult)
  //{
  //  alert("没有已申请的投保单，请录入印刷号开始录入！");
  //  return "";
  //}
  //
  ////查询成功则拆分字符串，返回二维数组
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //
  ////设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  //turnPage.pageDisplayGrid = GrpGrid;
  //
  ////保存SQL语句
  //turnPage.strQuerySql     = strSQL;
  //
  ////设置查询起始位置
  //turnPage.pageIndex       = 0;
  //
  ////在查询结果数组中取出符合页面显示大小设置的数组
  //var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //
  ////调用MULTILINE对象显示查询结果
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

}
function GoToInput()
{
 if(!queryflag){
  alert("请先查询");
  return false;
 }
 

 
  var i = 0;
  var checkFlag = 0;
  var state = "0";

  for (i=0; i<GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getSelNo(i))
    {
      checkFlag = GrpGrid.getSelNo();
      break;
    }
  }

  if (checkFlag)
  {
    
  
     var prtNo = GrpGrid.getRowColData(checkFlag - 1, 1);
     var contNo = GrpGrid.getRowColData(checkFlag - 1, 2);
    
     var bankcode=fm.all("bankcode").value;
     var accname=fm.all("accname").value;
     var bankaccno=fm.all("bankaccno").value;
     
     
     if((fm.bankcode.value!="" && fm.bankcode.value!=null) || (fm.accname.value!="" && fm.accname.value!=null) ||(fm.bankaccno.value!="" && fm.bankaccno.value!=null)){
 if((fm.bankcode.value=="" || fm.bankcode.value==null) || (fm.accname.value=="" || fm.accname.value==null) ||(fm.bankaccno.value=="" || fm.bankaccno.value==null)){
  alert("请将修改的银行信息填写完整!");
  return false;
 }else{
 
  var tSql="select bankcode from ldbank where bankcode='"+bankcode+"'";
    var arrResult=easyExecSql(tSql);
    if(arrResult==null || arrResult.length ==0){
    alert("录入的银行银行编码在系统中不存在,请先查询!");
    return false;
    }
    
    var tSqlapp="select appntno from lcappnt where contno='"+contNo+"' and AppntName='"+accname+"'";
    
    var arrResultapp=easyExecSql(tSqlapp);
    
    if(arrResultapp==null || arrResultapp.length ==0){
    alert("录入的户名与系统中不一致,请先查询!");
    return false;o
    }
 }
 
 }
   
   
      //var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
      //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
  }else{
    alert("请先选择一条保单信息！");
    return false;
  }
  
  fm.submit();
  
}

//选择事件
function selonclick(){
var i = 0;
  var checkFlag = 0;
  var state = "0";

  for (i=0; i<GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getSelNo(i))
    {
      checkFlag = GrpGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag)
  {
    var prtNo = GrpGrid.getRowColData(checkFlag - 1, 1);
    var bankcode = GrpGrid.getRowColData(checkFlag - 1, 4);
    var accname = GrpGrid.getRowColData(checkFlag - 1, 5);
    var bankaccno = GrpGrid.getRowColData(checkFlag - 1, 6);
    
    fm.all("bankcode").value=bankcode;
    fm.all("accname").value=accname;
    fm.all("bankaccno").value=bankaccno;
    
    fm.all("prtno").value=prtNo;
  }
  else
  {
    alert("请先选择一条保单信息！");
  }
}

function beforeSubmit()
{
  
}


function afterSubmit( FlagStr, content )
{

if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    else if (FlagStr=="Succ")
    { 
    
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
        //window.location.reload();
        easyQueryClick();
        fm.all("bankcode").value="";
        fm.all("accname").value="";
        fm.all("bankaccno").value="";
     }
}