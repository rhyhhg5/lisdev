//               该文件中包含客户端需要处理的函数和事件

var mDebug="1";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
window.onfocus=myonfocus;
var arrDataSet;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  getImportPath();

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	//showSubmitFrame(mDebug);
	//ImportPath="/upload";
	divImport.style.display='none';
	ImportFile = fm.all('FileName').value;
	parent.fraInterface.fm.action = "./DiskApplySave.jsp?ImportPath="+ImportPath;
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Result )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
			if (Result!=null&&Result!='')
			{
					var iArray;
				//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
				//保存查询结果字符串
	 		 	turnPage.strQueryResult  = Result;
	  		//使用模拟数据源，必须写在拆分之前
	  		turnPage.useSimulation   = 1;  
	    
	  		//查询成功则拆分字符串，返回二维数组
	  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
				
				turnPage.arrDataCacheSet =chooseArray(tArr,[3,0,1,10,8]);
				//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
			 	turnPage.pageDisplayGrid = LCGrpImportLogGrid;    
			  
			  //设置查询起始位置
			 	turnPage.pageIndex       = 0;
			 	//在查询结果数组中取出符合页面显示大小设置的数组
		  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
				//调用MULTILINE对象显示查询结果
		   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
			 	divImport.style.display='';
    	}
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在ProposalCopy.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./ProposalCopyQuery.html");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function getImportPath ()
{
		// 书写SQL语句
	var strSQL = "";

	strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
			 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	alert("未找到上传路径");
    return;
	}
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  ImportPath = turnPage.arrDataCacheSet[0][0];
}


function easyQueryClick()
{
	
  if(fmquery.all('BatchNo').value==""||fmquery.all('BatchNo').value==null)
  {
  	   if(fmquery.all('GrpPolNo').value==""||fmquery.all('GrpPolNo').value==null)
  	    { 
  	      alert("必须录入一个条件！");
  	      return false;
  	    }
  }
  	
  initDiskErrQueryGrid();
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	// 书写SQL语句
  var strSql = "select GrpContNo,BatchNo,ContID,InsuredID,InsuredName,ErrorInfo from LCGrpImportLog where 1=1 "
				 + "and ErrorType='1' ";
				 if(fmquery.all('GrpPolNo').value!=null&&fmquery.all('GrpPolNo').value!="")
				 {
				 strSql=strSql + "and GrpContNo='"+fmquery.all('GrpPolNo').value+"'" ;
				 }
			     if(fmquery.all('BatchNo').value!=null&&fmquery.all('BatchNo').value!="")
				 {
				  strSql=strSql+ "and BatchNo='"+fmquery.all('BatchNo').value+"'";
				 }
				  strSql=strSql+ " Order by BatchNo,IDNo";
//alert(strSql);

	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	showInfo.close();
    alert("未查询到满足条件的数据！");
     return false;
  }
  
  //设置查询起始位置
  //turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  turnPage.pageLineNum = 20 ;
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = DiskErrQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql 
  
  
  //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,MAXSCREENLINES);
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  showInfo.close();

}


/*********************************************************************
 *  按批次或集体保单号删除磁盘倒入的被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteInsured()
{

	if(fmquery.all('BatchNo').value==""||fmquery.all('BatchNo').value==null)
  {
  	   if(fmquery.all('GrpPolNo').value==""||fmquery.all('GrpPolNo').value==null)
  	    { 
  	      alert("必须录入一个条件！");
  	      return false;
  	    }
  }

  var tBatchNo = fmquery.all('BatchNo').value;
  var tGrpPolNo = fmquery.all('GrpPolNo').value;
  
  alert(tBatchNo);
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	parent.fraInterface.fm.action = "./DiskDeleteInsured.jsp?tBatchNo="+tBatchNo+"&tGrpPolNo="+tGrpPolNo;
  
  fm.submit(); //提交
  
}