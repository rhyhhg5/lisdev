<%@page import="com.sinosoft.utility.*"%>
<!--用户校验类-->
<%
	GlobalInput globalInput = (GlobalInput) session.getValue("GI");
	String strManageCom = globalInput.ManageCom;
	String strOperator = globalInput.Operator;
%>

<script language="JavaScript">
	function initInpBox() {
		try {
	        // 初始化
	       
	        fm.all('Riskcode').value = '';
	        fm.all('Riskname').value = '';
	        fm.all('afterMaxage').value = '';
	        fm.all('afterMinage').value = '';
	        fm.all('beforeMinage').value = '';
	        fm.all('beforeMaxage').value = '';
		} catch (ex) {
			alert("在QYModifyAmntInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}

	function initForm() {
		try {
			initInpBox();
			initPolGrid();
			showAllCodeName();
		} catch (re) {
			alert("QYModifyAmntInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}

	var PolGrid; //定义为全局变量，提供给displayMultiline使用

	// 保单信息列表的初始化
	function initPolGrid() {
		var iArray = new Array();

		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1] = "30px"; //列宽
			iArray[0][2] = 10; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "险种编码"; //列名
			iArray[1][1] = "100px"; //列宽
			iArray[1][2] = 100; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[2] = new Array();
			iArray[2][0] = "险种名称"; //列名
			iArray[2][1] = "100px"; //列宽
			iArray[2][2] = 100; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[3] = new Array();
			iArray[3][0] = "最大年龄限制"; //列名
			iArray[3][1] = "100px"; //列宽
			iArray[3][2] = 100; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array();
			iArray[4][0] = "最小年龄限制"; //列名
			iArray[4][1] = "100px"; //列宽
			iArray[4][2] = 100; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许

			

			PolGrid = new MulLineEnter("fm", "PolGrid");

			//这些属性必须在loadMulLine前
			PolGrid.mulLineCount = 0;
			PolGrid.displayTitle = 1;// 显示标题
			PolGrid.locked = 1;
			PolGrid.canSel = 1;// 显示单选框
			PolGrid.hiddenPlus = 1;
			PolGrid.selBoxEventFuncName = "Polinit";// 单击单选框时,响应 js 函数
			PolGrid.hiddenSubtraction = 1;// 不显示 - 号按钮

			PolGrid.loadMulLine(iArray);

		} catch (ex) {
			alert(ex);
		}
	}
</script>