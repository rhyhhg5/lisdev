//程序名称：AutoMove.js
//程序功能：外包录入随动
//创建日期：2007-11-5
//创建人  ：xiaoxin
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var k = 0;
var turnPage = new turnPageClass();
var cflag = "";
var canReplyFlag = false;

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
 
/**
 * 为每个界面录入控件增加相应随动的事件，在ProposalInput.js中调用 
 **/
function setFocus() {
  for (var elementsNum=0; elementsNum<window.document.forms[0].elements.length; elementsNum++) {
  	window.document.forms[0].elements[elementsNum].onfocus = goToArea;        		
  } 
}


/** 
 * 隐藏红框
 **/
function hiddenPosition() {
  top.fraPic.spanPosition.style.display = "none";
}


/** 
 * 显示红框，将控件框起来 
 **/
function showPosition(l, t, w, h) {
  //alert(l + " " + t + " " + w + " " + h);
  top.fraPic.spanPosition.style.display = "";
  top.fraPic.spanPosition.style.left = l;
  top.fraPic.spanPosition.style.top = t;
  top.fraPic.Rect.width = w;
  top.fraPic.Rect.height = h;
}

/*** 随动 ***/
function goToArea() {

  var goToLock = true;
  var objName = this.name;
  var hx = 0;
  var hy = 0;
  try { hiddenPosition(); } catch(e) {}
  var tPrtNo = fm.PrtNoOld.value;
  var sql = "select SubType "
              + "from Es_Doc_Main "
              + "where DocCode = '" + tPrtNo + "' ";
  var tSubType = easyExecSql(sql);

  if(tSubType =='TB05')
  {
    //合同信息
    try { if (objName == "PrtNo") { goToPic(0); top.fraPic.scrollTo(0, 15); showPosition(842+hx, 95+hy, 388, 104); } } catch(e) {} 
    try { if (objName == "ReceiveDate") { goToPic(0); top.fraPic.scrollTo(0, 15); showPosition(71+hx, 251+hy, 292, 40); } } catch(e) {} 
    try { if (objName == "PolApplyDate") { goToPic(0); top.fraPic.scrollTo(0, 1361); showPosition(648+hx, 1629+hy, 577, 99); } } catch(e) {} 
    try { if (objName == "FirstTrialOperator") { goToPic(0); top.fraPic.scrollTo(0, 15); showPosition(59+hx, 279+hy, 316, 35); } } catch(e) {} 
    try { if (objName == "AgentCode") { goToPic(0); top.fraPic.scrollTo(0, 30); showPosition(59+hx, 206+hy, 320, 37); } } catch(e) {} 
    try { if (objName == "CValiDate") { goToPic(0); top.fraPic.scrollTo(0, 902); showPosition(236+hx, 1204+hy, 641, 45); } } catch(e) {} 
    
    //投保人信息
    try { if (objName == "AppntInfo") { goToPic(0); top.fraPic.scrollTo(0, 194); showPosition(239+hx, 311+hy, 982, 189); } } catch(e) {} 
    try { if (objName == "AppntName") { goToPic(0); top.fraPic.scrollTo(0, 102); showPosition(266+hx, 349+hy, 305, 31); } } catch(e) {} 
    try { if (objName == "AppntBirthday") { goToPic(0); top.fraPic.scrollTo(0, 199); showPosition(545+hx, 342+hy, 465, 41); } } catch(e) {} 
    try { if (objName == "AppntSex") { goToPic(0); top.fraPic.scrollTo(0, 214); showPosition(979+hx, 339+hy, 208, 46); } } catch(e) {} 
    try { if (objName == "AppntSexName") { goToPic(0); top.fraPic.scrollTo(0, 214); showPosition(979+hx, 339+hy, 208, 46); } } catch(e) {} 
    try { if (objName == "AppntIDType") { goToPic(0); top.fraPic.scrollTo(0, 147); showPosition(266+hx, 376+hy, 305, 29); } } catch(e) {} 
    try { if (objName == "AppntIDTypeName") { goToPic(0); top.fraPic.scrollTo(0, 147); showPosition(266+hx, 376+hy, 305, 29); } } catch(e) {} 
    try { if (objName == "AppntIDNo") { goToPic(0); top.fraPic.scrollTo(0, 162); showPosition(558+hx, 373+hy, 643, 30); } } catch(e) {} 
    try { if (objName == "AppntGrpName") { goToPic(0); top.fraPic.scrollTo(0, 177); showPosition(253+hx, 401+hy, 331, 32); } } catch(e) {} 
    try { if (objName == "AppntPosition") { goToPic(0); top.fraPic.scrollTo(0, 192); showPosition(559+hx, 399+hy, 242, 35); } } catch(e) {} 
    try { if (objName == "AppntOccupationCode") { goToPic(0); top.fraPic.scrollTo(0, 207); showPosition(785+hx, 400+hy, 263, 32); } } catch(e) {} 
    try { if (objName == "AppntOccupationType") { goToPic(0); top.fraPic.scrollTo(0, 222); showPosition(1027+hx, 391+hy, 160, 51); } } catch(e) {} 
    try { if (objName == "AppntPostalAddress") { goToPic(0); top.fraPic.scrollTo(0, 237); showPosition(243+hx, 420+hy, 779, 50); } } catch(e) {} 
    try { if (objName == "AppntZipCode") { goToPic(0); top.fraPic.scrollTo(0, 278); showPosition(961+hx, 422+hy, 231, 41); } } catch(e) {} 
    try { if (objName == "AppntPhone") { goToPic(0); top.fraPic.scrollTo(0, 293); showPosition(254+hx, 450+hy, 328, 41); } } catch(e) {} 
    try { if (objName == "AppntMobile") { goToPic(0); top.fraPic.scrollTo(0, 308); showPosition(555+hx, 448+hy, 319, 41); } } catch(e) {} 
    try { if (objName == "AppntEMail") { goToPic(0); top.fraPic.scrollTo(0, 263); showPosition(842+hx, 440+hy, 352, 57); } } catch(e) {} 
   
    //缴费信息
    try { if (objName == "PayInfo") { goToPic(0); top.fraPic.scrollTo(0, 1077); showPosition(240+hx, 1228+hy, 992, 107); } } catch(e) {} 
    try { if (objName == "BPOBankCode") { goToPic(0); top.fraPic.scrollTo(0, 1035); showPosition(238+hx, 1255+hy, 975, 46); } } catch(e) {} 
    try { if (objName == "PremScope") { goToPic(0); top.fraPic.scrollTo(0, 1030); showPosition(814+hx, 1206+hy, 401, 42); } } catch(e) {} 
    try { if (objName == "AppntBankCodeName") { goToPic(0); top.fraPic.scrollTo(0, 1035); showPosition(238+hx, 1255+hy, 975, 46); } } catch(e) {} 
    try { if (objName == "BPOAccName") { goToPic(0); top.fraPic.scrollTo(0, 980); showPosition(907+hx, 1286+hy, 294, 41); } } catch(e) {} 
    try { if (objName == "BPOBankAccNo") { goToPic(0); top.fraPic.scrollTo(0, 1022); showPosition(236+hx, 1283+hy, 760, 45); } } catch(e) {} 
    //被保人信息
    try { if (objName == "InsuredInfo") { goToPic(0); top.fraPic.scrollTo(0, 425); showPosition(234+hx, 467+hy, 984, 195); } } catch(e) {} 
    try { if (objName == "RelationToAppnt") { goToPic(0); top.fraPic.scrollTo(0, 323); showPosition(230+hx, 478+hy, 1001, 43); } } catch(e) {} 
    try { if (objName == "RelationToAppntName") { goToPic(0); top.fraPic.scrollTo(0, 323); showPosition(230+hx, 478+hy, 1001, 43); } } catch(e) {} 
    try { if (objName == "BPOInsuredName") { goToPic(0); top.fraPic.scrollTo(0, 389); showPosition(246+hx, 501+hy, 352, 48); } } catch(e) {} 
    try { if (objName == "BPOInsuredBirthday") { goToPic(0); top.fraPic.scrollTo(0, 404); showPosition(545+hx, 497+hy, 477, 50); } } catch(e) {} 
    try { if (objName == "BPOInsuredSex") { goToPic(0); top.fraPic.scrollTo(0, 419); showPosition(976+hx, 500+hy, 218, 46); } } catch(e) {} 
    try { if (objName == "BPOInsuredSexName") { goToPic(0); top.fraPic.scrollTo(0, 419); showPosition(976+hx, 500+hy, 218, 46); } } catch(e) {} 
    try { if (objName == "BPOInsuredIDType") { goToPic(0); top.fraPic.scrollTo(0, 434); showPosition(243+hx, 526+hy, 359, 50); } } catch(e) {} 
    try { if (objName == "BPOInsuredIDTypeName") { goToPic(0); top.fraPic.scrollTo(0, 434); showPosition(243+hx, 526+hy, 359, 50); } } catch(e) {} 
    try { if (objName == "BPOInsuredIDNo") { goToPic(0); top.fraPic.scrollTo(0, 449); showPosition(544+hx, 528+hy, 662, 45); } } catch(e) {} 
    try { if (objName == "BPOInsuredGrpName") { goToPic(0); top.fraPic.scrollTo(0, 449); showPosition(243+hx, 552+hy, 364, 49); } } catch(e) {} 
    try { if (objName == "BPOInsuredPosition") { goToPic(0); top.fraPic.scrollTo(0, 464); showPosition(543+hx, 553+hy, 285, 49); } } catch(e) {} 
    try { if (objName == "BPOInsuredOccupationCode") { goToPic(0); top.fraPic.scrollTo(0, 479); showPosition(770+hx, 553+hy, 289, 48); } } catch(e) {} 
    try { if (objName == "BPOInsuredOccupationType") { goToPic(0); top.fraPic.scrollTo(0, 494); showPosition(1019+hx, 554+hy, 178, 46); } } catch(e) {} 
    try { if (objName == "BPOInsuredPostalAddress") { goToPic(0); top.fraPic.scrollTo(0, 509); showPosition(257+hx, 581+hy, 759, 42); } } catch(e) {} 
    try { if (objName == "BPOInsuredZipCode") { goToPic(0); top.fraPic.scrollTo(0, 524); showPosition(959+hx, 580+hy, 237, 45); } } catch(e) {} 
    try { if (objName == "BPOInsuredPhone") { goToPic(0); top.fraPic.scrollTo(0, 539); showPosition(252+hx, 609+hy, 338, 47); } } catch(e) {} 
    try { if (objName == "BPOInsuredMobile") { goToPic(0); top.fraPic.scrollTo(0, 554); showPosition(539+hx, 605+hy, 340, 52); } } catch(e) {} 
    try { if (objName == "BPOInsuredEMail") { goToPic(0); top.fraPic.scrollTo(0, 569); showPosition(823+hx, 605+hy, 382, 49); } } catch(e) {} 
  
    //套餐信息
    try { if (objName == "SetInfo") { goToPic(0); top.fraPic.scrollTo(0, 936); showPosition(231+hx, 1014+hy, 1006, 241); } } catch(e) {} 
    try { if (objName == "DutyInfo") { goToPic(0); top.fraPic.scrollTo(0, 936); showPosition(231+hx, 1014+hy, 1006, 241); } } catch(e) {} 
    //收益人信息
    try { if (objName == "BnfInfo") { goToPic(0); top.fraPic.scrollTo(0, 493); showPosition(236+hx, 634+hy, 985, 129); } } catch(e) {} 
  }
  else
  {
	try { if (objName == "PrtNo") 			{ goToPic(0); top.fraPic.scrollTo(0, 1174); showPosition(657+hx, 1442+hy, 466, 82); } } catch(e) {} 
	try { if (objName == "ManageCom") 	{ goToPic(0); top.fraPic.scrollTo(0, 945); showPosition(672+hx, 1084+hy, 389, 73); } } catch(e) {} 
	try { if (objName == "ReceiveDate") { goToPic(0); top.fraPic.scrollTo(0, 960); showPosition(674+hx, 1142+hy, 345, 61); } } catch(e) {} 
	try { if (objName == "PolApplyDate"){ goToPic(2); top.fraPic.scrollTo(0, 1367); showPosition(872+hx, 1530+hy, 354, 68); } } catch(e) {} 
	try { if (objName == "FirstTrialOperator") { goToPic(0); top.fraPic.scrollTo(0, 940); showPosition(652+hx, 1188+hy, 510, 74); } } catch(e) {} 
	try { if (objName == "SaleChnl")	 	{ goToPic(3); top.fraPic.scrollTo(0, 1245); showPosition(302+hx, 1441+hy, 358, 48); } } catch(e) {} 
	try { if (objName == "AgentCode") 	{ goToPic(3); top.fraPic.scrollTo(0, 1367); showPosition(301+hx, 1498+hy, 376, 64); } } catch(e) {} 
	try { if (objName == "CValiDate") 	{ goToPic(1); top.fraPic.scrollTo(0, 1300); showPosition(313+hx, 1441+hy, 335, 60); } } catch(e) {} 
	try { if (objName == "Remark") 			{ goToPic(2); top.fraPic.scrollTo(0, 1001); showPosition(169+hx, 1119+hy, 1044, 184); } } catch(e) {} 


	//投保人信息

	try { if (objName == "AppntName") { goToPic(1); top.fraPic.scrollTo(0, 15); showPosition(260+hx, 114+hy, 199, 51); } } catch(e) {} 
	try { if (objName == "AppntBirthday") { goToPic(1); top.fraPic.scrollTo(0, 30); showPosition(490+hx, 114+hy, 285, 54); } } catch(e) {} 
	try { if (objName == "AppntSex") { goToPic(1); top.fraPic.scrollTo(0, 45); showPosition(780+hx, 107+hy, 162, 60); } } catch(e) {} 
	try { if (objName == "AppntMarriage") { goToPic(1); top.fraPic.scrollTo(0, 60); showPosition(966+hx, 106+hy, 234, 66); } } catch(e) {} 
	try { if (objName == "AppntIDType") { goToPic(1); top.fraPic.scrollTo(0, 75); showPosition(275+hx, 143+hy, 215, 55); } } catch(e) {} 
	try { if (objName == "AppntIDNo") { goToPic(1); top.fraPic.scrollTo(0, 90); showPosition(513+hx, 132+hy, 461, 73); } } catch(e) {} 
	try { if (objName == "AppntNativePlace") { goToPic(1); top.fraPic.scrollTo(0, 105); showPosition(1008+hx, 163+hy, 220, 46); } } catch(e) {} 
	try { if (objName == "AppntGrpName") { goToPic(1); top.fraPic.scrollTo(0, 120); showPosition(270+hx, 174+hy, 335, 52); } } catch(e) {} 
	try { if (objName == "AppntPosition") { goToPic(1); top.fraPic.scrollTo(0, 135); showPosition(643+hx, 190+hy, 158, 44); } } catch(e) {} 
	try { if (objName == "AppntOccupationCode") { goToPic(1); top.fraPic.scrollTo(0, 150); showPosition(839+hx, 186+hy, 149, 53); } } catch(e) {} 
	try { if (objName == "AppntOccupationType") { goToPic(1); top.fraPic.scrollTo(0, 165); showPosition(990+hx, 195+hy, 86, 43); } } catch(e) {} 
	try { if (objName == "AppntSalary") { goToPic(1); top.fraPic.scrollTo(0, 180); showPosition(1081+hx, 190+hy, 120, 58); } } catch(e) {} 
	try { if (objName == "AppntPostalAddress") { goToPic(1); top.fraPic.scrollTo(0, 195); showPosition(272+hx, 202+hy, 763, 62); } } catch(e) {} 
	try { if (objName == "AppntZipCode") { goToPic(1); top.fraPic.scrollTo(0, 210); showPosition(1020+hx, 222+hy, 192, 50); } } catch(e) {} 
	try { if (objName == "AppntPhone") { goToPic(1); top.fraPic.scrollTo(0, 15); showPosition(268+hx, 244+hy, 298, 59); } } catch(e) {} 
	try { if (objName == "AppntMobile") { goToPic(1); top.fraPic.scrollTo(0, 179); showPosition(581+hx, 240+hy, 299, 53); } } catch(e) {} 
	try { if (objName == "AppntEMail") { goToPic(1); top.fraPic.scrollTo(0, 194); showPosition(931+hx, 247+hy, 274, 48); } } catch(e) {} 


	//缴费信息

	try { if (objName == "PayMode") { goToPic(1); top.fraPic.scrollTo(0, 758); showPosition(256+hx, 900+hy, 388, 56); } } catch(e) {} 
	try { if (objName == "PremScope") { goToPic(1); top.fraPic.scrollTo(0, 1300); showPosition(1050+hx, 1465+hy, 150, 40); } } catch(e) {} 
	try { if (objName == "BPOBankCode") { goToPic(1); top.fraPic.scrollTo(0, 773); showPosition(250+hx, 939+hy, 314, 48); } } catch(e) {} 
	try { if (objName == "BPOAccName") { goToPic(1); top.fraPic.scrollTo(0, 788); showPosition(975+hx, 988+hy, 210, 50); } } catch(e) {} 
	try { if (objName == "BPOBankAccNo") { goToPic(1); top.fraPic.scrollTo(0, 803); showPosition(268+hx, 973+hy, 680, 61); } } catch(e) {} 


	//被保人信息

	if(InsuredGrid.getSelNo() == 1)
	{
		//第一被保险人
		try { if (objName == "RelationToAppnt") 		{ goToPic(1); top.fraPic.scrollTo(0, 15); showPosition(475+hx, 281+hy, 458, 56); } } catch(e) {} 
		try { if (objName == "BPOInsuredName") 			{ goToPic(1); top.fraPic.scrollTo(0, 219); showPosition(264+hx, 298+hy, 179, 69); } } catch(e) {}  
		try { if (objName == "BPOInsuredBirthday") 	{ goToPic(1); top.fraPic.scrollTo(0, 234); showPosition(473+hx, 310+hy, 302, 62); } } catch(e) {} 
		try { if (objName == "BPOInsuredSex") 			{ goToPic(1); top.fraPic.scrollTo(0, 249); showPosition(798+hx, 317+hy, 132, 57); } } catch(e) {} 
		try { if (objName == "BPOInsuredMarriage") 	{ goToPic(1); top.fraPic.scrollTo(0, 279); showPosition(947+hx, 324+hy, 260, 49); } } catch(e) {} 
		try { if (objName == "BPOInsuredIDType") 		{ goToPic(1); top.fraPic.scrollTo(0, 264); showPosition(266+hx, 339+hy, 228, 59); } } catch(e) {} 
		try { if (objName == "BPOInsuredIDNo") 			{ goToPic(1); top.fraPic.scrollTo(0, 294); showPosition(509+hx, 344+hy, 470, 60); } } catch(e) {} 
		try { if (objName == "BPOInsuredNativePlace") { goToPic(1); top.fraPic.scrollTo(0, 176); showPosition(990+hx, 353+hy, 228, 58); } } catch(e) {} 
		try { if (objName == "BPOInsuredGrpName") 	{ goToPic(1); top.fraPic.scrollTo(0, 191); showPosition(261+hx, 369+hy, 360, 64); } } catch(e) {} 
		try { if (objName == "BPOInsuredPosition") 	{ goToPic(1); top.fraPic.scrollTo(0, 206); showPosition(634+hx, 372+hy, 170, 62); } } catch(e) {} 
		try { if (objName == "BPOInsuredOccupationCode") { goToPic(1); top.fraPic.scrollTo(0, 221); showPosition(823+hx, 374+hy, 162, 66); } } catch(e) {} 
		try { if (objName == "BPOInsuredOccupationType") { goToPic(1); top.fraPic.scrollTo(0, 236); showPosition(980+hx, 387+hy, 102, 54); } } catch(e) {} 
		try { if (objName == "BPOInsuredSalary") 		{ goToPic(1); top.fraPic.scrollTo(0, 251); showPosition(1083+hx, 386+hy, 118, 59); } } catch(e) {} 
		try { if (objName == "BPOInsuredPostalAddress") { goToPic(1); top.fraPic.scrollTo(0, 266); showPosition(264+hx, 395+hy, 769, 69); } } catch(e) {} 
		try { if (objName == "BPOInsuredZipCode") 		{ goToPic(1); top.fraPic.scrollTo(0, 281); showPosition(1007+hx, 416+hy, 195, 62); } } catch(e) {} 
		try { if (objName == "BPOInsuredPhone") 		{ goToPic(1); top.fraPic.scrollTo(0, 309); showPosition(260+hx, 427+hy, 299, 70); } } catch(e) {} 
		try { if (objName == "BPOInsuredMobile") 		{ goToPic(1); top.fraPic.scrollTo(0, 324); showPosition(570+hx, 434+hy, 321, 68); } } catch(e) {} 
		try { if (objName == "BPOInsuredEMail") 		{ goToPic(1); top.fraPic.scrollTo(0, 339); showPosition(920+hx, 450+hy, 287, 53); } } catch(e) {} 
	}
	else if(InsuredGrid.getSelNo() == 2)
	{
		//第二被保险人
		try { if (objName == "RelationToAppnt") 		{ goToPic(1); top.fraPic.scrollTo(0, 369); showPosition(543+hx, 479+hy, 418, 53); } } catch(e) {} 
		try { if (objName == "BPOInsuredName") 			{ goToPic(1); top.fraPic.scrollTo(0, 354); showPosition(257+hx, 506+hy, 182, 54); } } catch(e) {} 
		try { if (objName == "BPOInsuredBirthday") 	{ goToPic(1); top.fraPic.scrollTo(0, 384); showPosition(485+hx, 514+hy, 303, 51); } } catch(e) {} 
		try { if (objName == "BPOInsuredSex") 			{ goToPic(1); top.fraPic.scrollTo(0, 399); showPosition(800+hx, 513+hy, 163, 59); } } catch(e) {} 
		try { if (objName == "BPOInsuredMarriage") 	{ goToPic(1); top.fraPic.scrollTo(0, 414); showPosition(936+hx, 521+hy, 263, 53); } } catch(e) {} 
		try { if (objName == "BPOInsuredIDType") 		{ goToPic(1); top.fraPic.scrollTo(0, 429); showPosition(253+hx, 539+hy, 249, 62); } } catch(e) {} 
		try { if (objName == "BPOInsuredIDNo") 			{ goToPic(1); top.fraPic.scrollTo(0, 444); showPosition(505+hx, 543+hy, 479, 62); } } catch(e) {} 
		try { if (objName == "BPOInsuredNativePlace"){ goToPic(1); top.fraPic.scrollTo(0, 459); showPosition(990+hx, 560+hy, 220, 47); } } catch(e) {} 
		try { if (objName == "BPOInsuredGrpName") 	{ goToPic(1); top.fraPic.scrollTo(0, 420); showPosition(256+hx, 578+hy, 342, 53); } } catch(e) {} 
		try { if (objName == "BPOInsuredPosition") 	{ goToPic(1); top.fraPic.scrollTo(0, 435); showPosition(630+hx, 585+hy, 160, 50); } } catch(e) {} 
		try { if (objName == "BPOInsuredOccupationCode") { goToPic(1); top.fraPic.scrollTo(0, 450); showPosition(819+hx, 587+hy, 159, 55); } } catch(e) {} 
		try { if (objName == "BPOInsuredOccupationType") { goToPic(1); top.fraPic.scrollTo(0, 465); showPosition(974+hx, 592+hy, 99, 48); } } catch(e) {} 
		try { if (objName == "BPOInsuredSalary") 		{ goToPic(1); top.fraPic.scrollTo(0, 480); showPosition(1084+hx, 594+hy, 115, 51); } } catch(e) {} 
		try { if (objName == "BPOInsuredPostalAddress") { goToPic(1); top.fraPic.scrollTo(0, 466); showPosition(254+hx, 609+hy, 767, 59); } } catch(e) {} 
		try { if (objName == "BPOInsuredZipCode") 	{ goToPic(1); top.fraPic.scrollTo(0, 451); showPosition(1030+hx, 617+hy, 171, 56); } } catch(e) {} 
		try { if (objName == "BPOInsuredPhone") 		{ goToPic(1); top.fraPic.scrollTo(0, 421); showPosition(249+hx, 632+hy, 301, 61); } } catch(e) {} 
		try { if (objName == "BPOInsuredMobile") 		{ goToPic(1); top.fraPic.scrollTo(0, 436); showPosition(561+hx, 638+hy, 307, 58); } } catch(e) {}  
		try { if (objName == "BPOInsuredEMail") 		{ goToPic(1); top.fraPic.scrollTo(0, 406); showPosition(918+hx, 646+hy, 277, 49); } } catch(e) {} 
	}
	else if(InsuredGrid.getSelNo() == 3)
	{
		//第三被保险人
		try { if (objName == "RelationToAppnt") 		{ goToPic(1); top.fraPic.scrollTo(0, 495); showPosition(540+hx, 674+hy, 395, 61); } } catch(e) {} 
		try { if (objName == "BPOInsuredName") 			{ goToPic(1); top.fraPic.scrollTo(0, 441); showPosition(247+hx, 703+hy, 200, 60); } } catch(e) {} 
		try { if (objName == "BPOInsuredBirthday") 	{ goToPic(1); top.fraPic.scrollTo(0, 456); showPosition(470+hx, 707+hy, 293, 60); } } catch(e) {} 
		try { if (objName == "BPOInsuredSex") 			{ goToPic(1); top.fraPic.scrollTo(0, 471); showPosition(794+hx, 713+hy, 121, 60); } } catch(e) {} 
		try { if (objName == "BPOInsuredMarriage") 	{ goToPic(1); top.fraPic.scrollTo(0, 486); showPosition(935+hx, 722+hy, 266, 47); } } catch(e) {} 
		try { if (objName == "BPOInsuredIDType") 		{ goToPic(1); top.fraPic.scrollTo(0, 501); showPosition(243+hx, 739+hy, 240, 59); } } catch(e) {} 
		try { if (objName == "BPOInsuredIDNo") 			{ goToPic(1); top.fraPic.scrollTo(0, 516); showPosition(495+hx, 742+hy, 473, 68); } } catch(e) {} 
		try { if (objName == "BPOInsuredNativePlace"){ goToPic(1); top.fraPic.scrollTo(0, 531); showPosition(982+hx, 755+hy, 211, 60); } } catch(e) {} 
		try { if (objName == "BPOInsuredGrpName") 	{ goToPic(1); top.fraPic.scrollTo(0, 546); showPosition(250+hx, 768+hy, 341, 61); } } catch(e) {} 
		try { if (objName == "BPOInsuredPosition") 	{ goToPic(1); top.fraPic.scrollTo(0, 561); showPosition(630+hx, 781+hy, 144, 56); } } catch(e) {} 
		try { if (objName == "BPOInsuredOccupationCode") { goToPic(1); top.fraPic.scrollTo(0, 576); showPosition(816+hx, 781+hy, 147, 60); } } catch(e) {} 
		try { if (objName == "BPOInsuredOccupationType") { goToPic(1); top.fraPic.scrollTo(0, 621); showPosition(965+hx, 794+hy, 99, 44); } } catch(e) {} 
		try { if (objName == "BPOInsuredSalary") 		{ goToPic(1); top.fraPic.scrollTo(0, 606); showPosition(1064+hx, 790+hy, 134, 52); } } catch(e) {} 
		try { if (objName == "BPOInsuredPostalAddress") { goToPic(1); top.fraPic.scrollTo(0, 636); showPosition(256+hx, 807+hy, 768, 60); } } catch(e) {} 
		try { if (objName == "BPOInsuredZipCode") 	{ goToPic(1); top.fraPic.scrollTo(0, 651); showPosition(1034+hx, 819+hy, 166, 54); } } catch(e) {} 
		try { if (objName == "BPOInsuredPhone") 		{ goToPic(1); top.fraPic.scrollTo(0, 666); showPosition(257+hx, 832+hy, 294, 60); } } catch(e) {} 
		try { if (objName == "BPOInsuredMobile") 		{ goToPic(1); top.fraPic.scrollTo(0, 681); showPosition(560+hx, 841+hy, 315, 62); } } catch(e) {} 
		try { if (objName == "BPOInsuredEMail") 		{ goToPic(1); top.fraPic.scrollTo(0, 696); showPosition(917+hx, 849+hy, 278, 55); } } catch(e) {} 
	}
	
//	try { if (PolGrid.getSelNo() == 1) { goToPic(1); top.fraPic.scrollTo(0, 949); showPosition(159+hx, 1119+hy, 72, 43); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 2) { goToPic(1); top.fraPic.scrollTo(0, 964); showPosition(157+hx, 1148+hy, 75, 42); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 3) { goToPic(1); top.fraPic.scrollTo(0, 979); showPosition(154+hx, 1178+hy, 81, 40); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 4) { goToPic(1); top.fraPic.scrollTo(0, 994); showPosition(159+hx, 1203+hy, 78, 39); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 5) { goToPic(1); top.fraPic.scrollTo(0, 1009); showPosition(157+hx, 1227+hy, 79, 43); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 6) { goToPic(1); top.fraPic.scrollTo(0, 1024); showPosition(151+hx, 1259+hy, 76, 39); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 7) { goToPic(1); top.fraPic.scrollTo(0, 1039); showPosition(147+hx, 1285+hy, 87, 40); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 8) { goToPic(1); top.fraPic.scrollTo(0, 1054); showPosition(145+hx, 1310+hy, 91, 41); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 9) { goToPic(1); top.fraPic.scrollTo(0, 1069); showPosition(152+hx, 1332+hy, 83, 48); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 10) { goToPic(1); top.fraPic.scrollTo(0, 1084); showPosition(155+hx, 1366+hy, 85, 45); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 11) { goToPic(1); top.fraPic.scrollTo(0, 1099); showPosition(154+hx, 1393+hy, 81, 43); } } catch(e) {} 
//	try { if (PolGrid.getSelNo() == 12) { goToPic(1); top.fraPic.scrollTo(0, 1114); showPosition(150+hx, 1420+hy, 88, 47); } } catch(e) {} 
	
	try { if (objName == "PolInfo") { goToPic(1); top.fraPic.scrollTo(0, 1117); showPosition(126+hx, 1119+hy, 1094, 387); } } catch(e) {} 
	try { if (objName == "BnfInfo") { goToPic(1); top.fraPic.scrollTo(0, 1367); showPosition(130+hx, 1525+hy, 1067, 164); } } catch(e) {} 
	try { if (objName == "InsurInfo") { goToPic(2); top.fraPic.scrollTo(0, 116); showPosition(149+hx, 132+hy, 1072, 678); } } catch(e) {} 
  }	
}



