
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
function initForm(){
  try
  {
    initInpBox();  
    //initElementtype();
    //showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectMonth();
	// divPage.style.display="";
}
function initProjectMonth() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="50px";         		//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="险种编码";         	  //列名
    iArray[1][1]="100px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="险种名称";         	
    iArray[2][1]="160px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0; 
   
    iArray[3]=new Array();
    iArray[3][0]="状态";         	  //列名
    iArray[3][1]="80px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[3][10]="codetype";
    iArray[3][11]="0|^stoprisk|有签单限制|^startrisk|无签单限制";
    iArray[3][12]="3|4"
    iArray[3][13]="1|0"
    
    iArray[4]=new Array();
	iArray[4][0]="险种状态";
	iArray[4][1]="0px";
	iArray[4][2]=100;
	iArray[4][3]=3;
	iArray[4][21]="codetype";
    	
     
    ProjectMonth = new MulLineEnter("fm", "ProjectMonth"); 
    //设置Grid属性
    ProjectMonth.mulLineCount = 0;
    ProjectMonth.displayTitle = 1;
    ProjectMonth.locked = 1;
    ProjectMonth.canSel = 1;
    ProjectMonth.canChk = 0;
    ProjectMonth.hiddenSubtraction = 1;
    ProjectMonth.hiddenPlus = 1;
    ProjectMonth.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>