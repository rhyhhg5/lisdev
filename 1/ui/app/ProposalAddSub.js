//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function initQuery() {
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSql = "select PrtNo, MainPolNo, SerialNo, InputOperator, InputDate, InputTime from LCAddPol where " + ++queryBug + "=" + queryBug
    				 + " and ( InputState='0' or InputState is null )"              //状态为0，未完成录入
    				 + " and ( InputOperator is null or InputOperator='" + operator + "' )";
    				 
  //alert(strSql);
	turnPage.queryModal(strSql, PolGrid);

}

var prtNo = "";
var serialNo = ""; 
function scanApplyClick() {
  var state = "0";   //投保单录入状态，为1则表示录入完成，将不再查询出来

  if (PolGrid.getSelNo()) { 
  	prtNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  	serialNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3);
  	
    var urlStr = "./ProposalAddSubScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state + "&serialNo=" + serialNo;
    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    var strReturn = window.showModalDialog(urlStr, "", sFeatures);
    
    //打开扫描件录入界面
    //sFeatures = "";
    sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    if (strReturn == "1") window.open("./ProposalInputMain.jsp?LoadFlag=1&prtNo="+prtNo, "", sFeatures);        
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
  
  initQuery();
}

function afterInput() {
  //录入完成，询问是否完成该保单
  var completeFlag = window.confirm("该印刷号对应的保单录入是否全部完成？\n选择是将不再查询出该印刷号！");
  
  if (completeFlag) {
    var state = "1";
    var urlStr = "./ProposalAddSubScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state + "&serialNo=" + serialNo;
    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    window.showModalDialog(urlStr, "", sFeatures);
  }
  
  initQuery();
  
  return true;
}

function showExplain() {
  var strSql = "select Remark from LCAddPol where MainPolNo = '" 
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2) 
             + "' and SerialNo = '"
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3)
             + "'";
             
  //alert(easyExecSql(strSql));      
  fm.all("explain").value = easyExecSql(strSql);
    				 
}

