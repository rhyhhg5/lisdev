<%
//程序名称：ES_DOC_MAINQueryInit.jsp
//程序功能：扫描单证下载的主页面Init
//创建日期：2005-08-25
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = '' 	;
    fm.all('StartDate').value = ''	;
    fm.all('EndDate').value = ''	;    
    fm.all('ScanOperator').value = ''	;
    fm.all('SubType').value = 'TB01'	;
    fm.all('SubTypeName').value = '家庭投保书';
    fm.all('DocCode').value = ''	;
  }
  catch(ex)
  {
    alert("在CodeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
    try
    {
        initInpBox();
        initScannerGrid();

        fm.all('ManageCom').value = <%=strManageCom%>;

        if(fm.all('ManageCom').value == "86")
        {
            fm.all('ManageCom').readOnly = false;
        }
        else
        {
            fm.all('ManageCom').readOnly = true;
        }
        showAllCodeName();
    }
    catch(e)
    {
        alert("ScannerQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}


var ScannerGrid;
function initScannerGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;

        iArray[1]=new Array();
        iArray[1][0]="扫描流水号";
        iArray[1][1]="60px";
        iArray[1][2]=100;
        iArray[1][3]=0;

        iArray[2]=new Array();
        iArray[2][0]="单证号码";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;

        iArray[3]=new Array();
        iArray[3][0]="单证类型";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;

        iArray[4]=new Array();
        iArray[4][0]="页数";
        iArray[4][1]="25px";
        iArray[4][2]=100;
        iArray[4][3]=0;

        iArray[5]=new Array();
        iArray[5][0]="扫描机构";
        iArray[5][1]="55px";
        iArray[5][2]=100;
        iArray[5][3]=0;

        iArray[6]=new Array();
        iArray[6][0]="扫描日期";
        iArray[6][1]="50px";
        iArray[6][2]=100;
        iArray[6][3]=0;

        iArray[7]=new Array();
        iArray[7][0]="扫描人";
        iArray[7][1]="40px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="业务类型";
        iArray[8][1]="40px";
        iArray[8][2]=100;
        iArray[8][3]=3;


        ScannerGrid = new MulLineEnter("fm", "ScannerGrid"); 

        ScannerGrid.mulLineCount = 0;   
        ScannerGrid.displayTitle = 1;
        ScannerGrid.canSel = 1;
        ScannerGrid.hiddenSubtraction = 1;
        ScannerGrid.hiddenPlus = 1;
        ScannerGrid.canChk = 0;
        ScannerGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ScannerGrid时出错：" + ex);
    }
}
</script>