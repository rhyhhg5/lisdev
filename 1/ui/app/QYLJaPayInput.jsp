<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
	<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<title>契约保单信息运维功能查询页面</title>
		<script src="QYLJaPayInput.js"></script>
		<%@include file="QYLJaPayInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form method="post" name="fm" target="fraTitle">
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输入查询条件：
					</td>
				</tr>
			</table>

			<table class=common border=0 width=100%>
				<tr class=common>
					<td class=title>管理机构</td>
					<td class=input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true ></td>
					<td class=title>印 刷 号</td>
					<td class=input><input class=common name=PrtNo /></td>
					<td class=title>合 同 号</td>
					<td class=input><input class=common name=ContNo /></td>
				</tr>
				<tr class=common>
					<td class=title>签单日期起</td>
					<td class=input><Input class= coolDatePicker name=startSignDate /></td>
					<td class=title>签单日期止</td>
					<td class=input><Input class= coolDatePicker name=endSignDate /></td>
				</tr>
			</table>
			<INPUT VALUE="查询保单" class="cssButton" TYPE=button onclick="easyQueryClick();">
			
			<div id= "divLCPol1" style= "display: ''">
				<table class= common>
					<TR class= common>
						<td text-align: left colSpan=1>
							<span id="spanPolGrid" ></span>
						</td>
					</tr>
				</table>
	
				<table align=center>
					<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
					<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
					<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
					<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
				 </table>
			</div>
			<br />
			
			<INPUT VALUE="进行财务充负" class="cssButton" TYPE=button onclick="negativeRecoil();">
			<INPUT VALUE="进行财务充正" class="cssButton" TYPE=button onclick="positiveRecoil();">
			
			<input type="hidden" class=Common name="Action" value="" />
			<input type="hidden" class=Common name="FinancialAction" value="" />
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden name="mContNo" value="">
			<input type=hidden name="mPrtNo" value="">
			<input type=hidden name="mContType" value="">
			<INPUT  type="hidden" class=Common name=querySql >
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
