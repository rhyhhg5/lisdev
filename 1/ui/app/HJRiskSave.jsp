<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>


<!--用户校验类-->
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.lis.schema.*"%>
  
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%--
  <%@page import="com.sinosoft.lis.config.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  --%>
<% 
	
	String strOperation = request.getParameter("fmtransact");//操作類型
	System.out.println("操作类型是-----------------"+strOperation);
	
	String content = "";
	String FlagStr = "";
	String mDescType = "";
	LDCodeSchema tLDCodeSchema   = new LDCodeSchema();
	HJRiskUI hJRiskUI = new HJRiskUI();
	CErrors tError = null;
	
	if(strOperation.equals("INSERT"))
  	{
  		tLDCodeSchema.setCode(request.getParameter("riskind"));//险种编码
		tLDCodeSchema.setCodeName(request.getParameter("riskindName"));//险种名称
		tLDCodeSchema.setOtherSign("1");
		tLDCodeSchema.setCodeType("grphjrisk");
		tLDCodeSchema.setCodeAlias("");
		tLDCodeSchema.setComCode("");
    	mDescType = "新增";
  	}
  	if(strOperation.equals("UPDATE"))
  	{	
  		tLDCodeSchema.setCode(request.getParameter("subCode"));//险种编码
  		tLDCodeSchema.setOtherSign(request.getParameter("subCodeType"));//险种状态
  	   	tLDCodeSchema.setCodeType("grphjrisk");
   	 	mDescType = "修改";
  	}
  	if(strOperation.equals("DELETE"))
  	{	
  	   tLDCodeSchema.setCode(request.getParameter("subCode"));//险种编码
  	   tLDCodeSchema.setCodeType("grphjrisk");
   	   mDescType = "删除";
 	 }
 	VData tVData = new VData();
 	LDCodeSet tLDCodeSet = new LDCodeSet();
 	tLDCodeSet.add(tLDCodeSchema);
 	
	try{
		tVData.addElement(strOperation);
		tVData.addElement(tLDCodeSet);
		hJRiskUI.submitData(tVData,strOperation);
	}catch(Exception ex){
		content = mDescType+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	
  	if (FlagStr=="")
  	{
    	tError = hJRiskUI.mErrors;
    if (!tError.needDealError())
    {
      	content = mDescType+"成功";
    	FlagStr = "Succ";
    }
    else
    {
    	content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=content%>");
</script>
</html>