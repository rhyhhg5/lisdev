<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var comcode = "<%=tGI.ComCode%>";//记录登陆机构
var cardFlag = "<%=request.getParameter("cardFlag")%>";//记录登陆机构
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="OmnipotenceProposalPrint.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="OmnipotenceProposalPrintInit.jsp"%>
    <title>打印个人保单 </title>
    <script>
        var	agentComCode = "agentcombank";
        var	agentComValue = "ManageCom";
        var agentComName = fm.ManageCom.value;
    </script>
</head>
<body onload="initForm();" >
	<form action="./OmnipotenceProposalPrintSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 个人保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr><td class= titleImg align= center>请输入保单查询条件：</td></tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>销售渠道</TD>
				<TD class= input><Input class="codeNo"  name=SaleChnl  ondblclick="return showCodeList(tSaleChnlCode,[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey(tSaleChnlCode,[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true > </TD>			
				<TD class= title>保险合同号</TD>
				<TD class= input> <Input class= common name=ContNo verify="保险合同号|int"> </TD>
				<TD class= title>印刷号码</TD>
				<TD class= input> <Input class= common name=PrtNo verify="印刷号码"> </TD>
			</TR>
			<TR class= common>
				<!--TD class= title>险种编码</TD>
				<TD class= input> <Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);"> </TD-->
				<TD class= title>业务员代码</TD>
				<TD class= input><Input class="codeNo"  name=AgentCode  verify="业务员代码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this,AgentCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeName],[0,1],null,null,null,1);"><input class=codename name=AgentCodeName readonly=true > </TD>						
				<TD class= title>营销机构</TD>
				<TD class="input" nowrap="true">
					<Input class="common" name="BranchGroup" readonly >
					<input name="btnQueryBranch" class="common" type="button"  value="?" onclick="queryBranch()" style="width:20">
				</TD>
  			<TD class= title>管理机构</TD>
				<TD class= input><Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true > </TD>
				</TD>
			</TR>
            <TR  class= common>
                <td class= title>签单起始日期</TD>
                <td class= input>
                    <Input class="coolDatePicker" dateFormat="short" name=startSignDate verify="签单起始日期|notnull&Date" >
                </td>
                <td class= title>签单截至日期</TD>
                <td class= input>
                    <Input class="coolDatePicker" dateFormat="short" name=endSignDate verify="签单截至日期|notnull&Date" >
                </td>
                <td class="title">网点</td>
                <td class="input">
                    <input id="AgentComBank" class="codeno" name="AgentCom" style="display:none;" ondblclick="showCodeList(agentComCode,[this,AgentComName],[0,1],null,agentComValue,agentComName,1);" /><input class=codename name=AgentComName readonly=true >
                </td>
            </TR>
			<TR class= common id="PrintStateTRID" style="display: ''">
				<TD class= title>状态</TD>
				<TD class= input colspan="5">
				  <Input class=codeNo name=PrintState value="0" readonly CodeData="0|^0|未打印^1|已打印^2|全部" ondblclick="return showCodeListEx('printstate',[this,PrintStateName],[0,1]);" onkeyup="return showCodeListKey('printstate',[this,PrintStateName],[0,1]);"><input class=codename name=PrintStateName value="未打印" readonly=true></TD>
			</TR>
			
			<!--TR class= common>
			</TR-->
		</table>
		<input type="hidden" class="common" name="querySql" >
		<INPUT VALUE="查询保单" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<INPUT VALUE="打印保单" class="cssButton" TYPE=button onclick="printPol();" name='printButton' >
		<input value="下载清单" class="cssButton" type=button onclick="download();">
		<!--INPUT VALUE="扫描件下载" class="cssButton" TYPE=button onclick="downloadCustomerCard();" name='downloadCustomerCardButton'>
		<INPUT VALUE="打印保单2" class="cssButton" TYPE=button onclick="printPol1();" name='printButton1' -->
		<INPUT VALUE="客户卡信息下载(excel)" class="cssButton" TYPE=button onclick="downloadCustomerCard('xls');" name='downloadCustomerCardButtonExcel' style="display: none">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<Div id= "divLCPol1" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanContGrid" ></span>
					</td>
				</tr>
			</table>
			<INPUT VALUE="首 页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾 页" class="cssButton" TYPE=button onclick="getLastPage();">
		</div>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
