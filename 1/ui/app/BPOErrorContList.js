//程序功能：
//创建日期：2007-10-19 18:00
//创建人  ：YangYalin
//更新记录：更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

var tDefaultStartDate;

/*********************************************************************
 *  外包反馈错误信息的EasyQuery
 *  描述:外包反馈错误信息.显示条件:有外包反馈错误信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryContInfo()
{
  if(!verifyInput2())
  {
    return false;
  }
  
    // 对业务类型进行分类查询控制。
    var tSubTypeCond = "";
    var commsql="";
    if(tSubType != null && tSubType != "" && tSubType != "null")
    {
        var tActivityId = "";
        if(tSubType == "TB05")
        {
            tActivityId = "0000007999";
        }
        else if(tSubType == "TB01"||tSubType == "TB29" )
        {
            tActivityId = "0000001099";
            commsql = ", a.BPOID,(select codename from ldcode where codetype='bjwbbl' and code=a.bpoid ) "
        }
        tSubTypeCond += " and a.ActivityId = '" + tActivityId + "' ";
    }
    // ----------------------
	var sql = "select a.BPOBatchNo, a.BussNo, a.MissionID, a.ManageCom "
	        + commsql
	        + "from BPOMissionState a "
	        + "where State = '02' "
	        + "   and MakeDate >= '" + fm.StartDate.value + "' "
	        + "   and MakeDate <= '" + fm.EndDate.value + "' "
	        + "   and ManageCom like '" + tManageCom + "%' "
	        + "   and ManageCom like '" + fm.ManageCom.value + "%' "
	        + "   and not exists(select 1 from LCCont where PrtNo = a.BussNo and ContType = '1')"
	        + "   and not exists(select 1 from LBCont where PrtNo = a.BussNo and ContType = '1')"
	        + getWherePart("a.BussNo", "PrtNo")
	        + getWherePart("a.BPOBatchNo", "BPOBatchNo")
	        + getWherePart("a.BPOID", "BPOid")
            + tSubTypeCond 
	        + "order by MakeDate, MakeTime ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, ContGrid);
	
  setErrorInfo();

  return true;
}

//显示导入错误信息
function setErrorInfo()
{
  for(var i = 0; i < ContGrid.mulLineCount; i++)
  {
    //var sql = "select a.ErrorInfo "
    //        + "from LCGrpImportLog a, BPOMissionState b, BPOLCPol c "
    //        + "where a.BatchNo = b.BPOBatchNo "
    //        + "   and b.BPOBatchNO = c.BPOBatchNO "
    //        + "	  and b.BussNO = c.PrtNo "
    //        + "   and a.ContID = c.ContID "
    //        + "   and not exists(select 1 from LCCont where PrtNo = b.BussNo and ContType = '1')"
    //        + "   and c.PrtNo = '" + ContGrid.getRowColDataByName(i, "PrtNo") + "' "
    //        + "order by a.MakeDate desc, a.MakeTime desc ";
    //        
    var sql = "select a.ErrorInfo "
            + "from LCGrpImportLog a "
            + "where a.PrtNo = '" + ContGrid.getRowColDataByName(i, "PrtNo") + "' "
            + "order by a.MakeDate desc, a.MakeTime desc ";
    
    var rs = easyExecSql(sql);
    if(rs)
    {
      errorInfo = rs[0][0];  //只取最后一次导入日志
      ContGrid.setRowColDataByName(i, "ErrorInfo", errorInfo);
    }
  }
}

//初始化查询条件
function initInputBox()
{
  fm.ManageCom.value = tManageCom;
  fm.EndDate.value = tCurrentDate;
  
  var sql = "select date('" + tCurrentDate + "') - 3 month from dual ";
  tDefaultStartDate = easyExecSql(sql);
  fm.StartDate.value = tDefaultStartDate;
}

//进入修改页面
function modify()
{
  if(ContGrid.mulLineCount == 0)
  {
    alert("请先查询需要修改的保单");
    return false;
  }
  
  var row = ContGrid.getSelNo() - 1;
  if(row < 0)
  {
    alert("请先选择需要修改的保单");
    return false;
  }
  
  window.open("BPOContMain.jsp?BPOBatchNo=" + ContGrid.getRowColDataByName(row, "BPOBatchNo")
    + "&MissionID=" + ContGrid.getRowColDataByName(row, "MissionID")
    + "&PrtNo=" + ContGrid.getRowColDataByName(row, "PrtNo"));
}