
//程序名称：ES_DOC_MAINQuery.js
//程序功能：扫描单证下载的主页面JS
//创建日期：2007-09-25
//创建人  ：DongJianbin
//更新记录:  更新人   更新日期    更新原因/内容 


//               该文件中包含客户端需要处理的函数和事件

var arrDataSet 
var tArr;
var turnPage = new turnPageClass();

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    window.focus();
    showInfo.close();
//  fm.all("DownloadButton").disabled=false;
//	fm.all("DownloadAllButton").disabled=false;
 	if (FlagStr == "Fail" )
	{
		//如果失败，则返回错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		//如果提交成功，则执行查询操作
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	easyqueryClick();   
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在EsPicDownload.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

function easyqueryClick()
{
	initCodeGrid();
	var bpoid=fm.BPOid.value;
	var subtype=fm.SubType.value;
	var strSQL="";
	if(bpoid==""){
		strSQL = "select a.docid,a.doccode,a.subtype,a.numpages,a.managecom,a.makedate,a.maketime,a.scanoperator, CodeName('esmainstate', a.state) state,"
		            + " (select codename from ldcode where codetype='bjwbbl' and code=(select b.bpoid from BPOMissionState b where b.bussno=a.DocCode order by makedate,maketime desc fetch first 1 rows only ) )"
					+ " from ES_DOC_MAIN a  where 1=1 "
					+ getMngCom()
					+ getStartDate()
					+ getEndDate()
					+ getWherePart( "a.ScanOperator","ScanOperator" )
					+ getWherePart( "a.DocCode","DocCode" )
					+ getWherePart("a.SubType","SubType")
					+ getWherePart("a.State","State");
	
	}else{
	    strSQL = "select distinct a.docid,a.doccode,a.subtype,a.numpages,a.managecom,a.makedate,a.maketime,a.scanoperator, CodeName('esmainstate', a.state) state,"
					+ " (select codename from ldcode where codetype='bjwbbl' and code=b.bpoid ) " 
					+ " from ES_DOC_MAIN a , BPOMissionState b where 1=1 "
					+ " and a.doccode=b.bussno "
					+ getMngCom()
					+ getStartDate()
					+ getEndDate()
					+ getWherePart( "b.BPOid","BPOid" )
					+ getWherePart( "a.ScanOperator","ScanOperator" )
					+ getWherePart( "a.DocCode","DocCode" )
					+ getWherePart("a.SubType","SubType")
					+ getWherePart("a.State","State");
	
	}

		
	//alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  if (!turnPage.strQueryResult) {
    alert("未查询到满足条件的数据！");
     return false;
  }	
  turnPage.queryModal(strSQL,CodeGrid);

}  
function DownloadPic()
{
//	 var checkFlag="";
//  for (i=0; i<CodeGrid.mulLineCount; i++)
//  {
//    if (CodeGrid.getSelNo(i))
//    {
//      checkFlag = CodeGrid.getSelNo();
//      break;
//    }
//  }
	//如果没有打印数据，提示用户选择
//	if( checkFlag == 0 )
//	{
//		alert("请先选择一条记录，再下载单证");
//		return false;
//	}
//	else {
    fm.action="./ES_DOC_MAINQueryDownLoad.jsp";
    fm.EXESql.value = turnPage.strQuerySql;
    fm.submit();
//  } 
}
function getStartDate()
{
	var ssd=fm.StartDate.value;
	var sDate="";
	if(ssd=="")
	{
		sDate= "";
	}
  else
	{
	sDate= "and a.makedate>='"+ssd+"' ";
	}
	return sDate;
}
function getEndDate()
{
	var sed=fm.EndDate.value;
	var eDate="";
	if(sed=="")
	{
		eDate= "";
	}
else
	{
		eDate= "and a.makedate<='"+sed+"' ";
	}
	return eDate;
}
function getMngCom()
{
	var tManageCom=fm.ManageCom.value;
	var cManageCom="";
  if (tManageCom=="")
  {
    cManageCom="";
  }
  else
  {
    cManageCom=" and a.ManageCom like '"+tManageCom+"%' ";  
  }
  return cManageCom;
}
