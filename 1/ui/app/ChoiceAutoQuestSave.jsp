<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ChoiceAutoQuestSave.jsp
//程序功能：
//创建日期：2005-05-24 10:05:39
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LCGrpIssuePolSet tLCGrpIssuePolSet   = new LCGrpIssuePolSet();
  ChoiceAutoQuestUI tChoiceAutoQuestUI   = new ChoiceAutoQuestUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  	String tOnChk[] = request.getParameterValues("GrpQuestGridChk");
  	String ProposalGrpContNo[] = request.getParameterValues("GrpQuestGrid8");
  	String SerialNo[] = request.getParameterValues("GrpQuestGrid9");
  	System.out.println(ProposalGrpContNo);
  	System.out.println(SerialNo);
  	
  	for(int i=0;i<tOnChk.length;i++)
  	{
  		if ( "on".equals(tOnChk[i]))
			{
				LCGrpIssuePolSchema tLCGrpIssuePolSchema = new LCGrpIssuePolSchema();
				tLCGrpIssuePolSchema.setProposalGrpContNo(ProposalGrpContNo[i]);
				tLCGrpIssuePolSchema.setSerialNo(SerialNo[i]);	
				
				tLCGrpIssuePolSet.add(tLCGrpIssuePolSchema);
			}
				
  	}
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	System.out.println(tLCGrpIssuePolSet.encode());
	  tVData.add(tLCGrpIssuePolSet);
  	tVData.add(tG);
  	System.out.println(transact);
    tChoiceAutoQuestUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tChoiceAutoQuestUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
