//程序名称：InterfacetableMaintainInput.js
//程序功能：接口表信息维护
//创建日期：2016-09-14 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var mode_flag;
var tAppealFlag="0";
var turnPage = new turnPageClass();        
var turnPage1 = new turnPageClass();


//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}



//查询
function Query()
{ 
	 if(!beforeSubmit())
  {
    return false;
  }
	 
	// alert("222222233");
	initInterfacetableMaintainGrid();
    var sql="";

    
	sql = "select batchno,ImportType,VoucherType ,DCFlag ,AccountCode ,SumMoney,CostCenter,RiskCode,Chinal,ChargeDate,ReadState,VoucherID,serialno from interfacetable" +
			" where batchno = '"+fm.all('BatchNo').value+"' and serialno = '"+fm.all('SerialNo').value
			+"' and chargedate = '"+fm.all('ChargeDate').value+"'"
			+getWherePart('CostCenter','CostCenter')
			+getWherePart('Chinal','Chinal')
			+getWherePart('ReadState','StateF');
	
     turnPage.queryModal(sql,InterfacetableMaintainGrid); 
     afterSubmitl();
    return false;
  }

function beforeSubmit()
{
	if(fm.BatchNo.value == null||fm.BatchNo.value==''){
		alert("请输入您想要维护数据的批次号，谢谢！");
		return false;
	}
	
	
	return true;
}
/************************
*PDF打印功能实现 
***************************/
function submitForm()
{
 
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交

      
	}

/*********************************************************************
 *  后台执行完毕反馈信息
 *  描述: 后台执行完毕反馈信息
 *********************************************************************/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
}

function afterSubmitl( FlagStr, content )
{
  showInfo.close();
  if (!turnPage.strQueryResult )
  {        
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "没有查询到数据" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
  }
  else
  { 
  	if(turnPage.strQueryResult){
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "查询成功" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  } 
  }
}



function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//响应单选按钮,一点击某条数据，其相应的字段值就显示在相应的字段下面
function BankSelect() {
//       alert('oooo');
		var tSel = InterfacetableMaintainGrid.getSelNo();
		try
		{
			fm.all('BatchNo').value = InterfacetableMaintainGrid.getRowColData(tSel-1, 1);
			fm.all('SerialNo').value = InterfacetableMaintainGrid.getRowColData(tSel-1, 13);
//			alert (fm.all('Codea').value);
			fm.all('CostCenter').value = InterfacetableMaintainGrid.getRowColData(tSel-1, 7);
			fm.all('Chinal').value = InterfacetableMaintainGrid.getRowColData(tSel-1,9);// 业务名称
			fm.all('ChargeDate').value = InterfacetableMaintainGrid.getRowColData(tSel-1, 10);
			//fm.all('ReadState').value= InterfacetableMaintainGrid.getRowColData(tSel-1,11);// 编码		   
		}
		catch(ex)
		{
			alert( "读取数据错误,请刷新页面再使用。" + ex );
		}
		}


//提交，保存按钮对应操作
function Add(){
//		alert("add   add-------->");
	  if ((fm.all("BatchNo").value == null)||(fm.all("BatchNo").value == '')){
	  	    alert('请填写批次号！');
	  	     return false;
	  	}
	  	
	   if ((fm.all("SerialNo").value == null)||(fm.all("SerialNo").value == ''))
	    {
	      alert('请填写流水号！');
	      return false;
	    }

	    if ((fm.all("CostCenter").value == null)||(fm.all("CostCenter").value == '')){
	    	alert('请填写成本中心！');
	    	return false;
	    }
	    
	    if ((fm.all("Chinal").value == null)||(fm.all("Chinal").value == '')){
	  	  	alert('请填写渠道！');
	  	  	return false;
	  	  }
	    if ((fm.all("ChargeDate").value == null)||(fm.all("ChargeDate").value == '')){
	  	  	alert('请填写记账日期！');
	  	  	return false;
	  	  }
	 
	   // alert("add  qqqq------->");
	      fm.mOperate.value = "INSERT";
	      
	              var i = 0;
	        	    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	        	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	        	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	        	    fm.submit(); //提交
	    }


//修改
  function Update(){
	 if(!beforeSubmit()){
	    return false;
	  }
	  
 	if (fm.Chinal.value == null ||fm.Chinal.value == "")
	{
		alert('请将要修改的渠道数据填写完整');
		return false;
	}
	
	if (fm.CostCenter.value == null ||fm.CostCenter.value == "")
	{
		alert('请将要修改的成本中心数据填写完整');
		return false;
	}
	
	fm.mOperate.value = "UPDATE";
	
		if (confirm("您确实想修改该记录吗？"))
	    {
	    	
	            var i = 0;
	      	    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	      	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	      	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	      	    fm.submit(); //提交      
	      
	      }else
	    {
	        mOperate="";
	      alert("您取消了修改操作！");
	    } 
	  }
  
  
 
  //删除 
  function Delete(){
	  
    fm.mOperate.value = "DELETE";
	
    if (confirm("确认要删除该数据吗？"))
    {
      var i = 0;
      var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      submitForm();
    }
    else
    {
       alert("请先做保存操作，再进行删除!");
    } 
	
  }
