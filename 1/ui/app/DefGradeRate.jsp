<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<script>
    var ManageCom = "<%=request.getParameter("ManageCom")%>";
    var Grpcontno="<%=request.getParameter("GrpContNo")%>";
    var tsql="1 and grpcontno=#"+Grpcontno+"# ";
    var tLookFlag = "<%=request.getParameter("LookFlag")%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="DefGradeRate.js"></SCRIPT>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
<SCRIPT src="ProposalAutoMove.js"></SCRIPT>
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="DefGradeRateInit.jsp"%>
<title>保障计划伤残给付比例录入</title>
</head>
<body onload="initForm();">
	<form method=post name=fm target="fraSubmit" action="DefGradeRateSave.jsp">
	 
	<table id="table1">
  		<tr>
  			<td>
  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroupPol1);">
  			</td>
  			<td class="titleImg">保障计划责任信息
  			</td>
  		</tr>
  </table>
	<table class= common>
	    <tr class= common>
		<TD  class= title8>
            保障计划
          </TD>
          <TD  class= input8>
            <Input class=codeNo name=DefContPlan  ondblclick="return showCodeList('DefContPlan',[this,DefContPlanName],[0,1],null,tsql,'1',1);"  onkeyup="return showCodeListKey('DefContPlan',[this,DefContPlanName],[0,1],null,tsql,'1',1);" ><input class=codename name=DefContPlanName elementtype=nacessary  >
          </TD>
          <TD  class= title8>
            责任编码
          </TD> 
          <TD  class= input8>
          	<Input class=codeNo name=DefContDuty  ondblclick="return showCodeList('DefContDuty',[this,DefContDutyName],[0,1],null,tdutysql,'1',1);" onkeyup="return showCodeListKey('DefContDuty',[this,DefContDutyName],[0,1],null,tdutysql,'1',1);" ><input class=codename name=DefContDutyName elementtype=nacessary  >
          </TD>
          <td></td>
          <td></td>
          <td></td>
          </tr>
		</table>
		<input name=mManageCom type="hidden"> 
	    <input name=mGrpContNo type="hidden"> 
	    <input name=mOperate type="hidden">
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>伤残给付比例信息</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanDisableRateGrid" ></span>
				</td>
			</tr>
		</table>   
		
		<br>
		<INPUT VALUE="保  存" class=cssButton name=SaveIt TYPE=button onclick="saveDefGradeRate();"> 
		<INPUT VALUE="删  除" class=cssButton name=DeleteIt TYPE=button onclick="deleteDefGradeRate();">         
        <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
       <br><br>
			<table>
				<tr>
					<td>
						<font color = red  size = 2>
						注：<br>
						1.如使用条款给付比例，则直接保存各责任给付比例即可。<br>
						2.如不使用条款给付比例，可更改给付比例后保存即可。给付比例可录入0-1之间的小数，如给付比例为99%，则录入为0.99。<br>
						</font>
					</td>
				</tr>
			</table>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>