<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：InsuredChk.jsp
//程序功能：被保人查重
//创建日期：2002-11-23 17:06:57
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("operator:"+tGI.Operator);
%>
<script>
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var param="";	
</script>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	  <SCRIPT src="ComplexRiskInput.js"></SCRIPT>
	  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  <SCRIPT src="ProposalAutoMove.js"></SCRIPT> 	
	  <%@include file="ComplexRiskInputInit.jsp"%>
	<title>被保人校验</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraTitle">
  	<div id="divRiskInfo">
    <table  class= common>
			<tr  class= common>
				<td text-align: center colSpan=1>
					<span id="spanComplexRiskGrid" >
					</span>
				</td>
			</tr>
		</table>
		</div>
	      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();changeChar();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();changeChar();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();changeChar();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();changeChar();">
      </Div>			
  	</div>
		<p align="left">
			<td class=common>
				总保费
			</td>
			<td class=common>
				<input  class=common name=SumPrem readonly>
			</td>
            <td class=common>总追加保费</td>
            <td class="common">
                <input class="common" name="SumSupplementaryPrem" readonly="readonly" />
            </td>
		</p>
	
		<table>
	    	<tr>
	    		<td class= common>
	    			 受益人信息
	    		</td>
	    	</tr>
	    </table>
		<Div  id= "divLCBnf1" style= "display: ''" >
	    	<table  class= common>
	        	<tr  class= common>
	    	  		<td text-align: left colSpan=1>
						<span id="spanBnfGrid" >
						</span> 
					</td>
				</tr>
			</table>
		</div>
	
	<input type =button class=cssButton value="返 回" onclick="top.close()">
	<INPUT class=cssButton VALUE="查看外包错误"  TYPE=button onclick="return findIssue();">
	</TD>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
