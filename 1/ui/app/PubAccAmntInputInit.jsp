<%
//程序名称：LDClassInfoInput.jsp
//程序功能：
//创建日期：2005-05-30 18:29:02
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {            
  	fm.AccTypeCode.value="001";
  	fm.GrpContNo.value=GrpContNo;
  	fm.GrpPolNo.value=GrpPolNo;
  	fm.RiskCode.value=RiskCode;
  	operateButton.style.display='none';      
  	                
  }
  catch(ex)
  {
    alert("在PubAccInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在PubAccInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();   
    initButton();
    QueryAccInfo();
    initPublicAccGrid(); 
    //initAccInfoGrid();
    //initManageFeeGrid();
    showPubAccInfo();
    //initRiskInterestGrid();
    //initIndManageFeeGrid();
    initDisplay();
  }
  catch(re)
  {
    alert("PubAccInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPublicAccGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="要素";         		//列名
		iArray[1][1]="220px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
		iArray[2][0]="要素名称";         		//列名
		iArray[2][1]="220px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	//iArray[1][10] = "PubAccName";
  //iArray[1][11] = "0|^C|团体理赔帐户^D|团体固定账户";
  //iArray[1][19] = 1;
		
		iArray[3]=new Array();
		iArray[3][0]="要素说明";         		//列名
		iArray[3][1]="120px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="要素值";         		//列名
		iArray[4][1]="120px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="计划要素类型";         		//列名
		iArray[5][1]="120px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="账户金额";         		//列名
		iArray[6][1]="120px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许	
		
		
		PublicAccGrid = new MulLineEnter( "fm" , "PublicAccGrid" ); 
		//这些属性必须在loadMulLine前
		PublicAccGrid.mulLineCount = 3;   
		PublicAccGrid.displayTitle = 1;
		PublicAccGrid.locked = 0;
		PublicAccGrid.canSel = 0;
		PublicAccGrid.canChk = 0;
		PublicAccGrid.hiddenPlus = 1;
		PublicAccGrid.hiddenSubtraction = 1;
		PublicAccGrid.loadMulLine(iArray);  
		//PublicAccGrid.selBoxEventFuncName = "ChkState";
		
		//这些操作必须在loadMulLine后面
		//GrpPolGrid.setRowColData(1,1,"asdf");
	}
	catch(ex)
	{
	  alert(ex);
	}
}
function initAccInfoGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="要素";         		//列名
		iArray[1][1]="220px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
		iArray[2][0]="要素名称";         		//列名
		iArray[2][1]="220px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		AccInfoGrid = new MulLineEnter( "fm" , "AccInfoGrid" ); 
		//这些属性必须在loadMulLine前
		AccInfoGrid.mulLineCount = 0;   
		AccInfoGrid.displayTitle = 1;
		AccInfoGrid.locked = 0;
		AccInfoGrid.canSel = 1;
		AccInfoGrid.canChk = 0;
		AccInfoGrid.hiddenPlus = 1;
		AccInfoGrid.hiddenSubtraction = 1;
		AccInfoGrid.loadMulLine(iArray);  
		//PublicAccGrid.selBoxEventFuncName = "ChkState";
		
		//这些操作必须在loadMulLine后面
		//GrpPolGrid.setRowColData(1,1,"asdf");
	}
	catch(ex)
	{
	  alert(ex);
	}
}

function initManageFeeGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="集体保单险种号码";         		//列名
		iArray[1][1]="50px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
		iArray[2][0]="集体合同号码";         		//列名
		iArray[2][1]="50px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="险种编码";         		//列名
		iArray[3][1]="50px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
		iArray[4][0]="管理费编码";         		//列名
		iArray[4][1]="50px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="保险帐户号码";         		//列名
		iArray[5][1]="50px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
		iArray[6][0]="交费项编码";         		//列名
		iArray[6][1]="50px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="关系说明";         		//列名
		iArray[7][1]="50px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
		iArray[8][0]="管理费计算方式";         		//列名
		iArray[8][1]="50px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[9]=new Array();
		iArray[9][0]="计算类型";         		//列名
		iArray[9][1]="50px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
		iArray[10][0]="管理费计算公式";         		//列名
		iArray[10][1]="50px";            		//列宽
		iArray[10][2]=100;            			//列最大值
		iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[11]=new Array();
		iArray[11][0]="管理费名称";         		//列名
		iArray[11][1]="150px";            		//列宽
		iArray[11][2]=100;            			//列最大值
		iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[12]=new Array();
		iArray[12][0]="管理费比例";         		//列名
		iArray[12][1]="150px";            		//列宽
		iArray[12][2]=100;            			//列最大值
		iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[13]=new Array();
		iArray[13][0]="比较值";         		//列名
		iArray[13][1]="50px";            		//列宽
		iArray[13][2]=100;            			//列最大值
		iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[14]=new Array();
		iArray[14][0]="扣除管理费周期";         		//列名
		iArray[14][1]="50px";            		//列宽
		iArray[14][2]=100;            			//列最大值
		iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[15]=new Array();
		iArray[15][0]="扣除管理费最大次数";         		//列名
		iArray[15][1]="50px";            		//列宽
		iArray[15][2]=100;            			//列最大值
		iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[16]=new Array();
		iArray[16][0]="缺省标记";         		//列名
		iArray[16][1]="150px";            		//列宽
		iArray[16][2]=100;            			//列最大值
		iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[17]=new Array();
		iArray[17][0]="操作员";         		//列名
		iArray[17][1]="50px";            		//列宽
		iArray[17][2]=100;            			//列最大值
		iArray[17][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[18]=new Array();
		iArray[18][0]="入机日期";         		//列名
		iArray[18][1]="50px";            		//列宽
		iArray[18][2]=100;            			//列最大值
		iArray[18][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[19]=new Array();
		iArray[19][0]="入机时间";         		//列名
		iArray[19][1]="50px";            		//列宽
		iArray[19][2]=100;            			//列最大值
		iArray[19][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[20]=new Array();
		iArray[20][0]="最后一次修改日期";         		//列名
		iArray[20][1]="50px";            		//列宽
		iArray[20][2]=100;            			//列最大值
		iArray[20][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[21]=new Array();
		iArray[21][0]="最后一次修改时间";         		//列名
		iArray[21][1]="50px";            		//列宽
		iArray[21][2]=100;            			//列最大值
		iArray[21][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		
		ManageFeeGrid = new MulLineEnter( "fm" , "ManageFeeGrid" ); 
		//这些属性必须在loadMulLine前
		ManageFeeGrid.mulLineCount = 0;   
		ManageFeeGrid.displayTitle = 1;
		ManageFeeGrid.locked = 0;
		ManageFeeGrid.canSel = 0;
		ManageFeeGrid.canChk = 0;
		ManageFeeGrid.hiddenPlus = 1;
		ManageFeeGrid.hiddenSubtraction = 1;
		ManageFeeGrid.loadMulLine(iArray);  
		//PublicAccGrid.selBoxEventFuncName = "ChkState";
		
		//这些操作必须在loadMulLine后面
		//GrpPolGrid.setRowColData(1,1,"asdf");
	}
	catch(ex)
	{
	  alert(ex.message);
	}
}
function initRiskInterestGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="集体保单险种号码";         		//列名
		iArray[1][1]="50px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
		iArray[2][0]="集体合同号码";         		//列名
		iArray[2][1]="50px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="险种编码";         		//列名
		iArray[3][1]="50px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
		iArray[4][0]="利率编码";         		//列名
		iArray[4][1]="50px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="保险帐户号码";         		//列名
		iArray[5][1]="50px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="帐户名称";         		//列名
		iArray[6][1]="150px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
		iArray[7][0]="利率累计方式";         		//列名
		iArray[7][1]="50px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="固定利率类型";         		//列名
		iArray[8][1]="50px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
		iArray[9][0]="默认利率";         		//列名
		iArray[9][1]="50px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		RiskInterestGrid = new MulLineEnter( "fm" , "RiskInterestGrid" ); 
		//这些属性必须在loadMulLine前
		RiskInterestGrid.mulLineCount = 0;   
		RiskInterestGrid.displayTitle = 1;
		RiskInterestGrid.locked = 0;
		RiskInterestGrid.canSel = 0;
		RiskInterestGrid.canChk = 0;
		RiskInterestGrid.hiddenPlus = 1;
		RiskInterestGrid.hiddenSubtraction = 1;
		RiskInterestGrid.loadMulLine(iArray);  
		//PublicAccGrid.selBoxEventFuncName = "ChkState";
		
		//这些操作必须在loadMulLine后面
		//GrpPolGrid.setRowColData(1,1,"asdf");
	}
	catch(ex)
	{
	  alert(ex);
	}
}
function initIndManageFeeGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="集体保单险种号码";         		//列名
		iArray[1][1]="50px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
		iArray[2][0]="集体合同号码";         		//列名
		iArray[2][1]="50px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="险种编码";         		//列名
		iArray[3][1]="50px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
		iArray[4][0]="管理费编码";         		//列名
		iArray[4][1]="50px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="保险帐户号码";         		//列名
		iArray[5][1]="50px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
		iArray[6][0]="交费项编码";         		//列名
		iArray[6][1]="50px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="关系说明";         		//列名
		iArray[7][1]="50px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
		iArray[8][0]="管理费计算方式";         		//列名
		iArray[8][1]="50px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[9]=new Array();
		iArray[9][0]="计算类型";         		//列名
		iArray[9][1]="50px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
		iArray[10][0]="管理费计算公式";         		//列名
		iArray[10][1]="50px";            		//列宽
		iArray[10][2]=100;            			//列最大值
		iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[11]=new Array();
		iArray[11][0]="管理费名称";         		//列名
		iArray[11][1]="150px";            		//列宽
		iArray[11][2]=100;            			//列最大值
		iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[12]=new Array();
		iArray[12][0]="管理费比例";         		//列名
		iArray[12][1]="150px";            		//列宽
		iArray[12][2]=100;            			//列最大值
		iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[13]=new Array();
		iArray[13][0]="比较值";         		//列名
		iArray[13][1]="50px";            		//列宽
		iArray[13][2]=100;            			//列最大值
		iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[14]=new Array();
		iArray[14][0]="扣除管理费周期";         		//列名
		iArray[14][1]="50px";            		//列宽
		iArray[14][2]=100;            			//列最大值
		iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[15]=new Array();
		iArray[15][0]="扣除管理费最大次数";         		//列名
		iArray[15][1]="50px";            		//列宽
		iArray[15][2]=100;            			//列最大值
		iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[16]=new Array();
		iArray[16][0]="缺省标记";         		//列名
		iArray[16][1]="150px";            		//列宽
		iArray[16][2]=100;            			//列最大值
		iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[17]=new Array();
		iArray[17][0]="操作员";         		//列名
		iArray[17][1]="50px";            		//列宽
		iArray[17][2]=100;            			//列最大值
		iArray[17][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[18]=new Array();
		iArray[18][0]="入机日期";         		//列名
		iArray[18][1]="50px";            		//列宽
		iArray[18][2]=100;            			//列最大值
		iArray[18][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[19]=new Array();
		iArray[19][0]="入机时间";         		//列名
		iArray[19][1]="50px";            		//列宽
		iArray[19][2]=100;            			//列最大值
		iArray[19][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[20]=new Array();
		iArray[20][0]="最后一次修改日期";         		//列名
		iArray[20][1]="50px";            		//列宽
		iArray[20][2]=100;            			//列最大值
		iArray[20][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[21]=new Array();
		iArray[21][0]="最后一次修改时间";         		//列名
		iArray[21][1]="50px";            		//列宽
		iArray[21][2]=100;            			//列最大值
		iArray[21][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		
		IndManageFeeGrid = new MulLineEnter( "fm" , "IndManageFeeGrid" ); 
		//这些属性必须在loadMulLine前
		IndManageFeeGrid.mulLineCount = 0;   
		IndManageFeeGrid.displayTitle = 1;
		IndManageFeeGrid.locked = 0;
		IndManageFeeGrid.canSel = 0;
		IndManageFeeGrid.canChk = 0;
		IndManageFeeGrid.hiddenPlus = 1;
		IndManageFeeGrid.hiddenSubtraction = 1;
		IndManageFeeGrid.loadMulLine(iArray);  
		//PublicAccGrid.selBoxEventFuncName = "ChkState";
		//这些操作必须在loadMulLine后面
		//GrpPolGrid.setRowColData(1,1,"asdf");
	}
	catch(ex)
	{
	  alert(ex);
	}
}
</script>
