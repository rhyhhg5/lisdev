//程序名称：QuestQuery.js
//程序功能：问题件查询
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件
var k = 0;
var turnPage = new turnPageClass();

function query()
{
	// 初始化表格
	initQuestGrid();
	initContent();
	
	// 书写SQL语句
	k++;
	var strSQL = "";
	var ifreply = fm.IfReply.value;
	var tOperatePos = fm.OperatePos.value;
  var prtNo = fm.PrtNo.value;
  
  if (prtNo != "") {
    var strSql = "select proposalno from lcpol where prtno='" + prtNo + "'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
      fm.ProposalNo.value = arrResult[0][0];
    }
    else {
      return;
    }
  }
  	
  if (tOperatePos == "1")
  {
	strSQL = "select proposalno,issuetype,issuecont,replyresult,operator,makedate,OperatePos,BackObjType,needprint,SerialNo from lcissuepol where "+k+"="+k+" "				 	
			 + " and OperatePos in ('0','1','5')"
			 + " and IsueManageCom like '" + manageCom + "%%'"
			 + getWherePart( 'ProposalNo','ProposalNo' )
			 + getWherePart( 'BackObjType','BackObj' )
			 + getWherePart( 'Operator','Operator' )
			 + getWherePart( 'OperatePos','OperatePos')
			 + getWherePart( 'needprint','needprint')
			 + getWherePart( 'IssueType','IssueType')
			 + getWherePart( 'IsueManageCom','IsueManageCom')
  }		
  else if(tOperatePos == "2")
  {
	strSQL = "select proposalno,issuetype,issuecont,replyresult,operator,makedate,OperatePos,BackObjType,needprint,SerialNo from lcissuepol where "+k+"="+k+" "				 	
			 + " and OperatePos in ('1','5')"
			 + " and backobjtype = '1'"
			 + " and IsueManageCom like '" + manageCom + "%%'"
			 + getWherePart( 'ProposalNo','ProposalNo' )
			 + getWherePart( 'BackObjType','BackObj' )
			 + getWherePart( 'Operator','Operator' )
			 + getWherePart( 'OperatePos','OperatePos')
			 + getWherePart( 'needprint','needprint')
			 + getWherePart( 'IssueType','IssueType')
			 + getWherePart( 'IsueManageCom','IsueManageCom')
  }
  else if(tOperatePos == "3")
  {
	strSQL = "select proposalno,issuetype,issuecont,replyresult,operator,makedate,OperatePos,BackObjType,needprint,SerialNo from lcissuepol where "+k+"="+k+" "				 					 
			 + " and IsueManageCom like '" + manageCom + "%%'"
			 + getWherePart( 'ProposalNo','ProposalNo' )
			 + getWherePart( 'BackObjType','BackObj' )
			 + getWherePart( 'Operator','Operator' )
			 + getWherePart( 'OperatePos','OperatePos')
			 + getWherePart( 'needprint','needprint')
			 + getWherePart( 'IssueType','IssueType')
			 + getWherePart( 'IsueManageCom','IsueManageCom')
  }
  else if(tOperatePos == "4")
  {
	strSQL = "select proposalno,issuetype,issuecont,replyresult,operator,makedate,OperatePos,BackObjType,needprint,SerialNo from lcissuepol where "+k+"="+k+" "				 	
			 + " and OperatePos in ('0','1','5')"				 
			 + " and IsueManageCom like '" + manageCom + "%%'"
			 + getWherePart( 'ProposalNo','ProposalNo' )
			 + getWherePart( 'BackObjType','BackObj' )
			 + getWherePart( 'Operator','Operator' )
			 + getWherePart( 'OperatePos','OperatePos')
			 + getWherePart( 'needprint','needprint')
			 + getWherePart( 'IssueType','IssueType')
			 + getWherePart( 'IsueManageCom','IsueManageCom')
  }
  else if(tOperatePos == "5")
  {
	strSQL = "select proposalno,issuetype,issuecont,replyresult,operator,makedate,OperatePos,BackObjType,needprint,SerialNo from lcissuepol where "+k+"="+k+" "				 	
			 + " and OperatePos in ('0','1','5')"
			 + " and IsueManageCom like '" + manageCom + "%%'"
			 + getWherePart( 'ProposalNo','ProposalNo' )
			 + getWherePart( 'BackObjType','BackObj' )
			 + getWherePart( 'Operator','Operator' )
			 + getWherePart( 'OperatePos','OperatePos')
			 + getWherePart( 'needprint','needprint')
			 + getWherePart( 'IssueType','IssueType')
			 + getWherePart( 'IsueManageCom','IsueManageCom')
  }
  else
  {
	strSQL = "select proposalno,issuetype,issuecont,replyresult,operator,makedate,OperatePos,BackObjType,needprint,SerialNo from lcissuepol where "+k+"="+k+" "
			 + " and IsueManageCom like '" + manageCom + "%%'"
			 + getWherePart( 'ProposalNo','ProposalNo' )
			 + getWherePart( 'BackObjType','BackObj' )
			 + getWherePart( 'Operator','Operator' )
			 + getWherePart( 'OperatePos','OperatePos')
			 + getWherePart( 'needprint','needprint')
			 + getWherePart( 'IssueType','IssueType')
			 + getWherePart( 'IsueManageCom','IsueManageCom')
  }	  	  
  	
  if (ifreply == "N") {
    strSQL = strSQL + " and ReplyMan is null";
  }
  else if (ifreply == "Y") {
    strSQL = strSQL + " and ReplyMan is not null";
  }

  strSQL = strSQL + " order by makedate";
	
  turnPage.queryModal(strSQL, QuestGrid); 
  return true;	
}

function query2() {
  // 初始化表格
	initQuestGrid();
	initContent();
	
	// 书写SQL语句
	k++;
	var strSQL = "";
	var ifreply = fm.IfReply.value;
	var tOperatePos = fm.OperatePos.value;
  var prtNo = fm.PrtNo.value;
  
  if (prtNo != "") {
    var strSql = "select proposalno from lcpol where prtno='" + prtNo + "'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
      fm.ProposalNo.value = arrResult[0][0];
    }
    else {
      return;
    }
  }
  
  strSQL = "select proposalno,issuetype,issuecont,replyresult,operator,makedate,OperatePos,BackObjType,needprint,SerialNo from lcissuepol where "+k+"="+k+" "
			 + " and IsueManageCom like '" + manageCom + "%%'"
			 + " and proposalno in (select otherno from loprtmanager where prtseq='" + fm.SerialNo.value + "' and othernotype='00' and code='05') "
			 + " and backObjtype='3' and needprint='Y'";
			 
  turnPage.queryModal(strSQL, QuestGrid); 
  return true;	
}


function queryone(parm1, parm2)
{	
	var strSQL = "";
	var tcho;
	
	if(fm.all(parm1).all('InpQuestGridSel').value == '1' )
	{
	//当前行第1列的值设为：选中
   		tPos = fm.all(parm1).all('QuestGrid7').value;
   		tQuest = fm.all(parm1).all('QuestGrid2').value;
   		tProposalNo = fm.all(parm1).all('QuestGrid1').value;
   		tSerialNo = fm.all(parm1).all('QuestGrid10').value;
  	}

	if (tPos == "")
	{
		alert("请选择问题件!")
		return "";
	}	
	
	if (tProposalNo == "")
	{
		alert("保单号不能为空！");
		return "";
	}
	if (tQuest == "")
	{
		alert("问题件不能为空！");
		return "";
	}

	strSQL = "select issuecont,replyresult,issuetype,OperatePos from lcissuepol where "
	       + " ProposalNo='" + tProposalNo + "'"
	       + " and issuetype='" + tQuest + "'"
	       + " and OperatePos='" + tPos + "'"
	       + " and SerialNo='" + tSerialNo + "'";
	//alert(strSQL);
	
  //查询SQL，返回结果字符串
  var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有录入过问题键！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  var arrDataCacheSet = decodeEasyQueryResult(strQueryResult);
  
  var returnstr = "";
  var tcont = "";
  var treply = "";
  var ttype = "";
  var tOperatePos = "";
  var n = arrDataCacheSet.length;
  //alert("N:"+n);
  if (n > 0)
  {
  	m = arrDataCacheSet[0].length;
  		//alert("M:"+m);
  		if (m > 1)
  		{
  			//alert("turnPage:"+arrDataCacheSet[0][0]);
			tcont = arrDataCacheSet[0][0];
			treply = arrDataCacheSet[0][1];
			ttype = arrDataCacheSet[0][2];
			tOperatePos = arrDataCacheSet[0][3];
  		}
  		else
  		{
  			alert("没有录入过问题键！");
  			return "";
  		}
  	
  }
  else
  {
  	alert("没有录入过问题键！");
	return "";
  }
 
  if (tcont == "")
  {
  	alert("没有录入过问题键！");
  	return "";
  }
  
  fm.all('Content').value = tcont;
  fm.all('replyresult').value = treply;
  fm.all('Type').value = ttype;
  return returnstr;
}

function QuestQuery() {
  var strSQL = "select code, cont from ldcodemod where codetype = 'Question'";
  
  var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  
  fm.IssueType.CodeData = strQueryResult;
}