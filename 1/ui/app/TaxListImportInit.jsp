<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>

<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	String strManageCom = tGI.ComCode;
	String strBatchNo = request.getParameter("BatchNo");
%>

<script language="JavaScript">
	var mBatchNo = "<%=strBatchNo%>";

function initInpBox(){
    try{
        fmImport.BatchNo.value = mBatchNo;
    }catch(ex){
        alert("初始化界面错误!");
    }
}

function initForm(){
    try{
        initInpBox();
        initImportBatchGrid();
        initImportErrLogGrid();

        initBatchNoInput(fmImport.BatchNo.value);
        fm.ImportBatchNo.value = fmImport.BatchNo.value;
        
        queryBatchList();
        queryImportErrLog();

        showAllCodeName();
    }catch(e){
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

/**
 * 导入确认列表
 */
function initImportBatchGrid(){
	
	var iArray = new Array();

    try{
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="客户姓名";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="性别";
        iArray[3][1]="60px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="出生日期";
        iArray[4][1]="60px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="证件类型";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="证件号码";
        iArray[6][1]="120px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="内检状态";
        iArray[7][1]="60px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="内检失败原因";
        iArray[8][1]="100px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="批次状态";
        iArray[9][1]="60px";
        iArray[9][2]=100;
        iArray[9][3]=0;
        
        ImportBatchGrid = new MulLineEnter("fm", "ImportBatchGrid"); 

        ImportBatchGrid.mulLineCount = 0;   
        ImportBatchGrid.displayTitle = 1;
        ImportBatchGrid.canSel = 0;
        ImportBatchGrid.hiddenSubtraction = 1;
        ImportBatchGrid.hiddenPlus = 1;
        ImportBatchGrid.canChk = 1;
        ImportBatchGrid.loadMulLine(iArray);
    }catch(ex){
        alert("初始化ResultGrid时出错：" + ex);
    }
}

/**
 * 导入批次错误日志别表。
 */
function initImportErrLogGrid(){
	
    var iArray = new Array();

    try{
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="被保人导入序号";
        iArray[2][1]="60px";
        iArray[2][2]=60;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="错误日志";
        iArray[3][1]="150px";
        iArray[3][2]=150;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="导入时间";
        iArray[4][1]="120px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        ImportErrLogGrid = new MulLineEnter("fm", "ImportErrLogGrid"); 

        ImportErrLogGrid.mulLineCount = 0;   
        ImportErrLogGrid.displayTitle = 1;
        ImportErrLogGrid.canSel = 0;
        ImportErrLogGrid.hiddenSubtraction = 1;
        ImportErrLogGrid.hiddenPlus = 1;
        ImportErrLogGrid.canChk = 0;
        ImportErrLogGrid.loadMulLine(iArray);
    }catch(ex){
        alert("初始化ImportErrLogGrid时出错：" + ex);
    }
}
</script>