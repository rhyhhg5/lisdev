<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：ParticularStatistic.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="UWResultStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./UWResultStatisticSub.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
					<TD  class= title> 管理机构</TD>
					<TD  class= input><Input class= "codeno"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
          <TD  class= title>核保起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|Date&NOTNULL" > </TD> 
          <TD  class= title>核保止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|Date&NOTNULL" > </TD> 
       </TR>
       	<TR  class= common>

					<TD  class= title> 
					险种编码
					</TD>
					<TD  class= input>
					<Input class= "codeno"  name=RiskCode  readonly=true elementtype=nacessary ondblclick=" getRisk(this); return showCodeList('RiskCode',[this,RiskName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RiskCode',[this,RiskName],[0,1],null,null,null,1);" ><Input class=codename  name=RiskName></TD>
          
					<TD  class= title>
					  核保师
					</TD>
					<TD  class= input  >
					<Input class= common name="UWUserCode"  >
					</TD>
       </TR>
    </table>

    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		<input type="hidden" name=querySql >
		<hr>
		统计口径:核保期间：核保整单结论确认时对应的期间（必录）；各项核保结论显示结果为百分比。核保师:核保师为空或者该机构没有此核保师，统计为所有核保师。
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>




