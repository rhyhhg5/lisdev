<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LCPreviewSave.jsp
//程序功能：人工核保最终结论录入保存
//创建日期：2005-12-13 15:11
//创建人  ：yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
CErrors tError = null;
String FlagStr = "";
String Content = "";
GlobalInput tG = (GlobalInput) session.getValue("GI");
String tLCContGrids[] = request.getParameterValues("PreviewContNo");
String tMissionID[] = request.getParameterValues("PreviewContGrid1");
String tActivityID[] = request.getParameterValues("PreviewContGrid3");
String tContNo[] = request.getParameterValues("PreviewContGrid4");
String tSubMissionID[] = request.getParameterValues("PreviewContGrid2");
String tPrtNo[] = request.getParameterValues("PreviewContGrid5");
String tChecks[] = request.getParameterValues("InpPreviewContGridChk");
int nIndex = 0;
for(nIndex = 0; nIndex < tChecks.length; nIndex++ )
{
		if( !tChecks[nIndex].equals("1") )
	{
		continue;
	}
TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("ContNo", tContNo[nIndex]);
tTransferData.setNameAndValue("MissionID", tMissionID[nIndex]);
tTransferData.setNameAndValue("SubMissionID", tSubMissionID[nIndex]);
tTransferData.setNameAndValue("ActivityID", tActivityID[nIndex]);
System.out.println("ContNo  ::" + tContNo[nIndex]);
System.out.println("MissionID  ::" + tMissionID[nIndex]);
System.out.println("SubMissionID  ::" + tSubMissionID[nIndex]);
System.out.println("ActivityID  ::" + tActivityID[nIndex]);
try
{
  VData tVData = new VData();
  tVData.add(tTransferData);
  tVData.add(tG);
  TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
  if (tTbWorkFlowUI.submitData(tVData, tActivityID[nIndex]) == false)
  {
      Content += "个单"+tPrtNo[nIndex]+"预打保单失败，原因是: " + tTbWorkFlowUI.mErrors.getFirstError()+"<BR>";
    	FlagStr = "Fail";
    	tTbWorkFlowUI.mErrors.clearErrors();
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息

}
catch (Exception e)
{  
  Content = "个单预打保单失败";
  FlagStr = "Fail";
  break;
}
if (!FlagStr.equals("Fail"))
{
	Content = "个单预打保单成功! ";
	FlagStr = "Succ";
}
}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
