<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

	<%
		GlobalInput tGI = new GlobalInput();
		tGI = (GlobalInput) session.getValue("GI");
	%>
	<script>
		var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<script src="../common/Calendar/Calendar.js"></script>
		<SCRIPT src="EJAppGrpPolQuery.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="EJAppGrpPolQueryInit.jsp"%>
		<title>团体投保单查询</title>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit">
			<!-- 保单信息部分 -->
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输入查询条件：
					</td>
				</tr>
			</table>
			<table class=common align=center>
				<TR class=common>
					<TD class=title>
						保单号
					</TD>
					<TD class=input>
						<Input class=common name=GrpContNo>
					</TD>
					<TD class=title>
						印刷号码
					</TD>
					<TD class=input>
						<Input class=common name=PrtNo>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class="codeNo" name=ManageCom elementtype=nacessary
							verify="管理机构|code:comcode&NOTNULL"
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						销售渠道
					</TD>
					<TD class=input>
						<Input class=codeNo name=SaleChnl verify="销售渠道|code:SaleChnl"
							ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1]);"
							onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName readonly=true>
					</TD>
					<TD class=title>
						业务员代码
					</TD>
					<TD class=input>
						<Input class=codeNo readonly name=AgentCode verify="业务员代码|code:AgentCodet"
							ondblclick="return showCodeList('AgentCodet2',[this,AgentCodeName],[0,1,3,4],null,fm.all('ManageCom').value,'ManageCom',1);"
							onkeyup="return showCodeListKey('AgentCodet2',[this,AgentCodeName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true>
					</TD>
					<td class="title8">
						投保单位名称
					</td>
					<td class="input8">
						<input class="common" name="GrpName" />
					</td>
				</TR>
				<tr class="common">

					<td class="title8">
						客户名称
					</td>
					<td class="input8">
						<input class="common" name="InsuredName" />
					</td>
					<TD class=title style="display:''">
						性别
					</TD>
					<TD class=input style="display:''">
						<Input class="codeNo" name=InsuredSex verify="性别|code:sex" ondblclick="return showCodeList('sex',[this,CusSexName],[0,1],null);" onkeyup="return showCodeListKey('sex',[this,CusSexName],[0,1]);"><input class=codename name=CusSexName readonly=true>
					</TD>
					<td class="title8">
						证件号码
					</td>
					<td class="input8">
						<input class="common" name="InsuredIDNo" />
					</td>
				</tr>
				<tr>
					<td class="title8">
						出生日期
					</td>
					<td class="input8">
						<input class="coolDatePicker" dateFormat="short" name="InsuredBirthday" verify="出生日期|date" />
					</td>
					<td class="title8">
						生效日期起期
					</td>
					<td class="input8">
						<input class="coolDatePicker" dateFormat="short" name="StartCvalidate" verify="生效日期起期|date" />
					</td>
					<td class="title8">
						生效日期止期
					</td>
					<td class="input8">
						<input class="coolDatePicker" dateFormat="short" name="EndCvalidate" verify="生效日期止期|date" />
					</td>
				</tr>
			</table>
			<br />
			<input value="查  询" class="cssbutton" type="button" onclick="easyQueryClick();" />
			<input value="下  载" class="cssButton" type="button" onclick="downloadQryResults();" />
			<br />
			<br />
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);">
					</td>
					<td class=titleImg>
						保单信息
					</td>
				</tr>
			</table>
			<Div id="divLCPol1" style="display: ''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanPolGrid"> </span>
						</td>
					</tr>
				</table>
				<INPUT VALUE="首  页" Class=cssbutton TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" Class=cssbutton TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" Class=cssbutton TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" Class=cssbutton TYPE=button onclick="getLastPage();">
			</div>
			<P>
				<INPUT VALUE="团体投保单明细" Class=cssbutton TYPE=button onclick="returnParent();">
			</P>
			</div>
			<Div id="divLCPol2" style="display: ''">
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanPolStatuGrid"> </span>
						</td>
					</tr>
				</table>
			</div>

			<input name="querySql" type="hidden" />

		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
