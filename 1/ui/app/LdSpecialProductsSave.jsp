<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LdSpecialProductsSave.jsp
//程序功能：特殊产品定义（特殊产品打印时，指定医院和推荐意见合并统一显示，所以需配置特殊产品）
	//创建日期：201-07-07 12:00:16
	//创建人  ：郭忠华
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDCodeSchema tLDCodeSchema   = new LDCodeSchema();  
  LdSpecialProductsUI tLdSpecialProductsUI   = new LdSpecialProductsUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String Operator  = tG.Operator ;  //保存登陆管理员账号
  tLDCodeSchema.setCodeType("printriskcode1");
  tLDCodeSchema.setCode(request.getParameter("SpecRiskCode"));
  tLDCodeSchema.setCodeName(request.getParameter("RiskShowName"));
  tLDCodeSchema.setOtherSign("1");

  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLDCodeSchema);
  	tVData.add(tG);
  	if(!tLdSpecialProductsUI.submitData(tVData,transact)){
  		tError = tLdSpecialProductsUI.mErrors;
  		Content =" 操作失败，原因是:" + tError.getFirstError();
  		FlagStr = "Fail";
  	}
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLdSpecialProductsUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
 
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>