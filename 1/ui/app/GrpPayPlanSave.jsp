<%
//程序名称：GrpPayPlanSave.jsp
//程序功能：
//创建日期：2011-12-22
//创建人  ：gzh
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //输出参数
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput globalInput = new GlobalInput();
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  String mProposalGrpContNo = request.getParameter("ProposalGrpContNo");

  //校验处理
  //内容待填充
	try {
		 String tOperator = request.getParameter("fmtransact");
		 LCGrpPayPlanSet tLCGrpPayPlanSet = new LCGrpPayPlanSet();
		 if("INSERT".equals(tOperator))////保存缴费计划
		 {
		 	String[] tPlanCodes= request.getParameterValues("GrpPayPlanGrid1");
		 	String[] tPaytoDates= request.getParameterValues("GrpPayPlanGrid2");
		 	for(int i=0;i<tPlanCodes.length;i++){
			 	LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
			 	tLCGrpPayPlanSchema.setPlanCode(tPlanCodes[i]);
			 	tLCGrpPayPlanSchema.setPaytoDate(tPaytoDates[i]);
			 	tLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
		 	}
		 }else if("INSERTDetail".equals(tOperator)){
		 	String[] tPlanCodes= request.getParameterValues("GrpPayPlanDetailGrid1");
		 	String[] tPaytoDates= request.getParameterValues("GrpPayPlanDetailGrid2");
		 	String[] tContPlanCodes= request.getParameterValues("GrpPayPlanDetailGrid3");
		 	String[] tPrems= request.getParameterValues("GrpPayPlanDetailGrid4");
		 	for(int i=0;i<tPlanCodes.length;i++){
			 	LCGrpPayPlanSchema tLCGrpPayPlanSchema = new LCGrpPayPlanSchema();
			 	tLCGrpPayPlanSchema.setPlanCode(tPlanCodes[i]);
			 	tLCGrpPayPlanSchema.setPaytoDate(tPaytoDates[i]);
			 	tLCGrpPayPlanSchema.setContPlanCode(tContPlanCodes[i]);
			 	tLCGrpPayPlanSchema.setPrem(tPrems[i]);
			 	tLCGrpPayPlanSet.add(tLCGrpPayPlanSchema);
		 	}
		 }		 
		 TransferData mTransferData = new TransferData();
		 mTransferData.setNameAndValue("ProposalGrpContNo", mProposalGrpContNo);
		 mTransferData.setNameAndValue("LCGrpPayPlanSet",tLCGrpPayPlanSet);
		
		 // 准备传输数据 VData
		 VData vData = new VData();
		
		 vData.addElement(globalInput);
		 vData.addElement(tLCGrpPayPlanSet);
		 vData.add(mTransferData);
		 
		 // 数据传输
		 GrpPayPlanUI tGrpPayPlanUI = new GrpPayPlanUI();
		
		 if (!tGrpPayPlanUI.submitData(vData, tOperator)) {
		   Content = " 保存失败，原因是: " + tGrpPayPlanUI.mErrors.getFirstError();
		   FlagStr = "Fail";
		 } else {
		 	Content = " 保存成功 ";
		 	FlagStr = "Succ";
		 }
	} catch(Exception ex) {
		ex.printStackTrace( );
   		Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   		FlagStr = "Fail";
	}
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
<body>
</body>
</html>

