<%
//程序名称：ReProposalPrintInput.jsp
//程序功能：
//创建日期：2002-11-25
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%!
	String handleFunction(HttpSession session, HttpServletRequest request) {
	int nIndex = 0;
	String tLCPolGrids[] = request.getParameterValues("PolGridNo");
	String tContNo[] = request.getParameterValues("PolGrid1");
	String tChecks[] = request.getParameterValues("InpPolGridChk");
	String strOperation = request.getParameter("fmtransact");

	GlobalInput globalInput = new GlobalInput();

	if( (GlobalInput)session.getValue("GI") == null )
	{
		return "网页超时或者是没有操作员信息，请重新登录";
	}
	else
	{
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
	}

	if( tLCPolGrids == null ) {
		return "没有输入需要的打印参数";
	}

	LCContSet tLCContSet = new LCContSet();
	ReLCContF1PUI tReLCContF1PUI = new ReLCContF1PUI();

	for(nIndex = 0; nIndex < tChecks.length; nIndex++ )
	{
		// If this line isn't selected, continue
		if( !tChecks[nIndex].equals("1") )
		{
		  continue;
		}

		if( tContNo[nIndex] == null || tContNo[nIndex].equals("") ) {
		  return "请输入保单号的信息";
		}

		LCContSchema tLCContSchema = new LCContSchema();

		tLCContSchema.setContNo( tContNo[nIndex] );

		tLCContSet.add(tLCContSchema);
	}

	// Prepare data for submiting
	VData vData = new VData();

	vData.addElement(tLCContSet);
	vData.add(globalInput);

	try {
		if( !tReLCContF1PUI.submitData(vData, strOperation) )
		{
	   		if ( tReLCContF1PUI.mErrors.needDealError() )
	   		{
	   			return tReLCContF1PUI.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		return "保存失败，但是没有详细的原因";
			}
		}

	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		return ex.getMessage();
	}
	return "";
}
%>
<%
String FlagStr = "";
String Content = "";

try {
	Content = handleFunction(session, request);

	if( Content.equals("") ) {
		String strTemplatePath = application.getRealPath("xerox/printdata") + "/";
		FlagStr = "Succ";
		Content = "提交申请成功";
	} else {
		FlagStr = "Fail";
	}
} catch (Exception ex) {
	ex.printStackTrace();
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

