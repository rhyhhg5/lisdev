//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false )
    return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LDDisease.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  //  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
  fm.fmtransact.value = "INSERT||MAIN" ;
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
    var i = 0;
    if( verifyInput2() == false )
      return false;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmtransact.value = "UPDATE||MAIN";
    fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDiseaseQuery.html");
}


//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmtransact.value = "DELETE||MAIN";
    fm.submit(); //提交
    initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function showRiskInfo()
{
	var strSql = "select nvl(sum(prem), 0), nvl(sum(SupplementaryPrem), 0) from lcpol where PrtNo='"+prtNo+"'";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		fm.SumPrem.value = arr[0][0];
        fm.SumSupplementaryPrem.value = arr[0][1];
	}
	var strSql = "select b.name,a.riskcode,a.amnt,a.mult,"
	+"(select distinct BnfType from lcbnf where lcbnf.polno=a.polno and lcbnf.insuredno=a.insuredno),"
	+" trim(char(a.InsuYear))||a.InsuYearFlag,case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end  ,prem, SupplementaryPrem from lcpol a,lcinsured b where "
	+" a.contno=b.contno "
	+" and a.insuredno=b.insuredno "
	+" and b.PrtNo='"+prtNo+"'";
	turnPage.queryModal(strSql, ComplexRiskGrid);
	changeChar();
	//try { top.fraPic.service.width = top.fraPic.service.width *0.7; } catch (ex) {}
	try { goToPic(1); top.fraPic.scrollTo(0, 1045);  } catch(e) {} 
	
}

//反显受益人信息
function showBnfInfo()
{
	var strSql = "select a.BnfType,a.Name,a.IDType,a.IDNo,a.RelationToInsured,a.BnfLot,a.BnfGrade," 
	            + " (select riskcode from lcpol where polno = a.polno) from lcbnf a where a.ContNo in(select contno from lccont where prtno = '"+prtNo+"')";
	var arr = easyExecSql(strSql);
	turnPage.queryModal(strSql, BnfGrid);	
}

function changeChar(){
	for(var a=0 ; a< ComplexRiskGrid.mulLineCount ; a++){
			for(var i=6 ; i < ComplexRiskGrid.colCount ; i++){
				//alert(ComplexRiskGrid.getRowColData(a,i).indexOf("Y"));
				//alert(ComplexRiskGrid.getRowColData(a,i+1));
					ComplexRiskGrid.setRowColData(a,i,replace(ComplexRiskGrid.getRowColData(a,i),"1000A","终身"));
					ComplexRiskGrid.setRowColData(a,i,replace(ComplexRiskGrid.getRowColData(a,i),"70A","到70岁"));
					ComplexRiskGrid.setRowColData(a,i,replace(ComplexRiskGrid.getRowColData(a,i),"Y","年"));	

		}
	}
}




//外包反馈错误信息 2007-9-27 20:21
function findIssue()
{
	window.open("./BPOIssueInput.jsp?prtNo="+prtNo,"window1");
}