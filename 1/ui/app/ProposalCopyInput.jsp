<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="ProposalCopyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ProposalCopyInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ProposalCopySave.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人投保单查询条件 -->
    <TR  class= common>
     <!-- <TD  class= titles>
        个人投保单号码
      </TD>-->
        <Input class= common name=PolNo type="hidden">
    </TR>
       <INPUT VALUE="查询" TYPE=button onclick="queryGrp();"> 
    <!-- 个单查询结果 -->
    <table>
      <tr>
      <td>
	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divProposalCopy1);">
      </td>
      <td class= titleImg>
        个人投保单信息
      </td>
    	</tr>
    </table>
    <Div  id= "divProposalCopy1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
    <!--      <TD  class= title>
            个单投保单号码
          </TD>-->
            <Input class="readonly" readonly name=ProposalNo  type="hidden">
   <!--       <TD  class= title>
            集体投保单号码
          </TD>-->
            <Input class="readonly" readonly name=GrpProposalNo  type="hidden">
          <TD  class= title>
            保单生效日期
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=CValiDate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskVersion >
          </TD>
          <TD  class= title>
            交费位置
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=PayLocation >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            交费间隔
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=PayIntv >
          </TD>
          <TD  class= title>
            终交年龄年期
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=PayEndYear >
          </TD>
          <TD  class= title>
            终交年龄年期标志
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=PayEndYearFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            领取年龄年期
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GetYear >
          </TD>
          <TD  class= title>
            领取年龄年期标志
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GetYearFlag >
          </TD>
          <TD  class= title>
            起领日期计算类型
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GetStartType >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保险年龄年期
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuYear >
          </TD>
          <TD  class= title>
            保险年龄年期标志
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuYearFlag >
          </TD>
          <TD  class= title>
            份数
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Mult >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            总保费
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Prem >
          </TD>
          <TD  class= title>
            总保额
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Amnt >
          </TD>
        </TR>
      </table>
    </Div>
    <!-- 个单查询结果 -->
    <table>
      <tr>
      <td>
	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divProposalCopy2);">
      </td>
      <td class= titleImg>
        投保单位信息
      </td>
    	</tr>
    </table>
    <Div  id= "divProposalCopy2" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            单位号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GrpNo >
          </TD>
          <TD  class= title>
            名称
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Name >
          </TD>
          <TD  class= title>
            联系人
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=LinkMan >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Phone >
          </TD>
          <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Address >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ZipCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Fax >
          </TD>
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=EMail >
          </TD>
        </TR>
      </table>
    </Div>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInsuredGrid);">
    		</td>
    		<td class= titleImg>
    			 被保人信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divInsuredGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanInsuredGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="复制投保单" TYPE=button > 
      <INPUT VALUE="取消" TYPE=button > 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
