//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function initQuery() {
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSql = "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName from LCPol where " + ++queryBug + "=" + queryBug
    				 + " and AppFlag='0' "                 //个人保单
    				 + " and GrpPolNo='" + grpPolNo + "'"  //集体单号，必须为20个0
    				 + " and ContNo='" + contNo + "'"      //内容号，必须为20个0
	           + " and ManageCom like '" + manageCom + "%%'";
  //alert(strSql);
	turnPage.queryModal(strSql, PolGrid);
}

var mSwitch = parent.VD.gVSwitch;
function quashClick() {
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo(i);
      break;
    }
  }
  
  if (checkFlag) { 
  	var cProposalNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	//mSwitch.deleteVar( "PolNo" );
  	//mSwitch.addVar( "PolNo", "", cPolNo );
  	
    mAction = "DELETE";
		fm.all('fmAction').value = mAction;
		fm.all('ProposalNo').value = cProposalNo;
		fm.submit(); //提交
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

function firstPage() {
  
  turnPage.firstPage();
}

function previousPage() {
  turnPage.previousPage();
}

function nextPage() {
  turnPage.nextPage();
}

function lastPage() {
  turnPage.lastPage();
}

function turnPageCheck() {
  var i = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      alert("必须提交当前页面数据后，才能进行翻页！\n请按“撤消选中保单”按钮");
      break;
    }
  }
}

/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content ) {
	if (FlagStr == "Fail" )	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else { 
		content = "撤单成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	
	initQuery();
}


