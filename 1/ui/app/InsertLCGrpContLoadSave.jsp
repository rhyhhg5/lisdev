<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>


<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String tOperator = tG.Operator;
	String tComCode = tG.ComCode;
	
	//输出参数
	CErrors tError = null;
	String content = "";
	String FlagStr = "";
	String mDescType = "";
	
	//得到前台页面的数据
	String tEngineeringFlag = request.getParameter("EngineeringFlag");
	String tfmtransact = request.getParameter("fmtransact");
	String tPrtNo = request.getParameter("PrtNo");
	String tCountryName=
// 			new String(
			request.getParameter("CountryName")
// 			.getBytes("ISO-8859-1"),"GB2312")
	;
	String tEngineering=
// 			new String(
					request.getParameter("Engineering")
// 					.getBytes("ISO-8859-1"),"GB2312")
	;
	String tCountryCategory=request.getParameter("CountryCategory");
	//测试输出
	System.out.println("Country:"+tCountryName);
	System.out.println("tEngineeringFlag:"+tEngineeringFlag);
	System.out.println("tEngineering:"+tEngineering);
	System.out.println("PrtNo:"+tPrtNo);
	System.out.println("tComCode:"+tComCode);
	System.out.println("tCountryCategory:"+tCountryCategory);
	
	/* 封装LCGrpContRoadSchema数据 */
	LCGrpContRoadSchema tLCGrpContRoadSchema = new LCGrpContRoadSchema();

	tLCGrpContRoadSchema.setPrtNo(tPrtNo);
	//tLCGrpContRoadSchema.setCountry(request.getParameter("CountryName"));
	tLCGrpContRoadSchema.setCountryCategory(request.getParameter("CountryCategory"));
	tLCGrpContRoadSchema.setCountry(request.getParameter("Country"));
	tLCGrpContRoadSchema.setEngineeringFlag(request.getParameter("EngineeringFlag"));
	tLCGrpContRoadSchema.setEngineeringCategory(request.getParameter("Engineering"));
	tLCGrpContRoadSchema.setManageCom(tComCode);

	InsertLCGrpContLoadBL tInsertLCGrpContLoadBL = new InsertLCGrpContLoadBL();

	try{
		VData tVData = new VData();
		tVData.add(tLCGrpContRoadSchema);

		boolean b = tInsertLCGrpContLoadBL.submitData(tVData, tfmtransact);
		
	}catch(Exception ex){
		content = mDescType+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	System.out.println("end"+mDescType);

  	if (FlagStr=="")
  	{
    	tError = tInsertLCGrpContLoadBL.mErrors;
    	
    if (!tError.needDealError())
    {
      	content = mDescType+"成功";
    	FlagStr = "Succ";
    }
    else
    {
    	content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	System.out.println(content);
    	FlagStr = "Fail";
    }
  }
%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=content%>");
</script>
</html>
