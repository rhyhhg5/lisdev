<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContDeleteInit.jsp
//程序功能：个单整单删除
//创建日期：2004-12-06 11:10:36
//创建人  ：Zhangrong
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	String strManageCom = globalInput.ComCode;
	String strOperator = globalInput.Operator;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
    // 保单查询条件  
    fm.all('QProposalNo').value = '';
    fm.all('QContNo').value = '';
    fm.all('QAgentCode').value = '';
    fm.all('QState').value = '未签单';
    fm.all('QPrtNo').value = '';       
    fm.all('DeleteReason').value = '';                                  
  }
  catch(ex)
  {
    alert("在ContDeleteInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 保单基本信息显示框的初始化（单记录部分）
function initPolBox()
{ 
  try
  {                                   
	// 保单查询条件

  }
  catch(ex)
  {
    alert("在ContUWInit.jsp-->InitPolBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
  	fm.all('QManageCom').value = <%=strManageCom%>;
    if(fm.all('QManageCom').value ==86){
    	fm.all('QManageCom').readOnly=false;
    	}
    else{
    	fm.all('QManageCom').readOnly=true;
    	}
    if(fm.all('QManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('QManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('QManageComName').value=arrResult[0][0];
            } 
    	}
    initInpBox();
    initGrid();

  }
  catch(re)
  {
    alert("ContUWInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


// 保单信息列表的初始化
function initGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="投保单号";         		//列名
      iArray[1][1]="160px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="保单号";         		//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="印刷号";         		//列名
      iArray[3][1]="160px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;  
      
      iArray[4]=new Array();
      iArray[4][0]="投保人";         		//列名
      iArray[4][1]="160px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="管理机构";         		//列名
      iArray[5][1]="120px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              			//是否允许输入,1表示允许，0表示不允许
      iArray[6]=new Array();
      iArray[6][0]="销售渠道";                 //列名
      iArray[6][1]="80px";  
      iArray[6][2]=100;              //列名
      iArray[6][3]=0;                //列名

      iArray[7]=new Array();
      iArray[7][0]="网点";                 //列名
      iArray[7][1]="80px";
      iArray[7][2]=100;                //列名
      iArray[7][3]=0;                //列名        			

      Grid = new MulLineEnter( "fm" , "Grid" ); 
      //这些属性必须在loadMulLine前
      Grid.mulLineCount = 0;   
      Grid.displayTitle = 1;
      Grid.locked = 1;
      Grid.canSel = 1;
      Grid.hiddenPlus = 1;
      Grid.hiddenSubtraction = 1;
      Grid.loadMulLine(iArray);
      
      Grid.selBoxEventFuncName = "ContSelect"; 
      
      //这些操作必须在loadMulLine后面
      //Grid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>