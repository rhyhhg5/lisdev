<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolPrintSave.jsp
//程序功能：
//创建日期：2002-11-26
//创建人  ：Kevin
//修改人  ：朱向峰
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
String FlagStr = "";
String Content = "";
String tOperate = "";
String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
String sOutXmlPath = application.getRealPath("");	//xml文件输出路径

//获得mutline中的数据信息
int nIndex = 0;
String tLCGrpContGrids[] = request.getParameterValues("GrpContGridNo");
String tGrpContNo[] = request.getParameterValues("GrpContGrid1");
String tPrintCount[] = request.getParameterValues("GrpContGrid6");
String printInsureDetail[] = request.getParameterValues("GrpContGrid8");
String contPrintFlag[] = request.getParameterValues("GrpContGrid10");
String tChecks[] = request.getParameterValues("InpGrpContGridChk");

//获得session中的人员喜讯你
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//操作对象及容器
//LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
//VData vData = new VData();

LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
String tCode = "J202";//汇交件打印类型  告知书
TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("Code", tCode);

tG.ClientIP = request.getHeader("X-Forwarded-For");
if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
   tG.ClientIP = request.getRemoteAddr(); 
}
System.out.println("------操作员的IP地址:"+tG.ClientIP);

//循环打印选中的团单
for(nIndex = 0; nIndex < tChecks.length; nIndex++ )
{
    //LCHJGrpContF1PBL tLCHJGrpContF1PBL = new LCHJGrpContF1PBL();
    
	//If this line isn't selected, continue，如果没有选中当前行，则继续
	if( !tChecks[nIndex].equals("1") )
	{
		continue;
	}
	//将数据放入合同保单集合
	//tLCGrpContSchema = new LCGrpContSchema();
	//tLCGrpContSchema.setGrpContNo(tGrpContNo[nIndex]);
	tLOPRTManagerSchema = new LOPRTManagerSchema();
	//判定打印模式
	System.out.println("打印模式"+tPrintCount[nIndex]);
	tLOPRTManagerSchema.setOtherNo(tGrpContNo[nIndex]);
	tLOPRTManagerSchema.setStandbyFlag1(tGrpContNo[nIndex]);
	tLOPRTManagerSet.add(tLOPRTManagerSchema);
	
	//if (tPrintCount[nIndex].compareTo("0") == 0)
	//{
	//	tOperate = "PRINT";
	//}
	//else
	//{
	//	tOperate = "REPRINT";
	//}
	//将数据集合放入容器中，准备传入后台处理
	
	//vData = new VData();
	//vData.add(tG);
	
	//vData.addElement(tLCGrpContSchema);
	//vData.add(szTemplatePath);
	//vData.add(sOutXmlPath);
	//vData.add(printInsureDetail[nIndex]);
	//vData.add(contPrintFlag[nIndex]);
	//执行后台操作
	//try
	//{
	//	if (!tLCHJGrpContF1PBL.submitData(vData, "PRINT")){
	//		Content += "团单"+tGrpContNo[nIndex]+"打印失败，原因是:" + tLCHJGrpContF1PBL.mErrors.getFirstError()+"<BR>";
	//		FlagStr = "Fail";
	//		tLCHJGrpContF1PBL.mErrors.clearErrors();
	//		}
	//}
	//catch(Exception ex)
	//{
		//一旦有团单打印失败，则跳出循环
	//	Content += "团单"+tGrpContNo[nIndex]+"打印失败，原因是:" + ex.toString() +"<BR>";
	//	FlagStr = "Fail";
	//}
	
	
}

VData tVData = new VData();
tVData.add(tG);
tVData.add(tLOPRTManagerSet);
tVData.add(tTransferData);
try
{
	HJGrpSubContBatchPrtBL tHJGrpSubContBatchPrtBL = new HJGrpSubContBatchPrtBL();
	if (!tHJGrpSubContBatchPrtBL.submitData(tVData, "batch")){
		Content += "团单打印失败，原因是:" + tHJGrpSubContBatchPrtBL.mErrors.getFirstError()+"<BR>";
		FlagStr = "Fail";
		tHJGrpSubContBatchPrtBL.mErrors.clearErrors();
		}
}
catch(Exception ex)
{
	//一旦有团单打印失败，则跳出循环
	Content += "团单打印失败，原因是:" + ex.toString() +"<BR>";
	FlagStr = "Fail";
}
// Prepare data for submiting
//如果没有失败，则返回打印成功
if (!FlagStr.equals("Fail"))
{
	Content = "团单打印成功! ";
	FlagStr = "Succ";
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>