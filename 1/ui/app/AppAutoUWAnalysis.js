var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var i = 0;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>12)
	{
		alert("统计期最多为12个月！");
		return false;
	}
    var tManageCom = fm.ManageCom.value;  
    var tSaleChnl = fm.SaleChnl.value;
    var strSQL = " select a.managecom,a.prtno,getUniteCode(a.agentcode),(select name from laagent where agentcode =a.agentcode), "
             + "b.branchattr,b.name,db2inst1.codename('lcsalechnl',a.salechnl),a.approvedate,a.uwflag, "
             + "a.UWdate,a.UWoperator from lccont a,labranchgroup b where a.agentgroup = b.agentgroup and a.conttype='1' and a.approvedate  between " 
             + "'"+tStartDate+"' and '"+tEndDate+"'"
             + getWherePart('a.SaleChnl','SaleChnl')
             + " and a.managecom like '"+tManageCom+"%' and a.prtno in (select prtno from lcpol where renewcount=0) order by a.managecom,b.branchattr,a.agentcode with ur "

    fm.AnalysisSql.value = strSQL; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}  
	fm.submit();
}