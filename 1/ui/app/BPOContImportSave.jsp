<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：外包录入错误数据修改被保人保存页面
//创建日期：2007-10-25 20:40
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.ArrayList"%>
  
<%
	//输出参数            
	String FlagStr="";
	String Content = "";
	
	String tBPOBatchNo = request.getParameter("BPOBatchNo");
	String tContID = request.getParameter("ContID");
	String tMissionID = request.getParameter("MissionID");
	String tPrtNo = request.getParameter("PrtNo");
	
	String[] data = {tBPOBatchNo, tMissionID};
	ArrayList lis = new ArrayList();
  lis.add(data);
  
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	VData tVData = new VData();
	tVData.add(lis);                                       
	tVData.add(tG);
	
	BPOContImportUI tBPOContImportUI = new BPOContImportUI();                               
	if(!tBPOContImportUI.submitData( tVData, SysConst.UPDATE))                       
	{                                                                               
		Content = " 保存失败，原因是: " + tBPOContImportUI.mErrors.getErrContent();
		FlagStr = "Fail";
	}
	else
	{
		Content = " 处理成功! ";
		FlagStr = "Succ";
	}
  
  Content = PubFun.changForHTML(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterImportSubmitBPO("<%=FlagStr%>","<%=Content%>");
</script>
</html>

