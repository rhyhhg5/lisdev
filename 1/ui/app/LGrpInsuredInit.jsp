<%
//程序名称：LGrpInsuredInit.jsp
//程序功能：大团单被保人录入页面
//创建日期：2013-05-14
//创建人  ：张成轩
%>

<script language="JavaScript">
function initForm()
{
  try
  {
  	initContPlanPeoplesGrid();
  	initContInfo();
  }
  catch(re)
  {
    alert("LCInuredListInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initContPlanPeoplesGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";         			//列宽
        iArray[0][2]=10;          			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="保障计划";    			//列名
        iArray[1][1]="40px";            			//列宽
        iArray[1][2]=20;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许5

        iArray[2]=new Array();
        iArray[2][0]="人员类别";         		//列名
        iArray[2][1]="55px";            			//列宽
        iArray[2][2]=20;            			//列最大值
        iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0

        iArray[3]=new Array();
        iArray[3][0]="导入被保险人数";         		//列名
        iArray[3][1]="45px";            			//列宽
        iArray[3][2]=20;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0
        
        iArray[4]=new Array();
        iArray[4][0]="导入失败人数";         		//列名
        iArray[4][1]="45px";            			//列宽
        iArray[4][2]=20;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0
        
        iArray[5]=new Array();
        iArray[5][0]="已校验人数";         		//列名
        iArray[5][1]="45px";            			//列宽
        iArray[5][2]=20;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0
        
        iArray[6]=new Array();
        iArray[6][0]="校验失败人数";         		//列名
        iArray[6][1]="45px";            			//列宽
        iArray[6][2]=20;            			//列最大值
        iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0
        
        iArray[7]=new Array();
        iArray[7][0]="已算费人数";         		//列名
        iArray[7][1]="45px";            			//列宽
        iArray[7][2]=20;            			//列最大值
        iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0
        
        iArray[8]=new Array();
        iArray[8][0]="算费失败人数";         		//列名
        iArray[8][1]="45px";            			//列宽
        iArray[8][2]=20;            			//列最大值
        iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0

        ContPlanPeoplesGrid = new MulLineEnter("fm", "ContPlanPeoplesGrid");
        ContPlanPeoplesGrid.mulLineCount = 0;
        ContPlanPeoplesGrid.displayTitle = 1;
        ContPlanPeoplesGrid.hiddenPlus = 1;
        ContPlanPeoplesGrid.hiddenSubtraction = 1;
        ContPlanPeoplesGrid.canSel = 0;
        ContPlanPeoplesGrid.canChk = 0;
        ContPlanPeoplesGrid.loadMulLine(iArray);

    }
    catch(ex)
    {
        alert(ex);
    }
}
</script>
