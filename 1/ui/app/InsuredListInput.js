//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询事件
function easyQueryClick()
{
    if(fm.grpContNo.value == "" && fm.prtNo.value == "")
    {
        if(fm.startSignDate.value == "" && fm.endSignDate.value == "")
        {
            alert("没有录入印刷号及保单号，请录入承保日期！");
            return false;
        }
        if(dateDiff(fm.startSignDate.value, fm.endSignDate.value, "M") > 3)
        {
            alert("统计期最多为三个月！");
            return false;
        }
    }
    
    var strSQL = "select GrpContNo, PrtNo, ManageCom, getUniteCode(AgentCode), AgentCom, SaleChnl, SignDate, "
        + "(select count(1) from LCInsured b where a.GrpContNo = b.GrpContNo) "
        + "from LCGrpCont a "
        + "where 1 = 1 "
        + getWherePart('GrpContNo', 'grpContNo')
        + getWherePart('PrtNo', 'prtNo')
        + getWherePart('ManageCom', 'manageCom', 'like')
        + getWherePart('getUniteCode(AgentCode)', 'agentCode')
        + getWherePart('AgentCom', 'agentCom')
        + getWherePart('SaleChnl', 'saleChnl')
        + getWherePart('SignDate', 'startSignDate', '>=')
        + getWherePart('SignDate', 'endSignDate', '<=')
        + "order by SignDate";

    turnPage.queryModal(strSQL, InsuredListGrid);
}

//下载事件
function downloadList()
{
    var row = InsuredListGrid.getSelNo() - 1;
    if(row < 0)
    {
        alert("请先查询并选中！");
        return;
    }
    var grpContNo = InsuredListGrid.getRowColDataByName(row, "grpContNo");
    fmSave.prtNo.value = InsuredListGrid.getRowColDataByName(row, "prtNo");
    
    var strSQL = "select a.InsuredNo, a.Name, a.Sex, a.Birthday, a.IDType, a.IDNo, "
        + "(select nvl(sum(Prem),0) from LCPol b where a.GrpContNo=b.GrpContNo and b.InsuredNo=a.InsuredNo), "
        + "ContPlanCode, a.OccupationType, a.InsuredStat "
        + "from LCInsured a where a.GrpContNo='" + grpContNo +"' and a.Name<>'公共账户' "
        + "order by integer(a.DiskImportNo) with ur";
    fmSave.querySql.value = strSQL;
    
    fmSave.action = "InsuredListDownload.jsp";
    fmSave.target = "_blank";
    fmSave.submit();
}
