var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
/*"查询"按钮*/
function queryClick() {
	var strSQL = "select code,codename,othersign from ldcode where codetype = 'grphjrisk' "
				+getWherePart('code','riskind')
				+"with ur ";
	 turnPage1.queryModal(strSQL, ProjectMonth);
	if (!turnPage1.queryModal) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}	
}
/*"保存"按钮*/
function saveClick(){
	if(fm.riskind.value==""){
		alert("请您选择要保存的险种编号");
		return false;
	}
	fm.fmtransact.value = "INSERT";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
/*"更新"按钮*/
function updateClick(){
	var rowNum = ProjectMonth.getSelNo();
	if(rowNum==0){
		alert("请您选择要修改状态的行");
		return false;
	}
	var tCode = ProjectMonth.getRowColData(rowNum-1,1);//险种编码
	var tCodeType = ProjectMonth.getRowColData(rowNum-1,3);//险种状态
	fm.subCode.value = tCode; 
	fm.subCodeType.value = tCodeType;
	fm.fmtransact.value = "UPDATE";
	var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
/*"删除"按钮*/
function deleteClick(){
	var rowNum = ProjectMonth.getSelNo();
	if(rowNum==0){
		alert("请您选择要删除的行");
		return false;
	}
	var tCode = ProjectMonth.getRowColData(rowNum-1,1);//险种编码
	fm.subCode.value = tCode;
	fm.fmtransact.value = "DELETE";
	var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //initForm();
    var strSQL = "select code,codename,othersign from ldcode where codetype = 'grphjrisk' "
				+" and code= '"+fm.subCode.value+"'"
				+"with ur ";
	 turnPage1.queryModal(strSQL, ProjectMonth);
  	
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //initForm();
    var strSQL = "select code,codename,othersign from ldcode where codetype = 'grphjrisk' "
				+" and code= '"+fm.subCode.value+"'"
				+"with ur ";
	 turnPage1.queryModal(strSQL, ProjectMonth);
  }
}