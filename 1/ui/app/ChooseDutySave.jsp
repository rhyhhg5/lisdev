<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ProposalSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
  
  //校验处理
  //内容待填充
  
  //接收信息
  System.out.println("Start Save...");
  // 保单信息部分
  LCPolSchema tLCPolSchema   = new LCPolSchema();
  LCDutySchema tLCDutySchema = new LCDutySchema();
  
    tLCPolSchema.setPolNo(request.getParameter("PolNo"));
    tLCPolSchema.setPrtNo(request.getParameter("PrtNo"));
    tLCPolSchema.setProposalNo(request.getParameter("ProposalNo"));
    tLCPolSchema.setManageCom(request.getParameter("ManageCom"));
    tLCPolSchema.setSaleChnl(request.getParameter("SaleChnl"));
    tLCPolSchema.setAgentCom(request.getParameter("AgentCom"));
    tLCPolSchema.setAgentCode(request.getParameter("AgentCode"));
    tLCPolSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLCPolSchema.setHandler(request.getParameter("Handler"));
    tLCPolSchema.setAgentCode(request.getParameter("AgentCode1"));
    tLCPolSchema.setEndDate(request.getParameter("EndDate"));
    tLCPolSchema.setPayEndDate(request.getParameter("PayEndDate"));

  	// 险种信息部分
    tLCPolSchema.setRiskCode(request.getParameter("RiskCode"));
    tLCPolSchema.setRiskVersion(request.getParameter("RiskVersion"));
    tLCPolSchema.setCValiDate(request.getParameter("CValiDate"));
    tLCPolSchema.setMult(new Double(request.getParameter("Mult")).doubleValue());
    tLCPolSchema.setStandPrem(new Double(request.getParameter("StandPrem")).doubleValue());
    tLCPolSchema.setAmnt(new Double(request.getParameter("Amnt")).doubleValue());
    tLCDutySchema.setPayEndYear(request.getParameter("PayEndYear"));
    tLCDutySchema.setPayEndYearFlag(request.getParameter("PayEndYearFlag"));
    tLCDutySchema.setGetYear(request.getParameter("GetYear"));
    tLCDutySchema.setGetYearFlag(request.getParameter("GetYearFlag"));
    tLCDutySchema.setGetStartType(request.getParameter("GetStartType"));
    tLCDutySchema.setInsuYear(request.getParameter("InsuYear"));
    tLCDutySchema.setInsuYearFlag(request.getParameter("InsuYearFlag"));


    // 投保人信息部分
    LCAppntIndSchema tLCAppntIndSchema   = new LCAppntIndSchema();

    tLCAppntIndSchema.setCustomerNo(request.getParameter("AppntCustomerNo"));
    tLCAppntIndSchema.setName(request.getParameter("AppntName"));
    tLCAppntIndSchema.setSex(request.getParameter("AppntSex"));
    tLCAppntIndSchema.setBirthday(request.getParameter("AppntBirthday"));
    tLCAppntIndSchema.setIDType(request.getParameter("AppntIDType"));
    tLCAppntIndSchema.setIDNo(request.getParameter("AppntIDNo"));
    tLCAppntIndSchema.setRelationToInsured(request.getParameter("RelationToInsured"));
    tLCAppntIndSchema.setPhone(request.getParameter("AppntPhone"));
    tLCAppntIndSchema.setMobile(request.getParameter("AppntMobile"));
    tLCAppntIndSchema.setPostalAddress(request.getParameter("AppntPostalAddress"));
    tLCAppntIndSchema.setZipCode(request.getParameter("AppntZipCode"));
    tLCAppntIndSchema.setEMail(request.getParameter("AppntEMail"));
  	
  // 被保人信息
  LCInsuredSet tLCInsuredSet   = new LCInsuredSet();
	
	// 主被保人
	LCInsuredSchema tLCInsuredSchema   = new LCInsuredSchema();
    tLCInsuredSchema.setInsuredGrade("M");

    tLCInsuredSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLCInsuredSchema.setName(request.getParameter("Name"));
    tLCInsuredSchema.setSex(request.getParameter("Sex"));
    tLCInsuredSchema.setBirthday(request.getParameter("Birthday"));
    tLCInsuredSchema.setIDType(request.getParameter("IDType"));
    tLCInsuredSchema.setIDNo(request.getParameter("IDNo"));
    tLCInsuredSchema.setHealth(request.getParameter("Health"));
    tLCInsuredSchema.setOccupationType(request.getParameter("OccupationType"));
    tLCInsuredSchema.setMarriage(request.getParameter("Marriage"));

    tLCInsuredSet.add(tLCInsuredSchema);
    
	// 连带被保险人
	String tInsuredNum[] = request.getParameterValues("SubInsuredGridNo");
	String tInsuredCustomerNo[] = request.getParameterValues("SubInsuredGrid1");
	String tInsuredName[] = request.getParameterValues("SubInsuredGrid2");
	String tInsuredSex[] = request.getParameterValues("SubInsuredGrid3");
	String tInsuredBirthday[] = request.getParameterValues("SubInsuredGrid4");
	String tRelationToInsured[] = request.getParameterValues("SubInsuredGrid5");
	
	int InsuredCount = tInsuredNum.length;
	for (int i = 0; i < InsuredCount; i++)
	{
  		tLCInsuredSchema   = new LCInsuredSchema();
	    tLCInsuredSchema.setInsuredGrade("S");

	    tLCInsuredSchema.setCustomerNo(tInsuredCustomerNo[i]);
	    tLCInsuredSchema.setName(tInsuredName[i]);
	    tLCInsuredSchema.setSex(tInsuredSex[i]);
	    tLCInsuredSchema.setBirthday(tInsuredBirthday[i]);
	    tLCInsuredSchema.setRelationToInsured(tRelationToInsured[i]);
	    
	    tLCInsuredSet.add(tLCInsuredSchema);
	}

  // 受益人信息
  LCBnfSet tLCBnfSet   = new LCBnfSet();
	
	String tBnfNum[] = request.getParameterValues("BnfGridNo");
	String tBnfName[] = request.getParameterValues("BnfGrid1");
	String tBnfRelationToInsured[] = request.getParameterValues("BnfGrid2");
	String tBnfType[] = request.getParameterValues("BnfGrid3");
	String tBnfGrade[] = request.getParameterValues("BnfGrid4");
	String tBnfLot[] = request.getParameterValues("BnfGrid5");
	
	int BnfCount = tBnfNum.length;
	LCBnfSchema tLCBnfSchema;
	for (int i = 0; i < BnfCount; i++)
	{
  		tLCBnfSchema   = new LCBnfSchema();

	    tLCBnfSchema.setName(tBnfName[i]);
	    tLCBnfSchema.setRelationToInsured(tBnfRelationToInsured[i]);
	    tLCBnfSchema.setBnfType(tBnfType[i]);
	    tLCBnfSchema.setBnfGrade(tBnfGrade[i]);
	    tLCBnfSchema.setBnfLot(new Double(tBnfLot[i]).doubleValue());
	    
      tLCBnfSet.add(tLCBnfSchema);
	}

  // 告知信息
  LCCustomerImpartSet tLCCustomerImpartSet   = new LCCustomerImpartSet();

	String tImpartNum[] = request.getParameterValues("ImpartGridNo");
	String tImpartCustomerNo[] = request.getParameterValues("ImpartGrid1");
	String tImpartCode[] = request.getParameterValues("ImpartGrid3");
	String tImpartVer[] = request.getParameterValues("ImpartGrid4");
	String tImpartContent[] = request.getParameterValues("ImpartGrid5");

	int ImpartCount = tImpartNum.length;
	LCCustomerImpartSchema tLCCustomerImpartSchema;
	for (int i = 0; i < ImpartCount; i++)
	{
  		tLCCustomerImpartSchema   = new LCCustomerImpartSchema();

	    tLCCustomerImpartSchema.setCustomerNo(tImpartCustomerNo[i]);
	    tLCCustomerImpartSchema.setImpartCode(tImpartCode[i]);
	    tLCCustomerImpartSchema.setImpartContent(tImpartContent[i]);
        tLCCustomerImpartSchema.setImpartVer(tImpartVer[i]) ;
	    
      tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
	}

  // 特别约定
  LCSpecSet tLCSpecSet   = new LCSpecSet();
	String tSpecNum[] = request.getParameterValues("SpecGridNo");
	String tSpecCode[] = request.getParameterValues("SpecGrid1");
	String tSpecType[] = request.getParameterValues("SpecGrid2");
	String tSpecContent[] = request.getParameterValues("SpecGrid3");
	
	int SpecCount = tSpecNum.length;
	LCSpecSchema tLCSpecSchema;
	for (int i = 0; i < SpecCount; i++)
	{
  		tLCSpecSchema   = new LCSpecSchema();

	    tLCSpecSchema.setSpecCode(tSpecCode[i]);
	    tLCSpecSchema.setSpecType(tSpecType[i]);
	    tLCSpecSchema.setSpecContent(tSpecContent[i]);
	    
	    tLCSpecSet.add(tLCSpecSchema);
	}
  System.out.println("get data ready!");
  
  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLCPolSchema);
	tVData.addElement(tLCAppntIndSchema);
	tVData.addElement(tLCInsuredSet);
  tVData.addElement(tLCBnfSet);
  tVData.addElement(tLCCustomerImpartSet);
  tVData.addElement(tLCSpecSet);
  tVData.addElement(tG);

  // 数据传输
  ProposalUI tProposalUI   = new ProposalUI();
	if (!tProposalUI.submitData(tVData,"INSERT||PERSON"))
	{
      Content = " 保存失败，原因是: " + tProposalUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "")
  {
    tError = tProposalUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
  System.out.println(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

