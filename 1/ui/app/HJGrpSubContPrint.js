//程序名称：
//程序功能：
//创建日期：2009-7-15
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询团单。
 */
function queryGrpCont()
{
    fm.GS_GrpContNo.value = "";
    fm.GS_ManageCom.value = "";
    GrpSubContGrid.clearData();
    
    
    if(!verifyInput2())
    {
        return false;
    }

    var tStrSql = ""
        + " select lgc.GrpContNo, lgc.PrtNo, lgc.GrpName, "
        + " CodeName('salechnl', lgc.SaleChnl) SaleChnl, "
        + " getUniteCode(lgc.AgentCode), lgc.CValidate, lgc.ManageCom "
        + " from LCGrpCont lgc "
        + " where 1 = 1 "
        //+ " and lgc.CardFlag is null "
        + " and lgc.PrintCount >= 1 "
        //+ " and lgc.GrpContNo in(select grpcontno from lcgrppol where riskcode = '280101') "
        + " and lgc.ContPrintType='5' " // 5 表示汇交件
        + getWherePart("lgc.ManageCom", "ManageCom", "like")
        + getWherePart("lgc.GrpContNo", "GrpContNo")
        + getWherePart("lgc.PrtNo", "PrtNo")
        + getWherePart("getUniteCode(lgc.AgentCode)", "AgentCode")
        + getWherePart("lgc.CValiDate", "CValidateStart",">=")
        + getWherePart("lgc.CValiDate", "CValidateEnd","<=")
        + " with ur "
        ;
    
    turnPage1.pageDivName = "divGrpContGridPage";
    turnPage1.queryModal(tStrSql, GrpContGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有查到保单信息！");
        return false;
    }
    
    return true;
}

/**
 * 查询分单。
 */
function queryGrpSubCont()
{
    var tGrpContNo = fm.GS_GrpContNo.value;
    var tManageCom = fm.GS_ManageCom.value;
    
    if(tGrpContNo == null || tGrpContNo == "")
    {
        alert("尚未选取团单。");
        return false;
    }

    var tStrSql = ""
        + " select lcc.ContNo, lcc.CValidate, lci.InsuredNo, lci.Name, "
        + " CodeName('sex', lci.Sex) Sex, "
        + " CodeName('idtype', lci.IdType) IdType, lci.IdNo "
        + " from LCCont lcc "
        + " inner join LCInsured lci on lci.ContNo = lcc.ContNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '2' "
        + " and lcc.ManageCom = '" + tManageCom + "' "
        + " and lcc.GrpContNo = '" + tGrpContNo + "' "
        + getWherePart('lcc.InsuredName','GS_insuerdname')
        //+ " and lcc.ContNo in(select contno from lcpol where riskcode = '280101') "
        ;

    if(fm.PrintCount.value == "1"){
    	tStrSql += " and trim(trim(lcc.contno) || '_' || trim(lci.insuredno)) " 
    	          + " in(select otherno from loprtmanager where printtimes > 0 and code = 'J201')";
    }
    if(fm.PrintCount.value == "0"){
    	tStrSql += " and trim(trim(lcc.contno) || '_' || trim(lci.insuredno)) " 
    	          + " not in(select otherno from loprtmanager where printtimes > 0 and code = 'J201')";
    }
    if(fm.PrintCountG.value == "1"){
    	tStrSql += " and trim(trim(lcc.contno) || '_' || trim(lci.insuredno)) " 
    	          + " in(select otherno from loprtmanager where printtimes > 0 and code = 'J203')";
    }
    if(fm.PrintCountG.value == "0"){
    	tStrSql += " and trim(trim(lcc.contno) || '_' || trim(lci.insuredno)) " 
    	          + " not in(select otherno from loprtmanager where printtimes > 0 and code = 'J203')";
    }
    turnPage2.pageDivName = "divGrpSubContGridPage";
    turnPage2.queryModal(tStrSql, GrpSubContGrid);
    
    if (!turnPage2.strQueryResult)
    {
        alert("没有查到保单信息！");
        return false;
    }
    
    return true;
}

/**
 * 进入结算单录入。
 */
function inputCertifySettlement()
{
    var tRow = CertifySettleListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = CertifySettleListGrid.getRowData(tRow);
    
    var tPrtNo = tRowDatas[0];
    var tMissionId = tRowDatas[4];
    var tSubMissionId = tRowDatas[5];
    var tProcessId = tRowDatas[6];
    var tActivityId = tRowDatas[7];
    var tActivityStatus = tRowDatas[8];
    
    var tStrUrl = "./CertifyContSettlement.jsp"
        + "?PrtNo=" + tPrtNo
        + "&MissionId=" + tMissionId
        + "&SubMissionId=" + tSubMissionId
        + "&ProcessId=" + tProcessId
        + "&ActivityId=" + tActivityId
        + "&ActivityStatus=" + tActivityStatus
        ;

    window.location = tStrUrl;
}


function initGrpSubConf()
{
    var tRow = GrpContGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = GrpContGrid.getRowData(tRow);
    
    var tGrpContNo = tRowDatas[0];
    var tManageCom = tRowDatas[6];
    fm.GS_GrpContNo.value = tGrpContNo;
    fm.GS_ManageCom.value = tManageCom;
    GrpSubContGrid.clearData();
}


function printGrpSubPDFBatch()
{
    var tRowCount = GrpSubContGrid.mulLineCount;
    var tRowCkd = 0;

    for (var i = 0; i < tRowCount; i++)
    {
        if(GrpSubContGrid.getChkNo(i))
        {
            tRowCkd = 1;
            break;
        }
    }

    if(tRowCkd == 0)
    {
        alert("请至少选取一条保单！");
        return false;
    }
    
    //fm.fmtransact.value = "PRINT";
    fm.target = "fraSubmit";
    fm.action = "./HJGrpSubContPrintSave.jsp";
    fm.submit();

    return true;
}

//打印告知书
function printGrpSubNoticePDFBatch()
{
    var tRowCount = GrpSubContGrid.mulLineCount;
    var tRowCkd = 0;

    for (var i = 0; i < tRowCount; i++)
    {
        if(GrpSubContGrid.getChkNo(i))
        {
            tRowCkd = 1;
            break;
        }
    }

    if(tRowCkd == 0)
    {
        alert("请至少选取一条保单！");
        return false;
    }
    
    //fm.fmtransact.value = "PRINT";
    fm.target = "fraSubmit";
    fm.action = "./HJGrpSubNoticePrintSave.jsp";
    fm.submit();

    return true;
}

/**
 * PDF打印后回调函数
 */
function afterSubmit2(FlagStr, Content)
{
    // showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("打印失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    //fm.btnPrintPdf.disabled = false;
}


