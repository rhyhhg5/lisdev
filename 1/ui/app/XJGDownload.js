var showInfo;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mSql="";
function easyQueryClick()
{
    if(!verifyInput2())
    {
        return false;
    }

	var strSQL = ""
        + " select "
        + " TitleInfo.GrpContNo, TitleInfo.CValidate, TitleInfo.CInValidate, "
        + " TitleInfo.GrpName, TitleInfo.InsuCount, varchar(TitleInfo.SumActuPayMoney) SumPayMoney "
        + " from "
        + " ( "
        + " select "
        + " PolicInfo.GrpContNo, PolicInfo.CValidate, PolicInfo.CInValidate, PolicInfo.GrpName, PolicInfo.SumActuPayMoney, count(distinct PolicInfo.InsuredNo) InsuCount "
        + " from "
        + " ( "
        + " select "
        + " (select lgc.GrpContNo from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.GrpContNo from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) GrpContNo, "
        + " (select lgc.CValidate from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.CValidate from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) CValidate, "
        + " (select lgc.CInValidate from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.CInValidate from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) CInValidate, "
        + " (select lgc.GrpName from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.GrpName from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) GrpName, "
        + " (select lcp.InsuredNo from LCPol lcp where lcp.PolNo = ljapp.PolNo union all select lcp.InsuredNo from LBPol lcp where lcp.PolNo = ljapp.PolNo) InsuredNo, "
        + " ljap.SumActuPayMoney, "
        + " '' "
        + " from LJAPay ljap "
        + " inner join LJAPayPerson ljapp on ljapp.PayNo = ljap.PayNo "
        + " where 1 = 1 "
        + " and ljap.DueFeeType = '0' "
        + " and ljap.IncomeType = '1' "
        + " and not exists (select 1 from LCCont slcc where slcc.GrpContNo = ljap.IncomeNo and slcc.PolType = '1' union all select 1 from LBCont slcc where slcc.GrpContNo = ljap.IncomeNo and slcc.PolType = '1') "
        + getWherePart("ljap.ManageCom", "ManageCom")
        + getWherePart("ljap.ConfDate", "StartDate", ">=")
        + getWherePart("ljap.ConfDate", "EndDate", "<=")
        + " ) as PolicInfo "
        + " where 1 = 1 "
        + " group by PolicInfo.GrpContNo, PolicInfo.CValidate, PolicInfo.CInValidate, PolicInfo.GrpName, PolicInfo.SumActuPayMoney "
        + " union all "
        + " select "
        + " PolicInfo.GrpContNo, PolicInfo.CValidate, PolicInfo.CInValidate, PolicInfo.GrpName, PolicInfo.SumActuPayMoney, PolicInfo.InsuredNo InsuCount "
        + " from "
        + " ( "
        + " select "
        + " (select lgc.GrpContNo from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.GrpContNo from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) GrpContNo, "
        + " (select lgc.CValidate from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.CValidate from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) CValidate, "
        + " (select lgc.CInValidate from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.CInValidate from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) CInValidate, "
        + " (select lgc.GrpName from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.GrpName from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) GrpName, "
        + " (select lgc.Peoples2 from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.Peoples2 from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) InsuredNo, "
        + " ljap.SumActuPayMoney, "
        + " '' "
        + " from LJAPay ljap "
        + " inner join LJAPayPerson ljapp on ljapp.PayNo = ljap.PayNo "
        + " where 1 = 1 "
        + " and ljap.DueFeeType = '0' "
        + " and ljap.IncomeType = '1' "
        + " and exists (select 1 from LCCont slcc where slcc.GrpContNo = ljap.IncomeNo and slcc.PolType = '1' union all select 1 from LBCont slcc where slcc.GrpContNo = ljap.IncomeNo and slcc.PolType = '1') "
        + getWherePart("ljap.ManageCom", "ManageCom")
        + getWherePart("ljap.ConfDate", "StartDate", ">=")
        + getWherePart("ljap.ConfDate", "EndDate", "<=")
        + " ) as PolicInfo "
        + " where 1 = 1 "
        + " group by PolicInfo.GrpContNo, PolicInfo.CValidate, PolicInfo.CInValidate, PolicInfo.GrpName, PolicInfo.SumActuPayMoney, PolicInfo.InsuredNo "
        + " ) as TitleInfo "
        ;

	fm.qryContSQL.value = strSQL;
	turnPage1.queryModal(strSQL, BillDetailGrid);
    
    if (!turnPage1.strQueryResult)
    {
        fm.btnContDownload.disabled = true;
        fm.btnContDetailDownload.disabled = true;
        alert("没有查询到相关信息！");
        return false;
    }

    fm.btnContDownload.disabled = false;
    fm.btnContDetailDownload.disabled = false;
}

function downLoadCont()
{
    var tSql = fm.qryContSQL.value;
    if(tSql == "" || tSql == null)
    {
        alert("请先查询");
        return false;
    }

    try 
    {
        fm.qrySQL.value = fm.qryContSQL.value;
        fm.OperateType.value="DOWNLOADCont";
        
        fm.submit();
        
        fm.qrySQL.value = "";
    }
    catch(ex)
    {
        showInfo.close();
        alert(ex);
    }
}

function downLoadContDetail()
{
    var tContSql = fm.qryContSQL.value;
    if(tContSql == "" || tContSql == null)
    {
        alert("请先查询");
        return false;
    }

    try 
    {
        var strSQL = ""
            + " select "
            + " TitleInfo.GrpContNo, TitleInfo.ContPlanCode, TitleInfo.ContPlanName, "
            + " varchar(TitleInfo.Amnt) Amnt, varchar(TitleInfo.SumPayMoney) SumPayMoney, "
            + " TitleInfo.InsuName, "
            + " CodeName('ci_xj_idtype', TitleInfo.InsuIdType) InsuIdType, TitleInfo.InsuIdNo, "
            + " TitleInfo.InsuBirthday, CodeName('ci_xj_sex', TitleInfo.InsuSex) InsuSex, TitleInfo.InsuOthIdNo "
            + " from "
            + " ( "
            + " select "
            + " BasePolicyInfo.GrpContNo, "
            + " BasePolicyInfo.ContPlanCode, BasePolicyInfo.ContPlanName, "
            + " (select lci.Name from LCInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo union all select lci.Name from LBInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo) InsuName, "
            + " (select lci.Sex from LCInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo union all select lci.Sex from LBInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo) InsuSex, "
            + " (select lci.Birthday from LCInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo union all select lci.Birthday from LBInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo) InsuBirthday, "
            + " (select lci.IdType from LCInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo union all select lci.IdType from LBInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo) InsuIdType, "
            + " (select lci.IdNo from LCInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo union all select lci.IdNo from LBInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo) InsuIdNo, "
            + " (select lci.OthIdNo from LCInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo union all select lci.OthIdNo from LBInsured lci where lci.GrpContNo = BasePolicyInfo.GrpContNo and lci.InsuredNo = BasePolicyInfo.InsuredNo) InsuOthIdNo, "
            + " sum(BasePolicyInfo.Amnt) Amnt, sum(BasePolicyInfo.SumActuPayMoney) SumPayMoney "
            + " from "
            + " ( "
            + " select "
            + " ljapp.PolNo, "
            + " (select lgc.GrpContNo from LCGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo union all select lgc.GrpContNo from LBGrpCont lgc where lgc.GrpContNo = ljap.IncomeNo) GrpContNo, "
            + " (select lcp.ContPlanCode from LCPol lcp where lcp.PolNo = ljapp.PolNo union all select lcp.ContPlanCode from LBPol lcp where lcp.PolNo = ljapp.PolNo fetch first row only) ContPlanCode, "
            + " (select lccp.ContPlanName from LCContPlan lccp inner join LCPol lcp on lcp.GrpContNo = lccp.GrpContNo and lcp.ContPlanCode = lccp.ContPlanCode where lcp.PolNo = ljapp.PolNo union all select lccp.ContPlanName from LBContPlan lccp inner join LBPol lcp on lcp.GrpContNo = lccp.GrpContNo and lcp.ContPlanCode = lccp.ContPlanCode where lcp.PolNo = ljapp.PolNo fetch first row only) ContPlanName, "
            + " (select lcp.Amnt from LCPol lcp where lcp.PolNo = ljapp.PolNo union all select lcp.Amnt from LBPol lcp where lcp.PolNo = ljapp.PolNo) Amnt, "
            + " (select lcp.InsuredNo from LCPol lcp where lcp.PolNo = ljapp.PolNo union all select lcp.InsuredNo from LBPol lcp where lcp.PolNo = ljapp.PolNo) InsuredNo, "
            + " sum(ljapp.SumActuPayMoney) SumActuPayMoney, "
            + " '' "
            + " from LJAPay ljap "
            + " inner join LJAPayPerson ljapp on ljapp.PayNo = ljap.PayNo "
            + " where 1 = 1 "
            + " and ljap.DueFeeType = '0' "
            + " and ljap.IncomeType = '1' "
            + " and not exists (select 1 from LCCont slcc where slcc.GrpContNo = ljap.IncomeNo and slcc.PolType = '1' union all select 1 from LBCont slcc where slcc.GrpContNo = ljap.IncomeNo and slcc.PolType = '1') "
            + " and ljap.IncomeNo in "
            + " ( "
            + " select "
            + " ConfInfo.GrpContNo "
            + " from "
            + " ( "
            + tContSql // 此处为保持被保人信息与保单信息一致，直接
            + " ) as ConfInfo "
            + " ) "
            + " group by ljapp.PolNo, ljap.IncomeNo "
            + " ) as BasePolicyInfo "
            + " where 1 = 1 "
            + " group by BasePolicyInfo.GrpContNo, BasePolicyInfo.ContPlanCode, BasePolicyInfo.ContPlanName, BasePolicyInfo.InsuredNo "
            + " ) as TitleInfo "
            ;
        
        fm.qrySQL.value = strSQL;
        fm.OperateType.value="DOWNLOADContDetail";

        fm.submit();

        fm.qrySQL.value = "";
    }
    catch(ex)
    {
        showInfo.close();
        alert(ex);
    }
}

function afterSubmit(FlagStr,content)
{
	
	showInfo.close();
	window.focus();
	
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		
	}
	
}