<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="ProposalReceiveMain.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ProposalReceiveMainInit.jsp"%>
<title>重打个单</title>
</head>
<body onload="initForm();" >
	<form action="./ProposalReceiveSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr>
				<td class= titleImg align= center>请输入保单查询条件：</td>
			</tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>管理机构</TD>
				<TD  class= input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true > </TD>
				<TD class= title>印 刷 号</TD>
				<TD class= input><Input class= common name=PrtNo verify="印刷号|int"></TD>
				<TD class= title>合 同 号</TD>
				<TD class= input><Input class= common name=ContNo></TD>
				</TR>
			<TR class= common>
				<TD class= title>申请日期</TD>
				<TD class= input><Input class= common name=PolApplyDate></TD>
				<TD class= title>生效日期</TD>
				<TD class= input><Input class= common name=CValiDate></TD>
				<TD class= title>签单日期</TD>
				<TD class= input><Input class= common name=SignDate></TD>
			</TR>
			<TR class= common>
				<TD class= title>投 保 人</TD>
				<TD class= input><Input class= common name=AppntName verify="投保人|len<=20"></TD>
				<TD class= title>被 保 人</TD>
				<TD class= input><Input class= common name=InsuredName verify="被保人|len<=20"></TD>
			  <TD class= title>业务员代码</TD>
				<TD  class= input> <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true ></TD>
			</TR>
			<TR class= common>
				<TD class= title>接收类型</TD>
				<TD class= input><Input class=codeno name=ReceiveType  CodeData="0|^0|未接收^1|已接收^2|已退回" ondblclick="return showCodeListEx('ReceiveType',[this,ReceiveTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ReceiveType',[this,ReceiveTypeName],[0,1],null,null,null,1);"><input class=codename name=ReceiveTypeName readonly=true ></TD>
				<TD class= title>保单类型</TD>
				<TD class= input><Input class=codeno name=ContType  CodeData="0|^1|个单^2|团单" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);"><input class=codename name=ContTypeName readonly=true ></TD>
				</TR>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" TYPE=button onclick="easyQueryClick();">
	</form>
	<form action="./ProposalReceiveSave.jsp" method=post name=fmSave target="fraSubmit">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<Div id= "divLCPol1" style= "display: ''">
			<table class= common>
				<TR class= common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" ></span>
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td>
						<INPUT VALUE="接收合同" name=receivebtn class="cssButton" TYPE=button onclick="receivePol();">
						<INPUT VALUE="退回合同" name=rejectsbtn class="cssButton" TYPE=button onclick="rejectsPol();">
						<input VALUE="yes" name = "" type=hidden onclick="dataConfirm(this);">
					</td>
				</tr>
			</table>
		  <table align=center>
			<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
		 </table>
			<table>
				<tr>
					<TD  class= titleImg> 退回原因</TD>
				</tr>
				<tr>
					<TD  class= input><Input class=codeNo readonly=true name=BackReasonCode ondblclick="return showCodeList('backreasoncode',[this,BackReasonDetail],[0,1]);" onkeyup="return showCodeListKey('backreasoncode',[this,BackReasonDetail],[0,1]);"><input class=codename name=BackReasonDetail readonly=true > </TD>
        </tr>
        <tr>
        	<td>
        <textarea name=BackReason verify="问题描述|len<800" verifyorder="1" cols="110" rows="5" class="common" >
        </textarea>
          </td>
       </tr>
			</table>
		</div>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden name="mReceiveID" value="">
		<input type=hidden name="mContNo" value="">
		<input type=hidden name="mPrtNo" value="">
		<input type=hidden name="mContType" value="">
		
	</form>
<span id="spanCode"style="display: none; position:absolute; slategray"></span>
</body>
</html>
