<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalConfirmNoSave.jsp
//程序功能：
//创建日期：2007-11-15
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  
  transact = request.getParameter("fmtransact");
  
  LCUWConfirmInfoSchema tLCUWConfirmInfoSchema   = new LCUWConfirmInfoSchema();
  tLCUWConfirmInfoSchema.setPrtNo(request.getParameter("PrtNo"));
  tLCUWConfirmInfoSchema.setConfirmNo(request.getParameter("ConfirmNo"));
  tLCUWConfirmInfoSchema.setConfirmOperator(request.getParameter("ConfirmOperator"));
  tLCUWConfirmInfoSchema.setConfirmName(request.getParameter("ConfirmName"));
  tLCUWConfirmInfoSchema.setConfirmDate(request.getParameter("ConfirmDate"));
  tLCUWConfirmInfoSchema.setConfirmTime(request.getParameter("ConfirmTime"));
  tLCUWConfirmInfoSchema.setRemark(request.getParameter("Remark"));
  ProposalConfirmNoUI tProposalConfirmNoUI = new ProposalConfirmNoUI();
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLCUWConfirmInfoSchema);
  	tVData.add(tG);
    tProposalConfirmNoUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "")
  { 
    tError = tProposalConfirmNoUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = "操作成功! ";
      FlagStr = "Success";
    }
    else                                                                           
    {
      Content = "操作失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "<%=transact%>");
</script>
</html>
