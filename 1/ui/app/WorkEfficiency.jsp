<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：WorkEfficiency.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="WorkEfficiency.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./WorkEfficiencyDownload.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
					<TD  class= title> 管理机构</TD>
					<TD  class= input><Input class= "codeno"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
        <TR  class= common>

					<TD  class= title>
					契约类型
					</TD>
					<TD  class= Input>
					<input class=codeno name=QYType CodeData= "0|^个单|1^团单|2^全部|3" ondblclick="return showCodeListEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);" onkeyup="return showCodeListKeyEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);"><input class=codename name=QYType_ch></TD>    
		        
		    </TR>
		    </table>

    <input type="hidden" name=op value="">
    <input type="hidden" class="common" name="querySql" >
    <input type="hidden" class="common" name="querySqlH" >
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="downLoad()">
			    <div>
        <font color="red">
            <ul>查询字段说明：
                <li>统计起止期：保单签单日期的起止时间</li>      
            </ul>
            <ul>统计结果说明：
                <li>受理时效：保单申请日期至保单录入系统时间（如果保单有扫描件，则保单录入系统时间为：保单扫描件扫描进入系统的时间）</li>
                <li>承保时效：保单录入系统时间至保单核保完成时间</li>
                <li>收费时效：保单核保完成时间至收费成功时间</li>
                <li>出单时效：保单收费成功时间至客户签收保单时间</li>
                <li>等待时间：保单申请日期至客户签收保单时间</li>
            </ul>
        </font>
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


