<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BPOBatchList.jsp
//程序功能：外包保单修改清单
//创建日期：2008-1-29 03:48下午
//创建人  ：YangYalin
//更新记录：更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="BPOBatchList.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BPOBatchListInit.jsp"%>
  <title>扫描录入</title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table class="common">
  		<tr CLASS="common">
  			<td CLASS="title">批次号</td>
  			<td CLASS="input" COLSPAN="1">
  			  <input NAME="BPOBatchNo" CLASS="common">
      	</td> 
  			<td CLASS="title">印刷号码</td>
  			<td CLASS="input" COLSPAN="">
  			  <input NAME="PrtNo" CLASS="common">
      	</td>  		
  			<td CLASS="title">批次状态</td>
  			<td CLASS="input" COLSPAN="3">
  			  <Input class= "codeno"  name=State style="width:50" ondblclick="return showCodeList('bpobatchstate',[this,StateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bpobatchstate',[this,StateName],[0,1],null,null,null,1);" verify="批次状态|code:bpobatchstate" ><Input class=codename style="width:100" name=StateName elementtype=nacessary readonly></TD>
      	</td>  	
      	<td CLASS="title" style="display: none">管理机构</td>
  			<td CLASS="input" style="display: none">
  			  <Input class= "codeno"  name=ManageCom style="width:50" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" verify="管理机构|notnull&code:comcode" ><Input class=codename style="width:100" name=ManageComName elementtype=nacessary readonly></TD>
      	</td> 
  			<td CLASS="title"></td>
  			<td CLASS="input" COLSPAN="1"></td>	
  		</tr>
  		<tr CLASS="common">
  			<td CLASS="title">发送日期起</td>
  			<td CLASS="input" COLSPAN="1">
  			  <input NAME="StartDate" class="coolDatePicker" verify="发送日期起|notnull&date" elementtype=nacessary style="width:120">
      	</td> 
  			<td CLASS="title">止</td>
  			<td CLASS="input" COLSPAN="1">
  			  <input NAME="EndDate" class="coolDatePicker" verify="发送日期截止|notnull&date" elementtype=nacessary style="width:120">
      	</td>	
  			<td CLASS="title"></td>
  			<td CLASS="input" COLSPAN="1"></td>	
  		</tr>
    </table>
    <INPUT VALUE="查  询" class =cssButton TYPE=button onclick="queryBatchList();">
  <br></br>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBatchGrid);">
    		</td>
    		<td class= titleImg>
    			 外包批次信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBatchGrid" style= "display: ''" align=center>
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  					<span id="spanBatchGrid" >
  					</span> 
  			  </td>
  			</tr>
    	</table>
    </Div>
  	<Div id= "divPage2" align=center style= "display: none ">
      <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPage2.lastPage();"> 
    </Div>
    
    <div style="display: none">
      <INPUT VALUE="查看投保单信息" class =cssButton TYPE=button onclick="queryContInfo();">
      
      <p>
      
      <table>
      	<tr>
          <td class=common>
  			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
      		</td>
      		<td class= titleImg>
      			 批次下投保单信息
      		</td>
      	</tr>
      </table>
    	<Div  id= "divContGrid" style= "display: ''" align=center>
        <table  class= common>
         	<tr  class= common>
        	  <td text-align: left colSpan=1>
    					<span id="spanContGrid" >
    					</span> 
    			  </td>
    			</tr>
      	</table>
      </Div>
    	<Div id= "divPage3" align=center style= "display: none ">
        <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPage3.firstPage(); setErrorInfo();"> 
        <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage3.previousPage(); setErrorInfo();"> 					
        <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage3.nextPage(); setErrorInfo();"> 
        <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPage3.lastPage(); setErrorInfo();"> 
      </Div>
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
