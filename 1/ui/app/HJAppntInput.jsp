<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
		String tGrpcontno = request.getParameter("GrpContNo");
	    String tflag = request.getParameter("flag");
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="HJAppntInput.js"></script>
		<%@include file="HJAppntInputInit.jsp"%>

		<script>
    var tGrpcontno = "<%=tGrpcontno%>";
    var tflag = "<%=tflag%>";
  </script>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./HJAppntInputSave.jsp" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divProject);">
					</td>
					<td class=titleImg>
						汇交件投保人查询
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						姓名
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=Name  verify="姓名|notnull&len<=600">
					</TD>
					<TD class=title>
						性别
					</TD>
					<TD class=input>
						<Input class=codeNo name=Sex  verify="性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input class=codename name=SexName readonly=true >    
					</TD>
					<TD class=title>
						出生日期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="Birthday"  verify="出生日期|notnull">
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						证件类型
					</TD>
					<TD class=input>
						<Input class=codeNo name="IDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1]);"><input class=codename name=IDTypeName readonly=true >    
					</TD>
					<TD class=title>
						证件号码
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=IDNo verify="项目名称|notnull&len<=600">
					</TD>

					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
				</TR>
			</table>
			<table align='center'>
				<input type=hidden id="fmtransact" name="fmtransact">
				<input type=hidden id="GSManageCom" name="GSManageCom">
				<td class=button width="10%">
					<input type="button" class=cssButton value=" 查 询 " name=query onclick="queryInfo()">
					<input type="button" class=cssButton value=" 退  出 " name=insert22 onclick="returnParentone()">
				</td>
			</table>

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divProjectUW1);">
					</td>
					<td class=titleImg>
						投保人信息
					</td>
				</tr>
			</table>

			<Div id="divProjectUW1" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanAppntGrid"></span>
						</td>
					</tr>
				</table>
			</div>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divProjectUW1);">
					</td>
					<td class=titleImg>
						投保人信息修改
					</td>
				</tr>
			</table>
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						姓名
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=AppntName elementtype="nacessary" verify="项目名称|notnull&len<=600">
					</TD>
					<TD class=title>
						性别
					</TD>
					<TD class=input>
						<Input class=codeNo name=AppntSex  verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true elementtype=nacessary>    
					</TD>
					<TD class=title>
						出生日期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="AppntBirthday"  elementtype="nacessary" verify="出生日期|notnull">
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						证件类型
					</TD>
					<TD class=input>
						<Input class=codeNo name="AppntIDType" verify="证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1]);"><input class=codename name=AppntIDTypeName readonly=true elementtype=nacessary>    
					</TD>
					<TD class=title>
						证件号码
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=AppntIDNo elementtype="nacessary" verify="证件号码|notnull&len<=30">
					</TD>

					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
				</TR>
			</table>
			<input type="button" class=cssButton value=" 修 改 " name=updateApp onclick="updateinfo()">
            <!-- 隐藏域 -->
            <input type="hidden" name="GrpContNo" />
            <input type="hidden" name="InsuredId" />
            <input type="hidden" name="fmAction" />
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
