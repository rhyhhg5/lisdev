//程序功能：
//创建日期：2008-1-29 03:48下午
//创建人  ：YangYalin
//更新记录：更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();

var tDefaultStartDate;

/*********************************************************************
 *  描述:查询外包批次信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryBatchList()
{
  if(!verifyInput2())
  {
    return false;
  }
  
  if(!checkData())
  {
    return false;
  }
  
	var sql = "select a.BatchNo, a.SendOutDate, a.SendOutTime, a.DocNumber, "
	        + "   a.ReceiveDate, a.ReceiveTime, a.DocNumberBack, "
	        + "   a.State, CodeName('bpobatchstate', a.State) "
	        + "from BPOBatchInfo a "
	        + "where 1 = 1 "
	        + "   and MakeDate >= '" + fm.StartDate.value + "' "
	        + "   and MakeDate <= '" + fm.EndDate.value + "' "
	        + getWherePart("a.BatchNo", "BPOBatchNo")
	        + getWherePart("a.State", "State")
	        + (fm.PrtNo.value == "" ? "" : "  and exists(select 1 from BPOMissionState where BPOBatchNo = a.BatchNo and BussNo = '" + fm.PrtNo.value + "') ")
	        + "order by a.BatchNo desc ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, BatchGrid);

  return true;
}

//初始化查询条件
function initInputBox()
{
  fm.ManageCom.value = tManageCom;
  fm.EndDate.value = tCurrentDate;
  
  var sql = "select date('" + tCurrentDate + "') - 3 month from dual ";
  tDefaultStartDate = easyExecSql(sql);
  fm.StartDate.value = tDefaultStartDate;
}

//校验是否可以进行查询操作
function checkData()
{
  var sql = "select 1 from Dual "
          + "where date('" + fm.EndDate.value + "') - 3 month > date('" + fm.StartDate.value + "') ";
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] == "1")
  {
    if(!confirm("时间段长度超过两个月，数据较多，查询可能较慢，继续？"))
    {
      return false;
    }
  }
  
  return true;
}

//查询选中批次的投保单信息
function queryContInfo()
{
  var tBPOBatchNo = BatchGrid.getRowColDataByName(BatchGrid.getSelNo() - 1, "BPOBatchNo");
  
	var sql = "select a.BPOBatchNo, a.BussNo, a.MissionID, a.ManageCom, CodeName('bpomissionstate', a.State), '' "
	        + "from BPOMissionState a "
	        + "where BPOBatchNo = '" + tBPOBatchNo + "' "
	        //+ "   and State = '02' "
	        //+ "   and MakeDate >= '" + fm.StartDate.value + "' "
	        //+ "   and MakeDate <= '" + fm.EndDate.value + "' "
	        //+ "   and ManageCom like '" + tManageCom + "%' "
	        //+ "   and ManageCom like '" + fm.ManageCom.value + "%' "
	        //+ "   and not exists(select 1 from LCCont where PrtNo = a.BussNo and ContType = '1')"
	        //+ getWherePart("a.BussNo", "PrtNo")
	        //+ getWherePart("a.BPOBatchNo", "BPOBatchNo")
	        + "order by MakeDate, MakeTime ";
	turnPage3.pageDivName = "divPage3";
	turnPage3.queryModal(sql, ContGrid);
	
  setErrorInfo();

  return true;
}

//显示导入错误信息
function setErrorInfo()
{
  for(var i = 0; i < ContGrid.mulLineCount; i++)
  {
    var sql = "select a.ErrorInfo "
            + "from LCGrpImportLog a "
            + "where a.PrtNo = '" + ContGrid.getRowColDataByName(i, "PrtNo") + "' "
            + "order by a.MakeDate desc, a.MakeTime desc ";
    
    var rs = easyExecSql(sql);
    if(rs)
    {
      errorInfo = rs[0][0];  //只取最后一次导入日志
      ContGrid.setRowColDataByName(i, "ErrorInfo", errorInfo);
    }
  }
}