var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartSignDate = fm.StartSignDate.value;
    var tEndSignDate = fm.EndSignDate.value;

	if(dateDiff(tStartSignDate, tEndSignDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}
	var SaleChnlSQL="";
	var tSaleChnl1=fm.SaleChnl.value;
	var tSaleChnl2=fm.SaleChnl2.value;
	if(tSaleChnl1!=''||tSaleChnl2!=''){
	  if(tSaleChnl1!=''&&tSaleChnl2!=''){
	     SaleChnlSQL=" and lcc.SaleChnl in ('"+tSaleChnl1+"','"+tSaleChnl2+"') ";
	  }else if(tSaleChnl1!=''){
	    SaleChnlSQL=" and lcc.SaleChnl in ('"+tSaleChnl1+"') ";
	  }else{
	    SaleChnlSQL=" and lcc.SaleChnl in ('"+tSaleChnl2+"') ";
	  }
	   
	}
	var tContainsPAD="";
	if(fm.ContainsPAD.value=="" || fm.ContainsPAD.value =="0"){
		tContainsPAD="";
	}else if(fm.ContainsPAD.value == "1"){
		tContainsPAD=" and lcc.prtno like 'PD%' ";
	}else if(fm.ContainsPAD.value == "2"){
		tContainsPAD=" and lcc.prtno not like 'PD%' ";
	}
	
    var tStrSQL = ""
        + " select "
        + " tmp.ComCode,(select name from ldcom where comcode=tmp.comcode),"
        + " tmp.ManageCom, tmp.ManageComName, tmp.CardType, tmp.ContNo, tmp.PrtNo, "
        + " tmp.SaleChnl, getUniteCode(tmp.AgentCode), tmp.AgentName, tmp.BranchAttr, tmp.BranchName, "
        + " tmp.AppntName, tmp.AppntBirthday, tmp.AppntSex, tmp.AppntIdType, "
        + " tmp.AppntIdNo, tmp.AppntNativePlace, tmp.AppntMarriage, tmp.AppntOccupationType, "
        + " tmp.AppntOccupationCode, tmp.CValidate, tmp.CInValidate, "
        + " tmp.PolApplyDate, tmp.InputDate, tmp.ApproveDate, tmp.UWDate, tmp.SignDate, "
        + " tmp.CustomGetPolDate, tmp.GetPolDate, tmp.PayMode, "
        + " sum(tmp.Prem) AllPrem, sum(tmp.Amnt) AllAmnt, count(distinct tmp.InsuredNo) InsuCount, "
        + " sum(tmp.AllPolPrem) AllContPrem, "
        + " (case when tmp.ContAppFlag is null then '银保通撤单' else tmp.ContAppFlag end) ContAppFlag,tmp.ScanFlag,tmp.ScanDate "
        + " from "
        + " ( "
        + " select "
        + " substr(lcc.ManageCom,1,4) Comcode,"
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " (case when 1=(select count(1) from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl ) then CodeName('lcsalechnl', lcc.SaleChnl) " 
        + " else CodeName('salechnl', lcc.SaleChnl) end ) SaleChnl, "
        + " lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " labg.BranchAttr, labg.Name BranchName, "
        + " lca.AppntName AppntName, lca.AppntBirthday AppntBirthday, "
        + " CodeName('sex', lca.AppntSex) AppntSex, "
        + " CodeName('idtype', lca.IdType) AppntIdType, "
        + " lca.IdNo AppntIdNo, "
        + " CodeName('nativeplace', lca.NativePlace) AppntNativePlace, "
        + " CodeName('marriage', lca.Marriage) AppntMarriage, "
        + " CodeName('occupationtype', lca.OccupationType) AppntOccupationType, "
        + " lca.OccupationCode AppntOccupationCode, "
        + " lcc.CValidate, lcc.CInValidate, "
        + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate, "
        + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.Prem, lcp.Amnt, lcp.InsuredNo, "
        + " (lcp.Prem + nvl(lcp.SupplementaryPrem, 0)) AllPolPrem, "
        + " '承保' ContAppFlag, "
        + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag, "
        + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate, "
        + " '' "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LCAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.AppFlag = '1' "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
        + SaleChnlSQL
        + tContainsPAD
        + " union all "
        + " select "
        + " substr(lcc.ManageCom,1,4) Comcode, "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " (case when 1=(select count(1) from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl ) then CodeName('lcsalechnl', lcc.SaleChnl) " 
        + " else CodeName('salechnl', lcc.SaleChnl) end ) SaleChnl, "
        + " lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " labg.BranchAttr, labg.Name BranchName, "
        + " lca.AppntName AppntName, lca.AppntBirthday AppntBirthday, "
        + " CodeName('sex', lca.AppntSex) AppntSex, "
        + " CodeName('idtype', lca.IdType) AppntIdType, "
        + " lca.IdNo AppntIdNo, "
        + " CodeName('nativeplace', lca.NativePlace) AppntNativePlace, "
        + " CodeName('marriage', lca.Marriage) AppntMarriage, "
        + " CodeName('occupationtype', lca.OccupationType) AppntOccupationType, "
        + " lca.OccupationCode AppntOccupationCode, "
        + " lcc.CValidate, lcc.CInValidate, "
        + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate, "
        + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.Prem, lcp.Amnt, lcp.InsuredNo, "
        + " (lcp.Prem + nvl(lcp.SupplementaryPrem, 0)) AllPolPrem, "
        + " '承保' ContAppFlag, "
        + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag, "
        + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate, "
        + " '' "
        + " from LCCont lcc "
        + " inner join LBPol lcp on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LBAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.AppFlag = '1' "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
        + SaleChnlSQL
        + tContainsPAD
        + " union all "
        + " select "
        + " substr(lcc.ManageCom,1,4) Comcode, "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " (case when 1=(select count(1) from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl ) then CodeName('lcsalechnl', lcc.SaleChnl) " 
        + " else CodeName('salechnl', lcc.SaleChnl) end ) SaleChnl, "
        + " lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " labg.BranchAttr, labg.Name BranchName, "
        + " lca.AppntName AppntName, lca.AppntBirthday AppntBirthday, "
        + " CodeName('sex', lca.AppntSex) AppntSex, "
        + " CodeName('idtype', lca.IdType) AppntIdType, "
        + " lca.IdNo AppntIdNo, "
        + " CodeName('nativeplace', lca.NativePlace) AppntNativePlace, "
        + " CodeName('marriage', lca.Marriage) AppntMarriage, "
        + " CodeName('occupationtype', lca.OccupationType) AppntOccupationType, "
        + " lca.OccupationCode AppntOccupationCode, "
        + " lcc.CValidate, lcc.CInValidate, "
        + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate, "
        + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.Prem, lcp.Amnt, lcp.InsuredNo, "
        + " (lcp.Prem + nvl(lcp.SupplementaryPrem, 0)) AllPolPrem, "
        + " (select (case lpei.EdorType when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' end) from LPEdorItem lpei where lpei.ContNo = lcc.ContNo and lpei.EdorType in ('CT', 'WT', 'XT') order by lpei.EdorAppDate desc fetch first rows only) ContAppFlag, "
        + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag, "
        + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate, "
        + " '' "
        + " from LBCont lcc "
        + " inner join LBPol lcp on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LBAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.AppFlag = '1' "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
        + SaleChnlSQL
        + tContainsPAD
        + " ) as tmp "
        + " group by tmp.Comcode,tmp.ManageCom, tmp.ManageComName, tmp.CardType, tmp.ContNo, tmp.PrtNo, "
        + " tmp.SaleChnl, tmp.AgentCode, tmp.AgentName, tmp.BranchAttr, tmp.BranchName, "
        + " tmp.AppntName, tmp.AppntBirthday, tmp.AppntSex, tmp.AppntIdType, "
        + " tmp.AppntIdNo, tmp.AppntNativePlace, tmp.AppntMarriage, tmp.AppntOccupationType, "
        + " tmp.AppntOccupationCode, tmp.CValidate, tmp.CInValidate, "
        + " tmp.PolApplyDate, tmp.InputDate, tmp.ApproveDate, tmp.UWDate, tmp.SignDate, "
        + " tmp.CustomGetPolDate, tmp.GetPolDate, tmp.PayMode, "
        + " (case when tmp.ContAppFlag is null then '银保通撤单' else tmp.ContAppFlag end),tmp.ScanFlag,tmp.ScanDate "
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "ContCBDetailSave.jsp";
    fm.submit();
    fm.action = oldAction;

}