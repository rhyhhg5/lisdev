<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
    tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; 	//记录管理机构
    var comcode = "<%=tGI.ComCode%>";		//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <%@page import="com.sinosoft.utility.*"%>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <script src="GrpContAgentsaleUPInput.js"></script>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpContAgentsaleUPInit.jsp" %>
  <title>代理销售业务员信息维护</title>
</head>
<body onload="initForm();">
  <form action="./GrpContAgentsaleUPSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class=common border=0 width=100%>
      <tr>
	    <td class=titleImg align=center>请输入保单查询条件：</td>
      </tr>
    </table>
    <table class="common" align=center>
      <tr class="common">
        <td class="title">管理机构</td>
        <td class="input"><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true ></td>
        <td class="title">印刷号</td>
        <td class="input"><input class="common" name="PrtNo" /></td>
        <td class="title">保单合同号</td>
        <td class="input"><input class="common" name="ContNo" /></td>
	  </tr>
    </table>
    <input class="cssButton" type="button" value="查询保单" onclick="easyQueryClick();" />
    <table>
      <tr>
        <td class=common>
          <IMG src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divContGrid);">
        </td>
        <td class=titleImg>保单信息</td>
      </tr>
    </table>
    <div id="divContGrid" style= "display: ''">
      <table class=common>
        <tr class=common>
          <td text-align: left colSpan=1>
            <span id="spanSetGrid" ></span>
          </td>
        </tr>
      </table>
      <table class=common>
        <INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
        <INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
        <INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
        <INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
      </table>
    </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    <table class= common align=center>
      <tr class=common>
        <td class=common>
	      <input type=button class=cssButton value="业务员编号查询" style="width:120" onclick="queryAgentSaleCode()">
	    </td>
	    <td class=title>代理销售业务员编号</td>
	    <td class=input width="20%">
	      <input class=common name=AgentSaleCode>
	    </td>
	    <td class=title>代理销售业务员姓名</td>
	    <td class=input>
	      <input class=common name=AgentSaleName readonly>
	    </td>
	    <td class=title></td>
	  </tr>
    </table> 
    <table class= common>
    <INPUT VALUE="开始更新" name=updatebt class="cssButton" TYPE=button onclick="Gotoupdate();">
    </table>
  </form>
</body>
</html>