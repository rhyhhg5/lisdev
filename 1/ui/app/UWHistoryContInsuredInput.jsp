<%
//程序名称：TempFeeInput.jsp
//程序功能：财务收费的输入
//创建日期：2002-07-12 
//创建人  ：
//更新记录：  更新人     更新日期     更新原因/内容
%>
<!--%@include file="../common/jsp/UsrCheck.jsp"%-->
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<script>
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var prtNo = "<%=request.getParameter("prtNo")%>";
	if (prtNo == null)
	    prtNo="";	
	var checktype = "<%=request.getParameter("checktype")%>";	    
	var scantype = "<%=request.getParameter("scantype")%>";
	var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
	if (ScanFlag == "null") ScanFlag = "1";
	var LoadFlag = "<%=request.getParameter("LoadFlag")%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
	if (LoadFlag == null)
	    Loadflag=1
        var ContType = "<%=request.getParameter("ContType")%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
	if (ContType == "null")
	    ContType=1
	var Auditing = "<%=request.getParameter("Auditing")%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
	if (Auditing == "null")
	    Auditing=0;
	var ActivityID = "<%=request.getParameter("ActivityID")%>";
	var MissionID = "<%=request.getParameter("MissionID")%>";
	var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
	var AppntNo = "<%=request.getParameter("AppntNo")%>";
	var AppntName = "<%=request.getParameter("AppntName")%>";
	var BQFlag = "<%=request.getParameter("BQFlag")%>";	
	var EdorType = "<%=request.getParameter("EdorType")%>";
	var SaleChnl = "<%=request.getParameter("SaleChnl")%>";	
	var SaleChnlDetail = "<%=request.getParameter("SaleChnlDetail")%>";	
	var oldContNo = "<%=request.getParameter("oldContNo")%>";
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var GrpContNo = "<%=request.getParameter("polNo")%>";
	var Resource = "<%=request.getParameter("Resource")%>";
	var param="";	
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>	
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="ProposalAutoMove.js"></SCRIPT> 	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="ContHistoryInsuredLCImpart.js"></SCRIPT> <!--保障状况告知 ＆ 健康状况告知 引用函数-->
  <SCRIPT src="UWHistoryContInsuredInput.js"></SCRIPT>
  <%@include file="UWHistoryContInsuredInit.jsp"%>
  <%if(request.getParameter("scantype")!=null&&request.getParameter("scantype").equals("scan")){%>  
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  <%}%>   
</head>
<body  onload="initForm();initElementtype();" >
<Form action="" method=post name=fm target="fraSubmit">
<input type=hidden id="fmAction" name="fmAction">
<input type=hidden id="ContType" name="ContType">
<input type=hidden id="GrpContNo" name="GrpContNo">

    <Div  id= "divFamilyType" style="display:'none'">
    <table class=common>
       <tr class=common>
          <TD  class= title>
            个人合同类型
          </TD>
          <TD  class= input>
            <Input class="code" name=FamilyType value="1" ondblclick="showCodeList('FamilyType',[this]);" onkeyup="return showCodeListKey('FamilyType',[this]);" onfocus="choosetype();">
          </TD>
  <!--  			<TD  class= title>
    			  投保单号码
    			</TD>-->
    			  <Input class="common" readonly name=ProposalContNo  type="hidden">
       </tr>
    </table>
    </Div>
    <Input type= "hidden" class="common" readonly name=ContNo >       
    <DIV id="divPrtNo" style="display:'none'">
    	<table class=common>
    		<TR>
    			<TD  class= title>
    			  印刷号码
    			</TD>
    			<TD  class= input>
    			  <Input class="common" readonly name=PrtNo verify="印刷号码|notnull" >
    			</TD>
     			<TD  class= title>
    			</TD>
    			<TD  class= input>
    			</TD>   			
    		</TR>
    	</table>
    </DIV>
    <Div  id= "divTempFeeInput" style= "display: ''">
    	<Table  class= common>
    		<tr>
    			<td text-align: left colSpan=1>
	  				<span id="spanInsuredGrid" >
	  				</span> 
	  			</td>
    		</tr>
    	</Table>
    </div>

   <%@include file="ComplexInsured.jsp"%>      
     
  <!--table class=common>
   <TR  class= common> 
      <TD  class= title> 特别约定及备注 </TD>
    </TR>
    <TR  class= common>
      <TD  class= title>
      <textarea name="GrpSpec" cols="120" rows="3" class="common" >
      </textarea></TD>
    </TR>
  </table-->  
	  
<DIV id=DivLCImpart STYLE="display:''">
<!-- 告知信息部分（列表） -->
<div  id= "divLCImpart10" style= "display: 'none'">
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart0);">
</td>
<td class= titleImg>
保障状况告知
</td>
</tr>
</table>
</div>
<div  id= "divLCImpart0" style= "display: 'none'">

<table class = common>
	<tr>
		<td>1.您目前年收入 <input name="Impartbox1" class = "common6"> 万元，主要来源为：<input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 薪资 <input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 营业收入 <input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 房屋出租 <input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 证券投资 <input type="checkbox" name="Impartbox1" class="box">&nbsp&nbsp 银行利息 &nbsp&nbsp 其它 <input name="Impartbox1" class = "common6"></td>
	</tr>
	<tr>
		<td>2.目前您医疗费用支付方式: <input type="checkbox" name="Impartbox2" class="box">&nbsp&nbsp 社会医疗保险 <input type="checkbox" name="Impartbox2" class="box">&nbsp&nbsp 商业医疗保险 <input type="checkbox" name="Impartbox2" class="box">&nbsp&nbsp 自费 &nbsp&nbsp 其它 <input name="Impartbox2" class = "common6"></td>
	</tr>
	<tr>
		<td>3.您目前是否有正在生效的商业人身保险（医疗保险，重大疾病保险，意外险或寿险）产品？<input type="checkbox" name="" class="box">&nbsp&nbsp 是 <input type="checkbox" name="" class="box">&nbsp&nbsp 否<input class="common7"></td> 
	</tr>
	<tr>
		<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox3" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox3" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox3" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate1" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate2" class = "coolDatePicker1" dateFormat="short"></td>
	</tr>
	<tr>
		<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox4" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox4" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox4" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate3" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate4" class = "coolDatePicker1" dateFormat="short"></td>
	</tr>
	<tr>
		<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox5" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox5" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox5" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate5" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate6" class = "coolDatePicker1" dateFormat="short"></td>
	</tr>
	<tr>
		<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox6" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox6" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox6" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate7" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate8" class = "coolDatePicker1" dateFormat="short"></td>
	</tr>
	<tr>
		<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox17" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox17" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox17" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate16" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate17" class = "coolDatePicker1" dateFormat="short"></td>
	</tr>
	<tr>
		<td>&nbsp&nbsp&nbsp&nbsp 公司 <input name="Impartbox18" class = "common6"> &nbsp&nbsp 险种 <input name="Impartbox18" class = "common6"> &nbsp&nbsp 保额/档次 <input name="Impartbox18" class = "common6">  &nbsp&nbsp 起止时间 <input name="ImpartGridDate18" class = "coolDatePicker1" dateFormat="short"> 至 <input name="ImpartGridDate19" class = "coolDatePicker1" dateFormat="short"></td>
	</tr>		
	<tr>		
		<td>4.过去三年内，有无人身险事宜被商业保险公司拒保，延期，加费，免责的投保经历或向保险公司剔出过理赔申请？<!-- <input type="checkbox" name="" class="box">&nbsp&nbsp 是 <input type="checkbox" name="" class="box">&nbsp&nbsp 否<input class="common7"></td>-->
	</tr>
	<tr>		
		<td>&nbsp&nbsp&nbsp&nbsp 时间 <input name="ImpartGridDate9" class = "coolDatePicker1" dateFormat="short"> &nbsp&nbsp  事由 <input name="Impartbox7" class = "common6"> &nbsp&nbsp 结果 <input name="Impartbox7" class = "common6"></td>
	</tr>
	<tr>		
		<td>&nbsp&nbsp&nbsp&nbsp 时间 <input name="ImpartGridDate10" class = "coolDatePicker1" dateFormat="short"> &nbsp&nbsp  事由 <input name="Impartbox8" class = "common6"> &nbsp&nbsp 结果 <input name="Impartbox8" class = "common6"></td>
	</tr>
</table>
</div>

<div  id= "divLCImpart1" style= "display: 'none'">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartGrid" >
</span>
</td>
</tr>
</table>
</div>

<DIV id=DivLCImpart STYLE="display:'none'">
<!-- 告知信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart2);">
</td>
<td class= titleImg>
健康状况告知
</td>
</tr>
</table>
</div>

<div  id= "divLCImpart2" style= "display: 'none'">
      <table width="942" border="0" align="center" cellpadding="0" cellspacing="0" class="muline">
        <tr>
		<td colspan="7" class="mulinetitle2" height="20">&nbsp;</td>
	</tr>
  <tr> 
    <td colspan="7" class="mulinetitle3">您是否有直接寄至我公司的健康告知表格&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp身高 <input name="Detail04" class = "common66"> 厘米(cm) &nbsp&nbsp&nbsp&nbsp 体重 <input name="Detail04" class = "common66"> 千克(kg)</td>
  </tr>
  <tr> 
    <td width="104" class="mulinetitle3">5.<input type="checkbox" name="Detail05" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail05" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="6" class="mulinetitle3">你是否目前或曾经有吸烟的习惯？&nbsp&nbsp&nbsp&nbsp如是，请回答&nbsp&nbsp&nbsp&nbsp目前(<input type="checkbox" name="Detail05" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail05" class="box">&nbsp&nbsp 否)吸烟&nbsp&nbsp每日吸烟量 <input name="Detail05" class = "common66"> 支&nbsp&nbsp吸烟持续时间 <input name="Detail05" class = "common66"> 年</td>
  </tr>
  <tr> 
    <td class="mulinetitle3">6.<input type="checkbox" name="Detail06" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail06" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="6" class="mulinetitle3">你是否目前或曾经有饮酒的习惯？&nbsp&nbsp&nbsp&nbsp如是，请回答&nbsp&nbsp&nbsp&nbsp目前(<input type="checkbox" name="Detail06" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail06" class="box">&nbsp&nbsp 否)饮酒&nbsp&nbsp种类 <input name="Detail06" class = "common66"> &nbsp&nbsp量 <input name="Detail06" class = "common66"> ml/周 &nbsp&nbsp 时间 <input name="Detail06" class = "common66"> 年</td>
  </tr>
  <tr> 
    <td class="mulinetitle3">7.<input type="checkbox" name="Detail07" class="box" onclick="InputDetailToMuline07();">&nbsp&nbsp 是 <input type="checkbox" name="Detail07" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="3" class="mulinetitle3">您在过去五年内有无进行危险运动或不良嗜好？</td>
    <td width="101" class="mulinetitle3">15.<input type="checkbox" name="Detail15" class="box" onclick="InputDetailToMuline15();">&nbsp&nbsp 是 <input type="checkbox" name="Detail15" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="2" class="mulinetitle3">您在最近三个月中是否有发热、疼痛、大小便异常或者其他不适症状？</td>
  </tr>
  <tr> 
    <td class="mulinetitle3">8.<input type="checkbox" name="Detail08" class="box" onclick="InputDetailToMuline08();">&nbsp&nbsp 是 <input type="checkbox" name="Detail08" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="3" class="mulinetitle3">您过去五年内是否曾住院检查或治疗(包括疗养院、康复医院)？</td>
    <td class="mulinetitle3">16.<input type="checkbox" name="Detail16" class="box" onclick="InputDetailToMuline16();">&nbsp&nbsp 是 <input type="checkbox" name="Detail16" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="2" class="mulinetitle3">您是否接受过HIV（艾滋病）病毒检测并为阳性？</td>
  </tr>
  <tr> 
    <td class="mulinetitle3">9.<input type="checkbox" name="Detail09" class="box" onclick="InputDetailToMuline09();">&nbsp&nbsp 是 <input type="checkbox" name="Detail09" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="3" class="mulinetitle3">您过去五年内是否做过手术（包括门诊手术）？</td>
    <td rowspan="2" class="mulinetitle3">17.<input type="checkbox" name="Detail17" class="box" onclick="InputDetailToMuline17();">&nbsp&nbsp 是 <input type="checkbox" name="Detail17" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="2" rowspan="2" class="mulinetitle3">您是否有其他以上未提及的疾病、残疾及器官缺失或功能不全？</td>
  </tr>
  <tr> 
    <td class="mulinetitle3">10.<input type="checkbox" name="Detail10" class="box" onclick="InputDetailToMuline10();">&nbsp&nbsp 是 <input type="checkbox" name="Detail10" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="3" class="mulinetitle3">您过去五年内是否接受过心理或药物成瘾性治疗?</td>
  </tr>
  <tr> 
    <td class="mulinetitle3">11.<input type="checkbox" name="Detail11" class="box" onclick="InputDetailToMuline11();">&nbsp&nbsp 是 <input type="checkbox" name="Detail11" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="3" class="mulinetitle3">您在过去三年内是否因疾病而持续治疗超过2周？</td>
    <td rowspan="2" class="mulinetitle3">18.<input type="checkbox" name="Detail18" class="box" onclick="InputDetailToMuline218();">&nbsp&nbsp 是 <input type="checkbox" name="Detail18" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="2" rowspan="2" class="mulinetitle3">(2周岁以下儿童回答)&nbsp&nbsp出生体重 <input name="Detail18" class = "common66"> 公斤是否有新生儿窒息、产伤、先天性疾病、发育迟缓等？</td>
  </tr>
  <tr> 
    <td class="mulinetitle3">12.<input type="checkbox" name="Detail12" class="box" onclick="InputDetailToMuline12();">&nbsp&nbsp 是 <input type="checkbox" name="Detail12" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="3" class="mulinetitle3">最近一年内您是否参加身体检查并发现结果异常?</td>
  </tr>
  <tr> 
    <td class="mulinetitle3">13.<input type="checkbox" name="Detail13" class="box" onclick="InputDetailToMuline13();">&nbsp&nbsp 是 <input type="checkbox" name="Detail13" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="3" class="mulinetitle3">您目前或过去三月内是否使用过药物？</td>
    <td rowspan="2" class="mulinetitle3">19.<input type="checkbox" name="Detail19" class="box"  onclick="InputDetailToMuline219();">&nbsp&nbsp 是 <input type="checkbox" name="Detail19" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="2" rowspan="2" class="mulinetitle3">是否怀孕？如您目前怀孕，请告知怀孕 <input name="Detail19" class = "common66"> 月</td>
  </tr>
  <tr> 
    <td class="mulinetitle3">14.<input type="checkbox" name="Detail14" class="box"  onclick="InputDetailToMuline14();">&nbsp&nbsp 是 <input type="checkbox" name="Detail14" class="box">&nbsp&nbsp 否<input class="common7"></td>
    <td colspan="3" class="mulinetitle3">您过去三个月内是否有过医院门诊检查或治疗？</td>
  </tr>
  <tr> 
    <td colspan="7" class="mulinetitle3">在7－19问题中如果有回答“是”的，请提供相关证明并详细告知(如同一时间涉及多个问题，可一并回答)<input class="common7"></td>
  </tr>
</table>
<br>
<!--table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartDetailGrid1" >
</span>
</td>
</tr>
</table-->
</DIV>

<DIV id=DivChangeLCImpart STYLE="display:''">
<!-- 告知信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart3);">
</td>
<td class= titleImg>
被保人告知
</td>
</tr>
</table>
</div>

<div  id= "divLCImpart3" style= "display: ''">
      <table width="942" border="0" align="center" cellpadding="0" cellspacing="0" class="muline">
  <tr>
		<td colspan="7" class="mulinetitle2" height="20">&nbsp;</td>
	</tr>
  <tr> 
    <td colspan="7" class="mulinetitle3">1.被保险人&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp身高 <input name="Detail101" class = "common66"> 厘米(cm) &nbsp&nbsp&nbsp&nbsp 体重 <input name="Detail101" class = "common66"> 千克(kg)</td>
  </tr>
  <tr> 
    <td colspan="7"  class="mulinetitle3">2.被保险人的医疗费用支付方式？&nbsp&nbsp&nbsp&nbsp<input type="checkbox" name="Detail102" class="box">&nbsp&nbsp 公费医疗 <input class="common7"> <input type="checkbox" name="Detail102" class="box">&nbsp&nbsp 社会医疗保险 <input class="common7"> <input type="checkbox" name="Detail102" class="box">&nbsp&nbsp 商业医疗保险 <input class="common7"> <input type="checkbox" name="Detail102" class="box">&nbsp&nbsp 自费 <input class="common7"> </td>
  </tr>
  <tr> 
    <td colspan="7" class="mulinetitle3">3.是否有吸烟习惯？&nbsp&nbsp&nbsp&nbsp<input type="checkbox" name="Detail103" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail103" class="box">&nbsp&nbsp 否,&nbsp&nbsp每日吸烟量 <input name="Detail103" class = "common66"> 支&nbsp&nbsp烟龄 <input name="Detail103" class = "common66"> 年</td>
  </tr>  
  <tr> 
    <td colspan="7" class="mulinetitle3">4.是否有饮酒习惯？&nbsp&nbsp&nbsp&nbsp<input type="checkbox" name="Detail104" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail104" class="box">&nbsp&nbsp 否,&nbsp&nbsp种类 <input type="checkbox" name="Detail104" class="box">&nbsp&nbsp 白酒 <input type="checkbox" name="Detail104" class="box">&nbsp&nbsp 啤酒  <input type="checkbox" name="Detail104" class="box">&nbsp&nbsp 葡萄酒 <input type="checkbox" name="Detail104" class="box">&nbsp&nbsp 黄酒  &nbsp&nbsp每日饮酒量 <input name="Detail104" class = "common66"> 两 &nbsp&nbsp 酒龄 <input name="Detail104" class = "common66"> 年</td>
  </tr>
  <tr> 
  <tr> 
    <td colspan="6" class="mulinetitle3">5.是否有正在生效的商业人身保险？&nbsp&nbsp&nbsp&nbsp</td>
    <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail105" class="box" onclick="InputDetailToMuline105();"> &nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail105" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">6.是否有人身保险被商业保险公司拒保，延期，加费，免责的投保经历或向保险公司提出过理赔申请？&nbsp&nbsp&nbsp&nbsp</td>
    <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail106" class="box" onclick="InputDetailToMuline106();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail106" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">7.有无危险运动爱好？&nbsp&nbsp&nbsp&nbsp</td>
    <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail107" class="box" onclick="InputDetailToMuline107();" >&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail107" class="box">&nbsp&nbsp 否 <input class="common7"> </td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">8.过去十年内是否曾住院检查或治疗（包括疗养院、康复医院等医疗机构）？&nbsp&nbsp&nbsp&nbsp</td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail108" class="box" onclick="InputDetailToMuline108();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail108" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">9.过去十年内是否做过手术（包括门诊手术）？&nbsp&nbsp&nbsp&nbsp</td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail109" class="box" onclick="InputDetailToMuline109();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail109" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">10.过去十年内是否接受过心理或药物成瘾性治疗? &nbsp&nbsp&nbsp&nbsp</td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail110" class="box" onclick="InputDetailToMuline110();" >&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail110" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">11.过去两年内您是否参加身体检查并发现结果异常？&nbsp&nbsp&nbsp&nbsp</td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail111" class="box" onclick="InputDetailToMuline111();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail111" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">12.过去一年内是否有过医院门诊检查或治疗？&nbsp&nbsp&nbsp&nbsp</td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail112" class="box" onclick="InputDetailToMuline112();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail112" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">13.过去一年内是否是否使用过药物？&nbsp&nbsp&nbsp&nbsp</td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail113" class="box" onclick="InputDetailToMuline113();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail113" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">14.过去一年中是否有发热、疼痛、大小便异常、体重明显变化或者其他不适症状？&nbsp&nbsp&nbsp&nbsp</td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail114" class="box" onclick="InputDetailToMuline114();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail114" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">15.是否有其他未告知的疾病、残疾、器官缺失或功能不全、先天性或遗传性疾病？&nbsp&nbsp&nbsp&nbsp</td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail115" class="box" onclick="InputDetailToMuline115();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail115" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">16.父母兄弟姐妹是否有高血压、糖尿病、恶性肿瘤、心脏病、高脂血症、肾衰竭、中风、多囊肾等疾病？&nbsp&nbsp&nbsp&nbsp</td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail116" class="box" onclick="InputDetailToMuline116();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail116" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr> 
    <td colspan="6" class="mulinetitle3">17.(3周岁以下儿童回答) 是否有新生儿窒息、早产儿、产伤、出生体重异常、发育迟缓等？&nbsp&nbsp&nbsp&nbsp </td>
  <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail117" class="box" onclick="InputDetailToMuline117();" >&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail117" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  </tr>
  <tr>
    <td colspan="6" class="mulinetitle3">18.(16周岁以上女性回答) 是否怀孕？如是请告知妊娠周数以及是否有妊娠并发症</td>	 
    <td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail118" class="box" onclick="InputDetailToMuline118();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail118" class="box" >&nbsp&nbsp 否 <input class="common7"><input name="Detail118" class = "common66" type="hidden"></td>
  </tr>
  <tr>
    <td colspan="7" class="mulinetitle3">19.(18周岁以下未成年人回答)是否已拥有正在生效的以死亡为给付保险金条件的人身保险？如有请告知身故保险金额总额。&nbsp&nbsp&nbsp&nbsp<input type="checkbox" name="Detail119" class="box" onclick="InputDetailToMuline119();">&nbsp&nbsp 是 <input type="checkbox" name="Detail119" class="box" onclick="ImpartCheck119Radio2();">&nbsp&nbsp 否,&nbsp&nbsp保额 <input name="Detail119" class = "common66" > 万元</td>
  </tr>
  <tr> 
    <td colspan="7" class="mulinetitle3">在5－19问题中如果有回答“是”的，请填写告知明细并提供相关资料证明(如同一事件涉及多个告知项目，可一并回答)<input class="common7"></td>
  </tr>
</table>
<br>
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartDetailGrid" >
</span>
</td>
</tr>
</table>
<div id= "divAddDelButton" style= "display: ''" align=right>
</DIV>
<hr/>
<!-- 被保人险种信息部分 -->
<DIV id=DivLCPol STYLE="display:''">
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
</td>
<td class= titleImg>
被保险人险种信息
</td>
</tr>
</table>

<div  id= "divLCPol1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanPolGrid" >
</span>
</td>
</tr>
</table>
</div>
</DIV>
 <hr>
    <Div  id= "divGrpInputContButton" style= "display: 'none'" style="float: left"> 	
      <INPUT class=cssButton id="riskbutton1" VALUE="上一步" TYPE=button onclick="returnparent();">        
   <HR align="LEFT" size="2" width="100%" > 
    </DIV>
     <Div  id= "divGrpInputoverButton" style= "display: 'none'" style="float: right"> 
      </Div>
      
    <Div  id= "divInputContButton" style= "display: ''" style="float: left"> 	
    <INPUT class=cssButton id="riskbutton4" VALUE="问题件录入" TYPE=button onClick="QuestInput();">
      <INPUT class=cssButton id="riskbutton1" VALUE="上一步" TYPE=button onclick="returnparent();">        
    </DIV>
    <DIV id = "divApproveContButton1" style = "display:'none'" style="float: left">
     <table class=common>
    		<tr class=common>
    			<td class=common align="left">
    </td>     
    <td class=common>
		      <INPUT class=cssButton id="riskbutton5" VALUE="上一步" TYPE=button onclick="returnparent();">  
		 </td>
		 <td class=common align="right">
		  </td>	
		 </tr>
	 </table>
    </DIV>      
    <DIV id = "divApproveModifyContButton" style = "display:'none'" style="float: left">
      <INPUT class=cssButton id="riskbutton9" VALUE="上一步" TYPE=button onclick="returnparent();"> 
    </DIV>
    <DIV id = "divQueryButton" style = "display:'none'" style="float: left">
      <INPUT class=cssButton id="riskbutton9" VALUE="上一步" TYPE=button onclick="returnparent();"> 
    </DIV>	
         <DIV id = "divApproveContButton" style = "display:'none'" style="float: left">
     <DIV id = "divCheckInsuredButton" style = "display:''" style="float: left">
     </DIV> 
    </DIV>         
      <div id="autoMoveButton" style="display: none">
	<input type="button" name="autoMoveInput" value="随动定制确定" onclick="submitAutoMove(''+param+'');" class=cssButton>
        <INPUT class=cssButton id="riskbutton9" VALUE="定制投保人页面" TYPE=button onclick="returnparent();"> 	
	<input type="button" name="AutoMovePerson" value="定制第二被保险人" onclick="AutoMoveForNext();" class=cssButton>
	<input type="button" name="Next" value="定制险种页面" onclick="location.href='ProposalInput.jsp?LoadFlag='+LoadFlag+'&prtNo='+prtNo+'&scantype='+scantype+'&checktype='+checktype" class=cssButton>		
        <input type=hidden id="" name="autoMoveFlag">
        <input type=hidden id="" name="autoMoveValue"> 
        <input type=hidden id="" name="pagename" value="">      
                
      </div>          
	<input type=hidden id="WorkFlowFlag" name="WorkFlowFlag">
	<INPUT  type= "hidden" class= Common name= MissionID value= ""><!-- 工作流任务编码 -->
  <INPUT  type= "hidden" class= Common name= SubMissionID value= "">
  <INPUT  type= "hidden" class= Common name= ProposalGrpContNo value= "">
  <INPUT  type= "hidden" class= Common name= AppntNo value= "">
  <INPUT  type= "hidden" class= Common name= AppntName value= "">
  <INPUT  type= "hidden" class= Common name= SelPolNo value= "">
  <INPUT  type= "hidden" class= Common name= SaleChnl value= "">
  <INPUT  type= "hidden" class= Common name= ManageCom value= "">
  <INPUT  type= "hidden" class= Common name= AgentCode value= "">
  <INPUT  type= "hidden" class= Common name= AgentGroup value= "">
  <INPUT  type= "hidden" class= Common name= CValiDate value= "">
  <input type=hidden name=BQFlag>
   <input type=hidden  name=Resource> 
</Form> 
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>