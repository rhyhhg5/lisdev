//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function signGrpPol()
{
	var tSel = GrpGrid.getSelNo();
	var cPolNo = "";
	var tMissionID ="";
	var tSubMissionID = "";
	var tGrpContNo = "";
	fm.all("workType").value = "";
	if( tSel != null && tSel != 0 )
		cPolNo = GrpGrid.getRowColData( tSel - 1, 2 );
	if( cPolNo == null || cPolNo == "" )
		alert("请选择一张集体投保单后，再进行签单操作");
	else
	{
		//转账支票确认是否到账确认
		var prtno = GrpGrid.getRowColData(tSel-1,2);
		if(!isConfirm(prtno))
			return false;
		
		var tSRSQL = "select lcp.riskcode from lcpol lcp where lcp.prtno = '" + prtno + "' "
		        + "and lcp.riskcode in (select code from ldcode where codetype = 'stoprisk') "
		        + "and current date >= (select CODEALIAS from ldcode where codetype = 'stoprisk' and code = lcp.riskcode) ";
		var arrSRResult = easyExecSql(tSRSQL);
		if (arrSRResult!=null) {
		 alert("险种"+arrSRResult[0][0]+"已停售，不能签单！");
		 return false;
		}
		
		var tLock = "select 1 from  lcurgeverifylog where SerialNo = '" + prtno + "' ";
		var arrLockResult = easyExecSql(tLock);
		if (arrLockResult!=null) {
			var strSql = "select count(1) from lccont where appflag in ('1','9') and PrtNo='"+cPolNo+"' with ur";
			alert("该保单已锁定，请勿重复点击！保单正在签单过程中！已完成"+easyExecSql(strSql)+"个被保险人，请稍等");
			return;
		}
		
		var sql1 = " select count(1) from lccont where prtno='" + prtno + "' and uwflag='a' ";
		var	sql2 = " select count(1) from lccont where prtno='" + prtno + "'";
		var arr1 = easyExecSql(sql1);
		var arr2 = easyExecSql(sql2);
		if(arr1&&arr2){
			if(arr1[0][0]==arr2[0][0]){
				alert("团单下所有分单为撤销状态，签单失败！");
				return false;
			}
		}
		
		var strSql = "select * from ldsystrace where PolNo='" + cPolNo + "' and PolState=1006 ";
	  var arrResult = easyExecSql(strSql);
	  if (arrResult!=null && arrResult[0][1]!=Operator) {
	    alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
	    return;
	  }
	  //锁定该印刷号
	  tMissionID = GrpGrid.getRowColData( tSel - 1, 7 );
	  tSubMissionID = GrpGrid.getRowColData( tSel - 1, 8 );
	  tGrpContNo = GrpGrid.getRowColData( tSel - 1, 1 );
	  var strSql ="select ActivityStatus from lwmission where activityid='0000002006' and missionid='"+tMissionID+"' and submissionid='"+tSubMissionID+"' with ur";
	  var arr = easyExecSql(strSql);
		if(arr){
			if(arr[0][0]=="0"){
				var strSql = "select count(1) from lccont where appflag in ('1','9') and PrtNo='"+cPolNo+"' with ur";
				alert("该保单已锁定，请勿重复点击！保单正在签单过程中！已完成"+easyExecSql(strSql)+"个被保险人，请稍等");
				return;
			}
		}
	  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + cPolNo + "&CreatePos=承保签单&PolState=1006&Action=INSERT";
	  var a = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
		var i = 0;
		var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.action = "GroupPolSignSave.jsp";
		fm.submit(); //提交
	}
}

//大数据提交，保存按钮对应操作
function signBGrpPol()
{
    var tSel = GrpGrid.getSelNo();
    var cPolNo = "";
    var tMissionID ="";
    var tSubMissionID = "";
    var tGrpContNo = "";
    fm.all("workType").value = "";
    if( tSel != null && tSel != 0 )
        cPolNo = GrpGrid.getRowColData( tSel - 1, 2 );
    if( cPolNo == null || cPolNo == "" )
        alert("请选择一张集体投保单后，再进行签单操作");
    else
    {
        var strSql = "select * from ldsystrace where PolNo='" + cPolNo + "' and PolState=1006 ";
      var arrResult = easyExecSql(strSql);
      if (arrResult!=null && arrResult[0][1]!=Operator) {
        alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
        return;
      }
      //锁定该印刷号
      tMissionID = GrpGrid.getRowColData( tSel - 1, 7 );
      tSubMissionID = GrpGrid.getRowColData( tSel - 1, 8 );
      tGrpContNo = GrpGrid.getRowColData( tSel - 1, 1 );
      var strSql ="select ActivityStatus from lwmission where activityid='0000002007' and missionid='"+tMissionID+"' and submissionid='"+tSubMissionID+"' with ur";
      var arr = easyExecSql(strSql);
        if(arr){
            if(arr[0][0]=="0"){
                var strSql = "select count(1) from lccont where appflag in ('1','9') and PrtNo='"+cPolNo+"' with ur";
                alert("保单正在签单过程中！已完成"+easyExecSql(strSql)+"个被保险人，请稍等");
                return;
            }
        }
      var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + cPolNo + "&CreatePos=承保签单&PolState=1006&Action=INSERT";
      var a = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
        var i = 0;
        var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.action = "GroupPolSignSave.jsp?SubmitType=1";
        fm.submit(); //提交
    }
}

//提交，预打保单按钮对应操作
function priviewGrpPol()
{
	var tSel = GrpGrid.getSelNo();
	var cPolNo = "";
	fm.all("workType").value = "PREVIEW";
	if( tSel != null && tSel != 0 )
		cPolNo = GrpGrid.getRowColData( tSel - 1, 2 );

	if( cPolNo == null || cPolNo == "" )
		alert("请选择一张集体投保单后，再进行签单操作");
	else
	{   
        var verifySql ="select 1  from lcgrppol a where a.prtno='"+cPolNo+"' and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G') ";
        var arrVerifyResult = easyExecSql(verifySql);
        
        if(arrVerifyResult!=null &&arrVerifyResult[0][0]=="1"){
           alert('该保单为团体万能保单，不能预打保单');
           return;
        }
		var strSql = "select * from ldsystrace where PolNo='" + cPolNo + "' and PolState=1006 ";
	  var arrResult = easyExecSql(strSql);
	  if (arrResult!=null && arrResult[0][1]!=Operator) {
	    alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
	    return;
	  }
	  //锁定该印刷号
	  tMissionID = GrpGrid.getRowColData( tSel - 1, 7 );
	  tSubMissionID = GrpGrid.getRowColData( tSel - 1, 8 );
	  tGrpContNo = GrpGrid.getRowColData( tSel - 1, 1 );
	  var strSql ="select ActivityStatus from lwmission where activityid='0000002006' and missionid='"+tMissionID+"' and submissionid='"+tSubMissionID+"' with ur";
	  var arr = easyExecSql(strSql);
		if(arr){
			if(arr[0][0]=="0"){
				var strSql = "select count(1) from lccont where appflag in ('1','9') and PrtNo='"+cPolNo+"' with ur";
				alert("保单正在签单过程中！已完成"+easyExecSql(strSql)+"个被保险人，请稍等");
				return;
			}
		}
	  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + cPolNo + "&CreatePos=预打保单&PolState=1007&Action=INSERT";
	  var a = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
		var i = 0;
		var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.action = "GroupPolSignSave.jsp";
		fm.submit(); //提交
	}
}

/*********************************************************************
 *  集体投保单复核的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	window.focus();
	fm.all("workType").value = "";
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	 	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

		// 刷新查询结果
	//	easyQueryClick();		
	}
}

/*********************************************************************
 *  集体投保单复核的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit1( FlagStr, content )
{
	showInfo.close();
	window.focus();
	fm.all("workType").value = "";
	if( FlagStr == "Fail" )
	{	
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	 	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

		// 刷新查询结果
		easyQueryClick();		
	}
}




/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  调用EasyQuery查询保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
var turnPage = new turnPageClass();  
function easyQueryClick()
{
	// 初始化表格
	initGrpGrid();
	if(!verifyInput2())
	return false;
	
	// 书写SQL语句
	var strSQL = "";
	/*
	strSQL = "select grpcontno,PrtNo,AppntNo,GrpName,ManageCom,AgentCode from LCGrpcont where 1=1 "
				 + "and AppFlag='0' "
				 + "and ApproveFlag='9' "
				 + "and UWFlag in ('9','4') "			// 核保通过
				 + getWherePart( 'GrpContNo' )
				 + getWherePart( 'ManageCom' )
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'AgentGroup' )
				 + getWherePart( 'GrpNo' )
				 + getWherePart( 'GrpName' );
				 */
	//strSQL = "select lwmission.missionprop1,lwmission.missionprop2,lwmission.missionprop9,lwmission.missionprop7,lwmission.missionprop4,lwmission.missionprop5,lwmission.missionid,lwmission.submissionid,(case when a is null then '未交费' else '已交费' end)  from lwmission,(select max('已交费') a from ljtempfee o ) as x where 1=1"			 
	//       + " and lwmission.processid = '0000000004'"
	//       + " and lwmission.activityid = '0000002006' "
	//			 + getWherePart( 'missionprop1','GrpContNo' )
	//			 + getWherePart( 'missionprop2','PrtNo' )
	//			 + getWherePart( 'missionprop5','AgentCode' )
	//			 + getWherePart( 'missionprop6','AgentGroup' )
	//			 + getWherePart( 'missionprop9','GrpNo' )
	//			 + getWherePart( 'missionprop7','GrpName' )
	//			 + getWherePart( 'lwmission.missionprop4','ManageCom','like' )
	//			 + " order by lwmission.missionprop2";
	//A missionprop1
	//B missionprop2
	//C missionprop9
	//D missionprop7
	//E missionprop4
	//F missionprop5
	//G missionid
	//H submissionid
	//I 是否交费
	strSQL = "select A,B,C,D,E,F,G,H,I ,J, M from( "
	+"select missionprop1 A,missionprop2 B,missionprop9 C,missionprop7 D,missionprop4 E"
	+",getUniteCode(missionprop5) F,missionid G,submissionid H,(case "
	+"          when exists (select 1"
	+"                    from ljtempfee"
	+"                   where otherno = missionprop2"
	+"                      and enteraccdate is not null) then"
	+"              '已交费'"
	+"            when exists (select 1"
	+"                     from ljtempfeeclass a"
	+"                     left join ljtempfee b"
	+"                       on a.tempfeeno = b.tempfeeno"
	+"                   where b.otherno = missionprop2"
	+"                     and a.paymode in ('2', '3')"
	+"                       and b.enteraccdate is null) then"
    +"             '已交费，但未做到账确认'"
    +"             else"
    +"              '未交费'"
    +"           end) I"
	+", ( case when (select sum(paymoney) from ljtempfee where otherno=missionprop2	 and enteraccdate is not null) is null then 0  else (select sum(paymoney) from ljtempfee where otherno=missionprop2 and enteraccdate is not null and confflag = '0') end ) J  "
	+",(select stateflag from loprtmanager where otherno = missionprop1 and code = '76') M "
	+"from lwmission  where processid='0000000004' and activityid in ('0000002006', '0000002007') "
	
	+ getWherePart( 'missionprop1','GrpContNo' )
	+ getWherePart( 'missionprop2','PrtNo' )
	+ getWherePart( 'getUniteCode(missionprop5)','AgentCode' )
	+ getWherePart( 'missionprop6','AgentGroup' )
	+ getWherePart( 'missionprop9','GrpNo' )
	+ getWherePart( 'missionprop7','GrpName' )
	+ getWherePart( 'lwmission.missionprop4','ManageCom','like' )
	+" ) as x";
	turnPage.queryModal(strSQL, GrpGrid);
}

/*********************************************************************
 *  显示EasyQuery的查询结果
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpGrid();
		//HZM 到此修改
		GrpGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpGrid.loadMulLine(GrpGrid.arraySave);		
		//HZM 到此修改	
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		GrpGrid.delBlankLine();
	} // end of if
}

//发首期交费通知书
function SendFirstNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var tSel = GrpGrid.getSelNo();
	var cProposalNo = "";
	if( tSel != null && tSel != 0 )
	cProposalNo = GrpGrid.getRowColData( tSel - 1, 1 );
 
  cOtherNoType="01"; //其他号码类型
  cCode="76";        //单据类型
  
  
 	if( cProposalNo == null || cProposalNo == "" )
 	{
		alert("请选择一张保单");
	}
  else
	{
	fm.Code.value = "76";
	fm.OtherNoType.value = "01";
	fm.ProposalNoHide.value = cProposalNo;
  fm.action = "../uw/GrpUWSendPrintChk.jsp";
  fm.submit(); //提交
  }
 
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
    if(fm.all('AgentCode').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=2","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	  }
	if(fm.all('AgentCode').value != "")	 {

	var cAgentCode = fm.AgentCode.value;  //保单号码	
	var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
     }
	}	
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];
  	fm.AgentGroup.value = arrResult[0][1];fm.all('AgentGroupName').value = easyExecSql("select name from labranchgroup where  AgentGroup='"+arrResult[0][1]+"'");
  }
}

function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }
 if(cCodeName=="agentgroup2"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
 
}


function showPreviewBtn(cManageCom)
{
    var tIsGDCom = false;
    
    var tComCode = null;
    if(cManageCom.length >= 4)
    {
        var t4ComCode = cManageCom.substring(0, 4);
        if(t4ComCode == "8644")
            fm.btnPriviewGrpPol.style.display = "none";
    }
    else
    {
        fm.btnPriviewGrpPol.style.display = "";
    }
}
//校验转账支票是否已经到账确认
function isConfirm(prtno) {

	var valSQL = "select 1 as result from ljtempfeeclass a left join ljtempfee b on a.tempfeeno = b.tempfeeno  where  b.otherno = '"
			+ prtno
			+ "' and a.paymode in ('2','3') and a.enteraccdate is null with ur ";

	var result = easyExecSql(valSQL);

	if (result != null) {
		alert("该保单的收费方式是'转账支票'，尚未进行到账确认，不能进行签单");
		return false;
	}
	 return true;
}
