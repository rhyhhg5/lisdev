<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：契约保单发送及下载规则设置
//创建日期：2008-3-13 02:23下午
//创建人  ：YangYalin
//更新记录：更新人    更新日期     更新原因/内容
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="BusinessRuleConfig.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="BusinessRuleConfigInit.jsp"%>
<title>质检</title>
</head>
<body onload="initForm();" >
	<form action="./BusinessRuleConfigSave.jsp" method=post name=fm target="fraSubmit">
		<table class= common border=0>
			 <tr>
			 	<td class= titleImg align= center>请输入查询条件：</td>
			 </tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>管理机构</TD>
				<TD class= input>
			    <Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true  readonly>
			  </TD>
				<TD class= title style="display: none">规则类型</TD>
				<TD class= input style="display: none">
			    <Input class="codeNo"  name=RuleType value="01" verify="规则类型|code:tbruletype&NOTNULL" ondblclick="return showCodeList('TBRuleType',[this,RuleName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('TBRuleType',[this,RuleName],[0,1],null,null,null,1);"><input class=codename name=RuleName readonly=true readonly>
			  </TD>
				<TD class= title style="display: none">业务类型</TD>
				<TD class= input style="display: none">
			    <Input class="codeNo" name=BussType value="TB04" verify="业务类型|code:TBBussType&NOTNULL" ondblclick="return showCodeList('TBBussType',[this,BussName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('TBBussType',[this,BussName],[0,1],null,null,null,1);"><input class=codename name=BussName readonly=true readonly>
			  </TD>
			</TR>
		</table>
		<INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="queryRule();">
		
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>配置信息</td>
			</tr>
		</table>
		<Div id= "divUser" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanBusinessRuleGrid" ></span>
					</td>
				</tr>
			</table>
			<br>
    	<Div id= "divPage2" align=center style= "display: 'none' ">
        <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPage2.lastPage();"> 
      </Div>
		</Div>
		
		<Div id= "divButton" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td class= common>
						<INPUT VALUE="保存" name=QCershowpic class="cssButton" TYPE=button onclick="submitForm();">
					</td>
				</tr>
			</table>
		</Div>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
