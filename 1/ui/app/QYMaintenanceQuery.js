var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;

window.onfocus = myonfocus;

// 查询按钮
function easyQueryClick()
{
	initPolGrid();
	if(!verifyInput2())
		return false;
	
	if(fm.PrtNo.value == "" && fm.ContNo.value == "")
	{
		alert("印刷号和保单号至少录入一个！");
		return false;
	}
	
	// 书写SQL语句
    var strSQL = "";
    if(fm.ContType.value == 1)
    {
    	strSQL = "select lcc.Contno,lcc.Prtno,lcc.CValiDate,lcc.AppntName,lcc.Prem,lcc.AgentCom," 
    	+ "lcc.AgentCode,la.Name,lcc.ManageCom,lcc.ContType,lcc.SaleChnl "
    	+ "from lccont lcc, laagent la "
    	+ "where 1 = 1 "
    	+ " and lcc.AgentCode = la.AgentCode and lcc.ContType = '1' "
    	+ getWherePart('lcc.ContNo','ContNo')
        + getWherePart('lcc.PrtNo','PrtNo')
        + getWherePart('lcc.ManageCom','ManageCom','like')
        + getWherePart('lcc.CValiDate','startCValiDate','>=')
        + getWherePart('lcc.CValiDate','endCValiDate','<=');
    }
    else if(fm.ContType.value == 2)
    {
    	strSQL = "select lgc.GrpContno,lgc.Prtno,lgc.CValiDate,lgc.GrpName,lgc.Prem,lgc.AgentCom," 
    	+ "lgc.AgentCode,la.Name,lgc.ManageCom,'2' ContType,lgc.SaleChnl "
    	+ "from lcgrpcont lgc, laagent la "
    	+ "where 1 = 1 "
    	+ " and lgc.AgentCode = la.AgentCode "
    	+ getWherePart('lgc.GrpContNo','ContNo')
        + getWherePart('lgc.PrtNo','PrtNo')
        + getWherePart('lgc.ManageCom','ManageCom','like')
        + getWherePart('lcc.CValiDate','startCValiDate','>=')
        + getWherePart('lcc.CValiDate','endCValiDate','<=');
    }
    turnPage.pageLineNum = 50;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    fm.querySql.value = strSQL;
}

//维护销售渠道
function modifySaleChnl()
{
	var i = 0;
	var checkFlag = 0;
	var action = "SaleChnl"
	
	for(i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getSelNo(i))
		{
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	
	if(checkFlag == 0)
	{
		alert("请先选择一条保单信息！");
		return false;
	}
	
	var contno = PolGrid.getRowColData(checkFlag - 1, 1);
	var prtno = PolGrid.getRowColData(checkFlag - 1, 2);
	var conttype = PolGrid.getRowColData(checkFlag - 1, 10);
	
	var tStrURL = "./QYMaintenance.jsp?ContNo=" + contno + "&PrtNo=" + prtno + "&ContType=" + conttype + "&Action=" + action;
	window.location = tStrURL;
}

//维护业务员
function modifyAgentCode()
{
	var i = 0;
	var checkFlag = 0;
	var action = "AgentCode"
	
	for(i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getSelNo(i))
		{
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	
	if(checkFlag == 0)
	{
		alert("请先选择一条保单信息！");
		return false;
	}
	
	var contno = PolGrid.getRowColData(checkFlag - 1, 1);
	var prtno = PolGrid.getRowColData(checkFlag - 1, 2);
	var conttype = PolGrid.getRowColData(checkFlag - 1, 10);
	
	var tStrURL = "./QYMaintenance.jsp?ContNo=" + contno + "&PrtNo=" + prtno + "&ContType=" + conttype + "&Action=" + action;
	window.location = tStrURL;
}

//维护销售渠道与业务员
function modifyBoth()
{
	var i = 0;
	var checkFlag = 0;
	var action = "Both"
	
	for(i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getSelNo(i))
		{
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	
	if(checkFlag == 0)
	{
		alert("请先选择一条保单信息！");
		return false;
	}
	
	var contno = PolGrid.getRowColData(checkFlag - 1, 1);
	var prtno = PolGrid.getRowColData(checkFlag - 1, 2);
	var conttype = PolGrid.getRowColData(checkFlag - 1, 10);
	
	var tStrURL = "./QYMaintenance.jsp?ContNo=" + contno + "&PrtNo=" + prtno + "&ContType=" + conttype + "&Action=" + action;
	window.location = tStrURL;
}
