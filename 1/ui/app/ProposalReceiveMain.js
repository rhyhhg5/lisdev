//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;
var manageCom = '';
var AppntName ='';
var InsuredName='';

//提交，保存按钮对应操作,提交申请
function printPol()
{
	var i = 0;
	var flag = 0;
	flag = 0;
	
	for( i = 0; i < PolGrid.mulLineCount; i++ )
	{
		if( PolGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}
	
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//showSubmitFrame(mDebug);
	
	fm.submit();
	if(fmSave.fmtransact.value == "")
	{
		fmSave.fmtransact.value = "PRINT";
		fmSave.submit();
	}
	else
	{
		fmSave.submit();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	resetHiddenbutton();
	initPolGrid();
		if(!verifyInput2())
	return false;
	
	var strManageComWhere = " AND ManageCom LIKE '" + manageCom + "%25' ";
	if( fm.ManageCom.value != '' )
	{
		strManageComWhere += " AND ManageCom LIKE '" + fm.ManageCom.value + "%25' ";
	}
	var strAppntNameWhere = " AND b.AppntName LIKE '%25" + AppntName + "%25' ";
	var strgrpAppntNameWhere = " AND b.grpName LIKE '%25" + AppntName + "%25' ";
	if( fm.AppntName.value != '' )
	{
		strAppntNameWhere += " AND b.AppntName LIKE '%25" + fm.AppntName.value + "%25' ";
		strgrpAppntNameWhere+=" AND b.grpName LIKE '%25" + fm.AppntName.value + "%25' ";
	}
	var strInsuredNameWhere = " AND b.InsuredName LIKE '%25" + InsuredName + "%25' ";
	if( fm.InsuredName.value != '' )
	{
		strInsuredNameWhere += " AND b.InsuredName LIKE '%25" + fm.InsuredName.value + "%25' ";
	}

	// 书写SQL语句
	var strSQL = "";
	var cContType=fm.ContType.value;
	if(cContType==1){
	strSQL = "select a.ContNo, a.PrtNo, b.CValiDate,b.AppntName,b.Prem,getUniteCode(b.agentcode),a.managecom,"
	       + "a.PrintDate,a.ReceiveDate, a.ReceiveState,(case a.ReceiveState when '0' then '未接收'"
	       + " when '1' then '已接收' when '2' then '已退回' end), "
	       + "a.ReceiveOperator ,a.BackReasonCode,a.BackReason,a.receiveid,a.conttype  from LCContreceive a,lccont b "
	       + "where a.contno=b.contno and a.dealstate = '0' and a.conttype='1' "
				 + getWherePart( 'a.ContNo','ContNo' )
				 + getWherePart( 'a.PrtNo','PrtNo' )
				 + getWherePart( 'a.ManageCom','ManageCom','like' )
				 + getWherePart( 'b.PolApplyDate','PolApplyDate' )
				 + getWherePart( 'b.CValiDate','CValiDate' )
				 + getWherePart( 'b.SignDate','SignDate' )
				 + getWherePart( 'getUniteCode(b.agentcode)','AgentCode' )
				 + getWherePart( 'a.ReceiveState','ReceiveType' )
				 + strAppntNameWhere
				 + strInsuredNameWhere
				 + " order by a.ManageCom,b.AgentCode";
				}
	else if(cContType==2){
	strSQL = "select a.ContNo, a.PrtNo, b.CValiDate,b.grpName,b.Prem,getUniteCode(b.agentcode),a.managecom,"
	       + "a.PrintDate,a.ReceiveDate, a.ReceiveState,(case a.ReceiveState when '0' then '未接收'"
	       + " when '1' then '已接收' when '2' then '已退回' end), "
	       + "a.ReceiveOperator ,a.BackReasonCode,a.BackReason,a.receiveid,a.conttype  from LCContreceive a,lcgrpcont b "
	       + "where a.contno=b.grpcontno and a.dealstate = '0'  and a.conttype='2' "
				 + getWherePart( 'a.ContNo','ContNo' )
				 + getWherePart( 'a.PrtNo','PrtNo' )
				 + getWherePart( 'a.ManageCom','ManageCom','like' )
				 + getWherePart( 'b.PolApplyDate','PolApplyDate' )
				 + getWherePart( 'b.CValiDate','CValiDate' )
				 + getWherePart( 'b.SignDate','SignDate' )
				 + getWherePart( 'getUniteCode(b.agentcode)','AgentCode' )
				 + getWherePart( 'a.ReceiveState','ReceiveType' )
				 + strgrpAppntNameWhere
				 + " order by a.ManageCom,b.AgentCode";
				}
	else{
	strSQL = "select a.ContNo, a.PrtNo, b.CValiDate,b.AppntName,b.Prem,getUniteCode(b.agentcode),a.managecom,"
	       + "a.PrintDate,a.ReceiveDate, a.ReceiveState,(case a.ReceiveState when '0' then '未接收'"
	       + " when '1' then '已接收' when '2' then '已退回' end), "
	       + "a.ReceiveOperator ,a.BackReasonCode,a.BackReason,a.receiveid,a.conttype  from LCContreceive a,lccont b "
	       + "where a.contno=b.contno and a.dealstate = '0' and a.conttype='1' "
				 + getWherePart( 'a.ContNo','ContNo' )
				 + getWherePart( 'a.PrtNo','PrtNo' )
				 + getWherePart( 'a.ManageCom','ManageCom','like' )
				 + getWherePart( 'b.PolApplyDate','PolApplyDate' )
				 + getWherePart( 'b.CValiDate','CValiDate' )
				 + getWherePart( 'b.SignDate','SignDate' )
				 + getWherePart( 'getUniteCode(b.agentcode)','AgentCode' )
				 + getWherePart( 'a.ReceiveState','ReceiveType' )
				 + strAppntNameWhere
				 + strInsuredNameWhere
				 + " union "
				 + "select a.ContNo, a.PrtNo, b.CValiDate,b.grpName,b.Prem,getUniteCode(b.agentcode),a.managecom,"
	       + "a.PrintDate,a.ReceiveDate, a.ReceiveState,(case a.ReceiveState when '0' then '未接收'"
	       + " when '1' then '已接收' when '2' then '已退回' end), "
	       + "a.ReceiveOperator ,a.BackReasonCode,a.BackReason,a.receiveid,a.conttype  from LCContreceive a,lcgrpcont b "
	       + "where a.contno=b.grpcontno and a.dealstate = '0'  and a.conttype='2' "
				 + getWherePart( 'a.ContNo','ContNo' )
				 + getWherePart( 'a.PrtNo','PrtNo' )
				 + getWherePart( 'a.ManageCom','ManageCom','like' )
				 + getWherePart( 'b.PolApplyDate','PolApplyDate' )
				 + getWherePart( 'b.CValiDate','CValiDate' )
				 + getWherePart( 'b.SignDate','SignDate' )
				 + getWherePart( 'getUniteCode(b.agentcode)','AgentCode' )
				 + getWherePart( 'a.ReceiveState','ReceiveType' )
				 + strgrpAppntNameWhere
				 ;
				 
		}
	turnPage.strQueryResult  = easyQueryVer3(strSQL);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有查询到要处理的业务数据！");
		return false;
	}
	
	//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = PolGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

//判断是否生成新数据
function dataConfirm(rr)
{
	if(rr.checked == true)
	{
		if( confirm("是否生成新数据？生成新数据点击“确定”，否则点“取消”。"))
		{
			rr.checked = true;
			fmSave.fmtransact.value = "CONFIRM";
		}
		else
		{
			rr.checked = false;
			fmSave.fmtransact.value = "PRINT";
		}
	}
	else
	{
		rr.checked = false;
		fmSave.fmtransact.value = "PRINT";
	}
}
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }

}

function receivePol()
{
	if(fmSave.mReceiveID.value ==""||fmSave.mContNo.value ==""||fmSave.mPrtNo.value ==""||fmSave.mContType.value ==""){
		alert("请先选择选择一条记录"); 
		return;
		}
		fmSave.all('fmtransact').value = "RECEIVE";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//showSubmitFrame(mDebug);
	
	  fmSave.submit();
}

function rejectsPol()
{
	if(fmSave.BackReasonCode.value == ""||fmSave.BackReasonDetail.value == ""){
		alert("请先选择退回原因"); 
		return;
		}
	if(fmSave.mReceiveID.value ==""||fmSave.mContNo.value ==""||fmSave.mPrtNo.value ==""||fmSave.mContType.value ==""){
		alert("请先选择选择一条记录"); 
		return;
		}
		fmSave.all('fmtransact').value = "REJECT";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//showSubmitFrame(mDebug);
	
	  fmSave.submit();
}

function Polinit()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
     		 if (PolGrid.getSelNo(i)) { 
     			 checkFlag = PolGrid.getSelNo();
     			 break;
  		 	 }
  }
  if (checkFlag) { 
  				  	var tReceiveID=PolGrid.getRowColData(checkFlag-1,15);
  						var tContNo=PolGrid.getRowColData(checkFlag-1,1);
  						var tPrtNo=PolGrid.getRowColData(checkFlag-1,2);
  						var tContType=PolGrid.getRowColData(checkFlag-1,16);
  						var tBackReasonCode=PolGrid.getRowColData(checkFlag-1,13);
  						var tBackReason=PolGrid.getRowColData(checkFlag-1,14);
    					var tReceiveState=PolGrid.getRowColData(checkFlag-1,10);
    					var tBackReasonDetail="";
    					var tsql="select codename from ldcode where codetype='backreasoncode' and code='"+tBackReasonCode+"'";
    					var tarr=easyExecSql(tsql);
    					if(tarr){
    							tBackReasonDetail=tarr[0][0];
    				   }
  						fmSave.all('mReceiveID').value =tReceiveID;
              fmSave.all('mContNo').value = tContNo;
              fmSave.all('mPrtNo').value = tPrtNo;
              fmSave.all('mContType').value = tContType; 
              fmSave.all('BackReasonCode').value = tBackReasonCode;
              fmSave.all('BackReason').value = tBackReason;
              fmSave.all('BackReasonDetail').value = tBackReasonDetail;
              
              if (tReceiveState==0){
                fmSave.all("receivebtn").disabled=false;
                fmSave.all("rejectsbtn").disabled=false;
              	}
              else if (tReceiveState==1){
                fmSave.all("receivebtn").disabled=true;
                fmSave.all("rejectsbtn").disabled=true;
              	}
              else if (tReceiveState==2){
                fmSave.all("receivebtn").disabled=true;
                fmSave.all("rejectsbtn").disabled=true;
              	}
              else{
                fmSave.all("receivebtn").disabled=true;
                fmSave.all("rejectsbtn").disabled=true;
              	}
  }
  else{
  	 			alert("请先选择一条保单信息！"); 
 	}

}

function resetHiddenbutton()
{

    fmSave.all('mReceiveID').value = '';
    fmSave.all('mContNo').value = '';
    fmSave.all('mPrtNo').value = '';
    fmSave.all('mContType').value = '';
    fmSave.all('fmtransact').value = '';
    fmSave.all('BackReasonCode').value = '';
    fmSave.all('BackReasonDetail').value = '';
    fmSave.all('BackReason').value = '';
    fmSave.all("receivebtn").disabled=false;
    fmSave.all("rejectsbtn").disabled=false;
}