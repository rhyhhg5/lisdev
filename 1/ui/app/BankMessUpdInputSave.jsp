<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BankMessUpdInput.jsp
//程序功能：银行信息维护
//创建日期：2014-05-05 14:57:36
//创建人  ：zjd
//更新记录：  更新人    更新日期     更新原因/内容
System.out.println("Auto-begin:");
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
<%
  //输出参数
  request.setCharacterEncoding("GBK");
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String tFlag = request.getParameter("tFlag");
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  if(tG == null) 
  {
    System.out.println("session has expired");
    return;
   }
  //prepare data for workflow 
  VData tVData = new VData();
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("PrtNo",request.getParameter("prtno"));
  
  tTransferData.setNameAndValue("BackCode",request.getParameter("bankcode"));
  tTransferData.setNameAndValue("AccName",request.getParameter("accname"));
  tTransferData.setNameAndValue("BankAccNo",request.getParameter("bankaccno")); 
  tTransferData.setNameAndValue("Operator",tG.Operator);
  
    System.out.println("PrtNo"+request.getParameter("prtno") +"BackCode"+request.getParameter("bankcode") +"AccName"+request.getParameter("accname")+"BankAccNo"+request.getParameter("bankaccno"));
    tVData.add( tTransferData );
    tVData.add( tG ); 
    try
    {
        
      
          BankMessUpdUI tBankMessUpdUI = new BankMessUpdUI();
        if (tBankMessUpdUI.submitData(tVData,"UPDATE") == false)
                {
                    int n = tBankMessUpdUI.mErrors.getErrorCount();
                    System.out.println("n=="+n);
                    for (int j = 0; j < n; j++)
                    System.out.println("Error: "+tBankMessUpdUI.mErrors.getError(j).errorMessage);
                    Content = " 银行信息维护失败，原因是: " + tBankMessUpdUI.mErrors.getError(0).errorMessage;
                    FlagStr = "Fail";
                }       
                                //如果在Catch中发现异常，则不从错误类中提取错误信息
                if (FlagStr != "Fail")
                {
                    tError = tBankMessUpdUI.mErrors;
                    //tErrors = tTbWorkFlowUI.mErrors;
                    Content = " 银行信息维护成功! ";
                    if (!tError.needDealError())
                    {                          
                        int n = tError.getErrorCount();
                        if (n > 0)
                        {                     
                          for(int j = 0;j < n;j++)
                          {
                            //tError = tErrors.getError(j);
                            Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
                          }
                        }
        
                        FlagStr = "Succ";
                    }
                    else                                                                           
                    {
                        int n = tError.getErrorCount();
                        if (n > 0)
                        {
                          for(int j = 0;j < n;j++)
                          {
                            //tError = tErrors.getError(j);
                            Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
                          }
                            }
                        FlagStr = "Fail";
                    }
                }
        

    }
    catch(Exception ex)
    {
            ex.printStackTrace();
            Content = Content.trim() +" 提示:异常退出.";
    }
%>                      
<html>
<script language="javascript">

parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
