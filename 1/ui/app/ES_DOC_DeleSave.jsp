<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ES_DOC_DeleSave.jsp
//程序功能：投保单扫描件删除功能
//创建日期：2007-11-19
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
  System.out.println("input in to scan delete page!");
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";
  
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  //执行动作
  transact = request.getParameter("fmtransact");
  //获得单证主表信息
  String docId = request.getParameter("DocId");
  String docCode = request.getParameter("DocCode1");
  String subType = request.getParameter("SubType");
  System.out.println("docId : " + docId + "  docCode : " + docCode + "  subType : " + subType);

  //输入参数
  ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
  tES_DOC_MAINSchema.setDocID(docId);
  tES_DOC_MAINSchema.setDocCode(docCode);
  TransferData transferData = new TransferData();
  transferData.setNameAndValue("subType",subType);

  ScanDeleUI tScanDeleUI = new ScanDeleUI();
  try
  {
    VData vData =  new VData();
    vData.add(tES_DOC_MAINSchema);
    vData.add(tG);
    vData.add(transferData);

    tScanDeleUI.submitData(vData,transact);
  }
  catch(Exception ex)
  {
    Content = "删除失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("after submit!");
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "")
  {
    tError = tScanDeleUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 删除成功! ";
      FlagStr = "Success";
    }
    else
    {
      Content = " 删除失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  </script>
</html>
