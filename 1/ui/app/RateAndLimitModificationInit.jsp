<%
//程序名称：LMInsuAccRateInit.jsp
//程序功能：给付比例和免赔额的修改
//创建日期：2013-4-15
//创建人  ：zlx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ManageCom;
String strOperator =globalInput.Operator;
%>  
<script language="JavaScript" >
//输入框的初始化 
function initInpBox()	
{
  try
  {
	 fm.all('ManageCom').value = '<%=strManageCom%>';
        if(fm.all('ManageCom').value!=null)
        {
            var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('ManageComName').value=arrResult[0][0];
            }
        }
    fm.all('PrtNo').value = "";
    fm.all('GrpContNo').value = "";
    fm.all('ContPlanCode').value = "";
    fm.all('RiskCode').value = "";
    fm.all('DutyCode').value = "";
  }
  catch(ex)
  {
    alert("在LMInsuAccRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initInpBox();//把文本框设空
    initLMrateAndLimitGrid(); //初始化列名
    //showAllCodeName();
  }
  catch(re) 
  {
    alert("在TESTGroupListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//保单信息Mulline的初始化
var LMrateAndLimitGrid;
function initLMrateAndLimitGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="80px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=0;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="印刷号";      //列名RiskCode
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="合同号";      //列名RiskCode
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[3]=new Array();
    iArray[3][0]="生效日期";      //列名InsuAccNo
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="投保人";      //列名InsuAccNo
    iArray[4][1]="120px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[5]=new Array();
    iArray[5][0]="保障计划编码";      //列名BalaMonth
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[6]=new Array();
    iArray[6][0]="险种编码";      //列名RateType
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="责任编码";      //列名RateType
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[8]=new Array();
    iArray[8][0]="管理机构";      //列名RateIntv
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
      
    iArray[9]=new Array();
    iArray[9][0]="给付比例";      //列名RateIntv
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=0;             //是否允许输入,1表示允许，0表示不允许
   
    iArray[10]=new Array();
    iArray[10][0]="免赔额";      //列名RateIntv
    iArray[10][1]="100px";        //列宽
    iArray[10][2]=200;           //列最大值
    iArray[10][3]=0;             //是否允许输入,1表示允许，0表示不允许
   
    LMrateAndLimitGrid = new MulLineEnter("fm", "LMrateAndLimitGrid"); 
	  //设置Grid属性
    LMrateAndLimitGrid.mulLineCount = 0;
    LMrateAndLimitGrid.displayTitle = 1;
    LMrateAndLimitGrid.locked = 1;
    LMrateAndLimitGrid.canSel = 1;	
    LMrateAndLimitGrid.canChk = 0;
    LMrateAndLimitGrid.hiddenSubtraction = 1;
    LMrateAndLimitGrid.selBoxEventFuncName ="";
    LMrateAndLimitGrid.hiddenPlus = 1;
    LMrateAndLimitGrid.loadMulLine(iArray);
    LMrateAndLimitGrid.selBoxEventFuncName = "ShowDetail";//修改的时候用
 
  }
  catch(ex)
  {
  	alert("在函数中发生异常:初始化界面错误!");
  }
}
</script>

