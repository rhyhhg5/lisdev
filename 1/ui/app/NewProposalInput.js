//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量 
var showInfo;
var spanObj;
var mDebug = "100";
var mOperate = 0;
var mAction = "";
var arrResult = new Array();
var mShowCustomerDetail = "PROPOSAL";
var mCurOperateFlag = ""	// "1--录入，"2"--查询
var mGrpFlag = ""; 	//个人集体标志,"0"表示个人,"1"表示集体.
var cflag = "0";        //文件件录入位置
var sign=0;

var arrGrpRisk = null;

window.onfocus = myonfocus;
var hiddenBankInfo = "";

/*********************************************************************
 *  选择险种后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) {
	try	{	  
	    //险种选择
	   
	   try{
	       if( type =="noScan" && cCodeName == 'RiskInd')//个单无扫描件录入
		    {
		    	var strSql = "select distinct 1 from ldsysvar where VERIFYOPERATEPOPEDOM('"+Field.value+"','"+ManageCom+"','"+ManageCom+"',"+"'Pa')=1 ";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ;
	            //alert(strSql);
	           var arrResult = easyExecSql(strSql);
	           //alert(arrResult);
	          if (arrResult == null) {
	              alert("机构 ["+ManageCom+"] 无权录入险种 ["+Field.value+"] 的投保单!"); 
	              gotoInputPage();
	              return ;
	          }
	        }
	                
        }
        catch(ex) {}
        
	   try{
	        //alert(cCodeName);
	        if (typeof(prtNo)!="undefined" && typeof(type)=="undefined" && cCodeName == 'RiskInd')//个单有扫描件录入
		    {
		    	//alert(cCodeName);
		    	var strSql2 = "select distinct 1 from ldsysvar where VERIFYOPERATEPOPEDOM('"+Field.value+"','"+ManageCom+"','"+ManageCom+"',"+"'Pz')=1 ";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ;
	            //alert(strSql2);
	           var arrResult2 = easyExecSql(strSql2);
	           //alert(arrResult2);
	          if (arrResult2 == null) {
	              alert("机构 ["+ManageCom+"] 无权录入险种 ["+Field.value+"] 的投保单!"); 
	              gotoInputPage();
	              return ;
	          }
	        }	        
        }
        catch(ex) {}
          
		if( cCodeName == "RiskInd" || cCodeName == "RiskGrp" || cCodeName == "RiskCode") {
			//alert(loadFlag);
			getRiskInput(Field.value, loadFlag);//loadFlag在页面出始化的时候声明
			
			//将扫描件图片翻到第一页
		  try { goToPic(0);	}	catch(ex2) {} 	
		}
		
		//自动填写受益人信息
		if (cCodeName == "customertype") {
		  if (Field.value == "A") {
        var index = BnfGrid.mulLineCount;
        BnfGrid.setRowColData(index-1, 2, fm.all("AppntName").value);
        BnfGrid.setRowColData(index-1, 3, fm.all("AppntIDType").value);
        BnfGrid.setRowColData(index-1, 4, fm.all("AppntIDNo").value);
        BnfGrid.setRowColData(index-1, 5, fm.all("AppntRelationToInsured").value);
        BnfGrid.setRowColData(index-1, 8, fm.all("AppntHomeAddress").value);
		  } 
		  else if (Field.value == "I") {
		    var index = BnfGrid.mulLineCount;
        BnfGrid.setRowColData(index-1, 2, fm.all("Name").value);
        BnfGrid.setRowColData(index-1, 3, fm.all("IDType").value);
        BnfGrid.setRowColData(index-1, 4, fm.all("IDNo").value);
        BnfGrid.setRowColData(index-1, 5, "00");
        BnfGrid.setRowColData(index-1, 8, fm.all("AppntHomeAddress").value);
		  }
		}	
		
		//收费方式选择
		if (cCodeName == "PayLocation") {
		  if (Field.value != "0") {
		    if (hiddenBankInfo=="") hiddenBankInfo = DivLCKind.innerHTML;
		    
		    fm.all("BankCode").className = "readonly";
		    fm.all("BankCode").readOnly = true;
		    fm.all("BankCode").tabIndex = -1;
		    fm.all("BankCode").ondblclick = "";
		    
		    fm.all("AccName").className = "readonly";
		    fm.all("AccName").readOnly = true;
		    fm.all("AccName").tabIndex = -1;
		    fm.all("AccName").ondblclick = "";
		    
		    fm.all("BankAccNo").className = "readonly";
		    fm.all("BankAccNo").readOnly = true;
		    fm.all("BankAccNo").tabIndex = -1;
		    fm.all("BankAccNo").ondblclick = "";
		  }
		  else {
		    if (hiddenBankInfo!="") DivLCKind.innerHTML = hiddenBankInfo;
		    
		    fm.all("BankCode").value = "";
		    fm.all("AccName").value = fm.all("AppntName").value;
		    fm.all("BankAccNo").value = "";
		    fm.all("PayLocation").value = "0";
		    fm.all("PayLocation").focus();
		  }
		}
	
	}
	catch( ex ) {
	}
	emptyUndefined();	
}

/*********************************************************************
 *  根据LoadFlag设置一些Flag参数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function convertFlag( cFlag )
{
  //alert("cFlag:" + cFlag);
	if( cFlag == "1" || cFlag == "99" || cFlag == "8")		// 个人投保单直接录入
	{
		mCurOperateFlag = "1";
		mGrpFlag = "0";
	}
	if( cFlag == "2" || cFlag == "7" || cFlag == "9")		// 集体下个人投保单录入
	{
		mCurOperateFlag = "1";
		mGrpFlag = "1";
	}
	if( cFlag == "9" )		// 集体下无名单录入
	{
		mCurOperateFlag = "9";
		mGrpFlag = "1";
	}
	if( cFlag == "3" )		// 个人投保单明细查询
	{
		mCurOperateFlag = "2";
		mGrpFlag = "0";
	}
	if( cFlag == "4" )		// 集体下个人投保单明细查询
	{
		mCurOperateFlag = "2";
		mGrpFlag = "1";
	}
	if( cFlag == "5" )		// 个人投保单复核查询
	{
		mCurOperateFlag = "2";
		mGrpFlag = "3";
	}
}

/**
 * 获取该集体下所有险种信息
 */
function queryGrpPol(prtNo, riskCode) {
  var findFlag = false;
  
  //根据印刷号查找出所有的相关险种
  if (arrGrpRisk == null) { 
    var strSql = "select GrpProposalNo, RiskCode from LCGrpPol where PrtNo = '" + prtNo + "'";
    arrGrpRisk  = easyExecSql(strSql);
    
    //通过承保描述定义表找到主险
    for (i=0; i<arrGrpRisk.length; i++) {
      strSql = "select SubRiskFlag from LMRiskApp where RiskCode = '" 
             + arrGrpRisk[i][1] + "' and RiskVer = '2002'";
      var riskDescribe = easyExecSql(strSql);
      
      if (riskDescribe == "M") {
        top.mainRisk = arrGrpRisk[i][1];
        break;
      }
    }
  }
  //alert(arrGrpRisk);
  
  //获取选择的险种和集体投保单号码
  for (i=0; i<arrGrpRisk.length; i++) {
    if (arrGrpRisk[i][1] == riskCode) {
      fm.all("RiskCode").value = arrGrpRisk[i][1];
      fm.all("GrpPolNo").value = arrGrpRisk[i][0];
      
      if (arrGrpRisk[i][1] == top.mainRisk) {
        top.mainPolNo = "";  
      }
      
      findFlag = true;
      break;
    }
  }
  
  if (arrGrpRisk.length > 1) {
    fm.all("RiskCode").className = "code";
    fm.all("RiskCode").readOnly = false;
  }
  else {
    fm.all("RiskCode").onclick = "";
  }
  
  return findFlag;
}

/**
 * 根据身份证号码获取出生日期
 */
function grpGetBirthdayByIdno() {
  var id = fm.all("IDNo").value;
  
  if (fm.all("IDType").value == "0") {
    if (id.length == 15) {
      id = id.substring(6, 12);
      id = "19" + id; 
      id = id.substring(0, 4) + "-" + id.substring(4, 6) + "-" + id.substring(6);
      fm.all("Birthday").value = id; 
    } 
    else if (id.length == 18) {
      id = id.substring(6, 14);
      id = id.substring(0, 4) + "-" + id.substring(4, 6) + "-" + id.substring(6);
      fm.all("Birthday").value = id; 
    }     
  }
}

/**
 * 校验主附险包含关系
 */
function checkRiskRelation(tPolNo, cRiskCode) {
  // 集体下个人投保单
  if (mGrpFlag == "1") {
    var strSql = "select RiskCode from LCGrpPol where GrpProposalNo = '" + tPolNo 
               + "' and RiskCode in (select Code1 from LDCode1 where Code = '" + cRiskCode + "')";
    return true;
  }
  else {
    var strSql = "select RiskCode from LCPol where PolNo = '" + tPolNo 
               + "' and RiskCode in (select Code1 from LDCode1 where Code = '" + cRiskCode + "')";
  }
  
  return easyQueryVer3(strSql);
}

/**
 * 出错返回到险种选择界面
 */
function gotoInputPage() {
  // 集体下个人投保单
  if (mGrpFlag == "1") {              				  
    top.fraInterface.window.location = "../app/ProposalGrpInput.jsp?LoadFlag=" + loadFlag;
  }
  // 个人有扫描件投保单
  else if (typeof(prtNo)!="undefined" && typeof(type)=="undefined") {  
    top.fraInterface.window.location = "../app/ProposalInput.jsp?prtNo=" + prtNo;
  }
  // 个人无扫描件投保单
  else {
    top.fraInterface.window.location = "../app/ProposalOnlyInput.jsp?type=noScan";
  } 
}

function showSaleChnl() {
  showCodeList('SaleChnl',[this]);
}

/*********************************************************************
 *  根据不同的险种,读取不同的代码
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getRiskInput(cRiskCode, cFlag) {
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "";
	var tPolNo = "";
	//alert("getRiskInput.cRiskCode:"+cRiskCode);
	convertFlag(cFlag);
	
	// 首次进入集体下个人录入
	if( mGrpFlag == "0" )		// 个人投保单
		urlStr = "../riskinput/Risk" + cRiskCode + ".jsp";  
	if( mGrpFlag == "1" )		// 集体下个人投保单
		urlStr = "../riskgrp/Risk" + cRiskCode + ".jsp?NoListFlag=1";  
	if( mGrpFlag == "3" )		// 个人投保单复核
		urlStr = "../riskinput/Risk" + cRiskCode + ".jsp";

	//读取险种的界面描述
	showInfo = window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;dialogTop:-800;dialogLeft:-800;resizable=1");

  showDiv(inputButton, "true"); 
 
	if (cFlag == "99") showDiv(autoMoveButton, "true"); 
	
	if (cFlag == "3") {
	  fm.all("SaleChnl").readOnly = false;
    fm.all("SaleChnl").className = "code";
    fm.all("SaleChnl").ondblclick= showSaleChnl;
	}

  try { 
    //出始化公共信息
	  emptyForm();
    
    //根据需求单放开销售渠道只读的限制，by Minim at 2003-11-24
    fm.all("SaleChnl").readOnly = false;
    fm.all("SaleChnl").className = "code";
    fm.all("SaleChnl").ondblclick= showSaleChnl;
    
    //无扫描件录入
    if (typeof(type)!="undefined" && type=="noScan") {
      fm.all("PrtNo").readOnly = false;
      fm.all("PrtNo").className = "common";
      
      //通过承保描述定义表找到主险      
      strSql = "select SubRiskFlag from LMRiskApp where RiskCode = '" 
             + cRiskCode + "' and RiskVer = '2002'";
      var riskDescribe = easyExecSql(strSql);
      
      if (riskDescribe == "M") {
        top.mainPolNo = "";
      }
     
    }
    else {
      fm.all("PrtNo").value = prtNo;
      setFocus(prtNo);
    }
  } catch(e) {}

	//传入险种和印刷号信息
	fm.all("RiskCode").value = cRiskCode;
	
	//将焦点移动到印刷号，以方便随动录入
	fm.all("PrtNo").focus();

  //初始化界面的销售渠道
  try {
    prtNo = fm.all("PrtNo").value;
  	var riskType = prtNo.substring(2, 4);
  	if (riskType == "11") { fm.all("SaleChnl").value = "02"; }
  	else if (riskType == "12") { fm.all("SaleChnl").value = "01"; }
  	else if (riskType == "15") { fm.all("SaleChnl").value = "03"; }
  	
  	else if (riskType == "16") { 
  	  fm.all("SaleChnl").value = "02"; 
  	  
  	  fm.all("SaleChnl").readOnly = false;
      fm.all("SaleChnl").className = "code";
      fm.all("SaleChnl").ondblclick= showSaleChnl;
    }
  } catch(e) {}
  
  if (!(typeof(top.type)!="undefined" && (top.type=="ChangePlan" || top.type=="SubChangePlan"))) {
    //将是否指定生效日期在录入时失效
    fm.all("SpecifyValiDate").readOnly = true;
    fm.all("SpecifyValiDate").className = "readOnly";
    fm.all("SpecifyValiDate").ondblclick = "";
    fm.all("SpecifyValiDate").onkeyup = "";
    //fm.all("SpecifyValiDate").value = "N";
    fm.all("SpecifyValiDate").tabIndex = -1;
  }
  
  //alert("mCurOperateFlag:"+mCurOperateFlag);
	if( mCurOperateFlag == "1" ) {             // 录入
		// 集体下个人投保单
		//alert("mGrpFlag:"+mGrpFlag);
		if( mGrpFlag == "1" )	{ 
   			
			getGrpPolInfo();                       // 带入集体部分信息
      
			//支持集体下个人，录入身份证号带出出生日期
			fm.all("IDNo").onblur = grpGetBirthdayByIdno;
			//去掉问题件按钮		
			inputQuest.style.display = "none";
			
			// 获取该集体下所有险种信息
			//alert("judging if the RiskCode input has been input in group info.");
			if (!queryGrpPol(fm.all("PrtNo").value, cRiskCode))	{
			  alert("集体团单没有录入这个险种，集体下个人不允许录入！"); 
			  fm.all("RiskCode").value="";
			  //alert("now go to the new location- ProposalGrpInput.jsp");
			  top.fraInterface.window.location = "./ProposalGrpInput.jsp?LoadFlag=" + loadFlag;
			  //alert("top.location has been altered");
			  return false; //hezy
			}
		}
		
		//if(initDealForSpecRisk(cRiskCode)==false)//特殊险种在初始化时的特殊处理-houzm添加
		//{
			//alert("险种："+cRiskCode+"初始化时特殊处理失败！");
			//return false;
		//}
		//特殊险种在初始化时的特殊处理扩展-guoxiang add at 2004-9-6 16:33
        if(initDealForSpecRiskEx(cRiskCode)==false)
		{
			alert("险种："+cRiskCode+"初始化时特殊处理失败！");
			return false;
		}
		
		
		
		//alert("getRiskInput.isSubRisk begin...");
		//alert("cRiskCode:"+cRiskCode);
		if( isSubRisk( cRiskCode ) == true ) {   // 附险
		  //如果是新增附加险，则不清空缓存的主险保单号
		  if (cFlag != "8") {
			  top.mainPolNo = ""; //hezy add
			}
			tPolNo = getMainRiskNo(cRiskCode);   //弹出录入附险的窗口,得到主险保单号码
			if (!checkRiskRelation(tPolNo, cRiskCode)) {
			  alert("主附险包含关系错误，输入的主险号不能带这个附加险！"); 
			  gotoInputPage();
			  top.mainPolNo = "";
			  return false;
			}
			

//-----------------------------------------------------------------------
         if(cRiskCode=='121301'||cRiskCode=='321601')//出始化特殊的附险信息--houzm添加--可单独提出为一个函数
         {
         	if(cRiskCode=='121301')
         	{
				if (!initPrivateRiskInfo121301(tPolNo)) 
				{
				  gotoInputPage();
				  return false;
				}
			}
			if(cRiskCode=='321601')
         	{
				if (!initPrivateRiskInfo321601(tPolNo)) 
				{
				  gotoInputPage();
				  return false;
				}
			}
         }
         else
         {
         				//出始化附险信息
			if (!initPrivateRiskInfo(tPolNo)) 
			{
			  gotoInputPage();
			  return false;
			}
         }     
			
			try	{}	catch(ex1) { alert( "初始化险种出错" + ex1 );	}
		} // end of 附险if
		return false;
		
	} // end of 录入if
	
	
	mCurOperateFlag = "";
	mGrpFlag = "";
	
}

/*********************************************************************
 *  判断该险种是否是附险,在这里确定既可以做主险,又可以做附险的代码
 *  参数  ：  险种代码
 *  返回值：  无
 *********************************************************************
 */
function isSubRisk(cRiskCode) {
  //alert(cRiskCode);
  if (cRiskCode=="")
  {
   return false;	
  }
  var arrQueryResult = easyExecSql("select SubRiskFlag from LMRiskApp where RiskCode='" + cRiskCode + "'", 1, 0);

	if(arrQueryResult[0] == "S")    //需要转成大写
		return true;
	if(arrQueryResult[0] == "M")
		return false;

	if (arrQueryResult[0].toUpperCase() == "A")
		if (confirm("该险种既可以是主险,又可以是附险!选择确定进入主险录入,选择取消进入附险录入"))
			return false;
		else
			return true;

	return false;
}

/*********************************************************************
 *  弹出录入附险的窗口,得到主险保单号码
 *  参数  ：  险种代码
 *  返回值：  无
 *********************************************************************
 */
function getMainRiskNo(cRiskCode) {
	var urlStr = "../app/MainRiskNoInput.jsp";
	var tPolNo="";
  
  if (typeof(top.mainPolNo)!="undefined" && top.mainPolNo!="") {
    tPolNo = top.mainPolNo;
  }
  else {
    tPolNo = window.showModalDialog(urlStr,tPolNo,"status:no;help:0;close:0;dialogWidth:310px;dialogHeight:100px;center:yes;");
    top.mainPolNo = tPolNo;
  }
	
	return tPolNo;
}

/*********************************************************************
 *  初始化附险信息
 *  参数  ：  投保单号
 *  返回值：  无
 *********************************************************************
 */
function initPrivateRiskInfo(cPolNo) {
	if(cPolNo=="") {
		alert("没有主险保单号,不能进行附加险录入!");
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
		return false
	}
	
	var arrLCPol = new Array(); 
	var arrQueryResult = null;
	// 主保单信息部分
	var sql = "select * from lcpol where polno='" + cPolNo + "' "
			+ "and riskcode in "
			+ "( select riskcode from LMRiskApp where SubRiskFlag = 'M' )"; 
	
	arrQueryResult = easyExecSql( sql , 1, 0);
		
	if (arrQueryResult == null)	{
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
		
		top.mainPolNo = "";
		
		alert("读取主险信息失败,不能进行附加险录入!");
		return false
	}
	
	arrLCPol = arrQueryResult[0]; 	
	displayPol( arrLCPol );	
	
	fm.all("MainPolNo").value = cPolNo;
	var tAR;
			
  	//投保人信息
  	if (arrLCPol[28]=="2") {     //集体投保人信息
  	  arrQueryResult = null;
  	  arrQueryResult = easyExecSql("select * from lcappntgrp where polno='"+cPolNo+"'"+" and grpno='"+arrLCPol[26]+"'", 1, 0);	  		
  	  tAR = arrQueryResult[0];
  	  displayPolAppntGrp(tAR);
  	} else {                     //个人投保人信息
  	  arrQueryResult = null;
  	  arrQueryResult = easyExecSql("select * from lcappntind where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[26]+"'", 1, 0);
  	  tAR = arrQueryResult[0];
  	  displayPolAppnt(tAR);
  	}

	// 被保人信息部分
	if (arrLCPol[18] == arrLCPol[26]) {
	  fm.all("SamePersonFlag").checked = true;
		parent.fraInterface.isSamePersonQuery();
    parent.fraInterface.fm.all("CustomerNo").value = arrLCPol[18];
	}
	//else {
		arrQueryResult = null;
		arrQueryResult = easyExecSql("select * from lcinsured where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[18]+"'", 1, 0);
		tAR = arrQueryResult[0];
		displayPolInsured(tAR);	
	//}
	
	return true;
}

/*********************************************************************
 *  校验投保单的输入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function verifyProposal() {
  var passVerify = true;
  
  //保全新增附加险
  if (loadFlag == "8") {
    var newCValidate = fm.all('CValiDate').value;
    
    if (oldCValidate.substring(4) != newCValidate.substring(4)) {
      alert("指定的生效日期必须是主险生效日期的周年对应日"); 
      return false;
    }
    
    //正常的新增附加险不能这么限制
    //if (oldCValidate == newCValidate) {
    //  alert("根据新增附加险规则，不能指定为主险生效日期"); 
    //  return false;
    //}
    
    if (!confirm("指定的附加险生效日期为：(" + newCValidate + ")，确定吗？")) {
      return false;
    }
  }
  
  if(fm.AppntRelationToInsured.value=="00"){
    if(fm.AppntCustomerNo.value!= fm.CustomerNo.value){		
      alert("投保人与被保人关系为本人，但客户号不一致");
      return false;
    }
  }  
  
   if(needVerifyRiskcode()==true)
   {
		if(verifyInput() == false) passVerify = false;
		
		BnfGrid.delBlankLine();
		
		if(BnfGrid.checkValue("BnfGrid") == false) passVerify = false;
		
		 //校验单证是否发放给业务员
		 if (!verifyPrtNo(fm.all("PrtNo").value)) passVerify = false;
	}
	
	try {
	  var strChkIdNo = "";
    	
		  //以年龄和性别校验身份证号
		  if (fm.all("AppntIDType").value == "0") 
		    strChkIdNo = chkIdNo(fm.all("AppntIDNo").value, fm.all("AppntBirthday").value, fm.all("AppntSex").value);
		  if (fm.all("IDType").value == "0") 
		    strChkIdNo = chkIdNo(fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);
		    
		  if (strChkIdNo != "") {
		    alert(strChkIdNo);
		    passVerify = false;

	  
	  }  
	  //校验职业和职业代码
//	  var arrCode = new Array();
//	  arrCode = verifyCode("职业（工种）", fm.all("AppntWorkType").value, "code:OccupationCode", 1); 
//	  if (arrCode!=true && fm.all("AppntOccupationCode").value!=arrCode[0]) {
//	    alert("投保人职业和职业代码不匹配！");
//	    passVerify = false;
//	  }
//	  arrCode = verifyCode("职业（工种）", fm.all("WorkType").value, "code:OccupationCode", 1); 
//	  if (arrCode!=true && fm.all("OccupationCode").value!=arrCode[0]) {
//	    alert("被保人职业和职业代码不匹配！");
//	    passVerify = false;
//	  }
	    
	  //校验受益比例
	  var i;
	  var sumLiveBnf = new Array();
	  var sumDeadBnf = new Array();
	  for (i=0; i<BnfGrid.mulLineCount; i++) {
	    if (BnfGrid.getRowColData(i, 1) == "0") {
	      if (typeof(sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 7))]) == "undefined") 
	        sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 7))] = 0;
	      sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 7))] = sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 7))] + parseFloat(BnfGrid.getRowColData(i, 6));
	    }
	    else if (BnfGrid.getRowColData(i, 1) == "1") {
	      if (typeof(sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 7))]) == "undefined") 
	        sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 7))] = 0;
	      sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 7))] = sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 7))] + parseFloat(BnfGrid.getRowColData(i, 6));
	    }
	    
	  }
	  
	  for (i=0; i<sumLiveBnf.length; i++) {
  	  if (typeof(sumLiveBnf[i])!="undefined" && sumLiveBnf[i]>1) {
  	    alert("生存受益人受益顺序 " + i + " 的受益比例和为：" + sumLiveBnf[i] + " 。大于100%，不能提交！");
  	    passVerify = false; 
  	  }
  	  else if (typeof(sumLiveBnf[i])!="undefined" && sumLiveBnf[i]<1) {
  	    alert("注意：生存受益人受益顺序 " + i + " 的受益比例和为：" + sumLiveBnf[i] + " 。小于100%");
  	    passVerify = false;
  	  }
	  } 
	  
	  for (i=0; i<sumDeadBnf.length; i++) {
  	  if (typeof(sumDeadBnf[i])!="undefined" && sumDeadBnf[i]>1) {
  	    alert("死亡受益人受益顺序 " + i + " 的受益比例和为：" + sumDeadBnf[i] + " 。大于100%，不能提交！");
  	    passVerify = false; 
  	  }
  	  else if (typeof(sumDeadBnf[i])!="undefined" && sumDeadBnf[i]<1) {
  	    alert("注意：死亡受益人受益顺序 " + i + " 的受益比例和为：" + sumDeadBnf[i] + " 。小于100%");
  	    passVerify = false;
  	  }
	  } 
	  
	  if (trim(fm.BankCode.value)=="0101") {
      if (trim(fm.BankAccNo.value).length!=19 || !isInteger(trim(fm.BankAccNo.value))) {
        alert("工商银行的账号必须是19位的数字，最后一个星号（*）不要！\n如果客户填写无误，请一定发问题件！");
        passVerify = false;
      }
    }
    
    //校验客户是否死亡
    if (fm.AppntCustomerNo.value!="" && isDeath(fm.AppntCustomerNo.value)) {
      alert("投保人已经死亡！");
      passVerify = false;
    }
    
    if (fm.CustomerNo.value!="" && isDeath(fm.CustomerNo.value)) {
      alert("被保人已经死亡！");
      passVerify = false;
    }
	}
	catch(e) {}
	
	if (!passVerify) {
	  if (!confirm("投保单录入可能有错误，是否继续保存？")) return false;
	  else return true;
	}
}

//校验客户是否死亡
function isDeath(CustomerNo) {
  var strSql = "select DeathDate from LDPerson where CustomerNo='" + CustomerNo + "'";
  var arrResult = easyExecSql(strSql);
  
  if (arrResult == ""||arrResult == null) return false;
  else return true;
}

/*********************************************************************
 *  保存个人投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm() {
	if(sign != 0)
	{
	   alert("请不要重复点击!");
	   return;
	}
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
    var verifyGrade = "1";
  
 
  //根据特殊的险种做特殊处的理函数
  try {
  	 if(specDealByRisk()==false)
  	  return ;
  	}
  	 catch(e){}
    
    
  //检验出生日期，如果空，从身份证号取
  try {checkBirthday(); } catch(e){}
  
	// 校验被保人是否同投保人，相同则进行复制
  try { verifySamePerson(); } catch(e) {}
  	    	 
      	// 校验录入数据
    if( verifyProposal() == false ) return; 	 	
	
	if (trim(fm.all('ProposalNo').value) != "") {
	  alert("该投保单号已经存在，不允许再次新增，请重新进入录入界面！");
	  return false;
	}
	
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	if (loadFlag=="1") {
		mAction = "INSERTPERSON";
	}
	else {
		mAction = "INSERTGROUP";
	}

	fm.all( 'fmAction' ).value = mAction;
	
	fm.action="../app/ProposalSave.jsp"
	
	//为保全增加，add by Minim
	if (loadFlag=="7" || loadFlag=="8") {
	  fm.action="../app/ProposalSave.jsp?BQFlag=2"
	}
	
	//为无名单补名单增加，add by Minim
	if (loadFlag=="9") {
	  fm.action="../app/ProposalSave.jsp?BQFlag=4&MasterPolNo=" + parent.VD.gVSwitch.getVar('MasterPolNo');
	  //alert(fm.action);return;
	}
	sign = 1;
	fm.submit(); //提交
	sign = 0;
}

/**
 * 强制解除锁定
 */
function unLockTable() {
  if (fm.PrtNo.value == "") {
    alert("需要填写印刷号！");
    return;
  }
  
  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + fm.PrtNo.value + "&CreatePos=承保录单&PolState=1002&Action=DELETE";
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
}

/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	try { showInfo.close(); } catch(e) {}
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
    if (fm.GrpPolNo.value == "00000000000000000000" && cflag=="1") {
  		if (confirm("保存成功！\n要解除该印刷号的锁定，让复核人员能操作吗？")) {
  		  unLockTable();
  		}
	 }
	else {
		if(loadFlag == '3'){
		inputQuestButton.disabled = false;
		}		
		  content = "保存成功！";
		  var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  		
	}
		
		//暂时保存主险投保单号码，方便附险的录入，重新选择扫描件后失效
		try { if (top.mainPolNo == "") top.mainPolNo = fm.all("ProposalNo").value } catch(e) {}
	}
	
	//承保计划变更，附险终止的后续处理
	if (mAction=="DELETE") {
	  if (typeof(top.type)!="undefined" && top.type=="SubChangePlan") {
	    var tProposalNo = fm.all('ProposalNo').value;
    	var tPrtNo = fm.all('PrtNo').value;
    	var tRiskCode = fm.all('RiskCode').value;
    	
  		parent.fraTitle.window.location = "./ChangePlanSubWithdraw.jsp?polNo=" + tProposalNo + "&prtNo=" + tPrtNo + "&riskCode=" + tRiskCode;  
  	}
	}
	mAction = ""; 
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
	try	{
		//initForm();
		var tRiskCode = fm.RiskCode.value;
		var prtNo = fm.PrtNo.value;
		
		emptyForm();
		
		fm.RiskCode.value = tRiskCode;
		fm.PrtNo.value = prtNo;
		
		if (loadFlag == "2") {
		  getGrpPolInfo();  
		}
	}
	catch(re)	{
		alert("在ProposalInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
} 

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
}
 
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	//if( cDebug == "1" )
		//parent.fraMain.rows = "0,0,50,82,*";
	//else 
		//parent.fraMain.rows = "0,0,80,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	//showDiv( operateButton, "false" ); 
	//showDiv( inputButton, "true" ); 
}           

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick() {   
	if( mOperate == 0 )	{
		mOperate = 1;
		
		cGrpPolNo = fm.all( 'GrpPolNo' ).value;
		cContNo = fm.all( 'ContNo' ).value;
		window.open("./ProposalQueryMain.jsp?GrpPolNo=" + cGrpPolNo + "&ContNo=" + cContNo);		
	}
}           

/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
	var tProposalNo = "";
	tProposalNo = fm.all( 'ProposalNo' ).value;
	
	if( tProposalNo == null || tProposalNo == "" )
		alert( "请先做投保单查询操作，再进行修改!" );
	else {
		// 校验录入数据
		if (fm.all('DivLCInsured').style.display == "none") {
      for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {    
    	  if (fm.elements[elementsNum].verify != null && fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
    	    fm.elements[elementsNum].verify = "";
    	  }
    	} 
    }
    
		if( verifyProposal() == false ) return;
		
		// 校验被保人是否同投保人，相同则进行复制
    try { verifySamePerson(); } catch(e) {}

		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )	{
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			if (loadFlag=="1") {
				mAction = "UPDATEPERSON";
			}
			else {
				mAction = "UPDATEGROUP";
			}
			
			fm.all( 'fmAction' ).value = mAction;
			
			//承保计划变更(保持投保单状态不变：复核状态，核保状态)
			if (typeof(window.ChangePlanSub) == "object") fm.all('fmAction').value = "ChangePlan" + fm.all('fmAction').value;
			//修改浮动费率(保持投保单状态不变：复核状态，核保状态,作用比承保计划变更大一项，能修改浮动费率，为权限考虑)
			if(loadFlag=="10") fm.all('fmAction').value = "ChangePlan" + fm.all('fmAction').value;
			if(loadFlag=="3") fm.all('fmAction').value = "Modify" + fm.all('fmAction').value;
			//inputQuestButton.disabled = false;
			fm.submit(); //提交
		}
		
		try {
		  if (typeof(top.opener.modifyClick) == "object") top.opener.initQuery(); 
		}
		catch(e) {
		}
	}
}           

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick() { 
	var tProposalNo = fm.all('ProposalNo').value;
	
	if(tProposalNo==null || tProposalNo=="") {
		alert( "请先做投保单查询操作，再进行删除!" );
	}
	else {
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )	{
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}           

/*********************************************************************
 *  Click事件，当点击“选择责任”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function chooseDuty()
{
	cRiskCode = fm.RiskCode.value;
	cRiskVersion = fm.RiskVersion.value
	
	if( cRiskCode == "" || cRiskVersion == "" )
	{
		alert( "您必须先录入险种和险种版本才能修改该投保单的责任项。" );
		return false
	}

	showInfo = window.open("./ChooseDutyInput.jsp?RiskCode="+cRiskCode+"&RiskVersion="+cRiskVersion);
	return true
}           

/*********************************************************************
 *  Click事件，当点击“查询责任信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showDuty()
{
	//下面增加相应的代码
	cPolNo = fm.ProposalNo.value;
	if( cPolNo == "" )
	{
		alert( "您必须先保存投保单才能查看该投保单的责任项。" );
		return false
	}
	
	var showStr = "正在查询数据，请您稍候......";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	showModalDialog( "./ProposalDuty.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
	showInfo.close();
}           

/*********************************************************************
 *  Click事件，当点击“关联暂交费信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showFee()
{
	cPolNo = fm.ProposalNo.value;
	var prtNo = fm.PrtNo.value;
	
	if( cPolNo == "" )
	{
		alert( "您必须先保存投保单才能进入暂交费信息部分。" );
		return false
	}
	
	showInfo = window.open( "./ProposalFee.jsp?PolNo=" + cPolNo + "&polType=PROPOSAL&prtNo=" + prtNo );
}           

/*********************************************************************
 *  Click事件，当双击“投保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt()
{
	if( mOperate == 0 )
	{
		mOperate = 2;
		showInfo = window.open( "../sys/LDPersonMain.html" );
	}
}           

/*********************************************************************
 *  Click事件，当双击“被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsured()
{
	if( mOperate == 0 )
	{
		mOperate = 3;
		showInfo = window.open( "../sys/LDPersonMain.html" );
	}
}           

/*********************************************************************
 *  Click事件，当双击“连带被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubInsured( span, arrPara )
{
	if( mOperate == 0 )
	{
		mOperate = 4;
		spanObj = span;
		showInfo = window.open( "../sys/LDPersonMain.html" );
	}
}           

/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPol(cArr) 
{
	try 
	{
	    try { fm.all('PrtNo').value = cArr[6]; } catch(ex) { };
	    try { fm.all('ManageCom').value = cArr[12]; } catch(ex) { };
	    try { fm.all('SaleChnl').value = cArr[15]; } catch(ex) { };
	    try { fm.all('AgentCom').value = cArr[13]; } catch(ex) { };
	    try { fm.all('AgentType').value = cArr[14]; } catch(ex) { };
	    try { fm.all('AgentCode').value = cArr[87]; } catch(ex) { };
	    try { fm.all('AgentGroup').value = cArr[88]; } catch(ex) { };
	    //try { fm.all('Handler').value = cArr[82]; } catch(ex) { };
	    //try { fm.all('AgentCode1').value = cArr[89]; } catch(ex) { };
	    try { fm.all('Remark').value = cArr[90]; } catch(ex) { };
	
	    try { fm.all('ContNo').value = cArr[1]; } catch(ex) { };

	    //try { fm.all('Amnt').value = cArr[43]; } catch(ex) { };
	    try { fm.all('CValiDate').value = cArr[29]; } catch(ex) { };
	    try { fm.all('PolApplyDate').value = cArr[128]; } catch(ex) { };
	    try { fm.all('HealthCheckFlag').value = cArr[72]; } catch(ex) { };
	    try { fm.all('OutPayFlag').value = cArr[97]; } catch(ex) { };
	    try { fm.all('PayLocation').value = cArr[59]; } catch(ex) { };
	    try { fm.all('BankCode').value = cArr[102]; } catch(ex) { };
	    try { fm.all('BankAccNo').value = cArr[103]; } catch(ex) { };
	    try { fm.all('AccName').value = cArr[118]; } catch(ex) { };
	    try { fm.all('LiveGetMode').value = cArr[98]; } catch(ex) { };
	    try { fm.all('BonusGetMode').value = cArr[100]; } catch(ex) { };
	    try { fm.all('AutoPayFlag').value = cArr[65]; } catch(ex) { };
	    try { fm.all('InterestDifFlag').value = cArr[66]; } catch(ex) { };
     
	    try { fm.all('InsuYear').value = cArr[111]; } catch(ex) { };
	    try { fm.all('InsuYearFlag').value = cArr[110]; } catch(ex) { };
	    try { fm.all('PolTypeFlag').value = cArr[69]; } catch(ex) { };
	    try { fm.all('InsuredPeoples').value = cArr[24]; } catch(ex) { };
	    try { fm.all('InsuredAppAge').value = cArr[22]; } catch(ex) { };
	    

	    try { fm.all('StandbyFlag1').value = cArr[78]; } catch(ex) { };
	    try { fm.all('StandbyFlag2').value = cArr[79]; } catch(ex) { };
	    try { fm.all('StandbyFlag3').value = cArr[80]; } catch(ex) { };

	    
	} catch(ex) {
	  alert("displayPol err:" + ex + "\ndata is:" + cArr);
	}
}

/*********************************************************************
 *  把保单中的投保人信息显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolAppnt(cArr) 
{
	// 从LCAppntInd表取数据
	try { fm.all('AppntCustomerNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppntRelationToInsured').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppntPassword').value = cArr[5]; } catch(ex) { };
	try { fm.all('AppntName').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntSex').value = cArr[7]; } catch(ex) { };
	try { fm.all('AppntBirthday').value = cArr[8]; } catch(ex) { };
	try { fm.all('AppntNativePlace').value = cArr[9]; } catch(ex) { };
	try { fm.all('AppntNationality').value = cArr[10]; } catch(ex) { };
	try { fm.all('AppntMarriage').value = cArr[11]; } catch(ex) { };
	try { fm.all('AppntMarriageDate').value = cArr[12]; } catch(ex) { };
	try { fm.all('AppntOccupationType').value = cArr[13]; } catch(ex) { };
	try { fm.all('AppntStartWorkDate').value = cArr[14]; } catch(ex) { };
	try { fm.all('AppntSalary').value = cArr[15]; } catch(ex) { };
	try { fm.all('AppntHealth').value = cArr[16]; } catch(ex) { };
	try { fm.all('AppntStature').value = cArr[17]; } catch(ex) { };
	try { fm.all('AppntAvoirdupois').value = cArr[18]; } catch(ex) { };
	try { fm.all('AppntCreditGrade').value = cArr[19]; } catch(ex) { };
	try { fm.all('AppntIDType').value = cArr[20]; } catch(ex) { };
	try { fm.all('AppntProterty').value = cArr[21]; } catch(ex) { };
	try { fm.all('AppntIDNo').value = cArr[22]; } catch(ex) { };
	try { fm.all('AppntOthIDType').value = cArr[23]; } catch(ex) { };
	try { fm.all('AppntOthIDNo').value = cArr[24]; } catch(ex) { };
	try { fm.all('AppntICNo').value = cArr[25]; } catch(ex) { };
	try { fm.all('AppntHomeAddressCode').value = cArr[26]; } catch(ex) { };
	try { fm.all('AppntHomeAddress').value = cArr[27]; } catch(ex) { };
	try { fm.all('AppntPostalAddress').value = cArr[28]; } catch(ex) { };
	try { fm.all('AppntZipCode').value = cArr[29]; } catch(ex) { };
	try { fm.all('AppntPhone').value = cArr[30]; } catch(ex) { };
	try { fm.all('AppntBP').value = cArr[31]; } catch(ex) { };
	try { fm.all('AppntMobile').value = cArr[32]; } catch(ex) { };
	try { fm.all('AppntEMail').value = cArr[33]; } catch(ex) { };
	try { fm.all('AppntJoinCompanyDate').value = cArr[34]; } catch(ex) { };
	try { fm.all('AppntPosition').value = cArr[35]; } catch(ex) { };
	try { fm.all('AppntGrpNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('AppntGrpName').value = cArr[37]; } catch(ex) { };
	try { fm.all('AppntGrpPhone').value = cArr[38]; } catch(ex) { };
	try { fm.all('AppntGrpAddressCode').value = cArr[39]; } catch(ex) { };
	try { fm.all('AppntGrpAddress').value = cArr[40]; } catch(ex) { };
	try { fm.all('AppntDeathDate').value = cArr[41]; } catch(ex) { };
	try { fm.all('AppntRemark').value = cArr[42]; } catch(ex) { };
	try { fm.all('AppntState').value = cArr[43]; } catch(ex) { };
	try { fm.all('AppntWorkType').value = cArr[46]; } catch(ex) { };
	try { fm.all('AppntPluralityType').value = cArr[47]; } catch(ex) { };
	try { fm.all('AppntOccupationCode').value = cArr[48]; } catch(ex) { };
	try { fm.all('AppntDegree').value = cArr[49]; } catch(ex) { };
	try { fm.all('AppntGrpZipCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('AppntSmokeFlag').value = cArr[51]; } catch(ex) { };
	try { fm.all('AppntRgtAddress').value = cArr[52]; } catch(ex) { };
	try { fm.all('AppntHomeZipCode').value = cArr[53]; } catch(ex) { };
	try { fm.all('AppntPhone2').value = cArr[54]; } catch(ex) { };

}

/*********************************************************************
 *  把保单中的投保人数据显示到投保人部分
 *  参数  ：  集体客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolAppntGrp( cArr )
{
	// 从LCAppntGrp表取数据
	try { fm.all('AppntPolNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('AppntGrpNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppntRelationToInsured').value = cArr[2]; } catch(ex) { };
	try { fm.all('AppntAppntGrade').value = cArr[3]; } catch(ex) { };
	try { fm.all('AppntPassword').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppntGrpName').value = cArr[5]; } catch(ex) { };
	try { fm.all('AppntGrpAddressCode').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntGrpAddress').value = cArr[7]; } catch(ex) { };
	try { fm.all('AppntGrpZipCode').value = cArr[8]; } catch(ex) { };
	try { fm.all('AppntBusinessType').value = cArr[9]; } catch(ex) { };
	try { fm.all('AppntGrpNature').value = cArr[10]; } catch(ex) { };
	try { fm.all('AppntPeoples').value = cArr[11]; } catch(ex) { };
	try { fm.all('AppntRgtMoney').value = cArr[12]; } catch(ex) { };
	try { fm.all('AppntAsset').value = cArr[13]; } catch(ex) { };
	try { fm.all('AppntNetProfitRate').value = cArr[14]; } catch(ex) { };
	try { fm.all('AppntMainBussiness').value = cArr[15]; } catch(ex) { };
	try { fm.all('AppntCorporation').value = cArr[16]; } catch(ex) { };
	try { fm.all('AppntComAera').value = cArr[17]; } catch(ex) { };
	try { fm.all('AppntLinkMan1').value = cArr[18]; } catch(ex) { };
	try { fm.all('AppntDepartment1').value = cArr[19]; } catch(ex) { };
	try { fm.all('AppntHeadShip1').value = cArr[20]; } catch(ex) { };
	try { fm.all('AppntPhone1').value = cArr[21]; } catch(ex) { };
	try { fm.all('AppntE_Mail1').value = cArr[22]; } catch(ex) { };
	try { fm.all('AppntFax1').value = cArr[23]; } catch(ex) { };
	try { fm.all('AppntLinkMan2').value = cArr[24]; } catch(ex) { };
	try { fm.all('AppntDepartment2').value = cArr[25]; } catch(ex) { };
	try { fm.all('AppntHeadShip2').value = cArr[26]; } catch(ex) { };
	try { fm.all('AppntPhone2').value = cArr[27]; } catch(ex) { };
	try { fm.all('AppntE_Mail2').value = cArr[28]; } catch(ex) { };
	try { fm.all('AppntFax2').value = cArr[29]; } catch(ex) { };
	try { fm.all('AppntFax').value = cArr[30]; } catch(ex) { };
	try { fm.all('AppntPhone').value = cArr[31]; } catch(ex) { };
	try { fm.all('AppntGetFlag').value = cArr[32]; } catch(ex) { };
	try { fm.all('AppntSatrap').value = cArr[33]; } catch(ex) { };
	try { fm.all('AppntEMail').value = cArr[34]; } catch(ex) { };
	try { fm.all('AppntFoundDate').value = cArr[35]; } catch(ex) { };
	try { fm.all('AppntBankAccNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('AppntBankCode').value = cArr[37]; } catch(ex) { };
	try { fm.all('AppntGrpGroupNo').value = cArr[38]; } catch(ex) { };
	try { fm.all('AppntState').value = cArr[39]; } catch(ex) { };
	try { fm.all('AppntRemark').value = cArr[40]; } catch(ex) { };
	try { fm.all('AppntBlacklistFlag').value = cArr[41]; } catch(ex) { };
	try { fm.all('AppntOperator').value = cArr[42]; } catch(ex) { };
	try { fm.all('AppntMakeDate').value = cArr[43]; } catch(ex) { };
	try { fm.all('AppntMakeTime').value = cArr[44]; } catch(ex) { };
	try { fm.all('AppntModifyDate').value = cArr[45]; } catch(ex) { };
	try { fm.all('AppntModifyTime').value = cArr[46]; } catch(ex) { };
	try { fm.all('AppntFIELDNUM').value = cArr[47]; } catch(ex) { };
	try { fm.all('AppntPK').value = cArr[48]; } catch(ex) { };
	try { fm.all('AppntfDate').value = cArr[49]; } catch(ex) { };
	try { fm.all('AppntmErrors').value = cArr[50]; } catch(ex) { };
}

/*********************************************************************
 *  把保单中的被保人数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolInsured(cArr) 
{
	// 从LCInsured表取数据
	try { fm.all('CustomerNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('SequenceNo').value = cArr[2]; } catch(ex) { };
	try { fm.all('InsuredGrade').value = cArr[3]; } catch(ex) { };
	try { fm.all('RelationToInsured').value = cArr[4]; } catch(ex) { };
	try { fm.all('Password').value = cArr[5]; } catch(ex) { };
	try { fm.all('Name').value = cArr[6]; } catch(ex) { };
	try { fm.all('Sex').value = cArr[7]; } catch(ex) { };
	try { fm.all('Birthday').value = cArr[8]; } catch(ex) { };
	try { fm.all('NativePlace').value = cArr[9]; } catch(ex) { };
	try { fm.all('Nationality').value = cArr[10]; } catch(ex) { };
	try { fm.all('Marriage').value = cArr[11]; } catch(ex) { };
	try { fm.all('MarriageDate').value = cArr[12]; } catch(ex) { };
	try { fm.all('OccupationType').value = cArr[13]; } catch(ex) { };
	try { fm.all('StartWorkDate').value = cArr[14]; } catch(ex) { };
	try { fm.all('Salary').value = cArr[15]; } catch(ex) { };
	try { fm.all('Health').value = cArr[16]; } catch(ex) { };
	try { fm.all('Stature').value = cArr[17]; } catch(ex) { };
	try { fm.all('Avoirdupois').value = cArr[18]; } catch(ex) { };
	try { fm.all('CreditGrade').value = cArr[19]; } catch(ex) { };
	try { fm.all('IDType').value = cArr[20]; } catch(ex) { };
	try { fm.all('Proterty').value = cArr[21]; } catch(ex) { };
	try { fm.all('IDNo').value = cArr[22]; } catch(ex) { };
	try { fm.all('OthIDType').value = cArr[23]; } catch(ex) { };
	try { fm.all('OthIDNo').value = cArr[24]; } catch(ex) { };
	try { fm.all('ICNo').value = cArr[25]; } catch(ex) { };
	try { fm.all('HomeAddressCode').value = cArr[26]; } catch(ex) { };
	try { fm.all('HomeAddress').value = cArr[27]; } catch(ex) { };
	try { fm.all('PostalAddress').value = cArr[28]; } catch(ex) { };
	try { fm.all('ZipCode').value = cArr[29]; } catch(ex) { };
	try { fm.all('Phone').value = cArr[30]; } catch(ex) { };
	try { fm.all('BP').value = cArr[31]; } catch(ex) { };
	try { fm.all('Mobile').value = cArr[32]; } catch(ex) { };
	try { fm.all('EMail').value = cArr[33]; } catch(ex) { };
	try { fm.all('JoinCompanyDate').value = cArr[34]; } catch(ex) { };
	try { fm.all('Position').value = cArr[35]; } catch(ex) { };
	try { fm.all('GrpNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('GrpName').value = cArr[37]; } catch(ex) { };
	try { fm.all('GrpPhone').value = cArr[38]; } catch(ex) { };
	try { fm.all('GrpAddressCode').value = cArr[39]; } catch(ex) { };
	try { fm.all('GrpAddress').value = cArr[40]; } catch(ex) { };
	try { fm.all('DeathDate').value = cArr[41]; } catch(ex) { };
	try { fm.all('State').value = cArr[43]; } catch(ex) { };
	try { fm.all('WorkType').value = cArr[46]; } catch(ex) { };
	try { fm.all('PluralityType').value = cArr[47]; } catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[48]; } catch(ex) { };
	try { fm.all('Degree').value = cArr[49]; } catch(ex) { };
	try { fm.all('GrpZipCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('SmokeFlag').value = cArr[51]; } catch(ex) { };
	try { fm.all('RgtAddress').value = cArr[52]; } catch(ex) { };
	try { fm.all('HomeZipCode').value = cArr[53]; } catch(ex) { };
	try { fm.all('Phone2').value = cArr[54]; } catch(ex) { };
	return;

}

/*********************************************************************
 *  把查询返回的客户数据显示到连带被保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displaySubInsured()
{
	fm.all( spanObj ).all( 'SubInsuredGrid1' ).value = arrResult[0][0];
	fm.all( spanObj ).all( 'SubInsuredGrid2' ).value = arrResult[0][2];
	fm.all( spanObj ).all( 'SubInsuredGrid3' ).value = arrResult[0][3];
	fm.all( spanObj ).all( 'SubInsuredGrid4' ).value = arrResult[0][4];
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {
	if( arrQueryResult != null ) {
		arrResult = arrQueryResult;

		if( mOperate == 1 )	{           // 查询保单明细
			var tPolNo = arrResult[0][0];
			
			// 查询保单明细
			queryPolDetail( tPolNo );
		}
		
		if( mOperate == 2 ) {		// 投保人信息	  
			arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
			if (arrResult == null) {
			  alert("未查到投保人信息");
			} else {
			   displayAppnt(arrResult[0]);
			}

	  }
		if( mOperate == 3 )	{		// 主被保人信息
			arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
			if (arrResult == null) {
			  alert("未查到主被保人信息");
			} else {
			   displayInsured(arrResult[0]);
			}

	  }
		if( mOperate == 4 )	{		// 连带被保人信息
			displaySubInsured(arrResult[0]);
	  }
	}
	mOperate = 0;		// 恢复初态
	
	emptyUndefined(); 
}

/*********************************************************************
 *  根据查询返回的信息查询投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPolDetail( cPolNo )
{

	emptyForm();
	//var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	//var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  

	//showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	//parent.fraSubmit.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;
	parent.fraTitle.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//*************************************************************
//被保人客户号查询按扭事件
function queryInsuredNo() {
  if (fm.all("CustomerNo").value == "") {
    showInsured1();
  //} else if (loadFlag != "1" && loadFlag != "2") {
  //  alert("只能在投保单录入时进行操作！");
  }  else {
    arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + fm.all("CustomerNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到主被保人信息");
      displayInsured(new Array());
      emptyUndefined(); 
    } else {
      displayInsured(arrResult[0]);
    }
  }
}

//*************************************************************
//投保人客户号查询按扭事件
function queryAppntNo() {
  if (fm.all("AppntCustomerNo").value == "" && loadFlag == "1") {
    showAppnt1();
  //} else if (loadFlag != "1" && loadFlag != "2") {
  //  alert("只能在投保单录入时进行操作！");
  } else {
    arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + fm.all("AppntCustomerNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到投保人信息");
      displayAppnt(new Array());
      emptyUndefined(); 
    } else {
      displayAppnt(arrResult[0]);
    }
  }
}

//*************************************************************
//投保人与被保人相同选择框事件
function isSamePerson() {
  //对应未选同一人，又打钩的情况
  if (fm.AppntRelationToInsured.value!="00" && fm.SamePersonFlag.checked==true) {
    fm.all('DivLCInsured').style.display = "";
    fm.SamePersonFlag.checked = false;
    alert("投保人与被保人关系不是本人，不能进行该操作！");
  }
  //对应是同一人，又打钩的情况
  else if (fm.SamePersonFlag.checked == true) {
    fm.all('DivLCInsured').style.display = "none";
  }
  //对应不选同一人的情况
  else if (fm.SamePersonFlag.checked == false) {
    fm.all('DivLCInsured').style.display = "";  
  }  
  
  for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {    
	  if (fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
	    try {
	      insuredName = fm.elements[elementsNum].name.substring(fm.elements[elementsNum].name.indexOf("t") + 1);
	      if (fm.all('DivLCInsured').style.display == "none") {
	        fm.all(insuredName).value = fm.elements[elementsNum].value;
	      }
	      else {
	        fm.all(insuredName).value = "";
	      }
	    }
	    catch (ex) {}
	  }
	}

}  

//*************************************************************
//保存时校验投保人与被保人相同选择框事件
function verifySamePerson() {
  if (fm.SamePersonFlag.checked == true) {
    for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {    
  	  if (fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
  	    try {
  	      insuredName = fm.elements[elementsNum].name.substring(fm.elements[elementsNum].name.indexOf("t") + 1);
  	      if (fm.all('DivLCInsured').style.display == "none") {
  	        fm.all(insuredName).value = fm.elements[elementsNum].value;
  	      }
  	      else {
  	        fm.all(insuredName).value = "";
  	      }
  	    }
  	    catch (ex) {}
  	  }
	  } 
  }
  else if (fm.SamePersonFlag.checked == false) { 

  }  
	
}  


/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt(cArr) 
{
	// 从LDPerson表取数据
	try { fm.all('AppntCustomerNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('AppntPassword').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppntName').value = cArr[2]; } catch(ex) { };
	try { fm.all('AppntSex').value = cArr[3]; } catch(ex) { };
	try { fm.all('AppntBirthday').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppntNativePlace').value = cArr[5]; } catch(ex) { };
	try { fm.all('AppntNationality').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntMarriage').value = cArr[7]; } catch(ex) { };
	try { fm.all('AppntMarriageDate').value = cArr[8]; } catch(ex) { };
	try { fm.all('AppntOccupationType').value = cArr[9]; } catch(ex) { };
	try { fm.all('AppntStartWorkDate').value = cArr[10]; } catch(ex) { };
	try { fm.all('AppntSalary').value = cArr[11]; } catch(ex) { };
	try { fm.all('AppntHealth').value = cArr[12]; } catch(ex) { };
	try { fm.all('AppntStature').value = cArr[13]; } catch(ex) { };
	try { fm.all('AppntAvoirdupois').value = cArr[14]; } catch(ex) { };
	try { fm.all('AppntCreditGrade').value = cArr[15]; } catch(ex) { };
	try { fm.all('AppntIDType').value = cArr[16]; } catch(ex) { };
	try { fm.all('AppntProterty').value = cArr[17]; } catch(ex) { };
	try { fm.all('AppntIDNo').value = cArr[18]; } catch(ex) { };
	try { fm.all('AppntOthIDType').value = cArr[19]; } catch(ex) { };
	try { fm.all('AppntOthIDNo').value = cArr[20]; } catch(ex) { };
	try { fm.all('AppntICNo').value = cArr[21]; } catch(ex) { };
	try { fm.all('AppntHomeAddressCode').value = cArr[22]; } catch(ex) { };
	try { fm.all('AppntHomeAddress').value = cArr[23]; } catch(ex) { };
	try { fm.all('AppntPostalAddress').value = cArr[24]; } catch(ex) { };
	try { fm.all('AppntZipCode').value = cArr[25]; } catch(ex) { };
	try { fm.all('AppntPhone').value = cArr[26]; } catch(ex) { };
	try { fm.all('AppntBP').value = cArr[27]; } catch(ex) { };
	try { fm.all('AppntMobile').value = cArr[28]; } catch(ex) { };
	try { fm.all('AppntEMail').value = cArr[29]; } catch(ex) { };
	try { fm.all('AppntBankCode').value = cArr[30]; } catch(ex) { };
	try { fm.all('AppntBankAccNo').value = cArr[31]; } catch(ex) { };
	try { fm.all('AppntJoinCompanyDate').value = cArr[32]; } catch(ex) { };
	try { fm.all('AppntPosition').value = cArr[33]; } catch(ex) { };
	try { fm.all('AppntGrpNo').value = cArr[34]; } catch(ex) { };
	try { fm.all('AppntGrpName').value = cArr[35]; } catch(ex) { };
	try { fm.all('AppntGrpPhone').value = cArr[36]; } catch(ex) { };
	try { fm.all('AppntGrpAddressCode').value = cArr[37]; } catch(ex) { };
	try { fm.all('AppntGrpAddress').value = cArr[38]; } catch(ex) { };
	try { fm.all('AppntDeathDate').value = cArr[39]; } catch(ex) { };
	try { fm.all('AppntRemark').value = cArr[40]; } catch(ex) { };
	try { fm.all('AppntState').value = cArr[41]; } catch(ex) { };
	try { fm.all('AppntWorkType').value = cArr[48]; } catch(ex) { };
	try { fm.all('AppntPluralityType').value = cArr[49]; } catch(ex) { };
	try { fm.all('AppntOccupationCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('AppntDegree').value = cArr[51]; } catch(ex) { };
	try { fm.all('AppntGrpZipCode').value = cArr[52]; } catch(ex) { };
	try { fm.all('AppntSmokeFlag').value = cArr[53]; } catch(ex) { };
	try { fm.all('AppntRgtAddress').value = cArr[54]; } catch(ex) { };

}

/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  集体客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayAppntGrp( cArr )
{
	// 从LDGrp表取数据
	try { fm.all('AppGrpNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('Password').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppGrpName').value = cArr[2]; } catch(ex) { };
	try { fm.all('GrpAddressCode').value = cArr[3]; } catch(ex) { };
	try { fm.all('AppGrpAddress').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppGrpZipCode').value = cArr[5]; } catch(ex) { };
	try { fm.all('BusinessType').value = cArr[6]; } catch(ex) { };
	try { fm.all('GrpNature').value = cArr[7]; } catch(ex) { };
	try { fm.all('Peoples').value = cArr[8]; } catch(ex) { };
	try { fm.all('RgtMoney').value = cArr[9]; } catch(ex) { };
	try { fm.all('Asset').value = cArr[10]; } catch(ex) { };
	try { fm.all('NetProfitRate').value = cArr[11]; } catch(ex) { };
	try { fm.all('MainBussiness').value = cArr[12]; } catch(ex) { };
	try { fm.all('Corporation').value = cArr[13]; } catch(ex) { };
	try { fm.all('ComAera').value = cArr[14]; } catch(ex) { };
	try { fm.all('LinkMan1').value = cArr[15]; } catch(ex) { };
	try { fm.all('Department1').value = cArr[16]; } catch(ex) { };
	try { fm.all('HeadShip1').value = cArr[17]; } catch(ex) { };
	try { fm.all('Phone1').value = cArr[18]; } catch(ex) { };
	try { fm.all('E_Mail1').value = cArr[19]; } catch(ex) { };
	try { fm.all('Fax1').value = cArr[20]; } catch(ex) { };
	try { fm.all('LinkMan2').value = cArr[21]; } catch(ex) { };
	try { fm.all('Department2').value = cArr[22]; } catch(ex) { };
	try { fm.all('HeadShip2').value = cArr[23]; } catch(ex) { };
	try { fm.all('Phone2').value = cArr[24]; } catch(ex) { };
	try { fm.all('E_Mail2').value = cArr[25]; } catch(ex) { };
	try { fm.all('Fax2').value = cArr[26]; } catch(ex) { };
	try { fm.all('Fax').value = cArr[27]; } catch(ex) { };
	try { fm.all('Phone').value = cArr[28]; } catch(ex) { };
	try { fm.all('GetFlag').value = cArr[29]; } catch(ex) { };
	try { fm.all('Satrap').value = cArr[30]; } catch(ex) { };
	try { fm.all('EMail').value = cArr[31]; } catch(ex) { };
	try { fm.all('FoundDate').value = cArr[32]; } catch(ex) { };
	try { fm.all('BankAccNo').value = cArr[33]; } catch(ex) { };
	try { fm.all('BankCode').value = cArr[34]; } catch(ex) { };
	try { fm.all('GrpGroupNo').value = cArr[35]; } catch(ex) { };
	try { fm.all('State').value = cArr[36]; } catch(ex) { };
}

/*********************************************************************
 *  把查询返回的客户数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayInsured(cArr) 
{
	// 从LDPerson表取数据
	try { fm.all('CustomerNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('Password').value = cArr[1]; } catch(ex) { };
	try { fm.all('Name').value = cArr[2]; } catch(ex) { };
	try { fm.all('Sex').value = cArr[3]; } catch(ex) { };
	try { fm.all('Birthday').value = cArr[4]; } catch(ex) { };
	try { fm.all('NativePlace').value = cArr[5]; } catch(ex) { };
	try { fm.all('Nationality').value = cArr[6]; } catch(ex) { };
	try { fm.all('Marriage').value = cArr[7]; } catch(ex) { };
	try { fm.all('MarriageDate').value = cArr[8]; } catch(ex) { };
	try { fm.all('OccupationType').value = cArr[9]; } catch(ex) { };
	try { fm.all('StartWorkDate').value = cArr[10]; } catch(ex) { };
	try { fm.all('Salary').value = cArr[11]; } catch(ex) { };
	try { fm.all('Health').value = cArr[12]; } catch(ex) { };
	try { fm.all('Stature').value = cArr[13]; } catch(ex) { };
	try { fm.all('Avoirdupois').value = cArr[14]; } catch(ex) { };
	try { fm.all('CreditGrade').value = cArr[15]; } catch(ex) { };
	try { fm.all('IDType').value = cArr[16]; } catch(ex) { };
	try { fm.all('Proterty').value = cArr[17]; } catch(ex) { };
	try { fm.all('IDNo').value = cArr[18]; } catch(ex) { };
	try { fm.all('OthIDType').value = cArr[19]; } catch(ex) { };
	try { fm.all('OthIDNo').value = cArr[20]; } catch(ex) { };
	try { fm.all('ICNo').value = cArr[21]; } catch(ex) { };
	try { fm.all('HomeAddressCode').value = cArr[22]; } catch(ex) { };
	try { fm.all('HomeAddress').value = cArr[23]; } catch(ex) { };
	try { fm.all('PostalAddress').value = cArr[24]; } catch(ex) { };
	try { fm.all('ZipCode').value = cArr[25]; } catch(ex) { };
	try { fm.all('Phone').value = cArr[26]; } catch(ex) { };
	try { fm.all('BP').value = cArr[27]; } catch(ex) { };
	try { fm.all('Mobile').value = cArr[28]; } catch(ex) { };
	try { fm.all('EMail').value = cArr[29]; } catch(ex) { };
	try { fm.all('BankCode').value = cArr[30]; } catch(ex) { };
	try { fm.all('BankAccNo').value = cArr[31]; } catch(ex) { };
	try { fm.all('JoinCompanyDate').value = cArr[32]; } catch(ex) { };
	try { fm.all('Position').value = cArr[33]; } catch(ex) { };
	try { fm.all('GrpNo').value = cArr[34]; } catch(ex) { };
	try { fm.all('GrpName').value = cArr[35]; } catch(ex) { };
	try { fm.all('GrpPhone').value = cArr[36]; } catch(ex) { };
	try { fm.all('GrpAddressCode').value = cArr[37]; } catch(ex) { };
	try { fm.all('GrpAddress').value = cArr[38]; } catch(ex) { };
	try { fm.all('DeathDate').value = cArr[39]; } catch(ex) { };
	try { fm.all('State').value = cArr[41]; } catch(ex) { };
	try { fm.all('WorkType').value = cArr[48]; } catch(ex) { };
	try { fm.all('PluralityType').value = cArr[49]; } catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('Degree').value = cArr[51]; } catch(ex) { };
	try { fm.all('GrpZipCode').value = cArr[52]; } catch(ex) { };
	try { fm.all('SmokeFlag').value = cArr[53]; } catch(ex) { };
	try { fm.all('RgtAddress').value = cArr[54]; } catch(ex) { };

}

//*********************************************************************
function showAppnt1()
{
	if( mOperate == 0 )
	{
		mOperate = 2;
		showInfo = window.open( "../sys/LDPersonQuery.html" );
	}
}           

//*********************************************************************
function showInsured1()
{
	if( mOperate == 0 )
	{
		mOperate = 3;
		showInfo = window.open( "../sys/LDPersonQuery.html" );
	}
}  

function isSamePersonQuery() {
  fm.SamePersonFlag.checked = true;
  //divSamePerson.style.display = "none";
  DivLCInsured.style.display = "none";
}

//问题件录入
function QuestInput()
{
	if(inputQuestButton.disabled == true)
	   return;
	cProposalNo = fm.ProposalNo.value;  //保单号码
	if(cProposalNo == "")
	{
		alert("尚无投保单号，请先保存!");
	}
	else
	{	
		window.open("../uw/QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	}
}

//显示投保人年龄
function showAppntAge() {
  var age = calAge(fm.all("AppntBirthday").value);
  var today = new Date();
  
  fm.all("AppntBirthday").title = "投保人到今天 " + today.toLocaleString() 
                                + " \n的年龄为：" + age + " 岁!";
}

//显示被保人年龄
function showAge() {
  var age = calAge(fm.all("Birthday").value);
  var today = new Date();
  
  fm.all("Birthday").title = "被保人到今天 " + today.toLocaleString() 
                           + " \n的年龄为：" + age + " 岁!";
}

//检验投保人出生日期，如果空，且身份证号有，则从身份证取。取不到返回空格;
function checkBirthday()
{
	try{
		  var strBrithday = "";
		  if(trim(fm.all("AppntBirthday").value)==""||fm.all("AppntBirthday").value==null)
		  {
		  	if (trim(fm.all("AppntIDType").value) == "0") 
		  	 {
		  	   strBrithday=	getBirthdatByIdNo(fm.all("AppntIDNo").value);
		  	   if(strBrithday=="") passVerify=false;
		  	   
	           fm.all("AppntBirthday").value= strBrithday;
		  	 }	     
	      }	
	 }
	 catch(e)
	 {
	 	
	 }
}

//校验录入的险种是否不需要校验，如果需要返回true,否则返回false
function needVerifyRiskcode()
{
  	  
  try { 
  	   var riskcode=fm.all("RiskCode").value;
 	   
       var tSql = "select Sysvarvalue from LDSysVar where Sysvar='NotVerifyRiskcode'";          
       var tResult = easyExecSql(tSql, 1, 1, 1);       
       var strRiskcode = tResult[0][0];
       var strValue=strRiskcode.split("/");
       var i=0;
	   while(i<strValue.length)
	   {
	   	if(riskcode==strValue[i])
	   	{
           return false;
	   	}
	   	i++;
	   }   	   
  	 }
  	catch(e)
  	 {}
  	 
  	return true; 
  	
	
}


/*********************************************************************
 *  初始化特殊的附险信息-121301和其它险种的初始化不一样
 *  参数  ：  投保单号
 *  返回值：  无
 *********************************************************************
 */
function initPrivateRiskInfo121301(cPolNo) {
	if(cPolNo=="") {
		alert("没有主险保单号,不能进行附加险录入!");
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
		return false
	}
	
	var arrLCPol = new Array(); 
	var arrQueryResult = null;
	// 主保单信息部分
	var sql = "select * from lcpol where polno='" + cPolNo + "' "
			+ "and riskcode in "
			+ "( select riskcode from LMRiskApp where SubRiskFlag = 'M' )"; 
	
	arrQueryResult = easyExecSql( sql , 1, 0);
		
	if (arrQueryResult == null)	{
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
		
		top.mainPolNo = "";
		
		alert("读取主险信息失败,不能进行附加险录入!");
		return false
	}
	
	arrLCPol = arrQueryResult[0]; 	
	displayPol( arrLCPol );	
	displayPolSpec( arrLCPol );//初始化特殊要求的保单信息
	
	fm.all("MainPolNo").value = cPolNo;
	var tAR;
			
    //个人投保人信息
  	  arrQueryResult = null;
  	  arrQueryResult = easyExecSql("select * from lcappntind where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[26]+"'", 1, 0);
  	  tAR = arrQueryResult[0];
  	  displayPolAppnt(tAR);
  	  try { fm.all('AppntRelationToInsured').value = '00'; } catch(ex) { };
	  try { fm.all("SamePersonFlag").checked = true; } catch(ex) { };
	  try {isSamePerson();} catch(ex) { };
	  try { fm.all("SamePersonFlag").disabled=true} catch(ex) { };
	    

	// 被保人信息部分
	//	arrQueryResult = null;
  	// 	arrQueryResult = easyExecSql("select * from lcappntind where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[26]+"'", 1, 0);
  	// 	tAR = arrQueryResult[0];
	//	displayPolInsuredSpec(tAR);	
	
	
	return true;
}



/*********************************************************************
 *  把保单数组中的数据显示到特殊的险种信息显示部分-121301,
 *  参数  ：  保单的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolSpec(cArr) 
{
	try 
	{

	    try { fm.all('PayEndYear').value = cArr[109]; } catch(ex) { };
	    try { fm.all('PayEndYearFlag').value = cArr[108]; } catch(ex) { };
	    try { fm.all('PayIntv').value = cArr[57]; } catch(ex) { };
	    try { fm.all('Amnt').value = cArr[39]; } catch(ex) { };	    //主险的保费即附险的保额
	    
	} catch(ex) {
	  alert("displayPolSpec err:" + ex + "\ndata is:" + cArr);
	}
}



/*********************************************************************
 *  初始化特殊的附险信息-321601和其它险种的初始化不一样
 *  参数  ：  投保单号
 *  返回值：  无
 *********************************************************************
 */
function initPrivateRiskInfo321601(cPolNo) {
	if(cPolNo=="") {
		alert("没有主险保单号,不能进行附加险录入!");
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
		return false
	}
	
	var arrLCPol = new Array(); 
	var arrQueryResult = null;
	// 主保单信息部分
	var sql = "select * from lcpol where polno='" + cPolNo + "' "
			+ "and riskcode in "
			+ "( select riskcode from LMRiskApp where SubRiskFlag = 'M' )"; 
	
	arrQueryResult = easyExecSql( sql , 1, 0);
		
	if (arrQueryResult == null)	{
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
		
		top.mainPolNo = "";
		
		alert("读取主险信息失败,不能进行附加险录入!");
		return false
	}
	
	arrLCPol = arrQueryResult[0]; 	
	displayPol( arrLCPol );	
	
	//初始化特殊要求的保单信息--//主险的保费即附险的保额(取主险保费和500000之间小值)	
    try 
    { 
    	 if(arrLCPol[39]<500000)
    	   fm.all('Amnt').value = arrLCPol[39]; 
    	 else
    	   fm.all('Amnt').value = 500000;   
    } 
     catch(ex) { alert(ex);}	    

	
	fm.all("MainPolNo").value = cPolNo;
	var tAR;
			
  	//投保人信息
  	if (arrLCPol[28]=="2") {     //集体投保人信息
  	  arrQueryResult = null;
  	  arrQueryResult = easyExecSql("select * from lcappntgrp where polno='"+cPolNo+"'"+" and grpno='"+arrLCPol[26]+"'", 1, 0);	  		
  	  tAR = arrQueryResult[0];
  	  displayPolAppntGrp(tAR);
  	} else {                     //个人投保人信息
  	  arrQueryResult = null;
  	  arrQueryResult = easyExecSql("select * from lcappntind where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[26]+"'", 1, 0);
  	  tAR = arrQueryResult[0];
  	  displayPolAppnt(tAR);
  	}

	// 被保人信息部分
	if (arrLCPol[18] == arrLCPol[26]) {
	  fm.all("SamePersonFlag").checked = true;
		parent.fraInterface.isSamePersonQuery();
    parent.fraInterface.fm.all("CustomerNo").value = arrLCPol[18];
	}
	//else {
		arrQueryResult = null;
		arrQueryResult = easyExecSql("select * from lcinsured where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[18]+"'", 1, 0);
		tAR = arrQueryResult[0];
		displayPolInsured(tAR);	
	//}
	
	return true;
}


/*********************************************************************
 *  特殊险种处理：把主保单中的投保人数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolInsuredSpec(cArr) 
{
		// 从LCAppntInd表取数据
	try { fm.all('CustomerNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('Password').value = cArr[5]; } catch(ex) { };
	try { fm.all('Name').value = cArr[6]; } catch(ex) { };
	try { fm.all('Sex').value = cArr[7]; } catch(ex) { };
	try { fm.all('Birthday').value = cArr[8]; } catch(ex) { };
	try { fm.all('NativePlace').value = cArr[9]; } catch(ex) { };
	try { fm.all('Nationality').value = cArr[10]; } catch(ex) { };
	try { fm.all('Marriage').value = cArr[11]; } catch(ex) { };
	try { fm.all('MarriageDate').value = cArr[12]; } catch(ex) { };
	try { fm.all('OccupationType').value = cArr[13]; } catch(ex) { };
	try { fm.all('StartWorkDate').value = cArr[14]; } catch(ex) { };
	try { fm.all('Salary').value = cArr[15]; } catch(ex) { };
	try { fm.all('Health').value = cArr[16]; } catch(ex) { };
	try { fm.all('Stature').value = cArr[17]; } catch(ex) { };
	try { fm.all('Avoirdupois').value = cArr[18]; } catch(ex) { };
	try { fm.all('CreditGrade').value = cArr[19]; } catch(ex) { };
	try { fm.all('IDType').value = cArr[20]; } catch(ex) { };
	try { fm.all('Proterty').value = cArr[21]; } catch(ex) { };
	try { fm.all('IDNo').value = cArr[22]; } catch(ex) { };
	try { fm.all('OthIDType').value = cArr[23]; } catch(ex) { };
	try { fm.all('OthIDNo').value = cArr[24]; } catch(ex) { };
	try { fm.all('ICNo').value = cArr[25]; } catch(ex) { };
	try { fm.all('HomeAddressCode').value = cArr[26]; } catch(ex) { };
	try { fm.all('HomeAddress').value = cArr[27]; } catch(ex) { };
	try { fm.all('PostalAddress').value = cArr[28]; } catch(ex) { };
	try { fm.all('ZipCode').value = cArr[29]; } catch(ex) { };
	try { fm.all('Phone').value = cArr[30]; } catch(ex) { };
	try { fm.all('BP').value = cArr[31]; } catch(ex) { };
	try { fm.all('Mobile').value = cArr[32]; } catch(ex) { };
	try { fm.all('EMail').value = cArr[33]; } catch(ex) { };
	//try { fm.all('BankCode').value = cArr[34]; } catch(ex) { };
	//try { fm.all('BankAccNo').value = cArr[35]; } catch(ex) { };
	try { fm.all('JoinCompanyDate').value = cArr[34]; } catch(ex) { };
	try { fm.all('Position').value = cArr[35]; } catch(ex) { };
	try { fm.all('GrpNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('GrpName').value = cArr[37]; } catch(ex) { };
	try { fm.all('GrpPhone').value = cArr[38]; } catch(ex) { };
	try { fm.all('GrpAddressCode').value = cArr[39]; } catch(ex) { };
	try { fm.all('GrpAddress').value = cArr[40]; } catch(ex) { };
	try { fm.all('DeathDate').value = cArr[41]; } catch(ex) { };
	try { fm.all('State').value = cArr[43]; } catch(ex) { };
	try { fm.all('WorkType').value = cArr[46]; } catch(ex) { };
	try { fm.all('PluralityType').value = cArr[47]; } catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[49]; } catch(ex) { };
	try { fm.all('Degree').value = cArr[48]; } catch(ex) { };
	try { fm.all('GrpZipCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('SmokeFlag').value = cArr[51]; } catch(ex) { };
	try { fm.all('RgtAddress').value = cArr[52]; } catch(ex) { };

}


//险种页面数据提交时对特殊险种的特殊处理
function specDealByRisk()
{
	//如果是同心卡-阳光旅程险
	if(fm.all('RiskCode').value=='311603')
	{
	   if(trim(fm.all("AppntBirthday").value)==""||fm.all("AppntBirthday").value==null)
	   {
	   	if (trim(fm.all("AppntIDType").value) != "0"||fm.all("AppntIDNo").value==null||trim(fm.all("AppntIDNo").value)=="") 
	   	{
	   		fm.all("AppntBirthday").value='1970-1-1';
	   	}
	   }
	
		try
		{
			  var strBrithday = "";
			  if(trim(fm.all("Birthday").value)==""||fm.all("Birthday").value==null)
			  {
			  	if (trim(fm.all("IDType").value) == "0") 
			  	 {
			  	   strBrithday=	getBirthdatByIdNo(fm.all("IDNo").value);
			  	   if(strBrithday=="") passVerify=false;
			  	   
		           fm.all("Birthday").value= strBrithday;
			  	 }	     
		      }	
		 }
		 catch(e)
		 {
		}
		return true;
	}
	//如果是团体商业补充医疗险
	if(fm.all('RiskCode').value=='211801')
	{
		//可以对险种条件校验
		var strChooseDuty="";
		for(i=0;i<=8;i++)
		{
			if(DutyGrid.getChkNo(i)==true)
			{
				strChooseDuty=strChooseDuty+"1";
				DutyGrid.setRowColData(i, 5, fm.all('PayEndYear').value);//交费年期
				DutyGrid.setRowColData(i, 6, fm.all('PayEndYearFlag').value);//交费年期单位
				DutyGrid.setRowColData(i, 9, fm.all('InsuYear').value);//保险年期
				DutyGrid.setRowColData(i, 10, fm.all('InsuYearFlag').value);//保险年期单位
				DutyGrid.setRowColData(i, 11, fm.all('PayIntv').value);//缴费方式
			}
			else
			{
				strChooseDuty=strChooseDuty+"0";
			}
		}
		//alert(strChooseDuty);
		//fm.all('StandbyFlag1').value=strChooseDuty;
		return true;		
	}
	//如果是民生基业长青员工福利计划 add by guoxiang 2004-9-8 10:24
	if(fm.all('RiskCode').value=='211701')
	{
		//可以对险种条件校验
		var strChooseDuty="";
		for(i=0;i<=2;i++)
		{
			if(DutyGrid.getChkNo(i)==true)
			{
				strChooseDuty=strChooseDuty*1+1.0;
                DutyGrid.setRowColData(i, 3, fm.all('Prem').value);//保费
				DutyGrid.setRowColData(i, 9, fm.all('InsuYear').value);//保险年期
				DutyGrid.setRowColData(i, 10, fm.all('InsuYearFlag').value);//保险年期单位
				DutyGrid.setRowColData(i, 11, fm.all('PayIntv').value);//缴费方式
				DutyGrid.setRowColData(i, 12, fm.all('ManageFeeRate').value);//管理费比例
		
			}
			else
			{
				strChooseDuty=strChooseDuty*1+0.0;
			}
		}
		if(strChooseDuty>1){
		   alert("基业长青员工福利每张保单只允许选择一个责任，您选择的责任次数为"+strChooseDuty+"，请修改！！！");
		   return false;  		
	    }
		return true;		
	}
	
	//如果是个人长瑞险
	if(fm.all('RiskCode').value=='112401')	
	{
		if(fm.all('GetYear').value!=''&&fm.all('InsuYear').value!='')
		{
		  	if(fm.all('InsuYear').value=='A')
		  	{
		  		fm.all('InsuYear').value='88';
		  		fm.all('InsuYearFlag').value='A';		  		
		  	}
		  	else if(fm.all('InsuYear').value=='B')
		  	{
		  		fm.all('InsuYear').value=20+Number(fm.all('GetYear').value);
		  		fm.all('InsuYearFlag').value='A';		  		
		  	}
		  	else
		  	{
		  		alert("保险期间必须选择！");
		  		return false;
		  		
		  	}
		  	if(fm.all('PayIntv').value=='0')
		  	{
		  		fm.all('PayEndYear').value=fm.all('InsuYear').value;
		  		fm.all('PayEndYearFlag').value=fm.all('InsuYearFlag').value;		  		
		  	}
		  	
		}
		return true;
	}
	//如果是君安行险种
	if(fm.all('RiskCode').value=='241801')	
	{
		try
		{
			var InsurCount = fm.all('StandbyFlag2').value;			
			if(InsurCount>4||InsurCount<0)
			{
				alert("连带被保人人数不能超过4人");
				return false;	
			}
			SubInsuredGrid.delBlankLine("SubInsuredGrid"); 
			var rowNum=SubInsuredGrid.mulLineCount;
			if(InsurCount!=rowNum)
			{
			    alert("连带被保人人数和多行输入的连带被保人信息的行数不符合！ ");
				return false;		
			}			
			
	    }
	    catch(ex)
	    {
	      alert(ex);
	      return false;	
	    }
	    return true;		
	}
	
	
}

//险种页面初始化时对特殊险种的特殊处理
function initDealForSpecRisk(cRiskCode)
{
  try{	
	//如果是211801
	if(cRiskCode=='211801')
	{
		DutyGrid.addOne();
		DutyGrid.setRowColData(0, 1, '610001');
		DutyGrid.setRowColData(0, 2, '基本责任1档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(1, 1, '610002');
		DutyGrid.setRowColData(1, 2, '基本责任2档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(2, 1, '610003');
		DutyGrid.setRowColData(2, 2, '基本责任3档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(3, 1, '610004');
		DutyGrid.setRowColData(3, 2, '基本责任4档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(4, 1, '610005');
		DutyGrid.setRowColData(4, 2, '基本责任5档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(5, 1, '610006');
		DutyGrid.setRowColData(5, 2, '基本责任6档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(6, 1, '610007');
		DutyGrid.setRowColData(6, 2, '公共责任7档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(7, 1, '610008');
		DutyGrid.setRowColData(7, 2, '公共责任8档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(8, 1, '610009');
		DutyGrid.setRowColData(8, 2, '女员工生育责任');
		DutyGrid.lock();

		
	}
	
	//众悦年金
	if(cRiskCode=='212401')
	{

		PremGrid.addOne();
		PremGrid.setRowColData(0, 1, '601001');
		PremGrid.setRowColData(0, 2, '601101');
		PremGrid.setRowColData(0, 3, '集体交费');
		PremGrid.addOne();
		PremGrid.setRowColData(1, 1, '601001');
		PremGrid.setRowColData(1, 2, '601102');
		PremGrid.setRowColData(1, 3, '个人交费');
		PremGrid.lock();		

	}

    //基业长青
	if(cRiskCode=='211701')
	{
        var strSql = "select *  from lmdutypayrela where dutycode in  "
	               + " (select dutycode from lmriskduty where riskcode='"+cRiskCode+"')";
	    turnPage.queryModal(strSql, PremGrid);  
	    PremGrid.lock;    
    } 
  
  }catch(ex) {}
       
}

/*********************************************************************
* 险种页面初始化时对特殊险种的特殊处理扩展
*  add by guoxiang  at 2004-9-6 16:21
*  for update up function initDealForSpecRisk
*  not write function for every risk
*********************************************************************
 */
function initDealForSpecRiskEx(cRiskCode)
{
  try{	
	    var strSql="";
	    if(fm.all('inpNeedDutyGrid').value==1){
        
         strSql = "select dutycode,dutyname,'','','','','','','','','',''  from lmduty where dutycode in "
	               + " (select dutycode from lmriskduty where riskcode='"+cRiskCode+"')";
	     turnPage.queryModal(strSql, DutyGrid);  
	     DutyGrid.lock; 
        }
        if(fm.all('inpNeedPremGrid').value==1){
          strSql = "select * from lmdutypayrela where dutycode in  "
	               + " (select dutycode from lmriskduty where riskcode='"+cRiskCode+"')";

          turnPage.queryModal(strSql, PremGrid);  
          PremGrid.lock; 
        
        }
        
  
  }catch(ex) {}
         
}
function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
    if(fm.all('AgentCode').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	  }
	if(fm.all('AgentCode').value != "")	 {
	var cAgentCode = fm.AgentCode.value;  //保单号码	
	var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"'";// and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
     }
	}	
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];
  	fm.AgentGroup.value = arrResult[0][1];
  }
}

function queryAgent2()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==10 )	 {
	var cAgentCode = fm.AgentCode.value;  //保单号码	
	var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
     }
	}	
}
  function returnparent()
  {
  	parent.close();
}
