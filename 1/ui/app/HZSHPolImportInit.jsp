<%
//程序名称：
//程序功能：杭州商业银行代理保单导入
//创建日期：2004-07-19
//创建人  ：LiuQiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
		fm.all('FileName').value = '';
		//alert(fm.all('FileName').value);
		//alert(fm.all('FileName').dir);
  }
  catch(ex)
  {
    alert("在ProposalCopyInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {

  }
  catch(ex)
  {
    alert("在ProposalCopyInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();  
    initDiv();
    //alert('aaa');
    initLCGrpImportLogGrid();  
  }
  catch(re)
  {
    alert("ProposalCopyInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initLCGrpImportLogGrid()
{
    var iArray = new Array();
      
    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）             
      iArray[0][1]="40px";         			//列宽                                                     
      iArray[0][2]=10;          			//列最大值                                                 
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[1]=new Array();                                                                                       
      iArray[1][0]="投保单印刷号";    			//列名                                                     
      iArray[1][1]="100px";            			//列宽                                                     
      iArray[1][2]=100;            			//列最大值                                                 
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许5                    
                                                                                                                   
      iArray[2]=new Array();                                                                                       
      iArray[2][0]="保单号"                       //列名                                                     
      iArray[2][1]="130px";            			//列宽                                                     
      iArray[2][2]=100;            			//列最大值                                                 
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0                     

      iArray[3]=new Array();                                                                                       
      iArray[3][0]="险种代码";         		//列名                                                     
      iArray[3][1]="60px";            			//列宽                                                     
      iArray[3][2]=100;            			//列最大值                                                 
      iArray[3][3]=0;             			//是否允许输入,1表示允许，0表示不允许1                    
                                                                                                                   
      iArray[4]=new Array();                                                                                       
      iArray[4][0]="导入状态";         		//列名                                                     
      iArray[4][1]="50px";            			//列宽                                                     
      iArray[4][2]=100;            			//列最大值                                                 
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用6     
                                                                                                                   
      iArray[5]=new Array();                                                                                       
      iArray[5][0]="错误信息";         		//列名                                                     
      iArray[5][1]="280px";            			//列宽                                                     
      iArray[5][2]=100;            			//列最大值                                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用3
                                                                                                                   
      LCGrpImportLogGrid = new MulLineEnter( "fm" , "LCGrpImportLogGrid" ); 
      //这些属性必须在loadMulLine前
      LCGrpImportLogGrid.mulLineCount = 10;   
      LCGrpImportLogGrid.displayTitle = 1;
      //LCGrpImportLogGrid.canSel=1;
      //LCGrpImportLogGrid.selBoxEventFuncName ="reportDetailClick";
      //LPGetGrid.canChk=1;
      LCGrpImportLogGrid.loadMulLine(iArray);  

      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}
function initDiv()
{
	divImport.style.display='none';
}
</script>