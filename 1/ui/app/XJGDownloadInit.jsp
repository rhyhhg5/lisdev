<%@page contentType="text/html;charset=GBK" %>
<script language="JavaScript">
function initInpBox()
{
    try
    {
        fm.ManageCom.value = "86650105";
        fm.ManageComName.value = "新疆红山营销部";
        
        
        fm.btnContDownload.disabled = true;
        fm.btnContDetailDownload.disabled = true;
    }
    catch(ex)
    {
        alert("进行初始化是出现错误！！！！");
    }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LRBillDetailInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initPolGrid();
  }
  catch(re)
  {
    alert("LRBillDetailInit.jsp->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
  {                               
    var iArray = new Array();
      
    try{
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=30;            			//列最大值
	      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[1]=new Array();
	      iArray[1][0]="保单号";         		//列名
	      iArray[1][1]="60px";            		//列宽
	      iArray[1][2]=100;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		   
		  	
	      iArray[2]=new Array();
	      iArray[2][0]="保险起期";         		//列名
	      iArray[2][1]="60px";            		//列宽
	      iArray[2][2]=100;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	     
	      
	      iArray[3]=new Array();
	      iArray[3][0]="终止日期";         		//列名
	      iArray[3][1]="60px";            		//列宽
	      iArray[3][2]=100;            			//列最大值
	      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许            
		  
		  
	      iArray[4]=new Array();                                                       
	      iArray[4][0]="投保人名称";         		//列名                                     
	      iArray[4][1]="60px";            		//列宽                                   
	      iArray[4][2]=100;            			//列最大值                                 
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	      
	           
	      iArray[5]=new Array();
	      iArray[5][0]="总人数";         		//列名
	      iArray[5][1]="60px";            		//列宽
	      iArray[5][2]=100;            			//列最大值
	      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	      
	      
	      iArray[6]=new Array();
	      iArray[6][0]="总保费";         		//列名
	      iArray[6][1]="60px";            		//列宽
	      iArray[6][2]=100;            			//列最大值
	      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		  
	      
	     
	      
	      BillDetailGrid = new MulLineEnter("fm", "BillDetailGrid" ); 
	      //这些属性必须在loadMulLine前
	      BillDetailGrid.mulLineCount = 0;   
	      BillDetailGrid.displayTitle = 1;
	      BillDetailGrid.locked = 1;
	      BillDetailGrid.canSel = 0;
	      BillDetailGrid.canChk = 0; 
	      BillDetailGrid.hiddenPlus = 1;
	      BillDetailGrid.hiddenSubtraction = 1;
	      BillDetailGrid.loadMulLine(iArray);     
      
      	  
    }catch(ex){
      alert(ex);
    }
}
</script>