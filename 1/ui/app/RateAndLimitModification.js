var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;

// 查询全部信息
function queryByCondition() {

    initLMrateAndLimitGrid();
	if(!verifyInput2())
		return false;
	
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == "null"){
		alert("管理机构必须填写！");
		return false;
	}
	if((fm.PrtNo.value == null || fm.PrtNo.value == "" || fm.PrtNo.value == "null") && (fm.GrpContNo.value == null || fm.GrpContNo.value == "" || fm.GrpContNo.value == "null")){
		alert("印刷号或合同号必须填写其一！");
		return false;
	}
	/*
	if(fm.ContPlanCode.value == null || fm.ContPlanCode.value == "" || fm.ContPlanCode.value == "null"){
		alert("保障计划编码必须填写！");
		return false;
	}

	if(fm.RiskCode.value == null || fm.RiskCode.value == "" || fm.RiskCode.value == "null"){
		alert("险种编码必须填写！");
		return false;
	}
	*/
	

	var strSQL = "select distinct lgc.Prtno,lgc.GrpContno,lgc.CValiDate,lgc.GrpName,lcp.contplancode,lcp.riskcode,lcd.dutycode," 
    	+ "lgc.ManageCom,lcd.getrate,lcd.getlimit "
    	+ "from lcgrpcont lgc "
    	+ "inner join lcpol lcp on lgc.grpcontno = lcp.grpcontno "
    	+ "inner join lcduty lcd on lcd.polno = lcp.polno "
    	+ "where 1 = 1 "
    	+ "and lgc.appflag = '1' and lcp.contplancode != '11' "
        + getWherePart('lgc.ManageCom','ManageCom','like')
        + getWherePart('lgc.PrtNo','PrtNo')
        + getWherePart('lgc.GrpContNo','GrpContNo')
        + getWherePart('lcp.contplancode','ContPlanCode')
        + getWherePart('lcp.riskcode','RiskCode')
        + getWherePart('lcd.dutycode','DutyCode')
        + " order by lgc.grpcontno,lcp.contplancode,lcp.riskcode,lcd.dutycode ";
           
	turnPage1.queryModal(strSQL, LMrateAndLimitGrid);
	 if (!turnPage1.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
}


// 修改
function updateClick() {

	var getlimit = fm.all('Getlimit').value;
	var getrate = fm.all('Getrate').value;
	
	var mSelNo = LMrateAndLimitGrid.getSelNo();
	if (mSelNo == 0) {
		alert("请先选择需要处理的保单信息！");
		return false;
	}
	
	/**判断给付比例和免赔额同时空值问题*/
	if((getrate ==  null || getrate == "")&&(getlimit == null|| getlimit == "")){
		    alert("给付比例和免赔额至少要输入一个要修改的值.");
			return false;
	 }
	 /**判断给付比例有值*/
	 if(getrate !=  null && getrate != ""){		
	   if(!isNumeric(getrate)){
			alert("给付比例必须为数字。");
			return false;
	   }
	   if(getrate<0||getrate>1){
	        alert("给付比例必须为大于等于0,小于等于1的数字。");
	        return false;
	    }
	 }
	 
	  /**判断免赔额有值*/
	 if(getlimit != null && getlimit != ""){
	     if(!isNumeric(getlimit)){
			    alert("免赔额必须为数字。");
			    return false;
	      }
	     if(getlimit<0){
	        alert("免赔额必须为大于等于0的数字。");
	        return false;
	     }
	  } 
		
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "UPDATE||MAIN";
	fm.action = "RateAndLimitModificationSave.jsp";
	fm.submit();
	
}

/* 提交表单 */
function submitForm() {
	verifyInput2();// 输入校验
	//var showStr = "save";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=";
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); // 提交
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content) {
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail") {

		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		window.focus();
		window.location.reload();
	}
}

function ShowDetail() {
	var tSel = LMrateAndLimitGrid.getSelNo();
	try {
		fm.all('GETRATE').value = LMrateAndLimitGrid.getRowColData(tSel - 1, 9);
		fm.all('GETLIMIT').value = LMrateAndLimitGrid
				.getRowColData(tSel - 1, 10);
	} catch (ex) {
		alert("没有发现父窗口的接口" + ex);
	}
}