<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-08-01 18:29:02
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="PubAccInput.js"></SCRIPT>
  <SCRIPT>
  	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
  	var RiskCode = "<%=request.getParameter("RiskCode")%>";
  	var GrpPolNo = "<%=request.getParameter("GrpPolNo")%>";
  	var tLookFlag = "<%=request.getParameter("LookFlag")%>";
  	</SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PubAccInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./PubAccInputSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		<td class= titleImg>
        		团体帐户定义
       	</td>
    	</tr>
    </table>
    <table class= common >
    	<tr class=common>
    		
      	<TD  class= title8>
      	  帐户选择
      	</TD>
      	<TD  class= input8>
      		<Input name=InsuAccNo class= codeNo  verify="帐户选择|code:InsuAcc" ondblclick="if(GetInsuAccICodeData()) showCodeListEx('InsuAcc',[this,InsuAccName],[0,1],null,null,null,1);"  onkeyup="if(GetInsuAccICodeData()) showCodeListKeyEx('InsuAcc',[this,InsuAccName],[0,1],null,null,null,1);"><Input name=InsuAccName class= codeName>
      	</TD>
      	<td></td>
      	<td></td>
      	<td></td>
    	</tr>
    </table>
    <div id="divNone" style="display: 'none'">
    	<td class= title8>
      		保费
      	</td>
      	<td class= input8>
      		<Input name=Prem class= common>
      	</td>
    	<TD  class= title8>
        账户类型
      </TD>
      <TD  class= input8>
      	<Input name=AccTypeCode  class= codeNo CodeData="0|^001|集体公共账户^002|个人账户" ondblclick="showCodeListEx('AccTypeCode',[this,AccTypeName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('AccTypeCode',[this,AccTypeName],[0,1],null,null,null,1);" ><Input name=AccTypeName class= codeName>
      </TD>
      <TD  class= title8>
        审核类型
      </TD>
      <TD  class= input8>
      	<Input name=AccType2 class= codeNo  CodeData="0|^01|有审核型^02|普通型" ondblclick="showCodeListEx('AccType2',[this,AccType2Name],[0,1],null,null,null,1);"  onkeyup=" showCodeListKeyEx('AccType2',[this,AccType2Name],[0,1],null,null,null,1);"><Input name=AccType2Name class= codeName>
      </TD> 
      <td class=title8>
    		管理费收取方式
    	</td>
    	<td class=input8>
    		<Input name=ManageFeeType class= codeNo  CodeData="0|^01|一次^02|普通型" ondblclick="showCodeListEx('ManageFeeType',[this,ManageFeeTypeName],[0,1],null,null,null,1);"  onkeyup=" showCodeListKeyEx('ManageFeeType',[this,ManageFeeTypeName],[0,1],null,null,null,1);"><Input name=ManageFeeTypeName class= codeName>
    	</td>
    </div>
    <Div  id= "divLDClassInfo1" style= "display: 'none'">
			<table  class= common align='center' >
				<Div  id= "divLCPol2" style= "display: ''">
					<table  class= common>
						<tr  class= common>
							<td text-align: left colSpan=1>
								<span id="spanAccInfoGrid" >
								</span> 
							</td>
						</tr>
					</table>		
				</div>
			</table>
    </Div>
    <Div  id= "divLDClassInfo2" style= "display: ''">
			<table  class= common align='center' >
				<Div  id= "divLCPol2" style= "display: ''">
					<table  class= common>
						<tr  class= common>
							<td text-align: left colSpan=1>
								<span id="spanPublicAccGrid" >
								</span> 
							</td>
						</tr>
					</table>		
				</div>
			</table>
    </Div>
    <INPUT class=cssButton VALUE="保存账户信息" name=SaveAccInfo TYPE=button onclick="return submitForm();">
    <INPUT class=cssButton VALUE="删除全部账户" name=DelAccInfo TYPE=button onclick="return deleteClick();">
    <hr>
    
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		<td class= titleImg>
        		管理费信息定义
       		</td>
    	</tr>
    	<tr>
    		<td>
    		</td>
       		<td>
       			<font id= "zhu1" size = 2 color = red style= "display: ''">注：个人账户管理费收取比例同团体公共帐户</font>
       		</td>
    	</tr>
    </table>
    <Div  id= "zhu2" style= "display: 'none'">
    	<table>
    		<TR>
        	  <TD colspan=6>
				<font size = 2 color=red>填写说明：所缴纳的初始费用比例录入值范围为0-0.05；保单管理费录入值范围为0-5（单位：元）。</font>
         	 </TD>
       	 	</TR>
        	<TR>
         	 <TD colspan=6>
         	     <font size = 2 color=red>示例：所缴纳的初始费用比例为5%,则录入为0.05；保单管理费为5元，则录入5。</font>
         	 </TD>
        	</TR>             
    	</table>
    </Div>
    <INPUT class=cssButton VALUE="定义管理费" name=DefManageFee TYPE=button onclick="return queryManageFee();">
    <Div  id= "divLDClassInfo3" style= "display: ''">
			<table  class= common align='center' >
				<Div  id= "divLCPol2" style= "display: ''">
					<table  class= common>
						<tr  class= common>
							<td text-align: left colSpan=1>
								<span id="spanManageFeeGrid" >
								</span> 
							</td>
						</tr>
					</table>
					<div style="display: 'none'">
						<span id="spanIndManageFeeGrid" >
						</span> 
					</div>		
				</div>
			</table>
    </Div>
    <Div  id= "new2" style= "display: ''">
    <table class= common >
    	<tr class=common>
    		
      	<TD  class= title8>
      	  赔付顺序
      	</TD>
      	<TD  class= input8>
      		<Input name=ClaimNum class=codeNo  CodeData="0|^1|仅从个人账户赔付^2|先从个人账户赔付再从公共账户赔付"  ondblclick="showCodeListEx('ClaimNum',[this,ClaimNumName],[0,1],null,null,null,1);"  onkeyup=" showCodeListKeyEx('ClaimNum',[this,ClaimNumName],[0,1],null,null,null,1);"><Input name=ClaimNumName class= codeName>
      	</TD>
      	<td></td>
      	<td></td>
      	<td></td>
    	</tr>
    </table>
    </Div>
    <INPUT class=cssButton VALUE="保  存" name=SaveManaFee TYPE=button onclick="return saveManageFee();">
    <INPUT class=cssButton VALUE="测试beforeManageFeeSubmit()"  TYPE=hidden onclick="return beforeManageFeeSubmit();">
     <Div  id= "divHouston" style= "display: ''">    
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		<td class= titleImg>
        		委托基金信息定义
       		</td>
       	</tr>
       	<tr>
    		<td>
    		</td>
       		<td>
       			<font size = 2 color = red>注：委托基金规模精确到元</font>
       		</td>
    	</tr>
    </table>
    <table class=common>
		<td class= title8>
	    	是否进账
	    </td>
	    <td class= input>
                <input  class="codeNo" name="Houston" value="" CodeData="0|^0|是^1|否" ondblclick="return showCodeListEx('Houston',[this,HoustonName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Houston',[this,HoustonName],[0,1],null,null,null,1);"><input class="codename" name="HoustonName" elementtype=nacessary readonly="readonly"/>
        </td>
	  	<TD  class= title8>
	     委托基金规模
	    </TD>
	    <TD  class= input8>
	    	<Input elementtype=nacessary name=EntrustMoney  class=common  verify="委托基金规模|int">
	    </TD>
	    <TD class= input8></td>
	    <TD class= input8></td>
	    <hr>
	  </table>
      <INPUT class=cssButton VALUE="保  存" name=saveHousto TYPE=button onclick="return saveHouston();">
    </Div>
    
	 <Div  id= "new3" style= "display: ''">
    <table class=common>
			<td class= title8>
	    	团体帐户的帐户管理费
	    </td>
	    <td class= input8>
	    	<Input name=ManageFee class= common readonly >
	    </td>
	  	<TD  class= title8>
	      团体帐户的帐户金额
	    </TD>
	    <TD  class= input8>
	    	<Input name=GrpPubAcc  class=common readonly >
	    </TD>
	    <TD class= input8></td>
	    <TD class= input8></td>
	    <hr>
	    <tr>
	    </tr>
	    <tr>
	    </tr>
	    <tr>
	    </tr>
	  </table>
	  </Div>
	 <Div  id= "new4" style= "display: ''">
	  <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		<td class= titleImg>
        		利息信息定义
       	</td>
    	</tr>
    	<tr>
    		<td>
    		    
    		</td>
    		<td >
	        	<font color = red  size = 2>注：个人账户和团体账户共同计息</font>
       		</td>
    	</tr>
    </table>
	  <INPUT class=cssButton VALUE="定义利率" name=DefRate TYPE=button onclick="return queryInterest();">
	  </Div>
    <Div  id= "divLDClassInfo4" style= "display: ''">
			<table  class= common align='center' >
				<Div  id= "divLCPol3" style= "display: ''">
					<table  class= common>
						<tr  class= common>
							<td text-align: left colSpan=1>
								<span id="spanRiskInterestGrid" >
								</span> 
							</td>
						</tr>
					</table>		
				</div>
			</table>
    </Div>
    <Div  id= "new5" style= "display: ''">
   	 <INPUT class=cssButton VALUE="保  存" name=SaveRate TYPE=button onclick="return saveInterest();">
   	 <INPUT class=cssButton VALUE=" 退  出 "  TYPE=button name=close onclick="cancle();">
    </Div>
    <Div  id= "divLDClassInfo5" style= "display: 'none'">
    <hr>
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		<td class= titleImg>
        		保证利率定义
       		</td>
    	</tr>
    	<tr>
    		<td>
    		</td>
       		<td>
       			<font size = 2 color = red>注：保证利率录入范围：0-0.025</font>
       		</td>
    	</tr>
    </table>
    <table class= common >
    	<tr class=common>
    		
      	<TD  class= title8>
      	  保证利率（年利率）
      	</TD>
      	<TD  class= input8>
      		<Input name=GuaRate elementtype=nacessary class= codeName>
      	</TD>
      	<td></td>
      	<td></td>
      	<td></td>
    	</tr>
    </table>
    <INPUT class=cssButton VALUE="保  存" name=SaveGuaranteeRate TYPE=button onclick="return saveGuaranteeRate();">
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="PublicAccType" name="PublicAccType">
    <input type=hidden id="RiskCode" name="RiskCode">
    <input type=hidden id="InsuredName" name="InsuredName">
    <input type=hidden id="GrpContNo" name="GrpContNo">
    <input type=hidden id="GrpPolNo" name="GrpPolNo">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
