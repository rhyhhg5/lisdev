var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
// 使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		} catch (ex) {
			showInfo = null;
		}
	}
}
function submitForm() {
	var tSQL = "select markettype from lcgrpcont where prtno = '" + tPrtNo
			+ "' ";
	var strQueryResult = easyExecSql(tSQL);
	if (!strQueryResult) {
		alert("获取保单市场类型失败！");
		return false;
	}
	if (strQueryResult[0][0] == "1" || strQueryResult[0][0] == "9") {
		alert("该保单对应的市场类型，不需录入社保项目要素！");
		return false;
	}
	if (!BalanceCheck()) {
		return false;
	}
	if (!xbCheck()) {
		return false;
	}
	fm.fmtransact.value = "INSERT||MAIN";
	fm.PrtNo.value = tPrtNo;
	var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); // 提交
}
function deleteForm() {
	fm.fmtransact.value = "DELETE||MAIN";
	fm.PrtNo.value = tPrtNo;
	var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); // 提交
}
function cancel() {
	top.close();
}
function afterSubmit(FlagStr, content, cOperate) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	initForm();
}
function isBalanceTermFlag() {
	if (fm.BalanceTermFlag.checked == true) {
//		fm.BalanceTermFlag.value == "1";
		fm.all('BalanceTermFlag').value = "Y";
		fm.all('BalanceTermFlagDisplay').style.display = "";

	}
	if (fm.BalanceTermFlag.checked == false) {
		/*
		 * fm.all('Crs_SaleChnl').value = ""; fm.all('Crs_SaleChnlName').value =
		 * ""; fm.all('Crs_BussType').value = "";
		 * fm.all('Crs_BussTypeName').value = ""; fm.all('GrpAgentCom').value =
		 * ""; fm.all('GrpAgentComName').value = "";
		 * fm.all('GrpAgentCode').value = ""; fm.all('GrpAgentName').value = "";
		 * fm.all('GrpAgentIDNo').value = "";
		 */
//		fm.BalanceTermFlag.value = "0";
		fm.all('BalanceTermFlag').value = "N";
		fm.all('Recharge').value = "N";
		fm.all('RechargeName').value = "否";
		fm.all('Rebalance').value = "N";
		fm.all('RebalanceName').value = "否";
		fm.all('BalanceTermFlagDisplay').style.display = "";
	}
}

function initBalanceInfo() {
	if (tPrtNo == null || tPrtNo == "") {
		alert("获取印刷号失败！");
		return false;
	}
	var tSQL = " select ProjectName,ProjectNo,BalanceTermFlag,BalanceAmnt,BalanceTxt,bak1,rebalance,recharge "
			+ " from LCGrpContSub where PrtNo = '" + tPrtNo + "' ";
	// AccountCycle,BalanceLine,CostRate,BalanceRate,BalanceType,StopLine,SharedLine,SharedRate,RevertantLine,RevertantRate,
	// 执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if (strQueryResult) {
		if (strQueryResult[0][0] != null && strQueryResult[0][0] != ""
				&& strQueryResult[0][0] != "null") {
			fm.ProjectName.value = strQueryResult[0][0];
			fm.ProjectNo.value = strQueryResult[0][1];
			fm.BalanceTermFlag.value = strQueryResult[0][2];
			var cc = fm.BalanceTermFlag.value;
			if(strQueryResult[0][2] == "Y"){
				fm.all('BalanceTermFlagDisplay').style.display = "";
				fm.BalanceTermFlag.checked = true;
			}
			fm.BalanceAmnt.value = strQueryResult[0][3];
			/*
			 * fm.AccountCycle.value = strQueryResult[0][3];
			 * fm.BalanceLine.value = strQueryResult[0][4]; fm.CostRate.value =
			 * strQueryResult[0][5]; fm.BalanceRate.value =
			 * strQueryResult[0][6]; fm.BalanceType.value =
			 * strQueryResult[0][7]; fm.StopLine.value = strQueryResult[0][9];
			 * fm.SharedLine.value = strQueryResult[0][10]; fm.SharedRate.value =
			 * strQueryResult[0][11]; fm.RevertantLine.value =
			 * strQueryResult[0][12]; fm.RevertantRate.value =
			 * strQueryResult[0][13];
			 */
			fm.BalanceTxt.value = strQueryResult[0][4];
			fm.XBFlag.value = strQueryResult[0][5];
			fm.Rebalance.value = strQueryResult[0][6];
			fm.Recharge.value = strQueryResult[0][7];
		}
	}
}

function BalanceCheck() {
	if (fm.ProjectName.value == "" || fm.ProjectName.value == null) {
		alert("项目名称不能为空！");
		fm.ProjectName.focus();
		return false;
	}
	if (fm.ProjectName.value.length > 30) {
		alert("项目名称必须在30字以内！");
		fm.ProjectName.focus();
		return false;
	}
	if (fm.BalanceTermFlag.checked == false ) {
//		alert("是否有风险调节机制不能为空！");
//		fm.BalanceTermFlag.value = "N";
		fm.all('transferN').value = "N";
	}
	if (fm.BalanceAmnt.value != "" && fm.BalanceAmnt.value != null) {
		if (!isNumeric(fm.BalanceAmnt.value)) {
			alert("预估返还金额必须为非负数字！");
			fm.BalanceAmnt.focus();
			return false;
		}
		if (fm.BalanceAmnt.value < 0) {
			alert("预估返还金额不能为负数！");
			fm.BalanceAmnt.focus();
			return false;
		}
	}
	if (fm.Rebalance.value == "" || fm.Rebalance.value == null) {
		alert("是否有保费回补约定不能为空！");
		fm.Rebalance.focus();
		return false;
	}
	if (fm.Recharge.value == "" || fm.Recharge.value == null) {
		alert("是否有结余返还约定不能为空！");
		fm.Recharge.focus();
		return false;
	}
	/*
	 * if(fm.StopLine.value == "" || fm.StopLine.value == null) {
	 * alert("止损线不能为空！"); fm.StopLine.focus(); return false; }
	 * if(fm.SharedLine.value == "" || fm.SharedLine.value == null) {
	 * alert("共担线不能为空！"); fm.SharedLine.focus(); return false; }
	 * if(fm.RevertantLine.value == "" || fm.RevertantLine.value == null) {
	 * alert("回补线不能为空！"); fm.RevertantLine.focus(); return false; }
	 * if(fm.AccountCycle.value != "" && fm.AccountCycle.value != null) {
	 * if(!isNumeric(fm.AccountCycle.value)) { alert("核算周期必须为非负数字！");
	 * fm.AccountCycle.focus(); return false; } if(fm.AccountCycle.value<0) {
	 * alert("核算周期不能为负数！"); fm.AccountCycle.focus(); return false; } }
	 * if(fm.BalanceLine.value != "" && fm.BalanceLine.value != null) {
	 * if(!isNumeric(fm.BalanceLine.value)) { alert("结余线必须为非负数字！");
	 * fm.BalanceLine.focus(); return false; } if(fm.BalanceLine.value<0 ||
	 * fm.BalanceLine.value>1) { alert("结余线必须大于等于0，且小于1！");
	 * fm.BalanceLine.focus(); return false; } } if(fm.CostRate.value != "" &&
	 * fm.CostRate.value != null) { if(!isNumeric(fm.CostRate.value)) {
	 * alert("成本占比必须为非负数字！"); fm.CostRate.focus(); return false; }
	 * if(fm.CostRate.value<0 || fm.CostRate.value>1) {
	 * alert("成本占比必须大于等于0，且小于1！"); fm.CostRate.focus(); return false; } }
	 * if(fm.BalanceRate.value != "" && fm.BalanceRate.value != null) {
	 * if(!isNumeric(fm.BalanceRate.value)) { alert("返还比例必须为非负数字！");
	 * fm.BalanceRate.focus(); return false; } if(fm.BalanceRate.value<0 ||
	 * fm.BalanceRate.value>1) { alert("返还比例必须大于等于0，且小于1！");
	 * fm.BalanceRate.focus(); return false; } } if(fm.StopLine.value != "" &&
	 * fm.StopLine.value != null) { if(!isNumeric(fm.StopLine.value)) {
	 * alert("止损线必须为非负数字！"); fm.StopLine.focus(); return false; }
	 * if(fm.StopLine.value<0) { alert("止损线不能为负数！"); fm.StopLine.focus();
	 * return false; } } if(fm.SharedLine.value != "" && fm.SharedLine.value !=
	 * null) { if(!isNumeric(fm.SharedLine.value)) { alert("共担线必须为非负数字！");
	 * fm.SharedLine.focus(); return false; } if(fm.StopLine.value<0) {
	 * alert("共担线不能为负数！"); fm.SharedLine.focus(); return false; } }
	 * if(fm.SharedRate.value != "" && fm.SharedRate.value != null) {
	 * if(!isNumeric(fm.SharedRate.value)) { alert("共担比例必须为非负数字！");
	 * fm.SharedRate.focus(); return false; } if(fm.SharedRate.value<0 ||
	 * fm.SharedRate.value>1) { alert("共担比例必须大于等于0，且小于1！");
	 * fm.SharedRate.focus(); return false; } } if(fm.RevertantLine.value != "" &&
	 * fm.RevertantLine.value != null) { if(!isNumeric(fm.RevertantLine.value)) {
	 * alert("回补线必须为非负数字！"); fm.RevertantLine.focus(); return false; }
	 * if(fm.RevertantLine.value<0) { alert("回补线不能为负数！");
	 * fm.RevertantLine.focus(); return false; } } if(fm.RevertantRate.value != "" &&
	 * fm.RevertantRate.value != null) { if(!isNumeric(fm.RevertantRate.value)) {
	 * alert("回补比例必须为非负数字！"); fm.RevertantRate.focus(); return false; }
	 * if(fm.RevertantRate.value<0 || fm.RevertantRate.value>1) {
	 * alert("回补比例必须大于等于0，且小于1！"); fm.RevertantRate.focus(); return false; } }
	 */
	if (fm.BalanceTxt.value != "" && fm.BalanceTxt.value != null) {
		if (fm.BalanceTxt.value.length > 1000) {
			alert("风险调节文本最多1000字！");
			fm.BalanceTxt.focus();
			return false;
		}
	}
	if (fm.XBFlag.value == "" || fm.XBFlag.value == null) {
		alert("是否为新建项目不能为空！");
		fm.XBFlag.focus();
		return false;
	}
	// ---------------新添-----------------
	// 从数据库中获取 "是否为新建项目" 的值 v1
	var tSQL = "select 1 from lcgrpcontsub "
			+ "where 1=1 "
			+ "and projectname = '"
			+ fm.ProjectName.value
			+ "' "
			+ " union select 1 from LCProjectInfo lcp where lcp.projectname = '"
			+ fm.ProjectName.value + "' ";
	var tResult = easyExecSql(tSQL);
	// 将 v1 与 从页面获取的值对比，增加验证
	if (tResult != null) {
		if (fm.XBFlag.value == "1") {
			alert("请检查项目是否是新建项目！");
			return false;
		}
	} else {
		if (fm.XBFlag.value == "2") {
			alert("新建项目，不能作为已有项目录入！");
			return false;
		}
	}
	// ----------------结束----------------
	return true;
}

function initButton() {
	if (tLookFlag != "0") {
		fm.all("insert").style.display = "none";
		fm.all("delete").style.display = "none";
		fm.all("divShowInfo").style.display = "none";
		fm.all("queryprojectbutton").style.display = "none";
	}
}

// 2067 by gzh 20140714
function QueryProject() {
	window.open("./BalanceProjectQueryMain.jsp");
}
function afterQuery(arrResult) {
	if (arrResult != null) {
		fm.ProjectName.value = arrResult[0][0];
		fm.ProjectNo.value = arrResult[0][1];
	}
}
// 2067 by gzh 20140714 end
function xbCheck() {
	if (fm.XBFlag.value == "2") {
		var tSQL = "select 1 from lcgrpcontsub where projectname is not null and projectname != '' and "
				+ "projectname = '"
				+ fm.ProjectName.value
				+ "' union select 1 from LCProjectInfo lcp "
				+ "where  lcp.projectname is not null and lcp.projectname != '' and "
				+ "lcp.projectname = '" + fm.ProjectName.value + "' ";
		var strQueryResult = easyExecSql(tSQL);
		if (!strQueryResult) {
			alert("当为续保时，只能选择原有的项目编码及项目名称！");
			return false;
		}
	}
	return true;
}