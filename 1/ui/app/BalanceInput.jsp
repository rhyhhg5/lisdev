<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	String tPrtNo = request.getParameter("PrtNo");
	String tLookFlag = request.getParameter("LookFlag");
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="BalanceInput.js"></script>
		<%@include file="BalanceInputInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>";
  		var tPrtNo = "<%=tPrtNo%>";
  		var tLookFlag = "<%=tLookFlag%>";
  		</script>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./BalanceInputSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=titleImg>
						社保项目要素信息
					</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<TD class=title8>
						项目名称
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=ProjectName elementtype="nacessary"><input type =button id = "queryprojectbutton" name = "queryprojectbutton" class=cssButton value="查询项目" onclick="QueryProject();">
					</TD>
					<TD class=title8>
						项目编码
					</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=ProjectNo>
					</TD>
					<TD class=title8>
					</TD>
					<TD class=input>
					</TD>
				</tr>
				<tr class=common>
					<TD class=title8 colspan="6">
						<font color="red">已立项的项目请不要再新建项目名称，应通过“查询项目”功能选择已建立的项目。同一项目在不同年度出单时也应通过“查询项目”功能选择已建立的项目</font>
					</TD>
				</tr>
			<!-- 		<TD class=title8>
						是否有风险调节机制
					</TD>
					<TD class=input>
						<Input class=codeno name=BalanceTermFlag VALUE=""
							CodeData="0|^Y|是^N|否"
							ondblclick="return showCodeListEx('BalanceTermFlag',[this,BalanceTermFlagName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKeyEx('BalanceTermFlag',[this,BalanceTermFlagName],[0,1],null,null,null,1);" 
							 onkeydown="tempfunction();"readonly><input class=codename name=BalanceTermFlagName readonly=true elementtype="nacessary">
					</TD> -->
					<tr>
            <td colspan="6"><font color="black">是否有风险调节机制</font><INPUT
					TYPE="checkbox" NAME="BalanceTermFlag"
					onclick="isBalanceTermFlag();"></td>
        </tr>
				<tr><td><font color="red">注：当单选框被勾选时，选择为“是”</font></td></tr>	
				<tr class="common8" id="BalanceTermFlagDisplay" style="display: none">
					<TD class=title8>
						是否有保费回补约定
					</TD>
					<TD class="input8">
						<Input class=codeno name=Recharge VALUE=""
							CodeData="0|^Y|是^N|否"
							ondblclick="return showCodeListEx('Recharge',[this,RechargeName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKeyEx('Recharge',[this,RechargeName],[0,1],null,null,null,1);" readonly><input class=codename name=RechargeName readonly=true elementtype="nacessary">
					</TD>
					<TD class=title8>
						是否有结余返还约定
					</TD>
					<TD class="input8">
						<Input class=codeno name=Rebalance VALUE=""
							CodeData="0|^Y|是^N|否"
							ondblclick="return showCodeListEx('Rebalance',[this,RebalanceName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKeyEx('Rebalance',[this,RebalanceName],[0,1],null,null,null,1);" readonly><input class=codename name=RebalanceName readonly=true elementtype="nacessary">
					</TD>
				</tr>
				<tr>
				<!-- 	<TD class=title8>
						核算周期
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=AccountCycle>
					</TD>
					<TD class=title8>
						结余线
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=BalanceLine>
					</TD>
				</tr>
				<tr>
					<TD class=title8>
						成本占比
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=CostRate>
					</TD>
					<TD class=title8>
						返还比例
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=BalanceRate>
					</TD>
					<TD class=title8>
						返还类型
					</TD>
					<TD class=input>
						<Input class=codeno name=BalanceType VALUE=""
							CodeData="0|^0|常规^1|特殊"
							ondblclick="return showCodeListEx('BalanceType',[this,BalanceTypeName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKeyEx('BalanceType',[this,BalanceTypeName],[0,1],null,null,null,1);" readonly><input class=codename name=BalanceTypeName readonly=true>
					</TD>
				</tr>
				<tr>
					<TD class=title8>
						止损线
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=StopLine elementtype="nacessary">
					</TD>
					<TD class=title8>
						共担线
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=SharedLine elementtype="nacessary">
					</TD>
					<TD class=title8>
						共担比例
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=SharedRate>
					</TD>
				</tr>
				<tr>
					<TD class=title8>
						回补线
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=RevertantLine elementtype="nacessary">
					</TD>
					<TD class=title8>
						回补比例
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=RevertantRate>
					</TD> -->
					<TD class=title8>
						是否为新建项目
					</TD>
					<TD class=input>
						<Input class=codeno name=XBFlag VALUE=""
							CodeData="0|^1|新建项目^2|已有项目"
							ondblclick="return showCodeListEx('name=XBFlag',[this,XBFlagName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKeyEx('name=XBFlag',[this,XBFlagName],[0,1],null,null,null,1);" readonly><input class=codename name=XBFlagName readonly=true>
						<Input type="hidden" class="common" name=BalanceAmnt elementtype="nacessary">
					</TD>
				</tr>
			</table>
			<table class=common>
				<TR class=common>
					<td class=title>
						风险调节文本
					</td>
					<TD class=input colspan="5">
						<textarea class="common" name=BalanceTxt cols="100%" rows="3"></textarea>
					</TD>
				</TR>
				<TR class=common>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
				</TR>
			</table>
			</table>

			<table>
				<td class=button>
					<input type=hidden id="fmtransact" name="fmtransact">
					<input type=hidden id="transferN" name="transferN">
					<input type=hidden id="PrtNo" name="PrtNo">
					<input type="button" class=cssButton value=" 保  存 " name=insert onclick="submitForm()">
					<input type="button" class=cssButton value=" 删  除 " name=delete onclick="deleteForm()">
					<input type="button" class=cssButton value=" 退  出 " name=tuichu onclick="cancel()">
				</td>
			</table>
			<br><br>
			<div id="divShowInfo" style="display:''">
				<font color="red">录入说明：</font><br>
				<!--<font color="red">（1）项目名称，30字以内。</font><br>
				<font color="red">（2）是否为结余返还，双击下拉，包括“Y-是”和“N-否”。</font><br>
				<font color="red">（3）核算周期，数值类型，且大于等于0。</font><br>
				<font color="red">（4）结余线，数值类型，且大于等于0小于1。</font><br>
				<font color="red">（5）成本占比，数值类型，且大于等于0小于1。</font><br>
				<font color="red">（6）返还比例，数值类型，且大于等于0小于1。</font><br>
				<font color="red">（7）返还类型，双击下拉，包括“0-常规”和“1-特殊”。</font><br>
				<font color="red">（8）止损线，数值类型，且大于等于0。</font><br>
				<font color="red">（9）共担线，数值类型，且大于等于0。</font><br>
				<font color="red">（10）共担比例，且大于等于0小于1。</font><br>
				<font color="red">（11）回补线，数值类型，且大于等于0。</font><br>
				<font color="red">（12）回补比例，数值类型，且大于等于0小于1。</font><br>
				<font color="red">（13）风险调节文本，文本格式，最多1000字。</font><br>-->
				<font color="red">一、项目名称规则:</font><br>
   				<font color="red">市 (市直) （区县）+ 覆盖人群 ＋ 项目类型，对于核保时统一核算的项目，应作为一个项目。</font><br>
				<font color="red">覆盖人群：城镇职工、城镇居民、城乡居民、新农合、全民、公务员、老年人、残疾人、低保人群、学生。两类以上人群则在中间加上“及”，如：城镇职工及城镇居民</font><br>
				<font color="red">项目类型：基本医疗保险、大病保险、大额医疗保险、自费医疗保险、补充医疗保险、医疗救助保险、工伤保险、工伤补充保险、护理保险、重大疾病保险、意外险。两类以上的项目类型在中间加上“及”。</font><br>
<font color="red">大病保险：指大病保险政策出台后，由地方政府统一招标的大病保险业务。如包含自费医疗责任，命名时不需要再增加自费医疗保险。</font><br>
<font color="red">大额医疗保险：指保险责任中含基本医疗封顶线以上责任的业务。</font><br>
<font color="red">补充医疗保险：指除大病保险、大额保险、自费医疗保险外的补充医疗业务。如同时包含自费医疗责任，命名时不需要再增加自费医疗保险。</font><br>
<font color="red">示例：杭州市建德市城乡居民基本医疗保险</font><br>
<font color="red">二、是否有风险调节机制</font><br>
<font color="red">如协议中有相关约定，则填写有。与项目预计是否会发生返还无关。</font><br>
<!-- <font color="red">三、核算周期</font><br>
<font color="red">是指结余返还约定的统算年度，是1年还是2年。按照年度数值填写，如一年，则写1，如五年则填5。</font><br>
<font color="red">四、返还类型</font><br>
<font color="red">常规：指结余返还金额=（保费*结余线-赔款-成本）*返还比例。</font><br>
<font color="red">特殊：指不能转换为以上返还公式的结余返还类型。</font><br>
<font color="red">五、结余线</font><br>
<font color="red">赔付率达到特定数值时进行返还。例如，赔付率90%以下返还，则填写0.9。如返还金额计算公式与赔付率无关，那么结余线即为100%。</font><br>
<font color="red">六、成本占比</font><br>
<font color="red">是指返还时需要扣除的成本占保费收入的比例。例如：返还时需要先扣除管理费10%，则填写0.1。如果是固定的成本，应换算为占保费收入的比值。如不扣除成本，则填写0。</font><br>
<font color="red">七、返还比例</font><br>
<font color="red">返还时给投保方的比例。</font><br>
<font color="red">八、止损线</font><br>
<font color="red">如果有，则填写，为小数，如无则填写0。例如90%止损线，则填写0.9。</font><br>
<font color="red">九、共担线</font><br>
<font color="red">如果有，则填写，为小数，如无则填写0。例如当赔付率超出100%时，投保单位承担40%，我司承担60%，则在共担线填写1。</font><br>
<font color="red">十、共担比例</font><br>
<font color="red">共担线不为0时必须填写，为小数。共担比例为我司的承担比例。</font><br>
<font color="red">十一、回补线</font><br>
<font color="red">如果有，则填写，为小数，如无则填写0。例如当赔付率超出105%时，投保单位按照超赔金额的100%进行补偿。共担与回补的区别在于，回补需要对所有赔案进行理赔，超赔后通过保费回补手续进行补偿。</font><br>
<font color="red">十二、回补比例</font><br>
<font color="red">回补线不为0时必须填写，为小数。回补比例为回补金额/超出回补线的赔款金额。</font><br> -->
<font color="red">三、风险调节文本</font><br>
<font color="red">包含返还约定、止损约定、风险共担约定、回补约定。</font><br>
<font color="red">四、是否为新建项目</font><br>
<font color="red">已经录入过的项目为已有项目，未录入过项目信息的为新建项目，已有项目必须使用原有的项目编码和名称。</font><br>
				
			</div>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
