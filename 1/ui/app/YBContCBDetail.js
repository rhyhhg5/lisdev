var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartSignDate = fm.StartSignDate.value;
    var tEndSignDate = fm.EndSignDate.value;

	if(dateDiff(tStartSignDate, tEndSignDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}

    var tStrSQL = ""
        + " select "
        + " tmp.ManageCom, tmp.ManageComName, tmp.CardType, tmp.ContNo, tmp.PrtNo, "
        + " tmp.SaleChnl, getUniteCode(tmp.AgentCode), tmp.AgentName, tmp.BranchAttr, tmp.BranchName, "
        + " tmp.BankName,"//银行网点名称
        + " tmp.AppntName, tmp.AppntBirthday, tmp.AppntSex, tmp.AppntIdType, "
        + " tmp.AppntIdNo, tmp.AppntNativePlace, tmp.AppntMarriage, tmp.AppntOccupationType, "
        + " tmp.AppntOccupationCode,"
        + " tmp.AppntGrpName,tmp.AppntPosition,tmp.AppntSalary,tmp.AppntPostalAddress,tmp.AppntZipCode,tmp.AppntPhone,tmp.AppntMobile," 
        + " tmp.CValidate, tmp.CInValidate, "
        + " tmp.PolApplyDate, tmp.InputDate, tmp.ApproveDate, tmp.UWDate, tmp.SignDate, "
        + " tmp.CustomGetPolDate, tmp.GetPolDate, tmp.PayMode,tmp.bankcode,tmp.BankName,tmp.bankaccno,tmp.accname, "
        + " tmp.ContPrem AllPrem, tmp.ContAmnt AllAmnt,  "
        + " tmp.InsuName,tmp.InsuBirthday,tmp.InsuSex,tmp.InsuIdtype,tmp.InsuIdno,"
        + " tmp.InsuredGrpName,tmp.InsuredPosition,tmp.InsuredSalary,tmp.InsuredPostalAddress,tmp.InsuredZipCode,tmp.InsuredPhone,tmp.InsuredMobile," 
        + " tmp.InsuredMarriage,tmp.InsuredOccupationType,tmp.InsuredOccupationCode,tmp.InsuredNativePlace,"
        + " tmp.InsuCount,tmp.AllContPrem, "
        + " (case when tmp.ContAppFlag is null then '银保通撤单' else tmp.ContAppFlag end) ContAppFlag,tmp.ScanFlag,tmp.ScanDate "
        + " from "
        + " ( "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " labg.BranchAttr, labg.Name BranchName, "
        + " (select bankname from ldbank where bankcode = lcc.bankcode) BankName," //银行网点名称
        + " lca.AppntName AppntName, lca.AppntBirthday AppntBirthday, "
        + " CodeName('sex', lca.AppntSex) AppntSex, "
        + " CodeName('idtype', lca.IdType) AppntIdType, "
        + " lca.IdNo AppntIdNo, "
        + " CodeName('nativeplace', lca.NativePlace) AppntNativePlace, "
        + " CodeName('marriage', lca.Marriage) AppntMarriage, "
        + " CodeName('occupationtype', lca.OccupationType) AppntOccupationType, "
        + " lca.OccupationCode AppntOccupationCode, "
        + " (select GrpName from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntGrpName,"//工作单位
        + " lca.Position AppntPosition,"//岗位职务
        + " lca.Salary AppntSalary,"//年收入
        + " (select PostalAddress from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntPostalAddress,"//联系地址
        + " (select ZipCode from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntZipCode,"//邮编
        + " (select Phone from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntPhone,"//联系电话
        + " (select Mobile from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntMobile,"//手机
        + " lcc.CValidate, lcc.CInValidate, "
        + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate, "
        + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcc.Prem, lcc.Amnt, "
        + " lci.name InsuName,lci.birthday InsuBirthday,CodeName('sex',lci.sex) InsuSex, CodeName('idtype',lci.idtype) InsuIdtype,lci.idno InsuIdno,"
        + " lcp.InsuredNo, "
        + " (select GrpName from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredGrpName,"//工作单位
        + " lci.Position InsuredPosition,"//岗位职务
        + " lci.Salary InsuredSalary,"//年收入
        + " (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredPostalAddress,"//联系地址
        + " (select ZipCode from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredZipCode,"//邮编
        + " (select Phone from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredPhone,"//联系电话
        + " (select Mobile from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredMobile,"//手机
        + " CodeName('marriage', lci.Marriage) InsuredMarriage, "//婚姻
        + " CodeName('occupationtype', lci.OccupationType) InsuredOccupationType, "//职业类别
        + " lci.OccupationCode InsuredOccupationCode, "//职业代码
        + " CodeName('nativeplace', lci.NativePlace) InsuredNativePlace, "//国籍
        + " (lcp.Prem + nvl(lcp.SupplementaryPrem, 0)) AllPolPrem, "
        + " '承保' ContAppFlag, "
        + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag, "
        + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate, "
        + " lcc.prem ContPrem,lcc.amnt ContAmnt,"
		+ " (select count(1) from lcinsured where contno = lcc.contno) InsuCount,"
		+ " (select sum(Prem + nvl(SupplementaryPrem, 0)) from lcpol where contno = lcc.contno) AllContPrem,lcc.bankcode bankcode,lcc.bankaccno bankaccno,lcc.accname accname "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LCAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
        + " inner join LCInsured lci on lci.contno = lcc.contno and lci.insuredno = lcp.insuredno "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.AppFlag = '1' "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + " and lcc.CardFlag in ('9','c','d','e','f') "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + " union all "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " labg.BranchAttr, labg.Name BranchName, "
        + " (select bankname from ldbank where bankcode = lcc.bankcode) BankName," //银行网点名称
        + " lca.AppntName AppntName, lca.AppntBirthday AppntBirthday, "
        + " CodeName('sex', lca.AppntSex) AppntSex, "
        + " CodeName('idtype', lca.IdType) AppntIdType, "
        + " lca.IdNo AppntIdNo, "
        + " CodeName('nativeplace', lca.NativePlace) AppntNativePlace, "
        + " CodeName('marriage', lca.Marriage) AppntMarriage, "
        + " CodeName('occupationtype', lca.OccupationType) AppntOccupationType, "
        + " lca.OccupationCode AppntOccupationCode, "
        + " (select GrpName from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntGrpName,"//工作单位
        + " lca.Position AppntPosition,"//岗位职务
        + " lca.Salary AppntSalary,"//年收入
        + " (select PostalAddress from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntPostalAddress,"//联系地址
        + " (select ZipCode from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntZipCode,"//邮编
        + " (select Phone from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntPhone,"//联系电话
        + " (select Mobile from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntMobile,"//手机
        + " lcc.CValidate, lcc.CInValidate, "
        + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate, "
        + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.Prem, lcp.Amnt, "
        + " lci.name InsuName,lci.birthday InsuBirthday,CodeName('sex',lci.sex) InsuSex,CodeName('idtype',lci.idtype) InsuIdtype,lci.idno InsuIdno,"
        + " lcp.InsuredNo, "
        + " (select GrpName from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredGrpName,"//工作单位
        + " lci.Position InsuredPosition,"//岗位职务
        + " lci.Salary InsuredSalary,"//年收入
        + " (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredPostalAddress,"//联系地址
        + " (select ZipCode from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredZipCode,"//邮编
        + " (select Phone from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredPhone,"//联系电话
        + " (select Mobile from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredMobile,"//手机
        + " CodeName('marriage', lci.Marriage) InsuredMarriage, "//婚姻
        + " CodeName('occupationtype', lci.OccupationType) InsuredOccupationType, "//职业类别
        + " lci.OccupationCode InsuredOccupationCode, "//职业代码
        + " CodeName('nativeplace', lci.NativePlace) InsuredNativePlace, "//国籍
        + " (lcp.Prem + nvl(lcp.SupplementaryPrem, 0)) AllPolPrem, "
        + " '承保' ContAppFlag, "
        + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag, "
        + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate, "
        + " lcc.prem ContPrem,lcc.amnt ContAmnt,"
		+ " (select count(1) from lbinsured where contno = lcc.contno) InsuCount,"
		+ " (select sum(Prem + nvl(SupplementaryPrem, 0)) from lbpol where contno = lcc.contno) AllContPrem,lcc.bankcode bankcode,lcc.bankaccno bankaccno,lcc.accname accname "
        + " from LCCont lcc "
        + " inner join LBPol lcp on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LBAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
        + " inner join LBInsured lci on lci.contno = lcc.contno and lci.insuredno = lcp.insuredno "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.AppFlag = '1' "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + " and lcc.CardFlag in ('9','c','d','e','f') "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + " union all "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcc.CardFlag || ' - ' || (case lcc.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) CardType, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " labg.BranchAttr, labg.Name BranchName, "
        + " (select bankname from ldbank where bankcode = lcc.bankcode) BankName," //银行网点名称
        + " lca.AppntName AppntName, lca.AppntBirthday AppntBirthday, "
        + " CodeName('sex', lca.AppntSex) AppntSex, "
        + " CodeName('idtype', lca.IdType) AppntIdType, "
        + " lca.IdNo AppntIdNo, "
        + " CodeName('nativeplace', lca.NativePlace) AppntNativePlace, "
        + " CodeName('marriage', lca.Marriage) AppntMarriage, "
        + " CodeName('occupationtype', lca.OccupationType) AppntOccupationType, "
        + " lca.OccupationCode AppntOccupationCode, "
        + " (select GrpName from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntGrpName,"//工作单位
        + " lca.Position AppntPosition,"//岗位职务
        + " lca.Salary AppntSalary,"//年收入
        + " (select PostalAddress from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntPostalAddress,"//联系地址
        + " (select ZipCode from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntZipCode,"//邮编
        + " (select Phone from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntPhone,"//联系电话
        + " (select Mobile from LCAddress where customerno = lca.appntno and addressno = lca.addressno) AppntMobile,"//手机
        + " lcc.CValidate, lcc.CInValidate, "
        + " lcc.PolApplyDate, lcc.InputDate, lcc.ApproveDate, lcc.SignDate, "
        + " lcc.UWDate, lcc.CustomGetPolDate, lcc.GetPolDate, "
        + " lcc.PayMode || ' - ' || CodeName('paymode', lcc.PayMode) PayMode, "
        + " lcp.Prem, lcp.Amnt, "
        + " lci.name InsuName,lci.birthday InsuBirthday,CodeName('sex',lci.sex) InsuSex,CodeName('idtype',lci.idtype) InsuIdtype,lci.idno InsuIdno,"
        + " lcp.InsuredNo, "
        + " (select GrpName from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredGrpName,"//工作单位
        + " lci.Position InsuredPosition,"//岗位职务
        + " lci.Salary InsuredSalary,"//年收入
        + " (select PostalAddress from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredPostalAddress,"//联系地址
        + " (select ZipCode from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredZipCode,"//邮编
        + " (select Phone from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredPhone,"//联系电话
        + " (select Mobile from LCAddress where customerno = lci.insuredno and addressno = lci.addressno) InsuredMobile,"//手机
        + " CodeName('marriage', lci.Marriage) InsuredMarriage, "//婚姻
        + " CodeName('occupationtype', lci.OccupationType) InsuredOccupationType, "//职业类别
        + " lci.OccupationCode InsuredOccupationCode, "//职业代码
        + " CodeName('nativeplace', lci.NativePlace) InsuredNativePlace, "//国籍
        + " (lcp.Prem + nvl(lcp.SupplementaryPrem, 0)) AllPolPrem, "
        + " (select (case lpei.EdorType when 'CT' then '退保' when 'WT' then '犹豫期退保' when 'XT' then '协议退保' end) from LPEdorItem lpei where lpei.ContNo = lcc.ContNo and lpei.EdorType in ('CT', 'WT', 'XT') order by lpei.EdorAppDate desc fetch first rows only) ContAppFlag, "
        + " (case when (select nvl(count(distinct doccode),0) from es_doc_main where doccode = lcc.prtno) = 0 then '否' else '是' end) ScanFlag, "
        + " (select makedate from es_doc_main where doccode = lcc.prtno fetch first 1 rows only) ScanDate, "
        + " lcc.prem ContPrem,lcc.amnt ContAmnt,"
		+ " (select count(1) from lbinsured where contno = lcc.contno) InsuCount,"
		+ " (select sum(Prem + nvl(SupplementaryPrem, 0)) from lbpol where contno = lcc.contno) AllContPrem,lcc.bankcode bankcode,lcc.bankaccno bankaccno,lcc.accname accname "
        + " from LBCont lcc "
        + " inner join LBPol lcp on lcc.ContNo = lcp.ContNo "
        + " inner join LABranchGroup labg on labg.AgentGroup = lcc.AgentGroup "
        + " inner join LBAppnt lca on lca.ContNo = lcc.ContNo and lca.AppntNo = lcc.AppntNo "
        + " inner join LBInsured lci on lci.contno = lcc.contno and lci.insuredno = lcp.insuredno "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.AppFlag in ('1','3') "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + " and lcc.CardFlag in ('9','c','d','e','f') "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.SignDate", "StartSignDate", ">=")
        + getWherePart("lcc.SignDate", "EndSignDate", "<=")
        + getWherePart("lcc.SaleChnl", "SaleChnl")
        + " ) as tmp "
        + " group by tmp.ManageCom, tmp.ManageComName, tmp.CardType, tmp.ContNo, tmp.PrtNo, "
        + " tmp.SaleChnl, tmp.AgentCode, tmp.AgentName, tmp.BranchAttr, tmp.BranchName, "
        + " tmp.BankName,"
        + " tmp.AppntName, tmp.AppntBirthday, tmp.AppntSex, tmp.AppntIdType, "
        + " tmp.AppntIdNo, tmp.AppntNativePlace, tmp.AppntMarriage, tmp.AppntOccupationType, "
        + " tmp.AppntOccupationCode, "
        + " tmp.AppntGrpName,tmp.AppntPosition,tmp.AppntSalary,tmp.AppntPostalAddress,tmp.AppntZipCode,tmp.AppntPhone,tmp.AppntMobile," 
        + " tmp.CValidate, tmp.CInValidate, "
        + " tmp.PolApplyDate, tmp.InputDate, tmp.ApproveDate, tmp.UWDate, tmp.SignDate, "
        + " tmp.CustomGetPolDate, tmp.GetPolDate, tmp.PayMode, "
        + " tmp.InsuName,tmp.InsuBirthday,tmp.InsuSex,tmp.InsuIdtype,tmp.InsuIdno,"
        + " tmp.InsuredGrpName,tmp.InsuredPosition,tmp.InsuredSalary,tmp.InsuredPostalAddress,tmp.InsuredZipCode,tmp.InsuredPhone,tmp.InsuredMobile,"
        + " tmp.InsuredMarriage,tmp.InsuredOccupationType,tmp.InsuredOccupationCode,tmp.InsuredNativePlace,"
        + " tmp.ContPrem,tmp.ContAmnt,tmp.InsuCount,tmp.AllContPrem,"
        + " (case when tmp.ContAppFlag is null then '银保通撤单' else tmp.ContAppFlag end),tmp.ScanFlag,tmp.ScanDate,tmp.bankcode,tmp.bankaccno,tmp.accname "
        ;
 //prompt('',tStrSQL);
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "YBContCBDetailSave.jsp";
    fm.submit();
    fm.action = oldAction;

}