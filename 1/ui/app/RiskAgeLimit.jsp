<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>";// 记录管理机构
    var comcode = "<%=tGI.ComCode%>";// 记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<!-- 通用录入校验要是用的 js 文件 -->
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!-- 使用双击下拉需要引入的 JS 文件 -->
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>契约保单修改险种年领限制</title>
<!-- 自己的输入验证 js -->
<script src="RiskAgeLimitInput.js"></script>
<!-- 初始化 mulline -->
<%@include file="RiskAgeLimitInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="RiskAgeLimitSave.jsp" method="post" name="fm" target="fraSubmit">

		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>

		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>险种编号</td>
				<td class=input>
					<Input class=codeNo readonly=true name=Riskcode
					verify="险种编号|code:riskagelimit"
					ondblclick="return showCodeList('riskagelimit',[this,RiskName],[0,1],null,null,null,1);"
					onkeyup="return showCodeListKey('riskagelimit',[this,RiskName],[0,1]);"><input
					class=codename name=RiskName readonly=true>
				</td>
				<td class=title></td>
				<td class=input></td>
				<td class=title></td>
				<td class=input></td>
			</tr>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" TYPE=button
			onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divLCPol1);"></td>
				<td class=titleImg>限制信息表</td>
			</tr>
		</table>
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1><span id="spanPolGrid"></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button
					onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button
					onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button
					onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button
					onclick="getLastPage();">
			</table>
		</div>
		<br />
		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>修改前年龄最大限制</td>
				<td class=input><Input class=readonly readOnly=true
					name=beforeMaxage>
				<td class=title>修改前年龄最小限制</td>
				<td class=input><Input class=readonly readOnly=true
					name=beforeMinage>
			</tr>
			<tr class=common>
				<td class=title>修改后年龄最大限制</td>
				<td class=input>
				<input class=codename name=afterMaxage ></td>
				<td class=title>修改后年龄最小限制</td>
				<td class=input>
				<input class=codename name=afterMinage ></td>
			</tr>
		</table>
		<INPUT VALUE="确认修改" class="cssButton" TYPE=button 
			onclick="modifyage();">
		
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmtransact" name="querySql">
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
