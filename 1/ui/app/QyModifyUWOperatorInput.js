var arrDataSet
var showInfo;
var turnPage = new turnPageClass();

// 查询按钮
function easyQueryClick()
{
    // 初始化表格
    resetHiddenValue();
    initPolGrid();
    
    if(!verifyInput2())
    {
		return false;
    }
    
    if(fm.PrtNo.value == "" && fm.ContNo.value == "")
    {
        alert("印刷号和保单合同号至少录入一个！");
        return false;
    }
    
    // 书写SQL语句
    var strSQL = "";
    if(fm.ContType.value == 1)
    {
        strSQL = "select lcc.ContNo,lcc.PrtNo,lcc.AppntName,lcc.Prem,lcc.AgentCode,lagt.Name,"
            + "lcc.ManageCom,lwm.DefaultOperator,lcc.Operator,lcc.ContType,lcc.SaleChnl "
            + " from lccont lcc, lwmission lwm, laagent lagt "
            + "where lcc.PrtNo = lwm.MissionProp1 and lcc.AgentCode = lagt.AgentCode "
            + "and lwm.Activityid = '0000001100' and lcc.ContType = '1' "
            + getWherePart('lcc.ContNo','ContNo')
            + getWherePart('lcc.PrtNo','PrtNo')
            + getWherePart('lcc.ManageCom','ManageCom','like')
            + " order by lcc.ManageCom,lcc.ContNo ";
    }
    else if(fm.ContType.value == 2)
    {
        strSQL = "select lgc.GrpContNo,lgc.PrtNo,lgc.GrpName,lgc.Prem,lgc.AgentCode,lagt.Name,"
            + "lgc.ManageCom,lwm.DefaultOperator,lgc.Operator,'2' ContType,lgc.SaleChnl "
            + " from lcgrpcont lgc, lwmission lwm, laagent lagt "
            + "where lgc.PrtNo = lwm.MissionProp2 and lgc.AgentCode = lagt.AgentCode "
            + "and lwm.Activityid = '0000002004' "
            + getWherePart('lgc.GrpContNo','ContNo')
            + getWherePart('lgc.PrtNo','PrtNo')
            + getWherePart('lgc.ManageCom','ManageCom','like')
            + " order by lgc.ManageCom,lgc.AgentCode ";
    }
    turnPage.pageLineNum = 50;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    fm.querySql.value = strSQL;
}

//初始化查询数据
function Polinit()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) 
  {
  	if (PolGrid.getSelNo(i)) 
  	{
  		checkFlag = PolGrid.getSelNo();
		break;
	}
  }
  if (checkFlag) 
  {
  	var tContNo=PolGrid.getRowColData(checkFlag-1,1);
	var tPrtNo=PolGrid.getRowColData(checkFlag-1,2);
	var tNowUWOperator=PolGrid.getRowColData(checkFlag-1,8);
	var tOperator=PolGrid.getRowColData(checkFlag-1,9);
	var tContType=PolGrid.getRowColData(checkFlag-1,10);
	var tManageCom=PolGrid.getRowColData(checkFlag-1,7);

	fm.all('mContNo').value = tContNo;
	fm.all('mPrtNo').value = tPrtNo;
	fm.all('mNowUWOperator').value = tNowUWOperator;
	fm.all('mOperator').value = tOperator;
	fm.all('mContType').value = tContType;
	fm.all("modifybtn").disabled=false;
	fm.all("returnbtn").disabled=false;
  }
  else
  {
	alert("请先选择一条保单信息！"); 
  }
}

//重置隐藏的参数值
function resetHiddenValue()
{

    fm.all('mContNo').value = '';
    fm.all('mNowUWOperator').value = '';
    fm.all('mModifyUWOperator').value = '';
    fm.all('mOperator').value = '';
    fm.all('fmtransact').value = '';
    fm.all('mContType').value = '';
    fm.all('mPrtNo').value = '';
    fm.all('mAction').value = '';
    fm.all("UserCodeName").value = '';
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}

//修改保单核保师
function modifyUWOperator()
{
    if(fm.all("mModifyUWOperator").value == null || fm.all("mModifyUWOperator").value == "")
    {
        alert("请录入要修改的核保师");
        return;
    }
    
    if(!verifyInput2())
    {
		return false;
    }
    
    fm.all("fmtransact").value = "UPDATE";
    fm.all("mAction").value = "ModifyUWOperator";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.submit();
}

//将保单放回核保池
function returnUWPol()
{
	if(!verifyInput2())
    {
		return false;
    }
	
    fm.all('fmtransact').value = "UPDATE";
    fm.all("mAction").value = "ReturnUWPol";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.submit();
}