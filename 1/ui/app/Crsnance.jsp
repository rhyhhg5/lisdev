<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput) session.getValue("GI");
%>
	<head>
		<script>
		    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
		    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
		    var prtno = "<%=request.getParameter("PrtNo")%>";
		    var contno = "<%=request.getParameter("ContNo")%>";
		    var conttype = "<%=request.getParameter("ContType")%>";
		    var action = "<%=request.getParameter("Action")%>";
		</script>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<script src="../common/javascript/CommonTools.js"></script>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<title>契约保单信息运维功能维护页面</title>
		<script src="Crsnance.js"></script>
		<%@include file="CrsnanceInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form method="post" name="fm" target="fraTitle">
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						保单信息：
					</td>
				</tr>
			</table>
			<table class=common border=0 width=100%>
				<tr class=common>
					<td class=title>管理机构</td>
					<td class=input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ><input class=codename name=ManageComName readonly=true ></td>
					<td class=title>印 刷 号</td>
					<td class=input><input class=common name=PrtNo readonly=true /></td>
					<td class=title>合 同 号</td>
					<td class=input><input class=common name=ContNo readonly=true /></td>
				</tr>
				<tr class=common>
					<td class=title>保单类型</td>
					<td class=input><Input class=codeno name=ContType readonly=true CodeData="0|^1|个单^2|团单" /><input class=codename name=ContTypeName readonly=true ></td>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
				</tr>
			</table>
			
			<table>
				<tr>
					<td class=common>
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
					</td>
					<td class= titleImg>修改前的数据</td>
				</tr>
			</table>
			<table class=common border=0 width=100%>
				<tr class=common>
					<td class=title>销售渠道</td>
					<td class=input><input class=codeNo name=SaleChnl readonly=true/><input class=codename name=SaleChnlName readonly=true ></td>
					<td class=title>交叉销售渠道</td>
					<td class=input><input class=codeNo name=CrsSaleChnl  readonly=true/><input class=codename name=CrsSaleChnlName readonly=true ></td>
					<td class=title>交叉销售业务类型</td>
					<td class=input><input class=codeNo name=CrsBussType  readonly=true/><input class=codename name=CrsBussTypeName readonly=true ></td>
				</tr>
				<tr class=common>
					<td class=title>对方业务员代码</td>
					<td class=input><input class=common name=GrpAgentCode readonly=true/></td>
					<td class=title>对方业务员姓名</td>
					<td class=input><input class=common name=GrpAgentName readonly=true/></td>
					<td class=title>对方业务员证件号码</td>
					<td class=input><input class=common name=GrpAgentIdNo readonly=true/></td>
				</tr>
				<tr class=common>
					<td class=title>对方机构代码</td>
					<td class=input><input class=common name=GrpAgentCom readonly=true/></td>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
				</tr>
			</table>
			
			<hr />
			<table>
				<tr>
					<td class=common>
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
					</td>
					<td class= titleImg>需要修改的数据</td>
				</tr>
			</table>
			<table class=common border=0 width=100%>				
				<tr class=common style="display: ''">
					<td class=title>销售渠道</td>
					<td class=input><Input class=codeNo name=mSaleChnl readonly=true verify="销售渠道|notnull" ><input class=codename name=mSaleChnlName readonly=true></td>
					<td class=title>交叉销售渠道</td>
					<td class=input> <input class="codeNo" name="mCrsSaleChnl"  verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,mCrs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,mCrs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="mCrs_SaleChnlName" readonly="readonly"/></td>
					<td class=title>交叉销售业务类型</td>
					<td class=input><input class="codeNo" name="mCrsBussType"  verify="交叉销售业务类型|code:crs_busstype" ondblclick="return showCodeList('crs_busstype',[this,mCrs_BussTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_busstype',[this,mCrs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="mCrs_BussTypeName" readonly="readonly" ></td>
				</tr>
				<tr class=common>
					<td class=title>对方业务员代码</td>
					<td class=input><input class=common name=mGrpAgentCode /></td>
					<td class=title>对方业务员姓名</td>
					<td class=input><input class=common name=mGrpAgentName /></td>
					<td class=title>对方业务员证件号码</td>
					<td class=input><input class=common name=mGrpAgentIdNo /></td>
				</tr>
				<tr class=common>
					<td class=title>对方机构代码</td>
					<td class=input><input class=common name=mGrpAgentCom /></td>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
				</tr>
			</table>
			<br />			
			
			<INPUT VALUE="修改业务数据" class="cssButton" TYPE=button onclick="update();">
			<INPUT VALUE="补充上报" class="cssButton" TYPE=button onclick="report();">
			
			<input type="hidden" class=Common name="Action" value="" />
			<input type="hidden" class=Common name="FinancialAction" value="" />
		</form>
		
		<div>
			<font color="red">				
				<ul>注意事项：
					<li>销售渠道暂不能更改。</li>
					<li>在修改完毕后，需补充上报保单。</li>
					<li>所上报的保单，必须为已签发的保单</li>
				</ul>
			</font>
		</div>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
