<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：
//程序功能：
//创建日期：2009-08-13
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="ScanWorkload1Day.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();">    
<form action="" method=post name=fm target="fraSubmit">

    <table class="common">
        <tr class="common">
            <td class="title">管理机构</td>
            <td class="input">
                <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
            </td>
            <td class="title">扫描日期起</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="StartDate" verify="扫描日期起|date&notnull" elementtype="nacessary" />
            </td>
            <td class="title">扫描日期止</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="EndDate" verify="扫描日期止|date&notnull" elementtype="nacessary" />
            </td>
        </tr>            
    </table>
    
    <br />
    
    <input value="下  载" class="cssButton" type="button" onclick="submitForm();" />
    
    <hr />
    
    <div>
        <font color="red">
            <ul>报表说明：
                <li>本报表用于进行扫描人员工作量。</li>
                <li>日期查询口径：按照扫描上载时间为统计口径。</li>
            </ul>
            <ul>名词解释：
                <li>扫描件数：表示统计当天扫描进系统的保单数目。例如：一个投保书算一件；一个理赔件算一件。</li>
            </ul>
            <ul>统计条件：
                <li>管理机构、扫描日期段。</li>
                <li>扫描日期段间隔不超过三个月。</li>
            </ul>
        </font>
    </div>
    
    <input name="querySql" type="hidden" />
    <input type="hidden" name=op value="" />

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 