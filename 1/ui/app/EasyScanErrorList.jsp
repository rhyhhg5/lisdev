<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：EasyScanErrorList.jsp
//程序功能：外包异常保单修改状态页面
//创建日期：2008-6-4
//创建人  ：ZhangJianbao
//更新记录：更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="EasyScanErrorList.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="EasyScanErrorListInit.jsp"%>
  <title>外包错误数据</title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table class="common">
  		<tr CLASS="common">
  			<td CLASS="title">批次号</td>
  			<td CLASS="input" COLSPAN="1">
  			  <input NAME="BPOBatchNo">
      	</td> 
  			<td CLASS="title">管理机构</td>
  			<td CLASS="input" COLSPAN="1">
  			  <Input class= "codeno"  name=ManageCom style="width:50" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" verify="管理机构|notnull&code:comcode" ><Input class=codename style="width:100" name=ManageComName elementtype=nacessary readonly></TD>
      	</td> 
  			<td CLASS="title">印刷号码</td>
  			<td CLASS="input" COLSPAN="1">
  			<input NAME="PrtNo" CLASS="common">
      	</td>  		
  		</tr>
  		<tr CLASS="common">
  			<td CLASS="title">发送外包日期起</td>
  			<td CLASS="input" COLSPAN="1">
  			  <input NAME="StartDate" class="coolDatePicker" verify="发送外包日期起|notnull&date" elementtype=nacessary style="width:120">
      	</td> 
  			<td CLASS="title">止</td>
  			<td CLASS="input" COLSPAN="3">
  			  <input NAME="EndDate" class="coolDatePicker" verify="发送外包日期截止|notnull&date" elementtype=nacessary style="width:120">
      	</td>	
  		</tr>
    </table>
    <INPUT VALUE="查  询" class =cssButton TYPE=button onclick="queryContInfo();">
  <br></br>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			 外包反馈错误信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''" align=center>
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  </td>
  			</tr>
    	</table>
    </Div>
  	<Div id= "divPage2" align=center style= "display: '' ">
      <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPage2.firstPage(); setErrorInfo();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage2.previousPage(); setErrorInfo();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage2.nextPage(); setErrorInfo();"> 
      <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPage2.lastPage(); setErrorInfo();"> 
    </Div>
    <input type="hidden" name="BatchNo">
    <input type="hidden" name="BussNo">
    <INPUT VALUE="修改状态" class = cssButton TYPE=button onclick="modifyState();"> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
