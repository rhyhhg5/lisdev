<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ProposalManagerStatusSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  
<%
  System.out.println("\n\n---TempFeeWithdrawSave Start---");
  
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  
  ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
  tES_DOC_MAINSchema.setDOC_CODE(request.getParameter("PrtNo"));  
	//System.out.println("tES_DOC_MAINSchema:" + tES_DOC_MAINSchema.encode());
	
  // 准备传输数据 VData
	VData tVData = new VData();
	tVData.add(tES_DOC_MAINSchema);

  // 数据传输
	ModifyStateES_DOC_MAINUI tModifyStateES_DOC_MAINUI = new ModifyStateES_DOC_MAINUI();
	tModifyStateES_DOC_MAINUI.submitData(tVData, "UPDATE||MAIN");

  tError = tModifyStateES_DOC_MAINUI.mErrors;
  if (!tError.needDealError()) {                          
  	Content = " 退费成功! ";
  	FlagStr = "Succ";
  }
  else {
  	Content = " 退费失败，原因是:" + tError.getFirstError();
  	FlagStr = "Fail";
  }

	System.out.println(Content + "\n" + FlagStr + "\n---TempFeeWithdrawSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.easyQueryClick();
</script>
</html>
