
function submitData() {
	  
	  if(!verifyInput2()){
	    return fales;
	  }
	  
	  document.all("DownloadID").style.display = "none";
	  
	  if(dateDiff(fm.StartDate.value, fm.EndDate.value,"M") > 3){
	    if (!confirm("时间段大于3个月，下载缓慢，继续？")){
        return false;
      }
	  }
	  
	  if(dateDiff(fm.StartDate.value,fm.EndDate.value,"M") > 12){
	    alert("查询时间段不能大于1年。");
	    return false;
	  }
	  
    //var showStr = "正在操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
    //var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    //showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    fm.submit();
  }

/**
 * 提交后操作,服务器数据返回后执行的操作
 */
function afterSubmit(FlagStr, content) 
{
  showInfo.close();
  if (FlagStr == "Fail")
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else 
  {
	  document.all("DownloadID").style.display = "";
	  document.all("DownloadID").href = content;
	  
	  alert("文件生成成功，请点击页面上‘下载文件’连接，右键另存为。");
  }
}