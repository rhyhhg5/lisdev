//程序名称：
//程序功能：
//创建日期：2008-12-03
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


function printPolpdf()  
{
    var tManageCom = fm.ManageCom.value;
    var tCode = "TPA01";
    
    fm.fmtransact.value = "PRINT";
    var tUrl = ""
        + "../uw/PrintPDFSave.jsp"
        + "?Code=0" + tCode
        + "&ManageCom=" + tManageCom
        ;
    fm.action = tUrl;
    fm.submit();
    
    fm.action = "";
    fm.fmtransact.value = "";

    return true;
}

/**
 * 申请单证结算单提交后动作。
 */
function afterSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("打印失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    //fm.btnPrintPdf.disabled = false;
}


function printPolpdfNew()
{
    /*
    var tRow = CSLPayPrintGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }

    var tRowDatas = CSLPayPrintGrid.getRowData(tRow);
    
    var tPrtSeq = tRowDatas[0];
    var tPrtNo = tRowDatas[1];
    var tCode = tRowDatas[10];
    */
    
    var tManageCom = fm.ManageCom.value;
    var tCode = "TPAapp";

    fm.fmtransact.value = "PRINT";
    fm.target = "fraSubmit";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "../uw/PDFPrintSave.jsp?Code=" + tCode;
    fm.submit();
    
    return true;
}

/**
 * 申请单证结算单提交后动作。
 */
function afterSubmit2(FlagStr, Content)
{   
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("打印失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    //fm.btnPrintPdf.disabled = false;
}