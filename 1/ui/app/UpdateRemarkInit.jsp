
<%@page import="com.sinosoft.utility.*"%>

<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ManageCom;
	String strOperator =globalInput.Operator;
%>

<script language="JavaScript">
	
	function initInpBox()
	{
		try
    	{
        	// 初始化
        	fm.all('PrtNo').value = '';
        	fm.all('GrpContNo').value = '';
        	fm.all('ManageCom').value = '<%=strManageCom%>';
         	fm.all('ContType').value = '1';
        	fm.all('ContTypeName').value = '个人'; 
        	if(fm.all('ManageCom').value!=null)
        	{
            	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
            	//显示代码选择中文
            	if (arrResult != null) {
                	fm.all('ManageComName').value=arrResult[0][0];
            	}
        	}
        	fm.all('CValidateStart').value = '';
        	fm.all('CValidateEnd').value = '';
        	//fm.all('ContType').value = '';//个人标识  	
        	fm.all('AfterUpdateRemark').value = '';
    	}
    	catch(ex)
    	{
        	alert("在GrpUpdateRemarkInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    	}
	}
	
	function initForm()
	{
		try
		{
			initInpBox();
			initPolGrid();
		}
		catch(re)
		{
			alert("在GrpUpdateRemarkInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	
	var PolGrid;
	
	function initPolGrid(){
	
		var iArray = new Array();
	
		try{
			iArray[0]=new Array();
		    iArray[0][0]="序号";         				//列名（此列为顺序号，列名无意义，而且不显示）
		    iArray[0][1]="30px";            		//列宽
		    iArray[0][2]=10;            			//列最大值
		    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		     
		    iArray[1]=new Array();
		    iArray[1][0]="管理机构";
		    iArray[1][1]="65px";
		    iArray[1][2]=100;
		    iArray[1][3]=0;
		     
		    iArray[2]=new Array();
		    iArray[2][0]="合同号";
		    iArray[2][1]="82px";
		    iArray[2][2]=100;
		    iArray[2][3]=0;
		     
		    iArray[3]=new Array();
		    iArray[3][0]="印刷号";
		    iArray[3][1]="82px";
		    iArray[3][2]=100;
		    iArray[3][3]=0;
		     
		    iArray[4]=new Array();
		    iArray[4][0]="生效日期";
		    iArray[4][1]="72px";
		    iArray[4][2]=100;
		    iArray[4][3]=0;
		     
		    iArray[5]=new Array();
		    iArray[5][0]="特别约定";
		    iArray[5][1]="400px";
		    iArray[5][2]=100;
		    iArray[5][3]=0;
		    
		    iArray[6]=new Array();
		    iArray[6][0]="个体标识";
		    iArray[6][1]="0px";
		    iArray[6][2]=100;
		    iArray[6][3]=0;
		    
		    //生成MulLine对象
		    PolGrid = new MulLineEnter( "fm" , "PolGrid" );
		     
		    //设置MulLine属性
		   	//这些属性必须在loadMulLine前
		    PolGrid.mulLineCount = 0;   
		    PolGrid.displayTitle = 1;
		    PolGrid.locked = 1;
		    PolGrid.canChk = 0;
		    PolGrid.canSel = 1;
		    PolGrid.hiddenPlus = 1;
		    PolGrid.hiddenSubtraction = 1;
		    PolGrid.selBoxEventFuncName = "returnTableField"  ;//单选框激发函数

		    //初始化MulLine对象
		   	PolGrid.loadMulLine(iArray); 
		}catch(ex){
			alert(ex);
		}
	}   

</script> 