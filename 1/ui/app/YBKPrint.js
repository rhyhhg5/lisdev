//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var PolNo;
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//保单信息列表的初始化
  initContGrid();

  fm.submit(); // 提交
}

// 提交，保存按钮对应操作
function printPol() {
	var i = 0;
	var flag = 0;
	var row = 0; //用于记录被选中的行数
	
	for (i=0; i<ContGrid.mulLineCount; i++)
	  {
	    if (ContGrid.getSelNo(i))
	    {
	    	flag = ContGrid.getSelNo();
	      break;
	    }
	  }

	// 判定是否有选择打印数据
	if(!flag)
	{
		alert("请先选择一条记录，再打印保单！");
		return false;
	}
	fm.action="./YBKPrintSave.jsp";
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	// disable打印按钮，防止用户重复提交
	fm.all("printButton").disabled=true;
	fm.submit();
}

//提交，保存按钮对应操作
function sendPol() {
	var i = 0;
	var flag = 0;
	var row = 0; //用于记录被选中的行数
	for (i=0; i<ContGrid.mulLineCount; i++)
	  {
	    if (ContGrid.getSelNo(i))
	    {
	    	flag = ContGrid.getSelNo();
	      break;
	    }
	  }

	// 判定是否有选择打印数据
	if(!flag)
	{
		alert("请先选择一条记录，再发送保单！");
		return false;
	}
	fm.action="./YBKPrintSendSave.jsp";
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	// disable打印按钮，防止用户重复提交
	fm.all("sendButton").disabled=true;
	fm.submit();
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	// alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	// {alert("111");
	return arrSelected;
	// }
	// alert("222");
	arrSelected = new Array();
	// 设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	// 无论打印结果如何，都重新激活打印按钮
  window.focus();
  try
  {
	  showInfo.close();
	}
	catch(ex)
	{}
	
	fm.all("printButton").disabled=false;
	fm.all("sendButton").disabled=false;
	if (FlagStr == "Fail" )
	{
		// 如果打印失败，展现错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
	 
}

// 查询按钮
function easyQueryClick()
{
	var PrtNo = fm.PrtNo.value;
	var ContNo = fm.ContNo.value;
	
    if(fm.PrtNo.value == "" && fm.ContNo.value == "")
    {
        alert("印刷号、合同号至少录入一个！");
        return false;
    }
    
    var tWhereSQL = "";
    if(fm.PrtNo.value != "" && fm.PrtNo.value != null)
    {
    	tWhereSQL += " and prtno = '"+PrtNo+"' ";
    }
    if(fm.ContNo.value != "" && fm.ContNo.value != null)
    {
    	tWhereSQL += " and ContNo = '"+ContNo+"' ";
    }

	// 去掉了几个查询的筛选条件，有待以后考察是否需要保留
	var strSQL = "";
	strSQL = "select ContNo,PrtNo,Prem,AppntName,CValiDate,PrintCount " 
			+" from LCCont "
			+" where 1=1 "
			+" and conttype = '1' and appflag = '1' " //个单，已签单
			+" and (prtno like 'YWX%' or prtno like 'YBK%') "   //用以区分医保微信 和  医保卡 出单
			+tWhereSQL
			+" with ur";
	var strQueryResult = easyExecSql(strSQL);
	if(!strQueryResult){
		alert("未查询到数据！");
		return false;
	}

	turnPage1.queryModal(strSQL, ContGrid);
}