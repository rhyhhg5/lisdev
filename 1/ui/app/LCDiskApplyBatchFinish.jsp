<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>

  
<%
//输出参数
CErrors tError = null;
String tRela  = "";                
String FlagStr="";
String Content = "";
String tAction = "";
String tOperate = "";
String wFlag = request.getParameter("WorkFlowFlag");

LWMissionSchema lwmSchema = null;        
String strBatchNo = request.getParameter("BatchNo");
String strContID = "";
String strContNo = "";
String strPrtNo = "";
String strAppntNo = "";
String strAppntName = "";
String strAgentCode = "";
String strManagecome = "";
String strMissionID = "";
String strSubMissionID = "";
        
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  LWMissionDB lwmDB = new LWMissionDB();
  lwmDB.setActivityID(wFlag);
  //批次号
  lwmDB.setMissionProp5(strBatchNo);
  LWMissionSet lwmSet = lwmDB.query();  
  
        for (int i = 1; i <= lwmSet.size(); i++ ) 
        {
            lwmSchema = lwmSet.get(i);
            strContID = lwmSchema.getMissionProp6();
            strContNo = lwmSchema.getMissionProp7();
            strPrtNo = lwmSchema.getMissionProp1();
            strAppntNo = lwmSchema.getMissionProp8();
            strAppntName = lwmSchema.getMissionProp9();
            strAgentCode = lwmSchema.getMissionProp10();
            strManagecome = lwmSchema.getMissionProp3();
            strMissionID = lwmSchema.getMissionID();
            strSubMissionID = lwmSchema.getSubMissionID();    
            
            TransferData mTransferData = new TransferData();
            mTransferData.setNameAndValue("ContNo", strContNo);
            mTransferData.setNameAndValue("PrtNo", strPrtNo);
            mTransferData.setNameAndValue("AppntNo", strAppntNo);
            mTransferData.setNameAndValue("AppntName",strAppntName);
            mTransferData.setNameAndValue("AgentCode",strAgentCode);
            mTransferData.setNameAndValue("ManageCom", strManagecome);
            mTransferData.setNameAndValue("Operator",tG.Operator);
            mTransferData.setNameAndValue("MakeDate",PubFun.getCurrentDate());
            mTransferData.setNameAndValue("MissionID", strMissionID);  
            mTransferData.setNameAndValue("SubMissionID", strSubMissionID);
            
            VData tVData = new VData();
	    tVData.add(mTransferData);
	    tVData.add(tG);  
	    
	    System.out.println("-------------------start workflow---------------------");
	    TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
	    if( !tTbWorkFlowUI.submitData( tVData, wFlag ) ) {
	    	    Content =   " 录入确认失败，批次号：" + strBatchNo +
	    	    		"合同ID：" + strContID +
	    	    		"印刷号：" + strPrtNo +
	    	    		"原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
	    	    FlagStr = "Fail";
	    	    break;
	    } else {
	    	    Content = " 录入成功！";
	    FlagStr = "Succ";
	    }
	    System.out.println("-------------------end workflow---------------------");	             
        }  
    
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>