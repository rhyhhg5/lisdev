//*****************************************
//该文件中包含客户端需要处理的函数和事件
//*****************************************
parent.fraMain.rows = "0,0,0,0,*";
var mDebug="1";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();
window.onfocus = myonfocus;
var arrDataSet;
var prtNo = "";
var varPart = "";
var multcount = 0;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//排盘导入
function submitForm()
{
  var i = 0;
  getImportPath();
  ImportFile = fmlog.all('FileName').value;
  if ( ImportFile == null ||
       ImportFile == "")
  {
    alert("请浏览要导入的磁盘文件");
    return;
  }
  else
  {
    fmlog.action = "./LCDiskApplySave.jsp?ImportPath=" + ImportPath;
    var a= ImportFile.lastIndexOf("\\");
    var b= ImportFile.lastIndexOf(".");
    var BatchNo = ImportFile.substring(a+1,b);
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/SchedulePage.jsp?BatchNo="+BatchNo+"&Opicture=C&content=" + showStr;
    showInfo=window.open(urlStr,"",'height=250,width=500,top=250,left=270,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
    fmlog.submit();
  }

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Result )
{
	//alert("在after中"+multcount)
  if(multcount > 0)
  {
  	multcount--;
    submitData();
    return
  }
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,
                    "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    if (Result != null && Result != '')
    {
      var iArray;
      //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
      turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
      //保存查询结果字符串
      turnPage.strQueryResult = Result;
      //使用模拟数据源，必须写在拆分之前
      turnPage.useSimulation = 1;

      //查询成功则拆分字符串，返回二维数组
      var tArr = decodeEasyQueryResult(turnPage.strQueryResult,0);

      turnPage.arrDataCacheSet =chooseArray(tArr,[3,0,1,10,8]);
      //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
      turnPage.pageDisplayGrid = LCGrpImportLogGrid;

      //设置查询起始位置
      turnPage.pageIndex       = 0;
      //在查询结果数组中取出符合页面显示大小设置的数组
      var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
      //调用MULTILINE对象显示查询结果
      displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}

//*****************************************************************
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现
//函数的名称为initForm()
//*****************************************************************
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在ProposalCopy.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
  //  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./ProposalCopyQuery.html");
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function getImportPath ()
{
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select SysvarValue from ldsysvar where sysvar = 'XmlPath' ";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    alert("未找到上传路径");
    return;
  }

  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  ImportPath = turnPage.arrDataCacheSet[0][0];
}


function easyQueryClick()
{
  //	if(!verifyInput2())
  //	return false;
  if(fmquery.all('BatchNo').value == "" ||
      fmquery.all('BatchNo').value == null )
  {

    alert("请输入批次号！");
    return false;

  }
  initDiskErrQueryGrid();
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  // 书写SQL语句
  var strSql = " select prtno,ContID,InsuredID,InsuredName,ErrorInfo from LCGrpImportLog " +
               " where 1=1 and ErrorType='1' " +
               getPrtNoPart("prtno",fmquery.prtNo.value)+
               " and BatchNo='" + fmquery.all('BatchNo').value + "'" +
               " Order by length(BatchNo), BatchNo, length(ContID), ContID, IDNo";
  turnPage.queryModal(strSql,DiskErrQueryGrid);
  showInfo.close();

}

function easyQueryClick2()
{

  // 初始化表格
  initGrpGrid();
  if(!verifyInput2())
    return false;
  // 书写SQL语句
  //印刷号	MissionProp1
  //导入日期	MissionProp2
  //管理机构	MissionProp3
  //操作员	MissionProp4
  //批次号	MissionProp5
  //合同ID(excel)	MissionProp6
  //合同号	MissionProp7
  //投保人客户号	MissionProp8
  //投保人姓名	MissionProp9
  //代理人编码	MissionProp10
  var strSQL = "";
  queryContNo();
  strSQL = " select a.missionprop5, a.missionprop6, a.missionprop1, a.missionprop9, "
           + " a.missionprop10 , a.missionprop3, a.missionprop2, a.missionprop4 ,a.missionid , "
           + " a.submissionid,a.missionprop7,a.missionprop8 "
           + " from lwmission a,laagent b where 1=1 "
           + " and a.activityid = '0000001097' "
           + " and a.processid = '0000000003'"
           + " and b.agentcode=a.missionprop10"
           + getWherePart('a.missionprop1', 'tPrtNo')
           + getWherePart('a.missionprop2', 'ApplyDate')
           + getWherePart('a.missionprop3', 'ManageCom','like')
           //+ getWherePart('a.missionprop4', 'Operator')
           + getWherePart('a.missionprop5', 'BatchNo')
           + varPart
           + getWherePart('a.missionprop9', 'AppntNo')
           + getWherePart('a.missionprop10', 'Operator')
           + " order by length(missionprop5), missionprop5, length(missionprop6), missionprop6 "
           turnPage2.queryModal(strSQL,GrpGrid);

  return true;
}

//保单修改
function GoToInput()
{
  var i = 0;
  var checkFlag = 0;
  var state = "0";
  var checkAcount = 0;
  for (i=0; i<GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getChkNo(i))
    {
      checkAcount++;
      checkFlag = i;
    }
  }

  if (checkAcount == 1)
  {
    prtNo = GrpGrid.getRowColData(checkFlag, 3);
    var ManageCom = GrpGrid.getRowColData(checkFlag, 4);
    var MissionID = GrpGrid.getRowColData(checkFlag, 7);
    var SubMissionID = GrpGrid.getRowColData(checkFlag, 8);
    var strReturn="1";
    if ( prtNo == null || prtNo == "" )
    {
      alert("印刷号不能为空");
      return;
    }

    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    if (strReturn == "1")
      window.open("./ContInputScanMain.jsp?ScanFlag=1&LoadFlag=1&DiskInputFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&scantype=scan", "", sFeatures);
  }
  else
  {
    alert("请选择一条保单进行修改！");
  }

}

//录入完毕
function finish()
{
  var checkAcount = 0;
  for (i=0; i<GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getChkNo(i))
    {
      checkAcount++;
    }
  }
  if (checkAcount < 1)
  {
    alert("请至少选择一条保单！");
    return;
  }
  if (confirm("你确定要录入完毕所选保单吗？"))
  {
    //循环调用录入完毕相关操作
    var WorkFlowFlag = "0000001097";
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      multcount = GrpGrid.mulLineCount-1;
      submitData();
  }
}

//整批次录入完毕
function  batchFinish()
{
  var WorkFlowFlag = "0000001097";
  var BathchNo = fm.all('BatchNo').value;
  if (BathchNo == null | BathchNo =="")
  {
    alert("请输入批次号");
    return;
  }
  if (confirm("你确定要录入完毕" + BathchNo + "批次所有保单吗？"))
  {

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.action = "./LCDiskApplyBatchFinish.jsp?BathchNo=" + BathchNo
                + "&WorkFlowFlag=" + WorkFlowFlag;

    fm.submit(); //提交
  }
  else
  {
    return;
  }
}

function deleteCont()
{
  var i = 0;
  var checkFlag = 0;
  var state = "0";
  var checkAcount = 0;
  for (i=0; i<GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getChkNo(i))
    {
      checkAcount++;
      checkFlag = i;
    }
  }

  if (checkAcount == 1)
  {
    prtNo = GrpGrid.getRowColData(checkFlag, 3);
    var ManageCom = GrpGrid.getRowColData(checkFlag, 6);
    var MissionID = GrpGrid.getRowColData(checkFlag, 9);
    var SubMissionID = GrpGrid.getRowColData(checkFlag, 10);
    var strReturn="1";
  }
  if (prtNo == null || prtNo == "")
  {
    alert("请先选择一条保单！");
    return;
  }
  var strSql = "select contno from lccont where prtno='"+prtNo+"' and conttype='1'";
  var arr = easyExecSql(strSql)
            if (arr&&arr.length==1)
            {
              fm.ContNo.value = arr[0][0];
            }
            else
            {
              alert("合同信息不唯一！");
              return;
            }
            if (!confirm("确认要删除该保单吗？"))
            {
              return;
            }

            var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.action = "./ContDeleteChk.jsp";
  fm.submit();
}

//显示磁盘投保进度
function ShowSchedule1(BatchNo)
{
  var strSql="select errorinfo,contid from LCGRPIMPORTLOG where BATCHNO='"+BatchNo+"' and contid in ('C','X')";
  var arr = easyExecSql(strSql);
  var contNo = "";
  var xmlNo = "";
  if(arr)
  {
    var kind=arr[0][1];
    if(kind=="C")
    {
      contNo=arr[0][0];
      xmlNo =arr[1][0];
    }
    else
    {
      contNo=arr[1][0];
      xmlNo =arr[0][0];
    }
  }
  var strSql = "select count(contid) from LCGRPIMPORTLOG where BATCHNO='"+BatchNo+"' and contid not in ('C','X') and errorinfo='导入成功'";
  var arr = easyExecSql(strSql);
  var SuccCount = "";
  if(arr)
  {
    SuccCount = arr[0][0];
  }
  var strSql = "select count(contid) from LCGRPIMPORTLOG where BATCHNO='"+BatchNo+"' and contid not in ('C','X') and errorinfo<>'导入成功'";
  var arr = easyExecSql(strSql);
  var FailCount = "";
  if(arr)
  {
    FailCount = arr[0][0];
  }
  var contNo = contNo/1;
  SuccRate = (SuccCount/contNo)*100;
  FailRate = Math.round((FailCount/contNo)*10000)/100;
  showInfo.returnSchedule(contNo,xmlNo,SuccCount,FailRate);
}

function queryContNo()
{
  var insuredNo=fm.InsuredNo.value;
  if(insuredNo==""||insuredNo==null)
  {
    return;
  }
  //if(insuredNo.length==9)
  if(insuredNo!=""||insuredNo!=null)
  {
    var strSql = " and MissionProp7 in (select contno from lcinsured where name='"+trim(insuredNo)+"') ";
    varPart = strSql;
  }
}
//符合险种信息
function showComplexRiskInfo()
{
  var i = 0;
  var checkFlag = 0;
  var state = "0";
  var checkAcount = 0;
  for (i=0; i<GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getChkNo(i))
    {
      checkAcount++;
      checkFlag = i;
    }
  }

  if (checkAcount == 1)
  {
    prtNo = GrpGrid.getRowColData(checkFlag, 3);
    var strReturn="1";
  }
  if (prtNo == null || prtNo == "")
  {
    alert("请先选择一条保单！");
    return;
  }
  var strSql = "select contno from lccont where prtNo='"+prtNo+"'";
  var arr = easyExecSql(strSql);
  if(!arr)
  {
    alert("未查询到合同号！");
    return;
  }
  var ContNo = arr[0][0];
  window.open("./ComplexRiskMain.jsp?ContNo="+ContNo+"&prtNo="+prtNo,"", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}
function getPrtNoPart(prtNo,prtNoValue)
{
  if(prtNoValue != null && prtNoValue != "null" && prtNoValue!="")
  {
    return " and prtno='"+prtNoValue+"' ";
  }
  return "";
}
function none()
{
  //alert(1);
}

function submitData(){
	//alert(multcount);
	if(GrpGrid.getChkNo(multcount)){
			var	PrtNo = GrpGrid.getRowColData(multcount, 3);
      var ManageCom = GrpGrid.getRowColData(multcount, 6);
      var MissionID = GrpGrid.getRowColData(multcount, 9);
      var SubMissionID = GrpGrid.getRowColData(multcount, 10);
      var	ContNo = GrpGrid.getRowColData(multcount, 11);
      var	AppntNo = GrpGrid.getRowColData(multcount, 12);
      var	AppntName = GrpGrid.getRowColData(multcount, 4);
      AppntName = AppntName.replace("#", "%23");
      var	AgentCode = GrpGrid.getRowColData(multcount, 5);
			var WorkFlowFlag = "0000001097";
      fm.action = "./LCDiskApplyFinish.jsp?ProposalContNo=" + ContNo
                  + "&PrtNo=" + PrtNo
                  + "&AppntNo=" + AppntNo
                  + "&AppntName=" + AppntName
                  + "&AgentCode=" + AgentCode
                  + "&ManageCom=" + ManageCom
                  + "&MissionID=" + MissionID
                  + "&SubMissionID=" + SubMissionID
                  + "&WorkFlowFlag=" + WorkFlowFlag;
                 // alert(fm.action);
      fm.submit();
    }else if(multcount>0){
    	multcount--;
    	submitData();
    }else if(multcount==0){
    	afterSubmit( "Succ", "录入成功！" );
    }
}