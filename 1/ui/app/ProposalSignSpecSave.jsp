<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ProposalSignSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";

	try
	{
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
	  
	  	//接收信息
	  	// 投保单列表
		LCPolSet tLCPolSet = new LCPolSet();
	
		String tProposalNo[] = request.getParameterValues("PolGrid1");
		String tPtrNo[] = request.getParameterValues("PolGrid2");
		String tChk[] = request.getParameterValues("InpPolGridChk");
		boolean flag = false;
		int proposalCount = tProposalNo.length;
	
		for (int i = 0; i < proposalCount; i++)
		{
			if (tProposalNo[i] != null && tChk[i].equals("1"))
			{
System.out.println("ProposalNo:"+i+":"+tProposalNo[i]);
		  		LCPolSchema tLCPolSchema = new LCPolSchema();
		
			    tLCPolSchema.setPolNo( tProposalNo[i] );
			    tLCPolSchema.setPrtNo( tPtrNo[i] );
		    
			    tLCPolSet.add( tLCPolSchema );
			    flag = true;
			}
		}
		
	  	if (flag == true)
	  	{
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add( tLCPolSet );
			tVData.add( tG );
			
			// 数据传输
			ProposalSignUI tProposalSignUI   = new ProposalSignUI();
			tProposalSignUI.submitData( tVData, "INSERT" );
	
			int n = tProposalSignUI.mErrors.getErrorCount();
System.out.println("---ErrCount---"+n);
			if( n == 0 ) 
		    {                          
		    	Content = " 签单成功! ";
			   	FlagStr = "Succ";
		    }
		    else
		    {
				String strErr = "\\n";
				for (int i = 0; i < n; i++)
				{
					strErr += (i+1) + ": " + tProposalSignUI.mErrors.getError(i).errorMessage + "; \\n";
				}
				Content = "部分投保单签单失败，原因是: " + strErr;
				FlagStr = "Fail";
				
				
			} // end of if
		} // end of if
	} // end of try
	catch ( Exception e1 )
	{
		Content = "投保单签单失败，原因是: " + e1.toString();
		FlagStr = "Fail";
	}
	//System.out.println(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.initPolGrid();
	parent.fraInterface.easyQueryClick2();
</script>
</html>
