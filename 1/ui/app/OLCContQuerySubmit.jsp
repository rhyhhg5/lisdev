<%
//程序名称：OLCContQuery.js
//程序功能：
//创建日期：2002-08-16 16:01:25
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  LCContSchema tLCContSchema   = new LCContSchema();

  OLCContUI tOLCContQueryUI   = new OLCContUI();
  //读取Session中的全局类
	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

    tLCContSchema.setProposalNo(request.getParameter("ProposalNo"));
    tLCContSchema.setPrtNo(request.getParameter("PrtNo"));
    tLCContSchema.setManageCom(request.getParameter("ManageCom"));
    tLCContSchema.setSaleChnl(request.getParameter("SaleChnl"));
    tLCContSchema.setAgentCom(request.getParameter("AgentCom"));
    tLCContSchema.setAgentCode(request.getParameter("AgentCode"));
    tLCContSchema.setGrpNo(request.getParameter("GrpNo"));
    tLCContSchema.setName(request.getParameter("Name"));
    tLCContSchema.setContType(request.getParameter("ContType"));
    tLCContSchema.setLinkMan(request.getParameter("LinkMan"));
    tLCContSchema.setPhone(request.getParameter("Phone"));
    tLCContSchema.setFax(request.getParameter("Fax"));
    tLCContSchema.setAddress(request.getParameter("Address"));
    tLCContSchema.setZipCode(request.getParameter("ZipCode"));
    tLCContSchema.setEMail(request.getParameter("EMail"));



  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLCContSchema);
	tVData.add(tG);

  FlagStr="";
  // 数据传输
	if (!tOLCContQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " +  tError.getFirstError();
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tOLCContQueryUI.getResult();
		
		// 显示
		LCContSet mLCContSet = new LCContSet();
		mLCContSet.set((LCContSet)tVData.getObjectByObjectName("LCContSet",0));
		int n = mLCContSet.size();
		LCContSchema mLCContSchema;
		for (int i = 1; i <= n; i++)
		{
		  	mLCContSchema = mLCContSet.get(i);
		   	%>
		   	<script language="javascript">
        parent.fraInterface.ContGrid.addOne("ContGrid")
parent.fraInterface.fm.ContGrid1[<%=i-1%>].value="<%=mLCContSchema.getProposalNo()%>";
parent.fraInterface.fm.ContGrid2[<%=i-1%>].value="<%=mLCContSchema.getGrpNo()%>";
parent.fraInterface.fm.ContGrid3[<%=i-1%>].value="<%=mLCContSchema.getName()%>";
parent.fraInterface.fm.ContGrid4[<%=i-1%>].value="<%=mLCContSchema.getAgentCode()%>";
parent.fraInterface.fm.ContGrid5[<%=i-1%>].value="<%=mLCContSchema.getManageCom()%>";

			</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (!FlagStr.equals("Fail"))
  {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

