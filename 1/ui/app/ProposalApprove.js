//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;

/*********************************************************************
 *  进行投保单复核提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function approvePol1()
{
	var tSel = PolGrid.getSelNo();
	if( tSel == null || tSel == 0 )
		alert("请选择一张投保单后，再进行复核操作");
	else
	{
		var i = 0;
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		showSubmitFrame(mDebug);
		fm.submit(); //提交
	}
}

/*********************************************************************
 *  投保单复核的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	
	//解除印刷号的锁定
  var prtNo = PolGrid.getRowColData(PolGrid.getSelNo()-1, 2);
  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo="+prtNo+"&CreatePos=承保复核&PolState=1003&Action=DELETE";
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
	alert(FlagStr);
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{
		alert(1);
		cMissionID = fm.MissionID.value;
		strSQL = "select 1 from lwmission where 1=1 "
				 + " and missionid = '"+ cMissionID +"'"
				 + " and activityid = '" + tActivityID + "' "
				 + " and processid = '" + tProcessID + "' "
				 ;	 

	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  	//判断是否查询成功
  	if (turnPage.strQueryResult) {
			content = content + "但还有需要人工核保的保单！";
  	}
	}
	
  // 刷新查询结果
	queryCont();		
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
		cDiv.style.display="";
	else
		cDiv.style.display="none";  
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  显示投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showPolDetail()
{
	var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
	
	if (checkFlag) { 
  	var cPolNo = PolGrid.getRowColData( checkFlag - 1, 1 );
  	
  	mSwitch.deleteVar( "PolNo" );
  	mSwitch.addVar( "PolNo", "", cPolNo );
  	
  	window.open("./ProposalMain.jsp?LoadFlag=6","window1");
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}           

/*********************************************************************
 *  调用EasyQuery查询保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function easyQueryClick()
{
	initPolGrid();
	
	// 书写SQL语句
	var strSql = "";
	/*
	strSql = "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName, Operator, MakeDate from LCPol where 1=1 "
				 + "and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+ComCode+"','Pb')=1 "//分公司模式权限管理体现
				 + "and AppFlag='0' "
				 + "and UWFlag='0' "
				 + "and ApproveFlag = '0' "  //复核标志为0的才能查出来，复核不通过的不行
				 + "and GrpPolNo='00000000000000000000' "
				 + "and ContNo='00000000000000000000' "
				 + getWherePart( 'ProposalNo' )
				 + getWherePart( 'ManageCom', 'ManageCom' )//查询条件中的集中权限管理体现
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'AgentGroup' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'PrtNo' )
				 + getWherePart( 'MakeDate' )	
				 + " and ManageCom like '" + ComCode + "%%'";	//集中权限管理体现		 
				 + " order by MakeDate desc";

	strSql = "select ProposalContNo,PrtNo,AppntName,InsuredName, Operator, MakeDate from LCCont where 1=1 "
				 //+ "and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+ComCode+"','Pb')=1 "//分公司模式权限管理体现
				 + "and ApproveFlag = '0' "  //复核标志为0的才能查出来，复核不通过的不行
				 + getWherePart( 'ProposalContNo','ProposalNo' )
				 + getWherePart( 'ManageCom', 'ManageCom' )//查询条件中的集中权限管理体现
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'AgentGroup' )
				 + getWherePart( 'PrtNo' )
				 + getWherePart( 'MakeDate' )	
				 + " and ManageCom like '" + ComCode + "%%'";	//集中权限管理体现		 
				 + " order by MakeDate desc";			 
   	*/               
   	
   	var hasBPOErr = "";
   	if(fm.HaseBPOErr.value == "0")                 
   	{
   	  hasBPOErr = " = ";
   	}
   	else if(fm.HaseBPOErr.value == "1")                 
   	{
   	  hasBPOErr = " > ";
   	}
   	
   	strSql = "select a.missionprop1,a.missionprop2,a.missionprop4,a.missionprop5, "
        + "a.missionprop6,a.Missionid,a.submissionid, "
        + "(select name from ldcom where comcode=a.MissionProp8), "
        + "(select max(char(makedate)||' '||char(maketime)) from es_doc_main where doccode=a.MissionProp2 and subtype='" + tSubType + "'), "
        //+ "case when (select count(1) from BPOIssue a, BPOLCPol b where a.BPOBatchNo = b.BPOBatchNo and a.ContID = b.ContID and b.PrtNo = a.MissionProp2) > 0 then '有' else '无' end, "
        + " case when a.MissionProp9 = '1' then '有' else '无' end,"
        + "CodeName('approvestate', ActivityStatus), CodeName('lcsalechnl', SaleChnl),case when (select count(1) from LJSPay where OtherNo = b.prtno and BankOnTheWayFlag = '1') > 0 then '是' else '否' end "
        + "from LWMission a, LCCont b,laagent la where b.PrtNo = a.MissionProp2 "
        + "and a.ActivityID = '" + tActivityID + "' "
        + "and a.ProcessID = '" + tProcessID + "' "
        + "and ActivityStatus in ('1', '3', '4', '5') "
        + "and b.agentcode=la.agentcode "
        + getWherePart('a.MissionProp1','ProposalNo')
        + getWherePart('a.MissionProp2','PrtNo')
        + getWherePart('a.MissionProp6','MakeDate')
        + getWherePart('la.groupagentcode','AgentCode')
        + getWherePart('a.MissionProp8','ManageCom','like')
        + getWherePart('b.SaleChnl','SaleChnl')
        + (hasBPOErr == "" ? "" : " and (select count(1) from BPOIssue a, BPOLCPol b where a.BPOBatchNo = b.BPOBatchNo and a.ContID = b.ContID and b.PrtNo = a.MissionProp2) " + hasBPOErr + " 0 ")
        + " order by a.MissionProp6 "
        ;
	//execEasyQuery( strSql );
	turnPage.queryModal(strSql, PolGrid);
    
    queryPendingCont();
}

/*********************************************************************
 *  显示EasyQuery的查询结果
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
	  initPolGrid();
		//HZM 到此修改
		PolGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		PolGrid.loadMulLine(PolGrid.arraySave);		
		//HZM 到此修改
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		PolGrid.delBlankLine();
	} // end of if
}

function showApproveDetail()
{ 
    var tPolGridSelNo = PolGrid.getSelNo() - 1;
    var tPendingContGridSelNo = PendingContGrid.getSelNo() - 1;
    
    if(tPolGridSelNo == "-1" && tPendingContGridSelNo == "-1")
    {
        alert("请先选择一条保单信息！");
        return false;
    }
    
    if(tPolGridSelNo != "-1" && tPendingContGridSelNo != "-1")
    {
        alert("不能同事选中多条保单信息！");
        return false;
    }
    
    var tSelRowDate = null;
    if(tPolGridSelNo != "-1")
    {
        tSelRowDate = PolGrid.getRowData(tPolGridSelNo);
    }
    else
    {
        tSelRowDate = PendingContGrid.getRowData(tPendingContGridSelNo);
    }
    
    var tMissionID = tSelRowDate[5];
    var tSubMissionID = tSelRowDate[6];
    var polNo = tSelRowDate[0];
    var prtNo = tSelRowDate[1];
    var contNo = tSelRowDate[0];
    
//by gzh 增加保单录入人员与复核人员不能为同一人的校验
  var OperatorSql = "select inputoperator from lccont where prtno = '"+prtNo+"' ";
  var OperatorResult = easyExecSql(OperatorSql);
  if(OperatorResult == null){
  	alert("获取保单录入人员信息失败！");
  	return false;
  }else if(OperatorResult[0][0]==Operator){
  	alert("该印刷号的投保单由您（"+OperatorResult[0][0]+"）录入，录入和复核不能为同一人!");
  	return false;
  }
  
  var strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and  CreatePos='承保复核' and  PolState=1003";
  var arrResult = easyExecSql(strSql);
  if (arrResult!=null && arrResult[0][1]!=Operator) {
    alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
    return;
  }
  //锁定该印刷号
  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保复核&PolState=1003&Action=INSERT";
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 


  //配合以前实现过的页面功能，源于ProposalMain.jsp
  mSwitch.deleteVar("PolNo");
	mSwitch.addVar("PolNo", "", polNo);
	mSwitch.updateVar("PolNo", "", polNo);
	//alert(polNo);
	
	mSwitch.deleteVar("ApprovePolNo");
	mSwitch.addVar("ApprovePolNo", "", polNo);
	mSwitch.updateVar("ApprovePolNo", "", polNo);
	//alert(mSwitch.getVar("ApprovePolNo"));

  easyScanWin = window.open("./ProposalEasyScan.jsp?LoadFlag=5&ContNo="+contNo+"&prtNo="+prtNo+"&MissionID="+tMissionID+"&SubMissionID="+tSubMissionID+"&scantype=scan&ProcessID=" + tProcessID + "&ActivityID=" + tActivityID, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");    
  initPolGrid();
}

/*********************************************************************
 *  进行投保单复核提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function passApprovePol() {
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //提交
  var polNo = mSwitch.getVar("ApprovePolNo");
  //alert(polNo);
	window.top.fraSubmit.window.location = "./ProposalApproveSave.jsp?polNo="+polNo+"&approveFlag=9";    
}

function refuseApprovePol() {
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
	showSubmitFrame(mDebug);
  //提交
  var polNo = mSwitch.getVar("ApprovePolNo");
	window.top.fraSubmit.window.location = "./ProposalApproveSave.jsp?polNo="+polNo+"&approveFlag=1";    
}

//************************
var cflag = "5";          //问题件操作位置 5.复核

function QuestInput()
{
	cProposalNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);  //保单号码

	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("../uw/QuestInputMain.jsp?ContNo="+cProposalNo+"&Flag="+cflag,"window1");
	
	//initInpBox();
    	//initPolBox();
	//initPolGrid();
	
}

function QuestReply()
{
	cProposalNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);  //保单号码
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("../uw/QuestReplyMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	
	//initInpBox();
  //initPolBox();
	//initPolGrid();
	
}

function QuestQuery()
{
	cProposalNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);  //保单号码
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("../uw/QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	
	//initInpBox();
  //initPolBox();
	//initPolGrid();
	
}


function returnparent()
  {   
  	var backstr=fm.all("ContNo").value;    
  	//alert(backstr+"backstr");   
  	mSwitch.deleteVar("ContNo");               
	mSwitch.addVar("ContNo", "", backstr);     
	mSwitch.updateVar("ContNo", "", backstr); 
  	location.href="ContInsuredInput.jsp?LoadFlag=5&ContType="+ContType; 
}    


function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }
 if(cCodeName=="agentgroup1"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
 
}


/**
 * 待回复保单查询
 */ 
var turnPage1 = new turnPageClass();
function queryPendingCont()
{
    initPendingContGrid();

    var strSql = "";         

    var hasBPOErr = "";
    if(fm.HaseBPOErr.value == "0")                 
    {
       hasBPOErr = " = ";
    }
    else if(fm.HaseBPOErr.value == "1")                 
    {
       hasBPOErr = " > ";
    }
    
    strSql = ""
        + " select lwm.missionprop1, lwm.missionprop2, lwm.missionprop4, lwm.missionprop5, "
        + " lwm.missionprop6, lwm.Missionid, lwm.submissionid, "
        + " (select name from ldcom where comcode=lwm.MissionProp8), "
        + " (select max(char(makedate)||' '||char(maketime)) from es_doc_main where doccode=lwm.MissionProp2 and subtype='" + tSubType + "'), "
        //+ " case when (select count(1) from BPOIssue a, BPOLCPol b where a.BPOBatchNo = b.BPOBatchNo and a.ContID = b.ContID and b.PrtNo = lwm.MissionProp2) > 0 then '有' else '无' end, "
        + " case when lwm.MissionProp9 = '1' then '有' else '无' end,"
        + " CodeName('approvestate', lwm.ActivityStatus), "
        + " lca.groupAgentCode AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " (select labg.BranchAttr from LABranchGroup labg where labg.AgentGroup = lcc.AgentGroup) BranchAttr, "
        + " (select labg.Name from LABranchGroup labg where labg.AgentGroup = lcc.AgentGroup) BranchAttrName, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) "
        + " from LWMission lwm "
        + " inner join LCCont lcc on lcc.ContNo = lwm.MissionProp1 "
        + " inner join Laagent lca on lcc.agentcode = lca.agentcode "
        + " where 1 = 1 "
        + " and lwm.Activityid = '" + tActivityID + "' "
        + " and lwm.Processid = '" + tProcessID + "' "
        + " and lwm.ActivityStatus = '2' "
        + getWherePart('lwm.MissionProp1', 'ProposalNo')
        + getWherePart('lwm.MissionProp2', 'PrtNo')
        + getWherePart('lwm.MissionProp6', 'MakeDate')
        + getWherePart('lca.groupagentcode', 'AgentCode')
        + getWherePart('lwm.MissionProp8', 'ManageCom', 'like') 
        + getWherePart('lcc.SaleChnl', 'SaleChnl')
        + (hasBPOErr == "" ? "" : "  and (select count(1) from BPOIssue a, BPOLCPol b where a.BPOBatchNo = b.BPOBatchNo and a.ContID = b.ContID and b.PrtNo = lwm.MissionProp2) " + hasBPOErr + " 0 ")
        + " order by lwm.MissionProp6 "
        ;
//prompt('',strSql);
    turnPage1.pageDivName = "divPendingContGridPage";
    turnPage1.queryModal(strSql, PendingContGrid);
}

/**
 * 查询保单
 */
function queryCont()
{
    easyQueryClick();
    queryPendingCont();
}

function downloadPendingContClick()
{
    if(PendingContGrid.mulLineCount == 0)
    {
        alert("列表中没有数据可下载");
    }
    
    fm.querySql.value = PendingContGrid.SortPage.strQuerySql; 
  
    var oldAction = fm.action;
    fm.action = "ProposalApprovePendingQuestContDownload.jsp";
    fm.submit();
    fm.action = oldAction;
}

