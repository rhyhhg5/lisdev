//程序名称：
//程序功能：
//创建日期：2007-9-28 16:03
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var showInfo = null;

//-------------------------------------事件响应区----------------------------------
function sendScan()
{
  var showStr="正在进行处理，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	fm.action = "BPOManuSendSave.jsp";
	fm.submit();
}

function getXml()
{
  var showStr="正在进行处理，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	fm.action = "BPOManuGetSave.jsp";
	fm.submit();
}

function afterSubmit(FlagStr, content )
{
	showInfo.close();
	window.focus();
	
	if(FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}
