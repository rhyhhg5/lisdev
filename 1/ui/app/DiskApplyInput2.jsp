<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="DiskApplyInput2.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="DiskApplyInit2.jsp"%>
<script language='javascript'>
var showInfo;
var turnPage = new turnPageClass();        
var arrDataSet;
var ImportPath;
var GrpContNo ="<%=request.getParameter("GrpContNo")%>";
var ImportState = "no";
window.onbeforeunload = beforeAfterInput;
window.onunload= AfterInput;
function beforeAfterInput() {
   if ( ImportState=="Importing" )
   {
     alert("磁盘投保尚未完成，请不要离开!");
     return false;
     
   }
 }
 
 function AfterInput()
 {
   if ( ImportState=="Importing" )
   {
     
     return false;
     
   }
 }
function submitForm()
{
  if ( ImportState =="Succ")
  {
    if ( !confirm("确定您要再次磁盘投保吗?") )
    {
       return false ;
    }
   
   }
   
	 var i = 0;
	 getImportPath();
 
  	
	ImportFile = fm.all('FileName').value;
	var prtno = getPrtNo();
	var tprtno = ImportFile;
	
	if ( tprtno.indexOf("\\")>0 ) tprtno =tprtno.substring(tprtno.lastIndexOf("\\")+1); 
	if ( tprtno.indexOf("/")>0 ) tprtno =tprtno.substring(tprtno.lastIndexOf("/")+1); 
	if ( tprtno.indexOf("_")>0) tprtno = tprtno.substring( 0,tprtno.indexOf("_"));
	if ( tprtno.indexOf(".")>0) tprtno = tprtno.substring( 0,tprtno.indexOf("."));


	if ( prtno!=tprtno )
	{
	   alert("文件名与印刷号不一致,请检查文件名!");   
	   return ;
	}
	else
	{
		
		document.all("info").innerText="上传文件导入中，请稍后..."	
		ImportState = "Importing";
		fm.action = "./DiskApplySave.jsp?ImportPath="+ImportPath+"&mGrpContNo="+fm.mGrpContNo.value;	
    	fm.submit(); //提交
    }
    
}


function getImportPath ()
{
		// 书写SQL语句
	var strSQL = "";

	strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
			 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	alert("未找到上传路径");
    return;
	}
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  ImportPath = turnPage.arrDataCacheSet[0][0];
  
}

function getPrtNo ()
{
		// 书写SQL语句
	var strSQL = "";

	strSQL = "select PrtNo from lcgrpcont where GrpContNo ='"+ GrpContNo +"'";
			 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	alert("未查找到保单:" + GrpContNo);
    return;
	}
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  return turnPage.arrDataCacheSet[0][0];
  
  
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Result, mBatchNo )
{
	queryLog(mBatchNo);
	if(FlagStr=="Succ")
  	document.all("info").innerText="导入成功";
  if(FlagStr=="Fail")
  	document.all("info").innerText=content;	
  ImportState =FlagStr ;
}
//查询报错信息
function queryLog(mBatchNo)
{
	var strSQL="select BatchNo,contid,insuredid,riskcode,errorinfo,insuredname from LCGrpImportLog where BatchNo='"+mBatchNo+"' and errorinfo NOT LIKE '导入成功%%'";
  turnPage.queryModal(strSQL,ImportGrid);
}

</script>
</head>
<body onload="initForm();">
  <form action="./DiskApplySave.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
<table class = common >
    <TR  >
      <TD  width='20%' style="font:9pt">
        文件名：
      </TD>
      
      <TD  width='80%'>
        <Input  type="file"　width="100%" name=FileName class= common>
        <INPUT  VALUE="上载被保险人清单" class="cssbutton" TYPE=button onclick = "submitForm();" >
        <INPUT  VALUE="导入" TYPE=button onclick = "submitForm();" >
        <Input  type="hidden"　width="100%" name="insuredimport" value ="1">
      </TD>
    </TR>
    <TR>
      <TD  colspan=2>
        
      </TD>
  	</TR>
    <input type=hidden name=ImportFile>
    
</table>
<table class = common >
  <TR  >
      <TD  id="info" width='100%' style="font:10pt">
        
      </TD>
      </TR>
</table>
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart2);">
					</td>
					<td class= titleImg>
					错误信息
				</td>
			</tr>
		</table>
		<div  id= "divLCImpart2" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanImportGrid" >
						</span>
					</td>
				</tr>
			</table>
		</div>
	<Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=button VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=button VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=button VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=button VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  <INPUT CLASS=cssbutton VALUE="自动下发问题件" TYPE=button onclick="ChoiceAutoQuest();">
  <input type="text" name="mGrpContNo" value="<%=request.getParameter("GrpContNo")%>">
</form>
</body>