<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TempFeeWithdrawSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  
<%
  System.out.println("\n\n---TempFeeWithdrawSave Start---");
  
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String strResult = "";

	GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  
  LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
  LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
  
  String tTempFeeNo[] = request.getParameterValues("FeeGrid2");
  String tTempFeeType[] = request.getParameterValues("FeeGrid3");
  String tRiskCode[] = request.getParameterValues("FeeGrid4");
  String tGetReasonCode[] = request.getParameterValues("FeeGrid1");
  String tChk[] = request.getParameterValues("InpFeeGridChk"); 
   String tPayMode = request.getParameter("qPayMode");
  //循环处理所有进行选择的行
  for (int index=0; index<tChk.length; index++) {
    if (tChk[index].equals("1")) {          
      LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
      tLJTempFeeSchema.setTempFeeNo(tTempFeeNo[index]);
      tLJTempFeeSchema.setTempFeeType(tTempFeeType[index]);
      tLJTempFeeSchema.setRiskCode(tRiskCode[index]);     
      tLJTempFeeSet.add(tLJTempFeeSchema);
      
      LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
      tLJAGetTempFeeSchema.setGetReasonCode(tGetReasonCode[index]);
      tLJAGetTempFeeSchema.setPayMode(tPayMode);
      tLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
    }
  }     
	//System.out.println("tLJTempFeeSet:" + tLJTempFeeSet.encode());
	
  // 准备传输数据 VData
	VData tVData = new VData();
	tVData.add(tLJTempFeeSet);
	tVData.add(tLJAGetTempFeeSet);
	tVData.add(tG);

  // 数据传输
	TempFeeWithdrawUI tTempFeeWithdrawUI = new TempFeeWithdrawUI();
	tTempFeeWithdrawUI.submitData(tVData, "INSERT");
  
  tError = tTempFeeWithdrawUI.mErrors;
  System.out.println("needDeal:"+tError.needDealError());
  if (!tError.needDealError()) {                          
  	Content = " 退费成功! ";
  	FlagStr = "Succ";
  	System.out.println("----------after submitData");
  	//strResult = (String)tTempFeeWithdrawUI.getResult().get(0);
  	//System.out.println("strResult:" + strResult);
  }
  else {
  	Content = " 退费失败，原因是:" + tError.getFirstError();
  	FlagStr = "Fail";
  }

	System.out.println(Content + "\n" + FlagStr + "\n---TempFeeWithdrawSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.tempFeeNoQuery(1);
	
	//parent.fraInterface.open("./TempFeeWithdrawPrintMain.jsp?prtData=<%=strResult%>");
	//parent.fraInterface.fm2.PrtData.value = "<%=strResult%>";
	//parent.fraInterface.fm2.submit();
</script>
</html>
