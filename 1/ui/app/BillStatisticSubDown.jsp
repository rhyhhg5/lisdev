<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BillStatisticSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  String mStartDate=request.getParameter("StartDate");
  String mEndDate=request.getParameter("EndDate");
  System.out.print("aaaaaaabbbbbbbb");
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "新契约单证统计_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);

  String querySql = request.getParameter("querySql");
  
  querySql = querySql.replaceAll("%25","%");

  //设置表头
  String[][] tTitle = new String[2][];
  tTitle[0] = new String[] { "制表人：" , tG.Operator, "",
			"统计时间："+mStartDate+"至"+ mEndDate,"","","" };
  tTitle[1] = new String[] {"问题件类","管理机构","单证号码","初审人员","业务人员编码","下发日期","回销日期"};
 
 //String[][] tTitle = {{"问题件类","管理机构","单证号码","初审人员","业务人员编码","下发日期","回销日期"}};
	
  
  //表头的显示属性
  int []displayTitle = {1,2,3,4,5,6,7};
    
  //数据的显示属性
  int []displayData = {1,2,3,4,5,6,7};
  
  //生成文件
  CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
  createexcellist.createExcelFile();
  String[] sheetName ={"list"};
  createexcellist.addSheet(sheetName);
  int row = createexcellist.setData(tTitle,displayTitle);
  
  if(row ==-1) errorFlag = true;
  createexcellist.setRowColOffset(row+1,0);//设置偏移
  if(createexcellist.setData(querySql,displayData)==-1)
  {
  	errorFlag = true;
  	System.out.println(errorFlag);
  }
  if(!errorFlag)
  //写文件到磁盘
  try{
     createexcellist.write(tOutXmlPath);
  }catch(Exception e)
  {
  	errorFlag = true;
  	System.out.println(e);
  }
  //返回客户端
  if(!errorFlag)
  	downLoadFile(response,filePath,downLoadFileName);
  out.clear();
	out = pageContext.pushBody();
%>

