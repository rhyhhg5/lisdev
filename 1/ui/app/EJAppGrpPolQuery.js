//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
  if (PolGrid.getSelNo() == 0) { 
    alert("请先选择一条保单信息！");
    return false;
  } 
  
  var polNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  var prtNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
  var strSql = "SELECT 1 from lcgrppol l where prtno='"+prtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
	var arrResult = easyExecSql(strSql);
        mSwitch.deleteVar("PolNo");
	mSwitch.addVar("PolNo", "", polNo);
	mSwitch.updateVar("PolNo", "", polNo);
	
	mSwitch.deleteVar("GrpContNo");
	mSwitch.addVar("GrpContNo", "", polNo);
	mSwitch.updateVar("GrpContNo", "", polNo);

	if (arrResult == null) {
    	var str = "SELECT 1 from lwmission where missionprop1='"+prtNo+"' and activityid  in ('0000002098','0000002099') and  missionprop5='1'";
	   	var arr = easyExecSql(str);
	   	if (arr == null) {  
        	easyScanWin = window.open("./GroupPolApproveInfo.jsp?LoadFlag=16&polNo="+polNo+"&prtNo="+prtNo, "", "status=no,resizable=yes,scrollbars=yes ");    
       	}else{
   			easyScanWin = window.open("../uliapp/GroupPolApproveInfo.jsp?LoadFlag=16&polNo="+polNo+"&prtNo="+prtNo, "", "status=no,resizable=yes,scrollbars=yes ");    
        }
	}else{
    	easyScanWin = window.open("../uliapp/GroupPolApproveInfo.jsp?LoadFlag=16&polNo="+polNo+"&prtNo="+prtNo, "", "status=no,resizable=yes,scrollbars=yes ");    
   	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{	
	// 初始化表格
	initPolGrid();
	if(!verifyInput2())
	return false;
	// 书写SQL语句
	var strSQL = ""
        + " select distinct "
        + " lgc.GrpContNo GrpContNo, lgc.PrtNo PrtNo,CodeName('salechnl', lgc.SaleChnl) SaleChnl, "
        + " lgc.ManageCom ManageCom,getunitecode(lgc.AgentCode),"
        + " lgc.GrpName, "
        //+ " lci.name,CodeName('sex',lci.sex),lci.idno,lci.birthday,"
        + " lgc.CValidate CValidate "
        + " from LCGrpCont lgc "
        + " inner join LCInsured lci on lgc.prtno = lci.prtno "
        + " where 1 = 1  and lgc.appflag='9' and (lgc.cardflag is null or lgc.cardflag != '0') "
        + getWherePart("lgc.GrpContNo", "GrpContNo")
        + getWherePart("lgc.PrtNo", "PrtNo")
        + getWherePart("lgc.SaleChnl", "SaleChnl")
        + getWherePart("lgc.ManageCom", "ManageCom", "like")
        + getWherePart("getunitecode(lgc.AgentCode)", "AgentCode")
        + getWherePart("lgc.GrpName", "GrpName", "like")
        + getWherePart("lci.Name", "InsuredName")
        + getWherePart("lci.Sex", "InsuredSex")
        + getWherePart("lci.IDNo", "InsuredIDNo")
        + getWherePart("lci.Birthday", "InsuredBirthday")
        + getWherePart("lgc.Cvalidate", "StartCvalidate", ">=")
        + getWherePart("lgc.Cvalidate", "EndCvalidate", "<=")
        ;

    fm.querySql.value = strSQL;
    var tResult = easyExecSql(strSQL);
	if(!tResult){
		alert("未查询到数据！");
		return false;
	}
	turnPage.queryModal(strSQL, PolGrid);
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		//HZM 到此修改
		PolGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		PolGrid.loadMulLine(PolGrid.arraySave);		
		//HZM 到此修改
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//PolGrid.delBlankLine();
	} // end of if
}
/*********************************************************************
 *  点击扫描件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function ScanQuery()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var prtNo = PolGrid.getRowColData(tSel - 1,2);	
	    var subTypeValue ="";			
		if (prtNo == "")
		    return;
		 //
 		var strSql = "SELECT 1 from lcgrppol l where prtno='"+prtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
	    var arrResult = easyExecSql(strSql);
	    if (arrResult == null) {
    	    var str = "SELECT 1 from lwmission where missionprop1='"+prtNo+"' and activityid  in ('0000002098','0000002099') and  missionprop5='1'";
	   	    var arr = easyExecSql(str);
	   	    if (arr == null){
	   	    	var tBriefSQL = "select 1 from lcgrpcont where prtno = '"+prtNo+"' and ContPrintType = '4' ";
	   	    	var tBriefArr = easyExecSql(tBriefSQL);
	   	    	if(tBriefArr){
	   	    		var tXHSQL = "select 1 from es_doc_main where doccode = '"+prtNo+"' and subtype = 'TB26' ";
	   	    		var tXHArr = easyExecSql(tXHSQL);
	   	    		if(tXHArr){
	   	    			subTypeValue = "TB26";
	   	    		}else{
	   	    			subTypeValue = "TB1002";
	   	    		}
	   	    	}else{
	   	        	subTypeValue = "TB1002";
	   	        }
       	    }else{
       	        subTypeValue = "TB1007";
            }
	    }else{
	    	subTypeValue = "TB1007";
   	    }
		 window.open("../sys/ProposalEasyScan.jsp?prtNo="+prtNo+"&SubType="+subTypeValue+"&BussType=TB&BussNoType=12", "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");								
	}	     
}

function ChkState()
{
	fm.action ="../uw/GrpPolstatusChk.jsp";
	fm.submit(); //提交
}

function afterSubmit(FlagStr,Content)
{
	
}
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }
 if(cCodeName=="agentgroup2"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
 
}


function downloadQryResults()
{
    var tStrSql = fm.querySql.value;
    if(tStrSql == null || tStrSql == "")
    {
        alert("请先进行查询后，再下载查询结果清单。");
        return false;
    }
    
    fm.action = "./EJAppGrpContListSave.jsp";
    fm.submit();
    fm.action = oldAction;
    
    return true;
}

function queryPhone1()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var prtNo = PolGrid.getRowColData(tSel - 1,2);	
		if (prtNo == ""){
			alert("获取印刷号失败！");
		}
		window.open("./PhoneQueryInput.jsp?prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");								
	}	     
}

function queryEstimate() {
	var tSel = PolGrid.getSelNo();
	if(tSel == 0 || tSel == null){
		alert( "请先选择一条记录。" );
		return false;
	}else{
		var prtNo = PolGrid.getRowColData(tSel - 1,2);	
		if (prtNo == ""){
			alert("获取印刷号失败！");
			return false;
		}
		var tSQL = "select grpcontno from lcgrpcont where prtno = '"+prtNo+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("获取保单号码失败！");
			return false;
		}
		var tGrpContNo = arrResult[0][0];
		showInfo = window.open("./EstimateMain.jsp?GrpContNo=" + tGrpContNo +"&LookFlag=1");
	}
}

function HJAppnt(){
	var tSel = PolGrid.getSelNo();
	if(tSel == 0 || tSel == null){
		alert( "请先选择一条记录。" );
		return false;
	}else{
		var prtNo = PolGrid.getRowColData(tSel - 1,2);	
		if (prtNo == ""){
			alert("获取印刷号失败！");
			return false;
		}
		var tSQL = "select grpcontno from lcgrpcont where prtno = '"+prtNo+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("获取保单号码失败！");
			return false;
		}
		var tGrpContNo = arrResult[0][0];
		var tContPrintType = "";
		var tContPrintTypeSQL = "select ContPrintType,ManageCom from lcgrpcont where grpcontno = '"+tGrpContNo+"' ";
		var tContPrintTypeArr = easyExecSql(tContPrintTypeSQL);
		if(!tContPrintTypeArr){
			alert("获取保单数据失败！");
			return false;
		}else{
			tContPrintType = tContPrintTypeArr[0][0];
		}
		if(tContPrintType!="" && tContPrintType!="5"){
			alert("非汇交件不能查看投保人信息！");
			return;
		}else{
			showInfo = window.open("./HJAppntMain.jsp?GrpContNo="+tGrpContNo+"&flag=view");
		}

		
	}
	
}