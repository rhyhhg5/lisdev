<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>    
<%
//程序名称：QYTSInput.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  String CurrentDate= PubFun.getCurrentDate();   
             	               	
  String AheadDays="-180";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="QYTSInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<script>
	function initDate(){
    fm.StartDate.value="<%=afterdate%>";
    fm.EndDate.value="<%=CurrentDate%>";
  }
</script>
</head>
<body  onload="initDate();">    
  <form action="./QYTSReport.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>       
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
    </table>
    <hr>
     <table class=common>
		  	<TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="0">《各机构投保件数、保费》报表</td>
		        </tr>
		    <TR  class= common8>		  	   
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="1">《各机构承保件数、保费》报表</td>
		        </tr>
		    <TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="2">《分机构万能险期缴保费、基本保费、额外保费、追加保费保额、缴费年期》报表</td>
		        </tr>
		    <TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="3">《各机构问题件、体检件、契调件件数、及三个的回销件数》报表</td>
		        </tr>
		    <TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="4">《各机构待收费件数、保费》报表</td>
		        </tr>
		    <TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="5">《各机构外包错误修改件数》报表</td>
		        </tr>
		    <TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="6">《各机构新单撤消件数、保费》报表</td>
		        </tr>   
		        <TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="7">《各机构新单删除件数、保费》报表</td>
		        </tr>   
		        <TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="8">《各机构送人工核保件数》报表</td>
		        </tr>   
		        <TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="9">《各机构正常承保件数、保费，延期承保件数、保费，拒保件数、保费，变更承保件数、保费》报表</td>
		        </tr>   
		        <TR  class= common8>		  	  
		        <td colspan=6><INPUT TYPE="checkBox" NAME="PrintCode" value="10">《各机构体检阳性率总件数》报表</td>
		        </tr>        
	</table>
    <table class=common>
    	<tr class=common>
          <TD  class= title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="Print()"></TD>
			</tr>
	</table>
<Input type="hidden" class=common name="fmtransact">
	
  </form>
  <span id="spanCode" style="display: none; position:absolute; slategray"></span>
<iframe name="printfrm" src="" width=0 height=0></iframe>
<form id=printform target="printfrm" action="" method = "post">
<input type=hidden name=filename value=""/>
</form>
</body>
</html> 