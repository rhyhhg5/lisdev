//程序名称：
//程序功能：
//创建日期：2011-4-19
//创建人  ：zhangyang
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询团单。
 */
function queryGrpCont()
{
    if(!verifyInput2())
    {
        return false;
    }
    
    var tStrCond = "";
    var tScanFlag = fm.ScanFlag.value;
    
    if(tScanFlag == "0")
    {
        tStrCond = " and not exists (select 1 from Es_Doc_Main where SubType in ('TB08', 'TB09') and DocCode = lgc.PrtNo) ";
    }
    else if(tScanFlag == "1")
    {
        tStrCond = " and exists (select 1 from Es_Doc_Main where SubType in ('TB08', 'TB09') and DocCode = lgc.PrtNo) ";
    }

    var tStrSql = ""
        + " select lgc.GrpContNo, lgc.PrtNo, lgc.GrpName, "
        + " CodeName('salechnl', lgc.SaleChnl) SaleChnl, "
        + " getUniteCode(lgc.AgentCode), lgc.CValidate, lgc.ManageCom, "
        + " '打印', '1' "
        + " from LCGrpCont lgc "
        + " where 1 = 1 "
        + " and lgc.CardFlag = '4' "
        + " and lgc.PrintCount < 1 " 
        + " and lgc.AppFlag = '1' " 
        + tStrCond 
        + getWherePart("lgc.ManageCom", "ManageCom", "like")
        + getWherePart("lgc.GrpContNo", "GrpContNo")
        + getWherePart("lgc.PrtNo", "PrtNo")
        + getWherePart("getUniteCode(lgc.AgentCode)", "AgentCode")
        + getWherePart("lgc.CValiDate", "CValidateStart", ">=") 
        + getWherePart("lgc.CValiDate", "CValidateEnd", "<=") 
        + " with ur "
        ;
        
    turnPage1.pageDivName = "divGrpContGridPage";
    turnPage1.queryModal(tStrSql, GrpContGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有查到保单信息！");
        return false;
    }
    
    return true;
}


function printGrpPDFBatch()
{
	//判定是否有选择打印数据
    var count = 0;
    var tGetRow = 0;
	for(var i = 0; i < GrpContGrid.mulLineCount; i++ )
	{
		if( GrpContGrid.getChkNo(i) == true )
		{
			count ++;
			tGetRow = i;
		}
	}
	if(count == 0){
		alert("请选择一张保单后，再进行打印操作！");
		return;
	}
	if(count >1){
		alert("只能选择一张保单进行打印！");
		return;
	}
    var tRowDatas = GrpContGrid.getRowData(tGetRow);
    var tOtherno = tRowDatas[0];
    var tContPrintFlag = tRowDatas[8];

    if(tContPrintFlag == "0")
    {
        fm.target = "fraSubmit";
        fm.action = "./GrpGsContPrintNoPrintSave.jsp?OtherNo=" + tOtherno;
        fm.submit();
    }
    else
    {
        fm.fmtransact.value = "PRINT";
        fm.target = "fraSubmit";
        fm.action = "../uw/PDFPrintSave.jsp?Code=GS002&OtherNo=" + tOtherno;
        fm.submit();
    }
    
    fm.PrintButton.disabled = true;

    return true;
}

//function printGrpInsuPDFBatch()
//{
//	var tGetRow = GrpContGrid.getSelNo() - 1;
//    if(tGetRow < 0)
//    {
//        alert("请选取一条记录！");
//        return false;
//    }
//    
//    var tRowDatas = GrpContGrid.getRowData(tGetRow);
//    var tOtherno = tRowDatas[0];
//    
//    //fm.fmtransact.value = "PRINT";
//    fm.target = "fraSubmit";
//    fm.action = "./GrpGsContPrintSave.jsp?OtherNo=" + tOtherno;
//    fm.submit();
//    
//    fm.PrintInsuButton.disabled = true;
//}

/**
 * PDF打印后回调函数
 */
function afterSubmit2(FlagStr, Content)
{
    // showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("打印失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    queryGrpCont();
    
    fm.PrintButton.disabled = false;
//    fm.PrintInsuButton.disabled = false;
}

function printAllGrpPDFBatch()
{
	var count = 0;
	for(var i = 0; i < GrpContGrid.mulLineCount; i++ )
	{
		if( GrpContGrid.getChkNo(i) == true )
		{
			count ++;
		}
	}
	
	if(count == 0){
		alert("请选择一张保单后，再进行打印操作！");
		return;
	}
    fm.target = "fraSubmit";
    fm.action = "./GrpGsAllContPrintSave.jsp?Code=GS002";
    fm.submit();
    
    fm.PrintButton.disabled = true;

    return true;
}


