<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2009-7-15
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.wiitb.*" %>>

<%
System.out.println("GrpGsContPrintNoPrintSave.jsp start ...");

String FlagStr = "Fail";
String Content = "";
GlobalInput tG = (GlobalInput)session.getValue("GI");

try
{
    String tGrpContNo = request.getParameter("OtherNo");
    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
    
    tVData.add(tTransferData);
    
    tG.ClientIP = request.getRemoteAddr(); //操作员的IP地址
    System.out.println("------操作员的IP地址:"+tG.ClientIP);
  
    tVData.add(tG);
    
    GrpGSNoPrtPrintBL tGrpGSNoPrtPrintBL = new GrpGSNoPrtPrintBL();
    if(!tGrpGSNoPrtPrintBL.submitData(tVData, ""))
    {
        Content = " 打印失败，原因是: " + tGrpGSNoPrtPrintBL.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 打印成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 打印失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("GrpGsContPrintNoPrintSave.jsp end ...");

%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit2("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
