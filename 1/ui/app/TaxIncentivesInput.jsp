<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
	tGI=(GlobalInput)session.getValue("GI");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaxIncentivesInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="TaxIncentivesInit.jsp"%>
  <title>税收团体客户查询</title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询税收团体客户条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
      	  <TD  class= title>
            团体编号：
          </TD>
          <TD  class= input>
            <Input class= common name=GrpNo >
          </TD>
          <TD  class= title>
            单位名称：
          </TD>
          <TD  class= input>
            <Input class= common name=GrpName >
          </TD>
          <TD  class= title>
            税务登记证号码：
          </TD>
          <TD  class= input>
          	<Input class= common name=TaxNo >
          </TD>
        </TR>
        <TR>
        <TD  class= title>
            社会信用代码：
        </TD>
        <TD  class= input>
           <Input class= common name=OrgancomCode >
        </TD>
        </TR>
    </table>
          <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrp1);">
    		</td>
    		<td class= titleImg>
    			 税收团体客户信息
    		</td>
    	</tr>
    	<tr>
    	</tr>
    </table>
  	<Div  id= "divLCGrp1" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>				
  	</Div>
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  	<p>
  	  <INPUT VALUE="新增客户信息" class = cssButton TYPE=button onclick="AddInput();"> 
      <INPUT VALUE="修改客户信息" class = cssButton TYPE=button onclick="UpdateInput();">      
  	</p>  	      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
