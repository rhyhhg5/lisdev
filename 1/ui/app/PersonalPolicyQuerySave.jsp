<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bank.*"%>

<%
  String FlagStr = "";
  String Content = "";
    System.out.println("---print--getadvance---");
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  String StartDate=String.valueOf(request.getParameter("StartDate"));
  String EndDate=String.valueOf(request.getParameter("EndDate"));
  String IDNo=request.getParameter("IDNo");

  String fileName = "个单保单信息" + StartDate +"_"+ EndDate + "_"+ IDNo + ".xls";
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
  tTransferData.setNameAndValue("StartDate",request.getParameter("StartDate"));
  tTransferData.setNameAndValue("EndDate",request.getParameter("EndDate"));
  tTransferData.setNameAndValue("ManageCom",request.getParameter("ManageCom"));
  tTransferData.setNameAndValue("IDNo",request.getParameter("IDNo"));

  try
  {
      VData vData = new VData();
      vData.add(tG);
      vData.add(tTransferData);
      PersonalPolicyQueryUI tPersonalPolicyQueryUI = new PersonalPolicyQueryUI();
      if (!tPersonalPolicyQueryUI.submitData(vData))
      {
          Content = "报表下载失败，原因是:" + tPersonalPolicyQueryUI.mErrors.getFirstError();
          FlagStr = "Fail";
      }
      else
      {
          String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
          File file = new File(tOutXmlPath);
          
          response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+new String((fileName).getBytes("GBK"),"ISO8859-1")+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[10000];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
              file.delete();
          }
       }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  
  if (!FlagStr.equals("Fail"))
  {
	  
    Content = "";
    FlagStr = "Succ";
  }
%>
<html>
<script language="javascript"> 
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>