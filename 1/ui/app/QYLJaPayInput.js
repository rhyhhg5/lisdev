var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;

window.onfocus = myonfocus;

// 查询按钮
function easyQueryClick()
{
	initPolGrid();
	if(!verifyInput2())
		return false;
	if(fm.PrtNo.value == "" && fm.ContNo.value == ""){
		if(fm.startSignDate.value == "" && fm.endSignDate.value == "")
		{
			alert("印刷号和合同号为空时，签单日期起和签单日期止必录！");
			return false;
		}
	}
	// 书写SQL语句
    var strSQL = "";
    strSQL = "select a.grpcontno,a.prtno,b.payno,b.SumDuePayMoney,a.signdate,a.cvalidate,b.confdate,b.lastpaytodate,a.ManageCom from lcgrpcont a,ljapaygrp b " 
        + " where a.grpcontno=b.grpcontno "
 	   + " and a.appflag='1' "
 	   + " and a.cvalidate!=b.lastpaytodate "
 	   + " and b.SumDuePayMoney>0 "
 	   + getWherePart('a.GrpContNo','ContNo')
 	   + getWherePart('a.PrtNo','PrtNo')
 	   + getWherePart('a.ManageCom','ManageCom','like')
 	   + getWherePart('a.SignDate','startSignDate','>=')
 	   + getWherePart('a.SignDate','endSignDate','<=')
 	   + " group by a.grpcontno,a.prtno,b.payno,b.SumDuePayMoney,a.signdate,a.cvalidate,b.confdate,b.lastpaytodate,a.ManageCom "
 	   + " having (select count(*) from ljapay where incomeno=a.grpcontno) <= 2 "
 	   ;
    turnPage.pageLineNum = 50;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    fm.querySql.value = strSQL;
}

//进行财务充负
function negativeRecoil()
{
	fm.FinancialAction.value = "doNegative";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./QYLJaPaySave.jsp";
	fm.submit();
}

//进行财务充正
function positiveRecoil()
{
	fm.FinancialAction.value = "doPositive";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./QYLJaPaySave.jsp";
	fm.submit();
}

function afterSubmit( FlagStr, content )
{
	showInfo.close();
	
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		showContInfo();
	}
}

