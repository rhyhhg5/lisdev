<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：DelIsuredRisk.jsp
//程序功能：
//创建日期：2002-02-05 08:49:52
//创建人  ：yuanaq
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%

  TransferData tTransferData = new TransferData(); 
  ContInsuredUI tContInsuredUI   = new ContInsuredUI();
  //输出参数
  String FlagStr = "";
  String Content = "";
   
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  System.out.println("tGI"+tGI);
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
  CErrors tError = null;
  String tBmCert = "";
  String fmAction=request.getParameter("fmAction");
		
        tTransferData.setNameAndValue("InsuredNo",request.getParameter("InsuredNo")); //保全保存标记，默认为0，标识非保全  
        tTransferData.setNameAndValue("PolNo",request.getParameter("polno"));             
        try
         {
            // 准备传输数据 VData
             VData tVData = new VData();
             tVData.add(tTransferData);
             tVData.add(tGI);
                 System.out.println("asdasdasdasd"+fmAction);  
                 System.out.println("InsuredNo=="+request.getParameter("InsuredNo"));
                 System.out.println("polno=="+request.getParameter("polno")); 
            if ( tContInsuredUI.submitData(tVData,fmAction))
            {

                            %>
		            <%		      
		        if (fmAction.equals("DELETE||INSUREDRISK"))
		        {
		    	    
		            %>
		            <SCRIPT language="javascript">
                            parent.fraInterface.PolGrid.delRadioTrueLine("PolGrid");                    
		            </SCRIPT>
		            <%
		        }
	        }
	        
	        String prtno = request.getParameter("PrtNo");
	        
	       	LCPolSchema tLCPolSchema = new LCPolSchema();
	       	tLCPolSchema.setPrtNo(prtno);
	       
	       	VData exemptionVD = new VData();
  			exemptionVD.addElement(tGI);
  			exemptionVD.addElement(tLCPolSchema);
  			ExemptionRiskBL tExemptionRiskBL = new ExemptionRiskBL();
  			// 豁免险处理
  			if(!tExemptionRiskBL.submitData(exemptionVD, "")){
  				Content = "险种删除成功，但重算豁免险处理失败！";
  	 		    FlagStr = "Succ";
  			}
    }
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
  

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tContInsuredUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="险种删除成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "删除失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println("FlagStr:"+FlagStr+"Content:"+Content);
  
}//页面有效区
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.fm.action="./ContInsuredSave.jsp";
	parent.fraInterface.getInsuredPolInfo();
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

