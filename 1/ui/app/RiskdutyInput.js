//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询事件
function easyQueryClick()
{
  
    var strSQL = "select a.RiskCode, a.RiskName, c.dutycode,c.dutyname"
                  + " from LMRiskApp a,lmriskduty b,lmduty c "
                  + " where 1 = 1 " 
                  + " and a.riskcode =b.riskcode "
                  + " and  b.dutycode = c.dutycode "
                  + getWherePart('a.RiskCode', 'riskCode')
                  + " order by c.dutycode";
                 
               
            turnPage.queryModal(strSQL, RiskdutyGrid);

              fm.querySql.value = strSQL;
}

//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && RiskdutyGrid.mulLineCount > 0)
    {
        fm.action = "RiskdutyDownload.jsp";
        fm.target = "_blank";
        fm.submit();
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
