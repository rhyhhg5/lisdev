<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：ContPlanShareAmntInput.jsp
 //程序功能：共用保额要素维护
 //创建日期：2010-04-06
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="ContPlanShareAmnt.js"></SCRIPT>
  <%@include file="ContPlanShareAmntInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./ContPlanShareAmntSave.jsp" method=post name=fm target="fraSubmit">

<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			共用保额维护
    		</td>
    	</tr>
</table>
<Div  id= "divContGrid" align=left style= "display: ''">
<table  class="common" >
  <tr class="common">
    <td class="title">管理机构</td><td class="input"><Input class= "codeno"  name="ManageCom"  ondblclick="return showCodeList('comcode',[this,organname],[0,1],null,null,null,1);" onkeyup="return organname('comcode',[this,organname],[0,1],null,null,null,1);" readonly ><Input class=codename  name=organname></TD> 
    <td class="title">印刷号</td><td class="input"><input class="common" name="PrtNo" ></td>  
    <td class="title">保单号</td><td class="input"><input class="common" name="GrpContNo" ></td>  
  </tr>
</table>
<input value="查询"  onclick="searchGrpCont( )" class="cssButton" type="button" >
<br>
<table>
  <tr>
    <td class="titleImg" >保单信息</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanMul12Grid" >
     </span> 
      </td>
   </tr>
</table>
<table>
  <tr>
    <td class="titleImg" >保障计划信息</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanMul11Grid" >
     </span> 
      </td>
   </tr>
</table>
<table>
  <tr>
    <td class="titleImg" >保障计划详细信息</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanMul10Grid" >
     </span> 
      </td>
   </tr>
</table>
<input value="保存"  onclick="saveDutyParam( )" class="cssButton" type="button" >
<br>

<br><font size='4'>操作说明：</font></br>
<br><font size='3'>&nbsp;&nbsp;&nbsp;&nbsp;输入印刷号或团体保单号，点击查询，保单信息显示所查询的保单。选择该保单，自动带出该保单下的所有保障计划。选择需要修改的保障计划，自动带出该保障计划下各个责任的共用保额录入情况，可以修改要素值和特别说明。</font></br>
<br><font size='3'>&nbsp;&nbsp;&nbsp;&nbsp;如果未查询到保障计划详细信息，需要产品配置该险种下责任的共用保额要素。</font></br>
<br><font size='3' color= 'red' >&nbsp;&nbsp;&nbsp;&nbsp;要素值必须为数值或者空，且必须一致。如果不使用共用保额，不录入。如果共用保额不控制（任意赔付），录入0。</font></br>

</Div>
<input type=hidden name = "ContPlanCode">
<input type=hidden name = "mGrpContNo">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
