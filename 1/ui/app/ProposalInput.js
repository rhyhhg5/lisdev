//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var spanObj;
var mDebug = "100";
var mOperate = 0;
var mAction = "";
var arrResult = new Array();
var mShowCustomerDetail = "PROPOSAL";
var mCurOperateFlag = ""	// "1--录入，"2"--查询
                      var mGrpFlag = ""; 	//个人集体标志,"0"表示个人,"1"表示集体.
var cflag = "0";        //文件件录入位置
var sign=0;
var risksql;
var arrGrpRisk = null;
var mainRiskPolNo="";
var cMainRiskCode="";
var mSwitch = parent.VD.gVSwitch;
window.onfocus = myonfocus;
var hiddenBankInfo = "";

/*********************************************************************
 *  选择险种后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) {
  try	{
    //不能新增卡单险种
    if(LoadFlag == 18)
		{
		  try{
		  var sql = "  select 1 "
		            + "from LDCode1 "
		            + "where codetype = 'SimpleRisk' "
		            + "   and code = '" + Field.value + "' "
		  var rs = easyExecSql(sql);
		  if(rs)
		  {
		    alert("不能增加简易保单险种");
		    return false;
		  }
		  }
		  catch(ex){alert(ex);}
		}
    
    //险种选择

   	//try {
   	//  if( type =="noScan" && cCodeName == 'RiskInd')//个单无扫描件录入
   	//    {
   	//    var strSql = "select distinct 1 from ldsysvar where VERIFYOPERATEPOPEDOM('"+Field.value+"','"+ManageCom+"','"+ManageCom+"',"+"'Pa')=1 ";
   	//    ;
   	//    //alert(strSql);
   	//    var arrResult = easyExecSql(strSql);
   	//    //alert(arrResult);
   	//    if (arrResult == null) {
   	//      alert("机构 ["+ManageCom+"] 无权录入险种 ["+Field.value+"] 的投保单!");
   	//      gotoInputPage();
   	//      return ;
   	//      }
   	//    }
   	//
   	//  } catch(ex) {}

    //try {
    //  //alert(cCodeName);
    //  if (typeof(prtNo)!="undefined" && typeof(type)=="undefined" && cCodeName == 'RiskInd')//个单有扫描件录入
    //    {
    //    //alert(cCodeName);
    //    var strSql2 = "select distinct 1 from ldsysvar where VERIFYOPERATEPOPEDOM('"+Field.value+"','"+ManageCom+"','"+ManageCom+"',"+"'Pz')=1 ";
    //    ;
    //    //alert(strSql2);
    //    var arrResult2 = easyExecSql(strSql2);
    //    //alert(arrResult2);
    //    if (arrResult2 == null) {
    //      alert("机构 ["+ManageCom+"] 无权录入险种 ["+Field.value+"] 的投保单!");
    //      gotoInputPage();
    //      return ;
    //      }
    //    }
    //  } catch(ex) {}

    if( cCodeName == "RiskInd" || cCodeName == "RiskGrp" || cCodeName == "RiskCode") {
      var tProposalGrpContNo = parent.VD.gVSwitch.getVar( "GrpContNo" );
      var tContPlanCode = parent.VD.gVSwitch.getVar( "ContPlanCode" );
      //alert("mainRiskPolNo:"+mainRiskPolNo);
      if(mainRiskPolNo==""&&parent.VD.gVSwitch.getVar("mainRiskPolNo")==true) {
        mainRiskPolNo=parent.VD.gVSwitch.getVar("mainRiskPolNo");
        }
      if(cCodeName=="RiskCode") {
        if(!isSubRisk(Field.value)) {
          cMainRiskCode=Field.value;
          } else if(isSubRisk(Field.value)&&mainRiskPolNo!="") {
          var mainRiskSql="select riskcode from lcpol where polno='"+mainRiskPolNo+"'";
          var arr = easyExecSql(mainRiskSql);
          cMainRiskCode=arr[0];
          }
        //alert("mainriskcode:"+cMainRiskCode);
        //alert("mainriskpolno:"+mainRiskPolNo);
        }
      //alert("mainRiskPolNo2:"+mainRiskPolNo);

      if(LoadFlag!=2&&LoadFlag!=23&&LoadFlag!=9&&LoadFlag!=4) {
    	  
    	//LoadFlag在页面出始化的时候声明
        if(!getRiskInput(Field.value, LoadFlag)){
        	BnfGrid.clearData("BnfGrid");
			showDiv(divLCBnf1, "");
        	return false;
        }
        
        } else {
        if (tContPlanCode=='false'||tContPlanCode==null||tContPlanCode=='') {
          //如果没有保险计划编码,查询有没有默认值，如果有默认值也需要调用后台查询

          var strsql = "select ContPlanCode from LCContPlan where ContPlanCode='00' and ProposalGrpContNo='"+tProposalGrpContNo+"'";
          //alert(strsql);
          var defultContPlanCode=easyExecSql(strsql);
          if (defultContPlanCode=='00') {
            queryRiskPlan( tProposalGrpContNo,Field.value,defultContPlanCode,cMainRiskCode );
            } else {
            getRiskInput(Field.value, LoadFlag);//LoadFlag在页面出始化的时候声明
            }

          } else {
          queryRiskPlan( tProposalGrpContNo,Field.value,tContPlanCode,cMainRiskCode );
          }
        }

        if(LoadFlag == 18)
				{
				  try
				  {
        //若是保全调用则不显示受益人录入列表
				    var sql = "  select payIntv "
				              + "from LCCont "
				              + "where contNo = '" + fm.ContNo.value + "' "
				              + "   and appFlag = '1' ";
  					var rs = easyExecSql(sql);
  					if(rs && rs[0][0] != "" && rs[0][0] != "null")
  					{
  					  fm.PayIntv.value = rs[0][0];
  					}
  					
  					BnfGrid.clearData("BnfGrid");
  					showDiv(divLCBnf1, "");
				  }
				  catch(ex){alert(ex.message);}
				}

      //将扫描件图片翻到第一页
      try {
        goToPic(0);
        }	catch(ex2) {}
      }

    //自动填写受益人信息
    if (cCodeName == "customertype") {
      if (Field.value == "A") {
        if(ContType!="2") {
          var index = BnfGrid.mulLineCount;
          BnfGrid.setRowColData(index-1, 2, fm.all("AppntName").value);
          BnfGrid.setRowColData(index-1, 3, fm.all("AppntIDType").value);
          BnfGrid.setRowColData(index-1, 4, fm.all("AppntIDNo").value);
          BnfGrid.setRowColData(index-1, 5, fm.all("AppntRelationToInsured").value);
          //BnfGrid.setRowColData(index-1, 8, fm.all("AppntHomeAddress").value);
          BnfGrid.setRowColData(index-1, 8, "A");
          } else {
          alert("投保人为团体，不能作为受益人！")
          var index = BnfGrid.mulLineCount;
          BnfGrid.setRowColData(index-1, 2, "");
          BnfGrid.setRowColData(index-1, 3, "");
          BnfGrid.setRowColData(index-1, 4, "");
          BnfGrid.setRowColData(index-1, 5, "");
          BnfGrid.setRowColData(index-1, 8, "");
          }
        } else if (Field.value == "I") {
        var index = BnfGrid.mulLineCount;
        if(BnfGrid.getRowColData(index-1,1)=="1")
         {
              alert("身故受益人不能选择被保险人本人");
              BnfGrid.setRowColData(index-1, 2, "");
              BnfGrid.setRowColData(index-1, 3, "");
              BnfGrid.setRowColData(index-1, 4, "");
              BnfGrid.setRowColData(index-1, 5, "");
              BnfGrid.setRowColData(index-1, 6, "");
              BnfGrid.setRowColData(index-1, 7, "");
              BnfGrid.setRowColData(index-1, 8, "");
          }
         else
          {
 
              BnfGrid.setRowColData(index-1, 2, fm.all("Name").value);
              BnfGrid.setRowColData(index-1, 3, fm.all("IDType").value);
              BnfGrid.setRowColData(index-1, 4, fm.all("IDNo").value);
              BnfGrid.setRowColData(index-1, 5, "00");
              //BnfGrid.setRowColData(index-1, 8, fm.all("AppntHomeAddress").value);
              BnfGrid.setRowColData(index-1, 8, "I");
          }
        }
      }

    //收费方式选择
    if (cCodeName == "PayLocation") {
      if (Field.value != "0") {
        if (hiddenBankInfo=="")
          hiddenBankInfo = DivLCKind.innerHTML;

        fm.all("BankCode").className = "readonly";
        fm.all("BankCode").readOnly = true;
        fm.all("BankCode").tabIndex = -1;
        fm.all("BankCode").ondblclick = "";

        fm.all("AccName").className = "readonly";
        fm.all("AccName").readOnly = true;
        fm.all("AccName").tabIndex = -1;
        fm.all("AccName").ondblclick = "";

        fm.all("BankAccNo").className = "readonly";
        fm.all("BankAccNo").readOnly = true;
        fm.all("BankAccNo").tabIndex = -1;
        fm.all("BankAccNo").ondblclick = "";
        } else {
        if (hiddenBankInfo!="")
          DivLCKind.innerHTML = hiddenBankInfo;

        fm.all("BankCode").value = "";
        fm.all("AccName").value = fm.all("AppntName").value;
        fm.all("BankAccNo").value = "";
        fm.all("PayLocation").value = "0";
        fm.all("PayLocation").focus();
        }
      }
    //责任代码选择
    if(cCodeName == "DutyCode") {
      var index = DutyGrid.mulLineCount;
      var strSql = "select dutyname from lmduty where dutycode='"+Field.value+"'";
      var arrResult = easyExecSql(strSql);
      var dutyname= arrResult[0].toString();
      DutyGrid.setRowColData(index-1, 2, dutyname);
      }
    //alert(cCodeName);
    if(cCodeName == "PolCalRule1") {  //暂时未用
      if(Field.value=="1") {       //统一费率
        divFloatRate.style.display="none";
        divFloatRate2.style.display="";
        } else if(Field.value=="2") {  //约定费率折扣
        fm.all('FloatRate').value="";
        divFloatRate.style.display="";
        divFloatRate2.style.display="none";
        } else {
        divFloatRate.style.display="none";
        divFloatRate2.style.display="none";
        }
      }
    if(cCodeName=="PayEndYear") {
      getOtherInfo(Field.value);
      }
    if(cCodeName=="GetYear") {
      getGetYearFlag(Field.value);
      }

    if(cCodeName=="PayRuleCode") {
      fm.action="./ProposalQueryPayRule.jsp?AppFlag=0";
      fm.submit();
      //parent.fraSubmit.window.location ="./ProposalQueryPayRule.jsp?GrpContNo="
      //		+ fm.all('GrpContNo').value+"&RiskCode="+fm.all('RiskCode').value
      //		+"&InsuredNo="+fm.all('InsuredNo').value
      //		+"&PayRuleCode="+fm.all('PayRuleCode').value
      //		+"&JoinCompanyDate="+fm.all(JoinCompanyDate).value
      //		+"&AppFlag=0";
      }
      if(cCodeName=="PayIntv"){
      	if(Field.value=="0"){
	      	PayEndType.style.display="none";
	      }else{
	      	PayEndType.style.display="";
	      }
      }
    } catch( ex ) {}

  }
/*********************************************************************
 *  提交前的校验、计算  
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function beforeSubmit() {
  if( verifyInput() == false )
    return false;
  }

/*********************************************************************
 *  根据LoadFlag设置一些Flag参数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function convertFlag( cFlag ) {
  //alert("cFlag:" + cFlag);
  if( cFlag == "1"|| cFlag == "8")		// 个人投保单直接录入
    {
    mCurOperateFlag = "1";
    mGrpFlag = "0";
    }
  if( cFlag == "2" || cFlag == "7" || cFlag == "9" || cFlag == "13"||cFlag == "14"||cFlag == "16"||cFlag == "99"||cFlag=="23")		// 集体下个人投保单录入
    {
    mCurOperateFlag = "1";
    mGrpFlag = "1";
    }
  if(cFlag == "18") //保全增加险种－个险
  {
  	mCurOperateFlag = "0";
		mGrpFlag = "0";
  }
  if( cFlag == "3"||cFlag == "6" )		// 个人投保单明细查询
    {
    mCurOperateFlag = "2";
    mGrpFlag = "0";
    }
  if( cFlag == "4" )		// 集体下个人投保单明细查询
    {
    mCurOperateFlag = "2";
    mGrpFlag = "1";
    }
  if( cFlag == "5"||cFlag=="25" )		// 个人投保单复核查询
    {
    mCurOperateFlag = "2";
    mGrpFlag = "3";
    }
  if(cFlag=="99"&&checktype=="1") {
    mGrpFlag = "0";
    }
  if(cFlag=="99"&&checktype=="2") {
    mGrpFlag = "1";
    }
  }

/**
 * 获取该集体下所有险种信息
 */
function queryGrpPol(prtNo, riskCode) {
  var findFlag = false;

  //根据印刷号查找出所有的相关险种
  if (arrGrpRisk == null) {
    var strSql = "select GrpProposalNo, RiskCode from LCGrpPol where PrtNo = '" + prtNo + "'";
    arrGrpRisk  = easyExecSql(strSql);

    //通过承保描述定义表找到主险
    for (i=0; i<arrGrpRisk.length; i++) {
      strSql = "select SubRiskFlag from LMRiskApp where RiskCode = '"
               + arrGrpRisk[i][1] + "' and RiskVer = '2002'";
      var riskDescribe = easyExecSql(strSql);

      if (riskDescribe == "M") {
        top.mainRisk = arrGrpRisk[i][1];
        break;
        }
      }
    }
  //alert(arrGrpRisk);

  //获取选择的险种和集体投保单号码
  for (i=0; i<arrGrpRisk.length; i++) {
    if (arrGrpRisk[i][1] == riskCode) {
      fm.all("RiskCode").value = arrGrpRisk[i][1];
      fm.all("GrpPolNo").value = arrGrpRisk[i][0];

      if (arrGrpRisk[i][1] == top.mainRisk) {
        //top.mainPolNo = "";
        mainRiskPolNo ="";
        }

      findFlag = true;
      break;
      }
    }

  if (arrGrpRisk.length > 1) {
    fm.all("RiskCode").className = "code";
    fm.all("RiskCode").readOnly = false;
    } else {
    fm.all("RiskCode").onclick = "";
    }

  return findFlag;
  }

/**
 * 根据身份证号码获取出生日期
 */
function grpGetBirthdayByIdno() {
  var id = fm.all("IDNo").value;

  if (fm.all("IDType").value == "0") {
    if (id.length == 15) {
      id = id.substring(6, 12);
      id = "19" + id;
      id = id.substring(0, 4) + "-" + id.substring(4, 6) + "-" + id.substring(6);
      fm.all("Birthday").value = id;
      } else if (id.length == 18) {
      id = id.substring(6, 14);
      id = id.substring(0, 4) + "-" + id.substring(4, 6) + "-" + id.substring(6);
      fm.all("Birthday").value = id;
      }
    }
  }

/**
 * 校验主附险包含关系
 */
function checkRiskRelation(tPolNo, cRiskCode) {
  // 集体下个人投保单
  if (mGrpFlag == "1") {
    var strSql = "select RiskCode from LCGrpPol where GrpProposalNo = '" + tPolNo
                 + "' and RiskCode in (select Code1 from LDCode1 where Code = '" + cRiskCode + "')";
    return true;
    } else {
    var strSql = "select RiskCode from LCPol where PolNo = '" + tPolNo
                 + "' and RiskCode in (select Code1 from LDCode1 where Code = '" + cRiskCode + "')";
    }

  return easyQueryVer3(strSql);
  }

/**
 * 出错返回到险种选择界面
 */
function gotoInputPage() {
  // 集体下个人投保单
  if (mGrpFlag == "1") {
    //top.fraInterface.window.location = "../app/ProposalGrpInput.jsp?LoadFlag=" + LoadFlag;
    top.fraInterface.window.location = "../app/ProposalInput.jsp?LoadFlag=" + LoadFlag;
    }
  // 个人有扫描件投保单
  else if (typeof(prtNo)!="undefined" && typeof(type)=="undefined") {
    top.fraInterface.window.location = "../app/ProposalInput.jsp?prtNo=" + prtNo;
    }
  // 个人无扫描件投保单
  else {
    top.fraInterface.window.location = "../app/ProposalOnlyInput.jsp?type=noScan";
    }
  }

function showSaleChnl() {
  showCodeList('SaleChnl',[this]);
  }

/*********************************************************************
 *  根据不同的险种,读取不同的代码
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getRiskInput(cRiskCode, cFlag) {
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "";
  var tPolNo = "";
  convertFlag(cFlag);
  if(LoadFlag==18)
  {
	  //#3201-保全新增险种增加校验
	  var tBQContno = parent.VD.gVSwitch.getVar( "ContNo" );
	  if(cRiskCode == "340501" || cRiskCode == "340602"){
			var BQSql2 = "select 1 from LCPol where  ContNo ='"+tBQContno+"' and riskcode in('340301','340401','335301','335302','730503','730301','730302','730604') and appflag='1' and stateflag='1'";
		  	BQResult2 = easyExecSql(BQSql2);
		  	if(BQResult2 == null){
			  	alert("当前险种不允许进行保全新增健康一生个人护理保险（万能型,A款）");
			  	return false;
		  	}else if(BQResult2!=null && cRiskCode != "340501" && cRiskCode != "340602"){
			  alert("当前险种不能新增健康一生个人护理保险（万能型，A款）之外的险种");
	  	  	return false;
		 	}else if(BQResult2 == null && (cRiskCode == "340501" || cRiskCode == "340602")){
			  	alert("健康一生个人护理保险（万能型，A款）不能添加到该保单下");

			  	return false;
		  	}
		  
	  }else if (cRiskCode == "334801") {
//    	BQSql = "select 1 from LCPol where  ContNo ='"+tBQContno+"' and riskcode in('340101','340301') and appflag='1' and stateflag='1'";
		var BQSql2 = "select 1 from LCPol where  ContNo ='"+tBQContno+"' and riskcode in('340101','340301','730201','340401') and appflag='1' and stateflag='1'";
//    	BQResult = easyExecSql(BQSql);
	  	BQResult2 = easyExecSql(BQSql2);
//    	if(BQResult!=null && cRiskCode != "332301" && cRiskCode != "334801") {
	  	if(BQResult2 == null){
		  	alert("当前险种不允许进行保全新增险种");

		  	return false;
	  	}else if(BQResult2!=null && cRiskCode != "334801"){
		  alert("金生无忧或者美满今生或福满人生或美满一生险种不能添加附加个人护理保险B款之外的险种");
  	  	return false;
//  	  	}
//  	  	else{
//  		  	alert("金生无忧或者美满今生险种不能添加附加个人护理保险之外的险种");
//            	return false;
//  	  	}
	  	}else if(BQResult2!=null && cRiskCode != "334801") {
		  	alert("金生无忧或者美满今生或福满人生或美满一生险种不能添加附加个人护理保险之外的险种");

		  	return false;
//    	}
//    	else if (BQResult == null && cRiskCode == "332301") {
//  	  	alert("附加个人护理保险（万能型）不能添加到非金生无忧或美满今生险种的保单下");
//        	return false;
	 	}else if(BQResult2 == null && cRiskCode == "334801"){
		  	alert("附加个人护理保险（万能型B款）不能添加到非金生无忧或美满今生或福满人生或美满一生险种的保单下");

		  	return false;
	  	}
	  }else if (cRiskCode != "334801" && cRiskCode != "340501" && cRiskCode != "340602"){
		  	alert("该险种不能进行新增险种！");

		  	return false;
	  }
	  
      var tBQSql = "select 1 from LCPol where  ContNo ='"+tBQContno+"' and riskcode in('332301','334801','340501','340602') ";
      tBQResult = easyExecSql(tBQSql);
      if(tBQResult!=null ) {
    	  alert("该保单下已经存在万能附加险种，不能再次添加");
          return false;
        }
  }
  // 首次进入集体下个人录入
  if( mGrpFlag == "0" )		// 个人投保单
    urlStr = "../riskinput/Risk" + cRiskCode + ".jsp";
  if( mGrpFlag == "1" )		// 集体下个人投保单
    urlStr = "../riskgrp/Risk" + cRiskCode + ".jsp";
  if( mGrpFlag == "3" )		// 个人投保单复核
    urlStr = "../riskinput/Risk" + cRiskCode + ".jsp";
  //读取险种的界面描述
  showInfo = window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;dialogTop:-800;dialogLeft:-800;resizable=1");

  if(LoadFlag==18)
  {
	  
      var tBQContno = parent.VD.gVSwitch.getVar( "ContNo" );
      
      var BQDeSql = "select InsuYear,InsuYearFlag,PayEndYear,PayEndYearFlag,PayIntv,(select codename from ldcode where codetype='payintv' and code=a.payintv) " +
      		" from lcpol a where ContNo ='"+tBQContno+"' and riskcode in('340101','340301','730201','340401') and appflag='1' and stateflag='1' ";
      BQDeResult = easyExecSql(BQDeSql);
      if(BQDeResult!=null) {
    	  fm.InsuYear.value = BQDeResult[0][0];
    	  fm.InsuYearFlag.value = BQDeResult[0][1];
    	  fm.PayEndYear.value = BQDeResult[0][2];
    	  fm.PayEndYearFlag.value = BQDeResult[0][3];
    	  fm.Amnt.value = 0;
    	  fm.Prem.value = 0;
    	  fm.PayIntv.value = BQDeResult[0][4];
    	  fm.PayIntvName.value = BQDeResult[0][5];
    	  fm.all("InsuYear").readOnly = true;
    	  fm.all("InsuYearFlag").readOnly = true;
    	  fm.all("PayEndYear").readOnly = true;
    	  fm.all("PayEndYearFlag").readOnly = true;
    	  fm.all("Amnt").readOnly = true;
    	  fm.all("Prem").readOnly = true;
    	  fm.all("PayIntv").readOnly = true;
    	  fm.all("PayIntv").className = "readOnly";
    	  fm.all("PayIntv").ondblclick = "";
    	  fm.all("PayIntv").onkeyup = "";
    	  fm.all("PayIntv").tabIndex = -1;
    	  
    	  fm.all("PayIntvName").readOnly = true;
    	  fm.all("PayIntvName").className = "readOnly";
    	  fm.all("PayIntvName").ondblclick = "";
    	  fm.all("PayIntvName").onkeyup = "";
    	  fm.all("PayIntvName").tabIndex = -1;
    	  
    	  
        }

      if(cRiskCode=="340501" || cRiskCode=="340602") {
          var BQDeSql = "select InsuYear,InsuYearFlag,'0',(select codename from ldcode where codetype='payintv' and code='0') " +
  			"  from lmduty where dutycode in (select dutycode from lmriskduty where riskcode = '"+cRiskCode+"') ";
          BQDeResult = easyExecSql(BQDeSql);
    	  fm.InsuYear.value = BQDeResult[0][0];
    	  fm.InsuYearFlag.value = BQDeResult[0][1];
    	  fm.PayEndYear.value = BQDeResult[0][0];
    	  fm.PayEndYearFlag.value = BQDeResult[0][1];
    	  fm.Amnt.value = 0;
    	  fm.Prem.value = 0;
    	  fm.PayIntv.value = BQDeResult[0][2];
    	  fm.PayIntvName.value = BQDeResult[0][3];
    	  fm.all("InsuYear").readOnly = true;
    	  fm.all("InsuYearFlag").readOnly = true;
    	  fm.all("PayEndYear").readOnly = true;
	  	fm.all("PayEndYearFlag").readOnly = true;
	  	fm.all("Amnt").readOnly = true;
	  	fm.all("Prem").readOnly = true;
	  	fm.all("PayIntv").readOnly = true;
	  	fm.all("PayIntv").className = "readOnly";
	  	fm.all("PayIntv").ondblclick = "";
	  	fm.all("PayIntv").onkeyup = "";
	  	fm.all("PayIntv").tabIndex = -1;
	  	
	  	fm.all("SupplementaryPrem").className = "readOnly";
	  
	  	fm.all("PayIntvName").readOnly = true;
	  	fm.all("PayIntvName").className = "readOnly";
	  	fm.all("PayIntvName").ondblclick = "";
	  	fm.all("PayIntvName").onkeyup = "";
	  	fm.all("PayIntvName").tabIndex = -1;
	  
	  
  }
  }
  
  
  if(LoadFlag==4|ApproveFlag==5|LoadFlag ==25) {
    //showDiv(inputQuest, "true");
    
    var ttpol=parent.VD.gVSwitch.getVar("PolNo");
    if(ApproveFlag==5&&ttpol==""){
  	showDiv(inputButton, "true");
  	showDiv(inputQuest, "true");
  	}else if(ApproveFlag==5&&ttpol!=""){
  	showDiv(modifyButton, "true");
  	}

	    if(ApproveFlag!=5){
	    	showDiv(modifyButton,"true");
	    }
    } else {
    showDiv(inputButton, "true");
    showDiv(inputQuest, "true");
    }
  if(cFlag == "1"|| cFlag =="6") {
    strSql = "select CValiDate,PayMode,PolApplyDate,payintv,codename('payintv',PayIntv) from LCcont where  ContNo ='"+parent.VD.gVSwitch.getVar( "ContNo" )+"'  ";
    arrResult = easyExecSql(strSql);
    if(arrResult!=null) {
      fm.CValiDate.value=arrResult[0][0];
      fm.PayMode.value=arrResult[0][1];
      fm.PolApplyDate.value = arrResult[0][2];
      }
    strSql = "select payintv,codename('payintv',PayIntv) from lcpol where contno='"+parent.VD.gVSwitch.getVar( "ContNo" )+"' and riskcode = '"+cRiskCode+"'";
    var arr2 = easyExecSql(strSql);
    if(arr2!=null) {
    	if(arr2[0][0] != null && arr2[0][0] !=""){
    		 try{fm.PayIntv.value=arr2[0][0];}catch(ex){}
    	     try{fm.PayIntvName.value = arr2[0][1];}catch(ex){}
    	}
    }else{
		 try{fm.PayIntv.value=arrResult[0][3];}catch(ex){}
		 try{fm.PayIntvName.value = arrResult[0][4];}catch(ex){}
    }
    initElementtype();
    var rs = easyExecSql("select risktype4 from lmriskapp where riskcode='"+cRiskCode+"'");
    if(rs != null && rs != "" && rs =="2"){
    	var rs2 = easyExecSql("select BonusGetMode,codename('bonusgetmode',BonusGetMode) from lcpol where riskcode='"+cRiskCode+"' and prtno = '"+parent.VD.gVSwitch.getVar( "PrtNo" )+"' ");
    	if(rs2 != null && rs2 != ""){
    		fm.BonusGetMode.value=rs2[0][0];
    		fm.BonusGetModeName.value=rs2[0][1];
    	}
    }
  }
  if(LoadFlag==9) {
    //在这里应该添加录入完毕按钮的隐藏,但是方法暂时不会;
    }
    
    //保全新增附加险－个单
   if(LoadFlag==18)
   {
      fm.CValiDate.value = parent.VD.gVSwitch.getVar( "CValiDate" );
      fm.ContNo.value = parent.VD.gVSwitch.getVar( "ContNo" );
      
      if(cRiskCode=="340501" || cRiskCode=="340602"){
		  fm.all('PayIntv').value = 0;//针对340501缴费频次取趸交
      }else{
    	  var tSQL = "select min(PayIntv) from LCPol where payIntv > 0 and contNo='" + fm.all("ContNo").value + "'";
    	  var arrPayIntv = easyExecSql(tSQL);
    	  try{
    		  fm.all('PayIntv').value = arrPayIntv[0][0];

    		  showDiv(inputButton, "true");  //重置，保存按钮
    		  showDiv(inputQuest, "true"); //上一步按钮
    		  //showDiv(inputConFirm,"");//录入完毕按钮
    	  }
    	  catch(ex)
    	  {
    		  alert(ex.message);
    	  }
      }
   }
   
  if (cFlag == "99") {
    showDiv(inputButton, "false");
    showDiv(inputQuest, "false");
    showDiv(autoMoveButton, "true");
    }

  if (cFlag == "3") {
    fm.all("SaleChnl").readOnly = false;
    fm.all("SaleChnl").className = "code";
    fm.all("SaleChnl").ondblclick= showSaleChnl;
    }
  if (cFlag == "2") {
    var aGrpContNo=parent.VD.gVSwitch.getVar( "GrpContNo" );
    if(isSubRisk(cRiskCode))
      fm.all('MainPolNo').value=mainRiskPolNo;
    //alert("GrpContNo:"+ aGrpContNo);
    strSql = "select PayIntv from LCGrpPol where RiskCode = '" + cRiskCode + "' and GrpContNo ='"+aGrpContNo+"'";
    var PayIntv = easyExecSql(strSql);
    //alert("PayIntv:"+PayIntv);
    try {
      fm.PayIntv.value=PayIntv;
      } catch(ex) {}
    strSql = "select CValiDate from LCGrpCont where  GrpContNo ='"+aGrpContNo+"'";
    var CValiDate = easyExecSql(strSql);
    try {
      fm.CValiDate.value=CValiDate;
      } catch(ex) {}
    }

  try {
    //出始化公共信息
    emptyForm();

    //根据需求单放开销售渠道只读的限制，by Minim at 2003-11-24
    fm.all("SaleChnl").readOnly = false;
    fm.all("SaleChnl").className = "code";
    fm.all("SaleChnl").ondblclick= showSaleChnl;

    //无扫描件录入
    if (typeof(type)!="undefined" && type=="noScan") {
      //      fm.all("PrtNo").readOnly = false;
      //      fm.all("PrtNo").className = "common";

      //通过承保描述定义表找到主险
      strSql = "select SubRiskFlag from LMRiskApp where RiskCode = '"
               + cRiskCode + "' and RiskVer = '2002'";
      var riskDescribe = easyExecSql(strSql);
      //      if (riskDescribe == "M") {
      //        top.mainPolNo = "";
      //      }

      }
    if(scantype=="scan") {
      //fm.all("PrtNo").value = prtNo;
      fm.all("RiskCode").value=cRiskCode;
      setFocus();
      }
    getContInput();
    getContInputnew();
    } catch(e) {}

  //传入险种和印刷号信息
  fm.all("RiskCode").value = cRiskCode;

  //将焦点移动到印刷号，以方便随动录入
  //fm.all("PrtNo").focus();

  //初始化界面的销售渠道
  try {
    prtNo = fm.all("PrtNo").value;
    var riskType = prtNo.substring(2, 4);
    if (riskType == "11") {
      fm.all("SaleChnl").value = "02";
      } else if (riskType == "12") {
      fm.all("SaleChnl").value = "01";
      } else if (riskType == "15") {
      fm.all("SaleChnl").value = "03";
      }
    else if (riskType == "16") {
      fm.all("SaleChnl").value = "02";

      fm.all("SaleChnl").readOnly = false;
      fm.all("SaleChnl").className = "code";
      fm.all("SaleChnl").ondblclick= showSaleChnl;
      }
    } catch(e) {}

  if (!(typeof(top.type)!="undefined" && (top.type=="ChangePlan" || top.type=="SubChangePlan"))) {
    //将是否指定生效日期在录入时失效
    fm.all("SpecifyValiDate").readOnly = true;
    fm.all("SpecifyValiDate").className = "readOnly";
    fm.all("SpecifyValiDate").ondblclick = "";
    fm.all("SpecifyValiDate").onkeyup = "";
    //fm.all("SpecifyValiDate").value = "N";
    fm.all("SpecifyValiDate").tabIndex = -1;
    }
  //alert("mCurOperateFlag:"+mCurOperateFlag);
  if(mainRiskPolNo=="") {
    mainRiskPolNo=parent.VD.gVSwitch.getVar("mainRiskPolNo");
    }
  //alert("main 5:"+mainRiskPolNo);

  if( mCurOperateFlag == "1" ) {             // 录入
    // 集体下个人投保单
    //alert("mGrpFlag:"+mGrpFlag);
    if( mGrpFlag == "1" )	{

      getGrpPolInfo();                       // 带入集体部分信息

      getPayRuleInfo();

      //支持集体下个人，录入身份证号带出出生日期
      fm.all("IDNo").onblur = grpGetBirthdayByIdno;
      //暂时不去掉去掉问题件按钮,随动定制时例外
      if(LoadFlag!="99")
        inputQuest.style.display = "";

      // 获取该集体下所有险种信息
      //alert("judging if the RiskCode input has been input in group info.");
      //if (!queryGrpPol(fm.all("PrtNo").value, cRiskCode))	{
      //  alert("集体团单没有录入这个险种，集体下个人不允许录入！");
      //  fm.all("RiskCode").value="";
      //  //alert("now go to the new location- ProposalGrpInput.jsp");
      //  top.fraInterface.window.location = "./ProposalGrpInput.jsp?LoadFlag=" + LoadFlag;
      //  //alert("top.location has been altered");
      //  return false; //hezy
      //}
      }
    //if(initDealForSpecRisk(cRiskCode)==false)//特殊险种在初始化时的特殊处理-houzm添加
    //{
    //alert("险种："+cRiskCode+"初始化时特殊处理失败！");
    //return false;
    //}



    //特殊险种在初始化时的特殊处理扩展-guoxiang add at 2004-9-6 16:33
    if(initDealForSpecRiskEx(cRiskCode)==false) {
      alert("险种："+cRiskCode+"初始化时特殊处理失败！");
      return false;
      }

    //alert("getRiskInput.isSubRisk begin...");
    //alert("cRiskCode:"+cRiskCode);
    try {
      fm.all('SelPolNo').value=parent.VD.gVSwitch.getVar("PolNo");
      //alert("kjlkdsjal");
      if (fm.all('SelPolNo').value=='false') {
        fm.all('SelPolNo').value='';
        }
      if(fm.all('SelPolNo').value!='') { //更换险种
        var tSql="select riskcode from lcpol where polno='"+fm.all('SelPolNo').value+"'";
        var arr=easyExecSql(tSql);
        if(arr[0]!=cRiskCode) {
          fm.all('SelPolNo').value='';
          }
        }
      } catch(ex) {}
    //alert("selPolNo:"+fm.all('SelPolNo').value);
    if( isSubRisk( cRiskCode ) == true&&fm.all('SelPolNo').value=="" ) {   // 附险
      //如果是新增附加险，则不清空缓存的主险保单号
      if (cFlag != "8" && cFlag != "2") {
        //top.mainPolNo = ""; //hezy add
        mainRiskPolNo = "";
        }
      tPolNo = getMainRiskNo(cRiskCode);   //弹出录入附险的窗口,得到主险保单号码
      //alert("tPolNo:"+tPolNo);
      if (!checkRiskRelation(tPolNo, cRiskCode)) {
        alert("主附险包含关系错误，输入的主险号不能带这个附加险！");
        gotoInputPage();
        //top.mainPolNo = "";
        mainRiskPolNo = "";
        return false;
        }
      //-----------------------------------------------------------------------
      if(cRiskCode=='121301'||cRiskCode=='321601')//出始化特殊的附险信息--houzm添加--可单独提出为一个函数
        {
        if(cRiskCode=='121301') {
          if (!initPrivateRiskInfo121301(tPolNo)) {
            gotoInputPage();
            return false;
            }
          }
        if(cRiskCode=='321601') {
          if (!initPrivateRiskInfo321601(tPolNo)) {
            gotoInputPage();
            return false;
            }
          }
        }
      else {
        //出始化附险信息
        if (!initPrivateRiskInfo(tPolNo)) {
          gotoInputPage();
          return false;
          }
        }

      try	{}
      catch(ex1) {
        alert( "初始化险种出错" + ex1 );
        }
      } // end of 附险if


    return false;

    } // end of 录入if

  mCurOperateFlag = "";
  mGrpFlag = "";
  }

/*********************************************************************
 *  判断该险种是否是附险,在这里确定既可以做主险,又可以做附险的代码
 *  参数  ：  险种代码
 *  返回值：  无
 *********************************************************************
 */
function isSubRisk(cRiskCode) {
  //alert(cRiskCode);
  if (cRiskCode=="") {
    return false;
    }
  var arrQueryResult = easyExecSql("select SubRiskFlag from LMRiskApp where RiskCode='" + cRiskCode + "'", 1, 0);

  if(arrQueryResult[0] == "S")    //需要转成大写
    return true;
  if(arrQueryResult[0] == "M")
    return false;

  if (arrQueryResult[0].toUpperCase() == "A")
    if (confirm("该险种既可以是主险,又可以是附险!选择确定进入主险录入,选择取消进入附险录入"))
      return false;
    else
      return true;

  return false;
  }

/*********************************************************************
 *  弹出录入附险的窗口,得到主险保单号码
 *  参数  ：  险种代码
 *  返回值：  无
 *********************************************************************
 */
function getMainRiskNo(cRiskCode) {
  var urlStr = "../app/MainRiskNoInput.jsp";
  var tPolNo="";
  //alert("mainRiskPolNo:"+mainRiskPolNo);
  if (typeof(mainRiskPolNo)!="undefined" && mainRiskPolNo!="") {
    //tPolNo = top.mainPolNo;
    tPolNo = mainRiskPolNo;
    } else {
    tPolNo = window.showModalDialog(urlStr,tPolNo,"status:no;help:0;close:0;dialogWidth:310px;dialogHeight:100px;center:yes;");
    //top.mainPolNo = tPolNo;
    mainRiskPolNo = tPolNo;
    }

  return tPolNo;
  }

/*********************************************************************
 *  初始化附险信息
 *  参数  ：  投保单号
 *  返回值：  无
 *********************************************************************
 */
function initPrivateRiskInfo(cPolNo) {
  if(cPolNo=="") {
    alert("没有主险保单号,不能进行附加险录入!");
    mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
    return false
           }

         var arrLCPol = new Array();
  var arrQueryResult = null;
  // 主保单信息部分
  var sql = "select * from lcpol where polno='" + cPolNo + "' "
            + "and riskcode in "
            + "( select riskcode from LMRiskApp where SubRiskFlag = 'M' )";

  arrQueryResult = easyExecSql( sql , 1, 0);
  if (arrQueryResult == null)	{
    mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.

    //top.mainPolNo = "";
    mainRiskPolNo = "";

    alert("读取主险信息失败,不能进行附加险录入!");
    return false
           }

         arrLCPol = arrQueryResult[0];
  displayPol( arrLCPol );

  fm.all("MainPolNo").value = cPolNo;
  var tAR;

  //投保人信息
  if (arrLCPol[6]=="2") {     //集体投保人信息
    arrQueryResult = null;
    //arrQueryResult = easyExecSql("select * from lcappntgrp where polno='"+cPolNo+"'"+" and grpno='"+arrLCPol[28]+"'", 1, 0);
    arrQueryResult = easyExecSql("select * from lcgrpappnt where grpcontno='"+arrLCPol[0]+"'"+" and customerno='"+arrLCPol[28]+"'", 1, 0);
    tAR = arrQueryResult[0];
    displayPolAppntGrp(tAR);
    } else {                     //个人投保人信息
    arrQueryResult = null;
    arrQueryResult = easyExecSql("select * from lcappntind where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[28]+"'", 1, 0);
    tAR = arrQueryResult[0];
    displayPolAppnt(tAR);
    }

  // 被保人信息部分
  if (arrLCPol[21] == arrLCPol[28]) {
    fm.all("SamePersonFlag").checked = true;
    parent.fraInterface.isSamePersonQuery();
    parent.fraInterface.fm.all("CustomerNo").value = arrLCPol[21];
    }
  //else {
  arrQueryResult = null;
  arrQueryResult = easyExecSql("select * from lcinsured where contno='"+arrLCPol[2]+"'"+" and insuredno='"+arrLCPol[21]+"'", 1, 0);
  tAR = arrQueryResult[0];
  displayPolInsured(tAR);
  //}
  return true;
  }

/*********************************************************************
 *  校验投保单的输入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function verifyProposal() {
  var passVerify = true;

  //保全新增附加险
  if (LoadFlag == "8") {
    var newCValidate = fm.all('CValiDate').value;

    if (oldCValidate.substring(4) != newCValidate.substring(4)) {
      alert("指定的生效日期必须是主险生效日期的周年对应日");
      return false;
      }

    //正常的新增附加险不能这么限制
    //if (oldCValidate == newCValidate) {
    //  alert("根据新增附加险规则，不能指定为主险生效日期");
    //  return false;
    //}

    if (!confirm("指定的附加险生效日期为：(" + newCValidate + ")，确定吗？")) {
      return false;
      }
    }

  if(fm.AppntRelationToInsured.value=="00") {
    if(fm.AppntCustomerNo.value!= fm.CustomerNo.value) {
      alert("投保人与被保人关系为本人，但客户号不一致");
      return false;
      }
    }
  if(needVerifyRiskcode()==true) {
    //if(verifyInput() == false) passVerify = false;
		if(checkbnf() == false) passVerify = false;
    BnfGrid.delBlankLine();
    //if(BnfGrid.checkValue("BnfGrid") == false) passVerify = false;
    if(BnfGrid.checkValue2(BnfGrid.name,BnfGrid)== false)
      passVerify = false;
    //校验单证是否发放给业务员
    if (!verifyPrtNo(fm.all("PrtNo").value))
      passVerify = false;
    }
  try {
    /*
     var strChkIdNo = "";
      	
      //以年龄和性别校验身份证号
      if (fm.all("AppntIDType").value == "0") 
        strChkIdNo = chkIdNo(fm.all("AppntIDNo").value, fm.all("AppntBirthday").value, fm.all("AppntSex").value);
      if (fm.all("IDType").value == "0") 
        strChkIdNo = chkIdNo(fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);
        
      if (strChkIdNo != "") {
        alert(strChkIdNo);
        passVerify = false;	  
        }  
        */

    //校验职业和职业代码
    //	  var arrCode = new Array();
    //	  arrCode = verifyCode("职业（工种）", fm.all("AppntWorkType").value, "code:OccupationCode", 1);
    //	  if (arrCode!=true && fm.all("AppntOccupationCode").value!=arrCode[0]) {
    //	    alert("投保人职业和职业代码不匹配！");
    //	    passVerify = false;
    //	  }
    //	  arrCode = verifyCode("职业（工种）", fm.all("WorkType").value, "code:OccupationCode", 1);
    //	  if (arrCode!=true && fm.all("OccupationCode").value!=arrCode[0]) {
    //	    alert("被保人职业和职业代码不匹配！");
    //	    passVerify = false;
    //	  }

    //校验受益比例
    var i;
    var sumLiveBnf = new Array();
    var sumDeadBnf = new Array();
    for (i=0; i<BnfGrid.mulLineCount; i++) {
      if (BnfGrid.getRowColData(i, 7)==null||BnfGrid.getRowColData(i, 7)=='') {
        BnfGrid.setRowColData(i, 7,"1");
        }
      if (BnfGrid.getRowColData(i, 6)==null||BnfGrid.getRowColData(i, 6)=='') {
        BnfGrid.setRowColData(i, 6,"1");
        }
      if (BnfGrid.getRowColData(i, 1) == "0") {
        if (typeof(sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 7))]) == "undefined")
          sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 7))] = 0;
        sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 7))] = sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 7))] + parseFloat(BnfGrid.getRowColData(i, 6));
        } else if (BnfGrid.getRowColData(i, 1) == "1") {
        if (typeof(sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 7))]) == "undefined")
          sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 7))] = 0;
        sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 7))] = sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 7))] + parseFloat(BnfGrid.getRowColData(i, 6));
        }

      }

    for (i=0; i<sumLiveBnf.length; i++) {
      if (typeof(sumLiveBnf[i])!="undefined" && sumLiveBnf[i]>1) {
        alert("生存受益人受益顺序 " + i + " 的受益比例和为：" + sumLiveBnf[i] + " 。大于100%，不能提交！");
        passVerify = false;
        } else if (typeof(sumLiveBnf[i])!="undefined" && sumLiveBnf[i]<1) {
        alert("注意：生存受益人受益顺序 " + i + " 的受益比例和为：" + sumLiveBnf[i] + " 。小于100%");
        passVerify = false;
        }
      }

    for (i=0; i<sumDeadBnf.length; i++) {
      if (typeof(sumDeadBnf[i])!="undefined" && sumDeadBnf[i]>1) {
        alert("死亡受益人受益顺序 " + i + " 的受益比例和为：" + sumDeadBnf[i] + " 。大于100%，不能提交！");
        passVerify = false;
        } else if (typeof(sumDeadBnf[i])!="undefined" && sumDeadBnf[i]<1) {
        alert("注意：死亡受益人受益顺序 " + i + " 的受益比例和为：" + sumDeadBnf[i] + " 。小于100%");
        passVerify = false;
        }
      }


    if (trim(fm.BankCode.value)=="0101") {
      if (trim(fm.BankAccNo.value).length!=19 || !isInteger(trim(fm.BankAccNo.value))) {
        alert("工商银行的账号必须是19位的数字，最后一个星号（*）不要！\n如果客户填写无误，请一定发问题件！");
        passVerify = false;
        }
      }

    //校验客户是否死亡
    if (fm.AppntCustomerNo.value!="" && isDeath(fm.AppntCustomerNo.value)) {
      alert("投保人已经死亡！");
      passVerify = false;
      }

    if (fm.CustomerNo.value!="" && isDeath(fm.CustomerNo.value)) {
      alert("被保人已经死亡！");
      passVerify = false;
      }
    } catch(e) {}

  if (!passVerify) {
    if (!confirm("投保单录入可能有错误，是否继续保存？"))
      return false;
    else
      return true;
    }
  }

//校验客户是否死亡
function isDeath(CustomerNo) {
  var strSql = "select DeathDate from LDPerson where CustomerNo='" + CustomerNo + "'";
  var arrResult = easyExecSql(strSql);

  if (arrResult == ""||arrResult == null)
    return false;
  else
    return true;
  }

/*********************************************************************
 *  保存个人投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm() {
	if(!checkBonusGetMode(1)){
		return false;
	}
  //增加  点保存按钮时  受益人身份证校验  ranguo 20170823
  if(checkbnf() == false) return false;
  
  if(LoadFlag == "18" && fm.RiskCode.value !="340501" && fm.RiskCode.value !="340602")
  {
    var sql = "  select payIntv "
				              + "from LCCont "
				              + "where contNo = '" + fm.ContNo.value + "' "
				              + "   and appFlag = '1' ";
		var rs = easyExecSql(sql);
		if(rs && rs[0][0] != "" && rs[0][0] != "null")
		{
		  if(fm.PayIntv.value !== rs[0][0])
		  {
		    alert("缴费频次必须与保单缴费批次一致");
		    return false;
		  }
		}
  }
  
  if(sign != 0) {
    alert("请不要重复点击!");
    return;
    }
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

  var verifyGrade = "1";

  //根据特殊的险种做特殊处的理函数
  try {
    if(specDealByRisk()==false)
      return ;
    if(chkMult()==false)
      return;
    } catch(e) {}

  //检验出生日期，如果空，从身份证号取
  //try {checkBirthday(); } catch(e){}

  // 校验被保人是否同投保人，相同则进行复制
  try {
    verifySamePerson();
    } catch(e) {}

  // 校验录入数据
  if( verifyProposal() == false )
    return;


  if (trim(fm.all('ProposalNo').value) != "") {
    alert("该投保单号已经存在，不允许再次新增，请重新进入录入界面！");
    return false;
    }

  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  if (LoadFlag=="1"||LoadFlag=="5") {
    mAction = "INSERTPERSON";
    } else {
    mAction = "INSERTGROUP";
    }

  fm.all( 'fmAction' ).value = mAction;
  fm.action="../app/ProposalSave.jsp"

  //为保全增加，add by Yang Yalin
  if (LoadFlag=="7" || LoadFlag=="8" || LoadFlag=="18") {
    mAction = "INSERTPERSON";
    fm.fmAction.value = mAction;
    fm.action="../app/ProposalSave.jsp?BQFlag=2&EdorType="+EdorType;
    //fm.all("BQFlag").value=BQFlag;
    }

  //为无名单补名单增加，add by Minim
  if (LoadFlag=="9") {
    var strmasterpol="select proposalno from lcpol where contno='"+oldContNo+"' and riskcode='"+fm.RiskCode.value+"'";
    var arrmasterpol= easyExecSql(strmasterpol,1,0);
    fm.action="../app/ProposalSave.jsp?BQFlag=4&MasterPolNo=" + arrmasterpol[0][0];
    }
  sign = 1;
  //beforeSubmit();
  ChangeDecodeStr();
  fm.submit(); //提交
  sign = 0;
}

/**
 * 强制解除锁定
 */
function unLockTable() {
  if (fm.PrtNo.value == "") {
    alert("需要填写印刷号！");
    return;
    }

  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + fm.PrtNo.value + "&CreatePos=承保录单&PolState=1002&Action=DELETE";
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1");
  }

/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content ) {
  UnChangeDecodeStr();
  try {
    showInfo.close();
   
    } catch(e) {}
   	window.focus();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    } else {
    if (fm.ContType.value == "1" && cflag=="1") {
      if (confirm("保存成功！\n要解除该印刷号的锁定，让复核人员能操作吗？")) {
        unLockTable();
        }
      } else {

      try {
        mainRiskPolNo=parent.VD.gVSwitch.getVar("mainRiskPolNo");
        } catch(ex) {}
      //alert("loadflag:"+LoadFlag);
      if(LoadFlag == '3') {
        inputQuestButton.disabled = false;
        }

      content = "保存成功！";
      var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

      }

    //暂时保存主险投保单号码，方便附险的录入，重新选择扫描件后失效
    //try { if (top.mainPolNo == "") top.mainPolNo = fm.all("ProposalNo").value } catch(e) {}
    //try { if (mainRiskPolNo == "") mainRiskPolNo = fm.all("ProposalNo").value } catch(e) {alert("err");}
    }

  //承保计划变更，附险终止的后续处理
  if (mAction=="DELETE") {
    if (typeof(top.type)!="undefined" && top.type=="SubChangePlan") {
      var tProposalNo = fm.all('ProposalNo').value;
      var tPrtNo = fm.all('PrtNo').value;
      var tRiskCode = fm.all('RiskCode').value;

      parent.fraTitle.window.location = "./ChangePlanSubWithdraw.jsp?polNo=" + tProposalNo + "&prtNo=" + tPrtNo + "&riskCode=" + tRiskCode;
      }
    returnparent();
    }
  	mAction = "";
  }

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm() {
  try	{
    //initForm();
    var tRiskCode = fm.RiskCode.value;
    var prtNo = fm.PrtNo.value;

    emptyForm();

    fm.RiskCode.value = tRiskCode;
    fm.PrtNo.value = prtNo;

    if (LoadFlag == "2") {
      getGrpPolInfo();
      }
    } catch(re)	{
    alert("在ProposalInput.js-->resetForm函数中发生异常:初始化界面错误!");
    }
    
    //由于保全险种录入时不能多次下拉险种代码选择险种,在此刷新页面实现再次选择险种
    //若有其他方法，请告知yangyalin@sinosoft.com.cn，谢谢
    if(LoadFlag == "18")
    {
      window.location.reload();
    }
  }

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm() {
  //showDiv(operateButton,"true");
  //showDiv(inputButton,"false");
  }

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
  //if( cDebug == "1" )
  //parent.fraMain.rows = "0,0,50,82,*";
  //else
  //parent.fraMain.rows = "0,0,80,72,*";
  }

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick() {
  //下面增加相应的代码
  //showDiv( operateButton, "false" );
  //showDiv( inputButton, "true" );
  }

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick() {
  if( mOperate == 0 )	{
    mOperate = 1;

    cGrpPolNo = fm.all( 'GrpPolNo' ).value;
    cContNo = fm.all( 'ContNo' ).value;
    window.open("./ProposalQueryMain.jsp?GrpPolNo=" + cGrpPolNo + "&ContNo=" + cContNo);
    }
  }

/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick() {
	if(!checkBonusGetMode(1)){
		return false;
	}
  var tProposalNo = "";
  tProposalNo = fm.all( 'ProposalNo' ).value;
  if(checkbnf() == false) return false;
  
  if( tProposalNo == null || tProposalNo == "" )
    alert( "请先做投保单查询操作，再进行修改!" );
  else {
    // 校验录入数据
    if (fm.all('DivLCInsured').style.display == "none") {
      for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {
        if (fm.elements[elementsNum].verify != null && fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
          fm.elements[elementsNum].verify = "";
          }
        }
      }

    if( verifyProposal() == false )
      return;
    if(fm.all('RiskCode').value!="GC11001") {
      if(chkMult()==false)
        return;
      }
    // 校验被保人是否同投保人，相同则进行复制
    try {
      verifySamePerson();
      } catch(e) {}
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

    if( mAction == "" )	{
      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      if (LoadFlag=="1"||LoadFlag=="5") {
        mAction = "UPDATEPERSON";
        } else {
        mAction = "UPDATEGROUP";
        }

      fm.all( 'fmAction' ).value = mAction;

      //承保计划变更(保持投保单状态不变：复核状态，核保状态)
      if (typeof(window.ChangePlanSub) == "object")
        fm.all('fmAction').value = "ChangePlan" + fm.all('fmAction').value;
      //修改浮动费率(保持投保单状态不变：复核状态，核保状态,作用比承保计划变更大一项，能修改浮动费率，为权限考虑)
      if(LoadFlag=="10")
        fm.all('fmAction').value = "ChangePlan" + fm.all('fmAction').value;
      if(LoadFlag=="3")
        fm.all('fmAction').value = "Modify" + fm.all('fmAction').value;
      //inputQuestButton.disabled = false;
      ChangeDecodeStr();
      fm.submit(); //提交
      }

    try {
      if (typeof(top.opener.modifyClick) == "object")
        top.opener.initQuery();
      } catch(e) {}
    }
  }

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick() {
  var tProposalNo = fm.all('ProposalNo').value;
	  if(!confirm("是否要删除此险种？删除请点击“确定”，否则点“取消”。"))
  {
  	return;
  }
  if(tProposalNo==null || tProposalNo=="") {
    alert( "请先做投保单保存操作，再进行删除!" );
    } else {
    var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

    if( mAction == "" )	{
      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      mAction = "DELETE";
      fm.all( 'fmAction' ).value = mAction;
      fm.submit(); //提交
      }
    }
  }

/*********************************************************************
 *  Click事件，当点击“选择责任”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function chooseDuty() {
 cRiskCode = fm.RiskCode.value;
 cRiskVersion = fm.RiskVersion.value

 if( cRiskCode == "" || cRiskVersion == "" ) {
   alert( "您必须先录入险种和险种版本才能修改该投保单的责任项。" );
   return false
	}

	showInfo = window.open("./ChooseDutyInput.jsp?RiskCode="+cRiskCode+"&RiskVersion="+cRiskVersion);
 return true
}
/*********************************************************************
 *  Click事件，当点击“查询责任信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showDuty() {
  //下面增加相应的代码
  cPolNo = fm.ProposalNo.value;
  if( cPolNo == "" ) {
    alert( "您必须先保存投保单才能查看该投保单的责任项。" );
    return false
           }

         var showStr = "正在查询数据，请您稍候......";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  showModalDialog( "./ProposalDuty.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
  showInfo.close();
  }

/*********************************************************************
 *  Click事件，当点击“关联暂交费信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showFee() {
  cPolNo = fm.ProposalNo.value;
  var prtNo = fm.PrtNo.value;

  if( cPolNo == "" ) {
    alert( "您必须先保存投保单才能进入暂交费信息部分。" );
    return false
           }

         showInfo = window.open( "./ProposalFee.jsp?PolNo=" + cPolNo + "&polType=PROPOSAL&prtNo=" + prtNo );
  }

/*********************************************************************
 *  Click事件，当双击“投保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt() {
  if( mOperate == 0 ) {
    mOperate = 2;
    showInfo = window.open( "../sys/LDPersonMain.html" );
    }
  }

/*********************************************************************
 *  Click事件，当双击“被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsured() {
  if( mOperate == 0 ) {
    mOperate = 3;
    showInfo = window.open( "../sys/LDPersonMain.html" );
    }
  }

/*********************************************************************
 *  Click事件，当双击“连带被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubInsured( span, arrPara ) {
  if( mOperate == 0 ) {
    mOperate = 4;
    spanObj = span;
    showInfo = window.open( "../sys/LDPersonMain.html" );
    }
  }

/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPol(cArr) {
  try {
    /*
        try { fm.all('PrtNo').value = cArr[6]; } catch(ex) { };
        try { fm.all('ManageCom').value = cArr[12]; } catch(ex) { };
        try { fm.all('SaleChnl').value = cArr[15]; } catch(ex) { };
        try { fm.all('AgentCom').value = cArr[13]; } catch(ex) { };
        try { fm.all('AgentType').value = cArr[14]; } catch(ex) { };
        try { fm.all('AgentCode').value = cArr[87]; } catch(ex) { };
        try { fm.all('AgentGroup').value = cArr[88]; } catch(ex) { };
        //try { fm.all('Handler').value = cArr[82]; } catch(ex) { };
        //try { fm.all('AgentCode1').value = cArr[89]; } catch(ex) { };
        try { fm.all('Remark').value = cArr[90]; } catch(ex) { };

        try { fm.all('ContNo').value = cArr[1]; } catch(ex) { };

        //try { fm.all('Amnt').value = cArr[43]; } catch(ex) { };
        try { fm.all('CValiDate').value = cArr[29]; } catch(ex) { };
        try { fm.all('PolApplyDate').value = cArr[128]; } catch(ex) { };
        try { fm.all('HealthCheckFlag').value = cArr[72]; } catch(ex) { };
        try { fm.all('OutPayFlag').value = cArr[97]; } catch(ex) { };
        try { fm.all('PayLocation').value = cArr[59]; } catch(ex) { };
        try { fm.all('BankCode').value = cArr[102]; } catch(ex) { };
        try { fm.all('BankAccNo').value = cArr[103]; } catch(ex) { };
        try { fm.all('AccName').value = cArr[118]; } catch(ex) { };
        try { fm.all('LiveGetMode').value = cArr[98]; } catch(ex) { };
        try { fm.all('BonusGetMode').value = cArr[100]; } catch(ex) { };
        try { fm.all('AutoPayFlag').value = cArr[65]; } catch(ex) { };
        try { fm.all('InterestDifFlag').value = cArr[66]; } catch(ex) { };
        
        try { fm.all('InsuYear').value = cArr[111]; } catch(ex) { };
        try { fm.all('InsuYearFlag').value = cArr[110]; } catch(ex) { };
        try { fm.all('PolTypeFlag').value = cArr[69]; } catch(ex) { };
        try { fm.all('InsuredPeoples').value = cArr[24]; } catch(ex) { };
        try { fm.all('InsuredAppAge').value = cArr[22]; } catch(ex) { };
        

        try { fm.all('StandbyFlag1').value = cArr[78]; } catch(ex) { };
        try { fm.all('StandbyFlag2').value = cArr[79]; } catch(ex) { };
        try { fm.all('StandbyFlag3').value = cArr[80]; } catch(ex) { };
    */
    try {
      fm.all('PrtNo').value = cArr[5];
      } catch(ex) { }
    ;
    try {
      fm.all('ManageCom').value = cArr[13];
      } catch(ex) { }
    ;
    try {
      fm.all('SaleChnl').value = cArr[19];
      } catch(ex) { }
    ;
    try {
      fm.all('AgentCom').value = cArr[14];
      } catch(ex) { }
    ;
    try {
      fm.all('AgentType').value = cArr[15];
      } catch(ex) { }
    ;
    try {
      fm.all('AgentCode').value = cArr[16];
      } catch(ex) { }
    ;
    try {
      fm.all('AgentGroup').value = cArr[17];
      } catch(ex) { }
    ;
    try {
      fm.all('Handler').value = cArr[20];
      } catch(ex) { }
    ;
    try {
      fm.all('AgentCode1').value = cArr[18];
      } catch(ex) { }
    ;
    try {
      fm.all('Remark').value = cArr[92];
      } catch(ex) { }
    ;

    try {
      fm.all('ContNo').value = cArr[2];
      } catch(ex) { }
    ;

    try {
      fm.all('CValiDate').value = cArr[30];
      } catch(ex) { }
    ;
    try {
      fm.all('PolApplyDate').value = cArr[101];
      } catch(ex) { }
    ;
    try {
      fm.all('HealthCheckFlag').value = cArr[81];
      } catch(ex) { }
    ;
    //try { fm.all('OutPayFlag').value = cArr[97]; } catch(ex) { };
    try {
      fm.all('PayLocation').value = cArr[51];
      } catch(ex) { }
    ;
    //try { fm.all('BankCode').value = cArr[102]; } catch(ex) { };
    //try { fm.all('BankAccNo').value = cArr[103]; } catch(ex) { };
    //try { fm.all('AccName').value = cArr[118]; } catch(ex) { };
    try {
      fm.all('LiveGetMode').value = cArr[86];
      } catch(ex) { }
    ;
    try {
      fm.all('BonusGetMode').value = cArr[88];
      } catch(ex) { }
    ;
    try {
      fm.all('AutoPayFlag').value = cArr[77];
      } catch(ex) { }
    ;
    try {
      fm.all('InterestDifFlag').value = cArr[78];
      } catch(ex) { }
    ;

    try {
      fm.all('InsuYear').value = cArr[45];
      } catch(ex) { }
    ;
    try {
      fm.all('InsuYearFlag').value = cArr[44];
      } catch(ex) { }
    ;
    try {
      fm.all('PolTypeFlag').value = cArr[7];
      } catch(ex) { }
    ;
    try {
      fm.all('InsuredPeoples').value = cArr[26];
      } catch(ex) { }
    ;
    try {
      fm.all('InsuredAppAge').value = cArr[25];
      } catch(ex) { }
    ;


    try {
      fm.all('StandbyFlag1').value = cArr[104];
      } catch(ex) { }
    ;
    try {
      fm.all('StandbyFlag2').value = cArr[105];
      } catch(ex) { }
    ;
    try {
      fm.all('StandbyFlag3').value = cArr[106];
      } catch(ex) { }
    ;

    } catch(ex) {
    alert("displayPol err:" + ex + "\ndata is:" + cArr);
    }
  }

/*********************************************************************
 *  把保单中的投保人信息显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolAppnt(cArr) {
  // 从LCAppntInd表取数据
  try {
    fm.all('AppntCustomerNo').value = cArr[1];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntRelationToInsured').value = cArr[4];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPassword').value = cArr[5];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntName').value = cArr[6];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntSex').value = cArr[7];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntBirthday').value = cArr[8];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntNativePlace').value = cArr[9];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntNationality').value = cArr[10];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntMarriage').value = cArr[11];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntMarriageDate').value = cArr[12];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntOccupationType').value = cArr[13];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntStartWorkDate').value = cArr[14];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntSalary').value = cArr[15];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntHealth').value = cArr[16];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntStature').value = cArr[17];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntAvoirdupois').value = cArr[18];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntCreditGrade').value = cArr[19];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntIDType').value = cArr[20];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntProterty').value = cArr[21];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntIDNo').value = cArr[22];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntOthIDType').value = cArr[23];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntOthIDNo').value = cArr[24];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntICNo').value = cArr[25];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntHomeAddressCode').value = cArr[26];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntHomeAddress').value = cArr[27];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPostalAddress').value = cArr[28];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntZipCode').value = cArr[29];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPhone').value = cArr[30];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntBP').value = cArr[31];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntMobile').value = cArr[32];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntEMail').value = cArr[33];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntJoinCompanyDate').value = cArr[34];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPosition').value = cArr[35];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpNo').value = cArr[36];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpName').value = cArr[37];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpPhone').value = cArr[38];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpAddressCode').value = cArr[39];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpAddress').value = cArr[40];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntDeathDate').value = cArr[41];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntRemark').value = cArr[42];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntState').value = cArr[43];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntWorkType').value = cArr[46];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPluralityType').value = cArr[47];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntOccupationCode').value = cArr[48];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntDegree').value = cArr[49];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpZipCode').value = cArr[50];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntSmokeFlag').value = cArr[51];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntRgtAddress').value = cArr[52];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntHomeZipCode').value = cArr[53];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPhone2').value = cArr[54];
    } catch(ex) { }
  ;

  }

/*********************************************************************
 *  把保单中的投保人数据显示到投保人部分
 *  参数  ：  集体客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolAppntGrp( cArr ) {

  /*
  // 从LCAppntGrp表取数据
  try { fm.all('AppntPolNo').value = cArr[0]; } catch(ex) { };
  try { fm.all('AppntGrpNo').value = cArr[1]; } catch(ex) { };
  try { fm.all('AppntRelationToInsured').value = cArr[2]; } catch(ex) { };
  try { fm.all('AppntAppntGrade').value = cArr[3]; } catch(ex) { };
  try { fm.all('AppntPassword').value = cArr[4]; } catch(ex) { };
  try { fm.all('AppntGrpName').value = cArr[5]; } catch(ex) { };
  try { fm.all('AppntGrpAddressCode').value = cArr[6]; } catch(ex) { };
  try { fm.all('AppntGrpAddress').value = cArr[7]; } catch(ex) { };
  try { fm.all('AppntGrpZipCode').value = cArr[8]; } catch(ex) { };
  try { fm.all('AppntBusinessType').value = cArr[9]; } catch(ex) { };
  try { fm.all('AppntGrpNature').value = cArr[10]; } catch(ex) { };
  try { fm.all('AppntPeoples').value = cArr[11]; } catch(ex) { };
  try { fm.all('AppntRgtMoney').value = cArr[12]; } catch(ex) { };
  try { fm.all('AppntAsset').value = cArr[13]; } catch(ex) { };
  try { fm.all('AppntNetProfitRate').value = cArr[14]; } catch(ex) { };
  try { fm.all('AppntMainBussiness').value = cArr[15]; } catch(ex) { };
  try { fm.all('AppntCorporation').value = cArr[16]; } catch(ex) { };
  try { fm.all('AppntComAera').value = cArr[17]; } catch(ex) { };
  try { fm.all('AppntLinkMan1').value = cArr[18]; } catch(ex) { };
  try { fm.all('AppntDepartment1').value = cArr[19]; } catch(ex) { };
  try { fm.all('AppntHeadShip1').value = cArr[20]; } catch(ex) { };
  try { fm.all('AppntPhone1').value = cArr[21]; } catch(ex) { };
  try { fm.all('AppntE_Mail1').value = cArr[22]; } catch(ex) { };
  try { fm.all('AppntFax1').value = cArr[23]; } catch(ex) { };
  try { fm.all('AppntLinkMan2').value = cArr[24]; } catch(ex) { };
  try { fm.all('AppntDepartment2').value = cArr[25]; } catch(ex) { };
  try { fm.all('AppntHeadShip2').value = cArr[26]; } catch(ex) { };
  try { fm.all('AppntPhone2').value = cArr[27]; } catch(ex) { };
  try { fm.all('AppntE_Mail2').value = cArr[28]; } catch(ex) { };
  try { fm.all('AppntFax2').value = cArr[29]; } catch(ex) { };
  try { fm.all('AppntFax').value = cArr[30]; } catch(ex) { };
  try { fm.all('AppntPhone').value = cArr[31]; } catch(ex) { };
  try { fm.all('AppntGetFlag').value = cArr[32]; } catch(ex) { };
  try { fm.all('AppntSatrap').value = cArr[33]; } catch(ex) { };
  try { fm.all('AppntEMail').value = cArr[34]; } catch(ex) { };
  try { fm.all('AppntFoundDate').value = cArr[35]; } catch(ex) { };
  try { fm.all('AppntBankAccNo').value = cArr[36]; } catch(ex) { };
  try { fm.all('AppntBankCode').value = cArr[37]; } catch(ex) { };
  try { fm.all('AppntGrpGroupNo').value = cArr[38]; } catch(ex) { };
  try { fm.all('AppntState').value = cArr[39]; } catch(ex) { };
  try { fm.all('AppntRemark').value = cArr[40]; } catch(ex) { };
  try { fm.all('AppntBlacklistFlag').value = cArr[41]; } catch(ex) { };
  try { fm.all('AppntOperator').value = cArr[42]; } catch(ex) { };
  try { fm.all('AppntMakeDate').value = cArr[43]; } catch(ex) { };
  try { fm.all('AppntMakeTime').value = cArr[44]; } catch(ex) { };
  try { fm.all('AppntModifyDate').value = cArr[45]; } catch(ex) { };
  try { fm.all('AppntModifyTime').value = cArr[46]; } catch(ex) { };
  try { fm.all('AppntFIELDNUM').value = cArr[47]; } catch(ex) { };
  try { fm.all('AppntPK').value = cArr[48]; } catch(ex) { };
  try { fm.all('AppntfDate').value = cArr[49]; } catch(ex) { };
  try { fm.all('AppntmErrors').value = cArr[50]; } catch(ex) { };
  */
  try {
    fm.all('AppntPolNo').value = cArr[0];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpNo').value = cArr[1];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntRelationToInsured').value = cArr[2];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntAppntGrade').value = cArr[3];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPassword').value = cArr[4];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpName').value = cArr[5];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpAddressCode').value = cArr[6];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpAddress').value = cArr[7];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpZipCode').value = cArr[8];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntBusinessType').value = cArr[9];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpNature').value = cArr[10];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPeoples').value = cArr[11];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntRgtMoney').value = cArr[12];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntAsset').value = cArr[13];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntNetProfitRate').value = cArr[14];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntMainBussiness').value = cArr[15];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntCorporation').value = cArr[16];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntComAera').value = cArr[17];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntLinkMan1').value = cArr[18];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntDepartment1').value = cArr[19];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntHeadShip1').value = cArr[20];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPhone1').value = cArr[21];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntE_Mail1').value = cArr[22];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntFax1').value = cArr[23];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntLinkMan2').value = cArr[24];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntDepartment2').value = cArr[25];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntHeadShip2').value = cArr[26];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPhone2').value = cArr[27];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntE_Mail2').value = cArr[28];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntFax2').value = cArr[29];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntFax').value = cArr[30];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPhone').value = cArr[31];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGetFlag').value = cArr[32];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntSatrap').value = cArr[33];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntEMail').value = cArr[34];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntFoundDate').value = cArr[35];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntBankAccNo').value = cArr[36];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntBankCode').value = cArr[37];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntGrpGroupNo').value = cArr[38];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntState').value = cArr[39];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntRemark').value = cArr[40];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntBlacklistFlag').value = cArr[41];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntOperator').value = cArr[42];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntMakeDate').value = cArr[43];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntMakeTime').value = cArr[44];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntModifyDate').value = cArr[45];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntModifyTime').value = cArr[46];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntFIELDNUM').value = cArr[47];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntPK').value = cArr[48];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntfDate').value = cArr[49];
    } catch(ex) { }
  ;
  try {
    fm.all('AppntmErrors').value = cArr[50];
    } catch(ex) { }
  ;
  }

/*********************************************************************
 *  把保单中的被保人数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolInsured(cArr) {
  // 从LCInsured表取数据
  try {
    fm.all('ContNo').value=cArr[1];
    } catch(ex) {}
  ;
  //alert("contno mm:"+fm.all('ContNo').value);
  try {
    fm.all('CustomerNo').value = cArr[2];
    } catch(ex) { }
  ;
  try {
    fm.all('SequenceNo').value = cArr[11];
    } catch(ex) { }
  ;
  //try { fm.all('InsuredGrade').value = cArr[3]; } catch(ex) { };
  try {
    fm.all('RelationToInsured').value = cArr[8];
    } catch(ex) { }
  ;
  //try { fm.all('Password').value = cArr[5]; } catch(ex) { };
  try {
    fm.all('Name').value = cArr[12];
    } catch(ex) { }
  ;
  try {
    fm.all('Sex').value = cArr[13];
    } catch(ex) { }
  ;
  try {
    fm.all('Birthday').value = cArr[14];
    } catch(ex) { }
  ;
  try {
    fm.all('NativePlace').value = cArr[17];
    } catch(ex) { }
  ;
  try {
    fm.all('Nationality').value = cArr[18];
    } catch(ex) { }
  ;
  try {
    fm.all('Marriage').value = cArr[20];
    } catch(ex) { }
  ;
  try {
    fm.all('MarriageDate').value = cArr[21];
    } catch(ex) { }
  ;
  try {
    fm.all('OccupationType').value = cArr[34];
    } catch(ex) { }
  ;
  try {
    fm.all('StartWorkDate').value = cArr[31];
    } catch(ex) { }
  ;
  try {
    fm.all('Salary').value = cArr[33];
    } catch(ex) { }
  ;
  try {
    fm.all('Health').value = cArr[22];
    } catch(ex) { }
  ;
  try {
    fm.all('Stature').value = cArr[23];
    } catch(ex) { }
  ;
  try {
    fm.all('Avoirdupois').value = cArr[24];
    } catch(ex) { }
  ;
  try {
    fm.all('CreditGrade').value = cArr[26];
    } catch(ex) { }
  ;
  try {
    fm.all('IDType').value = cArr[15];
    } catch(ex) { }
  ;
  //try { fm.all('Proterty').value = cArr[21]; } catch(ex) { };
  try {
    fm.all('IDNo').value = cArr[16];
    } catch(ex) { }
  ;
  //try { fm.all('OthIDType').value = cArr[23]; } catch(ex) { };
  //try { fm.all('OthIDNo').value = cArr[24]; } catch(ex) { };
  //try { fm.all('ICNo').value = cArr[25]; } catch(ex) { };
  //try { fm.all('HomeAddressCode').value = cArr[26]; } catch(ex) { };
  //try { fm.all('HomeAddress').value = cArr[27]; } catch(ex) { };
  //try { fm.all('PostalAddress').value = cArr[28]; } catch(ex) { };
  //try { fm.all('ZipCode').value = cArr[29]; } catch(ex) { };
  //try { fm.all('Phone').value = cArr[30]; } catch(ex) { };
  //try { fm.all('BP').value = cArr[31]; } catch(ex) { };
  //try { fm.all('Mobile').value = cArr[32]; } catch(ex) { };
  //try { fm.all('EMail').value = cArr[33]; } catch(ex) { };
  //try { fm.all('JoinCompanyDate').value = cArr[34]; } catch(ex) { };
  //try { fm.all('Position').value = cArr[35]; } catch(ex) { };
  //try { fm.all('GrpNo').value = cArr[4]; } catch(ex) { };
  //try { fm.all('GrpName').value = cArr[37]; } catch(ex) { };
  //try { fm.all('GrpPhone').value = cArr[38]; } catch(ex) { };
  //try { fm.all('GrpAddressCode').value = cArr[39]; } catch(ex) { };
  //try { fm.all('GrpAddress').value = cArr[40]; } catch(ex) { };
  //try { fm.all('DeathDate').value = cArr[41]; } catch(ex) { };
  //try { fm.all('State').value = cArr[43]; } catch(ex) { };
  try {
    fm.all('WorkType').value = cArr[36];
    } catch(ex) { }
  ;
  try {
    fm.all('PluralityType').value = cArr[37];
    } catch(ex) { }
  ;
  try {
    fm.all('OccupationCode').value = cArr[35];
    } catch(ex) { }
  ;
  try {
    fm.all('Degree').value = cArr[25];
    } catch(ex) { }
  ;
  //try { fm.all('GrpZipCode').value = cArr[50]; } catch(ex) { };
  try {
    fm.all('SmokeFlag').value = cArr[38];
    } catch(ex) { }
  ;
  try {
    fm.all('RgtAddress').value = cArr[19];
    } catch(ex) { }
  ;
  //try { fm.all('HomeZipCode').value = cArr[53]; } catch(ex) { };
  //try { fm.all('Phone2').value = cArr[54]; } catch(ex) { };
  return;

  }

/*********************************************************************
 *  把查询返回的客户数据显示到连带被保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displaySubInsured() {
  fm.all( spanObj ).all( 'SubInsuredGrid1' ).value = arrResult[0][0];
  fm.all( spanObj ).all( 'SubInsuredGrid2' ).value = arrResult[0][2];
  fm.all( spanObj ).all( 'SubInsuredGrid3' ).value = arrResult[0][3];
  fm.all( spanObj ).all( 'SubInsuredGrid4' ).value = arrResult[0][4];
  }

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {
  if( arrQueryResult != null ) {
    arrResult = arrQueryResult;

    if( mOperate == 1 )	{           // 查询保单明细
      var tPolNo = arrResult[0][0];

      // 查询保单明细
      queryPolDetail( tPolNo );
      }

    if( mOperate == 2 ) {		// 投保人信息
      arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      if (arrResult == null) {
        alert("未查到投保人信息");
        } else {
        displayAppnt(arrResult[0]);
        }

      }
    if( mOperate == 3 )	{		// 主被保人信息
      arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LDPerson a Left Join LCAddress b On b.CustomerNo =a.CustomerNo Where 1=1 and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      if (arrResult == null) {
        alert("未查到主被保人信息");
        } else {
        displayInsured(arrResult[0]);
        }

      }
    if( mOperate == 4 )	{		// 连带被保人信息
      displaySubInsured(arrResult[0]);
      }
    }
  mOperate = 0;		// 恢复初态

  emptyUndefined();
  }

/*********************************************************************
 *  根据查询返回的信息查询投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPolDetail( cPolNo ) {
  emptyForm();
  //var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  //var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

  //showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //parent.fraSubmit.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;
  
  	parent.fraTitle.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;
  }


/*********************************************************************
 *  根据查询返回的信息查询险种的保险计划明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryRiskPlan( tProposalGrpContNo,tRiskCode,tContPlanCode,tMainRiskCode ) {
  emptyForm();
  //var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  //var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  //alert("./ProposalQueryRiskPlan.jsp?ProposalGrpContNo="
  //																		+ tProposalGrpContNo+"&RiskCode="+tRiskCode+"&ContPlanCode="+tContPlanCode);
  //showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //parent.fraSubmit.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;
  parent.fraSubmit.window.location = "./ProposalQueryRiskPlan.jsp?ProposalGrpContNo="
                                     + tProposalGrpContNo+"&RiskCode="+tRiskCode+"&ContPlanCode="+tContPlanCode+"&MainRiskCode="+tMainRiskCode;
  }
/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow) {
  if( cShow == "true" )
    cDiv.style.display = "";
  else
    cDiv.style.display = "none";
  }

function myonfocus() {
  if(showInfo!=null) {
    try {
      showInfo.focus();
      } catch(ex) {
      showInfo=null;
      }
    }
  }

//*************************************************************
//被保人客户号查询按扭事件
function queryInsuredNo() {
  if (fm.all("CustomerNo").value == "") {
    showInsured1();
    //} else if (LoadFlag != "1" && LoadFlag != "2") {
    //  alert("只能在投保单录入时进行操作！");
    }  else {
    arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LDPerson a Left Join LCAddress b On b.CustomerNo =a.CustomerNo Where 1=1 and a.CustomerNo = '" + fm.all("CustomerNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到主被保人信息");
      displayInsured(new Array());
      emptyUndefined();
      } else {

      displayInsured(arrResult[0]);
      }
    }
  }

//*************************************************************
//投保人客户号查询按扭事件
function queryAppntNo() {
  if (fm.all("AppntCustomerNo").value == "" && LoadFlag == "1") {
    showAppnt1();
    //} else if (LoadFlag != "1" && LoadFlag != "2") {
    //  alert("只能在投保单录入时进行操作！");
    } else {
    arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LDPerson a Left Join LCAddress b On b.CustomerNo =a.CustomerNo Where 1=1 and a.CustomerNo = '" + fm.all("AppntCustomerNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到投保人信息");
      displayAppnt(new Array());
      emptyUndefined();
      } else {
      displayAppnt(arrResult[0]);
      }
    }
  }

//*************************************************************
//投保人与被保人相同选择框事件
function isSamePerson() {
  //对应未选同一人，又打钩的情况
  if (fm.AppntRelationToInsured.value!="00" && fm.SamePersonFlag.checked==true) {
    fm.all('DivLCInsured').style.display = "";
    fm.SamePersonFlag.checked = false;
    alert("投保人与被保人关系不是本人，不能进行该操作！");
    }
  //对应是同一人，又打钩的情况
  else if (fm.SamePersonFlag.checked == true) {
    fm.all('DivLCInsured').style.display = "none";
    }
  //对应不选同一人的情况
  else if (fm.SamePersonFlag.checked == false) {
    fm.all('DivLCInsured').style.display = "";
    }

  for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {
    if (fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
      try {
        insuredName = fm.elements[elementsNum].name.substring(fm.elements[elementsNum].name.indexOf("t") + 1);
        if (fm.all('DivLCInsured').style.display == "none") {
          fm.all(insuredName).value = fm.elements[elementsNum].value;
          } else {
          fm.all(insuredName).value = "";
          }
        } catch (ex) {}
      }
    }

  }

//*************************************************************
//保存时校验投保人与被保人相同选择框事件
function verifySamePerson() {
  if (fm.SamePersonFlag.checked == true) {
    for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {
      if (fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
        try {
          insuredName = fm.elements[elementsNum].name.substring(fm.elements[elementsNum].name.indexOf("t") + 1);
          if (fm.all('DivLCInsured').style.display == "none") {
            fm.all(insuredName).value = fm.elements[elementsNum].value;
            } else {
            fm.all(insuredName).value = "";
            }
          } catch (ex) {}
        }
      }
    }
  else if (fm.SamePersonFlag.checked == false) {
    }

  }


/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt() {
  // 从LDPerson表取数据
  try {
    fm.all('AppntCustomerNo').value= arrResult[0][0];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntName').value= arrResult[0][1];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntSex').value= arrResult[0][2];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntBirthday').value= arrResult[0][3];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntIDType').value= arrResult[0][4];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntIDNo').value= arrResult[0][5];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntPassword').value= arrResult[0][6];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntNativePlace').value= arrResult[0][7];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntNationality').value= arrResult[0][8];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntRgtAddress').value= arrResult[0][9];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntMarriage').value= arrResult[0][10];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntMarriageDate').value= arrResult[0][11];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntHealth').value= arrResult[0][12];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntStature').value= arrResult[0][13];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntAvoirdupois').value= arrResult[0][14];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntDegree').value= arrResult[0][15];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntCreditGrade').value= arrResult[0][16];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntOthIDType').value= arrResult[0][17];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntOthIDNo').value= arrResult[0][18];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntICNo').value= arrResult[0][19];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntGrpNo').value= arrResult[0][20];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntJoinCompanyDate').value= arrResult[0][21];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntStartWorkDate').value= arrResult[0][22];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntPosition').value= arrResult[0][23];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntSalary').value= arrResult[0][24];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntOccupationType').value= arrResult[0][25];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntOccupationCode').value= arrResult[0][26];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntWorkType').value= arrResult[0][27];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntPluralityType').value= arrResult[0][28];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntDeathDate').value= arrResult[0][29];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntSmokeFlag').value= arrResult[0][30];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntBlacklistFlag').value= arrResult[0][31];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntProterty').value= arrResult[0][32];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntRemark').value= arrResult[0][33];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntState').value= arrResult[0][34];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntOperator').value= arrResult[0][35];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntMakeDate').value= arrResult[0][36];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntMakeTime').value= arrResult[0][37];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntModifyDate').value= arrResult[0][38];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntModifyTime').value= arrResult[0][39];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntGrpName').value= arrResult[0][40];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntHomeAddress').value= arrResult[0][41];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntHomeZipCode').value= arrResult[0][42];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntPhone').value= arrResult[0][43];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntPhone2').value= arrResult[0][44];
    } catch(ex) {}
  ;
  }

/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  集体客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayAddress1() {
  try {
    fm.all('GrpNo').value= arrResult[0][0];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpCustomerNo').value= arrResult[0][0];
    } catch(ex) { }
  ;
  try {
    fm.all('AddressNo').value= arrResult[0][1];
    } catch(ex) { }
  ;
  try {
    fm.all('AppGrpAddress').value= arrResult[0][2];
    } catch(ex) { }
  ;
  try {
    fm.all('AppGrpZipCode').value= arrResult[0][3];
    } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value= arrResult[0][4];
    } catch(ex) { }
  ;
  try {
    fm.all('Department1').value= arrResult[0][5];
    } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value= arrResult[0][6];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpPhone1').value= arrResult[0][7];
    } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value= arrResult[0][8];
    } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value= arrResult[0][9];
    } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value= arrResult[0][10];
    } catch(ex) { }
  ;
  try {
    fm.all('Department2').value= arrResult[0][11];
    } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value= arrResult[0][12];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpPhone2').value= arrResult[0][13];
    } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value= arrResult[0][14];
    } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value= arrResult[0][15];
    } catch(ex) { }
  ;
  try {
    fm.all('Operator').value= arrResult[0][16];
    } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value= arrResult[0][17];
    } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value= arrResult[0][18];
    } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value= arrResult[0][19];
    } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value= arrResult[0][20];
    } catch(ex) { }
  ;
  //以下是ldgrp表
  try {
    fm.all('BusinessType').value= arrResult[0][22];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][23];
    } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][24];
    } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][25];
    } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][26];
    } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][27];
    } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][28];
    } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][29];
    } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][30];
    } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][31];
    } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][32];
    } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][33];
    } catch(ex) { }
  ;
  try {
    fm.all('AppGrpNo').value= arrResult[0][34];
    } catch(ex) { }
  ;
  try {
    fm.all('AppGrpName').value= arrResult[0][35];
    } catch(ex) { }
  ;
  }
function displayAppntGrp( cArr ) {
  // 从LDGrp表取数据
  try {
    fm.all('AppGrpNo').value = cArr[0];
    } catch(ex) { }
  ;
  try {
    fm.all('Password').value = cArr[1];
    } catch(ex) { }
  ;
  try {
    fm.all('AppGrpName').value = cArr[2];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressCode').value = cArr[3];
    } catch(ex) { }
  ;
  try {
    fm.all('AppGrpAddress').value = cArr[4];
    } catch(ex) { }
  ;
  try {
    fm.all('AppGrpZipCode').value = cArr[5];
    } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value = cArr[6];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value = cArr[7];
    } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value = cArr[8];
    } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value = cArr[9];
    } catch(ex) { }
  ;
  try {
    fm.all('Asset').value = cArr[10];
    } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value = cArr[11];
    } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value = cArr[12];
    } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value = cArr[13];
    } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value = cArr[14];
    } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value = cArr[15];
    } catch(ex) { }
  ;
  try {
    fm.all('Department1').value = cArr[16];
    } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value = cArr[17];
    } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value = cArr[18];
    } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value = cArr[19];
    } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value = cArr[20];
    } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value = cArr[21];
    } catch(ex) { }
  ;
  try {
    fm.all('Department2').value = cArr[22];
    } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value = cArr[23];
    } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value = cArr[24];
    } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value = cArr[25];
    } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value = cArr[26];
    } catch(ex) { }
  ;
  try {
    fm.all('Fax').value = cArr[27];
    } catch(ex) { }
  ;
  try {
    fm.all('Phone').value = cArr[28];
    } catch(ex) { }
  ;
  try {
    fm.all('GetFlag').value = cArr[29];
    } catch(ex) { }
  ;
  try {
    fm.all('Satrap').value = cArr[30];
    } catch(ex) { }
  ;
  try {
    fm.all('EMail').value = cArr[31];
    } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value = cArr[32];
    } catch(ex) { }
  ;
  try {
    fm.all('BankAccNo').value = cArr[33];
    } catch(ex) { }
  ;
  try {
    fm.all('BankCode').value = cArr[34];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpGroupNo').value = cArr[35];
    } catch(ex) { }
  ;
  try {
    fm.all('State').value = cArr[36];
    } catch(ex) { }
  ;
  }

/*********************************************************************
 *  把查询返回的客户数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayInsured() {
  // 从LDPerson表取数据
  try {
    fm.all('GrpContNo').value=arrResult[0][0];
    } catch(ex) {}
  ;
  try {
    fm.all('ContNo').value=arrResult[0][1];
    } catch(ex) {}
  ;
  try {
    fm.all('InsuredNo').value=arrResult[0][2];
    } catch(ex) {}
  ;
  try {
    fm.all('PrtNo').value=arrResult[0][3];
    } catch(ex) {}
  ;
  try {
    fm.all('AppntNo').value=arrResult[0][4];
    } catch(ex) {}
  ;
  try {
    fm.all('ManageCom').value=arrResult[0][5];
    } catch(ex) {}
  ;
  try {
    fm.all('ExecuteCom').value=arrResult[0][6];
    } catch(ex) {}
  ;
  try {
    fm.all('FamilyID').value=arrResult[0][7];
    } catch(ex) {}
  ;
  try {
    fm.all('RelationToMainInsured').value=arrResult[0][8];
    } catch(ex) {}
  ;
  try {
    fm.all('RelationToAppnt').value=arrResult[0][9];
    } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value=arrResult[0][10];
    } catch(ex) {}
  ;
  try {
    fm.all('SequenceNo').value=arrResult[0][11];
    } catch(ex) {}
  ;
  try {
    fm.all('Name').value=arrResult[0][12];
    } catch(ex) {}
  ;
  try {
    fm.all('Sex').value=arrResult[0][13];
    } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value=arrResult[0][14];
    } catch(ex) {}
  ;
  try {
    fm.all('IDType').value=arrResult[0][15];
    } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value=arrResult[0][16];
    } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value=arrResult[0][17];
    } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value=arrResult[0][18];
    } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value=arrResult[0][19];
    } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value=arrResult[0][20];
    } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value=arrResult[0][21];
    } catch(ex) {}
  ;
  try {
    fm.all('Health').value=arrResult[0][22];
    } catch(ex) {}
  ;
  try {
    fm.all('Stature').value=arrResult[0][23];
    } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value=arrResult[0][24];
    } catch(ex) {}
  ;
  try {
    fm.all('Degree').value=arrResult[0][25];
    } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value=arrResult[0][26];
    } catch(ex) {}
  ;
  try {
    fm.all('BankCode').value=arrResult[0][27];
    } catch(ex) {}
  ;
  try {
    fm.all('BankAccNo').value=arrResult[0][28];
    } catch(ex) {}
  ;
  try {
    fm.all('AccName').value=arrResult[0][29];
    } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value=arrResult[0][30];
    } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value=arrResult[0][31];
    } catch(ex) {}
  ;
  try {
    fm.all('Position').value=arrResult[0][32];
    } catch(ex) {}
  ;
  try {
    fm.all('Salary').value=arrResult[0][33];
    } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value=arrResult[0][34];
    } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value=arrResult[0][35];
    } catch(ex) {}
  ;
  try {
    fm.all('WorkType').value=arrResult[0][36];
    } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value=arrResult[0][37];
    } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value=arrResult[0][38];
    } catch(ex) {}
  ;
  try {
    fm.all('ContPlanCode').value=arrResult[0][39];
    } catch(ex) {}
  ;
  try {
    fm.all('Operator').value=arrResult[0][40];
    } catch(ex) {}
  ;
  try {
    fm.all('InsuredStat').value=arrResult[0][41];
    } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value=arrResult[0][42];
    } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value=arrResult[0][43];
    } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value=arrResult[0][44];
    } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value=arrResult[0][45];
    } catch(ex) {}
  ;
  try {
    fm.all('GrpName').value= arrResult[0][46];
    } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= arrResult[0][47];
    } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= arrResult[0][48];
    } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= arrResult[0][49];
    } catch(ex) {}
  ;
  try {
    fm.all('Phone2').value= arrResult[0][50];
    } catch(ex) {}
  ;
  //alert("joindate:"+fm.all('JoinCompanyDate').value);
  //alert("grpcontno:"+fm.all('GrpContNo').value);
  }

//*********************************************************************
function showAppnt1() {
  if( mOperate == 0 ) {
    mOperate = 2;
    showInfo = window.open( "../sys/LDPersonQuery.html" );
    }
  }

//*********************************************************************
function showInsured1() {
  if( mOperate == 0 ) {
    mOperate = 3;
    showInfo = window.open( "../sys/LDPersonQuery.html" );
    }
  }

function isSamePersonQuery() {
  fm.SamePersonFlag.checked = true;
  //divSamePerson.style.display = "none";
  DivLCInsured.style.display = "none";
  }

//问题件录入
function QuestInput() {
  //if(inputQuestButton.disabled == true)
  // return;
  cContNo = fm.all("ContNo").value;  //保单号码
  if(LoadFlag=="2"||LoadFlag=="4") {
    if(mSwitch.getVar( "ProposalGrpContNo" )=="") {
      alert("尚无集体合同投保单号，请先保存!");
      } else {
      window.open("./GrpQuestInputMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+LoadFlag);
      }
    } else {
    if(cContNo == "") {
      alert("尚无合同投保单号，请先保存!");
      } else {
      window.open("../uw/QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+ LoadFlag,"window1");
      }
    }
  }
function QuestQuery() {
  
  if(LoadFlag == "18")
  {
    alert("契约功能，保全不可使用。");
    return false;
  }
  cContNo = fm.all("ContNo").value;  //保单号码
  if(LoadFlag=="2"||LoadFlag=="4"||LoadFlag=="13") {
    if(mSwitch.getVar( "ProposalGrpContNo" )==""||mSwitch.getVar( "ProposalGrpContNo" )==null) {
      alert("请先选择一个团体主险投保单!");
      return ;
      } else {
      window.open("./GrpQuestQueryMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+LoadFlag);
      }
    } else {
    if(cContNo == "") {
      alert("尚无合同投保单号，请先保存!");
      } else {
      window.open("../uw/QuestQueryMain.jsp?ContNo="+cContNo+"&Flag="+LoadFlag,"window1");
      }
    }
  }
//显示投保人年龄
function showAppntAge() {
  var age = calAge(fm.all("AppntBirthday").value);
  var today = new Date();

  fm.all("AppntBirthday").title = "投保人到今天 " + today.toLocaleString()
                                  + " \n的年龄为：" + age + " 岁!";
  }

//显示被保人年龄
function showAge() {
  var age = calAge(fm.all("Birthday").value);
  var today = new Date();

  fm.all("Birthday").title = "被保人到今天 " + today.toLocaleString()
                             + " \n的年龄为：" + age + " 岁!";
  }

//检验投保人出生日期，如果空，且身份证号有，则从身份证取。取不到返回空格;
function checkBirthday() {
  try {
    var strBrithday = "";
    if(trim(fm.all("AppntBirthday").value)==""||fm.all("AppntBirthday").value==null) {
      if (trim(fm.all("AppntIDType").value) == "0") {
        strBrithday=	getBirthdatByIdNo(fm.all("AppntIDNo").value);
        if(strBrithday=="")
          passVerify=false;

        fm.all("AppntBirthday").value= strBrithday;
        }
      }
    } catch(e) {
    }
  }

//校验录入的险种是否不需要校验，如果需要返回true,否则返回false
function needVerifyRiskcode() {

  try {
    var riskcode=fm.all("RiskCode").value;

    var tSql = "select Sysvarvalue from LDSysVar where Sysvar='NotVerifyRiskcode'";
    var tResult = easyExecSql(tSql, 1, 1, 1);
    var strRiskcode = tResult[0][0];
    var strValue=strRiskcode.split("/");
    var i=0;
    while(i<strValue.length) {
      if(riskcode==strValue[i]) {
        return false;
        }
      i++;
      }
    } catch(e) {}

  return true;


  }


/*********************************************************************
 *  初始化特殊的附险信息-121301和其它险种的初始化不一样
 *  参数  ：  投保单号
 *  返回值：  无
 *********************************************************************
 */
function initPrivateRiskInfo121301(cPolNo) {
  if(cPolNo=="") {
    alert("没有主险保单号,不能进行附加险录入!");
    mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
    return false
           }

         var arrLCPol = new Array();
  var arrQueryResult = null;
  // 主保单信息部分
  var sql = "select * from lcpol where polno='" + cPolNo + "' "
            + "and riskcode in "
            + "( select riskcode from LMRiskApp where SubRiskFlag = 'M' )";

  arrQueryResult = easyExecSql( sql , 1, 0);

  if (arrQueryResult == null)	{
    mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.

    //top.mainPolNo = "";
    mainRiskPolNo = "";

    alert("读取主险信息失败,不能进行附加险录入!");
    return false
           }

         arrLCPol = arrQueryResult[0];
  displayPol( arrLCPol );
  displayPolSpec( arrLCPol );//初始化特殊要求的保单信息

  fm.all("MainPolNo").value = cPolNo;
  var tAR;

  //个人投保人信息
  arrQueryResult = null;
  arrQueryResult = easyExecSql("select * from lcappntind where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[26]+"'", 1, 0);
  tAR = arrQueryResult[0];
  displayPolAppnt(tAR);
  try {
    fm.all('AppntRelationToInsured').value = '00';
    } catch(ex) { }
  ;
  try {
    fm.all("SamePersonFlag").checked = true;
    } catch(ex) { }
  ;
  try {
    isSamePerson();
    } catch(ex) { }
  ;
  try {
    fm.all("SamePersonFlag").disabled=true
                                      } catch(ex) { }
                                    ;


  // 被保人信息部分
  //	arrQueryResult = null;
  // 	arrQueryResult = easyExecSql("select * from lcappntind where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[26]+"'", 1, 0);
  // 	tAR = arrQueryResult[0];
  //	displayPolInsuredSpec(tAR);


  return true;
  }



/*********************************************************************
 *  把保单数组中的数据显示到特殊的险种信息显示部分-121301,
 *  参数  ：  保单的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolSpec(cArr) {
  try {

    try {
      fm.all('PayEndYear').value = cArr[109];
      } catch(ex) { }
    ;
    try {
      fm.all('PayEndYearFlag').value = cArr[108];
      } catch(ex) { }
    ;
    try {
      fm.all('PayIntv').value = cArr[57];
      } catch(ex) { }
    ;
    try {
      fm.all('Amnt').value = cArr[39];
      } catch(ex) { }
    ;	    //主险的保费即附险的保额
    try {
	      if(fm.all('PayIntv').value == 0){
	      	PayEndType.style.display="none";
	      }else{
	      	PayEndType.style.display="";
	      }
      } catch(ex) { }
    ;	 
    } catch(ex) {
    alert("displayPolSpec err:" + ex + "\ndata is:" + cArr);
    }
  }



/*********************************************************************
 *  初始化特殊的附险信息-321601和其它险种的初始化不一样
 *  参数  ：  投保单号
 *  返回值：  无
 *********************************************************************
 */
function initPrivateRiskInfo321601(cPolNo) {
  if(cPolNo=="") {
    alert("没有主险保单号,不能进行附加险录入!");
    mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
    return false
           }

         var arrLCPol = new Array();
  var arrQueryResult = null;
  // 主保单信息部分
  var sql = "select * from lcpol where polno='" + cPolNo + "' "
            + "and riskcode in "
            + "( select riskcode from LMRiskApp where SubRiskFlag = 'M' )";

  arrQueryResult = easyExecSql( sql , 1, 0);

  if (arrQueryResult == null)	{
    mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.

    //top.mainPolNo = "";
    fm.all('mainRiskPolNo').value = "";

    alert("读取主险信息失败,不能进行附加险录入!");
    return false
           }

         arrLCPol = arrQueryResult[0];
  displayPol( arrLCPol );

  //初始化特殊要求的保单信息--//主险的保费即附险的保额(取主险保费和500000之间小值)
  try {
    if(arrLCPol[39]<500000)
      fm.all('Amnt').value = arrLCPol[39];
    else
      fm.all('Amnt').value = 500000;
    } catch(ex) {
    alert(ex);
    }


  fm.all("MainPolNo").value = cPolNo;
  var tAR;

  //投保人信息
  if (arrLCPol[28]=="2") {     //集体投保人信息
    arrQueryResult = null;
    arrQueryResult = easyExecSql("select * from lcappntgrp where polno='"+cPolNo+"'"+" and grpno='"+arrLCPol[26]+"'", 1, 0);
    tAR = arrQueryResult[0];
    displayPolAppntGrp(tAR);
    } else {                     //个人投保人信息
    arrQueryResult = null;
    arrQueryResult = easyExecSql("select * from lcappntind where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[26]+"'", 1, 0);
    tAR = arrQueryResult[0];
    displayPolAppnt(tAR);
    }

  // 被保人信息部分
  if (arrLCPol[18] == arrLCPol[26]) {
    fm.all("SamePersonFlag").checked = true;
    parent.fraInterface.isSamePersonQuery();
    parent.fraInterface.fm.all("CustomerNo").value = arrLCPol[18];
    }
  //else {
  arrQueryResult = null;
  arrQueryResult = easyExecSql("select * from lcinsured where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[18]+"'", 1, 0);
  tAR = arrQueryResult[0];
  displayPolInsured(tAR);
  //}

  return true;
  }


/*********************************************************************
 *  特殊险种处理：把主保单中的投保人数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolInsuredSpec(cArr) {
  // 从LCAppntInd表取数据
  try {
    fm.all('CustomerNo').value = cArr[1];
    } catch(ex) { }
  ;
  try {
    fm.all('Password').value = cArr[5];
    } catch(ex) { }
  ;
  try {
    fm.all('Name').value = cArr[6];
    } catch(ex) { }
  ;
  try {
    fm.all('Sex').value = cArr[7];
    } catch(ex) { }
  ;
  try {
    fm.all('Birthday').value = cArr[8];
    } catch(ex) { }
  ;
  try {
    fm.all('NativePlace').value = cArr[9];
    } catch(ex) { }
  ;
  try {
    fm.all('Nationality').value = cArr[10];
    } catch(ex) { }
  ;
  try {
    fm.all('Marriage').value = cArr[11];
    } catch(ex) { }
  ;
  try {
    fm.all('MarriageDate').value = cArr[12];
    } catch(ex) { }
  ;
  try {
    fm.all('OccupationType').value = cArr[13];
    } catch(ex) { }
  ;
  try {
    fm.all('StartWorkDate').value = cArr[14];
    } catch(ex) { }
  ;
  try {
    fm.all('Salary').value = cArr[15];
    } catch(ex) { }
  ;
  try {
    fm.all('Health').value = cArr[16];
    } catch(ex) { }
  ;
  try {
    fm.all('Stature').value = cArr[17];
    } catch(ex) { }
  ;
  try {
    fm.all('Avoirdupois').value = cArr[18];
    } catch(ex) { }
  ;
  try {
    fm.all('CreditGrade').value = cArr[19];
    } catch(ex) { }
  ;
  try {
    fm.all('IDType').value = cArr[20];
    } catch(ex) { }
  ;
  try {
    fm.all('Proterty').value = cArr[21];
    } catch(ex) { }
  ;
  try {
    fm.all('IDNo').value = cArr[22];
    } catch(ex) { }
  ;
  try {
    fm.all('OthIDType').value = cArr[23];
    } catch(ex) { }
  ;
  try {
    fm.all('OthIDNo').value = cArr[24];
    } catch(ex) { }
  ;
  try {
    fm.all('ICNo').value = cArr[25];
    } catch(ex) { }
  ;
  try {
    fm.all('HomeAddressCode').value = cArr[26];
    } catch(ex) { }
  ;
  try {
    fm.all('HomeAddress').value = cArr[27];
    } catch(ex) { }
  ;
  try {
    fm.all('PostalAddress').value = cArr[28];
    } catch(ex) { }
  ;
  try {
    fm.all('ZipCode').value = cArr[29];
    } catch(ex) { }
  ;
  try {
    fm.all('Phone').value = cArr[30];
    } catch(ex) { }
  ;
  try {
    fm.all('BP').value = cArr[31];
    } catch(ex) { }
  ;
  try {
    fm.all('Mobile').value = cArr[32];
    } catch(ex) { }
  ;
  try {
    fm.all('EMail').value = cArr[33];
    } catch(ex) { }
  ;
  //try { fm.all('BankCode').value = cArr[34]; } catch(ex) { };
  //try { fm.all('BankAccNo').value = cArr[35]; } catch(ex) { };
  try {
    fm.all('JoinCompanyDate').value = cArr[34];
    } catch(ex) { }
  ;
  try {
    fm.all('Position').value = cArr[35];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpNo').value = cArr[36];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value = cArr[37];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpPhone').value = cArr[38];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressCode').value = cArr[39];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value = cArr[40];
    } catch(ex) { }
  ;
  try {
    fm.all('DeathDate').value = cArr[41];
    } catch(ex) { }
  ;
  try {
    fm.all('State').value = cArr[43];
    } catch(ex) { }
  ;
  try {
    fm.all('WorkType').value = cArr[46];
    } catch(ex) { }
  ;
  try {
    fm.all('PluralityType').value = cArr[47];
    } catch(ex) { }
  ;
  try {
    fm.all('OccupationCode').value = cArr[49];
    } catch(ex) { }
  ;
  try {
    fm.all('Degree').value = cArr[48];
    } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value = cArr[50];
    } catch(ex) { }
  ;
  try {
    fm.all('SmokeFlag').value = cArr[51];
    } catch(ex) { }
  ;
  try {
    fm.all('RgtAddress').value = cArr[52];
    } catch(ex) { }
  ;

  }


///险种页面数据提交时对特殊险种的特殊处理
//function specDealByRisk()
//{
//	//如果是同心卡-阳光旅程险
//	if(fm.all('RiskCode').value=='311603')
//	{
//	   if(trim(fm.all("AppntBirthday").value)==""||fm.all("AppntBirthday").value==null)
//	   {
//	   	if (trim(fm.all("AppntIDType").value) != "0"||fm.all("AppntIDNo").value==null||trim(fm.all("AppntIDNo").value)=="")
//	   	{
//	   		fm.all("AppntBirthday").value='1970-1-1';
//	   	}
//	   }
//
//		try
//		{
//			  var strBrithday = "";
//			  if(trim(fm.all("Birthday").value)==""||fm.all("Birthday").value==null)
//			  {
//			  	if (trim(fm.all("IDType").value) == "0")
//			  	 {
//			  	   strBrithday=	getBirthdatByIdNo(fm.all("IDNo").value);
//			  	   if(strBrithday=="") passVerify=false;
//
//		           fm.all("Birthday").value= strBrithday;
//			  	 }
//		      }
//		 }
//		 catch(e)
//		 {
//		}
//		return true;
//	}
//	//如果是团体商业补充医疗险
//	if(fm.all('RiskCode').value=='211801')
//	{
//		//可以对险种条件校验
//		var strChooseDuty="";
//		for(i=0;i<=8;i++)
//		{
//			if(DutyGrid.getChkNo(i)==true)
//			{
//				strChooseDuty=strChooseDuty+"1";
//				DutyGrid.setRowColData(i, 5, fm.all('PayEndYear').value);//交费年期
//				DutyGrid.setRowColData(i, 6, fm.all('PayEndYearFlag').value);//交费年期单位
//				DutyGrid.setRowColData(i, 9, fm.all('InsuYear').value);//保险年期
//				DutyGrid.setRowColData(i, 10, fm.all('InsuYearFlag').value);//保险年期单位
//				DutyGrid.setRowColData(i, 11, fm.all('PayIntv').value);//缴费方式
//			}
//			else
//			{
//				strChooseDuty=strChooseDuty+"0";
//			}
//		}
//		//alert(strChooseDuty);
//		//fm.all('StandbyFlag1').value=strChooseDuty;
//		return true;
//	}
//	//如果是民生基业长青员工福利计划 add by guoxiang 2004-9-8 10:24
//	if(fm.all('RiskCode').value=='211701')
//	{
//		//可以对险种条件校验
//		var strChooseDuty="";
//		for(i=0;i<=2;i++)
//		{
//			if(DutyGrid.getChkNo(i)==true)
//			{
//				strChooseDuty=strChooseDuty*1+1.0;
//                DutyGrid.setRowColData(i, 3, fm.all('Prem').value);//保费
//				DutyGrid.setRowColData(i, 9, fm.all('InsuYear').value);//保险年期
//				DutyGrid.setRowColData(i, 10, fm.all('InsuYearFlag').value);//保险年期单位
//				DutyGrid.setRowColData(i, 11, fm.all('PayIntv').value);//缴费方式
//				DutyGrid.setRowColData(i, 12, fm.all('ManageFeeRate').value);//管理费比例
//
//			}
//			else
//			{
//				strChooseDuty=strChooseDuty*1+0.0;
//			}
//		}
//		if(strChooseDuty>1){
//		   alert("基业长青员工福利每张保单只允许选择一个责任，您选择的责任次数为"+strChooseDuty+"，请修改！！！");
//		   return false;
//	    }
//		return true;
//	}
//
//	//如果是个人长瑞险
//	if(fm.all('RiskCode').value=='112401')
//	{
//		if(fm.all('GetYear').value!=''&&fm.all('InsuYear').value!='')
//		{
//		  	if(fm.all('InsuYear').value=='A')
//		  	{
//		  		fm.all('InsuYear').value='88';
//		  		fm.all('InsuYearFlag').value='A';
//		  	}
//		  	else if(fm.all('InsuYear').value=='B')
//		  	{
//		  		fm.all('InsuYear').value=20+Number(fm.all('GetYear').value);
//		  		fm.all('InsuYearFlag').value='A';
//		  	}
//		  	else
//		  	{
//		  		alert("保险期间必须选择！");
//		  		return false;
//
//		  	}
//		  	if(fm.all('PayIntv').value=='0')
//		  	{
//		  		fm.all('PayEndYear').value=fm.all('InsuYear').value;
//		  		fm.all('PayEndYearFlag').value=fm.all('InsuYearFlag').value;
//		  	}
//
//		}
//		return true;
//	}
//	//如果是君安行险种
//	if(fm.all('RiskCode').value=='241801')
//	{
//		try
//		{
//			var InsurCount = fm.all('StandbyFlag2').value;
//			if(InsurCount>4||InsurCount<0)
//			{
//				alert("连带被保人人数不能超过4人");
//				return false;
//			}
//			SubInsuredGrid.delBlankLine("SubInsuredGrid");
//			var rowNum=SubInsuredGrid.mulLineCount;
//			if(InsurCount!=rowNum)
//			{
//			    alert("连带被保人人数和多行输入的连带被保人信息的行数不符合！ ");
//				return false;
//			}
//
//	    }
//	    catch(ex)
//	    {
//	      alert(ex);
//	      return false;
//	    }
//	    return true;
//	}
//
//
//}

//险种页面初始化时对特殊险种的特殊处理
function initDealForSpecRisk(cRiskCode) {
  try {
    //如果是211801
    if(cRiskCode=='211801') {
      DutyGrid.addOne();
      DutyGrid.setRowColData(0, 1, '610001');
      DutyGrid.setRowColData(0, 2, '基本责任1档');
      DutyGrid.addOne();
      DutyGrid.setRowColData(1, 1, '610002');
      DutyGrid.setRowColData(1, 2, '基本责任2档');
      DutyGrid.addOne();
      DutyGrid.setRowColData(2, 1, '610003');
      DutyGrid.setRowColData(2, 2, '基本责任3档');
      DutyGrid.addOne();
      DutyGrid.setRowColData(3, 1, '610004');
      DutyGrid.setRowColData(3, 2, '基本责任4档');
      DutyGrid.addOne();
      DutyGrid.setRowColData(4, 1, '610005');
      DutyGrid.setRowColData(4, 2, '基本责任5档');
      DutyGrid.addOne();
      DutyGrid.setRowColData(5, 1, '610006');
      DutyGrid.setRowColData(5, 2, '基本责任6档');
      DutyGrid.addOne();
      DutyGrid.setRowColData(6, 1, '610007');
      DutyGrid.setRowColData(6, 2, '公共责任7档');
      DutyGrid.addOne();
      DutyGrid.setRowColData(7, 1, '610008');
      DutyGrid.setRowColData(7, 2, '公共责任8档');
      DutyGrid.addOne();
      DutyGrid.setRowColData(8, 1, '610009');
      DutyGrid.setRowColData(8, 2, '女员工生育责任');
      DutyGrid.lock();


      }

    //众悦年金
    if(cRiskCode=='212401') {

      PremGrid.addOne();
      PremGrid.setRowColData(0, 1, '601001');
      PremGrid.setRowColData(0, 2, '601101');
      PremGrid.setRowColData(0, 3, '集体交费');
      PremGrid.addOne();
      PremGrid.setRowColData(1, 1, '601001');
      PremGrid.setRowColData(1, 2, '601102');
      PremGrid.setRowColData(1, 3, '个人交费');
      PremGrid.lock();

      }

    //基业长青
    if(cRiskCode=='211701') {
      var strSql = "select *  from lmdutypayrela where dutycode in  "
                   + " (select dutycode from lmriskduty where riskcode='"+cRiskCode+"')";
      turnPage.queryModal(strSql, PremGrid);
      PremGrid.lock;
      }

    } catch(ex) {}

  }

/*********************************************************************
* 险种页面初始化时对特殊险种的特殊处理扩展
*  add by guoxiang  at 2004-9-6 16:21
*  for update up function initDealForSpecRisk
*  not write function for every risk
*********************************************************************
 */
function initDealForSpecRiskEx(cRiskCode) {
  try {
    var strSql="";
    if(fm.all('inpNeedDutyGrid').value==1) {
      initDutyGrid();  //根据险种初始化多行录入框
      strSql = "select dutycode,dutyname,'','','','','','','','','',''  from lmduty where dutycode in "
               + " (select dutycode from lmriskduty where riskcode='"+cRiskCode+"' and choflag!='B')";
      //alert("sql:"+strSql);
      turnPage.queryModal(strSql, DutyGrid);
      var cDutyCode="";
      var tSql="";
      for(var i=0;i<=DutyGrid.mulLineCount-1;i++) {
        cDutyCode=DutyGrid.getRowColData(i,1);
        tSql="select choflag from lmriskduty where riskcode='"+cRiskCode+"' and dutycode='"+cDutyCode+"'";
        var arrResult=easyExecSql(tSql,1,0);
        //alert("ChoFlag:"+arrResult[0]);
        if(arrResult[0]=="M") {
          DutyGrid.checkBoxSel(i+1);
          }
        }
      DutyGrid.lock;

      }
    if(fm.all('inpNeedPremGrid').value==1) {
      strSql = "select a.dutycode,a.payplancode,a.payplanname,'','','','','','' from lmdutypayrela a where dutycode in  "
               + " (select dutycode from lmriskduty where riskcode='"+cRiskCode+"')";

      turnPage.queryModal(strSql, PremGrid);
      PremGrid.lock;

      }


    } catch(ex) {}

  }
function queryAgent() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
    }
  if(fm.all('AgentCode').value == "")	{
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    }
  if(fm.all('AgentCode').value != "")	 {
    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"'";// and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null) {
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
      } else {
      fm.AgentGroup.value="";
      alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
      }
    }
  }

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult) {
  if(arrResult!=null) {
    fm.AgentCode.value = arrResult[0][0];
    fm.AgentGroup.value = arrResult[0][1];
    }
  }

function queryAgent2() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
    }
  if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==10 )	 {
    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
      } else {
      fm.AgentGroup.value="";
      alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
      }
    }
  }
function returnparent() {
  var backstr=fm.all("ContNo").value;
  //alert(backstr+"backstr");
  mSwitch.deleteVar("ContNo");
  mSwitch.addVar("ContNo", "", backstr);
  mSwitch.updateVar("ContNo", "", backstr);
  var strSql = "select 1 from lcpol where prtno='" + prtNo +"' and riskcode in (select code from ldcode where codetype='ybkriskcode') with ur";
  var arrResult = easyExecSql(strSql);
  if(ApproveFlag!=null&&ApproveFlag!="")
  {
  	LoadFlag=ApproveFlag;
  }
  //回到保全明细录入页面
  if(LoadFlag == "18")
  {
    top.location.reload();
  }else if(arrResult !="" && arrResult!=null){
	  location.href="ybkgz.jsp?LoadFlag="+LoadFlag+"&prtNo="+prtNo + "&ContType="+ContType+"&scantype="+scantype+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&ScanFlag="+ScanFlag+"&MissionID=" + MissionID+ "&SubMissionID=" + SubMissionID;
  }  
  //增加保全处理之前的页面
  else
  {
    location.href="ContInsuredInput.jsp?LoadFlag="+LoadFlag+"&prtNo="+prtNo + "&ContType="+ContType+"&scantype="+scantype+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&ScanFlag="+ScanFlag+"&MissionID=" + MissionID+ "&SubMissionID=" + SubMissionID;
  }
}
//(GrpContNo,LoadFlag);//根据集体合同号查出险种信息
function getRiskByGrpPolNo(GrpContNo,LoadFlag) {
  //alert("1");
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select riskcode,riskname from lmriskapp where riskcode in (select riskcode from LCGrpPol where GrpContNo='"+GrpContNo+"')" ;
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      }
    }
  //alert ("tcodedata : " + tCodeData);

  return tCodeData;
  }
function getRiskByGrpAll() {
  //alert("1");
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('G','A','B','D') order by RiskCode" ;
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      }
    }
  //alert ("tcodedata : " + tCodeData);

  return tCodeData;
  }
function getRisk() {
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('I','A','C','D') and (EndDate is null or EndDate>current date)"
           + " order by RiskCode";
  ;
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      }
    }

  return tCodeData;



  }

function getRiskByContPlan(GrpContNo,ContPlanCode) {
  //alert(GrpContNo+":"+ContPlanCode);
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select b.RiskCode, b.RiskName from LCContPlanRisk a,LMRiskApp b where  a.GrpContNo='"+GrpContNo+"' and a.ContPlanCode='"+ContPlanCode+"' and a.riskcode=b.riskcode";
  //alert(strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      }
    }
  //alert("tCodeData:"+tCodeData);
  return tCodeData;



  }


/*********************************************************************
*  把合同所有信息录入结束确认
*  参数  ：  无
*  返回值：  无
*********************************************************************
*/
function inputConfirm(wFlag) {

    if(!checkSaleChannel())
    {
        alert("销售渠道和业务员不相符!");
        return false;
    }
    //回到保全明细录入页面
    if(LoadFlag == "18")
    {
        top.location.reload();
        return true;
    }

    var strSql = "select grpcontno,prtno from lccont where contno='"+fm.ContNo.value+"'";
    var arr = easyExecSql(strSql);
    if(arr) {
        //如果不是团单
        if(arr[0][0]!="00000000000000000000") {
            var GrpContNo = arr[0][0];
            //判断工作流状态是否是待录入完毕状态
            var strSql = "select MissionID,SubMissionID from lwmission where activityid='0000002098' and MissionProp1='"+arr[0][1]+"'";
            var workflag = easyExecSql(strSql);
            if(workflag) {
                fm.MissionID.value=workflag[0][0];
                fm.SubMissionID.value = workflag[0][1];
                fm.ProposalGrpContNo.value = arr[0][0];
                fm.WorkFlowFlag.value = "0000002098";
                }
            else
            {
                var strSql = "select MissionID,SubMissionID from lwmission where activityid='0000002099' and MissionProp1='"+arr[0][1]+"'";
                workflag = easyExecSql(strSql);
                if(workflag) {
                    fm.MissionID.value=workflag[0][0];
                    fm.SubMissionID.value = workflag[0][1];
                    fm.ProposalGrpContNo.value = arr[0][0];
                    fm.WorkFlowFlag.value = "0000002099";
                }
            }
            var strSql = "select insuredno from lcinsured where grpcontno='"+GrpContNo+"' and insuredno not in (select insuredno from lcpol where grpcontno='"+GrpContNo+"')"  ;
            var arr = easyExecSql(strSql);
            if(arr){
                if(!confirm("存在被保险人没有险种信息,确认继续?")){
                    return false;
                }
            }
        }
    }
    //险种校验
    if(!checkRiskInfo())
    {
        return false;
    }
    if (wFlag ==1 && arr[0][0]=="00000000000000000000") //录入完毕确认
    {
        var tStr= "	select * from lwmission where 1=1 and lwmission.missionprop1 = '"+mSwitch.getVar("ContNo")+"'";
        var tStr2 = "select * from lwmission where 1=1 and lwmission.missionprop1 = '"
        +fm.ContNo.value+"' and activityid='0000001001' and MissionID='"+mSwitch.getVar("MissionID")+"'";
        var ApproveFlag = false;
        turnPage.strQueryResult = easyQueryVer3(tStr,1,0,1);
        if (turnPage.strQueryResult) {
            if(easyQueryVer3(tStr2,1,0,1))
            {
                ApproveFlag = true;
            }
            else{
                alert("该合同已经做过保存！");
                return;
            }
        }
        if(ScanFlag=="1") {
            fm.WorkFlowFlag.value = "0000001099";
        }
        if(ScanFlag=="0") {
            fm.WorkFlowFlag.value = "0000001098";
        }
        if(ApproveFlag)
        {
            fm.WorkFlowFlag.value = "0000001001";
        }
        fm.MissionID.value = mSwitch.getVar("MissionID");
        fm.SubMissionID.value = mSwitch.getVar("SubMissionID");			//录入完毕
        fm.WorkFlowFlag.value = mSwitch.getVar("ActivityID");
        fm.ProposalContNo.value=fm.ContNo.value;
    }
    else if (wFlag ==2)//复核完毕确认
    {
    	if(!checkBonusGetMode(2)){
    		return false;
    	}
    	
        if(fm.all('ProposalContNo').value == "") {
            alert("未查询出合同信息,不容许您进行 [复核完毕] 确认！");
            return;
        }
        fm.WorkFlowFlag.value = "0000001001";
        fm.MissionID.value = tMissionID;
        fm.SubMissionID.value = tSubMissionID;
    }
    else if (wFlag ==3)
    {
        if(fm.all('ProposalContNo').value == "") {
            alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
            return;
        }
        fm.WorkFlowFlag.value = "0000001002";
        fm.MissionID.value = tMissionID;
        fm.SubMissionID.value = tSubMissionID;
    }
    else if(arr)
    {
        
    }
    else
        return;

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./InputConfirm.jsp";
    try {
        if(fm.ProposalGrpContNo.value!=""&& fm.ProposalGrpContNo.value!="null") {
            fm.action = "./GrpInputConfirm.jsp";
        }
    } catch(ex) {}
    fm.submit(); //提交
}

//此函数的目的是：查询团单或者个单下的投保，被投保信息
function getContInputnew() {
  //取得个人投保人的所有信息
  if(fm.AppntCustomerNo.value!=""&&fm.AppntCustomerNo.value!="false") {
    arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LDPerson a Left Join LCAddress b On b.CustomerNo =a.CustomerNo Where 1=1 and a.CustomerNo ='"+fm.AppntCustomerNo.value+"'",1,0);
    displayAppnt(arrResult[0]);
    }
  //取得投保单位的 所有信息
  if(fm.GrpContNo.value!=""&&fm.GrpContNo.value!="false") {
    arrResult = easyExecSql("select a.*,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.CustomerNo,b.GrpName from LCGrpAddress a,LDGrp b where a.CustomerNo = b.CustomerNo and b.CustomerNo=(select CustomerNo from LCGrpAppnt  where GrpContNo = '" + fm.GrpContNo.value + "')", 1, 0);
    displayAddress1(arrResult[0]);
    }
  //取得被投保人的所有信息
  if(fm.all('CustomerNo').value!=""&&fm.all('CustomerNo').value!="false") {
    var tcustomerNo=fm.all('CustomerNo').value;
    //alert("contno:"+fm.all('ContNo').value);
    var tContNo=fm.all('ContNo').value;
    arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.InsuredNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LCInsured a Left Join LCAddress b On b.CustomerNo =a.InsuredNo Where 1=1 and a.InsuredNo ='"+tcustomerNo+"' and a.ContNo='"+tContNo+"'",1,0);
    displayInsured(arrResult[0]);
    }
  }



function checkbnf()
{
  for (var i=0; i<BnfGrid.mulLineCount; i++)
  {
      if(BnfGrid.getRowColData(i,1)=="1" && BnfGrid.getRowColData(i,5)=="00")
      {
      		alert("身故受益人不能选择被保险人本人,请检查!");
    	         return false;
      }
      //增加  受益人身份证校验  ranguo 20170823
      var IDType=BnfGrid.getRowColData(i,3);
      var IDNo=BnfGrid.getRowColData(i,4);
      var checkResult=checkIdNo(IDType,IDNo,"","");
      if(checkResult!=null && checkResult!=""){
    	  var j = i+1;
    	  alert("第"+j+"行 "+BnfGrid.getRowColData(i,2)+" "+checkResult);
    	  return false;
      }
  }
  return true;
}





//外包反馈错误信息 2007-9-27 20:21
function findIssue()
{
	window.open("./BPOIssueInput.jsp?prtNo="+prtNo,"window1");
}

//销售渠道和业务员的校验
function checkSaleChannel()
{
	if(prtNo != null && prtNo != '')
	{
		var sql = "select 1 from lccont a, laagent b, ldcode1 c where a.agentcode = b.agentcode and c.codetype = 'salechnl' "
						+ "and a.salechnl = c.code and b.BranchType = c.code1 and b.BranchType2 = c.CodeName and prtno = '" + prtNo + "'";
		var check = easyExecSql(sql);
		if(check != null && check == 1)
		{
			return true;
		}
		else{
			return false;
		}
	}
	else
	{
		alert("印刷号为空");
		return false;
	}
}

//险种的校验
function checkRiskInfo()
{
    //该分公司险种是否停售校验
    var riskEnd = "select 1 from LCPol a, LMRiskApp b, LDCode c where a.PrtNo = '" 
        + fm.PrtNo.value + "' and b.RiskType4 = '4' and c.CodeType = 'UniversalSale' "
        + "and a.RiskCode = b.RiskCode and c.Code = substr(a.ManageCom, 1, 4) ";
    var result = easyExecSql(riskEnd);
    if(result)
    {
        alert("该分公司万能险已停售！");
        return false;
    }
    
    //保险期间单位为空的校验
    var riskInsu = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and (InsuYearFlag is null or InsuYearFlag = '')";
    result = easyExecSql(riskInsu);
    if(result)
    {
        alert("保险期间单位为空，请修改！");
        return false;
    }
    
    //个人防癌套餐的校验
    var risk230501And331001 = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and RiskCode in ('230501', '331001')";
    result = easyExecSql(risk230501And331001);
    if(result)
    {
        var riskInfoSql = "select 1 from LCPol where PrtNo = '" + fm.PrtNo.value + "' "
            + "and RiskCode in ('230501', '331001') "
            + "and not (InsuYear in (10, 15, 20) and InsuYearFlag = 'Y' "
            + "and PayEndYear = InsuYear and PayEndYearFlag = InsuYearFlag "
            + "and PayIntv = 12)";
        result = easyExecSql(riskInfoSql);
        if(result)
        {
            alert("请检查缴费频次、保险期间和缴费期间！\n(1).保险期间必须为10年、15年、20年。"
                + "\n(2).缴费年期必须与保险期间一致。\n(3).缴费频次必须为年缴。");
            return false;
        }
    }
    return true;
}
function checkBonusGetMode(wFlag){
	var BonusGetMode = fm.BonusGetMode.value;
	if(wFlag == "1"){
		var rs3 = easyExecSql("select risktype4 from lmriskapp where riskcode='"+fm.RiskCode.value +"'");
		if(rs3 == "2" && BonusGetMode !="1" && BonusGetMode !="2"){
			alert("分红险必须录入红利领取方式：1或2！")
			return false;
		}
	}else {
		var rs4 =easyExecSql( "select a.risktype4,b.bonusgetmode from lmriskapp a, (select riskcode,BonusGetMode from lcpol where prtno = '"+parent.VD.gVSwitch.getVar( "PrtNo" )+"') b where a.riskcode =b.riskcode");
		if(rs4 != "" && rs4 != null){
			for(var i=0;i<rs4.length;i++){
				if(rs4[i][0] == "2" &&  rs4[i][1] != "1" && rs4[i][1] != "2"){
					alert("分红险必须录入红利领取方式：1或2！");
					return false;
				}
			}
		}
	}
	return true;
}

