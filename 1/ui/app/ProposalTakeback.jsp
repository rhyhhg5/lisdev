<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalQueryDownLoad.jsp
//程序功能：清单下载
//创建日期：2008-1-15 17:25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);

  String querySql = request.getParameter("querySql");
  
  querySql = querySql.replaceAll("%25","%");
  
	//设置表头
	String[][] tTitle = {{"合同号", "印刷号", "生效日期", "投保人", "保险费", "业务员代码", 
	    "业务员姓名", "管理机构", "接收日期", "回销日期", "客户签收日期", "销售渠道", 
	    "网点", "回销状态"}};
	//表头的显示属性
	int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
    
  //数据的显示属性
  int []displayData = {1,2,3,4,5,6,7,8,9,10,0,0,11,0,0,0,0,12,13,14};
  //生成文件
  CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
  createexcellist.createExcelFile();
  String[] sheetName ={"list"};
  createexcellist.addSheet(sheetName);
  int row = createexcellist.setData(tTitle,displayTitle);
  
  if(row ==-1) errorFlag = true;
  createexcellist.setRowColOffset(row+1,0);//设置偏移
  if(createexcellist.setData(querySql,displayData)==-1)
  {
  	errorFlag = true;
  	System.out.println(errorFlag);
  }
  if(!errorFlag)
  //写文件到磁盘
  try{
     createexcellist.write(tOutXmlPath);
  }catch(Exception e)
  {
  	errorFlag = true;
  	System.out.println(e);
  }
  //返回客户端
  if(!errorFlag)
  	downLoadFile(response,filePath,downLoadFileName);
  out.clear();
	out = pageContext.pushBody();
%>

