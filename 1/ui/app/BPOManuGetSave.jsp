<%
//程序名称：BPOManuGetSave.jsp
//程序功能：
//创建日期：2007-9-25 11:52
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String flag = "Succ";
  String content = "处理成功";

  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  VData data = new VData();
  data.add(tG);
  
  BPODealXMLBL bl = new BPODealXMLBL();
  if(bl.getSubmitMap(data, "") == null)
  {
    flag = "Fail";
    content = bl.mErrors.getErrContent();
  }
  else if(bl.mErrors.needDealError())
  {
    content = bl.mErrors.getErrContent();
  }
  else
  {
    content = bl.getDealInfo();
  }
  
  content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>

