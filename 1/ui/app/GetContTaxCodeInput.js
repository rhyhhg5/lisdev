var turnPage = new turnPageClass();

function easyQueryClick() {

  // 书写SQL语句
    var strSQL = "";
	initContGrid();
	strSQL = "select "
	       + " lcc.contno,lcc.prtno,lcc.appntname,lcs.taxcode,case when lcs.succflag='00' then '成功' when lcs.succflag is null then '' else '失败' end,lcs.errorinfo " 
	       + " from lccont lcc,lccontsub lcs where 1=1 " 
	       + " and lcc.prtno=lcs.prtno "
	       + " and lcc.appflag='1' " 
	       + " and lcc.conttype='1' "
	       //+ " and lcc.signdate>='2015-10-1' "
	       + " and exists (select 1 from lcpol lcp,lmriskapp lm where lcp.contno=lcc.contno and lcp.riskcode=lm.riskcode and lm.TaxOptimal='Y' )" 
	       + getWherePart("lcc.prtno", "PrtNo") 
	       + getWherePart("lcc.contno", "ContNo") 
	       + getWherePart("lcc.appntname", "AppntName", "like") 
	       + getWherePart("lcc.managecom", "ManageCom", "like") 
	       + getWherePart("lcc.signdate", "StartDate", ">=")
	       + getWherePart("lcc.signdate", "EndDate", "<=")
	       ;
	fm.strSQL.value=strSQL;
	turnPage.queryModal(strSQL, ContGrid);
	return true;
}
function GetTaxCode() {
    var checkFlag = 0;
    for (i=0; i<ContGrid.mulLineCount; i++)
    {
      if (ContGrid.getSelNo(i)){
          checkFlag = ContGrid.getSelNo();
          break;
      }
   }
   if(checkFlag){
        var	tContNo = ContGrid.getRowColData(checkFlag - 1, 1);
        var	tPrtNo = ContGrid.getRowColData(checkFlag - 1, 2);
        var	tSuccFlag = ContGrid.getRowColData(checkFlag - 1, 5);
        if(tSuccFlag=='成功'){
            alert("已获取税优识别码，不需要再次获取！");
            return false
        }
        var showStr="正在获取税优识别码，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	    var ttAction = fm.action;      
        fm.action=fm.action + "?getContNo=" + tContNo+"&getPrtNo=" + tPrtNo;        
        fm.submit(); 
        fm.action=ttAction;
   
   }else{
      alert("请先选择一条保单信息！");
      return false
   }
}

function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
		easyQueryClick(); 
	}
	else
	{ 
		content = "获取成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		easyQueryClick();
	}

}

//下载事件
function download()
{
    if(fm.strSQL.value != null && fm.strSQL.value != "" &&ContGrid.mulLineCount > 0)
    {
        fm.action = "GetContTaxCodeDownload.jsp";
        fm.submit();
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}

