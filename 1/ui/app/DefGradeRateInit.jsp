<script language="JavaScript">
//表单初始化
function initForm(){
try{
     fm.all('mManageCom').value = ManageCom;
     fm.all('mGrpContNo').value=Grpcontno;
     initButton();
     initDisableRateGrid();
   }catch(ex){ 
	   alert("初始化页面错误"+ex.message);
   }
}
//伤残给付比例信息列表的初始化
function initDisableRateGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][18]=300;

      iArray[2]=new Array();
      iArray[2][0]="责任编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][18]=700;

      iArray[3]=new Array();
      iArray[3][0]="伤残等级编码";         		//列名
      iArray[3][1]="250px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="伤残等级";         	    //列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=150;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="给付比例";         	    //列名
      iArray[5][1]="150px";            		//列宽
      iArray[5][2]=150;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      DisableRateGrid = new MulLineEnter( "fm" , "DisableRateGrid" );
      //这些属性必须在loadMulLine前
      DisableRateGrid.mulLineCount = 0;
      DisableRateGrid.displayTitle = 1;
      DisableRateGrid.hiddenPlus = 1;
      DisableRateGrid.hiddenSubtraction = 1;
      DisableRateGrid.canChk=0;
      DisableRateGrid.loadMulLine(iArray);
      
      
    }
    catch(ex) {
      alert(ex);
    }
}

</script>