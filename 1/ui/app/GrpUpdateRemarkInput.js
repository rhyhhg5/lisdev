var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var showInfo;

window.onfocus = myonfocus;

function easyQueryClick(){
	
	initPolGrid();
	
	if(fm.ManageCom.value==null || fm.ManageCom.value=="" || fm.ManageCom.value=="null"){
		alert("请录入管理机构");
		return false；
	}
	if((fm.PrtNo.value==null || fm.PrtNo.value=="" || fm.PrtNo.value=="null") && (fm.GrpContNo.value==null || fm.GrpContNo.value=="" || fm.GrpContNo.value=="null")){
		alert("合同号和印刷号不能同时为空");
		return false;
	}
	
	var strSql = "select lgc.managecom,lgc.grpcontno,lgc.prtno,lgc.cvalidate,lgc.remark " 
			+ "from lcgrpcont lgc " 
			+ "where appFlag = '1' "
			+ getWherePart('lgc.managecom','ManageCom','like')
			+ getWherePart('lgc.prtno','PrtNo')
			+ getWherePart('lgc.grpcontno','GrpContNo')
			+ getWherePart('lgc.cvalidate','CValidateStart','>=')
			+ getWherePart('lgc.cvalidate','CValidateEnd','<=')
			+ " with ur";
	
	turnPage1.pageLineNum = 50;
    turnPage1.strQueryResult  = easyQueryVer3(strSql);
	if (!turnPage1.strQueryResult) 
    {
        alert("没有查询到相关数据！");
        return false;
    }
	//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage1.arrDataCacheSet = clearArrayElements(turnPage1.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage1.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage1.strQuerySql = strSql;
    //设置查询起始位置
    turnPage1.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);
	
}

function updateAppoint(){
	
	var checkFlag = 0;
	var remark  = fm.AfterUpdateRemark.value;
	
	for(var i=0;i<PolGrid.mulLineCount;i++){
		if(PolGrid.getSelNo(i))
		{
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	if(checkFlag == 0){
		alert("请先选择一条保单信息！");
		return false;
	}else{
		if(remark==null || remark=="" || remark=="null"){
			if(confirm("没有输入修改后的特别约定，是否继续？")){
				var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
				var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
				showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.action="./GrpUpdateRemarkSave.jsp";
				fm.submit();
			}else{
				return false;
			}
		}else{
			var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
			var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
			showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.action="./GrpUpdateRemarkSave.jsp";
			fm.submit();
		}
	}
	
}

function afterSubmit(FlagStr,content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
	
}