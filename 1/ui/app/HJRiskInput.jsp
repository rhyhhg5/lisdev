<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>

<html>
  <head>
    <title>My JSP 'HJRiskInput.jsp' starting page</title>
   
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<script type="text/javascript" src="HJRiskInput.js" charset="gbk"></script>
	<%@include file="HJRiskInit.jsp"%>
  </head>
  
  <body onload="initForm();">
  	<form action="HJRiskSave.jsp" method=post name=fm target="fraSubmit">
  	<input type=hidden id="fmtransact" name=fmtransact>
  		<div id="divQueryInput" , style="display:hidden">
  			<table>
					<tr>
						<td class=titleImg>
							查询条件
						</td>
					</tr>
			</table>
			<table class=common align='center'>
					<TR class=common>
						<TD  class= title8>
            				险种编码
          				</TD>
          				<TD  class= input8>
           			    	<Input class="code8" name=riskind 
           			    	verify="行业编码|notnull&code:riskind&len<=20" 
           			    	ondblclick="return showCodeList('riskind',[this,riskindName,BusinessBigType],[0,1,2],null,null,null,1,300);" 
           			    	onkeyup="return showCodeListKey('riskind',[this,riskindName,BusinessBigType],[0,1,2],null,null,null,1,300);">
          				</TD> 
          				<TD  class= title8>
            				险种名称
          				</TD>                   
          				<TD  class= input8>   
            				<Input class="common8" name=riskindName readonly >
            				<Input class="common8" type="hidden" name=BusinessBigType>
          				</TD> 
						</TR>
			</table>
			<table>
					<td class=button align=left>
						<input type="button" class=cssButton value="查  询" id=QUERY onclick="queryClick()">
					</td>
					<td class=button align=left>
						<input type="button" class=cssButton value="保  存" id=SAVE  onclick="saveClick()">
					</td>
					<td class=button align=left>
						<input type="button" class=cssButton value="修  改" id=UPDATE onclick="updateClick()">
					</td>
					<td class=button align=left>
						<input type="button" class=cssButton value="删  除" id=DELETE onclick="deleteClick()">
						<input type="hidden" name=subCode id="subCode">
						<input type="hidden" name=subCodeType id="subCodeType">
					</td>
			</table>	
  			<!-- 查询结果部分 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divProjectMonth1);">
					</td>
					<td class=titleImg>
						险种信息
					</td>
					</tr>
				</table>
  		</div>
  		<!--信息（列表）-->
  		<Div id="divProjectMonth1" style="display:''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1>
						<span id="spanProjectMonth"> </span>
					</td>
				</tr>
			</table>
		</div>
		<!--分页部分-->
		<Div id="divPage" align=center style="display:'' ">
			<table>
				<tr>
					<td class=button>
						<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
						<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
						<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
						<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
					</td>
				</tr>
			</table>
		</Div>
  	</form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>
