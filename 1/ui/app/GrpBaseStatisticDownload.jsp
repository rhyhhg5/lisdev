<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "清单_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + request.getParameter("queryType"));
    
    String queryType = request.getParameter("queryType");
    String querySql = request.getParameter("querySql");
    querySql = querySql.replaceAll("%25","%");
    //设置标题
    String[][] global = {{"机构：",request.getParameter("ManageCom"),"","",
        "制表时间：",PubFun.getCurrentDate()}};
    //表头的显示属性
    int []displayGlobal = {1,2,3,4,5,6};
    //设置表头
    String[][] tTitle1 = {{"机构代码", "机构名称", "投保件数", "投保客户数", "投保人数", "投保金额"}};
    String[][] tTitle2 = {{"机构代码", "机构名称", "契撤件数", "契撤保费", "核保通过待签单件数", "核保通过待签单人数", "核保通过待签单保费"}};
    String[][] tTitle3 = {{"机构代码", "机构名称", "承保件数", "承保客户数", "承保人数", "承保保费", "年度承保总保费"}};
    String[][] tTitle = {{"", "", "", "", "", "", ""}};
    if("1".equals(queryType))
        for(int i = 0; i < tTitle1[0].length; i++)  tTitle[0][i] = tTitle1[0][i];
    else if("2".equals(queryType))
        for(int i = 0; i < tTitle2[0].length; i++)  tTitle[0][i] = tTitle2[0][i];
    else if("3".equals(queryType))
        for(int i = 0; i < tTitle3[0].length; i++)  tTitle[0][i] = tTitle3[0][i];
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list", "help"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(global,displayGlobal);
    if(row ==-1) errorFlag = true;
    row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
    createexcellist.setRowColOffset(row+1,0);//设置偏移
    row = createexcellist.setData(querySql,displayData);
    if(row == -1)
        errorFlag = true;

    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>

