
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%!
	String handleFunction(HttpSession session, HttpServletRequest request) {
	String tPrtNo = request.getParameter("PrtNo");
	String strOperation = "DELETE";
	

	GlobalInput globalInput = new GlobalInput();

	if( (GlobalInput)session.getValue("GI") == null )
	{
		return "网页超时或者是没有操作员信息，请重新登录";
	}
	else
	{
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
	}
	
  	SysBreakLockBL tSysBreakLockBL =new SysBreakLockBL();
  
	VData vData = new VData();
	vData.add(tPrtNo);
	vData.add(globalInput);

	try {
		if( !tSysBreakLockBL.submitData(vData, strOperation) )
		{
	   		if ( tSysBreakLockBL.mErrors.needDealError() )
	   		{
	   			return tSysBreakLockBL.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		return "保存失败，但是没有详细的原因";
			}
		}

	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		return ex.getMessage();
	}
	return "";
}
%>
<%
String FlagStr = "";
String Content = "";

try {
	Content = handleFunction(session, request);

	if( Content.equals("") ) {
		FlagStr = "Succ";
		Content = "解锁成功";
	} else {
		FlagStr = "Fail";
	}
} catch (Exception ex) {
	ex.printStackTrace();
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

