<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ES_DOC_MAINQuery.jsp
//程序功能：扫描单证下载的主页面
//创建日期：2007-09-24
//创建人  :zhousp
//更新记录：  更新人shaoax    更新日期2007-11-20     更新原因/内容:扫描单证添加删除控件
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ES_DOC_MAINQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ES_DOC_MAINQueryInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ES_DOC_MAINQueryDownLoad.jsp" method=post name=fm target="fraSubmit">
    <%//@include file="../common/jsp/OperateButton.jsp"%>
    <%//@include file="../common/jsp/InputButton.jsp"%>
    
    <Div  id= "divCode1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
		<TD class= title>扫描机构</TD>
		<TD class= input>
		<Input class="codeNo" name=ManageCom verify="扫描机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true > 
		</TD>
		<TD class= title>单证类型
		</TD>
		<TD class= input>
		<Input class=codeNo name="SubType" value="TB01" ondblclick="return showCodeList('scansubtype',[this,SubTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('scansubtype',[this,SubTypeName],[0,1],null,null,null,1);"><input class=codename name=SubTypeName value="家庭投保书" readonly=true > 
		</TD>
		      <TD  class= title>
            扫描人
          </TD>
          <TD  class= input>
            <Input class= common name=ScanOperator >
          </TD>	
        </TR>          	
        <TR  class= common>		          
          <TD  class= title>
            单证号码
          </TD>
          <TD  class= input>
            <Input class= common name=DocCode >
          </TD>
 		      <TD  class= title>
            扫描日期起 
          </TD>
          <TD  class= input>
            <Input class= coolDatePicker name=StartDate >
          </TD>
 		      <TD  class= title>
            扫描日期止
          </TD>
          <TD  class= input>
            <Input class= coolDatePicker name=EndDate >
          </TD>                       
        </TR>
        <TR  class= common>
      <TD CLASS=title  >
	      状态 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input class=codeNo  NAME="State" verify="状态|code:State" CodeData="0|^01|待处理 ^02|系统录入 ^03|发送成功 ^04|不录入 ^05|数据错误 ^06|已录入" ondblclick="showCodeListEx('State',[this,StateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('State',[this,StateName],[0,1],null,null,null,1);" ><input class=codename name=StateName readonly=true >
	    </TD>	
	    <TD CLASS=title  >
	      外包公司 
	    </TD>
	    <TD class= input COLSPAN=3>
	      <Input class="codeNo" name=BPOid verify="外包公司|code:bjwbbl" ondblclick="return showCodeList('bjwbbl',[this,BPOName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bjwbbl',[this,BPOName],[0,1],null,null,null,1);"><input class=codename name=BPOName readonly=true ><font color="#FF0000">  *该外包公司选项只针对单证类型为：TB01 可选择查询</font>
	    </TD>	           	        	
        </TR>	                       
      </table>
    </Div>
    <table>
    	<tr>
        	<td class=common>
			        <INPUT VALUE="查询单证信息" TYPE=button class=cssButton onclick="easyqueryClick()"> 	
    		</td>
    	</tr>
    </table>    
    <input type=hidden id="fmtransact" name="fmtransact">

    <Input TYPE=hidden  name=BussNoType value="">
    <Input TYPE=hidden  name=BussType value="">
    
    
  	<Div align=center id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCodeGrid" >
  					</span> 
  			  	</td>
  			</tr>
  			</table>
    	<br>
      	<INPUT VALUE="首页" TYPE=button class=cssButton onclick="getFirstPage();"> 
      	<INPUT VALUE="上一页" TYPE=button class=cssButton onclick="getPreviousPage();"> 					
      	<INPUT VALUE="下一页" TYPE=button class=cssButton onclick="getNextPage();"> 
      	<INPUT VALUE="尾页" TYPE=button class=cssButton onclick="getLastPage();"> 					
  	</div>
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
    <!--<INPUT VALUE="查看" TYPE=button class=cssButton onclick="ShowPagesDetails()"> -->
    <INPUT VALUE="" type=hidden name=EXESql>      
    <INPUT VALUE="下载" TYPE=button name=DownloadButton class=cssButton onclick="DownloadPic()"> 
    

  			  	</td>
  			</tr>
  			  </table>  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
