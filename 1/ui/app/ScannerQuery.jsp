<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：ScannerQuery.jsp
//程序功能：
//创建日期：2008-08-07
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="ScannerQuery.js"></script>
    <%@include file="ScannerQueryInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">扫描机构</td>
                    <td class="input">
                        <input class="codeNo" name="ManageCom" verify="扫描机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" /> 
                    </td>
                    <td class="title">单证类型</td>
                    <td class="input">
                        <input class="codeNo" name="SubType" value="TB01" ondblclick="return showCodeList('scansubtype',[this,SubTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('scansubtype',[this,SubTypeName],[0,1],null,null,null,1);" /><input class="codename" name="SubTypeName" value="家庭投保书" readonly="readonly" /> 
                    </td>
                    <td class="title">扫描人</td>
                    <td class="input">
                        <input class="common" name="ScanOperator" />
                    </td>
                </tr>
                
                <tr class="common">		          
                    <td class="title">单证号码</td>
                    <td class="input">
                        <input class="common" name="DocCode" />
                    </td>
                    <td class="title">扫描日期起</td>
                    <td class="input">
                        <input class="coolDatePicker" name="StartDate" />
                    </td>
                    <td class="title">扫描日期止</td>
                    <td class="input">
                        <input class="coolDatePicker" name="EndDate" />
                    </td>                       
                </tr>                       
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查询单证信息" onclick="queryScanner()" /> 	
                </td>
            </tr>
        </table>

        <input type="hidden" id="fmtransact" name="fmtransact" />
        <input type="hidden" id="DocId" name="DocId" value="" />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this,divLCPol1);" />
                </td>
                <td class="titleImg">扫描单证</td>
            </tr>
        </table>
        <div align="center" id="divScannerGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanScannerGrid"></span>
                    </td>
                </tr>
            </table>
        </div>
        
        <div id="divScannerPage" style="display: ''" align="center">
            <input type="button" class="cssButton" value="首  页" onclick="getFirstPage();" />
            <input type="button" class="cssButton" value="上一页" onclick="getPreviousPage();" />
            <input type="button" class="cssButton" value="下一页" onclick="getNextPage();" />
            <input type="button" class="cssButton" value="尾  页" onclick="getLastPage();" />
        </div>

        <table class="common">
            <tr class="common">
                <td>
                    <input type="hidden" name="EXESql" />
                    <input class="cssButton" type="button" value="下  载" name="btnDownloadScanner" onclick="DownloadPic()" style="display:'none'" />
                    <input class="cssButton" type="button" value="查  看" name="btnViewScanner" onclick="viewPic();" />
                    <input class="cssButton" type="button" value="删  除" name="btnDelScanner" onclick="delScanner();" />
                </td>
            </tr>
        </table>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
