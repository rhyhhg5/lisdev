//程序名称：
//程序功能：
//创建日期：2015-11-30
//创建人  ：
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询批次信息。
 */
function queryGrpCont()
{
    fm.GS_BatchNo.value = "";
    fm.GS_GrpNo.value = "";
    fm.GS_GrpName.value = "";
    GrpContGrid.clearData();
    GrpSubContGrid.clearData();
    if(!verifyInput2())
    {
        return false;
    }
    var tStrSql = "select lsb.batchno,lsb.grpno,lsg.grpname,lsg.TaxRegistration,lsg.OrgancomCode,lsb.applydate  "
			 + "from lsbatchinfo lsb,lsgrp lsg "
			 + "where 1=1 "
			 + "and lsb.grpno=lsg.grpno "
			 + "and lsb.BatchState='3' "
			 + getWherePart("lsg.GrpNo","GrpNo")
			 + getWherePart("lsg.GrpName","GrpName","like")
			 + getWherePart("lsg.TaxRegistration","TaxRegistration","like")
			 + getWherePart("lsg.OrgancomCode","organCode","like")
			 + getWherePart("lsb.ManageCom","ManageCom","like")			 
			 + getWherePart("lsb.BatchNo","BatchNo")
			 + getWherePart("lsb.ApplyDate","StartDate",">=")
			 + getWherePart("lsb.ApplyDate","EndDate","<=")
			 + " order by lsb.batchno " ;
    turnPage1.pageDivName = "divGrpContGridPage";
    turnPage1.queryModal(tStrSql, GrpContGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有查到导入批次信息！");
        return false;
    }
    
    return true;
}

/**
 * 查询批次待收费。
 */
function queryGrpSubCont()
{
    var tBatchNo = fm.GS_BatchNo.value;
    
    if(tBatchNo == null || tBatchNo == "")
    {
        alert("尚未选择查询批次。");
        return false;
    }

    var tStrSql =" select lcc.prtno ,trim(lcc.prtno)||'801',lcc.appntname ,codename('paymode',lcc.paymode),lcc.prem  "
                + " from lccont lcc ,LSInsuredList lsi,lccontsub lcs "
                + " where 1=1 "
                + " and exists (select 1 from lwmission  "
                + " where processid='0000000003'  "
                + " and activityid='0000001150' "
                + " and Missionprop1=lcc.Proposalcontno  "
                + " and missionprop2=lcc.prtno)  " //签单工作流
                + " and lsi.prtno=lcc.prtno "
                + " and lcs.prtno=lcc.prtno "
                + " and lcs.TaxFlag='2'  "//投保标识为团体 
                + " and lcc.paymode<>'4' "//个单非银行转账 
                + " and not exists (select 1 from ljtempfee where otherno=lcc.prtno and Confflag = '0' and enteraccdate is not null and Othernotype = '4')  "//已缴费
                + " and exists (select 1 from lcpol lcp,lmriskapp lmr where lcp.contno=lcc.contno and lcp.prtno=lcc.prtno and lmr.riskcode=lcp.riskcode and lmr.TaxOptimal='Y' and uwflag in ('4','9')) "//税优险种 
                + getWherePart("lsi.BatchNo","GS_BatchNo")
                + " with ur";

    turnPage2.pageDivName = "divGrpSubContGridPage";
    turnPage2.queryModal(tStrSql, GrpSubContGrid);
    
    if (!turnPage2.strQueryResult)
    {
        alert("没有查到待收费信息！");
        return false;
    }
    fm.querySql.value = tStrSql;
    return true;
}


function initGrpSubConf()
{
    var tRow = GrpContGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = GrpContGrid.getRowData(tRow);
    
    fm.GS_BatchNo.value = tRowDatas[0];
    fm.GS_GrpNo.value = tRowDatas[1];
    fm.GS_GrpName.value = tRowDatas[2];
    GrpSubContGrid.clearData();
}

function downChargeInfo(){
	
	 if(GrpSubContGrid.mulLineCount==0)
     {
         alert("请先查询或批次无待收费信息可下载！");
         return false;
     }
	  fm.action = "TaxChargeInfoSave.jsp";  
	  fm.submit();
}


