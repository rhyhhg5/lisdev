<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolSignSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
  
  	//接收信息
  	// 投保单列表
	LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
	String tGrpPolNo[] = request.getParameterValues("GrpGrid1");
	String tRadio[] = request.getParameterValues("InpGrpGridSel");
	boolean flag = false;
	int grouppolCount = tGrpPolNo.length;
	for (int i = 0; i < grouppolCount; i++)
	{
		if( tGrpPolNo[i] != null && tRadio[i].equals( "1" ))
		{
System.out.println("GrpPolNo:"+i+":"+tGrpPolNo[i]);
		    tLCGrpPolSchema.setGrpPolNo( tGrpPolNo[i] );
		    flag = true;
			break;
		}
	}

  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCGrpPolSchema );
		tVData.add( tG );
		
		// 数据传输
		GrpPolSignUI tGrpPolSignUI   = new GrpPolSignUI();
		tGrpPolSignUI.submitData( tVData, "INSERT" );

		int n = tGrpPolSignUI.mErrors.getErrorCount();
System.out.println("---ErrCount---"+n);
		if( n == 0 ) 
	    {                          
	    	Content = " 签单成功! ";
		   	FlagStr = "Succ";
	    }
	    else
	    {
			String strErr = "";
			for (int i = 0; i < n; i++)
			{
				strErr += (i+1) + ": " + tGrpPolSignUI.mErrors.getError(i).errorMessage + "; ";
			}
			Content = "集体投保单签单失败，原因是: " + strErr;
			FlagStr = "Fail";
		} // end of if
	} // end of if
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
