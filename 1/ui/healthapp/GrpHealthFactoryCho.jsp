<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpHealthFactoryCho.jsp
//程序功能：团单要约资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
 
  	// 投保单列表
	LCFactorySet tLCFactorySet=new LCFactorySet();
  	TransferData tTransferData = new TransferData();
	
	String tGrpPolNo = request.getParameter("GrpPolNo");
	String tPolNo = request.getParameter("PolNo");
	String tContNo = request.getParameter("ContNo");
	String tRiskCode = request.getParameter("RiskCode");
	String tFlag = request.getParameter("flag");
	
	String tOtherNo[] = request.getParameterValues("ImpartGrid2");
	String tFactoryType[] = request.getParameterValues("ImpartGrid1");
	String tFactoryCode[] = request.getParameterValues("ImpartGrid3");
	String tParams[] = request.getParameterValues("ImpartGrid5");
	
	System.out.println("polno:"+tPolNo);
	System.out.println("GrpPolNo:"+tGrpPolNo);
	
	boolean flag = true;
	int ChkCount = 0;
	if(tOtherNo != null)
	{		
		ChkCount = tOtherNo.length;
	}
	System.out.println("count:"+ChkCount);
	if (ChkCount == 0 || tPolNo.equals("") || tGrpPolNo.equals(""))
	{
		Content = "条件录入不完整!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{
		   //准备特约信息
            tTransferData.setNameAndValue("GrpPolNo",tGrpPolNo);
	        tTransferData.setNameAndValue("PolNo",tPolNo) ;
	        tTransferData.setNameAndValue("ContNo",tContNo);
	        tTransferData.setNameAndValue("RiskCode",tRiskCode);
	        
	    	System.out.println("chkcount="+ChkCount);
	    	if (ChkCount > 0)
	    	{
	    		for (int i = 0; i < ChkCount; i++)
			{
				if (!tOtherNo[i].equals("")&&!tFactoryType[i].equals(""))
				{
					//要素资料
		  			LCFactorySchema tLCFactorySchema = new LCFactorySchema();
		  			tLCFactorySchema.setGrpPolNo(tGrpPolNo);	
		            tLCFactorySchema.setPolNo(tPolNo);	
		            tLCFactorySchema.setContNo(tContNo);	      		
					tLCFactorySchema.setOtherNo( tOtherNo[i]);
					tLCFactorySchema.setFactoryType( tFactoryType[i]);
					tLCFactorySchema.setFactoryCode( tFactoryCode[i]);
					tLCFactorySchema.setParams( tParams[i]);
					
	    		    System.out.println("计算编码:"+tFactoryCode[i]);
	    		 	System.out.println("i:"+i);
	    		    System.out.println("Params:"+tParams[i]);	    		    
			    	tLCFactorySet.add( tLCFactorySchema );			    
			   		flag = true;
				}
			}
			    
		}
		else
		{
			Content = "传输数据失败!";
			flag = false;
		}
	}
	
	System.out.println("flag:"+flag);
	System.out.println("tFlag:"+tFlag);
  	if (flag == true)
  	{
		// 准备传输数据 VData
		tTransferData.setNameAndValue("LCFactorySet",tLCFactorySet);
		VData tVData = new VData();
		tVData.add( tTransferData);
		tVData.add( tG );
		
		// 数据传输
		GrpHealthFactorySaveUI tGrpHealthFactorySaveUI   = new GrpHealthFactorySaveUI();
		if (tGrpHealthFactorySaveUI.submitData(tVData,tFlag) == false)
		{
			int n = tGrpHealthFactorySaveUI.mErrors.getErrorCount();
			if(tFlag.equals("save"))
			Content = " 要素保存失败，原因是: " + tGrpHealthFactorySaveUI.mErrors.getError(0).errorMessage;
			if(tFlag.equals("delete"))
			Content = " 要素删除失败，原因是: " + tGrpHealthFactorySaveUI.mErrors.getError(0).errorMessage;			
			FlagStr = "Fail";		
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tGrpHealthFactorySaveUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	if(tFlag.equals("save"))
		    	Content = " 要素保存成功! ";
		    	if(tFlag.equals("delete"))
		    	Content = " 要素删除成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		        if(tFlag.equals("save"))
		    	Content = " 要素保存失败，原因是:" + tError.getFirstError();
		    	if(tFlag.equals("delete"))
		    	Content = " 要素删除失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
		}
	} 
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
