<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = "";
	try
	{
		tContNo = request.getParameter( "ContNo" );
		
		//默认情况下为集体保单
		if( tContNo == null || tContNo.equals( "" ))
			tContNo = "00000000000000000000";
	}
	
	catch( Exception e1 )
	{
		tContNo = "00000000000000000000";
			System.out.println("---contno:"+tContNo);

	}
	System.out.println("---contno:"+tContNo);
        String tDisplay = "";
	try
	{
		tDisplay = request.getParameter("display");
		if(tDisplay == null || tDisplay.equals( "" ))
		 { tDisplay = "0";}
	}
	catch( Exception e )
	{
		tDisplay = "";
	}
%>

<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   
<script>
	var contNo = "<%=tContNo%>";  //个人单的查询条件.
	var comCode = "<%=tG.ComCode%>";
	var tDisplay = "<%=tDisplay%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpHealthFactoryQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="GrpHealthFactoryQueryInit.jsp"%>
  <title>集体保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 集体保单号码 </TD>
          <TD  class= input> <Input class= common name=GrpPolNo >  </TD>
          <TD  class= title>  印刷号码 </TD>
          <TD  class= input>  <Input class= common name=PrtNo ></TD>
          <TD  class= title> 险种编码 </TD>
          <TD  class= input> <Input class="code" name=RiskCode ondblclick="return showCodeList('RiskGrp',[this]);" onkeyup="return showCodeListKey('RiskGrp',[this]);"></TD>
        </TR>
        <TR  class= common>
          <TD  class= title> 管理机构 </TD>
          <TD  class= input> <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);"> </TD>
          <TD  class= title> 代理人组别 </TD>
          <TD  class= input> <Input class="code" name=AgentGroup ondblclick="return showCodeList('AgentGroup',[this]);" onkeyup="return showCodeListKey('AgentGroup',[this]);"> </TD>
          <TD  class= title> 代理人编码 </TD>
          <TD  class= input> <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this]);" onkeyup="return showCodeListKey('AgentCode',[this]);"> </TD>
        </TR>
        
    </table>
          <INPUT VALUE="查询" class = common TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="集体保单明细" class = common TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 集体保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class = common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class = common TYPE=button onclick="getLastPage();"> 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
