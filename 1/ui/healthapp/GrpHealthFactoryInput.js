//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var arrResult;
var mDebug = "0";
var mOperate = "";
var mAction = "";
window.onfocus=myonfocus;
var mSwitch = parent.VD.gVSwitch;
var mShowCustomerDetail = "GROUPPOL";


/*********************************************************************
 *  保存集体投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  

  if( verifyInput() == false ) return false;

	if( mAction == "" )
	{
		showSubmitFrame(mDebug);
		mAction = "INSERT";
		fm.all( 'fmAction' ).value = mAction;
		
		if (fm.all('GrpProposalNo').value != "") {
		  alert("查询结果只能进行修改操作！");
		  mAction = "";
		} else {
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		  fm.submit(); //提交
		}
	}
}

/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "操作成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

		if (fm.all( 'fmAction' ).value == "DELETE")
		{
		  top.opener.easyQueryClick();
		  top.close(); 
		}
	}
	mAction = ""; 
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
	try
	{
		initForm();
	}
	catch( re )
	{
		alert("在GroupPolInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
} 

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
	showDiv(operateButton,"true"); 
	showDiv(inputButton,"false"); 
}
 
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	showDiv( operateButton, "false" ); 
	showDiv( inputButton, "true" ); 
}           

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick()
{
	if( mOperate == 0 )
	{
		mOperate = 1;
		cContNo = fm.all( 'ContNo' ).value;
		showInfo = window.open("./GroupPolQueryMain.jsp?ContNo=" + cContNo);
	}
} 

/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
	var tGrpProposalNo = "";
	tGrpProposalNo = fm.all( 'GrpProposalNo' ).value;
	if( tGrpProposalNo == null || tGrpProposalNo == "" )
		alert( "请先做投保单查询操作，再进行修改!" );
	else
	{
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "UPDATE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}           

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick()
{
	var tGrpProposalNo = "";
	tGrpProposalNo = fm.all( 'GrpProposalNo' ).value;
	if( tGrpProposalNo == null || tGrpProposalNo == "" )
		alert( "请先做投保单查询操作，再进行删除!" );
	else
	{
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}           

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

/*********************************************************************
 *  当点击“进入个人信息”按钮时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoPol()
{
	//下面增加相应的代码
	tGrpPolNo = fm.GrpPolNo.value;
	if( tGrpPolNo == "" )
	{
		alert("您必须先录入集体信息才能进入个人信息部分。");
		return false
	}
	else
	{
		window.open( "./GrpPolQueryMain.jsp?GrpPNo=" + tGrpPolNo );
	}
	
}           

/*********************************************************************
 *  把集体信息放入内存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function putGrpPol()
{
	delGrpPolVar();
	addIntoGrpPol();
}

/*********************************************************************
 *  把集体信息放入加到变量中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addIntoGrpPol()
{
	mSwitch.addVar( "intoPolFlag", "", "GROUPPOL" );
	// body信息
	mSwitch.addVar( "BODY", "", window.document.body.innerHTML );
	// 集体信息
	//由"./AutoCreatLDGrpInit.jsp"自动生成
  try { mSwitch.addVar('GrpNo', '', fm.all('GrpNo').value); } catch(ex) { };
  try { mSwitch.addVar('PrtNo', '', fm.all('PrtNo').value); } catch(ex) { };
  try { mSwitch.addVar('Password', '', fm.all('Password').value); } catch(ex) { };
  try { mSwitch.addVar('GrpName', '', fm.all('GrpName').value); } catch(ex) { };
  try { mSwitch.addVar('GrpAddressCode', '', fm.all('GrpAddressCode').value); } catch(ex) { };
  try { mSwitch.addVar('GrpAddress', '', fm.all('GrpAddress').value); } catch(ex) { };
  try { mSwitch.addVar('GrpZipCode', '', fm.all('GrpZipCode').value); } catch(ex) { };
  try { mSwitch.addVar('BusinessType', '', fm.all('BusinessType').value); } catch(ex) { };
  try { mSwitch.addVar('GrpNature', '', fm.all('GrpNature').value); } catch(ex) { };
  try { mSwitch.addVar('Peoples', '', fm.all('Peoples').value); } catch(ex) { };
  try { mSwitch.addVar('RgtMoney', '', fm.all('RgtMoney').value); } catch(ex) { };
  try { mSwitch.addVar('Asset', '', fm.all('Asset').value); } catch(ex) { };
  try { mSwitch.addVar('NetProfitRate', '', fm.all('NetProfitRate').value); } catch(ex) { };
  try { mSwitch.addVar('MainBussiness', '', fm.all('MainBussiness').value); } catch(ex) { };
  try { mSwitch.addVar('Corporation', '', fm.all('Corporation').value); } catch(ex) { };
  try { mSwitch.addVar('ComAera', '', fm.all('ComAera').value); } catch(ex) { };
  try { mSwitch.addVar('LinkMan1', '', fm.all('LinkMan1').value); } catch(ex) { };
  try { mSwitch.addVar('Department1', '', fm.all('Department1').value); } catch(ex) { };
  try { mSwitch.addVar('HeadShip1', '', fm.all('HeadShip1').value); } catch(ex) { };
  try { mSwitch.addVar('Phone1', '', fm.all('Phone1').value); } catch(ex) { };
  try { mSwitch.addVar('E_Mail1', '', fm.all('E_Mail1').value); } catch(ex) { };
  try { mSwitch.addVar('Fax1', '', fm.all('Fax1').value); } catch(ex) { };
  try { mSwitch.addVar('LinkMan2', '', fm.all('LinkMan2').value); } catch(ex) { };
  try { mSwitch.addVar('Department2', '', fm.all('Department2').value); } catch(ex) { };
  try { mSwitch.addVar('HeadShip2', '', fm.all('HeadShip2').value); } catch(ex) { };
  try { mSwitch.addVar('Phone2', '', fm.all('Phone2').value); } catch(ex) { };
  try { mSwitch.addVar('E_Mail2', '', fm.all('E_Mail2').value); } catch(ex) { };
  try { mSwitch.addVar('Fax2', '', fm.all('Fax2').value); } catch(ex) { };
  try { mSwitch.addVar('Fax', '', fm.all('Fax').value); } catch(ex) { };
  try { mSwitch.addVar('Phone', '', fm.all('Phone').value); } catch(ex) { };
  try { mSwitch.addVar('GetFlag', '', fm.all('GetFlag').value); } catch(ex) { };
  try { mSwitch.addVar('Satrap', '', fm.all('Satrap').value); } catch(ex) { };
  try { mSwitch.addVar('EMail', '', fm.all('EMail').value); } catch(ex) { };
  try { mSwitch.addVar('FoundDate', '', fm.all('FoundDate').value); } catch(ex) { };
  try { mSwitch.addVar('BankAccNo', '', fm.all('BankAccNo').value); } catch(ex) { };
  try { mSwitch.addVar('BankCode', '', fm.all('BankCode').value); } catch(ex) { };
  try { mSwitch.addVar('GrpGroupNo', '', fm.all('GrpGroupNo').value); } catch(ex) { };
  try { mSwitch.addVar('State', '', fm.all('State').value); } catch(ex) { };
  try { mSwitch.addVar('BlacklistFlag', '', fm.all('BlacklistFlag').value); } catch(ex) { };

  try { mSwitch.addVar( "ContNo", "", fm.all( 'ContNo' ).value ); } catch(ex) { };
  try { mSwitch.addVar( "GrpProposalNo", "", fm.all( 'GrpProposalNo' ).value ); } catch(ex) { };
  try { mSwitch.addVar( "ManageCom", "", fm.all( 'ManageCom' ).value ); } catch(ex) { };
  try { mSwitch.addVar( "SaleChnl", "", fm.all( 'SaleChnl' ).value ); } catch(ex) { };
  try { mSwitch.addVar( "AgentCom", "", fm.all( 'AgentCom' ).value ); } catch(ex) { };
  try { mSwitch.addVar( "AgentCode", "", fm.all( 'AgentCode' ).value ); } catch(ex) { };
  try { mSwitch.addVar( "AgentGroup", "", fm.all( 'AgentGroup' ).value ); } catch(ex) { };
  try { mSwitch.addVar( "AgentCode1", "", fm.all( 'AgentCode1' ).value ); } catch(ex) { };

  try { mSwitch.addVar( "RiskCode", "", fm.all( 'RiskCode' ).value ); } catch(ex) { };
  try { mSwitch.addVar( "RiskVersion", "", fm.all( 'RiskVersion' ).value ); } catch(ex) { };
  try { mSwitch.addVar( "CValiDate", "", fm.all( 'CValiDate' ).value ); } catch(ex) { };
}

/*********************************************************************
 *  把集体信息从变量中删除
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delGrpPolVar()
{
	mSwitch.deleteVar( "intoPolFlag" );
	// body信息
	mSwitch.deleteVar( "BODY" );
	// 集体信息
	//由"./AutoCreatLDGrpInit.jsp"自动生成
  try { mSwitch.deleteVar('GrpNo'); } catch(ex) { };
  try { mSwitch.deleteVar('Password'); } catch(ex) { };
  try { mSwitch.deleteVar('GrpName'); } catch(ex) { };
  try { mSwitch.deleteVar('GrpAddressCode'); } catch(ex) { };
  try { mSwitch.deleteVar('GrpAddress'); } catch(ex) { };
  try { mSwitch.deleteVar('GrpZipCode'); } catch(ex) { };
  try { mSwitch.deleteVar('BusinessType'); } catch(ex) { };
  try { mSwitch.deleteVar('GrpNature'); } catch(ex) { };
  try { mSwitch.deleteVar('Peoples'); } catch(ex) { };
  try { mSwitch.deleteVar('RgtMoney'); } catch(ex) { };
  try { mSwitch.deleteVar('Asset'); } catch(ex) { };
  try { mSwitch.deleteVar('NetProfitRate'); } catch(ex) { };
  try { mSwitch.deleteVar('MainBussiness'); } catch(ex) { };
  try { mSwitch.deleteVar('Corporation'); } catch(ex) { };
  try { mSwitch.deleteVar('ComAera'); } catch(ex) { };
  try { mSwitch.deleteVar('LinkMan1'); } catch(ex) { };
  try { mSwitch.deleteVar('Department1'); } catch(ex) { };
  try { mSwitch.deleteVar('HeadShip1'); } catch(ex) { };
  try { mSwitch.deleteVar('Phone1'); } catch(ex) { };
  try { mSwitch.deleteVar('E_Mail1'); } catch(ex) { };
  try { mSwitch.deleteVar('Fax1'); } catch(ex) { };
  try { mSwitch.deleteVar('LinkMan2'); } catch(ex) { };
  try { mSwitch.deleteVar('Department2'); } catch(ex) { };
  try { mSwitch.deleteVar('HeadShip2'); } catch(ex) { };
  try { mSwitch.deleteVar('Phone2'); } catch(ex) { };
  try { mSwitch.deleteVar('E_Mail2'); } catch(ex) { };
  try { mSwitch.deleteVar('Fax2'); } catch(ex) { };
  try { mSwitch.deleteVar('Fax'); } catch(ex) { };
  try { mSwitch.deleteVar('Phone'); } catch(ex) { };
  try { mSwitch.deleteVar('GetFlag'); } catch(ex) { };
  try { mSwitch.deleteVar('Satrap'); } catch(ex) { };
  try { mSwitch.deleteVar('EMail'); } catch(ex) { };
  try { mSwitch.deleteVar('FoundDate'); } catch(ex) { };
  try { mSwitch.deleteVar('BankAccNo'); } catch(ex) { };
  try { mSwitch.deleteVar('BankCode'); } catch(ex) { };
  try { mSwitch.deleteVar('GrpGroupNo'); } catch(ex) { };
  try { mSwitch.deleteVar('State'); } catch(ex) { };
  try { mSwitch.deleteVar('BlacklistFlag'); } catch(ex) { };

	mSwitch.deleteVar( "ContNo" );
	mSwitch.deleteVar( "GrpProposalNo" );
	mSwitch.deleteVar( "ManageCom" );
	mSwitch.deleteVar( "SaleChnl" );
	mSwitch.deleteVar( "AgentCom" );
	mSwitch.deleteVar( "AgentCode" );
	mSwitch.deleteVar( "AgentCode1" );
	
	mSwitch.deleteVar( "RiskCode" );
	mSwitch.deleteVar( "RiskVersion" );
	mSwitch.deleteVar( "CValiDate" );

}

/*********************************************************************
 *  Click事件，当双击“投保单位客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt()
{
	if( mOperate == 0 )
	{
		mOperate = 2;
		if (loadFlag == 6) {
		  alert("只能在投保单录入时进行操作！");
		  mOperate = 0;
		  return false;
		}
		showInfo = window.open( "../sys/GroupMain.html" );
	}
}           

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {
   //alert("here:" + arrQueryResult[0][0] + "\n" + mOperate);
	if( arrQueryResult != null ) {
		arrResult = arrQueryResult;
		mOperate =1;
		if( mOperate == 1 )	{		// 查询集体保单	
			fm.all( 'GrpPolNo' ).value = arrQueryResult[0][0];
			
			arrResult = easyExecSql("select * from LCGrpPol where GrpPolNo = '" + arrQueryResult[0][0] + "'", 1, 0);

			if (arrResult == null) {
			  alert("未查到投保单位信息");
			} else {
			   displayLCGrpPol(arrResult[0]);
			}
		}
		
		if( mOperate == 2 )	{		// 投保单位信息
		  arrResult = easyExecSql("select * from LDGrp where GrpNo = '" + arrQueryResult[0][0] + "'", 1, 0);
			
			if (arrResult == null) {
			  alert("未查到投保单位信息");
			} else {
			   displayAppnt(arrResult[0]);
			}
		}
	}
	
	mOperate = 0;		// 恢复初态
	
	if (loadFlag == "6") {
	  try { divGroupPol4.style.display = ""; } catch(e) {}
	}
}

/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt()
{
  //由"./AutoCreatLDGrpInit.jsp"自动生成
  
  try { fm.all('GrpNo').value = arrResult[0][0]; } catch(ex) { };
  try { fm.all('Password').value = arrResult[0][1]; } catch(ex) { };
  try { fm.all('GrpName').value = arrResult[0][2]; } catch(ex) { };
  try { fm.all('GrpAddressCode').value = arrResult[0][3]; } catch(ex) { };
  try { fm.all('GrpAddress').value = arrResult[0][4]; } catch(ex) { };
  try { fm.all('GrpZipCode').value = arrResult[0][5]; } catch(ex) { };
  try { fm.all('BusinessType').value = arrResult[0][6]; } catch(ex) { };
  try { fm.all('GrpNature').value = arrResult[0][7]; } catch(ex) { };
  try { fm.all('Peoples').value = arrResult[0][8]; } catch(ex) { };
  try { fm.all('RgtMoney').value = arrResult[0][9]; } catch(ex) { };
  try { fm.all('Asset').value = arrResult[0][10]; } catch(ex) { };
  try { fm.all('NetProfitRate').value = arrResult[0][11]; } catch(ex) { };
  try { fm.all('MainBussiness').value = arrResult[0][12]; } catch(ex) { };
  try { fm.all('Corporation').value = arrResult[0][13]; } catch(ex) { };
  try { fm.all('ComAera').value = arrResult[0][14]; } catch(ex) { };
  try { fm.all('LinkMan1').value = arrResult[0][15]; } catch(ex) { };
  try { fm.all('Department1').value = arrResult[0][16]; } catch(ex) { };
  try { fm.all('HeadShip1').value = arrResult[0][17]; } catch(ex) { };
  try { fm.all('Phone1').value = arrResult[0][18]; } catch(ex) { };
  try { fm.all('E_Mail1').value = arrResult[0][19]; } catch(ex) { };
  try { fm.all('Fax1').value = arrResult[0][20]; } catch(ex) { };
  try { fm.all('LinkMan2').value = arrResult[0][21]; } catch(ex) { };
  try { fm.all('Department2').value = arrResult[0][22]; } catch(ex) { };
  try { fm.all('HeadShip2').value = arrResult[0][23]; } catch(ex) { };
  try { fm.all('Phone2').value = arrResult[0][24]; } catch(ex) { };
  try { fm.all('E_Mail2').value = arrResult[0][25]; } catch(ex) { };
  try { fm.all('Fax2').value = arrResult[0][26]; } catch(ex) { };
  try { fm.all('Fax').value = arrResult[0][27]; } catch(ex) { };
  try { fm.all('Phone').value = arrResult[0][28]; } catch(ex) { };
  try { fm.all('GetFlag').value = arrResult[0][29]; } catch(ex) { };
  try { fm.all('Satrap').value = arrResult[0][30]; } catch(ex) { };
  try { fm.all('EMail').value = arrResult[0][31]; } catch(ex) { };
  try { fm.all('FoundDate').value = arrResult[0][32]; } catch(ex) { };
  try { fm.all('BankAccNo').value = arrResult[0][33]; } catch(ex) { };
  try { fm.all('BankCode').value = arrResult[0][34]; } catch(ex) { };
  try { fm.all('GrpGroupNo').value = arrResult[0][35]; } catch(ex) { };
  try { fm.all('State').value = arrResult[0][36]; } catch(ex) { };
  try { fm.all('Remark').value = arrResult[0][37]; } catch(ex) { };
  try { fm.all('BlacklistFlag').value = arrResult[0][38]; } catch(ex) { };
  try { fm.all('Operator').value = arrResult[0][39]; } catch(ex) { };
  try { fm.all('MakeDate').value = arrResult[0][40]; } catch(ex) { };
  try { fm.all('MakeTime').value = arrResult[0][41]; } catch(ex) { };
  try { fm.all('ModifyDate').value = arrResult[0][42]; } catch(ex) { };
  try { fm.all('ModifyTime').value = arrResult[0][43]; } catch(ex) { };
  try { fm.all('FIELDNUM').value = arrResult[0][44]; } catch(ex) { };
  try { fm.all('PK').value = arrResult[0][45]; } catch(ex) { };
  try { fm.all('fDate').value = arrResult[0][46]; } catch(ex) { };
  try { fm.all('mErrors').value = arrResult[0][47]; } catch(ex) { };
	showCodeName();
}

function displayLCGrpPol() {
	
  //由"./AutoCreatLCGrpPolInit.jsp"自动生成
  try { fm.all('GrpPolNo').value = arrResult[0][0]; } catch(ex) { };
  try { fm.all('ContNo').value = arrResult[0][1]; } catch(ex) { };
  try { fm.all('GrpProposalNo').value = arrResult[0][2]; } catch(ex) { };
  try { fm.all('PrtNo').value = arrResult[0][3]; } catch(ex) { };
  try { fm.all('KindCode').value = arrResult[0][4]; } catch(ex) { };
  try { fm.all('RiskCode').value = arrResult[0][5]; } catch(ex) { };
  try { fm.all('RiskVersion').value = arrResult[0][6]; } catch(ex) { };
  try { fm.all('SignCom').value = arrResult[0][7]; } catch(ex) { };
  try { fm.all('ManageCom').value = arrResult[0][8]; } catch(ex) { };
  try { fm.all('AgentCom').value = arrResult[0][9]; } catch(ex) { };
  try { fm.all('AgentType').value = arrResult[0][10]; } catch(ex) { };
  try { fm.all('SaleChnl').value = arrResult[0][11]; } catch(ex) { };
  try { fm.all('Password').value = arrResult[0][12]; } catch(ex) { };
  try { fm.all('GrpNo').value = arrResult[0][13]; } catch(ex) { };
  try { fm.all('Password2').value = arrResult[0][14]; } catch(ex) { };
  try { fm.all('GrpName').value = arrResult[0][15]; } catch(ex) { };
  try { fm.all('GrpAddressCode').value = arrResult[0][16]; } catch(ex) { };
  try { fm.all('GrpAddress').value = arrResult[0][17]; } catch(ex) { };
  try { fm.all('GrpZipCode').value = arrResult[0][18]; } catch(ex) { };
  try { fm.all('BusinessType').value = arrResult[0][19]; } catch(ex) { };
  try { fm.all('GrpNature').value = arrResult[0][20]; } catch(ex) { };
  try { fm.all('Peoples2').value = arrResult[0][21]; } catch(ex) { };
  try { fm.all('RgtMoney').value = arrResult[0][22]; } catch(ex) { };
  try { fm.all('Asset').value = arrResult[0][23]; } catch(ex) { };
  try { fm.all('NetProfitRate').value = arrResult[0][24]; } catch(ex) { };
  try { fm.all('MainBussiness').value = arrResult[0][25]; } catch(ex) { };
  try { fm.all('Corporation').value = arrResult[0][26]; } catch(ex) { };
  try { fm.all('ComAera').value = arrResult[0][27]; } catch(ex) { };
  try { fm.all('LinkMan1').value = arrResult[0][28]; } catch(ex) { };
  try { fm.all('Department1').value = arrResult[0][29]; } catch(ex) { };
  try { fm.all('HeadShip1').value = arrResult[0][30]; } catch(ex) { };
  try { fm.all('Phone1').value = arrResult[0][31]; } catch(ex) { };
  try { fm.all('E_Mail1').value = arrResult[0][32]; } catch(ex) { };
  try { fm.all('Fax1').value = arrResult[0][33]; } catch(ex) { };
  try { fm.all('LinkMan2').value = arrResult[0][34]; } catch(ex) { };
  try { fm.all('Department2').value = arrResult[0][35]; } catch(ex) { };
  try { fm.all('HeadShip2').value = arrResult[0][36]; } catch(ex) { };
  try { fm.all('Phone2').value = arrResult[0][37]; } catch(ex) { };
  try { fm.all('E_Mail2').value = arrResult[0][38]; } catch(ex) { };
  try { fm.all('Fax2').value = arrResult[0][39]; } catch(ex) { };
  try { fm.all('Fax').value = arrResult[0][40]; } catch(ex) { };
  try { fm.all('Phone').value = arrResult[0][41]; } catch(ex) { };
  try { fm.all('GetFlag').value = arrResult[0][42]; } catch(ex) { };
  try { fm.all('Satrap').value = arrResult[0][43]; } catch(ex) { };
  try { fm.all('EMail').value = arrResult[0][44]; } catch(ex) { };
  try { fm.all('FoundDate').value = arrResult[0][45]; } catch(ex) { };
  try { fm.all('BankAccNo').value = arrResult[0][46]; } catch(ex) { };
  try { fm.all('BankCode').value = arrResult[0][47]; } catch(ex) { };
  try { fm.all('GrpGroupNo').value = arrResult[0][48]; } catch(ex) { };
  try { fm.all('PayIntv').value = arrResult[0][49]; } catch(ex) { };
  try { fm.all('PayMode').value = arrResult[0][50]; } catch(ex) { };
  try { fm.all('CValiDate').value = arrResult[0][51]; } catch(ex) { };
  try { fm.all('GetPolDate').value = arrResult[0][52]; } catch(ex) { };
  try { fm.all('SignDate').value = arrResult[0][53]; } catch(ex) { };
  try { fm.all('FirstPayDate').value = arrResult[0][54]; } catch(ex) { };
  try { fm.all('PayEndDate').value = arrResult[0][55]; } catch(ex) { };
  try { fm.all('PaytoDate').value = arrResult[0][56]; } catch(ex) { };
  try { fm.all('RegetDate').value = arrResult[0][57]; } catch(ex) { };
  try { fm.all('Peoples').value = arrResult[0][58]; } catch(ex) { };
  try { fm.all('Mult').value = arrResult[0][59]; } catch(ex) { };
  try { fm.all('Prem').value = arrResult[0][60]; } catch(ex) { };
  try { fm.all('Amnt').value = arrResult[0][61]; } catch(ex) { };
  try { fm.all('SumPrem').value = arrResult[0][62]; } catch(ex) { };
  try { fm.all('SumPay').value = arrResult[0][63]; } catch(ex) { };
  try { fm.all('Dif').value = arrResult[0][64]; } catch(ex) { };
  try { fm.all('SSFlag').value = arrResult[0][65]; } catch(ex) { };
  try { fm.all('PeakLine').value = arrResult[0][66]; } catch(ex) { };
  try { fm.all('GetLimit').value = arrResult[0][67]; } catch(ex) { };
  try { fm.all('GetRate').value = arrResult[0][68]; } catch(ex) { };
  try { fm.all('MaxMedFee').value = arrResult[0][69]; } catch(ex) { };
  try { fm.all('ExpPeoples').value = arrResult[0][70]; } catch(ex) { };
  try { fm.all('ExpPremium').value = arrResult[0][71]; } catch(ex) { };
  try { fm.all('ExpAmnt').value = arrResult[0][72]; } catch(ex) { };
  try { fm.all('DisputedFlag').value = arrResult[0][73]; } catch(ex) { };
  try { fm.all('BonusRate').value = arrResult[0][74]; } catch(ex) { };
  try { fm.all('Lang').value = arrResult[0][75]; } catch(ex) { };
  try { fm.all('Currency').value = arrResult[0][76]; } catch(ex) { };
  try { fm.all('State').value = arrResult[0][77]; } catch(ex) { };
  try { fm.all('LostTimes').value = arrResult[0][78]; } catch(ex) { };
  try { fm.all('AppFlag').value = arrResult[0][79]; } catch(ex) { };
  try { fm.all('ApproveCode').value = arrResult[0][80]; } catch(ex) { };
  try { fm.all('ApproveDate').value = arrResult[0][81]; } catch(ex) { };
  try { fm.all('UWOperator').value = arrResult[0][82]; } catch(ex) { };
  try { fm.all('AgentCode').value = arrResult[0][83]; } catch(ex) { };
  try { fm.all('AgentGroup').value = arrResult[0][84]; } catch(ex) { };
  try { fm.all('AgentCode1').value = arrResult[0][85]; } catch(ex) { };
  try { fm.all('Remark').value = arrResult[0][86]; } catch(ex) { };
  try { fm.all('UWFlag').value = arrResult[0][87]; } catch(ex) { };
  try { fm.all('OutPayFlag').value = arrResult[0][88]; } catch(ex) { };
  try { fm.all('ApproveFlag').value = arrResult[0][89]; } catch(ex) { };
  try { fm.all('EmployeeRate').value = arrResult[0][90]; } catch(ex) { };
  try { fm.all('FamilyRate').value = arrResult[0][91]; } catch(ex) { };
  try { fm.all('Operator').value = arrResult[0][92]; } catch(ex) { };
  try { fm.all('MakeDate').value = arrResult[0][93]; } catch(ex) { };
  try { fm.all('MakeTime').value = arrResult[0][94]; } catch(ex) { };
  try { fm.all('ModifyDate').value = arrResult[0][95]; } catch(ex) { };
  try { fm.all('ModifyTime').value = arrResult[0][96]; } catch(ex) { };
  try { fm.all('FIELDNUM').value = arrResult[0][97]; } catch(ex) { };
  try { fm.all('PK').value = arrResult[0][98]; } catch(ex) { };
  try { fm.all('fDate').value = arrResult[0][99]; } catch(ex) { };
  try { fm.all('mErrors').value = arrResult[0][100]; } catch(ex) { };
  try { fm.all('GrpSpec').value = arrResult[0][101]; } catch(ex) { };
  showCodeName();
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


/*********************************************************************
 *  Click事件，当点击“关联暂交费信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showFee()
{
	cPolNo = fm.GrpProposalNo.value;
	if( cPolNo == "" )
	{
		alert( "您必须先查询投保单才能进入暂交费信息部分。" );
		return false
	}
	
	showInfo = window.open( "./ProposalFee.jsp?PolNo=" + cPolNo + "&polType=GROUP" );
}  

function returnParent()
{
	top.close();
}