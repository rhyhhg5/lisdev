<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2004-08-30
//创建人  ：sunxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>

<script>
  var loadFlag = "<%=request.getParameter("loadFlag")%>";
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="GrpPolInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpPolInit.jsp"%>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
</head>
<body  onload="initForm();" >
  <form action="./GroupPolSave.jsp" method=post name=fm target="fraSubmit">
     <!-- 合同信息部分 GroupPolSave.jsp-->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol1);">
    		</td>
    		
    		<td class= titleImg>
    			 集体保单信息 <!--<INPUT class=common VALUE="关联暂交费信息" TYPE=button onclick="showFee();">-->
    		</td>
    		
    	</tr>
    </table>
    <Div  id= "divGroupPol1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            总单/合同号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ContNo  >
          </TD>
          <TD  class= title>
            集体保单号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GrpPolNo >
          </TD>
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <!--<Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">-->
            <Input class="code" name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
          </TD>
          <TD  class= title>
            销售渠道
          </TD>
          <TD  class= input>
            <Input class="code" name=SaleChnl ondblclick="return showCodeList('SaleChnl',[this]);" onkeyup="return showCodeListKey('SaleChnl',[this]);">
          </TD>
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCom ondblclick="return showCodeList('AgentCom',[this]);" onkeyup="return showCodeListKey('AgentCom',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode verify="代理人编码|notnull&code:AgentCode" ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');" onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');">
          </TD>
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AgentGroup verify="代理人组别|notnull&len<=12" >
          </TD>          
          <TD  class= title>
            联合代理人代码
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCode1 >
          </TD>
        </TR>
      </table>
    </Div>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol2);">
    		</td>
    		<td class= titleImg>
    			 投保单位资料（客户号 <Input class= common name=GrpNo > <INPUT id="butGrpNoQuery" class=common VALUE="查询" TYPE=button onclick="showAppnt();"> ）
    		</td>
    	</tr>
    </table>
    
    <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input class= common name=GrpName verify="单位名称|notnull&len<=60">
          </TD>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input>
            <Input class= common name=GrpAddress verify="单位地址|len<=80">
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpZipCode verify="邮政编码|zipcode">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            单位性质
          </TD>
          <TD  class= input>
            <Input class= common name=GrpNature verify="单位性质|len<=10">
          </TD>
          <TD  class= title>
            行业类别
          </TD>
          <TD  class= input>
            <Input class= common name=BusinessType verify="行业类别|len<=20">
          </TD>
          <TD  class= title>
            单位总人数
          </TD>
          <TD  class= input>
            <Input class= common name=Peoples verify="单位总人数|int">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            注册资本金
          </TD>
          <TD  class= input>
            <Input class= common name=RgtMoney verify="注册资本金|num&len<=17">
          </TD>
          <TD  class= title>
            资产总额
          </TD>
          <TD  class= input>
            <Input class= common name=Asset verify="资产总额|num&len<=17">
          </TD>
          <TD  class= title>
            净资产收益率
          </TD>
          <TD  class= input>
            <Input class= common name=NetProfitRate verify="净资产收益率|num&len<=17">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            主营业务
          </TD>
          <TD  class= input>
            <Input class= common name=MainBussiness verify="主营业务|len<=60">
          </TD>
          <TD  class= title>
            单位法人代表
          </TD>
          <TD  class= input>
            <Input class= common name=Corporation verify="单位法人代表|len<=20">
          </TD>
          <TD  class= title>
            机构分布区域
          </TD>
          <TD  class= input>
            <Input class= common name=ComAera verify="机构分布区域|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            保险联系人一
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=LinkMan1 verify="保险联系人一姓名|len<=10">
          </TD>
          <TD  class= title>
            部门
          </TD>
          <TD  class= input>
            <Input class= common name=Department1 verify="保险联系人一部门|len<=30">
          </TD>
          <TD  class= title>
            职务
          </TD>
          <TD  class= input>
            <Input class= common name=HeadShip1 verify="保险联系人一职务|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone1 verify="保险联系人一联系电话|len<=30">
          </TD>
          <TD  class= title>
            E-MAIL
          </TD>
          <TD  class= input>
            <Input class= common name=E_Mail1 verify="保险联系人一E-MAIL|len<=20">
          </TD>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax1 verify="保险联系人一传真|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            保险联系人二
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=LinkMan2 verify="保险联系人二姓名|len<=10">
          </TD>
          <TD  class= title>
            部门
          </TD>
          <TD  class= input>
            <Input class= common name=Department2 verify="保险联系人二部门|len<=30">
          </TD>
          <TD  class= title>
            职务
          </TD>
          <TD  class= input>
            <Input class= common name=HeadShip2 verify="保险联系人二职务|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone2 verify="保险联系人二联系电话|len<=30">
          </TD>
          <TD  class= title>
            E-MAIL
          </TD>
          <TD  class= input>
            <Input class= common name=E_Mail2 verify="保险联系人二E-MAIL|len<=20">
          </TD>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax2 verify="保险联系人二传真|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            付款方式
          </TD>
          <TD  class= input>
            <Input class="code" name=GetFlag verify="付款方式|code:PayMode" ondblclick="return showCodeList('PayMode',[this]);" onkeyup="return showCodeListKey('PayMode',[this]);">
          </TD>
          <TD  class= title>
            开户银行
          </TD>
          <TD  class= input>
            <Input class= common name=BankCode verify="开户银行|len<=24">
          </TD>
          <TD  class= title>
            帐号
          </TD>
          <TD  class= input>
            <Input class= common name=BankAccNo verify="帐号|len<=40">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            币种
          </TD>
          <TD  class= input>
            <Input class= common name=Currency verify="币种|len<=2">
          </TD>
          <TD  class= title>
            雇员自付比例
          </TD>
          <TD  class= input>
            <Input class= common name=EmployeeRate verify="雇员自付比例|num&len<=5">
          </TD>
          <TD  class= title>
            家属自付比例
          </TD>
          <TD  class= input>
            <Input class= common name=FamilyRate verify="家属自付比例|num&len<=80">
          </TD>       
        </TR>      

      </table>
    </Div>
    
    <Div  id= "divGroupPol21" style= "display: 'none'">
    <table>
        <TR  class= common>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone >
          </TD>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax >
          </TD>
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
        </TR> 
    </table>
    </Div>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol3);">
    		</td>
    		<td class= titleImg>
    			 险种信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divGroupPol3" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskCode ondblclick="return showCodeList('RiskGrp',[this]);">
          </TD>
          <TD  class= title>
            保单生效日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=CValiDate >
          </TD>
          <TD  class= title>
            交费间隔
          </TD>
          <TD  class= input>
            <Input class="code" name=PayIntv ondblclick="return showCodeList('PayIntv',[this]);">
          </TD>
        </TR>
        <TR  class= common>          
          <TD  class= title>
            封顶线
          </TD>
          <TD  class= input>
            <Input class= common name=PeakLine >
          </TD>
          <TD  class= title>
            起付限
          </TD>
          <TD  class= input>
            <Input class= common name=GetLimit >
          </TD>
          <TD  class= title>
            赔付比例
          </TD>
          <TD  class= input>
            <Input class= common name=GetRate >
          </TD>
        </TR>
        <TR  class= common>          
          <TD  class= title>
            医疗费用限额
          </TD>
          <TD  class= input>
            <Input class= common name=MaxMedFee >
          </TD>
          <TD  class= title>
            分红比率
          </TD>
          <TD  class= input>
            <Input class= common name=BonusRate >
          </TD>
        </TR>
      </table>
	  <table>
	    <TR  class= common> 
	      <TD  class= title> 特别约定及备注 </TD>
	    </TR>
	    <TR  class= common>
	      <TD  class= title>
	      <textarea name="GrpSpec" cols="120" rows="3" class="common" >
	      </textarea></TD>
	    </TR>
	  </table>       
    </Div>
    
    <Div  id= "divGroupPol4" style= "display: 'none'">
      <table  class= common>
      <TR  class= common>
        <TD  class= title>
          投保总人数
        </TD>
        <TD  class= input>
          <Input class=ReadOnly ReadOnly name=Peoples2 >
        </TD>
        <TD  class= title>
          总保费
        </TD>
        <TD  class= input>
          <Input class=ReadOnly ReadOnly name=Prem >
        </TD>
        <TD  class= title>
          总保额
        </TD>
        <TD  class= input>
          <Input class=ReadOnly ReadOnly name=Amnt >        </TD>
      </TR>
      </table>
    </Div>
    
      <INPUT VALUE="个人明细信息" class = common TYPE=button onclick="intoPol()"> 
      <!--INPUT VALUE="修 改" TYPE=button onclick="updateClick()"--> 
      <!--INPUT VALUE="删 除" TYPE=button onclick="deleteClick()"--> 
      <INPUT VALUE="返 回" class = common TYPE=button onclick="returnParent()"> 
		<input type=hidden id="fmAction" name="fmAction">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
