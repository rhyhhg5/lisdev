<%
//程序名称：GrpHealthFactoryQueryInit.jsp
//程序功能：
//创建日期：2004-08-30
//创建人  ：sunxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('ContNo').value = contNo;
  }
  catch(ex)
  {
    alert("在PolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在PolQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initGrpPolGrid();
    //initImpartGrid();
  }
  catch(re)
  {
    alert("PolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var GrpPolGrid; 
// 保单信息列表的初始化
function initGrpPolGrid()
  {    
   
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="集体保单号";         		//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[2]=new Array();
      iArray[2][0]="印刷号码";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][3]=0; 
      
      
      iArray[3]=new Array();
      iArray[3][0]="投保人编码";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="投保人姓名";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="险种编码";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=2; 
      iArray[5][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      iArray[5][5]="4";              	                //引用代码对应第几列，'|'为分割符
      iArray[5][9]="险种编码|code:RiskCode&NOTNULL";
      iArray[5][18]=250;
      iArray[5][19]= 0 ;
      

      iArray[6]=new Array();
      iArray[6][0]="出单机构";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=2; 
      iArray[6][4]="station";              	        //是否引用代码:null||""为不引用
      iArray[6][5]="6";              	                //引用代码对应第几列，'|'为分割符
      iArray[6][9]="出单机构|code:station&NOTNULL";
      iArray[6][18]=250;
      iArray[6][19]= 0 ;

        
      iArray[7]=new Array();
      iArray[7][0]="保险金额";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
      
  

      GrpPolGrid = new MulLineEnter( "fm" , "GrpPolGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPolGrid.mulLineCount = 0;   
      GrpPolGrid.displayTitle = 1;
      GrpPolGrid.locked = 1;
      GrpPolGrid.canSel = 1;
      GrpPolGrid.loadMulLine(iArray);  
      GrpPolGrid. selBoxEventFuncName = "easyQueryAddClick";
     
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}



// 要约信息列表的初始化
function initImpartGrid(tFactoryType) {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="要素类别";    	        //列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;                       //是否允许输入,1表示允许，0表示不允许 2表示代码选择
      iArray[1][10] = "HealthFactoryType";             			
      iArray[1][11] = tFactoryType;
      iArray[1][12] = "1|6";
      iArray[1][13] = "0|2";
      iArray[1][19] = 1;	
      
      iArray[2]=new Array();
      iArray[2][0]="要素目标编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="HealthFactoryNo";
      iArray[2][9]="要素目标编码|len<=6";
      iArray[2][15]="RiskCode";
      iArray[2][17]="6";
      iArray[2][18]=700;
      
      
      iArray[3]=new Array();
      iArray[3][0]="要素计算编码";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][4]="HealthFactory";
      iArray[3][5]="3|4|5";
      iArray[3][6]="0|1|2";
      iArray[3][9]="要素计算编码|len<=4";
      iArray[3][15]="RiskCode";
      iArray[3][17]="6";
      iArray[3][18]=700;

      iArray[4]=new Array();
      iArray[4][0]="要素内容";         		//列名
      iArray[4][1]="300px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="要素值";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=150;            			//列最大值
      iArray[5][3]=1;                           //是否允许输入,1表示允许，0表示不允许
      iArray[5][7]="showdyDiv";              			
      
      iArray[6]=new Array();
      iArray[6][0]="险种编码";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=150;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许  
      iArray[6][4]="RiskCode";

      ImpartGrid = new MulLineEnter( "fm" , "ImpartGrid" ); 
      //这些属性必须在loadMulLine前
      ImpartGrid.mulLineCount = 1;   
      ImpartGrid.displayTitle = 1;
      //ImpartGrid.tableWidth   ="500px";
      ImpartGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ImpartGrid.setRowColData(1,1,"asdf");
    }
    catch(ex) {
      alert(ex);
    }
}



</script>