

var showInfo;
var mDebug="0";

var mSwitch = parent.VD.gVSwitch;


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}




function getQueryDetail()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = PolGrid.getRowColData(tSel - 1,1);		
	    if (cPolNo == "")
		    return;
	
		window.open("./GrpPolQuery.jsp");  
		
	}
}



//保全查询
function PerEdorQueryClick()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = PolGrid.getRowColData(tSel - 1,1);				

		
		if (cPolNo == "")
		    return;
//		  var cRiskCode = PolGrid.getRowColData(tSel - 1,3);
//		  var cInsuredName = PolGrid.getRowColData(tSel - 1,4);
//		  var cAppntName = PolGrid.getRowColData(tSel - 1,5);
		  window.open("../sys/AllGBqQueryMain.jsp?PolNo=" + cPolNo + "&flag=0");										
	}	
}


// 保单明细查询
function PolClick()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cPolNo = PolGrid.getRowColData(tSel - 1,2);				
		
		if (cPolNo == "")
		    return;		    
 
		    window.open("../sys/PolDetailQueryMain.jsp?PolNo=" + cPolNo);	
	}
}




var turnPage = new turnPageClass(); 
function easyQueryClick()
{
	
	var strSQL = "";
		
	strSQL = "select grppolno,PrtNo,PolNo,InsuredName,RiskCode,ManageCom  from LCPol where "
	           + " prtno ='" + tPrtNo + "'"
               + getWherePart( 'PolNo' )
               + getWherePart( 'GrpPolNo' )
               + getWherePart( 'AppntName' )
               + getWherePart( 'InsuredName' )
               + getWherePart( 'InsuredSex' )
               + getWherePart( 'InsuredBirthday' )
               + " order by PolNo ";			 
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}


function GrpPerPolDefine(parm1,parm2)
{
	// 初始化表格
	// 初始化表格
	var str = "";
	if(fm.all(parm1).all('InpPolGridChk').value=='1' )
	{
	var tRiskType = fm.all(parm1).all("PolGrid5").value;	
    var tFactoryType=initFactoryType(tRiskType);
	initImpartGrid(tFactoryType);
	
	divLCImpart1.style.display= "";
	
   //var GrpPolNo = PolGrid.getRowColData(PolGrid.getSelNo()-1, 1);
   fm.GrpPolNoHide.value = fm.all(parm1).all("PolGrid1").value ;
   fm.RiskCode.value = fm.all(parm1).all("PolGrid5").value ;
   
   fm.PolNoHide.value = fm.all(parm1).all("PolGrid3").value ;
     var strSQL = "select a.FactoryType,a.OtherNo,a.FactoryCode||a.FactorySubCode,a.CalRemark,a.Params,a.FactoryType||"+fm.RiskCode.value+" from LCFactory a where   "
              +"  GrpPolNo='" +fm.GrpPolNoHide.value+ "'"
              +"  and PolNo='" +fm.PolNoHide.value+ "'"
              +"  order by a.FactoryType, a.OtherNo,a.FactoryCode,a.FactorySubCode ";

   turnPage = new turnPageClass();
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (turnPage.strQueryResult) {
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = ImpartGrid;            
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;   
  //设置查询起始位置
  turnPage.pageIndex = 0;   
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES); 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}
}
else
{
}

	
}
function GrpPerPolDefineSave()
{
	// 初始化表格
	var str = "";
   fm.all('ContNo').value = "00000000000000000000";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  fm.all('flag').value = "save";
   fm.submit(); //提交
}

function GrpPolDefineDelete()
{
	// 初始化表格
	var str = "";

   fm.all('ContNo').value = "00000000000000000000";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
    fm.all('flag').value = "delete";
   fm.submit(); //提交
}


function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showInfo.close();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
	var showStr="操作成功";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  	showInfo.close();
  	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


function initFactoryType(tRiskCode)
{
	// 书写SQL语句
	var k=0;
	var strSQL = "";
	strSQL = "select distinct a.FactoryType,b.FactoryTypeName,a.FactoryType||"+tRiskCode+" from LMFactoryMode a ,LMFactoryType b  where 1=1 "
		   + " and a.FactoryType= b.FactoryType "
		   + " and (RiskCode = '"+tRiskCode+"' or RiskCode ='000000' )";
    var str  = easyQueryVer3(strSQL, 1, 0, 1); 
    return str;
}

function getQueryDetail1()
{
	var arrReturn = new Array();
	window.open("../app/AppGrpPolQuery.jsp");

}

function getQueryResult()
{
	var arrSelected = null;
	arrSelected = new Array();
	arrSelected[0]= new Array();
	//设置需要返回的数组
	//alert("4:"+arrGrid);
	arrSelected[0][0] =  fm.GrpPolNoHide.value; 
	return arrSelected;
}