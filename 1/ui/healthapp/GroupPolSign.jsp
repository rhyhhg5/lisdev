<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="GroupPolSign.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GroupPolSignInit.jsp"%>
  <title>集体签单 </title>
</head>
<body  onload="initForm();" >
  <form action="./GroupPolSignSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 集体信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询集体条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            集体投保单号
          </TD>
          <TD  class= input>
            <Input class= common name=GrpProposalNo >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            投保单位客户号
          </TD>
          <TD  class= input>
            <Input class= common name=GrpNo >
          </TD>
          <TD  class= title>
            投保单位名称
          </TD>
          <TD  class= input>
            <Input class= common name=GrpName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode verify="代理人编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');" onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');">
          </TD>
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AgentGroup >
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button class= common onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrp1);">
    		</td>
    		<td class= titleImg>
    			 集体信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCGrp1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button class= common onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button class= common onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button class= common onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button class= common  onclick="getLastPage();"> 					
  	</div>
  	<p>
      <INPUT VALUE="签发保单" TYPE=button class= common  onclick="signGrpPol();"> 
  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
