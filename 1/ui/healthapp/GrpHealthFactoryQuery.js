//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击集体保单明细按钮。" );
	else
	{
		var cGrpPrtNo = GrpPolGrid.getRowColData( tSel - 1, 2 );
		try
		{
		  window.open("./GrpHealthFactoryDetailQueryMain.jsp?GrpPrtNo="+ cGrpPrtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");  	
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	var strSQL = "select GrpPolNo,PrtNo,RiskCode,GrpName,Peoples2,Prem from LCGrpPol where 1=1 "
    				 + " and riskcode in(select riskcode from lmriskapp where subriskflag='M')"
    				 + " and AppFlag='0' "
    				 + getWherePart( 'PrtNo' )
    				 + getWherePart( 'GrpPolNo' )
    				 + getWherePart( 'ManageCom','ManageCom','like' )
    				 + getWherePart( 'AgentCode' )
    				 + getWherePart( 'AgentGroup' )
    				 + getWherePart( 'RiskCode' );
    			 	

  strSQL = strSQL + " and ManageCom like '" + comCode + "%%'"
    				      + " Order by grppolno desc";
//alert(strSQL);	

 turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("无满足条件的投保单信息！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = GrpPolGrid;    
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  //设置查询起始位置
  turnPage.pageIndex = 0;  
   //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}


