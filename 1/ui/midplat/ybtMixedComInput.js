//               该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function DoSave()
{
	var tState=fm.all('State').value;
	if(tState!=null && tState!=''){
		alert("查询出来的记录只能进行修改和删除操作！");
		return false;
	}
	fm.fmAction.value = "INSERT";
	if(!verifyInput())
	{
	 return false;
	}
	if(!beforeSubmit())
	{
	 return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交

}

function queryGrpHistoryDetail()
{
    var tRow = SetGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = SetGrid.getRowData(tRow);
    fm.all('ManageCom').value=tRowDatas[0];
    fm.all('ManageComName').value=tRowDatas[1];
    fm.all('BankType2').value=tRowDatas[2];
    fm.all('BankType2Name').value=tRowDatas[3];
    fm.all('AgentCom').value=tRowDatas[4];
    fm.all('AgentComName').value=tRowDatas[5];
    fm.all('Crs_SaleChnl').value=tRowDatas[6];
    fm.all('Crs_BussType').value=tRowDatas[7];
    fm.all('GrpAgentCom').value=tRowDatas[8];
    fm.all('GrpAgentCode').value=tRowDatas[9];
    fm.all('GrpAgentName').value=tRowDatas[10];
    fm.all('GrpAgentIDNo').value=tRowDatas[11];
    fm.all('InvalidDate').value=tRowDatas[12];
    fm.all('State').value=tRowDatas[13];
    
    fm.all('OldBandCode').value=tRowDatas[4];
    fm.all('Crs_BussTypeName').value=tRowDatas[15];
    fm.all('Crs_SaleChnlName').value=tRowDatas[16];
    fm.all('GrpAgentComName').value=tRowDatas[17];
    return true;
}


function DoDel()
{
	var tState=fm.all('State').value;
	if(tState==null && tState==''){
		alert("此记录不是查询出来的记录，请先查询再进行删除操作！");
		return false;
	}
	if(tState=='1'){
		alert("此记录已确定，不能进行删除操作！");
		return false;
	}
	
	fm.fmAction.value = "DELETE";
	if(!verifyInput())
	{
	 return false;
	}
	if(fm.all('OldBandCode').value!=fm.all('AgentCom').value){
		alert("银行机构已经被修改，不能修改该值！");
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交
}

// 查询按钮
function easyQueryClick()
{
	//alert ("start easyQueryClick");
	// 初始化表格
	//if(!verifyInput())
	//{
	 //return false;
	//}
	if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输管理机构！");
  	return false;
  }
	initSetGrid();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select b.managecom,(select name from ldcom where comcode=b.managecom),"
	+"b.banktype,case when b.banktype='01' then '分行' when b.banktype='02' then '支行' when b.banktype='03' then '分理处' else '网点' end,"
	+"a.bandcode,(select name from lacom where agentcom=a.bandcode),"
	+"a.crs_salechnl,a.crs_busstype,a.grpagentcom,a.grpagentcode,a.grpagentname,a.grpagentidno,"
	+"a.invalidate,a.state,case when a.state='0' then '未确认' else '已确认' end,"
	+"(select codename from ldcode where codetype='jcsaletype' and code=a.crs_busstype), "
	+"(select codename from ldcode where codetype='jcsalechnl' and code=a.crs_salechnl),  "
	+"(select under_orgname from lomixcom where grpagentcom=a.grpagentcom) "
	+ " from LOMixBKAttribute a,lacom b "
	+ " where 1=1 and a.bandcode=b.agentcom "
	+ getWherePart( 'b.managecom','ManageCom','like')
	+ getWherePart( 'a.BandCode','AgentCom','like')
	+ getWherePart( 'a.Crs_BussType','Crs_BussType')
	+ getWherePart( 'a.GrpAgentCom','GrpAgentCom')
	+ getWherePart( 'a.InvalidDate','InvalidDate')
	+ getWherePart( 'a.GrpAgentCode','GrpAgentCode')
	+ getWherePart( 'a.GrpAgentName','GrpAgentName')
	+ getWherePart( 'a.GrpAgentIDNo','GrpAgentIDNo')
	+ getWherePart( 'a.Crs_SaleChnl','Crs_SaleChnl');
	

					//查询SQL，返回结果字符串
					turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

					//判断是否查询成功
					if (!turnPage.strQueryResult) {
						alert("没有符合条件的数据，请重新录入查询条件！");
						return false;
					}

					//查询成功则拆分字符串，返回二维数组
					  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
					  turnPage.arrDataCacheSet = arrDataSet;

					//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
					//设置初始化过的MULTILINE对象
					turnPage.pageDisplayGrid = SetGrid;
					//保存SQL语句
					turnPage.strQuerySql = strSQL;

					//设置查询起始位置
					turnPage.pageIndex = 0;

					//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
					tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

					//调用MULTILINE对象显示查询结果
					displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
				}


				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}
function changeGrpAgentCom(){
	if (getWherePart('GrpAgentCom')=='')
	return false;
	var strSQL = "";
	strSQL = "select under_orgname from lomixcom where 1=1 "
	+" and grpagentcom = '"+fm.all('GrpAgentCom').value+"'";
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	if (!strQueryResult)
	{
		alert('不存在该集团代理机构！');
		fm.all('GrpAgentCom').value='';
		fm.all('GrpAgentComName').value='';
		return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	fm.all('GrpAgentComName').value = trim(arr[0][0]);
	return true;
} 
	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoUpdate(){
	var tState=fm.all('State').value;
	if(tState==null && tState==''){
		alert("此记录不是查询出来的记录，请先查询再进行修改操作！");
		return false;
	}
	if(tState=='1'){
		alert("此记录已确定，不能进行修改操作！");
		return false;
	}
	fm.fmAction.value = "UPDATE";
	if(!verifyInput())
	{
	 return false;
	}
	if(!beforeSubmit())
	{
	 return false;
	}
	if(fm.all('OldBandCode').value!=fm.all('AgentCom').value){
		alert("银行机构已经被修改，不能修改该值！");
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

function getAgentCom(cObj,cName)
{
	if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输管理机构！");
  	return false;
  } 
  //var tempstr=fm.all('BankType2').value;
  //alert(tempstr);
if(fm.all('BankType2').value!=null &&fm.all('BankType2').value !="")
{  
var strsql =" 1 and   banktype=#" + fm.all('BankType2').value + "#  and managecom  like #" + fm.all('ManageCom').value + "%#   and branchtype=#3# and branchtype2=#01#" ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,'1',1);
}
else
	{
	var strsql =" 1 and managecom  like #" + fm.all('ManageCom').value + "%#  and branchtype=#3#  and branchtype2=#01# " ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,'1',1);
	
	}
}


	//显示frmSubmit框架，用来调试
	function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}



//--									//提交前的校验、计算
function beforeSubmit(){
	if(fm.all('AgentCom').value==null || fm.all('AgentCom').value==''){
		alert("银行机构不能为空！操作失败！");
		return false;
	}
	if(fm.all('Crs_BussType').value==null || fm.all('Crs_BussType').value==''){
		alert("集团销售业务类型不能为空！操作失败！");
		return false;
	}
	if(fm.all('GrpAgentCom').value==null || fm.all('GrpAgentCom').value==''){
		alert("集团代理机构不能为空！操作失败！");
		return false;
	}
	if(fm.all('InvalidDate').value==null || fm.all('InvalidDate').value==''){
		alert("失效日期不能为空！操作失败！");
		return false;
	}
	if(fm.all('GrpAgentCode').value==null || fm.all('GrpAgentCode').value==''){
		alert("集团代理人编码不能为空！操作失败！");
		return false;
	}
	if(fm.all('GrpAgentName').value==null || fm.all('GrpAgentName').value==''){
		alert("集团代理人姓名不能为空！操作失败！");
		return false;
	}
	if(fm.all('GrpAgentIDNo').value==null || fm.all('GrpAgentIDNo').value==''){
		alert("集团代理人身份证不能为空！操作失败！");
		return false;
	}
	if(fm.all('Crs_SaleChnl').value==null || fm.all('Crs_SaleChnl').value==''){
		alert("集团交叉销售渠道不能为空！操作失败！");
		return false;
	}
	
	return true;
}

function showDiv(cDiv,cShow){
		if (cShow=="true"){
				cDiv.style.display="";}else{
						cDiv.style.display="none";
				}
}

									
//显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value = "";
        fm.all('Crs_SaleChnlName').value = "";
        fm.all('Crs_BussType').value = "";
        fm.all('Crs_BussTypeName').value = "";
        fm.all('GrpAgentCom').value = "";
        fm.all('GrpAgentComName').value = "";
        fm.all('GrpAgentCode').value = "";
        fm.all('GrpAgentName').value = "";
        fm.all('GrpAgentIDNo').value = "";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}
//双击对方业务员框要显示的页面
function otherSalesInfo(){
	//alert("双击对方业务员代码");
	if(fm.ManageCom.value==null||fm.ManageCom.value==""){
		alert("请先选择管理机构");
	}else{
		window.open("../sys/MixedSalesAgentMain.jsp?ManageCom="+fm.ManageCom.value);
	}
}
//对方业务员页面查询完“返回”按钮，接收数据
function afterQueryMIX(arrResult){
	if(arrResult!=null){
		var arr = arrResult.split("#");
		fm.GrpAgentCom.value = arr[0]; //对方机构代码
		//fm.GrpAgentComName.value = arr[1]; //对方机构名称
		fm.GrpAgentCode.value = arr[2]; //对方业务员代码
		fm.GrpAgentName.value = arr[3]; //对方业务员姓名
		fm.GrpAgentIDNo.value = arr[4]; //对方业务员身份证
		
	}else{
		alert("返回过程出现异常，请重新操作");
	}	
}
			





