var turnPage = new turnPageClass();
// 查询按钮
function easyQueryClick() {    
	// 初始化表格
	initPremQueryGrid3();
	
	// 书写SQL语句
	var strSQL = "";
	var strtSQL = "";
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankCode = fm.all('BankCode').value;
	var BankBranch = fm.all('BankBranch').value;
	var BankNode = fm.all('BankNode').value;
		
	if ((null==StartTransDate) || (""==StartTransDate) 
		||
		(null==EndTransDate) || (""==EndTransDate)) {
	   alert("请选择查询日期");
	   return;
	}
	if (fm.all('StartTransDate').value > fm.all('EndTransDate').value) {
		alert("查询终止日期不能在查询起始日期之前！");  	
		return false;
	}	
	if((null==BankCode) || (""==BankCode))	{
		alert("请输入银行代码");
		return;
	}
	
	strSQL = "select c.riskcode,d.riskname,sum(c.prem) from lccont a,lktransstatus b,lcpol c,lmrisk d "
			+ "where a.contno = b.polno and c.contno = a.contno and c.riskcode = d.riskcode and a.salechnl = '04' and a.cardflag = '9' and a.appflag = '1' and " 
			+ "b.bankcode = '" + BankCode + "' and b.funcflag = '01' and a.PolApplyDate between '" + StartTransDate + "' and  '"
			+ EndTransDate + "'"
			+ getWherePart("b.bankbranch","BankBranch")
			+ getWherePart("b.banknode","BankNode")
			+ "group by c.riskcode, d.riskname"
			+ " union all "
			+ "select c.riskcode,d.riskname,sum(c.prem) from lbcont a,lktransstatus b,lbpol c,lmrisk d "
			+ "where a.contno = b.polno and c.contno = a.contno and c.riskcode = d.riskcode and a.salechnl = '04' and a.cardflag = '9' and a.appflag = '3' and " 
			+ "b.bankcode = '" + BankCode + "' and b.funcflag = '01' and a.PolApplyDate between '" + StartTransDate + "' and  '"
			+ EndTransDate + "'"
			+ getWherePart("b.bankbranch","BankBranch")
			+ getWherePart("b.banknode","BankNode")
      + "group by c.riskcode, d.riskname with ur";

	var strSql2 = "select sum(A) from (select sum(c.prem) as A from lccont a,lktransstatus b,lcpol c,lmrisk d "
			+ "where a.contno = b.polno and c.contno = a.contno and c.riskcode = d.riskcode and a.salechnl = '04' and a.cardflag = '9' and a.appflag = '1' and " 
			+ "b.bankcode = '" + BankCode + "' and b.funcflag = '01' and a.PolApplyDate between '" + StartTransDate + "' and  '"
			+ EndTransDate + "'"
			+ getWherePart("b.bankbranch","BankBranch")
			+ getWherePart("b.banknode","BankNode")
			+ " union all "
			+ "select sum(c.prem) as A from lbcont a,lktransstatus b,lbpol c,lmrisk d "
			+ "where a.contno = b.polno and c.contno = a.contno and c.riskcode = d.riskcode and a.salechnl = '04' and a.cardflag = '9' and a.appflag = '3' and " 
			+ "b.bankcode = '" + BankCode + "' and b.funcflag = '01' and a.PolApplyDate between '" + StartTransDate + "' and  '"
			+ EndTransDate + "'"
			+ getWherePart("b.bankbranch","BankBranch")
			+ getWherePart("b.banknode","BankNode")
      + " ) t with ur";
	
  var arrResult2 = easyExecSql(strSql2);
  
  fm.SumPrem.value = arrResult2;  
  
  turnPage.queryModal(strSQL,PremQueryGrid3, 0, 0);
  
  //把导出按钮变为可用
  fm.ExportButton.disabled="";
}

//导出成EXCEL表格
function NCExecel()
{
	var strSQL = "";
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankBranch = fm.all('BankBranch').value;
	var BankNode = fm.all('BankNode').value;
	var BankCode = fm.all('BankCode').value;
	
	if(StartTransDate == null ||StartTransDate == ""||EndTransDate == null||EndTransDate == "")
	{
	   alert("请选择查询日期");
	   return;
	}  	
  if (fm.all('StartTransDate').value > fm.all('EndTransDate').value)
  {
  		alert("查询终止日期不能在查询起始日期之前！");  	
  		return false;
  }
	if(BankCode == null ||BankCode == ""||BankCode == null||BankCode == "")
	{
	   alert("请输入银行代码");
	   return;
	}	

   fm.action="./FinanceDailyBalanceTableSave.jsp";
   fm.submit(); //提交
	}

//打印
function BillPrint()
{
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankBranch = fm.all('BankBranch').value;
	var BankNode = fm.all('BankNode').value;
	var BankCode = fm.all('BankCode').value;
	
	if(StartTransDate == null ||StartTransDate == ""||EndTransDate == null||EndTransDate == "")
	{
	   alert("请选择查询日期");
	   return;
	}  	
  if (fm.all('StartTransDate').value > fm.all('EndTransDate').value)
  {
  		alert("查询终止日期不能在查询起始日期之前！");  	
  		return false;
  }
	if(BankCode == null ||BankCode == ""||BankCode == null||BankCode == "")
	{
	   alert("请输入银行代码");
	   return;
	}	
		fm.action="./DailyBalanceTableSave1.jsp";
		fm.target="f1print";
		fm.fmtransact.value="PRINT";
		fm.submit();
}