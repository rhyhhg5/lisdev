<script language="JavaScript">

function initForm() {
	initYBTPolGrid();
}

function initYBTPolGrid() {
	var iArray = new Array();
	
	try {
		iArray[0]=new Array();
		
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="银行编码";         	//列名
		iArray[1][1]="60px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="银行名称";         	//列名
		iArray[2][1]="80px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="地区代码";         	//列名
		iArray[3][1]="60px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="网点代码";         	//列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="对帐明细交易日期";    //列名
		iArray[5][1]="60px";            		//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[6]=new Array();
		iArray[6][0]="明细交易结果";        //列名
		iArray[6][1]="60px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[7]=new Array();
		iArray[7][0]="对帐汇总交易日期";    //列名
		iArray[7][1]="60px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;  

		iArray[8]=new Array();
		iArray[8][0]="汇总交易结果";        //列名
		iArray[8][1]="60px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=0;  

		YBTPolGrid = new MulLineEnter("fm", "YBTPolGrid");
		YBTPolGrid.mulLineCount = 5;
		YBTPolGrid.displayTitle = 1;
		YBTPolGrid.locked = 1;
		YBTPolGrid.canSel = 0;
		YBTPolGrid.canChk = 0;
		YBTPolGrid.hiddenSubtraction = 1;
		YBTPolGrid.hiddenPlus = 1;
		YBTPolGrid.loadMulLine(iArray);
	} catch(ex) {
		alert(ex);
	}
}

</script>