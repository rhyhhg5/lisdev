<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.HashMap"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.sinosoft.midplat.kernel.management.TertianBalanceUI"%>
<%
	Logger mLogger = Logger.getLogger("ui.midplat.tertianBalanceSave_jsp");

	GlobalInput mGlobalInput = (GlobalInput) session.getValue("GI");

	//后面要执行的动作：添加，修改，删除
	String mBankCode = request.getParameter("BalanceBankCode");

	String mZoneNo = request.getParameter("BalanceZoneNo");
	String mBankNode = request.getParameter("BalanceBankNode");
	String mTransDate = request.getParameter("TransDate");

	HashMap mHashMap = new HashMap();
	mHashMap.put("BankCode", mBankCode);
	mHashMap.put("ZoneNo", mZoneNo);
	mHashMap.put("BankNode", mBankNode);
	mHashMap.put("TransDate", mTransDate);
	
	String mFlagStr = null;
	String mContent = null;
	try {
		TertianBalanceUI tTertianBalanceUI = new TertianBalanceUI(mHashMap, mGlobalInput);
		tTertianBalanceUI.deal();
		mFlagStr = "Succ";
		mContent = "对账成功！";
	} catch(Exception ex) {
		mLogger.error("补对账(tertianBalanceSave.jsp)失败！", ex);
		
		mFlagStr = "Fail";
		mContent = "Fail,reasion:" + ex.getMessage();
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=mFlagStr%>","<%=mContent%>");
</script>
</html>