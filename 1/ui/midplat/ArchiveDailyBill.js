var turnPage = new turnPageClass();
// 查询按钮
function easyQueryClick()
{

	// 初始化表格
	initPremQueryGrid3();

	// 书写SQL语句
	var strSQL = "";
	var strQueryResult = "";
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankCode = fm.all('BankCode').value;
	var ManageCom = fm.all('Mng').value;
	var PrtNo = fm.all('PrtNo').value;
	var ProposalContNo = fm.all('ProposalContNo').value;
	var Flag = fm.all('Flag').value;
	var ContFlag = fm.all('ContFlag').value;
	
	if(BankCode == null || BankCode == "")
	{
//	   alert("请输入银行代码");
//	    return;
	}

	if(StartTransDate == null ||StartTransDate == ""||EndTransDate == null||EndTransDate == "")
	{
	   alert("请选择查询日期");
	   return;
	}
  if (fm.all('StartTransDate').value > fm.all('EndTransDate').value)
  {
  		alert("查询终止日期不能在查询起始日期之前！");
  		return false;
  }
  
  if (fm.all('ManageCom').value != null && fm.all('ManageCom').value != "") {
  	ManageCom = fm.all('ManageCom').value;
  }
  if (Flag == '1') {
  	strSQL = "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通柜面出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END,(days(a.PolApplyDate) - days(h.Makedate)) "
			+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%'"
			+ getWherePart("b.bankCode","BankCode")
			+ getWherePart("a.PrtNo","PrtNo")
			+ getWherePart("a.ProposalContNo","ProposalContNo")
			if(BankCode == null || BankCode == ""){
				strSQL+= " and b.bankcode in ('01','02','03','04','10','16')";
			}
  			if(ContFlag != null && ContFlag != ""){
  				strSQL+= " and a.cardflag= '"+ContFlag+"'";
  			}
  			if (null != PrtNo && !PrtNo.equals("")) {
		  		strSQL += "and a.PrtNo='" + PrtNo + "' ";
    		}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		strSQL += "and a.ProposalContNo='" + ProposalContNo + "' "; 
	    	}
  			
  		//add by zengzm 2016-03-23
		strSQL+= " union all "
		+ "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END ,(days(a.PolApplyDate) - days(h.Makedate)) "
			+ "from lbcont a,lktransstatus b,lbpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%'"
			+ getWherePart("b.bankCode","BankCode")
			+ getWherePart("a.PrtNo","PrtNo")
			+ getWherePart("a.ProposalContNo","ProposalContNo")
			if(BankCode == null || BankCode == ""){
				strSQL+= " and b.bankcode in ('01','02','03','04','10','16')";
			}
			if(ContFlag != null && ContFlag != ""){
				strSQL+= " and a.cardflag= '"+ContFlag+"'";
			}
			if (null != PrtNo && !PrtNo.equals("")) {
		  		strSQL += "and a.PrtNo='" + PrtNo + "' ";
    		}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		strSQL += "and a.ProposalContNo='" + ProposalContNo + "' "; 
	    	}
			
			strSQL+=" order by bankcode,PolApplyDate, ContNo with ur";
  } else if (Flag == '0') {
  	strSQL = "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, " +
  			"d.riskname, c.prem, '', '否',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通出单' " +
  			"WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END ,'0'"
			+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f,lacom g  "
			+ "where b.funcflag='01' and b.polno=a.contno  and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%' "
			+ "and not exists (select 1 from ES_DOC_Main h where h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04'))"
			+ getWherePart("b.bankCode","BankCode")
			+ getWherePart("a.PrtNo","PrtNo")
			+ getWherePart("a.ProposalContNo","ProposalContNo")
			if(BankCode == null || BankCode == ""){
				strSQL+= " and b.bankcode in ('01','02','03','04','10','16')";
			}
	  		if(ContFlag != null && ContFlag != ""){
				strSQL+= " and a.cardflag= '"+ContFlag+"'";
			}
	  		if (null != PrtNo && !PrtNo.equals("")) {
		  		strSQL += "and a.PrtNo='" + PrtNo + "' ";
    		}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		strSQL += "and a.ProposalContNo='" + ProposalContNo + "' "; 
	    	}
	  		
	  	//add by zengzm 2016-03-23
		strSQL+= " union all "
		+ "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END ,(days(a.PolApplyDate) - days(h.Makedate)) "
			+ "from lbcont a,lktransstatus b,lbpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%'"
			+ getWherePart("b.bankCode","BankCode")
			+ getWherePart("a.PrtNo","PrtNo")
			+ getWherePart("a.ProposalContNo","ProposalContNo")
			if(BankCode == null || BankCode == ""){
				strSQL+= " and b.bankcode in ('01','02','03','04','10','16')";
			}
			if(ContFlag != null && ContFlag != ""){
				strSQL+= " and a.cardflag= '"+ContFlag+"'";
			}
			if (null != PrtNo && !PrtNo.equals("")) {
		  		strSQL += "and a.PrtNo='" + PrtNo + "' ";
    		}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		strSQL += "and a.ProposalContNo='" + ProposalContNo + "' "; 
	    	}
	  		
			strSQL+=" order by bankcode,PolApplyDate, ContNo with ur";
  } else {
  	strSQL = "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, '', '否',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END,'0' "
			+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f,lacom g "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%' "
			+ "and not exists (select 1 from ES_DOC_Main h where h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04'))"
			+ getWherePart("b.bankCode","BankCode")
			+ getWherePart("a.PrtNo","PrtNo")
			+ getWherePart("a.ProposalContNo","ProposalContNo")
			if(BankCode == null || BankCode == ""){
				strSQL+= " and b.bankcode in ('01','02','03','04','10','16')";
			}
		  	if(ContFlag != null && ContFlag != ""){
				strSQL+= " and a.cardflag= '"+ContFlag+"'";
			}
		  	if (null != PrtNo && !PrtNo.equals("")) {
		  		strSQL += "and a.PrtNo='" + PrtNo + "' ";
    		}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		strSQL += "and a.ProposalContNo='" + ProposalContNo + "' "; 
	    	}
	    	
  		strSQL+= " union all "
  		+ "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END ,(days(a.PolApplyDate) - days(h.Makedate)) "
			+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%'"
			+ getWherePart("b.bankCode","BankCode")
			+ getWherePart("a.PrtNo","PrtNo")
			+ getWherePart("a.ProposalContNo","ProposalContNo")
			if(BankCode == null || BankCode == ""){
				strSQL+= " and b.bankcode in ('01','02','03','04','10','16')";
			}
  			if(ContFlag != null && ContFlag != ""){
				strSQL+= " and a.cardflag= '"+ContFlag+"'";
			}
  			
  			//add by zengzm 2016-03-23
		strSQL+= " union all "
		+ "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END ,(days(a.PolApplyDate) - days(h.Makedate)) "
			+ "from lbcont a,lktransstatus b,lbpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%'"
			+ getWherePart("b.bankCode","BankCode")
			+ getWherePart("a.PrtNo","PrtNo")
			+ getWherePart("a.ProposalContNo","ProposalContNo")
			if(BankCode == null || BankCode == ""){
				strSQL+= " and b.bankcode in ('01','02','03','04','10','16')";
			}
			if(ContFlag != null && ContFlag != ""){
				strSQL+= " and a.cardflag= '"+ContFlag+"'";
			}
			if (null != PrtNo && !PrtNo.equals("")) {
				strSQL += "and a.PrtNo='" + PrtNo + "' ";
    		}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		strSQL += "and a.ProposalContNo='" + ProposalContNo + "' "; 
	    	}
  			
			strSQL+=" order by bankcode,PolApplyDate, ContNo with ur";
  }
  
turnPage.queryModal(strSQL,PremQueryGrid3, 0, 0);

//把导出按钮变为可用
   fm.ExportButton.disabled="";
}

//导出成EXCEL表格
function NCExecel()
{
	var strSQL = "";
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankCode = fm.all('BankCode').value;
	var ManageCom = fm.all('ManageCom').value;
	var PrtNo = fm.all('PrtNo').value;
	var ProposalContNo = fm.all('ProposalContNo').value;
	var ContFlag = fm.all('ContFlag').value;
	var Flag = fm.all('Flag').value;
	if(BankCode == null || BankCode == "")
	{
	   //alert("请输入银行代码");
	   //return;
	}

	if(StartTransDate == null ||StartTransDate == ""||EndTransDate == null||EndTransDate == "")
	{
	   alert("请选择查询日期");
	   return;
	}
  if (fm.all('StartTransDate').value > fm.all('EndTransDate').value)
  {
  		alert("查询终止日期不能在查询起始日期之前！");
  		return false;
  }

   fm.action="./ArchiveDailyBillSave.jsp";
   fm.submit(); //提交
   fm.ExportButton.disabled="disabled";//导出一次后将按钮置灰
	}

//打印
function BillPrint()
{
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankBranch = fm.all('BankBranch').value;
	var BankNode = fm.all('BankNode').value;

	if(StartTransDate == null ||StartTransDate == ""||EndTransDate == null||EndTransDate == "")
	{
	   alert("请选择查询日期");
	   return;
	}
  if (fm.all('StartTransDate').value > fm.all('EndTransDate').value)
  {
  		alert("查询终止日期不能在查询起始日期之前！");
  		return false;
  }
		fm.action="./DailyBillSave1.jsp";
		fm.target="f1print";
		fm.fmtransact.value="PRINT";
		fm.submit();
}

//选择银行代理机构
function SelectBankCom(){
	showInfo=window.open('../treeCom/jsp/BankSelectCategory.jsp','newwindow','height=400, width=800, top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-800)/2+', toolbar=no, menubar=no, scrollbars=yes, resizable=no, location=no, status=no');
}