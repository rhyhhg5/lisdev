var turnPage = new turnPageClass();

// 查询按钮
function easyQueryClick() {
	var TransDate = fm.all('TransDate').value;
	var strSQL = '';
	var strsql = '';
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankCode = fm.all('BankCode').value;
	
	if((null==BankCode) || (""==BankCode)) {
	   alert("请选择银行代码！");
	   return false;
	}
	if((null==StartTransDate) || (""==StartTransDate)) {
	   alert("请输入开始日期！");
	   return false;
	}
	if((null==EndTransDate) || (""==EndTransDate)) {
	   alert("请输入结束日期！");
	   return false;
	}	
	if (fm.all('StartTransDate').value > fm.all('EndTransDate').value) {
		alert("查询终止日期不能在查询起始日期之前！");
		return false;
	}

/**
 * 

	strSQL = " select a.bankcode,( select name from lacom where banktype ='00' and agentcom = a.bankcode ),a.transdate,case rcode when '1' then '成功' when '0' then '失败' end, "
	        + " (select distinct transdate from lktransstatus where bankcode = a.bankcode and transdate = a.transdate and funcflag ='16' ),"
	        + " case (select max(rcode) from lktransstatus where bankcode = a.bankcode and transdate = a.transdate and funcflag ='16' ) when '1' then '成功' when '0' then '失败' end " 
	        + " from lktransstatus a where funcflag = '16' and exists ( select 'X' from lkcodemapping b where remark5 ='1' and bankcode = a.bankcode "              
	        + getWherePart( 'a.bankcode','BankCode' )  
	        + getWherePart( 'a.transdate','StartTransDate','>=')
	        + getWherePart( 'a.transdate','EndTransDate','<=')
	        + " )"
	        + " order by transdate";           
*/

	strSQL = "select a.bankcode"
			+ ", (select codename from ldcode where codetype='ybtbank' and code=a.bankcode)"
			+ ", a.BankBranch, a.BankNode, a.transdate"
			+ ", case rcode when '1' then '成功' when '0' then '失败' end"
			+ ", (select transdate from lktransstatus where substr(a.TransNo, 1, 10)=substr(TransNo, 1, 10) and funcFlag='16')"
			+ ", case (select rcode from lktransstatus where substr(a.TransNo, 1, 10)=substr(TransNo, 1, 10) and funcFlag='16') when '1' then '成功' when '0' then '失败' end"
			+ " from lktransstatus a where funcflag='17'"
			+ getWherePart( 'a.bankcode', 'BankCode')
			+ getWherePart( 'a.transdate', 'StartTransDate', '>=')
			+ getWherePart( 'a.transdate', 'EndTransDate', '<=')
			+ "and substr(a.TransNo, 1, 10)=(select max(substr(TransNo, 1, 10)) from lktransstatus where funcflag = '17' and transdate=a.transdate and BankCode=a.BankCode and bankbranch=a.bankbranch and BankNode=a.BankNode)"
			+ " order by a.transdate";
			
	turnPage.queryModal(strSQL, YBTPolGrid, 0, 0);
	//turnPage.queryModal(strSQL, YBTPolGrid);
}

function BalanceDetail() {
	var TransDate = fm.all('TransDate').value;
	var BankCode = fm.all('BalanceBankCode').value;
	fm.action="./tertianBalanceSave.jsp";
	fm.submit(); //提交
}

function afterSubmit(FlagStr, Content) {
	var FlagDel = FlagStr;
	var info = Content;
	if (FlagStr == "Fail" ) {
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		alert("补对账成功！");
	}
}