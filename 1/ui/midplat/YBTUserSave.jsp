<%@page import="com.sinosoft.midplat.kernel.util.YBTUserUI"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  System.out.println("开始执行Save页面");
  LCYBTCustomerSchema mLCYBTCustomerSchema = new LCYBTCustomerSchema();
  LCYBTCustomerSet mLCYBTCustomerSet = new LCYBTCustomerSet();
  YBTUserUI mYBTUserUI = new YBTUserUI();
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  //String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String mDescType = "";//将操作标志的英文转换成汉字的形式

  System.out.println("开始进行获取数据的操作！！！");
  mLCYBTCustomerSchema.setAuthorizeNo(request.getParameter("AuthorizeNo"));
  mLCYBTCustomerSchema.setName(request.getParameter("Name"));
  mLCYBTCustomerSchema.setSex(request.getParameter("Sex"));
  mLCYBTCustomerSchema.setIDType(request.getParameter("IDType"));
  mLCYBTCustomerSchema.setIDNo(request.getParameter("IDNo"));
  mLCYBTCustomerSchema.setEffectiveDate(request.getParameter("EffectiveDate"));
  

  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增";
  }
  if(mOperateType.equals("UPDATE"))
  {
    mDescType = "修改";
    //mLMCertifyDesSchema.setRiskVersion(request.getParameter("CertifyCode_1"));
    //System.out.println("修改时的校验单证号码是"+mLMCertifyDesSchema.getRiskVersion());
  }
  if(mOperateType.equals("DELETE"))
  {
    mDescType = "删除";
  }
  if(mOperateType.equals("QUERY"))
  {
    mDescType = "查询";
  }
  
  mLCYBTCustomerSet.add(mLCYBTCustomerSchema);
  VData tVData = new VData();
  try
  {
    tVData.addElement(mOperateType);
    tVData.addElement(mLCYBTCustomerSet);
    mYBTUserUI.submitData(tVData, mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mYBTUserUI.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+"成功!";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+"失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>