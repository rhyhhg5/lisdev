<%@include file="../common/jsp/UsrCheck.jsp"%>
 

<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
%>   

<script>
  var manageCom="<%=tG.ManageCom%>"; //记录登陆机构
  var comCode = <%=tG.ComCode%>
</script>
<html>
<%
  //程序名称：ybtBaoQuan.jsp
  //程序功能：银保通保全按地区开通关闭
  //创建日期：20170915
  //创建人  ：gaojinfu
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%
GlobalInput tGI=new GlobalInput();
tGI=(GlobalInput)session.getValue("GI");
%>
<script>
	var tmanageCom="<%=tGI.ManageCom%>"; //记录登陆机构
	var comcode="<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="../common/Calendar/Calendar.js"></SCRIPT>
<script src="ybtBaoQuan.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="ybtBaoQuanInit.jsp"%>
<title>银保通查询 </title>
</head>

<body  onload="initForm();initElementtype();" >
  <form action="./ybtStopSaleMSG.jsp" method=post name=fm target="fraSubmit">
  	<table class="common" align=center>
		<tr align=right>
			<td class=button>&nbsp;&nbsp;</td>
			<td class=button width="10%">
				<INPUT VALUE="查  询" class=CssButton type=button OnClick="return queryClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="增  加" name="add" TYPE=button onclick="return addClick();">
			</td>
			<td class=button width="20%">
				<INPUT class=cssButton VALUE="修  改" name="update" TYPE=button onclick="return updateClick();">
				<INPUT class=cssButton VALUE="保  存" name="updateSubmit" disabled="disabled" TYPE=button onclick="return updateSubmitForm();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="删  除" name="del" TYPE=button onclick="return delClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="重  置" TYPE=button onclick="return resetForm();">
			</td>
	</table>
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>保全功能：</td>
		</tr>
	</table>
    <table  class= common align=center>
		<TR  class= common>
			<td class=title>银行代码</td>
	      	<td class=input>
	     		 <input class=codeno verify="银行代码|NotNull" name=BankCode ondblclick="return showCodeList('banknum',[this,BankCodeName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('banknum',[this,BankCodeName],[0,1],null,null,null,1,null,1);"><input class=codename  name=BankCodeName readonly=true elementtype=nacessary>	   
	      	</td>
	      	<td class=title>机构代码</td>
			<td class=input>
				<Input class="codeno" name=ComCode CodeData="" ondblclick="getComcode();return showCodeListEx('ComCode',[this,ComCodeName],[0,1]);" onkeyup="return showCodeListKeyEx('ComCode',[this,ComCodeName],[0,1]);" ><input name=ComCodeName class=codename readonly=true elementtype=nacessary>
			</td>
			<TD  class= title>产品代码</TD>
			<TD  class= input> 
				<input class=codeno verify="险种代码|notnull" name=RiskCode ondblclick="return showCodeList('ybtriskcode',[this,RiskName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('ybtriskcode',[this,RiskName],[0,1],null,null,null,1,null,1);"><input class=codename  name=RiskName elementtype=nacessary >
				<!-- <Input class=codeNo verify="险种代码|notnull" name=RiskCode CodeData="" ondblclick="getRiskCode();return showCodeListEx('riskcode1',[this,RiskName], [0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('riskcode1',[this,RiskName], [0,1],null,null,null,1);"><Input class=codename name=RiskName elementtype=nacessary> -->
			</TD>
		</TR>
		<tr class=common>
			<td class=title>渠道标识</td>
			<td class=input>
				<input class='codeno'  verify="渠道标识|NotNull" name=Channel CodeData="0|^00|所有渠道^0|柜面^1|网银^8|ATM^e|手机银行^f|智慧柜员机" ondblclick="return showCodeListEx('Channel',[this,ChannelName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Channel',[this,ChannelName],[0,1],null,null,null,1);"  ><input class=codename name=ChannelName elementtype=nacessary>  
			</td> 
			<td class=title>保全状态</td>
			<td class=input>
				<input class='codeno'  verify="保全状态|NotNull" name=Status CodeData="0|^0|关闭^1|开通" ondblclick="return showCodeListEx('Status',[this,StatusName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Status',[this,StatusName],[0,1],null,null,null,1);"  ><input class=codename name=StatusName elementtype=nacessary>  
			</td> 
			<TD  class= title>生效日期 </TD>
			<TD  class= input><Input class= "coolDatePicker" dateFormat="short" verify="生效日期 |NotNull" name=StartDate elementtype=nacessary > </TD>
		</tr>
		<TR>
			<td class="title">操作员</td>
			<td class="input"><input class="readonly" readonly name="Operator"></td>
        </TR>
	</table>
	<Input type=hidden  name=ID value=''>
	<input type=hidden name=hideOperate value=''>
	<input type=hidden name=DataType value=''>
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
		<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanybtBQPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class = CssButton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class = CssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class = CssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class = CssButton TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
        