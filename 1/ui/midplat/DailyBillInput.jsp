<%

//DailyBillInput.jsp

//程序功能：银宝通日清单

//创建日期：2008-01-02 

//创建人  ：WangHaiBing

%>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<html> 

<head>	

<meta http-equiv="Content-Type" content="text/html; charset=GBK">

  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>

  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>

	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>

	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>



  <SCRIPT src="DailyBill.js"></SCRIPT>

	<%@include file="DailyBillInit.jsp"%>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>



  <title>银保通日清单</title>

</head>



<%

     //添加页面控件的初始化。

  GlobalInput tG =new GlobalInput();

     tG.Operator ="001";

     tG.ComCode = "8600";

     tG.ManageCom ="86";

    

	if( session.getValue("GI") == null ) {		

		session.putValue("GI", tG);

	}

//	System.out.println(String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom.length()));

	String temp =String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom);

%>









<body onload="initForm();">  

  <form action="./DailyBillSave.jsp" method=post name=fm target="fraSubmit">

    <table class= common >

    	<tr>

				<td class= titleImg >请输入查询条件：</td>

			</tr>

    </table>

    <table  class= common >  

    	

  <!--  	   	

    	  <tr  class= common> 

        <td  class= title>代理机构 </td>

        <td  class= input> <input class=common name=UpAgentCom onchange="getComName(UpComName)"><input type=button name="selectBank" class="cssButton" value="选择" onClick="SelectBankCom();">

        </td>        

	      <td  class= title>机构名称</td>

        <td  class= input> <input class='readonly' readonly name=UpComName > </td>   

       </tr>

   -->    

           	

	   	<tr  class= common>

   		

				<td class=title>银行代码</td>

	      <td class=input>	      

	        <Input class=codeno name=BankCode ondblclick="return showCodeList('ybtbank',[this,BankName],[0,1]);" onkeyup="return showCodeList('ybtbank',[this,BankName],null,null,['2'],['acctype'],[0,1]);"><input class=codename name=BankName readonly=true>

	      </td>

    <!--

        <td class=title>代理机构</td>

	      <td class=input>

	      <input class=codeno  verify="银行代码|NotNull" name=AgentCom ondblclick="return showCodeList('agentcom',[this,BankCodeName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('agentcom',[this,BankCodeName],[0,1],null,null,null,1,null,1);"><input class=codename  name=BankCodeName readonly=true elementtype=nacessary>

	      </td>

	      -->

	<!-- 

	      <td  class= title>代理机构</td>

        <td  class= input> <input class=common name=UpAgentCom onchange="getComName(UpComName)">  

        	                 <input type=button name="selectBank" class="cssButton" value="选择" onClick="SelectBankCom();">

        </td>  

	 -->    

	    	<td  class= title>地区代码</td>

	    	<td class= input>

	    		<!--input class=code verify="地区代码|NotNull" name=BankBranch ondblclick="return showCodeList('zoneno',[this],[0],null,fm.BankCode.value,'BankCode',1,null,1);" onkeyup="return showCodeListKey('zoneno',[this],[0],null,fm.BankCode.value,'BankCode',1,null,1);"-->

	    		<input class=common verify="地区代码|NotNull" name=BankBranch>

	    		<!--<Input class=code name=AgentCode ondblclick="queryAgent()" onblur="GetManageCom();">-->

	    	</td>

	    	<td  class= title>网点代码</td>

	    	<td  class= input>

	       <!--input class=code verify="网点代码|NotNull" name=BankNode ondblclick="return showCodeList('banknode',[this],[0],null,fm.BankBranch.value,'zoneno',1,null,1);" onkeyup="return showCodeListKey('banknode',[this],[0],null,fm.BankBranch.value,'zoneno',1,null,1);"-->

	       <input class=common verify="网点代码|NotNull" name=BankNode>     

	      </td>	

	    </tr>    
		<tr>
	     	<td  class= title>开始日期</td>

	      <td  class= input> <Input class= "coolDatePicker" dateFormat="short" name=StartTransDate elementtype=nacessary > </td>

	    	<td  class= title>结束日期</td>

	    	<td  class= input><Input class= "coolDatePicker" dateFormat="short" name=EndTransDate elementtype=nacessary > </td>

	   </tr>  

	    <td>

	    	<input type=hidden  name="Mng" value="<%=temp%>">    			

	    </td>      

    </table>

    <table  class= common >

      <tr> 

				<td>

					<input class= cssbutton type=Button name="QueryButton" value="查询数据" onclick="easyQueryClick();">

					<input class= cssbutton type=button name="ExportButton" disabled="disabled" value="导  出" onclick="NCExecel();">

					<!--<input class= cssbutton type=button value="打  印" onclick="BillPrint();">-->  

				</td>			

			</tr>    

		</table>

    <table class=common >      	

	   	<tr class=common>

	    	<td class=title>金额合计</td>

	    	<td class=input>

	        <input class="readonly" name=SumPrem readonly>

	    	</td>

	    	<td class=title>(单位:元)</td>

	    	<td class=input></td>

	    </tr>

	  </table>



<Div id="divIssuedoc3">

  <table class=common>

    <tr class=common>

      <td text-align:left colSpan=1>

        <span id="spanPremQueryGrid3">        

        	</span>

      </td>

    </tr>

  </table>

</div>



<center>      

      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 

      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">           

      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 

      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">      

</center> 

<input type=hidden id="fmtransact" name="fmtransact">

  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>

</html>

