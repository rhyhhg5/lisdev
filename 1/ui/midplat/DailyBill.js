

var turnPage = new turnPageClass();
// 查询按钮
function easyQueryClick()
{

	// 初始化表格
	initPremQueryGrid3();

	// 书写SQL语句
	var strSQL = "";
	var strtSQL="";
	var strQueryResult = "";
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankBranch = fm.all('BankBranch').value;
	var BankNode = fm.all('BankNode').value;
	var BankCode = fm.all('BankCode').value;

	if(StartTransDate == null ||StartTransDate == ""||EndTransDate == null||EndTransDate == "")
	{
	   alert("请选择查询日期");
	   return;
	}
  if (fm.all('StartTransDate').value > fm.all('EndTransDate').value)
  {
  		alert("查询终止日期不能在查询起始日期之前！");
  		return false;
  }
//  	if(BankBranch == null ||BankBranch == ""||BankBranch == null||BankBranch == "")
//	{
//	   alert("请输入地区代码");
//	   return;
//	}
//	  	if(BankNode == null ||BankNode == ""||BankNode == null||BankNode == "")
//	{
//	   alert("请输入网点代码");
//	   return;
//	}
		  	if(BankCode == null ||BankCode == ""||BankCode == null||BankCode == "")
	{
	   alert("请输入银行代码");
	   return;
	}

strSQL = "select e.name , b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, c.riskcode, d.riskname, case when a.payintv = 0 then '趸交' else '期交' end , c.prem, b.transno "
		+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f "
		+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode "
		+ "and a.appflag='1' and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "'"
		+ getWherePart("b.bankCode","BankCode")
		+ getWherePart("b.bankbranch","BankBranch")
		+ getWherePart("b.banknode","BankNode")
		+" with ur"

	 var strSql2 = "select sum(c.prem) " + strSQL.substring(strSQL.indexOf("from"));
  var arrResult2 = easyExecSql(strSql2);

  fm.SumPrem.value = arrResult2;

turnPage.queryModal(strSQL,PremQueryGrid3, 0, 0);

//把导出按钮变为可用
   fm.ExportButton.disabled="";
}

//导出成EXCEL表格
function NCExecel()
{
	var strSQL = "";
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankBranch = fm.all('BankBranch').value;
	var BankNode = fm.all('BankNode').value;
	var BankCode = fm.all('BankCode').value;

	if(StartTransDate == null ||StartTransDate == ""||EndTransDate == null||EndTransDate == "")
	{
	   alert("请选择查询日期");
	   return;
	}
  if (fm.all('StartTransDate').value > fm.all('EndTransDate').value)
  {
  		alert("查询终止日期不能在查询起始日期之前！");
  		return false;
  }
//  	if(BankBranch == null ||BankBranch == ""||BankBranch == null||BankBranch == "")
//	{
//	   alert("请输入地区代码");
//	   return;
//	}
//	  	if(BankNode == null ||BankNode == ""||BankNode == null||BankNode == "")
//	{
//	   alert("请输入网点代码");
//	   return;
//	}
		  	if(BankCode == null ||BankCode == ""||BankCode == null||BankCode == "")
	{
	   alert("请输入银行代码");
	   return;
	}

   fm.action="./DailyBillSave.jsp";
   fm.submit(); //提交
   fm.ExportButton.disabled="disabled";//导出一次后将按钮置灰
	}

//打印
function BillPrint()
{
	var StartTransDate = fm.all('StartTransDate').value;
	var EndTransDate = fm.all('EndTransDate').value;
	var BankBranch = fm.all('BankBranch').value;
	var BankNode = fm.all('BankNode').value;

	if(StartTransDate == null ||StartTransDate == ""||EndTransDate == null||EndTransDate == "")
	{
	   alert("请选择查询日期");
	   return;
	}
  if (fm.all('StartTransDate').value > fm.all('EndTransDate').value)
  {
  		alert("查询终止日期不能在查询起始日期之前！");
  		return false;
  }
//  	if(BankBranch == null ||BankBranch == ""||BankBranch == null||BankBranch == "")
//	{
//	   alert("请输入地区代码");
//	   return;
//	}
//	  	if(BankNode == null ||BankNode == ""||BankNode == null||BankNode == "")
//	{
//	   alert("请输入网点代码");
//	   return;
//	}
		fm.action="./DailyBillSave1.jsp";
		fm.target="f1print";
		fm.fmtransact.value="PRINT";
		fm.submit();
}

//选择银行代理机构
function SelectBankCom(){
	showInfo=window.open('../treeCom/jsp/BankSelectCategory.jsp','newwindow','height=400, width=800, top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-800)/2+', toolbar=no, menubar=no, scrollbars=yes, resizable=no, location=no, status=no');
}