<%@page contentType="text/html;charset=gb2312" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>

<%
    //在此设置导出Excel的列名，应与sql语句取出的域相对应

    ExportExcel.Format format = new ExportExcel.Format();
    ArrayList listCell = new ArrayList();
    ArrayList listLB = new ArrayList();
    ArrayList listColWidth = new ArrayList();
    format.mListCell=listCell;
    format.mListBL=listLB;
    format.mListColWidth=listColWidth;

    ExportExcel.Cell tCell=null;
    ExportExcel.ListBlock tLB=null;

			 GlobalInput tG = (GlobalInput) session.getValue("GI");
//       String BankCode = request.getParameter("BankCode");
//      String ManageCom = request.getParameter("ManageCom");
//       String PrtNo = request.getParameter("PrtNo");
//       String ProposalContNo = request.getParameter("ProposalContNo");
//       String Flag = request.getParameter("Flag");
       String StartTransDate = request.getParameter("StartTransDate");
       String EndTransDate = request.getParameter("EndTransDate");
       
      
    listColWidth.add(new String[]{"0","5000"});
    
    StringBuffer sb = new StringBuffer();
    if(StartTransDate == null || "".equals(StartTransDate)){
    	StartTransDate = "current date";
    }
    if(EndTransDate == null || "".equals(EndTransDate)){
    	EndTransDate = "current date";
    }
    
    
    String strSQL = "select a,coalesce(cprem,0),coalesce(bprem,0),case when ((coalesce(cprem,0)+coalesce(bprem,0)) is null) then 0 else (coalesce(cprem,0)+coalesce(bprem,0)) end from "
	 +" (select ldcom.name as a,(select sum(a.prem) "
	 +" from lccont a,lktransstatus b where b.funcflag='01' and b.polno=a.contno "
	 +" and a.appflag='1' and a.PolApplyDate between '"+StartTransDate+"' and '"+EndTransDate+"' "
	 +" and b.bankCode = '16' and b.RBankVSMP = '00' and b.Resultbalance = '0'"
	 +" and substr(a.managecom,1,4) = ldcom.comcode ) as cprem,(select sum(a.prem) "
	 +" from lbcont a,lktransstatus b where b.funcflag='01' and b.polno=a.contno"
	 +" and a.PolApplyDate between '"+StartTransDate+"' and '"+EndTransDate+"' and b.bankCode = '16'"
	 +" and b.RBankVSMP = '00' and b.Resultbalance = '0' and substr(a.managecom,1,4) = ldcom.comcode"
	 +" ) as bprem from ldcom ldcom where ldcom.comcode in "
	 +" ('8610','8611','8612','8613','8614','8615','8621','8622','8631','8632','8633','8634','8635','8636',"
	 +" '8637','8641','8642','8643','8644','8651','8653','8661','8665','8691','8694','8695')) as x with ur";

    String strSum = "select '合计(元)',cprem,bprem,(cprem+bprem) from ("
    		+" select   "
    		+" (select coalesce(sum(a.prem),0)  from lccont a,lktransstatus b"
    		+" 	where b.funcflag='01' and b.polno=a.contno"
    		+" 	and a.appflag='1' and a.PolApplyDate between '"+StartTransDate+"' and '"+EndTransDate+"' "
    		+" 	and b.bankCode = '16' and b.RBankVSMP = '00' and b.Resultbalance = '0') cprem,"
    		+" (select coalesce(sum(a.prem),0) "
    		+" 	from lbcont a,lktransstatus b where b.funcflag='01' and b.polno=a.contno"
    		+" 	and a.PolApplyDate between '"+StartTransDate+"' and '"+EndTransDate+"' "
    		+" 	and b.bankCode = '16' and b.RBankVSMP = '00' and b.Resultbalance = '0') bprem"
    		+" from dual ) as x "
    		+" with ur";
    System.out.println("邮保通查询收入sql："+strSQL);
    tLB = new ExportExcel.ListBlock("001");
    tLB.colName=new String[]{"分公司名称","有效保单","无效保单","合计(元)"};
    tLB.sql=strSQL;
    tLB.row1=0;
    tLB.col1=0;
    tLB.InitData();
    listLB.add(tLB);
    ExportExcel.ListBlock tLB2 = new ExportExcel.ListBlock("001");
    tLB2.sql=strSum;
    tLB2.row1=26;
    tLB2.col1=0;
    tLB2.InitData();
    listLB.add(tLB2);
    try{
	  //将输出流置空,否则会因为流的冲突造成websphere报错.
	  out.clear(); 
	  out = pageContext.pushBody();
	  	
      response.reset();
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition","attachment; filename=YBT_"+StartTransDate+"_"+EndTransDate+"_IncomeList.xls");
      OutputStream outOS=response.getOutputStream();

      BufferedOutputStream bos=new BufferedOutputStream(outOS);

      ExportExcel excel = new ExportExcel();

      excel.write(format,bos);

      bos.flush();
      bos.close();
    }
    catch(Exception e){
      System.out.println("导出Excel失败！");
    };

%>
