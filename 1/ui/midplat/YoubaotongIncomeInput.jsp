<%

//ArchiveDailyBillInput.jsp

//程序功能：邮保通收入统计表

//创建日期：2012-06-08 

//创建人  ：wuzhen

%>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<html> 

<head>	

<meta http-equiv="Content-Type" content="text/html; charset=GBK">

  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>

  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>

	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>

	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>



  <SCRIPT src="YoubaotongIncome.js"></SCRIPT>

	<%@include file="YoubaotongIncomeInit.jsp"%>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>



  <title>邮保通收入统计表</title>

</head>



<%

     //添加页面控件的初始化。

  GlobalInput tG =new GlobalInput();

     tG.Operator ="001";

     tG.ComCode = "8600";

     tG.ManageCom ="86";

    

	if( session.getValue("GI") == null ) {		

		session.putValue("GI", tG);

	}

//	System.out.println(String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom.length()));

	String temp =String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom);

%>

<body onload="initForm();initElementtype();">  

  <form action="./YoubaotongIncomeSave.jsp" method=post name=fm target="fraSubmit">

    <table class= common >

    	<tr>

				<td class= titleImg >请输入查询条件：</td>

			</tr>

    </table>

    <table  class= common >

	   
		<tr>
	     	<td  class= title>开始日期</td>

	      <td  class= input> <Input class= "coolDatePicker" dateFormat="short" name=StartTransDate elementtype=nacessary > </td>

	    	<td  class= title>结束日期</td>

	    	<td  class= input><Input class= "coolDatePicker" dateFormat="short" name=EndTransDate elementtype=nacessary > </td>

	   </tr>
	   

	    <td>

	    	<input type=hidden  name="Mng" value="<%=temp%>">

	    </td>

    </table>

    <table  class= common >

      <tr> 

				<td>

					<input class= cssbutton type=Button name="QueryButton" value="查  询" onclick="easyQueryClick();">

					<input class= cssbutton type=button name="ExportButton" disabled="disabled" value="导  出" onclick="NCExecel();">

				</td>			

			</tr>    

		</table>

<Div id="divIssuedoc3">

  <table class=common>

    <tr class=common>

      <td text-align:left colSpan=1>

        <span id="spanPremQueryGrid3">        

        	</span>
		
      </td>

    </tr>
    
    <tr class=common>

      <td text-align:left colSpan=1 class= title>

      有效保单合计：<input type = "text" style="width:100px;" id = "youxiaoSum" disabled="disabled"/><br/>
      失效保单合计：<input type = "text" style="width:100px;" id = "wuxiaoSum" disabled="disabled"/><br/>
      &nbsp;&nbsp;总金额合计：<input type = "text" style="width:100px;" id = "zongSum" disabled="disabled"/><br/>
    
		
      </td>

    </tr>

  </table>

</div>



<center>      

      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 

      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">           

      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 

      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">      

</center> 

<input type=hidden id="fmtransact" name="fmtransact">

  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>

</html>

