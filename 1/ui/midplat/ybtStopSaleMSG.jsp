<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
  //程序名称：ybtStopSaleMSG.jsp
  //程序功能：
  //创建日期：20180305
  //创建人  ：GaoJinfu
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@ page import="org.apache.log4j.Logger"%>
<%@page import="com.sinosoft.utility.TransferData"%>
<%@page import="com.sinosoft.lis.schema.YBTRiskSwtichSchema"%>
<%@page import="com.sinosoft.midplat.kernel.management.YBTStopSaleUI"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	Logger cLogger = Logger.getLogger("midplat.ybtStopSaleMSG_jsp");
	cLogger.info("into ybtStopSaleMSG.jsp...");

	//接收信息，并作校验处理。输入参数
	YBTRiskSwtichSchema tYBTRiskSwtichSchema = new YBTRiskSwtichSchema();
	//输出参数
	String tOperate = request.getParameter("hideOperate").trim();
	GlobalInput tG = (GlobalInput) session.getValue("GI");
	String BankCode = request.getParameter("BankCode");
	String ComCode = request.getParameter("ComCode");
	String RiskCode = request.getParameter("RiskCode");
	String MainRiskCode = request.getParameter("MainRiskCode");
	String DataType = request.getParameter("DataType");
	String swtype = request.getParameter("swtype");
	String hideSwvalue = request.getParameter("hideSwvalue");//根据swtype判断取值
	String Status = request.getParameter("Status");
	String Channel = request.getParameter("Channel");
	if ("0".equals(Channel)) {
		Channel = "9";
	} else if ("1".equals(Channel)) {
		Channel = "c";
	} else if ("8".equals(Channel)) {
		Channel = "d";
	}
	String Operator = request.getParameter("Operator");
	String StartDate = request.getParameter("StartDate");
	String ID = request.getParameter("ID");
	if (BankCode == null)
		BankCode = "";
	if (ComCode == null)
		ComCode = "";
	if (RiskCode == null)
		RiskCode = "";
	if (MainRiskCode == null)
		MainRiskCode = "";
	if (DataType == null)
		DataType = "";
	if (swtype == null)
		swtype = "";
	if (hideSwvalue == null)
		hideSwvalue = "";
	if (Status == null)
		Status = "";
	if (Channel == null)
		Channel = "";
	if (Operator == null)
		Operator = "";
	if (StartDate == null)
		StartDate = "";
	if (ID == null)
		ID = "";
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("BankCode", BankCode);
	tTransferData.setNameAndValue("ComCode", ComCode);
	tTransferData.setNameAndValue("RiskCode", RiskCode);
	tTransferData.setNameAndValue("MainRiskCode", MainRiskCode);
	tTransferData.setNameAndValue("DataType", DataType);
	tTransferData.setNameAndValue("swtype", swtype);
	tTransferData.setNameAndValue("hideSwvalue", hideSwvalue);
	tTransferData.setNameAndValue("Status", Status);
	tTransferData.setNameAndValue("Channel", Channel);
	tTransferData.setNameAndValue("Operator", Operator);
	tTransferData.setNameAndValue("StartDate", StartDate);
	tTransferData.setNameAndValue("ID", ID);

	String cFlagStr = null;
	String cContent = null;
	try {
		YBTStopSaleUI tYBTStopSaleUI = new YBTStopSaleUI(tTransferData,
				tG, tOperate);
		tYBTStopSaleUI.deal();
		cFlagStr = "Succ";
		cContent = "操作成功！";
	} catch (Exception ex) {
		cLogger.error("操作失败！", ex);
		cFlagStr = "Fail";
		cContent = "操作失败：" + ex.getMessage();
	}

	cLogger.info("out ybtManageSave.jsp!");
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=cFlagStr%>","<%=cContent%>","");
</script></html>