<script language="JavaScript">

function initForm() 
{	
	initErrQueryGrid();
}


function initErrQueryGrid()
{

  var iArray = new Array();

  try {
  iArray[0]=new Array();
  iArray[0][0]="序号";         		//列名
  iArray[0][1]="30px";         		
  iArray[0][3]=0;         		

  iArray[1]=new Array();
	iArray[1][0]="交易流水号";         	  //列名
	iArray[1][1]="80px";            	//列宽
	iArray[1][2]=100;            			//列最大值
	iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	iArray[2]=new Array();
	iArray[2][0]="投保单号";
	iArray[2][1]="120px";
	iArray[2][2]=200;
	iArray[2][3]=0;

	iArray[3]=new Array();
	iArray[3][0]="交易日期";         	//列名
	iArray[3][1]="80px";            	//列宽
	iArray[3][2]=200;                   //列最大值
	iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	
	iArray[4]=new Array();
	iArray[4][0]="交易类型";         	//列名
	iArray[4][1]="60px";            	//列宽
	iArray[4][2]=200;                   //列最大值
	iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	
	iArray[5]=new Array();
	iArray[5][0]="交易金额";         	//列名
	iArray[5][1]="60px";            	//列宽
	iArray[5][2]=200;                   //列最大值
	iArray[5][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	
	iArray[6]=new Array();
	iArray[6][0]="银行代码";         	//列名
	iArray[6][1]="40px";            	//列宽
	iArray[6][2]=200;                   //列最大值
	iArray[6][3]=0;              		//是否允许输入,1表示允许，0表示不允许

	iArray[7]=new Array();
	iArray[7][0]="地区代码";         	//列名
	iArray[7][1]="60px";            	//列宽
	iArray[7][2]=200;                   //列最大值
	iArray[7][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	
	iArray[8]=new Array();
	iArray[8][0]="网点代码";
	iArray[8][1]="60px";
	iArray[8][2]=200;
	iArray[8][3]=0;	

	iArray[9]=new Array();
	iArray[9][0]="交易出错信息";
	iArray[9][1]="160px";
	iArray[9][2]=200;
	iArray[9][3]=0;

	ErrQueryGrid = new MulLineEnter("fm", "ErrQueryGrid");
	ErrQueryGrid.mulLineCount = 4;
	ErrQueryGrid.displayTitle = 1;
	ErrQueryGrid.locked = 1;
	ErrQueryGrid.canSel = 0;
	ErrQueryGrid.canChk = 0;
	ErrQueryGrid.hiddenSubtraction = 1;
	ErrQueryGrid.hiddenPlus = 1;
	ErrQueryGrid.loadMulLine(iArray);

  }
  catch(ex) {
    alert(ex);
  }
}
</script>
