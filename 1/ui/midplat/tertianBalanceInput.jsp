<%
//tertianBalanceInput.jsp
//程序功能：页面补对账
//创建日期：2008-04-24
//创建人  ：Wanghb
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>	
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

  <SCRIPT src="tertianBalanceInput.js"></SCRIPT>
	<%@include file="tertianBalanceInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <title>页面补对账</title>
</head>

<%
     //添加页面控件的初始化。
  GlobalInput tG =new GlobalInput();
     tG.Operator ="001";
     tG.ComCode = "8600";
     tG.ManageCom ="86";
    
	if( session.getValue("GI") == null ) {		
		session.putValue("GI", tG);
	}
//	System.out.println(String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom.length()));
	String temp =String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom);
%>

<body onload="initForm();">  
  <form action="" method=post name=fm target="fraSubmit">
    <table class= common >
    	<tr>
				<td class= titleImg >请输入查询条件：</td>
			</tr>
    </table>
    		    <table  class= common >  
    	   	<tr  class= common>   		
        <td class=title>银行代码</td>
	      <td class=input>
	      <input class=codeno  verify="银行代码|NotNull" name=BankCode ondblclick="return showCodeList('ybtbank',[this,BankCodeName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('ybtbank',[this,BankCodeName],[0,1],null,null,null,1,null,1);"><input class=codename  name=BankCodeName readonly=true elementtype=nacessary>	   
	      </td>
	     	<td  class= title>开始日期</td>
	      <td  class= input> <Input class= "coolDatePicker" dateFormat="short" name=StartTransDate elementtype=nacessary > </td>
	      <td  class= title>结束日期</td>
	      <td  class= input> <Input class= "coolDatePicker" dateFormat="short" name=EndTransDate elementtype=nacessary > </td>
       </tr>  
	    <td>
	    	<input type=hidden  name="Mng" value="<%=temp%>">    			
	    </td>      
    </table>
    
    <table  class= common >
      <tr> 
				<td>
					<input class= cssbutton type=Button value="对账情况查询" onclick="return easyQueryClick();">
				</td>			
			</tr>    
		</table>
		<Div id="divIssuedoc3">
  <table class=common>
    <tr class=common>
      <td text-align:left colSpan=1>
        <span id="spanYBTPolGrid">        
        	</span>
      </td>
    </tr>
  </table>
</div>		
		<hr>			
		    <table  class= common >
		    <tr>
		 <td class= titleImg colspan="2">补对账执行条件：</td>
		  </tr>	    		  
    	  <tr  class= common>   		
        <td class=title>银行代码</td>
	      <td class=input>
	        	<input name=BalanceBankCode class='codeno' ondblclick="return showCodeList('ybtbank',[this,BalanceBankName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('ybtbank',[this,BalanceBankName],[0,1],null,null,null,1,null,1);"><input class=codename  name=BalanceBankName readonly=true elementtype=nacessary>
	      </td>
	      <td class=title>地区代码</td>
	      <td class=input><input name=BalanceZoneNo class='common' /></td>
	      <td class=title>网点代码</td>
	      <td class=input><input name=BalanceBankNode class='common' /></td>
     	  <td  class= title>对账日期</td>
	      <td  class= input> <Input class= "coolDatePicker" dateFormat="short" name=TransDate elementtype=nacessary > </td>
       </tr>  
	    <td>
	    	<input type=hidden  name="Mng" value="<%=temp%>">    			
	    </td>      
    </table>
		    <table  class= common >
      <tr> 
				<td>
					<input class= cssbutton type=Button value="开始执行补对账" onclick="BalanceDetail();">
				</td>			
			</tr>    
		</table>
		
<input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
