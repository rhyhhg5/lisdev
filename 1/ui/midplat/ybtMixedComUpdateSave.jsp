<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2009-01-23
//创建人  ：   miaoxz
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.midplat.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  String tRiskCode=request.getParameter("RiskCode");


  ybtMixedComUpdateUI tybtMixedComUpdateUI = new ybtMixedComUpdateUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      System.out.println("..............here1"+tAction);
      LOMixBKAttributeSchema tSch = new LOMixBKAttributeSchema();
      tSch.setBandCode(request.getParameter("AgentCom"));
      tSch.setBankNode("1");
      tSch.setCrs_BussType(request.getParameter("Crs_BussType"));
      tSch.setCrs_SaleChnl(request.getParameter("Crs_SaleChnl"));
      tSch.setGrpAgentCode(request.getParameter("GrpAgentCode"));
      tSch.setGrpAgentCom(request.getParameter("GrpAgentCom"));
      tSch.setGrpAgentIDNo(request.getParameter("GrpAgentIDNo"));
      tSch.setGrpAgentName(request.getParameter("GrpAgentName"));
      tSch.setInvalidate(request.getParameter("InvalidDate"));
      tSch.setZoneNo("1");
      tSch.setState("1");
      
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      tVData.add(tGI);
      tVData.add(tSch);
      tybtMixedComUpdateUI.submitData(tVData,tAction);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tybtMixedComUpdateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
