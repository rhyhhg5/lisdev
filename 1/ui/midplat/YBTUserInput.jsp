<%@page contentType="text/html;charset=GBK" %>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.certify.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head >
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src = "YBTUserInput.js"></SCRIPT>
    <%@include file="YBTUserInit.jsp"%>
</head>
<body onload="initForm();initElementtype();" >
    <form action="./YBTUserSave.jsp" method=post name=fm target="fraSubmit">
        <%@include file="../common/jsp/OperateButton.jsp"%>
        <%@include file="../common/jsp/InputButton.jsp"%>
        <table class=common>
            <tr class=common>
                <td class=title>客户姓名</td>
                <td class=input><input class=common name=Name id=NameID verify="员工姓名|len<=20"></td>
                <td class=title>性别</td>
                <td class=input><input class=codeNo name=Sex verify="性别|code:Sex&num" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" readonly=true><input class=codename name=SexName readonly=true></td>
            </tr>
            <tr class=common>
                <td class=title>证件类型</td>
                <td class=input><input class=codeno name=IDType CodeData="0|1^0|身份证^4|其他" ondblclick="return showCodeListEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);" readonly=true><input class=codename name=IDTypeName readonly=true ></td>
                <td class=title>证件号码</td>
                <td class=input><input class= common name=IDNo verify="证件号码|NUM&len<=20" ></td>
            </tr>
            <tr class=common>
                <td class=title >录入有效日期</td> 
  	        	<td class=input ><input class=coolDatePicker name=EffectiveDate verify="有效日期|date"></td>
            </tr>
        </table>


        <input type="hidden" name=OperateType >
        <input type="hidden" name="AuthorizeNo" value="">
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>