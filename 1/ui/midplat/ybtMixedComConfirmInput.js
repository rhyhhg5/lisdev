//               该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!verifyInput())
	{
	 	return false;
	}
	if(!beforeDoSave()){
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

function queryGrpHistoryDetail()
{
    var tRow = SetGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = SetGrid.getRowData(tRow);
    fm.all('ManageCom').value=tRowDatas[0];
    fm.all('ManageComName').value=tRowDatas[1];
    fm.all('BankType2').value=tRowDatas[2];
    fm.all('BankType2Name').value=tRowDatas[3];
    fm.all('AgentCom').value=tRowDatas[4];
    fm.all('AgentComName').value=tRowDatas[5];
    fm.all('Crs_SaleChnl').value=tRowDatas[6];
    fm.all('Crs_BussType').value=tRowDatas[7];
    fm.all('GrpAgentCom').value=tRowDatas[8];
    fm.all('GrpAgentCode').value=tRowDatas[9];
    fm.all('GrpAgentName').value=tRowDatas[10];
    fm.all('GrpAgentIDNo').value=tRowDatas[11];
    fm.all('InvalidDate').value=tRowDatas[12];
    fm.all('State').value=tRowDatas[13];
    
    fm.all('OldBandCode').value=tRowDatas[4];
    fm.all('Crs_BussTypeName').value=tRowDatas[15];
    fm.all('Crs_SaleChnlName').value=tRowDatas[16];
    fm.all('GrpAgentComName').value=tRowDatas[17];
    return true;
}


function DoReset()
{
	fm.all('ManageCom').value='';
    fm.all('ManageComName').value='';
    fm.all('BankType2').value='';
    fm.all('BankType2Name').value='';
    fm.all('AgentCom').value='';
    fm.all('AgentComName').value='';
    fm.all('Crs_SaleChnl').value='';
    fm.all('Crs_BussType').value='';
    fm.all('GrpAgentCom').value='';
    fm.all('GrpAgentComName').value='';
    fm.all('GrpAgentCode').value='';
    fm.all('GrpAgentName').value='';
    fm.all('GrpAgentIDNo').value='';
    fm.all('InvalidDate').value='';
    fm.all('State').value='';   
    
    fm.all('OldBandCode').value='';
    fm.all('OldBankNode').value='';
    fm.all('OldZoneNo').value='';
    
    fm.all('Crs_BussTypeName').value='';
    fm.all('Crs_SaleChnlName').value='';
}

// 查询按钮
function easyQueryClick()
{
	//alert ("start easyQueryClick");
	// 初始化表格
	//if(!verifyInput())
	//{
	 //return false;
	//}
	if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输管理机构！");
  	return false;
  }
	initSetGrid();
	var tStateSQL = "";
	if(fm.WDState.value == '1'){
		tStateSQL = "and a.state = '0' ";
	}else if(fm.WDState.value == '2'){
		tStateSQL = "and a.state = '1' ";
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select b.managecom,(select name from ldcom where comcode=b.managecom),"
	+"b.banktype,case when b.banktype='01' then '分行' when b.banktype='02' then '支行' when b.banktype='03' then '分理处' else '网点' end,"
	+"a.bandcode,(select name from lacom where agentcom=a.bandcode),"
	+"a.crs_salechnl,a.crs_busstype,a.grpagentcom,a.grpagentcode,a.grpagentname,a.grpagentidno,"
	+"a.invalidate,a.state,case when a.state='0' then '未确认' else '已确认' end,"
	+"(select codename from ldcode where codetype='jcsaletype' and code=a.crs_busstype), "
	+"(select codename from ldcode where codetype='jcsalechnl' and code=a.crs_salechnl),  "
	+"(select under_orgname from lomixcom where grpagentcom=a.grpagentcom) "
	+ " from LOMixBKAttribute a,lacom b "
	+ " where 1=1 and a.bandcode=b.agentcom "
	+ tStateSQL
	+ getWherePart( 'b.managecom','ManageCom','like')
	+ getWherePart( 'a.BandCode','AgentCom','like')
	+ getWherePart( 'a.Crs_BussType','Crs_BussType')
	+ getWherePart( 'a.GrpAgentCom','GrpAgentCom')
	+ getWherePart( 'a.InvaliDate','InvalidDate')
	+ getWherePart( 'a.GrpAgentCode','GrpAgentCode')
	+ getWherePart( 'a.GrpAgentName','GrpAgentName')
	+ getWherePart( 'a.GrpAgentIDNo','GrpAgentIDNo')
	+ getWherePart( 'a.Crs_SaleChnl','Crs_SaleChnl');

	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
	
	turnPage.pageLineNum = 20;
    turnPage.queryModal(strSQL, SetGrid);
}


				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}

	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoUpdate(){
	var tState=fm.all('State').value;
	if(tState==null && tState==''){
		alert("此记录不是查询出来的记录，请先查询再进行修改操作！");
		return false;
	}
	if(tState=='1'){
		alert("此记录已确定，不能进行修改操作！");
		return false;
	}
	fm.fmAction.value = "UPDATE";
	if(!verifyInput())
	{
	 return false;
	}
	if(!beforeSubmit())
	{
	 return false;
	}
	if(fm.all('OldBandCode').value!=fm.all('AgentCom').value){
		alert("银行机构已经被修改，不能修改该值！");
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
function changeGrpAgentCom(){
	if (getWherePart('GrpAgentCom')=='')
	return false;
	var strSQL = "";
	strSQL = "select under_orgname from lomixcom where 1=1 "
	+" and grpagentcom = '"+fm.all('GrpAgentCom').value+"'";
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	if (!strQueryResult)
	{
		alert('不存在该集团代理机构！');
		fm.all('GrpAgentCom').value='';
		fm.all('GrpAgentComName').value='';
		return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	fm.all('GrpAgentComName').value = trim(arr[0][0]);
	return true;
}
function getAgentCom(cObj,cName)
{
	if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输管理机构！");
  	return false;
  } 
  //var tempstr=fm.all('BankType2').value;
  //alert(tempstr);
if(fm.all('BankType2').value!=null &&fm.all('BankType2').value !="")
{  
var strsql =" 1 and   banktype=#" + fm.all('BankType2').value + "#  and managecom  like #" + fm.all('ManageCom').value + "%#   and branchtype=#3# and branchtype2=#01#" ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,'1',1);
}
else
	{
	var strsql =" 1 and managecom  like #" + fm.all('ManageCom').value + "%#  and branchtype=#3#  and branchtype2=#01# " ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,'1',1);
	
	}
}



	//显示frmSubmit框架，用来调试
	function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}



//--									//提交前的校验、计算
function beforeSubmit(){
	if(fm.all('AgentCom').value==null || fm.all('AgentCom').value==''){
		alert("银行机构不能为空！操作失败！");
		return false;
	}
	if(fm.all('Crs_BussType').value==null || fm.all('Crs_BussType').value==''){
		alert("集团销售业务类型不能为空！操作失败！");
		return false;
	}
	if(fm.all('GrpAgentCom').value==null || fm.all('GrpAgentCom').value==''){
		alert("集团代理机构不能为空！操作失败！");
		return false;
	}
	if(fm.all('InvalidDate').value==null || fm.all('InvalidDate').value==''){
		alert("失效日期不能为空！操作失败！");
		return false;
	}
	if(fm.all('GrpAgentCode').value==null || fm.all('GrpAgentCode').value==''){
		alert("集团代理人编码不能为空！操作失败！");
		return false;
	}
	if(fm.all('GrpAgentName').value==null || fm.all('GrpAgentName').value==''){
		alert("集团代理人姓名不能为空！操作失败！");
		return false;
	}
	if(fm.all('GrpAgentIDNo').value==null || fm.all('GrpAgentIDNo').value==''){
		alert("集团代理人身份证不能为空！操作失败！");
		return false;
	}
	if(fm.all('Crs_SaleChnl').value==null || fm.all('Crs_SaleChnl').value==''){
		alert("集团交叉销售渠道不能为空！操作失败！");
		return false;
	}
	
	return true;
}

function showDiv(cDiv,cShow){
		if (cShow=="true"){
				cDiv.style.display="";}else{
						cDiv.style.display="none";
				}
}
function beforeDoSave(){
	//判定是否有选择打印数据
    var count = 0;
    var tErrors = "";
	for(var i = 0; i < SetGrid.mulLineCount; i++ )
	{
		if( SetGrid.getChkNo(i) == true )
		{
			count ++;
			var tRowDatas = SetGrid.getRowData(i);
			var tBandcode = tRowDatas[4];
		    var tState = tRowDatas[13];
	    	if(tState == '1'){
		    	tErrors = tErrors + "银行代码为："+tBandcode+"的机构已确认，不能再次进行确认！\n";
		    }
		}
	}
	if(count == 0){
		alert("请选择需要处理的信息！");
		return false;
	}
	if(tErrors != ""){
		alert(tErrors);
		return false;
	}
	return true;
}