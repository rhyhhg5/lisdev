var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var strSql = "select count(1) from LCYBTCustomer where authorizeno='"+fm.AuthorizeNo.value+"'";
  var count = easyExecSql(strSql);
  if(count>0){
	  alert("已经存在的数据只能进行修改！");
	  return;
  }
  
  if(!checkAllapp()){
	   return ;
	}

  fm.OperateType.value="INSERT";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = './YBTUserSave.jsp';
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    initForm();
  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
    alert("初始化页面错误，重置出错");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
      parent.fraMain.rows = "0,0,0,0,*";
  }
   else
   {
      parent.fraMain.rows = "0,0,0,0,*";
   }
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if(!fm.AuthorizeNo.value){
		  alert("请先选择一条记录！")
		  return;
	}
    if (confirm("您确实想修改该记录吗?"))
    {
    	if(!checkAllapp()){
    		return ;
    	}
        fm.OperateType.value = "UPDATE";
        var i = 0;
        var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        showSubmitFrame(mDebug);
        fm.action = './YBTUserSave.jsp';
        
        fm.submit(); //提交
    }
    else
    {
        fm.OperateType.value = "";
        alert("您取消了修改操作！");
    }
    
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  fm.OperateType.value="QUERY";
  window.open("./FrameYBTUserQuery.jsp");
}

function deleteClick()
{
  if(!fm.AuthorizeNo.value){
	  alert("请先选择一条记录！")
	  return;
  }
  if (confirm("您确实要删除该记录吗？"))
  {
    fm.OperateType.value = "DELETE";
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或连接其他的界面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    showSubmitFrame(mDebug);
    fm.action = './YBTUserSave.jsp';

    fm.submit();//提交
  }
  else
  {
    fm.OperateType.value = "";
    alert("您已经取消了删除操作！");
  }
}



function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function afterQueryUserInfo(tUserCode)
{
    var strSQL = "select authorizeno,name,sex,idtype,idno,effectivedate "
        + "from LCYBTCustomer where authorizeno = '" + tUserCode + "'";
    var arr = easyExecSql(strSQL);
    if(arr)
    {
        fm.AuthorizeNo.value = arr[0][0];
        fm.Name.value = arr[0][1];
        fm.Sex.value = arr[0][2];
        if(fm.Sex.value == "0")
            fm.SexName.value = "男";
        else if(fm.Sex.value == "1")
        	fm.SexName.value = "女";
        fm.IDType.value = arr[0][3];
        if(fm.IDType.value == "0")
        	fm.IDTypeName.value = "身份证";
        else
        	fm.IDTypeName.value = "其他";
        fm.IDNo.value = arr[0][4];
        fm.EffectiveDate.value = arr[0][5];
       
    }
}

function checkAllapp(){
	   var idtype = fm.IDType.value;
	   var idno = fm.IDNo.value;
	  
	   if((fm.Name.value=="")||(fm.Name.value=="null"))
	   {
	     alert("请您录入姓名！！！");
	     return ;
	   }
	   if((fm.IDType.value=="")||(fm.IDType.value=="null"))
	   {
	     alert("请您录入证件类型！！！");
	     return ;
	   }
	   if((fm.IDNo.value=="")||(fm.IDNo.value=="null"))
	   {
	     alert("请您录入证件号！！！");
	     return ;
	   }   
	   if(!chenkIdNo(idtype,idno,'')){
		 return false;
	   }
	    
	   return true;
}

function chenkIdNo(idtype,idno,name){
    if(idtype == "" || idtype == null){
         alert("证件类型不能为空！");
         return false;
    }else{
         var arr = easyExecSql("select code1,codealias,othersign from ldcode1 where codetype='checkidtype' and code='"+idtype+"'");
         if(arr!=null){
            var othersign = arr[0][2];
            var first = arr[0][0];
            var two = arr[0][1];
            if("double"==othersign){
               if(idno.length!=first&&idno.length!=two){
                   alert("输入的身份证号位数错误，请检查！");
                   return false;
               }
               if(idno.length==18){
             	  var tSql = "select 1 from ldcode where codetype='idnolast' and code='"+idno.substring(17,18)+"'";
             	  var tResult = easyExecSql(tSql, 1, 0, 1);
             	  if(tResult==null||tResult==""){
             		alert("输入的18位身份证号最后一位错误，只能为数字或大写字母X");
             		return false;
             	  }
             	  if(!isNumer(idno.substring(0,idno.length-1))){
             		  	alert("输入的18位身份证号除最后一位其他必须为数字");
             		    return false;	
             	  }
               }
               if(idno.length==15){
             	  if(!isNumer(idno.substring(0,idno.length))){
             		  	alert("输入的15位身份证号必须为数字");
             		    return false;	
             	  }
               }
               var tSql = "select 1 from ldcode where codetype='idnoprovince' and code='"+idno.substring(0,2)+"'";
               var tResult = easyExecSql(tSql, 1, 0, 1);

               if(tResult==null){
             	  alert("输入的身份证号前两位不是有效的省份代码");
             	  return strReturn;
               }
            }
            if("min"==othersign){
               if(idno.length<first){
                  alert("录入的证件号码位数错误，请检查！");
                   return false;
               }
            }
             
         }else{
            alert("选择的证件类型有误，请检查！");
            return false;
         }
    }
    
    return true;
}

function isNumer(strValue)
{
  var NUM="0123456789";
  var i;
  if(strValue==null ||strValue=="") return false;
  for(i=0;i<strValue.length;i++)
  {
    if(NUM.indexOf(strValue.charAt(i))<0) return false
  }
  return true;
}


