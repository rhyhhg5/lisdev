<%
  //程序名称：ybtManageInputInit.jsp
  //程序功能：
  //创建日期：2005-08-29 15:12:33
  //创建人  ：liuyx程序创建
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">
var manageCom = '<%= globalInput.ManageCom %>';
var comcode = '<%= globalInput.ComCode %>';
function initInpBox()
{
  try
  {
    fm.all('BankCode').value = '';
    fm.all('ZoneNo').value = '';
    fm.all('BankNode').value = '';
    fm.all('Remark5').value = '';
    fm.all('NodeType').value = '';
    fm.all('UpAgentCom').value = '';
    fm.all('ManageComName').value ='';
    fm.all('BankCodeName').value ='';
  //  fm.all('UpComName').value = '';
    fm.all('ComCode').value = '';
    fm.all('ManageCom').value = '';
    fm.all('ManageCom1').value = '';
    fm.all('Remark').value = '';
    fm.all('Operator').value = '';
    fm.all('Remark1').value = '';
    fm.all('Remark3').value = '';
    fm.FTPAddress.value='';
        fm.FTPDir.value='';
        fm.FTPUser.value='';
        fm.FTPPassWord.value='';
       
    divYes.style.display="none";
    divNo.style.display="none";
  

  }
  catch(ex)
  {
    alert("在LAComInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  
  }
  catch(ex)
  {
    alert("在LAComInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    
    initInpBox();    
    
    initSelBox();
    initLKCodeGrid();
    
    
  }
  catch(re)
  {
    alert("LAComInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LKCodeGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initLKCodeGrid()
{
    var iArray = new Array();
    //var i11Array = getAgentGradeStr();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="银行代码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="地区代码";         		//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="网点代码";         		//列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="机构代码";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="机构名称";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="登陆机构";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="登陆机构名称";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[8]=new Array();
      iArray[8][0]="管理机构";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[9]=new Array();
      iArray[9][0]="管理机构名称";         		//列名
      iArray[9][1]="100px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[10]=new Array();
      iArray[10][0]="日终对帐映射地址";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;


      iArray[11]=new Array();
      iArray[11][0]="划款银行编码";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;

      iArray[12]=new Array();
      iArray[12][0]="对帐网点编码";         		//列名
      iArray[12][1]="40px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=0;

      iArray[13]=new Array();
      iArray[13][0]="对帐地区编码";         		//列名
      iArray[13][1]="40px";            		//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=0;

      iArray[14]=new Array();
      iArray[14][0]="FTP地址";         		//列名
      iArray[14][1]="100px";            		//列宽
      iArray[14][2]=200;            			//列最大值
      iArray[14][3]=0;


      iArray[15]=new Array();
      iArray[15][0]="FTP目录";         		//列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][2]=200;            			//列最大值
      iArray[15][3]=0;

      iArray[16]=new Array();
      iArray[16][0]="FTP用户";         		//列名
      iArray[16][1]="40px";            		//列宽
      iArray[16][2]=200;            			//列最大值
      iArray[16][3]=0;

      iArray[17]=new Array();
      iArray[17][0]="FTP密码";         		//列名
      iArray[17][1]="40px";            		//列宽
      iArray[17][2]=200;            			//列最大值
      iArray[17][3]=0;
      
      iArray[18]=new Array();
      iArray[18][0]="网点类别";         		//列名
      iArray[18][1]="40px";            		//列宽
      iArray[18][2]=200;            			//列最大值
      iArray[18][3]=0;      
      
      LKCodeGrid = new MulLineEnter( "fm" , "LKCodeGrid" );
      //这些属性必须在loadMulLine前
      LKCodeGrid.displayTitle = 1;
      LKCodeGrid.locked = 0;
      LKCodeGrid.mulLineCount = 1;
      LKCodeGrid.hiddenPlus = 1;
      LKCodeGrid.hiddenSubtraction = 1;
      LKCodeGrid.canSel = 1;

      LKCodeGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //LAComGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
