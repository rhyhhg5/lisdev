<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="java.io.*"%>
<%
  response.setContentType("image/gif");
  ServletOutputStream out1 = response.getOutputStream();
  String codeStr = request.getParameter("CODE");
  BarCode bcode = new BarCode(codeStr);
  bcode.setFormatType(BarCode.FORMAT_GIF);
  String strParameter = request.getQueryString();
  //System.out.println(UrlDecode.urlEncode("#"));
  String params[] = strParameter.split("&");
  String t_Str = "";
    System.out.println(strParameter);
    System.out.println(params.length);
  //获得条形码参数
  for (int j = 0; j < params.length; j++) {
    //获得条形码宽度
    System.out.println(params[j]);
    if (params[j].toLowerCase().startsWith("barheight")) {
      t_Str = params[j].split("=")[1];
      if (JRptUtility.IsNumeric(t_Str)) {
        bcode.setBarHeight(Integer.parseInt(t_Str));
      }
    }
    //获得条形码宽度
    if (params[j].toLowerCase().startsWith("barwidth")) {
      t_Str = params[j].split("=")[1];
      if (JRptUtility.IsNumeric(t_Str)) {
        bcode.setBarWidth(Integer.parseInt(t_Str));
      }
    }
    //获得条形码粗细线条比例
    if (params[j].toLowerCase().startsWith("barratio")) {
      t_Str = params[j].split("=")[1];
      if (JRptUtility.IsNumeric(t_Str)) {
        bcode.setBarRatio(Integer.parseInt(t_Str));
      }
    }
    //获得条形码图片背景色
    if (params[j].toLowerCase().startsWith("bgcolor")) {
      t_Str = params[j].split("=")[1];
      bcode.setBgColor(t_Str);
    }
    //获得条形码颜色
    if (params[j].toLowerCase().startsWith("forecolor")) {
      t_Str = params[j].split("=")[1];
      bcode.setForeColor(t_Str);
    }
    //获得条形码图片横向空白区长度
    if (params[j].toLowerCase().startsWith("xmargin")) {
      t_Str = params[j].split("=")[1];
      if (JRptUtility.IsNumeric(t_Str)) {
        bcode.setXMargin(Integer.parseInt(t_Str));
      }
    }
    //获得条形码图片竖向空白区长度
    if (params[j].toLowerCase().startsWith("ymargin")) {
      t_Str = params[j].split("=")[1];
      if (JRptUtility.IsNumeric(t_Str)) {
        bcode.setYMargin(Integer.parseInt(t_Str));
      }
    }
  }
  bcode.getOutputStream((OutputStream) out1);
  out1.close();
%>
