var showInfo;

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	if (fm.ReComCode.value==""||fm.ReComCode.value==null)
	{
	}
	else
	{
		alert("此合同已经保存！");
		return false;
	}
	if(fm.ReComName.value==""||fm.ReComName.value==null)
	{
		alert("请录入公司名称！");
		return false;
	}
	if (!isTel(fm.FaxNo.value))
	{
		alert("传真录入格式不对！");
		return false;
	}
	if (!isWeb(fm.WebAddress.value))
	{
		alert("网址录入格式不对！");
		return false;
	}
	if (!isInteger1(fm.PostalCode.value))
	{
		alert("邮政编码录入格式不对！");
		return false;
	}
	
	var rowNum=RelateGrid. mulLineCount ; //行数 
	
	for(var i=0;i<rowNum;i++)
	{
		num=i+1;
		if(RelateGrid.getRowColData(i,1)==0||RelateGrid.getRowColData(i,1)==null)
		{
			alert("第"+num+"行,请录入联系人姓名！");
			return false;
		}
		
		if(!isTel(RelateGrid.getRowColData(i,4)))
		{
			alert("第"+num+"行,电话录入不对！");
			return false;
		}
		if(!isTel(RelateGrid.getRowColData(i,5)))
		{
			alert("第"+num+"行,手机录入不对！");
			return false;
		}
		if(!isTel(RelateGrid.getRowColData(i,6)))
		{
			alert("第"+num+"行,传真录入不对！");
			return false;
		}
		if(!isEMail(RelateGrid.getRowColData(i,7)))
		{
			alert("第"+num+"行,电子邮箱录入不对！");
			return false;
		}
	}
	
	return true;
}

function queryClick()
{
  fm.OperateType.value="QUERY";
  
  window.open("./FrameReComQuery.jsp?Serial="+fm.ReComCode.value);
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  resetForm();
  }
}

function updateClick()
{
	fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	if (fm.ReComCode.value==""||fm.ReComCode.value==null)
	{
		alert("请先查询公司信息！");
		return false;
	}
	var strSQL = "select * from LRReComInfo where ReComCode='"+fm.ReComCode.value+"'";
	var strResult = easyExecSql(strSQL);
	
	if (strResult == null)
	{
		alert("该再保公司不存在！");
		return false;
	}
	if(fm.ReComName.value==""||fm.ReComName.value==null)
	{
		alert("请录入公司名称！");
		return false;
	}
	if (!isTel(fm.FaxNo.value))
	{
		alert("传真录入格式不对！");
		return false;
	}
	if (!isWeb(fm.WebAddress.value))
	{
		alert("网址录入格式不对！");
		return false;
	}
	if (!isInteger1(fm.PostalCode.value))
	{
		alert("邮政编码录入格式不对！");
		return false;
	}
	
	var rowNum=RelateGrid. mulLineCount ; //行数 
	
	for(var i=0;i<rowNum;i++)
	{
		num=i+1;
		if(RelateGrid.getRowColData(i,1)==0||RelateGrid.getRowColData(i,1)==null)
		{
			alert("第"+num+"行,请录入联系人姓名！");
			return false;
		}
		
		if(!isTel(RelateGrid.getRowColData(i,4)))
		{
			alert("第"+num+"行,电话录入不对！");
			return false;
		}
		if(!isTel(RelateGrid.getRowColData(i,5)))
		{
			alert("第"+num+"行,手机录入不对！");
			return false;
		}
		if(!isTel(RelateGrid.getRowColData(i,6)))
		{
			alert("第"+num+"行,传真录入不对！");
			return false;
		}
		if(!isEMail(RelateGrid.getRowColData(i,7)))
		{
			alert("第"+num+"行,电子邮箱录入不对！");
			return false;
		}
	}
	return true;
	
}

function deleteClick()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该批次吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	if (fm.ReComCode.value==""||fm.ReComCode.value==null)
	{
		alert("请先查询需要删除的公司信息");
		return false;
	}	
	return true;
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

 /**
   * 对输入域是否是电话的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isTel(strValue)
{
  var NUM="0123456789-() ";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(NUM.indexOf(strValue.charAt(i))<0) return false;

  }
  return true;
}

/**
   * 对输入域是否是网址的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isWeb(strValue)
{
  var NUM=".";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(strValue.indexOf(NUM)<0)
    {
    	return false;
    }
  }
  return true;
}

 /**
   * 对输入域是否是邮编的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isInteger1(strValue)
{
  var NUM="0123456789";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(NUM.indexOf(strValue.charAt(i))<0) return false;

  }
  return true;
}

/**
   * 对输入域是否是邮箱的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isEMail(strValue)
{
  var NUM1="@";
  var NUM2=".";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(strValue.indexOf(NUM1)<0)
    {
    	return false;
    }
    if(strValue.indexOf(NUM2)<0)
    {
    	return false;
    }
  }
  return true;
}