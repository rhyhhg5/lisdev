var showInfo;

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if(fm.ModifyFlag.value=="null"||fm.ModifyFlag.value==null) 
	{
		var tSel=PolGrid.getSelNo();
		if( tSel == 0 || tSel == null )
		{
			alert( "请先选择一条记录。" );
			return false;
		}
	}
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true && PolGrid.checkValue("PolGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./CessConclusionSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{            
	var strSQL="";
	var arrResult="";

	//校验保单是否签单 
	if(fm.ModifyFlag.value==null||fm.ModifyFlag.value=="null"||fm.ModifyFlag.value=="1") //如果是个单
	{
		var tSel=PolGrid.getSelNo();
		if(PolGrid.getRowColData(tSel-1,7)==""||PolGrid.getRowColData(tSel-1,7)==null)
		{
			alert("没有保单险种号！");
			return false;
		}
		strSQL="select appflag from LCPOL where proposalno='"+PolGrid.getRowColData(tSel-1,7)+"' ";
		arrResult=easyExecSql(strSQL);
		if(arrResult==null)
		{
				alert("没有查询到保单状态！");
				return false;
		}
		if(arrResult[0]=='0')
		{
				alert("保单未签单，不能下临分结论！");
				return false;
		}
	}
	else //如果是团单下个人
	{
		if(PolGrid.getRowColData(0,7)==""||PolGrid.getRowColData(0,7)==null)
		{
			alert("没有保单险种号！");
			return false;
		}
		strSQL="select appflag from LCPOL where proposalno='"+PolGrid.getRowColData(0,7)+"' ";
		arrResult=easyExecSql(strSQL);
		if(arrResult==null)
		{
				alert("没有查询到保单状态！");
				return false;
		}
		if(arrResult[0]=='0')
		{
				alert("保单未签单，不能下临分结论！");
				return false;
		}
	}
	
	var polCount = PolGrid.mulLineCount;
	
	if(fm.ModifyFlag.value=="null"||fm.ModifyFlag.value=="1"||fm.ModifyFlag.value==null) //个单保存，修改
	{
		var tSel=PolGrid.getSelNo();
		
		for(var i=0;i<polCount;i++)
		{
			if(tSel-1==i)
			{
				if(PolGrid.getRowColData(tSel-1,17)==""||PolGrid.getRowColData(tSel-1,17)==null)
				{
					alert("第"+tSel+"行临分结论不能为空！");
					return false;
				}
				
				if(PolGrid.getRowColData(tSel-1,17)=="3") //如果是临分
				{
					if(PolGrid.getRowColData(tSel-1,8)==""||PolGrid.getRowColData(tSel-1,8)==null)
					{
						alert("请录入被保险人:"+PolGrid.getRowColData(tSel-1,1)+"的分保保费计算方式!");
						return false;
					}
					if(PolGrid.getRowColData(tSel-1,8)=="C")
					{
						if((PolGrid.getRowColData(tSel-1,9)==""||PolGrid.getRowColData(tSel-1,9)==null)
							&&(PolGrid.getRowColData(tSel-1,10)==""||PolGrid.getRowColData(tSel-1,10)==null))
						{
							alert("请录入被保险人:"+PolGrid.getRowColData(tSel-1,1)+"的自留额或分保比例!");
							return false;
						}
						
						if((PolGrid.getRowColData(tSel-1,9)!=""&&PolGrid.getRowColData(tSel-1,9)!=null&&PolGrid.getRowColData(tSel-1,9)!="0")
							&&(PolGrid.getRowColData(tSel-1,10)!=""&&PolGrid.getRowColData(tSel-1,10)!=null)&&PolGrid.getRowColData(tSel-1,10)!="0")
						{
							alert("自留额与分保比例不能同时录入!"); 
							return false; 
						}
						
					}
					if(PolGrid.getRowColData(tSel-1,8)=="S")
					{
						if(PolGrid.getRowColData(tSel-1,11)==""||PolGrid.getRowColData(tSel-1,11)==null||PolGrid.getRowColData(tSel-1,12)==""||PolGrid.getRowColData(tSel-1,12)==null)
						{
							alert("请录入被保险人:"+PolGrid.getRowColData(tSel-1,1)+"的分出保额和分出保费!");
							return false;
						}
					}
					if(PolGrid.getRowColData(tSel-1,9)!=""&&PolGrid.getRowColData(tSel-1,9)!=null)
					{
						if (parseFloat(PolGrid.getRowColData(tSel-1,9))>parseFloat(PolGrid.getRowColData(tSel-1,6)))
						{
							alert("自留额大于风险保额！");
							return false;
						}
					}
					if(PolGrid.getRowColData(tSel-1,10)!=""&&PolGrid.getRowColData(tSel-1,10)!=null)
					{
						if (parseFloat(PolGrid.getRowColData(tSel-1,10))>1)
						{
							alert("分保比例不能大于1！");
							return false;
						}
					}
					if(PolGrid.getRowColData(tSel-1,13)!=""&&PolGrid.getRowColData(tSel-1,13)!=null)
					{
						if (parseFloat(PolGrid.getRowColData(tSel-1,13))>1)
						{
							alert("手续费比例不能大于1！");
							return false;
						}
					}
				}
			}
		}
	}else //团单下个人保存，修改
	{
		var polCount = PolGrid.mulLineCount;
		for(var i=0;i<polCount;i++)
		{
			if(PolGrid.getRowColData(i,17)=="3") //如果是临分
			{
				if(PolGrid.getRowColData(i,8)==""||PolGrid.getRowColData(i,8)==null)
				{
					alert("请录入被保险人:"+PolGrid.getRowColData(i,1)+"的分保保费计算方式!");
					return false;
				}
				if(PolGrid.getRowColData(i,8)=="C")
				{
					if((PolGrid.getRowColData(i,9)==""||PolGrid.getRowColData(i,9)==null)&&(PolGrid.getRowColData(i,10)==""||PolGrid.getRowColData(i,10)==null))
					{
						alert("请录入被保险人:"+PolGrid.getRowColData(i,1)+"的自留额或分保比例!");
						return false;
					}
				}
				if(PolGrid.getRowColData(i,8)=="S")
				{
					if(PolGrid.getRowColData(i,11)==""||PolGrid.getRowColData(i,11)==null||PolGrid.getRowColData(i,12)==""||PolGrid.getRowColData(i,12)==null)
					{
						alert("请录入被保险人:"+PolGrid.getRowColData(i,1)+"的分出保额和分出保费!");
						return false;
					}
				}
				if(PolGrid.getRowColData(i,10)!=""&&PolGrid.getRowColData(i,10)!=null)
				{
					if (parseFloat(PolGrid.getRowColData(i,10))>1)
					{
						alert("分保比例不能大于1！");
						return false;
					}
				}
				if(PolGrid.getRowColData(i,13)!=""&&PolGrid.getRowColData(i,13)!=null)
				{
					if (parseFloat(PolGrid.getRowColData(i,13))>1)
					{
						alert("手续费比例不能大于1！");
						return false;
					}
				}
			}
			for(var n=0;n<PolGrid.mulLineCount;n++) 
			{ 
			   for(var m=n+1;m<PolGrid.mulLineCount;m++) 
			   { 
			     if(PolGrid.getRowColData(n,1)==PolGrid.getRowColData(m,1)) 
			     {
			         alert("不能录入重复的被保险人！");	
			         return false; 
			     }
			   }
			}
		}
	}
	return true;
}

//当前核保记录
function showNewUWSub() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
  cContNo=fm.ProposalContNo.value;
	
  //window.open("./PUWErrMain.jsp?ProposalNo1="+cProposalNo,"window1");
  window.open("../uw/UWErrMain.jsp?ContNo="+cContNo,"window1");
  showInfo.close();
}

function UWQuery()
{
	var strProposalNo = "''";
	for(var i=0;i<PolGrid.mulLineCount;i++)
	{
		strProposalNo = strProposalNo+",'"+PolGrid.getRowColData(i,7)+"'"
	}
	window.open("FrameUWInfo.jsp?ProposalNos="+strProposalNo+"&PageFlag=1","window2");
}

function easyQuery()
{
	var strPol = "''"; //''是拼sql的条件字段
	var strSQL = ""; 
	if(fm.LoadFlag.value=='6') //如果是个单临分处理
	{
		if(fm.ModifyFlag.value=='1') //个单修改
		{
			if(fm.ReContCode.value==null||fm.ReContCode.value=="")
			{
				strSQL="select a.insuredno,a.insuredname,a.riskcode,a.amnt,a.prem,a.riskamnt,a.ProposalNo,''"
				+ "from LCPol a where  Reinsureflag in ('3','4','5') and prtno='"+fm.PrtNo.value+"' "
				;
				arrResult = easyExecSql(strSQL);  
				if(arrResult==null)
				{
					return false;
				}
				displayMultiline(arrResult,PolGrid);
			}
			else
			{
				strSQL = "select (select insuredno from lcpol where proposalno=a.proposalno),"
				+ " (select insuredname from lcpol where proposalno=a.proposalno),riskcode,"
				+ " (select amnt from lcpol where proposalno=a.proposalno),"
				+ " (select prem from lcpol where proposalno=a.proposalno),"
				+ " (select riskamnt from lcpol where proposalno=a.proposalno),a.ProposalNo,a.CalMode,Retainamnt,CessionRate,"
				+ " CessionAmount,CessionFee,ReinProcFeeRate,DeadAddFee,DiseaseAddFee,ReUWConlcusion,"
				+ " (select ReinsureFlag from lcpol where proposalno=a.proposalno)"
				+ " from LRTempCessContInfo a where recontcode='"+fm.ReContCode.value+"' "
				;
				arrResult = easyExecSql(strSQL);
				if(arrResult==null)
				{
					return false;
				}
				displayMultiline(arrResult,PolGrid);
			}
		}else if(fm.ModifyFlag.value=='2') //团单下个人修改
		{
			if(fm.ReContCode.value!=null&&fm.ReContCode.value!="")
			{
				 strSQL = "select a.insuredno,a.insuredname,a.riskcode,a.amnt,a.prem,a.riskamnt,a.ProposalNo,"
				 	+" (select CalMode from LRTempCessContInfo where a.proposalno=proposalno and recontcode='"+fm.ReContCode.value+"' ),"
					+" (select Retainamnt from LRTempCessContInfo where a.proposalno=proposalno and recontcode='"+fm.ReContCode.value+"' ),"
					+" (select CessionRate from LRTempCessContInfo where a.proposalno=proposalno and recontcode='"+fm.ReContCode.value+"'),"
					+" (select CessionAmount from LRTempCessContInfo where a.proposalno=proposalno and recontcode='"+fm.ReContCode.value+"'),"
					+" (select CessionFee from LRTempCessContInfo where a.proposalno=proposalno and recontcode='"+fm.ReContCode.value+"'),"
					+" (select ReinProcFeeRate from LRTempCessContInfo where a.proposalno=proposalno and recontcode='"+fm.ReContCode.value+"'),"
				  +"(select DeadAddFee from LRTempCessContInfo where a.proposalno=proposalno and recontcode='"+fm.ReContCode.value+"'),"
				  +"(select DiseaseAddFee from LRTempCessContInfo where a.proposalno=proposalno and recontcode='"+fm.ReContCode.value+"'),"
				  +"(select ReUWConlcusion from LRTempCessContInfo where a.proposalno=proposalno and recontcode='"+fm.ReContCode.value+"'),"
				  +"(select ReinsureFlag from LCPol where a.proposalno=proposalno) "
				  +"from LCPol a where  Reinsureflag ='3' and prtno='"+fm.PrtNo.value+"' and RiskCode='"+fm.RiskCode.value+"'"
				  +" and proposalno in (select proposalno from LRTempCessContInfo where recontcode='"+fm.ReContCode.value+"')"
					;
					arrResult = easyExecSql(strSQL);
					if(arrResult==null)
					{
						return false;
					}
					displayMultiline(arrResult,PolGrid);
			}
			else //个单保存
			{
				
			}
		}	
		else if(fm.ModifyFlag.value=='3') //团单下个人保存
		{
			
		}	
		else	//个单保存,也就是fm.ModifyFlag.value == null
		{
			strSQL = "select insuredno,insuredname,riskcode,amnt,prem,riskamnt,ProposalNo "
			+ " from LCPol where Reinsureflag='2' and prtno='" + fm.PrtNo.value + "'"
			;
			arrResult = easyExecSql(strSQL);
			if(arrResult!=null)
			{
				displayMultiline(arrResult,PolGrid);
			}
		}
	}else if(fm.LoadFlag.value=='7') //如果是团单临分处理            
	{
		polCount=top.opener.SelPolGrid.mulLineCount;
		for(var i=0;i<polCount;i++)
		{
			strPol = strPol+",'"+top.opener.SelPolGrid.getRowColData(i,6)+"'";
		}
		strSQL = "select insuredno,insuredname,riskcode,amnt,prem,riskamnt,ProposalNo " 
		+ " from LCPol where ProposalNo in ("+strPol+")"
		;
		arrResult = easyExecSql(strSQL);
		displayMultiline(arrResult,PolGrid);
	}
	//turnPage.queryModal(strSQL, PolGrid); 
	handleMulLine();
}

function handleMulLine()
{
	for(var i=0;i<PolGrid.mulLineCount;i++)
	{
		for(var j=1;j<=17;j++)
		{
			if(PolGrid.getRowColData(i,j)==null||PolGrid.getRowColData(i,j)==""||PolGrid.getRowColData(i,j)=="null")
			{
				PolGrid.setRowColData(i,j,"");
			}
		}
	}
}
function queryClick()
{
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else {
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  
		fm.reset();
		//if(fm.ModifyFlag.value!="null"||fm.ModifyFlag.value!=null)
		//{
		//	document.all.ReContCode.readOnly="true";
		//}
	  if(fm.ModifyFlag.value!="3") //团单下个人保存
	  {
		  initForm();
	  	fillCessInfo();
		}
		
  }
}

function fillCessInfo()
{
	var strStr="";
	var arrResult;
	var polCount = PolGrid.mulLineCount;
	for(var i=0;i<polCount;i++)
	{
		if(PolGrid.getRowColData(i,17)=="3")
		{
			strStr="select CessionAmount,CessionFee from LRTempCessContInfo where proposalno='"+PolGrid.getRowColData(i,7)
			+"' and ReContCode='"+fm.ReContCode.value+"'";
			arrResult=easyExecSql(strStr);
			if(arrResult!=null)
			{
				if(arrResult[0][0]=="0"||arrResult[0][0]==0)
				{
					PolGrid.setRowColData(i,11,"");
				}else
				{
					PolGrid.setRowColData(i,11,arrResult[0][0]);
				}
				PolGrid.setRowColData(i,12,arrResult[0][1]);
			}
		}
	}
}

function updateClick()
{
}

function veriryInput4() //UPDATE 校验
{
	return true;
}

function deleteClick()
{
	
}

function veriryInput5()
{
	return true;
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function initInfo() 
{  
}

//个单修改时填充mulLine再保信息部分
function initModify() 
{
	document.all.ReContCode.readOnly="true";
	var strSQL="";
	var arrResult="";
	//显示临分协议信息
	strSQL="select a.recontcode,a.recontname,a.ReComCode,(select RecomName from LRReComInfo where ReComCode=a.ReComCode),a.inputdate from LRTempCessCont a "
		+ " where prtno='"+fm.PrtNo.value+"'"
		;
	arrResult=easyExecSql(strSQL);
	if(arrResult==null)
	{
		alert("没有该保单的临分协议！");
		for(var i=0;i<PolGrid.mulLineCount;i++)
		{
			strSQL="select ReinsureFlag from lcpol where ProposalNo='"+PolGrid.getRowColData(i,7)+"'";
			arrResult=easyExecSql(strSQL);
			PolGrid.setRowColData(i,17,arrResult[0][0]);
		}
		return false;
	}else
	{
		//if(fm.ModifyFlag.value=="3"||fm.ModifyFlag.value=="1")
		//{
		//	fm.ReContCode.value	=arrResult[0][0];
		//	fm.ReContName.value	=arrResult[0][1];
		//	fm.ReComCode.value	=arrResult[0][2];
		//	fm.ReComName.value	=arrResult[0][3];
		//}
		
		for(var i=0;i<PolGrid.mulLineCount;i++)
		{
			strSQL = "select CalMode,RetainAmnt,CessionRate,CessionAmount,CessionFee,ReinProcFeeRate,DeadAddFee,DiseaseAddFee,ReUWConlcusion,TempCessConclusion "
			+ " from LRTempCessContInfo where ProposalNo='"+PolGrid.getRowColData(i,7)+"'";
			arrResult=easyExecSql(strSQL);
			if(arrResult!=null)
			{
				PolGrid.setRowColData(i,8,arrResult[0][0]);
				PolGrid.setRowColData(i,9,arrResult[0][1]);
				PolGrid.setRowColData(i,10,arrResult[0][2]);
				PolGrid.setRowColData(i,11,arrResult[0][3]);
				PolGrid.setRowColData(i,12,arrResult[0][4]);
				PolGrid.setRowColData(i,13,arrResult[0][5]);
				PolGrid.setRowColData(i,14,arrResult[0][6]);
				PolGrid.setRowColData(i,15,arrResult[0][7]);
				PolGrid.setRowColData(i,16,arrResult[0][8]);
				PolGrid.setRowColData(i,17,arrResult[0][9]);
			}	
		}
	}
}

//个单保存
function initModifyForm()
{
	var tSel=PolGrid.getSelNo();
	
	var strSQL="";
	var arrResult="";
	if(fm.ModifyFlag.value=="1")
	{
		strSQL="select a.recontcode,a.recontname,a.ReComCode,(select RecomName from LRReComInfo where ReComCode=a.ReComCode),a.inputdate from LRTempCessCont a "
			+ " where prtno='"+fm.PrtNo.value+"' and a.RecontCode="
			+ " (select distinct Recontcode from LRTempCessContInfo where ProposalNo='"+PolGrid.getRowColData(tSel-1,7)+"')"
			;
		arrResult=easyExecSql(strSQL);
		if(arrResult==null)
		{
			alert("没有该保单的临分协议！");
			for(var i=0;i<PolGrid.mulLineCount;i++)
			{
				strSQL="select ReinsureFlag from lcpol where ProposalNo='"+PolGrid.getRowColData(i,7)+"'";
				arrResult=easyExecSql(strSQL);
				PolGrid.setRowColData(i,17,arrResult[0][0]);
			}
			return false;
		}else
		{
			//再保协议信息
			fm.ReContCode.value	=arrResult[0][0];
			fm.ReContName.value	=arrResult[0][1];
			fm.ReComCode.value	=arrResult[0][2];
			fm.ReComName.value	=arrResult[0][3];
			document.all.ReContCode.readOnly="true";
		}
	}
}

//修改团单下个人
function initModifyGrpInd()
{
	var strSQL="";
	var arrResult="";
	
	//得到保单信息
	strSQL="select PrtNo,GrpName,ManageCom from LCgrppol where Grpproposalno='"+fm.GrpProposalNo.value+"'";
	arrResult=easyExecSql(strSQL);
	if(arrResult!=null)
	{
		fm.PrtNo.value=arrResult[0][0];
		fm.AppntName.value=arrResult[0][0];
		fm.ManageCom.value=arrResult[0][0];
	}
	
	//得到再保协议信息,!!CalMode非空的是个单
	if(fm.ModifyFlag.value=='2')
	{
		strSQL="select a.RecontCode,a.RecontName,a.RecomCode,(select RecomName from LRReComInfo where RecomCode=a.RecomCode)"
			+ "	from LRTempCessCont a where a.RecontCode= (select distinct RecontCode from LRTempCessContInfo "
			+ " where PrtNo='"+fm.PrtNo.value+"' and RiskCode='"+fm.RiskCode.value+"' and CalMode is not null)"
			;
		arrResult=easyExecSql(strSQL);
		if(arrResult!=null)
		{
			fm.ReContCode.value=arrResult[0][0];
			fm.ReContName.value=arrResult[0][1];
			fm.ReComCode.value=arrResult[0][2];
			fm.ReComName.value=arrResult[0][3];
			document.all.ReContCode.readOnly="true";
		}else
		{
			alert("没有临分协议");
			return false;
		}
	}
}

function divTempContQuery()
{
	var prtNo=fm.PrtNo.value;
	var grpProposalNo = fm.GrpProposalNo.value;
	var modifyFlag = fm.ModifyFlag.value;
	var riskCode = fm.RiskCode.value;
	window.open("./FrameTempRecontQuery.jsp?LoadFlag=6&GrpProposalNo="+grpProposalNo+"&ModifyFlag=2&RiskCode="+riskCode+"&PrtNo="+prtNo,"CessConclusion");
}

function returnClick()
{
	if(fm.ModifyFlag.value=='3')
	{
		top.opener.top.close();
	  top.close();
	}else
	{
		top.close();
	}
}

function UWAnswer()
{
	//if(fm.ModifyFlag.value=="null"||fm.ModifyFlag.value==null)
	//{
	//	var tSel=PolGrid.getSelNo();
	//	if( tSel == 0 || tSel == null )
	//	{
	//		alert( "请先选择一条记录。" );
	//		return false;
	//	}
	//}
	//var proposalNo=PolGrid.getRowColData(tSel-1,7)
	
	window.open("./FrameUWAnser.jsp?PrtNo="+fm.PrtNo.value,"ReinsureAnswer");
}