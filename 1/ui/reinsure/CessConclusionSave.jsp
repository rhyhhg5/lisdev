<%
//程序名称：CessConclusionSave.jsp
//程序功能：
//创建日期：2006-09-06
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.util.*"%>
	
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  System.out.println("开始执行Save页面1");
  LRTempCessContSchema mLRTempCessContSchema = new LRTempCessContSchema();
 	LRTempCessContInfoSet mLRTempCessContInfoSet = new LRTempCessContInfoSet();
 	LCPolSet mLCPolSet = new LCPolSet();
 	
  System.out.println("开始执行Save页面2");
  TempCessContUI mTempCessContUI = new TempCessContUI();
  
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  String mModifyFlag	= request.getParameter("ModifyFlag");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  		= "";
  String FlagStr 		= "";
  String Content 		= "";
  String mDescType 	= ""; //将操作标志的英文转换成汉字的形式
  
  System.out.println("开始进行获取数据的操作！！！");
  String strProposalContNo = request.getParameter("ProposalContNo") ;

  String[] strNumber 			 				= request.getParameterValues("PolGridNo");
  String[] strInsuredNo  					= request.getParameterValues("PolGrid1");
  String[] strRiskCode  					= request.getParameterValues("PolGrid3");
  String[] strProposalNo					= request.getParameterValues("PolGrid7");
  String[] strCalMode		 	 				= request.getParameterValues("PolGrid8");  
  String[] strRetainAmnt	 				= request.getParameterValues("PolGrid9");
  String[] strCessionRate  				= request.getParameterValues("PolGrid10");
  
  String[] strCessionAmnt	 				= request.getParameterValues("PolGrid11");
  String[] strCessionFee  				= request.getParameterValues("PolGrid12");
  
  String[] strReinProcFeeRate 		= request.getParameterValues("PolGrid13");
  String[] strDeadAddFee		  		= request.getParameterValues("PolGrid14");
  String[] strDiseaseAddFee				= request.getParameterValues("PolGrid15");
  String[] strReUWConlcusion			= request.getParameterValues("PolGrid16");
  String[] strTempCessConclusion	= request.getParameterValues("PolGrid17");
  
	System.out.println("in jsp1 ->before submitData..........");    
	//临分协议信息
	mLRTempCessContSchema.setReContCode(		request.getParameter("ReContCode"));
	mLRTempCessContSchema.setReContName(		request.getParameter("ReContName"));
	mLRTempCessContSchema.setReComCode(			request.getParameter("ReComCode"));
	mLRTempCessContSchema.setInputDate(			request.getParameter("InputDate"));
	mLRTempCessContSchema.setPrtNo(					request.getParameter("PrtNo"));
	
	ArrayList proposalList = new ArrayList();
/*************************************************************************/  

	if(mModifyFlag==null||mModifyFlag.equals("1")||mModifyFlag.equals("null")) //个单保存、修改
	{
	  String tRadio[] = request.getParameterValues("InpPolGridSel"); 
	  for (int index=0; index<tRadio.length;index++)
	  {
		  if(tRadio[index].equals("1"))
			{
				LCPolSchema tLCPolSchema = new LCPolSchema();
		  	if (strTempCessConclusion[index].equals("3"))
		  	{
			    LRTempCessContInfoSchema tLRTempCessContInfoSchema = new LRTempCessContInfoSchema(); 
					tLRTempCessContInfoSchema.setReContCode(request.getParameter("ReContCode")); 
					tLRTempCessContInfoSchema.setProposalContNo(request.getParameter("ProposalContNo")); 
					tLRTempCessContInfoSchema.setReComCode(request.getParameter("ReComCode")); 
					tLRTempCessContInfoSchema.setPrtNo(request.getParameter("PrtNo")); 
					tLRTempCessContInfoSchema.setProposalNo(strProposalNo[index]); 
			    tLRTempCessContInfoSchema.setInsuredNo(strInsuredNo[index]); 
			    tLRTempCessContInfoSchema.setRiskCode(strRiskCode[index]); 
			    tLRTempCessContInfoSchema.setCessFeeMode(""); 
			    tLRTempCessContInfoSchema.setCalMode(strCalMode[index]);
			    tLRTempCessContInfoSchema.setRetainAmnt(strRetainAmnt[index]);
			    tLRTempCessContInfoSchema.setCessionRate(strCessionRate[index]); 
			     
					tLRTempCessContInfoSchema.setCessionAmount(strCessionAmnt[index]);
			    tLRTempCessContInfoSchema.setCessionFee(strCessionFee[index]);
			    
			    tLRTempCessContInfoSchema.setReinProcFeeRate(strReinProcFeeRate[index]);
			    tLRTempCessContInfoSchema.setDeadAddFee(strDeadAddFee[index]); 
			    tLRTempCessContInfoSchema.setDiseaseAddFee(strDiseaseAddFee[index]);
			    tLRTempCessContInfoSchema.setReUWConlcusion(strReUWConlcusion[index]); 
			    tLRTempCessContInfoSchema.setTempCessConclusion(strTempCessConclusion[index]); 
			    
			    mLRTempCessContInfoSet.add(tLRTempCessContInfoSchema);
			    
			    tLCPolSchema.setProposalNo(strProposalNo[index]);
		  		tLCPolSchema.setReinsureFlag(strTempCessConclusion[index]);
		    }
		  	else if (strTempCessConclusion[index].equals("4")) //如果临分结论是合同分保
		  	{
		  		tLCPolSchema.setProposalNo(strProposalNo[index]);
		  		tLCPolSchema.setReinsureFlag("4");
		  		proposalList.add(strProposalNo[index]);
		  	}
		  	else if(strTempCessConclusion[index].equals("5")) //如果临分结论是临分未实现
		  	{
		  		tLCPolSchema.setProposalNo(strProposalNo[index]);
		  		tLCPolSchema.setReinsureFlag("5");
		  		proposalList.add(strProposalNo[index]);
		  	}
		  	mLCPolSet.add(tLCPolSchema);
System.out.println("In Save: "+tLCPolSchema.getProposalNo());
		  }
	  }
	}
  /************************************************************************/
	else //团单下个人保存与修改
	{
	  if(strNumber!=null)
	  {
	  	int tLength = strNumber.length;
	    for(int i = 0 ;i < tLength ;i++)
	    {
	    	LCPolSchema tLCPolSchema = new LCPolSchema();
	    	if (strTempCessConclusion[i].equals("3"))
	    	{
		      LRTempCessContInfoSchema tLRTempCessContInfoSchema = new LRTempCessContInfoSchema(); 
					tLRTempCessContInfoSchema.setReContCode(request.getParameter("ReContCode")); 
					tLRTempCessContInfoSchema.setProposalContNo(request.getParameter("ProposalContNo")); 
					tLRTempCessContInfoSchema.setReComCode(request.getParameter("ReComCode")); 
					tLRTempCessContInfoSchema.setPrtNo(request.getParameter("PrtNo")); 
					tLRTempCessContInfoSchema.setProposalNo(strProposalNo[i]); 
		      tLRTempCessContInfoSchema.setInsuredNo(strInsuredNo[i]); 
		      tLRTempCessContInfoSchema.setRiskCode(strRiskCode[i]); 
		      tLRTempCessContInfoSchema.setCessFeeMode(""); 
		      tLRTempCessContInfoSchema.setCalMode(strCalMode[i]);
		      tLRTempCessContInfoSchema.setRetainAmnt(strRetainAmnt[i]);
		      tLRTempCessContInfoSchema.setCessionRate(strCessionRate[i]); 
		       
					tLRTempCessContInfoSchema.setCessionAmount(strCessionAmnt[i]);
		      tLRTempCessContInfoSchema.setCessionFee(strCessionFee[i]);
		      
		      tLRTempCessContInfoSchema.setReinProcFeeRate(strReinProcFeeRate[i]); 
		      tLRTempCessContInfoSchema.setDeadAddFee(strDeadAddFee[i]); 
		      tLRTempCessContInfoSchema.setDiseaseAddFee(strDiseaseAddFee[i]); 
		      tLRTempCessContInfoSchema.setReUWConlcusion(strReUWConlcusion[i]); 
		      tLRTempCessContInfoSchema.setTempCessConclusion(strTempCessConclusion[i]); 
		      
		      mLRTempCessContInfoSet.add(tLRTempCessContInfoSchema);
		      
		      tLCPolSchema.setProposalNo(strProposalNo[i]);
	    		tLCPolSchema.setReinsureFlag(strTempCessConclusion[i]);
	      }
	    	else if (strTempCessConclusion[i].equals("4")) //如果临分结论是合同分保
	    	{
	    		tLCPolSchema.setProposalNo(strProposalNo[i]);
	    		tLCPolSchema.setReinsureFlag("4");
	    		proposalList.add(strProposalNo[i]);
	    	}
	    	else if(strTempCessConclusion[i].equals("5")) //如果临分结论是临分未实现
	    	{
	    		tLCPolSchema.setProposalNo(strProposalNo[i]);
	    		tLCPolSchema.setReinsureFlag("5");
	    		proposalList.add(strProposalNo[i]);
	    	}
	    	mLCPolSet.add(tLCPolSchema);
	    }
	  }
	}
	System.out.println("in jsp0 ->before submitData"); 
	/*  */
	if(mModifyFlag.equals("1")||mModifyFlag.equals("2"))
  {
  	mOperateType="UPDATE";
  }
	else
	{
		mOperateType="INSERT";
	}
 	
  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增临分协议";
  }
  if(mOperateType.equals("UPDATE"))
  {
    mDescType = "修改临分协议";
  }
  if(mOperateType.equals("DELETE"))
  {
    mDescType = "删除临分协议";
  }
  if(mOperateType.equals("QUERY"))
  {
    mDescType = "查询临分协议";
  }
	
  VData tVData = new VData();
  try
  {
  	tVData.addElement(globalInput);
  	
  	tVData.addElement(mLRTempCessContSchema);
    tVData.addElement(mLRTempCessContInfoSet);
    tVData.addElement(mLCPolSet);
    tVData.addElement(proposalList);
    
    mTempCessContUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mTempCessContUI.mErrors;
    if (!tError.needDealError())
    {
    	if(mTempCessContUI.getResult()!=null)
    	{
      	Content = mDescType+"成功，"+" 合同编号："+mTempCessContUI.getResult();
      }else
      {
      	Content = "已下临分结论,没有要保存的临分协议";
      }
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mTempCessContUI.getResult()%>");
</script>
</html>