//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  var strSQL = 
  	 "select Recontcode,Recontname,Recomcode,InputDate,Operator from LRTempCessCont where Recontcode in ("
		+"select Recontcode from LRTempCessContInfo where proposalno in "              
		+"(select proposalno from lcpol where grppolno = (select  GrpPolno from LCgrppol where grpproposalno='"+fm.GrpProposalNo.value+"')))"
	  + getWherePart("Recontcode","Recontcode")
	  + getWherePart("Recontname","Recontname","like")
  ;
  
  //strSQL = strSQL +" order by ReComCode";
  
  var arrResult = new Array();
	//arrResult = easyExecSql(strSQL);
	
	turnPage.queryModal(strSQL, ReComGrid)
  
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}

	parent.fraMain.rows = "0,0,0,0,*";
}

function queryInfo()
{
	var tSel=ReComGrid.getSelNo();
	var strSQL="select Recontcode,(select Recontname from LRTempCessCont where Recontcode=a.Recontcode),"
		+" (select name from LDPerson where customerno=a.Insuredno),"
		+" RiskCode a, Retainamnt a,CessionRate a,CessionAmount a,CessionFee a,"
		+" Reinprocfeerate a,DeadAddFee a,DiseaseAddFee a,ReUWConlcusion a,TempCessconclusion a"
		+" from LRTempCessContInfo a where a.Recontcode='"+ReComGrid.getRowColData(tSel-1,1)+"'"
		;
		
	var arrResult=easyExecSql(strSQL);
	if(arrResult==null)
	{
		alert("没有临分协议信息！");
		return false;
	}
	turnPage.queryModal(strSQL, TempContInfGrid); 
}

function ClosePage()
{
	top.close();
}