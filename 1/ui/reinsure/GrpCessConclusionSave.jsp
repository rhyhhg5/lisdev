<%
//程序名称：GrpCessConclusionSave.jsp
//程序功能：
//创建日期：2006-09-07
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  System.out.println("开始执行Save页面1");
  LRTempCessContSchema mLRTempCessContSchema = new LRTempCessContSchema();
 	LRTempCessContInfoSet mLRTempCessContInfoSet = new LRTempCessContInfoSet();
 	
  System.out.println("开始执行Save页面2");
  GrpTempCessContUI mGrpTempCessContUI = new GrpTempCessContUI();
  
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  		= "";
  String FlagStr 		= "";
  String Content 		= "";
  String mDescType 	= ""; //将操作标志的英文转换成汉字的形式
  
  System.out.println("开始进行获取数据的操作！！！");
  mLRTempCessContSchema.setReContCode(		request.getParameter("ReContCode"));
  mLRTempCessContSchema.setReContName(		request.getParameter("ReContName"));
  mLRTempCessContSchema.setReComCode(			request.getParameter("ReComCode"));
  mLRTempCessContSchema.setInputDate(			request.getParameter("InputDate"));
  mLRTempCessContSchema.setPrtNo(					request.getParameter("PrtNo"));
  
  String strProposalContNo = request.getParameter("ProposalContNo") ;
    
  String[] strNumber 			 				= request.getParameterValues("PolGridNo");
  String[] strRiskCode  					= request.getParameterValues("PolGrid4"); //险种代码
  String[] strGrpProposalNo 			= request.getParameterValues("PolGrid10");//
  String[] strRetainAmnt	 				= request.getParameterValues("PolGrid11");
  String[] strCessionRate  				= request.getParameterValues("PolGrid12");
  String[] strReinProcFeeRate 		= request.getParameterValues("PolGrid13");
  String[] strReUWConlcusion			= request.getParameterValues("PolGrid14");
  String[] strTempCessConclusion	= request.getParameterValues("PolGrid15");
  
	String tRadio[] = request.getParameterValues("InpPolGridSel"); 
	
	String strReComCode				 = request.getParameter("ReComCode");
	String strPrtNo						 = request.getParameter("PrtNo");
	
	String sRiskCode					 = "";
	String sRetainAmnt			   = "";
	String sGrpProposalNo 		 = "";
	String sCessionRate 		   = "";
	String sReinProcFeeRate    = "";
	String sReUWConlcusion	   = "";
	String sTempCessConclusion = "";
	
	for (int index=0; index< tRadio.length;index++) //通过循环仅对radiobox为1的记录进行处理
	{
	  if(tRadio[index].equals("1"))
	  {
			sRiskCode					  = strRiskCode[index];
			sRetainAmnt			    = strRetainAmnt[index];
			sCessionRate 		    = strCessionRate[index];
			sGrpProposalNo			= strGrpProposalNo[index];
			sReinProcFeeRate    = strReinProcFeeRate[index];
			sReUWConlcusion	    = strReUWConlcusion[index];
			sTempCessConclusion = strTempCessConclusion[index];
	  } 
	}  

	String mModifyFlag=request.getParameter("ModifyFlag");
	if(mModifyFlag.equals("1")||mModifyFlag.equals("2"))
	{
		mOperateType="UPDATE";
	}
	else
	{
		mOperateType="INSERT";
	}
	 	
  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增临分协议";
  }
  if(mOperateType.equals("UPDATE"))
  {
    mDescType = "修改临分协议";
  }
  if(mOperateType.equals("DELETE"))
  {
    mDescType = "删除临分协议";
  }
  if(mOperateType.equals("QUERY"))
  {
    mDescType = "查询临分协议";
  }
	
  VData tVData = new VData(); 
  //将团单的公共信息通过TransferData传到UI
  try
  {
  	tVData.addElement(globalInput);
  	TransferData grpCessData = new TransferData();
		grpCessData.setNameAndValue("ProposalContNo"		 ,strProposalContNo	 ); 
		grpCessData.setNameAndValue("ReComCode"					 ,strReComCode			 ); //再保公式代码
		grpCessData.setNameAndValue("PrtNo"							 ,strPrtNo					 ); //印刷号
   	grpCessData.setNameAndValue("RiskCode"					 ,sRiskCode					 ); //险种
  	grpCessData.setNameAndValue("RetainAmnt"			   ,sRetainAmnt			   ); //自留额
  	grpCessData.setNameAndValue("CessionRate" 		   ,sCessionRate 		   ); //分保比例
  	grpCessData.setNameAndValue("GrpProposalNo" 		 ,sGrpProposalNo 		 ); //GrpProposalNo
  	grpCessData.setNameAndValue("ReinProcFeeRate"    ,sReinProcFeeRate   ); //手续费比例
  	grpCessData.setNameAndValue("ReUWConlcusion"	   ,sReUWConlcusion	   ); //再保核保结论
  	grpCessData.setNameAndValue("TempCessConclusion" ,sTempCessConclusion); //临分结论
  	
  	tVData.addElement(mLRTempCessContSchema);
  	tVData.addElement(grpCessData);
  	mGrpTempCessContUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mGrpTempCessContUI.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+"成功，"+" 协议编号："+mGrpTempCessContUI.getResult();
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mGrpTempCessContUI.getResult()%>");
</script>
</html>