<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRClaimQuery.jsp
//程序功能：退保减人查询
//创建日期：2007-04-8 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LRClaimQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRClaimQueryInit.jsp"%>
  <title>退保减人查询</title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "" >
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClaimInfo);"></td>
          <td class="titleImg">请录入查询条件</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divClaimInfo" style= "display: ''">
    <table class=common>
  		<tr class=common>
  			<TD class= title>
          被保人客户号
        </TD>
        <TD class= input>
          <Input class=common name="InsuredNo">
        </TD>
        <TD  class= title>
          保单合同号
        </TD>
        <TD  class= input>
          <Input class=common name="ContNo" verify="保单合同号|len<=20">
        </TD>
        <TD  class= title>
          险种代码
        </TD>
        <TD  class= input>
          <Input class=common name="RiskCode">
        </TD>
  		</tr>
  		<tr class=common>
  			<TD  class= title>
  				再保公司
        </TD>
        <TD  class= input>
         <input class="codeno" name= "ReComCode"  
	       	ondblclick="return showCodeList('reinsurecomcode',[this,ReComCodeName],[0,1],null,null,null,1,300);"
	       	onkeyup="return showCodeListKey('reinsurecomcode', [this,ReComCodeeName],[0,1],null,null,null,1,300);" verify="再保公司|NOTNULL"><input 
	       	class=codename name="ReComCodeName" >
        </TD>
        <TD  class= title>
          再保合同
        </TD>
        <TD  class= input>
          <Input class=codeNo name=ReContCode
          ondblclick="return showCodeList('recontcode',[this,ReContCodeName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('recontcode',[this,ReContCodeName],[0,1]);"><input class=codename name=ReContCodeName >
        </TD>
  		</tr>
  		<tr class=common>
  			<TD class= title>
          起始日期
        </TD>
        <TD class= input>
          <Input class=coolDatePicker name="StartDate" dateFormat='short' verify="起始日期|notnull&Date">
        </TD>
        <TD  class= title>
          终止日期
        </TD>
        <TD  class= input>
          <Input class=coolDatePicker name="EndDate" dateFormat='short' verify="终止日期|notnull&Date">
        </TD>
      </tr>
  	</table>   
  </Div>
  <INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
  <INPUT class=cssButton id="riskbutton" VALUE="重  置" TYPE=button onClick="resetForm();">         
	<Div  id= "divClaimResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanClaimPolGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">  
	</Div>
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>