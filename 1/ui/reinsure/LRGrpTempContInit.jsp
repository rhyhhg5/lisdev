<%
//Creator :张斌
//Date :2006-08-18
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
	String grpProposalNo=request.getParameter("GrpProposalNo");
%>
<script language="JavaScript">
function initInpBox()
{
  try
  { 
  	fm.GrpProposalNo.value='<%=grpProposalNo%>';
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}


// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initReComGrid();
    initTempContInfGrid();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initReComGrid()
{
	var iArray = new Array();
      
      try
      {
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";         			//列宽
	      iArray[0][2]=10;          				//列最大值
	      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[1]=new Array();
	      iArray[1][0]="协议编号";    			//列名
	      iArray[1][1]="150px";            	//列宽
	      iArray[1][2]=100;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[2]=new Array();
	      iArray[2][0]="协议名称";         	//列名
	      iArray[2][1]="100px";            	//列宽
	      iArray[2][2]=100;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	 
	      iArray[3]=new Array();
	      iArray[3][0]="再保公司代码";      //列名
	      iArray[3][1]="60px";            	//列宽
	      iArray[3][2]=60;            			//列最大值
	      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[4]=new Array();
	      iArray[4][0]="协议录入日期";      //列名
	      iArray[4][1]="60px";            	//列宽
	      iArray[4][2]=60;            			//列最大值
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[5]=new Array();
	      iArray[5][0]="协议录入人";        //列名
	      iArray[5][1]="100px";            	//列宽
	      iArray[5][2]=1000;            		//列最大值
	      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
				
	      ReComGrid = new MulLineEnter( "fm" , "ReComGrid" ); 
	      ReComGrid.mulLineCount = 0;   
	      ReComGrid.displayTitle = 1;
	      ReComGrid.canSel=1;
	      ReComGrid.locked = 1;	
				ReComGrid.hiddenPlus = 1;
				ReComGrid.hiddenSubtraction = 1;
	      ReComGrid.loadMulLine(iArray);  
	      ReComGrid.detailInfo="单击显示详细信息";
	      ReComGrid.selBoxEventFuncName = "queryInfo";
     
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initTempContInfGrid()
{
	var iArray = new Array();
  
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";         			//列宽
	  iArray[0][2]=10;          				//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="协议编号";    			//列名
	  iArray[1][1]="150px";            	//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[2]=new Array();
	  iArray[2][0]="协议名称";         	//列名
	  iArray[2][1]="100px";            	//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[3]=new Array();
	  iArray[3][0]="被保险人名称";         	//列名
	  iArray[3][1]="100px";            	//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="险种编码";         	//列名
	  iArray[4][1]="100px";            	//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[5]=new Array();
    iArray[5][0]="自留额";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
         
    iArray[6]=new Array();
    iArray[6][0]="分保比例";         	//列名
    iArray[6][1]="50px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
    
    iArray[7]=new Array();
    iArray[7][0]="分保保额";         		//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
         
    iArray[8]=new Array();
    iArray[8][0]="分保保费";         	//列名
    iArray[8][1]="50px";            		//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
    
    iArray[9]=new Array();
    iArray[9][0]="死亡加费评点";         		//列名
    iArray[9][1]="80px";            		//列宽
    iArray[9][2]=100;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
         
    iArray[10]=new Array();
    iArray[10][0]="重疾加费评点";         	//列名
    iArray[10][1]="50px";            		//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[11]=new Array();
    iArray[11][0]="手续费比例";         //列名
    iArray[11][1]="70px";            		//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
                
    iArray[12]=new Array();
    iArray[12][0]="再保核保结论";       //列名
    iArray[12][1]="80px";            		//列宽
    iArray[12][2]=100;            			//列最大值
    iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[13]=new Array();
    iArray[13][0]="临分结论";         	//列名
    iArray[13][1]="80px";            		//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
		
	  TempContInfGrid = new MulLineEnter( "fm" , "TempContInfGrid" ); 
	  TempContInfGrid.mulLineCount = 0;   
	  TempContInfGrid.displayTitle = 1;
	  TempContInfGrid.canSel=0;
	  TempContInfGrid.locked = 1;	
		TempContInfGrid.hiddenPlus = 1;
		TempContInfGrid.hiddenSubtraction = 1;
	  TempContInfGrid.loadMulLine(iArray);  
	  TempContInfGrid.detailInfo="单击显示详细信息";
  
  }
  catch(ex)
  {
    alert(ex);
  }
}


</script>