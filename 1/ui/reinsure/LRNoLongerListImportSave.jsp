<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
System.out.println("Import Begin ...");



GlobalInput tG = (GlobalInput)session.getValue("GI");

String FlagStr = "Fail";
String Content = "";

/** 文件上传成功标志。 */
boolean tSucFlag = false;

/** 是否可以上传文件标志。 */
boolean tCanUploadFlag = false;

String mImportPath = null;
String mFileName = null;

String tImportDir = new ExeSQL().getOneValue("select SysVarValue from LDSysVar where SysVar = 'XmlPath'");

String tBatchNo = "";

if(tImportDir != null && !tImportDir.equals(""))
{
    tCanUploadFlag = true;
}
else
{
    Content += "未找到清单上传临时目录！";
}

mImportPath = application.getRealPath("").replace('\\','/') + '/';
mImportPath += tImportDir;

System.out.println("...开始上载文件");
System.out.println("mImportPath:" + mImportPath);

// Initialization
DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
fu.setSizeMax(10000000);
// maximum size that will be stored in memory?
// 设置最多只允许在内存中存储的数据,单位:字节
fu.setSizeThreshold(4096);
// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
fu.setRepositoryPath(mImportPath);

//开始读取上传信息
List fileItems = null;
try
{
	fileItems = fu.parseRequest(request);
    tSucFlag = true;
}
catch(Exception ex)
{
    tSucFlag = false;
	ex.printStackTrace();
}

// 依次处理每个上传的文件
System.out.println("开始在服务器上创建文件");

Iterator iter = fileItems.iterator();

while(iter.hasNext())
{
	FileItem item = (FileItem)iter.next();
    
    //忽略其他不是文件域的所有表单信息
    if (!item.isFormField())
    {
        String name = item.getName();
        System.out.println("name:" + name);
        
        long size = item.getSize();
        
        if((name == null || name.equals("")) && size == 0)
            continue;

        mFileName = name.substring(name.lastIndexOf("\\") + 1);
        
        //保存上传的文件到指定的目录
        try
        {
            item.write(new File(mImportPath + mFileName));
            tSucFlag = true;
        }
        catch(Exception e)
        {
            tSucFlag = false;
        	e.printStackTrace();
            System.out.println("upload file error ...");
        }
        
    }
}

if(tSucFlag)
{
    try
    {
        
            VData tVData = new VData();
            
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("FileName", mFileName); 
            tTransferData.setNameAndValue("FilePath", mImportPath);
            
            
            tVData.add(tTransferData);
            tVData.add(tG);
        
            LRNoLongerListBL tLRNoLongerListBL = new LRNoLongerListBL();
            
            if (!tLRNoLongerListBL.submitData(tVData, "Import"))
            {
                Content = " 导入清单失败! 原因是: " + tLRNoLongerListBL.mErrors.getLastError();
                FlagStr = "Fail";
            }
            else
            {
                Content = " 导入清单成功！";
                FlagStr = "Succ";
            }
      
       
    }
    catch (Exception e)
    {
        Content = " 获取批次信息失败。";
        FlagStr = "Fail";
        e.printStackTrace();
    }
}

System.out.println("Import End ...");
%>
                  
<html>
<script language="javascript">
parent.fraInterface.afterImportCertifyList("<%=FlagStr%>", "<%=Content%>");
</script>
</html>

