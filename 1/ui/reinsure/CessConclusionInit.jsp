<html>
<%
//name :LRIndiCessInit.jsp
//function :Manage CessInfo
//Creator :
//date :2006-08-15
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 		GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator			= tG.Operator;
  	String Comcode			= tG.ManageCom;
 		String CurrentDate	= PubFun.getCurrentDate();   
    String tCurrentYear	=	StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=	StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate	=	StrTool.getVisaDay(CurrentDate);   
    String modifyFlag		= request.getParameter("ModifyFlag");
    String riskCode		= request.getParameter("RiskCode");
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.PrtNo.value 					= '<%=request.getParameter("PrtNo")%>';
  	fm.ManageCom.value 			= '<%=request.getParameter("ManageCom")%>';
  	fm.InputDate.value			= '<%=CurrentDate%>';
  	fm.LoadFlag.value				= '<%=request.getParameter("LoadFlag")%>';
  	fm.ModifyFlag.value 		=	'<%=modifyFlag%>';
  	fm.GrpProposalNo.value  = '<%=request.getParameter("GrpProposalNo")%>';
  	fm.RiskCode.value 			= '<%=request.getParameter("RiskCode")%>';
  	
  	fm.ReContCode.value="";
  	fm.ReComCode.value="";
  	
  	var strSQL="select distinct AppntName from LCPol where PrtNo='"+fm.PrtNo.value+"'";
  	var arrResult=easyExecSql(strSQL);
  	if (arrResult==null)
  	{
  		fm.AppntName.value="";
  	}
  	else
  	{
  		fm.AppntName.value=arrResult[0][0];
  	}
  	strSQL=" select distinct ProposalContNo from LCPol where PrtNo='"+fm.PrtNo.value+"'" ;
  	arrResult=easyExecSql(strSQL);
  	if (arrResult==null)
  	{
  		fm.ProposalContNo.value = "";
  	}
  	else
  	{
  		fm.ProposalContNo.value = arrResult[0][0];
  	}
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CessConclusionInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    if(fm.ModifyFlag.value=="1") //个单修改
  	{
  		initPolGrid();
    	easyQuery(); 	//填充mulLine保单信息部分
  		initModify();	//个单修改时填充mulLine再保信息部分
  	}
  	else if(fm.ModifyFlag.value=="2") //团单下个人修改
  	{
  		initModifyGrpInd();
  		initPolGrid();
  		easyQuery(); //填充mulLine保单信息部分
  		
  	}else if(fm.ModifyFlag.value=="3") //团单下个人保存
  	{
  		initPolGrid();
  		initModifyGrpInd();
  		easyQuery(); //填充mulLine保单信息部分
  		
  	}else	 //fm.ModifyFlag.value==null 个单保存
  	{
  		initPolGrid();
    	easyQuery(); //填充mulLine保单信息部分
  	}
  }
  catch(re)
  {
    alert("3在CessConclusionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
{                            
    var iArray = new Array();
      
    try
    {
    iArray[0]=new Array();
    iArray[0][0]="序号";         					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            			//列宽
    iArray[0][2]=30;            					//列最大值
    iArray[0][3]=0;              					//是否允许输入,1表示允许，0表示不允许
                                        	
    iArray[1]=new Array();              	
    iArray[1][0]="被保险人代码";         			//列名
    iArray[1][1]="80px";            			//列宽
    iArray[1][2]=100;            					//列最大值
<%    
    if(modifyFlag.equals("2"))            //如果是修改则显示下拉菜单
		{
%>
    	iArray[1][3]=2;              					//是否允许输入,1表示允许，0表示不允许  
    	iArray[1][4]="lrinsuredno"; 
		  iArray[1][5]="1|2|3|4|5|6|7"; 	 	//将引用代码分别放在第1、2、3、4列
		  iArray[1][6]="0|1|2|3|4|5|6";	 
		  iArray[1][15]=fm.PrtNo.value;
		  iArray[1][17]="2";
		  iArray[1][19]=1;
<%
		}else
		{
%>	
		iArray[1][3]=0;              					//是否允许输入,1表示允许，0表示不允许     	
<%		
		}
%>    
                                          	
    iArray[2]=new Array();              	
    iArray[2][0]="被保险人";        		//列名
    iArray[2][1]="80px";            			//列宽
    iArray[2][2]=100;            					//列最大值
    iArray[2][3]=0;              					//是否允许输入,1表示允许，0表示不允许
 
                                        	
    iArray[3]=new Array();              	
    iArray[3][0]="险种代码";         			//列名
    iArray[3][1]="60px";            			//列宽
    iArray[3][2]=100;            					//列最大值
    iArray[3][3]=0;              					//是否允许输入,1表示允许，0表示不允许
                                        	
    iArray[4]=new Array();              	
    iArray[4][0]="保额";         					//列名
    iArray[4][1]="70px";            			//列宽
    iArray[4][2]=100;            					//列最大值
    iArray[4][3]=0;              					//是否允许输入,1表示允许，0表示不允许
                                        	
    iArray[5]=new Array();              	
    iArray[5][0]="基本保费";         			//列名
    iArray[5][1]="80px";            			//列宽
    iArray[5][2]=100;            					//列最大值
    iArray[5][3]=0;              					//是否允许输入,1表示允许，0表示不允许            
                                        	
    iArray[6]=new Array();              	                                         
    iArray[6][0]="风险保额";         			//列名                                     
    iArray[6][1]="80px";            			//列宽                                   
    iArray[6][2]=100;            					//列最大值                                 
    iArray[6][3]=0;              					//是否允许输入,1表示允许，0表示不允许   
                                        	
    iArray[7]=new Array();              	
    iArray[7][0]="ProposalNo";         		//列名
    iArray[7][1]="80px";            			//列宽
    iArray[7][2]=200;            					//列最大值
    iArray[7][3]=3;              					//是否允许输入,1表示允许，0表示不允许 
                                        	
    iArray[8]=new Array();              	
    iArray[8][0]="计算方式";         			//列名
    iArray[8][1]="50px";            			//列宽
    iArray[8][2]=200;            					//列最大值
    iArray[8][3]=2;              					//是否允许输入,1表示允许，0表示不允许 
    iArray[8][10]="CalMode"; 		  			
	  iArray[8][11]="0|^C|合同费率|^S|手工计算" 	  
	  iArray[8][13]="1|0"		  						
    
    iArray[9]=new Array();
    iArray[9][0]="自留额";         				//列名
    iArray[9][1]="80px";            			//列宽
    iArray[9][2]=100;            					//列最大值
    iArray[9][3]=1;              					//是否允许输入,1表示允许，0表示不允许
    iArray[9][9]="自留额|NUM"; 						//检验格式：
    
    iArray[10]=new Array();
    iArray[10][0]="分保比例";         		//列名
    iArray[10][1]="50px";            			//列宽
    iArray[10][2]=100;            				//列最大值
    iArray[10][3]=1;              				//是否允许输入,1表示允许，0表示不允许            
    iArray[10][9]="分保比例|NUM"; 				//检验格式：

    iArray[11]=new Array();                                                       
    iArray[11][0]="分出保额";         		//列名                                     
    iArray[11][1]="80px";            			//列宽                                   
    iArray[11][2]=100;            				//列最大值                                 
    iArray[11][3]=1;              				//是否允许输入,1表示允许，0表示不允许   
                                        	
    iArray[12]=new Array();             	
    iArray[12][0]="分出保费";         		//列名
    iArray[12][1]="80px";            			//列宽
    iArray[12][2]=100;            				//列最大值
    iArray[12][3]=1;              				//是否允许输入,1表示允许，0表示不允许 
    
    
    iArray[13]=new Array();
    iArray[13][0]="手续费比例";         	//列名
    iArray[13][1]="70px";            			//列宽
    iArray[13][2]=200;            				//列最大值
    iArray[13][3]=1;              				//是否允许输入,1表示允许，0表示不允许 
    iArray[13][9]="手续费比例|NUM"; 			//检验格式：
                                        	
    iArray[14]=new Array();             	
    iArray[14][0]="死亡加费评点";       	//列名
    iArray[14][1]="80px";            			//列宽
    iArray[14][2]=100;            				//列最大值
    iArray[14][3]=1;              				//是否允许输入,1表示允许，0表示不允许            
    iArray[14][9]="死亡加费评点|NUM"; 		//检验格式：
                                        	
    iArray[15]=new Array();             	                                          
    iArray[15][0]="疾病加费评点";       	//列名                                     
    iArray[15][1]="80px";            			//列宽                                   
    iArray[15][2]=100;            				//列最大值                                 
    iArray[15][3]=1;              				//是否允许输入,1表示允许，0表示不允许   
    iArray[15][9]="疾病加费评点|NUM"; 		//检验格式：
                                        	
    iArray[16]=new Array();             	
    iArray[16][0]="再保核保结论";       	//列名
    iArray[16][1]="80px";            			//列宽
    iArray[16][2]=100;            				//列最大值
    iArray[16][3]=1;              				//是否允许输入,1表示允许，0表示不允许 
                                        	
    iArray[17]=new Array();             	
    iArray[17][0]="临分结论";         		//列名
    iArray[17][1]="80px";            			//列宽
    iArray[17][2]=200;            				//列最大值
    iArray[17][3]=2;              				//是否允许输入,1表示允许，0表示不允许 
    //iArray[17][9]="临分结论|NOTNULL"; 	//检验格式：
    iArray[17][10]="CessConclusion"; 		  			
	  iArray[17][11]="0|^3|临分标志|^4|合同分保标志|^5|临分未实现标志" 
	  iArray[17][13]="1|0"		  						
	  
    PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
    //这些属性必须在loadMulLine前
    PolGrid.mulLineCount = 3;   
    PolGrid.displayTitle = 1;
    PolGrid.locked = 0;
    //
    if(fm.ModifyFlag.value=="null"||fm.ModifyFlag.value=="1"||fm.ModifyFlag.value==null) //个单保存
    {
    	PolGrid.canSel = 1;
			PolGrid.selBoxEventFuncName = "initModifyForm";
    }
		PolGrid.hiddenSubtraction=1;
		
    if(fm.ModifyFlag.value=='2') //团单下个人修改
    {
	    PolGrid.hiddenPlus=0;
	    //PolGrid.hiddenSubtraction=0;
    }else
    {
    	PolGrid.hiddenPlus=1;
	    //PolGrid.hiddenSubtraction=1;
    }
    PolGrid.loadMulLine(iArray);     
    
    //PolGrid. selBoxEventFuncName = "easyQueryAddClick";
    
    //这些操作必须在loadMulLine后面
    //PolGrid.setRowColData(1,1,"asdf");
    }
    catch(ex)
    {
      alert(ex);
    }
}


</script>
