var showInfo;

var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 

function easyQueryClick()
{
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
 	var day = showMonthLastDay();  //获取提取月份的最后一天
    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
	var strWhere="";
	var strWhereT="";
 	if(fm.ReComCode.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='"+fm.ReComCode.value+"') ";
 	    strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='"+fm.ReComCode.value+"') ";
 	}
 	if(fm.DiskKind.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
 		strWhere+=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
 	}
 	if(fm.Year.value!=''){
 		strWhere+=" and a.getdatadate between '"+StartDate+"' and '"+EndDate+"'";
 		strWhereT+=" and a.getdatadate between '"+StartDate+"' and '"+EndDate+"'";
 	} 
 	if(fm.CessionMode.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and cessionmode='"+fm.CessionMode.value+"') ";
 		strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and cessionmode='"+fm.CessionMode.value+"') ";
 	}
  var strSQL="select a.riskcode,a.ReContCode,"
    +" TempCessFlag, "
    +" a.ContNo contno, a.AppntName, a.InsuredName, a.CValiDate, a.signdate,"
    +" a.Amnt,a.CessionAmount,a.AccidentDate,a.getdatadate,a.RealPay,"
    +"a.ClaimBackFee,'',a.polno,'' "
    +"from lrclaimlist a  "
    +" where grpcontno='00000000000000000000' "
    + strWhere
    + getWherePart("a.riskcode","RiskCode")
    + getWherePart("a.Contno","ContNo")
    + getWherePart("a.RecontCode","RecontCode")
    
    + getWherePart("a.AppntName","AppntName","like")
    + getWherePart("a.InsuredName","InsuredName",'like');
    
    strSQL+=" union all select a.riskcode,a.ReContCode,"
    +" TempCessFlag, "
    +" a.GrpContno contno, a.AppntName, a.InsuredName, a.CValiDate, a.signdate,"
    +" a.Amnt,a.CessionAmount,a.AccidentDate,a.getdatadate,a.RealPay,"
    +"a.ClaimBackFee,'',a.polno,'' "
    +"from lrclaimlist a  "
    +" where grpcontno<>'00000000000000000000' "
    + strWhere
    + getWherePart("a.riskcode","RiskCode")
    + getWherePart("a.GrpContno","ContNo")
    + getWherePart("a.RecontCode","RecontCode")
    
    + getWherePart("a.AppntName","AppntName","like")
    + getWherePart("a.InsuredName","InsuredName",'like');
    
   
    strSQL+=" order by ReContCode with ur";
 
	 turnPage.queryModal(strSQL, NewClmDetailGrid); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}
function easyQueryContClick(){
	
	var strSQL="select recontcode,recontname from lrcontinfo  " +
			" union select tempcontcode,tempcontname from LRTempCessCont where recontstate='01'  with ur"
	turnPage1.queryModal(strSQL, ContGrid);
}

function checkNum(control){
   var a = control.value.match(/^(-?\d+)(\.\d+)?$/); 
   if (a == null) {
      alert('数值不符合');
			control.select();
			control.focus();
			return false;
   } 
}


function check(){
	if(vCheck()==false){
		return false;
	}
	if(confirm('是否确定对选中的信息做审核通过操作')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="CHECK";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}

function vCheck(){
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
 	var day = showMonthLastDay();  //获取提取月份的最后一天
    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
	var strWhere="";
	if(fm.RecontCode.value !=''){
		strWhere=" and recontcode='"+fm.RecontCode.value+"'";
	}
 	var sqla = "select payflag from LRAccounts where startdate='"+StartDate+"' and enddata='"+EndDate+"'" 
                +strWhere+" with ur";
 	var strResult = easyExecSql(sqla);
 	if(strResult!=null){
 		for(var i=0;i<strResult.length;i++){
 			var flag = strResult[i][1];
 			if(flag=="03"){
 			  alert('存在结算的账单,您不能进行清单审核,请重新录入查询条件 !');
 			  return false;
 			}
 		}
 	} 
}

function onloadlist(){
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
 	try 
	{
		var i = 0;
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.OperateType.value="ONLOAD";
		fm.submit();
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}


function downAfterSubmit(cfilePath) {
	showInfo.close();
	fileUrl.href = cfilePath; 
    fileUrl.click();
}
function showMonthLastDay()
 {
     var tmpDate=new Date(fm.Year.value,fm.Month.value,1); 
     var MonthLastDay=new Date(tmpDate-86400000);
     return MonthLastDay.getDate();      
 }

        

