<html>
<%
//name :ReComManage.jsp
//function :ReComManage
//Creator :liuli
//date :2008-11-06
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.reinsure.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "./ReTempConfirmContInput.js"></SCRIPT> 
<%@include file="./ReTempConfirmContInit.jsp"%>
</head>
<body  onload="initElementtype();initForm()" >
  <form action="" method=post name=fm target="fraSubmit" >     
	<br>
  	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		合同编号
   			</TD>
   			<TD class= input>
   				<Input class= common name="TempContCode" id="TempContCodeId" verify="合同编号|NOTNULL"> 
   			</TD>
   		<TD class= title>
          保单号
        </TD>
        <TD class= input>
        	<Input class= common name="Contno"> 
        </TD>
   	  <TD class= title>
   		 		保单类型
   	   </TD>
   	  <TD class= input>
   		 <Input class="codeno"  name= "ContType"  CodeData="0|^1|个单^2|团单" 
          ondblClick="showCodeListEx('DiskKind',[this,ContTypeName],[0,1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('DiskKind',[this,ContTypeName],[0,1],null,null,null,1);" verify="险种类别|NOTNULL"><Input 
          class= codename name= 'ContTypeName' >
   	   </TD>   		        	
      </TR> 
      <TR>
      	 <TD class= title>
          合同状态
        </TD>
        <TD class= input >
        	<Input class="codeno"  name= "ReContState"  CodeData="0|^01|有效^02|失效" 
          ondblClick="showCodeListEx('ReContState',[this,ReContStateName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('ReContState',[this,ReContStateName],[0,1],null,null,null,1);" verify="保单状态|NOTNULL" ><Input 
          class= codename name= 'ReContStateName'>
        </TD>
      	<TD class= title>
   		 		险种类别
   			</TD>
   			<TD class= input>
   				<Input class="codeno"  name= "DiskKind" CodeData="0|^L|长险^M|短险" 
          ondblClick="showCodeListEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);" verify="险种类别|NOTNULL"><Input 
          class= codename name= 'DiskKindName'>
   			</TD>
   			<TD class= title>
   				分保方式
        </TD>
        <TD class= input>
        	<Input class="codeno"  name= "CessionMode" CodeData="0|^1|成数^2|溢额" 
          ondblClick="showCodeListEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);" verify="分保方式|NOTNULL" ><Input 
          class= codename name= 'CessionModeName' >
        </TD>
      </TR>      
      <TR>
      	<TD class= title>
   		 		再保公司
   			</TD>
   			<TD class= input >
   				<input class="code" name="ComCode" 
	            ondblclick="return showCodeList('reinsurecomcode',[this,ComCodeName],[0,1],null,null,null,1,300);"
	            onkeyup="return showCodeListKey('reinsurecomcode', [this,ComCodeName],[0,1],null,null,null,1,300);" verify="再保公司编号|NOTNULL"> 
   			</TD>
	    <TD class= title>
          再保公司名称
        </TD>
        <TD class= input colspan=3>
        	<Input class="common" name= ComCodeName style="width:98%" readonly > 
        </TD> 
      </TR>  
   	</Table>
   	<br>
   	<input class=cssButton  VALUE="查  询" TYPE=button onClick="QueryInfo();">
    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divContRisk);"></td>
    	<td class= titleImg>合同信息</td></tr>
    </table>
    <Div  id= "divContRisk" style= "display: ''">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanContGrid" ></span>
        		</td>
      		</tr>
    	</table>
    	  	<br>
  	<Div id= "div1" align="center" style= "display: '' ">
	  	<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">    
    </div>
	</div>	
  <input type=hidden name=flag >
  <input class=cssButton  VALUE="审  核" TYPE=button onClick="submitForm();">
   	
</form>
<form name=cessfm method=post target="fraSubmit">
   <input class=cssButton  VALUE="临分提数" TYPE=hidden onClick="CessForm();"> 
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>