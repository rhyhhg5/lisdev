<%
//Creator :liuli
//Date :2008-10-27
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
 		String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);               	               	
 %>
 <%
 String flag = request.getParameter("flag");
 String recontcode = request.getParameter("recontcode");
// System.out.println(flag+"-------------"+recontcode);
%>	 
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.ReContCode.value = "";
    fm.all('ReContName').value 		 		= '';
    fm.all('ReComCode').value 		 		= '';
    fm.all('ReComName').value 		 		= '';
    fm.all('RValidate').value 		 		= '';
    fm.all('RInvalidate').value 	 		= '';
    fm.all('ReType').value 		 		= '';
    fm.all('CessionMode').value 			= '';
    fm.all('CessionModeName').value 	= '';
    fm.all('CessionFeeMode').value		= '';
    fm.all('CessionFeeModeName').value= '';
    fm.flag.value = "<%=flag%>";
//    alert(fm.flag.value+"ddddddddddd");
  }         
  catch(ex) 
  {
    alert("初始化变量值时出现错误！！！！");
  }
}
;

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initContGetDutyGrid();
    initContRiskGrid();
    initRelDiskGrid();
    if(fm.flag.value=="UPDATE"){
    	initQuery();
      savedisk.style.display='none';
      updatedisk.style.display='';
      confirmdisk.style.display='none';
    }else if(fm.flag.value=="CONFIRM"){
        initQuery();
      savedisk.style.display='none';
      updatedisk.style.display='none';
      confirmdisk.style.display='';
    }else{
     savedisk.style.display='';
     updatedisk.style.display='none';
     confirmdisk.style.display='none';
    }
  }
  catch(re)
  {
    alert("ReContManageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//根据所接收到的合同号码查询合同的详细信息
function initQuery(){
	//合同信息
   var sqla = "select * from LRContInfo where recontcode='<%=recontcode%>' with ur";
  // alert(sqla)
   var res1 = easyExecSql(sqla);
    fm.ReContCode.value = res1[0][0];
    fm.all('ReContName').value = res1[0][1];
    fm.all('ReComCode').value = res1[0][2];
    var sqlcom = " select recomname From lrrecominfo where recomcode='"+fm.all('ReComCode').value+"' with ur ";
    var res2 = easyExecSql(sqlcom);
    if(res2!=null){
    fm.all('ReComName').value = res2[0][0];
    }
    fm.all('RValidate').value = res1[0][3];
    fm.all('RInvalidate').value= res1[0][4];
    fm.all('DiskKind').value = res1[0][7];
    if(fm.all('DiskKind').value=="L"){
    	fm.all('DiskKindName').value="长险";
    }else{
    	fm.all('DiskKindName').value="短险";
    }
    fm.all('CessionMode').value = res1[0][8];
    if(fm.all('CessionMode').value=="1"){
       fm.all('CessionModeName').value = "成数";
    }else if (fm.all('CessionMode').value=="2") {
       fm.all('CessionModeName').value = "溢额";
       document.getElementById('cessionDetailTR').style.display='';
    }
    fm.all('CessionDetailType').value =res1[0][18];
    if(fm.all('CessionDetailType').value=="01")
   	{
    	fm.all('CessionDetailTypeName').value="按合同终止日分保";
   	}
    else if(fm.all('CessionDetailType').value=="02")
   	{
    	fm.all('CessionDetailTypeName').value="按保单终止日分保";
   	}
    else if(fm.all('CessionDetailType').value=="03")
   	{
    	fm.all('CessionDetailTypeName').value="按月分保";
   	}
    	 
    else if (fm.all('CessionMode').value=="3") {
       fm.all('CessionModeName').value = "风险溢额";
       //divCessMO.style.display = 'none';	
	  //divCessMO2.style.display = 'none';
	  divRelDiskM.style.display = 'none';
    }

    fm.all('CessionFeeMode').value	= res1[0][9];
    if(fm.all('CessionFeeMode').value=="1"){
        fm.all('CessionFeeModeName').value= "风险保费方式";
    }else if(fm.all('CessionFeeMode').value=="2"){
    	fm.all('CessionFeeModeName').value="原始保费方式";
    }else{
    	fm.all('CessionFeeModeName').value="修正共保方式";
    }
    fm.all('ReType').value 	=res1[0][16];
    if(fm.all('ReType').value=="01"){
    	fm.all('ReTypeName').value="按险种";
    }else{
    	fm.all('ReTypeName').value="按责任";
    }
    fm.all('ReContState').value = res1[0][17];
    if(fm.all('ReContState').value=="01"){
        fm.all('ReContStateName').value="已审核";
    }else{
    	fm.all('ReContStateName').value="失效";
    }
    
    //分保险种信息
    
    if(fm.ReType.value == "02"){
    	divContGetDuty.style.display ='';
    	var sqlc ="select distinct a.riskcode,b.getdutyname from lrcalfactorvalue a,lmdutyget b where a.riskcode=b.getdutycode "
                  +" and a.recontcode='<%=recontcode%>' with ur";
    	turnPage.queryModal(sqlc, ContGetDutyGrid); //按责任
    }else{ 
      var sqlb = "select distinct a.riskcode ,b.riskname from lrcalfactorvalue a,lmriskapp b where a.riskcode=b.riskcode" 
               +" and recontcode='<%=recontcode%>' with ur";
       turnPage.queryModal(sqlb, ContRiskGrid); //按险种
    }
        //关联险种信息         
    var sqle = "select a.riskcode,b.riskname from LRAmntRelRisk a,lmriskapp b where a.riskcode=b.riskcode"
              +" and recontcode='<%=recontcode%>' with ur";
      turnPage.queryModal(sqle, RelDiskGrid);
     //计算要素信息
     var sqld = "select distinct factorcode,factorvalue from lrcalfactorvalue "
              +" where recontcode='<%=recontcode%>' order by factorcode with ur"; 
     var res1 = easyExecSql(sqld);
     if (res1!=null)
	{
		for(var i=0;i<res1.length;i++)
		{
			if(res1[i][1]==null||res1[i][1]=="")
			{
				continue;
			}
			try{
			   eval("fm.all('"+"Fac"+res1[i][0]+"').value="+res1[i][1]);
			   if(res1[i][0]=="TBLmt"){
				   eval("fm.all('"+"Val"+res1[i][0]+"').value="+res1[i][1]);
			   }
			   showAllCodeName();
		    }catch(ex){
		     }	
		} 
	}       
    //   alert("aaaaaaaaaaa");
                 
}

//分保险种信息
function initContRiskGrid() 
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="险种代码";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=2;
    iArray[1][4]="RiskCode";
    iArray[1][5]="1|2";             	//引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";             	//上面的列中放置引用代码中第几位值
    iArray[1][18]=300;
    iArray[1][19]="200";             	//上面的列中放置引用代码中第几位值

    iArray[2]=new Array();
    iArray[2][0]="险种名称";         	//列名
    iArray[2][1]="250px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许


    ContRiskGrid = new MulLineEnter( "fm" , "ContRiskGrid" );
    ContRiskGrid.mulLineCount = 0;
    ContRiskGrid.displayTitle = 1;
    ContRiskGrid.canSel=1;
    ContRiskGrid.selBoxEventFuncName  ="getRiskCode";
    ContRiskGrid.loadMulLine(iArray);
    ContRiskGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("险种初始化时出错:"+ex);
  }
}

//分保险种责任信息
function initContGetDutyGrid() 
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="责任编码";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=2;
    iArray[1][4]="getDutyCode";
    iArray[1][5]="1|2";             	//引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";             	//上面的列中放置引用代码中第几位值
    iArray[1][15]="riskcode";
    iArray[1][17]="3";
//    iArray[1][7]="getDutyCode";
//    iArray[1][8]="";
    iArray[1][19]="200";             	//上面的列中放置引用代码中第几位值

    iArray[2]=new Array();
    iArray[2][0]="责任名称";         	//列名
    iArray[2][1]="250px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="险种号";         	//列名
    iArray[3][1]="0px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=1; 


    ContGetDutyGrid = new MulLineEnter( "fm" , "ContGetDutyGrid" );
    ContGetDutyGrid.mulLineCount = 1;
    ContGetDutyGrid.displayTitle = 1;
    //ContGetDutyGrid.canSel=1;
    ContGetDutyGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ContGetDutyGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)

    ContGetDutyGrid.loadMulLine(iArray);
    ContGetDutyGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("责任信息初始化时出错:"+ex);
  }
}

//关联险种信息
function initRelDiskGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;
    
  iArray[1]=new Array();
    iArray[1][0]="险种代码";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=2;
    iArray[1][4]="RiskCode";
    iArray[1][5]="1|2";             	//引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";             	//上面的列中放置引用代码中第几位值
    iArray[1][19]="200";             	//

    iArray[2]=new Array();
    iArray[2][0]="险种名称";         	//列名
    iArray[2][1]="250px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    RelDiskGrid = new MulLineEnter( "fm" , "RelDiskGrid" );
    RelDiskGrid.mulLineCount = 0;
    RelDiskGrid.displayTitle = 1;
    //RelDiskGrid.canSel=1;
    RelDiskGrid.loadMulLine(iArray);
    RelDiskGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("关联险种信息初始化时出错"+ex);
  }
}
</script>


