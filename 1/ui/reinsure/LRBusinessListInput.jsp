<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LRBusinessListInput.jsp
//程序功能：
//创建日期：2007-03-30
//创建人  ：huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LRBusinessList.js"></SCRIPT>
	<%@include file="LRBusinessListInit.jsp"%>
</head>

<body onload="initElementtype();initForm();">    
  <form action="" method=post name=fm target="f1print" >
    <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">再保业务账单</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divCessGetData" style= "display: ''">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	        </TR>
	        <TR  class= common>
						<TD  class= title>险种类型</TD>
						<TD  class= input> 
	          	<Input class="codeno" readOnly name= "ReRiskSort" CodeData="0|^1|短险成数分保^2|短险溢额分保^3|长险溢额分保^4|长险成数分保" ondblClick="showCodeListEx('DiskKind',[this,ReRiskSortName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('DiskKind',[this,ReRiskSortName],[0,1],null,null,null,1);" verify="险种类别|NOTNULL"><Input class="codename" name= 'ReRiskSortName' elementtype=nacessary>
	          </TD>
	          <TD  class= title>再保合同</TD>
						<TD  class= input> 
	          	<Input class="codeno" readOnly name= "RecontCode" ondblClick="return showCodeList('RecontCode',[this,RecontCodeName],[0,1],null,fm.all('ReRiskSort').value,'DiskKind',1);" onkeyup="return showCodeListKey('RecontCode',[this,RecontCodeName],[0,1],null,fm.all('ReRiskSort').value,'DiskKind',1);"><Input class="codename" name= 'RecontCodeName' >
	          </TD>
	          <TD  class= title>再保险种</TD>
						<TD  class= input> 
	          	<Input class="codeno" readOnly name= "ReRiskCode" ondblClick="return showCodeList('ReRiskCode',[this,ReRiskCodeName],[0,1],null,fm.all('RecontCode').value,'RecontCode',1);" onkeyup="return showCodeListKey('ReRiskCode',[this,ReRiskCodeName],[0,1],null,fm.all('RecontCode').value,'RecontCode',1);"><Input class="codename" name= 'ReRiskCodeName' >
	          </TD> 
	        </TR>  
	    </table>
	    <br>
		<INPUT class=cssButton  VALUE="分保计算结果明细" TYPE=button onClick="cessDataDetail1();">	
		<INPUT class=cssButton  VALUE="保全计算结果明细" TYPE=button onClick="edorDataDetail();">	
		<INPUT class=cssButton  VALUE="理赔计算结果明细" TYPE=button onClick="claimDataDetail();">	
		</Div> 
  </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 