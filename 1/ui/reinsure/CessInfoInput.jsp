<html>
<%
//name :ReComManage.jsp
//function :ReComManage
//Creator :
//date :2006-08-14
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.reinsure.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "./CessInfoInput.js"></SCRIPT> 
<%@include file="./CessInfoInit.jsp"%>
</head>
<body  onload="initElementtype();initForm()" >
  <form action="" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  
  <Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		合同编号
   			</TD>
   			<TD class= input>
   				<Input class= common name= ReContCode id="ReContCodeId" Readonly > 
   			</TD>
   			<TD class= title></TD>
        <TD class= input></TD>
        <TD class= title></TD>
        <TD class= input></TD>
      </TR>
  </Table> 
  
  <table>
    <tr>
      <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divLLReport1);"></td>
    	<td class= titleImg>分保信息</td></tr>
  </table>
    		
  <Div id= "divLLReport1" style= "display: ''">  		
  	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		分保方式
   			</TD>
   			<TD class= input>
   				<Input class= common name= CessionModeName Readonly > 
   				<Input type="hidden" name= CessionMode > 
   			</TD>
   			<TD class= title>
   				<Div id= "divRetainAmntName" style= "display: ''"> 
          	自留额
          </Div>
        </TD>
        <TD class= input>
        	<Div id= "divRetainAmnt" style= "display: ''"> 
	        	<Input class= common name= FacRetainAmnt > 
	        	<input type="hidden" name="NamRetainAmnt" value="自留额" >
	        	<input type="hidden" name="ValRetainAmnt" value="Float" >
        	</Div>  		
        </TD>
        <TD class= title>
          <Div id= "divCessRateName" style= "display: ''"> 
          	分保比例
          </Div>
        </TD>
        <TD class= input>
        	<Div id= "divCessRate" style= "display: ''"> 
	        	<Input class= common name= FacCessionRate> 
	        	<input type="hidden" name="NamCessionRate" value="分保比例" >
	        	<input type="hidden" name="ValCessionRate" value="Float" >
        	</Div> 
        </TD>
      </TR> 
      
      <TR>
      	<TD class= title>
   		 		接受份额
   			</TD>
   			<TD class= input >
   				<Input class= common name= FacAccQuot> 
   				<input type="hidden" name="NamAccQuot" value="接受份额" >
   				<input type="hidden" name="ValAccQuot" value="Float" >
   			</TD>
   			<TD class= title>
   				自动接受限额
        </TD>
        <TD class= input>
        	<Input class= common name= FacAutoLimit >
        	<input type="hidden" name="NamAutoLimit" value="自动接受限额" >
        	<input type="hidden" name="ValAutoLimit" value="Float" >
        </TD>
        <td class="title">
        	盈余佣金比例
        </td>
        <td class="input">
        	<Input class= common name= FacProfitCom > 
        	<input type="hidden" name="NamProfitCom" value="盈余佣金比例" >
        	<input type="hidden" name="ValProfitCom" value="Float" >
        </td>
      </TR>
      
      <TR>
      	<TD class= title>
   		 		最低再保保额
   			</TD>
   			<TD class= input >
   				<Input class= common name= FacLowReAmnt > 
   				<input type="hidden" name="NamLowReAmnt" value="最低再保保额" >
   				<input type="hidden" name="ValLowReAmnt" value="Float" >
   			</TD>
   			<TD class= title>
   				管理费比例
        </TD>
        <TD class= input>
        	<Input class= common name= FacReinManFeeRate >
        	<input type="hidden" name="NamReinManFeeRate" value="管理费比例" >
        	<input type="hidden" name="ValReinManFeeRate" value="Float" >
        </TD>
        <td class="title">
        	手续费比例
        </td>
        <td class="input">
        	<Input class= common name= FacReinProcFeeRate > 
        	<input type="hidden" name="NamReinProcFeeRate" value="手续费比例" >
        	<input type="hidden" name="ValReinProcFeeRate" value="Float" >
        </td>
      </TR>
      
      <TR>
      	<TD class= title>
   		 		选择折扣比例
   			</TD>
   			<TD class= input>
   				<Input class= common name= FacChoiRebaRate> 
   				<input type="hidden" name="NamChoiRebaRate" value="选择折扣比例" >
   				<input type="hidden" name="ValChoiRebaRate" value="Float" >
   			</TD>
   			<TD class= title>
   				特殊佣金比例
        </TD>
        <TD class= input>
        	<Input class= common name= FacSpeComRate >
        	<input type="hidden" name="NamSpeComRate" value="特殊佣金比例" >
        	<input type="hidden" name="ValSpeComRate" value="Float" >
        </TD>
        <td class="title">
        	理赔参与限额
        </td>
        <td class="input">
        	<Input class= common name= FacClaimAttLmt > 
        	<input type="hidden" name="NamClaimAttLmt" value="理赔参与限额" >
        	<input type="hidden" name="ValClaimAttLmt" value="Float" >
        </td>
      </TR>
      
      <TR>
      	<TD class= title>
   		 		通知限额
   			</TD>
   			<TD class= input>
   				<Input class= common name= FacNoticeLmt > 
   				<input type="hidden" name="NamNoticeLmt" value="通知限额" >
   				<input type="hidden" name="ValNoticeLmt" value="Float" >
   			</TD>
   			<TD class= title></TD>
        <TD class= input></TD>
        <td class="title"></td>
        <td class="input"></td>
      </TR>
   	</Table>
   	<br>
  
  	<table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divContRisk);"></td>
    	<td class= titleImg>合同险种信息</td></tr>
    </table>
   	
   	<Div  id= "divContRisk" style= "display: ''">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanContRiskGrid" ></span>
        		</td>
      		</tr>
    	</table>
	</div>
  
  <input class="cssButton" type="button" value="累计计算险种" onclick="amntRisk()" >
  <input class="cssButton" type="button" value="返  回" onclick="returnClick()" >
  
  <input type="hidden" name="OperateType" >
  <input type="hidden" name="RiskSort" >
  
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>