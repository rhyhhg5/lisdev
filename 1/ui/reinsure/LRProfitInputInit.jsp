<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LRErrorQueryInit.jsp
//程序功能：退保减人查询
//创建日期：2007-04-08 11:10:36
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
                            
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  fm.StartDate.value="2006-1-1";  
	fm.EndDate.value="2006-3-31"
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  { 
  }
  catch(ex)
  {
    alert("在ReInsureInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
	  initInpBox();
		initSelBox();
		initErrorPolGrid();
  }
  catch(re)
  {
    alert("LRErrorQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


// 责任信息列表的初始化
function initErrorPolGrid()
{                               
    var iArray = new Array();      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="错误类型";    	//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="险种保单号";         			//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="错误发生时间";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="错误信息";         		//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      ErrorPolGrid = new MulLineEnter( "fm" , "ErrorPolGrid" ); 
      //这些属性必须在loadMulLine前
      ErrorPolGrid.mulLineCount = 10;   
      ErrorPolGrid.displayTitle = 1;
      ErrorPolGrid.locked = 1;
      ErrorPolGrid.hiddenPlus = 1;
      ErrorPolGrid.canSel = 1;
      ErrorPolGrid.hiddenSubtraction = 1;
      ErrorPolGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>




