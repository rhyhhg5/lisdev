var showInfo;
var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReContManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	if(compareDate(fm.RValidate.value,fm.RInvalidate.value)==1)
	{
		alert("起始日期不能大于终止日期!");
		return false;
	}
	return true;
}

function queryClick()
{
  fm.OperateType.value="QUERY";
  window.open("./FrameReContQuery.jsp?Serial="+fm.ReContCode.value+"&PageFlag=CONT"); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode)
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
  	
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  if (fm.OperateType.value=="DELETE")
	  {
	  	resetForm();
	  }
  }
}

function updateClick()
{
	fm.OperateType.value="UPDATE";
	if(!confirm("你确定要修改该合同吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true ) 
		{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReContManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	if(compareDate(fm.RValidate.value,fm.RInvalidate.value)==1)
	{
		alert("起始日期不能大于终止日期!");
		return false;
	}
	return true;
	
}

function deleteClick()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该合同吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true ) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReContManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	
	return true;
	
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

 /**
   * 对输入域是否是电话的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isTel(strValue)
{
  var NUM="0123456789-() ";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(NUM.indexOf(strValue.charAt(i))<0) return false;

  }
  return true;
}

/**
   * 对输入域是否是网址的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isWeb(strValue)
{
  var NUM=".";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(strValue.indexOf(NUM)<0)
    {
    	return false;
    }
  }
  return true;
}

 /**
   * 对输入域是否是邮编的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isInteger1(strValue)
{
  var NUM="0123456789";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(NUM.indexOf(strValue.charAt(i))<0) return false;

  }
  return true;
}

/**
   * 对输入域是否是邮箱的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isEMail(strValue)
{
  var NUM1="@";
  var NUM2=".";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(strValue.indexOf(NUM1)<0)
    {
    	return false;
    }
    if(strValue.indexOf(NUM2)<0)
    {
    	return false;
    }
  }
  return true;
}
function addCessInfo()
{
	if (fm.ReContCode.value==''||fm.ReContCode.value==null)
	{
		alert("请先查询再保合同再添加分保信息！");
		return false;
	}
	var strSQL="select ReContCode from LRContInfo where ReContCode='"+fm.ReContCode.value+"'";
	var arrResult=easyExecSql(strSQL);
	if (arrResult==null||arrResult=="")
	{
		alert("合同编号为空或未保存合同基本信息！");
		return false;
	}
	var reContCode = fm.all('ReContCode').value;
	var cessionMode = fm.all('CessionMode').value;
	var diskKind = fm.all('DiskKind').value;
  var varSrc="&ReContCode="+ reContCode+"&CessionMode="+cessionMode+"&DiskKind="+diskKind ;
  window.open("./FrameMainCessInfo.jsp?Interface=CessInfoInput.jsp"+varSrc,"true");                   
}  

