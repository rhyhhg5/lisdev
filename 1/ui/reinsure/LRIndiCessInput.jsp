<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：LRIndiCessInput.jsp
//程序功能：个单临分管理
//创建日期：2006-8-28 
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	String tFlag = "";
	tFlag = request.getParameter("type");
%>
<html>
<%	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operFlag = "1";		//区分团险和个险的标志
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
	var spanCode = false;
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="LRIndiCessInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <%@include file="LRIndiCessInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action=""> 
  <Div  id= "queryPage" style= "display: ''" style="float: right">
  	<table class=common>
  		<tr class=common>
  			<TD  class= title>
          印刷号
        </TD>
        <TD  class= input_Acc>
          <Input class=common name="PrtNo" verify="印刷号码|len=11&int">
        </TD>
        <TD  class= title>
          投保人姓名
        </TD>
        <TD  class= input>
          <Input class=common name="AppntName" verify="投保人|len<=20">
        </TD>
        <TD  class= title>
          申请日期
        </TD>
        <TD  class= input>
          <Input class=coolDatePicker name="ApplyDate" verify="申请日期|date">
        </TD>
  		</tr>
  		<tr class=common>
  			<TD  class= title>
  				保单状态
        </TD>
        <TD  class= input>
          <Input class=codeno name="State" CodeData="0|2^0|未签单^1|已签单" 
          onClick="return showCodeListEx('State',[this,StateName],[0,1]);" 
       		onkeyup="return showCodeListKeyEx('State',[this,StateName],[0,1]);" ><input class=codename name=StateName readonly=true >
        </TD>
        <TD  class= title>
          管理机构
        </TD>
        <TD  class= input>
          <Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" 
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >
        </TD>
        <TD  class= title>
        </TD>
        <TD  class= input>
        </TD>
  		</tr>
  	</table>
  </div>
  
    	<INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
  		<INPUT class=cssButton id="riskbutton" VALUE="重  置" TYPE=button onClick="resetForm();">
  		
  <DIV id=DivLCContInfo STYLE="display:''"> 
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 临分申请保单：
    		</td>
    		
    	</tr>  	
    </table>
   </Div>
   <Div  id= "divLCPol1" style= "display: ''" align = left>
   	<tr  class=common>
     		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" ></span> 
  	  	</td>
  	</tr>
  	<br>
  	<Div id= "div1" align="center" style= "display: '' ">
	  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">    
    </div>
   </Div>
   <br>
   <INPUT class=cssButton id="riskbutton" VALUE="临分结论" TYPE=button onClick="cessConclusion();">
   
   <INPUT class=cssButton id="riskbutton" VALUE="保单信息查询" TYPE=button onClick="showContInfo();">
   
  <span id="spanCode"  style="display: none; position:absolute; slategray" onclick="return ;	if(spanCode) showCodeList('bank',[ManageCom],null,null,null,null,1); spanCode=false;" onkeyup="return showCodeListKey('bank',[this],null,null,null,null,1);"></span>	
	<Input type="hidden" name="ApplyType" >
	<Input type="hidden" name="ModifyFlag">
</form>
</body>
</html>
