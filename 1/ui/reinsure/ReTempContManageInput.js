var showInfo;
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
function vCheck(XGrid,check){
	var rowCount=0;
	for(var i=0;i<XGrid.mulLineCount;i++){
		if(XGrid.getChkNo(i)==true){
			if(rowCount>1){
				break;
			}
			rowCount++;
		}
	}
	if(rowCount==0){
		if(check=="1")
			alert('请至少选择一条险种信息');
		else if(check=="2")
			alert('请至少选择一条保障计划信息');
		return false;
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true) 
		{			
			var	risk = ContRiskGrid. mulLineCount;//险种记录数
			var duty = ContGetDutyGrid. mulLineCount;//责任记录数
			var rel = RelDiskGrid.mulLineCount;//关联险种记录数
			var plancode=PlanGrid.mulLineCount;
			if(vCheck(ContRiskGrid,"1")==false){
				return false;
			}
			else if(fm.ContType.value=="2"){
				if(vCheck(PlanGrid,"2")==false){
					return false;
				}
			}
			if(rCheck()==false){
				return false;
			}
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		    fm.action="./ReTempContManageSave.jsp?risk="+risk+"&duty="+duty+"&rel="+rel;
		  	fm.submit(); //提交
	  	}	  	
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}
//合同修改
function updateForm(){
fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true) 
		{
			var	risk = ContRiskGrid. mulLineCount;//险种记录数
			var duty = ContGetDutyGrid. mulLineCount;//责任记录数
			var rel = RelDiskGrid.mulLineCount;//关联险种记录数
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		    fm.action="./ReTempContManageSave.jsp?risk="+risk+"&duty="+duty+"&rel="+rel;
		  	fm.submit(); //提交
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

//合同删除
function deleteForm(){
 fm.OperateType.value="DELETE";
	try 
	{
		if( verifyInput() == true) 
		{
			if (veriryInput2()==true)
			{
			var	risk = ContRiskGrid. mulLineCount;//险种记录数
			var duty = ContGetDutyGrid. mulLineCount;//责任记录数
			var rel = RelDiskGrid.mulLineCount;//关联险种记录数
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		    fm.action="./ReTempContManageSave.jsp?risk="+risk+"&duty="+duty+"&rel="+rel;
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }	
}
//合同审核
function confirmForm(){
	 fm.OperateType.value="CONFIRM";
	try 
	{
		if( verifyInput() == true) 
		{
			var	risk = ContRiskGrid. mulLineCount;//险种记录数
			var duty = ContGetDutyGrid. mulLineCount;//责任记录数
			var rel = RelDiskGrid.mulLineCount;//关联险种记录数
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		    fm.action="./ReTempContManageSave.jsp?risk="+risk+"&duty="+duty+"&rel="+rel;
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }	
}

//删除合同的之前的校验
function veriryInput2()
{
	var sqla = "select count(*) from lrpol where recontcode='"+ fm.all('TempContCode').value+"' fetch first 10 rows only with ur";
	var ssrs = easyExecSql(sqla);
	if(ssrs[0][0]>"0"){
	    if(confirm("该合同下存在已分保的保单，不能删除本合同！")){
	       return false;
	    }	
	}
	return true;
	
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode)
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
  	
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  if (fm.OperateType.value=="DELETE")
	  {
	  //	resetForm();
	  // window.parent.location.reload(); 
       window.parent.close(); 
           
	  }
  }
}

function veriryInput5()
{
	
	return true;
	
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


function getRiskCode()
{
	if(fm.flag.value!="UPDATE"&&fm.flag.value!="CONFIRM"){
		initContGetDutyGrid();
	}
	var selno = ContRiskGrid.getSelNo();
	//var selno2 = GContRiskGrid.getSelNo();
	if (selno !=0){
     ContGetDutyGrid.setRowColData(0,3,ContRiskGrid.getRowColData(selno-1,1)); 
 //    alert(ContGetDutyGrid.getRowColData(0,3));
    }
//	else if(selno2 !=0){
//	     ContGetDutyGrid.setRowColData(0,3,GContRiskGrid.getRowColData(selno2-1,1)); 
//	         //alert(ContGetDutyGrid.getRowColData(0,3));
//	        }
}

function getPlanCode()
{
	var selno = PlanGrid.getSelNo();
	if (selno !=0){
		fm.plancode.value=PlanGrid.getRowColData(selno-1,1);
		if(fm.flag.value!="UPDATE"&&fm.flag.value!="CONFIRM"){
			initContRiskGrid();
			}
		//alert(fm.plancode.value);
		//initGContRiskGrid();
		//initContGetDutyGrid();
//		if(fm.plancode.value!=null){
//			var sql="select a.riskcode,(select RiskName from LMRisk where riskcode=a.riskcode)" +
//     			" from lccontplandutyparam a where a.grpcontno='"+fm.Contno.value+"'" +
//     					" and a.contplancode='"+fm.plancode.value+"' group by a.riskcode";
//			turnPage1.queryModal(sql, ContRiskGrid); 
//		}
//		else{
//			alert("保障计划选择出错");
//			return false;
//		}
 
    }
	
}

function afterCodeSelect( cCodeName, Field ){
	//如果按责任分保，则显示责任信息。
	if(cCodeName=="ReType"){
	if(fm.ReType.value=="01"){
	  divContGetDuty.style.display = 'none';	
	}else if(fm.ReType.value=="02"){
	  divContGetDuty.style.display ='';
	  shit.style.display='';
	  
	}
   }
    if(cCodeName=="ContType"){
		if(fm.ContType.value=="1"){
			divPlanRiskW.style.display = 'none';
			if(fm.Contno.value!=null&&fm.Contno.value!="")
				if(fm.flag.value!="UPDATE"&&fm.flag.value!="CONFIRM")
					getVerify();
		}else if(fm.ContType.value=="2"){
			divPlanRiskW.style.display ='';
			if(fm.Contno.value!=null&&fm.Contno.value!="")
				if(fm.flag.value!="UPDATE"&&fm.flag.value!="CONFIRM")
					getVerify();
	  
		}
	   }
    if(cCodeName=="CessionMode"){
    	if(fm.CessionMode.value=="1"){
    		PremTitle.style.display='none';
    		PremValue.style.display='none';
    	}else if(fm.CessionMode.value=="2"){
    	    PremTitle.style.display='';
    	    PremValue.style.display='';
    	  
    	}
       }
}
function rCheck(){
	var tCessionRate=fm.CessionRate.value;
	var tReinProcFeeRate=fm.ReinProcFeeRate.value;
	var tCessionMode=fm.CessionMode.value;
	var tCessPremRate=fm.CessPremRate.value;
	if(tCessionRate==null||tCessionRate=="")
	{
		alert("分保比例不能为空");
		return false;
	}
	if(tReinProcFeeRate==null||tReinProcFeeRate=="")
	{
		alert("手续费比例不能为空");
		return false;
	}
	if(tCessionRate<0||tCessionRate>1){
		alert("分保比例请录入0-1的数值");
		return false;
	}
	if(tReinProcFeeRate<0||tReinProcFeeRate>1){
		alert("手续费比例请录入0-1的数值");
		return false;
	}
	if(tCessionMode=="2"&&(tCessPremRate==null||tCessPremRate=="")){
		alert("溢额分保请录入再保净费率");
		return false;
	}
	return true;
}

function getVerify(){
  //alert("ddddd");
//   var sql =" select * from lcpol where (GrpContNo='"+fm.Contno.value+"' and conttype='"+fm.ContType.value+"' ) "
//            +" or (Contno='"+fm.Contno.value+"' and conttype='"+fm.ContType.value+"' and grpcontno='00000000000000000000') fetch first 1 rows only with ur";
	var sql =" select 1 from lcgrpcont where GrpContNo='"+fm.Contno.value+"' "
  +" union all " +
  		" select 1 from lccont where Contno='"+fm.Contno.value+"'   fetch first 1 rows only with ur";
    var ssrs = easyExecSql(sql);
	if(ssrs == null){
	    if(confirm("保单类型或该保单号错误！")){
	    	initPlanGrid();
	    	return false;
	    }	
	}
	else if(fm.ContType.value=="1"){
		var  StrSql=" select riskcode,(select riskname from LMRisk where riskcode=a.riskcode)" +
				" from lcpol a where contno='"+fm.Contno.value+"' group by riskcode ";
		turnPage.queryModal(StrSql, ContRiskGrid);
	}
	else if(fm.ContType.value=="2")
	{
		sql="select  contplancode,contplanname from lccontplandutyparam  " +
				"where grpcontno='"+fm.Contno.value+"' and contplancode<>'11' " +
						"group by contplancode,contplanname with ur"
		turnPage.pageLineNum = 100;
		

		turnPage.queryModal(sql, PlanGrid); 
		var  StrSql=" select riskcode,(select riskname from LMRisk where riskcode=a.riskcode) " +
				"from lccontplandutyparam a where grpcontno='"+fm.Contno.value+"' group by riskcode";
		turnPage.queryModal(StrSql, ContRiskGrid);
	}
	else if(fm.flag.value!="UPDATE"&&fm.flag.value!="CONFIRM") initContRiskGrid();
	return true;        
}