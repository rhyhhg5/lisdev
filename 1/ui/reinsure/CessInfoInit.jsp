<html>
<%
//name :CessInfoInit.jsp
//function :Manage CessInfo
//Creator :
//date :2006-08-15
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 		GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator			= tG.Operator;
  	String Comcode			= tG.ManageCom;
 		String CurrentDate	= PubFun.getCurrentDate();   
    String tCurrentYear	=	StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=	StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate	=	StrTool.getVisaDay(CurrentDate);   
    
    String reContCode 	= request.getParameter("ReContCode"); 
    String cessionMode 	= request.getParameter("CessionMode"); 
    String riskSort 		= request.getParameter("RiskSort");         
    String cessionModeName = "";
    if (cessionMode.equals("1"))
    {
    	cessionModeName="成数";
    }else if(cessionMode.equals("2"))
    {
    	cessionModeName="溢额";
    }
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.ReContCode.value	 			= "<%=reContCode%>";
  	fm.CessionMode.value 			= "<%=cessionMode%>";
  	fm.RiskSort.value 	 			= "<%=riskSort%>";
  	fm.CessionModeName.value 	= "<%=cessionModeName%>";
  	fm.FacRetainAmnt.value 		= "";
  	fm.FacCessionRate.value 	= "";
  	fm.FacAccQuot.value 			= "";
  	fm.FacAutoLimit.value 		= "";
  	fm.FacProfitCom.value 		= "";
  	fm.FacLowReAmnt.value 		= "";
  	fm.FacReinManFeeRate.value= "";
  	fm.FacReinProcFeeRate.value = "";
  	fm.FacChoiRebaRate.value 	= "";
  	fm.FacSpeComRate.value 		= "";
  	fm.FacClaimAttLmt.value 	= "";
  	fm.FacNoticeLmt.value 		= "";
  	
  	if(fm.CessionMode.value=="1")
  	{
  		divRetainAmnt.style.display="none";
  		divRetainAmntName.style.display="none";
  		divCessRateName.style.display="";
  		divCessRate.style.display="";
  	}else if(fm.CessionMode.value=="2")
  	{
  		divRetainAmnt.style.display="";
  		divRetainAmntName.style.display="";
  		divCessRate.style.display="none";
  		divCessRateName.style.display="none";
  	}
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initContRiskGrid();
  	initInfo();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initContRiskGrid() 
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="险种代码";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=2;
    iArray[1][4]="RiskCode";
    iArray[1][5]="1|2";             	//引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";             	//上面的列中放置引用代码中第几位值
    iArray[1][19]="200";             	//上面的列中放置引用代码中第几位值

    iArray[2]=new Array();
    iArray[2][0]="险种名称";         	//列名
    iArray[2][1]="250px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    //iArray[3]=new Array();
    //iArray[3][0]="";         				//列名
    //iArray[3][1]="60px";            //列宽
    //iArray[3][2]=60;            		//列最大值
    //iArray[3][3]=1;              		//是否允许输入,1表示允许，0表示不允许


    ContRiskGrid = new MulLineEnter( "fm" , "ContRiskGrid" );
    ContRiskGrid.mulLineCount = 0;
    ContRiskGrid.displayTitle = 1;
    //ContRiskGrid.canSel=1;
    ContRiskGrid.loadMulLine(iArray);
    ContRiskGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("初始化时出错:"+ex);
  }
}

</script>


