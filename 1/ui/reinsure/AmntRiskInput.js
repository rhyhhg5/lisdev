var showInfo;

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true && RelDiskGrid.checkValue("RelDiskGrid"))
		{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./AmntRiskSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	return true;
}

function queryClick()
{
	alert("只能显示本合同的累计险种信息"); 
  //fm.OperateType.value="QUERY"; 
  //window.open("./FrameAmntRiskQuery.jsp?ReContCode="+fm.ReContCode.value); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) 
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else 
  { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  resetForm();
  }
}

function updateClick()
{
	if(!confirm("你确定要修改该合同的累计险种信息吗？"))
	{
		return false;
	}
	
	fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true && RelDiskGrid.checkValue("RelDiskGrid")) 
		{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./AmntRiskSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	for(var n=0;n<RelDiskGrid.mulLineCount;n++) 
	{ 
		if (RelDiskGrid.getRowColData(n,1).length>2)
		{
			alert("组名长度不能超过两位字符！");
			return false;
		}
		
		if (RelDiskGrid.getRowColData(n,1)==""||RelDiskGrid.getRowColData(n,1)==null)
		{
			alert("组名不能为空！");
			return false;
		}
		
		if (RelDiskGrid.getRowColData(n,2)==""||RelDiskGrid.getRowColData(n,2)==null)
		{
			alert("险种代码不能为空！");
			return false;
		}
		
	  for(var m=n+1;m<RelDiskGrid.mulLineCount;m++) 
	  { 
	    if(RelDiskGrid.getRowColData(n,2)==RelDiskGrid.getRowColData(m,2)) 
	    {
	        alert("不能录入重复的险种！");	
	        return false; 
	    }
	  }
	}
	
	return true;
}

function deleteClick()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该合同的累计险种信息吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true ) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.action="./AmntRiskSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	
	return true;
	
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function initInfo() 
{
  if (fm.ReContCode.value==null||fm.ReContCode.value=="")
  {
  	alert("没有合同编码信息!");
  	return false;
  }
  
  var strSQL = "";
  strSQL = "select * from LRContInfo where RecontCode='"+fm.ReContCode.value+"'";
  strArray = easyExecSql(strSQL);
  if (strArray==null)
  {
  	alert("没有此合同编码!");
  	return false;
  }
  
	strSQL = "select a.GroupName,a.RiskCode,b.RiskName from LRAmntRelRisk a,LMRisk b where RecontCode='"+fm.ReContCode.value+"' and a.riskcode=b.riskcode"
	;
	strArray=easyExecSql(strSQL);
	RelDiskGrid.clearData();
	if (strArray!=null)
	{
		for(var k=0;k<strArray.length;k++)
		{
			RelDiskGrid.addOne("RelDiskGrid");
			RelDiskGrid.setRowColData(k,1,strArray[k][0]);
			RelDiskGrid.setRowColData(k,2,strArray[k][1]);
			RelDiskGrid.setRowColData(k,3,strArray[k][2]);
		}
	}
}

function returnClick()
{
	top.close();
}