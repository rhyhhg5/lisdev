<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LRCessClaimInput.jsp
//程序功能：
//创建日期：2010-09-25
//创建人  ：龙程彬
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 	
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LRCessClaimInput.js"></SCRIPT> 
	<%@include file="LRCessClaimInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>

<body onload="initElementtype();initInpBox();">    
  <form action="" method=post name=fm target="fraSubmit" >
    <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">在保提数</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divCessGetData" style= "display: ''" style="float: right">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
			  <TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	        </TR>
	    </table>
	    <br>
		<INPUT class=cssButton  VALUE="分保数据提取" TYPE=button onClick="getCessData();">	
		<INPUT class=cssButton  VALUE="理赔数据提取" TYPE=button onClick="getClaimData();">	
		</Div>
    <input type="hidden" name=OperateType value="">
    <input type="hidden" name="Operator" value="">
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 