<%
//程序名称：ReComManageSave.jsp
//程序功能：
//创建日期：2006-08-17
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  LRCalFactorValueSet mLRCalFactorValueSet = new LRCalFactorValueSet();
  LRContInfoSchema mLRContInfoSchema = new LRContInfoSchema();
  
  ReCessInfoUI mReCessInfoUI = new ReCessInfoUI();
  
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  		= "";
  String FlagStr 		= "";
  String Content 		= "";
  String mDescType 	= "";	//将操作标志的英文转换成汉字的形式
  
  System.out.println("开始进行获取数据的操作！！！"); 
  
  HashMap facMap = new HashMap(); //存放要素名与值
  ArrayList facList = new ArrayList(); //存放要素
  ArrayList namList = new ArrayList(); //存放要素名称
  ArrayList valList = new ArrayList(); //存放要素类型
  
  Enumeration enu = request.getParameterNames();
  String factor = "";
  
  while(enu.hasMoreElements())
	{
		factor = (String)enu.nextElement();
		
		if(factor.substring(0,3).equals("Fac"))
		{
			facList.add(factor);
			facMap.put(factor,request.getParameter(factor));
		}
		if(factor.substring(0,3).equals("Nam"))
		{
			namList.add(factor);
		}
		if(factor.substring(0,3).equals("Val"))
		{
			valList.add(factor);
		}
	}
  
  String reContCode 	= request.getParameter("ReContCode");
  String riskSort 		= request.getParameter("RiskSort");
  mLRContInfoSchema.setReContCode(reContCode);
  mLRContInfoSchema.setDiskKind(riskSort);
  
	System.out.println("in jsp0 ->before submitData.........."); 
  /****************************************************************************
   * 判断单证的类型，若是定额单证则要对定额单险种信息表进行描述
   ***************************************************************************/

  String[] strNumber 			= request.getParameterValues("ContRiskGridNo");
  String[] strRiskCode 		= request.getParameterValues("ContRiskGrid1");
  String[] strRiskName	 	= request.getParameterValues("ContRiskGrid2");
  
  String factorCode = "";
  String factorName = "";
  String factorValue = "";
  
	System.out.println("in jsp1 ->before submitData..........");    
  if(strNumber!=null)
  {  
  	int tLength = strNumber.length;	
    for(int i = 0 ;i < tLength ;i++)
    {
	  	Iterator facIte = facList.iterator();
	  	
    	while(facIte.hasNext())
		  {
		  	factorCode = (String)facIte.next();
		  	
		  	LRCalFactorValueSchema tLRCalFactorValueSchema = new LRCalFactorValueSchema();
						  	
		    tLRCalFactorValueSchema.setReContCode(reContCode);
		    tLRCalFactorValueSchema.setRiskSort(riskSort);
		    tLRCalFactorValueSchema.setRiskCode(strRiskCode[i]);
		    
		    tLRCalFactorValueSchema.setFactorCode(factorCode.substring(3)); //添加要素代码
		    
		    tLRCalFactorValueSchema.setFactorValue(request.getParameter(factorCode));
		    Iterator namIte = namList.iterator(); //添加要素名称
		    while(namIte.hasNext())
		  	{  
		  		factorName = (String)namIte.next();
		  		if (factorName.substring(3).equals(factorCode.substring(3)))
		  		{
		  			tLRCalFactorValueSchema.setFactorName(request.getParameter(factorName));
		  		}
		  	}
		  			  			  	
		  	Iterator valIte = valList.iterator(); //添加要素数值类型
		    while(valIte.hasNext())
		  	{  
		  		factorValue = (String)valIte.next();
		  		
		  		if (factorValue.substring(3).equals(factorCode.substring(3)))
		  		{
		  			tLRCalFactorValueSchema.setValueType(request.getParameter(factorValue));
		  		}
		  	}
		    mLRCalFactorValueSet.add(tLRCalFactorValueSchema);
		  }
    }
  }
	
	for (int i=0;i<mLRCalFactorValueSet.size();i++)
	{
		System.out.println("Save aaa:  "+mLRCalFactorValueSet.get(i+1).getFactorCode());
	}
	
  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增再保公司";
  }
  if(mOperateType.equals("UPDATE"))
  {
    mDescType = "修改再保公司信息";
  }
  if(mOperateType.equals("DELETE"))
  {
    mDescType = "删除再保公司";
  }
  if(mOperateType.equals("QUERY"))
  {
    mDescType = "查询再保公司";
  }

  VData tVData = new VData();
  try
  {
  	tVData.addElement(globalInput);
    tVData.addElement(mLRCalFactorValueSet);
    tVData.addElement(mLRContInfoSchema);
    mReCessInfoUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReCessInfoUI.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+"成功，"+" 合同编号："+mReCessInfoUI.getResult();
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mReCessInfoUI.getResult()%>");
</script>
</html>