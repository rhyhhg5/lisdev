/*********************************************************************
 *  程序名称：LRCessQuery.js
 *  程序功能：再保新单续期查询
 *  创建日期：2007-04-09 
 *  创建人  ：Huxl
 *  返回值：  无
 *  更新记录：  更新人    更新日期     更新原因/内容
 *********************************************************************
 */
var turnPage = new turnPageClass();
/**
	*查询核保发起任务
	*/
function easyQueryClick()
{
	var str = "";
	if(fm.ContNo.value != null && trim(fm.ContNo.value) != ""){
		str = " and ((a.ContType = '1' and a.ContNo = '"+fm.ContNo.value+"') or (a.Conttype = '2' and a.GrpContNo = '"+fm.ContNo.value+"')) ";
	}
	mSQL = "select (case a.Conttype when '1' then a.ContNo else a.GrpContNo end),a.InsuredNo,a.insuredName," +
				 "a.riskcode,d.ReComCode,a.RecontCode,b.PayCount,c.CessionRate,c.CessionAmount,c.CessPrem, "+
				 "c.ReProcFee,c.ManageFee " + 
				 "from LRPol a, LRPolDetail b,LRPolResult c,LRContInfo d "+
				 "where a.PolNo = b.Polno and a.Polno=c.polno and a.Renewcount = b.RenewCount "+
				 "and a.Renewcount = c.RenewCount and a.riskCalsort = '1' and b.paycount=c.paycount "+
				 "and ((a.GetDataDate between '" + fm.StartDate.value + "' and '" +fm.EndDate.value
				 +"' and b.GetDataDate <= '"+fm.EndDate.value+"') or (a.GetDataDate < '"+fm.StartDate.value
				 +"' and b.GetDataDate between '"+fm.StartDate.value +"' and '"+fm.EndDate.value+"')) "
				 + str 
				 +getWherePart("a.InsuredNo","InsuredNo")
				 +getWherePart("a.Riskcode","RiskCode")
				 +getWherePart("d.ReComCode","ReComCode")
				 +getWherePart("a.ReContCode","ReContCode")
				 +getWherePart("a.tempcessflag","TempCessFlag")
				 +" union " +
				 "select (case a.Conttype when '1' then a.ContNo else a.GrpContNo end),a.InsuredNo,a.insuredName," +
				 "a.riskcode,d.ReComCode,a.RecontCode,d.PayCount,c.CessionRate,c.CessionAmount,c.CessPrem, "+
				 "c.ReProcFee,c.ManageFee " + 
				 "from LRPol a, LRPolResult c,LRContInfo d "+
				 "where a.Polno=c.polno and a.Renewcount = c.RenewCount and a.riskCalsort != '1' "+
				 "and a.GetDataDate between '" + fm.StartDate.value + "' and '" +fm.EndDate.value+"' "
				 + str 
				 +getWherePart("a.InsuredNo","InsuredNo")
				 +getWherePart("a.Riskcode","RiskCode")
				 +getWherePart("d.ReComCode","ReComCode")
				 +getWherePart("a.ReContCode","ReContCode")
				 +getWherePart("a.tempcessflag","TempCessFlag");
	turnPage.queryModal(mSQL,CessPolGrid);
}		
function resetForm()
{
	fm.InsuredNo.value = "";
	fm.ContNo.value = "";
	fm.RiskCode.value = "";
	fm.ReComCode.value = "";
	fm.ReComCodeName.value = "";
	fm.ReContCode.value = "";
	fm.ReContCodeName.value = "";
	fm.TempCessFlag.value = "";
	fm.TempCessFlagName.value = "";
	fm.StartDate.value="2006-1-1";  
	fm.EndDate.value="2006-3-31"
}


   
