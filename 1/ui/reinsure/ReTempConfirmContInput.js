var showInfo;
var turnPage = new turnPageClass(); 

//合同维护
function submitForm(){	
	var selno = ContGrid.getSelNo();
	if (selno !=0){
    var str = ContGrid.getRowColData(selno-1,1);
    //alert(str);
    showInfo =window.open("./ReTempContManageMain.jsp?flag=CONFIRM&tempcontcode="+str,"合同审核",'width=1000,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');

    }else{
    alert("请选择合同！");	
    }
}

//查询合同信息
function QueryInfo(){

	var strSQL = "select a.TempContCode,a.TempContName,a.ReContState,a.DiskKind,a.CessionMode,"
	    +"(select appntno from lcpol where grpcontno=a.contno fetch first 1 rows only),"
	    +"(select appntname from lcpol where grpcontno=a.contno fetch first 1 rows only) from LRTempCessCont a  where 1=1"
	    +" and a.ContType='2' "
	  + getWherePart("a.TempContCode","TempContCode")
	  + getWherePart("a.ReContState","ReContState")
	  + getWherePart("a.CessionMode","CessionMode")
	  + getWherePart("a.DiskKind","DiskKind")
	  + getWherePart("a.ContNo","Contno")
	  + getWherePart("a.ContType","ContType")
	  +getWherePart("a.ComCode","ComCode")
	   +" union all select a.TempContCode,a.TempContName,a.ReContState,a.DiskKind,a.CessionMode,"
	   +"(select appntno from lcpol where contno=a.contno fetch first 1 rows only),"
	   +"(select appntname from lcpol where contno=a.contno fetch first 1 rows only) from LRTempCessCont a  where 1=1"
	   +" and a.ContType='1' "
	  + getWherePart("a.TempContCode","TempContCode")
	  + getWherePart("a.ReContState","ReContState")
	  + getWherePart("a.CessionMode","CessionMode")
	  + getWherePart("a.DiskKind","DiskKind")
	  + getWherePart("a.ContNo","Contno")
	  + getWherePart("a.ContType","ContType")
	  +getWherePart("a.ComCode","ComCode")
	  ;
	strSQL = strSQL +" order by TempContCode";
	turnPage.queryModal(strSQL, ContGrid);
}

function CessForm()
{
	var selno = ContGrid.getSelNo();
	if (selno !=0){
      var strCode = ContGrid.getRowColData(selno-1,1);
      var str = "select ReContState from LRTempCessCont where TempContCode='"+strCode+"' with ur";
      var  strRes = easyExecSql(str);
      if(strRes == null || strRes[0][0]=="02"){
    	alert("请对该合同先时行审核操作！");  
    	return false;  	
       }else{
         var showStr="正在进行临时分保提数，请您稍候并且不要修改屏幕上的值或链接其他页面";
		 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		 cessfm.action="./ReTempCessSave.jsp?tempcode="+strCode;
		 cessfm.submit(); //提交
       }   
    }else{
    	alert("请选择临分合同！");
    	return false;
    }   
}

function afterSubmit( FlagStr, content, ReComCode, CertifyCode)
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
  	
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  if (fm.OperateType.value=="DELETE")
	  {
	  //	resetForm();
	  // window.parent.location.reload(); 
       window.parent.close(); 
           
	  }
  }
}