<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：LRIndiCessInput.jsp
//程序功能：个单临分结论
//创建日期：2006-8-30 
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	String tFlag = "";
	tFlag = request.getParameter("type");
%>
<html>
<%	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operFlag = "1";		//区分团险和个险的标志
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
	var spanCode = false;
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="GrpCessConclusionInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <%@include file="GrpCessConclusionInit.jsp"%>
</head>
<body  onload="initElementtype();initForm()" >
	
  <form method=post name=fm target="fraSubmit" action=""> 
 	 <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCessInfo);"></td>
          <td class="titleImg">团体保单信息</td>
        </tr>
      </table>
  </div>
  
  <Div  id= "divGrpCessInfo" style= "display: ''" style="float: right">
  	<table class=common>
  		<tr class=common>
  			<TD  class= title>
          印刷号
        </TD>
        <TD  class= input_Acc>
          <Input class=common name="PrtNo" readonly >
        </TD>
        <TD  class= title>
          投保单位
        </TD>
        <TD  class= input colspan=3 >
          <Input class=common name="AppntName" id="AppntNameI" style="width:96%" readonly >
        </TD>
  		</tr>
  		<tr>
        <TD  class= title>
          企业类型
        </TD>
        <TD  class= input>
          <Input class=common name="GrpName" readonly >
        </TD>
        <TD  class= title>
          管理机构
        </TD>
        <TD  class= input>
          <Input class=common name="ManageCom" readonly >
        </TD>
        <TD  class= title>
        	保单状态
        </TD>
        <TD  class= input>
        	<Input class=common name="StateName" readonly >
        	<Input class=common type="hidden" name="State"  >
        </TD>
  		</tr>
	  </table>
  </Div>
  
  <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCessContInfo);"></td>
          <td class="titleImg">团体临分协议信息</td>
        </tr>
      </table>
  </div>
  <Div  id= "divGrpCessContInfo" style= "display: ''" style="float: right">
  	<table class=common>
  		<tr class=common>
  			<TD  class= title>
          协议编号
        </TD>
        <TD  class= input_Acc>
<%
				if(modifyFlag.equals("1")) //如果是修改页面则"再保协议编号"设为readonly
				{
%>        	
          <Input class=common name="ReContCode" verify="再保协议编号|NOTNULL" elementtype=nacessary readonly >
<%
				}else{
%>
					<Input class=common name="ReContCode" verify="再保协议编号|NOTNULL" elementtype=nacessary>
<%
				}
%>        	
        </TD>
        <TD  class= title>
        	协议名称
        </TD>
        <TD  class= input colspan=3>
          <Input class= common name= ReContName id="ReContNameId" style="width:96%"> 
        </TD>
  		</tr>
  		<tr class=common>
  			<TD  class= title>
          再保公司编号
        </TD>
        <TD  class= input_Acc>
          <input class="code" name="ReComCode" 
	         ondblclick="return showCodeList('reinsurecomcode',[this,ReComName],[0,1],null,null,null,1,300);"
	         onkeyup="return showCodeListKey('reinsurecomcode', [this,ReComName],[0,1],null,null,null,1,300);" 
	         verify="再保公司编号|NOTNULL" elementtype=nacessary>
        </TD>
        <TD  class= title>
          再保公司名称
        </TD>
        <TD  class= input colspan=3>
          <Input class= common name= ReComName id="ReComNameId" style="width:96%" >
        </TD>
  		</tr>
  		<tr class=common>
  			<TD  class= title>
          合同录入日期
          
        </TD>
        <TD  class= input_Acc>
          <Input class="coolDatePicker" name="InputDate" elementtype=nacessary>
        </TD>
        <TD  class= title></TD>
        <TD  class= input></TD>
        <TD  class= title></TD>
        <TD  class= input></TD>
  		</tr>
  	</table>
  </div>
  <br>
  
  <DIV id=DivLCContInfo STYLE="display:''"> 
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 临分申请保单：
    		</td>
    	</tr>  	
    </table>
  </Div>
   <Div  id= "divLCPol1" style= "display: ''" align = left>
   	<tr  class=common>
     		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" ></span> 
  	  	</td>
  	</tr>
  	<br>
  	<Div id= "div1" align="center" style= "display: '' ">
	  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">    
    </div>
   </Div>
   <br>
   <INPUT class=cssButton id="riskbutton" VALUE="团体临分自检信息" TYPE=button onClick="showGrpTempCess();">		 
<%
		if(modifyFlag.equals("1"))
		{
%>  	
  	<INPUT class=cssButton id="riskbutton" VALUE="个人临分自检信息" TYPE=button onClick="modifyIndivTempCessQuery();">
<%
		}else{
%>  	
  	<INPUT class=cssButton id="riskbutton" VALUE="个人临分自检信息" TYPE=button onClick="indivTempCessQuery();">
<%
		}
%>
   
   <INPUT class=cssButton id="riskbutton" VALUE="人工核保信息" TYPE=button onClick="UWQuery();">
   <INPUT class=cssButton id="riskButtonId" Style="display:''" VALUE="临分协议查询" TYPE=button onClick="easyQueryClick();">
   <br><br>
   <INPUT class=cssButton id="riskbutton" VALUE="确&nbsp;&nbsp;&nbsp;&nbsp;定" TYPE=button onClick="submitForm();">
   <INPUT class=cssButton id="riskbutton" VALUE="返&nbsp;&nbsp;&nbsp;&nbsp;回" TYPE=button onClick="returnClick();">
   
  <span id="spanCode"  style="display: none; position:absolute; slategray" onclick="return ;	if(spanCode) showCodeList('bank',[ManageCom],null,null,null,null,1); spanCode=false;" onkeyup="return showCodeListKey('bank',[this],null,null,null,null,1);"></span>	
  
	<Input type="hidden" 	name="ApplyType" >
	<Input type="hidden" 	name="ProposalContNo" > 
	<Input type="hidden" 	name="OperateType" >
	<Input type="hidden" 	name="SaveFlag" >
	<Input type="hidden"	name="ModifyFlag" >
	
</form>
</body>
</html>
