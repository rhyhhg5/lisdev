<%
//程序名称：ReNewClmDetailSave.jsp
//程序功能：
//创建日期：2008-11-15
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  ReNewClmDetailUI mReNewClmDetailUI = new ReNewClmDetailUI();
  CErrors tError = null;

  String FlagStr = "";
  String Content = "";
  String mReComCode= request.getParameter("ReComCode");
  String mCessionMode= request.getParameter("CessionMode");
  String mDiskKind= request.getParameter("DiskKind");
  String mRiskCode= request.getParameter("RiskCode");
  String mYear= request.getParameter("Year");
  String mMonth= request.getParameter("Month");
  String mContNo= request.getParameter("ContNo");
  String mAppntName= request.getParameter("AppntName");
  String mInsuredName= request.getParameter("InsuredName");

	String mOperateType = request.getParameter("OperateType");
  String tChk[]=request.getParameterValues("InpNewEdorDetailGridChk");
  String tReNewCount[] = request.getParameterValues("NewEdorDetailGrid16");
  String tPolNo[] = request.getParameterValues("NewEdorDetailGrid17");
  String tActuGetNo[] = request.getParameterValues("NewEdorDetailGrid18");
  LRPolSet tLRPolSet=new LRPolSet();
  if(tChk!=null){
  	for(int i=0;i<tChk.length;i++){
			if(tChk[i]!=null && tChk[i].equals("1")){
				LRPolSchema tLRPolSchema=new LRPolSchema();
				tLRPolSchema.setPolNo(tPolNo[i]);
				tLRPolSchema.setReNewCount(tReNewCount[i]);  
				tLRPolSchema.setActuGetNo(tActuGetNo[i]);
				tLRPolSet.add(tLRPolSchema);
			}
		}
	}
  

	TransferData tTransferData = new TransferData();
 	
 	//查询条件
 	tTransferData.setNameAndValue("ReComCode",mReComCode);
 	tTransferData.setNameAndValue("CessionMode",mCessionMode);
 	tTransferData.setNameAndValue("DiskKind",mDiskKind);
 	tTransferData.setNameAndValue("RiskCode",mRiskCode);
 	tTransferData.setNameAndValue("Year",mYear);
 	tTransferData.setNameAndValue("Month",mMonth);
 	tTransferData.setNameAndValue("ContNo",mContNo);
 	tTransferData.setNameAndValue("AppntName",mAppntName);
 	tTransferData.setNameAndValue("InsuredName",mInsuredName);

  VData tVData = new VData();
  try
  {
  	tVData.addElement(tTransferData);
  	tVData.addElement(globalInput);
  	tVData.addElement(tLRPolSet);
    mReNewClmDetailUI.submitData(tVData,mOperateType);
    CreateAccountBL tCreateAccountBL=new CreateAccountBL();
    tCreateAccountBL.dealData(tActuGetNo);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReNewClmDetailUI.mErrors;
    if (!tError.needDealError())
    {
      Content = "保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<%
	if(mOperateType.equals("ONLOAD")){
		VData mResult = mReNewClmDetailUI.getResult();
		String url=(String)mResult.get(0);
		System.out.println(url);
		%> 
		<HTML>
			<script language="javascript">
				parent.fraInterface.downAfterSubmit("<%=url%>");
				</script>
		</HTML>   
<%
 }else{
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
<%}%>

