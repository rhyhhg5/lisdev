<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
/*
*程序名称：LRBillDetailInput.jsp
*程序功能：清单明细
*创建日期：2010-04-12
*创建人  ：石和平
*/
/*
*更新记录：
*更新人：    
*更新日期：     
*更新原因/内容：
*/
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<script src="../common/javascript/Common.js" ></script>
	<script src="../common/javascript/MulLine.js"></script>
	<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
	<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
	<script src="../common/javascript/VerifyInput.js"></script>
	<script src="../common/Calendar/Calendar.js"></script>
	<script src="../common/cvar/CCodeOperate.js"></script>
	<script src="LRBillDetailInput.js"></script>
	<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
	<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">	
	<%@include file="./LRBillDetailInit.jsp"%>
	<title>清单明细</title>
	
</head>
<body onload="initElementtype();initForm();">
	<form name="fm" target="fraSubmit" method="post" action="./LRBillDetailSave.jsp">
		<div style="width:200">
	    	<table class="common">
	      		<tr class="common">
	      			<td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInfo);"></td>
	      			<td class="titleImg">请输入查询条件</td>
	      		</tr>
	    	</table>
		</div>
		<div id="divInfo" style= "display: ''">
			<table class="common">
				<tr class="common">
				   	<td class="title">保单号</td>
   					<td class="input"><input class="common" name="ContNo"/></td>
   					<td class="title">再保合同</td>
   					<td class="input">
   						<Input class="code" name="RecontCode" 
   							ondblClick="return showCodeList('RecontCode',[this,null],[0,1],null,null,null,1);" 
   							onkeyup="return showCodeListKey('RecontCode',[this,null],[0,1],null,null,null,1);">
   					</td>
	    		 	<td class="title">再保公司</td>
	   				<td class="input">
	   					<input class="codeno" name="ReinsureCom" 
		            		ondblclick="return showCodeList('reinsurecomcode',[this,ReinsureComName],[0,1],null,null,null,1,300);"
		            			onkeyup="return showCodeListKey('reinsurecomcode', [this,ReinsureComName],[0,1],null,null,null,1,300);"/><input class="codename" name= 'ReinsureComName' readonly/>
	   				</td>
	        	</tr>
	        	<tr class="common">
	        		<td class="title">分保类型</td>
	           		<td class="input">
        					<input class="codeno" readOnly name= "TempCessFlag" CodeData="0|^Y|临时分保^N|合同分保" 
          						ondblClick="showCodeListEx('ReinsureType',[this,TempCessFlagName],[0,1],null,null,null,1);	"
          							onkeyup="showCodeListKeyEx('ReinsureType',[this,TempCessFlagName],[0,1],null,null,null,1);"><input class="codename" name= "TempCessFlagName" readonly>
        			</td>
	        		<td class="title">险种类别</td>
	   				<td class="input">
   						<input class="codeno" readOnly name= "DiskKind" CodeData="0|^L|长险^M|短险" 
          						ondblClick="showCodeListEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);	"
          							onkeyup="showCodeListKeyEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);"><input class="codename" name= "DiskKindName" readonly>
   					</td>
	   				<td class="title">分保方式</td>
        			<td class="input">
        				<Input class="codeno" readOnly name= "RiskCalSort" CodeData="0|^1|成数^2|溢额" 
          					ondblClick="showCodeListEx('CessionMode',[this,RiskCalSortName],[0,1],null,null,null,1);"
          					onkeyup="showCodeListKeyEx('CessionMode',[this,RiskCalSortName],[0,1],null,null,null,1);" ><input class="codename"  readonly name="RiskCalSortName" />
        			</td>			
	        	</tr>
	        	<tr class="common">
	        		<td class="title">险种编码</tD>
        			<td class="input"><input class="codeno" name="RiskCode" 
          						ondblclick = "return showCodeList('criskcode',[this,RiskName],[0,1],null,[fm.ReinsureCom.value,fm.RecontCode.value],1,1,300);"
          						onkeyup = "return showCodeListKey('criskcode',[this,RiskName],[0,1],null,[fm.ReinsureCom.value,fm.RecontCode.value],1,1,300);"><input class="codename" name= 'RiskName' /> 
        			</td>
	        		<td class="title">年份</td>
        			<td class="input"><input class="code" readOnly name= "Year" CodeData="0|^1|2008^2|2009^3|2010^4|2011^5|2012^6|2013^7|2014^8|2015^9|2016^10|2017^11|2018^12|2019^13|2020^14|2021^15|2022^16|2023^17|2024^18|2025^19|2026^20|2027" 
          				ondblClick="showCodeListEx('Year',[this],[1],null,null,null,1);	"
          				onkeyup="showCodeListKeyEx('Year',[this],[1],null,null,null,1);"/>
   					</td>
   					<td class="title">月份</td>
   					<td class="input"><input class="code" readOnly name= "Month" CodeData="0|^1|月^2|月^3|月^4|月^5|月^6|月^7|月^8|月^9|月^10|月^11|月^12|月" 
          				ondblClick="showCodeListEx('Month',[this],[0],null,null,null,1);	"
          				onkeyup="showCodeListKeyEx('Month',[this],[0],null,null,null,1);"/>
   					</td>
	        	</tr>
	        	<tr class="common">
	        		<td class="title">投保人</td>
	        		<td class="input"><input class="common" name="AppntName"></td> 			        		
	        		<td class="title">被保险人</td>
	        		<td class="input"><input class="common" name="InsuredName"></td>
	        	</tr>
	        </table>
		</div>
		<input class="cssButton" id="riskbutton" value="查  询" type="button" onClick="easyQueryClick();">
		<!--  input class="cssButton" id="riskbutton" value="选  择  合  同" type="button" onClick="easyQueryContClick();"-->
		<!--  div style="width:200" >
	    	<table class="common">
	      		<tr class="common">
	      			<td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divResult);"></td>
	      			<td class="titleImg">下载合同选择</td>
	      		</tr>
	    	</table>
		</div>
		<div id="divCont" style="display:''">
    		<table class="common">
        		<tr class="common">
    	  			<td text-align:left colSpan="1">
					 	<span id="spanContGrid" >
					 </span> 
				  </td>
			 	</tr>
			</table>   
  		</div>
  		<div id= "div1" align="center" style= "display: '' ">
			<input value="首  页" class="cssButton" type="button" onclick="turnPage1.firstPage();" /> 
	    	<input value="上一页" class="cssButton" type="button" onclick="turnPage1.previousPage();" /> 					
	    	<input value="下一页" class="cssButton" type="button" onclick="turnPage1.nextPage();" /> 
	    	<input value="尾  页" class="cssButton" type="button" onclick="turnPage1.lastPage();" />  
		</div-->
		<div style="width:200">
	    	<table class="common">
	      		<tr class="common">
	      			<td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divResult);"></td>
	      			<td class="titleImg">分保明细</td>
	      		</tr>
	    	</table>
		</div>
		<div id="divResult" style="display:''">
    		<table class="common">
        		<tr class="common">
    	  			<td text-align:left colSpan="1">
					 	<span id="spanBillDetailGrid" >
					 </span> 
				  </td>
			 	</tr>
			</table>   
  		</div>
  		<div id= "div1" align="center" style= "display: '' ">
			<input value="首  页" class="cssButton" type="button" onclick="turnPage.firstPage();" /> 
	    	<input value="上一页" class="cssButton" type="button" onclick="turnPage.previousPage();" /> 					
	    	<input value="下一页" class="cssButton" type="button" onclick="turnPage.nextPage();" /> 
	    	<input value="尾  页" class="cssButton" type="button" onclick="turnPage.lastPage();" />  
		</div>
		
		<div id="divdetailinfo" style= "display: ''">
			<table class="common">
       		 	<tr class="common">
				   	<td class="title">险种编码</td>
   					<td class="input"><input class="common" ReadOnly name="riskcode1" /></td>
   					<td class="title">再保合同号</td>
   					<td class="input"><input class="common" ReadOnly name="recontcode1" /></td>
	    		 	<td class="title">分保类型</td>
	   				<td class="input"><input class="common" ReadOnly name="tempcessflag1" /></td>
	        	</tr>
	        	<tr class="common">
	        		<td class="title">保单号</td>
	           		<td class="input"><input class="common" ReadOnly name="contno1" /></td>
	        		<td class="title">投保人</td>
	   				<td class="input"><input class="common" ReadOnly name="appntname1" /></td>
	   				<td class="title">被保险人</td>
        			<td class="input"><input class="common" ReadOnly name="insueredname1" /></td>			
	        	</tr>
	        	<tr class="common">
	        		<td class="title">生效日期</tD>
        			<td class="input"><input class="common" ReadOnly name="cvalidate1"/></td>
	        		<td class="title">签单日期</td>
        			<td class="input"><input class="common" ReadOnly name="signdate1" /></td>
   					<td class="title">保额</td>
   					<td class="input"><input class="common" ReadOnly name="amnt1" /></td>
   					</td>
	        	</tr>
	        	<tr class="common">
	        		<td class="title">标准保费</td>
	        		<td class="input"><input class="common" ReadOnly name="standprem1"/></td> 			        		
	        		<td class="title">实收保费</td>
	        		<td class="input"><input class="common" ReadOnly name="prem1"/></td>
	        		<td class="title">分出保额</td>
	        		<td class="input"><input class="common" name="CessionAmount1"/></td>
	        	</tr>
	        	<tr class="common">
	        		<td class="title">分出保费</td>
	        		<td class="input"><input class="common" name="CessPrem1"/></td> 			        		
	        		<td class="title">再保手续费</td>
	        		<td class="input"><input class="common" name="sum1"/></td>
	        	</tr>
            </table>
      	</div>
		<input class="cssButton" value="审核通过" type="hidden" onClick="check()">
  		<input class="cssButton" value="保存" type="hidden" onClick="update()">
  		<input class="cssButton" value="下载清单" type="hidden" onClick="onloadlist()">
  		
  		<input type="hidden" name="OperateType" >
  		<input type="hidden" name="ActuGetNo1" >
  		<input type="hidden" name="ReNewCount1" >
  		<input type="hidden" name="polno1" >
  		<input type="hidden" name="reinsureitem1" >
  		<input type="hidden" name="SQL">
  		<div id="DivFileDownload" style="display:'none'">
     		<a id="fileUrl" href=""></a>
    	</div>
	</form>	
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>