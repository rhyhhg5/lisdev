<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRNewAccountCheckInput.jsp
//程序功能：账单审核
//创建日期：2008-11-11
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LRCessCheckInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRCessCheckInit.jsp"%>
  <title>分保数据校验报表</title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "./LRCessCheckSave.jsp" >
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInfo);"></td>
          <td class="titleImg">请输入查询条件</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divInfo" style= "display: ''">
    <table class=common>
    	    	
     	<TR  class= common>
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	         
	        </TR>
  	</table>   
  </Div>


  
 
  <INPUT class=cssButton VALUE="下载校验数据" TYPE=button onClick="onloadCesslist()">
  <input type="hidden" name="OperateType" >
  <input type=hidden class=Common name=querySql >
  <Div id=DivFileDownload style="display:'none'">
    <A id=fileUrl href=""></A>
  </Div>
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>