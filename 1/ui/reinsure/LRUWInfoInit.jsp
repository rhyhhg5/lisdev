<%
//Creator :张斌
//Date :2006-09-22
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 String pageFlag = request.getParameter("PageFlag"); 
 String reContCode = request.getParameter("ReContCode"); 
 String proposalNos = request.getParameter("ProposalNos"); 
%>
<script language="JavaScript">
function initInpBox()
{
  try
  { 
  	fm.PageFlag.value = "<%=pageFlag%>";
  	fm.ProposalNos.value = "<%=proposalNos%>";
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initGrpUWGrid();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGrpUWGrid()
{
	var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";         			//列宽
	  iArray[0][2]=10;          			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="处理人代码";    	//列名
	  iArray[1][1]="150px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[2]=new Array();
	  iArray[2][0]="处理人姓名";         			//列名
	  iArray[2][1]="100px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="处理完成时间";         			//列名
	  iArray[3][1]="60px";            		//列宽
	  iArray[3][2]=60;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="核保结论";         			//列名
	  iArray[4][1]="60px";            		//列宽
	  iArray[4][2]=60;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="核保人意见";         			//列名
	  iArray[5][1]="100px";            		//列宽
	  iArray[5][2]=1000;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[6]=new Array();
	  iArray[6][0]="GrpcontNo";         			//列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=1000;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
	  GrpUWGrid = new MulLineEnter( "fm" , "GrpUWGrid" ); 
	  GrpUWGrid.mulLineCount = 0;   
	  GrpUWGrid.displayTitle = 1;
	  GrpUWGrid.canSel=1;
	  GrpUWGrid.locked = 1;	
		GrpUWGrid.hiddenPlus = 1;
		GrpUWGrid.hiddenSubtraction = 1;
	  GrpUWGrid.loadMulLine(iArray);  
	  GrpUWGrid.detailInfo="单击显示详细信息";
  
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>