<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ReinsureClaimList.jsp
//程序功能：
//创建日期：2007-03-31
//创建人  ：huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
	System.out.println("startClaim");
  String FlagStr = "";
	String Content = "";
	String FullPath = "";
	
	String StartDate = request.getParameter("StartDate");
	String EndDate = request.getParameter("EndDate");
	String ReRiskSort = request.getParameter("ReRiskSort");
	String RecontCode = request.getParameter("RecontCode");
	String ReRiskCode = request.getParameter("ReRiskCode");
	

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("StartDate", StartDate);
  tTransferData.setNameAndValue("EndDate",EndDate);
  tTransferData.setNameAndValue("ReRiskSort",ReRiskSort);
	tTransferData.setNameAndValue("RecontCode",RecontCode);
	tTransferData.setNameAndValue("ReRiskCode",ReRiskCode);
  
  tVData.addElement(tG);
  tVData.addElement(tTransferData);
  
  String sysPath = application.getRealPath("/")+"/" ;    
  ReinsureClaimListBL tReinsureClaimListBL = new ReinsureClaimListBL();
  if(!tReinsureClaimListBL.submitData(tVData,sysPath))
  {
      FlagStr = "Fail";
      Content = tReinsureClaimListBL.mErrors.getFirstError().toString();                 
  }else{    
			FullPath = tReinsureClaimListBL.getResult();
			Content="导出数据成功";		
	}
	%>
<html>
<script language="javascript">	
	 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 	
	  <%if(Content.equals("导出数据成功")){%>  
          window.open("<%=FullPath%>");
    <%}else{%>	
	  	alert("导出数据失败！");
    <%}%>	
</script>
</html>