<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRProfitInput.jsp
//程序功能：再保盈余佣金计算
//创建日期：2007-04-8 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LRProfitInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRProfitInputInit.jsp"%>
  <title>盈余佣金计算</title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "" >
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divProfitInfo);"></td>
          <td class="titleImg">盈余佣金计算</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divProfitInfo" style= "display: ''">
    <table class=common>
     	<tr class=common>
  			<TD  class= title>
        	再保合同
        </TD>
        <TD  class= input>
          <Input class=codeNo name=ReContCode
          ondblclick="return showCodeList('recontcode',[this,ReContCodeName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('recontcode',[this,ReContCodeName],[0,1]);"><input class=codename name=ReContCodeName >
        </TD>
        <TD  class= title>
        	佣金计算类型
        </TD>
        <TD  class= input>
        	<Input class=codeNo name=CalType CodeData="0|^1|重疾险^2|医疗险^3|建工意外^4|交通工具境内旅游意外医疗" 
          ondblclick="return showCodeListEx('TempCessFlag',[this,CalTypeName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKeyEx('TempCessFlag',[this,CalTypeName],[0,1]);"><input class=codename name=CalTypeName >
        </TD>
      </tr>  
      <tr class=common>
        <TD class= title>
          起始日期
        </TD>
        <TD class= input>
          <Input class=coolDatePicker name="StartDate" dateFormat='short' verify="起始日期|notnull&Date">
        </TD>
        <TD  class= title>
          终止日期
        </TD>
        <TD  class= input>
          <Input class=coolDatePicker name="EndDate" dateFormat='short' verify="终止日期|notnull&Date">
        </TD>
      </tr>
  	</table>   
  </Div>
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divIncomeInfo);"></td>
          <td class="titleImg">收入项</td>
        </tr>
      </table>
  </Div>
  	<Div  id= "divIncomeInfo" style= "display: ''">
    <table class=common>
     	<tr class=common>
  			<TD  class= title>
        	分保费
        </TD>
        <TD  class= input>
          <Input class=common8 name=WrittenPrem readonly>
        </TD>
        <TD  class= title>
        	期初未期满风险准备金
        </TD>
        <TD  class= input>
        	<Input class=common name=IncomingClmReserve >
        </TD>
      </tr>  
      <tr class=common>
        <TD class= title>
          期初已报未决准备金
        </TD>
        <TD class= input>
          <Input class=common name=IncomingReserve1 >
        </TD>
        <TD  class= title>
          期初未报未决准备金
        </TD>
        <TD  class= input>
          <Input class=common name=IncomingReserve2 >
        </TD>
      </tr>
  	</table>   
  </Div>
  <Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOutgoInfo);"></td>
          <td class="titleImg">支出项</td>
        </tr>
      </table>
  </Div>
  	<Div  id= "divOutgoInfo" style= "display: ''">
    <table class=common>
     	<tr class=common>
  			<TD  class= title>
        	再保理赔支出
        </TD>
        <TD  class= input>
          <Input class=common name=ClaimBackFee readonly>
        </TD>
        <TD  class= title>
        	再保佣金支出
        </TD>
        <TD  class= input>
        	<Input class=common name=ReProcFee readonly >
        </TD>
         <TD  class= title>
        	期末未期满风险准备金
        </TD>
        <TD  class= input>
        	<Input class=common name=OutgoingClmReserve >
        </TD>
      </tr>  
      <tr class=common>
        <TD class= title>
          期末已报未决准备金
        </TD>
        <TD class= input>
          <Input class=common name=OutgoingReserve1 >
        </TD>
        <TD  class= title>
          期末未报未决准备金
        </TD>
        <TD  class= input>
          <Input class=common name=OutgoingReserve2 >
        </TD>
        <TD  class= title>
          再保险管理费
        </TD>
        <TD  class= input>
          <Input class=common name=ManageFee readonly >
        </TD>
      </tr>
      <tr class=common>
      	<TD  class= title>
          上年度亏损
        </TD>
        <TD  class= input>
          <Input class=common name=PreYearLoss readonly >
        </TD>
      </tr> 
  	</table>   
  </Div>
  <INPUT class=cssButton id="riskbutton" VALUE="计算" TYPE=button onClick="CalProfit();">
  <Div  id= "divResultInfo" style= "display: 'none'">
    <table class=common>
     	<tr class=common>
  			<TD  class= title>
        	收入小计
        </TD>
        <TD  class= input>
          <Input class=common name=SubTotalIncome readonly>
        </TD>
        <TD  class= title>
        	支出小计
        </TD>
        <TD  class= input>
        	<Input class=common name=SubTotalOutgo readonly >
        </TD>
         <TD  class= title>
        	本年利润
        </TD>
        <TD  class= input>
        	<Input class=common name=UWProfit readonly>
        </TD>
      </tr>  
      <tr class=common>
        <TD class= title>
          本年盈余佣金
        </TD>
        <TD class= input>
          <Input class=common name=UWProfit readonly>
        </TD>
        <TD  class= title>
          年度利润累计
        </TD>
        <TD  class= input>
          <Input class=common name=SumUWProfit readonly>
        </TD>
        <TD  class= title>
          年度盈余佣金累计
        </TD>
        <TD  class= input>
          <Input class=common name=SumUWProfitComm readonly >
        </TD>
      </tr>
  	</table>   
  	<INPUT class=cssButton id="riskbutton" VALUE="保存" TYPE=button onClick="saveProfit();">
  </Div>
  <INPUT class=cssButton id="riskbutton" VALUE="查询" TYPE=button onClick="easyQueryClick();">
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>