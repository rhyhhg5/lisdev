<%
//程序名称：LRNewGetDataBatchSave.jsp
//程序功能：
//创建日期：2008-10-24
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  System.out.println("开始执行Save页面");
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
	
	LRBTReContInterfaceTask tLRBTReContInterfaceTask = new LRBTReContInterfaceTask();

  CErrors tError = null;
  String mOperateType=request.getParameter("OperateType"); 
  System.out.println("操作的类型是"+mOperateType);
  String tRela  		= "";
  String FlagStr 		= "";
  String Content 		= "";
  String mDescType 	= ""; //将操作标志的英文转换成汉字的形式
  
  System.out.println("开始进行获取数据的操作！！！");
  
	String startDate=request.getParameter("StartDate"); 
	String endDate=request.getParameter("EndDate"); 
	String tReContCode=request.getParameter("ReContCode"); 
	System.out.println("startDate="+startDate);
  VData tVData = new VData(); 
  //将团单的公共信息通过TransferData传到UI
  try
  {
  	tVData.addElement(globalInput);
  	TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("StartDate",startDate); 
		tTransferData.setNameAndValue("EndDate",endDate); 
		tTransferData.setNameAndValue("ReContCode",tReContCode); 
  	tVData.addElement(tTransferData);
  	tLRBTReContInterfaceTask.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  
	if (FlagStr=="")
	{
		tError = tLRBTReContInterfaceTask.mErrors;
	  
	  if (!tError.needDealError())
	  {
	    Content = mDescType+"完成";
	  	FlagStr = "Succ";
	  }
	  else
	  {
	  	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
	  	FlagStr = "Fail";
	  }
	}
	
	 
%>

<html>
	<script language="javascript">
			parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>