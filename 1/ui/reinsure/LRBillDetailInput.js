var showInfo;
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass();


//再保合同号RecontCode
//投保人AppntName
//被保险人InsuredName
//年份Year
//月份Month
//分保类型TempCessFlag
//险种类别DiskKind
//分保方式RiskCalSort
//险种编码RiskCode
//保单号ContNo
//再保公司ReinsureCom

function easyQueryClick(){
	var day = showMonthLastDay();  //获取提取月份的最后一天
	
	var strWhere="";
	var strWhereT="";
	
	if(fm.ReinsureCom.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='"+fm.ReinsureCom.value+"') ";
 		strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='"+fm.ReinsureCom.value+"') ";
 	}
 	
 	if(fm.DiskKind.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
 		strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
 	}
 	if(fm.RiskCalSort.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and cessionmode='"+fm.RiskCalSort.value+"') ";
 		strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and cessionmode='"+fm.RiskCalSort.value+"') ";
 	}
	if(fm.Year.value!=""&&fm.Month.value!=""){
	    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
	    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
	    
	    strWhere+=" and a.GetDataDate between '"+StartDate+"' and '"+EndDate+"' ";
 		strWhereT +=" and a.GetDataDate between '"+StartDate+"' and '"+EndDate+"' ";
	}
	
	var strSQL="select a.riskcode,a.ReContCode, "
		+" a.TempCessFlag , "
		+"a.ContNo , "
		+"a.AppntName,a.InsuredName,a.CValiDate,a.signdate,a.Amnt,a.StandPrem,a.prem, "   
		+"a.CessionAmount,a.CessPrem,a.ReProcFee,'',a.ReNewCount,a.polno,a.reinsureitem "
		+"from lrcesslist a "
		+" where  grpcontno='00000000000000000000' "
 		+ strWhere
 		+ getWherePart("a.Contno","ContNo")
 		+ getWherePart("a.RecontCode","RecontCode")
 		+ getWherePart("a.riskcode","RiskCode")
 		//+ getWherePart("a.ReinsureCom","ReinsureCom")
 		+ getWherePart("a.TempCessFlag","TempCessFlagName")
 		
 		+ getWherePart("a.RiskCode","RiskCode")
		+ getWherePart("a.AppntName","AppntName","like")
 		+ getWherePart("a.InsuredName","InsuredName",'like');
	strSQL+=" union all select a.riskcode,a.ReContCode, "
		+" a.TempCessFlag , "
		+"a.GrpContNo , "
		+"a.AppntName,a.InsuredName,a.CValiDate,a.signdate,a.Amnt,a.StandPrem,a.prem, "   
		+"a.CessionAmount,a.CessPrem,a.ReProcFee,'',a.ReNewCount,a.polno,a.reinsureitem "
		+"from lrcesslist a "
		+" where  grpcontno<>'00000000000000000000' "
 		+ strWhere
 		+ getWherePart("a.GrpContno","ContNo")
 		+ getWherePart("a.RecontCode","RecontCode")
 		+ getWherePart("a.riskcode","RiskCode")
 		//+ getWherePart("a.ReinsureCom","ReinsureCom")
 		+ getWherePart("a.TempCessFlag","TempCessFlagName")
 		
 		+ getWherePart("a.RiskCode","RiskCode")
		+ getWherePart("a.AppntName","AppntName","like")
 		+ getWherePart("a.InsuredName","InsuredName",'like');
 		
	
 	
 	strSQL +=" with ur";
 	turnPage.queryModal(strSQL, BillDetailGrid);	
}

function easyQueryContClick(){
	
	var strSQL="select recontcode,recontname from lrcontinfo where recontstate='01' " +
			" union select tempcontcode,tempcontname from LRTempCessCont where recontstate='01'  with ur"
	turnPage1.queryModal(strSQL, ContGrid);
}

function onloadlist(){
 	try 
	{
		var i = 0;
		var showStr="正在获取数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.OperateType.value="ONLOAD";
		fm.submit();
  	} catch(ex) {
	  	showInfo.close( );
	  	alert(ex);
  	}
}

function downAfterSubmit(cfilePath) {
	showInfo.close();
	fileUrl.href = cfilePath; 
    fileUrl.click();
}

function showMonthLastDay(){
	var tmpDate=new Date(fm.Year.value,fm.Month.value,1); 
	var MonthLastDay=new Date(tmpDate-86400000);
	return MonthLastDay.getDate();      
}

function getMulLineSeData(){
	var num = BillDetailGrid.getSelNo();
	if(num <= 0){
		reSetData();
		alert("请选择数据！");
		return false ;
	}
	fm.riskcode1.value = BillDetailGrid.getRowColDataByName(num-1,"riskcode");	
	fm.recontcode1.value = BillDetailGrid.getRowColDataByName(num-1,"recontcode");
	fm.tempcessflag1.value = BillDetailGrid.getRowColDataByName(num-1,"tempcessflag");
	fm.contno1.value = BillDetailGrid.getRowColDataByName(num-1,"contno");
	fm.appntname1.value = BillDetailGrid.getRowColDataByName(num-1,"appntname");
	fm.insueredname1.value = BillDetailGrid.getRowColDataByName(num-1,"insueredname");
	fm.cvalidate1.value = BillDetailGrid.getRowColDataByName(num-1,"cvalidate");
	fm.signdate1.value = BillDetailGrid.getRowColDataByName(num-1,"signdate");
	fm.amnt1.value = BillDetailGrid.getRowColDataByName(num-1,"amnt");
	fm.standprem1.value = BillDetailGrid.getRowColDataByName(num-1,"standprem");
	fm.prem1.value = BillDetailGrid.getRowColDataByName(num-1,"prem");
	fm.CessionAmount1.value = BillDetailGrid.getRowColDataByName(num-1,"CessionAmount");
	fm.CessPrem1.value = BillDetailGrid.getRowColDataByName(num-1,"CessPrem");
	fm.sum1.value = BillDetailGrid.getRowColDataByName(num-1,"sum");
	fm.ActuGetNo1.value =BillDetailGrid.getRowColDataByName(num-1,"ActuGetNo");
	fm.ReNewCount1.value =BillDetailGrid.getRowColDataByName(num-1,"ReNewCount");
	fm.polno1.value =BillDetailGrid.getRowColDataByName(num-1,"polno");
	fm.reinsureitem1.value =BillDetailGrid.getRowColDataByName(num-1,"reinsureitem");
}

function reSetData(){
	fm.recontcode1.value = "";
	fm.tempcessflag1.value = "";
	fm.contno1.value = "";
	fm.appntname1.value = "";
	fm.insueredname1.value = "";
	fm.cvalidate1.value = "";
	fm.signdate1.value = "";
	fm.amnt1.value = "";
	fm.standprem1.value = "";
	fm.prem1.value = "";
	fm.CessionAmount1.value = "";
	fm.CessPrem1.value = "";
	fm.sum1.value = "";
	fm.ReNewCount1.value ="";
	fm.polno1.value ="";
	fm.reinsureitem1.value ="";
}

function check(){
	var day = showMonthLastDay();  //获取提取月份的最后一天
	
	var strWhere="";
	var strWhereT="";
	
	if(fm.ReinsureCom.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='"+fm.ReinsureCom.value+"') ";
 		strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='"+fm.ReinsureCom.value+"') ";
 	}
 	
 	if(fm.DiskKind.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
 		strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
 	}
	
	if(fm.Year.value!=""&&fm.Month.value!=""){
	    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
	    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
	    
	    strWhere+=" and a.GetDataDate between '"+StartDate+"' and '"+EndDate+"' ";
 		strWhereT +=" and a.GetDataDate between '"+StartDate+"' and '"+EndDate+"' ";
	}
	
	var strSQL="select a.riskcode,a.ReContCode, "
		+"(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end) riskcalsort, "
		+"a.ContNo contno, "
		+"a.AppntName,a.InsuredName,a.CValiDate,a.signdate,a.Amnt,a.StandPrem,a.prem, "   
		+"c.CessionAmount,c.CessPrem,c.ReProcFee+c.ChoiRebaFee+c.SpeCemm sum ,a.ActuGetNo,a.ReNewCount,a.polno,a.reinsureitem "
		+"from LRPol a,LRPolResult c "
		+"where (a.polno=c.polno or a.masterpolno=c.polno) and "
		+"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount and a.reinsureitem=c.reinsureitem and a.ReNewCount = c.ReNewCount "
 		+ strWhere
 		+ getWherePart("a.Contno","ContNo")
 		+ getWherePart("a.RecontCode","RecontCode")
 		+ getWherePart("a.riskcode","RiskCode")
 		//+ getWherePart("a.ReinsureCom","ReinsureCom")
 		+ getWherePart("a.TempCessFlag","TempCessFlag")
 		+ getWherePart("a.RiskCalSort","RiskCalSort")
 		//+ getWherePart("a.DiskKind","DiskKind")
 		+ getWherePart("a.RiskCode","RiskCode")
		+ getWherePart("a.AppntName","AppntName","like")
 		+ getWherePart("a.InsuredName","InsuredName",'like');
	strSQL+=" union select a.riskcode,a.ReContCode, "
		+"(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end) riskcalsort, "
		+"a.ContNo contno, "
		+"a.AppntName,a.InsuredName,a.CValiDate,a.signdate,a.Amnt,a.StandPrem,a.prem, "   
		+"c.CessionAmount,d.edorbackfee,d.edorprocfee,a.ActuGetNo,a.ReNewCount,a.polno,a.reinsureitem "
		+"from LRPol a,LRPolResult c,lrpoledor b,lrpoledorresult d "
		+"where (a.polno=c.polno or a.masterpolno=c.polno) and "
		+"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount and a.reinsureitem=c.reinsureitem and a.ReNewCount = c.ReNewCount " 
		+" and a.polno=b.polno and a.polno=d.polno and a.recontcode=b.recontcode and a.recontcode=d.recontcode" 
		+" and b.edorno=d.edorno and a.reinsureitem=b.reinsureitem and a.reinsureitem=d.reinsureitem" 
		+" and a.renewcount=b.renewcount and c.renewcount=b.renewcount and a.renewcount=d.renewcount and c.renewcount=d.renewcount "
 		+ strWhere
 		+ getWherePart("a.Contno","ContNo")
 		+ getWherePart("a.RecontCode","RecontCode")
 		+ getWherePart("a.riskcode","RiskCode")
 		//+ getWherePart("a.ReinsureCom","ReinsureCom")
 		+ getWherePart("a.TempCessFlag","TempCessFlag")
 		+ getWherePart("a.RiskCalSort","RiskCalSort")
 		//+ getWherePart("a.DiskKind","DiskKind")
 		+ getWherePart("a.RiskCode","RiskCode")
		+ getWherePart("a.AppntName","AppntName","like")
 		+ getWherePart("a.InsuredName","InsuredName",'like');
 		
	if(fm.ReinsureCom.value!=''||fm.DiskKind.value!=''){		
 		strSQL+= " union "
 				+"select a.riskcode,a.ReContCode, "  //增加临分的查询SQL
				+"(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end)  riskcalsort, "
				+"a.ContNo contno, "
				+"a.AppntName,a.InsuredName,a.CValiDate,a.signdate,a.Amnt,a.StandPrem,a.prem, "   
				+"c.CessionAmount,c.CessPrem,c.ReProcFee+c.ChoiRebaFee+c.SpeCemm sum,a.ActuGetNo,c.ReNewCount,c.polno,c.reinsureitem "
 				+"from LRPol a,LRPolResult c "
				+"where (a.polno=c.polno or a.masterpolno=c.polno) and "
 				+"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount and a.reinsureitem=c.reinsureitem and a.ReNewCount = c.ReNewCount  " 
 				+" and a.recontcode in (select recontcode from lrcontinfo where recomcode='"+fm.ReinsureCom.value+"')" 
 				+" and exists (select 1 from lmriskapp where riskcode=a.riskcode and riskperiod='"+fm.DiskKind.value+"')"  
 				+ strWhereT
 				+ getWherePart("a.Contno","ContNo")
		 		+ getWherePart("a.RecontCode","RecontCode")
		 		+ getWherePart("a.riskcode","RiskCode")
		 		//+ getWherePart("a.ReinsureCom","ReinsureCom")
		 		+ getWherePart("a.TempCessFlag","TempCessFlag")
		 		+ getWherePart("a.RiskCalSort","RiskCalSort")
		 		//+ getWherePart("a.DiskKind","DiskKind")
		 		+ getWherePart("a.RiskCode","RiskCode")
				+ getWherePart("a.AppntName","AppntName","like")
		 		+ getWherePart("a.InsuredName","InsuredName",'like');
 		strSQL+=" union select a.riskcode,a.ReContCode, "
 			+"(case a.TempCessFlag when 'N' then '合同分保' else '临时分保' end) riskcalsort, "
 			+"a.ContNo contno, "
 			+"a.AppntName,a.InsuredName,a.CValiDate,a.signdate,a.Amnt,a.StandPrem,a.prem, "   
 			+"c.CessionAmount,d.edorbackfee,d.edorprocfee,a.ActuGetNo,a.ReNewCount,a.polno,a.reinsureitem "
 			+"from LRPol a,LRPolResult c,lrpoledor b,lrpoledorresult d "
 			+"where a.polno=c.polno and "
 			+"a.RecontCode=c.RecontCode and a.ReNewCount=c.ReNewCount and a.reinsureitem=c.reinsureitem and a.ReNewCount = c.ReNewCount " 
 			+" and (a.polno=c.polno or a.masterpolno=c.polno) and a.polno=d.polno and a.recontcode=b.recontcode and a.recontcode=d.recontcode" 
 			+" and b.edorno=d.edorno and a.reinsureitem=b.reinsureitem and a.reinsureitem=d.reinsureitem"
 			+" and a.renewcount=b.renewcount and c.renewcount=b.renewcount and a.renewcount=d.renewcount and c.renewcount=d.renewcount "
 			+ strWhereT
 	 		+ getWherePart("a.Contno","ContNo")
 	 		+ getWherePart("a.RecontCode","RecontCode")
 	 		+ getWherePart("a.riskcode","RiskCode")
 	 		//+ getWherePart("a.ReinsureCom","ReinsureCom")
 	 		+ getWherePart("a.TempCessFlag","TempCessFlag")
 	 		+ getWherePart("a.RiskCalSort","RiskCalSort")
 	 		//+ getWherePart("a.DiskKind","DiskKind")
 	 		+ getWherePart("a.RiskCode","RiskCode")
 			+ getWherePart("a.AppntName","AppntName","like")
 	 		+ getWherePart("a.InsuredName","InsuredName",'like');
 	}
 	
 	strSQL +=" with ur";
	
	if(confirm('是否确定对查询出未审核的清单进行审核')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="CHECK";
				fm.SQL.value = strSQL ;
				fm.submit();
	 		} catch(ex) {
	  		showInfo.close( );
	  		alert(ex);
	 	}
	}
}

function vCheck(){
	var strDate ;
	if(fm.Year.value!=""&&fm.Month.value!=""){
	    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
	    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
	    
	    strDate=" and startdate= "+StartDate+" and enddata= "+EndDate;
	}
 	var day = showMonthLastDay();  //获取提取月份的最后一天
    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
	var strWhere="";
	if(fm.RecontCode.value !=''){
		strWhere=" and recontcode='"+fm.RecontCode.value+"'";
	}
	var sqla = "select payflag from LRAccounts where 1=1 " ;
	if (strDate!="" && strDate!=null){
		sqla += strDate ;
	}
 	sqla = "select payflag from LRAccounts where 1=1 "
                +strWhere+" with ur";
 	var strResult = easyExecSql(sqla);
 	if(strResult!=null){
 		for(var i=0;i<strResult.length;i++){
 			var flag = strResult[i][1];
 			if(flag=="03"){
 			  alert('存在结算的账单,您不能进行清单审核,请重新录入查询条件!');
 			  return false;
 			}
 		}
 	}
}

function update(){
	if(updateCheck()==false){
		return false;
	}
	if(confirm('是否确定对该条信息的分保信息进行修改')==true){
		try{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="UPDATE";
				fm.submit();
  		} catch(ex) {
	  		showInfo.close( );
	  		alert(ex);
  		}
  	}
}


function updateCheck(){
	var rowCount=BillDetailGrid.getSelNo();
	
	if(rowCount<=0){
		alert('请选择一条信息');
		return false;
	}else{
		var ActuGetNo = BillDetailGrid.getRowColDataByName(rowCount-1,"ActuGetNo");
		if(ActuGetNo=="03"){  //状态问题，结算完成的账单不能做修改
		   	alert('账单已结算，分保数据不能修改！！！');
		  	return false;	
		}
	}	
	
	if(checkNum(fm.sum1)==false){
		return false;
	}
	
	if(checkNum(fm.CessPrem1)==false){
		return false;
	}
	
	if(checkNum(fm.CessionAmount1)==false){
		return false;
	}
}

function checkNum(control){
   var a = control.value.match(/^(-?\d+)(\.\d+)?$/); 
   if (a == null) {
      	alert('请输入数字！');
		control.select();
		control.focus();
		return false;
   } 
}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}