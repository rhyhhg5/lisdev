var showInfo;

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	return true;
}

function easyQueryClick()
{
	if(fm.ModifyFlag.value=="1") //修改临分协议
	{
		var strSQL = "select Prtno,GrpName,'',Prem,ManageCom,ProposalGrpContNo from lcgrpcont a "
		+ " where exists (select 1 from lcpol where ReinsureFlag in ('3','4','5') and GrpContNo = a.GrpContNo ) " 
		+ getWherePart("PrtNo","PrtNo")
		+ getWherePart("GrpName","GrpName","like")
		+ getWherePart("ManageCom","ManageCom","like")
		+ getWherePart("AppFlag","State")
		+ " with ur "
		;
	}else //录入临分协议
	{
		var strSQL = "select Prtno,GrpName,'',Prem,ManageCom,ProposalGrpContNo from lcgrpcont a "
		+ " where exists (select 1 from lcpol where ReinsureFlag ='2' and GrpContNo = a.GrpContNo) " 
		+ getWherePart("PrtNo","PrtNo")
		+ getWherePart("GrpName","GrpName","like")
		+ getWherePart("ManageCom","ManageCom","like")
		+ getWherePart("AppFlag","State")
		+ " with ur "
		;
	}
	turnPage.queryModal(strSQL, PolGrid); 
}

function cessConclusion() //临分结论
{
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert("请先选择保单！");
		return false;
	}
  var cPrtNo 	= PolGrid.getRowColData(tSel-1, 1);
  var cAppntName = PolGrid.getRowColData(tSel-1, 3); 
  var cManageCom = PolGrid.getRowColData(tSel-1, 5); 
	var cContNo = PolGrid.getRowColData(tSel-1, 6);
  window.open("./FramGrpCessConclusion.jsp?LoadFlag=6&PrtNo="+cPrtNo+"&ContNo="+cContNo+"&AppntName="+cAppntName+"&ManageCom="+cManageCom+"&ModifyFlag="+fm.ModifyFlag.value,"GrpCessConlusion");
}

/*********************************************************************
 *  进入团体合同
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGrpCont()
{
	var tSel=PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert("请先选择保单！");
		return false;
	}
	
	var PrtNo=PolGrid.getRowColData(tSel-1,1);
  var cGrpContNo=PolGrid.getRowColData(tSel-1,6);
  
  //cGrpContNo=arr[0][0];
  //alert(arr[0][0]);
  //var newWindow=window.open("../app/GroupPolApproveInfo.jsp?LoadFlag=16&polNo="+cGrpContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
  window.open("../app/GroupPolApproveInfo.jsp?ScanFlag=1&scantype=scan&prtNo="+PrtNo+"&scantype=scan&prtNo="+PrtNo+"&Resource="+"null"+"&LoadFlag=16&polNo="+cGrpContNo,"window1");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else {
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  resetForm();
  }
}

function updateClick()
{
	fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	return true;
	
}

function deleteClick()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该批次吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	
	return true;
	
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
  	fm.reset();
  	PolGrid.clearData();
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

