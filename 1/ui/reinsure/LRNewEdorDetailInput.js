var showInfo;

var turnPage = new turnPageClass(); 

function easyQueryClick()
{
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
 	var day = showMonthLastDay();  //获取提取月份的最后一天
    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
	var strWhere="";
	var strWhereT="";
 	if(fm.ReComCode.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='"+fm.ReComCode.value+"') ";
 		strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='"+fm.ReComCode.value+"') ";
 	}
 	if(fm.DiskKind.value!=''){
 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
 		strWhere+=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
 	}
 	if(fm.Year.value!=''){
 		strWhere+=" and b.getdatadate between '"+StartDate+"' and '"+EndDate+"'";
 		strWhereT+=" and b.getdatadate between '"+StartDate+"' and '"+EndDate+"'";
 	}
  
  var strSQL="select a.riskcode,a.ReContCode,"
    +"(select case CessionMode when '1' then '成数' when '2' then '溢额' end "
    +" from lrcontinfo where recontcode = a.recontcode) CessionMode, "
    +" a.ContNo contno, a.AppntName, a.InsuredName, a.CValiDate, a.signdate,"
    +"(case b.FeeoperationType when 'BF' then '保障调整' "
    +"when 'CM' then '客户资料变更' when 'CT' then '退保' when 'FX' then '个单复效' "
    +"when 'LQ' then '领取' when 'LR' then '保单遗失补发' when 'MJ' then '满期' "
    +"when 'NI' then '增人' when 'NS' then '新增险种' when 'TB' then '投保事项变更' "
    +"when 'WJ' then '无名单减人' when 'WT' then '犹豫期退保' when 'WZ' then '无名单增人' "
    +"when 'XT' then '协议退保' when 'ZB' then '追加保费' when 'ZT' then '减人' else b.FeeoperationType end) FeeoperationType,"
    +" b.EdorValidate,a.Amnt,a.StandPrem,c.CessionAmount,c.CessPrem,"
    +"c.ReProcFee + c.ChoiRebaFee + c.SpeCemm sum,a.ReNewCount,a.polno,b.actugetno "
    +"from LRPol a, LRPolEdor b, LRPolResult c, LRPolEdorResult e "
    +" where a.polno = b.polno and b.polno = c.polno and c.polno = e.polno "
    +"and a.RecontCode = b.RecontCode and b.RecontCode = c.RecontCode "
    +"and c.RecontCode = e.RecontCode and a.ReNewCount = c.ReNewCount "
    +"and b.ReNewCount = e.ReNewCount  "
    + strWhere
    + getWherePart("a.riskcode","RiskCode")
    + getWherePart("a.Contno","ContNo")
    + getWherePart("a.RecontCode","RecontCode")
 	+ getWherePart("a.ActuGetState","ActuGetState")
 	+ getWherePart("a.RiskCalSort","CessionMode")
    + getWherePart("a.AppntName","AppntName","like")
    + getWherePart("a.InsuredName","InsuredName",'like');
  if(fm.ReComCode.value!=''||fm.DiskKind.value!=''){	
	 strSQL +=" union all select a.riskcode,a.ReContCode,"
    +"(select case CessionMode when '1' then '成数' when '2' then '溢额' end "
    +" from lrcontinfo where recontcode = a.recontcode) CessionMode, "
    +" a.ContNo contno, a.AppntName, a.InsuredName, a.CValiDate, a.signdate,"
    +"(case b.FeeoperationType when 'BF' then '保障调整' "
    +"when 'CM' then '客户资料变更' when 'CT' then '退保' when 'FX' then '个单复效' "
    +"when 'LQ' then '领取' when 'LR' then '保单遗失补发' when 'MJ' then '满期' "
    +"when 'NI' then '增人' when 'NS' then '新增险种' when 'TB' then '投保事项变更' "
    +"when 'WJ' then '无名单减人' when 'WT' then '犹豫期退保' when 'WZ' then '无名单增人' "
    +"when 'XT' then '协议退保' when 'ZB' then '追加保费' when 'ZT' then '减人' else b.FeeoperationType end) FeeoperationType,"
    +" b.EdorValidate,a.Amnt,a.StandPrem,c.CessionAmount,c.CessPrem,"
    +"c.ReProcFee + c.ChoiRebaFee + c.SpeCemm sum,a.ReNewCount,a.polno,b.actugetno "
    +"from LRPol a, LRPolEdor b, LRPolResult c, LRPolEdorResult e "
    +" where a.polno = b.polno and b.polno = c.polno and c.polno = e.polno "
    +"and a.RecontCode = b.RecontCode and b.RecontCode = c.RecontCode "
    +"and c.RecontCode = e.RecontCode and a.ReNewCount = c.ReNewCount "
    +"and b.ReNewCount = e.ReNewCount  "
    + strWhereT
    + getWherePart("a.riskcode","RiskCode")
    + getWherePart("a.Contno","ContNo")
    + getWherePart("a.RecontCode","RecontCode")
    + getWherePart("a.RiskCalSort","CessionMode")
 	+ getWherePart("a.ActuGetState","ActuGetState")
    + getWherePart("a.AppntName","AppntName","like")
    + getWherePart("a.InsuredName","InsuredName",'like');
  }    
    strSQL +=" order by ReContCode,FeeoperationType,EdorValidate desc with ur";
 
	turnPage.queryModal(strSQL, NewEdorDetailGrid); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

function updateCheck(){
	var rowCount=0;
	for(var i=0;i<NewEdorDetailGrid.mulLineCount;i++){
		if(NewEdorDetailGrid.getChkNo(i)==true){
			if(rowCount>1){
				break;
			}
			rowCount++;
		}
	}
	if(rowCount==0){
		alert('请选择一条信息');
		return false;
	}
	if(rowCount>1){
		alert('只能对一条信息修改');
		NewEdorDetailGrid.checkBoxAllNot ()
		return false;
	}
	if(checkNum(fm.CessionAmount)==false){
		return false;
	}
	if(checkNum(fm.CessPrem)==false){
		return false;
	}
	if(checkNum(fm.ReProcFee)==false){
		return false;
	}
	if(checkNum(fm.ChoiRebaFee)==false){
		return false;
	}
	if(checkNum(fm.SpeCemm)==false){
		return false;
	}
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
}

function checkNum(control){
   var a = control.value.match(/^(-?\d+)(\.\d+)?$/); 
   if (a == null) {
      alert('数值不符合');
			control.select();
			control.focus();
			return false;
   } 
}


function update(){
	if(updateCheck()==false){
		return false;
	}
	if(confirm('是否确定对该条信息的分保信息进行修改')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="UPDATE";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}

function check(){
	if(vCheck()==false){
		return false;
	}
	if(confirm('是否确定对选中的信息做审核通过操作')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="CHECK";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}

function vCheck(){	
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
 	var day = showMonthLastDay();  //获取提取月份的最后一天
    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
	var strWhere="";
	if(fm.RecontCode.value !=''){
		strWhere=" and recontcode='"+fm.RecontCode.value+"'";
	}
 	var sqla = "select payflag from LRAccounts where startdate='"+StartDate+"' and enddata='"+EndDate+"'" 
                +strWhere+" with ur";
 	var strResult = easyExecSql(sqla);
 	if(strResult!=null){
 		for(var i=0;i<strResult.length;i++){
 			var flag = strResult[i][1];
 			if(flag=="03"){
 			  alert('存在结算的账单,您不能进行清单审核,请重新录入查询条件 !');
 			  return false;
 			}
 		}
 	} 	
}

function onloadlist(){
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
 	try 
	{
		var i = 0;
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.OperateType.value="ONLOAD";
		fm.submit();
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}


function downAfterSubmit(cfilePath) {
	showInfo.close();
	fileUrl.href = cfilePath; 
    fileUrl.click();
}
function showMonthLastDay()
 {
     var tmpDate=new Date(fm.Year.value,fm.Month.value,1); 
     var MonthLastDay=new Date(tmpDate-86400000);
     return MonthLastDay.getDate();      
 }

        

