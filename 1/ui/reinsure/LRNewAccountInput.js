var showInfo;

var turnPage = new turnPageClass(); 

function easyQueryClick()
{
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
	var day = showMonthLastDay();  //获取提取月份的最后一天
    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
    var EndDate = fm.Year.value+"-"+fm.Month.value+"-"+day;
	var strWhere="";
	var strWhereT="";
 	if(fm.ReComCode.value!=''){
 		strWhere+=" and b.ReComCode='"+fm.ReComCode.value+"' ";
 		strWhereT += " and b.ComCode='"+fm.ReComCode.value+"' ";
 	}
 	if(fm.CessionMode.value!=''){
 		strWhere+=" and b.CessionMode='"+fm.CessionMode.value+"' ";
 		strWhereT += " and b.CessionMode='"+fm.CessionMode.value+"' ";
 	}
 	if(fm.DiskKind.value!=''){
 		strWhere+=" and b.DiskKind='"+fm.DiskKind.value+"' ";
 		strWhereT += " and b.DiskKind='"+fm.DiskKind.value+"' ";
 	}
 	if(StartDate!=''){
 		strWhere+=" and a.StartDate ='"+StartDate+"' ";
 		strWhereT+=" and a.StartDate ='"+StartDate+"' ";
 	}
 	if(EndDate!=''){
 		strWhere+=" and a.EndData = '"+EndDate+"' ";
 		strWhereT +=" and a.EndData = '"+EndDate+"' ";
 	}
 	if(fm.RecontCode.value!=''){
 		strWhere+=" and a.RecontCode='"+fm.RecontCode.value+"' ";
 		strWhereT+=" and a.RecontCode='"+fm.RecontCode.value+"' ";
 	}
 	if(fm.RiskCode.value != ''){
 		strWhere+=" and a.RiskCode='"+fm.RiskCode.value+"' ";
 		strWhereT +=" and a.RiskCode='"+fm.RiskCode.value+"' ";
 	}
 	var strSQL="";
 	if(fm.TempCessFlag.value=="Y"){
 		strSQL = "select b.ComCode,'临时分保',b.DiskKind," +
"(case b.CessionMode when '1' then '成数' when '2' then '风险溢额' end) , a.riskcode, " +
"year(a.StartDate),month(a.StartDate)," +
"sum(a.CessPrem)+sum(EdorBackFee),sum(ReProcFee)+sum(a.EdorProcFee),sum(a.claimbackfee)," +
"sum(a.CessPrem)+sum(EdorBackFee)-sum(a.claimbackfee),(select b.paydate from lraccounts b where a.StandbyFlag1=b.ActuGetNo)," +
"(select b.Operator from lraccounts b where a.StandbyFlag1=b.ActuGetNo)," +
"(select b.PayFlag from lraccounts b where a.StandbyFlag1=b.ActuGetNo),a.StandbyFlag1,b.tempcontcode "+
	"from LRComRiskResult a,LRTempCessCont b where a.recontcode=b.TempContCode "+
	" and exists (select 1 from lraccounts b where a.StandbyFlag1=b.ActuGetNo and b.PayFlag in ('02','03') ) "+   //只查询清单审核完成的账单
	strWhereT+
	" group by a.riskcode,b.ComCode,a.StandbyFlag1,a.StandbyFlag2,b.DiskKind," +
" b.CessionMode,a.StartDate,b.tempcontcode "+
" with ur";
	 	}
	 else if(fm.TempCessFlag.value=="N"){
		 strSQL = "select b.recomcode,'合同分保',b.DiskKind," +
			"(case b.CessionMode when '1' then '成数' when '2' then '风险溢额' end) , a.riskcode, " +
			"year(a.StartDate),month(a.StartDate)," +
			"sum(a.CessPrem)+sum(EdorBackFee),sum(ReProcFee)+sum(a.EdorProcFee),sum(a.claimbackfee)," +
			"sum(a.CessPrem)+sum(EdorBackFee)-sum(a.claimbackfee),(select b.paydate from lraccounts b where a.StandbyFlag1=b.ActuGetNo)," +
"(select b.Operator from lraccounts b where a.StandbyFlag1=b.ActuGetNo)," +
			"(select b.PayFlag from lraccounts b where a.StandbyFlag1=b.ActuGetNo),a.StandbyFlag1,b.recontcode "+
	"from LRComRiskResult a,lRContInfo b where a.recontcode=b.recontcode "+
	" and exists (select 1 from lraccounts b where a.StandbyFlag1=b.ActuGetNo and b.PayFlag in ('02','03') ) "+   //只查询清单审核完成的账单
	strWhere+
 " group by a.riskcode, b.recomcode,a.StandbyFlag1,a.StandbyFlag2,b.DiskKind," +
 " b.CessionMode,a.StartDate,b.recontcode "+
  " with ur";
	 	}else{
 	 strSQL = "select b.recomcode,'合同分保',b.DiskKind," +
 			"(case b.CessionMode when '1' then '成数' when '2' then '风险溢额' end) , a.riskcode, " +
 			"year(a.StartDate),month(a.StartDate)," +
 			"sum(a.CessPrem)+sum(EdorBackFee),sum(ReProcFee)+sum(a.EdorProcFee),sum(a.claimbackfee)," +
 			"sum(a.CessPrem)+sum(EdorBackFee)-sum(a.claimbackfee),(select b.paydate from lraccounts b where a.StandbyFlag1=b.ActuGetNo)," +
"(select b.Operator from lraccounts b where a.StandbyFlag1=b.ActuGetNo)," +
 			"(select b.PayFlag from lraccounts b where a.StandbyFlag1=b.ActuGetNo),a.StandbyFlag1,b.recontcode "+
 	"from LRComRiskResult a,lRContInfo b where a.recontcode=b.recontcode "+
 	" and exists (select 1 from lraccounts b where a.StandbyFlag1=b.ActuGetNo and b.PayFlag in ('02','03') ) "+   //只查询清单审核完成的账单
 	strWhere+
    " group by a.riskcode, b.recomcode,a.StandbyFlag1,a.StandbyFlag2,b.DiskKind," +
    " b.CessionMode,a.StartDate,b.recontcode "+
    " union all "+
    "select b.ComCode,'临时分保',b.DiskKind," +
    "(case b.CessionMode when '1' then '成数' when '2' then '风险溢额' end) , a.riskcode, " +
    "year(a.StartDate),month(a.StartDate)," +
    "sum(a.CessPrem)+sum(EdorBackFee),sum(ReProcFee)+sum(a.EdorProcFee),sum(a.claimbackfee)," +
    "sum(a.CessPrem)+sum(EdorBackFee)-sum(a.claimbackfee),(select b.paydate from lraccounts b where a.StandbyFlag1=b.ActuGetNo)," +
"(select b.Operator from lraccounts b where a.StandbyFlag1=b.ActuGetNo)," +
    "(select b.PayFlag from lraccounts b where a.StandbyFlag1=b.ActuGetNo),a.StandbyFlag1,b.tempcontcode "+
 	"from LRComRiskResult a,LRTempCessCont b where a.recontcode=b.TempContCode "+
 	" and exists (select 1 from lraccounts b where a.StandbyFlag1=b.ActuGetNo and b.PayFlag in ('02','03') ) "+   //只查询清单审核完成的账单
 	strWhereT+
 	" group by a.riskcode, b.ComCode,a.StandbyFlag1,a.StandbyFlag2,b.DiskKind," +
    " b.CessionMode,a.StartDate,b.tempcontcode "+
    " with ur";
	 	}
	  turnPage.queryModal(strSQL, NewAccountGrid); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}


function checkNum(control){
   var a = control.value.match(/^(-?\d+)(\.\d+)?$/); 
   if (a == null) {
      alert('数值不符合');
			control.select();
			control.focus();
			return false;
   } 
}


function check(){
	if(vCheck()==false){
		return false;
	}
	if(confirm('是否确定对选中的信息做审核通过操作')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="JIESUAN";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}

function vCheck(){
	var rowCount=0;
	for(var i=0;i<NewAccountGrid.mulLineCount;i++){
		if(NewAccountGrid.getChkNo(i)==true){
			if(rowCount>1){
				break;
			}
			rowCount++;
		}
	}
	if(rowCount==0){
		alert('至少选择一条信息');
		return false;
	}
}

function onloadlist(){
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
 	try 
	{
		var i = 0;
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.OperateType.value="ONLOAD";
		fm.submit();
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function check2(){
	if(vCheck()==false){
		return false;
	}
	try {
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		  	fm.action = "./LRNewAccountPrintSave.jsp";
			fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close();
  		alert(ex);
  	}
}

function downAfterSubmit(cfilePath) {
	showInfo.close();
	fileUrl.href = cfilePath; 
  fileUrl.click();
}
function showMonthLastDay()
{
     var tmpDate=new Date(fm.Year.value,fm.Month.value,1); 
     var MonthLastDay=new Date(tmpDate-86400000);
     return MonthLastDay.getDate();      
}

        

