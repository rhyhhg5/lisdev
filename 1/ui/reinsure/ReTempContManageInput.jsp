<html>
<%
//name :ReTempContManageInput.jsp
//function :ReTempContManageInput
//Creator :liuli
//date :2008-12-25
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.reinsure.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "./ReTempContManageInput.js"></SCRIPT> 
<%@include file="./ReTempContManageInit.jsp"%>
</head>
<body  onload="initElementtype();initForm()" >
  <form action="" method=post name=fm target="fraSubmit">  
   <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divLLReport1);"></td>
    	<td class= titleImg>基本信息</td></tr>
    </table>
  
  <Div id= "divLLReport1" style= "display: ''">
	<br>
   	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		保单类型
   			</TD>
   			<TD class= input>
   				<Input class="codeno"  name= "ContType"  value="2" CodeData="0|^1|个单^2|团单" 
          ondblClick="showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" verify="险种类别|NOTNULL" ><Input 
          class= codename name= 'ContTypeName' value="团单" elementtype=nacessary >
   			</TD>
   			<TD class= title>
          保单号
        </TD>
        <TD class= input>
        	<Input class= common name="Contno" elementtype=nacessary onchange="getVerify();" > 
        </TD>
        <TD class= title>
          合同状态
        </TD>
        <TD class= input colspan=3>
        	<Input class="codeno" readOnly name= "ReContState" value="02" CodeData="0|^01|有效^02|失效" 
          ondblClick="showCodeListEx('ReContState',[this,ReContStateName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('ReContState',[this,ReContStateName],[0,1],null,null,null,1);" verify="合同状态|NOTNULL" ><Input 
          class= codename name= 'ReContStateName' elementtype=nacessary value="失效">
        </TD>
        
      </TR> 
      <TR>
      		<TD class= title>
   		 		合同编号
   			</TD>
   			<TD class= input>
   				<Input class= common name="TempContCode" id="TempContCodeId" verify="合同编号|NOTNULL" elementtype=nacessary> 
   			</TD>
   			<TD class= title>
          合同名称
        </TD>
        <TD class= input>
        	<Input class= common name= TempContName id="TempContNameId" verify="合同名称|NOTNULL" elementtype=nacessary> 
        </TD>
 
   			<TD class= title>
   				分保方式
        </TD>
        <TD class= input>
        	<Input class="codeno" readOnly name= "CessionMode" CodeData="0|^1|成数^2|溢额" 
          ondblClick="showCodeListEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);" verify="分保方式|NOTNULL" ><Input 
          class= codename name= 'CessionModeName' elementtype=nacessary >
        </TD>
 
      </TR>      
      <TR>  
      	     	<TD class= title>
   		 		险种类别
   			</TD>
   			<TD class= input>
   				<Input class="codeno" readOnly name= "DiskKind" CodeData="0|^L|长险^M|短险" 
          ondblClick="showCodeListEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);" verify="险种类别|NOTNULL"><Input 
          class= codename name= 'DiskKindName' elementtype=nacessary>
   			</TD>   	
   			       <td class="title">
        	分保保费方式
        </td>
        <td class="input">
        	<Input class="codeno" readOnly name= "CessionFeeMode" CodeData="0|^1|风险保费方式^2|原始保费方式^3|修正共保方式" 
          ondblClick="showCodeListEx('CessionFeeMode',[this,CessionFeeModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionFeeMode',[this,CessionFeeModeName],[0,1],null,null,null,1);"><Input 
          class= codename name= 'CessionFeeModeName'  elementtype=nacessary>
        </td>
        <td class="title">
        	 分保规则
        </td>
        <td class="input">
        	<Input class="codeno" readOnly name= "ReType" value="01" CodeData="0|^01|按险种"  
          ondblClick="showCodeListEx('ReType',[this,ReTypeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('ReType',[this,ReTypeName],[0,1],null,null,null,1);" verify="分保规则|NOTNULL" ><Input 
          class= codename name= 'ReTypeName' elementtype=nacessary  value="按险种"> 
        </td>
      </TR>  
      <TR>
      	<TD class= title>
   		 		再保公司编号
   			</TD>
   			<TD class= input >
   				<input class="code" name="ReComCode" 
	            ondblclick="return showCodeList('reinsurecomcode',[this,ReComName],[0,1],null,null,null,1,300);"
	            onkeyup="return showCodeListKey('reinsurecomcode', [this,ReComName],[0,1],null,null,null,1,300);" verify="再保公司编号|NOTNULL" elementtype=nacessary>
   			</TD>
   			<TD class= title>
          再保公司名称
        </TD>
        <TD class= input colspan=3>
        	<Input class="common" name= ReComName style="width:98%" readonly > 
        </TD>   
    </TR>
   	</Table>
  </div>
   	<br>
   	
	<!--  Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">  
	</Div-->
	</div>
	
    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divContRisk);"></td>
    	<td class= titleImg>险种信息</td></tr>
    </table>
    <Div  id= "divContRisk" style= "display: ''">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanContRiskGrid" ></span>
        		</td>
      		</tr>
    	</table>
	</div>	
	<Div  id= "divPlanRiskW" style= "display: ''">
   	<table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divPlanRisk);"></td>
    	<td class= titleImg>保障计划信息</td></tr>
    </table>
    <Div  id= "divPlanRisk" style= "display: ''">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanPlanGrid" ></span>
        		</td>
      		</tr>
    	</table>
	</div>	
	</div>
     <!--  table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divContGetDuty);"></td>
    	<td class= titleImg>责任信息</td></tr>
    </table-->
    <Div  id= "divContGetDuty" style= "display:'none'">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanContGetDutyGrid" ></span>
        		</td>
      		</tr>
    	</table>
    	</div>

	<table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" ></td>
    	<td class= titleImg>分保信息</td></tr>
    </table>
    <Table class= common>
   		<TR class= common>
        <TD class= title>
   				最低再保保额
        </TD>
        <TD class= input>
        	<Input class= common name= CalMode verify="最低再保保额|NUM" >
        </TD>
        <TD class= title>
   				最高再保保额
        </TD>
        <TD class= input>
        	<Input class= common name= RetainAmnt verify="最高再保保额|NUM">
        </TD>
        <TD class= title>
          	分保比例
        </TD>
        <TD class= input>
	      <Input class= common name= CessionRate verify="分保比例|NUM" elementtype=nacessary> 
        </TD>
      </TR>       
      <TR>
      	<TD class= title>
   		 		接受份额
   			</TD>
   			<TD class= input >
   				<Input class= common name= AccQuot verify="接受份额|NUM"> 
   			</TD>
   		<td class="title">
        	手续费比例
        </td>
        <td class="input">
        	<Input class= common name= ReinProcFeeRate verify="手续费比例|NUM" elementtype=nacessary> 
        </td>
         <td class="title">
        	理赔参与限额
        </td>
        <td class="input">
        	<Input class= common name= ClaimAttLmt verify="理赔参与限额|NUM"> 
        </td>
       
      </TR>
      
     <TR>   
       
      	<TD class= title>
   		 		理赔通知限额
   			</TD>
   			<TD class= input>
   				<Input class= common name= ClaimNoticeLmt verify="理赔通知限额|NUM"> 
   			</TD>
   		<TD class= title id="PremTitle" style="display: 'none'">
   		 		再保净费率
   			</TD>
   			<TD class= input id="PremValue" style="display: 'none'">
   				<Input  class= common name= CessPremRate verify="再保净费率|NUM"> 
   				
   			</TD>
      </TR>
      
   	</Table>
   	<font color="red" >溢额再保净费率为每千元保费的费率。如：录入再保净费率为0.1则在计算时的实际净费率为0.1/1000=0.0001</font>
   	    <!--  table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divRelDisk);"></td>
    	<td class= titleImg>关联险种信息</td></tr>
    </table-->
       	<Div  id= "divRelDisk" style= "display: 'none'">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanRelDiskGrid" ></span>
        		</td>
      		</tr>
    	</table>
	</div>
  <input type=hidden name=OperateType >
 <input type=hidden name=riskcode >
  <input type=hidden name=flag >
   <input type=hidden name=recontcode >
   <input type=hidden name=plancode value="1">
   <Input type=hidden name= ProfitCom verify="盈余佣金比例|NUM" > 
   <Input type=hidden name= ReinManFeeRate verify="管理费比例|NUM" >
   	<Input type=hidden name= ChoiRebaRate verify="选择折扣比例|NUM" > 
   	<Input type=hidden name= SpeComRate verify="特殊佣金比例|NUM" >
      <Div id="savedisk" style="display:'none'">
  <input class=cssButton  VALUE="保 存" TYPE=button onClick="submitForm();">
  </Div>
  <Div id="updatedisk" style="display:'none'">
  	<input class=cssButton  VALUE="修 改" TYPE=hidden onClick="updateForm();">
  	
  	<input class=cssButton  VALUE="删 除" TYPE=button onClick="deleteForm();">
  </Div> 
   <Div id="confirmdisk" style="display:'none'">
  <input class=cssButton  VALUE="审核通过" TYPE=button onClick="confirmForm();">
  </Div>	
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>