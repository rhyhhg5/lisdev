<%
//Creator :liuli
//Date :2008-10-27
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
    String CurrentDate= PubFun.getCurrentDate();               	               	
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ReContCode').value 		 		= '';
    fm.all('ReContState').value             = '';
    fm.all('ReContStateName').value         = '';
    fm.all('ReComCode').value 		 		= '';
    fm.all('ReComName').value 		 		= '';
//    fm.all('ReType').value 		 		= '';
    fm.all('CessionMode').value 			= '';
    fm.all('CessionModeName').value 	= '';
    fm.all('DiskKind').value 			= '';
    fm.all('DiskKindName').value 	= '';
  }         
  catch(ex) 
  {
    alert("初始化变量值时出现错误！！！！");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initContGrid();
  }
  catch(re)
  {
    alert("ReContManageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function getComName(comC)
{
	var strSQL="select name from LDCom where comcode= '"+ comC +"' ";
	var comN = easyExecSql(strSQL);	
	
	if (comN)
	{	
		fm.all("ManageComName").value= comN[0][0];
		//alert();
		
	}
}

function showCom()
{
	var typeRadio="";
	for(var i=0;fm.StateRadio.length;i++)
	{
		if(fm.StateRadio[i].checked)
		{
			typeRadio=fm.StateRadio[i].value;
			break;
		}
	}
	if (typeRadio=="0")
	{
		divComName.style.display='none';
		divComCode.style.display='none';
		fm.ManageCom.value = 'A';
	}
	else
	{
		divComName.style.display='';
		divComCode.style.display='';
		fm.ManageCom.value = <%=Comcode%>;
	}
}
//分保险种信息
function initContGrid() 
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="合同编号";
    iArray[1][1]="80px";
    iArray[1][2]=100;
    iArray[1][3]=1;

    iArray[2]=new Array();
    iArray[2][0]="合同名称";         	//列名
    iArray[2][1]="250px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="合同状态";         	//列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="险类";         	//列名
    iArray[4][1]="100px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="分保方式";         	//列名
    iArray[5][1]="110px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="合同录入日期";         	//列名
    iArray[6][1]="110px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许


    ContGrid = new MulLineEnter( "fm" , "ContGrid" );
    ContGrid.mulLineCount = 10;
    ContGrid.displayTitle = 1;
    ContGrid.canSel=1;
    ContGrid. hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ContGrid. hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)

    ContGrid.loadMulLine(iArray);
    //ContGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("合同初始化时出错:"+ex);
  }
}

</script>


