var showInfo;

var turnPage = new turnPageClass(); 

function easyQueryClick()
{
	    var day = showMonthLastDay();  //获取提取月份的最后一天
	    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
	    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
		var strWhere="";
		var strWhereT="";
		
	 	if(fm.ReComCode.value!=''){
	 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and ReComCode='"+fm.ReComCode.value+"') ";
	 	    strWhereT +=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and ComCode='"+fm.ReComCode.value+"') ";
	 	}
	 	if(fm.DiskKind.value!=''){
	 		strWhere+=" and exists(select 1 from lrcontinfo where recontcode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
	 		strWhere+=" and exists(select 1 from LRTempCessCont where TempContCode=a.recontcode and DiskKind='"+fm.DiskKind.value+"') ";
	 	}
	 	if(fm.Year.value!=''){
	 		strWhere+=" and b.getdatadate between '"+StartDate+"' and '"+EndDate+"'";
	 		strWhereT+=" and b.getdatadate between '"+StartDate+"' and '"+EndDate+"'";
	 	} 
		if(fm.MntClm.value!=''){
			if(fm.MntClm.value == '1'){
				strWhere=" and a.realpay>(select decimal(factorvalue,12,2)" 
						 +" from lrcalfactorvalue f where f.recontcode=a.recontcode"
						 +" and f.riskcode=a.riskcode and  factorcode='NoticeLmt'"
						 +" and factorvalue is not null) ";
			}
			
			if(fm.MntClm.value == '2'){
				strWhere=" and a.realpay>(select decimal(factorvalue,12,2) from lrcalfactorvalue f where" 
						 +" f.recontcode=a.recontcode and f.riskcode=a.riskcode and  factorcode='ClaimAttLmt'"
						 +" and factorvalue is not null) ";
			}
	 	}
	 	
	 	strSQL =" select a.riskcode,(select d.RecomName from lrrecominfo d,lrcontinfo e where d.Recomcode = e.Recomcode"
	 		    +" and e.RecontCode = a.recontCode),"
	 		    +" (case a.riskcalsort when '1' then '成数' when '2' then '溢额' end) riskcalsort, "
	 		    +" a.contno,b.appntname ,b.insuredname,b.cvalidate,b.signdate,b.amnt,"
	 		    +" b.CessAmnt,a.AccidentDate,a.EndCaseDate,a.RealPay,"
	 		    +" c.claimbackfee from lrpolclm a,lrpol b,lrpolclmresult c where a.recontcode=b.recontcode and a.polno=b.polno"
	 		    +" and a.reinsureitem=b.reinsureitem and "
	 		    +" a.PolNo = c.Polno and a.RecontCode = c.RecontCode and a.ClmNo = c.ClmNo and b.conttype='1' "
	 		    +" union"
	 		    +" select a.riskcode,(select d.RecomName from lrrecominfo d,lrcontinfo e where d.Recomcode = e.Recomcode "
	 		    +" and e.RecontCode = a.recontCode),(case a.riskcalsort"
	 		    +" when '1' then '成数' when '2' then '溢额' end) riskcalsort,"
	 		    +" a.grpcontno,b.appntname ,b.insuredname,b.cvalidate,b.signdate,b.amnt,"
	 		    +" b.CessAmnt,a.AccidentDate,a.EndCaseDate,a.RealPay,c.claimbackfee"
	 		    +" from lrpolclm a,lrpol b,lrpolclmresult c where a.recontcode=b.recontcode and a.polno=b.polno"
	 		    +" and a.reinsureitem=b.reinsureitem and a.PolNo = c.Polno and a.RecontCode = c.RecontCode and a.ClmNo = c.ClmNo and b.conttype='2' "
	 		    + strWhere;
	 	
	 	strSQL += "with ur";
 		turnPage.queryModal(strSQL, NewClmGrid); 
}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  // content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

function getinfo(){
	 var index=NewClmGrid.getSelNo()
     if(index>0){
     	if(NewClmGrid.getRowColData(index-1 ,3)=="成数"){
     	  document.getElementById('divadisk').style.display='none';
     	  document.getElementById('divbdisk').style.display='';
		  fm.CessPrem.value=NewClmGrid.getRowColData(index-1 ,13);
		  fm.ReProcFee.value=NewClmGrid.getRowColData(index-1 ,16);	
		  var xx = NewClmGrid.getRowColData(index-1,20);
		  if(xx==""||xx==null||xx=="00"){
			 fm.BState.value="00";
		  }else{	
		    fm.BState.value=xx;
		  }
		  fm.CessionAmount.value=NewClmGrid.getRowColData(index-1 ,12);	
		  fm.ChoiRebaFee.value=NewClmGrid.getRowColData(index-1 ,17);
		  var xx = NewClmGrid.getRowColData(index-1,20);
		  if(xx==""||xx==null||xx=="00"){	
		   fm.AState.value="00";
		  }else{	
		   fm.AState.value=xx;
		  }
		}else{
		  document.getElementById('divadisk').style.display='';
    	  document.getElementById('divbdisk').style.display='none';
		  fm.CessionAmount.value=NewClmGrid.getRowColData(index-1 ,12);	
		  fm.ChoiRebaFee.value=NewClmGrid.getRowColData(index-1 ,17);
		  var xx = NewClmGrid.getRowColData(index-1,20);
		  if(xx==""||xx==null||xx=="00"){	
		   fm.AState.value="00";
		  }else{	
		   fm.AState.value=xx;
		  }
		   fm.CessPrem.value=NewClmGrid.getRowColData(index-1 ,13);
		  fm.ReProcFee.value=NewClmGrid.getRowColData(index-1 ,16);	
		  var xx = NewClmGrid.getRowColData(index-1,20);
		  if(xx==""||xx==null||xx=="00"){
			 fm.BState.value="00";
		  }else{	
		    fm.BState.value=xx;
		  }
		}
     }
}

function updateCheck(){
	var rowCount=NewClmGrid.getSelNo();
	if(rowCount<=0){
		alert('请选择一条信息');
		return false;
	}else{
		var ActuGetNo = NewClmGrid.getRowColData(rowCount,20);
		if(ActuGetNo=="03"){  // 状态问题，结算完成的账单不能做修改
		   alert('账单已结算，分保数据不能修改！！！');
		  return false;	
		}
	}	
	if(checkNum(fm.CessionAmount)==false){
		return false;
	}
	if(checkNum(fm.CessPrem)==false){
		return false;
	}
	if(checkNum(fm.ReProcFee)==false){
		return false;
	}
	if(checkNum(fm.ChoiRebaFee)==false){
		return false;
	}
	
}

function checkNum(control){
   var a = control.value.match(/^(-?\d+)(\.\d+)?$/); 
   if (a == null) {
      alert('数值不符合');
			control.select();
			control.focus();
			return false;
   } 
}


function update(){
	if(updateCheck()==false){
		return false;
	}
	if(confirm('是否确定对该条信息的分保信息进行修改')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="UPDATE";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}

function check(){
	if(vCheck()==false){
		return false;
	}
	if(confirm('是否确定对选中的信息做审核通过操作')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="CHECK";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}

function vCheck(){
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
 	var day = showMonthLastDay();  // 获取提取月份的最后一天
    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  // 格试化日期
    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
	var strWhere="";
	if(fm.RecontCode.value !=''){
		strWhere=" and recontcode='"+fm.RecontCode.value+"'";
	}
 	var sqla = "select payflag from LRAccounts where startdate='"+StartDate+"' and enddata='"+EndDate+"'" 
                +strWhere+" with ur";
 	var strResult = easyExecSql(sqla);
 	if(strResult!=null){
 		for(var i=0;i<strResult.length;i++){
 			var flag = strResult[i][1];
 			if(flag=="03"){
 			  alert('存在结算的账单,您不能进行清单审核,请重新录入查询条件!');
 			  return false;
 			}
 		}
 	}
}

function onloadlist(){
 	try 
	{
		var i = 0;
		var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.OperateType.value="ONLOAD";
		fm.submit();
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}


function downAfterSubmit(cfilePath) {
	showInfo.close();
	fileUrl.href = cfilePath; 
  fileUrl.click();
}

function showMonthLastDay()
    {
        var tmpDate=new Date(fm.Year.value,fm.Month.value,1); 
        var MonthLastDay=new Date(tmpDate-86400000);
        return MonthLastDay.getDate();      
    }

        

