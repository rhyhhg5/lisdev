var showInfo;

var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var strSQL ="";


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}


function checkNum(control){
   var a = control.value.match(/^(-?\d+)(\.\d+)?$/); 
   if (a == null) {
      alert('数值不符合');
			control.select();
			control.focus();
			return false;
   } 
}


function check(){
	if(vCheck()==false){
		return false;
	}
	if(confirm('是否确定对选中的信息做审核通过操作')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="CONFIRM";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}

function vCheck(){
	var rowCount=0;
	for(var i=0;i<NewAccountCheckGrid.mulLineCount;i++){
		if(NewAccountCheckGrid.getChkNo(i)==true){
			if(rowCount>1){
				break;
			}
			rowCount++;
		}
	}
	if(rowCount==0){
		alert('请选择需要审核的信息');
		return false;
	}
}



function onloadClaimlist(){
	try 
	{
		if (verifyInput() == false)
		    return false;
 	 	strSQL ="select"
 	 		+"   b.riskcode,"
 	 		+"  a.cessprem,"
 	 		+"  b.accprem,"
 	 		+"  (case b.accprem when 0 then a.cessprem else (CAST (a.cessprem as double))/ b.accprem end) as checkRate"
 	 		+" from"
 	 		+"    (select"
 	 		+"      lrc.riskcode,"
 	 		+"      sum (lrc.claimbackfee) as cessprem"
 	 		+"    from"
 	 		+"      lrclaimlist lrc"
 	 		+"    where"
 	 		+"      lrc.riskcode in ('1602','1604','1601','160306' , '160307' , '160308' , '5601' , '161101' , '161201' , '1607' , '560301' , '190106' , '5901' , '590206' )"
 	 		+"     and"
 	 		+"      lrc.getdatadate between '"+fm.StartDate.value+"' and '"+fm.EndDate.value+"' "
 	 		+"             and "
 	 		+"              lrc.reinsureitem <> 'G'"
 	 		+"            group by"
 	 		+"              lrc.riskcode) as a right join "
 	 		+"                ( select"
 	 		+"                  lid.riskcode,"
 	 		+"                  sum (lid.pay) as accprem"
 	 		+"                from"
 	 		+"                  ljagetclaim lid"
 	 		+"                where"
 	 		+"                  lid.riskcode in ('1602','1604','1601','160306' , '160307' , '160308' , '5601' , '161101' , '161201' , '1607' , '560301' , '190106' , '5901' , '590206' )"
 	 		+"                 and"
 	 		+"                  lid.othernotype='5'"
 	 		+"                 and"
 	 		+"                  lid.makedate between '"+fm.StartDate.value+"' and '"+fm.EndDate.value+"' "
 	 		+"                        group by"
 	 		+"                          lid.riskcode) b"
 	 		+"                            on a.riskcode = b.riskcode"
 	 		+" union "
 	 		+" select"
 	 		+"  a.riskcode,"
 	 		+"  a.cessprem,"
 	 		+"  b.accprem,"
 	 		+"  (case b.accprem when 0 then a.cessprem else (CAST (a.cessprem as double))/ b.accprem end) as checkRate"
 	 		+" from"
 	 		+"    (select"
 	 		+"      lrc.riskcode,"
 	 		+"      sum (lrc.claimbackfee) as cessprem"
 	 		+"    from"
 	 		+"      lrclaimlist lrc"
 	 		+"    where"
 	 		+"      lrc.riskcode in ('1602','1604','1601','160306' , '160307' , '160308' , '5601' , '161101' , '161201' , '1607' , '560301' , '190106' , '5901' , '590206' )"
 	 		+"     and"
 	 		+"      lrc.getdatadate between '"+fm.StartDate.value+"' and '"+fm.EndDate.value+"' "
 	 		+"             and "
 	 		+"              lrc.reinsureitem <> 'G' "
 	 		+"            group by "
 	 		+"              lrc.riskcode) as a left join "
 	 		+"                ( select "
 	 		+"                  lid.riskcode,"
 	 		+"                  sum (lid.pay) as accprem"
 	 		+"                from"
 	 		+"                  ljagetclaim lid"
 	 		+"                where"
 	 		+"                  lid.riskcode in ('1602','1604','1601','160306' , '160307' , '160308' , '5601' , '161101' , '161201' , '1607' , '560301' , '190106' , '5901' , '590206' )"
 	 		+"                 and"
 	 		+"                  lid.othernotype='5'"
 	 		+"                 and"
 	 		+"                  lid.makedate between '"+fm.StartDate.value+"' and '"+fm.EndDate.value+"' "
 	 		+"                        group by "
 	 		+"                          lid.riskcode) b "
 	 		+"                            on  a.riskcode = b.riskcode"
 	 		+" order by 1,2"
 	 		+" with ur";
	 	
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	//fm.OperateType.value="ONLOADSum";
	 fm.querySql.value = strSQL;
	 //fm.action = "LREstimateWSave.jsp";
	 fm.submit();
} catch(ex) 
{
	//showInfo.close( );
	alert(ex);
}
}

function onloadClaimGlist(){
	try 
	{
		if (verifyInput() == false)
		    return false;
 	 	strSQL ="select"
 	 		+"  b.riskcode,"
 	 		+"  a.cessprem,"
 	 		+"  b.accprem,"
 	 		+"  (case b.accprem when 0 then a.cessprem else (CAST (a.cessprem as double))/ b.accprem end) as checkRate"
 	 		+" from"
 	 		+"    (select"
 	 		+"      lrc.riskcode,"
 	 		+"      sum (lrc.claimbackfee) as cessprem"
 	 		+"    from"
 	 		+"      lrclaimlist lrc"
 	 		+"    where"
 	 		+"      lrc.riskcode in ('1602','1604','1601','160306' , '160307' , '160308' , '5601' , '161101' , '161201' , '1607' , '560301' , '190106' , '5901' , '590206' )"
 	 		+"     and"
 	 		+"      lrc.getdatadate between '"+fm.StartDate.value+"' and '"+fm.EndDate.value+"' "
 	 		+"             and "
 	 		+"              lrc.reinsureitem = 'G' "
 	 		+"            group by "
 	 		+"              lrc.riskcode) as a right join "
 	 		+"                ( select "
 	 		+"                  lid.riskcode,"
 	 		+"                  sum (lid.claimmoney) as accprem"
 	 		+"                from"
 	 		+"                  lcispayget lid"
 	 		+"                where"
 	 		+"                  lid.riskcode in ('1602','1604','1601','160306' , '160307' , '160308' , '5601' , '161101' , '161201' , '1607' , '560301' , '190106' , '5901' , '590206' )"
 	 		+"                 and"
 	 		+"                  othernotype='5'"
 	 		+"                 and"
 	 		+"                  lid.makedate between '"+fm.StartDate.value+"' and '"+fm.EndDate.value+"' "
 	 		+"                        group by "
 	 		+"                          lid.riskcode) b "
 	 		+"                            on   a.riskcode = b.riskcode "
 	 		+" union "
 	 		+" select"
 	 		+"  a.riskcode,"
 	 		+"  a.cessprem,"
 	 		+"  b.accprem,"
 	 		+"  (case b.accprem when 0 then a.cessprem else (CAST (a.cessprem as double))/ b.accprem end) as checkRate"
 	 		+" from"
 	 		+"    (select"
 	 		+"      lrc.riskcode,"
 	 		+"      sum (lrc.claimbackfee) as cessprem"
 	 		+"    from"
 	 		+"      lrclaimlist lrc"
 	 		+"    where"
 	 		+"      lrc.riskcode in ('1602','1604','1601','160306' , '160307' , '160308' , '5601' , '161101' , '161201' , '1607' , '560301' , '190106' , '5901' , '590206' )"
 	 		+"     and"
 	 		+"      lrc.getdatadate between '"+fm.StartDate.value+"' and '"+fm.EndDate.value+"' "
 	 		+"             and "
 	 		+"              lrc.reinsureitem = 'G'"
 	 		+"            group by"
 	 		+"              lrc.riskcode) as a left join"
 	 		+"                ( select"
 	 		+"                  lid.riskcode,"
 	 		+"                  sum (lid.claimmoney) as accprem"
 	 		+"                from"
 	 		+"                  lcispayget lid"
 	 		+"                where"
 	 		+"                  lid.riskcode in ('1602','1604','1601','160306' , '160307' , '160308' , '5601' , '161101' , '161201' , '1607' , '560301' , '190106' , '5901' , '590206' )"
 	 		+"                 and"
 	 		+"                  othernotype='5'"
 	 		+"                 and"
 	 		+"                  lid.makedate between '"+fm.StartDate.value+"' and '"+fm.EndDate.value+"' "
 	 		+"                        group by "
 	 		+"                          lid.riskcode) b"
 	 		+"                            on  a.riskcode = b.riskcode"
 	 		+" order by 1,2"
 	 		+" with ur";

	 	
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	//fm.OperateType.value="ONLOADSum";
	 fm.querySql.value = strSQL;
	 //fm.action = "LREstimateWSave.jsp";
	 fm.submit();
} catch(ex) 
{
	//showInfo.close( );
	alert(ex);
}
}



function downAfterSubmit(cfilePath) {
	showInfo.close();
	fileUrl.href = cfilePath; 
  fileUrl.click();
}

function showMonthLastDay()
{
     var tmpDate=new Date(fm.Year.value,fm.Month.value,1); 
     var MonthLastDay=new Date(tmpDate-86400000);
     return MonthLastDay.getDate();      
}

        

