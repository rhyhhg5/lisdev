<html>
<%
//name :CessInfoInit.jsp
//function :Manage CessInfo
//Creator :
//date :2006-08-15
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 		GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator			= tG.Operator;
  	String Comcode			= tG.ManageCom;
 		String CurrentDate	= PubFun.getCurrentDate();   
    String tCurrentYear	=	StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=	StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate	=	StrTool.getVisaDay(CurrentDate);   
    String tGrpProposalNo = request.getParameter("GrpProposalNo");
    String tModifyFlag 		= request.getParameter("ModifyFlag");
    
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.GrpProposalNo.value	 	= "<%=tGrpProposalNo%>";
  	fm.ModifyFlag.value 			= "<%=tModifyFlag%>";
  	if(fm.ModifyFlag.value=="1")
  	{
  		divReContNo.style.display="";
  		divSearch.style.display="none";
  	}
  	else
  	{
  		divReContNo.style.display="none";
  		divSearch.style.display="";
  	}
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initPolGrid();
    initSelPolGrid();
    if(fm.ModifyFlag.value!="1")
    {
   		easyQueryClick();
  	}
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名
      iArray[0][1]="30px";            //列宽
      iArray[0][2]=30;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="印刷号";        	//列名
      iArray[1][1]="120px";           //列宽
      iArray[1][2]=100;            		//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="被保险人客户号";  //列名
      iArray[2][1]="120px";           //列宽
      iArray[2][2]=100;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="被保险姓名";      //列名
      iArray[3][1]="120px";           //列宽
      iArray[3][2]=100;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="临分检测信息";    //列名
      iArray[4][1]="160px";           //列宽
      iArray[4][2]=100;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="管理机构";        //列名
      iArray[5][1]="100px";           //列宽
      iArray[5][2]=100;            		//列最大值
      iArray[5][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="ProposalNo";   				//列名
      iArray[6][1]="100px";           //列宽
      iArray[6][2]=100;            		//列最大值
      iArray[6][3]=3;              		//是否允许输入,1表示允许，0表示不允许            

      
      
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 3;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 0;
      PolGrid.canChk = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);     
      
      //PolGrid. selBoxEventFuncName = "easyQueryAddClick";
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initSelPolGrid()
{                              
    var iArray = new Array();
      
    try
    {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";            //列宽
	    iArray[0][2]=30;            		//列最大值
	    iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[1]=new Array();
	    iArray[1][0]="印刷号";        //列名
	    iArray[1][1]="120px";           //列宽
	    iArray[1][2]=100;            		//列最大值
	    iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	
	    iArray[2]=new Array();
	    iArray[2][0]="被保险人客户号";        //列名
	    iArray[2][1]="120px";           //列宽
	    iArray[2][2]=100;            		//列最大值
	    iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[3]=new Array();
	    iArray[3][0]="被保险姓名";        //列名
	    iArray[3][1]="120px";           //列宽
	    iArray[3][2]=100;            		//列最大值
	    iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	
	    iArray[4]=new Array();
	    iArray[4][0]="临分检测信息";    //列名
	    iArray[4][1]="160px";           //列宽
	    iArray[4][2]=100;            		//列最大值
	    iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	
	    iArray[5]=new Array();
	    iArray[5][0]="管理机构";        //列名
	    iArray[5][1]="100px";           //列宽
	    iArray[5][2]=100;            		//列最大值
	    iArray[5][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[6]=new Array();
	    iArray[6][0]="ProposalNo";   //列名
	    iArray[6][1]="100px";           //列宽
	    iArray[6][2]=100;            		//列最大值
	    iArray[6][3]=3;              		//是否允许输入,1表示允许，0表示不允许            
	
	    
	    
	    
	    SelPolGrid = new MulLineEnter( "fm" , "SelPolGrid" ); 
	    //这些属性必须在loadMulLine前
	    SelPolGrid.mulLineCount = 0;                                                       
	    SelPolGrid.displayTitle = 1;                                                       
	    SelPolGrid.canSel=0;                                                               
	    SelPolGrid.canChk=0;                                                               
	    SelPolGrid.hiddenPlus=0;   			//是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)    
	    SelPolGrid.hiddenSubtraction=0; 	//是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)  
	    SelPolGrid.loadMulLine(iArray);                                                    
	    SelPolGrid.detailInfo="单击显示详细信息";                                             
	    
	    //PolGrid. selBoxEventFuncName = "easyQueryAddClick";
	    
	    //这些操作必须在loadMulLine后面
	    //PolGrid.setRowColData(1,1,"asdf");
    }
    catch(ex)
    {
      alert(ex);
    }
}
</script>
