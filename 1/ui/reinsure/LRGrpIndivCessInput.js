var showInfo;

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	return true;
}

function easyQueryClick()
{
	var strSQL = "select PrtNo,InsuredNo,b.Name,'',ManageCom,ProposalNo "
	+ " From lcpol a ,LDPerson b where grppolno=(select grppolno from lcgrppol where grpproposalno='"+fm.GrpProposalNo.value+"') and b.customerno=a.InsuredNo and reinsureflag in ('2','3','4','5') " 
	+ getWherePart("a.InsuredNo","InsuredNo")
	+ getWherePart("b.Name","InsuredName","like")
	; 
	turnPage.queryModal(strSQL, PolGrid); 
}

function recontQuery()
{
  window.open("./FrameTempRecontQuery.jsp?GrpProposalNo="+fm.GrpProposalNo.value+"&ModifyFlag="+fm.ModifyFlag.value,"RecontQuery"); 
}

function addCessConclusion()
{
	var EventCount=PolGrid.mulLineCount; //得到MulLine的行数	
	var chkFlag=false; //用于标记是否有事件被选中	
	for (var i=0;i<EventCount;i++) //根据mulLineCount数循环
	{
		if(PolGrid.getChkNo(i)==true) //得到被选中的事件,就将chkFlag置为true
		{
			var selCount = SelPolGrid.mulLineCount; //得到下面临分处理保单mulLine的长度
			var selFlag=false; //检测是否重复数据
			for (var j=0;j<selCount;j++) //循环下面mulLine，找到重复数据
			{
				if(PolGrid.getRowColData(i,6)==SelPolGrid.getRowColData(j,6)) //如果上面mulLine的proposalNo与下面相同
				{
					var selFlag=true;
				}
			}
			if(selFlag==false) //如果没有则向最后一行插入数据
			{
				SelPolGrid.addOne();
				SelPolGrid.setRowColData(SelPolGrid.mulLineCount-1,1,PolGrid.getRowColData(i, 1));
				SelPolGrid.setRowColData(SelPolGrid.mulLineCount-1,2,PolGrid.getRowColData(i, 2));
				SelPolGrid.setRowColData(SelPolGrid.mulLineCount-1,3,PolGrid.getRowColData(i, 3));
				SelPolGrid.setRowColData(SelPolGrid.mulLineCount-1,4,PolGrid.getRowColData(i, 4));
				SelPolGrid.setRowColData(SelPolGrid.mulLineCount-1,5,PolGrid.getRowColData(i, 5));
				SelPolGrid.setRowColData(SelPolGrid.mulLineCount-1,6,PolGrid.getRowColData(i, 6));
			}
			chkFlag=true;
		}
	}
	if (chkFlag==false)
	{
		alert("请选中关联事件");
		return false;
	}
}

function cessConclusion() //临分结论
{
	var strSQL="";
	var arrResult;
	//strSQL="select RecontCode from LRTempCessContInfo a where a.RiskCode = (select riskcode from lcgrppol where grpproposalno='"+fm.GrpProposalNo.value+"')"
	//	+ " and a.PrtNo=(select PrtNo from lcgrppol where grpproposalno='"+fm.GrpProposalNo.value+"')"
	//	;
	//arrResult=easyExecSql(strSQL);
	//if(arrResult!=null)
	//{
	//	alert("此险种已经对特殊保单下过临分结论,如需修改请临分协议修改中进行!");
	//	return false;
	//}
	
	if(SelPolGrid.mulLineCount==0)
	{
		alert("没有要临分处理的险种保单！");
		return false;
	}
	
	var cPrtNo 	= SelPolGrid.getRowColData(0, 1); 		//印刷号
	strSQL = "select GrpName,ManageCom from LCGrpCont where PrtNo='" + cPrtNo + "'";
	
	var cAppntName="";
	var cManageCom="";
	
	arrResult=easyExecSql(strSQL);
	if(arrResult==null)
	{
	}
	else
	{
		cAppntName = arrResult[0][0]; 	//投保人
  	cManageCom = arrResult[0][1];  //管理机构
	}
  
	window.open("./FramCessConclusion.jsp?LoadFlag=7&PrtNo="+cPrtNo+"&AppntName="+cAppntName+"&ManageCom="+cManageCom+"&ModifyFlag=3","GrpIndivCess");
}

function showPolDetail() //保单信息查询
{
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert("请先选择保单！");
		return false;
	}
	var cContNo 		= PolGrid.getRowColData(tSel-1, 2);
  var cPrtNo 			= PolGrid.getRowColData(tSel-1, 1); 
  window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+cPrtNo+"&ContNo="+cContNo+"&AppntName="+cAppntName, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  resetForm();
  }
}

function updateClick()
{
	fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	return true;
	
}

function deleteClick()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该批次吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	
	return true;
	
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function ClosePage()
{
	top.close();
}