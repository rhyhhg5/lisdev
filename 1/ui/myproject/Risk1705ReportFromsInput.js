var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}

try{
	var turnPage = new turnPageClass();        //使用翻页功能，必须建立为全局变量
}
catch(ex)
{}

//提交前校验
function beforeSumbit(){
	if(fm.sdate.value == null || fm.sdate.value == "" || fm.sdate.value == "null"){
		alert("统计截止日期不能为空！");
		return false;
	}
	return true;
}

//下载
function downLoad() {
	
	var q = document.getElementById('querySql').value;
	if(q != null && q != "" && ProjectGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "Risk1705ReportFromsDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}

function queryClick() {
	if(!beforeSumbit()){
		return false;
	}
	initProjectGrid();
	var sdate = fm.sdate.value;
	var strSQL1 = "Select b.Managecom 机构, "
		+"       (Select Riskname From Lmrisk Where Rtrim(Riskcode) = b.Riskcode) 产品名称, "
		+"       b.Riskcode 产品代号,'2' 个团标志,b.Grpcontno 保单号,c.Grpname 客户名称, "
		+"       c.Appntno 客户号,c.Signdate 签单日期,c.Cvalidate 生效日期,c.Cinvalidate 终止日期, "
		+"       (select Codename from ldcode where codetype='payintv' and code=c.Payintv) 缴费方式, "
		+"       (Select Sum(Ljpg.Sumactupaymoney)From Ljapay Lja "
		+"        Inner Join Ljapaygrp Ljpg On Lja.Payno = Ljpg.Payno "
		+"        Where Duefeetype = '0' And Ljpg.Grppolno = b.Grppolno) 保费收入, "
		+"       '-' 年龄, '-' 性别, "
		+"       Case "
		+"         When Exists (Select 1 From Lccont Where Conttype = '2' And Poltype = '1'And Grpcontno = b.Grpcontno)  "
		+"		 Then "
		+"          (Select Sum(Insuredpeoples)From Lcpol Where Grpcontno = b.Grpcontno And Riskcode = b.Riskcode "
		+"           And Conttype = '2' And Poltypeflag = '1' And Stateflag <>'0') "
		+"         Else "
		+"          (Select Count(1) From Lcpol Where Grpcontno = b.Grpcontno And Riskcode = b.Riskcode "
		+"           And Conttype = '2'And Poltypeflag = '0' And Stateflag <>'0') "
		+"       End 被保险人人数, "
		+"       b.Amnt 公共保额,b.Salechnl 销售渠道, b.Sumprem 共收保费, "
		+"       b.Prem+(select nvl(sum(getmoney),0) from ljagetendorse where feeoperationtype='BJ' and grppolno=b.grppolno)*c.payintv/12 当期应收保费, "
		+"       '0' 续保, c.Stateflag 保单状态, '0001-01-01' 退保终止日期, c.Markettype 市场类型, "
		+"       (select costcenter from labranchgroup where agentgroup=c.agentgroup) 成本中心, "
		+"       (select distinct CalFactorValue from lccontplandutyparam where grpcontno=b.grpcontno and CalFactor='Mult' and  grppolno=b.grppolno and contplancode='11') 档次, "
		+"       (select sum(realpay) from llclaimdetail a ,llcase b where  a.caseno=b.caseno and b.rgtstate in ('11','12') and  grpcontno=c.grpcontno ) 死伤医疗给付, "
		+"       case when (select max(lpp.confdate) from lpgrpedoritem lpg,lpedorapp lpp where lpg.edorno=lpp.edoracceptno and lpg.grpcontno  =c.grpcontno and lpg.edortype in ('WT','XT','CT')) is not null and  "
		+"       (select max(lpp.confdate) from lpgrpedoritem lpg,lpedorapp lpp where lpg.edorno=lpp.edoracceptno " 
		+"       and lpg.grpcontno  =c.grpcontno and lpg.edortype in ('WT','XT','CT')) <= '"+sdate+"' "
		+"       then "
		+"       (select getmoney from lpgrpedoritem where grpcontno =c.grpcontno and edortype in ('WT','XT','CT')) "
		+"       else 0 end 退保金, "
		+"       (select lpa.confdate from lpgrpedoritem lpg,lpedorapp lpa where lpg.edorno=lpa.edoracceptno "
		+"				and lpg.edortype in ('CT','XT','WT') and lpa.edorstate='0' and lpg.grpcontno=c.grpcontno) 退保日期 "
		+"From Lcgrppol b, "
		+"     Lcgrpcont c "
		+"Where b.Riskcode = '170501' And c.Appflag = '1' And c.Grpcontno = b.Grpcontno and c.signdate <= '"+sdate+"' and c.cvalidate <= '"+sdate+"'"
		+" union Select b.Managecom 机构, "
		+"       (Select Riskname From Lmrisk Where Rtrim(Riskcode) = b.Riskcode) 产品名称, "
		+"       b.Riskcode 产品代号, '2' 个团标志,b.Grpcontno 保单号,c.Grpname 客户名称,c.Appntno 客户号, "
		+"       c.Signdate 签单日期,c.Cvalidate 生效日期, c.Cinvalidate 终止日期, "
		+"       (select Codename from ldcode where codetype='payintv' and code=c.Payintv) 缴费方式, "
		+"       (Select Sum(Ljpg.Sumactupaymoney)From Ljapay Lja "
		+"        Inner Join Ljapaygrp Ljpg On Lja.Payno = Ljpg.Payno "
		+"        Where Duefeetype = '0' And Ljpg.Grppolno = b.Grppolno) 保费收入, "
		+"       '-' 年龄,'-' 性别, "
		+"       Case "
		+"         When Exists (Select 1 From Lccont Where Conttype = '2' And Poltype = '1' And Grpcontno = b.Grpcontno) Then "
		+"          (Select Sum(Insuredpeoples) From Lcpol Where Grpcontno = b.Grpcontno And Riskcode = b.Riskcode "
		+"           And Conttype = '2' And Poltypeflag = '1' And Stateflag <>'0') "
		+"         Else "
		+"          (Select Count(1) From Lcpol Where Grpcontno = b.Grpcontno And Riskcode = b.Riskcode "
		+"           And Conttype = '2' And Poltypeflag = '0' And Stateflag <>'0') "
		+"       End 被保险人人数, "
		+"       b.Amnt 公共保额, b.Salechnl 销售渠道, b.Sumprem 共收保费, "
		+"       b.Prem+(select nvl(sum(getmoney),0) from ljagetendorse where feeoperationtype='BJ' and grppolno=b.grppolno)*c.payintv/12  当期应收保费, "
		+"       '0' 续保,c.Stateflag 保单状态,'0001-01-01' 退保终止日期, c.Markettype 市场类型, "
		+"       (select costcenter from labranchgroup where agentgroup=c.agentgroup) 成本中心, "
		+"       (select distinct CalFactorValue from lbcontplandutyparam where grpcontno=b.grpcontno and CalFactor='Mult' and grppolno=b.grppolno and contplancode='11') 档次, "
		+"       (select sum(realpay) from llclaimdetail a ,llcase b where  a.caseno=b.caseno and b.rgtstate in ('11','12') and  grpcontno=c.grpcontno) 死伤医疗给付, "
		+"       case when (select max(lpp.confdate) from lpgrpedoritem lpg,lpedorapp lpp where lpg.edorno=lpp.edoracceptno and " +
				"lpg.grpcontno =c.grpcontno and lpg.edortype in ('WT','XT','CT')) is not null and  "
		+"       (select max(lpp.confdate) from lpgrpedoritem lpg,lpedorapp lpp where lpg.edorno=lpp.edoracceptno and " +
				"lpg.grpcontno  =c.grpcontno and lpg.edortype in ('WT','XT','CT')) <= '"+sdate+"' "
		+"       then "
		+"       (select getmoney from lpgrpedoritem where grpcontno =c.grpcontno and edortype in ('WT','XT','CT')) "
		+"       else 0 end 退保金, "
		+"       (select lpa.confdate from lpgrpedoritem lpg,lpedorapp lpa where lpg.edorno=lpa.edoracceptno "
		+"				and lpg.edortype in ('CT','XT','WT') and lpa.edorstate='0' and lpg.grpcontno=c.grpcontno) 退保日期 "
		+"From Lbgrppol b, "
		+"     Lbgrpcont c "
		+"Where b.Riskcode = '170501'And c.Appflag = '1' And c.Grpcontno = b.Grpcontno and c.signdate <= '"+sdate+"' and c.cvalidate <= '"+sdate+"' with ur" ;
			document.getElementById('querySql').value = strSQL1;
	     turnPage.queryModal(strSQL1,ProjectGrid);
	    return false;
    		
}

