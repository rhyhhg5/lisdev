<%
//程序名称：LDControlCodeInput.jsp
//程序功能：
//创建日期：2002-03-18 16:23:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LDControlCodeSchema tLDControlCodeSchema   = new LDControlCodeSchema();

  UILDControlCode tLDControlCode   = new UILDControlCode();

  //输出参数
  DBTerror dbTerror = null;
  String tBmCert = "";

  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

    tLDControlCodeSchema.setCode(request.getParameter("Code"));
    tLDControlCodeSchema.setCodeType1(request.getParameter("CodeType1"));
    tLDControlCodeSchema.setOtherSign(request.getParameter("OtherSign"));
    tLDControlCodeSchema.setCodeType2(request.getParameter("CodeType2"));
    tLDControlCodeSchema.setCodeDescription(request.getParameter("CodeDescription"));


  try
  {
    tLDControlCode.submitData(tLDControlCodeSchema,"控制数据");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  dbTerror = tLDControlCode.getDBTerror();
  if (dbTerror.getErrno()==0)
  {                          
    Content = " 保存成功";
  	FlagStr = "Succ";
  }
  else                                                                           
  {
  	Content = " 保存失败，原因是:" + dbTerror.getErrmessage() ;
  	FlagStr = "Fail";
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

