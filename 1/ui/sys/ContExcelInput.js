var showInfo;
var turnPage = new turnPageClass();

function Print()
{	
	if(!checkData()){
		return false;
	}
	var salechnl=fm.TheSaleChnl.value;
	var riskcode=fm.TheRiskCode.value;
	var managecom=fm.TheManagecom.value;
	var startdate=fm.SignStartDate.value;
	var enddate=fm.SignEndDate.value;
	var  tCodeData="(";
	var tSQL1="";
	var condition=" group by substr(lcp.managecom, 1, 4),substr(lcp.managecom, 1, 6),POLAPPLYDATE order by lcp.POLAPPLYDATE ";
	if(riskcode==0){
		tSQL1="select distinct substr(lcp.managecom, 1, 4) , "
			+"                 (select name  "
			+"                    from ldcom  "
			+"                   where comcode = substr(lcp.managecom, 1, 4) ) 分公司名称, "
			+"                 substr(lcp.managecom, 1, 6)  , "
			//+"                 nvl((select name "
			+"  ( select name "
			+"                    from ldcom "
			+"                   where comcode =substr(lcp.managecom, 1, 6) )地市公司名称," 
			//+"  (select name  "
			//+"                   where comcode = substr(lcp.managecom, 1, 4) )) 地市公司名称, "
			+"                 lcp.POLAPPLYDATE 投保日期, "
			+"                 sum(lcp.prem) 投保保费, "
			//+" sum(lcp.SUPPLEMENTARYPREM) 追加保费, "		
			+"                 count(distinct lcp.prtno) "
			+"   from lcpol lcp, lmriskapp lmr "
			+"  where 1 = 1 "
			+"    and lcp.riskcode = lmr.riskcode "			
			+"    and not EXISTS (select * from lcrnewstatelog where prtno=lcp.prtno) "
			+" and lmr.riskprop='I'"
			+" and (lmr.enddate is null	or lmr.enddate>'"+enddate+"') "
			+" and lmr.startdate<'"+startdate+"'"
			+" and lcp.conttype='1' "
			+" and EXISTS (select 1 from lccont where prtno=lcp.prtno and stateflag in ('0','1')) "
			+"    and lmr.RiskPeriod = 'L'  "
			+" and lmr.riskcode not in ('331501','232001') "
			+" and  exists (select 1 from  lmriskpayintv where payintv <> '0' and riskcode=lmr.riskcode)"
			+ getWherePart("lcp.POLAPPLYDATE","SignStartDate",">=")					 
			+ getWherePart("lcp.POLAPPLYDATE","SignEndDate","<=");		
	}else{	
		var sql="select code from ldcode1 where codetype = 'mainsubriskrela' and code1='"+riskcode+"'";
		 turnPage.strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
		  if (turnPage.strQueryResult != "")
		  {
		    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		    m = turnPage.arrDataCacheSet.length;
		    for (i = 0; i < m; i++)
		    {
		      j = i + 1;
		      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
		      tCodeData = tCodeData +"'"+turnPage.arrDataCacheSet[i][0] +"' , ";
		    }
		    tCodeData=tCodeData.substring(0,tCodeData.length-2);
		    tCodeData+=",'"+riskcode+"'";
			  tCodeData=tCodeData+")";
			
		  }else{
			  tCodeData="('"+riskcode+"')"
		  }
		  
		tSQL1="select distinct substr(lcp.managecom, 1, 4) , "
			+"                 (select name  "
			+"                    from ldcom  "
			+"                   where comcode = substr(lcp.managecom, 1, 4) ) 分公司名称, "
			+"                 substr(lcp.managecom, 1, 6)  , "
			//+"                 nvl((select name "
			+" (select name "
			+"                    from ldcom "
			+"                   where comcode =substr(lcp.managecom, 1, 6) )," 
			//+"  (select name  "
			//+"                    from ldcom  "
			//+"                   where comcode = substr(lcp.managecom, 1, 4) )) 地市公司名称, "
			+"                 lcp.POLAPPLYDATE 投保日期, "
			+"                 sum(lcp.prem) 投保保费, "
		//+"                 sum(lcp.SUPPLEMENTARYPREM) 追加保费, "		
			+"                 count(distinct lcp.prtno) "
			+"   from lcpol lcp "
			+"  where 1 = 1 "	
			+"    and not EXISTS (select * from lcrnewstatelog where prtno=lcp.prtno) "
			+" and lcp.conttype='1' "			
			+" and EXISTS (select 1 from lccont where prtno=lcp.prtno and stateflag in ('0','1')) "
			+" and lcp.riskcode in "+tCodeData+" and   exists (select 1 from  lmriskpayintv where payintv <> '0' and riskcode=lcp.riskcode)"
			+" "
			+ getWherePart("lcp.POLAPPLYDATE","SignStartDate",">=")					 
			+ getWherePart("lcp.POLAPPLYDATE","SignEndDate","<=");	
					
	}
	if(salechnl==0){
		tSQL1=tSQL1+" and lcp.salechnl in ('01','10')"
		
	}else{
		tSQL1=tSQL1+ getWherePart("lcp.salechnl","TheSaleChnl");
	}
	if(managecom!=0){
		
		tSQL1=tSQL1+ getWherePart("lcp.managecom","TheManagecom","like");
		
	}
	
	
	
	tSQL1=tSQL1+condition;
	
	
	
    fm.querySql1.value=tSQL1;  
    
    fm.action = "ContExcelDownload.jsp";
    fm.submit();
    fm.action = "";
    fm.querySql1.value="";  
  
}


function checkData(){
	
	var signdate = fm.SignStartDate.value == "" || fm.SignEndDate.value == "";	
	var salechnl=fm.TheSaleChnl.value;
	var riskcode=fm.TheRiskCode.value;
	var managecom=fm.TheManagecom.value;
	if(signdate ){
		
		alert("投保日起期和投保日止期为保单申请日，必录。");
		return false;
	}
	if(!salechnl){
		alert("销售渠道不能为空");
		return false;
		
	}
	if(!riskcode){
		alert("险种不能为空");
		return false;
		
	}
	if(!managecom){
		managecom("管理机构不能为空");
		return false;
		
	}
	if(riskcode==0||managecom==0){
		var sql = "select 1 from Dual where date('" + fm.SignEndDate.value + "') > date('" + fm.SignStartDate.value + "') + 3 month ";
		  var rs = easyExecSql(sql);
		  if(rs)
		  {
		    if(!confirm("投保日期起止时间大于3个月！是否继续？"))
		    {
		      return false;
		    }
		  }
	}
	 
	  
	
	return true;
}

