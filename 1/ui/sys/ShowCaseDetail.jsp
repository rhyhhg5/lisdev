<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
 * Name     ：ShowcheckInfo.jsp
 * Function ：显示综合查询之审核信息查询
 * Author   :LiuYansong
 * Date     :2004-2-19
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
<%
  //输出参数
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Succ";
	String RgtNo = request.getParameter("RgtNo_1");
	String ClmUWNo =request.getParameter("ClmUWNo");
  String InsuredName = request.getParameter("InsuredName");
	String PolNo = request.getParameter("PolNo");
	String LPJC = StrTool.unicodeToGBK(request.getParameter("LPJC"));

	String LPZT = "";

	//初始化接受的文件
	 LLReportSchema mLLReportSchema = new LLReportSchema();
   LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
   LLClaimSchema mLLClaimSchema = new LLClaimSchema();
   LLClaimUWMDetailSchema mLLClaimUWMDetailSchema = new LLClaimUWMDetailSchema();
   LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
   LLCasePolicySchema mLLCasePolicySchema = new LLCasePolicySchema();
   LJAGetClaimSchema mLJAGetClaimSchema = new LJAGetClaimSchema();
   LLClaimUnderwriteSchema mLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();

  VData tVData = new VData();
  tVData.addElement(RgtNo);
  tVData.addElement(ClmUWNo);
  tVData.addElement(InsuredName);
  tVData.addElement(PolNo);
  System.out.println("2004-4-14日，2理赔进程是"+LPJC);
  ShowCaseDetailUI mShowCaseDetailUI = new ShowCaseDetailUI();
  if (!mShowCaseDetailUI.submitData(tVData,"QUERY"))
  {
    Content = " 没有符合条件的信息，原因是: " + mShowCaseDetailUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
  }
  else
  {
    tVData.clear();
    tVData = mShowCaseDetailUI.getResult();
    mLLReportSchema.setSchema((LLReportSchema)tVData.getObjectByObjectName("LLReportSchema",0));
    mLLRegisterSchema.setSchema((LLRegisterSchema)tVData.getObjectByObjectName("LLRegisterSchema",0));
    mLLClaimSchema.setSchema((LLClaimSchema)tVData.getObjectByObjectName("LLClaimSchema",0));
    mLLClaimUWMDetailSchema.setSchema((LLClaimUWMDetailSchema)tVData.getObjectByObjectName("LLClaimUWMDetailSchema",0));
    mLLSubReportSchema.setSchema((LLSubReportSchema)tVData.getObjectByObjectName("LLSubReportSchema",0));
    mLLCasePolicySchema.setSchema((LLCasePolicySchema)tVData.getObjectByObjectName("LLCasePolicySchema",0));
    mLJAGetClaimSchema.setSchema((LJAGetClaimSchema)tVData.getObjectByObjectName("LJAGetClaimSchema",0));
    mLLClaimUnderwriteSchema.setSchema((LLClaimUnderwriteSchema)tVData.getObjectByObjectName("LLClaimUnderwriteSchema",0));
    if(mLLRegisterSchema.getRgtReason()==null)
    	mLLRegisterSchema.setRgtReason("");
    if(LPJC.trim().equals("签批退回"))
      LPZT = "签批退回";


    %>
     <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
     <script language="javascript">
      parent.fraInterface.fm.all("RptNo").value = "<%=mLLReportSchema.getRptNo()%>";
      parent.fraInterface.fm.all("RgtNo").value = "<%=mLLRegisterSchema.getRgtNo()%>";

      <%
        if(LPJC.trim().equals("签批退回"))
        {
          %>

          parent.fraInterface.fm.all("CaseState").value = "<%=LPZT%>";
          <%
        }
        else
        {
          %>
          parent.fraInterface.fm.all("CaseState").value = "<%=mLLCasePolicySchema.getGrpPolNo()%>";
          <%
        }
      %>
      parent.fraInterface.fm.all("CustomerType").value = "<%=mLLCasePolicySchema.getContNo()%>";
      parent.fraInterface.fm.all("CustomerName").value = "<%=mLLCasePolicySchema.getInsuredName()%>";
      parent.fraInterface.fm.all("AccidentDate").value = "<%=mLLRegisterSchema.getAccidentDate()%>";
      parent.fraInterface.fm.all("AccidentSite").value = "<%=mLLRegisterSchema.getAccidentSite()%>";
      parent.fraInterface.fm.all("AccidentType").value = "<%=mLLSubReportSchema.getRemark()%>";
      parent.fraInterface.fm.all("RptDate").value = "<%=mLLReportSchema.getMakeDate()%>";
      parent.fraInterface.fm.all("RptOperatorName").value = "<%=mLLReportSchema.getRptorName()%>";
      parent.fraInterface.fm.all("RgtDate").value = "<%=mLLRegisterSchema.getMakeDate()%>";
      parent.fraInterface.fm.all("RgtantName").value = "<%=mLLRegisterSchema.getRgtantName()%>";
      parent.fraInterface.fm.all("PDNo").value = "<%=mLJAGetClaimSchema.getGetNoticeNo()%>";
      parent.fraInterface.fm.all("EndCaseDate").value = "<%=mLLClaimSchema.getEndCaseDate()%>";
      parent.fraInterface.fm.all("InformDate").value = "<%=mLLClaimSchema.getModifyDate()%>";
      parent.fraInterface.fm.all("InformName").value = "<%=mLLClaimSchema.getRgtNo()%>";
      parent.fraInterface.fm.all("CaseGetMode").value = "<%=mLLRegisterSchema.getIDNo()%>";
      parent.fraInterface.fm.all("TotalPayMoney").value = "<%=mLLClaimSchema.getRealPay()%>";
      parent.fraInterface.fm.all("CaseNo").value = "<%=mLLCasePolicySchema.getCaseNo()%>";
      parent.fraInterface.fm.all("InsuredNo").value = "<%=mLLCasePolicySchema.getInsuredNo()%>";
			parent.fraInterface.fm.all("ClmDecision").value = "<%=mLLClaimUnderwriteSchema.getRemark()%>";
      var tAccidentReason = Conversion("<%=mLLReportSchema.getAccidentReason()%>");
      var tRgtReason = Conversion("<%=mLLRegisterSchema.getRgtReason()%>");
      parent.fraInterface.fm.all("AccidentReason").value = tAccidentReason;
      parent.fraInterface.fm.all("RgtReason").value = tRgtReason;
      parent.fraInterface.emptyUndefined();
    </script>
  <%
  }

System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>