<%@page contentType="text/html;charset=GBK"%>
<html>
<script>
	var tPrtNo = "<%=request.getParameter("PrtNo")%>";	
	var tIDType = "<%=request.getParameter("IDType")%>";	
	var tIDNo = "<%=request.getParameter("IDNo")%>";	
	var tIDTypeName = "<%=request.getParameter("IDTypeName")%>";
	var tName = "<%=request.getParameter("Name")%>";
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="./GrpTaxCommonQuery.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<%@include file="./GrpTaxCommonQueryInit.jsp"%>
		<title>税优团体客户查询</title>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit">
			<!--查询条件 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLAAgent);">
					</td>
					<td class=titleImg>
						税优团体客户条件
					</td>
				</tr>
			</table>
			<Div id="divLAAgent" style="display: ''">
				<table class=common>
					<TR class=common>
						<TD class=title>
							团体编号
						</TD>
						<TD class=input>
							<Input class=common name=GrpNo>
						</TD>
						<TD class=title>
							单位名称
						</TD>
						<TD class=input>
							<Input class=common name=GrpName>
						</TD>
						<TD class=title>
							税务登记证号码
						</TD>
						<TD class=input>
							<Input class=common name=TaxNo>
						</TD>
						<TD class=title>
							社会信用代码
						</TD>
						<TD class=input>
							<Input class=common name=OrgancomCode>
						</TD>
					</TR>
					<TR>
					    <TD class=title>
							导入批次号
						</TD>
						<TD class=input>
							<Input name=BatchNo class=common >
						</TD>
						<TD class=title>
							被保人姓名
						</TD>
						<TD class=input>
							<Input name=Name class=common >
						</TD>
						<TD class=title>
							证件类型
						</TD>
						<TD class=input>
							<Input name=IDType class=codeNo MAXLENGTH=1
								ondblclick="return showCodeList('idtype',[this,IDTypeName],[0,1]);"
								onkeyup="return showCodeListKey('idtype',[this,IDTypeName],[0,1]);"><input class=codename name=IDTypeName readonly=true>
						</TD>
						<TD class=title>
							证件号码
						</TD>
						<TD class=input>
							<Input name=IDNo class=common >
						</TD>
					</TR>

				</table>

				<!--INPUT class=common VALUE="查询" TYPE=button onclick="easyQueryClick();"-->
				<table>
					<tr>
						<td>
							<INPUT class=cssbutton VALUE="查  询" TYPE=button
								onclick="easyQueryClick();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="返  回" TYPE=button
								onclick="returnParent();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="取  消" TYPE=button
								onclick="closePage();">
						</td>

						<td>
						</td>
						<td>
						</td>
						<td>
						</td>
						<td>
						</td>
					</tr>
				</table>
			</Div>

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divAgentGrid);">
					</td>
					<td class=titleImg>
						查询结果
					</td>
				</tr>
			</table>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanBatchGrid" align=center> </span>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td>
							<INPUT class=cssbutton VALUE="首  页" TYPE=button
								onclick="turnPage.firstPage();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="上一页" TYPE=button
								onclick="turnPage.previousPage();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="下一页" TYPE=button
								onclick="turnPage.nextPage();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="尾  页" TYPE=button
								onclick="turnPage.lastPage();">
						</td>
					</tr>
				</table>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
