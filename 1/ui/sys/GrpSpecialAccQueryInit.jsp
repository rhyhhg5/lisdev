<%
//程序名称：PolQueryInit.jsp
//程序功能：
//创建日期：2002-12-16
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
  //  fm.all('CustomerNo').value = tCustomerNo;
  //  fm.all('Name').value = tName;
  }
  catch(ex)
  {
    alert("GrpPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
     initInpBox();	  
	   initManageFeeGrid();
	   initSpecGrid();
	   initHealthTraceGrid();
	   getAccInfo();
	   //returnParent();
	   //setContSever();
  }
  catch(re)
  {
    alert("GrpSpecialAccQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 
// 管理费定义清单的初始化
function initManageFeeGrid()
  {    
    var iArray = new Array();
      try
      {
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="50px";            		//列宽
	      iArray[0][2]=10;            			//列最大值
	      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
		    iArray[1]=new Array();
				iArray[1][0]="管理费名称";         		//列名
				iArray[1][1]="80px";            		//列宽
				iArray[1][2]=100;            			//列最大值
				iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
				
				iArray[2]=new Array();
				iArray[2][0]="账户类型";         		//列名
				iArray[2][1]="80px";            		//列宽
				iArray[2][2]=100;            			//列最大值
				iArray[2][3]=0;  
				
				iArray[3]=new Array();
				iArray[3][0]="管理费比例";         		//列名
				iArray[3][1]="80px";            		//列宽
				iArray[3][2]=100;            			//列最大值
				iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
				
				iArray[4]=new Array();
				iArray[4][0]="缺省标记";         		//列名
				iArray[4][1]="50px";            		//列宽
				iArray[4][2]=100;            			//列最大值
				iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
				
				/*iArray[4]=new Array();
				iArray[4][0]="缺省标记";         		//列名
				iArray[4][1]="50px";            		//列宽
				iArray[4][2]=100;            			//列最大值
				iArray[4][3]=3;   */           			//是否允许输入,1表示允许，0表示不允许
	
	      ManageFeeGrid = new MulLineEnter( "fm" , "ManageFeeGrid" ); 
	      //这些属性必须在loadMulLine前
	      ManageFeeGrid.mulLineCount = 0;   
	      ManageFeeGrid.displayTitle = 1;
	      ManageFeeGrid.locked = 1;
	      ManageFeeGrid.canSel = 1;
	      ManageFeeGrid.hiddenPlus = 1;
	      ManageFeeGrid.hiddenSubtraction = 1;
	      ManageFeeGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {

        alert("GrpSpecialAccQueryInit.jsp-->initManageFeeGrid函数中发生异常:初始化界面错误!");
      }
}

// 特需医疗帐户交易信息的初始化
function initSpecGrid()
  {    
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[1]=new Array();
      iArray[1][0]="时间";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="30px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="业务类型";         		//列名
      iArray[2][1]="37px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
			iArray[3][0]="业务控制号";         		//列名
			iArray[3][1]="50px";            		//列宽
			iArray[3][2]=100;            			//列最大值
			iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			iArray[4]=new Array();
			iArray[4][0]="账户类型";         		//列名
			iArray[4][1]="50px";            		//列宽
			iArray[4][2]=100;            			//列最大值
			iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			iArray[5]=new Array();
			iArray[5][0]="收费金额";         		//列名
			iArray[5][1]="50px";            		//列宽
			iArray[5][2]=100;            			//列最大值
			iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[6]=new Array();
			iArray[6][0]="退费金额";         		//列名
			iArray[6][1]="50px";            		//列宽
			iArray[6][2]=100;            			//列最大值
			iArray[6][3]=0;              			//是否允许输入,1

      SpecGrid = new MulLineEnter( "fm" , "SpecGrid" ); 
      //这些属性必须在loadMulLine前
      SpecGrid.mulLineCount = 0;   
      SpecGrid.displayTitle = 1;
      SpecGrid.locked = 1;
      SpecGrid.canSel = 1;
      SpecGrid.hiddenPlus = 1;
      SpecGrid.hiddenSubtraction = 1;
      SpecGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert("GrpSpecialAccQueryInit.jsp-->initSpecGrid函数中发生异常:初始化界面错误!");
      }
}

// 特需医疗帐户交易信息的初始化
function initHealthTraceGrid()
  {    
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[1]=new Array();
      iArray[1][0]="时间";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="30px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="业务类型";         		//列名
      iArray[2][1]="37px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
			iArray[3][0]="业务控制号";         		//列名
			iArray[3][1]="50px";            		//列宽
			iArray[3][2]=100;            			//列最大值
			iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			iArray[4]=new Array();
			iArray[4][0]="账户类型";         		//列名
			iArray[4][1]="50px";            		//列宽
			iArray[4][2]=100;            			//列最大值
			iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			iArray[5]=new Array();
			iArray[5][0]="收费金额";         		//列名
			iArray[5][1]="50px";            		//列宽
			iArray[5][2]=100;            			//列最大值
			iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[6]=new Array();
			iArray[6][0]="退费金额";         		//列名
			iArray[6][1]="50px";            		//列宽
			iArray[6][2]=100;            			//列最大值
			iArray[6][3]=0;              			//是否允许输入,1

      HealthTraceGrid = new MulLineEnter( "fm" , "HealthTraceGrid" ); 
      //这些属性必须在loadMulLine前
      HealthTraceGrid.mulLineCount = 0;   
      HealthTraceGrid.displayTitle = 1;
      HealthTraceGrid.locked = 1;
      HealthTraceGrid.canSel = 1;
      HealthTraceGrid.hiddenPlus = 1;
      HealthTraceGrid.hiddenSubtraction = 1;
      HealthTraceGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert("GrpSpecialAccQueryInit.jsp-->initHealthTraceGrid函数中发生异常:初始化界面错误!");
      }
}
</script>