<%
//程序名称：OLDUserQuery.js
//程序功能：
//创建日期：2002-08-16 17:44:47
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  LDUserSchema tLDUserSchema   = new LDUserSchema();

  OLDUserUI tOLDUserQueryUI   = new OLDUserUI();
  //读取Session中的全局类
	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

    tLDUserSchema.setUserCode(request.getParameter("UserCode"));
    tLDUserSchema.setUserName(request.getParameter("UserName"));
    tLDUserSchema.setComCode(request.getParameter("ComCode"));
    tLDUserSchema.setPassword(request.getParameter("Password"));
    tLDUserSchema.setUserDescription(request.getParameter("UserDescription"));
    tLDUserSchema.setUserState(request.getParameter("UserState"));
    tLDUserSchema.setUWPopedom(request.getParameter("UWPopedom"));
    tLDUserSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLDUserSchema.setOtherPopedom(request.getParameter("OtherPopedom"));
    tLDUserSchema.setPopUWFlag(request.getParameter("PopUWFlag"));
    tLDUserSchema.setSuperPopedomFlag(request.getParameter("SuperPopedomFlag"));
    tLDUserSchema.setOperator(request.getParameter("Operator"));
    tLDUserSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDUserSchema.setMakeTime(request.getParameter("MakeTime"));
    tLDUserSchema.setValidStartDate(request.getParameter("ValidStartDate"));
    tLDUserSchema.setValidEndDate(request.getParameter("ValidEndDate"));



  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLDUserSchema);
	tVData.add(tG);

  FlagStr="";
  // 数据传输
	if (!tOLDUserQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " +  tError.getFirstError();
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tOLDUserQueryUI.getResult();
		
		// 显示
		LDUserSet mLDUserSet = new LDUserSet();
		mLDUserSet.set((LDUserSet)tVData.getObjectByObjectName("LDUserSet",0));
		int n = mLDUserSet.size();
		LDUserSchema mLDUserSchema;
		for (int i = 1; i <= n; i++)
		{
		  	mLDUserSchema = mLDUserSet.get(i);
		   	%>
		   	<script language="javascript">
        parent.fraInterface.UserGrid.addOne("UserGrid")
parent.fraInterface.fm.UserGrid1[<%=i-1%>].value="<%=mLDUserSchema.getUserCode()%>";
parent.fraInterface.fm.UserGrid2[<%=i-1%>].value="<%=mLDUserSchema.getUserName()%>";
parent.fraInterface.fm.UserGrid3[<%=i-1%>].value="<%=mLDUserSchema.getComCode()%>";
parent.fraInterface.fm.UserGrid4[<%=i-1%>].value="<%=mLDUserSchema.getUserDescription()%>";

			</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (!FlagStr.equals("Fail"))
  {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

