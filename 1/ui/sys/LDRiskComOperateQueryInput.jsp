<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDRiskComOperateQueryInput.jsp
//程序功能：
//创建日期：2003-10-28 15:15:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LDRiskComOperateQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LDRiskComOperateQueryInit.jsp"%>
  <title>险种机构操作岗位权限控制表</title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
  <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDRiskComOperateGrid1);">
      </td>
      <td class= titleImg>
        请您输入查询条件： 
      </td>
    	</tr>   
    </table>
  <Div  id= "divLDRiskComOperateGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title> 险种编码 </TD>
    <TD  class= input> <Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);">  </TD>
    <TD  class= title> 管理机构  </TD>
    <TD  class= input>  <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">  </TD> 
    <TD  class= title> 授权机构  </TD>
    <TD  class= input>  <Input class="code" name=ComCode ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">  </TD> 
  </TR>
  <TR  class= common>
    <TD  class= title> 功能标识 </TD>
    <TD  class= input>  <Input class="code" name=OperateType ondblclick="return showCodeList('OperateType',[this, Remark], [0, 1]);" onkeyup="return showCodeListKey('OperateType', [this, Remark], [0, 1]);">  </TD> 
    <TD  class= title> 功能描述 </TD>
    <TD  class= input>  <Input class= 'common' name=Remark readonly> </TD>
  </TR>
</table>
  </Div>
          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" class= common TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDRiskComOperateGrid);">
    		</td>
    		<td class= titleImg>
    			 险种机构操作岗位权限控制表查询结果:
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLDRiskComOperateGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLDRiskComOperateGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
