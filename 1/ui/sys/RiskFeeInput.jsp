<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：RiskFeeInput.jsp
//程序功能：
//创建日期：2002-08-15 11:48:42
//创建人 ：CrtHtml程序创建
//更新记录： 更新人  更新日期   更新原因/内容
%>
<%
String GrpContNo = request.getParameter("GrpContNo");
String LoadFlag = request.getParameter("LoadFlag");
String AddData = request.getParameter("AddData");
String cContType=request.getParameter("cContType");
//GrpContNo = "220110000000071";
%>
<head>
<script>
GrpContNo="<%=GrpContNo%>";
LoadFlag="<%=LoadFlag%>";
cContType="<%=cContType%>";
var AddData="<%=AddData%>";
var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
if (ScanFlag == "null") ScanFlag = "0";
var scantype = "<%=request.getParameter("scantype")%>";
var oldContNo ="<%=request.getParameter("oldContNo")%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<!--<SCRIPT>window.document.onkeydown = forconfirm;</SCRIPT>-->
<SCRIPT src="RiskFeeInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="ProposalAutoMove.js"></SCRIPT>
<%if(request.getParameter("scantype")!=null&&request.getParameter("scantype").equals("scan")){%>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<%}%> 
<%@include file="RiskFeeInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form method=post name=fm target="fraSubmit" action="RiskFeeSave.jsp">
		<table>
			<tr>
				<td class=common>
					<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroupPol);">
				</td>
				<td class=titleImg align=center>团体保单信息</td>
			</tr>
		</table>
		<Div id="divGroupPol" style="display: ''">
		<table class=common align=center>
			<TR class=common>
				<TD class=title>
					合同号
				</TD>
				<TD class=input>
					<Input class="readonly" readonly name=GrpContNo value="<%=GrpContNo%>">
					<input type=hidden name="mOperate">
				</TD>
				<TD class=title>
					险种编码
				</TD>
				<TD class=input>
					<Input class=code name=RiskCode ondblclick="return showCodeList('GrpRisk',[this,GrpPolNo],[0,2],null,fm.GrpContNo.value,'b.GrpContNo',1);" onkeyup="return showCodeListKey('GrpRisk',[this,GrpPolNo],[0,2],null,fm.GrpContNo.value,'b.GrpContNo',1);">
				</TD>
				<!--TD class=title>
					分保险单号
				</TD-->
				<TD class=input>
					<input class="readonly" readonly type=hidden name="GrpPolNo">
				</tD>
			</TR>
		</table>
	    <input type=button value="管理费查询" class="cssButton" onclick="easyQueryClick();">
	    <hr>
		  <Div  id= "divInverstFlag" style= "display: 'none'">
  <TABLE class=common>
    <tr class=common>
        <td class=title>
            未归属帐户投资标志
        </td>
        <td class= input>
            <Input class="codeNo" readonly name=C4Flag value='0' CodeData="0|^0|非直接投资^1|直接投资" ondblclick="return showCodeListEx('C4Flag', [this,C4FlagName],[0,1],null,null,null,1)"	onkeyup="return showCodeListKeyEx('C4Flag', [this,C4FlagName],[0,1],null,null,null,1)" ><input class=codename name=C4FlagName value='非直接投资' readonly=true elementtype=nacessary>
        </td>
        <td class=title>
        </td>
        <td class= input >
        </td>
        <td class=title> </td>
        <td class= input> </td>
        </tr>
    </TABLE>
     </Div>

  <Div  id= "divTQRulesInput" style= "display: 'none'">
  <table>
	<tr>
		<td class=common>
			<img src="../common/images/butExpand.gif" style="cursor:hand;" >
		</td>
		<td class=titleImg align=center>领取信息</td>
	</tr>
  </table>
  <TABLE class=common>
    <tr class=common>
        <td class=title>
            最低起领年限
        </td>
        <td class= input>
            <Input class=common name=MinGetIntv value='' elementtype=nacessary>
        </td>
         <td class=title>
            领取间隔（年）
        </td>
        <td class= input>
            <Input class=common name=GetIntvF value='' elementtype=nacessary>
        </td>
         <td class=title>
            领取宽限期（天）
        </td>
        <td class= input>
            <Input class=common name=GetIntvWT value='' elementtype=nacessary>
        </td>
      </tr>
      <tr class=common>
         <td class=title>
            领取次数
        </td>
        <td class= input>
            <Input class=common name=TotalGetTime value='' elementtype=nacessary>
        </td>
      </tr>
    </TABLE>
     </Div>
     <Div  id= "divGrpLimit" style= "display: 'none'">
      	<table>
			<tr>
				<td class=common>
					<img src="../common/images/butExpand.gif" style="cursor:hand;">
				</td>
				<td class=titleImg align=center>转换信息</td>
			</tr>
  		</table>
  		<TABLE class=common>
    		<tr class=common>
        		<td class=title>
            		产品/投资转换次数
       			</td>
        		<td class= input>
            		<Input class=common name=LimitNum value='' elementtype=nacessary>
        		</td>
        		<td class=title>
        		    超转换次数扣除费用(元)
        		</td>
        		<td class= input >
        			<Input class=common name=LimitFee value='' elementtype=nacessary>
        		</td>
        		<td class=title> </td>
         		<td class= input> </td>
        	</tr>
    	</TABLE>
     </Div>
		</Div>		
		<br>
		<br>
		<table>
			<tr>
				<td class=common>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divRiskFee);">
				</td>
				<td class=titleImg>
					险种管理费明细
				</td>
			</tr>
		</table>
		<Div id="divRiskFee" style="display: ''">
			<table class=common>
			    <tr>
			    	<td><font color=red>填写说明：所缴纳的初始费用比例录入值范围为0-0.05；账户管理费录入值范围为0-5（单位：元）。</font></td>
			    </tr>
			    <tr>
			    	<td><font color=red>示例：所缴纳的初始费用比例为5%,则录入为0.05；账户管理费为5元，则录入5。</font></td>
			    </tr>
				<tr class=common>
					<td text-align: left colSpan=1>
						<span id="spanRiskFeeGrid">
						</span>
					</td>
				</tr>
			</table>
			
			<table class=common>
				<tr>
					<td class= title8>个人帐户保单管理费收取方式：	</td>
		        	<td class=input>
		        	<Input class=codeno name=RiskFeeMode VALUE="0" CodeData="0|^1|从个人账户收取^2|从公共账户收取" ondblclick="return showCodeListEx('RiskFeeMode',[this,RiskFeeModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('RiskFeeMode',[this,RiskFeeModeName],[0,1],null,null,null,1);"><input class=codename name=RiskFeeModeName readonly=true > 		     		
		        	</td>
				</tr>
				<tr>
					<td class= title8>公共帐户保单管理费收取方式：	</td>
		        	<td class=input>
		        	<Input class=codeno name=GGRiskFeeMode VALUE="2" readonly=true><input class=codename name=GGRiskFeeModeName readonly=true value = "从公共账户收取" > 		     		
		        	</td>
				</tr>
			</table>
		</div>
		<br>
		<div id="divRiskFeeParam" style="display:'none'">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;">
					</td>
					<td class=titleImg>
						管理费明细参数
					</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1>
						<span id="spanRiskFeeParamGrid">
						</span>
					</td>
				</tr>
			</table>
		</div>
		<input type=button class="cssButton" value="上一步" onclick="returnparent();">
	   <Div  id= "divRiskFeeSave" style= "display: ''" align= left> 
		<input type=button class="cssButton" value="保  存" onclick="this.disabled=true; setEnable('tSubmit'); submitForm();" id="tSubmit"> <!-- setEnable('tSubmit'); -->
		<input type=button class="cssButton" value="删  除" onclick="this.disabled=true; setEnable('tDelete'); deleteClick();" id="tDelete">
		</Div>
	<div id="autoMoveButton" style="display: none">
	<input type="button" name="autoMoveInput" value="随动定制确定" onclick="submitAutoMove('666');" class=cssButton>
	<%--<input type="button" name="Next" value="下一步" onclick="location.href='ContInsuredInput.jsp?LoadFlag='+tLoadFlag+'&prtNo='+prtNo+'&checktype=2&scantype='+scantype" class=cssButton>	
        --%>
        <INPUT VALUE="返回定制投保人界面" class=cssButton TYPE=button onclick="returnparent();">     
        <input type=hidden id="" name="autoMoveFlag">
        <input type=hidden id="" name="autoMoveValue">   
        <input type=hidden id="" name="pagename" value="666">                         
      </div>  
		<input type=hidden name="AddData">
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
