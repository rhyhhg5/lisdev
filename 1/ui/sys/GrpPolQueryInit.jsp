<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件

    fm.all('CustomerNo').value =customerNo;	
    fm.all('Name').value = '';
    fm.all('Birthday').value = '';    
    fm.all('Sex').value = '';
    fm.all('IDNo').value = '';
	  //fm.all('ContNo').value = '';
	  fm.all('HomeAddress').value = '';
    fm.all('HomeZipCode').value = '';

  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    
  }      
}
function initForm()
{
  try
  {
    initInpBox();
    initGrpPolGrid();
    initPersonGrid();
	  initGrpPersonGrid();
	  initBContGrid();
	if (customerNo =='' || customerNo ==null)
	{}
	else
	{
		easyQueryClick();
	}
  }
  catch(re)
  {
    alert("GroupPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpPolGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[1]=new Array();
    iArray[1][0]="保单号码";         		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="ContNo";
    
    iArray[2]=new Array();
    iArray[2][0]="印刷号";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0; 
    iArray[2][21]="PrtNo"; 
    
    iArray[3]=new Array();
    iArray[3][0]="客户性质";         		//列名
    iArray[3][1]="30px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0; 
    iArray[3][21]="CustomerKind"; 
  
    iArray[4]=new Array();
    iArray[4][0]="保单性质";         		//列名
    iArray[4][1]="50px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0; 
    iArray[4][21]="ContKind"; 
    
    iArray[5]=new Array();
    iArray[5][0]="投保人";         		//列名
    iArray[5][1]="80px";            		//列宽                                              
    iArray[5][2]=100;            			//列最大值                                              
    iArray[5][3]=0;                   
    iArray[5][3]="AppntNo"; 
    
    iArray[6]=new Array();
    iArray[6][0]="被保人";         		//列名
    iArray[6][1]="60px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="InsuredName";
  
    iArray[7]=new Array();
    iArray[7][0]="生效日期";         		//列名
    iArray[7][1]="70px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="满期日期";         		//列名
    iArray[8][1]="70px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[9]=new Array();
    iArray[9][0]="保费交至日";         		//列名
    iArray[9][1]="70px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[10]=new Array();
    iArray[10][0]="期交保费";         		//列名
    iArray[10][1]="50px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[11]=new Array();
    iArray[11][0]="保单状态";         		//列名
    iArray[11][1]="60px";            		//列宽
    iArray[11][2]=100;            			//列最大值
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="服务机构";         		//列名
    iArray[12][1]="50px";            		//列宽
    iArray[12][2]=100;            			//列最大值
    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[13]=new Array();
    iArray[13][0]="新单回访状态";         		//列名
    iArray[13][1]="80px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    iArray[14]=new Array();
    iArray[14][0]="服务人员";         		//列名
    iArray[14][1]="50px";            		//列宽
    iArray[14][2]=100;            			//列最大值
    iArray[14][3]=0;
    
    iArray[15]=new Array();
    iArray[15][0]="保单类型";         		//列名
    iArray[15][1]="50px";            		//列宽
    iArray[15][2]=100;            			//列最大值
    iArray[15][3]=0; 
    iArray[15][21]="ContType";
    
    iArray[16]=new Array();
    iArray[16][0]="机构代码";         		//列名
    iArray[16][1]="50px";            		//列宽
    iArray[16][2]=100;            			//列最大值
    iArray[16][3]=0; 
    iArray[16][21]="ManageCom";
    
    iArray[17]=new Array();
    iArray[17][0]="表类型";         		//C表或B表
    iArray[17][1]="50px";            		//列宽
    iArray[17][2]=100;            			//列最大值
    iArray[17][3]=3; 
    iArray[17][21]="TableType";
    
    iArray[18]=new Array();
    iArray[18][0]="渠道";         		//C表或B表
    iArray[18][1]="50px";            		//列宽
    iArray[18][2]=100;            			//列最大值
    iArray[18][3]=0; 
    iArray[18][21]="SaleChnlCode";
  	
  	iArray[19]=new Array();
    iArray[19][0]="网点";         		//C表或B表
    iArray[19][1]="50px";            		//列宽
    iArray[19][2]=100;            			//列最大值
    iArray[19][3]=0; 
    iArray[19][21]="AgentComBank";
    
    iArray[20]=new Array();
    iArray[20][0]="网点名称";         		//C表或B表
    iArray[20][1]="50px";            		//列宽
    iArray[20][2]=100;            			//列最大值
    iArray[20][3]=0; 
    iArray[20][21]="AgentComName";
    
    iArray[21]=new Array();
    iArray[21][0]="拆分前保单号";         		//C表或B表
    iArray[21][1]="50px";            		//列宽
    iArray[21][2]=100;            			//列最大值
    iArray[21][3]=0; 
    iArray[21][21]="Lcont1";
    
    iArray[22]=new Array();
    iArray[22][0]="签单日期";         		//C表或B表
    iArray[22][1]="70px";            		//列宽
    iArray[22][2]=100;            			//列最大值
    iArray[22][3]=0; 
    iArray[22][21]="SignDate";
    
    iArray[23]=new Array();
    iArray[23][0]="签单时间";         		//C表或B表
    iArray[23][1]="70px";            		//列宽
    iArray[23][2]=100;            			//列最大值
    iArray[23][3]=0; 
    iArray[23][21]="SignDateTime";
    
    iArray[24]=new Array();
    iArray[24][0]="市场类型";         		//C表或B表
    iArray[24][1]="50px";            		//列宽
    iArray[24][2]=100;            			//列最大值
    iArray[24][3]=0; 
    iArray[24][21]="MarkTypeV";
    
     iArray[25]=new Array();
    iArray[25][0]="移动电话"; //C表或B表
    iArray[25][1]="70px"; //列宽
    iArray[25][2]=100; //列最大值
    iArray[25][3]=0; 
    iArray[25][21]="MobilePhone";
    
    
     iArray[26]=new Array();
    iArray[26][0]="联系电话"; //C表或B表
    iArray[26][1]="70px"; //列宽
    iArray[26][2]=100; //列最大值
    iArray[26][3]=0; 
    iArray[26][21]="Phone";
    
    
    
     iArray[27]=new Array();
    iArray[27][0]="联系地址"; //C表或B表
    iArray[27][1]="70px"; //列宽
    iArray[27][2]=100; //列最大值
    iArray[27][3]=0; 
    iArray[27][21]="Address";
    
    
      
  
    GrpPolGrid = new MulLineEnter( "fm" , "GrpPolGrid" ); 
    //这些属性必须在loadMulLine前
    GrpPolGrid.mulLineCount = 0;   
    GrpPolGrid.displayTitle = 1;
    GrpPolGrid.locked = 1;
    GrpPolGrid.canSel = 1;
    GrpPolGrid.hiddenPlus = 1;
    GrpPolGrid.hiddenSubtraction = 1;
    GrpPolGrid.loadMulLine(iArray); 
    GrpPolGrid.selBoxEventFuncName = "setContValue";
  }
  catch(ex)
  {
    alert("GroupPolQueryInit.jsp-->initGrpPolGrid函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initBContGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[1]=new Array();
    iArray[1][0]="保单号码";         		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="ContNo";
    
    iArray[2]=new Array();
    iArray[2][0]="印刷号";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0; 
    iArray[2][21]="PrtNo"; 
    
    iArray[3]=new Array();
    iArray[3][0]="客户性质";         		//列名
    iArray[3][1]="30px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0; 
    iArray[3][21]="CustomerKind"; 
    
    iArray[4]=new Array();
    iArray[4][0]="投保人";         		//列名
    iArray[4][1]="80px";            		//列宽                                              
    iArray[4][2]=100;            			//列最大值                                              
    iArray[4][3]=0;                   
    iArray[4][3]="AppntNo"; 
    
    iArray[5]=new Array();
    iArray[5][0]="被保人";         		//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="InsuredName";
  
    iArray[6]=new Array();
    iArray[6][0]="生效日期";         		//列名
    iArray[6][1]="70px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="期交保费";         		//列名
    iArray[7][1]="70px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;            			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="SumActuPayMoney";
  
    iArray[8]=new Array();
    iArray[8][0]="保单状态";         		//列名
    iArray[8][1]="60px";            		//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="终止日期";         		//列名
    iArray[9][1]="50px";            		//列宽
    iArray[9][2]=100;            			//列最大值
    iArray[9][3]=0; 
    
    iArray[10]=new Array();
    iArray[10][0]="终止原因";         		//列名
    iArray[10][1]="60px";            		//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=0; 
    
    iArray[11]=new Array();
    iArray[11][0]="退保原因";         		//列名
    iArray[11][1]="50px";            		//列宽
    iArray[11][2]=100;            			//列最大值
    iArray[11][3]=0; 
    
    iArray[12]=new Array();
    iArray[12][0]="承保机构";         		//列名
    iArray[12][1]="50px";            		//列宽
    iArray[12][2]=100;            			//列最大值
    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[13]=new Array();
    iArray[13][0]="新单回访状态";         		//列名
    iArray[13][1]="80px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;         		//是否允许录入，0--不能，1--允许   
    
    iArray[14]=new Array();
    iArray[14][0]="保单类型";         		//列名
    iArray[14][1]="50px";            		//列宽
    iArray[14][2]=100;            			//列最大值
    iArray[14][3]=0; 
    iArray[14][21]="ContType";
    
    iArray[15]=new Array();
    iArray[15][0]="机构代码";         		//列名
    iArray[15][1]="50px";            		//列宽
    iArray[15][2]=100;            			//列最大值
    iArray[15][3]=0; 
    iArray[15][21]="ManageCom";
    
    iArray[16]=new Array();
    iArray[16][0]="表类型";         		//C表或B表
    iArray[16][1]="0";            		//列宽
    iArray[16][2]=100;            			//列最大值
    iArray[16][3]=3;    
    iArray[16][21]="TableType";   
    
    iArray[17]=new Array();
    iArray[17][0]="渠道";         		//C表或B表
    iArray[17][1]="50px";            		//列宽
    iArray[17][2]=100;            			//列最大值
    iArray[17][3]=0;    
    iArray[17][21]="SaleChnlCode";   
    
    iArray[18]=new Array();
    iArray[18][0]="网点";         		//C表或B表
    iArray[18][1]="50px";            		//列宽
    iArray[18][2]=100;            			//列最大值
    iArray[18][3]=0;    
    iArray[18][21]="AgentComBank";   
    
    iArray[19]=new Array();
    iArray[19][0]="网点名称";         		//C表或B表
    iArray[19][1]="50px";            		//列宽
    iArray[19][2]=100;            			//列最大值
    iArray[19][3]=0;    
    iArray[19][21]="AgentComName";   
    
    iArray[20]=new Array();
    iArray[20][0]="拆分前保单号";         		//C表或B表
    iArray[20][1]="50px";            		//列宽
    iArray[20][2]=100;            			//列最大值
    iArray[20][3]=0; 
    iArray[20][21]="Lcont2";
    
    iArray[21]=new Array();
    iArray[21][0]="签单日期";         		//C表或B表
    iArray[21][1]="70px";            		//列宽
    iArray[21][2]=100;            			//列最大值
    iArray[21][3]=0; 
    iArray[21][21]="SignDate";
    
    iArray[22]=new Array();
    iArray[22][0]="签单时间";         		//C表或B表
    iArray[22][1]="70px";            		//列宽
    iArray[22][2]=100;            			//列最大值
    iArray[22][3]=0; 
    iArray[22][21]="SignDateTime";
    
    iArray[23]=new Array();
    iArray[23][0]="市场类型";         		//C表或B表
    iArray[23][1]="50px";            		//列宽
    iArray[23][2]=100;            			//列最大值
    iArray[23][3]=0; 
    iArray[23][21]="MarkTypeB";
    
     iArray[24]=new Array();
    iArray[24][0]="移动电话"; //C表或B表
    iArray[24][1]="70px"; //列宽
    iArray[24][2]=100; //列最大值
    iArray[24][3]=0; 
    iArray[24][21]="MobilePhone";
    
    
     iArray[25]=new Array();
    iArray[25][0]="联系电话"; //C表或B表
    iArray[25][1]="70px"; //列宽
    iArray[25][2]=100; //列最大值
    iArray[25][3]=0; 
    iArray[25][21]="Phone";
    
    
    
     iArray[26]=new Array();
    iArray[26][0]="联系地址"; //C表或B表
    iArray[26][1]="70px"; //列宽
    iArray[26][2]=100; //列最大值
    iArray[26][3]=0; 
    iArray[26][21]="Address";
    
  
    BContGrid = new MulLineEnter( "fm" , "BContGrid" ); 
    //这些属性必须在loadMulLine前
    BContGrid.mulLineCount = 0;   
    BContGrid.displayTitle = 1;
    BContGrid.locked = 1;
    BContGrid.canSel = 1;
    BContGrid.hiddenPlus = 1;
    BContGrid.hiddenSubtraction = 1;
    BContGrid.loadMulLine(iArray); 
    BContGrid.selBoxEventFuncName = "setContValue1";
  }
  catch(ex)
  {
    alert("GroupPolQueryInit.jsp-->initBContGrid函数中发生异常:初始化界面错误!");
  }
}

// 个单客户信息列表的初始化
function initPersonGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="客户号码";         		//列名
    iArray[1][1]="90px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="CustomerNo";
    
    iArray[2]=new Array();
    iArray[2][0]="客户姓名";         		//列名
    iArray[2][1]="85px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="性别";         		//列名
    iArray[3][1]="50px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="出生日期";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="证件类型";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=130;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="证件号码";         		//列名
    iArray[6][1]="130px";            		//列宽
    iArray[6][2]=70;            			//列最大值
    iArray[6][3]=0; 
    
    iArray[7]=new Array();
    iArray[7][0]="客户类别";         		//列名
    iArray[7][1]="140px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="客户风险等级";         	//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=130;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="授权使用客户信息";         	//列名
    iArray[9][1]="80px";            		//列宽
    iArray[9][2]=130;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    PersonGrid = new MulLineEnter( "fm" , "PersonGrid" ); 
    //这些属性必须在loadMulLine前
    PersonGrid.mulLineCount = 0;   
    PersonGrid.displayTitle = 1;
    PersonGrid.locked = 1;
    PersonGrid.canSel = 1;
    PersonGrid.hiddenPlus=1;
    PersonGrid.hiddenSubtraction=1;
    PersonGrid.loadMulLine(iArray); 
    PersonGrid.selBoxEventFuncName = "setPolValue";
    
    //这些操作必须在loadMulLine后面
    //ClientGrid.setRowColData(1,1,"asdf");
  }
  catch(ex)
  {
    alert("GroupPolQueryInit.jsp-->initPersonGrid函数中发生异常:初始化界面错误!");
  }
}
//团单客户信息列表的初始化
//function initGrpPersonGrid()
//{     
//	 var iArray = new Array();
//      
//      try
//      {
//      iArray[0]=new Array();
//      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
//      iArray[0][1]="30px";            		//列宽
//      iArray[0][2]=10;            			//列最大值
//      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//
//      iArray[1]=new Array();
//      iArray[1][0]="客户号码";         		//列名
//      iArray[1][1]="90px";            		//列宽
//      iArray[1][2]=100;            			//列最大值
//      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//      iArray[1][21]="CustomerNo";  
//
//      iArray[2]=new Array();
//      iArray[2][0]="单位名称";         		//列名
//      iArray[2][1]="150px";            		//列宽
//      iArray[2][2]=100;            			//列最大值
//      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//
//      iArray[3]=new Array();
//      iArray[3][0]="负责人";         		//列名
//      iArray[3][1]="50px";            		//列宽
//      iArray[3][2]=200;            			//列最大值
//      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//     
//
//      iArray[4]=new Array();
//      iArray[4][0]="成立日期";         		//列名
//      iArray[4][1]="80px";            		//列宽
//      iArray[4][2]=90;            			//列最大值
//      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//     
//	    iArray[5]=new Array();
//      iArray[5][0]="单位性质";         		//列名
//      iArray[5][1]="100px";            		//列宽
//      iArray[5][2]=90;            			//列最大值
//      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//
//	    iArray[6]=new Array();
//      iArray[6][0]="行业类别";         		//列名
//      iArray[6][1]="150px";            		//列宽
//      iArray[6][2]=200;            			//列最大值
//      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//    
//      GrpPersonGrid = new MulLineEnter( "fm" , "GrpPersonGrid" ); 
//      //这些属性必须在loadMulLine前
//      GrpPersonGrid.mulLineCount = 0;   
//      GrpPersonGrid.displayTitle = 1;
//      GrpPersonGrid.locked = 1;
//      GrpPersonGrid.canSel = 1;
//      GrpPersonGrid.hiddenPlus=1;
//      GrpPersonGrid.hiddenSubtraction=1;
//      GrpPersonGrid.loadMulLine(iArray); 
//	    GrpPersonGrid.selBoxEventFuncName = "setGrpPolValue";
//      
//      //这些操作必须在loadMulLine后面
//      //ClientGrid.setRowColData(1,1,"asdf");
//      }
//      catch(ex)
//      {
//        alert("GroupPolQueryInit.jsp-->initGrpPersonGrid函数中发生异常:初始化界面错误!");
//      }
//}
//团单客户信息列表的初始化
function initGrpPersonGrid()
{     
	 var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="客户号码";         		//列名
      iArray[1][1]="90px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="CustomerNo"; 

      iArray[2]=new Array();
      iArray[2][0]="单位名称";         		//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="负责人";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[4]=new Array();
      iArray[4][0]="成立日期";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=90;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
	    iArray[5]=new Array();
      iArray[5][0]="单位性质";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=90;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	   iArray[6]=new Array();
      iArray[6][0]="行业类别";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="客户风险等级";         	//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=130;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      GrpPersonGrid = new MulLineEnter( "fm" , "GrpPersonGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPersonGrid.mulLineCount = 0;   
      GrpPersonGrid.displayTitle = 1;
      GrpPersonGrid.locked = 1;
      GrpPersonGrid.canSel = 1;
      GrpPersonGrid.hiddenPlus=1;
      GrpPersonGrid.hiddenSubtraction=1;
      GrpPersonGrid.loadMulLine(iArray); 
	  GrpPersonGrid.selBoxEventFuncName = "setGrpPolValue";
      
      //这些操作必须在loadMulLine后面
      //ClientGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("GroupPolQueryInit.jsp-->initGrpPersonGrid函数中发生异常:初始化界面错误!");
      }
}
</script>