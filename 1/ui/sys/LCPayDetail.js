//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var arrAllDataSet；
window.onfocus = myonfocus;

/*********************************************************************
 *  查询被保险人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function getRiskQuery()  
 {  
	var arrReturn = new Array();
	var  strSQL = "select Name,InsuredNo,Birthday,OccupationCode,RelationToAppnt from LCInsured where ContNo='" + tContNo + "' and InsuredNo='" + tInsuredNo + "'";			
	arrReturn = easyExecSql(strSQL);
	if ( arrReturn == null)
	{
         alert("未查到险种信息");

	}
    displayInsured(arrReturn[0]);
	 
 }
  
/*********************************************************************
 *  设置险种信息
 *  参数  ：  cArr
 *  返回值：  无
 *********************************************************************
 */
  function displayInsured(cArr)  
 { 
    try { fm.all('RiskSeqNo').value = tRiskSeqNo; } catch(ex) { };
	try { fm.all('Name').value = cArr[0]; } catch(ex) { };
	try { fm.all('InsuredNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('Birthday').value = cArr[2]; } catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[3]; } catch(ex) { };
	try { fm.all('RelationToAppnt').value = cArr[4]; } catch(ex) { };
 }

 /*********************************************************************
 *  查询险种信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryRisk()
{
	
	// 初始化表格
	initRiskGrid();	
	// 书写SQL语句RiskCode
	var strSQL = "";
	//if(tIsCancelPolFlag=="0"){
	 //strSQL = "select LCPol.grppolno,LCPol.PolNo,LCPol.PrtNo,LCPol.AppntName,LCPol.InsuredName,LCPol.RiskCode,LCPol.ManageCom,LCPol.Prem ,LCPol.Amnt ,LCPol.CValiDate,LCpol.AgentCode,substr(LCPol.PolState,1,2),substr(LCPol.PolState,1,4) from LCPol where ContNo='" + tContNo + "' and appflag<>'9' order by LCPol.PrtNo";			
	
	strSQL = "select LCPol.InsuredName,LCPol.InsuredNo,LCPol.RiskSeqNo,lmrisk.riskname,LCPol.RiskCode,LCPol.Amnt,LCPol.Mult,LCPol.Prem ,LCPol.CValiDate"
	+ " from LCPol,lmrisk  where ContNo='" + tContNo + "' and appflag<>'9' order by lmrisk.riskname"
	//+ " and lmrisk.RiskCode=LCPol.RiskCode";		
	/*}	
	else
	 if(tIsCancelPolFlag=="1"){//销户保单查询
	   strSQL = "select LBPol.grppolno,LBPol.PolNo,LBPol.PrtNo,LBPol.AppntName,LBPol.InsuredName,LBPol.RiskCode,LBPol.ManageCom,LBPol.Prem ,LBPol.Amnt ,LBPol.CValiDate,LBpol.AgentCode,substr(LBPol.PolState,1,2),substr(LBPol.PolState,1,4) from LBPolwhere LBPol.MainPolNo='" + tPolNo + "' and appflag<>'9' order byLBPol.proposalno";
	   alert("销户保单查询"+strSQL);			
	}
	else
	{
	  alert("保单类型传输错误!");
	  return ;
	}*/
	//查询SQL，返回结果字符串
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	RiskGrid.clearData();
  	alert("数据库中没有满足条件的险种信息！");
 
    return false;
  }
 
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  arrAllDataSet = turnPage.arrDataCacheSet;  
 
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = RiskGrid;   
         
  //保存SQL语句
  turnPage.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 // setInvalidate(); 
  var arrDataSet = turnPage.getData(arrAllDataSet, turnPage.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
} 

/*********************************************************************
 *  查询受益人信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryBnf()
{
	
	// 初始化表格
	initBnfGrid();	
	// 书写SQL语句RiskCode
	var strSQL = "";
	//if(tIsCancelPolFlag=="0"){
	 //strSQL = "select LCPol.grppolno,LCPol.PolNo,LCPol.PrtNo,LCPol.AppntName,LCPol.InsuredName,LCPol.RiskCode,LCPol.ManageCom,LCPol.Prem ,LCPol.Amnt ,LCPol.CValiDate,LCpol.AgentCode,substr(LCPol.PolState,1,2),substr(LCPol.PolState,1,4) from LCPol where ContNo='" + tContNo + "' and appflag<>'9' order by LCPol.PrtNo";			
	
	strSQL = "select LCBnf.BnfType,'',LCBnf.IDType,LCBnf.IDNo,LCBnf.RelationToInsured,'','','' ,''"
	+ " from LCBnf  where LCBnf.ContNo='" + tContNo + "' and InsuredNo ='"+ tInsuredNo + "'"
	//+ " and lmrisk.RiskCode=LCPol.RiskCode";		
	//}	
	//else
	// if(tIsCancelPolFlag=="1"){//销户保单查询
	 //  strSQL = "select LBPol.grppolno,LBPol.PolNo,LBPol.PrtNo,LBPol.AppntName,LBPol.InsuredName,LBPol.RiskCode,LBPol.ManageCom,LBPol.Prem ,LBPol.Amnt ,LBPol.CValiDate,LBpol.AgentCode,substr(LBPol.PolState,1,2),substr(LBPol.PolState,1,4) from LBPolwhere LBPol.MainPolNo='" + tPolNo + "' and appflag<>'9' order byLBPol.proposalno";
	 //  alert("销户保单查询"+strSQL);			
	//}
	//else
	//{
	//  alert("保单类型传输错误!");
	 // return ;
	//}
	//查询SQL，返回结果字符串
   turnPage1.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
 
  //判断是否查询成功
  if (!turnPage1.strQueryResult) {
  	BnfGrid.clearData();
  	alert("数据库中没有满足条件的险种信息！");
 
    return false;
  }
 
  //查询成功则拆分字符串，返回二维数组
  turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
 
  //设置初始化过的MULTILINE对象
  turnPage1.pageDisplayGrid = BnfGrid;   
         
  //保存SQL语句
  turnPage1.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage1.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //setInvalidate(); 
  var arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);
}  

/*********************************************************************
 *  查询理赔信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPayGrid()
{
	// 初始化表格
	initPayGrid();	
	// 书写SQL语句RiskCode
	var strSQL = "";
	//if(tIsCancelPolFlag=="0"){
	 //strSQL = "select LCPol.grppolno,LCPol.PolNo,LCPol.PrtNo,LCPol.AppntName,LCPol.InsuredName,LCPol.RiskCode,LCPol.ManageCom,LCPol.Prem ,LCPol.Amnt ,LCPol.CValiDate,LCpol.AgentCode,substr(LCPol.PolState,1,2),substr(LCPol.PolState,1,4) from LCPol where ContNo='" + tContNo + "' and appflag<>'9' order by LCPol.PrtNo";			
	
	strSQL = "select LCPol.InsuredName,LCPol.InsuredNo,LCPol.RiskSeqNo,LCPol.CValiDate"
	+ " from LCPol,lmrisk  where ContNo='" + tContNo + "' and appflag<>'9' order by lmrisk.riskname"

	//+ " and lmrisk.RiskCode=LCPol.RiskCode";		
	//}	
	//else
	// if(tIsCancelPolFlag=="1"){//销户保单查询
	   //strSQL = "select LBPol.grppolno,LBPol.PolNo,LBPol.PrtNo,LBPol.AppntName,LBPol.InsuredName,LBPol.RiskCode,LBPol.ManageCom,LBPol.Prem ,LBPol.Amnt ,LBPol.CValiDate,LBpol.AgentCode,substr(LBPol.PolState,1,2),substr(LBPol.PolState,1,4) from LBPolwhere LBPol.MainPolNo='" + tPolNo + "' and appflag<>'9' order byLBPol.proposalno";
	  // alert("销户保单查询"+strSQL);			
	//}
	//else
	//{
	 // alert("保单类型传输错误!");
	 // return ;
	//}
	//查询SQL，返回结果字符串
   turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
 
  //判断是否查询成功
  if (!turnPage2.strQueryResult) {
  	PayGrid.clearData();
  	alert("数据库中没有满足条件的险种信息！");
 
    return false;
  }
 
  //查询成功则拆分字符串，返回二维数组
  turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult);
 
  //设置初始化过的MULTILINE对象
  turnPage2.pageDisplayGrid = PayGrid;   
         
  //保存SQL语句
  turnPage2.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage2.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //setInvalidate(); 
  var arrDataSet = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage2.pageDisplayGrid);
}  

/*********************************************************************
 *  返回上一页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */  
function returnparent()
{  
	if ( tLogFlag==1)
	{
		window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + tContNo +"&IsCancelPolFlag=0"); 
	}else  if ( tLogFlag==2)
	{
		//parent.fraInterface.window.open("../sys/ContInsuredInput.jsp?cInsuredNo=" + tInsuredNo + "&ContNo=" + tContNo);
	}
	
}

