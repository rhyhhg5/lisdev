<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ScanContInit.jsp
//程序功能：工单管理工单查看页面
//创建日期：2005-01-19 15:52:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
var WorkNo = "<%=request.getParameter("WorkNo")%>";

//初始化表单
function initForm()
{
	initTaskInfo();     //初始化工单信息，在TaskCommonViewInit.jsp页面定义
	initCustomerGrid(); //初始化客服信息，在TaskCommonCustomerInit.jsp页面定义
	initHistoryGrid();  //初始化作业历史信息，在TaskCommonHistoryInit.jsp页面定义
	initGrid();

}

//保单信息列表的初始化
function initGrid()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="扫描件类型";         	  //列名
		iArray[1][1]="60px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="扫描机构";              //列名
		iArray[2][1]="80px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="扫描人";              //列名
		iArray[3][1]="100px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="扫描日期";         	//列名
		iArray[4][1]="70px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		ScanGrid = new MulLineEnter("fm", "ScanGrid"); 
		//这些属性必须在loadMulLine前
		ScanGrid.mulLineCount = 10;
		ScanGrid.displayTitle = 1;
		ScanGrid.locked = 1;
		ScanGrid.canSel = 1;
		ScanGrid.canChk = 0;
		ScanGrid.hiddenSubtraction = 1;
		ScanGrid.hiddenPlus = 1;
		ScanGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
	queryScanGrid();
}

</script>