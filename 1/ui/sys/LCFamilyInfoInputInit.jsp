<%
//程序名称：LCFamilyInfoInput.jsp
//程序功能：
//创建日期：2005-02-26 15:55:46
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('FamilyNo').value = "";
    fm.all('Remark').value = "";
    fm.all('HomeAddress').value = "";
    fm.all('HomeZipCode').value = "";
    fm.all('HomePhone').value = "";
    fm.all('HomeFax').value = "";
  }
  catch(ex)
  {
    alert("在LCFamilyInfoInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LCFamilyInfoInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox(); 
    initLCFamilyRelaInfoGrid();   
  }             
  catch(re)     
  {             
    alert("LCFamilyInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initLCFamilyRelaInfoGrid() {                               
  var iArray = new Array();
                     
  try {              
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="客户号";         		//列名
    iArray[1][1]="30px";         		//列名
    iArray[1][3]=1;         		//列名

    iArray[2]=new Array();
    iArray[2][0]="客户名称";         		//列名
    iArray[2][1]="30px";         		//列名
    iArray[2][3]=1;         		//列名
        
    iArray[3]=new Array();
    iArray[3][0]="家庭身份";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=1;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="层级关系";         		//列名
    iArray[4][1]="30px";         		//列名
    iArray[4][3]=1;         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="备注";         		//列名
    iArray[5][1]="30px";         		//列名
    iArray[5][3]=1;         		//列名                

    
    LCFamilyRelaInfoGrid = new MulLineEnter( "fm" , "LCFamilyRelaInfoGrid" ); 
    LCFamilyRelaInfoGrid.mulLineCount = 0;                   
    LCFamilyRelaInfoGrid.displayTitle = 1;                   
    LCFamilyRelaInfoGrid.hiddenPlus = 0;                     
    LCFamilyRelaInfoGrid.hiddenSubtraction = 0;
    LCFamilyRelaInfoGrid.canSel = 0;
    LCFamilyRelaInfoGrid.canChk = 0;
    LCFamilyRelaInfoGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
