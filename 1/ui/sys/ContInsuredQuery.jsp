<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = "";
	String tInsuredNo = "";
	String tLoadFlag = "";
	String tMoadFlag = "";
	String tContType = "";
	try
	{
		tContNo = request.getParameter("ContNo");
		tInsuredNo = request.getParameter("InsuredNo");
		tLoadFlag = request.getParameter("LoadFlag");
		tMoadFlag = request.getParameter("MoadFlag");
		tContType = request.getParameter("ContType");
	}
	catch( Exception e )
	{
		tContNo = "";
		tInsuredNo = "";
		tLoadFlag = "";
	}	
	
%>

<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   
<script>
	var tContNo = "<%=tContNo%>"; 
	var  tInsuredNo = "<%=tInsuredNo%>"; 
	var  tLoadFlag = "<%=tLoadFlag%>"; 
	var  tContType = "<%=request.getParameter("ContType")%>"; 
	var  tModeFlag = "<%=request.getParameter("ModeFlag")%>"; 
	
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="ContInsuredQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="ContInsuredQueryInit.jsp"%>
  <title>保单查询 </title>
   
</head>
<body  onload="initForm();sePrem();" >
  <form method=post name=fm target="fraSubmit" action="" >
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
		  <td class= titleImg align= center>被保人资料：</td>
		  <TD  class= title>
            客户属性
          </TD>
          <TD  class= input8>
            <Input class= common8 name=InsuredPro readonly>
          </TD> 
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
		  <TD  class= title>
            姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Name readonly>
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input8>
            <Input class="common8" name=Sex readonly>
          </TD> 
		  <TD  class= title>
                职业
           </TD>
           <TD  class= input >
                <Input class="common8" name="OccupationName"  readonly >
           </TD> 
		   <TD  class= title8>
                职业代码
           </TD>
           <TD  class= input8>
                <Input class="common8" name="OccupationCode" readonly >
          </TD>
        </TR>
        <TR  class= common>
		  <TD  class= title>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class="common8" name="IDType" value="0" readonly >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input8 >
            <Input class= common8 name="IDNo" readonly  >
          </TD>
		  <TD  class= title>
		    出生日期
          </TD>
          <TD  class= input>
          <input class="common8"  name="Birthday" readonly >
          </TD> 
		  <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class="common8" name="Marriage" elementtype=nacessary  readonly >
          </TD>    
        </TR>
       <TR  class= common>  
		  <TD  class= title>
            单位地址
          </TD>
          <TD  class= input >
            <Input class= common3 name="CompanyAddress" readonly >      
          </TD>          
          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= common8 name="CompanyZipCode" readonly >
          </TD>
		   <TD  class= title>
            工作单位
          </TD>
          <TD  class= input>
            <Input class= common8 name="GrpName" readonly >
          </TD>
           <TD  class= title>
                单位电话
           </TD>
           <TD  class= input8>
                <Input class= common8 name="CompanyPhone" verify="被保险人办公电话|len<=15" readonly>
           </TD> 
        </TR>                           
        <TR  class= common>
		<TD class=title>
			  家庭地址
			</TD>
			<TD class=input >
			  <Input name="HomeAddress" class=common8 readonly>
			</TD>
			<TD class=title>
			  邮编
			</TD>
			<TD class=input>
			  <Input name="HomeZipCode" class=common8 readonly>
		    </TD>	
		    <TD class=title>
	         住宅电话
	        </TD>
	        <TD class=input>
	          <Input name="HomePhone" class=common8 readonly>
	        </TD>   
		</TR>
		<TR>
           <TD  class= title>
            联系地址
          </TD>
          <TD  class= input>
           <input class="common8" name="NativePlace"  readonly>
          </TD>  
		  <TD  class= title>
                邮编
            </TD>
            <TD  class= input>
                <Input class= common8 name="ZipCode"  MAXLENGTH="6" verify="被保险人邮政编码|zipcode" >
            </TD>
           <TD  class= title>
                移动电话
            </TD>
            <TD  class= input>
                <Input class= common8 name="Mobile" verify="被保险人移动电话|len<=15" readonly >
            </TD>
            <TD  class= title>
                E-mail
            </TD>
            <TD  class= input>
                <Input class= common8 name="EMail" verify="被保险人电子邮箱|Email" readonly>
            </TD> 
        </TR>
    </table>
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFee);">
    		</td>
    		<td class= titleImg>
    			理赔帐号：
    		</td>
    	</tr>
    </table>
    <Div  id= "divFee" style= "display: ''">     
      <table class=three>    
        <TR  class= common>
          <TD  class= title8>
            银行
          </TD>
          <TD  class= input8>
            <Input class=common8 name=BankCode readonly >
          </TD>
          <TD  class= title8>
            账号
          </TD>
          <TD  class= input8>
            <Input class= common8 name=BankAccNo verify="帐号|len<=40" readonly>
          </TD> 
		  <TD  class= title8>
            户名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AccName verify="户名|len<=40" readonly>
          </TD>       
        </TR> 
       </table>   
    </Div>
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCClent);">
    		</td>
    		<td class= titleImg>
    			 体检信息：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCClent" style= "display: ''" >
      	<table  class=three>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPEGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<Div  id= "divLCButton" style= "display: ''" align = center>
		<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage3.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage3.previousPage();"> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage3.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage3.lastPage();"> 
		</div>
    </div>
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保障信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="getFirstPage();sePrem();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="getPreviousPage();sePrem();"> 		
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="getNextPage();sePrem();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="getLastPage();sePrem();"> 		
    </div>     
	<br>
	 <table class= common border=0 width=100%>
    	<tr>
		    <td><INPUT VALUE="险种明细" class = cssbutton TYPE=button onclick="riskQueryClick();"> </td>
		</tr>
	</table>
	
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFee);">
    		</td>
    		<td class= titleImg>
    			个人历史保额查询：
    		</td>
    	</tr>
    </table>
    <Div  id= "divFee" style= "display: ''">     
      <table class=three>    
        <TR  class= common>
          <TD  class= input8>
           <Input class="coolDatePicker" onblur="ceDate(this.value)" id=seStart name=seStart style="width:110" elementtype="nacessary"  dateFormat="short" verify="开始时间|notnull">
          </TD>
           <TD  class= title8>
            <INPUT VALUE="查  询" class = cssbutton TYPE=button  onclick="seMoney();">
          </TD>
           
          <TD  class= title8>
            <input name="h_grpcontno" type="hidden" value=<%=tContNo %> />
          </TD>
          <TD  class= input8>
            <input name="h_insuredno" type="hidden" value=<%=tInsuredNo %> />
          </TD>
			<TD  class= input8>
            <input name="h_conttype" type="hidden" value=<%=tContType %> />
          </TD>
           
        </TR> 
       </table>   
    </Div>
	
	
  </form>
<iframe name="fraSubmit" style="display:none"></iframe> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
