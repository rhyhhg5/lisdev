/** 
 * 程序名称：LOGrpPolDataInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2003-12-17 21:28:38
 * 创建人  ：yt
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
/*	
	var strSql = "select a.GetNoticeNo, a.otherno, b.name, a.PayDate, a.SumDuePayMoney, a.bankcode, a.bankaccno, a.accname "
	           + " from LJSPay a, ldperson b where a.appntno=b.customerno and OtherNoType='2' "
          	 + getWherePart("a.GetNoticeNo", "GetNoticeNo2")
          	 + getWherePart("a.OtherNo", "OtherNo")
          	 + getWherePart("a.BankCode", "BankCode")
          	 + getWherePart("a.SumDuePayMoney", "SumDuePayMoney");
  
  if (fm.AppntName.value != "") strSql = strSql + " and a.appntno in (select c.customerno from ldperson c where name='" + fm.AppntName.value + "')";
  if (fm.PrtNo.value != "") strSql = strSql + " and a.otherno in (select polno from lcpol where prtno='" + fm.PrtNo.value + "')";
*/  			
     
	var strSql = "select PolNo,PolType,PolCurYear,Rate,State,SpecExeDate,MakeDate from LOGrpPolData where 1=1 "
    + getWherePart("PolNo", "PolNo")
    + getWherePart("PolType", "PolType")
    + getWherePart("PolCurYear", "PolCurYear")
    + getWherePart("Rate", "Rate")
    + getWherePart("State", "State")
    + getWherePart("SpecExeDate", "SpecExeDate")
    + " order by makedate"
  ;
  //alert(strSql);
	turnPage.queryModal(strSql, LOGrpPolDataGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}


function returnParent()
{
        var arrReturn = new Array();
	var tSel = LOGrpPolDataGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();

				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	
	tRow = LOGrpPolDataGrid.getSelNo();
	if( tRow == 0 || tRow == null)
	{
	//{alert("111");
	return arrSelected;
	}
	//}
	//alert("222");	
	arrSelected = new Array();
	//设置需要返回的数组
//	arrSelected[0] = arrDataSet[tRow-1];
	tRow=tRow-1;
	arrSelected[0]=LOGrpPolDataGrid.getRowColData(tRow,1);
	arrSelected[1]=LOGrpPolDataGrid.getRowColData(tRow,2);
	arrSelected[2]=LOGrpPolDataGrid.getRowColData(tRow,3);
	arrSelected[3]=LOGrpPolDataGrid.getRowColData(tRow,4);
	arrSelected[4]=LOGrpPolDataGrid.getRowColData(tRow,5);
	arrSelected[5]=LOGrpPolDataGrid.getRowColData(tRow,6);
	return arrSelected;
}
