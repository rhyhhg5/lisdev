var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

/*********************************************************************
 *  查询个单客户险种信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function RiskValue() 
{
  var arrReturn1 = new Array();
  try {
    var strSQL="";
    if(tContType=="1") {
      strSQL = "select a.RiskCode,(select RiskName from LMRisk where RiskCode=a.RiskCode),a.InsuredName,a.polno from LcPol a where a.appflag='1' and a.conttype='1' and a.ContNo='"+tContNo+"'"
               +" union all "
               +"select a.RiskCode,(select RiskName from LMRisk where RiskCode=a.RiskCode),a.InsuredName,a.polno from LBPol a where a.appflag='1' and a.conttype='1' and a.ContNo='"+tContNo+"'";
   
    } else if(tContType=="2") {
      var strSQL = "select a.RiskCode,(select RiskName from LMRisk where RiskCode=a.RiskCode),a.InsuredName,a.polno from LBPol a where a.appflag='1' and a.conttype='1' and a.ContNo='"+tContNo+"'";
    }
    if (strSQL == "") {
      return false;
    } else {
      turnPage.queryModal(strSQL,RiskGrid);
    }
  } catch(ex) {
    alert("在PayHistoryInfoInit.jsp-->11setRiskValue函数中发生异常:初始化界面错误!");
  }

}
function HistoryRisk() {
	var tSel = RiskGrid.getSelNo();
  var cPolNo = RiskGrid.getRowColData( tSel - 1, 4 );
  var arrReturn1 = new Array();
   try {
    var strSQl="";
    strSQL ="select a.RiskCode,(select RiskName from LMRisk  where RiskCode=a.RiskCode),a.SumActuPayMoney,a.EnterAccDate,a.polno from LJAPayPerson a where a.polno='"+cPolNo+"'";
     if (strSQL == "") {
      return false;
    } else {
      turnPage.queryModal(strSQL,RiskExtendGrid);
    }
  } 
 /* */
  catch(ex) {
    alert("在PayHistoryInfoInit.jsp-->DetailedRisk函数中发生异常:初始化界面错误!");
  }
}

/*********************************************************************
 *  设置保单详细信息
 *  参数  ：  cArr 数组
 *  返回值：  无
 *********************************************************************
 */
function DetailedRisk()
{
	var tSel = RiskExtendGrid.getSelNo();
  var cPolNo = RiskExtendGrid.getRowColData( tSel - 1, 5 );
  var arrReturn1 = new Array();
try {
    var strSQl="";
    strSQL ="select a.RiskCode,(select RiskName from LMRisk where RiskCode=a.RiskCode),"
            +"a.InsuredName,'',b.paydate,b.paydate,a.SumPrem,a.paymode,a.paymode,"
            +"a.payintv,b.paycount,a.InsuYear,a.standprem,(select prem from lcprem where polno='"+cPolNo+"' and substr(payplancode,1,6)='000000'),(select Name from laagent where AgentCode=a.AgentCode) "
            +"from LCPol a,ljapayperson b where a.appflag='1' and a.conttype='1' and "
            +"a.polno='"+cPolNo+"' and a.polno=b.polno"
            +" union all "
            +"select a.RiskCode,(select RiskName from LMRisk where RiskCode=a.RiskCode),"
            +"a.InsuredName,'',b.paydate,b.paydate,a.SumPrem,a.paymode,a.paymode,"
            +"a.payintv,b.paycount,a.InsuYear,a.standprem,(select prem from lbprem where polno='"+cPolNo+"' and substr(payplancode,1,6)='000000'),(select Name from laagent where AgentCode=a.AgentCode) "
            +"from LBPol a,ljapayperson b where a.appflag='1' and a.conttype='1' and "
            +"a.polno='"+cPolNo+"' and a.polno=b.polno"
            ;

    if(strSQL != "") {
      arrReturn1 =easyExecSql(strSQL);
    }
    if (arrReturn1 == ""||arrReturn1 == null) {
      return false;
    } else {
      display(arrReturn1[0]);
     
    }
  }
  catch(ex) {
    alert("在PayHistoryInfoInit.jsp-->11setRiskValue函数中发生异常:初始化界面错误!");
  }
}
function display(cArr) {
	var tSel = RiskExtendGrid.getSelNo();
	var cPolNo = RiskExtendGrid.getRowColData( tSel - 1, 5 );                                                                                                                                                                                                                                                                           
  try {
    fm.all('RiskCode').value = cArr[0];
  } catch(ex) { }
  try {
    fm.all('RiskName').value = cArr[1];
  } catch(ex) { }
  try {
    fm.all('InsuredName').value = cArr[2];
  } catch(ex) { }
  try {
    fm.all('LCInsuredRelatedName').value = cArr[3];
  } catch(ex) { }
  try {
    fm.all('PayDate').value = cArr[4];
  } catch(ex) { }
  try {
    fm.all('PayDate1').value = cArr[5];
  } catch(ex) { }
  try {
    fm.all('SumPrem').value = cArr[6];
  } catch(ex) { }
  try {
    fm.all('PayMode1').value = cArr[7];
  } catch(ex) { }
  fm.all('PayMode1Name').value = easyExecSql("select codename from ldcode where  codetype='paymode' and code='"+cArr[7]+"'");
 try {
    fm.all('PayMode').value = cArr[8];
  } catch(ex) { }
  fm.all('PayModeName').value = easyExecSql("select codename from ldcode where  codetype='paymode' and code='"+cArr[8]+"'");
 try {
    fm.all('PayIntv').value = cArr[9];
  } catch(ex) { }
  fm.all('PayIntvName').value = easyExecSql("select codename from ldcode where  codetype='payintv' and code='"+cArr[9]+"'"); 
  	 try {
    fm.all('PayCount').value = cArr[10];
  } catch(ex) { }
  try {
    fm.all('InsuYear').value = cArr[11];
  } catch(ex) { } 
  	 try {
    fm.all('StandPrem').value = cArr[12];
  } catch(ex) { }
  	 try {
  	 	if(cArr[13]==null || cArr[13]=="null")
  			cArr[13] = "0"; 
    fm.all('InsuredRiskPrem').value = cArr[13];
  } catch(ex) { }
  	 try {
    fm.all('AgentName').value = cArr[14];
  } catch(ex) { }
}
