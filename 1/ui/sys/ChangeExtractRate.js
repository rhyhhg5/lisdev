//ChangExtractRate.js该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass();	//使用翻页功能，必须建立为全局变量

//查询所有信息
function initQuery()
{
	var strSql = " select a.riskcode,b.riskname,a.grppolno from lcgrppol a,lmriskapp b " +
				 " where a.riskcode=b.riskcode and (b.risktype4='3' or b.risktype4='4')" +
				 " and a.grpcontno = '"+ tGrpContNo +"'union select a.riskcode,b.riskname,a.grppolno from lbgrppol a,lmriskapp b " +
				 " where a.riskcode=b.riskcode and (b.risktype4='3' or b.risktype4='4')" +
				 " and a.grpcontno = '"+ tGrpContNo +"'";
	turnPage.queryModal(strSql, RiskInfoGrid);
}

//提交提取保费费率的修改
function Submit()
{  
	if(RiskInfoGrid.getSelNo() == 0)
	{
		alert("请选择一条记录");
	}
	else
	{
	var cRiskcode = RiskInfoGrid.getRowColData(RiskInfoGrid.getSelNo() - 1, 1);
	var strSql = "SELECT EXTRACTRATE FROM LMRISKZTFEE WHERE RISKCODE ='"+cRiskcode+"'";
	var arrResult = easyExecSql(strSql);

	for(var li=0;li<ChangeExtractRateGrid.mulLineCount;li++)
	{
		if(!isNumeric(ChangeExtractRateGrid.getRowColData(li,1))){
			alert("序号为"+(li+1)+"的'被保险人参保起始年期'是非法数字.");
			return false;
		}
		if(!isNumeric(ChangeExtractRateGrid.getRowColData(li,2))){
			alert("序号为"+(li+1)+"的'被保险人参保终止年期'是非法数字.");
			return false;
		}
		if(ChangeExtractRateGrid.getRowColData(li,1).length>ChangeExtractRateGrid.getRowColData(li,2).length){
			alert("序号为"+(li+1)+"的'被保险人参保起始年期'不能大于或等于'被保险人参保终止年期'.");
			return false;
		}
		if(ChangeExtractRateGrid.getRowColData(li,1)>=ChangeExtractRateGrid.getRowColData(li,2)){
			alert("序号为"+(li+1)+"的'被保险人参保起始年期'不能大于或等于'被保险人参保终止年期'.");
			return false;
		}
		if((ChangeExtractRateGrid.getRowColData(li,3))=='')
	    {
	       alert("第"+(li+1)+"行费用提取比例不能为空!");
	       return false;
	    }
	    if((ChangeExtractRateGrid.getRowColData(li,3)-1)>0||ChangeExtractRateGrid.getRowColData(li,3)<0)
	    {
	       if((li+1)==6){
	       		alert("第"+(li+1)+"行费用提取比例必须为0!");
	       }else{
	       		alert("第"+(li+1)+"行费用提取比例必须大于0小于"+arrResult[li][0]+"！");
	       }	       
	       return false;
	    }
	    if(ChangeExtractRateGrid.getRowColData(li,3)>arrResult[li][0]){
	     alert("第"+(li+1)+"行费用提取比例不能大于"+arrResult[li][0]);
	       return false;
	    }
	}
		var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();	
	if( FlagStr == "Fail" )
	{             
		content = "保存失败！";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;   
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	}
	initForm();
}

//Mulline中单选钮响应事件
function ShowExtratRate()
{
	var cRiskcode = RiskInfoGrid.getRowColData(RiskInfoGrid.getSelNo() - 1, 1);
	var cGrppolno = RiskInfoGrid.getRowColData(RiskInfoGrid.getSelNo() - 1, 3);
	fm.GrpPolNo.value = cGrppolno;
	var strSql = "SELECT beginPOLYEAR,endPolYear,EXTRACTRATE,'已保存' FROM LCRISKZTFEE WHERE GRPPOLNO = '"+ cGrppolno +"'union SELECT beginPOLYEAR,endPolYear,EXTRACTRATE,'已保存' FROM LbRISKZTFEE WHERE GRPPOLNO = '"+ cGrppolno +"'"
	var arrResult = easyExecSql(strSql);
	if(arrResult != null)
	{
		turnPage.queryModal(strSql, ChangeExtractRateGrid);
	}
	else
	{
		var strSql = "SELECT beginPOLYEAR,endPolYear,EXTRACTRATE ,'未保存' FROM LMRISKZTFEE WHERE RISKCODE ='"+cRiskcode+"'";
		turnPage.queryModal(strSql, ChangeExtractRateGrid);
	}
	if(scantype=="scan")
  {
    setFocus();
  } 
}
function returnparent(){

mSwitch = parent.VD.gVSwitch; 
top.fraInterface.window.location = "./ContPolInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+LoadFlag+"&scantype="+scantype+"&polNo="+mSwitch.getVar( "GrpContNo" )+"&ContType="+cContType;
//alert(top.fraInterface.window.location+"====123");

}
