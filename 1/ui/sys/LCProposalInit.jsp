<%
//程序名称：LCProposalInput.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
   if(checktype=="1")
   {
     param="13";
     fm.pagename.value="13";     
   }
   if(checktype=="2")
   {
     param="23";
     fm.pagename.value="23";     
   } 
  }
  catch(ex)
  {
    alert("在LCProposalInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                      

//初始化界面
function initForm() {
	try	{ 	
		initBnfGrid();
		initRiskGrid();
		initPayGrid();
		initAddPremGrid();
		initSpecGrid();
		
		getRiskQuery();
		queryRisk();
		queryAddPrem();
		querySpecGrid();
		queryBnf();
		queryPayGrid();
		queryPay();
		//zhanggm 071207
		queryMoney();
	} catch(ex) {
	}
}


// 受益人信息列表的初始化
function initBnfGrid() {                               
  var iArray = new Array();

  try {
    iArray[0]=new Array();
    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";		//列宽
    iArray[0][2]=10;			//列最大值
    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许
  
    iArray[1]=new Array();
    iArray[1][0]="受益人类别"; 		//列名
    iArray[1][1]="90px";		//列宽
    iArray[1][2]=40;			//列最大值
    iArray[1][3]=0;			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="BnfType";
    iArray[1][9]="受益人类别|notnull&code:BnfType";
   
    iArray[2]=new Array();
    iArray[2][0]="姓名"; 	//列名
    iArray[2][1]="120px";		//列宽
    iArray[2][2]=30;			//列最大值
    iArray[2][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[2][9]="姓名|len<=20";//校验
  
    iArray[3]=new Array();
    iArray[3][0]="证件类型"; 		//列名
    iArray[3][1]="90px";		//列宽
    iArray[3][2]=40;			//列最大值
    iArray[3][3]=0;			//是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="IDType";
    iArray[3][9]="证件类型|code:IDType";
  
    iArray[4]=new Array();
    iArray[4][0]="证件号码"; 		//列名
    iArray[4][1]="150px";		//列宽
    iArray[4][2]=80;			//列最大值
    iArray[4][3]=0;			//是否允许输入,1表示允许，0表示不允许
    iArray[4][9]="证件号码|len<=20";

    iArray[5]=new Array();
    iArray[5][0]="与被保人关系"; 	//列名
    iArray[5][1]="90px";		//列宽
    iArray[5][2]=60;			//列最大值
    iArray[5][3]=0;			//是否允许输入,1表示允许，0表示不允许
    iArray[5][4]="Relation";
    iArray[5][9]="与被保人关系|code:Relation";
  
    iArray[6]=new Array();
    iArray[6][0]="受益比例"; 		//列名
    iArray[6][1]="90px";		//列宽
    iArray[6][2]=40;			//列最大值
    iArray[6][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[6][9]="受益比例|num&len<=10";
  
    iArray[7]=new Array();
    iArray[7][0]="受益顺序"; 		//列名
    iArray[7][1]="90px";		//列宽
    iArray[7][2]=40;			//列最大值
    iArray[7][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[7][4]="OccupationType";
    iArray[7][9]="受益顺序|code:OccupationType";
  
    iArray[8]=new Array();
    iArray[8][0]="住址（填序号）"; 		//列名
    iArray[8][1]="160px";		//列宽
    iArray[8][2]=100;			//列最大值
    iArray[8][3]=3;			//是否允许输入,1表示允许，0表示不允许
    iArray[8][9]="住址|len<=80";
  
  
    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" ); 
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 0; 
    BnfGrid.displayTitle = 1;
    BnfGrid.locked = 1;
    BnfGrid.loadMulLine(iArray); 
  
    //这些操作必须在loadMulLine后面
    //BnfGrid.setRowColData(0,8,"1");
    //BnfGrid.setRowColData(0,9,"1");
  } catch(ex) {
    alert("在LCProposalInit.jsp-->initBnfGrid函数中发生异常:初始化界面错误!");
  }
}

// 告知信息列表的初始化
function initRiskGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="险种序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="30px";            		//列宽
    iArray[1][2]=10;            			//列最大值
    iArray[1][3]=0; 

    iArray[2]=new Array();
    iArray[2][0]="生效日期";         		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=60;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="保险期间(年)";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="缴费年期(年)";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=0; 

    iArray[5]=new Array();
    iArray[5][0]="险种名称";         		//列名
    iArray[5][1]="120px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="险种代码";         		//列名
    iArray[6][1]="120px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=1;  
    iArray[6][3]="RiskCode"; 

    iArray[7]=new Array();
    iArray[7][0]="保额";         		//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=90;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="Amnt"; 

    iArray[8]=new Array();
    iArray[8][0]="档次";         		//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=150;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="Mult"; 
    
    iArray[9]=new Array();
    iArray[9][0]="保费";         		//列名
    iArray[9][1]="80px";            		//列宽
    iArray[9][2]=90;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[10]=new Array();
    iArray[10][0]="附加保费";         		//列名
    iArray[10][1]="80px";            		//列宽
    iArray[10][2]=150;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[11]=new Array();
    iArray[11][0]="标准保费";         		//列名
    iArray[11][1]="80px";            		//列宽
    iArray[11][2]=90;            			//列最大值
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="原因";         		//列名
    iArray[12][1]="100px";            		//列宽
    iArray[12][2]=90;            			//列最大值
    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[13]=new Array();
    iArray[13][0]="险种号";         		//列名
    iArray[13][1]="100px";            		//列宽
    iArray[13][2]=90;            			//列最大值
    iArray[13][3]=0;
    iArray[13][3]="PolNo";

    RiskGrid = new MulLineEnter( "fm" , "RiskGrid" ); 
    //这些属性必须在loadMulLine前
    RiskGrid.mulLineCount = 0;   
    RiskGrid.displayTitle = 1;
    RiskGrid.locked = 1;
    RiskGrid.canSel = 1;
    RiskGrid.selBoxEventFuncName = "setPolValue";

    //RiskGrid.tableWidth   ="500px";
    RiskGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
    //RiskGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert("在LCProposalInit.jsp-->initRiskGrid函数中发生异常:初始化界面错误!");
  }
}

// 加费信息列表的初始化
function initAddPremGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="加费金额";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=10;            			//列最大值
    iArray[1][3]=0; 

    iArray[2]=new Array();
    iArray[2][0]="加费比例";         		//列名
    iArray[2][1]="60px";            		//列宽
    iArray[2][2]=60;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="加费起始日期";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="加费终止日期";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=0; 

    AddPremGrid = new MulLineEnter( "fm" , "AddPremGrid" ); 
    //这些属性必须在loadMulLine前
    AddPremGrid.mulLineCount = 0;   
    AddPremGrid.displayTitle = 1;
    AddPremGrid.locked = 1;
    AddPremGrid.canSel = 1;
    AddPremGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert("在LCProposalInit.jsp-->initAddPremGrid函数中发生异常:初始化界面错误!");
  }
}

// 加费信息列表的初始化
function initSpecGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="加费金额";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=10;            			//列最大值
    iArray[1][3]=0; 

    iArray[2]=new Array();
    iArray[2][0]="加费比例";         		//列名
    iArray[2][1]="60px";            		//列宽
    iArray[2][2]=60;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="加费起始日期";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="加费终止日期";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=0; 

    SpecGrid = new MulLineEnter( "fm" , "SpecGrid" ); 
    //这些属性必须在loadMulLine前
    SpecGrid.mulLineCount = 0;   
    SpecGrid.displayTitle = 1;
    SpecGrid.locked = 1;
    SpecGrid.canSel = 1;
    SpecGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert("在LCProposalInit.jsp-->initAddPremGrid函数中发生异常:初始化界面错误!");
  }
}

// 告知信息列表的初始化
function initPayGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="受理时间";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      

      iArray[2]=new Array();
      iArray[2][0]="结案时间";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="发生时间";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="理赔时间";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=150;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      PayGrid = new MulLineEnter( "fm" , "PayGrid" ); 
      //这些属性必须在loadMulLine前
      PayGrid.mulLineCount = 0;   
      PayGrid.displayTitle = 1;
	    PayGrid.locked = 1;
      PayGrid.loadMulLine(iArray);  
    }
    catch(ex) {
      alert("在LCProposalInit.jsp-->initRiskGrid函数中发生异常:初始化界面错误!");
    }
}

</script>