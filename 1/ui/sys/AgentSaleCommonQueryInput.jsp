<%
//程序名称：AgentQueryInput.jsp
//程序功能：
//创建日期：2003-04-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
 	String tManageCom = "";
	try
	{
		tManageCom = request.getParameter( "ManageCom" );
		System.out.println("---tManageCom:"+tManageCom);
	}
	catch( Exception e1 )
	{
			System.out.println("---Exception:"+e1);
	}
%>

<script>
	var ManageCom = "<%= tManageCom%>";
	var AgentCom="<%=request.getParameter("AgentCom")%>";	
	var specFlag="<%=request.getParameter("specFlag")%>";	
	
</script>	
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./AgentSaleCommonQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <%@include file="./AgentSaleCommonQueryInit.jsp"%>
  <title>业务员查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./AgentCommonQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--业务员查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            </td>
            <td class= titleImg>
                代理销售业务员查询条件
            </td>            
    	</tr>
    </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <TD class= title>  管理机构 </TD>
        <TD class= input> 
        	<Input name=ManageCom class=codeNo id="ManageCom" verify="管理机构|code:comcode" 
        	ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,null,null,1);" 
        	onkeyup="return showCodeListKey('ComCode', [this, ComName], [0, 1],null,null,null,1);"
        	><input class=codename name=ManageComName readonly=true >   
        </TD>
        <TD class= title>   中介机构  </TD>
        <TD class= input> <Input class=common name=AgentCom readonly=true>  </TD>
        <TD class= title></TD>
        <TD class= input></TD>
      </TR>
      <TR  class= common> 
        <TD  class= title> 业务员代码 </TD>
        <TD  class= input>   <Input name=AgentCode class= common>  </TD>
        <TD class= title></TD>
        <TD class= input></TD>
        <TD class= title></TD>
        <TD class= input></TD>
      </TR>
      <TR>
        <TD  class= title> 姓名 </TD>
        <TD  class= input>   <Input name=Name class= common verify="姓名|len<=12">  </TD>
        <TD  class= title> 性别  </TD>
        <TD  class= input> <Input name=Sex class=codeNo MAXLENGTH=1 verify="性别|code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" ><input class=codename name=SexName readonly=true > </TD>
        <TD  class= title>  证件号码  </TD>
        <TD  class= input>   <Input name=IDNo class= common></TD>
      </TR>
   
    </table>
          
          <!--INPUT class=common VALUE="查询" TYPE=button onclick="easyQueryClick();"--> 
	   <table> 
		    <tr>
		    <td><INPUT class=cssbutton VALUE="查  询" TYPE=button onclick="easyQueryClick();"> </td>
		    <td><INPUT class=cssbutton VALUE="返  回" TYPE=button onclick="returnParent();"> 	</td>
		    </tr> 
	   </table> 
    </Div>      
          				
    <table>
    	<tr>
        <td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 结果清单
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''" align =center>
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" align=center>
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table>
    		<tr>
    			<td>
			      <INPUT class=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
			    </td>
			    <td>  
			      <INPUT class=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    </td>
			    <td> 			      
			      <INPUT class=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    </td>
			    <td> 			      
			      <INPUT class=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 						
			    </td>  			
  			</tr>
  		</table>
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
