<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：DoubleContListInput.jsp
//程序功能：双录保单清单报表
//创建时间：2017-11-1
//创建人  ：yangjian
%>


<head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="DoubleContListInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="DoubleContListInit.jsp"%> 
</head>

<body  onload="initForm();initElementtype();" >    
  <form action=""  method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		<td class= titleImg>查询条件</td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR  class= common> 
      	  <TD  class= title> 管理机构</TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="机构管理|NOTNULL" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"
            ><Input class ='codename' name ='ManageComName' elementtype=nacessary>
          </TD>
          
          <TD class=title> 销售渠道</TD>
		  <TD class=input>
		  	<Input class='codeno' name=Salechnl
		    ondblclick="return showCodeList('LCSaleChnl',[this,SalechnlName],[0,1]);"
			onkeyup="return showCodeListKey('LCSaleChnl',[this,SalechnlName],[0,1]);"
			readonly><Input class=codename name=SalechnlName >
	     </TD>       

        </TR>
        
		<TR class=common>
          <TD  class= title > 代理人编码</TD>
           <TD  class= input>
            <Input class=common name=AgentCode  >
          </TD>
         
          <TD  class= title > 代理人姓名</TD>
          <TD  class= input>
            <Input class=common name=Name  >
          </TD>
        </TR>
        
		<TR class="common">
			<TD class="title">保单申请起期</TD>
			<TD class="input">
				<input class="coolDatePicker" dateFormat="short" name="SignStartDate" verify="保单申请起期|date" >
			</TD>
			
			<TD class="title">保单申请止期</TD>
			<TD class="input">
				<input class="coolDatePicker" dateFormat="short" name="SignEndDate" verify="保单申请止期|date" >
			</TD>
		</TR>
      </table>
	</Div>
	
	<table>
  	  <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="DoubleContListQuery();">
     		<input type=button value="下  载" class=cssButton onclick="DoubleContListDownload();">

    	</td>
   
      </tr>      
  	</table> 
	
      <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentQuery);">
    		</td>
    		<td class= titleImg>查询结果</td>   		 
    	</tr>
    </table>
    <Div  id= "divAgentQuery" style= "display: ''">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanDoubleContListGrid" ></span> 
  			</TD>
      </TR>
     </Table>	
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
   </Div>
   
   
   <input type=hidden class=Common name=querySql > 
   	
  </form>   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
<script>
  var mcodeSql = "#1# and (code = #2# or code = #3#)";
</script>

