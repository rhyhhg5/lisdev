//该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}
//提交，保存按钮对应操作
function DoInsert()
{
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
// 查询按钮
function easyQueryClick()
{
	//alert ("start easyQueryClick");
	// 初始化表格
	initSetGrid();
	if( verifyInput() == false ) return false;
	if(!checkData()){
		return false;
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.ManageCom"
		+",(select name from ldcom where comcode=a.managecom)"
		+",a.grpcontno,a.prtno"
		+",(select distinct codename from ldcode where (codetype='salechnl' or codetype='lcsalechnl') and code=a.salechnl)"
		+",(select distinct codename from ldcode where codetype='markettype' and code=a.markettype)"
		+",getUniteCode(a.agentcode),(select name from laagent where agentcode=a.agentcode)"
		+",(select branchattr from labranchgroup where agentgroup=a.agentgroup)"
		+",(select name from labranchgroup where agentgroup=a.agentgroup)"
		+",a.grpname,a.polapplydate,a.inputdate,a.cvalidate,a.cinvalidate"
		+",a.signdate"
		+",(select codename from ldcode where code=a.paymode and codetype='paymode')"
		+",(select codename from ldcode where code=char(a.payintv) and codetype='payintv')"
		+",(case when a.payintv = -1 then (select sum(prem) from lcgrppayplan where prtno = a.prtno) else ( case when a.appflag = '1' then (select nvl(sum(sumactupaymoney),0) from ljapay where incomeno = a.grpcontno and duefeetype = '0') else a.prem end) end) "
		+",a.peoples2"
		+",case when a.uwflag = 'a' then '已撤销' when a.uwflag = '1' then '拒保' else (select codename from ldcode where code=a.appflag and codetype='appflag') end "
		+",case when a.appflag = '0' then '0' else varchar((select nvl(sum(sumactupaymoney),0) from ljapay where incomeno = a.grpcontno and duefeetype = '0')) end "
		+",case when a.appflag = '0' then '0' else varchar((select (select nvl(sum(sumactupaymoney),0) from ljapay where incomeno = a.grpcontno) + (select nvl(sum(getmoney),0) from ljagetendorse where grpcontno = a.grpcontno and feeoperationtype<>'MJ' ) from dual)) end "
		+",(select bigprojectno from lcbigprojectcont where prtno=a.prtno)"
		+",(select bigprojectname from lcbigprojectinfo where bigprojectno = (select bigprojectno from lcbigprojectcont where prtno=a.prtno))"
		+ " from lcgrpcont a "
		+ " where  1=1 "
		+ getWherePart('a.ManageCom', 'ManageCom', 'like')
		+ getWherePart('a.InputDate', 'StartDate', '>=')
		+ getWherePart('a.InputDate', 'EndDate', '<=')
		+ getWherePart('a.CValiDate', 'StartCvaliDate', '>=')
		+ getWherePart('a.CValiDate', 'EndCvaliDate', '<=')
		+ getWherePart('a.SignDate', 'StartSignDate', '>=')
		+ getWherePart('a.SignDate', 'EndSignDate', '<=')
		+ getWherePart('getUniteCode(a.agentcode)','AgentCode')
		+" and not exists (select 'X' from lcrnewstatelog where newgrpcontno = a.grpcontno)"
		+" Order by a.managecom ";
	fm.querySql.value = strSQL;
    turnPage.queryModal(strSQL, SetGrid);
    if(SetGrid.mulLineCount==0){
    	alert("没有查询到任何满足条件的数据！");
    	return false;
 	}
    
}
function DoNewDownload()
{
	if( verifyInput() == false ) return false;
 // 书写SQL语句
	if(fm.querySql.value != null && fm.querySql.value != "" && SetGrid.mulLineCount > 0)
    {
        fm.action = "GrpCDQListReport1.jsp";
    	fm.submit();
    }
    else
    {
        alert("请先执行查询操作，再下载！");
        return ;
    }
}					
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}
	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
		{
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
		}
	//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}
		//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
		{
			if (confirm("您确实想修改该记录吗?"))
			{
				fm.fmAction.value = "UPDATE";
				if(!beforeSubmit()) return false;
				var i = 0;
				var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.submit(); //提交
			}
			else
				{
					fm.OperateType.value = "";
					alert("您取消了修改操作！");
				}
			}
							//判断是否选择了要增加、修改或删除的行
function chkMulLine()
		{
										return true;
									}
									//提交前的校验、计算
function beforeSubmit()
									{
									  if(!SetGrid.checkValue("SetGrid")) return false;
										if(!chkMulLine()) return false;
										return true;
									}

function showDiv(cDiv,cShow)
									{
										if (cShow=="true")
										{
											cDiv.style.display="";
										}
										else
											{
												cDiv.style.display="none";
											}
									}
function diskImport()
{ 
    //alert("111111111111111");
		//alert("====="+fm.all("diskimporttype").value);
   var strUrl = "../agentconfig/DiskImportRateComminsionMain.jsp?diskimporttype="+fm.all("diskimporttype").value+"&branchtype="+fm.all("BranchType").value+"&branchtype2="+fm.all("branchtype2").value;
   showInfo=window.open(strUrl,"佣金率信息导入","width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=yes");
}
function moduleDownload(){
	var tSQL="";
	fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "RateCommisionModule.jsp";
    fm.submit();
    fm.action = oldAction;
}
//校验查询条件
function checkData()
{
	var tStartDate = fm.all('StartDate').value;
	var tEndDate = fm.all('EndDate').value;
	var tStartCValiDate = fm.all('StartCValiDate').value;
	var tEndCValiDate = fm.all('EndCValiDate').value;
	var tStartSignDate = fm.all('StartSignDate').value;
	var tEndSignDate = fm.all('EndSignDate').value;
	
	if((tStartDate == null || tStartDate == "") && (tEndDate == null || tEndDate == "")
		&&(tStartCValiDate == null || tStartCValiDate == "")&&(tEndCValiDate == null || tEndCValiDate == "")
		&&(tStartSignDate == null || tStartSignDate == "")&&(tEndSignDate == null || tEndSignDate == ""))
	{
		alert("录入日期起止、生效日期起止和签单日期起止三组条件，必须填写其中一组才能进行查询！");
		return false;
	}
	
	if((tStartDate != null && tStartDate != "") 
		&& (tEndDate == null || tEndDate == ""))
	{
		alert("请选择录入日期止期！");
		return false;
	}
	
	if((tStartDate == null || tStartDate == "") 
		&& (tEndDate != null && tEndDate != ""))
	{
		alert("请选择录入日期起期！");
		return false;
	}
	
	// 因原有报表无时间段校验，经与IT商定，暂时不加时间段校验处理。
	//后查询时间太长，增加时间校验。3个时间有一个在12个月内即可
	var tStartDateFlag = false;
	if((tStartDate != null && tStartDate != "") 
		&& (tEndDate != null && tEndDate != ""))
	{
		if(dateDiff(tStartDate,tEndDate,"M") > 12)
		{
			tStartDateFlag = true;
		}
	}
	
	if((tStartCValiDate != null && tStartCValiDate != "") 
		&& (tEndCValiDate == null || tEndCValiDate == ""))
	{
		alert("请选择生效日期止期！");
		return false;
	}
	
	if((tStartCValiDate == null || tStartCValiDate == "") 
		&& (tEndCValiDate != null && tEndCValiDate != ""))
	{
		alert("请选择生效日期起期！");
		return false;
	}
	
	// 因原有报表无时间段校验，经与IT商定，暂时不加时间段校验处理。
	var tStartCValiDateFlag = false;
	if((tStartCValiDate != null && tStartCValiDate != "") 
		&& (tEndCValiDate != null && tEndCValiDate != ""))
	{
		if(dateDiff(tStartCValiDate,tEndCValiDate,"M") > 12)
		{
			tStartCValiDateFlag = true;
		}
	}
	
	if((tStartSignDate != null && tStartSignDate != "") 
		&& (tEndSignDate == null || tEndSignDate == ""))
	{
		alert("请选择签单日期止期！");
		return false;
	}
	
	if((tStartSignDate == null || tStartSignDate == "") 
		&& (tEndSignDate != null && tEndSignDate != ""))
	{
		alert("请选择签单日期起期！");
		return false;
	}
	
	// 因原有报表无时间段校验，经与IT商定，暂时不加时间段校验处理。
	var tStartSignDateFlag = false;
	if((tStartSignDate != null && tStartSignDate != "") 
		&& (tEndSignDate != null && tEndSignDate != ""))
	{
		if(dateDiff(tStartSignDate,tEndSignDate,"M") > 12)
		{
			tStartSignDateFlag = true;
		}
	}
	
	if(tStartDateFlag && tStartCValiDateFlag && tStartSignDateFlag){
		alert("选择的时间段，必须有一个时间段在12个月内！");
		return false;
	}
	
	if(tStartDate == null || tStartDate == ""){
		if(tStartCValiDate == null || tStartCValiDate == ""){
			if(tStartSignDateFlag){
				alert("查询时间间隔最长为十二个月！");
				return false;
			}
		}else{
			if(tStartSignDate == null || tStartSignDate == ""){
				if(tStartCValiDateFlag){
					alert("查询时间间隔最长为十二个月！");
					return false;
				}
			}else{
				if(tStartCValiDateFlag && tStartSignDateFlag){
					alert("选择的时间段，必须有一个时间段在十二个月内！");
					return false;
				}
			}
		}
	}else{
		if(tStartCValiDate == null || tStartCValiDate == ""){
			if(tStartSignDate == null || tStartSignDate == ""){
				if(tStartDateFlag){
					alert("查询时间间隔最长为十二个月！");
					return false;
				}
			}else{
				if(tStartDateFlag && tStartSignDateFlag){
					alert("选择的时间段，必须有一个时间段在十二个月内！");
					return false;
				}
			}
		}else{
			if(tStartSignDate == null || tStartSignDate == ""){
				if(tStartDateFlag && tStartCValiDateFlag){
					alert("选择的时间段，必须有一个时间段在十二个月内！");
					return false;
				}
			}
		}
	}
	
	return true;
}