//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAAgentQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
        var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
			try
			{	
				//alert("1");
				arrReturn = getQueryResult();
				//alert("2");
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult(ContNo,ContType)
{
	//alert(111);
	//alert("111" + ContNo +"/"+ContType);
	var arrSelected = new Array();
	var strSQL = "";
	if( ContType == 1)
	  {
			strSQL = "select a.agentcode,a.name,";
			strSQL += "case when  a.Sex='0' then '男'when  a.Sex='1' then '女' end ,";
			strSQL += "case when a.IDNoType = '0' then '身份证' when a.IDNoType = '1' then '护照'" 
			strSQL += "when a.IDNoType = '2' then '军官证' when a.IDNoType = '3' then '工作证' when a.IDNoType = '4' then '其他' end ,";
			strSQL += "a.IDNo,a.Phone,a.EmployDate,a.OutWorkDate,(select gradename from laagentgrade where laagentgrade.gradecode=c.agentgrade), ";
		  strSQL += " (select name from labranchgroup where agentgroup = a.AgentGroup) from laagent a,lccont b,latree c where a.agentcode = b.agentcode ";
		  strSQL += " and a.agentcode = c.agentcode and b.contno = '"+ContNo+"'";
		}
		else if (ContType ==2)
		{
		  strSQL = "select a.agentcode,a.name,";
			strSQL += "case when  a.Sex='0' then '男'when  a.Sex='1' then '女' end ,";
			strSQL += "case when a.IDNoType = '0' then '身份证' when a.IDNoType = '1' then '护照'" 
			strSQL += "when a.IDNoType = '2' then '军官证' when a.IDNoType = '3' then '工作证' when a.IDNoType = '4' then '其他' end ,";
			strSQL += "a.IDNo,a.Phone,a.EmployDate,a.OutWorkDate,(select gradename from laagentgrade where laagentgrade.gradecode=c.agentgrade), ";
		  strSQL += " (select name from labranchgroup where agentgroup = a.AgentGroup) from laagent a,lbcont b,latree c where a.agentcode = b.agentcode ";
		  strSQL += " and a.agentcode = c.agentcode and b.contno = '"+ContNo+"'";
		}
		//alert(strSQL);
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
        //判断是否查询成功
       if (!turnPage.strQueryResult) {
         alert("查询失败！");
         return false;
       }
       
       var arr = decodeEasyQueryResult(turnPage.strQueryResult);
	
	   fm.all('AgentCode').value = trim(arr[0][0]);
	   fm.all('Name').value = trim(arr[0][1]);
	   fm.all('Sex').value = trim(arr[0][2]);
	   fm.all('IDNoStyle').value = trim(arr[0][3]);
	   fm.all('IDNo').value = trim(arr[0][4]);
	   fm.all('PhoneNo').value = trim(arr[0][5]);
	   fm.all('EmployeeDate').value = trim(arr[0][6]);
	   fm.all('DimissionDate').value = trim(arr[0][7]);
	   fm.all('AgentGrade').value = trim(arr[0][8]);
	   fm.all('AgentGroup').value = trim(arr[0][9]);
	  
       //查询成功则拆分字符串，返回二维数组
       //arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	//return arrSelected;
}

function getvalue(arr)



// 查询按钮
//function easyQueryClick()
{
	// 初始化表格
	initAgentGrid();
	//var tReturn = getManageComLimitlike("a.managecom");
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.agentcode,a.name,"
	         +"case when  a.Sex='0' then '男' " 
		       +"when  a.Sex='1' then '女' end,"
	         +"a.idno,b.BranchAttr from LAAgent a,LABranchGroup b,LATree c where 1=1 "
	         + "and (a.AgentState is null or a.AgentState < '03') and a.BranchCode = b.agentGroup and c.agentcode=a.agentcode and (b.state<>'1' or b.state is null)"
	         + getWherePart('a.AgentCode','AgentCode','like')
	         + getWherePart('b.BranchAttr','AgentGroup','like')
	         //+ getWherePart('a.ManageCom','ManageCom','like')
	         + getWherePart('a.Name','Name')
	         //+ getWherePart('a.Password','Password')
	         //+ getWherePart('a.EntryNo','EntryNo')
	         + getWherePart('a.Sex','Sex')
	         //+ getWherePart('a.Birthday','Birthday')
	         + getWherePart('a.IDNo','IDNo')
	         //+ getWherePart('a.NativePlace','NativePlace')
	         //+ getWherePart('a.Nationality','Nationality')
	         //+ getWherePart('a.Source','Source')
	         //+ getWherePart('a.BloodType','BloodType')
	         //+ getWherePart('a.Marriage','Marriage')
	         //+ getWherePart('a.MarriageDate','MarriageDate')
	         //+ getWherePart('HomeAddressCode')
	         //+ getWherePart('HomeAddress')
	         //+ getWherePart('a.PostalAddress','PostalAddress')
	         //+ getWherePart('c.IntroAgency','IntroAgency')
	         //+ getWherePart('a.Phone','Phone')
	         //+ getWherePart('c.AgentGrade','AgentGrade')
	         //+ getWherePart('a.Mobile','Mobile')
	         //+ getWherePart('a.EMail','EMail')
	         //+ getWherePart('a.TrainPeriods','TrainPeriods')
	         //+ getWherePart('a.RecommendAgent','RecommendAgent')
	         //+ getWherePart('a.Business','Business')
	         //+ getWherePart('a.CreditGrade','CreditGrade')
	         //+ getWherePart('a.SaleQuaf','SaleQuaf')
	         //+ getWherePart('a.QuafNo','QuafNo')
	         //+ getWherePart('a.DevNo1','DevNo1')
	         //+ getWherePart('a.EmployDate','EmployDate')
	         ///+ getWherePart('a.BranchType','BranchType')
	         ///+ getWherePart('a.BranchType2','BranchType2');
	         //+ getWherePart('PolityVisage')
	         //+ getWherePart('Degree')
	         //+ getWherePart('GraduateSchool')
	         //+ getWherePart('Speciality')
	         //+ getWherePart('PostTitle')
	         //+ getWherePart('ForeignLevel')
	         //+ getWherePart('WorkAge','','','1')
	         //+ getWherePart('PolityVisage')
	         //+ getWherePart('Operator');		
     	//alert(strSQL); 
     	turnPage.queryModal(strSQL, AgentGrid); 
//	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
//  
//  //判断是否查询成功
//  if (!turnPage.strQueryResult) {
//    alert("查询失败！");
//    return false;
//}
//
////查询成功则拆分字符串，返回二维数组
//  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
//  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
//  turnPage.arrDataCacheSet = arrDataSet;
//  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
//  turnPage.pageDisplayGrid = AgentGrid;    
//          
//  //保存SQL语句
//  turnPage.strQuerySql     = strSQL; 
//  
//  //设置查询起始位置
//  turnPage.pageIndex       = 0;  
//  
//  //在查询结果数组中取出符合页面显示大小设置的数组
//  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
//  var tArr = new Array();
//  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
//  //调用MULTILINE对象显示查询结果
//  
//  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
//  displayMultiline(tArr, turnPage.pageDisplayGrid);
}