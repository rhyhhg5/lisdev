<%
//程序名称：AgentQueryInput.jsp
//程序功能：
//创建日期：2003-04-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./AgentQuery1.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./AgentQueryInit1.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>代理人查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./AgentQuerySubmit.jsp" method=post name=fm target="fraSubmit">
     
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            </td>
            <td class= titleImg>
                代理人查询条件
            </td>            
    	</tr>
    </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          代理人编码 
        </TD>
        <TD  class= input> 
          <Input class=common  name=AgentCode >
        </TD>
        <TD class= title>
          销售机构 
        </TD>
        <TD class= input>
          <Input class=common name=AgentGroup > 
        </TD>       
         <TD class= title>
          管理机构
        </TD>
        <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
          </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD>      
         <TD  class= title>
          性别 
        </TD>
        <TD  class= input>
          <Input name=Sex class="code" MAXLENGTH=1 ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);" >
        </TD>
        
        <TD  class= title>
          身份证号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common > 
        </TD>
      </TR>
     
     <TR>
     <TD class= title>
          代理人职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="code"  verify="代理人职级|notnull&code:AgentGrade" ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" > 
        </TD>
        <TD  class= title>
          推荐人 
        </TD>
        <TD  class= input> 
          <Input name=RecommendAgent class= common > 
        </TD>
        <TD  class= title>
          代理人状态 
        </TD>
        <TD  class= input> 
          <Input name=AgentState class='code'id="AgentState" ondblclick="return showCodeList('AgentState',[this]);" onkeyup="return showCodeListKey('AgentState',[this]);"  > 
        </TD>
     </TR>   
      <TR>
       <TD  class= title>
          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class="coolDatePicker" dateFormat="short" > 
        </TD>
        <TD  class= title width="25%">
  	    	起始日期
   	  </TD>
      <TD  class= input width="25%">
         <Input class= "coolDatePicker" dateFormat="short" name=StartDay >
       </TD>      
          <TD  class= title width="25%">
            结束日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=EndDay  >
          </TD> 
      	</TR>     
        <input type=hidden name=BranchType value=''>
    </table>          
	
	   <table> 
		    <tr>		
			    <td>
			      <INPUT class=common VALUE="查  询" TYPE=button onclick="easyQueryClick();"> 
			    </td>
			    <td>  
			      <INPUT class=common VALUE="代理人信息" TYPE=button onclick="returnParent();">  
			    </td>
		    </tr>  
		    <tr>
			    <td>
			      <INPUT class=common VALUE="投保单信息" TYPE=button onclick="ProposalClick();"> 
			    </td>
			    <td> 
			      <INPUT class=common VALUE="保单信息" TYPE=button onclick="PolClick();"> 
			    </td>
			    <td>  
			      <INPUT class=common VALUE="销户保单信息" TYPE=button onclick="DesPolClick();"> 
			    </td>
		    </tr> 
	   </table> 
    </Div>      
          				
    <table>
    	<tr>
        <td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 代理人结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table>
    		<tr>
    			<td>
			      <INPUT class=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			    </td>
			    <td>  
			      <INPUT class=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    </td>
			    <td> 			      
			      <INPUT class=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    </td>
			    <td> 			      
			      <INPUT class=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 						
			    </td>  			
  			</tr>
  		</table>
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
