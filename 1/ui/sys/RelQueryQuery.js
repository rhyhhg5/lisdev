//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();   

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
	var tAgentCode="";
	// 初始化表格
	initPersonGrid();
	try
	{
	  tAgentCode = fm.all('aAgentCode').value;
	}
	catch(ex)
	{
	}
//	alert(tAgentCode);
	// 书写SQL语句
	var strSQL = "";	
	
	strSQL = "select * from LDPerson where 1=1 "
	if (tAgentCode!=null&&tAgentCode!='')
	{
	strSQL = "select CustomerNo,Name,Sex,Birthday,IDType,IDNo from LDPerson where 1=1 "
				 + getWherePart( 'CustomerNo' )
				 + getWherePart( 'Name' )
				+ getWherePart( 'Sex' )
				 + getWherePart( 'Birthday' )
				 + getWherePart( 'IDType' )
				 + getWherePart( 'IDNo' )
				 + " and customerno in (select insuredno from lcpol where agentcode='"+tAgentCode+"' union select insuredno from lbpol where agentcode='"+tAgentCode+"')" ;
	}
	else
	{
		strSQL = "select CustomerNo,Name,Sex,Birthday,IDType,IDNo from LDPerson where 1=1 "
				 + getWherePart( 'CustomerNo' )
				 + getWherePart( 'Name' )
				+ getWherePart( 'Sex' )
				 + getWherePart( 'Birthday' )
				 + getWherePart( 'IDType' )
				 + getWherePart( 'IDNo' );
		}
		
		//alert(strSQL);
		turnPage.queryModal( strSQL, PersonGrid);
}

//选择页面上查询的字段对应于"select *"中的位置
function getSelArray()
{
	var arrSel = new Array();
	
//	arrSel[0] = 0;
//	arrSel[1] = 2;
//	arrSel[2] = 3;
//	arrSel[3] = 4;
//	arrSel[4] = 16;
//	arrSel[5] = 18;
	arrSel[0] = 0;
	arrSel[1] = 1;
	arrSel[2] = 2;
	arrSel[3] = 3;
	arrSel[4] = 4;
	arrSel[5] = 5;

	return arrSel;
	
}

//function displayEasyResult( arrQueryResult )
//{
//	var i, j, m, n;
//	var arrSelected = new Array();
//	var arrResult = new Array();
//
//	if( arrQueryResult == null )
//		alert( "没有找到相关的数据!" );
//	else
//	{
//		// 初始化表格
//		initPersonGrid();
//		PersonGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
//		PersonGrid.loadMulLine(PersonGrid.arraySave);
//
//		arrGrid = arrQueryResult;
//		// 转换选出的数组
//		arrSelected = getSelArray();
//		arrResult = chooseArray( arrQueryResult, arrSelected );
//		// 显示查询结果
//		n = arrResult.length;
//		for( i = 0; i < n; i++ )
//		{
//			m = arrResult[i].length;
//			for( j = 0; j < m; j++ )
//			{
//				PersonGrid.setRowColData( i, j+1, arrResult[i][j] );
//			} // end of for
//		} // end of for
//		//alert("result:"+arrResult);
//		
//		//PersonGrid.delBlankLine();
//	} // end of if
//}

function returnParent()
{
	tRow=PersonGrid.getSelNo();
  if (tRow==0)
  {
   	alert("请您先进行选择");
  	return;
  }
  tCol=1;
  tCustomerNo = PersonGrid.getRowColData(tRow-1,tCol);
	window.open("../sys/CQPersonMain.jsp?CustomerNo="+tCustomerNo);
}

// 投保单信息查询
function ProposalClick()
{
	var arrReturn = new Array();
	var tSel = PersonGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cCustomerNo = PersonGrid.getRowColData(tSel - 1,1);				
		
		if (cCustomerNo == "")
		    return;		    
		    var cName = PersonGrid.getRowColData(tSel - 1,2);
		    window.open("../sys/PolQueryMain.jsp?CustomerNo=" + cCustomerNo + "&Name=" + cName + "&Flag=Customer2");	
	}
}


// 保单信息查询
function PolClick()
{
	var arrReturn = new Array();
	var tSel = PersonGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cCustomerNo = PersonGrid.getRowColData(tSel - 1,1);				
		
		if (cCustomerNo == "")
		    return;		    
		  //alert(cCustomerNo);
		    var cName = PersonGrid.getRowColData(tSel - 1,2);
		    //alert(cName);
		    window.open("../sys/PolQueryMain.jsp?CustomerNo=" + cCustomerNo + "&Name=" + cName + "&Flag=Customer");	
				//window.open("../sys/PolQuery.jsp?CustomerNo=" + cCustomerNo + "&Name=" + cName);	
	}
}


//销户保单信息查询
function DesPolClick()
{
	var arrReturn = new Array();
	var tSel = PersonGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cCustomerNo = PersonGrid.getRowColData(tSel - 1,1);				
		
		if (cCustomerNo == "")
		    return;		    
		  //alert(cCustomerNo);
		    var cName = PersonGrid.getRowColData(tSel - 1,2);
		    //alert(cName);
		    window.open("../sys/DesPolQueryMain.jsp?CustomerNo=" + cCustomerNo + "&Name=" + cName + "&Flag=Customer");	
			
	}
}
