<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-03-17 15:12:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LDUserInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDUserInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LDUsersave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDUser1);">
    </IMG>
    <Div  id= "divLDUser1" style= "display: ''">
    <fieldset >
    <legend>用户代码维护</legend>
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            用户编码
          </TD>
          <TD  class= input>
            <Input class= common name=UserCode >
          </TD>
          <TD  class= title>
            核赔权限
          </TD>
          <TD  class= input>
            <Input class= common name=ClaimPopedom >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            用户姓名
          </TD>
          <TD  class= input>
            <Input class= common name=UserName >
          </TD>
          <TD  class= title>
            其它权限
          </TD>
          <TD  class= input>
            <Input class= common name=OtherPopedom >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            机构编码
          </TD>
          <TD  class= input>
            <Input class= "code" name=ComCode value="test" ondblclick="return showCodeList('com',[this]);" >
          </TD>
          <TD  class= title>
            首席核保标志
          </TD>
          <TD  class= input>
            <Input class= common name=PopUWFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            口令
          </TD>
          <TD  class= input>
            <Input class= common name=Password >
          </TD>
          <TD  class= title>
            超级权限标志
          </TD>
          <TD  class= input>
            <Input class= common name=SuperPopedomFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            用户描述
          </TD>
          <TD  class= input>
            <Input class= common name=UserDescription >
          </TD>
          <TD  class= title>
            操作员
          </TD>
          <TD  class= input>
            <Input class= common name=Operator >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            用户状态
          </TD>
          <TD  class= input>
            <Input class= common name=UserState >
          </TD>
          <TD  class= title>
            入机日期
          </TD>
          <TD  class= input>
            <Input class= common name=MakeDate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            核保权限
          </TD>
          <TD  class= input>
            <Input class= common name=UWPopedom >
          </TD>
          <TD  class= title>
            入机时间
          </TD>
          <TD  class= input>
            <Input class= common name=MakeTime >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            有效结束日期
          </TD>
          <TD  class= input>
            <Input class= common name=ValidEndDate >
          </TD>
          <TD  class= title>
            有效开始日期
          </TD>
          <TD  class= input>
            <Input class= common name=ValidStartDate >
          </TD>
        </TR>
      </table>
    </fieldset>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
