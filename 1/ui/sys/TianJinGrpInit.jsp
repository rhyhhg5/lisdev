<%
//程序名称：TianJinGrpInit.jsp
//程序功能：天津社保补充业务数据采集统计分析
//创建时间：2017-12-4
//创建人  ：yangjian
//添加页面控件的初始化。
%>

<script language="JavaScript">



function initForm()
{
	try
	  {
	    
	    initTianJinGrpGrid();
	  }
	  catch(re)
	  {
	    alert("TianJinGrpInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	  }
}

function initTianJinGrpGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         
        iArray[0][1]="30px";        
        iArray[0][2]=100;         
        iArray[0][3]=0;         

        iArray[1]=new Array();
  	    iArray[1][0]="文件名";          		//列名
  	    iArray[1][1]="100px";      	      		//列宽
  	    iArray[1][2]=20;            			//列最大值
  	    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

  		iArray[2]=new Array();
	    iArray[2][0]="数据统计日期";          		//列名
	    iArray[2][1]="100px";      	      		//列宽
	    iArray[2][2]=20;            			//列最大值
	    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	    
	    iArray[3]=new Array();
	    iArray[3][0]="文件路径";          		//列名
	    iArray[3][1]="100px";      	      		//列宽
	    iArray[3][2]=20;            			//列最大值
	    iArray[3][3]=3;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	    
	    
        
     
        TianJinGrpGrid = new MulLineEnter( "fm" , "TianJinGrpGrid" ); 
        //这些属性必须在loadMulLine前
        TianJinGrpGrid.mulLineCount = 10;   
        TianJinGrpGrid.displayTitle = 1;
        TianJinGrpGrid.hiddenPlus = 1;
        TianJinGrpGrid.hiddenSubtraction = 1;
        TianJinGrpGrid.locked=0;
        TianJinGrpGrid.canSel=1;
//        TianJinGrpGrid.canChk=1;
        TianJinGrpGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化TianJinGrpGrid时出错："+ ex);
      }
    }
</script>