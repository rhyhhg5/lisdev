//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAAgentQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
        var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
			try
			{	
				//alert("1");
				arrReturn = getQueryResult();
				//alert("2");
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult(ContNo,ContType)
{
	//alert(111);
	//alert("111" + ContNo +"/"+ContType);
	var arrSelected = new Array();
	var strSQL = "";
	var addressSql = "";
	if( ContType == 1)
	  {
			strSQL = "select BankCode,BankAccNo,'','',PrintCount,ShowManageName(trim(ManageCom)),Remark" 
				 + " from LCCont where ContNo= '"+ContNo+"'";

			addressSql = "select a.PostalAddress,a.ZipCode from LCAddress a,LCAppnt b"
			     + " where a.CustomerNo=b.AppntNo and a.AddressNo=b.AddressNo and b.ContNo='" +ContNo + "'";
		}
		else if (ContType ==2)
		{
		     strSQL = "select BankCode,BankAccNo,'','',PrintCount,ManageCom,Remark" 
				 + " from LBCont where ContNo= '"+ContNo+"'";

		     addressSql = "select a.PostalAddress,a.ZipCode from LCAddress a,LBAppnt b"
		         +" where a.CustomerNo=b.AppntNo and a.AddressNo=b.AddressNo and b.ContNo='" +ContNo + "'";
		}
		
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	//判断是否查询成功
    if (!turnPage.strQueryResult) {
	 return false;
    }
    var arr = decodeEasyQueryResult(turnPage.strQueryResult);

	fm.all('BankCode').value = trim(arr[0][0]);
    fm.all('BankAccNo').value = trim(arr[0][1]);
    fm.all('PostalAddress').value = trim(arr[0][2]);
    fm.all('ZipCode').value = trim(arr[0][3]);
    fm.all('PrintCount').value = trim(arr[0][4]);
    fm.all('ManageCom').value = trim(arr[0][5]);
    fm.all('Remark').value = trim(arr[0][6]); 


	var	arrReturn= easyExecSql(addressSql);
	if ( !(arrReturn == null || arrReturn ==0))
	{
          fm.all('PostalAddress').value = trim(arrReturn[0][0]);
		  fm.all('ZipCode').value = trim(arrReturn[0][1]);
    }
       
}

