<%
//程序名称：DoubleContListInit.jsp
//程序功能：双录保单清单报表
//创建时间：2017-11-1
//创建人  ：yangjian
//添加页面控件的初始化。
%>

<script language="JavaScript">

function initInpBox()
{ 
  try
  {                
    fm.all('ManageCom').value = '';
    fm.all('Salechnl').value = '';
    fm.all('AgentCode').value = ''; 
    fm.all('Name').value = ''; 
    fm.all('SignStartDate').value = ''; 
    fm.all('SignEndDate').value = ''; 
  }
  catch(ex)
  {
    alert("在DoubleContListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
	try
	  {
	    initInpBox();
	    initDoubleContListGrid();
	  }
	  catch(re)
	  {
	    alert("DoubleContListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	  }
}

function initDoubleContListGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         
        iArray[0][1]="30px";        
        iArray[0][2]=100;         
        iArray[0][3]=0;         

        iArray[1]=new Array();
  	    iArray[1][0]="机构编码";          		//列名
  	    iArray[1][1]="100px";      	      		//列宽
  	    iArray[1][2]=20;            			//列最大值
  	    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

        iArray[2]=new Array();
        iArray[2][0]="机构名称"; //列名
        iArray[2][1]="100px";        //列宽
        iArray[2][2]=100;            //列最大值
        iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
      
      	iArray[3]=new Array();
      	iArray[3][0]="销售渠道"; //列名
      	iArray[3][1]="100px";        //列宽
      	iArray[3][2]=100;            //列最大值
      	iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许 
         
        iArray[4]=new Array();
        iArray[4][0]="代理人编码"; //列名
        iArray[4][1]="100px";        //列宽
        iArray[4][2]=100;            //列最大值
        iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许
         
        iArray[5]=new Array();
        iArray[5][0]="代理人姓名"; //列名
        iArray[5][1]="100px";        //列宽
        iArray[5][2]=100;            //列最大值
        iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许
       
     
        iArray[6]=new Array();
        iArray[6][0]="团队外部编码"; //列名
        iArray[6][1]="100px";        //列宽
        iArray[6][2]=100;            //列最大值
        iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许    
          
        iArray[7]=new Array();
        iArray[7][0]="团队名称"; //列名
        iArray[7][1]="100px";        //列宽
        iArray[7][2]=100;            //列最大值
        iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许    
      
      
        iArray[8]=new Array();
        iArray[8][0]="印刷号"; //列名
        iArray[8][1]="100px";        //列宽
        iArray[8][2]=100;            //列最大值
        iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许   
  	 	
        iArray[9]=new Array();
        iArray[9][0]="投保人姓名"; //列名
        iArray[9][1]="100px";        //列宽
        iArray[9][2]=100;            //列最大值
        iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许   
      
      	iArray[10]=new Array();
      	iArray[10][0]="投保人生日"; //列名
      	iArray[10][1]="100px";        //列宽
      	iArray[10][2]=100;            //列最大值
      	iArray[10][3]=0;              //是否允许输入,1表示允许,0表示不允许 
      
        iArray[11]=new Array();
        iArray[11][0]="投保人性别"; //列名
        iArray[11][1]="100px";        //列宽
        iArray[11][2]=100;            //列最大值
        iArray[11][3]=0;              //是否允许输入,1表示允许,0表示不允许   
      
        iArray[12]=new Array();
        iArray[12][0]="投保人证件类型"; //列名
        iArray[12][1]="100px";        //列宽
        iArray[12][2]=100;            //列最大值
        iArray[12][3]=0;              //是否允许输入,1表示允许,0表示不允许   
      
        iArray[13]=new Array();
        iArray[13][0]="投保人证件号码"; //列名
        iArray[13][1]="100px";        //列宽
        iArray[13][2]=100;            //列最大值
        iArray[13][3]=0;              //是否允许输入,1表示允许,0表示不允许   
      
        iArray[14]=new Array();
        iArray[14][0]="投保日期"; //列名
        iArray[14][1]="100px";        //列宽
        iArray[14][2]=100;            //列最大值
        iArray[14][3]=0;              //是否允许输入,1表示允许,0表示不允许   
      
        iArray[15]=new Array();
        iArray[15][0]="保单生效日期"; //列名
        iArray[15][1]="100px";        //列宽
        iArray[15][2]=100;            //列最大值
        iArray[15][3]=0;              //是否允许输入,1表示允许,0表示不允许   
      
        iArray[16]=new Array();
        iArray[16][0]="保单状态"; //列名
        iArray[16][1]="100px";        //列宽
        iArray[16][2]=100;            //列最大值
        iArray[16][3]=0;              //是否允许输入,1表示允许,0表示不允许   
      
        iArray[17]=new Array();
        iArray[17][0]="有无上传双录资料"; //列名
        iArray[17][1]="100px";        //列宽
        iArray[17][2]=100;            //列最大值
        iArray[17][3]=0;              //是否允许输入,1表示允许,0表示不允许   
      
        iArray[18]=new Array();
        iArray[18][0]="质检状态"; //列名
        iArray[18][1]="100px";        //列宽
        iArray[18][2]=100;            //列最大值
        iArray[18][3]=0;              //是否允许输入,1表示允许,0表示不允许   

        iArray[19]=new Array();
        iArray[19][0]="代理机构名称"; //列名
        iArray[19][1]="100px";        //列宽
        iArray[19][2]=100;            //列最大值
        iArray[19][3]=0;              //是否允许输入,1表示允许,0表示不允许 
        
        iArray[20]=new Array();
        iArray[20][0]="代理机构编码"; //列名
        iArray[20][1]="100px";        //列宽
        iArray[20][2]=100;            //列最大值
        iArray[20][3]=0;              //是否允许输入,1表示允许,0表示不允许  
        
        iArray[21]=new Array();
        iArray[21][0]="代理机构业务员姓名"; //列名
        iArray[21][1]="100px";        //列宽
        iArray[21][2]=100;            //列最大值
        iArray[21][3]=0;              //是否允许输入,1表示允许,0表示不允许
        
        iArray[22]=new Array();
        iArray[22][0]="代理机构业务员编码"; //列名
        iArray[22][1]="100px";        //列宽
        iArray[22][2]=100;            //列最大值
        iArray[22][3]=0;              //是否允许输入,1表示允许,0表示不允许
     
        DoubleContListGrid = new MulLineEnter( "fm" , "DoubleContListGrid" ); 
        //这些属性必须在loadMulLine前
        DoubleContListGrid.mulLineCount = 10;   
        DoubleContListGrid.displayTitle = 1;
        DoubleContListGrid.hiddenPlus = 1;
        DoubleContListGrid.hiddenSubtraction = 1;
        DoubleContListGrid.locked=0;
//        DoubleContListGrid.canSel=0;
//        DoubleContListGrid.canChk=1;
        DoubleContListGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化DoubleContListGrid时出错："+ ex);
      }
    }
</script>