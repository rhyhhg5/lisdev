<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 14:47:23
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="OLCAcceptInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="OLCAcceptInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./OLCAcceptSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOLCAccept1);">
    </IMG>
    <Div  id= "divOLCAccept1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='code' name=ManageCom ondblclick="return showCodeList('station',[this]);" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class='code' name=RiskCode ondblclick="return showCodeList('riskcode',[this]);" >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class= common name=RiskVersion >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            团单标志
          </TD>
          <TD  class= input>
            <Input class= common name=GrpFlag >
          </TD>
          <TD  class= title>
            体件标志
          </TD>
          <TD  class= input>
            <Input class= common name=MEflag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            交费金额
          </TD>
          <TD  class= input>
            <Input class= common name=PayMoney >
          </TD>
          <TD  class= title>
            投保人名称
          </TD>
          <TD  class= input>
            <Input class= common name=AppntName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            被保人数目
          </TD>
          <TD  class= input>
            <Input class= common name=InsuredPeoples >
          </TD>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class='code' name=AgentCode ondblclick="return showCodeList('agentcode',[this]);" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            附件份数
          </TD>
          <TD  class= input>
            <Input class= common name=AffixNum >
          </TD>
          <TD  class= title>
            附件页数
          </TD>
          <TD  class= input>
            <Input class= common name=AffixPageNum >
          </TD>
        </TR>
      </table>
    </Div>
    <input type=hidden name=hideOperate value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
