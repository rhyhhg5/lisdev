//---------------------------------------------------
//程序名称：报表设计
//程序功能：
//创建日期：2010-03-12
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------

var turnPage = new turnPageClass(); 
var turnPage1= new turnPageClass();



function queryClick()
{
	initDataGrid();
	initDataTable();
	var strRiskCode=document.all["RiskCode"].value;
	var strValidateDate=document.all["ValidateDate"].value;
	var strInValidateDate=document.all["InValidateDate"].value;
	var strContNo=document.all["ContNo"].value;
	//window.alert(strRiskCode);
	//window.alert(strValidateDate);
	//window.alert(strInValidateDate);
	//window.alert(strContNo);
	if(strValidateDate==""||strValidateDate==null)
	{
		window.alert("起始日期不得为空!");
		return false;
	}
	
	var strStart = new Date(strValidateDate.replace(/-/g,"\/")).getTime();
    var strEnd = new Date(strInValidateDate.replace(/-/g,"\/")).getTime();
    var strDays = (strEnd-strStart)/(24*60*60*1000);  
    if(strDays>92 )
    {
	    window.alert("查询起止时间不得超过3个月！")
	    return false;
    }  
	
	datagrid(strRiskCode,strValidateDate,strInValidateDate,strContNo)
	datatable(strRiskCode,strValidateDate,strInValidateDate,strContNo)
    
} 

function DownLoadclm(){
	
	var strSql="";
	var strRiskCode=document.all["RiskCode"].value;
	var strValidateDate=document.all["ValidateDate"].value;
	var strInValidateDate=document.all["InValidateDate"].value;
	var strContNo=document.all["ContNo"].value;
	if(strValidateDate==""||strValidateDate==null)
	{
		window.alert("起始日期不得为空!");
		return false;
	}
	
	var strStart = new Date(strValidateDate.replace(/-/g,"\/")).getTime();
    var strEnd = new Date(strInValidateDate.replace(/-/g,"\/")).getTime();
    var strDays = (strEnd-strStart)/(24*60*60*1000);  
    if(strDays>92 )
    {
	    window.alert("查询起止时间不得超过3个月！")
	    return false;
    }  
	strSql="select a.mngcom 机构代码,b.riskcode 产品代码,case b.grpcontno when '00000000000000000000' then b.contno else b.grpcontno end 保单号,"+
 		   "(select cvalidate from lcpol where polno=b.polno union select cvalidate from lbpol where polno=b.polno ) 生效日期,"+
 		   "(select enddate from lcpol where polno=b.polno union select enddate from lbpol where polno=b.polno ) 失效日期,"+
 		   "(select signdate from lcpol where polno=b.polno union select signdate from lbpol where polno=b.polno ) 签单日期,"+
 		   "(select AppntName from lcpol where polno=b.polno union select AppntName from lbpol where polno=b.polno ) 投保人,"+
 		   "(select AppntNo from lcpol where polno=b.polno union select AppntNo from lbpol where polno=b.polno ) 投保人客户号,"+
 		   "(select InsuredName from lcpol where polno=b.polno union select InsuredName from lbpol where polno=b.polno ) 被保险人,"+
 		   "(select amnt from lcpol where polno=b.polno union select amnt from lbpol where polno=b.polno ) 保额,"+
 		   "sum(b.ClaimMoney) 索赔额,sum(b.realpay) 赔付金额,"+
 		   "(select DiseaseName from LLCaseCure where caseno=a.caseno order by serialno desc fetch first 1 rows only) 诊断,"+
 		   "(select n.accdesc from llcaserela m,llsubreport n where m.caseno=a.caseno and m.subrptno=n.subrptno fetch first 1 rows only) 出险原因,"+
 		   "(select n.AccDate from llcaserela m,llsubreport n where m.caseno=a.caseno and m.subrptno=n.subrptno fetch first 1 rows only) 出险日期,"+
 		   "(select hospenddate from llfeemain where caseno=a.caseno fetch first 1 rows only) 出院日期,"+
 		   "a.rgtdate 索赔日期,(select makedate from ljagetclaim where otherno=a.caseno fetch first 1 rows only) 结案日期,(select CodeName from ldcode where codetype='llclaimdecision' and Code=b.givetype) 理赔结论,sum(b.realpay) 赔款"+
 
 		   " from llcase a,llclaimdetail b"+
 		   " where a.caseno=b.caseno"+
 		   " and a.rgtstate in ('11','12')";
 		   if(strRiskCode!=""&&strRiskCode!=null)
 		   {
 		   		strSql+=" and b.riskcode='"+strRiskCode+"'";
 		   }
 		   
 		   if(strContNo!=""&&strContNo!=null)
 		   {
 		   		//strSql+=" and b.grpcontno='"+strContNo+"'";
 		   		strSql+=" and b.contno='"+strContNo+"'";
 		   }
 		   strSql+=" and exists (select 1 from ljagetclaim where otherno=b.caseno";
 		   
 		   
 		   if(strValidateDate!=""&&strValidateDate!=null)
 		   {
 		        strSql+=" and makedate>='"+strValidateDate+"'";
 		   }
 		   
 		   if(strInValidateDate!=""&&strInValidateDate!=null)
 		   {
 		   		strSql+=" and makedate<='"+strInValidateDate+"'";
 		   }
 		   
 		   	strSql+=") group by a.mngcom ,b.riskcode ,b.grpcontno,b.contno,b.polno,a.caseno,a.rgtdate,a.endcasedate,b.givetype";
 		   	strSql+=" order by 保单号  with ur";
 		   	var i = 0;
 			var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 			fm.querySqlclm.value = strSql;
 			fm.action = "XXContractClmList.jsp";
 			fm.submit();
	}


//function datatable(strRiskCode,strValidateDate,strInValidateDate,strContNo)
//{
//	var strSql="";
//	
//	strSql="select a.mngcom 机构代码,b.riskcode 产品代码,case b.grpcontno when '00000000000000000000' then b.contno else b.grpcontno end 保单号,"+
// 		   "(select cvalidate from lcpol where polno=b.polno union select cvalidate from lbpol where polno=b.polno ) 生效日期,"+
// 		   "(select enddate from lcpol where polno=b.polno union select enddate from lbpol where polno=b.polno ) 失效日期,"+
// 		   "(select signdate from lcpol where polno=b.polno union select signdate from lbpol where polno=b.polno ) 签单日期,"+
// 		   "(select AppntName from lcpol where polno=b.polno union select AppntName from lbpol where polno=b.polno ) 投保人,"+
// 		   "(select AppntNo from lcpol where polno=b.polno union select AppntNo from lbpol where polno=b.polno ) 投保人客户号,"+
// 		   "(select InsuredName from lcpol where polno=b.polno union select InsuredName from lbpol where polno=b.polno ) 被保险人,"+
// 		   "(select amnt from lcpol where polno=b.polno union select amnt from lbpol where polno=b.polno ) 保额,"+
// 		   "sum(b.ClaimMoney) 索赔额,sum(b.realpay) 赔付金额,"+
// 		   "(select DiseaseName from LLCaseCure where caseno=a.caseno order by serialno desc fetch first 1 rows only) 诊断,"+
// 		   "(select n.accdesc from llcaserela m,llsubreport n where m.caseno=a.caseno and m.subrptno=n.subrptno fetch first 1 rows only) 出险原因,"+
// 		   "(select n.AccDate from llcaserela m,llsubreport n where m.caseno=a.caseno and m.subrptno=n.subrptno fetch first 1 rows only) 出险日期,"+
// 		   "(select hospenddate from llfeemain where caseno=a.caseno fetch first 1 rows only) 出院日期,"+
// 		   "a.rgtdate 索赔日期,(select makedate from ljagetclaim where otherno=a.caseno fetch first 1 rows only) 结案日期,(select CodeName from ldcode where codetype='llclaimdecision' and Code=b.givetype) 理赔结论,sum(b.realpay) 赔款"+
// 
// 		   " from llcase a,llclaimdetail b"+
// 		   " where a.caseno=b.caseno"+
// 		   " and a.rgtstate in ('11','12')";
// 		   if(strRiskCode!=""&&strRiskCode!=null)
// 		   {
// 		   		strSql+=" and b.riskcode='"+strRiskCode+"'";
// 		   }
// 		   
// 		   if(strContNo!=""&&strContNo!=null)
// 		   {
// 		   		//strSql+=" and b.grpcontno='"+strContNo+"'";
// 		   		strSql+=" and b.contno='"+strContNo+"'";
// 		   }
// 		   strSql+=" and exists (select 1 from ljagetclaim where otherno=b.caseno";
// 		   
// 		   
// 		   if(strValidateDate!=""&&strValidateDate!=null)
// 		   {
// 		        strSql+=" and makedate>='"+strValidateDate+"'";
// 		   }
// 		   
// 		   if(strInValidateDate!=""&&strInValidateDate!=null)
// 		   {
// 		   		strSql+=" and makedate<='"+strInValidateDate+"'";
// 		   }
// 		   
// 		   strSql+=") group by a.mngcom ,b.riskcode ,b.grpcontno,b.contno,b.polno,a.caseno,a.rgtdate,a.endcasedate,b.givetype";
// 		   strSql+=" order by 保单号";
//   	
//	//-----------------------------------------------------------------------------
//	//显示理赔数据
//	turnPage1.strQueryResult=easyQueryVer3(strSql, 1, 0, 1);
//  	if (!turnPage1.strQueryResult)
//    {
//  		 //window.alert("数据库中无该行记录！");
//  		 return false;
//  	}
//  	
//    //查询成功则拆分字符串，返回二维数组
//     var arrDataSet=decodeEasyQueryResult(turnPage1.strQueryResult);
//     turnPage1.arrDataCacheSet=arrDataSet;
//     //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
//     turnPage1.pageDisplayGrid = DataTable;
//     turnPage1.strQueryResult = easyQueryVer3(strSql, 1, 0, 1);
//     turnPage1.strQuerySql=strSql;
//  	
//    //设置查询起始位置
//     turnPage1.pageIndex=0;
//     
//     //在查询结果数组中取出符合页面显示大小设置的数组
//     var tArr = new Array();
//     tArr = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex, 10);
//     //调用MULTILINE对象显示查询结果
//     displayMultiline(tArr, turnPage1.pageDisplayGrid);
//}

function DownLoadnew(){
	
	var strSQL="";
	var strRiskCode=document.all["RiskCode"].value;
	var strValidateDate=document.all["ValidateDate"].value;
	var strInValidateDate=document.all["InValidateDate"].value;
	var strContNo=document.all["ContNo"].value;
	if(strValidateDate==""||strValidateDate==null)
	{
		window.alert("起始日期不得为空!");
		return false;
	}
	
	var strStart = new Date(strValidateDate.replace(/-/g,"\/")).getTime();
    var strEnd = new Date(strInValidateDate.replace(/-/g,"\/")).getTime();
    var strDays = (strEnd-strStart)/(24*60*60*1000);  
    if(strDays>92 )
    {
	    window.alert("查询起止时间不得超过3个月！")
	    return false;
    }  
   	//个单
   	strSQL+=" select";
	strSQL+=" a.ManageCom as 管理机构,";
	strSQL+=" b.RiskCode as 产品代码,";
	strSQL+=" a.ContNo as 保单号,";
	strSQL+=" a.CValiDate as 生效日期,";
	strSQL+=" b.EndDate as 失效日期,";
	strSQL+=" a.CValiDate as 增减人日期,";
	strSQL+=" a.SignDate as 签单日期,";
	strSQL+=" a.AppntName as 投保人,";
	strSQL+=" a.AppntNo as 投保人客户号,";
	strSQL+=" b.InsuredName as 被保险人,";
	strSQL+=" b.Amnt as 保额,";
	strSQL+=" b.StandPrem as 标准保费,";
    //strSQL+=" (select SUM(Sumactupaymoney) from LJAPayPerson where PayNo in (select PayNo from LJAPay where IncomeNo=b.ContNo and RiskCode=b.RiskCode and DUEFEETYPE='0')) as 实收保费,";
	strSQL+=" b.Prem as 实收保费,";
	strSQL+=" (case SUM(b.StandPrem) when 0 then 0 else decimal(SUM(b.Prem)/SUM(b.StandPrem),12,2) end) as 折扣率,";
	strSQL+=" SUM(b.Prem) as 年度保费";
    strSQL+=" from LCCont a,LCPol b";
	strSQL+=" where a.ContNo=b.ContNo and a.ContType='1'";

	
	if(strRiskCode!=""&&strRiskCode!=null)
    {
   		strSQL+=" and b.RiskCode='"+strRiskCode+"'";
    }
   
    if(strValidateDate!=""&&strValidateDate!=null)
    {
        strSQL+=" and ((a.SignDate>=a.cvalidate and a.SignDate>='"+strValidateDate+"') or" +
        		" (a.SignDate<a.cvalidate and a.cvalidate>='"+strValidateDate+"'))";
    }
   
    if(strInValidateDate!=""&&strInValidateDate!=null)
    {
    	strSQL+=" and ((a.SignDate>=a.cvalidate and a.SignDate<='"+strInValidateDate+"') or" +
		" (a.SignDate<a.cvalidate and a.cvalidate<='"+strInValidateDate+"'))";
    }
	
	if(strContNo!=""&&strContNo!=null)
	{
		strSQL+=" and a.ContNo='"+strContNo+"'";
	}
	
	strSQL+=" group by a.ManageCom,RiskCode,a.ContNo,";
	strSQL+=" a.CValiDate,b.EndDate,a.CValidate,";
	strSQL+=" a.SignDate,a.AppntName,a.AppntNo,";
	strSQL+=" b.InsuredName,b.Amnt,b.StandPrem,";
	strSQL+=" b.Prem,";
	strSQL+=" b.ContNo";

	//团单有名单
	strSQL+=" union all";

	strSQL+=" select";
	strSQL+=" a.ManageCom as 管理机构,";
	strSQL+=" b.RiskCode as 产品代码,";
	strSQL+=" a.GrpContNo as 保单号,";
	strSQL+=" a.CValiDate as 生效日期,";
	strSQL+=" b.EndDate as 失效日期,";
	strSQL+=" a.CValiDate as 增减人日期,";
	strSQL+=" a.SignDate as 签单日期,";
	strSQL+=" a.AppntName as 投保人,";
	strSQL+=" a.AppntNo as 投保人客户号,";
	strSQL+=" (select char(Peoples) from LCGrpCont where GrpContNo=a.GrpContNo) as 被保人数,";
	strSQL+=" SUM(a.Amnt) as 保额,";
	strSQL+=" SUM(b.StandPrem) as 标准保费,";
	//strSQL+=" (select SUM(Sumactupaymoney) from LJAPayPerson where PayNo in (select PayNo from LJAPay where IncomeNo=b.ContNo and RiskCode=b.RiskCode and DUEFEETYPE='0')) as 实收保费,";
	strSQL+=" SUM(b.Prem) as 实收保费,";
	strSQL+=" (case SUM(b.StandPrem) when 0 then 0 else decimal(SUM(b.Prem)/SUM(b.StandPrem),12,2) end) as 折扣率,";
	strSQL+=" SUM(b.Prem) as 年度保费";
	strSQL+=" from LCCont a,LCPol b";
	strSQL+=" where a.ContNo=b.ContNo and a.ContType='2' and b.PolTypeFlag='1'";

	if(strRiskCode!=""&&strRiskCode!=null)
    {
   		strSQL+=" and b.RiskCode='"+strRiskCode+"'";
    }
   
	if(strValidateDate!=""&&strValidateDate!=null)
    {
        strSQL+=" and ((a.SignDate>=a.cvalidate and a.SignDate>='"+strValidateDate+"') or" +
        		" (a.SignDate<a.cvalidate and a.cvalidate>='"+strValidateDate+"'))";
    }
   
    if(strInValidateDate!=""&&strInValidateDate!=null)
    {
    	strSQL+=" and ((a.SignDate>=a.cvalidate and a.SignDate<='"+strInValidateDate+"') or" +
		" (a.SignDate<a.cvalidate and a.cvalidate<='"+strInValidateDate+"'))";
    }
	
	if(strContNo!=""&&strContNo!=null)
	{
		strSQL+=" and a.GrpContNo='"+strContNo+"'";
	}
	
	strSQL+=" group by a.ManageCom,RiskCode,a.ContNo,";
	strSQL+=" a.CValiDate,b.EndDate,a.CValidate,";
	strSQL+=" a.SignDate,a.AppntName,a.AppntNo,";
	strSQL+=" a.GrpContNo,";
	strSQL+=" a.Amnt,";
	strSQL+=" b.StandPrem,";
	strSQL+=" b.Prem,";
	strSQL+=" b.ContNo";

	strSQL+=" union all";

    //团单(无名单)
	strSQL+=" select";
	strSQL+=" a.ManageCom as 管理机构,";
	strSQL+=" b.RiskCode as 产品代码,";
	strSQL+=" a.grpContNo as 保单号,";
	strSQL+=" a.CValiDate as 生效日期,";
	strSQL+=" b.EndDate as 失效日期,";
	strSQL+=" a.CValiDate as 增减人日期,";
	strSQL+=" a.SignDate as 签单日期,";
	strSQL+=" a.AppntName as 投保人,";
	strSQL+=" a.AppntNo as 投保人客户号,";
	strSQL+=" (select char(count(distinct PolNo)) from LCPol where InsuredName<>'无名单' and GrpContNo=a.GrpContNo  and RiskCode=b.RiskCode) as 无名单,";
	strSQL+=" SUM(a.Amnt) as 保额,";
	strSQL+=" SUM(b.StandPrem) as 标准保费,";
	//strSQL+=" (select SUM(Sumactupaymoney) from LJAPayPerson where PayNo in (select PayNo from LJAPay where IncomeNo=b.ContNo and RiskCode=b.RiskCode and DUEFEETYPE='0')) as 实收保费,";
	strSQL+=" SUM(b.Prem) as 实收保费,";
	strSQL+=" (case SUM(b.StandPrem) when 0 then 0 else decimal(SUM(b.Prem)/SUM(b.StandPrem),12,2) end) as 折扣率,";
	strSQL+=" SUM(b.Prem) as 年度保费";
	strSQL+=" from LCCont a,LCPol b";
	strSQL+=" where a.ContNo=b.ContNo and a.grpcontno=b.grpcontno and a.ContType='2' and b.PolTypeFlag='1'";
	
	
	if(strRiskCode!=""&&strRiskCode!=null)
    {
   		strSQL+=" and b.RiskCode='"+strRiskCode+"'";
    }
    
    if(strContNo!=""&&strContNo!=null)
	{
		strSQL+=" and a.GrpContNo='"+strContNo+"'";
	}
	
    if(strValidateDate!=""&&strValidateDate!=null)
    {
        strSQL+=" and ((a.SignDate>=a.cvalidate and a.SignDate>='"+strValidateDate+"') or" +
        		" (a.SignDate<a.cvalidate and a.cvalidate>='"+strValidateDate+"'))";
    }
   
    if(strInValidateDate!=""&&strInValidateDate!=null)
    {
    	strSQL+=" and ((a.SignDate>=a.cvalidate and a.SignDate<='"+strInValidateDate+"') or" +
		" (a.SignDate<a.cvalidate and a.cvalidate<='"+strInValidateDate+"'))";
    }
	
	strSQL+=" group by a.ManageCom,RiskCode,";
	strSQL+=" a.CValiDate,b.EndDate,a.CValidate,";
	strSQL+=" a.SignDate,a.AppntName,a.AppntNo,b.grppolno,";
	strSQL+=" a.GrpContNo,";
	strSQL+=" b.ContNo,b.RiskCode  with ur"; 
	var i = 0;
	var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.querySqlnew.value = strSQL;
    fm.action = "XXContractNewList.jsp";
    fm.submit();
}

//function datagrid(strRiskCode,strValidateDate,strInValidateDate,strContNo)
//{
//	//--------------------------------------------------------------------------------
//   	//显示新单数据
// 	//--------------------------------------------------------------------------------
//   	var strSQL="";
//   	
//   	//个单
//   	strSQL+=" select";
//	strSQL+=" a.ManageCom as 管理机构,";
//	strSQL+=" b.RiskCode as 产品代码,";
//	strSQL+=" a.ContNo as 保单号,";
//	strSQL+=" a.CValiDate as 生效日期,";
//	strSQL+=" b.EndDate as 失效日期,";
//	strSQL+=" a.CValiDate as 增减人日期,";
//	strSQL+=" a.SignDate as 签单日期,";
//	strSQL+=" a.AppntName as 投保人,";
//	strSQL+=" a.AppntNo as 投保人客户号,";
//	strSQL+=" b.InsuredName as 被保险人,";
//	strSQL+=" b.Amnt as 保额,";
//	strSQL+=" b.StandPrem as 标准保费,";
//    //strSQL+=" (select SUM(Sumactupaymoney) from LJAPayPerson where PayNo in (select PayNo from LJAPay where IncomeNo=b.ContNo and RiskCode=b.RiskCode and DUEFEETYPE='0')) as 实收保费,";
//	strSQL+=" b.Prem as 实收保费,";
//	strSQL+=" (case SUM(b.StandPrem) when 0 then 0 else decimal(SUM(b.Prem)/SUM(b.StandPrem),12,2) end) as 折扣率,";
//	strSQL+=" SUM(b.Prem) as 年度保费";
//    strSQL+=" from LCCont a,LCPol b";
//	strSQL+=" where a.ContNo=b.ContNo and a.ContType='1'";
//
//	
//	if(strRiskCode!=""&&strRiskCode!=null)
//    {
//   		strSQL+=" and b.RiskCode='"+strRiskCode+"'";
//    }
//   
//    if(strValidateDate!=""&&strValidateDate!=null)
//    {
//        strSQL+=" and a.SignDate>='"+strValidateDate+"'";
//    }
//   
//    if(strInValidateDate!=""&&strInValidateDate!=null)
//    {
//   		strSQL+=" and a.SignDate<='"+strInValidateDate+"'";
//    }
//	
//	if(strContNo!=""&&strContNo!=null)
//	{
//		strSQL+=" and a.ContNo='"+strContNo+"'";
//	}
//	
//	strSQL+=" group by a.ManageCom,RiskCode,a.ContNo,";
//	strSQL+=" a.CValiDate,b.EndDate,a.CValidate,";
//	strSQL+=" a.SignDate,a.AppntName,a.AppntNo,";
//	strSQL+=" b.InsuredName,b.Amnt,b.StandPrem,";
//	strSQL+=" b.Prem,";
//	strSQL+=" b.ContNo";
//
//	//团单有名单
//	strSQL+=" union all";
//
//	strSQL+=" select";
//	strSQL+=" a.ManageCom as 管理机构,";
//	strSQL+=" b.RiskCode as 产品代码,";
//	strSQL+=" a.GrpContNo as 保单号,";
//	strSQL+=" a.CValiDate as 生效日期,";
//	strSQL+=" b.EndDate as 失效日期,";
//	strSQL+=" a.CValiDate as 增减人日期,";
//	strSQL+=" a.SignDate as 签单日期,";
//	strSQL+=" a.AppntName as 投保人,";
//	strSQL+=" a.AppntNo as 投保人客户号,";
//	strSQL+=" (select char(Peoples) from LCGrpCont where GrpContNo=a.GrpContNo) as 被保人数,";
//	strSQL+=" SUM(a.Amnt) as 保额,";
//	strSQL+=" SUM(b.StandPrem) as 标准保费,";
//	//strSQL+=" (select SUM(Sumactupaymoney) from LJAPayPerson where PayNo in (select PayNo from LJAPay where IncomeNo=b.ContNo and RiskCode=b.RiskCode and DUEFEETYPE='0')) as 实收保费,";
//	strSQL+=" SUM(b.Prem) as 实收保费,";
//	strSQL+=" (case SUM(b.StandPrem) when 0 then 0 else decimal(SUM(b.Prem)/SUM(b.StandPrem),12,2) end) as 折扣率,";
//	strSQL+=" SUM(b.Prem) as 年度保费";
//	strSQL+=" from LCCont a,LCPol b";
//	strSQL+=" where a.ContNo=b.ContNo and a.ContType='2' and b.PolTypeFlag='1'";
//
//	if(strRiskCode!=""&&strRiskCode!=null)
//    {
//   		strSQL+=" and b.RiskCode='"+strRiskCode+"'";
//    }
//   
//    if(strValidateDate!=""&&strValidateDate!=null)
//    {
//        strSQL+=" and a.SignDate>='"+strValidateDate+"'";
//    }
//   
//    if(strInValidateDate!=""&&strInValidateDate!=null)
//    {
//   		strSQL+=" and a.SignDate<='"+strInValidateDate+"'";
//    }
//	
//	if(strContNo!=""&&strContNo!=null)
//	{
//		strSQL+=" and a.GrpContNo='"+strContNo+"'";
//	}
//	
//	strSQL+=" group by a.ManageCom,RiskCode,a.ContNo,";
//	strSQL+=" a.CValiDate,b.EndDate,a.CValidate,";
//	strSQL+=" a.SignDate,a.AppntName,a.AppntNo,";
//	strSQL+=" a.GrpContNo,";
//	strSQL+=" a.Amnt,";
//	strSQL+=" b.StandPrem,";
//	strSQL+=" b.Prem,";
//	strSQL+=" b.ContNo";
//
//	strSQL+=" union all";
//
//    //团单(无名单)
//	strSQL+=" select";
//	strSQL+=" a.ManageCom as 管理机构,";
//	strSQL+=" b.RiskCode as 产品代码,";
//	strSQL+=" a.grpContNo as 保单号,";
//	strSQL+=" a.CValiDate as 生效日期,";
//	strSQL+=" b.EndDate as 失效日期,";
//	strSQL+=" a.CValiDate as 增减人日期,";
//	strSQL+=" a.SignDate as 签单日期,";
//	strSQL+=" a.AppntName as 投保人,";
//	strSQL+=" a.AppntNo as 投保人客户号,";
//	strSQL+=" (select char(count(distinct PolNo)) from LCPol where InsuredName<>'无名单' and GrpContNo=a.GrpContNo  and RiskCode=b.RiskCode) as 无名单,";
//	strSQL+=" SUM(a.Amnt) as 保额,";
//	strSQL+=" SUM(b.StandPrem) as 标准保费,";
//	//strSQL+=" (select SUM(Sumactupaymoney) from LJAPayPerson where PayNo in (select PayNo from LJAPay where IncomeNo=b.ContNo and RiskCode=b.RiskCode and DUEFEETYPE='0')) as 实收保费,";
//	strSQL+=" SUM(b.Prem) as 实收保费,";
//	strSQL+=" (case SUM(b.StandPrem) when 0 then 0 else decimal(SUM(b.Prem)/SUM(b.StandPrem),12,2) end) as 折扣率,";
//	strSQL+=" SUM(b.Prem) as 年度保费";
//	strSQL+=" from LCCont a,LCPol b";
//	strSQL+=" where a.ContNo=b.ContNo and a.grpcontno=b.grpcontno and a.ContType='2' and b.PolTypeFlag='1'";
//	
//	
//	if(strRiskCode!=""&&strRiskCode!=null)
//    {
//   		strSQL+=" and b.RiskCode='"+strRiskCode+"'";
//    }
//    
//    if(strContNo!=""&&strContNo!=null)
//	{
//		strSQL+=" and a.GrpContNo='"+strContNo+"'";
//	}
//	
//    if(strValidateDate!=""&&strValidateDate!=null)
//    {
//        strSQL+=" and a.SignDate>='"+strValidateDate+"'";
//    }
//   
//    if(strInValidateDate!=""&&strInValidateDate!=null)
//    {
//   		strSQL+=" and a.SignDate<='"+strInValidateDate+"'";
//    }
//	
//	strSQL+=" group by a.ManageCom,RiskCode,";
//	strSQL+=" a.CValiDate,b.EndDate,a.CValidate,";
//	strSQL+=" a.SignDate,a.AppntName,a.AppntNo,b.grppolno,";
//	strSQL+=" a.GrpContNo,";
//	strSQL+=" b.ContNo,b.RiskCode";
//   
//   	
//   	//turnPage.queryModal(strSQL,DataGrid);
//   	
//   	//显示新单数据
//	turnPage.strQueryResult=easyQueryVer3(strSQL, 1, 0, 1);
//  	if (!turnPage.strQueryResult)
//    {
//  		 //window.alert("数据库中无该行记录！");
//  		 return false;
//  	}
//  	
//    //查询成功则拆分字符串，返回二维数组
//     var arrDataSet1=decodeEasyQueryResult(turnPage.strQueryResult);
//     turnPage.arrDataCacheSet=arrDataSet1;
//     //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
//     turnPage.pageDisplayGrid = DataGrid;
//     turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
//     turnPage.strQuerySql=strSQL;
//  	
//    //设置查询起始位置
//     turnPage.pageIndex=0;
//     
//     //在查询结果数组中取出符合页面显示大小设置的数组
//     var tArr = new Array();
//     tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 10);
//     //调用MULTILINE对象显示查询结果
//     displayMultiline(tArr, turnPage.pageDisplayGrid);
//}

function downAfterSubmit(cfilePath) {
	showInfo.close();
	fileUrl.href = cfilePath; 
  fileUrl.click();
}

function afterSubmit( FlagStr, content )
{
	
//  showInfo.close();
//  if (FlagStr == "Fail" ) {             
//    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
//    
//  } else { 
//	  //content="保存成功！";
//	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
//	  
//	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
//  }
}