<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DoubleContListReport.jsp
//程序功能：双录保单清单报表
//创建时间：2017-11-1
//创建人  ：yangjian
%>


<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>

<%
  String FlagStr = "";
  String Content = "";
  System.out.println("ssssss");
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  Calendar cal = new GregorianCalendar();
  String year = String.valueOf(cal.get(Calendar.YEAR));
  String month=String.valueOf(cal.get(Calendar.MONTH)+1);
  String date=String.valueOf(cal.get(Calendar.DATE));
  String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String now = year + month + date + hour + min + sec + "_" ;
  
  String  millis = String.valueOf(System.currentTimeMillis());  
  String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;

  System.out.println("OutXmlPath:" + tOutXmlPath);
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
  tTransferData.setNameAndValue("querySql",request.getParameter("querySql"));
  System.out.println(request.getParameter("querySql"));
  System.out.println(tTransferData.getValueByName("querySql"));
  
  
  try
  {
      VData vData = new VData();
  	  vData.add(tG);
  	  vData.add(tTransferData);
      
      /**错误信息容器*/
      CErrors mErrors = new CErrors();
      DoubleContListBL tDoubleContListBL = new DoubleContListBL();
         
      if (!tDoubleContListBL.submitData(vData, ""))
      {
          Content = "报表下载失败，原因是:" + tDoubleContListBL.mErrors.getFirstError();
          FlagStr = "Fail";
      }
	  else
	  {
	 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
	 	  File file = new File(tOutXmlPath);
	 	  
	      response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[4096];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
              file.delete();
          }
	   }
	}
  
  
  
	catch(Exception ex)
	{
	  ex.printStackTrace();
	}
  
  if (!FlagStr.equals("Fail"))
  {
  	Content = "";
  	FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>