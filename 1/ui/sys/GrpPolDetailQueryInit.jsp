<%
//程序名称：PolQueryInit.jsp
//程序功能：
//创建日期：2002-12-16
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
  //  fm.all('CustomerNo').value = tCustomerNo;
  //  fm.all('Name').value = tName;
  }
  catch(ex)
  {
    alert("GrpPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
	initPolGrid();
	initSpecGrid();
	initInnerCueGrid();
	initAppAccGrid();
	initAppAccTraceGrid();
	returnParent();
	setContSever();
	queryAppAcc();
	initCrosalexp();
	initpremSayallright();
	initBeneficiaryDetailGrid();
	initCoInsuranceParamInput();
	initCoInsurancePrem();
	showAllCodeName();
	displayBnf();
  }
  catch(re)
  {
    alert("GrpPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var PolGrid; 
// 保单信息列表的初始化
function initPolGrid()
  {    
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[1]=new Array();
      iArray[1][0]="保障计划";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=10;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="ContPlanCode"; 

      iArray[2]=new Array();
      iArray[2][0]="人员类别";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=10;            			//列最大值
      iArray[2][3]=0; 
      
      iArray[3]=new Array();
      iArray[3][0]="应保人数";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=0; 

      iArray[4]=new Array();
      iArray[4][0]="参保人数";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
            
      iArray[5]=new Array();
      iArray[5][0]="险种名称";         		//列名
      iArray[5][1]="240px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="险种代码";         		//列名
      iArray[6][1]="50px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;             			//是否允许输入,1表示允许，0表示不允许
      iArray[6][21]="RiskCode";  
    
      iArray[7]=new Array();
      iArray[7][0]="保额";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][21]="Amnt";     
      
      iArray[8]=new Array();
      iArray[8][0]="档次";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=3;   
     
      iArray[9]=new Array();
      iArray[9][0]="期交保费";         		//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;            			//是否允许输入,1表示允许，0表示不允许 

      iArray[10]=new Array();
      iArray[10][0]="团险号码";         		//列名
      iArray[10][1]="0px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=3;            			//是否允许输入,1表示允许，0表示不允许 
      iArray[10][21]="GrpPolNo"; 
      
      iArray[11]=new Array();
      iArray[11][0]="保费计算方式";         		//列名
      iArray[11][1]="100px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;            			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[12]=new Array();
      iArray[12][0]="宽限期";         		//列名
      iArray[12][1]="100px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0;            			//是否允许输入,1表示允许，0表示不允许
      
      iArray[13]=new Array();
      iArray[13][0]="应缴但未缴保费";         		//列名
      iArray[13][1]="100px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0;            			//是否允许输入,1表示允许，0表示不允许

      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}

//受益所有人列表的初始化
function initBeneficiaryDetailGrid() {                               
  var iArray = new Array();

  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="受益所有人姓名";         	//列名
    iArray[1][1]="100px";            		//列宽
    iArray[1][2]=60;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="身份";         			//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=60;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="地址";         		//列名
    iArray[3][1]="200px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="证件编码";         		//列名
    iArray[4][1]="40px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="证件类型";         		//列名
    iArray[5][1]="100px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    
    iArray[6]=new Array();
    iArray[6][0]="证件号码";         		//列名
    iArray[6][1]="100px";            		//列宽
    iArray[6][2]=150;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="证件生效日期";         		//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=90;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="证件失效日期";         		//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=90;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
  
    BeneficiaryDetailGrid = new MulLineEnter( "fm" , "BeneficiaryDetailGrid" ); 
    //这些属性必须在loadMulLine前
    BeneficiaryDetailGrid.mulLineCount = 1;   
    BeneficiaryDetailGrid.displayTitle = 1;
    BeneficiaryDetailGrid.locked = 1;
    BeneficiaryDetailGrid.hiddenPlus = 1;
    BeneficiaryDetailGrid.hiddenSubtraction = 1;
    BeneficiaryDetailGrid.loadMulLine(iArray);  
    
  }
  catch(ex) {
    alert(ex);
  }
}

// 特别缴费约定信息列表的初始化
function initInnerCueGrid()
{    
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="50px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="录入人";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="30px";            		//列宽
    iArray[1][2]=80;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  
    iArray[2]=new Array();
    iArray[2][0]="录入时间";         		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=80;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="内容";         		//列名
    iArray[3][1]="150px";            		//列宽
    iArray[3][2]=150;            			//列最大值
    iArray[3][3]=0;  
  
    InnerCueGrid = new MulLineEnter( "fm" , "InnerCueGrid" ); 
    //这些属性必须在loadMulLine前
    InnerCueGrid.mulLineCount = 0;   
    InnerCueGrid.displayTitle = 1;
    InnerCueGrid.locked = 1;
    InnerCueGrid.canSel = 1;
    InnerCueGrid.hiddenPlus = 1;
    InnerCueGrid.hiddenSubtraction = 1;
    InnerCueGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
  }
  catch(ex)
  {
    alert(ex);
  }
}

// 特别缴费约定信息列表的初始化
function initSpecGrid()
  {    
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[1]=new Array();
      iArray[1][0]="约定金额";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="30px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="收费时间";         		//列名
      iArray[2][1]="37px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      SpecGrid = new MulLineEnter( "fm" , "SpecGrid" ); 
      //这些属性必须在loadMulLine前
      SpecGrid.mulLineCount = 0;   
      SpecGrid.displayTitle = 1;
      SpecGrid.locked = 1;
      SpecGrid.canSel = 1;
      SpecGrid.hiddenPlus = 1;
      SpecGrid.hiddenSubtraction = 1;
      SpecGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}

//查询账户信息
function initAppAccGrid()
{    
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="50px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="账户建立日期";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="30px";            		//列宽
    iArray[1][2]=80;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  
    iArray[2]=new Array();
    iArray[2][0]="账户余额";         		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=80;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="可领金额";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=150;            			//列最大值
    iArray[3][3]=0;  
    
    iArray[4]=new Array();
    iArray[4][0]="账户状态";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=150;            			//列最大值
    iArray[4][3]=0;  
  
    AppAccGrid = new MulLineEnter( "fm" , "AppAccGrid" ); 
    //这些属性必须在loadMulLine前
    AppAccGrid.mulLineCount = 0;   
    AppAccGrid.displayTitle = 1;
    AppAccGrid.locked = 1;
    AppAccGrid.canSel = 1;
    AppAccGrid.hiddenPlus = 1;
    AppAccGrid.hiddenSubtraction = 1;
    AppAccGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
  }
  catch(ex)
  {
    alert(ex);
  }
}

//查询账户历史信息
function initAppAccTraceGrid()
{    
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="50px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="账户类型";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="30px";            		//列宽
    iArray[1][2]=80;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  
    iArray[2]=new Array();
    iArray[2][0]="流水号";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=80;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="其他号码";         		//列名
    iArray[3][1]="50px";            		//列宽
    iArray[3][2]=150;            			//列最大值
    iArray[3][3]=0;  
    
    iArray[4]=new Array();
    iArray[4][0]="其他号码类型";         		//列名
    iArray[4][1]="50px";            		//列宽
    iArray[4][2]=150;            			//列最大值
    iArray[4][3]=0;  
    
    iArray[5]=new Array();
    iArray[5][0]="目标或来源";         		//列名
    iArray[5][1]="50px";            		//列宽
    iArray[5][2]=150;            			//列最大值
    iArray[5][3]=0;  
    
    iArray[6]=new Array();
    iArray[6][0]="本次金额";         		//列名
    iArray[6][1]="50px";            		//列宽
    iArray[6][2]=150;            			//列最大值
    iArray[6][3]=0;  
    
    iArray[7]=new Array();
    iArray[7][0]="账户余额";         		//列名
    iArray[7][1]="50px";            		//列宽
    iArray[7][2]=150;            			//列最大值
    iArray[7][3]=0;  
    
    iArray[8]=new Array();
    iArray[8][0]="确认日期";         		//列名
    iArray[8][1]="50px";            		//列宽
    iArray[8][2]=150;            			//列最大值
    iArray[8][3]=0;  
    
    iArray[9]=new Array();
    iArray[9][0]="操作员";         		//列名
    iArray[9][1]="50px";            		//列宽
    iArray[9][2]=150;            			//列最大值
    iArray[9][3]=0;  
  
    AppAccTraceGrid = new MulLineEnter( "fm" , "AppAccTraceGrid" ); 
    //这些属性必须在loadMulLine前
    AppAccTraceGrid.mulLineCount = 0;   
    AppAccTraceGrid.displayTitle = 1;
    AppAccTraceGrid.locked = 1;
    AppAccTraceGrid.canSel = 1;
    AppAccTraceGrid.hiddenPlus = 1;
    AppAccTraceGrid.hiddenSubtraction = 1;
    AppAccTraceGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>