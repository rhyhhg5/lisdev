//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//  showSubmitFrame(mDebug);
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
//  alert(FlagStr);
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在ClientMergeInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  //个人信息表中不能为空的字段检验,包括2部分
  //页面显示控件中要输入的字段(CustomerNo,Name,Sex,Birthday,Operator)	
  //隐藏的字段(MakeDate,MakeTime,ModifyDate,ModifyTime),在ClientMergeUI中输入
    if(fm.all('Name').value == '')
    {
    	alert("客户姓名不能为空!");
    	return false;
    	}
    if(fm.all('Sex').value == '')
    {
    	alert("客户性别不能为空!");
    	return false;
    	}
    if(fm.all('Birthday').value == '')
    {
    	alert("客户出生日期不能为空!");
    	return false;
    	}
    if(fm.all('Operator').value == '')
    {
    	alert("操作员代码不能为空!");
    	return false;
    	}

    if(fm.all('Sex').value !='0'&&fm.all('Sex').value!='1'&&fm.all('Sex').value!='2')
    {
    	alert("客户性别输入有误!");
    	return false;
    	}  
    if(!isDate(fm.all('Birthday').value))
    {
    	alert("客户出生日期输入有误!");
    	return false;
    	}  
    return true;
}           

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
	parent.fraMain.rows = "0,0,50,82,*";
  } else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

function mergeClick()
{
  submitForm();  
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//  window.showModalDialog("./ClientMergeQuery.html",window,"status:0;help:0;edge:sunken;dialogHide:0");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
 
  window.open("./LDPersonQuery.html");
  
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
