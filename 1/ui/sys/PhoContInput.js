 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var arrAllDateSet;
var cPerCustomerNo = "";
var tContType="";



// 王珑制作

/*********************************************************************
 *  查询按钮实现
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
    setPersonValue();
	  setGrpValue();	
	  initGrpPolGrid();
}

/*********************************************************************
 *  查询个单客户信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setPersonValue()
{

   initPersonGrid();   
   var strSQL = "";
   if (fm.all('ContNo').value == '' || fm.all('ContNo').value == null)
   {    
   	   var strCon ="";  
   	   if (!(fm.all('Birthday').value == '' || fm.all('Birthday').value == null)) strCon = " and substr(char(LDPerson.Birthday),6,5)= '"+ fm.all('Birthday').value +"'";
      	   strSQL ="(select  LDPerson.CustomerNo a,LDPerson.Name,LDPerson.Sex,LDPerson.Birthday,LDPerson.IDNo,LDPerson.IDType" 
		   + "  from LDPerson,LCAppnt where 1=1 and LDPerson.CustomerNo=LCAppnt.AppntNo and LCAppnt.GrpContNo='00000000000000000000' and LCAppnt.ContNo in (select ContNo from  LCCont where AppFlag='1') "
		   + strCon
		   + getWherePart( 'LDPerson.CustomerNo','CustomerNo' ) 
		   + getWherePart( 'LDPerson.Name','Name','like' ) 		   
		   + getWherePart( 'LDPerson.Sex','Sex') 
		   + getWherePart( 'LDPerson.IDNo','IDNo' )	
		   + " union  "
		   + " select  LDPerson.CustomerNo a,LDPerson.Name,LDPerson.Sex,LDPerson.Birthday,LDPerson.IDNo,LDPerson.IDType" 
		   + "  from LDPerson,LCInsured where 1=1 and LDPerson.CustomerNo=LCInsured.InsuredNo and LCInsured.GrpContNo='00000000000000000000' and LCInsured.ContNo in (select ContNo from  LCCont where AppFlag='1')"
		   + strCon
		   + getWherePart( 'LDPerson.CustomerNo','CustomerNo' ) 
		   + getWherePart( 'LDPerson.Name','Name','like' ) 		   
		   + getWherePart( 'LDPerson.Sex','Sex') 
		   + getWherePart( 'LDPerson.IDNo','IDNo' )
		   + " union "
		   + " select  LDPerson.CustomerNo a,LDPerson.Name,LDPerson.Sex,LDPerson.Birthday,LDPerson.IDNo,LDPerson.IDType"
		   + " from LDPerson,LBAppnt where 1=1 and LDPerson.CustomerNo=LBAppnt.AppntNo and LBAppnt.GrpContNo='00000000000000000000' and LBAppnt.ContNo in (select ContNo from  LBCont where AppFlag in('1', '3')) "
		   + "    and LBAppnt.EdorNo not like 'xb%' "
		   + strCon
		   + getWherePart( 'LDPerson.CustomerNo','CustomerNo' ) 
		   + getWherePart( 'LDPerson.Name','Name','like' ) 		   
		   + getWherePart( 'LDPerson.Sex','Sex') 
		   + getWherePart( 'LDPerson.IDNo','IDNo' )	
		   + " union  "
		   + " select  LDPerson.CustomerNo a,LDPerson.Name,LDPerson.Sex,LDPerson.Birthday,LDPerson.IDNo,LDPerson.IDType"
		   + " from LDPerson,LBInsured where 1=1 and LDPerson.CustomerNo=LBInsured.InsuredNo and LBInsured.GrpContNo='00000000000000000000' and exists(select 1 from LBPol where ContNo = LBInsured.ContNo and InsuredNo = LBInsured.InsuredNo and AppFlag in('1', '3')) "
		   + "    and LBInsured.EdorNo not like 'xb%' "
		   + strCon
		   + getWherePart( 'LDPerson.CustomerNo','CustomerNo' ) 
		   + getWherePart( 'LDPerson.Name','Name','like' ) 		   
		   + getWherePart( 'LDPerson.Sex','Sex') 
		   + getWherePart( 'LDPerson.IDNo','IDNo' )
		   + "   ) order by a"
		   ; 
	   
   }else
   {
	   alert("只按保单信息查询，不按客户信息查询");
	  var strSqlCont = " select count(*) from lccont where contno='" + trim(fm.all('ContNo').value) +"' and AppFlag='1'" ;
      var	arrReturn= easyExecSql(strSqlCont);
	  if (arrReturn == 0 || arrReturn ==null ) {
		  strSQL = "select distinct g,b,c,d,e,f from (select  AppntNo g,AppntName b,AppntSex c,AppntBirthday d,IDNo e,IDType f"
	     + " from lbappnt  where contno='" + trim(fm.all('ContNo').value) + "'"	
		 + getWherePart( 'lbappnt.ManageCom','ManageCom' ) 
		 + " union all "
	     + " select  InsuredNo g,Name b,Sex c,Birthday d,IDNo e,IDType f"
		 + " from LBInsured  where contno='" + trim(fm.all('ContNo').value) + "'" 
		 + getWherePart( 'LBInsured.ManageCom','ManageCom' ) 
		 + ")  picc order by g "
	     ;
	  }else
	  {
		  strSQL = "select distinct g,b,c,d,e,f from (select  AppntNo g,AppntName b,AppntSex c,AppntBirthday d,IDNo e,IDType f"
	     + " from lcappnt  where contno='" + trim(fm.all('ContNo').value) + "'"	
		 + getWherePart( 'lcappnt.ManageCom','ManageCom' ) 
		 + " union all "
	     + " select  InsuredNo g,Name b,Sex c,Birthday d,IDNo e,IDType f"
		 + " from LCInsured  where contno='" + trim(fm.all('ContNo').value) + "'" 
		 + getWherePart( 'LCInsured.ManageCom','ManageCom' ) 
		 + ")  picc order by g "
	     ;
	  }
	  
   }   
	turnPage1.queryModal(strSQL,PersonGrid);
	showCodeName();
}
/*********************************************************************
 *  查询团单客户信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setGrpValue()
{
   initGrpPersonGrid();
   initGrpPolGrid();
   var  strSQL ="";
       if (fm.all('ContNo').value=='' || fm.all('ContNo').value == null )
      {
         if (fm.all('Birthday').value == '' && fm.all('Sex').value == ''&&  fm.all('IDNo').value == '')
         {
  
				   strSQL = "select distinct LDGrp.CustomerNo,LDGrp.GrpName,LDGrp.Satrap,LDGrp.FoundDate,LDGrp.GrpNature,LDGrp.BusinessType"
				   + " from LDGrp where 1=1 "	
				   + getWherePart( 'LDGrp.CustomerNo','CustomerNo' ) 
				   + getWherePart( 'LDGrp.GrpName','Name','like' ) 
				   
				   + " order by LDGrp.CustomerNo"
				   ;
				 }else
				 	return false;
				 
      }
	  else
	  {
		  alert("只按保单信息查询，不按客户信息查询");
		  var strSqlCont = " select * from lcgrpcont where GrpContNo='" + trim(fm.all('ContNo').value) +"' and AppFlag='1'" ;
		  var	arrReturn= easyExecSql(strSqlCont);
		  if (arrReturn == 0 || arrReturn ==null ) {

			  strSQL = "select distinct LDGrp.CustomerNo,LDGrp.GrpName,LDGrp.Satrap,LDGrp.FoundDate,LDGrp.GrpNature,LDGrp.BusinessType"
		      + " from LDGrp,LBGrpAppnt where 1=1 AND LDGrp.CustomerNo=LBGrpAppnt.CustomerNo "	
			  + " AND LBGrpAppnt.GrpContNo='" + trim(fm.all('ContNo').value) + "'"
			  + getWherePart( 'LBGrpAppnt.ManageCom','ManageCom' ) 
	  	      + getWherePart( 'LBGrpAppnt.Name','AppntName') 
			  ;
			  
		  }else
		  {			 
			  strSQL = "select distinct LDGrp.CustomerNo,LDGrp.GrpName,LDGrp.Satrap,LDGrp.FoundDate,LDGrp.GrpNature,LDGrp.BusinessType"
		      + " from LDGrp,lcgrpcont where 1=1 AND LDGrp.CustomerNo=lcgrpcont.AppntNo "	
			  + " AND lcgrpcont.GrpContNo='" + trim(fm.all('ContNo').value) +"'"
			  + getWherePart( 'lcgrpcont.ManageCom','ManageCom' ) 
	  	      + getWherePart( 'lcgrpcont.Name','AppntName') 
			  ;
		  }
	  }
 	  turnPage2.queryModal(strSQL,GrpPersonGrid);
 	  
}


/*********************************************************************
 *  通过个单客户查询保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setPolValue()
{
	// 初始化表格
	initGrpPolGrid();

   //个单客户选中客户号码
	var tSel = PersonGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{	
    }else
    {	
	    cPerCustomerNo = PersonGrid.getRowColData( tSel - 1, 1 );
	}

	//保单性质为1：已签单； 2：撤保或退保
	//查询保单信息
	var strSQL = "select  a.ContNo as contno,'',a.AppntName as appname,a.CValiDate,a.Prem ,a.AppFlag,a.AppntNo,'个人' as flag,'1'"
	    + " from LCCont a where a.grpcontno='00000000000000000000' "
		+ " and a.AppFlag='1'  and a.AppntNo='" + cPerCustomerNo + "'"
		+ " union "
		+ " select  a.ContNo as contno,'',a.AppntName as appname,a.CValiDate,a.Prem ,a.AppFlag,a.AppntNo,'个人' as flag,'1' "
		+ " from LCCont a where a.grpcontno='00000000000000000000' 	and a.AppFlag='1' "
		+ " and (select count(*)  from LCInsured where ContNo=a.ContNo and grpcontno='00000000000000000000' and  InsuredNo='" + cPerCustomerNo + "')>0"
		+ " union all"
		+ " select  a.ContNo as contno,'',a.AppntName as appname,a.CValiDate,a.Prem ,'犹豫期撤保/退保',a.AppntNo,'个人' as flag,'2'"
    + " from LBCont a, LPEdorItem d where a.edorno=d.edorno and a.grpcontno='00000000000000000000'"
    + " and a.AppntNo='" + cPerCustomerNo + "' "
    + " and a.ContNo not in(select NewContNo from LCRnewStateLog) "
		+ " union "
		+ " select  a.ContNo as contno,'',a.AppntName as appname,a.CValiDate,a.Prem ,'犹豫期撤保/退保',a.AppntNo,'个人' as flag,'2'"
		+ " from LBCont a, LPEdorItem d where a.edorno=d.edorno and a.grpcontno='00000000000000000000'"
		+ " and   (select count(*) from LBInsured where ContNo=a.ContNo and grpcontno='00000000000000000000' and  InsuredNo='"+cPerCustomerNo+"')>0 "
    + " and a.ContNo not in(select NewContNo from LCRnewStateLog) "
        ;

   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		return false;
	}
  
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    arrAllDateSet =  turnPage.arrDataCacheSet ;
	//设置客户属性值
    setCosProrperty();

	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = GrpPolGrid;    
		  
	//保存SQL语句
	turnPage.strQuerySql     = strSQL; 
  
	//设置查询起始位置
	turnPage.pageIndex = 0;  
  
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(arrAllDateSet, turnPage.pageIndex, MAXSCREENLINES);
  
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);  

}	
/*********************************************************************
 *  通过团单客户查询保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setGrpPolValue()
{
	// 初始化表格
	initGrpPolGrid();

	//团单客户选中客户号码
	var tSel2 = GrpPersonGrid.getSelNo();
	
	if( tSel2 == 0 || tSel2 == null )
	{		
	}else
	{
		cPerCustomerNo = GrpPersonGrid.getRowColData( tSel2 - 1, 1 );
	}
    
	//查询保单信息
	var strSQL = " select  a.GrpContNo as contno,'',a.GrpName as appname,a.CValiDate ,a.Prem,a.AppFlag,a.AppntNo,'团体' as flag,'1'" 
		+ " from LCGrpCont a where 1=1 "
		+ " and a.AppFlag='1'"
        + " and a.AppntNo='" + cPerCustomerNo + "' "
		+ " union all"
		+ " select  a.GrpContNo as contno,'',a.GrpName as appname,a.CValiDate ,a.Prem,'犹豫期撤保/退保',a.AppntNo,'团体' as flag,'2'" 
		+ " from LBGrpCont a left join LCRnewStateLog b "
		+ " on a.GrpContNo = b.NewGrpContNo "
		+ "where 1=1 "
    + " and a.AppntNo='" + cPerCustomerNo + "' "
    + " and b.NewGrpContNo is null "
		;

   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		return false;
	}
  
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    arrAllDateSet =  turnPage.arrDataCacheSet ;
	//设置客户属性值
    setCosProrperty();

	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = GrpPolGrid;    
		  
	//保存SQL语句
	turnPage.strQuerySql     = strSQL; 
  
	//设置查询起始位置
	turnPage.pageIndex = 0;  
  
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(arrAllDateSet, turnPage.pageIndex, MAXSCREENLINES);
  
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);  

}	

/*********************************************************************
 *  查询团单被保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryInsured(i)
{
	var strGrpNo = arrAllDateSet[i][0];
	var strFlag = arrAllDateSet[i][7];
	var strType = arrAllDateSet[i][8];
	var strSql = "";
    if (strFlag == '团体')
	{
        if (strType == '1')
     	{
            strSql = "select PolNo from lcpol where GrpContNo='" + arrAllDateSet[i][0] + "' and  InsuredNo='" +  cPerCustomerNo +"'"; 
		}else if (strType == '2')
		{
            strSql = "select PolNo from lbpol where GrpContNo='" + arrAllDateSet[i][0] + "' and  InsuredNo='" +  cPerCustomerNo +"'"; 
		}

	}else if (strFlag == '个人')
	{
	    if (strType == '1')
     	{
	        strSql = "select PolNo from lcpol where ContNo='" + arrAllDateSet[i][0] + "' and  InsuredNo='" +  cPerCustomerNo +"'";
		}else if (strType == '2')
		{
            strSql = "select PolNo from lbpol where ContNo='" + arrAllDateSet[i][0] + "' and  InsuredNo='" +  cPerCustomerNo +"'";
		}
	   
	}

	var	arrReturn= easyExecSql(strSql);
	if (arrReturn == 0 || arrReturn ==null ) {
		arrAllDateSet[i][1] = "投保人";
	}else
	{
		arrAllDateSet[i][1] = "投保人/被保人";
	}
}
/*********************************************************************
 *  查询客户属性信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setCosProrperty()
{
	for (i=0; i < arrAllDateSet.length ;i++ )
	{
		
		if (arrAllDateSet[i][6] == cPerCustomerNo)
		{
			queryInsured(i);
		}else 
		{
			arrAllDateSet[i][1] = "被保人";
		}
	}
}

/*********************************************************************
 *  查询保单明细
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function returnParent()
{	
   fromListReturn();
	
}

/*********************************************************************
 *  投保信息列表中选择一条，查看投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function fromListReturn()
{
	//从客户投保信息列表中选择一条，查看投保信息
	var tSel = GrpPolGrid.getSelNo();

	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录" );
	else
	{
		var cContNo = GrpPolGrid.getRowColData( tSel - 1, 1 );
		var cFlag = GrpPolGrid.getRowColData( tSel - 1, 8 );
		var cContType = GrpPolGrid.getRowColData( tSel - 1,9);
		try
		{
			if (cFlag == '团体')
			{
				//window.open("./GrpPolDetailQueryMain.jsp?ContNo="+ cContNo+"&ContType="+cContType);
			}else if (cFlag == '个人')
			{
				window.open("../sys/BaseInfoQuery.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType="+cContType);	
			}
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}

//王珑制作

