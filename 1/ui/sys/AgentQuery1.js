//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1= new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在AgentQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
  var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();	
	var tsql;
	var arrSelected1=null;	
	var departrsn="";
	var departdate="";
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		if (AgentGrid.getRowColData(tSel-1,6)>='03')
		{
			tsql="select AgentCode,DepartDate,DepartRsn from LADimission where AgentCode='"+AgentGrid.getRowColData(tSel-1,1)+"' and DepartTimes=(select max(DepartTimes) from LADimission where AgentCode='"+AgentGrid.getRowColData(tSel-1,1)+"')";
		        turnPage1.strQueryResult=easyQueryVer3(tsql, 1, 0, 1);
					        //判断是否查询成功
			  if (!turnPage1.strQueryResult) {
			    alert("查询离职信息失败！");
			    }
			    else
			    {
		        arrSelected1 = decodeEasyQueryResult(turnPage1.strQueryResult);
		        if (arrSelected1[0][1]==null)
		        departdate="";
		        else
		        departdate=arrSelected1[0][1];
		        if (arrSelected1[0][2]==null)
		        departrsn="";
		        else
		        departrsn=arrSelected1[0][2];
			}
		}
		window.open("../sys/AgentDetailInput1.jsp?AgentCode=" + AgentGrid.getRowColData(tSel-1,1)+"&DepartDate="+ departdate+"&DepartRsn="+departrsn);
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = AgentGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	  return arrSelected;
	
	arrSelected = new Array();
	
	var strSQL = "";
	strSQL = strSQL = "select a.*,c.BranchManager,b.IntroAgency,b.AgentSeries,b.AgentGrade,c.BranchAttr,b.AscriptSeries from LAAgent a,LATree b,LABranchGroup c where 1=1 "
	         + "and a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup and a.AgentCode='"+AgentGrid.getRowColData(tRow-1,1)+"' and (c.state<>'1' or c.state is null)"; 
	     
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
	var tReturn = getManageComLimitlike("a.ManageCom");
	// 初始化表格
	initAgentGrid();
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.agentcode,c.branchattr,a.managecom,a.name,a.idno,a.agentstate from LAAgent a,LATree b,LABranchGroup c where 1=1 "
	         + "and a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup and a.branchtype='1' and (c.state<>'1' or c.state is null)"
	         + tReturn
	         + getWherePart('a.AgentCode','AgentCode','like')
	         + getWherePart('c.BranchAttr','AgentGroup','like')
	         + getWherePart('a.ManageCom','ManageCom')
	         + getWherePart('a.Name','Name')	         
	         + getWherePart('a.Sex','Sex')	         
	         + getWherePart('a.IDNo','IDNo')
	         + getWherePart('b.AgentGrade','AgentGrade')
	         + getWherePart('a.RecommendAgent','RecommendAgent')
	         + getWherePart('a.AgentState','AgentState')
	         + getWherePart('a.Birthday','Birthday')
	if ((fm.all('AgentState').value != '')&&(fm.all('AgentState').value != null)&&(fm.all('StartDay').value != '')&&(fm.all('StartDay').value != null)&&(fm.all('EndDay').value != '')&&(fm.all('EndDay').value != null))
       { 
       	if ((fm.all('AgentState').value == '01')||(fm.all('AgentState').value =='02'))
  	 strSQL = strSQL +" and EmployDate>='"+fm.all('StartDay').value+"' and EmployDate<='"+fm.all('EndDay').value+"'";
  	 if ((fm.all('AgentState').value == '03')||(fm.all('AgentState').value =='04'))
  	 strSQL = strSQL +" and OutWorkDate>='"+fm.all('StartDay').value+"' and OutWorkDate<='"+fm.all('EndDay').value+"'";
       }      
	         
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}


// 投保单信息查询
function ProposalClick()
{
	var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cCustomerNo = AgentGrid.getRowColData(tSel - 1,1);				
		if (cCustomerNo == "")
		    return;		    
		    var cName = AgentGrid.getRowColData(tSel - 1,4);
				window.open("../sys/ProposalQuery.jsp?CustomerNo=" + cCustomerNo + "&Name=" + cName + "&Flag=Agent");	
	}
}

// 保单信息查询
function PolClick()
{
	var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cCustomerNo = AgentGrid.getRowColData(tSel - 1,1);				
		
		if (cCustomerNo == "")
		    return;		    
		    var cName = AgentGrid.getRowColData(tSel - 1,4);
		    window.open("../sys/PolQueryMain.jsp?CustomerNo=" + cCustomerNo + "&Name=" + cName + "&Flag=Agent");	
	}
}

//销户保单信息查询
function DesPolClick()
{
	var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cCustomerNo = AgentGrid.getRowColData(tSel - 1,1);				
		if (cCustomerNo == "")
		    return;		    
		    var cName = AgentGrid.getRowColData(tSel - 1,4);
		    window.open("../sys/DesPolQueryMain.jsp?CustomerNo=" + cCustomerNo + "&Name=" + cName + "&Flag=Agent");	
	}
}
