<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ChangePolStateChk.jsp
//程序功能：修改保单状态
//创建日期：
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  if(tG == null) {
    out.println("session has expired");
    return;
  }

  LCPolSchema tLCPolSchema = new LCPolSchema();//定义一个Schema
  String tPolNo = request.getParameter("PolNo");//定义变量并取值
  String tOldPolStateReason = request.getParameter("CurrPolStateReason");
  String tNewPolStateReason = request.getParameter("NewPolStateReason");
  System.out.println("polno:"+tPolNo);    //需要显示的部分
  System.out.println("tOldPolStateReason:"+tOldPolStateReason);
  System.out.println("tNewPolStateReason:"+tNewPolStateReason);
  boolean flag = true;
  tLCPolSchema.setPolNo(tPolNo);//给t...Schema赋值
  tLCPolSchema.setPolState(tNewPolStateReason+tOldPolStateReason);
  if (flag == true)
  {      
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.add( tLCPolSchema);
    tVData.add( tG );
    // 数据传输
    ChangPolStateBLS tChangPolStateBLS   = new ChangPolStateBLS();
    if (tChangPolStateBLS.submitData(tVData,"UPDATA") == false)
    {
      int n = tChangPolStateBLS.mErrors.getErrorCount();
      for (int i = 0; i < n; i++)
        Content = " 保单状态修改失败，原因是: " + tChangPolStateBLS.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
    }
      System.out.println("FlagStr1:"+FlagStr);
   //如果在Catch中发现异常，则不从错误类中提取错误信息
    if (FlagStr == "Fail"){
      tError = tChangPolStateBLS.mErrors;
      if (!tError.needDealError()){
      Content = " 保单状态修改成功! ";
      FlagStr = "Succ";
      }
      else{
      Content = " 保单状态修改失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
      }
      }
  System.out.println("FlagStr:"+FlagStr);
}
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
