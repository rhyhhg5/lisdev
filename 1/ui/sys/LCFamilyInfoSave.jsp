<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LCFamilyInfoSave.jsp
//程序功能：
//创建日期：2005-02-26 15:55:46
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LCFamilyInfoSchema tLCFamilyInfoSchema   = new LCFamilyInfoSchema();
  LCFamilyRelaInfoSet tLCFamilyRelaInfoSet   = new LCFamilyRelaInfoSet();
  OLCFamilyInfoUI tOLCFamilyInfoUI   = new OLCFamilyInfoUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    tLCFamilyInfoSchema.setFamilyNo(request.getParameter("FamilyNo"));
    tLCFamilyInfoSchema.setRemark(request.getParameter("Remark"));
    tLCFamilyInfoSchema.setHomeAddress(request.getParameter("HomeAddress"));
    tLCFamilyInfoSchema.setHomeZipCode(request.getParameter("HomeZipCode"));
    tLCFamilyInfoSchema.setHomePhone(request.getParameter("HomePhone"));
    tLCFamilyInfoSchema.setHomeFax(request.getParameter("HomeFax"));
    
        String tPersonNum[] = request.getParameterValues("LCFamilyRelaInfoGridNo");
	String tCustomerNo[] = request.getParameterValues("LCFamilyRelaInfoGrid1");            
	String tFamilyStatus[] = request.getParameterValues("LCFamilyRelaInfoGrid3");
	String tLevelRela[] = request.getParameterValues("LCFamilyRelaInfoGrid4");               
	String tRemark[] = request.getParameterValues("LCFamilyRelaInfoGrid5");      
  
 
 			int PersonCount = 0;
			if (tPersonNum != null) PersonCount = tPersonNum.length;
	        
			for (int i = 0; i < PersonCount; i++)	
			{
			  LCFamilyRelaInfoSchema tLCFamilyRelaInfoSchema   = new LCFamilyRelaInfoSchema(); 
			  tLCFamilyRelaInfoSchema.setCustomerNo(tCustomerNo[i]);
			  tLCFamilyRelaInfoSchema.setFamilyStatus(tFamilyStatus[i]);
			  tLCFamilyRelaInfoSchema.setLevelRela(tLevelRela[i]);
			  tLCFamilyRelaInfoSchema.setRemark(tRemark[i]);
			  tLCFamilyRelaInfoSet.add(tLCFamilyRelaInfoSchema); 
			 }
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLCFamilyInfoSchema);
	tVData.add(tLCFamilyRelaInfoSet);
  	tVData.add(tG);
    tOLCFamilyInfoUI.submitData(tVData,transact);
    		    if (transact.equals("INSERT||MAIN"))
		        {			        	
		    	    tVData.clear();
		    	    tVData = tOLCFamilyInfoUI.getResult();     
                            LCFamilyInfoSchema mLCFamilyInfoSchema   = new LCFamilyInfoSchema();   
                            mLCFamilyInfoSchema.setSchema(( LCFamilyInfoSchema )tVData.getObjectByObjectName( "LCFamilyInfoSchema", 0 ));
			%>
			   <script language="javascript">
			    	parent.fraInterface.fm.all("FamilyNo").value = "<%=mLCFamilyInfoSchema.getFamilyNo()%>";			         	
			   </script>  
<%  
                        }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLCFamilyInfoUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
