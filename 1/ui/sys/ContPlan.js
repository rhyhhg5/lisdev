//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var QueryResult="";
var QueryCount = 0;
var mulLineCount = 0;
var QueryWhere="";
var tSearch = 0;
var divprem="";
var sumarray=new Array();
var sumarraycount=0;
window.onfocus=myonfocus;

function afterCodeSelect( cCodeName, Field )
{
  //判定双击操作执行的是什么查询
  if (cCodeName=="GrpRisk")
  {
    var tRiskFlag = fm.all('RiskFlag').value;
    //由于附加险不带出主险录入框，因此判定当主附险为S的时候隐藏
    if (tRiskFlag!="S")
    {
      divmainriskname.style.display = 'none';
      divmainrisk.style.display = 'none';
      fm.all('MainRiskCode').value = fm.all('RiskCode').value;
    }
    else
    {
      divmainriskname.style.display = '';
      divmainrisk.style.display = '';
      fm.all('MainRiskCode').value = "";
    }
    //判断险种是否是账户型，如果帐户型险种，没有总保费，只有帐户金额，
    //如果帐户是个人账户，则显示需要填入的个人账户金额
    if (Field.value!=null&&Field.value!="")
    {
      var strSql = "select InsuAccNo from LMRisktoAcc where RiskCode='"+Field.value+"'";
      var arr = easyExecSql(strSql);
      if(arr)
      {
        //为节省数据库资源，拼sql条件部分
        var getWherePart = "(";
        for(var i=0;i<arr.length;i++)
        {
          getWherePart += "'";
          getWherePart += arr[i][0];
          getWherePart += "'";
          if(i!=arr.length-1)
          {
            getWherePart += ",";
          }
        }
        getWherePart += ")";
        var strSql = "select 1 from LMRiskInsuAcc where AccType<>'001' and InsuAccNo in "+getWherePart;
        var arr = easyExecSql(strSql);
        if(arr)
        {
          SumPremTitle.style.display='none';
          SumPremInput.style.display='none';
          AccPremTitle.style.display='';
          AccPremInput.style.display='';
        }
      }
    }
  }
  //判断是否选择保险套餐
  if (cCodeName=="RiskPlan1")
  {
    var tRiskPlan1 = fm.all('RiskPlan1').value;
    if (tRiskPlan1!="0")
    {
      divriskcodename.style.display = 'none';
      divriskcode.style.display = 'none';
      divcontplanname.style.display = '';
      divcontplan.style.display = '';
      ContPlanGrid.lock();
    }
    else
    {
      divriskcodename.style.display = '';
      divriskcode.style.display = '';
      divcontplanname.style.display = 'none';
      divcontplan.style.display = 'none';
      ContPlanGrid.unLock();
    }
  }
  //将保险套餐数据带入界面
  if (cCodeName=="RiskPlan")
  {
    var tRiskPlan = fm.all('RiskPlan').value;
    if (tRiskPlan!=null&&tRiskPlan!="")
    {
      showPlan();
    }
  }
}


//返回上一步
function returnparent()
{
  parent.close();
}


function initFactoryType(tRiskCode)
{
  // 书写SQL语句
  var k=0;
  var strSQL = "";
  strSQL = "select distinct a.FactoryType,b.FactoryTypeName,a.FactoryType||"+tRiskCode+" from LMFactoryMode a ,LMFactoryType b  where 1=1 "
           + " and a.FactoryType= b.FactoryType "
           + " and (RiskCode = '"+tRiskCode+"' or RiskCode ='000000' )";
  var str  = easyQueryVer3(strSQL, 1, 0, 1);
  return str;
}

function QueryDutyClick()
{
  if (fm.all('ContPlanCode').value == "")
  {
    alert("请输入保障计划！");
    fm.all('ContPlanCode').focus();
    return false;
  }
  if(fm.all('RiskCode').value =="")
  {
    alert("请选择险种！");
    fm.all('RiskCode').focus();
    return false;
  }

  initContPlanDutyGrid();

  //查询该险种下的险种计算要素
  strSQL = "select distinct a.DutyCode,b.DutyName,a.ChoFlag,case a.ChoFlag when 'M' then '必选' when 'B' then '备用' else '可选' end ChoFlagName "
           + "from LMRiskDuty a, LMDuty b ,LMRiskDutyFactor c "
           + "where a.DutyCode = b.DutyCode and a.RiskCode = c.RiskCode and a.DutyCode = c.DutyCode "
           + "and a.RiskCode = '"+fm.all('RiskCode').value+"' order by a.DutyCode";
  //fm.all('ContPlanName').value = strSQL;
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
    alert("没有该险种下的责任信息！");
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ContPlanDutyGrid;
  //保存SQL语句
  turnPage.strQuerySql = strSQL;
  //设置查询起始位置
  turnPage.pageIndex = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //调用MULTILINE对象显示查询结果
  displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
  
}

function easyQueryClick()
{
  initContPlanCodeGrid();  
  

  //查询该险种下的险种计算要素
  strSQL = "select ContPlanCode,ContPlanName,PlanSql,Peoples3,Peoples2 "
           + "from LCContPlan "
           + "where 1=1 "
           + "and GrpContNo = '"+fm.all('GrpContNo').value;
  if(fm.LoadFrame.value == "TASK")
  {
	 strSQL = "select ContPlanCode,ContPlanName,PlanSql,Peoples3,Peoples2 "
           + " from LCContPlan a "
           + " where 1=1 "
           + " and GrpContNo = '"+fm.all('GrpContNo').value
		   + " ' and ContPlanCode in(select distinct ContPlanCode from LCInsured where GrpContNo = a.GrpContNo) "
		   + " union "
		   +" select ContPlanCode,ContPlanName,PlanSql,Peoples3,Peoples2 "
           + "from LBContPlan a "
           + " where 1=1 "
           + " and GrpContNo = '"+fm.all('GrpContNo').value
		   + " ' and ContPlanCode in(select distinct ContPlanCode from LCInsured where GrpContNo = a.GrpContNo) "
		   ;
     strSQL = strSQL + " order by ContPlanCode";
  }
  else
  {
    strSQL = strSQL + "' and ContPlanCode not in ('00','11') order by ContPlanCode";
  }
  //fm.all('ContPlanName').value = strSQL;
  turnPage.pageLineNum = 50;
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  
  //如果没数据也无异常
  if (!turnPage.strQueryResult)
  {
    return false;
  }
  else
  {
    //QueryCount = 1;
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = ContPlanCodeGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    //调用MULTILINE对象显示查询结果
    displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
  }
  //不管前面的查询什么结果，都执行下面的查询
  //如果发生参数错误，导致查询失败，这里程序会出错
  
  strSQL = "select GrpContNo,ProposalGrpContNo,ManageCom,AppntNo,GrpName from LCGrpCont where GrpContNo = '" +GrpContNo+ "'"
            + " union "
			+ "select GrpContNo,ProposalGrpContNo,ManageCom,AppntNo,GrpName from LBGrpCont where GrpContNo = '" +GrpContNo+ "'";
  turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  fm.all('GrpContNo').value = turnPage.arrDataCacheSet[0][0];
  fm.all('ProposalGrpContNo').value = turnPage.arrDataCacheSet[0][1];
  fm.all('ManageCom').value = turnPage.arrDataCacheSet[0][2];
  fm.all('AppntNo').value = turnPage.arrDataCacheSet[0][3];
  fm.all('GrpName').value = turnPage.arrDataCacheSet[0][4];
  
}

//单选框点击触发事件
function ShowContPlan(parm1,parm2)
{
  var ContPlanCodeForManageFee = "";
  if(fm.all(parm1).all('InpContPlanCodeGridSel').value == '1')
  {
    //当前行第1列的值设为：选中
    var cContPlanCode = fm.all(parm1).all('ContPlanCodeGrid1').value;	//计划编码
    ContPlanCodeForManageFee = cContPlanCode;
    fm.ContplanCode.value=cContPlanCode; //add by zjd 共用保额获取保障计划编码
    var cContPlanName = fm.all(parm1).all('ContPlanCodeGrid2').value;	//计划名称
    var cPlanSql = fm.all(parm1).all('ContPlanCodeGrid3').value;	//分类说明
    var cPeoples3 = fm.all(parm1).all('ContPlanCodeGrid4').value;	//可保人数
    var cPeoples2 = fm.all(parm1).all('ContPlanCodeGrid5').value;	//可保人数
    fm.all('ContPlanCode').value = cContPlanCode;
    fm.all('ContPlanName').value = cContPlanName;
    fm.all('PlanSql').value = cPlanSql;
    fm.all('Peoples3').value = cPeoples3;
    fm.all('Peoples2').value = cPeoples2;
    var cGrpContNo = fm.all('GrpContNo').value;

    //查询该险种下的险种计算要素
    strSQL = "(select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti,d.CalFactorValue,d.Remark,b.RiskVer,d.GrpPolNo,d.MainRiskCode,d.CalFactorType,c.CalMode,e.RiskPrem,a.PayPlanCode,a.GetDutyCode,a.InsuAccNo "
             + "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCContPlanDutyParam d ,LCContPlanRisk e  "
             + "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
             + "and a.DutyCode = d.DutyCode and a.CalFactor = d.CalFactor and b.RiskVer = d.RiskVersion "
             + "and d.ContPlanCode = '"+fm.all('ContPlanCode').value+"'"
             + " and a.InsuAccNo = d.InsuAccNo "
             + " and e.GrpContNo = d.GrpContNo"
             + " and e.RiskCode = d.RiskCode "
             + " and e.ContPlanCode = d.ContPlanCode "
             + " and a.CalFactor  not in (select CalFactor from LCContPlanDutyParam where  "
             + " GrpContNo='"+GrpContNo+"' and ContPlanCode='11') "
             + "and d.GrpContNO = '"+GrpContNo+"'"
			 + " union "
			 +" select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti,d.CalFactorValue,d.Remark,b.RiskVer,d.GrpPolNo,d.MainRiskCode,d.CalFactorType,c.CalMode,e.RiskPrem,a.PayPlanCode,a.GetDutyCode,a.InsuAccNo "
             + "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCContPlanDutyParam d ,LBContPlanRisk e  "
             + "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
             + "and a.DutyCode = d.DutyCode and a.CalFactor = d.CalFactor and b.RiskVer = d.RiskVersion "
             + "and d.ContPlanCode = '"+fm.all('ContPlanCode').value+"'"
             + " and a.InsuAccNo = d.InsuAccNo "
             + " and e.GrpContNo = d.GrpContNo"
             + " and e.RiskCode = d.RiskCode "
             + " and e.ContPlanCode = d.ContPlanCode "
             + " and a.CalFactor  not in (select CalFactor from LCContPlanDutyParam where  "
             + " GrpContNo='"+GrpContNo+"' and ContPlanCode='11') "
             + "and d.GrpContNO = '"+GrpContNo+"')"
			 + " order by RiskCode,MainRiskCode,DutyCode";
    //fm.all('PlanSql').value = strSQL;
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //原则上不会失败，嘿嘿
    if (!turnPage.strQueryResult)
    {
      alert("查询失败！");
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = ContPlanGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    //调用MULTILINE对象显示查询结果
    displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
    var strSQL = "select a.RiskCode,(select b.riskname from lmrisk b where "
                 +"b.riskcode=a.riskCode),(select codename from ldcode where codetype='calrule' and code = (select distinct CalFactorValue from lccontplandutyparam  where CalFactor='CalRule' and RiskCode=a.riskCode and GrpContNo = a.grpcontno))" 
                 +",a.RiskPrem  from LCContPlanRisk a where a.grpcontno='"
                 +fm.all('GrpContNo').value+"' and a.contplancode='"
                 +fm.all('ContPlanCode').value+"'"
				 + " union "
				 + "select a.RiskCode,(select b.riskname from lmrisk b where "
                 +"b.riskcode=a.riskCode),(select codename from ldcode where codetype='calrule' and code = (select distinct CalFactorValue from lccontplandutyparam  where CalFactor='CalRule' and RiskCode=a.riskCode and GrpContNo = a.grpcontno))"
                 + ",a.RiskPrem from LBContPlanRisk a where a.grpcontno='"
                 +fm.all('GrpContNo').value+"' and a.contplancode='"
                 +fm.all('ContPlanCode').value+"'";
    turnPage.queryModal(strSQL,RiskPremGrid);
    var sumPrem = 0;
   
    var strSql= "select sum(c.RiskPrem) from (select RiskPrem from LCContPlanRisk a where a.grpcontno='"
                 +fm.all('GrpContNo').value+"' and a.contplancode='"
                 +fm.all('ContPlanCode').value+"'"
                 	 + " union "
				         + "select a.RiskPrem from LBContPlanRisk a where a.grpcontno='"
                 +fm.all('GrpContNo').value+"' and a.contplancode='"
                 +fm.all('ContPlanCode').value+"') c"

	 var arrReturn3 =easyExecSql(strSql);
	 if (arrReturn3 == null) {
	 } else {
		 try {fm.all('SumPrem').value= arrReturn3[0][0];   } catch(ex) { }; 
	 }
    QueryCount = 1;
    tSearch = 1;
  }
  //QueryManageFee(ContPlanCodeForManageFee);
  
  queryContPlanRisk();
}

//查询被选中的保障计划险种信息
function queryContPlanRisk()
{
  var row = ContPlanCodeGrid.getSelNo();
  var contPlanCode = ContPlanCodeGrid.getRowColDataByName(row - 1, "ContPlanCode");
  var contPlanName = ContPlanCodeGrid.getRowColDataByName(row - 1, "ContPlanName");
  var Peoples3 = ContPlanCodeGrid.getRowColDataByName(row - 1, "Peoples3");
  var Peoples2 = ContPlanCodeGrid.getRowColDataByName(row - 1, "Peoples2");
  
  var sql = "select '" + contPlanCode + "', '" + contPlanName + "', '" 
          + Peoples3 + "', '" + Peoples2 + "', a.RiskCode, b.RiskName, "
          + "   (select sum(prem) from LCPol where grpcontno=a.grpcontno and ContPlanCode = a.ContPlanCode and RiskCode = a.RiskCode) "
          + "from LCContPlanRisk a, LMRisk b "
          + "where a.RiskCode = b.RiskCode "
          + "   and a.GrpContNo = '" + fm.all('GrpContNo').value + "' "
          + "   and a.ContPlanCode = '" + contPlanCode + "' "
          + "union "
          + "select '" + contPlanCode + "', '" + contPlanName + "', '" 
          + Peoples3 + "', '" + Peoples2 + "', a.RiskCode, b.RiskName, "
          + "   (select sum(prem) from LBPol where grpcontno=a.grpcontno and ContPlanCode = a.ContPlanCode and RiskCode = a.RiskCode) "
          + "from LBContPlanRisk a, LMRisk b "
          + "where a.RiskCode = b.RiskCode "
          + "   and a.GrpContNo = '" + fm.all('GrpContNo').value + "' "
          + "   and a.ContPlanCode = '" + contPlanCode + "' ";

  turnPage3.pageDivName = "divPage3";
  turnPage3.queryModal(sql, ContPlanRiskGrid);   
}

//在隐藏的数据区域显示所选险种的信息
function setContPlanRiskInfo()
{
  fm.RiskCode.value = ContPlanRiskGrid.getRowColDataByName(ContPlanRiskGrid.getSelNo() - 1, "RiskCode");
}
//选择保险套餐
function showPlan()
{
  var arrResult  = new Array();
  strSQL = "select a.ContPlanCode,a.ContPlanName,a.PlanSql,a.Peoples3,b.RiskCode,b.MainRiskCode from LDPlan a,LDPlanRisk b where a.ContPlanCode = b.ContPlanCode and a.ContPlanCode='"+fm.all("RiskPlan").value+"'";

  var cGrpContNo = fm.all('GrpContNo').value;

  arrResult =  decodeEasyQueryResult(easyQueryVer3(strSQL, 1, 0, 1));
  if(arrResult==null)
  {
    alert("查询保险套餐数据失败！");
  }
  else
  {
    fm.all('ContPlanCode').value = arrResult[0][0];
    fm.all('ContPlanName').value = arrResult[0][1];
    fm.all('PlanSql').value = arrResult[0][2];
    fm.all('Peoples3').value = arrResult[0][3];
    fm.all('RiskCode').value = arrResult[0][4];
    fm.all('MainRiskCode').value = arrResult[0][5];
  }
  //查询该险种下的险种计算要素
  strSQL = "select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti,d.CalFactorValue,d.Remark,b.RiskVer,e.GrpPolNo,d.MainRiskCode,d.CalFactorType,c.CalMode "
           + "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LDPlanDutyParam d,LCGrpPol e "
           + "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
           + "and a.DutyCode = d.DutyCode and a.CalFactor = d.CalFactor and b.RiskVer = d.RiskVersion "
           + "and ContPlanCode = '"+fm.all('RiskPlan').value+"' "
           + "and e.GrpContNO = '"+cGrpContNo+"' and e.RiskCode = d.RiskCode  order by a.RiskCode,d.MainRiskCode,a.DutyCode";

  //fm.all('PlanSql').value = strSQL;
  //alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //原则上不会失败，嘿嘿
  if (!turnPage.strQueryResult)
  {
    alert("查询失败！");
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ContPlanGrid;
  //保存SQL语句
  turnPage.strQuerySql = strSQL;
  //设置查询起始位置
  turnPage.pageIndex = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //调用MULTILINE对象显示查询结果
  displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
  QueryCount = 1;
  tSearch = 1;
}

//计划变更则修改查询状态变量
function ChangePlan()
{
  QueryCount = 0;
  initContPlanDutyGrid();
  initContPlanGrid();
}
function floatRound(myFloat,mfNumber)
{
  var cutNumber = Math.pow(10,mfNumber-1);
  return Math.round(myFloat * cutNumber)/cutNumber;
}

//初始化
function initRiskCode()
{
  initForm();

  fm.all('Peoples3').value="";
  fm.all('RiskCodeName').value="";
  fm.all('SumPrem').value="";
}
//录入管理费及管理费比率
function ShowManageFee()
{

  if(fm.RiskCode.value=="")
  {
    alert("请填入险种代码！");
    return;
  }
  var strSql = "select 1 from LMRiskAccPay where riskcode='"+fm.RiskCode.value+"'";
  var arr = easyExecSql(strSql);
  if(!arr)
  {
    //alert("此险种未描述管理费！");
    return;
  }
  var strSql = "select a.FeeCode,a.FeeName,'',b.InsuAccNo,b.riskcode,b.PayPlanCode from LMRiskFee a,LMRiskAccPay b where a.InsuAccNo=b.InsuAccNo "
               +" and a.PayPlanCode=b.PayPlanCode and b.riskcode='"+fm.RiskCode.value+"'";
  turnPage.queryModal(strSql,ManageFeeGrid);
  divManageFee.style.display='';
}
//管理费查询
function QueryManageFee(ContPlanCode)
{
  var strSql = "select a.FeeCode,b.feename,a.FeeValue,a.InsuAccNo,a.RiskCode,a.PayPlanCode from LCGrpFee a,lmriskfee b where a.grpcontno='"+GrpContNo+"' and a.feecode=b.feecode"
               +" and a.riskcode in (select riskcode from lccontplanrisk where grpcontno='"+GrpContNo+"' and contplancode='"+ContPlanCode+"')";
  var arr=easyExecSql(strSql);
  if(arr)
  {
    displayMultiline(arr, ManageFeeGrid, turnPage);
    divManageFee.style.display='';
  }
  else
  {
    //divManageFee.style.display='';
    ManageFeeGrid.clearData();
  }
}
//添加公共账户，保存类型
//个人账户为每个人的保费，公共账户需要录入
//且不区分保险计划。
function AddPublicAcc()
{
  var ShowInfo = window.open("./PublicAccMain.jsp?GrpContNo="+fm.all('GrpContNo').value,"PublicAccMain",'width='+screen.availWidth*0.7+',height='+screen.availHeight*0.7+',top='+screen.availHeight*0.2+',left='+screen.availWidth*0.2+',toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

//如果选择帐户型险种，添加个人账户金额时，将金额填入责任要素中
function dealPubAcc()
{
  if(fm.AccPrem.value!=null&&fm.AccPrem.value!="")
  {
    //如果存在此金额则表示有个人账户。要将个人账户的金额存入责任中
    var dutyCount=0;
    for(var i=0;i<ContPlanDutyGrid.mulLineCount;i++)
    {
      //首先判断选择几个责任，帐户金额将会分摊到每一个责任上。
      if(ContPlanDutyGrid.getChkNo(i))
      {
        dutyCount++;
      }
    }
    var AccPrem = fm.AccPrem.value/dutyCount;
    AccPrem +="";
    for(var i=0;i<ContPlanGrid.mulLineCount;i++)
    {
      //遍历全部要素为Prem的字段,
      if(ContPlanGrid.getRowColData(i,5)=="Prem")
      {
        ContPlanGrid.setRowColData(i,8,AccPrem);
      }
    }
  }
}

//by gzh 20111221
//约定费率录入按钮
function PlanCodeClick()
{
	var newWindow = window.open("../app/GrpPayPlanMain.jsp?ProposalGrpContNo="+fm.all('ProposalGrpContNo').value+"&Flag=sys");
}

//by zjd 20140728
//保障计划共用保额配置按钮
function ShareAmntClick()
{
  var tContPlanCodeSql = "select 1 from lccontplan where grpcontno = '"+fm.all('GrpContNo').value+"' and contplancode !='11' ";
  //prompt('',tContPlanCodeSql);
  var arr = easyExecSql(tContPlanCodeSql);
  if(!arr){
      alert("请先录入保障计划后，再录入共用保额计划！");
      return false;
  }
  
  //var tcontplancode = ContPlanCodeGrid.getRowColDataByName(ContPlanCodeGrid.getSelNo() - 1, "ContPlanCode");
  if(fm.ContplanCode.value ==""){
      alert("请先选择一条保障计划!");
      return false;
  }
  var tPropsqlSql = "select ProposalGrpContNo from lcgrpcont where grpcontno = '"+fm.all('GrpContNo').value+"'  ";
  var arrno = easyExecSql(tPropsqlSql);
  if(!arrno){
      alert("查询投保单号失败！");
      return false;
  }
  var newWindow = window.open("../app/GrpShareAmntMain.jsp?ProposalGrpContNo="+arrno+"&GrpContNo="+fm.all('GrpContNo').value+"&ContPlanCode="+fm.ContplanCode.value+"&LoadFlag=16");
}

