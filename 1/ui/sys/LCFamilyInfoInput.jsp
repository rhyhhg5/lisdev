<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-02-26 15:55:46
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LCFamilyInfoInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>  
  <%@include file="LCFamilyInfoInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LCFamilyInfoSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 家庭信息表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLCFamilyInfo1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      家庭号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FamilyNo  readonly >
    </TD>
    <TD  class= title>
      备注
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Remark >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      家庭地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HomeAddress >
    </TD>
    <TD  class= title>
      家庭邮编
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HomeZipCode >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      家庭电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HomePhone >
    </TD>
    <TD  class= title>
      家庭传真
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HomeFax >
    </TD>
  </TR>
</table>
    </Div>  
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCFamilyRelaInfo1);">
  		</td>
  		<td class=titleImg>
  			  家庭成员信息
  		</td>
  	</tr>
  </table>
	<Div id="divLCFamilyRelaInfo1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLCFamilyRelaInfoGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
