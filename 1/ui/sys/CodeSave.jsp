<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CodeInput.jsp
//程序功能：
//创建日期：2002-08-16 17:44:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LDCodeSchema tLDCodeSchema   = new LDCodeSchema();

  CodeUI tCodeUI   = new CodeUI();

  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("------transact:"+transact);
  
  //防止跨站脚本攻击 BEGIN By Test At 2011.02.23
  String tCodeType = request.getParameter("CodeType");
  String tCode = request.getParameter("Code");
  String tCodeName = request.getParameter("CodeName");
  String tCodeAlias = request.getParameter("CodeAlias");
  String tComCode = request.getParameter("ComCode");
  String tOtherSign = request.getParameter("OtherSign");
  
  tCodeType = ScriptKeyFilter.CrsSitScptFilter(tCodeType);
  tCode = ScriptKeyFilter.CrsSitScptFilter(tCode);
  tCodeName = ScriptKeyFilter.CrsSitScptFilter(tCodeName);
  tCodeAlias = ScriptKeyFilter.CrsSitScptFilter(tCodeAlias);
  tComCode = ScriptKeyFilter.CrsSitScptFilter(tComCode);
  tOtherSign = ScriptKeyFilter.CrsSitScptFilter(tOtherSign);
  //防止跨站脚本攻击 END By Test At 2011.02.23
  
    tLDCodeSchema.setCodeType(tCodeType);
    tLDCodeSchema.setCode(tCode);
    tLDCodeSchema.setCodeName(tCodeName);
    tLDCodeSchema.setCodeAlias(tCodeAlias);
    tLDCodeSchema.setComCode(tComCode);
    tLDCodeSchema.setOtherSign(tOtherSign);

  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.addElement(tLDCodeSchema);
  
    tCodeUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCodeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

