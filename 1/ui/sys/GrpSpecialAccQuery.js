
var showInfo;
var strTableName1 ;
var strTableName2 ;
var strTableName3 ;
var strTableName4 ;
var strTableName5 ;
var strTableName6 ;

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

//团体理赔账户编码
var grpInsuAccNo = getInsuAccNo("001");

//团体固定账户编码
var grpFixInsuAccNo = getInsuAccNo("004");

//个人理赔账户编码
var grpIndiInsuAccNo = getInsuAccNo("002");

//得到险种的帐户号码
function getInsuAccNo(accType)
{
  var sql = "  select a.InsuAccNo "
            + "from LMRiskToAcc a, LMRiskInsuAcc b "
            + "where a.InsuAccNo = b.InsuAccNo "
	          + "   and b.AccType = '" + accType + "' "
	          + "   and RiskCode = '" + tRiskCode + "' ";
	var rs = easyExecSql(sql);
	if(rs)
	{
	  return rs[0][0];
	}
	
	return null;
}

function GoBack(){
	//工单复用，修改请慎重yangyalin
	if(loadFlag == "TASK")
	{
		top.opener.focus();
		top.close();
	}
	else
	{
		top.opener.easyQueryClick();
		top.close();
	}
}
	

//王珑制作


/*********************************************************************
 *  设置 账户信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function getAccInfo()
 {
 	
	var arrReturn1 = new Array();
	var arrReturn2 = new Array();
	var arrReturn3 = new Array();
	var arrReturn4 = new Array();
	var arrReturn5 = new Array();
	var arrReturn6 = new Array();
	try{
	
		
		if (tContType=='1')
		{
			
			strTableName1 = "lCPol";
			strTableName2 = "LCInsureAcc";
			strTableName3 = "LCContPlanDutyParam";
			strTableName4 = "LCInsured";
			strTableName5 = "LCInsureAccFee";
			strTableName6 = "LCGrpCont";
		}else if (tContType=='2')
		{  
			
			strTableName1 = "lBPol";
			strTableName2 = "LBInsureAcc";
			strTableName3 = "LBContPlanDutyParam";
			strTableName4 = "LBInsured";
			strTableName5 = "LBInsureAccFee";
			strTableName6 = "LBGrpCont"; 
		}

    //个人账户信息
    
    //有效被保人总数
    var strSQL = " select count(*) from " + strTableName4 + " where GrpContNo='" + tContNo + "'"
	  arrReturn5 =easyExecSql(strSQL);          
	  if (!(arrReturn5 == null)) try { fm.all('InsuerCount').value = arrReturn5[0][0]; } catch(ex) { };
		//账户总额
		strSQL = " select sum(InsuAccBala), max(ModifyDate) from " + strTableName2 + " where GrpContNo='" + tContNo + "' and riskcode='" + tRiskCode + "' and InsuAccNo='" + grpIndiInsuAccNo + "' ";
	  arrReturn6 =easyExecSql(strSQL);          
	  if (!(arrReturn6 == null)) 
	  {
	    try { fm.all('AccInsuAccBala').value = arrReturn6[0][0]; } catch(ex) { };
	    try { fm.all('AccInsuModifyDate').value = arrReturn6[0][1]; } catch(ex) { };
	  }
	  
		//团体医疗账户信息
		 strSQL = "select (SELECT INSUACCNAME from  lmriskinsuacc where insuaccno=b.InsuAccNo),b.AccFoundDate,b.State,b.InsuAccBala, b.ModifyDate from " + strTableName1+" a," + strTableName2 +" b" 
		 + " where a.PolTypeFlag='2' and a.riskcode='" + tRiskCode + "'  and a.conttype='2' "
		 + " and a.GrpContNo=b.GrpContNo and a.Polno=b.polno and b.InsuAccNo='" + grpInsuAccNo + "' "
		 + " and a.GrpContNo= '"+tContNo+"'";
		
		arrReturn1 =easyExecSql(strSQL);
		if (!(arrReturn1 == null)) {
		   displayGrpAcc(arrReturn1[0]);
		   //showCodeName();
		}	
    
    //团体固定账户信息
			var strSQL1 = "select (SELECT INSUACCNAME from  lmriskinsuacc where insuaccno=b.InsuAccNo),b.AccFoundDate,b.State,b.InsuAccBala, b.ModifyDate from " + strTableName1+" a," + strTableName2 +" b" 
		 + " where a.PolTypeFlag='2' and a.riskcode='" + tRiskCode + "'  and a.conttype='2' "
		 + " and a.GrpContNo=b.GrpContNo and a.Polno=b.polno and b.InsuAccNo='" + grpFixInsuAccNo + "' "
		 + " and a.GrpContNo= '"+tContNo+"'";
		
		arrReturn2 =easyExecSql(strSQL1);
		if (!(arrReturn2 == null)) {
		   displayGrpFixAcc(arrReturn2[0]);
		   //showCodeName();
		}
		
		//健管账户信息
		var strSQL1 = "select min(b.AccFoundDate), sum(b.InsuAccBala), min(b.ModifyDate) from LMRiskApp a, "+ strTableName2 + " b " 
		 + "where a.RiskCode = b.RiskCode "
		 + "    and b.GrpPolNo= '" + tGrpPolNo + "' "
		 + "    and RiskType = 'M' ";
		arrReturnHealth =easyExecSql(strSQL1);
		if (arrReturnHealth != null && arrReturnHealth[0][0] != null && arrReturnHealth[0][0] != "") {

		   displayGrpHealthAcc(arrReturnHealth[0]);
		   //showCodeName();
		}
		
		var sql = "select DefaultRate from LCGrpInterest where GrpPolNo = '" + tGrpPolNo + "' ";
		var rs = easyExecSql(sql);
		if(rs)
		{
		  fm.Rate.value = rs[0][0];
		}
		
		// 投保账户事项
		//管理费收取方式
    strSQL = "select case when a.CalFactorValue='01' then '一次收取' else '二次收取' end from " + strTableName3 + " a where " 
           + " a.GrpContNo='"+tContNo+"' "
           +" and a.RiskCode='"+tRiskCode+"'"
	         +"  and a.CalFactor='ChargeFeeType'";
	  
	  arrReturn3 =easyExecSql(strSQL);       
	  if (!(arrReturn3 == null)) try { fm.all('ManageFeeType').value = arrReturn3[0][0]; } catch(ex) { };	  
		//团体账户保费
		/*
		strSQL = "select prem from " + strTableName6 + " a where " 
           + " a.GrpContNo='"+tContNo+"' "
    */
    // 现在修改成查询保单的首期实收保费。Modify by fuxin 2007-12-6 18:36 
    strSQL ="select sumduepaymoney from ljapaygrp where grpcontno ='" +tContNo 
    				+"' and paycount=1 "
    				+" and getnoticeno is null" ;
        
	  
	  arrReturn4 =easyExecSql(strSQL);   
	   if (!(arrReturn4 == null)) try { fm.all('GrpAccPrem').value = arrReturn4[0][0]; } catch(ex) { };	      
  
	  //团体帐户管理费金额,
	  calFee();
				
		//管理费定义清单	   
    feeQueryClick();
    
    //特需医疗帐户交易查询
    specQueryClick();
    specHealthTraceGrid();
	}
	catch(ex)
	{
		alert( "请先选择一条非空记录。");
		
	}
 }
 
/*********************************************************************
*  健管账户信息：
*  参数  ：  cArr ：二位数组
*  返回值：  无
*********************************************************************
*/
function displayGrpHealthAcc(arrReturnHealth)
{
	try { fm.all('HealthAccFoundDate').value = arrReturnHealth[0]; } catch(ex) { };
	try { fm.all('HealthAccBala').value = arrReturnHealth[1]; } catch(ex) { };
	
	try { fm.all('HealthAccModifyDate').value = arrReturnHealth[2]; } catch(ex) { };
}

 /*********************************************************************
 *  查询团体医疗账户信息：
 *  参数  ：  cArr ：二位数组
 *  返回值：  无
 *********************************************************************
 */
  function displayGrpAcc(cArr)
  {  
	//内控信息
  	try { fm.all('GrpOwner').value = cArr[0]; } catch(ex) { };
  	try { fm.all('GrpAccFoundDate').value = cArr[1]; } catch(ex) { };
  	try { fm.all('GrpState').value = cArr[2]; } catch(ex) { };
  	try { fm.all('GrpInsuAccBala').value = cArr[3]; } catch(ex) { };
  	try { fm.all('GrpAccModifyDate').value = cArr[4]; } catch(ex) { };
  	
  }
  
  /*********************************************************************
 *  查询固定账户信息
 *  参数  ：  cArr ：二位数组
 *  返回值：  无
 *********************************************************************
 */
  function displayGrpFixAcc(cArr)
  {   
	//内控信息
  	try { fm.all('FixOwner').value = cArr[0]; } catch(ex) { };
  	try { fm.all('FixAccFoundDate').value = cArr[1]; } catch(ex) { };
  	try { fm.all('FixState').value = cArr[2]; } catch(ex) { };
  	try { fm.all('FixAccBala').value = cArr[3]; } catch(ex) { };
  	try { fm.all('FixAccModifyDate').value = cArr[4]; } catch(ex) { };
  	
  }


/*********************************************************************
 *  管理费定义查询
 *  参数  ： 无
 *  返回值：  无
 *********************************************************************
 */

function feeQueryClick()
{
	
	   initManageFeeGrid();
	   var strSQL = "select distinct b.FeeName, (select InsuAccName from LMRiskToAcc where InsuAccNo=b.InsuAccNo), a.FeeValue,case a.DefaultFlag when '0' then '约定管理费' when '1' then '标准管理费' end from LMRiskFee b,LCGrpFee a  where "
		+" b.FeeCode = a.FeeCode and b.InsuAccNo=a.InsuAccNo and b.PayPlanCode=a.PayPlanCode and a.GrpContNo='"+tContNo+"'";
		+ " order by b.FeeName,a.FeeValue "
		 turnPage.queryModal(strSQL, ManageFeeGrid);
	  
  
}


/*********************************************************************
 *  特需医疗帐户交易信息查询
 *  参数  ： 无
 *  返回值：  无
 *********************************************************************
 */

function specQueryClick()
{
  initSpecGrid();
  
  var traceTableName = "";
  
  if(tContType == "1")
  {
    traceTableName = "LCInsureAccTrace";
  }
  else
  {
    traceTableName = "LBInsureAccTrace";
  }
  
  var sql = 
          //契约
            "select MakeDate a, '契约', GrpContNo, "
          + "   (select min(InsuAccName) from LMRiskToAcc where InsuAccNo = a.InsuAccNo), "
          + "   sum(Money), 0 "
          + "from " + traceTableName + " a "
          + "where a.GrpPolNo = '" + tGrpPolNo + "' "
          + "   and OtherType = '1' "
          + "group by MakeDate, GrpContNo, InsuAccNo "
          
          //保全
          + "union "
          + "select (select ConfDate from LPEdorApp where EdorAcceptNo = b.EdorNo) a, "
          + "   (select EdorName from LMEdorItem where EdorCode = b.EdorType and AppObj = 'G'), "
          + "   b.EdorNo, (select min(InsuAccName) from LMRiskToAcc where InsuAccNo = a.InsuAccNo), "
          + "   sum(case when Money > 0 then money else 0 end), "
          + "   abs(sum(case when Money < 0 then money else 0 end)) "
          + "from " + traceTableName + " a, LPGrpEdorItem b "
          + "where a.OtherNo = b.EdorNo "
          + "   and a.GrpContNo = b.GrpContNo "
          + "   and a.GrpPolNo = '" + tGrpPolNo + "' "
          + "group by b.EdorNo, b.EdorType, a.InsuAccNo "
          
          //理赔
          + "union "
          + "select b.MakeDate a, '理赔', a.OtherNo, "
          + "   (select min(InsuAccName) from LMRiskToAcc where InsuAccNo = a.InsuAccNo), "
          + "   sum(case when Money > 0 then money else 0 end), "
          + "   abs(sum(case when Money < 0 then money else 0 end)) "
          + "from " + traceTableName + " a, LJAGetClaim b "
          + "where a.GrpPolNo = b.GrpPolNo "
          + "   and a.OtherNo = b.OtherNo "
          + "   and a.GrpPolNo = '" + tGrpPolNo + "' "
          + "group by b.MakeDate, a.OtherNo, a.InsuAccNo "
          + "order by a ";
  
//  turnPage2.pageDivName = "divPage2";
  turnPage1.queryModal(sql, SpecGrid);  
  fm.LLAccSql.value = sql;
}

/*********************************************************************
 *  特需医疗帐户交易信息查询
 *  参数  ： 无
 *  返回值：  无
 *********************************************************************
 */

function specHealthTraceGrid()
{
  initHealthTraceGrid();
  
  var traceTableName = "";
  
  if(tContType == "1")
  {
    traceTableName = "LCInsureAccTrace";
  }
  else
  {
    traceTableName = "LBInsureAccTrace";
  }
  
  var sql = 
          //契约
            "select MakeDate a, '契约', GrpContNo, "
          + "   (select min(InsuAccName) from LMRiskToAcc where InsuAccNo = a.InsuAccNo), "
          + "   sum(Money), 0 "
          + "from " + traceTableName + " a "
          + "where a.GrpPolNo = '" + tGrpPolNo + "' "
          + "   and OtherType = '1' "
          + "   and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType = 'M') "
          + "group by MakeDate, GrpContNo, InsuAccNo "
          
          //保全
          + "union "
          + "select (select ConfDate from LPEdorApp where EdorAcceptNo = b.EdorNo) a, "
          + "   (select EdorName from LMEdorItem where EdorCode = b.EdorType and AppObj = 'G'), "
          + "   b.EdorNo, (select min(InsuAccName) from LMRiskToAcc where InsuAccNo = a.InsuAccNo), "
          + "   sum(case when Money > 0 then money else 0 end), "
          + "   abs(sum(case when Money < 0 then money else 0 end)) "
          + "from " + traceTableName + " a, LPGrpEdorItem b "
          + "where a.OtherNo = b.EdorNo "
          + "   and a.GrpContNo = b.GrpContNo "
          + "   and a.GrpPolNo = '" + tGrpPolNo + "' "
          + "   and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType = 'M') "
          + "group by b.EdorNo, b.EdorType, a.InsuAccNo "
          
          //理赔
          + "union "
          + "select b.MakeDate a, '理赔', a.OtherNo, "
          + "   (select min(InsuAccName) from LMRiskToAcc where InsuAccNo = a.InsuAccNo), "
          + "   sum(case when Money > 0 then money else 0 end), "
          + "   abs(sum(case when Money < 0 then money else 0 end)) "
          + "from " + traceTableName + " a, LJAGetClaim b "
          + "where a.GrpPolNo = b.GrpPolNo "
          + "   and a.OtherNo = b.OtherNo "
          + "   and a.GrpPolNo = '" + tGrpPolNo + "' "
          + "   and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType = 'M') "
          + "group by b.MakeDate, a.OtherNo, a.InsuAccNo "
          + "order by a ";
  
//  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(sql, HealthTraceGrid);  
}


/*********************************************************************
 *  计算管理费
 *  参数  ： 无
 *  返回值：  无
 *********************************************************************
 */
function calFee()
{
	var pubAcc = fm.all('ManageFeeType').value;	
	
	var strSql = "select AccType, Sum(Fee) from ( "
	            + "   select AccType , Fee from LCInsureAccFee a "
	            + "   where a.GrpContNo = '" + tContNo + "' "
	            + "       and a.riskcode = '" + tRiskCode + "' "
	            + "       and  a.GrpPolNo = '" + tGrpPolNo + "' "
	            + "   union all "
	            + "   select AccType, Fee from LBInsureAccFee a "
	            + "   where a.GrpContNo = '" + tContNo + "' "
	            + "       and a.riskcode = '" + tRiskCode + "' "
	            + "       and  a.GrpPolNo = '" + tGrpPolNo + "' "
	            + ") t "
	            + "group by AccType "
	            + "order by AccType";
	
	var arr = easyExecSql(strSql);
	var InsuAcc = 0;
	if(arr == null ){
	}else{
	  
	  var sumFee = 0;
	  
  	try { 
  	  fm.all('GrpAccFee').value = arr[0][1]; 
  	  sumFee = sumFee + parseFloat(fm.all('GrpAccFee').value);
  	} catch(ex) 
  	{ 
  	  fm.all('GrpAccFee').value = "0";
  	};
  	try { 
  	  fm.all('GrpFixAccFee').value = arr[2][1]; 
  	  sumFee = sumFee + parseFloat(fm.all('GrpFixAccFee').value);
  	} catch(ex) 
  	{ 
  	  fm.all('GrpFixAccFee').value = "0"; 
  	};
  	  
  	try { 
  	  fm.all('InsuredAccFee').value = arr[1][1];
  	  sumFee = sumFee + parseFloat(fm.all('InsuredAccFee').value);
  	} catch(ex) 
  	{ 
  	  fm.all('InsuredAccFee').value = "0";
  	};
  	try { 
  	  fm.all('SumAccFee').value = sumFee; 
  	} catch(ex) { };
	}
	
	strSql = "select sum(a.InsuAccBala) from " + strTableName2  + " a where a.GrpContNo='"
	+tContNo+"' and a.InsuAccNo='"+grpInsuAccNo + "'and a.riskcode='" + tRiskCode + "' and  a.GrpPolNo='" + tGrpPolNo + "'";
		
	var arr1 = easyExecSql(strSql);
	if(arr1 == null ){
	}else{	
  InsuAcc = Math.round(arr1[0][0],2) ;
}
  fm.GrpPubAcc.value = InsuAcc;
  
}
//add lyc #2120
function printf(){
	//	alert(tContType);
		//alert( fm.LLAccSql.value);
	  fm.action = "LLGrpAccPrintf.jsp";
//	  var showStr="正在打开页面，请您稍候...";
//	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//	  fm.target = "_blank";
	  fm.submit();
//	  showInfo.close();
	}

//add lyc #2120