 <%
//程序名称：CollClientBMSave.jsp
//程序功能：
//创建日期：2002-08-19
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%

  //接收信息，并作校验处理。
  //输入参数
  CollClientBMSchema tCollClientBMSchema   = new CollClientBMSchema();
  CollClientBMUI tCollClientBMUI   = new CollClientBMUI();

  //输出参数
  CErrors tError = null;
  String tBmCert = "";
  //后面要执行的动作：添加，修改，删除
  String transact = "";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
//    if(request.getParameter("GrpNo").length()>0)
    tCollClientBMSchema.setGrpNo(request.getParameter("GrpNo"));
//    if(request.getParameter("GrpName").length()>0)
    tCollClientBMSchema.setGrpName(request.getParameter("GrpName"));
//    if(request.getParameter("GrpAddressCode").length()>0)
    tCollClientBMSchema.setGrpAddressCode(request.getParameter("GrpAddressCode"));
//    if(request.getParameter("GrpAddress").length()>0)
    tCollClientBMSchema.setGrpAddress(request.getParameter("GrpAddress"));
//    if(request.getParameter("GrpZipCode").length()>0)
    tCollClientBMSchema.setGrpZipCode(request.getParameter("GrpZipCode"));
//    if(request.getParameter("Phone").length()>0)
    tCollClientBMSchema.setPhone(request.getParameter("Phone"));
//    if(request.getParameter("Fax").length()>0)
    tCollClientBMSchema.setFax(request.getParameter("Fax"));
//    if(request.getParameter("Satrap").length()>0)
    tCollClientBMSchema.setSatrap(request.getParameter("Satrap"));
//    if(request.getParameter("Corporation").length()>0)
    tCollClientBMSchema.setCorporation(request.getParameter("Corporation"));
//    if(request.getParameter("EMail").length()>0)
    tCollClientBMSchema.setEMail(request.getParameter("EMail"));
//    if(request.getParameter("LinkMan").length()>0)
    tCollClientBMSchema.setLinkMan(request.getParameter("LinkMan"));
//    if(request.getParameter("Peoples").length()>0)
    tCollClientBMSchema.setPeoples(request.getParameter("Peoples"));
//    if(request.getParameter("BankCode").length()>0)
    tCollClientBMSchema.setBankCode(request.getParameter("BankCode"));
//    if(request.getParameter("BankAccNo").length()>0)
    tCollClientBMSchema.setBankAccNo(request.getParameter("BankAccNo"));
//    if(request.getParameter("BusinessType").length()>0)
    tCollClientBMSchema.setBusinessType(request.getParameter("BusinessType"));
//    if(request.getParameter("GrpNature").length()>0)
    tCollClientBMSchema.setGrpNature(request.getParameter("GrpNature"));
//    if(request.getParameter("FoundDate").length()>0)
    tCollClientBMSchema.setFoundDate(request.getParameter("FoundDate"));
//    if(request.getParameter("GrpGroupNo").length()>0)
    tCollClientBMSchema.setGrpGroupNo(request.getParameter("GrpGroupNo"));
//    if(request.getParameter("State").length()>0)
    tCollClientBMSchema.setState(request.getParameter("State"));
//    if(request.getParameter("Remark").length()>0)
    tCollClientBMSchema.setRemark(request.getParameter("Remark"));
//    if(request.getParameter("BlacklistFlag").length()>0)
    tCollClientBMSchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));
//    if(request.getParameter("Operator").length()>0)
    tCollClientBMSchema.setOperator(request.getParameter("Operator"));
    transact=request.getParameter("Transact");

System.out.println("Peoples"+tCollClientBMSchema.getPeoples());
  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tCollClientBMSchema);
    
   //执行动作：INSERT 添加纪录，UPDATE 修改纪录，DELETE 删除纪录
   tCollClientBMUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCollClientBMUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = transact+" 成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = transact+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

