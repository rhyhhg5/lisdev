<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<html> 
<head> 
	<%

    int serverCount = 0;

    String serverListSql = "select * from LDCode where codetype = 'server' order by code desc";
    LDCodeDB tLDCodeDB = new LDCodeDB();
    LDCodeSet tLDCodeSet = tLDCodeDB.executeQuery(serverListSql);
    if(tLDCodeSet != null && tLDCodeSet.size() > 0){
        serverCount = tLDCodeSet.size();
    }

    String serverLength = "*";
    for(int j=1;j<serverCount;j++){
        serverLength += ",*";
    }

%>
  <!--<script src="../sys/ViewMemory.js"></script>-->
  <!--<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>-->
<title>系统状态</title> 

</head> 

<frameset name="fraMainView" rows="8%,*" frameborder="no" border="1" framespacing="0">
	<frameset name="fraTitleView" cols="<%=serverLength%>" frameborder="yes" border="1" framespacing="0">
	
	  <%
	  for(int i=1;i<=serverCount;i++){
	      String serverName = tLDCodeSet.get(i).getCodeName();
	      out.println("<frame id='fraTitleView"+i+"' name='fraTitle"+i+"' scrolling='auto' src='../sys/ViewServerName.jsp?serverName="+serverName+"' noresize />");
	  }
	  %>
	</frameset>
	<frameset name="fraSetView" cols="<%=serverLength%>" frameborder="yes" border="1" framespacing="0">
	
	  <%
	  for(int i=1;i<=serverCount;i++){
	      String serverAddress = tLDCodeSet.get(i).getCodeAlias();
	      String serverName = tLDCodeSet.get(i).getCodeName();
	      
	      out.println("<frame id='fraNextView"+i+"' name='fraNext"+i+"' scrolling='auto' src='"+serverAddress+"/sys/ViewServerInfo.jsp?serverName="+serverName+"' noresize />");
	  }
	  %>
	</frameset>

<!--</frameset>-->

<noframes>
    <body bgcolor="#FFFFFF">
        <br><br><br><br><br>
        <center>
            <font size="2">对不起，您的浏览器不支持框架网页。</font>
            <br><br>
            <font size="2">请使用 IE 5.0 或其以上浏览本系统。</font>
        </center>
    </body>
</noframes>

</html>
