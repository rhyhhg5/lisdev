<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
	String tPolNo = "";
	try
	{
		tPolNo = request.getParameter("PolNo");
	}
	catch( Exception e )
	{
		tPolNo = "";
	}
	String tflag = "";
		try
	{
		tflag = request.getParameter("flag");
		
		
	}
	catch( Exception e )
	{
		tflag = "";
	}
%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>   
<script>
var tflag = "<%=tflag%>"
var GrpPolNo = "<%=tPolNo%>"
var comCode = <%=tG.ComCode%> 
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="AllGBqQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AllGBqQueryInit.jsp"%>
  <title>保全查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
	<table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入集体保全查询条件：（当保全受理号有值时，查询将不受时间限制）</td>
		</tr>
	</table>
    <table  class= common align=center>
   
      <TR  class= common>
      	<TD  class= title> 保全受理号</TD>
        <TD  class= input> 
            <Input class= common name=EdorNo >
        </TD> 
        <TD  class= title> 客户号码</TD>
        <TD  class= input> 
            <Input class= common name=GrpContNo >
        </TD>
        <td class=title>申请人姓名</td>
        <td class= input>
            <Input type="input" class="common" name=EdorAppName>
        </td>
      </TR>
     <% if (tflag==null){
 	 %>
      <TR  class= common>
        <td class=title>申请方式</td>
        <td class= input >
            <Input class= "codeno" name=AppType ondblclick="return showCodeList('AcceptWayNo',[this,AppTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AcceptWayNo',[this,AppTypeName],[0,1],null,null,null,1);" ><Input class=codename style="width:80" readonly name=AppTypeName>
        </td>
        <TD  class= title>
              保全申请日期起期
          </TD>
          <TD  class= input>
              <Input class= "coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>
              保全申请日期止期
          </TD>
          <TD  class= input>
              <Input class= "coolDatePicker" dateFormat="short" name=EndDate >
          </TD>
      </TR>
      <TR>
		    <TD  class= title> 管理机构 </TD>
        <TD  class= input>
          <Input class= "codeno" name=ManageCom value="<%=tG.ComCode%>" verify="管理机构|notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename style="width:80" readonly name=ManageComName>
        </TD>
		    <TD  class= title> 受理人代码 </TD>
        <TD  class= input>
          <Input class= "codeno" name=Acceptor ondblclick="return showCodeList('memberingroup',[this,AcceptorName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('memberingroup',[this,AcceptorName],[0,1],null,null,null,1);" ><Input class=codename style="width:80" readonly name=AcceptorName>
        </TD>
        <TD  class= title> 保全项目类别 </TD>
        <TD  class= input>
            <Input class= "codeno" name=EdorType ondblclick="return showCodeList('edortypeg',[this,EdorTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('edortypeg',[this,EdorTypeName],[0,1],null,null,null,1);" ><Input class=codename style="width:80" readonly name=EdorTypeName>
        </TD>
      </TR>
      <%}
      %>
    </table>
    <% if (tflag==null){
 	%>
      <INPUT VALUE="查  询" class = cssbutton TYPE=button onclick="easyQueryClick();"> 
 <%}
 %>       
 <br>
  	<Div  id= "divLPGrpEdorMain1" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" >
						</span> 
					</td>
				</tr>
			</table>
			<Div id= "divPage2" align=center style= "display: '' ">
			  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage(); setEdorName();"> 
			  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage(); setEdorName();"> 					
			  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage(); setEdorName();"> 
			  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage(); setEdorName();"> 
			</Div>				
  	</div>
		<INPUT class= cssbutton VALUE="明细查询" TYPE=button onclick="PrtEdor();">     			
		<INPUT class= cssbutton VALUE="项目查询" TYPE=hidden onclick="ItemQuery();">     	 
		<INPUT class= cssbutton VALUE="该批单所附清单" TYPE=button onclick="showInsuredList();">	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>