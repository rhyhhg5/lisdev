<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskPersonalBox.jsp
//程序功能：工单管理个人信箱主界面
//创建日期：2005-01-15 14:10:36
//创建人  ：qiuyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
	String sCustomer = "";
	try
	{
		sCustomer = request.getParameter("Customer");
		System.out.println("sCustomer="+sCustomer);
	}
	catch( Exception e )
	{
		sCustomer = "";
	}
%>
<head >
<script>
	var tCustomer = "<%=sCustomer%>"; 
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskPersonalBox.js"></SCRIPT>
  <%@include file="TaskPersonalBoxInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body  onload="initForm();" >
 <form name=fm action="./TaskPersonalBoxSave.jsp" target=fraSubmit method=post>
 <Input type="hidden" class= common name="WorkNo">
 <Input type="hidden" class= commpn name="AcceptNo">
 <!-- 用CheckBox隐藏域来标记是否使用CheckBox传参数 -->
 <Input type="hidden" class= commpn name="CheckBox" value="YES">
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskList);"> 
      </td>
      <td width="185" class= titleImg>保全申请 </td>
      <td><INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewTask('0');"></td>
    </tr>
  </table>
  <Div  id= "divTaskList" style= "display: ''" align=center>  
  	 <table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanTaskGrid" >
				</span> 
		  	</td>
		</tr>
	  </table>
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();"> 
  </Div>
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskList);"> 
      </td>
      <td width="185" class= titleImg>咨询/投诉申请 </td>
      <td><INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewTask('1');"></td>
    </tr>
  </table>
  <Div  id= "divTaskList" style= "display: ''" align=center>  
  	 <table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanTask1Grid" >
				</span> 
		  	</td>
		</tr>
	  </table>
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();"> 
  </Div>
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskList);"> 
      </td>
      <td width="185" class= titleImg>理赔申请 </td>
      <td><INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewTask('2');"></td>
    </tr>
  </table>
  <Div  id= "divTaskList" style= "display: ''" align=center>  
  	 <table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanTask2Grid" >
				</span> 
		  	</td>
		</tr>
	  </table>
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
  </Div>
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskList);"> 
      </td>
      <td width="185" class= titleImg>投保申请 </td>
      <td><INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewTask('3');"></td>
    </tr>
  </table>
  <Div  id= "divTaskList" style= "display: ''" align=center>  
  	 <table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanTask3Grid" >
				</span> 
		  	</td>
		</tr>
	  </table>
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage();"> 
  </Div>
  </Form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
