//程序名称：ContrlUrgeNoticeInput.js
//程序功能：催办控制
//创建日期：2003-07-16 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initUrgeNoticeInputGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    alert(content); 
  }
  else
  { 
  	alert(content); 
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initUrgeNoticeInputGrid();
	
	// 书写SQL语句
	k++;
	var strSql = "";
	
    strSql="select a.PrtSeq,a.code,a.stateflag,b.polapplydate,b.cvalidate,b.appntname,b.prem,getUniteCode(a.agentcode),a.ManageCom,a.ForMakeDate, "
          +" b.SaleChnl, b.AgentCom "
          +" from loprtmanager a,lccont b where a.otherno = b.contno"
          +" and a.code in ('03', '05','07','85')"
          +" and a.stateflag in ('1','3')"
          +" and b.appflag='0' and b.uwflag not in ('1','2','a')"
          +" and a.ManageCom like '" + manageCom +"%%'"
          + getWherePart('b.PrtNo','PrtNo')
          + getWherePart('b.AppntName','AppntName')
          + getWherePart('a.MakeDate','MakeDate')
          + getWherePart('b.PolApplyDate','PolApplyDate')
          + getWherePart('a.ManageCom','ManageCom')
          + getWherePart('b.cvalidate','CvaliDate')
          + getWherePart('getUniteCode(a.agentcode)','AgentCode')
          + getWherePart('a.Code','NoticeType')
          + getWherePart('a.stateflag','State')
          + getWherePart("b.SaleChnl","SaleChnlCode")
          + getWherePart("b.AgentCom","AgentComBank")
          + "order by a.code,a.makedate";

	  //查询SQL，返回结果字符串
	  //alert(strSql);
	  turnPage.pageLineNum = 50;
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("目前没有待催发的通知书!");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = UrgeNoticeInputGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  fm.querySql.value = strSql;
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolFeeGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				UrgeNoticeInputGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}


/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,*,0,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function Commit()
{	
	var tSel = UrgeNoticeInputGrid.getSelNo();

  if( tSel == 0 || tSel == null )
  {
		alert("请选择一条记录！" );
		return;
	}
	fm.PrtSeq.value = UrgeNoticeInputGrid.getRowColData(tSel - 1,1);	
  var i = 0;
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      
  fm.action="./ContrlUrgeNoticeSave.jsp"
  fm.submit(); //提交
}

/**
 * 下拉框，选定后触发事件。
 */
function afterCodeSelect(tCodeName, tObj)
{
    if (tCodeName == "lcsalechnl")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    showInfo=window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
    //alert(showInfo.fm);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
    }
}

function downloadClick()
{
    if(UrgeNoticeInputGrid.mulLineCount==0)
    {
        alert("列表中没有数据可下载");
    }
    var oldAction = fm.action;
    fm.action = "ContrlUrgeNoticeDownload.jsp";
    fm.submit();
    fm.action = oldAction;
}