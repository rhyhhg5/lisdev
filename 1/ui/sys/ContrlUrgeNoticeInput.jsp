<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ContrlUrgeNoticeInput.jsp
//程序功能：催办控制
//创建日期：2006-01-10 11:10:36
//创建人  ：张星
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	
%>
<script>
	var operator = "<%=tGI.Operator%>";     //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="ContrlUrgeNoticeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ContrlUrgeNoticeInit.jsp"%>
  <title>发催办通知书 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./ContrlUrgeNoticeSave.jsp">
    <!-- 催办通知书信息部分 -->
    <!--%@include file="../common/jsp/OperateButton.jsp"%-->
    <!--%@include file="../common/jsp/InputButton.jsp"%-->
   <table  class= common align=center>
      	<TR  class= common>
          <!--<TD  class= title>  投保单号码   </TD>
          <TD  class= input> --> <Input class= common name=ContNo  type="hidden">
          <TD  class= title>  印刷号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo verify="印刷号码|int&len=11"> </TD>
          <TD  class= title> 投保人</TD>
          <TD  class= input><Input class="common" name=AppntName verify="投保人|len<=20"></TD>
          <TD  class= title> 下发日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=MakeDate verify="下发日期|date">   </TD>
         </TR> 
         <TR  class= common>
      	  <TD  class= title> 申请日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=PolApplyDate verify="申请日期|date">   </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
         
          <TD  class= title>  生效日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=CvaliDate verify="生效日期|date"> </TD>
         
         </TR> 
         <TR>
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
          <TD  class= title>  类  型  </TD>
		  <TD  class= Input><input class=codeno name=NoticeType verify="操作类型|NOTNULL" CodeData="0|4^03|体检通知书^05|承保计划变更通知书^07|缴费通知书^15|问题件通知书" ondblclick="return showCodeListEx('NoticeType',[this,NoticeTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('NoticeType',[this,NoticeTypeName],[0,1],null,null,null,1);"><input class=codename name=NoticeTypeName></TD>
          <TD  class= title>  状 态  </TD>
		  <TD  class= Input><input class=codeno name=State verify="状态|NOTNULL" CodeData="0|3^1|未催办^3|已催办^4|催办过期" ondblclick="return showCodeListEx('State',[this,StateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('State',[this,StateName],[0,1],null,null,null,1);"><input class=codename name=StateName></TD>
         </TR>
         
         <tr>
         <td class="title8">渠道</td>
         <td class="input8"><input class="codeNo" name="SaleChnlCode" ondblclick="return showCodeList('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" /></td>
         <td class="title8">网点</td>
         <td class="input8"><input id="AgentComBank" class="code" name="AgentComBank" style="display:none;" ondblclick="return queryAgentComBank();" /></td>
         </tr>
         
    </table>
    <p>
    <INPUT VALUE="查  询" class= CssButton  TYPE=button onclick="easyQueryClick();"> 
    <INPUT class=cssbutton VALUE="清单下载" TYPE=button onclick="downloadClick();"> 
    <INPUT  type= "hidden" class= Common name= querySql >
    </p>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUrgeNoticeInput1);">
    		</td>
    		<td class= titleImg>
    			 通知书信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divUrgeNoticeInput1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanUrgeNoticeInputGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<P>
   <td class ="title">指定催办日期</td>
	 <td class ="input" >
	 <input NAME="UrgeDate"  class="coolDatePicker"  verify="催办日期|date">
   </td>
   </P>
  	
  	<p>
      <INPUT VALUE="确  定" class= CssButton TYPE=button onclick= "Commit();">     
  	</p>
    
  	<Div  id= "divUrgeNotice2Input" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanUrgeNotice2InputGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
    </div>

    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="PrtSeq" name="PrtSeq">
   
          
    </Div>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
