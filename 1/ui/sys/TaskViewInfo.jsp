<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskViewInfo.jsp
//程序功能：工单管理工单查看页面
//创建日期：2005-03-22 13:21:37
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskViewInfo.js"></SCRIPT>
  <%@include file="TaskViewInfoInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单查看</title>
</head>
<body onload="initForm();">
	<form action="" name="fm" target=fraSubmit method=post>
	<%@include file="TaskCommonView.jsp"%>
	<%@include file="TaskCommonHistory.jsp"%>
    <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divScanlist);">
	      </td>
        <td class= titleImg>扫描件 </td>
        <td class=common>(共X件)</td>
	    </tr>
    </table>	
     <Div  id= "divScanlist" style= "display: ''"> 
	  	<table class= common>
	   		<tr class= common>
	  	  		<td text-align: left colSpan=1>
					<span id="spanScanGrid" >
					</span> 
			  	</td>
			</tr>
	</table>
	</div>
	<br>
    <input type="button" class=cssButton name="Return" value="返  回" onclick="javascript:location.href='TaskPersonalBox.jsp'">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
