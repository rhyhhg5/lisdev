<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

  if(globalInput == null) {
	out.println("session has expired");
	return;
}

String strOperator = globalInput.Operator;
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">

function initInpBox()
{
  try
  {
  
    fm.all('BlacklistCode').value = '';
    fm.all('Name').value = '';
}
  catch(ex)
  {
  	
    alert("在BlackListPersonQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initEvaluateGrid();
    easyQueryClick();
  }
  catch(re)
  {
    alert("BlackListPersonQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 var EvaluateGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initEvaluateGrid()
{

    var iArray = new Array();
  try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px";            		//列宽
      iArray[0][2]=4;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]=" 人员编号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=40;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="姓名";      		//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;
      
      iArray[3]=new Array();
      iArray[3][0]="曾用名";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=40;            			//列最大值
      iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="国籍";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=40;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="证件类型";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=40;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="证件号码";         		//列名
      iArray[6][1]="150px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     iArray[7]=new Array();
      iArray[7][0]="风险等级";         		//列名
     iArray[7][1]="80px";            		//列宽
     iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="发布机构";      		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="类别";         		//列名
      iArray[9][1]="150px";            		//列宽
      iArray[9][2]=40;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="备注";      		//列名
      iArray[10][1]="300px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;                  //是否允许输入,1表示允许，0表示不允许
      
      EvaluateGrid = new MulLineEnter( "fm" , "EvaluateGrid" );
      //EvaluateGrid.selBoxEventFuncName = "getPara";
      //这些属性必须在loadMulLine前
      EvaluateGrid.displayTitle = 1;
      EvaluateGrid.mulLineCount = 0;
      EvaluateGrid.hiddenPlus=1;
      EvaluateGrid.hiddenSubtraction=1;
      EvaluateGrid.canSel = 1;
      EvaluateGrid.canChk =0;  // 1为显示CheckBox列，0为不显示 (缺省值)
      EvaluateGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //EvaluateGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
