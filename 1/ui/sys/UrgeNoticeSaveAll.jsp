<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UrgeNoticeSaveAll.jsp
//程序功能：发催办通知书
//创建日期：2003-07-16 11:10:36
//创建人  ：SXY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  
  //获取信息
  VData mResult = null;
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  System.out.println("UrgeNoticeSaveAll-----Begin!");
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
	
  	
      //向后台提出处理请求	
   try
     {
  		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tG );
		
		// 数据传输
		System.out.println("tUrgeNoticeUI.submitData------Begin");
		UrgeNoticeUI tUrgeNoticeUI = new UrgeNoticeUI();
		if (tUrgeNoticeUI.submitData(tVData,"INSERT") == false)
		{
			int n = tUrgeNoticeUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tUrgeNoticeUI.mErrors.getError(i).errorMessage);
			Content = " 查询失败，原因是: " + tUrgeNoticeUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tUrgeNoticeUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	mResult = tUrgeNoticeUI.getResult();
    	        String note = (String)(mResult.getObjectByObjectName("String",0));                        
                Content = " 成功发放了" + note + "条催办通知书! ";
    	        FlagStr = "Success";
		    	
		    	
%>
				<script language="javascript">					
					parent.fraInterface.UrgeNoticeInputGrid.clearData ();				
				</script>         				
<%
		    
		    }
		    else                                                                           
		    {
		        Content = " 保存失败，原因是:" + tError.getFirstError();
    	        FlagStr = "Fail";
		    }
		}
	else
	{
		Content = "若为所选通知书发通知书,请点击'催发当前所有通知书'按钮;为所有显示的通知书发通知书,请点击'催发当前所有通知书'按钮";
	}  
}
catch( Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
