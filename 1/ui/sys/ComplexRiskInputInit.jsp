<%
//程序名称：ComplexRiskInputInit.jsp
//程序功能：功能描述
//创建日期：2005-05-30 18:29:02
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
  }
  catch(ex) {
    alert("在ComplexRiskInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
		initComplexRiskGrid();
		showRiskInfo();
  }
  catch(re) {
    alert("ComplexRiskInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ComplexRiskGrid;
function initComplexRiskGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();                    
		iArray[1][0]="被保险人";         		//列名    
		iArray[1][1]="30px";         		//列名    
		iArray[1][3]=0;         		//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="险种代码";         		//列名    
		iArray[2][1]="30px";         		//列名    
		iArray[2][3]=0;         		//列名        	
		
		iArray[3]=new Array();                    
		iArray[3][0]="险种名称";         		//列名    
		iArray[3][1]="150px";         		//列名    
		iArray[3][3]=0;         		//列名   
		
		iArray[4]=new Array();  
		iArray[4][0]="保险期间";
		iArray[4][1]="20px";   
		iArray[4][3]=0;         


		iArray[5]=new Array();  
		iArray[5][0]="档次";
		iArray[5][1]="20px";   
		iArray[5][3]=0;  
		
		iArray[6]=new Array();  
		iArray[6][0]="保额";
		iArray[6][1]="20px";   
		iArray[6][3]=0;  
		
		iArray[7]=new Array();  
		iArray[7][0]="期缴保费";
		iArray[7][1]="20px";   
		iArray[7][3]=0;  
		     
    
    ComplexRiskGrid = new MulLineEnter( "fm" , "ComplexRiskGrid" ); 
    //这些属性必须在loadMulLine前           
    
    ComplexRiskGrid.mulLineCount = 0;   
    ComplexRiskGrid.displayTitle = 1;
    ComplexRiskGrid.hiddenPlus = 1;
    ComplexRiskGrid.hiddenSubtraction = 1;
    ComplexRiskGrid.canSel = 0;
    ComplexRiskGrid.canChk = 0;
    //ComplexRiskGrid.selBoxEventFuncName = "showOne";
    ComplexRiskGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ComplexRiskGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
