
// 查询按钮
//此函数根据3个录入框查询响应的值


/*----------第一个MulLine查询------------------*/

function queryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";

	/*
	strSQL = "select distinct a.caseno, (select riskname from lmrisk where riskcode=a.riskcode),"
		+" case when b.appflag='1' then '缴费有效' else '无效' end,"
		+" b.InsuredNo, b.insuredname, c.rgtdate, b.PolNo"
		+" from llclaimpolicy a,lcpol b,llcase c "
		+" where a.polno=b.polno and c.caseno=a.caseno" 
		+" order by c.rgtdate desc";
		*/
	
	var contParam = fm.all('contNoParam').value;
	
	strSQL = "select distinct a.CaseNo 报案登记号, (select riskname from lmrisk where riskcode=b.riskcode) 险种名称,"
		+ " (case when b.appflag='1' and c.AccDate>=b.cvalidate and c.AccDate<=b.enddate then '缴费有效' else '险种无效' end) 险种状态,"
		+ " a.CustomerNo 出险人客户号, a.CustomerName 出险人姓名 ,"
		+ " a.rgtdate 受理日期, ContNo 个人保单号, a.rgtno,"
		+ " (select codename from ldcode where code=a.rgtstate and codetype='llrgtstate') 案件状态,"
		+ " a.rgtNo"
		+ " from llcase a, lcpol b, llsubreport c,llCaseRela d"
		+ " where a.CustomerNo=b.InsuredNo and c.SubRptNo=d.SubRptNo and  d.caseno=a.caseno and a.customerno=b.Insuredno"
		+ " and contno='" + contParam + "'"
		+ " union all "
		+ " select distinct a.CaseNo 报案登记号, (select riskname from lmrisk where riskcode=b.riskcode) 险种名称,"
		+ " (case when b.appflag='1' and c.AccDate>=b.cvalidate and c.AccDate<=b.enddate then '缴费有效' else '险种无效' end) 险种状态,"
		+ " a.CustomerNo 出险人客户号, a.CustomerName 出险人姓名 ,"
		+ " a.rgtdate 受理日期, ContNo 个人保单号, a.rgtno,"
		+ " (select codename from ldcode where code=a.rgtstate and codetype='llrgtstate') 案件状态,"
		+ " a.rgtNo"
		+ " from llcase a, lbpol b, llsubreport c,llCaseRela d"
		+ " where a.CustomerNo=b.InsuredNo and c.SubRptNo=d.SubRptNo and  d.caseno=a.caseno and a.customerno=b.Insuredno"
		+ " and contno='" + contParam + "'"
;

	//alert(strSQL); alert(easyExecSql(strSQL));
	//easyExecSql()在EasyQueryVer3.js中。
	
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //!!!这是查询函数
  
  /*
  divHealthInfo.style.display='none';
  initHealthInfoGrid();
  /*************************************/
   
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有相关理赔信息！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.strQueryResult 查询结果字符串
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //showCodeName();
}


// 数据返回父窗口
function returnParent()
{
	//alert(tDisplay);
	if (tDisplay=="1")
	{
	    var arrReturn = new Array();
	    var tSel = PolGrid.getSelNo();
	    //alert(tSel);
	    if( tSel == 0 || tSel == null )
	    	alert( "请先选择一条记录，再点击返回按钮。" );
	    else
	    {
	    	try
	    	{
	    		arrReturn = getQueryResult();
	    		
	    		top.opener.afterQuery( arrReturn );
	    		
	    		top.close();
	    	}
	    	catch(ex)
	    	{
	    		alert( "请先选择一条非空记录，再点击返回按钮。");
	    	}
	    	
	    }
	 }
	 else
	 {
	    top.close();
	 }
}

function claimDetailInfo()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	var caseNo;
	
	if(tSel==0 || tSel==null)
	{
		alert("请先选择一条记录，再点击返回按钮。");
	}
	else
	{
	 		divClaimDetail.style.display="";
	 		
			var caseNo    		 = PolGrid.getRowColData(tSel-1, 1);
			var riskName 			 = PolGrid.getRowColData(tSel-1, 2);
			var riskState			 = PolGrid.getRowColData(tSel-1, 3);
			var riskPersonNo 	 = PolGrid.getRowColData(tSel-1, 4);
			var riskPersonName = PolGrid.getRowColData(tSel-1, 5);
			var rgtDate				 = PolGrid.getRowColData(tSel-1, 6);
			var cont					 = PolGrid.getRowColData(tSel-1, 7);
			var rgtno					 = PolGrid.getRowColData(tSel-1, 8);
			var caseState  		 = PolGrid.getRowColData(tSel-1, 9);
			var rgtNo					 = PolGrid.getRowColData(tSel-1, 10);
			
			//var f= PolGrid.getRowColData(tSel-1, 7);

			//PolGrid是MulLine中MulLineEnter的一个对象，而 getRowColData是MulLine中_GetRowColData（）函数
			//_GetRowColData（cRow,cCol,cObjInstance）函数:得到指定行列的数据,输入：cRow:行,cCol: 列,cObjInstance:Muline对象
	    
	    fm.all('caseNo').value				 = caseNo;  
		  fm.all('riskName').value			 = riskName;                            
		  fm.all('riskState').value			 = riskState;                             
		  fm.all('riskPersonNo').value   = riskPersonNo;
		  fm.all('riskPersonName').value = riskPersonName;
			fm.all('rgtDate').value				 = rgtDate;
			fm.all('requestDate').value		 = rgtDate;
			fm.all('caseState').value      = caseState;
			
			displayClaimInfo(caseNo);
			/**************************************************************************/
			/****************************************************************************/
	}
}

function displayClaimInfo(rgtNo,caseNo)
{
		/*---------------------------------strSQL1 查询 赔付金额--------------------------------*/
    var strSQLClaim1="select sum(a.realpay) 赔付金额 from llclaimdetail a where a.caseno='"+ rgtNo +"'";
    
    var arrResult1 = new Array();

    arrResult1=easyExecSql(strSQLClaim1); 
		
    
    if(arrResult1==""||arrResult1==null)
    {
			try{fm.all('ClaimMoney').value = "";}catch(ex){}; 	
			//try{fm.all('caseState').value	 = "";}catch(ex){};
    }
  	else
  	{
			try{fm.all('ClaimMoney').value = arrResult1[0][0];}catch(ex){};
			//try{fm.all('caseState').value	 = arrResult1[0][0];}catch(ex){};
		}
		
		/*--------------------------strSQL2 查询财务到帐日期，给付方式---------------------------*/
		
		var strSQL2="select g.confDate, "
			+ " (select c.codename from ldcode c where c.code = g.PayMode or c.codetype='paymode')"
			+ "from ljaget g where g.otherno='" + rgtNo + "' and g.otherno='" + caseNo + "'"
			;
		
		var arrResult3 = new Array();
		arrResult3=easyExecSql(strSQL2);
		//alert(arrResult2);
		//alert(arrResult2[0]);
		
		
		if(arrResult3==''||arrResult3==null)
		{
			try{fm.all('payDate').value	  = '暂未给付';}catch(ex){};
			try{fm.all('payManner').value	  = '现金';}catch(ex){};
		}
		else
		{
			try{fm.all('payDate').value	  = arrResult3[0][0];}catch(ex){};
			try{fm.all('payManner').value	  = arrResult3[0][1];}catch(ex){};
		}
		
		/*---------------------strSQL查询 给付责任 ---------------------------------*/
		queryClickDuty(rgtNo);
		
		/*-----------------查询险种状态--------------------*/
		
		
		/*-----------------strSQL查询 出险信息---------------------*/
		var strSQL= "select a.accdate 出险日期, a.accdesc 出险描述 "
			+ " from llsubreport a, llcaserela b "
			+ " where a.subrptno=b.SubRptNo and b.CaseNo='" + rgtNo + "' "
			;
			
		var arrResult4 = easyExecSql(strSQL);
		//alert("arrResult4" + arrResult4);
		var strRisk ="";
		if(arrResult4==null||arrResult4=='')
		{
			fm.all("riskInfo").value= "";
		}
		else
		{
			for(var i=0; i<arrResult4.length; i++)
			{
				strRisk= strRisk + "第"+(i+1)+ "出险事件：  出险日期 : " + arrResult4[i][0]+"     出险描述 : " + arrResult4[i][1]+ "\n";
			}
			fm.all("riskInfo").value= strRisk;
		}
		
}

/*----------第二个MulLine查询------------------*/

function queryClickDuty(rgtNo)
{
	// 初始化表格
	initPolGridDuty();
	
	// 书写SQL语句
	var strSQL = "";

	
	var strSQL = "select ContNo 保单号, RiskCode 险种号, b.getdutyname 给付责任 ,"
	   + " a.GiveTypeDesc 赔付结论 ,a.GiveReasonDesc 拒付原因,a.RealPay 赔付金额 "
	   + " from LLClaimdetail a, LMDutyGetClm b where  a.caseno='"+ rgtNo +"' "
	   + " and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind "
	;
	//var arrResult = easyExecSql(strSQL);
		
	//alert(strSQL); alert(easyExecSql(strSQL));
	//easyExecSql()在EasyQueryVer3.js中。
	
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //!!!这是查询函数
  
  /*
  divHealthInfo.style.display='none';
  initHealthInfoGrid();
  /*************************************/
   
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert("查询失败！");
    divAllGive.style.display='none';
    //fm.all('divGiveDuty').display='none';
    return false;
  }
 	divAllGive.style.display=''; 
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.strQueryResult 查询结果字符串
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGridDuty;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //showCodeName();
}

//第三个MulLine查询

function riskState()
{
	
}

function closeWindow()
{
	window.close();
	return;
}
