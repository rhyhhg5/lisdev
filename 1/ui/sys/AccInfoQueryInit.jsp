<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<% 
   String nowDate = PubFun.getCurrentDate();
   //String nowYear = nowDate.substring(0, nowDate.indexOf("-"));
   //String nowMonth = nowDate.substring(nowDate.indexOf("-") + 1,nowDate.lastIndexOf("-"));
   //String nowDay = nowDate.substring(nowDate.lastIndexOf("-") + 1);
%>
<script language="JavaScript">
function initAccGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="产品代码";
    iArray[1][1]="100px";
    iArray[1][2]=60;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="产品名称";
    iArray[2][1]="160px";
    iArray[2][2]=60;
    iArray[2][3]=0;


    iArray[3]=new Array();
    iArray[3][0]="账户名称";
    iArray[3][1]="90px";
    iArray[3][2]=200;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="账户金额";
    iArray[4][1]="70px";
    iArray[4][2]=200;
    iArray[4][3]=0;	
    
    iArray[5]=new Array();
    iArray[5][0]="结息日期";
    iArray[5][1]="70px";
    iArray[5][2]=200;
    iArray[5][3]=0;	
    
    iArray[6]=new Array();
    iArray[6][0]="被保人姓名";
    iArray[6][1]="60px";
    iArray[6][2]=200;
    iArray[6][3]=0;	
    
    iArray[7]=new Array();
    iArray[7][0]="合同号";
    iArray[7][1]="60px";
    iArray[7][2]=200;
    iArray[7][3]=3;	
    iArray[8]=new Array();
    iArray[8][0]="交费计划名称";
    iArray[8][1]="60px";
    iArray[8][2]=200;
    iArray[8][3]=3;	

    AccGrid = new MulLineEnter( "fm" , "AccGrid" );
    //这些属性必须在loadMulLine前
    AccGrid.mulLineCount = 1;
    AccGrid.displayTitle = 1;
    AccGrid.hiddenSubtraction = 1;
    AccGrid.hiddenPlus = 1;
    AccGrid.canSel = 1;
    AccGrid.selBoxEventFuncName="queryAccTrace" ;
    AccGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
function setDateInfo()//获取当前系统日期
{
	  var d = new Date();
	  var h = d.getYear();
	  var m = d.getMonth(); 
	  var day = d.getDate();  
	  var Date1;       
	  if(h<10){h = "0"+d.getYear();}  
	  if(m<9){ m++; m = "0"+m;}
	  else{m++;}
	  if(day<10){day = "0"+d.getDate();}
	  Date1 = h+"-"+m+"-"+day;   
	  //alert("<%=nowDate%>");                     
    fm.all('CurrentDate').value = "<%=nowDate%>";
}  
function initForm()
{
 setDateInfo();
 initAccGrid();
 initAccClassGrid();
 initAccTraceGrid()
 getGrpAccInfo(); 
 chooseQueryType();
}
//保险账户分类表
function initAccClassGrid()
{
 var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="产品代码";
    iArray[1][1]="100px";
    iArray[1][2]=60;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="产品名称";
    iArray[2][1]="160px";
    iArray[2][2]=60;
    iArray[2][3]=0;


    iArray[3]=new Array();
    iArray[3][0]="账户名称";
    iArray[3][1]="90px";
    iArray[3][2]=200;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="账户金额";
    iArray[4][1]="70px";
    iArray[4][2]=200;
    iArray[4][3]=0;	
    
    iArray[5]=new Array();
    iArray[5][0]="结息日期";
    iArray[5][1]="70px";
    iArray[5][2]=200;
    iArray[5][3]=0;	
    
    iArray[6]=new Array();
    iArray[6][0]="被保人姓名";
    iArray[6][1]="60px";
    iArray[6][2]=200;
    iArray[6][3]=0;	
    
    iArray[7]=new Array();
    iArray[7][0]="合同号";
    iArray[7][1]="60px";
    iArray[7][2]=200;
    iArray[7][3]=3;	
    iArray[8]=new Array();
    iArray[8][0]="交费计划名称";
    iArray[8][1]="60px";
    iArray[8][2]=200;
    iArray[8][3]=3;	
    

    AccClassGrid = new MulLineEnter( "fm" , "AccClassGrid" );
    //这些属性必须在loadMulLine前
    AccClassGrid.mulLineCount = 1;
    AccClassGrid.displayTitle = 1;
    AccClassGrid.hiddenSubtraction = 1;
    AccClassGrid.hiddenPlus = 1;
    AccClassGrid.canSel = 1;
    AccClassGrid.selBoxEventFuncName="queryPerTrace" ;
    AccClassGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}

//账户轨迹
function initAccTraceGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="流水号";
    iArray[1][1]="110px";
    iArray[1][2]=60;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="账户分类名称";
    iArray[2][1]="120px";
    iArray[2][2]=60;
    iArray[2][3]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="交易类型";
    iArray[3][1]="120px";
    iArray[3][2]=200;
    iArray[3][3]=0;
    
    iArray[4]=new Array();
    iArray[4][0]="本次金额";
    iArray[4][1]="120px";
    iArray[4][2]=200;
    iArray[4][3]=0;

    iArray[5]=new Array();
    iArray[5][0]="交易日期";
    iArray[5][1]="80px";
    iArray[5][2]=200;
    iArray[5][3]=0;	

    
    //iArray[8]=new Array();
    //iArray[8][0]="管理费金额";
    //iArray[8][1]="0px";
    //iArray[8][2]=200;
    //iArray[8][3]=3;	

    AccTraceGrid = new MulLineEnter( "fm" , "AccTraceGrid" );
    //这些属性必须在loadMulLine前
    AccTraceGrid.mulLineCount = 1;
    AccTraceGrid.displayTitle = 1;
    AccTraceGrid.hiddenSubtraction = 1;
    AccTraceGrid.hiddenPlus = 1;
    //AccTraceGrid.canSel = 1;
    AccTraceGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}

</script>