<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="java.util.*"%>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>

<script>
  var managecom = <%=tGI.ManageCom%>;
</script> 
	<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		
		<SCRIPT src="./ContExcelInput.js"></SCRIPT>
		<%@include file="./ContExcelInit.jsp"%>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

	</head>
	<body onload="initForm();initElementtype();">
		<form action="" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class="titleImg">
						资料清单
					</td>
				</tr>
			</table>
			<br>
			<TABLE class="common">
				<tr class=common>
					<TD class=title>销售渠道</TD>
				   	<TD class=input>
				   		<input name=TheSaleChnl class=codeNo CodeData="0|^0|全部^01|个险直销^10|个险中介"  readonly=true
				   		ondblclick="return showCodeListEx('TheSaleChnl',[this,TheSaleChnlName],[0,1],null,null,null,1);" 
				   		onkeyup="return showCodeListKeyEx('TheSaleChnl',[this,TheSaleChnlName],[0,1],null,null,null,1);"
				   		><input name=TheSaleChnlName class=codeName readonly=true>
				   	</TD>
					<TD  class= title> 
					险种编码
					</TD>
					<TD  class= input>
					<Input class= "codeno"  name=TheRiskCode  readonly=true  ondblclick="return showCodeListEx('TheRiskCode',[this,RiskName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('TheRiskCode',[this,RiskName],[0,1],null,null,null,1);"  ><Input class=codename  name=RiskName readonly=true></TD>
          			<TD  class= title> 
					管理机构
					</TD>
					<TD  class= input>
					<Input class= "codeno"  name=TheManagecom  readonly=true  ondblclick="return showCodeListEx('TheManagecom',[this,TheManagecomName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('TheManagecom',[this,TheManagecomName],[0,1],null,null,null,1);"  ><Input class=codename  name=TheManagecomName readonly=true></TD>
   
				</tr>
				<tr class="common">
					<td class="title">
						投保日期起期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="SignStartDate" verify="签单日期起期|date" elementtype="nacessary">
					<td class="title">
						投保日期止期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="SignEndDate" verify="签单日期止期|date" elementtype="nacessary">
					</td>
				</tr>
				
			</TABLE><br>
			
			<input value="下   载" type=button onclick="Print()" class="cssButton"
				type="button">
			
			<input type="hidden" value="" name="downloadtype">
			<br><br>
			<font color="red">
            <ul>报表说明：
                <li>该报表是按投保日期范围内的长期险新单期缴（缴费期在一年以上）的险种统计。</li>
                <li>统计数据包含：投保/承保。</li>                
                <li>本报表只包含首次投保的保单，不包括续保保单。</li>
                <li>保单件数按投保单号为条件统计投保件数，一个投保单号算作一件。</li>
                <li>投保日期查询口径：这里指投保单申请日期。</li>
            </ul>
          
       	 </font>			
			<input type="hidden" name="querySql1" value="">
			
		</form>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</body>
</html>
