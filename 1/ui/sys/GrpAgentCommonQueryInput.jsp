<%
//程序名称：GrpAgentQueryInput.jsp
//程序功能：
//创建日期：2010-10-29
//创建人  ：gzh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<script>
	var GrpAgentCom = "<%= request.getParameter("GrpAgentCom")%>";
	var Sales_Cod="<%=request.getParameter("Sales_Cod")%>";
	var GrpAgentName="<%=request.getParameter("GrpAgentName")%>";
	var GrpAgentIDNo="<%=request.getParameter("GrpAgentIDNo")%>";			
</script>	
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./GrpAgentCommonQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <%@include file="./GrpAgentCommonQueryInit.jsp"%>
  <title>交叉销售对方业务员查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./GrpAgentCommonQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--业务员查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAGrpAgent);">
            </td>
            <td class= titleImg>
                业务员查询条件
            </td>            
    	</tr>
    </table>
  <Div  id= "divLAGrpAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <TD class= title>   对方业务员代码  </TD>
        <TD  class= input>  <Input class=common  name=GrpAgentCode verify="对方业务员代码|int&len<=20"> </TD>
        <TD class= title>   对方机构编码  </TD>
        <TD class= input> 
        <Input class=code name="GrpAgentCom"  verify="对方机构代码|notnull&grpagentcom" ondblclick="return showCodeList('grpagentcom',[this,GrpAgentComName],[0,1],null,1);" onkeyup="return showCodeListKey('grpagentcom',[this,GrpAgentComName],null,null, '1 and #1# = #1# and comp_cod =(case #' + fm.all('Crs_SaleChnl').value + '# when #01# then #000002# when #02# then #000100# end)', '1');" >
        <Input type=hidden class="common" name="GrpAgentComName" elementtype=nacessary TABINDEX="-1" readonly >
        </TD>
        <!-- 
        <Input class=common name=GrpAgentCom verify="对方机构编码|int&len<=20">  
        <TD class= title>  交叉销售渠道 </TD>
        <TD class= input>
            <input class=codeNo name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" 
            ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" 
            onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);"><input class="codename" name="Crs_SaleChnlName" readonly="readonly" elementtype=nacessary/>   
        </TD>
         -->
        <TD  class= title> 姓名 </TD>
        <TD  class= input>   <Input name=Name class= common verify="姓名|len<=12">  </TD>
      </TR>
      <TR  class= common>         
        <TD  class= title> 性别  </TD>
        <TD  class= input> <Input name=Sex class=codeNo MAXLENGTH=1 verify="性别|code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" ><input class=codename name=SexName readonly=true > </TD>
        <TD  class= title>  身份证号码  </TD>
        <TD  class= input>   <Input name=IDNo class= common verify="身份证号码|NUM&len<=20">  </TD>
        <TD  class= title></TD>
        <TD  class= input><Input type=hidden name=COMP_COD class= common>  </TD>
      </TR>
   
    </table>
          
          <!--INPUT class=common VALUE="查询" TYPE=button onclick="easyQueryClick();"--> 
	   <table> 
		    <tr>
		    <td><INPUT class=cssbutton VALUE="查  询" TYPE=button onclick="easyQueryClick();"> </td>
		    <td><INPUT class=cssbutton VALUE="返  回" TYPE=button onclick="returnParent();"> 	</td>
		    <td><INPUT class=cssbutton VALUE="取  消" TYPE=button onclick="closePage();"> 	</td>

			<td>  
			 <!--INPUT class=common VALUE="业务员信息" TYPE=button onclick="returnParent();"-->  </td>
		   <td><!--INPUT class=common VALUE="投保单信息" TYPE=button onclick="ProposalClick();"-->  </td>
			<td> <!--INPUT class=common VALUE="保单信息" TYPE=button onclick="PolClick();"-->  </td>
			<td>  
			<!--INPUT class=common VALUE="销户保单信息" TYPE=button onclick="DesPolClick();"-->  </td>
		    </tr> 
	   </table> 
    </Div>      
          				
    <table>
    	<tr>
        <td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 业务员结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divGrpAgentGrid" style= "display: ''" align =center>
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpAgentGrid" align=center>
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table>
    		<tr>
    			<td>
			      <INPUT class=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
			    </td>
			    <td>  
			      <INPUT class=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    </td>
			    <td> 			      
			      <INPUT class=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    </td>
			    <td> 			      
			      <INPUT class=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 						
			    </td>  			
  			</tr>
  		</table>
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
