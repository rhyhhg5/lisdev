<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String Comcode = tG.ManageCom;
%>

<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  { 
    fm.all('Name').value = tName;
    fm.all('IDType').value = tIDType;
    fm.all('IDNo').value = tIDNo;
    if(tIDType!=null &&tIDType!=""){
       var sql = "select CodeName('idtype', '" + tIDType + "') from dual";
       var rs = easyExecSql(sql)
       if(rs)
       {
         fm.all('IDTypeName').value = rs[0][0];
       }
    }
  }
  catch(ex)
  {
    alert("在GrpTaxCommonQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      

function initForm()
{
  try
  {
    initInpBox();
    initBatchGrid();    
  }
  catch(re)
  {
    alert("GrpTaxCommonQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentGrid
 ************************************************************
 */
function initBatchGrid()
  {                             
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="团体编号";         //列名
        iArray[1][1]="40px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="单位名称";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="税收登记证号码";         //列名
        iArray[3][1]="40px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="社会信用代码";         //列名
        iArray[4][1]="40px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="法人代表";         //列名
        iArray[5][1]="60px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="批次号";         //列名
        iArray[6][1]="60px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="被保人姓名";         //列名
        iArray[7][1]="60px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="被保人证件号码";         //列名
        iArray[8][1]="60px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="被保人序号";         //列名
        iArray[9][1]="0px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
        
  
        BatchGrid = new MulLineEnter( "fm" , "BatchGrid" ); 

        //这些属性必须在loadMulLine前
        BatchGrid.mulLineCount = 10;   
        BatchGrid.displayTitle = 1;
        BatchGrid.canSel=1;
        BatchGrid.locked=1;
	    BatchGrid.hiddenPlus=1;
	    BatchGrid.hiddenSubtraction=1;
        BatchGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化AgentGrid时出错："+ ex);
      }
    }


</script>
