<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
  <title>理赔查询 </title>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" >					</SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js">					</SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js">				</SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js">						</SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js">	</SCRIPT>
	  
  <SCRIPT src="LLClaimQuery.js">													</SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
<%
	String tDisplay="";
	
	//tDisplay在zb_testInit.jsp中要用到
	String tContNo="";
	try
	{
		tDisplay = request.getParameter("display");
	}
	catch( Exception e )
	{
		tDisplay = "";
	}
%>  
</head>
<%@include file="LLClaimQueryInit.jsp"%>
<script>
  var turnPage = new turnPageClass();
  //turnPageClass()在EasyQueryVer2.js中
  
  //var turnPage1 = new turnPageClass();
	//turnPage1.pageLineNum=100;

</script>

	<body onLoad="initForm();queryClick()">
<%
	String contNoParam = request.getParameter("ContNo");
%>
		<form name=fm>
			<input type="hidden" name="contNoParam" value="<%=contNoParam%>">
			<table class= common border=0 width=100%>
    		<tr>
					<td class= titleImg align= center>保单理赔历史：</td>
				</tr>
			</table>
			
			<br>
			<table>
	    	<tr>
	        	<td class=common>
	        		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
	        	</td>
	    			<td class= titleImg >理赔信息</td>
	    	</tr>
    	</table>
    	
    	<!--The first MulLine at here-->
 			<div id="divLCPol1" style="dispaly:''" align='center'>   	
    		<table class="common">
    			<tr class="common">
    				<td>
    					<span id="spanPolGrid" align='center'>
    					</span>
    				</td>
    			</tr>
    		</table>
    		<input value= "首  页" class= CssButton type= "button" onClick= "turnPage.firstPage();" >
    		<input value= "上一页" class= CssButton type= "button" onClick= "turnPage.previousPage();">
    		<input value= "下一页" class= CssButton type= "button" onClick= "turnPage.nextPage();" >
    		<input value= "尾  页" class= CssButton type= "button" onClick= "turnPage.lastPage();">
    	</div>
    	
   <INPUT VALUE="查询详细信息" class = CssButton TYPE=button onclick="claimDetailInfo();">
   <input value="返回" class = CssButton TYPE=button onclick="closeWindow();">
     <div  id= "divClaimDetail" style="display:'none'">	
		  <table class= common border=0  width=100%>
    		<tr>
    			<td class=titleImg>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" 
				    	OnClick= "showPage(this,divClaimDetail2);">
  				    详细信息
				  </td>
				</tr>
			</table>
		
    	
	    <div  id= "divClaimDetail2" style="display:''">	
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				
				    </td>
					</tr>
				</table>
				
				<table  class= common>
			      <TR  class= common>      
							    <TD  class= title>
							      报案登记号
							    </TD>
							    <TD  class= input>
							      <Input class="readonly" name=caseNo readonly >
							    </TD>
							    <TD  class= title>
							      险种名称
							    </TD>
							    <TD  class= input colspan=3>
							      <Input class= 'readonly' name=riskName style="width:100%" readonly >
							    </TD>
			  		</TR>
					  <TR  class= common>
					  			<TD  class= title>
							      险种状态
							    </TD>
							    <TD  class= input>
							      <Input class="readonly" name=riskState  readonly >
							    </TD>
					  			<TD  class= title>
							      出险人号码
							    </TD>
							    <TD  class= input>
							      <Input class="readonly" name=riskPersonNo readonly >
							    </TD>
							    <TD  class= title>
							      出险人姓名
							    </TD>
							    <TD  class= input>
							      <Input class="readonly" name=riskPersonName readonly >
							    </TD>
							    
					  </TR>
	  
	  

		  
					 <TR class= common>
					 			<TD  class= title>
						      报案日期
						    </TD>
						    <TD  class= input>
									<Input class="readonly" name=rgtDate readonly >
						    </TD>
					 			<TD  class= title>
						      申请受理时间
						    </TD>
						    <TD  class= input>
						      <Input class= 'readonly' name=requestDate readonly >
						    </TD>
						    <TD  class= title>
						      案件状态
						    </TD>
						    <TD  class= input>
						      <Input class= 'readonly' name=caseState readonly >
						    </TD>
						    
					  </TR>
				</Table>
			</div>
				
			<div id="divAllGive" style="display:''">
				<table class=common>	  
					  <TR  class= common>
						    <TD  class= title>
						      付款日期
						    </TD>
						    <TD  class= input>
						       <Input class='readonly' name=payDate readonly >
						    </TD>
						    <TD  class= title>
						      付款方式
						    </TD>
						    <TD  class= input>
						       <Input class='readonly' name=payManner readonly >
						    </TD>
						    <TD  class= title>
						      赔付金额
						    </TD>
						    <TD  class= input>
						       <Input class='readonly' name=ClaimMoney  readonly >
						    </TD>
					  </TR>
					  
					  <TR  class= common>
						    
						</TR>
	    	</table>
	    
			<table>
	    	<tr>
	        	<td class=common>
	        		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGiveDuty);">
	        	</td>
	    			<td class= titleImg >给付责任</td>
	    	</tr>
    	</table>
    	<div id="divGiveDuty" style="display:''" align='center'>
    		<!------------------------------开发区the second MulLine----------------------------->		
    		<table class="common">
    			<tr class="common">
    				<td>
    					<span id="spanPolGridDuty" align='center'>
    					</span>
    				</td>
    			</tr>
    		</table>
    		<input value= "首  页" class= CssButton type= "button" onClick= "turnPage.firstPage();" >
    		<input value= "上一页" class= CssButton type= "button" onClick= "turnPage.previousPage();">
    		<input value= "下一页" class= CssButton type= "button" onClick= "turnPage.nextPage();" >
    		<input value= "尾  页" class= CssButton type= "button" onClick= "turnPage.lastPage();">
    		
    		<!----------------------------------------------------------------->
   		</div>
   	</div>
   		
    	<table>
	    	<tr>
	        	<td class=common>
	        		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskInfo);">
	        	</td>
	    			<td class= titleImg >出险信息</td>
	    	</tr>
    </table>
    
    <div id="divRiskInfo" style="display:''">
    	&nbsp;
    	<textarea name="riskInfo" rows=3 cols=100 readonly >
    			
    	</textarea>
    </div>
    </div>
			
	 </form>	
	</body>
</html>