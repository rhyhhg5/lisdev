<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>             
<html>
<%
//程序名称：CaseDetailInput.jsp
//程序功能：综合查询之理赔明细信息查询
//创建日期：2004-2-19
//创建人  ：刘岩松
%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="CaseDetailInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="CaseDetailInit.jsp"%>

	<title>理赔明细信息 </title>
</head>

<body  onload="initForm();" >
  <form action="./ShowCaseDetail.jsp" method=post name=fm target="fraSubmit">
  <table >
    	<tr>
    	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    	</td>
			<td class= titleImg>
				理赔明细信息
			</td>
		</tr>
	</table>
	<Div  id= "divLCPol1" style= "display: ''">
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            报案号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=RptNo >
          </TD>
          <TD  class= title>
            立案号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=RgtNo >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            审核类型 
          </TD>
          <TD  class= input>
            <Input class= "readonly"  readonly name=CheckType >
          </TD>
          <TD  class= title>
            理赔状态
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CaseState >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            事故者类型 
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerType >
          </TD>
          <TD  class= title>
            事故者姓名
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerName >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            出险日期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AccidentDate >
          </TD>
          <TD  class= title>
            出险地点
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AccidentSite >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            事故类型
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AccidentType >
          </TD>
          <TD  class= title>
            核赔意见
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=ClmDecision >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            报案日期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=RptDate >
          </TD>
          <TD  class= title>
            报案人姓名
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=RptOperatorName >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            立案日期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=RgtDate >
          </TD>
          <TD  class= title>
            申请权利人姓名
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=RgtantName >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            合计给付金额
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=TotalPayMoney >
          </TD>
          <TD  class= title>
            合计豁免金额
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=TotalReleaseMoney >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            已发生未领取生存保险金
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=LiveMoney>
          </TD>
          <TD  class= title>
            保单红利
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=BonusMoney >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            合计退还保险费
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=TotalPermMoney >
          </TD>
          <TD  class= title>
            合计扣款
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=TotalSubMoney >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            批单号码
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=PDNo>
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            结案日期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=EndCaseDate >
          </TD>
          <TD  class= title>
            通知日期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=InformDate >
          </TD>
				</TR>
				
				<TR  class= common>
          <TD  class= title>
            理算审核人
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Clmer >
          </TD>
          <TD  class= title>
            签批人
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=ClmUWer >
          </TD>
				</TR>
				
				<tr >
        	<td class=common>
        	  <input class=common type= hidden name= RgtNo_1 >
        	  <input class=common type= hidden name= ClmUWNo >
        	  <input class=common type= hidden name= PolNo >
        	  <input class=common type= hidden name= InsuredName >
        		<input class=common type= hidden name= CaseNo >
        		<input class=common type= hidden name= InsuredNo >
        		<input class=common type= hidden name= LPJC >
        		
        	</td>
				</tr>
				<TR  class= common>
          <TD  class= title>
            给付通知人
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=InformName >
          </TD>
          <TD  class= title>
            保险金领取方式
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CaseGetMode >
          </TD>
				</TR>
			</table>
			
			<table class=common>
				<TR class= common>
			    <TD class= title>
 				    事故经过描述
  			  </TD>
		    </TR>

		    <TR  class= common>
  			  <TD  class= input>
    				<textarea class= "readonly" readonly name="AccidentReason" cols="120%" rows="3" witdh=25% class="common">
            </textarea>
    			</TD>
  		  </TR>
  		  
  		  <TR class= common>
          <TD  class= title>
            案件撤销原因
          </TD>
        </TR>
        
        <TR>
          <TD  class= input>
            <textarea class= "readonly" readonly name="RgtReason" cols="110%" rows="3" witdh=25% class="common">
            </textarea>
           </TD>
        </TR>
        
        <TR class= common>
          <TD  class= title>
            批单内容
          </TD>
        </TR>
        
        <TR>
          <TD  class= input>
            <textarea class= "readonly" readonly name="PDContent" cols="110%" rows="3" witdh=25% class="common">
            </textarea>
           </TD>
        </TR>
        
        <tr>
        	<td>
        		<input class=common type=button value="二次核保" onclick="SecondUW()">
  					<input class=common type=button value="解约暂停" onclick="EndAgreement()">
  				</td>
  			</tr>
     </table>
     
  </Div>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  fm.ClmUWNo.value = '<%= request.getParameter("ClmUWNo") %>';
  fm.RgtNo_1.value = '<%= request.getParameter("RgtNo") %>';
  fm.InsuredName.value = '<%= StrTool.unicodeToGBK(request.getParameter("InsuredName")) %>';
	fm.PolNo.value = '<%= request.getParameter("PolNo")%>';
	fm.ClmUWer.value = '<%= StrTool.unicodeToGBK(request.getParameter("ClmUWer")) %>';
	fm.CheckType.value = '<%= StrTool.unicodeToGBK(request.getParameter("CheckType")) %>';	
	fm.Clmer.value = '<%= StrTool.unicodeToGBK(request.getParameter("Clmer")) %>';
	fm.LPJC.value = '<%= StrTool.unicodeToGBK(request.getParameter("LPJC")) %>'				
}
catch(ex)
{
  alert(ex);
}
</script>
</html>