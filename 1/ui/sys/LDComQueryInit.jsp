<%
//程序名称：LDComQueryInit.js
//程序功能：
//创建日期：2002-08-16 17:44:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
   // fm.all('ComCode').value = '';
  }
  catch(ex)
  {
    alert("在LDComQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                     

function initForm()
{
  try
  {
    initInpBox();  
     initCodeGrid();
  }
  catch(re)
  {
    alert("LDComQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化CodeGrid
 ************************************************************
 */
function initCodeGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="50px";         //列名
        iArray[0][2]=50;         //列名
        iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="机构编码";         //列名
    iArray[1][1]="100px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         //列名
    iArray[2][1]="150px";         //宽度
    iArray[2][2]=150;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="管理机构简称";         //列名
    iArray[3][1]="100px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="地址";         //列名
    iArray[4][1]="150px";         //宽度
    iArray[4][2]=150;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array();
    iArray[5][0]="邮编";         //列名
    iArray[5][1]="100px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();
    iArray[6][0]="电话";         //列名
    iArray[6][1]="100px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="传真";         //列名
    iArray[7][1]="100px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="电邮地址";         //列名
    iArray[8][1]="100px";         //宽度
    iArray[8][2]=100;         //最大长度
    iArray[8][3]=0;         //是否允许录入，0--不能，1--允许


   CodeGrid = new MulLineEnter( "fm" , "CodeGrid" ); 
      //这些属性必须在loadMulLine前
      CodeGrid.mulLineCount = 0;   
      CodeGrid.displayTitle = 1;
      CodeGrid.locked = 1;
      CodeGrid.canSel = 1;
      CodeGrid.loadMulLine(iArray);  
      
      }
      catch(ex)
      {
        alert("初始化CodeGrid时出错："+ ex);
      }
    }


</script>