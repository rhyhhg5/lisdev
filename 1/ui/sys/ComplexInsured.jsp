<DIV id=DivLCInsuredButton STYLE="display:''">
<!-- 被保人信息部分 -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCInsured);">
</td>
<td class= titleImg>
<Input class= mulinetitle value="第一被保险人资料"  name=InsuredSequencename readonly >客户号：<Input class= common name=InsuredNo >

<Div  id= "divSamePerson" style="display:'none'">
<font color=red>
如投保人为被保险人本人，可免填本栏，请选择
<INPUT TYPE="checkbox" NAME="SamePersonFlag" onclick="isSamePerson();">
</font>
</div>
</td>
</tr>
</table>
</DIV> 
<DIV id=DivRelation STYLE="display:''">
    <table  class= common>  
        <TR  class= common>  
           </TD> 
	        <TD  class= title>
                客户内部号码</TD>             
            <TD  class= input>
                <Input class="common" name="SequenceNo"  elementtype=nacessary  CodeData="0|^1|第一被保险人 ^2|第二被保险人 ^3|第三被保险人" ondblclick="return showCodeListEx('SequenceNo', [this]);" onkeyup="return showCodeListKeyEx('SequenceNo', [this]);"></TD>              
          <TD  class= title>
            与投保人关系
            </TD>
          <TD  class= input>           
          <Input class="common" name="RelationToAppnt" elementtype=nacessary ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);" >
           </TD> 
	        <TD  class= title>与第一被保险人关系</TD>             
            <TD  class= input>
              <Input class="common" name="RelationToMainInsured"  elementtype=nacessary verify="主被保险人关系|code:Relation" ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);">
            </TD>                       
          <TD  class= title>
            国籍
          </TD>
          <TD class= input>
            <input class="common" name="NativePlace"  ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">
          </TD>
          <TD  class= input>
	       <input class="common" style="display:none"  name="NativeCity" >
	      </TD>  
        </TR> 
    </table> 
</DIV>       
<DIV id=DivLCInsured STYLE="display:''">  
    <table  class= common>  
        <TR  class= common>        
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name elementtype=nacessary verify="被保险人姓名|notnull&len<=20" onblur="getallinfo();"readonly>
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class="common" name=Sex elementtype=nacessary verify="被保险人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
          </TD> 
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
          <input class="common"  name="Birthday" readonly >
          </TD> 
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class="common" name="Marriage" elementtype=nacessary  ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);"  verify="婚姻状况|notnull" >
          </TD>                    
        </TR>       
        <TR  class= common>
          <TD  class= title>
              职业
          </TD>
          <TD  class= input>
              <Input class="common3" name="OccupationName"  readonly >
          </TD>       
          <TD  class= title>
              职业代码
          </TD>
          <TD  class= input>
              <Input class="common" name="OccupationCode"  elementtype=nacessary>
          </TD>   
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common3 name="IDNo" readonly onblur="checkidtype();getBirthdaySexByIDNo(this.value);getallinfo();" verify="被保险人证件号码|len<=20"  >
          </TD>    
        <TD  class= title>
          证件类型
        </TD>
        <TD  class= input>
          <Input class="common" name="IDType" value="0" verify="被保险人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);" onblur="getallinfo();" >
        </TD>
      </TR>
      <tr class = "commmen">
      	<TD  class= title>
		证件生效日期
	    </TD>
	    <TD  class= input>
		  <input class="common"  name="InsuredIDStartDate"  >
	    </TD>
	    <TD  class= title>
		  证件失效日期
	    </TD>
	    <TD  class= input>
		  <input class="common"  name="InsuredIDEndDate"  >
	    </TD>
	    <TD  class= title>
      	岗位职务
        </TD>
        <TD  class= input>
      	 <Input class= 'common' name="InsuredPosition" >
        </TD>
         <TD  class= title>
            		授权使用客户信息
          </TD>
          <TD  class= input>
            <Input class=codeNo name="InsuredAuth" readonly=true  verify="授权使用客户信息|code:Auth" ondblclick=" return false ;return showCodeList('AUTH',[this,InsuredAuthName],[0,1]);" onkeyup="return showCodeListKey('Auth',[this,InsuredAuthName],[0,1]);"><input class=codename name=InsuredAuthName readonly=true elementtype=nacessary>    
          </TD>
      </tr> 
    </Table>        
<DIV id=DivAddress STYLE="display:''">   
    <table  class= common>         
        <TR  class= common>        
          <TD  class= title>
            工作单位
          </TD>
          <TD  class= input>
            <Input class= common3 name="GrpName" readonly >
          </TD>
            <TD  class= title>
                办公电话
            </TD>
            <TD  class= input>
                <Input class= common name="GrpPhone" verify="被保险人办公电话|len<=15" readonly>
            </TD> 
         </TR>                            
        <TR  class= common>                          
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input  colspan=3>
            <Input class= common3 name="GrpAddress" readonly >      
          </TD>          
          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= common name="GrpZipCode" readonly >
          </TD>
         </TR>  
		<TR class=common>
			<TD class=title>
			  家庭地址
			</TD>
			<TD class=input colspan=3>
			  <Input name="HomeAddress" class=common3 readonly>
			</TD>
			<TD class=title>
			  家庭邮编
			</TD>
			<TD class=input>
			  <Input name="HomeZipCode" class=common readonly>
			</TD>					
		</TR>
		<TR class=common>
			<TD class=title>
			  联系地址
			</TD>
			<TD class=input colspan=3>
			  <Input name="PostalAddress" class=common3 readonly>
			</TD>
			<TD class=title>
			  邮编
			</TD>
			<TD class=input>
			  <Input name="ZipCode" class=common readonly>
			</TD>					
		</TR>
        <TR  class= common>
	    <TD class=title>
	      住宅电话
	    </TD>
	    <TD class=input>
	      <Input name="HomePhone" class=common readonly>
	    </TD>                              
            <TD  class= title>
                移动电话
            </TD>
            <TD  class= input>
                <Input class= common name="Mobile" verify="被保险人移动电话|len<=15" readonly >
            </TD>
            <TD  class= title>
                E-mail
            </TD>
            <TD  class= input>
                <Input class= common name="EMail" verify="被保险人电子邮箱|Email" readonly>
            </TD> 
         </TR> 	                                
    </Table> 
</DIV>       
<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFee);">
    		</td>
    		<td class= titleImg>
    			理赔帐号：
    		</td>
    	</tr>
    </table>
    <Div  id= "divFee" style= "display: ''">     
      <table class=three>    
        <TR  class= common>
          <TD  class= title8>
            银行
          </TD>
          <TD  class= input8>
            <Input class=common8 name=BankCode readonly >
          </TD>
          <TD  class= title8>
            账号
          </TD>
          <TD  class= input8>
            <Input class= common8 name=BankAccNo verify="帐号|len<=40" readonly>
          </TD> 
		  <TD  class= title8>
            户名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AccName verify="户名|len<=40" readonly>
          </TD>       
        </TR> 
       </table>   
    </Div>
<TABLE class= common>
    <TR class= common>
        <TD  class= title>
            <DIV id="divContPlan" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            保险计划
                    </TD>
                    <TD  class= input>
                        <Input class="code" name="ContPlanCode" ondblclick="showCodeListEx('ContPlanCode',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ContPlanCode',[this],[0],'', '', '', true);">
                    </TD>
		
                    </TR>
	            </TABLE>
            </DIV>
        </TD>
        <TD  class= title>
            <DIV id="divExecuteCom" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            处理机构
                        </TD>
                        <TD  class= input>
                            <Input class="code" name="ExecuteCom" ondblclick="showCodeListEx('ExecuteCom',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ExecuteCom',[this],[0],'', '', '', true);">
                        </TD>
		            </TR>
	            </TABLE>
            </DIV>
        </TD>
        <TD  class= title>
        </TD>		
    </TR>
</TABLE>
