<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContrlUrgeNoticeSave.jsp
//程序功能：催办控制
//创建日期：2006-01-10 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  VData mResult = null;
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  System.out.println("UrgeNoticeSave-----Begin!");
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
	
  	//接收信息
  String tPrtSeq = request.getParameter("PrtSeq");
  String tUrgeDate = request.getParameter("UrgeDate");
 	TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("PrtSeq",tPrtSeq);
  tTransferData.setNameAndValue("UrgeDate",tUrgeDate);
	 	
  	
try
{ 
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
		System.out.println("tUrgeNoticeUI.submitData------Begin");
		ContrlUrgeNoticeUI tContrlUrgeNoticeUI = new ContrlUrgeNoticeUI();		
		if (tContrlUrgeNoticeUI.submitData(tVData,"") == false)
		{
			int n = tContrlUrgeNoticeUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tContrlUrgeNoticeUI.mErrors.getError(i).errorMessage);
			Content = " 查询失败，原因是: " + tContrlUrgeNoticeUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tContrlUrgeNoticeUI.mErrors;
		    if (!tError.needDealError())
		    { 
    	     FlagStr = "Success";	   
    	     Content = "保存成功";
    	  }
		    else                                                                           
		    {
		        Content = " 保存失败，原因是:" + tError.getFirstError();
    	      FlagStr = "Fail";
		    }
		}

	else
	{
		Content = "保存成功";
	}  
}
catch( Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
