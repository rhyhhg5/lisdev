<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = request.getParameter( "ContNo" );
	String tAppObj = request.getParameter( "AppObj" );
%>

<%
   GlobalInput tG = (GlobalInput)session.getValue("GI");//添加页面控件的初始化。
%>   
<script>
	var tContNo = "<%=tContNo%>";  //个人单的查询条件.
	var tComCode = "<%=tG.ComCode%>";
	var tAppObj = "<%=tAppObj%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="EdorList.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="EdorListInit.jsp"%>
  <title>保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <table  class= common align=center>
    	<TR  class= common>
	      <TD  class= title> 业务类型 </TD>  
	      <TD  class= input><Input class="codeNo" name="WorkTypeNo" readonly CodeData=""  ondblclick="return showCodeListEx('worktypeno',[this,WorkTypeName],[0,1], null, null, null, 1);"><Input class="codeName" name="WorkTypeName" readonly></TD>
	      <TD  class= title colspan=6> <INPUT VALUE="查  询" class = cssbutton TYPE=button onclick="queryEdorList();"> </TD>

      </TR>
      <TR  class= common>
	      <TD  class= title> 申请时间 </TD>  
	      <TD  class= input> <Input class="coolDatePicker" name=ApplyDateStart style="width:110" elementtype="nacessary"  dateFormat="short" verify="开始时间|notnull"></TD>
	      <TD  class= title> 至 </TD>
        <TD  class= input> <Input class="coolDatePicker" name=ApplyDateEnd style="width:110" elementtype="nacessary" dateFormat="short" verify="开始时间|notnull"> </TD>          
        <TD  class= title> </TD>  
	      <TD  class= input></TD>
	      <TD  class= title>  </TD>
        <TD  class= input> </TD>          
      </TR>
    </table>
    
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 有效保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  					<span id="spanEdorListGrid" >
  					</span> 
  			  </td>
  			</tr>
    	</table>
  	  <Div id= "divPage2" align="center" style= "display: '' ">
        <INPUT VALUE="查看保全服务明细" onclick="viewEdorInfo();" class = cssbutton TYPE=button>
        <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage2.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage2.lastPage();">
        <INPUT VALUE="打印全部清单" onclick="printList();" class = cssbutton TYPE=button>
      </Div>
  	</div>
  	<Input class="common" name="EdorListSql" type=hidden>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
