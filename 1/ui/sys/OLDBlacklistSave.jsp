<%
//程序名称：OLDBlacklistInput.jsp
//程序功能：
//创建日期：2002-08-16 17:19:55
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=gb2312" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LDBlacklistSchema tLDBlacklistSchema   = new LDBlacklistSchema();

  OLDBlacklistUI tOLDBlacklist   = new OLDBlacklistUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLDBlacklistSchema.setBlacklistNo(request.getParameter("BlacklistNo"));
    tLDBlacklistSchema.setBlacklistType(request.getParameter("BlacklistType"));
    tLDBlacklistSchema.setBlackName(request.getParameter("BlackName"));
    tLDBlacklistSchema.setBlacklistOperator(request.getParameter("BlacklistOperator"));
    tLDBlacklistSchema.setBlacklistMakeDate(request.getParameter("BlacklistMakeDate"));
    tLDBlacklistSchema.setBlacklistMakeTime(request.getParameter("BlacklistMakeTime"));
    tLDBlacklistSchema.setBlacklistReason(request.getParameter("BlacklistReason"));


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLDBlacklistSchema);
	tVData.add(tG);
  try
  {
    tOLDBlacklist.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tOLDBlacklist.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

