<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ComInteractInput.jsp
//程序功能：
//创建日期：2002-08-16 17:44:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//接收信息，并作校验处理。
//输入参数
LDComSchema tLDComSchema   = new LDComSchema();
ComInteractUI tComInteractUI = new ComInteractUI();
GlobalInput globalInput = new GlobalInput();
globalInput = (GlobalInput) session.getValue("GI");
if(globalInput == null) 
{
  System.out.println("session has expired");
  return;
}

//输出参数
CErrors tError = null;

String tRela  = "";                
String FlagStr = "";
String Content = "";
String transact = "";
String currentDate = PubFun.getCurrentDate();
String currentTime = PubFun.getCurrentTime();

LDComDB tLDComDB = new LDComDB();
tLDComDB.setComCode(request.getParameter("ComCode"));
LDComSet tLDComSet = tLDComDB.query();
if (tLDComSet.size() == 1){
  tLDComSchema = tLDComSet.get(1);
}
else
{
  tLDComSchema.setMakeDate(currentDate);
  tLDComSchema.setMakeTime(currentTime);
}

transact = request.getParameter("fmtransact");
System.out.println("------transact:"+transact);
tLDComSchema.setComCode(request.getParameter("ComCode"));
tLDComSchema.setOutComCode(request.getParameter("OutComCode"));
tLDComSchema.setName(request.getParameter("Name"));
tLDComSchema.setShortName(request.getParameter("ShortName"));
tLDComSchema.setAddress(request.getParameter("Address"));
tLDComSchema.setZipCode(request.getParameter("ZipCode"));
tLDComSchema.setPhone(request.getParameter("Phone"));
tLDComSchema.setFax(request.getParameter("Fax"));
tLDComSchema.setEMail(request.getParameter("EMail"));
tLDComSchema.setWebAddress(request.getParameter("WebAddress"));
tLDComSchema.setSatrapName(request.getParameter("SatrapName"));
tLDComSchema.setSign(request.getParameter("Sign"));
tLDComSchema.setLetterServiceName(request.getParameter("LetterServiceName"));
tLDComSchema.setServicePhone(request.getParameter("ServicePhone"));
tLDComSchema.setServicePostAddress(request.getParameter("ServicePostAddress"));
tLDComSchema.setServicePostZipcode(request.getParameter("ServicePostZipcode"));
tLDComSchema.setLetterServicePostAddress(request.getParameter("LetterServicePostAddress"));
tLDComSchema.setLetterServicePostZipcode(request.getParameter("LetterServicePostZipcode"));
tLDComSchema.setClaimReportPhone(request.getParameter("ClaimReportPhone"));
tLDComSchema.setPEOrderPhone(request.getParameter("PEOrderPhone"));
tLDComSchema.setServicePhone1(request.getParameter("ServicePhone1"));
tLDComSchema.setServicePhone2(request.getParameter("ServicePhone2"));
tLDComSchema.setBackupPhone1(request.getParameter("BackupPhone1"));
tLDComSchema.setBackupPhone2(request.getParameter("BackupPhone2"));
tLDComSchema.setBackupAddress1(request.getParameter("BackupAddress1"));
tLDComSchema.setBackupAddress2(request.getParameter("BackupAddress2"));
tLDComSchema.setEName(request.getParameter("EName"));
tLDComSchema.setEShortName(request.getParameter("EShortName"));
tLDComSchema.setEAddress(request.getParameter("EAddress"));
tLDComSchema.setEServicePostAddress(request.getParameter("EServicePostAddress"));
tLDComSchema.setELetterServiceName(request.getParameter("ELetterServiceName"));
tLDComSchema.setELetterServicePostAddress(request.getParameter("ELetterServicePostAddress"));
tLDComSchema.setEBackupAddress1(request.getParameter("EBackupAddress1"));
tLDComSchema.setEBackupAddress2(request.getParameter("EBackupAddress2"));
tLDComSchema.setShowName(request.getParameter("ShowName"));
tLDComSchema.setPrintComName(request.getParameter("PrintComName"));
tLDComSchema.setSuperComCode(request.getParameter("SuperComCode"));
tLDComSchema.setTaxRegistryNo(request.getParameter("TaxRegistryNo"));
tLDComSchema.setPolicyRegistCode(request.getParameter("PolicyRegistCode"));
tLDComSchema.setAreaCode(request.getParameter("AreaCode"));
if(transact.equals("INSERT||COM")){
	tLDComSchema.setCrs_Check_Status("00");//新插入
}else if(transact.equals("UPDATE||COM")){
	tLDComSchema.setCrs_Check_Status("99"); //更新
}
tLDComSchema.setOperator(globalInput.Operator);
tLDComSchema.setModifyDate(currentDate);
tLDComSchema.setModifyTime(currentTime);
tLDComSchema.setContyFlag(request.getParameter("ContyFlag"));
tLDComSchema.setInteractFlag(request.getParameter("InteractFlag"));
try {
	// 准备传输数据 VData
	VData tVData = new VData();
	tVData.addElement(tLDComSchema);
	tComInteractUI.submitData(tVData,transact);
}
catch(Exception ex) {
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="") {
	tError = tComInteractUI.mErrors;
	if (!tError.needDealError()) {                          
		Content = " 保存成功! ";
		FlagStr = "Success";
	}
	else {
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
%>                      
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>