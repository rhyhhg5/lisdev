<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TianJinGrpReport.jsp
//程序功能：天津社保补充业务数据采集统计分析
//创建时间：2017-11-5
//创建人  ：yangjian
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
  String FlagStr = "";
  String Content = "";
  System.out.println("ssssss");
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");

  String directory = "";
  String filename = "";
  boolean exist = false; 

  
  if(tG==null){
	  	System.out.println("页面失效,请重新登陆");   
	    FlagStr = "Fail";        
	    Content = "页面失效,请重新登陆"; 
  }else{//页面有效
		int lineCount = 0;
		// 在JSP中获取Radio每一行
		String tRadio[] = request.getParameterValues("InpTianJinGrpGridSel");
		// 在JSP中得到MulLine中的值
		String Filename[] = request.getParameterValues("TianJinGrpGrid1");
		String Directory[] = request.getParameterValues("TianJinGrpGrid3");

		for(int index=0;index<tRadio.length;index++){
		    if(tRadio[index].equals("1")){
		    	filename = new String(Filename[index]);
		    	directory = new String(Directory[index]);
		    	//TODO 
		    	filename = new String(filename.getBytes("ISO-8859-1"),"GB2312");
		    	System.out.println(filename);
		    	break;
		    }
	 	 }
		
		
 		//校验要下载的数据是否存在
		File dir = new File(directory);
		File[] listFiles = dir.listFiles();
		for(File file : listFiles){
			if(file.isFile()){
				System.out.println("呵呵");
				System.out.println(file.getName());
				
				if(file.getName().equals(filename)){
					exist = true;
					break;
				}
			}
		}	
		if(!exist){
			FlagStr = "Fail"; 
			Content = "您所要下载的数据已失效：系统找不到指定的文件，请联系相关IT人员";
			System.out.println("系统找不到指定的文件。");
			
		}else{
			try{
				  String fileName = filename;
				  //本地测试
				  //TODO
				  //Path = "E:/tasktianjin"; 
				  String Path = directory;
				  String tOutXmlPath = Path + "/" + fileName;
				  System.out.println("OutXmlPath:" + tOutXmlPath);

			 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
			 	  System.out.println(FileName);
			 	  File file = new File(tOutXmlPath);
			 	  
			      response.reset();
			      response.setContentType("application/octet-stream"); 
			   	  response.setHeader( "Content-Disposition", "attachment;filename=" + new String( FileName.getBytes("gb2312"), "ISO8859-1" ) ); 
			      response.setContentLength((int) file.length());
			  
			      byte[] buffer = new byte[4096];
			      BufferedOutputStream output = null;
			      BufferedInputStream input = null;    
			      //写缓冲区
			      try {
					output = new BufferedOutputStream(response.getOutputStream());//弹出下载弹窗的关键
					input = new BufferedInputStream(new FileInputStream(file));
					    
					int len = 0;
					while((len = input.read(buffer)) >0){
					    output.write(buffer,0,len);
					}
					input.close();
					output.close();
			      }catch (Exception e){
			       	e.printStackTrace();
			      } // maybe user cancelled download
			      finally{
			         if (input != null) input.close();
			         if (output != null) output.close();
			      }
				   
				}catch(Exception ex){
				 	ex.printStackTrace();
				}
			   	if (!FlagStr.equals("Fail")){
					Content = "";
					FlagStr = "Succ";
				} 
		}
  }

  
 
%>
<html>
<script language="javascript">	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
<%-- 	alert("<%=Content%>");
	top.close();  --%>
</script>
</html>