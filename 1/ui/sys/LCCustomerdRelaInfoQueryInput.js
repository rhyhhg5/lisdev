/** 
 * 程序名称：LCCustomerdRelaInfoInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-02-26 14:32:38
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     		    
	var strSql = "select a.CustomerNo,b.Name,a.RelaCustomerNo,c.Name,a.Relation,a.Remark,a.MakeDate from LCCustomerRelaInfo a,LDPerson b,LDPerson c where 1=1 and a.customerno=b.customerno and a.RelaCustomerNo=c.customerno  "
    + getWherePart("a.CustomerNo", "CustomerNo")
    + getWherePart("a.RelaCustomerNo", "RelaCustomerNo")
    + getWherePart("a.Relation", "Relation")
    + getWherePart("a.Remark", "Remark")
    + getWherePart("a.MakeDate", "MakeDate")
  ;
  //alert(strSql);
	turnPage.queryModal(strSql, LCCustomerdRelaInfoGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  //alert("此处选择某一行的代码");
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LCCustomerdRelaInfoGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery1( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LCCustomerdRelaInfoGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LCCustomerdRelaInfoGrid.getRowData(tRow-1);
	
	return arrSelected;
}
