<%
//程序名称：LOGrpPolDataQueryInit.jsp
//程序功能：功能描述
//创建日期：2003-12-17 21:28:33
//创建人  ：yt
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('PolNo').value = "";
    fm.all('PolType').value = "";
    fm.all('PolCurYear').value = "";
    fm.all('Rate').value = "";
    fm.all('State').value = "";
  }
  catch(ex) {
    alert("在LOGrpPolDataQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLOGrpPolDataGrid();  
  }
  catch(re) {
    alert("LOGrpPolDataQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LOGrpPolDataGrid;
function initLOGrpPolDataGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="保单号码";         		//列名
    iArray[0+1][1]="150px";         		//列名
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="类型";         		//列名
    iArray[1+1][1]="90px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="保单年度末";         		//列名
    iArray[2+1][1]="90px";         		//列名
 
    iArray[3+1]=new Array();
    iArray[3+1][0]="比例";         		//列名
    iArray[3+1][1]="90px";         		//列名
 
    iArray[4+1]=new Array();
    iArray[4+1][0]="状态";         		//列名
    iArray[4+1][1]="100px";         		//列名

    iArray[6]=new Array();
    iArray[6][0]="约定执行日期";         		//列名
    iArray[6][1]="102px";         		//列名

    iArray[7]=new Array();
    iArray[7][0]="入机日期";         		//列名
    iArray[7][1]="102px";         		//列名
 
    LOGrpPolDataGrid = new MulLineEnter( "fm" , "LOGrpPolDataGrid" ); 
    LOGrpPolDataGrid.mulLineCount = 1; 
    LOGrpPolDataGrid.displayTitle = 1; 
    LOGrpPolDataGrid.locked = 1; 
 
    LOGrpPolDataGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LOGrpPolDataGrid.mulLineCount = 0;   
    LOGrpPolDataGrid.displayTitle = 1;
    LOGrpPolDataGrid.hiddenPlus = 1;
    LOGrpPolDataGrid.hiddenSubtraction = 1;
    LOGrpPolDataGrid.canSel = 1;
    LOGrpPolDataGrid.canChk = 0;
    LOGrpPolDataGrid.selBoxEventFuncName = "showOne";
*/
    LOGrpPolDataGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LOGrpPolDataGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
