//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var arrAllDataSet；
var arrPayData;
window.onfocus = myonfocus;
 


 /*********************************************************************
 *  查询险种信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryRisk()
{
	
	// 初始化表格
	initRiskGrid();	
	// 书写SQL语句RiskCode
	var strSQL = "";

	//区分签单和撤保/退保
	var strInValidate = "";

	strSQL = "select a.RiskCode,b.riskname,InsuredNo,a.InsuredName,a.PolNo "
	+ " from  LCPol a,lmrisk b where ContNo='" + tContNo + "' and  a.RiskCode=b.riskcode "
	+ " union all "
	+ " select a.RiskCode,b.riskname,InsuredNo,a.InsuredName,a.PolNo" 
	+ " from  LBPol a,lmrisk b where ContNo='" + tContNo + "' and  a.RiskCode=b.riskcode "
	
	//查询SQL，返回结果字符串
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	RiskGrid.clearData();
 
    return false;
  }
 
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  arrAllDataSet = turnPage.arrDataCacheSet;  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = RiskGrid;   
         
  //保存SQL语句
  turnPage.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(arrAllDataSet, turnPage.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
} 


/*********************************************************************
 *  查询受益人信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryBnf()
{
	var tSel = RiskGrid.getSelNo();
    var tInsuredNo = RiskGrid.getRowColData( tSel - 1, 3 );
	var tPolNo = RiskGrid.getRowColData( tSel - 1, 5 );
	// 初始化表格
	initBnfGrid();	
	//区分签单和撤保/退保
	
	//查询被保人
		var strSQL  = "select a.Name,a.Birthday, case when a.Sex='0' then '男'  when  a.Sex='1' then '女' end, a.BnfType,a.BnfLot,'',a.Bnfgrade,a.RelationToInsured,"
		+ " case when a.IDType = '0' then '身份证' when a.IDType = '1' then '护照' when a.IDType = '2' then '军官证' when a.IDType = '3' then '工作证' when a.IDType = '4' then '其他' end ,a.IDNo"
		+ " from LCBnf a where a.ContNo='" + tContNo + "' and InsuredNo ='"+ tInsuredNo + "'"
		+ " and polno='" + tPolNo+ "' "
		+ "union "
		+ "select a.Name,a.Birthday, case when a.Sex='0' then '男'  when  a.Sex='1' then '女' end, a.BnfType,a.BnfLot,'',a.Bnfgrade,a.RelationToInsured,"
		+ " case when a.IDType = '0' then '身份证' when a.IDType = '1' then '护照' when a.IDType = '2' then '军官证' when a.IDType = '3' then '工作证' when a.IDType = '4' then '其他' end ,a.IDNo"
		+ " from LBBnf a where a.ContNo='" + tContNo + "' and InsuredNo ='"+ tInsuredNo + "'"
		+ " and polno='" + tPolNo+ "' "
		;
	//查询SQL，返回结果字符串
   turnPage1.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
 
  //判断是否查询成功
  if (!turnPage1.strQueryResult) {
  	BnfGrid.clearData();
    return false;
  }

 
  //查询成功则拆分字符串，返回二维数组
  turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
  arrAllDataSet=turnPage1.arrDataCacheSet;
  //设置初始化过的MULTILINE对象

  turnPage1.pageDisplayGrid = BnfGrid;   
         
  //保存SQL语句
  turnPage1.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage1.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);
}  

/*********************************************************************
 *  返回上一页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */  
function returnparent()
{  
    window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + tContNo +"&IsCancelPolFlag=0" + "&ContType=" + tContType); 
}

