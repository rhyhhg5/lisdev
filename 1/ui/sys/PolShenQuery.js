 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var arrAllDateSet;
var cPerCustomerNo = "";
var tContType="";
var mContNo = "";

// 王珑制作

/*********************************************************************
 *  查询按钮实现
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
  if (trim(fm.all('CustomerNo').value) ==''&& trim(fm.all('Name').value) == ''
        && trim(fm.all('Birthday').value) == '' && trim(fm.all('Sex').value) == '' 
        && trim(fm.all('IDNo').value) == '' && trim(fm.all('ContNo').value) == '' 
        && trim(fm.all('HomeAddress').value) == '' &&  trim(fm.all('HomeZipCode').value) == '')
      {
      	  alert("请输入查询条件");
          return false;
      }
      
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
    setPolValue();    
    showInfo.close();
  
}



/*********************************************************************
 *  通过个单客户查询保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setPolValue()
{

	initGrpPolGrid();
  //var customerNo = PersonGrid.getRowColData(PersonGrid.getSelNo() - 1, 1);
 
	//保单性质为1：已签单； 2：撤保或退保
	//查询保单信息
	var strSQL = "";
	if (fm.all('ContNo').value == '') 
	{
      strSQL = strSQL + "select a.ContNo,a.AppntNo,a.InsuredNo,'个单','',a.AppntName,a.CValiDate,a.Prem ,'已签单', "
      + " a.PaytoDate,'','',a.PrtNo,'1',ShowManageName(a.ManageCom) "
      + "from LCCont a where a.GrpContNo='00000000000000000000' "
      + " and a.AppFlag='1' "
      + getWherePart( 'a.AppntNo','CustomerNo' ) 
		  + getWherePart( 'a.AppntName','Name') 
		  + getWherePart( 'a.AppntBirthday','Birthday' ) 
		  + getWherePart( 'a.AppntSex','Sex') 
		  + getWherePart( 'a.AppntIDNo','IDNo' )		  
		  + " and a.ManageCom like '"+comCode+"%%'"
		  ;
		  if (fm.all('HomeAddress').value != '') 
		  {
				  strSQL = strSQL + " and a.AppntNo in (select CustomerNo FROM LCAddress where HomeAddress like  '"
				  + fm.all('HomeAddress').value +"%%' )";
		  }
		  if (fm.all('HomeZipCode').value != '') 
		  {
				  strSQL = strSQL + " and a.AppntNo in (select CustomerNo FROM LCAddress where HomeZipCode = '"
				  + fm.all('HomeZipCode').value +"' )";
		  }
		  
      strSQL = strSQL + " union "
      + "select a.ContNo,a.AppntNo,a.InsuredNo,'个单','',a.AppntName,a.CValiDate,a.Prem,'已签单',a.PaytoDate,'','', "
      + " a.PrtNo,'2',ShowManageName(a.ManageCom)"
      + "from LCCont a, LCInsured b "
      + "where a.GrpContNo='00000000000000000000' "
      + " and a.AppFlag = '1' "
      + " and a.ContNo = b.ContNo "
      + getWherePart( 'b.InsuredNo','CustomerNo' ) 
		  + getWherePart( 'b.Name','Name') 
		  + getWherePart( 'b.Birthday','Birthday' ) 
		  + getWherePart( 'b.Sex','Sex') 
		  + getWherePart( 'b.IDNo','IDNo' )		  
		  + " and a.ManageCom like '"+comCode+"%%'"
		  + " and b.InsuredNo not in (select AppntNo from LCCont where ContNo = b.ContNo)"
		  if (fm.all('HomeAddress').value != '') 
		  {
				  strSQL = strSQL + " and b.InsuredNo in (select CustomerNo FROM LCAddress where HomeAddress like  '"
				  + fm.all('HomeAddress').value +"%%' )";
		  }
		  if (fm.all('HomeZipCode').value != '') 
		  {
				  strSQL = strSQL + " and b.InsuredNo in (select CustomerNo FROM LCAddress where HomeZipCode = '"
				  + fm.all('HomeZipCode').value +"' )";
		  }
		  ;
    }else
    {
    	 strSQL = strSQL + "select a.ContNo,a.AppntNo,a.InsuredNo,'个单','',a.AppntName,a.CValiDate,a.Prem ,'已签单', "
      + " a.PaytoDate,'','',a.PrtNo,'1',ShowManageName(a.ManageCom) "
      + "from LCCont a where a.GrpContNo='00000000000000000000' and a.AppFlag='1' "     
		  + getWherePart( 'a.ContNo','ContNo' )
		  + " and a.ManageCom like '"+comCode+"%%'"
		  ;
		  strSQL = strSQL + " union "
      + "select a.ContNo,a.AppntNo,a.InsuredNo,'个单','',a.AppntName,a.CValiDate,a.Prem,'已签单',a.PaytoDate,'','', "
      + " a.PrtNo,'1',ShowManageName(a.ManageCom)"
      + "from LCCont a, LCInsured b "
      + "where a.GrpContNo='00000000000000000000' "
      + " and a.AppFlag = '1' "
      + " and a.ContNo = b.ContNo "      
		  + getWherePart( 'a.ContNo','ContNo' )
		  + " and a.ManageCom like '"+comCode+"%%'"
    }
     
    turnPage.queryModal(strSQL, GrpPolGrid); 
    setCosProrperty(); 
    setPolValue1();
}	
/*********************************************************************
 *  通过个单客户查询其他保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setPolValue1()
{
	initBContGrid();
  
	//保单性质为1：已签单； 2：撤保或退保
	//查询保单信息
	var strSQL = "";
	if (fm.all('ContNo').value == '') 
	{
		strSQL = "select a.ContNo, a.AppntNo, a.InsuredNo, '个单', '', a.AppntName, a.CValiDate, a.Prem, '已终止', b.ModifyDate, "
	      + " case when b.EdorType='CT' then '终止合同' "
	      + " when b.EdorType='XT' then '协议退保' "
	      + " when b.EdorType='WT' then '犹豫期退保' "
	      + " when b.EdorType='AE' then '更换投保人' end, "
	      + " b.ReasonCode, a.PrtNo, '1', ShowManageName(a.ManageCom)"
	      + "from LBCont a, LPEdorItem b "
	      + "where a.EdorNo = b.EdorNo "
	      + "and a.GrpContNo = '00000000000000000000' "
	      + getWherePart( 'a.AppntNo','CustomerNo' ) 
			  + getWherePart( 'a.AppntName','Name' ) 
			  + getWherePart( 'a.AppntBirthday','Birthday' ) 
			  + getWherePart( 'a.AppntSex','Sex') 
			  + getWherePart( 'a.AppntIDNo','IDNo' )
			  + getWherePart( 'a.ContNo','ContNo' )
			  + " and a.ManageCom like '"+comCode+"%%'"
			 ;
			   if (fm.all('HomeAddress').value != '') 
			  {
					  strSQL = strSQL + " and a.AppntNo in (select CustomerNo from LCAddress where HomeAddress like  '"
					  + fm.all('HomeAddress').value +"%%' )";
			  }
			  if (fm.all('HomeZipCode').value != '') 
			  {
					  strSQL = strSQL + " and a.AppntNo in (select CustomerNo from LCAddress where HomeZipCode = '"
					  + fm.all('HomeZipCode').value +"' )";
			  }
			  
			  strSQL = strSQL + " union "
	      + "select a.ContNo, a.AppntNo, a.InsuredNo, '个单', '', a.AppntName, a.CValiDate, a.Prem, '已终止', b.ModifyDate, "
	      + " case when b.EdorType='CT' then '终止合同' "
	      + " when b.EdorType='XT' then '协议退保' "
	      + " when b.EdorType='WT' then '犹豫期退保' "
	      + " when b.EdorType='AE' then '更换投保人' end, "
	      + " b.ReasonCode, a.PrtNo, '2', ShowManageName(a.ManageCom)"
	      + "from LBCont a, LPEdorItem b , LBInsured c "
	      + "where a.EdorNo = b.EdorNo "
	      + "and a.GrpContNo = '00000000000000000000' "
	      + getWherePart( 'c.InsuredNo','CustomerNo' ) 
			  + getWherePart( 'c.Name','Name' ) 
			  + getWherePart( 'c.Birthday','Birthday' ) 
			  + getWherePart( 'c.Sex','Sex') 
			  + getWherePart( 'c.IDNo','IDNo' )
			  + getWherePart( 'a.ContNo','ContNo' )
			  + " and a.ManageCom like '"+comCode+"%%'"
			  + " and a.ContNo = c.ContNo "
			  + " and c.InsuredNo not in (select AppntNo from LBCont where ContNo = a.ContNo)"
			  ;
			  if (fm.all('HomeAddress').value != '') 
			  {
					  strSQL = strSQL + " and c.InsuredNo in (select CustomerNo from LCAddress where HomeAddress like  '"
					  + fm.all('HomeAddress').value +"%%' )";
			  }
			  if (fm.all('HomeZipCode').value != '') 
			  {
					  strSQL = strSQL + " and c.InsuredNo in (select CustomerNo from LCAddress where HomeZipCode = '"
					  + fm.all('HomeZipCode').value +"' )";
			  }
			  ;
			}else
			{
				 strSQL = "select a.ContNo, a.AppntNo, a.InsuredNo, '个单', '', a.AppntName, a.CValiDate, a.Prem, '已终止', b.ModifyDate, "
	      + " case when b.EdorType='CT' then '终止合同' "
	      + " when b.EdorType='XT' then '协议退保' "
	      + " when b.EdorType='WT' then '犹豫期退保' "
	      + " when b.EdorType='AE' then '更换投保人' end, "
	      + " b.ReasonCode, a.PrtNo, '2', ShowManageName(a.ManageCom)"
	      + "from LBCont a, LPEdorItem b "
	      + "where a.EdorNo = b.EdorNo "
	      + "and a.GrpContNo = '00000000000000000000' "	      
			  + getWherePart( 'a.ContNo','ContNo' )
			  + " and a.ManageCom like '"+comCode+"%%'"
			 ;		  
			  
			  strSQL = strSQL + " union "
	      + "select a.ContNo, a.AppntNo, a.InsuredNo, '个单', '', a.AppntName, a.CValiDate, a.Prem, '已终止', b.ModifyDate, "
	      + " case when b.EdorType='CT' then '终止合同' "
	      + " when b.EdorType='XT' then '协议退保' "
	      + " when b.EdorType='WT' then '犹豫期退保' "
	      + " when b.EdorType='AE' then '更换投保人' end, "
	      + " b.ReasonCode, a.PrtNo, '2', ShowManageName(a.ManageCom)"
	      + "from LBCont a, LPEdorItem b , LCInsured c "
	      + "where a.EdorNo = b.EdorNo "
	      + "and a.GrpContNo = '00000000000000000000' "	      
			  + getWherePart( 'a.ContNo','ContNo' )
			  + " and a.ManageCom like '"+comCode+"%%'"
			  + " and a.ContNo = c.ContNo "
			  + " and c.InsuredNo not in (select AppntNo from LBCont where ContNo = a.ContNo)"
			  ;
			 
			}
     
    turnPage1.queryModal(strSQL, BContGrid);
    setCosProrperty1();
}	




/*********************************************************************
 *  查询有效保单客户属性信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setCosProrperty()
{
	if (GrpPolGrid.mulLineCount==0)
	{
		 alert("未查询到符合条件的有效保单信息");
     return false;
	}
	for (i = 0; i < GrpPolGrid.mulLineCount; i++)
	{
		if (fm.all('ContNo').value == '') 
	 {
		  var prorperty = "";
		  var contNo = GrpPolGrid.getRowColData(i, 1);	  
		  var contType = GrpPolGrid.getRowColData(i, 14);
		  var appntNo = GrpPolGrid.getRowColData(i, 2);
		  var insuredNo =  GrpPolGrid.getRowColData(i, 3);
	
		  //是被保人的单子
			  if (contType == "2") 
			  {
			      prorperty = "被保人";
			  }
			  else if (contType == "1")
			  {
				    if (insuredNo == appntNo)
				    {
				      prorperty = "投保人/被保人";
				    }
				    else
				    {
				      prorperty = "投保人";
				    }
			  }  
			  GrpPolGrid.setRowColData(i, 5, prorperty);	    
	  }   
	}
}
/*********************************************************************
 *  查询其他保单客户属性信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setCosProrperty1()
{
	if (BContGrid.mulLineCount==0)
	{
		 alert("未查询到符合条件的其他保单信息");
     return false;
	}
	for (i = 0; i < BContGrid.mulLineCount; i++)
	{
	  
	  if (fm.all('ContNo').value == '') 
	  {
	  	var prorperty = "";
	    var contNo = BContGrid.getRowColData(i, 1);	  
	    var contType = BContGrid.getRowColData(i, 14);
	    var appntNo = BContGrid.getRowColData(i, 2);
	    var insuredNo =  BContGrid.getRowColData(i, 3);
		  //是被保人的单子
			  if (contType == "2") 
			  {
			      prorperty = "被保人";
			  }
			  else if (contType == "1")
			  {
				    if (insuredNo == appntNo)
				    {
				      prorperty = "投保人/被保人";
				    }
				    else
				    {
				      prorperty = "投保人";
				    }
			  } 
			  BContGrid.setRowColData(i, 5, prorperty); 	    
	   }
	 }
}

//王珑制作