<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//修改：刘岩松
//修改日期：2004-2-17
%>
<%
	String tContNo = "";
	String tIsCancelPolFlag = "";
	String tContType = "";
	try
	{
		tContNo = request.getParameter("ContNo");
		tIsCancelPolFlag = request.getParameter("IsCancelPolFlag");
		tContType = request.getParameter("ContType");
		if(tContType == null)
		tContType = "1";//用于测试；
		System.out.println("tContNodfdf="+tContNo);
	}
	catch( Exception e )
	{
		tContNo = "";
		tIsCancelPolFlag = "0";
	}
%>
<head >
<script>
	var tContNo = "<%=tContNo%>";  
	var tIsCancelPolFlag = "<%=tIsCancelPolFlag%>";
	var tContType = "<%=tContType%>";	
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="PayHistoryInfo.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="PayHistoryInfoInit.jsp"%>
  <title>保单详细信息</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>保费历史资料</td>
		</tr>
	</table>
  	<Div  id= "divLCClent" style= "display: ''" align = center>
      	<table  class= common align = center>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanRiskGrid">
  					</span> 
  			  	</td>
     </div>
  			</tr>
		</table>
	<table align = center>
		<tr>
			<td>
  			<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage.previousPage();"> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage.lastPage();"> 
	</td>
</tr>
</table>
	<br>
  	<Div  id= "divLCClent" style= "display: ''" align = center>
      	<table  class= common align = center>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanRiskExtendGrid">
  					</span> 
  			  	</td>
     </div>
  			</tr>
		</table>
	<table align = center>
		<tr>
			<td>
  			<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage1.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage1.previousPage();"> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage1.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage1.lastPage();"> 
	</td>
</tr>
</table>
<br>
	<table class="common" id="table2">
		<tr CLASS="common">
			<td CLASS="title">险种代码</td>    		       
				<td CLASS="input" COLSPAN="1">
					<input NAME="RiskCode" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" READONLY="true">
				</td>	
			<td CLASS="title">险种名称</td>    		         
				<td CLASS="input" COLSPAN="1">
					<input NAME="RiskName" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20"READONLY="true">
				</td>		
			<td CLASS="title">被保人名称</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="InsuredName" VALUE CLASS="common" READONLY="true"TABINDEX="-1" >
				</td>
</tr>
<tr CLASS="common">
			<td CLASS="title">
				连身人姓名 </td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="LCInsuredRelatedName" VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
    	</td>	
		<TD CLASS=title  >
	     应缴日期 
	   </TD>
	   <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PayDate VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
	   </TD>	

			<TD  class= title>
      缴费日期
			</TD>
    <TD  class= input>
      <Input name=PayDate1 VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>
</tr>
<tr CLASS="common">
		<TD  class= title>
      期缴保费合计
			</TD>
    <TD  class= input>
      <Input name=SumPrem VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>	
    <TD  class= title>
        付费方式
    </TD>
    <TD  class= input>
       <Input  name=PayMode1 type=hidden VALUE MAXLENGTH="10" CLASS="common"  READONLY="true">
			 <Input  name=PayMode1Name VALUE MAXLENGTH="10" CLASS="common"  READONLY="true">    
    </TD> 
  <TD  class= title>
        收费方式
    </TD>
    <TD  class= input>
       <Input  name=PayMode type=hidden VALUE MAXLENGTH="10" CLASS="common"  READONLY="true">
       <Input  name=PayModeName VALUE MAXLENGTH="10" CLASS="common" READONLY="true">
    </TD>
</tr>
<tr CLASS="common">             			
		<TD CLASS=title  >
	      缴费方式 
	   </TD>
	   <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PayIntv type=hidden VALUE MAXLENGTH="10" CLASS="common" READONLY="true" >
	   		<Input NAME=PayIntvName  VALUE MAXLENGTH="10" CLASS="common" READONLY="true" >
	   </TD>	
	   		<TD CLASS=title  >
	     缴次 
	   </TD>
	   <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PayCount VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
	   </TD>	
	   <td CLASS="title">投保年度</td>
		<td CLASS="input" COLSPAN="1">
			<input NAME="InsuYear" VALUE CLASS="common" READONLY="true"TABINDEX="-1" MAXLENGTH="20" >
		</td>
</tr>
<tr CLASS="common">             			
		<TD CLASS=title  >
	      标准保费 
	   </TD>
	   <TD CLASS=input COLSPAN=1  >
	      <Input NAME=StandPrem  VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
	   </TD>	
	  			<td CLASS="title">被保人风险加费</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="InsuredRiskPrem" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
				</td>
					<td CLASS="title">业务员名称</td>
		<td CLASS="input" COLSPAN="1">
			<input NAME="AgentName" VALUE CLASS="common" READONLY="true"TABINDEX="-1" MAXLENGTH="20" >
		</td>
	</tr>
</table>
<br>
<table align= left>
<td>
     <INPUT class=CssButton VALUE="返 回" TYPE=button onclick="top.close()"> 
</td>
</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

