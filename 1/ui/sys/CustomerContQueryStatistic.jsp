<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：CustomerQueryStatistic
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>

<%
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>	        //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<%@include file="CustomerContQueryStatisticInit.jsp"%>
<SCRIPT src="CustomerContQueryStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>

<body  onload="initForm();initElementtype();" >
  <form  action="./CustomerContQueryStatisticSub.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class = codename name = ManageComName elementtype=nacessary></TD>          
     <TD  class= title>
          年龄段(岁)
     </TD>
    <TD  class= input  >
   <Input class= common name="AgeBegin" verify="开始年龄|len<=2" >
   </TD>
    <TD  class= title>
          至
     </TD>
    <TD  class= input  >
   <Input class= common name="AgeEnd" verify="结束年龄|len<=2" >
   </TD>   
          <TD>
          </TD>
        </TR>
        <TR  class= common>
           <TD  class= title>
            险种代码
          </TD>
          <TD  class= input>
            <Input class="codeno" name=RiskCode CodeData=""   ondblclick="getRisk(this); showCodeListEx('RiskCode',[this,RiskCodeName],[0,1],'', '', '', true);" onkeyup="showCodeListKeyEx('RiskCode',[this,RiskCodeName],[0,1],'', '', '', true);"><input class = codename name = RiskCodeName >
          </TD>
          <TD  class= title8>
            承保时间段（YYYY—MM—DD） 
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker"  dateFormat="short" name=SignDateBegin    verify="承保开始时间|date"  onkeyup="return ChangDateFormate(this,this.value)">
          </TD>
          <TD  class= title8>
            至 （YYYY—MM—DD）
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker"  dateFormat="short" name=SignDateEnd    verify="承保结束时间|date" onkeyup="return ChangDateFormate(this,this.value)">
          </TD>
                             
     <TD  class= title>
     类型
    </TD>
    <TD  class= input>
      <Input class= codeno name=QYType verify="|len<=20" CodeData= "0|^投保人方式查询|1^被保人方式查询|2^被保人保费方式查询|3" ondblClick= "showCodeListEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);" onkeyup= "showCodeListKeyEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);"><Input class= 'codename' name=QYType_ch  elementtype=nacessary >
    </TD>
    </TR>
    <TR  class= common>
          <TD  class= title>
            业务员代码
          </TD>
          <TD  class= input>
            <Input class=codeNo name=AgentCode  ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1,3,4],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
          </TD>
     <TD  class= title>
          保费范围(元)
     </TD>
    <TD  class= input  >
   <Input class= common name="PremBegin">
   </TD>
    <TD  class= title>
          至
     </TD>
    <TD  class= input  >
   <Input class= common name="PremEnd" >
   </TD>   
          <TD>
           </TD>  
        </TR>        
    </table>

    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>