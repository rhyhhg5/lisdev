<%
//程序名称：PayHistoryInfoInit.jsp
//程序功能：
//创建日期：2005-08-02
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">
                                      

function initForm()
{
  try
  {
    initRiskGrid();
	  RiskValue();
	  initRiskExtendGrid();
  }
  catch(ex)
  {
    alert("PayHistoryInfoInit.jsp->InitForm函数中发生异常:初始化界面错误!"+ex.message);
  }
}

// 保单信息列表的初始化
function initRiskGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种代码";         		//列名
      iArray[1][1]="130px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[2]=new Array();
      iArray[2][0]="险种名称";         		    //列名
      iArray[2][1]="200px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="被保人姓名";         		//列名
      iArray[3][1]="130px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[4]=new Array();
      iArray[4][0]="保单险种号码";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许

 
      RiskGrid = new MulLineEnter( "fm" , "RiskGrid" ); 
      //这些属性必须在loadMulLine前
      RiskGrid.mulLineCount = 0;   
      RiskGrid.displayTitle = 1;
      RiskGrid.locked = 1;
      RiskGrid.canSel = 1;
      RiskGrid.hiddenPlus = 1;
      RiskGrid.hiddenSubtraction = 1;
      RiskGrid.loadMulLine(iArray);
	    RiskGrid.selBoxEventFuncName = "HistoryRisk"; 

      }
      catch(ex)
      {
        alert("PayHistoryInfoInit.jsp-->RiskGrid:初始化界面错误!");
      }
}
function initRiskExtendGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种代码";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[2]=new Array();
      iArray[2][0]="险种名称";         		    //列名
      iArray[2][1]="200px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="金额";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[4]=new Array();
      iArray[4][0]="缴费日期";         		//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="保单险种号码";         //列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

 
      RiskExtendGrid = new MulLineEnter( "fm" , "RiskExtendGrid" ); 
      //这些属性必须在loadMulLine前
      RiskExtendGrid.mulLineCount = 0;   
      RiskExtendGrid.displayTitle = 1;
      RiskExtendGrid.locked = 1;
      RiskExtendGrid.canSel = 1;
      RiskExtendGrid.hiddenPlus = 1;
      RiskExtendGrid.hiddenSubtraction = 1;
      RiskExtendGrid.loadMulLine(iArray);
      RiskExtendGrid.selBoxEventFuncName = "DetailedRisk"; 

      }
      catch(ex)
      {
        alert("PayHistoryInfoInit.jsp-->RiskExtendGrid:初始化界面错误!");
      }
}
</script>