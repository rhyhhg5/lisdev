//程序名称:PEfficiencyInput.jsp
//程序功能：打印效率清单程序的函数处理程序
//创建日期：2003-04-04
//创建人  ：刘岩松

var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功

function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  showDiv(operateButton,"true");
    showDiv(inputButton,"false");
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
  	parent.fraMain.rows = "0,0,0,0,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//打印录入效率清单函数
function printInput()
{
	fm.OperateFlag.value = "Input";
	if ((fm.StartDate.value=="")||(fm.StartDate.value=="null"))
	{
		alert("请您输入开始日期！");
		return;
	}
	if((fm.EndDate.value=="")||(fm.EndDate.value=="null"))
	{
		alert("请您录入结束日期!");
		return;
	}
	else
	{
		fm.action = "./PEfficiencySave.jsp";
		fm.target="f1print";
    fm.submit();
  }
}

//打印复核效率清单函数
function printCheck()
{
	fm.OperateFlag.value = "Check";
	if((fm.StartDate.value=="")||(fm.StartDate.value=="null"))
	{
		alert("请您录入开始日期！");
		return;
	}
	if((fm.EndDate.value=="")||(fm.EndDate.value=="null"))
	{
		alert("请您录入结束日期！");
		return;
	}
	else
	{
		fm.action = "./PEfficiencySave.jsp";
		fm.target = "f1jprint";
		fm.submit();
	}
} 