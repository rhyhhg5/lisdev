//程序名称：TaskBack.js
//程序功能：程序功能：工单管理工单收回页面
//创建日期：2005-1-17 11:03:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var showInfo;

/* 提交表单 */
function submitForm()
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit();
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		location.reload();
	}
}    

/* 查询工单在列表中显示结果 */
function easyQueryClick()
{
	//初始化表格
	initGrid();
	
	//书写SQL语句
	var strSQL;
	strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, w.TypeNo, w.CustomerNo, " +
			 "       (Select Name from LDPerson where CustomerNo = w.CustomerNo), '记录', w.AcceptorNo, w.AcceptCom, w.AcceptDate, " +
			 "       Case When w.DateLimit Is Null then '' Else  trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
			 "       w.StatusNo, '', '' " +
			 "from   LGWork w, LGWorkTrace t, LGWorkBox b " +
	         "where  w.WorkNo = t.WorkNo " +
	      	 "and    w.NodeNo = t.NodeNo " +
			 "and    t.SendPersonNo = '" + operator + "' " +
			 "and    w.StatusNo = '2' " +    //未经办
			 "and    t.InMethodNo = '2' " +  //转交
			 "and    t.WorkBoxNo = b.WorkBoxNo " +
			 "order by t.InDate desc ";
	     
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	  
	//判断是否查询成功
	if (!turnPage.strQueryResult) 
	{
		return "";
	}
	
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = TaskGrid;    
	      
	//保存SQL语句
	turnPage.strQuerySql = strSQL; 
	
	//设置查询起始位置
	turnPage.pageIndex = 0;  
	
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	return true;
}

/* 得到被选中项� */
function getCheckNo() 
{	
	var checkNo = 0;
	for (i=0; i<TaskGrid.mulLineCount; i++) 
	{
		if (TaskGrid.getSelNo(i)) 
		{  
			checkNo =TaskGrid.getSelNo();
			break;
		}
	}
	return checkNo;
}

/* 查看选中的工单 */
function viewTask()
{
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) //取得选中工单的行号
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = TaskGrid.getRowColData(i, 1);
				CustomerNo = TaskGrid.getRowColData(i, 5);
			}
			if(chkNum > 1)
			{
				alert("您每次只能选一条工单！")
				return false;
			}
		}
	}
	
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (WorkNo)
	{
		location.replace("./TaskView.jsp?WorkNo="+WorkNo);
	}
}

/* 收回选中的工单 */
function backTask()
{
/*	var checkNo = getCheckNo();
	if (checkNo) 
	{ 
		var	WorkNo = TaskGrid.getRowColData(checkNo - 1, 1);
		if (WorkNo)
		{
			fm.WorkNo.value = WorkNo;
			submitForm();
		}
	}
	else 
	{
		alert("请先选择一条工单！"); 
	}
*/
	var chkNum = 0;
	var WorkNo;
	var AcceptNo;
	var StatusNo;
	
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
		/*	WorkNo = TaskGrid.getRowColData(i, 1);
			AcceptNo = TaskGrid.getRowColData(i, 2);
		  	StatusNo = TaskGrid.getRowColData(i, 12);
		  	if ((StatusNo != "2") && (StatusNo != "3") && (StatusNo != "3"))
		  	{
		  		alert("受理号为：" + AcceptNo + " 的工单不能转交！");
		  		return false;
		  	}*/		
			WorkNo = TaskGrid.getRowColData(i, 1);
		  	fm.all("TaskGridChk")[i].value = WorkNo;
		  	chkNum = chkNum + 1;
		}
	}
	
	if (chkNum == 0)
	{
		alert("请选择需转交的工单！");
		return false;
	}
	
	submitForm();
}

