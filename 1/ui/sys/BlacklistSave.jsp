<%
//程序名称：BlacklistInput.jsp
//程序功能：
//创建日期：2003-01-10
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LDBlacklistSchema tLDBlacklistSchema   = new LDBlacklistSchema();

  LDBlacklistUI tLDBlacklistUI = new LDBlacklistUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

//	tG.Operator = "Admin";
//	tG.ComCode  = "001";
//  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

		String tBlacklistType = new String();
    tLDBlacklistSchema.setBlacklistNo(request.getParameter("BlacklistNo"));
    tBlacklistType = request.getParameter("BlacklistType");
    tBlacklistType=StrTool.GBKToUnicode(tBlacklistType);
    System.out.println("类型1"+tBlacklistType);	
    if (tBlacklistType.equals("个人客户"))
    	System.out.println("111");
    	tLDBlacklistSchema.setBlacklistType("0");
    	
    if (tBlacklistType.equals("集体客户"))
    	tLDBlacklistSchema.setBlacklistType("1");	
    System.out.println("类型"+tLDBlacklistSchema.getBlacklistType());	
    tLDBlacklistSchema.setBlackName(request.getParameter("BlackName"));
//    tLDBlacklistSchema.setBlacklistOperator(request.getParameter("BlacklistOperator"));
    tLDBlacklistSchema.setBlacklistMakeDate(request.getParameter("BlacklistMakeDate"));
//    tLDBlacklistSchema.setBlacklistMakeTime(request.getParameter("BlacklistMakeTime"));
    tLDBlacklistSchema.setBlacklistMakeTime(request.getParameter("BlacklistOperator"));
    String Path = application.getRealPath("config//Conversion.config");
		tLDBlacklistSchema.setBlacklistReason(StrTool.Conversion(request.getParameter("BlacklistReason"),Path));


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLDBlacklistSchema);
	tVData.addElement(tG);
  try
  {
    tLDBlacklistUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLDBlacklistUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	//Content = " 保存失败，原因是:" + tError.getFirstError();
    	Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

