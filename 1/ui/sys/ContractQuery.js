var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var ContNo=" ";
var prtNo=" ";

/*********************************************************************
 *  查询个单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{

   initPolGrid();
   var strSQL = "";
   strSQL = "select a.contno, a.prtno,case ContType when '1' then '个人' when '2' then '团体' end,'', a.AppntName,'1'"
	   + "  from lccont a left join LCRnewStateLog b on a.ContNo = b.NewContNo "
	   + "where 1=1 "
	   + " and b.NewContNo is null "	
	   + getWherePart( 'a.AppntName','AppntName' ) 
	   + getWherePart( 'a.AppntSex','AppntSex' ) 
	   + getWherePart( 'a.AppntIDType','AppntIDType' ) 
	   + getWherePart( 'a.AppntIDNo','AppntIDNo') 
	   + getWherePart( 'a.InsuredName','InsuredName' )		   
	   + getWherePart( 'a.InsuredSex','InsuredSex' ) 
	   + getWherePart( 'a.InsuredIDType','InsuredIDType' )
	   + getWherePart( 'a.InsuredIDNo','InsuredIDNo' ) 
	   + getWherePart( 'a.ManageCom','ManageCom' ) 
	   + getWherePart( 'a.PolApplyDate','PolApplyDate' ) 
	   + " and  a.ContType='1' "
	   + " and a.appflag='0' "
	   ; 
	turnPage.queryModal(strSQL,PolGrid);
}

function Contract() {
	var tSel = PolGrid.getSelNo();
  var cProposalContNo = PolGrid.getRowColData( tSel - 1, 2 );
  var cConttype=PolGrid.getRowColData( tSel - 1, 6 );
  var arrReturn1 = new Array();
  try {
  	if(cConttype=='1'){
    var strSQl1="";
    strSQL1 ="select a.prtno,case a.ContType when '1' then '个人' when '2' then '团体' end,a.AppntName,a.PolApplyDate,a.prem,'','0',a.AppntName,a.AgentCode,(select Name from laagent where AgentCode=a.AgentCode),a.contno,a.SignDate,case a.appflag when '1' then '已签单' when '0' then '未签单' end,a.CValiDate from lccont a where a.prtno='"+cProposalContNo+"' and a.conttype='1'";
      }
    if(strSQL1 != "") {
    	
//    	var str3 = easyQueryVer3(strSQL1);       
//    	arrReturn1 = decodeEasyQueryResult(str3);
     arrReturn1 =easyExecSql(strSQL1);
    }
    if (arrReturn1 == ""||arrReturn1 == null) {
      return false;
    } else {
      display(arrReturn1[0]);
    }
  } catch(ex) {
    alert("在ContractQueryInit.jsp-->Contract函数中发生异常:初始化界面错误!" + ex);
  }
  initPolStatuGrid();
  QueryAddFee();
  fm.action = "../uw/PolStatusChk.jsp";
  fm.submit(); //提交
}
/*********************************************************************
 *  设置保单基本信息
 *  参数  ：  cArr 数组
 *  返回值：  无
 *********************************************************************
 */
  function display(cArr)
  {    
  	try { fm.all('prtNo').value = cArr[0]; } catch(ex) { };
  	try { fm.all('Conttype').value = cArr[1]; } catch(ex) { };
  	try { fm.all('AppntName1').value = cArr[2]; } catch(ex) { };
  	try { fm.all('PolApplyDate1').value = cArr[3]; } catch(ex) { };
		try { fm.all('Prem').value = cArr[4]; } catch(ex) { };
  	try { fm.all('FirstPrem').value = cArr[5]; } catch(ex) { };
		try { fm.all('Dif').value = cArr[6]; } catch(ex) { };
		try { fm.all('PayPerson').value = cArr[7]; } catch(ex) { };
		try { fm.all('AgentCode').value = cArr[8]; } catch(ex) { };
		try { fm.all('AgentName').value = cArr[9]; } catch(ex) { };		
		try { fm.all('ContNo').value = cArr[10]; } catch(ex) { };
		try { fm.all('SignDate').value = cArr[11]; } catch(ex) { };
		try { fm.all('ContState').value = cArr[12]; } catch(ex) { };
		try { fm.all('CValiDate').value = cArr[13]; } catch(ex) { };
	}
function afterSubmit(FlagStr,Content)
{
	
}
function QueryAddFee(){
	var tSel = PolGrid.getSelNo();
  var cProposalContNo = PolGrid.getRowColData( tSel - 1, 1 );
	var strSql = "select sum(prem) from lcprem where contno='"+cProposalContNo+"' and payplancode like '000000%%'";
	var arr = easyExecSql(strSql);
	var strSql_1 = "select sum(prem) from lcpol where contno='"+cProposalContNo+"'";
	var arr_1 = easyExecSql(strSql_1);
	if(arr!="null" && arr_1 !="null"){
		var strResult = "保单加费"+arr[0][0]+"元,总保费"+arr_1[0][0]+"元";
		AddfeeInfoGrid.clearData();
		AddfeeInfoGrid.addOne();
		AddfeeInfoGrid.setRowColData(0,1,strResult);
	}
}
function QuerySpecInfo(){
	var tSel = PolGrid.getSelNo();
  var cProposalContNo = PolGrid.getRowColData( tSel - 1, 1 );
	var strSql = "select speccontnet from LCSpec where contno='"+cProposalContNo+"'";
	turnPage.queryModal(strSql,SpecInfoGrid);
}

function showComplexRiskInfo()
{
	var tSel2 = PolGrid.getSelNo();
	if( tSel2 == 0 || tSel2 == null )
	{		
		alert("请选择列表中的一条记录!!!");
	}
	else{
		ContNo = PolGrid.getRowColData( tSel2 - 1, 1 );
		prtNo=PolGrid.getRowColData( tSel2 - 1, 2 );
		window.open("./ComplexRiskInput.jsp?ContNo="+ContNo+"&prtNo="+prtNo,"", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
	}
}
function ScanQuery() {
var tSel2 = PolGrid.getSelNo();
	if( tSel2 == 0 || tSel2 == null )
	{		
		alert("请选择列表中的一条记录!!!");
	}
	else{
		prtNo=PolGrid.getRowColData( tSel2 - 1, 2 );
    window.open("../sys/LCProposalScanQuery.jsp?prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
			}
}
