<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//修改：刘岩松
//修改日期：2004-2-17
%>
<%
	String tContNo = "";
	String tIsCancelPolFlag = "";
	String tContType = "";
	try
	{
		tContNo = request.getParameter("ContNo");
		tIsCancelPolFlag = request.getParameter("IsCancelPolFlag");
		tContType = request.getParameter("ContType");
		System.out.println("tContNodfdf="+tContNo);
	}
	catch( Exception e )
	{
		tContNo = "";
		tIsCancelPolFlag = "0";
	}
%>
<head >
<script>
	var tContNo = "<%=tContNo%>"; 
	var tIsCancelPolFlag = "<%=tIsCancelPolFlag%>";
	var tContType = "<%=tContType%>";	
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="PolDetailQuery.js"></SCRIPT>
	<!--<SCRIPT src="AllProposalQuery.js"></SCRIPT>-->
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="PolDetailQueryInit.jsp"%>
	
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>保单查询 </title>
</head>

<body  onload="initForm();" >
  <form  name=fm >   
    <table>
    	<tr>
        <td class=common>
			
    		</td>
    		<td class= titleImg>
    			合同信息
    		</td>
    	</tr>
    </table>
   <div id="DivLCCont" STYLE="display:''">
	<table class="common" id="table2">
		<tr CLASS="common">
			<td CLASS="title">保单状态2 </td>    		       
			<td CLASS="input" COLSPAN="1">
			  <input NAME="ContNo" type=hidden VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" >
    		<input NAME="AppFlag" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20"READONLY="true">
    	</td>
			<td CLASS="title">最后处理人</td>    		         
			<td CLASS="input" COLSPAN="1">
			<input NAME="Operator" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="14" READONLY="true"></td>
    		        
			<td CLASS="title">最后处理时间</td>
    		         
			<td CLASS="input" COLSPAN="1">
			  <input NAME="ModifyDate" VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    	</td>
    	<td CLASS="title">管理机构 </td>    		        
			<td CLASS="input" COLSPAN="1">
			<Input class="code" name=ManageCom ondblclick="return showCodeList('comcode',[this]);" onkeyup="return showCodeListKey('comcode',[this]);"> 
			</td>    		          
		</tr>
		<tr CLASS="common">
		  <td CLASS="title">销售渠道 </td>    		        
			<td CLASS="input" COLSPAN="1">
			  <Input class="code" name=SaleChnl> 
			</td>    
			<td CLASS="title">业务员代码</td>    		           
			<td CLASS="input" COLSPAN="1">
			<input NAME="AgentCode" VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
    		         </td>
			<td CLASS="title">业务员名称</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="AgentName" VALUE CLASS="common" READONLY="true"TABINDEX="-1" MAXLENGTH="20" >
    	</td>
			<td CLASS="title">所属部门</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="BranchCode" VALUE CLASS="common" READONLY="true"TABINDEX="-1" >
			</td>
		</tr>
		<tr class=common>
			<td class=title>资格证书号码</td>
			<td class=input>
				<input class=common readonly=true name=Certificate>
			</td>
			<td class=title>展业证书号码</td>
			<td class=input>
				<input class=common readonly=true name=Exhibition>
			</td>
			<td class=title id=T1 style="display:''">代理销售业务员编码</td>
			<td class=input id=I1 style="display:''">
				<input class=common readonly=true name=SalesAgentCode>
			</td>
			<td class=title id=T2 style="display:''">代理销售业务员姓名</td>
			<td class=input id=I2 style="display:''">
				<input class=common readonly=true name=SalesAgentName>
			</td>
		</tr>
		<tr CLASS="common">
       <td CLASS="title">联系电话</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="Phone" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
			<td CLASS="title">联系地址</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="PostalAddress" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
			<td CLASS="title">上次抽档时间</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="PostalDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
			<td CLASS="title">是否缓缴</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="Huan" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
    </tr>
    <tr CLASS="common">
    	<td CLASS="title">销售机构代码</td>
		<td CLASS="input" COLSPAN="1">
			<input NAME="SaleChnlCode" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
    	<td CLASS="title">销售机构地址</td>
		<td CLASS="input" COLSPAN="1">
			<input NAME="SaleChnlAddress" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
       <td CLASS="title">银代销售</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="BankName" VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
    	</td>
			<td CLASS="title">中介销售</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="agency" VALUE CLASS="common" READONLY="true" TABINDEX="-1">
			</td>
		
    </tr>
    
		<tr CLASS="common" style="display:none">
			<td CLASS="title">邮政编码  </td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="ZipCode" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
<!--			
      <td CLASS="title">缴费频次 </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="PayIntv" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
-->    	
			<td CLASS="title">期缴保费 </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="Prem2" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
    </tr>
		<tr CLASS="common">
			<td CLASS="title">保单回执客户签收日期  </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="CustomGetPolDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
      <td CLASS="title">业务员递交客户回执时间跨度（天） </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="CustomPolInteDay" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
    	<td CLASS="title">保单补发次数  </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="LostTimes" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
			
    </tr>
		<tr CLASS="common">
			<td CLASS="title">承保日期  </td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="SignDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
            <td CLASS="title">保单生效日 </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="CValiDate" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
			<td CLASS="title">保单满期日</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="CInValiDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
    	<td CLASS="title">保费交至日</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="PayToDate2" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
    </tr>
  </table>
	   <table class=common>	     
         <TR  class= common> 
           <TD  class= common>  特别约定 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Remark" cols="110" rows="2" class="common" READONLY="TRUE"></textarea>
           </TD>
         </TR>
		 <TR  class= common> 
           <TD  class= common>  核保结论 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="PassFalg" cols="110" rows="2" class="common" READONLY="TRUE"></textarea>
           </TD>
         </TR>		  
      </table>
        <table>
    	<tr>
        <td class=common>
			
    		</td>
    		<td class= titleImg> 
    			交叉销售信息 <font color="#ff0000">暂不显示银保通保单交叉销售信息</font> 
    		</td>
    	</tr>
    </table>
      <table class="common" id="table2">
       <tr class="common8" id="GrpAgentComID">
            <td class="title8">交叉销售渠道</td>
            <td class="input8">
                <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl"  readonly="readonly" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" />
            </td>
            <td class="title8">交叉销售业务类型</td>
            <td class="input8">
                <input class="codeNo" name="Crs_BussType" id="Crs_BussType" readonly="readonly" /><input class="codename" name="Crs_BussTypeName" readonly="readonly" />
            </td>
            <td class="title8">&nbsp;</td>
            <td class="input8">&nbsp;</td>
        </tr>	
		<tr class=common id="GrpAgentTitleID">
    		<td CLASS="title" >对方机构代码</td>
			<td CLASS="input" COLSPAN="1" >
    	      <Input class="code" name="GrpAgentCom" readonly="readonly" />
    	    </td>
    	    <td  class= title>对方机构名称</td>
	        <td  class= input>
	          <Input class="common" name="GrpAgentComName"  TABINDEX="-1" readonly >
	        </td>  
			<td CLASS="title">对方业务员代码</td>
    		<td CLASS="input" COLSPAN="1">
			<input NAME="GrpAgentCode" VALUE MAXLENGTH="20" CLASS="code" readonly="readonly" / >
    		</td>
    </tr>
    <tr class=common id="GrpAgentTitleIDNo">
        <td  class="title" >对方业务员姓名</td>
	    <td  class="input" COLSPAN="1">
	        <Input  name=GrpAgentName CLASS="common" readonly="readonly" />
	    </td>
        <td CLASS="title">对方业务员身份证</td>
     	<td CLASS="input" COLSPAN="1">
			<input NAME="GrpAgentIDNo" VALUE MAXLENGTH="18" CLASS="code" readonly="readonly" />
    	</td>
    </tr>	
      </table>
      
      
   </div>  
    <%@include file="ComplexAppnt.jsp"%>   
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick= "showPage(this,divPol1);">
    		</td>
    		<td class= titleImg>
    			 保单险种信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>    	
    	
    	<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage.firstPage();">
    	<INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage.previousPage();"> 
    	<INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage.nextPage();"> 
    	<INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage.lastPage();"> 		      
    </Div>
    <br>
    <Div>
    <table>
      <INPUT class=CssButton VALUE="险种明细" TYPE=button onclick="riskQueryClick();"> 
      <INPUT class=CssButton VALUE="被保人信息" TYPE=button onclick="InsuredQueryClick();"> 
      <INPUT class=CssButton VALUE="保单首/续期缴费明细"TYPE=button onclick="payQueryClick();">
      <INPUT class=CssButton VALUE="扫描件查询" TYPE=button onclick="ScanQuery();"> 
      <INPUT class=CssButton VALUE="保全查询"TYPE=button onclick="taskQueryClick();"> 
      <INPUT class=CssButton VALUE="理赔查询" TYPE=button onclick="claimQueryClick();">  
      <INPUT class=CssButton VALUE="客服信息" TYPE=button onclick="serviceClick();">   
      <INPUT class=CssButton VALUE="万能账户信息" TYPE=button onclick="omnipotenceAcc();">  
      <INPUT class=CssButton VALUE="分红信息" TYPE=button onclick="bonusShareQuery();">  
      <INPUT class=CssButton VALUE="返  回" TYPE=button onclick="GoBack();"> 
  	</table>		
  	</Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


