<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件

    fm.all('CustomerNo').value =customerNo;	
    fm.all('Name').value = '';
    fm.all('Birthday').value = '';    
    fm.all('Sex').value = '';
    fm.all('IDNo').value = '';
	fm.all('ContNo').value = '';	

  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm()
{
  try
  {
    initInpBox();
    initGrpPolGrid();
    initPersonGrid();
	initGrpPersonGrid();
	if (customerNo =='' || customerNo ==null)
	{}
	else
	{
		easyQueryClick();
	}
  }
  catch(re)
  {
    alert("GroupPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号码";         		//列名
      iArray[1][1]="130px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[2]=new Array();
      iArray[2][0]="属性";         		    //列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="投保人";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[4]=new Array();
      iArray[4][0]="生效时间";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[5]=new Array();
      iArray[5][0]="总保费";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="保单状态";         		//列名
      iArray[6][1]="50px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[7]=new Array();
      iArray[7][0]="投保人客户号";         		//列名
      iArray[7][1]="0px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=3; 

      iArray[8]=new Array();
      iArray[8][0]="标志";         		//列名
      iArray[8][1]="30px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0; 


	  iArray[9]=new Array();
      iArray[9][0]="保单性质";         		//列名
      iArray[9][1]="0px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=3; 


      GrpPolGrid = new MulLineEnter( "fm" , "GrpPolGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPolGrid.mulLineCount = 0;   
      GrpPolGrid.displayTitle = 1;
      GrpPolGrid.locked = 1;
      GrpPolGrid.canSel = 1;
      GrpPolGrid.hiddenPlus = 1;
      GrpPolGrid.hiddenSubtraction = 1;
      GrpPolGrid.loadMulLine(iArray); 
	  
      
      //这些操作必须在loadMulLine后面
      //GrpPolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("GroupPolQueryInit.jsp-->initGrpPolGrid函数中发生异常:初始化界面错误!");
      }
}
// 个单客户信息列表的初始化
function initPersonGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="客户号码";         		//列名
      iArray[1][1]="90px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="客户姓名";         		//列名
      iArray[2][1]="90px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="性别";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][10] = "Sex";
      iArray[3][11] = "0|^0|男^1|女^2|不详";
      iArray[3][12] = "3";
      iArray[3][19] = "0";		

      iArray[4]=new Array();
      iArray[4][0]="出生日期";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="证件号码";         		//列名
      iArray[5][1]="130px";            		//列宽
      iArray[5][2]=130;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[6]=new Array();
      iArray[6][0]="证件类型";         		//列名
      iArray[6][1]="50px";            		//列宽
      iArray[6][2]=70;            			//列最大值
      iArray[6][3]=2; 
      iArray[6][4]="IDType";              	        //是否引用代码:null||""为不引用
      iArray[6][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[6][9]="证件类型|code:IDType&NOTNULL";
      iArray[6][18]=250;
      iArray[6][19]= 0 ;

	
      PersonGrid = new MulLineEnter( "fm" , "PersonGrid" ); 
      //这些属性必须在loadMulLine前
      PersonGrid.mulLineCount = 0;   
      PersonGrid.displayTitle = 1;
      PersonGrid.locked = 1;
      PersonGrid.canSel = 1;
      PersonGrid.hiddenPlus=1;
      PersonGrid.hiddenSubtraction=1;
      PersonGrid.loadMulLine(iArray); 
	  PersonGrid.selBoxEventFuncName = "setPolValue";
      
      //这些操作必须在loadMulLine后面
      //ClientGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("GroupPolQueryInit.jsp-->initPersonGrid函数中发生异常:初始化界面错误!");
      }
}
//团单客户信息列表的初始化
function initGrpPersonGrid()
{     
	 var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="客户号码";         		//列名
      iArray[1][1]="90px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="单位名称";         		//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="负责人";         		//列名
      iArray[3][1]="90px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[4]=new Array();
      iArray[4][0]="成立日期";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=70;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
	  iArray[5]=new Array();
      iArray[5][0]="单位性质";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[6]=new Array();
      iArray[6][0]="行业类别";         		//列名
      iArray[6][1]="40px";            		//列宽
      iArray[6][2]=80;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      GrpPersonGrid = new MulLineEnter( "fm" , "GrpPersonGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPersonGrid.mulLineCount = 0;   
      GrpPersonGrid.displayTitle = 1;
      GrpPersonGrid.locked = 1;
      GrpPersonGrid.canSel = 1;
      GrpPersonGrid.hiddenPlus=1;
      GrpPersonGrid.hiddenSubtraction=1;
      GrpPersonGrid.loadMulLine(iArray); 
	  GrpPersonGrid.selBoxEventFuncName = "setGrpPolValue";
      
      //这些操作必须在loadMulLine后面
      //ClientGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("GroupPolQueryInit.jsp-->initGrpPersonGrid函数中发生异常:初始化界面错误!");
      }
}
</script>