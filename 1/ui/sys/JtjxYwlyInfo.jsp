<%@page contentType="text/html;charset=GBK" %>
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>

<body >
<Form method=post name=fm target="fraSubmit">
	<br>
	<br> 
	<font size=3>
  	<b>交叉销售来源说明</b>
  	<p></p>
	  &nbsp;&nbsp;相互代理：即代理公司利用销售队伍、柜面服务人员或公司员工、电话营销、网络营销等营销资源独立完成销售被代理公司保险产品，	或者被代理公司利用代理公司的银保专员队伍实现产品销售。<br>
	  			<p></p>
	  &nbsp;&nbsp;联合展业：即代理公司通过提供重要信息等方式协助被代理公司展业，或者代理公司和被代理公司联合向同一目标客户销售各自的保险产品，通过共同展业或者共保展业，形成产品、服务综合优势，最终实现成功销售。<br>
				<p></p>
	  &nbsp;&nbsp;渠道共享：即代理公司凭借与银行、邮政、4S店等兼业代理渠道已经建立的良好合作关系，协助被代理公司建立与该兼业代理机构的合作关系，销售被代理公司保险产品；或者代理公司与被代理公司借助综合实力和统一品牌联合开拓新的第三方销售渠道。<br>
	  			<p></p>
	  &nbsp;&nbsp;互动部：即在人保寿险或人保健康未设县（或地市）级机构的地区，由人保财险在县（或地市）级机构内部设立互动部，代理寿险或健康险业务。待互动部达到一定业务规模时，经监管机构批准可以成立人保寿险或人保健康的分支机构。<br>
	  			<p></p>
	  &nbsp;&nbsp;农网共建（互动业务）：即在人保财险、人保寿险、人保健康均不具备共建条件的地区，借助农经站、畜牧站、林管站和银邮网点等外部服务资源，建立中国人保农村保险服务站或联络点，以政策性农险业务为切入点，宣传推荐中国人保的保险产品。<br>
				<p></p>
	</font>
	<table class= common>
		<tr><td align=center><input type =button class=cssButton value="关 闭" onclick="window.close();"></td></tr>
	</table>
</body>
</html>
 