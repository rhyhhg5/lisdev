<%
//程序名称：LOPolLuckNoInput.jsp
//程序功能：功能描述
//创建日期：2004-08-01 20:54:02
//创建人  ：yangtao
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LOPolLuckNoQueryInput.js"></SCRIPT> 
  <%@include file="LOPolLuckNoQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLOPolLuckNoGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      印刷号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PrtNo >
    </TD>
    <TD  class= title>
      投保人名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      备注
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Remark >
    </TD>
    <TD  class= title>
      保单状态
    </TD>
    <TD  class= input>
      <Input class= 'code' name="PolState" CodeData="0|^1|未签单^2|已经签单未打印^3|已经签单已打印" ondblClick="showCodeListEx('PolState_LuckPol',[this],[0,1,2]);" onkeyup="showCodeListKeyEx('PolState_LuckPol',[this],[0,1,2]);" value="2">
    </TD>
  </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询" TYPE=button class=common onclick="easyQueryClick();">
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLOPolLuckNo1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLOPolLuckNo1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLOPolLuckNoGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
