<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.monitor.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.math.*"%>
<%@page import="java.util.*"%>
<html> 
<head>
	<title>系统状态</title>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
</head>
<body>

<form action="./ViewServerInfoSubmit.jsp" method=post name=fm target="_blank">
	<input VALUE="建议GC" class = cssButton TYPE=button onclick="fm.submit();">
</form>
	
<%
   String serverName = request.getParameter("serverName");

   String a = "服务器时间______= "+PubFun.getCurrentDate() +" "+ PubFun.getCurrentTime();
   IMonitorService service = new MonitorServiceImpl();
   MonitorInfoBean monitorInfo = service.getMonitorInfoBean();
   
   String b = "操作系统________= " + monitorInfo.getOsName();
   String c = "Heap可用内存____= " + monitorInfo.getTotalMemory()+" KB";
   String d = "Heap最大可用内存= " + monitorInfo.getMaxMemory()+" KB";
   String f = "Heap剩余内存____= " + monitorInfo.getFreeMemory()+" KB";
   String g = "Heap使用内存____= " + (monitorInfo.getTotalMemory() - monitorInfo.getFreeMemory()) + " KB";

   long memoryRate = Math.round(100 * (monitorInfo.getTotalMemory() - monitorInfo.getFreeMemory()) / monitorInfo.getMaxMemory());
   String h = "Heap已用占比____= "+ memoryRate + "%";
   
   String i = "";
   String s = "";
   boolean healthy = true;
   
   if(memoryRate > 30){
       healthy = false;
       i = "★★★"+serverName+"--> Heap已用占比为："+memoryRate+"%，已建议系统进行垃圾回收（GC）。";
       s = "★已建议垃圾回收★";
       try{
           System.gc();
       }catch(Exception ex){
           ex.printStackTrace();
       }
   }else{
       i = "☆☆☆"+serverName+"--> Heap已用占比为："+memoryRate+"%，未建议系统进行垃圾回收（GC）。";
       s = "☆不必垃圾回收☆";
   }
   
   System.out.println(a);
   System.out.println(b);
   System.out.println(c);
   System.out.println(d);
   System.out.println(f);
   System.out.println(g);
   System.out.println(h);
   System.out.println(i);
   
   String userCount = "0";
   String userCodeList = "";
   
   SSRS tSS = new SSRS();
   tSS = PubFun.getSome("LDUser","'1'","1","UserCode,UserName","");
   HashMap tMap = new HashMap();
   if(tSS != null && tSS.getMaxRow() >0 ){
       for(int t=1;t<=tSS.getMaxRow();t++){
           tMap.put(tSS.GetText(t,1), tSS.GetText(t,2));
       }
   }
   
   HashMap ht = (HashMap) application.getAttribute("ht");

   if(ht!=null){
       HashMap haveALook = (HashMap) ht.clone();//深拷贝
       userCount = String.valueOf(haveALook.keySet().size());
       System.out.println("HashMap-> 当前Session数："+userCount);
			 Iterator iterator = haveALook.keySet().iterator();
			 while (iterator.hasNext()) {
		      try {
			        String sessionid2 = (String) iterator.next();
			        HttpSession object2 = (HttpSession) haveALook.get(sessionid2);
			        System.out.println("HashMap-> 当前SessionID："+object2.getId());
			        try {
				          GlobalInput tG2 = (GlobalInput) object2.getAttribute("GI");
				          if (tG2 !=null){
				              String tOperatorName = (String)tMap.get(tG2.Operator);
                      userCodeList += tG2.Operator + " -> " + tOperatorName +"<br>";
				          }
			        } catch (IllegalStateException ex) {
			            //这个时候可能Session已经被销毁了，不用再统计他了
                  //userCodeList = "统计在线用户过程中，有人登入或登出，请稍后刷新重试。";
			        }
		      } catch (Exception ex) {
		          userCodeList = "统计在线用户过程中，有人登入或登出，请稍后刷新重试。";
		      }
			 }
   }
   
  //HashMap hs = (HashMap) application.getAttribute("sessions");
  //if(hs!=null){
  //    System.out.println("sessions-> 当前Session数: "+hs.size());
	//	   Iterator iterator = hs.keySet().iterator();
	//	   while (iterator.hasNext()) {
	//	       String sessionid2 = (String) iterator.next();
	//	       System.out.println("sessions-> 当前SessionID: "+sessionid2);
	//	   }
	// }
   
   String p = "当前用户数："+userCount;
   System.out.println(p);
   
   String q = "当前用户如下：";
   String r = userCodeList;
   
   Properties props=System.getProperties(); //系统属性

   String j = "Java版本________= "+props.getProperty("java.version");
   String k = "Java供应商______= "+props.getProperty("java.vendor");
   String l = "Java虚拟机版本__= "+props.getProperty("java.vm.version");
   String m = "操作系统的版本__= "+props.getProperty("os.version");
   String n = "操作系统的构架__= "+props.getProperty("os.arch");

//   System.out.println(j);
//   System.out.println(k);   
//   System.out.println(l);
//   System.out.println(m);
//   System.out.println(n);
   
//   System.out.println("Java供应商的URL："+props.getProperty("java.vendor.url"));
//   System.out.println("Java的安装路径："+props.getProperty("java.home"));
//   System.out.println("Java的虚拟机规范版本："+props.getProperty("java.vm.specification.version"));
//   System.out.println("Java的虚拟机规范供应商："+props.getProperty("java.vm.specification.vendor"));
//   System.out.println("Java的虚拟机规范名称："+props.getProperty("java.vm.specification.name"));
//   System.out.println("Java的虚拟机实现供应商："+props.getProperty("java.vm.vendor"));
//   System.out.println("Java的虚拟机实现名称："+props.getProperty("java.vm.name"));
//   System.out.println("Java运行时环境规范版本："+props.getProperty("java.specification.version"));
//   System.out.println("Java运行时环境规范供应商："+props.getProperty("java.specification.vender"));
//   System.out.println("Java运行时环境规范名称："+props.getProperty("java.specification.name"));
//   System.out.println("Java的类格式版本号："+props.getProperty("java.class.version"));
//   System.out.println("Java的类路径："+props.getProperty("java.class.path"));
//   System.out.println("加载库时搜索的路径列表："+props.getProperty("java.library.path"));
//   System.out.println("默认的临时文件路径："+props.getProperty("java.io.tmpdir"));
//   System.out.println("一个或多个扩展目录的路径："+props.getProperty("java.ext.dirs"));
//   System.out.println("操作系统的名称："+props.getProperty("os.name"));
//   System.out.println("文件分隔符："+props.getProperty("file.separator"));   //在 unix 系统中是＂／＂
//   System.out.println("路径分隔符："+props.getProperty("path.separator"));   //在 unix 系统中是＂:＂
//   System.out.println("行分隔符："+props.getProperty("line.separator"));   //在 unix 系统中是＂/n＂
//   System.out.println("用户的账户名称："+props.getProperty("user.name"));
//   System.out.println("用户的主目录："+props.getProperty("user.home"));
//   System.out.println("用户的当前工作目录："+props.getProperty("user.dir"));
   
%>
	<%=a%>
	<br>
	<%=b%>
	<br>
	<%=c%>
	<br>
	<%=d%>
	<br><br>
	<%=f%>
	<br>
	<%=g%>
	<br>

	<%
	   if(healthy){
	%>
	<font color=blue><%=h%></font>
  <br>
	<font color=blue><%=s%></font>	
	<%
     }else{
	%>	
	<font color=red><%=h%></font>
  <br>
	<font color=red><%=s%></font>		
	<%
	   }
	%>

	<br><br>
	<%=j%>
	<br>
	<%=k%>
	<br>
	<%=l%>
	<br>
	<%=m%>
	<br>
	<%=n%>
	<br><br>
	<%=p%>
	<br><br>
	<%=q%>
	<br>
	<%=r%>

</body>
</html>

