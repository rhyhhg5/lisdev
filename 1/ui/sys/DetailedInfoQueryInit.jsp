<%
//程序名称：DetailedInfoQueryInit.jsp
//程序功能：
//创建日期：2005-08-02
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">
                                      

function initForm()
{
  try
  {
    initRiskPolGrid();
    setRiskPolValue();
  }
  catch(ex)
  {
    alert("DetailedInfoQueryInit.jsp->InitForm函数中发生异常:初始化界面错误!"+ex.message);
  }
}

// 保单信息列表的初始化
function initRiskPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种代码";         		//列名
      iArray[1][1]="130px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[2]=new Array();
      iArray[2][0]="险种名称";         		//列名
      iArray[2][1]="130px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  	iArray[3]=new Array();
      iArray[3][0]="被保人姓名";         		    //列名
      iArray[3][1]="200px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="客户号";         		//列名
      iArray[4][1]="130px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[5]=new Array();
      iArray[5][0]="保单险种号码";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

 
      RiskPolGrid = new MulLineEnter( "fm" , "RiskPolGrid" ); 
      //这些属性必须在loadMulLine前
      RiskPolGrid.mulLineCount = 0;   
      RiskPolGrid.displayTitle = 1;
      RiskPolGrid.locked = 1;
      RiskPolGrid.canSel = 1;
      RiskPolGrid.hiddenPlus = 1;
      RiskPolGrid.hiddenSubtraction = 1;
      RiskPolGrid.loadMulLine(iArray);
      RiskPolGrid.selBoxEventFuncName = "DetailedRiskpol"; 
	  
      
      //这些操作必须在loadMulLine后面
      //GrpPolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("DetailedInfoQueryInit.jsp-->RiskPolGrid:初始化界面错误!");
      }
}

</script>