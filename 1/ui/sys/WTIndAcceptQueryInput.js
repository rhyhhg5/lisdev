var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mSql="";
//简单查询
function easyQuery(){

    if (verifyInput() == false)
    return false;
    var startDate=fm.all('StartDate').value;
    var endDate=fm.all('EndDate').value;
    var tManageCom = fm.all('ManageCom').value;//管理机构
    var tSaleChnl = fm.all('SaleChnl').value;//销售渠道

    var wherePar="";
    
    //限制管理机构
    if(!(tManageCom == "" || tManageCom == null))
    {
      wherePar+=" and  a.ManageCom like '"+tManageCom+"%'" ; 
    }
    //限制销售渠道
    if(!(tSaleChnl == "" || tSaleChnl == null))
    {
      wherePar+=" and  a.SaleChnl ='"+tSaleChnl+"'" ; 
    }
    //统计起期
    if(!(startDate == "" || startDate == null))
    {
      wherePar+=" and  (select confdate from lpedorapp where edoracceptno=b.edoracceptno) >='"+startDate+"'" ; 
    }
    if(!(endDate == "" || endDate == null))
    {
      wherePar+=" and  (select confdate from lpedorapp where edoracceptno=b.edoracceptno) <='"+endDate+"'" ; 
    }



    var strSQL="select a.contno 保单号,"
              +"(select name from ldcom where comcode=a.managecom)  所属机构 ,"
              +"a.signdate 签单日期,"
              +"a.cvalidate 生效日期 ,"
              +"a.payenddate  满期日期,"
              +"current date  数据抽档日期,"
              +"a.appntno 客户号码,"
              +"a.appntname 客户名称,"
              +"(select (case when appntsex ='0'  then '男' when appntsex='1'  then '女' else '其它' end )  from lbappnt where contno=a.contno) 性别,"
              +" (select appntbirthday  from lbappnt where contno=a.contno)  出生日期,"
              +"(select companyphone from lcaddress c where customerno=a.appntno and c.addressno=(select addressno from lbappnt where contno=a.contno)) 单位电话,"
              +"(select HomePhone from lcaddress c where customerno=a.appntno and c.addressno=(select addressno from lbappnt where contno=a.contno)) 住宅电话,"
              +"(select mobile from lcaddress c where customerno=a.appntno and c.addressno=(select addressno from lbappnt where contno=a.contno)) 移动电话,"
              +"(select (case when phone is null then (case when homephone is null then companyphone else homephone end) else phone end) from lcaddress c where customerno=a.appntno and c.addressno=(select addressno from lbappnt where contno=a.contno)) 联系电话,"
              +"(select AccName from lbcont where contno=a.contno) 银行,"
              +"(select BankAccNo from lbcont where contno=a.contno) 帐号,"
              +"(select PostalAddress from lcaddress c where customerno=a.appntno and c.addressno=(select addressno from lbappnt where contno=a.contno)) 联系地址,"
              +"(select ZipCode from lcaddress c where customerno=a.appntno and c.addressno=(select addressno from lbappnt where contno=a.contno)) 邮编, "
              +"a.insuredno 被保人客户号码,"
              +"a.insuredname 被保险人姓名,"
              +"(case when a.insuredsex ='0'  then '男' when a.insuredsex='1'  then '女' else '其它' end )   性别, "
              +"a.insuredbirthday 出生日期,"
              +"a.riskcode 险种代码,"
              +"(select riskname from lmrisk where riskcode = a.riskcode) 险种名称,"
              +"a.Amnt 保额,"
              +"a.Mult 档次,"
              +"a.prem 期缴保费,"
              +"a.PayIntv 交费频次,"
              +"a.PayEndYear 交费年期,"
              +"(select name from laagent where agentcode=a.agentcode) 业务员姓名,"
              +"(select (case when sex='0' then '男' when sex='1' then '女' else '其它' end) from laagent where agentcode=a.agentcode) 性别,"
              +"a.agentcode 业务员代码,"
              +"(select Name from labranchgroup where AgentGroup=a.AgentGroup) 部组,"
              +"a.AgentGroup 销售机构代码,"
              +"(select phone from laagent where agentcode=a.agentcode) 业务员电话,"
              +"(select mobile from laagent where agentcode=a.agentcode) 业务员手机,"
              +"nvl(supplementaryprem,0) 追加保费,"
              +"(select customgetpoldate from lbcont where contno=a.contno) 保单回执客户签收日期,"
              +"(select getpoldate from lbcont where contno=a.contno) 回执回销录入日期,"
              +"(select min(edorappdate) from lpedorapp where edoracceptno=b.edoracceptno) 犹豫期退保申请日期,"
              +"(select ConfDate from lpedorapp where edoracceptno=b.edoracceptno) 退保给付确认日期 "
              +" from lbpol a ,lpedoritem b "
              +" where a.contno=b.contno and a.conttype='1' and exists (select 1 from lpedorapp where edoracceptno=b.edorno and edorstate='0') "
              +" and b.edortype='WT'"          
              +wherePar;
    mSql= strSQL;         
    turnPage1.queryModal(strSQL, WTGrid);
    if( WTGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}
	//showCodeName();
              
}
function easyPrint(){
    if(WTGrid.mulLineCount==0)
    {
        alert("打印列表没有数据");
		return false;
    }
    fm.all('sql').value=mSql;
	fm.action="./WTPrint.jsp";
	fm.submit();
}







