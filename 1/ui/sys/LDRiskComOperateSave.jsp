<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDRiskComOperateSave.jsp
//程序功能：
//创建日期：2003-10-28 15:15:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
   System.out.println("RiskCode:ManageCom");
  LDRiskComOperateSchema tLDRiskComOperateSchema   = new LDRiskComOperateSchema();
  OLDRiskComOperateUI tOLDRiskComOperateUI   = new OLDRiskComOperateUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
 
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    tLDRiskComOperateSchema.setRiskCode(request.getParameter("RiskCode"));
    tLDRiskComOperateSchema.setManageCom(request.getParameter("ManageCom"));
    tLDRiskComOperateSchema.setComCode(request.getParameter("ComCode"));
    tLDRiskComOperateSchema.setOperateType(request.getParameter("OperateType"));
    tLDRiskComOperateSchema.setRemark(request.getParameter("Remark"));
    System.out.println("transact"+transact);
    System.out.println("transact:"+transact+"ManageCom"+request.getParameter("ManageCom"));
  try
  {
    // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLDRiskComOperateSchema);
    tOLDRiskComOperateUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDRiskComOperateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<html>
<script language="javascript">
	alert("<%=Content%>");
	top.close();
</script>
</html>
