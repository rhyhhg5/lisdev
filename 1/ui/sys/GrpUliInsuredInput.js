 //               该文件中包含客户端需要处理的函数和事件
 // 王珑制作
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 
var turnPage4 = new turnPageClass(); 
var turnPage5 = new turnPageClass(); 

var arrAllDateSet;
var cPerCustomerNo = "";
var showInfo;


/*********************************************************************
 *  查询按钮实现
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{	
  //查询被保人信息
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //查询被保人信息汇总，如按被保人状态或保障计划查询时显示
  queryGrpInsuredSum();
  
  initGrpInseGrid();
  
  //查询有效被保人
  var sqlVali = "select a.InsuredNo, a.Name, "
    + "   (select min(name) from LCInsured where ContNo = a.ContNo and RelationToMainInsured = '00'), "
    + "   CodeName('relation',a.RelationToMainInsured), CodeName('sex', a.Sex), a.Birthday, "
    + "   CodeName('idtype',a.IDType), a.IDNo, CodeName('occupationtype',a.OccupationType), "
    + "   a.ContPlanCode, (select sum(Prem) from LCPol where ContNo = a.ContNo and InsuredNo = a.InsuredNo), "
    + "   '', (select min(cvalidate) from LCPol where ContNo = a.ContNo and InsuredNo = a.InsuredNo), CInvaliDate, "
    + "   (select nvl(sum(money), 0) from LCInsureAccTrace where polno in (select polno from lcpol where ContNo = a.ContNo and InsuredNo = a.InsuredNo) and payplancode in (select payplancode from lmdutypay where accpayclass='4')), "
    + "   (select nvl(sum(money), 0) from LCInsureAccTrace where polno in (select polno from lcpol where ContNo = a.ContNo and InsuredNo = a.InsuredNo) and payplancode in (select payplancode from lmdutypay where accpayclass in ('5','6'))), "
    + "   (select nvl(sum(money), 0) from LCInsureAccTrace where polno in (select polno from lcpol where ContNo = a.ContNo and InsuredNo = a.InsuredNo)), "
    + "   a.BankAccNo, "
    + " b.ContNo "
    + "from LCInsured a, LCCont b "
    + "where a.ContNo = b.ContNo "
    + "   and (b.StateFlag is null or b.StateFlag in('1')) "
    + "   and a.GrpContNo = '" + contNo + "' "
    + getWherePart( 'a.InsuredNo','InsuredNo')
    + getWherePart( 'a.IDNo','IDNo')
    + getWherePart( 'a.Name','Name', 'like')
    + getWherePart( 'a.ContPlanCode','ContPlanCode');
	    
	//查询C表的无效被保人
	var sqlInvaliC = "select a.InsuredNo, a.Name, "
    + "   (select InsuredName from LCInsured where ContNo = a.ContNo and RelationToMainInsured = '00'), "
    + "   CodeName('relation',a.RelationToMainInsured), CodeName('sex', a.Sex), a.Birthday, "
    + "   CodeName('idtype',a.IDType), a.IDNo, CodeName('occupationtype',a.OccupationType), "
    + "   a.ContPlanCode, (select sum(Prem) from LCPol where ContNo = a.ContNo and InsuredNo = a.InsuredNo), '', b.CValiDate, CInvaliDate, "
    + "   (select nvl(sum(InsuAccBala), 0) from LCInsureAcc where ContNo = a.ContNo and InsuredNo = a.InsuredNo), "
    + "   a.BankAccNo, "
    + " b.ContNo "
    + "from LCInsured a, LCCont b "
    + "where a.ContNo = b.ContNo "
    + "   and b.StateFlag in('2', '3') "
    + "   and a.GrpContNo = '" + contNo + "' "
    + getWherePart( 'a.InsuredNo','InsuredNo')
    + getWherePart( 'a.IDNo','IDNo')
    + getWherePart( 'a.Name','Name', 'like')
    + getWherePart( 'a.ContPlanCode','ContPlanCode');
  
  //查询B表的无效被保人
  var sqlInvaliB = "select a.InsuredNo, a.Name, "
    + "   (select Name from LCInsured where ContNo = a.ContNo and RelationToMainInsured = '00' union select Name from LBInsured where ContNo = a.ContNo and RelationToMainInsured = '00'), "
    + "   CodeName('relation',a.RelationToMainInsured), CodeName('sex', a.Sex), a.Birthday, "
    + "   CodeName('idtype',a.IDType), a.IDNo, CodeName('occupationtype',a.OccupationType), "
    + "   a.ContPlanCode, sum(b.Prem), '', min(b.CValiDate), "
    + "   (select (case when max(lpdiskimport.EdorValiDate) is null then  max(a.EdorValiDate) else max(lpdiskimport.EdorValiDate)   end) from LPGrpEdorItem a, lpdiskimport "
    + " where lpdiskimport.EdorNo = a.EdorNo "
    + " and a.EdorType in ('CT', 'XT', 'WT', 'ZT') "
    + " and a.EdorNo = lpdiskimport.EdorNo "
    + " and lpdiskimport.insuredno = b.insuredno ) ,"
    + "   (select nvl(sum(InsuAccBala), 0) from LBInsureAcc where ContNo = a.ContNo and InsuredNo = a.InsuredNo), "
    + "   a.BankAccNo, "
    + " a.ContNo "
    + "from LBInsured a, LBPol b "
    + "where a.ContNo = b.ContNo "
    + "   and a.GrpContNo = '" + contNo + "' "
    + getWherePart( 'a.InsuredNo','InsuredNo')
    + getWherePart( 'a.IDNo','IDNo')
    + getWherePart( 'a.Name','Name', 'like')
    + getWherePart( 'a.ContPlanCode','ContPlanCode')
    + "group by a.InsuredNo, a.Name, a.RelationToMainInsured, a.Sex, a.Birthday, a.IDType, "
    + "	  a.IDNo, a.OccupationType, a.ContNo, a.ContPlanCode , a.EdorNo, a.BankAccNo,b.insuredno ";
    
    
  //总sql
  var sql = "";
  
  //有效
  if(fm.InsuredStat.value == "0")
  {
    sql = sqlVali
        + "order by InsuredNo "
        + "with ur ";;
  }
  //无效
  else if(fm.InsuredStat.value == "1")
  {
    sql = sqlInvaliC + " union " + sqlInvaliB + " "
        + "order by InsuredNo "
        + "with ur ";
  }
  else
  {
    sql = sqlVali + " union " + sqlInvaliC + " union " + sqlInvaliB 
        + "order by InsuredNo "
        + "with ur ";
  }
  
  fm.InsuredSql.value = sql;
  turnPage4.pageDivName = "divPage4";
  turnPage4.queryModal(sql, GrpInseGrid);
  showInfo.close();
  fm.all("divPage4").style.display = "";
  
  setSumActuPayMoney();
  
//   var strTableName = "";
//	 var strTableName1 = "";
//	 var strTableName2 = "";
//	 var strTableName3 = "";
//	 var strTabType = "";//1 从 C 表取数据;2: 从 B 表取数据
//	 var strAccCons="";  //帐户余额
//	 
//	 //tContType=='1' 有效保单 ；tContType=='2' 其他保单 
//	 if (tContType=='1')
//	 {
//	 	    strTabType = "1";
//			  strTableName = "LCInsured";
//		    strTableName1 = "LCPol";	
//		    strTableName2 = "LCInsureAcc";	
//		    // 判断是否是特需医疗帐户
//		   var accSQL = " select count(*) from " + strTableName2 + " where  GrpContNo='" + contNo+ "'  AND InsuAccNo='100000'" ;
//			 var arrReturn = new Array();
//			 arrReturn = easyExecSql(accSQL);
//			 if (!(arrReturn == null || arrReturn ==0))
//			 {
//			 	  strAccCons = ",getPerAccDif(a.InsuredNo,a.GrpContNo,'" + strTabType + "')";
//			 }  		  
//		    var selCSql = "select a.InsuredNo,a.Name,(select insuredname from LCCont where ContNo = a.ContNo),getCodeName('relation',a.RelationToMainInsured),getCodeName('sex',a.Sex),a.Birthday,getCodeName('idtype',a.IDType),a.IDNo,getCodeName('occupationtype',a.OccupationType),a.ContPlanCode,(select nvl((select sum(e.sumactupaymoney) from ljapayperson e ,lcpol f where f.contno = b.contno and f.insuredno = a.insuredno and e.polno = f.polno and e.curpaytodate = f.paytodate),2) from dual),b.CValiDate,''" + strAccCons;
//	      var selBSql = "select a.InsuredNo,a.Name,(select insuredname from LCCont where ContNo = a.ContNo),getCodeName('relation',a.RelationToMainInsured),getCodeName('sex',a.Sex),a.Birthday,getCodeName('idtype',a.IDType),a.IDNo,getCodeName('occupationtype',a.OccupationType),a.ContPlanCode,(select nvl((select sum(e.sumactupaymoney) from ljapayperson e ,lcpol f where f.contno = b.contno and f.insuredno = a.insuredno and e.polno = f.polno and e.curpaytodate = f.paytodate),2) from dual),b.CValiDate,char(e.ModifyDate)" + strAccCons;
//	  
//	 }else
//	 {
//	 	    strTabType = "2";
//		    strTableName = "LBInsured";
//	   strTableName1 = "LBPol";
//	   strTableName2 = "LBInsureAcc";	 			    
//	   // 判断是否是特需医疗帐户
//			   var accSQL = " select count(*) from " + strTableName2 + " where  GrpContNo='" + contNo+ "'  AND InsuAccNo='100000'" ;
//				 var arrReturn = new Array();
//				 arrReturn = easyExecSql(accSQL);
//				 if (!(arrReturn == null || arrReturn ==0))
//				 {
//				 	  strAccCons = ",getPerAccDif(a.InsuredNo,a.GrpContNo,'" + strTabType + "')";
//				 } 
//	      var selCSql = "select a.InsuredNo,a.Name,(select insuredname from LBCont where ContNo = a.ContNo),getCodeName('relation',a.RelationToMainInsured),getCodeName('sex',a.Sex),a.Birthday,getCodeName('idtype',a.IDType),a.IDNo,getCodeName('occupationtype',a.OccupationType),a.ContPlanCode,sum(b.Prem),b.CValiDate,''" + strAccCons;
//	      var selBSql = "select a.InsuredNo,a.Name,(select insuredname from LBCont where ContNo = a.ContNo),getCodeName('relation',a.RelationToMainInsured),getCodeName('sex',a.Sex),a.Birthday,getCodeName('idtype',a.IDType),a.IDNo,getCodeName('occupationtype',a.OccupationType),a.ContPlanCode,sum(b.Prem),b.CValiDate,char(e.ModifyDate)" + strAccCons;
//	  
//	 }
//	 
//	 
//	  
//	  
//	  // modeFlag=0 : 按合同号查被保人 否则 /按保障号查被保人信息
//	 if (modeFlag == "0")
//	 { 
//	     var tempSql3 = "" ;
//	 }else
//	 {
//		  var tempSql3 = " and a.ContPlanCode='" + planCode + "'" ;
//	 }
//	  var tempSql1 =  " from " + strTableName+ " a," +strTableName1 + " b where 1=1 "	
//   + getWherePart( 'a.InsuredNo','InsuredNo' ) 
//   + getWherePart( 'a.Name','Name','like' ) 
//   + getWherePart( 'a.IDNo','IDNo' )
//   + " and a.GrpContNo='" + contNo + "'"
//   + tempSql3
//   + " and b.InsuredNo=a.InsuredNo"
//   + " and b.GrpContNo=a.GrpContNo"  
//   + " and b.Appflag = '1'"
//   + " and b.polTypeFlag != '1' "   
//   ;
//	 
//	 var tempSql2 = " group by a.Name,a.RelationToMainInsured,a.Sex,a.Birthday,a.IDType,a.IDNo,a.OccupationType,a.ContPlanCode,a.InsuredNo,b.CValiDate,a.GrpContNo,a.ContNo,b.ContNo"
//	              + " order by  a.Name" ;
//	 
//	 
//	 var tempStr4 = " and a.EdorNo not in (select EdorNo from LPGrpEdorItem where EdorNo =a.EdorNo and GrpContNo=a.GrpContNo and EdorType='ZT')";
//	 var tempStr5 = " and a.EdorNo in (select EdorNo from LPGrpEdorItem where EdorNo =a.EdorNo and GrpContNo=a.GrpContNo and EdorType='ZT')";
//	 
//	 //客户状态 2:全部；0：有效；1：终止
//	 var strState = fm.all('InsuredStat').value;
//	 var strSQL ="";
//	  //客户状态
//   
//		///// 有效
//	  if (strState == '0')
//		{		
//			if (tContType == '1')
//		   {  		    
//		      strSQL = selCSql + tempSql1 + tempSql2;	
//		   }else
//		   {
//		   	  strSQL = selCSql + tempSql1 + tempStr4 + tempSql2;	
//		   }
//			  
//		}else if (strState == '1')
//		{
//			   
//			   //保全减人
//	    strSQL =  selBSql + " from  LBInsured  a,LBPol  b,LPGrpEdorItem e where 1=1 "	
//			   + getWherePart( 'a.InsuredNo','InsuredNo' ) 
//			   + getWherePart( 'a.Name','Name','like' ) 
//			   + getWherePart( 'a.IDNo','IDNo' )
//			   + " and a.GrpContNo='" + contNo + "'"
//			   + tempSql3
//			   + " and b.InsuredNo=a.InsuredNo"
//			   + " and b.GrpContNo=a.GrpContNo"			   
//			   + " and b.Appflag = '1'" 
//			   + " and b.polTypeFlag != '1' "
//			   + " and e.EdorNo =a.EdorNo and e.GrpContNo=a.GrpContNo and e.EdorType='ZT'"			   
//			   + " group by a.Name,a.RelationToMainInsured,a.Sex,a.Birthday,a.IDType,a.IDNo,a.OccupationType,a.ContPlanCode,a.InsuredNo,b.CValiDate,e.ModifyDate,a.GrpContNo,a.ContNo,b.ContNo"
//      + " order by  a.Name" 
//			   ;			  
//			   		  
//			
//		}else
//		{
//			//全部
//		
//			 if (tContType == '1')
//		   {
//		   	strSQL = "(select a.InsuredNo,a.Name,(select insuredname from LCCont where ContNo = a.ContNo),getCodeName('relation',a.RelationToMainInsured),getCodeName('sex',a.Sex),a.Birthday,getCodeName('idtype',a.IDType),a.IDNo,getCodeName('occupationtype',a.OccupationType),a.ContPlanCode,sum(b.Prem),b.CValiDate,''" + strAccCons
//		    	   +  " from " + strTableName+ " a," +strTableName1 + " b where 1=1 "	
//					   + getWherePart( 'a.InsuredNo','InsuredNo' ) 
//					   + getWherePart( 'a.Name','Name','like' ) 
//					   + getWherePart( 'a.IDNo','IDNo' )
//					   + " and a.GrpContNo='" + contNo + "'"
//					   + tempSql3
//					   + " and b.InsuredNo=a.InsuredNo"
//					   + " and b.GrpContNo=a.GrpContNo"					   
//					   + " and b.Appflag = '1'" 					   
//					   + " group by a.Name,a.RelationToMainInsured,a.Sex,a.Birthday,a.IDType,a.IDNo,a.OccupationType,a.ContPlanCode,a.InsuredNo,b.CValiDate,a.GrpContNo,a.ContNo"
//					   + " union "
//					   + selBSql +  " from  LBInsured  a,LBPol  b,LPGrpEdorItem e where 1=1 "	
//					   + getWherePart( 'a.InsuredNo','InsuredNo' ) 
//					   + getWherePart( 'a.Name','Name','like' ) 
//					   + getWherePart( 'a.IDNo','IDNo' )
//					   + " and a.GrpContNo='" + contNo + "'"
//					   + tempSql3
//					   + " and b.InsuredNo=a.InsuredNo"
//					   + " and b.GrpContNo=a.GrpContNo"		
//					   + " and b.polTypeFlag != '1' "			 
//					   + " and b.Appflag = '1'" 
//					   + " and e.EdorNo =a.EdorNo and e.GrpContNo=a.GrpContNo and EdorType='ZT'"					    
//					   + " group by a.Name,a.RelationToMainInsured,a.Sex,a.Birthday,a.IDType,a.IDNo,a.OccupationType,a.ContPlanCode,a.InsuredNo,b.CValiDate,e.ModifyDate,a.GrpContNo,a.ContNo,b.ContNo)"
//		 				 ;
//		 				
//		   	  
//		   }else
//		   {
//		        strSQL = selCSql + tempSql1 + tempSql2;
//		 	 }
//		}			    	
//  strSQL = strSQL + " with ur" ;
//  
//  turnPage4.pageDivName = "divPage4";
//  turnPage4.queryModal(strSQL, GrpInseGrid);
//  showInfo.close();
	 
}

//计算每个被保人的当期交费
function setSumActuPayMoney()
{
  for(var i = 0; i < GrpInseGrid.mulLineCount; i++)
  {
    var insuredNo = GrpInseGrid.getRowColDataByName(i, "InsuredNo");
    var ContNo = GrpInseGrid.getRowColDataByName(i, "ContNo");
    var sql =   "select sum(a) from ( "
              + "   select sum(SumActuPayMoney) a "
              + "   from LJAPayPerson a, LCPol b "
              + "   where a.PolNo = b.PolNo  "
              + "     and a.CurPayToDate = b.PayToDate "
              + "     and b.GrpContNo = '" + contNo + "' "
              + "     and b.InsuredNo = '" + insuredNo + "' "
              + "     and b.ContNo = '" + ContNo + "' "
              + "   union "
              + "   select sum(SumActuPayMoney) a "
              + "   from LJAPayPerson a, LBPol b "
              + "   where a.PolNo = b.PolNo  "
              + "     and a.CurPayToDate = b.PayToDate "
              + "     and b.GrpContNo = '" + contNo + "' "
              + "     and b.InsuredNo = '" + insuredNo + "' "
              + "     and b.ContNo = '" + ContNo + "' "
              + ") t ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      GrpInseGrid.setRowColDataByName(i, "SumActuPayMoney", rs[0][0]);
    }
  }
}
  

//查询被保人信息汇总，如按被保人状态或保障计划查询时显示
function queryGrpInsuredSum()
{
  if(fm.Name.value != "" || fm.InsuredNo.value != "" || fm.IDNo.value != "")
  {
    return true;
  }
  
  //查询有效被保人
  var sqlVali = "select count(1) a, count(1) b, 0 c, min(b.CValiDate) d, max(b.CInValiDate) e, "
      + "   min(b.PayToDate) f, nvl(sum(b.Prem), 0) g, nvl(sum(b.Prem), 0) h "
      + "from LCInsured a, LCCont b "
      + "where a.ContNo = b.ContNo "
      + "   and a.GrpContNo = '" + contNo + "' "
      + "   and (b.StateFlag is null or b.StateFlag in('1')) "
	    + getWherePart( 'a.ContPlanCode','ContPlanCode');
  
  //查询C表的无效被保人
  var sqlInvaliC = "select count(1) a, 0 b, count(1) c, min(b.CValiDate) d, max(b.CInValiDate) e, "
      + "   min(b.PayToDate) f, nvl(sum(b.Prem), 0) g, 0 h "
      + "from LCInsured a, LCCont b "
      + "where a.ContNo = b.ContNo "
      + "   and a.GrpContNo = '" + contNo + "' "
      + "   and b.StateFlag in('2', '3') "
	    + getWherePart( 'a.ContPlanCode','ContPlanCode');
      
  //查询B表的无效被保人
  var sqlInvaliB = "select count(1) a, 0 b, count(1) c, min(b.CValiDate) d, max(b.CInValiDate) e, "
      + "   min(b.PayToDate) f, nvl(sum(b.Prem), 0) g, 0 h "
      + "from LBInsured a, LBCont b "
      + "where a.ContNo = b.ContNo "
      + "   and a.GrpContNo = '" + contNo + "' "
	    + getWherePart( 'a.ContPlanCode','ContPlanCode')
	    + "union "
	    + "select count(1) a, count(1) b, 0 c, min(b.CValiDate) d, max(b.CInValiDate) e, "
      + "   min(b.PayToDate) f, nvl(sum(b.Prem), 0) g, nvl(sum(b.Prem), 0) h "
      + "from LBInsured a, LCCont b "
      + "where a.ContNo = b.ContNo "
      + "   and a.GrpContNo = '" + contNo + "' "
	    + getWherePart( 'a.ContPlanCode','ContPlanCode');
	    
  //总sql
  var sql = "";
  
  //有效
  if(fm.InsuredStat.value == "0")
  {
    sql = sqlVali;
  }
  //无效
  else if(fm.InsuredStat.value == "1")
  {
    sql = "select sum(a), sum(b), sum(c), min(d), max(e), min(f), sum(g), sum(h) "
        + "from ( "
        +         sqlInvaliC + " union " + sqlInvaliB 
        + ") t with ur ";
  }
  else
  {
    sql = "select sum(a), sum(b), sum(c), min(d), max(e), min(f), sum(g), sum(h) "
        + "from ( "
        +         sqlVali + " union " + sqlInvaliC + " union " + sqlInvaliB
        + ") t with ur ";
  }
  
  turnPage5.pageDivName = "divPage5";
  turnPage5.queryModal(sql, GrpInsuredSumGrid);
}

/*********************************************************************
 *  被保人信息列表中选择一条，查看被保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function fromListReturn(){

	 if (trim(fm.all('InsuredStat').value) == "1")
	 { 
	    tContType = "2" ;
	 }	
	      
	//从客户投保信息列表中选择一条，查看投保信息
	var tSel = GrpInseGrid.getSelNo();	
    var cInsuredNo = GrpInseGrid.getRowColData( tSel - 1, 1 );
    window.open("../sys/ContInsuredQuery.jsp?InsuredNo=" + cInsuredNo + "&ContNo=" + contNo+ "&LoadFlag=1"+ "&ContType=" + tContType+"&ModeFlag=" + modeFlag);
}

//查询保单无名单当前信息
function queryNoNameCont()
{
  var sql = "  select distinct a.contPlanCode, a.contPlanName, a.peoples2, "
            + "   (select sum(prem) "
            + "   from LCPol "
            + "   where grpContNo = a.grpcontNo "
            + "       and contPlanCode = a.contPlanCode "
            + "       and polTypeFlag = '1' ), "
            + "   (select sum(amnt) "
            + "   from LCPol "
            + "   where grpContNo = a.grpContNo "
            + "       and contPlanCode = a.contPlanCode "
            + "       and polTypeFlag = '1' ), b.contNo, b.insuredNo "
            + "from LCContPlan a , LCInsured b, LCCont c "
            + "where a.grpContNo = b.grpContNo "
            + "   and a.contPlanCode = b.contPlanCode "
            + "   and b.contNo = c.contNo "
            + "   and c.polType = '1' "  //无名单
            + "   and a.grpContNo = '" + contNo + "' "
            + "   and a.contPlanCode != '11' ";
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, NoNameCont);
}

//查询所选无名单的增减人操作轨迹
//默认显示所有
function queryOperateTrace()
{
  var contPlanCode = "";
  var contNoSelect = "";
  
  var row = NoNameCont.getSelNo() - 1;
  if(row >= 0)
  {
    contPlanCode = NoNameCont.getRowColDataByName(row, "contPlanCode");
    contNoSelect = NoNameCont.getRowColDataByName(row, "contNo");
  }
  
  var showOneNNC = "";
  if(contPlanCode != "")
  {
    showOneNNC = "   and a.contNo = '" + contNoSelect + "' "
               + "   and a.contPlanCode = '" + contPlanCode + "' ";
  }
  
  var sql = "  select a.edorNo, (select edorName from LMEdorItem where edorCode = b.edorType), "
            + "   a.contPlanCode, "
            + "   (select contPlanName from LCContPlan "
            + "   where grpContNo = a.grpContNo and contPlanCode = a.contPlanCode), "
            + "   a.noNamePeoples, a.edorPrem, a.edorValiDate "
            + "from LCInsuredList a, LPGrpEdorItem b "
            + "where a.edorNo = b.edorNo "
            + "   and a.grpContNo = b.grpContNo "
            + "   and a.grpContNo = '" + contNo + "' "
            + showOneNNC
            + "   and b.edorType in('WZ', 'WJ') "
            + "   and exists"
            + "     (select 1 from LPEdorApp where edorAcceptNo=b.edorNo and edorState = '0') "
            + "order by edorNo desc ";
  turnPage3.pageDivName = "divPage3";
	turnPage3.queryModal(sql, NNCOperateTrace);
}

function setContPlanCode()
{
  //查询保障计划
  var sql = "select a.ContPlanCode, b.ContPlanName "
            + "from LCInsured a, LCContPlan b "
            + "where a.GrpContNo = b.GrpContNo "
            + "   and a.ContPlanCode = b.ContPlanCode "
            + "   and a.GrpContNo = '" + contNo + "' "
            + "union "
            + "select a.ContPlanCode, b.ContPlanName "
            + "from LBInsured a, LBContPlan b "
            + "where a.GrpContNo = b.GrpContNo "
            + "   and a.ContPlanCode = b.ContPlanCode "
            + "   and a.GrpContNo = '" + contNo + "' "
            + "order by ContPlanCode ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    var codes = "0|";
    for(var i = 0; i < rs.length; i++)
    {
      codes = codes + "^" + rs[i][0] + "|" + rs[i][1];
    }
    
    fm.ContPlanCode.CodeData = codes;
  }
}

//打印被保人清单
function printList()
{
  if(GrpInseGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }
  
  fm.action = "PrintUliInsuredListSave.jsp";
	fm.target = "_blank";
	fm.submit();
}

//返回上一页面
function returnParent()
{
  top.opener.focus();
  top.close();
}