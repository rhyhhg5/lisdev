//该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}
//提交，保存按钮对应操作
function DoInsert()
{
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
// 查询按钮
function easyQueryClick()
{
	//alert ("start easyQueryClick");
	// 初始化表格
	initSetGrid();
	if( verifyInput() == false ) return false;
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.ManageCom"
		+",(select name from ldcom where comcode=a.managecom)"
		+",a.contno,a.prtno"
		+",(select distinct codename from ldcode where (codetype='salechnl' or codetype='lcsalechnl') and code=a.salechnl)"
		+",getUniteCode(a.agentcode),(select name from laagent where agentcode=a.agentcode)"
		+",(select branchattr from labranchgroup where agentgroup=a.agentgroup)"
		+",(select name from labranchgroup where agentgroup=a.agentgroup)"
		+",a.appntname,a.polapplydate,a.inputdate,a.cvalidate,a.cinvalidate"
		+",a.signdate"
		+",(select codename from ldcode where code=a.paymode and codetype='paymode')"
		+",(select codename from ldcode where code=char(a.payintv) and codetype='payintv')"
		+",a.prem,(select count(distinct insuredno) from lcinsured where contno=a.contno)"
		+",(select codename from ldcode where code=a.appflag and codetype='appflag')"
	+ " from lccont a "
	+ " where  a.grpcontno='00000000000000000000' and a.managecom like '"+fm.all('ManageCom').value+"%'"
	+" and not exists (select 'X' from lcrnewstatelog where newcontno = a.contno)"
	+" and a.makedate between '"+fm.all('StartDate').value+"'"
	+" and '"+fm.all('EndDate').value+"' "
	+getWherePart('getUniteCode(a.agentcode)','AgentCode')
	+ " Order by a.managecom ";
    turnPage.queryModal(strSQL, SetGrid);
    if(SetGrid.mulLineCount==0){
    	alert("没有查询到任何满足条件的数据！");
    	return false;
 	}
    
}
function DoNewDownload()
{
	if( verifyInput() == false ) return false;
 // 书写SQL语句
	var stSQL = "";
	strSQL = "select a.ManageCom"
		+",(select name from ldcom where comcode=a.managecom)"
		+",a.contno,a.prtno"
		+",(select distinct codename from ldcode where (codetype='salechnl' or codetype='lcsalechnl') and code=a.salechnl)"
		+",getUniteCode(a.agentcode),(select name from laagent where agentcode=a.agentcode)"
		+",(select branchattr from labranchgroup where agentgroup=a.agentgroup)"
		+",(select name from labranchgroup where agentgroup=a.agentgroup)"
		+",a.appntname,a.polapplydate,a.inputdate,a.cvalidate,a.cinvalidate"
		+",a.signdate"
		+",(select codename from ldcode where code=a.paymode and codetype='paymode')"
		+",(select codename from ldcode where code=char(a.payintv) and codetype='payintv')"
		+",a.prem,(select count(distinct insuredno) from lcinsured where contno=a.contno)"
		+",(select codename from ldcode where code=a.appflag and codetype='appflag')"
	+ " from lccont a "
	+ " where  a.grpcontno='00000000000000000000' and a.managecom like '"+fm.all('ManageCom').value+"%'"
	+" and not exists (select 'X' from lcrnewstatelog where newcontno = a.contno)"
	+" and a.makedate between '"+fm.all('StartDate').value+"'"
	+" and '"+fm.all('EndDate').value+"' "
	+getWherePart('getUniteCode(a.agentcode)','AgentCode')
	+ " Order by a.managecom ";
    fm.querySql.value = strSQL;
    fm.action = "PerCDQListReport.jsp";
    fm.submit();
}					
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}
	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
		{
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
		}
	//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}
		//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
		{
			if (confirm("您确实想修改该记录吗?"))
			{
				fm.fmAction.value = "UPDATE";
				if(!beforeSubmit()) return false;
				var i = 0;
				var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.submit(); //提交
			}
			else
				{
					fm.OperateType.value = "";
					alert("您取消了修改操作！");
				}
			}
							//判断是否选择了要增加、修改或删除的行
function chkMulLine()
		{
										return true;
									}
									//提交前的校验、计算
function beforeSubmit()
									{
									  if(!SetGrid.checkValue("SetGrid")) return false;
										if(!chkMulLine()) return false;
										return true;
									}

function showDiv(cDiv,cShow)
									{
										if (cShow=="true")
										{
											cDiv.style.display="";
										}
										else
											{
												cDiv.style.display="none";
											}
									}
function diskImport()
{ 
    //alert("111111111111111");
		//alert("====="+fm.all("diskimporttype").value);
   var strUrl = "../agentconfig/DiskImportRateComminsionMain.jsp?diskimporttype="+fm.all("diskimporttype").value+"&branchtype="+fm.all("BranchType").value+"&branchtype2="+fm.all("branchtype2").value;
   showInfo=window.open(strUrl,"佣金率信息导入","width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=yes");
}
function moduleDownload(){
	var tSQL="";
	fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "RateCommisionModule.jsp";
    fm.submit();
    fm.action = oldAction;
}