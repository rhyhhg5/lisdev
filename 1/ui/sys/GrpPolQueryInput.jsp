<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = "";
	String tCustomerNo="";
	try
	{
		tContNo = request.getParameter( "ContNo" );
		
		//默认情况下为集体保单
		if( tContNo == null || tContNo.equals( "" ))
			tContNo = "00000000000000000000";

		tCustomerNo = request.getParameter( "CustomerNo" );
		if( tCustomerNo == null )
	       tCustomerNo="";
	}
	
	
	catch( Exception e1 )
	{
		tContNo = "00000000000000000000";
			System.out.println("---contno:"+tContNo);

	}
	System.out.println("---contno:"+tContNo);
        String tDisplay = "";
	try
	{
		tDisplay = request.getParameter("display");
		if(tDisplay == null || tDisplay.equals( "" ))
		 { tDisplay = "0";}
	}
	catch( Exception e )
	{
		tDisplay = "";
	}
%>

<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   
<script>
	var contNo = "<%=tContNo%>";  //个人单的查询条件.
	var comCode = "<%=tG.ComCode%>";
	var tDisplay = "<%=tDisplay%>";
	var customerNo = "<%=tCustomerNo%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpPolQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="GrpPolQueryInit.jsp"%>
  <title>保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
    	  <td class= common align= left style="display:none">请输入查询条件：</td>
		  </tr>
    	<tr>
			  <td class= titleImg align= center>请按客户查询：</td>
			  <td><INPUT VALUE="查  询" class = cssbutton TYPE=button onclick="easyQueryClick();"></td> 
		  </tr>
	  </table>
    <table  class= common align=center>
    	<TR  class= common>
	      <TD  class= title> 客户号码 </TD>  
	      <TD  class= input> <Input class="common1" name=CustomerNo > </TD>
	      <TD  class= title> 客户名称 </TD>
        <TD  class= input> <Input class="common1" name=Name> </TD>          
        <TD  class= title> 性别 </TD>
        <TD  class= input><Input class=codeNo name=Sex readonly ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);showAllCodeName();"><input class=codename name=SexName readonly style="width:80" ></TD>
        <TD  class= title> 出生日期 </TD>
        <TD  class= input> <Input class="coolDatePicker" name=Birthday style="width:110" dateFormat="short"></TD>
      </TR>
      <TR  class= common>
		    <TD  class= title> 证件号码 </TD>
        <TD  class= input> <Input class="common1" name=IDNo> </TD>
		    <TD  class= title> 地址  </TD>
        <TD  class= input> <Input class="common1" name=HomeAddress> </TD>
        <TD  class= title>邮编 </TD>
        <TD  class= input> <Input class="common1" name=HomeZipCode> </TD>
		    <TD  class= title> 客户性质 </TD>
        <TD  class= input><Input class=codeNo name=CustomerKind readonly ondblclick="return showCodeList('syscustomerkind',[this,CustomerKindName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('syscustomerkind',[this,CustomerKindName],[0,1]);"><input class=codename name=CustomerKindName style="width:80" readonly></TD>
      </TR>
    </table>
  	<table>
      <tr>
        <td class=common>
  			   <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCClent);">
      	</td>
      	<td class= titleImg>
      			 个单客户信息
      	</td>
      </tr>
     </table>
  	<Div  id= "divLCClent" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPersonGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		    <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage1.firstPage();showCodeName();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage1.previousPage();showCodeName();"> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage1.nextPage();showCodeName();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage1.lastPage();showCodeName();"> 		
    </div>
  	<table>
      <tr>
        <td class=common>
  			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpClent);">
      	</td>
      	<td class= titleImg>
      			 团单客户信息
      	</td>
      </tr>
    </table>
  	<Div  id= "divLCGrpClent" style= "display: ''" align = center>
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanGrpPersonGrid" >
					</span> 
			  	</td>
  			</tr>
    	</table>
				<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage2.firstPage();showCodeName();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage2.previousPage();showCodeName();"> 		
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage2.nextPage();showCodeName();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage2.lastPage();showCodeName();"> 		
    </div>   
    
    <br>
    
    <!--保单查询-->
    <table class= common border=0 width=100%>
    	<tr>
			  <td class= titleImg align= center>按保单查询：</td>
			  <td><INPUT VALUE="查  询" class = cssbutton TYPE=button onclick="queryCont();"></td> 
		  </tr>
	  </table>
    <table  class= common align=center>
    	<TR  class= common>
	      <TD  class= title> 保单号 </TD>  
	      <TD  class= input> <Input class="common1" name=ContNo > </TD>
	      <TD  class= title> 投保单印刷号 </TD>
        <TD  class= input> <Input class="common1" name=PrtNo> </TD>          
        <TD  class= title> 保单状态 </TD>
        <TD  class= input><Input class=codeNo name=StateFlag readonly ondblclick="return showCodeList('stateflag',[this,StateFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('stateflag',[this,StateFlagName],[0,1]);"><input class=codename name=StateFlagName readonly style="width:80" ></TD>
        <TD  class= title> 保单性质 </TD>
        <TD  class= input><Input class=codeNo name=ContKind readonly ondblclick="return showCodeList('syscontkind',[this,ContKindName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('syscontkind',[this,ContKindName],[0,1]);"><input class=codename name=ContKindName readonly style="width:80" ></TD>
      </TR>
      <TR  class= common>
		    <TD  class= title> 管理机构 </TD>
        <TD  class= input>
          <Input class= "codeno" name=ManageCom value="<%=tG.ComCode%>" verify="管理机构|notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename style="width:80" name=ManageComName>
        </TD>
		    <TD  class= title> 营销部  </TD>
        <TD  class= input><Input class= "codeno"  name=Group03  ondblclick="return showCodeList('agentgroupbq',[this,Group03Name],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,Group03Name],[0,1],null,'03','BranchLevel',1);" ><Input class=codename style="width:80" name=Group03Name></TD>
        <TD  class= title>代理人编码 </TD>
        <TD  class= input>
        	<Input class= "codeno"  name=AgentCode  ondblclick="return showCodeList('agentcode',[this,AgentName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('agentcode',[this,AgentName],[0,1],null,null,null,1);" ><Input class=codename style="width:80" name=AgentName>
		</td> 
		<TD  class= title>市场类型 </TD>
        <TD  class= input>
          <Input class=codeno name=MarketType verify="市场类型|code:MarketType" ondblclick="return showCodeList('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);"><input class=codename style="width:80" name=MarketTypeName readonly=true elementtype=nacessary> 		     		
        </td>  
		  </TR>
		  <TR  class= common>
		    <TD  class= title> 包含险种1 </TD>
        <TD  class= input><input class="codeNo" name="RiskCode1" ondblclick="return showCodeList('riskcode', [this,RiskCode1Name], [0,1]);" onkeyup="return showCodeListKey('riskgrp', [this,RiskCode1Name],[0,1]);"><input class="codename" name="RiskCode1Name" style="width:80" readonly></TD>
		    <TD  class= title> 包含险种2  </TD>
        <TD  class= input colspan=5><input class="codeNo" name="RiskCode2" ondblclick="return showCodeList('riskcode', [this,RiskCode2Name], [0,1]);" onkeyup="return showCodeListKey('riskgrp', [this,RiskCode2Name],[0,1]);"><input class="codename" name="RiskCode2Name" style="width:80" readonly></TD>
       </TR>
       	 </TD>
     <td class="title8">渠道</td>
     <td class="input8">
       <input class="codeNo" name="SaleChnlCode" ondblclick="return showCodeList('salechnlall',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('salechnlall',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" />
     </td>
     <td class="title8">网点</td>
     <td class="input8">
       <input id="AgentComBank" class="code" name="AgentComBank" style="display:none;" ondblclick="return queryAgentComBank();" />
     </td>
     <td class="title8">网点名称</td>
     <td class="input8">
       <input readonly="readonly"  class="readonly" name="AgentComName"  />
     </td>
       <TR  class= common>
	      <TD  class= title> 投保日期范围 </TD>  
	      <TD  class= input> <Input class="coolDatePicker" name=ApplyDateStart style="width:110" elementtype="nacessary"  dateFormat="short" verify="开始时间|notnull"></TD>
	      <TD  class= title> 至 </TD>
        <TD  class= input> <Input class="coolDatePicker" name=ApplyDateEnd style="width:110" elementtype="nacessary" dateFormat="short" verify="开始时间|notnull"> </TD>          
        <TD  class= title> 交至日期范围 </TD>  
	      <TD  class= input> <Input class="coolDatePicker" name=PayToDateStart style="width:110" elementtype="nacessary" dateFormat="short" verify="开始时间|notnull"></TD>
	      <TD  class= title> 至 </TD>
        <TD  class= input> <Input class="coolDatePicker" name=PayToDateEnd style="width:110" elementtype="nacessary" dateFormat="short" verify="开始时间|notnull"> </TD>          
      </TR>
    </table>
    
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 有效保单信息
    		</td>
    		<td class= titleImg><font color="#ff0000">
            若保单为&lsquo;约定缴费&lsquo;的保单，则&lsquo;期交保费&lsquo;不具参考性
   			 </font> 
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  					<span id="spanGrpPolGrid" >
  					</span> 
  			  </td>
  			</tr>
    	</table>
  	  <Div id= "divPage3" align="center" style= "display: '' ">
        <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage3.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage3.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage3.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage3.lastPage();">
        <INPUT VALUE="打印全部清单" onclick="printContList('Vali');" id="PrintContListInValiID" class = cssbutton TYPE=button>
      </Div>
  	</div>
  	<table>
    	<tr>
        	<td class=common>
			    <font color="#ff0000"><img src="../common/images/butExpand.gif" style="" onclick="showPage(this,divLBCont);"></font>
    		</td>
    		<td class= titleImg>
    			 失效保单信息 
    		</td>
    		<td class= titleImg><font color="#ff0000">
    			 若保单为&lsquo;约定缴费&lsquo;的保单，则&lsquo;期交保费&lsquo;不具参考性
    			 </font> 
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLBCont" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
  	  <Div id= "divPage4" align="center" style= "display: '' ">
        <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage4.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage4.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage4.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage4.lastPage();"> 					
        <INPUT VALUE="打印全部清单" onclick="printContList('InVali');" id="PrintContListInValiID" class = cssbutton TYPE=button>
    	</Div>
  	</div>
	  <br>
	 <table class= common border=0 width=100%>
     <tr>
      <td><INPUT VALUE="保单明细" class = cssbutton TYPE=button onclick="contDetail();"> </td> 
			<td><INPUT VALUE="被保人清单" class = cssbutton TYPE=button onclick="insuredQueryClick();"> </td> 
			<td><INPUT VALUE="保单缴费明细" class = cssbutton TYPE=button onclick="payQueryClick();"> </td> 
			<td><INPUT VALUE="投保书查询" class = cssbutton TYPE=button onclick="ScanQuery();"> </td>
			<td><INPUT VALUE="客服信息" class = cssbutton TYPE=button onclick="edorInfo();"> </td> 
			<td style="display:none"><INPUT VALUE="理赔明细" class = cssbutton TYPE=button onclick="claimInfo();"> </td> 
			<td><INPUT VALUE="扫描件查询" class = cssbutton TYPE=button onclick="allScanInfo();"> </td> 
			<td><INPUT VALUE="汇交件投保人清单" class = cssbutton TYPE=button onclick="HJAppnt();"> </td> 
		</tr>
	</table>
	<input name= "ContSql" type=hidden>
	<input name= "InvaliContSql" type=hidden>
	<input name= "ContNoSelected" type=hidden>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
