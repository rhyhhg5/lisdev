<html>
<%
//程序名称:PEfficiencyInput.jsp
//程序功能：打印效率清单程序
//创建日期：2003-04-03
//创建人  ：刘岩松
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>


<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="PEfficiencyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  </head>
<body>
  <form action="./PEfficiencySave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <Div  id= "divRegister1" style= "display: ''">
		  <table class= common>
      	<TR>
		  		<td>
        		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRegister1);">
      		</td>
       		<td class= titleImg>
        		请录入起止日期
      		</td>
    		</tr>
    		
    		<TR>
          <TD  class= title >
            开始日期
          </TD>
          <TD  class= input>
          	<input class="coolDatePicker" dateFormat="short" name="StartDate" >
          </TD>
          <TD  class= title >
            结束日期
          </TD>
          <TD  class= input>
          	<input class="coolDatePicker" dateFormat="short" name="EndDate" >
          </TD>
        </TR>
        <TR>
        	<TD>
        		<input type=hidden name="OperateFlag">
        	</TD>
        </TR>
      </table>
    </Div>
      <table class=common>
				<tr>
  				<td>
  					<input class=common type=button value="打印复核效率清单" onclick="printCheck()">
					</td>
				</tr>
			</table>
    </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>