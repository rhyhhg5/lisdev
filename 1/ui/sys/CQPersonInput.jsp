<%
//程序名称：LDPersonInput.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="CQPersonInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CQPersonInit.jsp"%>
</head>
<body onload="initForm();easyQueryClick();">
  <form  method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏LDPerson1的信息 -->
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
	      </td>
	      <td class= titleImg>
	        个人客户信息
	      </td>
	  </tr>
    </table>

    <Div  id= "divLDPerson1" style= "display: ''">
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonBasic);">
	      </td>
	      <td class= titleImg>
	        客户基本信息
	      </td>
	  </tr>
    </table>

    <Div  id= "divLDPersonBasic" style= "display: ''">    
      <table  class= common>
        <TR  class= common>           
    <TD  class= title>
      客户号码
    </TD>
    <TD  class= input>
      <Input class="readonly" name=CustomerNo readonly >
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Name readonly >
    </TD>
    <TD  class= title>
      客户性别
    </TD>
    <TD  class= input>
      <Input class="readonly" name=Sex  readonly >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      客户出生日期
    </TD>
    <TD  class= input>
      <Input class="readonly" readonly name=Birthday >
    </TD>
    <TD  class= title>
      证件类型
    </TD>
    <TD  class= input>
      <Input class="readonly" name=IDType readonly >
    </TD>
    <TD  class= title>
      证件号码
    </TD>
    <TD  class= input>
      <Input class= readonly name=IDNo readonly >
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      密码
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Password readonly >
    </TD>
    <TD  class= title>
      国籍
    </TD>
    <TD  class= input>
      <Input class="readonly" name=NativePlace readonly >
    </TD>
    <TD  class= title>
      民族
    </TD>
    <TD  class= input>
      <Input class="readonly" name=Nationality  readonly >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      户口所在地
    </TD>
    <TD  class= input>
      <Input class="readonly" name=RgtAddress  readonly >
    </TD>
    <TD  class= title>
      婚姻状况
    </TD>
    <TD  class= input>
      <Input class="readonly" name=Marriage readonly >
    </TD>
    <TD  class= title>
      结婚日期
    </TD>
    <TD  class= input>
      <Input class="readonly" name=MarriageDate readonly >
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      健康状况
    </TD>
    <TD  class= input>
       <Input class="readonly" name=Health readonly >                                                                          
    </TD>
    <TD  class= title>
      身高
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Stature readonly >
    </TD>
     <TD  class= title>
      体重
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Avoirdupois readonly >
    </TD>
  </TR>
  <TR  class= common>
   
    <TD  class= title>
      学历
    </TD>
    <TD  class= input>
      <Input class="readonly" name=Degree readonly >
    </TD>
    <TD  class= title>
      信用等级
    </TD>
    <TD  class= input>
            <Input class="readonly" name=CreditGrade  readonly >                        
    </TD>
    <TD  class= title>
      其它证件类型
    </TD>
    <TD  class= input>
            <Input class="readonly" name=OthIDType readonly >                                                                                                                    
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      其它证件号码
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=OthIDNo readonly >
    </TD>
    <TD  class= title>
      ic卡号
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=ICNo readonly >
    </TD>
    <TD  class= title>
      单位编码
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=GrpNo readonly >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      入司日期
    </TD>
    <TD  class= input>
      <Input class="readonly" name=JoinCompanyDate readonly >
    </TD>
    <TD  class= title>
      参加工作日期
    </TD>
    <TD  class= input>
      <Input class="readonly" name=StartWorkDate readonly >
    </TD>
    <TD  class= title>
      职位
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Position readonly >
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      工资
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Salary readonly >
    </TD>
    <TD  class= title>
      职业类别
    </TD>
    <TD  class= input>
       <Input class="readonly" name=OccupationType readonly >
    </TD>
     <TD  class= title>
      职业代码
    </TD>
    <TD  class= input>
	<Input class="readonly" name=OccupationCode readonly >
    </TD>
  </TR>
  <TR  class= common>
   
    <TD  class= title>
      职业（工种）
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=WorkType readonly >
    </TD>
    <TD  class= title>
      兼职（工种）
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=PluralityType readonly >
    </TD>
    <TD  class= title>
      死亡日期
    </TD>
    <TD  class= input>
      <Input class="readonly"  name=DeathDate readonly >
    </TD>
  </TR>
 
  <TR  class= common>
    <TD  class= title>
      是否吸烟标志
    </TD>
    <TD  class= input>
            <Input class="readonly" name=SmokeFlag readonly >
    </TD>
    <TD  class= title>
      黑名单标记
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=BlacklistFlag readonly >
    </TD>
    <TD  class= title>
      属性
    </TD>
    <TD  class= input>
      <Input class="readonly" name=Proterty readonly >                                                                                                        
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      备注
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Remark readonly >
    </TD>
    <TD  class= title>
      状态
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=State readonly >
    </TD>
    <TD  class= title>
      VIP值
    </TD>
    <TD  class= input>
            <Input class='readonly' name=VIPValue readonly>
    </TD>
  </TR>
  <TR  class= common>
    
  </TR>                     
      </Table>
    </Div>
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonAddress);">
	      </td>
	      <td class= titleImg>
	        客户联系方式
	      </td>
	  </tr>
    </table>

    <Div  id= "divLDPersonAddress" style= "display: ''">    
      <table  class= common>
       <TR  class= common>               
          <TD  class= title>
            地址编码<br><font color=red size=1>(选此查看地址信息)</font>
          </TD>
          <TD  class= input>
            <Input class= code name=AddressNo ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this],[0],'', '', '', true);" onkeyup="return showCodeListKeyEx('GetAddressNo',[this],[0],'', '', '', true);" onfocus="getdetailaddress();">
          </TD>
        </TR>
<TR  class= common>
    <TD  class= title>
      通讯地址
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=PostalAddress readonly >
    </TD>
    <TD  class= title>
      通讯邮编
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=ZipCode readonly >
    </TD>
    <TD  class= title>
      通讯电话
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Phone readonly >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      通讯传真
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Fax readonly >
    </TD>
     <TD  class= title>
      家庭地址
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=HomeAddress readonly >
    </TD>
    <TD  class= title>
      家庭邮编
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=HomeZipCode readonly >
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      家庭电话
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=HomePhone readonly >
    </TD>
    <TD  class= title>
      家庭传真
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=HomeFax readonly >
    </TD>
    <TD  class= title>
      单位地址
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=CompanyAddress readonly >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      单位邮编
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=CompanyZipCode readonly >
    </TD>
     <TD  class= title>
      单位电话
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=CompanyPhone readonly >
    </TD>
    <TD  class= title>
      单位传真
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=CompanyFax readonly >
    </TD>
  </TR>
 
  <TR  class= common>
    <TD  class= title>
      手机
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Mobile readonly >
    </TD>
    <TD  class= title>
      手机中文标示
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=MobileChs readonly >
    </TD>
    <TD  class= title>
      e_mail
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=EMail readonly >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      传呼
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=BP  readonly >
    </TD>
    <TD  class= title>
      手机2
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Mobile2 readonly >
    </TD>
    <TD  class= title>
      手机中文标示2
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=MobileChs2 readonly >
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      e_mail2
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=EMail2 readonly >
    </TD>
    <TD  class= title>
      传呼2
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=BP2 readonly >
    </TD>
  </TR>
     </table>
    </Div>   
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonAccount);">
	      </td>
	      <td class= titleImg>
	        客户账户信息
	      </td>
	  </tr>
    </table>

    <Div  id= "divLDPersonAccount" style= "display: ''">    
      <table  class= common>  
  <TR  class= common>      
    <TD  class= title>
      是否默认账户
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=AccKind readonly >
    </TD>
    <TD  class= title>
      银行编码
    </TD>    
    <TD  class= input>
      <Input  name=BankCode CLASS="readonly" MAXLENGTH=20 readonly >
    </TD>
    <TD  class= title>
      银行帐号
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=BankAccNo readonly >
    </TD>
  </TR>     
  <TR  class= common>      
    
    <TD  class= title>
      银行帐户名
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=AccName readonly >
    </TD>
  </TR>       
        </TR>  
          <TD>
          <input type=hidden name=Transact >
          </TD>        
      </table>
    </Div>      
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
