<%
//程序名称：AgentQueryInput.jsp
//程序功能：
//创建日期：2003-04-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   

<script>
  var comCode = <%=tG.ComCode%>
</script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./AgentQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./AgentQueryInit.jsp"%>
  <title>代理人查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./AgentQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            </td>
            <td class= titleImg>
                代理人查询条件
            </td>            
    	</tr>
    </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <TD class= title>   代理人编码  </TD>
        <TD  class= input>  <Input class=common  name=AgentCode > </TD>
        <TD class= title>   销售机构  </TD>
        <TD class= input> <Input class=common name=AgentGroup >  </TD>
        <TD class= title>  管理机构 </TD>
        <TD class= input> <Input name=ManageCom class='codeno' id="ManageCom" ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" ><input class=codename name=ManageComName></TD>
      </TR>
      <TR  class= common> 
        <TD  class= title> 姓名 </TD>
        <TD  class= input>   <Input name=Name class= common >  </TD>
        <TD  class= title> 性别  </TD>
        <TD  class= input> <Input name=Sex class="codeno" MAXLENGTH=1 ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" ><input class=codename name=SexName></TD>
        <TD  class= title>  推荐报名编号   </TD>
        <TD  class= input>  <Input name=EntryNo class= common >  </TD>
      </TR>
      <TR  class= common> 
       <TD  class= title>  出生日期  </TD>
        <TD  class= input> <Input name=Birthday class="coolDatePicker" dateFormat="short" >  </TD>
        <TD  class= title>  身份证号码  </TD>
        <TD  class= input>   <Input name=IDNo class= common >  </TD>
        <TD  class= title>  邮政编码  </TD>
        <TD  class= input>   <Input name=ZipCode class= common >  </TD>
      </TR>
      <!--TR  class= common> 
        <TD  class= title>  民族</TD>
        <TD  class= input>  <Input name=Nationality class="code" id="Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);" >  </TD>
        <TD  class= title>  籍贯 </TD>
        <TD  class= input> <Input name=NativePlace class="code" MAXLENGTH=2 id="NativePlaceBak" ondblclick="return showCodeList('NativePlaceBak',[this]);" onkeyup="return showCodeListKey('NativePlaceBak',[this]);">  </TD>
        <TD  class= title> 来源地  </TD>
        <TD  class= input>  <Input name=Source class= common >  </TD>        
      </TR-->
      <!--tr class=common>      
        <TD  class= title>  血型 </TD>
        <TD  class= input>  <Input name=BloodType class="code" MAXLENGTH=1 id="BloodType" ondblclick="return showCodeList('BloodType',[this]);" onkeyup="return showCodeListKey('BloodType',[this]);">  </TD>        
        <TD  class= title> 婚姻状况  </TD>
        <TD  class= input> <Input name=Marriage class="code" MAXLENGTH=1 ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);" > </TD>        
        <TD  class= title>  结婚日期 </TD>
        <TD  class= input>    <Input name=MarriageDate class="coolDatePicker" dateFormat="short" >  </TD>        
      </tr-->
      <!--TR  class= common> 
        <TD  class= title>
          政治面貌 
        </TD>
        <TD  class= input> 
          <Input name=PolityVisage class= common > 
        </TD> 
        <TD  class= title>
          户口所在地
        </TD>
        <TD  class= input> 
          <Input name=RgtAddress class= common > 
        </TD>
        <TD  class= title>
          学历 
        </TD>
        <TD  class= input> 
          <Input name=Degree class=common > 
        </TD>
      </TR>
      <TR  class= common>
       <TD  class= title> 
          毕业院校
        </TD>
        <TD  class= input> 
          <Input name=GraduateSchool class= common > 
        </TD>
        <TD  class= title>
          专业 
        </TD>
        <TD  class= input> 
          <Input name=Speciality class= common > 
        </TD>
        <TD  class= title> 
          外语水平 
        </TD>
        <TD  class= input> 
          <Input name=ForeignLevel class=common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          职称 
        </TD>
        <TD  class= input> 
          <Input name=PostTitle class=common > 
        </TD>
        <TD  class= title>
          家庭地址编码
        </TD>
        <TD  class= input>
          <Input name=HomeAddressCode class= common >
        </TD>
        <TD  class= title>
          家庭地址 
        </TD>
        <TD  class= input> 
          <Input name=HomeAddress class= common > 
        </TD>
      </TR-->
      <!--TR  class= common> 
        <TD  class= title>  通讯地址  </TD>
        <TD  class= input>  <Input name=PostalAddress class= common >   </TD>
        <TD  class= title>  邮政编码  </TD>
        <TD  class= input>   <Input name=ZipCode class= common >  </TD>
        <TD  class= title> 电话  </TD>
        <TD  class= input>  <Input name=Phone class= common >  </TD>
      </TR-->
      <TR  class= common> 
        <TD  class= title>   传呼  </TD>
        <TD  class= input> <Input name=BP class= common >  </TD>
        <TD  class= title>  手机  </TD>
        <TD  class= input>  <Input name=Mobile class= common >  </TD>
        <TD  class= title> e_mail  </TD>
        <TD  class= input>  <Input name=EMail class= common >  </TD>
      </TR>
      <!--TR  class= common>
        <TD  class= title>
          是否吸烟标志 
        </TD>
        <TD  class= input> 
          <Input name=SmokeFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);"> 
        </TD>
        <TD  class= title>
          银行编码
        </TD>
        <TD  class= input> 
          <Input name=BankCode class=common > 
        </TD>
        <TD  class= title>
          银行账号 
        </TD>
        <TD  class= input> 
          <Input name=BankAccNo class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          从业年限 
        </TD>
        <TD  class= input> 
          <Input name=WorkAge class= common  > 
        </TD>
        <TD  class= title>
          原工作单位 
        </TD>
        <TD  class= input> 
          <Input name=OldCom class= common > 
        </TD>
        <TD  class= title> 
          原职业 
        </TD>
        <TD  class= input> 
          <Input name=OldOccupation class= common > 
        </TD>
      </TR-->
      <TR  class= common> 
        <TD  class= title>  工作职务   </TD>
        <TD  class= input>   <Input name=HeadShip class= common  >   </TD>
        <TD  class= title>  推荐代理人  </TD>
        <TD  class= input>  <Input name=RecommendAgent class= common  >   </TD>
        <TD  class= title>  工种/行业 </TD>
        <TD  class= input> <Input name=Business class=common >  </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>  信用等级  </TD>
        <TD  class= input>  <Input name=CreditGrade class=common >   </TD>
        <TD  class= title>  销售资格  </TD>
        <TD  class= input>  <Input name=SaleQuaf class='codeno' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this,SaleQuafName],[0,1]);" onkeyup="return showCodeListKey('yesno',[this,SaleQuafName],[0,1]);" ><input class=codename name=SaleQuafName></TD>
        <TD  class= title> 代理人资格证号码   </TD>
        <TD  class= input>   <Input name=QuafNo class= common >   </TD>
      </TR>
      <!--TR  class= common> 
        <TD  class= title> 
          证书开始日期 
        </TD>
        <TD  class= input> 
          <Input name=QuafStartDate class="coolDatePicker" dateFormat="short" > 
        </TD>
        <TD  class= title>
          证书结束日期 
        </TD>
        <TD  class= input> 
          <Input name=QuafEndDate class="coolDatePicker" dateFormat="short" > 
        </TD>
        <TD  class= title>
          展业证号码1 
        </TD>
        <TD  class= input> 
          <Input name=DevNo1 class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title> 
          展业证号码2
        </TD>
        <TD  class= input> 
          <Input name=DevNo2 class= common > 
        </TD>
        <TD  class= title>
          聘用合同号码 
        </TD>
        <TD  class= input> 
          <Input name=RetainContNo class= common > 
        </TD>
        <TD  class= title>
          代理人类别 
        </TD>
        <TD  class= input> 
          <Input name=AgentKind class='code' ondblclick="return showCodeList('AgentKind',[this]);" onkeyup="return showCodeListKey('AgentKind',[this]);"  > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          业务拓展级别
        </TD>
        <TD  class= input> 
          <Input name=DevGrade class=common > 
        </TD>
        <TD  class= title>
          内勤标志 
        </TD>
        <TD  class= input> 
          <Input name=InsideFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
        <TD  class= title>
          是否专职标志 
        </TD>
        <TD  class= input> 
          <Input name=FullTimeFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          是否有待业证标志 
        </TD>
        <TD  class= input> 
          <Input name=NoWorkFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
        <TD  class= title>
          培训日期 
        </TD>
        <TD  class= input> 
          <Input name=TrainDate class='coolDatePicker' dateFormat='short' > 
        </TD>
        <TD  class= title>
          录用日期 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          转正日期 
        </TD>
        <TD  class= input> 
          <Input name=InDueFormDate class='coolDatePicker' dateFormat='short' > 
        </TD>
        <TD  class= title>
          代理人状态 
        </TD>
        <TD  class= input> 
          <Input name=AgentState class='code'  maxlength=2 ondblclick="return showCodeList('AgentState',[this]);" onkeyup="return showCodeListKey('AgentState',[this]);" > 
        </TD>
        <TD  class= title>
          离司日期 
        </TD>
        <TD  class= input> 
          <Input name=OutWorkDate class='coolDatePicker' dateFormat='short' > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          保证金 
        </TD>
        <TD  class= input> 
          <Input name=AssuMoney class= common > 
        </TD>
        <TD  class= title>
          复核员
        </TD>
        <TD  class= input> 
          <Input name=Approver class= common > 
        </TD>
        <TD  class= title>
          复核日期
        </TD>
        <TD  class= input> 
          <Input name=ApproveDate class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          标志位
        </TD>
        <TD  class= input> 
          <Input name=QualiPassFlag class= common MAXLENGTH=1 > 
        </TD>
        <TD  class= title>
          备注
        </TD>
        <TD class=input> 
          <Input name=Remark class= common > 
        </TD>
        <TD  class= title>
          操作员代码 
        </TD>
        <TD  class= input> 
          <Input name=Operator class= common > 
        </TD>
      </TR-->
    </table>
          
          <!--INPUT class=common VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT class=common VALUE="返回" TYPE=button onclick="returnParent();"--> 	
	   <table> 
		    <tr>		
			    <td>
			      <INPUT class=cssbutton VALUE="查  询" TYPE=button onclick="easyQueryClick();"> 
			    </td>
			    <td>  
			      <INPUT class=cssbutton VALUE="代理人信息" TYPE=button onclick="returnParent();">  
			    </td>
		        <td>
			      <INPUT class=cssbutton VALUE="投保单信息" TYPE=button onclick="ProposalClick();"> 
			    </td>
			    <td> 
			      <INPUT class=cssbutton VALUE="保单信息" TYPE=button onclick="PolClick();"> 
			    </td>
			    <td>  
			      <INPUT class=cssbutton VALUE="销户保单信息" TYPE=button onclick="DesPolClick();"> 
			    </td>
		    </tr> 
	   </table> 
    </Div>      
          				
    <table>
    	<tr>
        <td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 代理人结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
<center>    	
    	<table>
    		<tr>
    			<td>
			      <INPUT class=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			    </td>
			    <td>  
			      <INPUT class=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    </td>
			    <td> 			      
			      <INPUT class=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    </td>
			    <td> 			      
			      <INPUT class=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 						
			    </td>  			
  			</tr>
  		</table>
<center>  		
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
