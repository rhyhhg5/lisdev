//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var arrAllDataSet;

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
} 
/*********************************************************************
 *  查询保单基本信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function returnParent()
 {
 	               
	var arrReturn1 = new Array();
if(tContType=="1")
{
	var ContType=easyExecSql("select ContType from lccont where contno='"+tContNo+"' and appflag='1'");
}else if(tContType=="2")
{
	var ContType=easyExecSql("select ContType from lbcont where contno='"+tContNo+"'and appflag='1'");
}
		try{
			var strSQL="";
			if(tContType=="1" && ContType=="1"){
				var strSQL = "select a.prtno,case a.ContType when '1' then '个人' when '2' then '团体' end,a.SumPrem,a.PayIntv,a.PayMode,a.PolApplyDate,"
						+"a.SignDate,a.CValiDate,a.PaytoDate,(select Name from laagent where AgentCode=a.AgentCode),a.AgentCode,a.AppntName,b.Name,"
						+"b.RelationToMainInsured,case a.appflag when '1' then '已签单' when '0' then '未签单' end from lccont a,LCInsured b where a.ContNo= '"+tContNo+"'and b.contNo=a.contNo and SequenceNo='1' and a.appflag='1'" 
			}else if(tContType=="2"&& ContType=="1"){
       var strSQL = "select a.prtno,case a.ContType when '1' then '个人' when '2' then '团体' end,a.SumPrem,a.PayIntv,a.PayMode,a.PolApplyDate," 
				 + "a.SignDate,a.CValiDate,a.PaytoDate,(select Name from laagent where AgentCode=a.AgentCode),a.AgentCode,a.AppntName,b.Name,"
				 + " b.RelationToMainInsured,case when d.edortype='CT' then '终止合同' when d.edortype='XT' then '协议退保' when d.edortype='WT' then '犹豫期退保' end from lbcont a,LbInsured b,LPEdoritem d where a.ContNo= '"+tContNo+"'and b.contNo=a.contNo and SequenceNo='1' and a.edorno = d.edorno"
			}
			if(strSQL != "")
			{
				arrReturn1 =easyExecSql(strSQL);	
			}		
			if (arrReturn1 == ""||arrReturn1 == null) {
                return false;
            } else {
			   display(arrReturn1[0]);
			}
		}
	catch(ex)
  	{
    	alert("在BaseInfoQueryInit.jsp-->returnParent函数中发生异常:初始化界面错误!");
 		 }  
	}
  
/*********************************************************************
 *  设置保单基本信息
 *  参数  ：  cArr 数组
 *  返回值：  无
 *********************************************************************
 */
  function display(cArr)
  {    
  	try { fm.all('prtno').value = cArr[0]; } catch(ex) { };
  	try { fm.all('ContType').value = cArr[1]; } catch(ex) { };
  	try { fm.all('SumPrem').value = cArr[2]; } catch(ex) { };
  	try { fm.all('PayIntv').value = cArr[3]; } catch(ex) { };
  	fm.all('PayIntvName').value 
  	= 
  	easyExecSql("select codename from ldcode where  codetype='payintv' and code='"+cArr[3]+"'");
		try { fm.all('PayMode').value = cArr[4]; } catch(ex) { };fm.all('PayModeName').value = easyExecSql("select codename from ldcode where  codetype='paymode' and code='"+cArr[4]+"'");
  	try { fm.all('PolApplyDate').value = cArr[5]; } catch(ex) { };
		try { fm.all('SignDate').value = cArr[6]; } catch(ex) { };
		try { fm.all('CValiDate').value = cArr[7]; } catch(ex) { };
		try { fm.all('PaytoDate').value = cArr[8]; } catch(ex) { };
		try { fm.all('AgentName').value = cArr[9]; } catch(ex) { };		
		try { fm.all('AgentCode').value = cArr[10]; } catch(ex) { };
		try { fm.all('AppntName').value = cArr[11]; } catch(ex) { };
		try { fm.all('InsuredName').value = cArr[12]; } catch(ex) { };
		try { fm.all('RelationToMainInsured').value = cArr[13]; } catch(ex) { };fm.all('RelationToMainInsuredName').value = easyExecSql("select  CodeName from ldcode where codetype = 'relation' and Code='"+cArr[13]+"'");
		try { fm.all('ContState').value = cArr[14]; } catch(ex) { };
	}

/*********************************************************************
 *  详细信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function DetailedInfoQueryClick()  
 {  
		window.open("../sys/DetailedInfoQuery.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);
 }

/*********************************************************************
 *  客户资料查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function CustomInfoQueryClick()
{
    window.open("../sys/PhoCustomerQuery.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);										
}

/*********************************************************************
 *  保单试算查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function NumerationQueryClick()
{
	var tCustomer = trim(fm.all('AppntNo').value); 
	if (tCustomer=="" || tCustomer==null)
	{}else
	{
		window.open("../sys/TaskPersonalBox.jsp?ContNo=" + tContNo + "&IsCancelPolFlag=" + tIsCancelPolFlag + "&Customer=" + tCustomer);
	}
}


//孟维涛修改 


