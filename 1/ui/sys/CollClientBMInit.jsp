<%
//程序名称：CollClientBMInit.jsp
//程序功能：
//创建日期：2002-08-19
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('GrpNo').value = '';
    fm.all('GrpName').value = '';
    fm.all('GrpAddressCode').value = '';
    fm.all('GrpAddress').value = '';
    fm.all('GrpZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('Fax').value = '';
    fm.all('Satrap').value = '';
    fm.all('Corporation').value = '';
    fm.all('EMail').value = '';
    fm.all('LinkMan').value = '';
    fm.all('Peoples').value = 0;       
    fm.all('BankCode').value = '';
    fm.all('BankAccNo').value = '';       
    fm.all('BusinessType').value = '';   
    fm.all('GrpNature').value = '';
    fm.all('FoundDate').value = '2001-1-2';
    fm.all('GrpGroupNo').value = '';
    fm.all('State').value = '';
    fm.all('Remark').value = '';
    fm.all('BlacklistFlag').value = '';
    fm.all('Operator').value = 'hzm';
    fm.all('Transact').value = '';

  }
  catch(ex)
  {
    alert("在CollClientBMInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在CollClientBMInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("CollClientBMInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>