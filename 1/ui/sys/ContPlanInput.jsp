<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
String GrpContNo = request.getParameter("GrpContNo");
String LoadFlag = request.getParameter("LoadFlag");
String ContType= request.getParameter("ContType");
System.out.println(ContType);
%>
<head>
<script>
GrpContNo="<%=GrpContNo%>";
LoadFlag="<%=LoadFlag%>";
ContType="<%=ContType%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="ContPlan.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ContPlanInit.jsp"%>
<title>团体保障计划定制 </title>
</head>
<body onload="initForm();">
	<form method=post name=fm target="fraSubmit" action="ContPlanSave.jsp">
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>合同信息</td>
			</tr>
		</table>
		<table  class= common align=center>
			<TR  class= common>
				<!--<TD  class= title>集体合同号</TD>
				<TD  class= input>-->
					<Input class=readonly readonly name=GrpContNo value="<%=GrpContNo%>" type=hidden>
					<Input type=hidden name=ProposalGrpContNo>
					<input type=hidden name=mOperate>
					<input type=hidden name=ContplanCode>
					<input type=hidden name=LoadFrame value="<%=request.getParameter("LoadFrame")%>">
				</TD>
				<TD  class= title>管理机构</TD>
				<TD  class= input>
					<Input class=readonly readonly name=ManageCom>
				</TD>
				<TD  class= title>投保人客户号</TD>
				<TD  class= input>
					<Input class= readonly readonly name=AppntNo>
				</TD>

				
				<TD  class= title>投保单位名称</TD>
				<TD  class= input>
					<Input class= readonly readonly name=GrpName>
				</TD>
			</TR>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>已存在保障计划</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanCodeGrid" ></span>
				</td>
			</tr>
		</table>
		
		<!--所选保障计划下险种-->
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>保障计划险种信息</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanRiskGrid" ></span>
				</td>
			</tr>
		</table>
		<Div id= "divPage3" align="center" style= "display: 'none' ">
	    <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage3.getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage3.getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage3.getNextPage();"> 
      <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage3.getLastPage();"> 			
    </Div>
	
	  <br>
		<table style="display:none">
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>保障计划定制</td>
			</tr>
		</table>
		<table  class= common align=center style="display:none">
			<TR  class= common>
				<TD  class= title>保障计划</TD>
				<TD  class= input>
					<Input name=ContPlanCodeName CLASS="code" type="hidden">
					<Input class="code" name=ContPlanCode  maxlength="2" onchange="ChangePlan();" CodeData="0|^1|A^2|B^3|C^4|D^5|E" ondblclick="showCodeListEx('ContPlanCode',[this,ContPlanCodeName],[1,0],null,null,null,1);" onkeyup="showCodeListEx('ContPlanCodeName',[this,ContPlanCode],[1,0],null,null,null,1);">
				</TD>
				<TD  class= title>人员类别</TD>
				<TD  class= input>
					<input class=common name=ContPlanName>
				</TD>
			</tr>
			<tr class=common>
				<TD  class= title>参保人数</TD>
				<TD  class= input>
					<Input class=common name=Peoples3>
				</TD> 		
				<TD  class= title>应保人数</TD>
				<TD  class= input>
					<Input class=common name=Peoples2>
				</TD> 			
					<Input type=hidden name=PlanSql>
			</TR>
			<TR>
				

				<TD id=divriskcodename class= title>险种代码</TD>
				<TD id=divriskcode  class= input>
					<Input class=codeNo name=RiskCode ondblclick="return showCodeList('GrpRisk',[this,RiskCodeName,RiskFlag],[0,1,3],null,fm.GrpContNo.value,'b.GrpContNo',1);" onkeyup="return showCodeListKey('GrpRiskB',[this,RiskCodeName,RiskFlag],[0,1,3],null,fm.GrpContNo.value,'b.GrpContNo');"><Input class=codename name="RiskCodeName" readonly=true>
					<input type=hidden name=RiskFlag>
				</TD>
				<TD id=divmainriskname style="display:none" class=title>主险编码</TD>
				<TD id=divmainrisk style="display:none" class=input>
					<Input class=codeno maxlength=6 name=MainRiskCode ondblclick="return showCodeList('GrpMainRisk',[this,MainRiskCodeName],[0,1],null,fm.GrpContNo.value,'b.GrpContNo',1);" onkeyup="return showCodeListKey('GrpMainRisk',[this,MainRiskCodeName],[0,1],null,fm.GrpContNo.value,'b.GrpContNo');"><Input class=codename name="MainRiskCodeName" readonly=true>
				</TD>
				<TD id=divcontplanname style="display:none" class=title>保险套餐</TD>
				<TD id=divcontplan style="display:none" class=input>
					<Input class=codeno maxlength=6 name=RiskPlan ondblclick="return showCodeList('RiskPlan',[this,RiskPlanName],[0,1],null,'','',1);" onkeyup="return showCodeListKey('RiskPlan',[this,RiskPlanName],[0,1],null,'','');"><Input class=codename name="RiskPlanName" readonly=true>
				</TD>
				<TD  class= title id="SumPremTitle">总保费</TD>
				<TD  class= input id="SumPremInput">
					<Input class=common name=SumPrem>
				</TD>
				<TD  class= title  id="AccPremTitle" style="display: 'none'" >个人账户保费</TD>
				<TD  class= input  id="AccPremInput" style="display: 'none'">
					<Input class=common name=AccPrem>
				</TD>
			</TR>
		</table>
		<Input VALUE="险种责任查询" class=cssButton TYPE=button onclick="QueryDutyClick();">
		
		<br>
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>险种责任信息</td>
			</tr>
		</table>
		
		<br>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanDutyGrid" ></span>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>保障计划详细信息</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanGrid" ></span>
				</td>
			</tr>
		</table>		
		<div id="divManageFee" style="display: 'none'">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanManageFeeGrid" ></span>
					</td>
				</tr>
			</table>
		</div>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanRiskPremGrid" ></span>
				</td>
			</tr>
		</table>
		<div id="divManageFee" style="display: 'none'">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanContPlanPremGrid" ></span>
					</td>
				</tr>
			</table>
		</table>
	</div>
	<Div  id= "divRiskPlanRela" style= "display: ''" align= left> 
		<INPUT VALUE="约定缴费计划" class="cssButton"   TYPE=button onclick="PlanCodeClick();">
		<INPUT VALUE="共用保额计划" class="cssButton" TYPE=button onclick="ShareAmntClick();">
  		<INPUT VALUE="返  回" class="cssButton"  TYPE=button onclick="returnparent();">
  		<!--INPUT VALUE="保障计划要约录入" class="cssButton"  TYPE=button onclick="nextstep();"-->
		</Div>
  	<input name= "test" type=hidden>
  </form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>