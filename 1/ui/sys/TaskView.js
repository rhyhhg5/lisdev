//程序名称：TaskPersonalBox.js
//程序功能：个单新契约扫描件保单录入
//创建日期：2005-1-17 11:03:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

/* 保存完成后的操作，由弹出窗口调用 */
var turnPage3 = new turnPageClass();
function afterSubmit2(FlagStr, content, tUrl)
{
	if ((tUrl != null) && (tUrl != ""))
	{
		location.replace(tUrl);
	}
}

	//打印批单
function PrtEdor()
{
	var EdorAcceptNo=fm.all('AcceptNo').value;
	window.open("../f1print/AppEndorsementF1PJ1.jsp?EdorAcceptNo="+EdorAcceptNo+"");
			
}
//查询作业历史
function queryScanGrid()
{
	var strSQL;
	strSQL = "select  a.PageType,a.ManageCom,a.Operator,a.MakeDate from ES_DOC_PAGES a,ES_DOC_MAIN  b where a.DocID=b.DocID"
			+ " order by a.PageType ";
			 
	turnPage3.queryModal(strSQL, ScanGrid);
	
	return true;
}