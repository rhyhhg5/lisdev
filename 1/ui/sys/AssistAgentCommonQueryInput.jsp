<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：GrpAgentQueryInput.jsp
	//程序功能：
	//创建日期：2010-10-29
	//创建人  ：gzh
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="./AssistAgentCommonQuery.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<%@include file="./AssistAgentCommonQueryInit.jsp"%>
		<script>
  			var ManageCom = "<%=tGI.ManageCom%>";
  			var tAssistSaleChnl = "<%=request.getParameter("AssistSaleChnl")%>";
  		</script>
		<title>协助销售业务员查询</title>
	</head>
	<body onload="initForm();">
		<form action="" method=post name=fm target="fraSubmit">
			<!--业务员查询条件 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLAGrpAgent);">
					</td>
					<td class=titleImg>
						业务员查询条件
					</td>
				</tr>
			</table>
			<Div id="divLAGrpAgent" style="display: ''">
				<table class=common>
					<TR class=common>
						<td class=title>
							销售渠道
						</td>
						<td class=input>
							<Input class=codeNo name=AssistSaleChnl readonly=true><input class=codename name=AssistSaleChnlName readonly=true>
						</td>
						<TD class=title>
							协助销售业务员代码
						</TD>
						<TD class=input>
							<Input class=common name=AssistAgentCode verify="协助销售业务员代码|int&len<=20">
						</TD>
						<TD class=title>
							协助销售业务员姓名
						</TD>
						<TD class=input>
							<Input class=common name=AssistAgentName>
						</TD>
					</TR>
				</table>
				<table>
					<tr>
						<td>
							<INPUT class=cssbutton VALUE="查  询" TYPE=button onclick="easyQueryClick();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="返  回" TYPE=button onclick="returnParent();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="取  消" TYPE=button onclick="closePage();">
						</td>
					</tr>
				</table>
			</Div>

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divAgentGrid);">
					</td>
					<td class=titleImg>
						业务员结果
					</td>
				</tr>
			</table>
			<Div id="divAgentGrid" style="display: ''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanAgentGrid" align=center> </span>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td>
							<INPUT class=cssbutton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
						</td>
						<td>
							<INPUT class=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</div>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
