<%
//程序名称：OLDComQuery.js
//程序功能：
//创建日期：2002-08-16 17:44:45
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  LDComSchema tLDComSchema   = new LDComSchema();

  OLDComUI tOLDComQueryUI   = new OLDComUI();
  //读取Session中的全局类
	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

    tLDComSchema.setComCode(request.getParameter("ComCode"));
    tLDComSchema.setOutComCode(request.getParameter("OutComCode"));
    tLDComSchema.setName(request.getParameter("Name"));
    tLDComSchema.setShortName(request.getParameter("ShortName"));
    tLDComSchema.setAddress(request.getParameter("Address"));
    tLDComSchema.setZipCode(request.getParameter("ZipCode"));
    tLDComSchema.setPhone(request.getParameter("Phone"));
    tLDComSchema.setFax(request.getParameter("Fax"));
    tLDComSchema.setEMail(request.getParameter("EMail"));
    tLDComSchema.setWebAddress(request.getParameter("WebAddress"));
    tLDComSchema.setSatrapName(request.getParameter("SatrapName"));
    tLDComSchema.setSign(request.getParameter("Sign"));



  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLDComSchema);
	tVData.add(tG);

  FlagStr="";
  // 数据传输
	if (!tOLDComQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " +  tError.getFirstError();
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tOLDComQueryUI.getResult();
		
		// 显示
		LDComSet mLDComSet = new LDComSet();
		mLDComSet.set((LDComSet)tVData.getObjectByObjectName("LDComSet",0));
		int n = mLDComSet.size();
		LDComSchema mLDComSchema;
		for (int i = 1; i <= n; i++)
		{
		  	mLDComSchema = mLDComSet.get(i);
		   	%>
		   	<script language="javascript">
        parent.fraInterface.ComGrid.addOne("ComGrid")
parent.fraInterface.fm.ComGrid1[<%=i-1%>].value="<%=mLDComSchema.getComCode()%>";
parent.fraInterface.fm.ComGrid2[<%=i-1%>].value="<%=mLDComSchema.getName()%>";
parent.fraInterface.fm.ComGrid3[<%=i-1%>].value="<%=mLDComSchema.getAddress()%>";

			</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (!FlagStr.equals("Fail"))
  {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

