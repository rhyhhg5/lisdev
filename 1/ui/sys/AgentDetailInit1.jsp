<%
//程序名称：AgentDetailInit.jsp
//程序功能：
//创建日期：2003-4-8 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            
<script> 
	var tAgentCode = "<%=tAgentCode%>"; 	
</script>
<script language="JavaScript">
function initInpBox()
{ 
  try
  {     
  	fm.all('DepartDate').value='<%=tDepartDate%>';
  	fm.all('DepartRsn').value='<%=tDepartRsn%>';                                
        if (top.opener != null)
        {
          fm.all('BranchType').value = top.opener.fm.BranchType.value;
        } 
  }
  catch(ex)
  {
    alert("在AgentDetailInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AgentDetailInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
 // alert('aaa');
    initInpBox();
    initSelBox();
    initWarrantorGrid();         
    afterQuery(tAgentCode);   
  }
  catch(re)
  {
    alert("AgentDetailInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  
// 担保人信息的初始化
function initWarrantorGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;
      
      iArray[1]=new Array();
      iArray[1][0]=" 姓名";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="60px"; 	           		//列宽
      iArray[1][2]=1;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="性别";          		        //列名
      iArray[2][1]="30px";      	      		//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[3]=new Array();
      iArray[3][0]="身份证号码";         		        //列名
      iArray[3][1]="120px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="单位";      	   		//列名
      iArray[4][1]="130px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="家庭地址";      	   		//列名
      iArray[5][1]="80px";            			//列宽
      iArray[5][2]=10;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="手机";      	   		//列名
      iArray[6][1]="110px";            			//列宽
      iArray[6][2]=10;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="邮政编码";      	   	//列名
      iArray[7][1]="80px";            			//列宽
      iArray[7][2]=6;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="电话";      	   		//列名
      iArray[8][1]="120px";            			//列宽
      iArray[8][2]=10;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="关系";      	   		//列名
      iArray[9][1]="60px";            			//列宽
      iArray[9][2]=10;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      

      

      WarrantorGrid = new MulLineEnter( "fm" , "WarrantorGrid" ); 
      //这些属性必须在loadMulLine前
      WarrantorGrid.mulLineCount = 1;   
      WarrantorGrid.displayTitle = 1;
      //WarrantorGrid.locked=1;  
      WarrantorGrid.hiddenPlus=1;
	    WarrantorGrid.hiddenSubtraction=1;    
      WarrantorGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
}


</script>