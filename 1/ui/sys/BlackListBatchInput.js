//批量导入
var mDebug="0";
var mOperate="";
var showInfo;
var ImportPath;
var turnPage = new turnPageClass();

function QueryOnKeyDown(){

	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		var tSql = "select (case when type='0' then '个人' when type='1' then '组织' else '' end),blacklistcode,"+
				"name,'',nationality,(select codename from ldcode where codetype = 'idtype' and code = IDNoType),"+
				"idno,name8,name9,name10,remark from lcblacklist where 1=1 "
		
		if(fm.BlackGroupName.value != "" && fm.BlackPersonName.value != ""){
			alert("请只填写'黑名单组织姓名'或'黑名单个人姓名'");
			return false;
		}
		else if(fm.BlackGroupName.value != ""){
			tSql += 
				getWherePart('Name', 'BlackGroupName')
			    + " and type='1'"
			    + " ORDER BY BlacklistCode" ;
		 }else if(fm.BlackPersonName.value != ""){
		 	tSql +=
				getWherePart('Name', 'BlackPersonName')
			    + " and type='0'"
			    + " ORDER BY BlacklistCode" ;
		 }
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		return false;
	}
    else turnPage.queryModal(tSql, EvaluateGrid);
	}
}


function easyQuery(aBatchNo)
{  
	var tSql = "select (case when type='0' then '个人' when type='1' then '组织' else '' end),blacklistcode,"+
		"name,'',nationality,(select codename from ldcode where codetype = 'idtype' and code = IDNoType),"+
		"idno,name8,name9,name10,remark from lcblacklist where rem1 = '"+aBatchNo+"' "
		+getWherePart('Name', 'BlackGroupName')
	    +getWherePart('Name', 'BlackPersonName')
	    + " ORDER BY BlacklistCode" ;
	    
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		return false;
	}
    else turnPage.queryModal(tSql, EvaluateGrid);
	
}

//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(BatchNo){
  var tBatchNo = BatchNo;
  easyQuery(tBatchNo);
  //
  //easyQueryClick();
  //parent.fraInterface.afterSubmit(FlagStr,Content);
}
