
var showInfo;

var turnPage = new turnPageClass();


//***************************************************
//* 验证必填项
//***************************************************
function checkValid()
{
	if (document.fm.Name.value== "")
	{
		alert("请输入姓名!");
		document.fm.Name.value="";
		fm.all('Name').focus();
		return false;
	}
	return true;
}


//***************************************************
//* 点击‘保存’进行的操作
//***************************************************
function submitForm()
{
	 if(verifyInput() == false)
 {
 	return false;
 }
    if (checkValid() == false)
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN";
  fm.submit(); //提交
  //alert("chenggong 3");
  
  return true;
}


//***************************************************
//* 单击“修改”进行的操作
//***************************************************

function updateClick()
{
  //下面增加相应的代码
  
  if (fm.all('BlacklistCode').value==null || fm.all('BlacklistCode').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('BlacklistCode').value="";
 	return;
}
  
  if (fm.all('Name').value==null || fm.all('Name').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('Name').value="";
 	return;
}
 
  if (confirm("您确实想修改该条记录?"))
  {
 
  //showSubmitFrame(mDebug);
  
   //alert("xiu1");
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}           
//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content,aBlacklistCode)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  fm.all('BlacklistCode').value  = aBlacklistCode;
}
function initUpdate()
{
	if(transact == "update"){
		if(tBlacklistCode == null || tBlacklistCode == ""){
			alert("修改操作时，获取黑名单人员编码失败！");
			return false;
		}
		var tSQL = "select BlacklistCode,Name,Name1,Nationality,IDNoType,(select codename from ldcode where codetype = 'idtype' and code = IDNoType),"
		+ " IDNo,Name8,Name9,Name10,Remark"
	    + " FROM LCBlackList where  1=1  and type = '0' and BlacklistCode = '"+tBlacklistCode+"'";
		
		//执行查询并返回结果
		var strQueryResult = easyExecSql(tSQL);
		if(!strQueryResult){
			alert("修改操作时，根据黑名单人员编码获取人员信息失败！");
			return false;
		}
		fm.all('BlacklistCode').value = strQueryResult[0][0];
	    fm.all('Name').value = strQueryResult[0][1];
	    fm.all('Name1').value = strQueryResult[0][2];
	    fm.all('Nationality').value = strQueryResult[0][3];
	    fm.all('IDNoType').value = strQueryResult[0][4];
	    fm.all('IDNoTypeName').value = strQueryResult[0][5];
	    fm.all('IDNo').value = strQueryResult[0][6];
	    fm.all('Name8').value = strQueryResult[0][7];
	    fm.all('Name9').value = strQueryResult[0][8];
	    fm.all('Name10').value = strQueryResult[0][9];
	    fm.all('Remark').value = strQueryResult[0][10];
	}
	return true;
}