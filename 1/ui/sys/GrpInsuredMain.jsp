<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：TaskViewMain.jsp
//程序功能：工单管理框架页面
//创建日期：2007-3-18 16:03
//创建人  :YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<%

  String contNo = request.getParameter("ContNo");
	String tModeFlag = request.getParameter("ModeFlag" );
	String tPlanCode = request.getParameter("PlanCode" );
	String tContLoadFlag = request.getParameter("ContLoadFlag" );
	String tContType = request.getParameter("ContType" );
	
	String url = "../sys/GrpInsuredInput.jsp?ContNo=" + contNo + "&ModeFlag=" + tModeFlag 
	    + "&ContLoadFlag=" + tContLoadFlag +"&ContType=" + tContType + "&PlanCode=" + tPlanCode;
%>  


<html>
<head>
<title>工单录入 </title>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="1">
	<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="no" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="25,*,0" cols="*">
	    <frame id="fraTopButton" name="fraTopButton" noresize scrolling="no" src="./TaskInputTopButton.jsp">
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="<%=url%>">
		<frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
