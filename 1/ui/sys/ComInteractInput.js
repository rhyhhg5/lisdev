//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
	  initForm();
  }
  catch(re)
  {
  	alert("在OLDCom.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击保存图片时触发该函数
function submitForm()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
	showDiv( operateButton, "false" ); 
	showDiv( inputButton, "true" ); 
  if(!checkOutComCode()) return false;
  if(!checkpolicyregist()) return false;
  if (confirm("您确实想新增一条记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	  
  fm.all( 'fmtransact' ).value = "INSERT||COM";
  fm.submit(); //提交  
  }
  else
  {
    //mOperate="";
    alert("您取消了新增操作！");
  }  
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if(!checkOutComCode()) return false;
  if(!checkpolicyregist()) return false;
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //showSubmitFrame(mDebug);
  fm.all( 'fmtransact' ).value = "UPDATE||COM";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./ComInteractQuery.html");
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //showSubmitFrame(mDebug);
  fm.all( 'fmtransact' ).value = "DELETE||COM";

  fm.submit(); //提交
  initForm();
  }
  else
  {
    //mOperate="";
    alert("您取消了删除操作！");
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all( 'ComCode' ).value = arrResult[0][0];
		fm.all('OutComCode').value = arrResult[0][1];
		fm.all('Name').value = arrResult[0][2];
		fm.all('ShortName').value = arrResult[0][3];
		fm.all('Address').value = arrResult[0][4];
		fm.all('ZipCode').value = arrResult[0][5];
		fm.all('Phone').value = arrResult[0][6];
		fm.all('Fax').value = arrResult[0][7];
		fm.all('EMail').value = arrResult[0][8];
		fm.all('WebAddress').value = arrResult[0][9];
		fm.all('SatrapName').value = arrResult[0][10];
		fm.all('Sign').value = arrResult[0][11];
		fm.all('LetterServiceName').value = arrResult[0][12];
		fm.all('ServicePhone').value = arrResult[0][13];
		fm.all('ServicePostAddress').value = arrResult[0][14];
		fm.all('ServicePostZipCode').value = arrResult[0][15];
		fm.all('LetterServicePostAddress').value = arrResult[0][16];
		fm.all('LetterServicePostZipCode').value = arrResult[0][17];
		fm.all('ClaimReportPhone').value = arrResult[0][18];
		fm.all('PEOrderPhone').value = arrResult[0][19];
		fm.all('ServicePhone1').value = arrResult[0][20];
		fm.all('ServicePhone2').value = arrResult[0][21];
		fm.all('BackupPhone1').value = arrResult[0][22];
		fm.all('BackupPhone2').value = arrResult[0][23];
		fm.all('BackupAddress1').value = arrResult[0][24];
		fm.all('BackupAddress2').value = arrResult[0][25];
		fm.all('EName').value = arrResult[0][26];
		fm.all('EShortName').value = arrResult[0][27];
		fm.all('EAddress').value = arrResult[0][28];
		fm.all('EServicePostAddress').value = arrResult[0][29];
		fm.all('ELetterServiceName').value = arrResult[0][30];
		fm.all('ELetterServicePostAddress').value = arrResult[0][31];
		fm.all('EBackupAddress1').value = arrResult[0][32];
		fm.all('EBackupAddress2').value = arrResult[0][33];	
		fm.all('ShowName').value = arrResult[0][34];
		fm.all('PrintComName').value = arrResult[0][35];
		fm.all('SuperComCode').value = arrResult[0][36];
		fm.all('TaxRegistryNo').value = arrResult[0][37];
		fm.all('ContyFlag').value = arrResult[0][38];
		fm.all('InteractFlag').value = arrResult[0][39];
		fm.all('PolicyRegistCode').value = arrResult[0][40];
		fm.all('AreaCode').value = arrResult[0][41];
	}
	
	if(fm.all('Sign').value!=null && fm.all('Sign').value!=''){
		if(fm.all('Sign').value=='0'){
			fm.all('SignName').value ='未开业';
		}else if(fm.all('Sign').value=='1'){
			fm.all('SignName').value ='开业';
		}
	}else{
		fm.all('SignName').value ='';
	}
	
	if(fm.all('ContyFlag').value!=null && fm.all('ContyFlag').value!=''){
		if(fm.all('ContyFlag').value=='0'){
			fm.all('ContyFlagName').value ='非县级机构';
		}else if(fm.all('ContyFlag').value=='1'){
			fm.all('ContyFlagName').value ='县级机构';
		}
	}else{
		fm.all('ContyFlagName').value ='';
	}
	
	if(fm.all('InteractFlag').value!=null && fm.all('InteractFlag').value!=''){
		if(fm.all('InteractFlag').value=='Y'){
			fm.all('InteractFlagName').value ='是';
		}else if(fm.all('InteractFlag').value=='N'){
			fm.all('InteractFlagName').value ='否';
		}
	}else{
		fm.all('InteractFlagName').value ='';
	}
}
function checkOutComCode(){
    var OutComCode = fm.OutComCode.value;
    OutComCode = OutComCode.replace(/(^\s*)/g, "");
    OutComCode = OutComCode.replace(/(^\s*)|(\s*$)/g, "");
    if(OutComCode.length!=6){
    alert("对外公布的机构代码的长度应该为6！");
    return false;
    }
    fm.OutComCode.value = OutComCode;
    return true;
}

function checkpolicyregist(){
	var tComCode = fm.ComCode.value;
	var tPolicyRegistCode = fm.PolicyRegistCode.value;
	var tAreaCode = fm.AreaCode.value;
	if(tComCode.length == 8)
	{  
		if(tPolicyRegistCode == null || tPolicyRegistCode =='')
		{
			alert("机构编码为8位时，“保单登记平台对应代码”字段为必录项!");
			fm.all('PolicyRegistCode').focus();
		    return false;
		}
		if( tAreaCode== null || tAreaCode =='')
		{
			alert("机构编码为8位时，“地区代码”字段为必录项!");
			fm.all('AreaCode').focus();
		    return false;
		}
		if( tAreaCode.length!= 6||!isInteger(tAreaCode))
		{
			alert("地区代码必须为6位数字!");
			fm.all('AreaCode').focus();
		    return false;
		}
		
	}else{
		if( tAreaCode!= null && tAreaCode !='')
		{
			alert("非8位管理机构，请勿录入地区代码");
			fm.all('AreaCode').focus();
		    return false;
		}	
	}
	return true;
}

function checkpolicyregistcode(){
	var tOutComCode = fm.OutComCode.value;
	var tPolicyRegistCode = fm.PolicyRegistCode.value;
	if( tPolicyRegistCode != null )
	{
	   if
	     (!isInteger(tPolicyRegistCode) || tPolicyRegistCode.length > 15 ||tPolicyRegistCode.length<12)
	     {     
	        alert( tPolicyRegistCode + "保单登记平台对应代码需为12-15位数字，请核查！");
	        fm.all('PolicyRegistCode').focus();
	      	return false;
    }
	else
    {
        if(tPolicyRegistCode.substr(0,6)!='000085'){
        	alert("保单登记平台对应代码的前六位必须是000085！");
        	return false;
        }    	
    } 
	if(tOutComCode.length == 8)
	{
		if(tPolicyRegistCode == null || tPolicyRegistCode =='')
		{
			alert("机构编码为8位时，“保单登记平台对应代码”字段为必录项");
		    return false;
		}
	}
	 return true;
  }
}	