<%
//程序名称：OLDCodeQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 17:44:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

  <SCRIPT src="./OLDCodeQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./OLDCodeQueryInit.jsp"%>
  <title>公用代码表 </title>
</head>
<body  onload="initForm();" >
  <form action="./OLDCodeQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <table  class= common>
  <TR  class= common>
    <TD  class= title>
      编码类型
    </TD>
    <TD  class= input>
      <Input class= common name=CodeType >
    </TD>
    <TD  class= title>
      编码
    </TD>
    <TD  class= input>
      <Input class= common name=Code >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      编码名称
    </TD>
    <TD  class= input>
      <Input class= common name=CodeName >
    </TD>
    <TD  class= title>
      编码别名
    </TD>
    <TD  class= input>
      <Input class= common name=CodeAlias >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      机构代码
    </TD>
    <TD  class= input>
      <Input class= common name=ComCode >
    </TD>
    <TD  class= title>
      其它标志
    </TD>
    <TD  class= input>
      <Input class= common name=OtherSign >
    </TD>
  </TR>
</table>

    </table>
          <INPUT VALUE="查询" TYPE=button onclick="submitForm();return false;"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCodeGrid);">
    		</td>
    		<td class= titleImg>
    			 公用代码表结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCodeGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
     	<TR class = common >	
      <INPUT class = common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class = common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class = common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class = common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">	  			
  		</TR>						
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
