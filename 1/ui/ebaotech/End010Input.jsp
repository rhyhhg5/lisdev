<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：
	//程序功能：
	//创建日期：2015-10-16 17:32:48
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	String tProjectNo = request.getParameter("ProjectNo");
	String transact = request.getParameter("transact");
	String tLookFlag = request.getParameter("LookFlag");
%>
<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<script src="End010Input.js"></script>
<%@include file="End010InputInit.jsp"%>

<script>
  		var ManageCom = "<%=tGI.ManageCom%>"; 
  		var tProjectNo = "<%=tProjectNo%>";
		var transact = "<%=transact%>";
		var tLookFlag = "<%=tLookFlag%>";
</script>
</head>
<body onload="initForm();initElementtype();">
	<form action="End010InputSave.jsp" method=post name=fm
		target="fraSubmit">
		<!-- ----------------------------------------------------- -->
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divXXXX);"></td>
				<td class=titleImg>查询条件(与中保信交互)</td>
			</tr>
		</table>
		<div id="divXXXX" style="display: ''">
			<table class=common align=center>
				<TR class=common>
					<TD class=title8>查询起期</TD>
					<TD class=input8><Input class="coolDatePicker" readonly
						dateFormat="short" name=StartDate verify="查询起期|notnull"
						MAXLENGTH="10" MaskType=MTDate elementtype=nacessary></TD>
					<TD class=title8>查询止期</TD>
					<TD class=input8><Input class="coolDatePicker" readonly
						dateFormat="short" name=EndDate verify="查询止期|notnull"
						MAXLENGTH="10" MaskType=MTDate elementtype=nacessary></TD>
						
				</TR>
     		</table>
		</div>
		<table>
			<td class=button><input type="button" class=cssButton
				value="查 询 " id="AddIDCh" name="AddIDCh" onclick="b_Submit()">
			</td>
		</table>
		<br/>
		<hr>
		<!-- ----------------------------------------------- -->
		<br/>
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divXXXX);"></td>
				<td class=titleImg>查询条件</td>
			</tr>
		</table>
		<div id="divXXXX" style="display: ''">
			<table class=common align=center>
				<TR class=common>
					<TD class=title8>查询起期</TD>
					<TD class=input8><Input class="coolDatePicker"
						dateFormat="short" name=queryStartDate verify="查询起期|notnull"
						MAXLENGTH="10" MaskType=MTDate elementtype=nacessary></TD>
					<TD class=title8>查询止期</TD>
					<TD class=input8><Input class="coolDatePicker"
						dateFormat="short" name=queryEndDate verify="查询止期|notnull"
						MAXLENGTH="10" MaskType=MTDate elementtype=nacessary></TD>
						
				</TR>
     		</table>
		</div>
		<table>
			<td class=button><input type="button" class=cssButton
				value="查 询 " id="AddID" name="AddID" onclick="queryEnd010()">
			</td>
		</table>
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divEnd010Grid);"></td>
				<td class=titleImg>保单转入余额信息</td>
			</tr>
		</table>
		<!-- mulLine -->
		<Div id="divEnd010Grid" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1><span id="spanEnd010Grid">
					</span></td>
				</tr>
			</table>
		</div>
		<Div id="divPage" align=center style="display: 'none'">
			<table>
				<tr align=center>
					<td class=button width="10%"><INPUT CLASS=cssButton
						VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
						<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
						onclick="turnPage.previousPage();"> <INPUT
						CLASS=cssButton VALUE="下一页" TYPE=button
						onclick="turnPage.nextPage();"> <INPUT CLASS=cssButton
						VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
					</td>
				</tr>
			</table>
		</Div>
	</form>
</body>
</html>
