<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：
	//程序功能：
	//创建日期：2015-10-16 17:32:48
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	String tProjectNo = request.getParameter("ProjectNo");
	String transact = request.getParameter("transact");
	String tLookFlag = request.getParameter("LookFlag");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
%>
<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<script src="Qry001Input.js"></script>
<%@include file="Qry001InputInit.jsp"%>
<script>
  		var ManageCom = "<%=tGI.ManageCom%>"; 
  		var tProjectNo = "<%=tProjectNo%>";
		var transact = "<%=transact%>";
		var tLookFlag = "<%=tLookFlag%>";
		var operator = "<%=tG.Operator%>";
</script>
</head>
<body onload="initForm();initElementtype();">
	<form action="Qry001InputSave.jsp" method=post name=fm
		target="fraSubmit">
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divXXX);"></td>
				<td class=titleImg>查询(与中保信交互)</td>
			</tr>
			
		</table>
		<div id="divXXX" style="display: ''">
			<table class=common>
				<TR class=common>
				
				<TD class=title>查询起期</TD>
					<TD class=input><Input class="coolDatePicker" readonly 
						dateFormat="short" name=startDate1 verify="查询起期|notnull"
						MAXLENGTH="10" MaskType=MTDate></TD>
				
				
					<TD class=title>查询止期</TD>
					<TD class=input><Input class="coolDatePicker" readonly 
						dateFormat="short" name=endtDate1 verify="查询止期|notnull"
						MAXLENGTH="10" MaskType=MTDate></TD>
						
					
					
					
					</tr>
					<br/>
					<tr>
					
					<TD class=title>客户平台编码</TD>
					<TD class=input><input class=common name="CustomerNo" elementtype=nacessary></TD>
					
					
					
					<TD class=title>保单号</TD>
					<TD class=input><input class=common name="ContNo" elementtype=nacessary></TD>
					
					
					
				</TR>
				<tr><td class=button><input type=hidden id="fmtransact"
						name="fmtransact"> <input type=button class=cssButton
						value="查询" id="AddID" name="AddID" onclick="add();"></td>
						</tr>
			</table>
		</div>
<br>
<hr>
<br>

		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divXXXX);"></td>
				<td class=titleImg>查询条件</td>
			</tr>
		</table>
		<div id="divXXXX" style="display: ''">
			<table class=common>
				<TR class=common>
					<TD class=title>平台客户编码</TD>
					<TD class=input><input class=common name="customerNo"
						elementtype=nacessary></TD>
					<TD class=title>查询起期</TD>
					<TD class=input><Input class="coolDatePicker"
						dateFormat="short" name=startDate verify="查询起期|notnull"
						MAXLENGTH="10" MaskType=MTDate></TD>
					<TD class=title>查询止期</TD>
					<TD class=input><Input class="coolDatePicker"
						dateFormat="short" name=endtDate verify="查询止期|notnull"
						MAXLENGTH="10" MaskType=MTDate></TD>
				</TR>
				<TR class=common>
					<td class= title>投保税优标识</td>
				    <td class=input>
					<Input class= "codeno" name=taxDiscountedIndi CodeData="0|^0|无效识别码^1|有效识别码" ondblclick="return showCodeListEx('taxDiscountedIndi',[this,taxDiscountedIndiName],[0,1]);" onkeyup="return showCodeListKeyEx('taxDiscountedIndi',[this,taxDiscountedIndiName],[0,1]);" ><Input class=codename name=taxDiscountedIndiName readonly  >
				    </td>
					<TD class=title>投保单号</TD>
					<TD class=input><input class=common name="applyNo"></TD>
					<TD class=title>保单号码</TD>
					<TD class=input><input class=common name="policyNo"></TD>
				</TR>

				
			</table>
		</div>
		<table>
			<tr>
				<td class=button><input type="button" class=cssButton
					value="保单查询 " id="AddID" name="AddID" onclick="queryPolicy();"></td>
			</tr>
		</table>
		<!-- 保单信息 -->
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divPolicyGrid);"></td>
				<td class=titleImg>保单信息</td>
			</tr>
		</table>
		<!-- mulLine1 -->
		<Div id="divPolicyGrid" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1><span id="spanPolicyGrid">
					</span></td>
				</tr>
			</table>
		</div>
		<Div id="divPage1" align=center style="display: ''">
			<table>
				<tr align=center>
					<td class=button width="10%"><INPUT CLASS=cssButton
						VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
						<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
						onclick="turnPage.previousPage();"> <INPUT
						CLASS=cssButton VALUE="下一页" TYPE=button
						onclick="turnPage.nextPage();"> <INPUT CLASS=cssButton
						VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
					</td>
				</tr>
			</table>
		</Div>
		
		<!-- 险种信息 -->
		<table>
			<tr>
				<td class=button><input type="button" class=cssButton
					value="险种查询 " id="AddRisk" name="AddRisk" onclick="queryRisk();"></td>
			</tr>
		</table>
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divCoverageGrid);"></td>
				<td class=titleImg>险种信息</td>
			</tr>
		</table>
		<!-- mulLine2 -->
		<Div id="divCoverageGrid" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1><span id="spanCoverageGrid">
					</span></td>
				</tr>
			</table>
		</div>
		<Div id="divPage2" align=center style="display: ''">
			<table>
				<tr align=center>
					<td class=button width="10%"><INPUT CLASS=cssButton
						VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
						<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
						onclick="turnPage2.previousPage();"> <INPUT
						CLASS=cssButton VALUE="下一页" TYPE=button
						onclick="turnPage2.nextPage();"> <INPUT CLASS=cssButton
						VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
					</td>
				</tr>
			</table>
		</Div>
	
		<table>
			<tr>
				<td class=button><input type="button" class=cssButton
					value="赔案查询 " id="AddClaim" name="AddClaim" onclick="queryClaim();"></td>
			</tr>
		</table>
		<!-- 赔案信息 -->
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divClaimGrid);"></td>
				<td class=titleImg>赔案信息</td>
			</tr>
		</table>
		<!-- mulLine3 -->
		<Div id="divClaimGrid" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1><span id="spanClaimGrid">
					</span></td>
				</tr>
			</table>
		</div>
		<Div id="divPage3" align=center style="display: 'none'">
			<table>
				<tr align=center>
					<td class=button width="10%"><INPUT CLASS=cssButton
						VALUE="首  页" TYPE=button onclick="turnPage3.firstPage();">
						<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
						onclick="turnPage3.previousPage();"> <INPUT
						CLASS=cssButton VALUE="下一页" TYPE=button
						onclick="turnPage3.nextPage();"> <INPUT CLASS=cssButton
						VALUE="尾  页" TYPE=button onclick="turnPage3.lastPage();">
					</td>
				</tr>
			</table>
		</Div>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
