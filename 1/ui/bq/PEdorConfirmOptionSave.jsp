<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorConfirmOptionSave.jsp
//程序功能：保全个单缴费方式保存页面
//创建日期：2005-04-19
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	String flag = "Succ";
	String content = "";
  
	//接收参数
	String fmtransact = request.getParameter("fmtransact");
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String payMode = request.getParameter("PayMode");
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("payMode", payMode);	
	tTransferData.setNameAndValue("endDate", request.getParameter("EndDate"));
	tTransferData.setNameAndValue("payDate", request.getParameter("PayDate"));
	tTransferData.setNameAndValue("bank", request.getParameter("Bank"));
	tTransferData.setNameAndValue("bankAccno", request.getParameter("BankAccno"));
	tTransferData.setNameAndValue("accName", request.getParameter("AccName"));
  
  //生成交退费通知书
	FeeNoticeVtsUI tFeeNoticeVtsUI = new FeeNoticeVtsUI(edorAcceptNo);
	if (!tFeeNoticeVtsUI.submitData(tTransferData))
	{
		flag = "Fail";
		content = "生成批单失败！原因是：" + tFeeNoticeVtsUI.getError();
	}

	//银行转帐
	if ((payMode != null) && (payMode.equals("4")))
	{
		GlobalInput tGI = (GlobalInput)session.getValue("GI");
		VData data = new VData();
		data.add(tGI);
		data.add(tTransferData);
		SetPayInfo spi = new SetPayInfo(edorAcceptNo);
		if (!spi.submitDate(data, fmtransact))
		{
			System.out.println("设置转帐信息失败！");
		}
	}
	content = PubFun.changForHTML(content);
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>

