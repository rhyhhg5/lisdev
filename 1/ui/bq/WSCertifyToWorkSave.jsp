<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
	//程序名称：
	//程序功能：激活卡补激活并生成客户资料变更的工单
	//创建日期：2010-08-12
	//创建人 :  XP
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%>


<%
	System.out.println("WSCertifyToWorkSave.jsp start ...");

	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tGI = (GlobalInput) session.getValue("GI");

	try {
		String tFlag = request.getParameter("Flag");
		String tCardNo = request.getParameter("BCardNo");
		String tName = request.getParameter("BName");
		String tIDNo = request.getParameter("BIDNo");
		System.out.println("tCardNo:" + tCardNo);
		System.out.println("tFlag:" + tFlag);
		LICertifySchema tLICertifySchema = new LICertifySchema();

		tLICertifySchema.setCardNo(tCardNo);

		System.out.println("tCardNo:" + tCardNo);
		VData tVData = new VData();

		TransferData tTransferData = new TransferData();

		tTransferData.setNameAndValue("Name", tName);
		tTransferData.setNameAndValue("IDNo", tIDNo);

		tVData.add(tLICertifySchema);
		tVData.add(tTransferData);
		tVData.add(tGI);

		if ("1".equals(tFlag)) {
			TransferData mTransferData = new TransferData();
			mTransferData.setNameAndValue("CardNo", tCardNo);

			VData mVData = new VData();
			mVData.add(mTransferData);
			mVData.add(tGI);
			CertActSyncUI tCardActiveRenewUI = new CertActSyncUI();
			if (!tCardActiveRenewUI.submitData(mVData, "ActSync")) {
				Content = " 卡激活insured数据处理失败，原因是: "
						+ tCardActiveRenewUI.mErrors.getFirstError();
				FlagStr = "Fail";
			} else {
				System.out.println(tCardNo + "卡号的licertifyinsured生成成功");
				FlagStr = "Succ1";
			}
		}
		if("0".equals(tFlag)){
		System.out.println(tCardNo + "卡号的licertifyinsured之前已经生成");
		FlagStr = "Succ1";
		} 

		System.out.println("CardActiveRenewUI的FlagStr:" + FlagStr);
		
		String checkWS="";
		checkWS=new ExeSQL().getOneValue("select 1 from lccont where contno='"+tCardNo+"' with ur");
	
		if(checkWS.equals("1"))
		{
		System.out.println("该卡已经实名化完毕,卡号为:" + tCardNo);
		FlagStr = "Succ2";
		}

		if (FlagStr == "Succ1") {
		
			WSCertifyUI tWSCertifyUI = new WSCertifyUI();
			if (!tWSCertifyUI.submitData(tVData, "WSCertify")) {
				Content = "激活卡实名化处理失败，原因是: "
						+ tWSCertifyUI.mErrors.getFirstError();
				FlagStr = "Fail";
			} else {
				System.out.println(tCardNo + "卡号的实名化处理成功！");
				FlagStr = "Succ2";
			}
		}
		System.out.println("tWSCertifyUI的FlagStr:" + FlagStr);

		String checkClaim="";
		checkClaim=new ExeSQL().getOneValue("select 1 from llcase where customerno in (select insuredno from lcpol where contno='"+tCardNo+"') and rgtstate not in ('11','12','14') with ur");
	
		if(checkClaim.equals("1"))
		{
		Content = "工单生成处理失败，原因是保单号: "+tCardNo+"所对应的被保人存在理赔";
		FlagStr = "Fail";
		}

		if (FlagStr == "Succ2") {
			WSCertifyToWorkBL tWSCertifyToWorkBL = new WSCertifyToWorkBL();
			if (!tWSCertifyToWorkBL.submitData(tVData, null)) {
				Content = "工单生成处理失败，原因是: "
						+ tWSCertifyToWorkBL.mErrors.getFirstError();
				FlagStr = "Fail";
			} else {
				Content = "激活卡客户资料变更工单生成成功,对应工单号为:"
						+ tWSCertifyToWorkBL.getmEdorNo()
						+ ",请在个人信箱选择对应工单进行处理！";
				FlagStr = "Succ";
			}
		}
	} catch (Exception e) {
		Content = " 处理失败。";
		FlagStr = "Fail";
		e.printStackTrace();
	}

	System.out.println("WSCertifyToWorkSave.jsp end ...");
%>

<html>
	<script language="javascript">
    parent.fraInterface.afterWSSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
