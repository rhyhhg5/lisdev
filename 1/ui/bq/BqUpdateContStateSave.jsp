<%@page import="java.util.Vector"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sinosoft.lis.bq.BqUpdateContStateUI"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：BqUpdateContStateSave.jsp
	//程序功能：保单状态维护
	//创建日期：20170426
	//创建人  ：wxd
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输入参数
	BqUpdateContStateUI contStateUI = new BqUpdateContStateUI();
	String tChk[] = request.getParameterValues("InpContStateGridChk");
	String tGridNo[] = request.getParameterValues("ContStateGridNo"); //得到序号列的所有值 
	String tPolNoArray[] = request.getParameterValues("ContStateGrid3");
	String tcontNo = request.getParameter("ContNo");
	//System.out.println("contno====------"+tcontNo);

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	StringBuffer tPolNo = new StringBuffer();

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();

	// 从 mulline 中取得的是一列的值,所以遍历取出一行的值,然后把参数放入 TransferData 容器?
	for (int i = 0; i < tGridNo.length; i++) {
		if ("1".equals(tChk[i])) {
			System.out.println("第" + i + "行被选中,i=" + i);
			System.out.println(tPolNoArray[i]);
			tPolNo.append(tPolNoArray[i] + ",");
		}
	}
	transferData.setNameAndValue("TContNo", tcontNo);
	transferData.setNameAndValue("tPolNo", tPolNo);
	if (!"Fail".equals(FlagStr)) {
		// 准备向后台传输数据 VData
		VData tVData = new VData();// Vector List 实现类之一,支持线程的同步
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			contStateUI.submitData(tVData, "");// 提交数据和要进行的操作
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = contStateUI.mErrors;
			if (!tError.needDealError()) {
				Content = "修改成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>