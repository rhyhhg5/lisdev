var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  easyQueryClick();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(ex)
  {
  	alert("在FailListInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{
  var tStartDate = fm.all("StartDate").value;
  var tEndDate = fm.all("EndDate").value;
  var strSQL = "select a.ContNo, "
             + "(select managecom from lccont where contno = a.contno union select managecom from lbcont where contno = a.contno fetch first 1 row only), "
             + "a.sgetdate, "
             + "(case a.bonusflag when '1' then '现金' else '累积生息' end), "
             + "a.fiscalyear, "
             + "a.bonusmoney,a.bonusflag,a.polno "
             + " from LOBonusPol a "
			 + " where 1=1 "
             + " and exists (select 1 from lccont where contno = a.contno and ManageCom like '"+ fm.ManageCom.value + "%' union select 1 from lbcont where contno = a.contno and ManageCom like '"+ fm.ManageCom.value + "%' fetch first 1 row only) "
             + getWherePart( 'a.ContNo ','ContNo') 
			 + getWherePart( 'a.bonusflag ','queryType') 
             + " and a.Sgetdate between '" + tStartDate + "' and '" + tEndDate + "' with ur ";        
             
  turnPage1.queryModal(strSQL, BonusGrid); 
  
  fm.sql.value = strSQL;
}

function queryClick()
{
	
	//得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	var tStartDate = fm.all("StartDate").value;
	var tEndDate = fm.all("EndDate").value;
	
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("红利应派发起期不能为空！");
		return false;
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("红利应派发止期不能为空！");
		return false;
	}
	
	var tTodayDate=fm.Today.value;
	
	if(fm.all("ContNo").value==null||fm.all("ContNo").value==""){
		//应收时间起止期三个月校验
		var t1 = new Date(tStartDate.replace(/-/g,"\/")).getTime();
	    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
	    var tMinus = (t2-t1)/(24*60*60*1000);  
	    if(tMinus>93)
	    {
			alert("分红日期止期不能大于起期90天!");
			return false;
	    }
	}
	var tqueryType = fm.all("queryType").value;
	easyQueryClick();
  if(BonusGrid.mulLineCount == 0)
  {
   alert("没有符合条件的给付信息");
   return false;
  }
}

function printClick()
{
	var tSel = BonusGrid.getSelNo();	
	  if( tSel == 0 || tSel == null )
	  {
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
		  fm.submit();
	  }else{
	  	 var tRow = BonusGrid.getSelNo() - 1;	        
		 var tPolNo=BonusGrid.getRowColData(tRow,8);
 		 var tYear=BonusGrid.getRowColData(tRow,5);
		 if(fm.all("queryType").value=="1"){
			var showStr="正在准备打印数据，请稍后...";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.target = "fraSubmit";
			fm.action="../uw/PDFPrintSave.jsp?Code=HL001&StandbyFlag2="+tPolNo+"&StandbyFlag1="+tYear;
			fm.submit();
		 }
		 if(fm.all("queryType").value=="2"){
		 	var showStr="正在准备打印数据，请稍后...";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.target = "fraSubmit";
			fm.action="../uw/PDFPrintSave.jsp?Code=HL002&StandbyFlag2="+tPolNo+"&StandbyFlag1="+tYear;
			fm.submit();
		 }
	  }
}

