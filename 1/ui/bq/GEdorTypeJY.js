//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var addrFlag="NEW"

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function edorTypeCTReturn()
{
		initForm();
}

function edorTypeCTSave()
{
  if (!beforeSubmit())
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmAction').value = "INSERT||EDORAC";
  fm.submit();
}

function customerQuery()
{	
	window.open("./LCAppntGrpQuery.html");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content)
{
  showInfo.close(); 
  window.focus();
    
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  if(FlagStr == "Succ")
  {
    top.close();
    top.opener.top.focus();
    top.opener.getGrpEdorItem();
  }
}


function afterCodeSelect( cCodeName, Field )
{
}
//被保人模板
function grpInsuList(){
  window.open("../sys/GrpUliInsuredInput.jsp?ContNo=" + fm.all( 'GrpContNo' ).value + "&ModeFlag=0&ContLoadFlag=2" +"&ContType=1");  
}
//万能险账户查询按钮功能
function GPublicAccQuery()
{
  var strSql = "SELECT prtno from lcgrppol l where grpcontno='"+fm.all( 'GrpContNo' ).value+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode) ";
  var arrResult = easyExecSql(strSql);
  if(arrResult != null){
      window.open("../sys/GPublicAccQueryInput.jsp?GrpContNo="+fm.all( 'GrpContNo' ).value+"&PrtNo="+arrResult[0][0]);  
  }else{
  	alert("查询异常");
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  
  var checkedCount = 0;
  for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
	{
	  if (LCGrpPolGrid.getChkNo(i)) 
		{
		  checkedCount = checkedCount + 1;
		}
	}
	if(checkedCount == 0)
	{
	  alert("请选择险种。");
	  return false;
	}
  
  return true;
}           
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
}

function returnParent()
{
  top.opener.focus();
  try
  {
		top.opener.getGrpEdorItem();
	}
	catch (ex) {}
  top.close();
}

/*********************************************************************
 *  查询投保单位信息
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function getGrpCont()
{
  //var strSQL = "select * from LCGrpCont  where GrpContNo = '" 
  //              + fm.GrpContNo.value + "'";
  var strSQL = "select b.*,(select min(paytodate) from lccont a where a.GrpContNo = b.GrpContNo) "
  						+" from LCGrpCont b where GrpContNo = '" 
                + fm.GrpContNo.value + "'";
  var arrResult = easyExecSql(strSQL, 1, 0);
  
    if (arrResult != null) 
    {
        try {fm.all('AppntNo').value= arrResult[0][12]; } catch(ex) { }; //客户号码
        try {fm.all('Peoples2').value= arrResult[0][14]; } catch(ex) { }; //投保总人数
        try {fm.all('GrpName').value= arrResult[0][15]; } catch(ex) { }; //单位名称
        try {fm.all('SignCom').value= arrResult[0][48]; } catch(ex) { }; //签单机构
        try {fm.all('SignDate').value= arrResult[0][49]; } catch(ex) { }; //签单日期
        try {fm.all('CValiDate').value= arrResult[0][51]; } catch(ex) { }; //保单生效日期
        try {fm.all('CValiDate1').value= arrResult[0][51]; } catch(ex) { }; //保单生效日期
        try {fm.all('Prem').value= arrResult[0][59]; } catch(ex) { }; //总保费
        try {fm.all('Amnt').value= arrResult[0][60]; } catch(ex) { }; //总保额
        try {fm.all('InsueredDate').value= arrResult[0][108]; } catch(ex) { }; //投保日期
        try {fm.all('PayToDay').value= arrResult[0][130]; } catch(ex) { }; //交至日期
    }
    else
    {
        alert('未查到该集体保单！');
    } 
}

//处理险种相关信息
function queryLCGrpPolGrid()
{      //cjg 判断是否是团体万能保单
        var verifySql ="select 1  from lcgrppol a where a.grpcontno='"+fm.GrpContNo.value +"' and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G') ";
        var arrVerifyResult = easyExecSql(verifySql);
        var sql ="";    
        if(arrVerifyResult!=null &&arrVerifyResult[0][0]=="1"){
        sql = " select  '',(select riskSeqNo from LCGrPPol where grpContNo = a.grpContNo and riskCode = c.riskCode),c.riskcode ,(select riskname from lmriskapp where riskcode=c.riskcode),a.peoples2, sum(c.prem),"
          + "  a.ProposalGrpContNo, '', '' from  LCgrpcont a,  LCPol c where a.grpContNo = c.grpContNo and a.grpContNo ='"+fm.GrpContNo.value+"' group by a.grpcontno,a.ProposalGrpContNo,a.peoples2,c.riskcode ";
         }else{

         var sql = "  select a.contPlanCode, f.riskSeqNo, a.riskCode, b.riskName, "
            + "   (select count(1) "
            + "   from LCInsured c, LCPol d "
            + "   where c.contNo = d.contNo "
            + "       and c.insuredNo = d.insuredNo "
            + "       and c.contPlanCode = a.ContPlanCode "
            + "       and d.grpPolNo = f.grpPolNo), "
            + "   (select sum(prem) "
            + "   from LCInsured c, LCPol d "
            + "   where c.contNo = d.contNo "
            + "       and c.insuredNo = d.insuredNo "
            + "       and c.contPlanCode = a.ContPlanCode "
            + "       and d.grpPolNo = f.grpPolNo), "
            + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType "
            + "from LCContPlanRisk a, LMRiskApp b, LCGrpCont e, LCGrpPol f "
            + "where a.grpContNo = e.grpContNo "
            + "   and a.riskCode = b.riskCode "
            + "   and a.grpContNo = f.grpContNo "
            + "   and a.riskCode = f.riskCode "
            + "   and a.contPlanCode not in('00', '11') "
            + "   and b.riskType3 != '7' "
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
            
            + "union "
            
            + "  select a.contPlanCode, f.riskSeqNo, a.riskCode, b.riskName, "
            + "   (select count(1) "
            + "   from LCInsured c, LCPol d "
            + "   where c.contNo = d.contNo "
            + "       and c.insuredNo = d.insuredNo "
            + "       and c.contPlanCode = a.ContPlanCode "
            + "       and d.grpPolNo = f.grpPolNo), "
            + "   (select sum(prem) "
            + "   from LCInsured c, LCPol d "
            + "   where c.contNo = d.contNo "
            + "       and c.insuredNo = d.insuredNo "
            + "       and c.contPlanCode = a.ContPlanCode "
            + "       and d.grpPolNo = f.grpPolNo), "
            + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType "
            + "from LCContPlanRisk a, LMRiskApp b, LCGrpCont e, LCGrpPol f "
            + "where a.grpContNo = e.grpContNo "
            + "   and a.riskCode = b.riskCode "
            + "   and a.grpContNo = f.grpContNo "
            + "   and a.riskCode = f.riskCode "
            + "   and (e.cardFlag = '0' or riskType3 = '7' or risktype8='6' ) "
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' ";
            }
    turnPage.pageLineNum = 50;
	turnPage.queryModal(sql, LCGrpPolGrid);
}

//预算退保后的保费
function calLeavingPrem()
{
  var selectPrem = 0;
  var mulLineCount = LCGrpPolGrid.mulLineCount;
	for(var i = 0; i < mulLineCount; i++)
	{
	  if (LCGrpPolGrid.getChkNo(i)) 
		{
		  selectPrem = selectPrem + parseFloat(LCGrpPolGrid.getRowColDataByName(i, "prem"));
		}
	}
	fm.leavingPrem.value = mathRound(parseFloat(fm.Prem.value) - selectPrem);
}

//钩选已录入的保障计划
function checkSelectContPlanRisk()
{
	LCGrpPolGrid.checkBoxAllNot(this, LCGrpPolGrid.colCount);
	
	var sql = "  select ProposalGrpContNo, MainRiskCode, RiskCode, ContPlanCode, PlanType "
	          + "from LPContPlanRisk "
	          + "where edorNo = '" + fm.EdorNo.value + "' "
	          + "   and edorType = '" + fm.EdorType.value + "' "
	          + "   and grpContNo = '" + fm.GrpContNo.value + "' ";
	var result = easyExecSql(sql);
	if(result)
	{
  	for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
  	{
  	  for(var t = 0; t < result.length; t++)
  	  {
    		if(LCGrpPolGrid.getRowColDataByName(i, "contPlanCode") == result[t][3]
    		  && LCGrpPolGrid.getRowColDataByName(i, "riskCode") == result[t][2]
    		  && LCGrpPolGrid.getRowColDataByName(i, "proposalGrpContNo") == result[t][0]
    		  && LCGrpPolGrid.getRowColDataByName(i, "mainRiskCode") == result[t][1]
    		  && LCGrpPolGrid.getRowColDataByName(i, "planType") == result[t][4])
    		{
    			LCGrpPolGrid.checkBoxSel(i + 1);
    		}
    	}
  	}
  }
}