<%
//程序名称：PEdorTypeZTSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----ZTsubmit---");
  LPInsuredSchema tLPInsuredSchema   = new LPInsuredSchema();
  PInsuredUI tPInsuredUI   = new PInsuredUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("------transact:"+transact);
 
  
  //个人保单批改信息
	tLPInsuredSchema.setPolNo(request.getParameter("PolNo"));
  	tLPInsuredSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPInsuredSchema.setEdorType(request.getParameter("EdorType"));
	tLPInsuredSchema.setCustomerNo(request.getParameter("CustomerNo"));
	tLPInsuredSchema.setName(request.getParameter("Name"));
	tLPInsuredSchema.setNationality(request.getParameter("Nationality"));
	tLPInsuredSchema.setMarriage(request.getParameter("Marriage"));
	tLPInsuredSchema.setStature(request.getParameter("Stature"));
	tLPInsuredSchema.setAvoirdupois(request.getParameter("Avoirdupois"));
	tLPInsuredSchema.setICNo(request.getParameter("ICNo"));
	tLPInsuredSchema.setHomeAddressCode(request.getParameter("HomeAddressCode"));
	tLPInsuredSchema.setHomeAddress(request.getParameter("HomeAddress"));
	tLPInsuredSchema.setPostalAddress(request.getParameter("PostalAddress"));
	tLPInsuredSchema.setZipCode(request.getParameter("ZipCode"));
	tLPInsuredSchema.setPhone(request.getParameter("Phone"));
	tLPInsuredSchema.setMobile(request.getParameter("Mobile"));
	tLPInsuredSchema.setEMail(request.getParameter("EMail"));

try
  {
  // 准备传输数据 VData
  
  	 VData tVData = new VData();  
  	
	 //保存个人保单信息(保全)	
	 
  	tVData.addElement(tLPInsuredSchema);
        tPInsuredUI.submitData(tVData,transact);
	
	
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPInsuredUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

