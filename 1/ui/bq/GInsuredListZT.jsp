<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.pubfun.*" %>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  XmlExport txmlExport = null;   
    
  String edorAcceptNo = request.getParameter("edorAcceptNo");
  LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();
  mLPEdorAppSchema.setEdorAcceptNo(edorAcceptNo);
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("edorType", request.getParameter("edorType"));
  
  VData tVData = new VData();
  tVData.addElement(mLPEdorAppSchema);
  tVData.addElement(tG);
  tVData.addElement(tTransferData);
         
  PrtGrpInsuredListZTUI tPrtGrpInsuredListZTUI = new PrtGrpInsuredListZTUI(); 
  if(!tPrtGrpInsuredListZTUI.submitData(tVData,"PRINT"))
  {          
     	operFlag = false;
     	Content = tPrtGrpInsuredListZTUI.mErrors.getFirstError(); 
  }
  else
  {             
    VData mResult = tPrtGrpInsuredListZTUI.getResult();			
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    
    if(txmlExport==null)
    {
    	operFlag=false;
    	Content="没有得到要显示的数据文件";	  
    }
  }
  
  Readhtml rh=new Readhtml();
  rh.XmlParse(tPrtGrpInsuredListZTUI.getInputStream()); //相当于XmlExport.getInputStream();
  
  
  String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
  String temp=realpath.substring(realpath.length()-1,realpath.length());
  if(!temp.equals("/"))
  {
	  realpath=realpath+"/"; 
  }
  String templatename=rh.getTempLateName();//模板名字
  String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
  System.out.println("*********************templatepathname= " + templatepathname);
  //System.out.println("************************realpath="+realpath);
  String date=PubFun.getCurrentDate().replaceAll("-","");
  String time=PubFun.getCurrentTime().replaceAll(":","");
  String outname="被保险人清单打印"+tG.Operator+date+time+".xls";
  String outpathname=realpath+"vtsfile/"+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
 
  rh.setReadFileAddress(templatepathname);
  rh.setWriteFileAddress(outpathname);
  rh.start("vtsmuch");
  try {
		outname = java.net.URLEncoder.encode(outname, "UTF-8");
		outname = java.net.URLEncoder.encode(outname, "UTF-8");
		outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
		outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
%>
<html>
<a href="../f1print/download.jsp?filename=<%=outname%>&filenamepath=<%=outpathname%>">点击下载</a>
</html>
