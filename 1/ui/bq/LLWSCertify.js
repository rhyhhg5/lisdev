//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryCertifyList()
{
    if(!verifyInput2())
    {
        return false;
    }

    var tStrSql = ""
        + " select lict.CardNo, lict.PrtNo, lict.CValidate, lict.ActiveDate, "
        + " licti.Name, licti.IdNo, "
        + " CodeName('certifycontstate', lict.State) State, "
        + " (case when lict.WSState='00' then '未确认' else '已确认' end) WSState,licti.customerno"
        + " from LICertify lict "
        + " left join LICertifyInsured licti on licti.CardNo = lict.CardNo "
        + " where "
        + " lict.ManageCom like '" + fm.ManageCom.value + "%' "
     //   + "and exists (select 1 from lcpol where riskcode = '520301' and stateflag = '1' and prtno = lict.prtno) "
        + getWherePart("lict.CValidate", "StartCValidate", ">=")
        + getWherePart("lict.CValidate", "EndCValidate", "<=")
        + getWherePart("lict.CardNo", "CardNo")
        + getWherePart("lict.PrtNo", "PrtNo")
        + getWherePart("licti.idno","tIDNo")
        + getWherePart("licti.name","CustomerName")
        + getWherePart("licti.idtype","tIDType")
        + " with ur "
        ;
    
    turnPage1.pageDivName = "divCertifyListGridPage";
    turnPage1.queryModal(tStrSql, CertifyListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有待处理的结算信息！");
        return false;
    }
    return true;
}

function addPeoples()
{
  var tSel = CertifyListGrid.getSelNo();
  
  if(!checkData(tSel))
  {
    return false;
  }
  var tContNo = CertifyListGrid.getRowColDataByName(tSel-1,"ContNo");
  
  var url = "./LLWSCertifyPMain.jsp?ContNo="+tContNo;
  window.open(url);
}

function checkData(aSel)
{
  if(CertifyListGrid.mulLineCount == 0)
  {
    alert("卡折清单没有数据，请先查询！");
    return false;
  }

  
  if( aSel == 0 || aSel == null )
  {
    alert( "请先选择一条记录，再增加连带被保人！" );
    return false;
  }
  
  if(!checkWS(aSel))
  {
    return false;
  }
  
  if(!checkWrap(aSel))
  {
    return false;
  }
  
  return true;
}

//未实名化的卡折进行增加连带被保险人提示：该卡折未实名化，必须先实名化。
function checkWS(aSel)
{
  var tContNo = CertifyListGrid.getRowColDataByName(aSel-1,"ContNo");
  var sql = "select 1 from lccont where contno = '" + tContNo + "' " ;
  var rs = easyExecSql(sql);
  if(!rs)
  {
    alert("该卡折未实名化，必须先实名化!");	
    return false;	
  }
  return true;
}

//进行增加连带被保险人的卡折套餐必须是：民用燃气用户综合人身意外伤害保险
function checkWrap(aSel)
{
  var tPrtNo = CertifyListGrid.getRowColDataByName(aSel-1,"PrtNo");
  var sql = "select riskwrapcode from ldwrap where wrapname in ( "
          + "select certifyname from lmcertifydes where certifycode in ( "
          + "select certifycode from licertify where prtno= '" + tPrtNo + "')) " ;
  var rs = easyExecSql(sql);
  if(!rs)
  {
    alert("没有查询到该卡折的套餐定义!");	
    return false;
  }
  var riskWrapCode = rs[0][0];
  if(riskWrapCode != "WR0126")
  {
    alert("进行增加连带被保险人的卡折套餐必须是：民用燃气用户综合人身意外伤害保险！");	
    return false;	
  }
  return true;
}
