<%
//程序名称：GEdorTypeBCSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----BCsubmit---");
  LPBnfSchema tLPBnfSchema   = new LPBnfSchema();
  LPBnfSet tLPBnfSet = new LPBnfSet();
  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
  LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
  PGrpEdorBCDetailUI tPGrpEdorBCDetailUI   = new PGrpEdorBCDetailUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("------transact:"+transact);
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
  
  //个人保单批改信息
	tLPGrpEdorMainSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
  tLPGrpEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPGrpEdorMainSchema.setEdorType(request.getParameter("EdorType"));
	
	tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));
	tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));
	tLPEdorMainSchema.setInsuredNo(request.getParameter("CustomerNo"));

	if (transact.equals("INSERT||GRPMAIN"))
 {
 //准备给付项信息
	 String tGridNo[] = request.getParameterValues("LCBnfGridNo");
	 String tBnfType[] = request.getParameterValues("LCBnfGrid1");
	 String tCustomerNo[] = request.getParameterValues("LCBnfGrid2");
	 String tName[] = request.getParameterValues("LCBnfGrid3");
	 String tSex[] = request.getParameterValues("LCBnfGrid4");
   String tBirthday[] = request.getParameterValues("LCBnfGrid5");
	 String tIDType[] = request.getParameterValues("LCBnfGrid6");
	 String tIDNo[] = request.getParameterValues("LCBnfGrid7");
	 String tRelationToInsured[] = request.getParameterValues("LCBnfGrid8");
	 String tBnfGrade[] = request.getParameterValues("LCBnfGrid9");
	 String tBnfLot[] = request.getParameterValues("LCBnfGrid10");
	 String tAddress[] = request.getParameterValues("LCBnfGrid11");
	 String tZipCode[] = request.getParameterValues("LCBnfGrid12");
	 String tPhone[] = request.getParameterValues("LCBnfGrid13");
	 String tRgtAddress[] = request.getParameterValues("LCBnfGrid14");

		int Count = tGridNo.length;
	  for (int i=0;i<Count;i++)
	  {  	
	                tLPBnfSchema = new LPBnfSchema();
  			tLPBnfSchema.setPolNo(request.getParameter("PolNo"));
			  tLPBnfSchema.setEdorNo(request.getParameter("EdorNo"));
				tLPBnfSchema.setEdorType(request.getParameter("EdorType"));		
				tLPBnfSchema.setBnfType(tBnfType[i]);
				tLPBnfSchema.setCustomerNo(tCustomerNo[i]);
				tLPBnfSchema.setName(tName[i]);
				tLPBnfSchema.setSex(tSex[i]);
				tLPBnfSchema.setBirthday(tBirthday[i]);
				tLPBnfSchema.setIDType(tIDType[i]);
				tLPBnfSchema.setIDNo(tIDNo[i]);
				tLPBnfSchema.setRelationToInsured(tRelationToInsured[i]);
				tLPBnfSchema.setBnfGrade(tBnfGrade[i]);
				tLPBnfSchema.setBnfLot(tBnfLot[i]);
				tLPBnfSchema.setAddress(tAddress[i]);
				tLPBnfSchema.setRgtAddress(tRgtAddress[i]);
				tLPBnfSchema.setZipCode(tZipCode[i]);
				tLPBnfSchema.setPhone(tPhone[i]);
			 	tLPBnfSet.add(tLPBnfSchema);
  	}
  }

	

try
  {
  // 准备传输数据 VData
  
  	 VData tVData = new VData();  
  	
	 //保存个人保单信息(保全)	
	 	tVData.addElement(tG);
	 	tVData.addElement(tLPGrpEdorMainSchema);
	 	tVData.addElement(tLPEdorMainSchema);
  	tVData.addElement(tLPBnfSet);
    tPGrpEdorBCDetailUI.submitData(tVData,transact);
	
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
  	System.out.println("------success");
    tError = tPGrpEdorBCDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))
    	{
	    	if (tPGrpEdorBCDetailUI.getResult()!=null&&tPGrpEdorBCDetailUI.getResult().size()>0)
	    	{
	    		Result = (String)tPGrpEdorBCDetailUI.getResult().get(0);
	    		if (Result==null||Result.trim().equals(""))
	    		{
	    			FlagStr = "Fail";
	    			Content = "提交失败!!";
	    		}
	    	}
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

