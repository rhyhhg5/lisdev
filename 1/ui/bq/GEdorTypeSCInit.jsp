<%
//GEdorTypeACInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">  
//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divLPAppntGrpDetail.style.left=ex;
  	divLPAppntGrpDetail.style.top =ey;
   	detailQueryClick();
}
function initInpBox()
{ 

  try
  {      
  	
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('GrpPolNo').value = top.opener.fm.all('GrpPolNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('ContType').value ='2';
    
    //15,17,18,19,20,21,22,23,24,25,26,27,28,29,42,43,44,45,46,47,48,49,50,30,31,32,33,34,35,36,37,38,39,40,41 //
    fm.all('GrpNo').value = '';
    fm.all('GrpName').value = '';
    fm.all('GrpSpec').value = '';
    
    showOneCodeName("EdorCode", "EdorTypeName");

  }
  catch(ex)
  {
    alert("在GEdorTypeACInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在GEdorTypeACInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox(); 
    initDiv();
    initQuery();  
  }
  catch(re)
  {
    alert("GEdorTypeACInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initQuery()
{	
	 var i = 0;
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.all('fmtransact').value = "QUERY||MAIN";
		//alert("----begin---");
	//	showSubmitFrame(mDebug);
		fm.submit();	  	 	 
}

// 信息列表的初始化
function initLCAppntGrpGrid()
{
    var iArray = new Array();
      
      try
      {

      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";    	//列名1
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="客户号";         			//列名2
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="客户姓名";         			//列名8
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="投保人级别";         		//列名5
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[5]=new Array();
      iArray[5][0]="被保人与投保人关系";         		//列名6
      iArray[5][1]="150px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      LCAppntGrpGrid = new MulLineEnter( "fm" , "LCAppntGrpGrid" ); 
      //这些属性必须在loadMulLine前
      LCAppntGrpGrid.mulLineCount = 10;   
      LCAppntGrpGrid.displayTitle = 1;
      LCAppntGrpGrid.canSel=1;
      LCAppntGrpGrid.selBoxEventFuncName ="reportDetailClick";
      LCAppntGrpGrid.loadMulLine(iArray);  
      LCAppntGrpGrid.detailInfo="单击显示详细信息";
      }
      catch(ex)
      {
        alert(ex);
      }
}

function detailQueryClick()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
	fm.all('fmtransact').value = "QUERY||DETAIL";
	
	var tSel=LCAppntGrpGrid.getSelNo();
	
	if (tSel==0||tSel==null)
		alert("不能是空记录!");
	else
	{
		var tCustomerNo =LCAppntGrpGrid.getRowColData(tSel-1,2);
//		alert(tCustomerNo);
		fm.all('CustomerNo').value =tCustomerNo;
		//showSubmitFrame(mDebug);
		fm.submit();
	}
}
function initDiv()
{
}

</script>