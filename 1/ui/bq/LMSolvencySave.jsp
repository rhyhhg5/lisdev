
<%
	//程序名称：LDBonusInterestRateSave.jsp
	//程序功能：
	//创建日期：2012-8-30
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息

	CErrors tError = null;
	//后面要执行的动作：添加，修改
	String FlagStr = "";
	String Content = "";
	String transact = "";
	
	//执行动作：insert 添加纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");
	transact = request.getParameter("fmtransact");
	String Year = request.getParameter("tYear");
	String Quarter = request.getParameter("tQuarter");
	String Solvency = request.getParameter("tSolvency");
	String Flag = request.getParameter("tFlag");

	
	LDRiskRateSet tLDRiskRateSet=new LDRiskRateSet();
	LDRiskRateSchema tLDRiskRateSchema = new LDRiskRateSchema();
	tLDRiskRateSchema.setYear(Year);
	tLDRiskRateSchema.setQuarter(Quarter);
	tLDRiskRateSchema.setSolvency(Solvency);
	tLDRiskRateSchema.setFlag(Flag);
	tLDRiskRateSet.add(tLDRiskRateSchema);

	LMSolvencyUI tLMSolvencyUI = new LMSolvencyUI();
	try{
		// 准备传输数据 VData	
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tVData.add(tGlobalInput);
		tVData.addElement(tLDRiskRateSet);
		tTransferData.setNameAndValue("tLDRiskRateSchema",tLDRiskRateSchema);
		tVData.add(tTransferData);
		tLMSolvencyUI.submitData(tVData,transact);
	}catch(Exception ex){
		Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
	if (FlagStr == "")
  { 
    tError = tLMSolvencyUI.mErrors;
    if (!tError.needDealError())
    {                        

      Content = "操作成功! ";
      FlagStr = "Success";
    }
    else                                                                           
    {
      Content = "操作失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

