var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var turnPage5 = new turnPageClass();
var mEdorNo;
var mEdorType;
var mPolNo;
var mFlag = 0; //核保完毕标志

//业务类型改变时显示相应的信息录入页面
function afterCodeSelect(cCodeName, Field)
{
	if (cCodeName == "PassFlag")
	{
		var passFlag = fm.all("PassFlag").value
		if (passFlag == "4") //附加条件
		{
			document.all("trAddCondition").style.display = "";
			document.all("tdDisagreeDeal1").style.display = "";
			document.all("tdDisagreeDeal2").style.display = "";
			fm.DisagreeDeal.verify = "客户不同意处理|notnull&code:DisagreeDeal";
			showAddPrem();
			showAddSpec();
			showSubMult();
			showSubAmnt();
			if(fm.EdorType.value == "XB")
			{
			  document.all("DisagreeDeal").value= "3";
			  showAllCodeName();
			}
		}
		else
		{
			document.all("trAddCondition").style.display = "none";
			document.all("trAddFee").style.display = "none";
			document.all("trAddSpec").style.display = "none";
			document.all("trSubMult").style.display = "none";
			document.all("trSubAmnt").style.display = "none";
		  document.all("tdDisagreeDeal1").style.display = "none";
			document.all("tdDisagreeDeal2").style.display = "none";
			fm.DisagreeDeal.verify = "";
		}
	}
}

//显示加费信息
function showAddPrem()
{
	if (fm.AddFeeFlag.checked == true)
	{
		document.all("trAddFee").style.display = "";
		queryPrem();
	}
  else
  {
  	document.all("trAddFee").style.display = "none";
  }
}

//显示免责信息
function showAddSpec()
{
	if (fm.SpecFlag.checked == true)
	{
		document.all("trAddSpec").style.display = "";
	}
  else
  {
  	document.all("trAddSpec").style.display = "none";
  }
}

//显示降档信息
function showSubMult()
{
  var sql = "select Mult from LCPol " + 
	          "where PolNo = '" + mPolNo + "' ";
	var result = easyExecSql(sql);
	if (result)
	{
	  fm.initMult.value = result[0][0];
	}
	if (fm.SubMultFlag.checked == true)
	{
		document.all("trSubMult").style.display = "";
		document.all("trSubAmnt").style.display = "none";
		fm.SubAmntFlag.checked = false;
	}
  else
  {
  	document.all("trSubMult").style.display = "none";
  }
}

//显示减额信息
function showSubAmnt() 
{
  var sql = "select Amnt from LCPol " + 
	          "where PolNo = '" + mPolNo + "' ";
	var result = easyExecSql(sql);
	if (result)
	{
	  fm.initAmnt.value = result[0][0];
	}
	if (fm.SubAmntFlag.checked == true)
	{
		document.all("trSubAmnt").style.display = "";
		document.all("trSubMult").style.display = "none";
		fm.SubMultFlag.checked = false;
	}
  else
  {
  	document.all("trSubAmnt").style.display = "none";
  }
}

//查询自核错误信息
function queryUWErr()
{
	var sql = "select (select min(edorname) from lmedoritem a where a.edorcode = EdorType), ContNo, (select PrtNo from LCCont where ContNo = a.ContNo), InsuredName, " +
	          " (select riskcode from LCPol where PolNo = a.PolNo), UWError,UWRuleCode,EdorType  " +
						"from LPUWERROR a " + 
						"where EdorNo = '" + mEdorNo + "' ";
  turnPage.pageLineNum = 100 ;
	turnPage.queryModal(sql, UWErrGrid);
	if (UWErrGrid.mulLineCount > 0)
	{
		if (UWErrGrid.mulLineCount == 1)
		{
		  fm.UWErrGridSel.checked = "true";
		}
		else
		{
		  fm.UWErrGridSel[0].checked = "true";
		}
	}
	fm.EdorType.value = UWErrGrid.getRowColDataByName(0,"EdorType"); //项目类型 
}

//得到待核保的险种信息
function queryUWPol()
{
	var edorNo = fm.EdorNo.value;
	var sql = "select distinct a.PolNo, (select min(edorname) from lmedoritem a where a.edorcode = c.EdorType)," +
						" a.ContNo, a.PrtNo, a.RiskSeqNo, b.riskcode, a.InsuredNo, a.InsuredName, a.Prem, a.Amnt, a.Mult, " +
	          "       a.CValiDate, EndDate - 1 days, PayIntv, PayYears, " +
	          "       case when (select EdorNo from LPUWMaster where EdorNo = '" + mEdorNo + "' " +
	          "                  and EdorType = c.EdorType and PolNo=a.PolNo and AutoUWflag = '2') " +
	          "       is null " +
	          "       then '未核保' else '已核保' end, c.EdorType " +
	          "from LPPol a, LMRisk b ,LPUWError c " + 
	          "where a.RiskCode = b.RiskCode " +
	          "and a.EdorNo = '" + edorNo + "' " +
	          " and c.EdorNo = a.EdorNo " +
	          " and c.Polno = a.Polno "+
//	          "and EdorType <> 'DL' " +
//	          "and exists (select 1 from LPUWError where EdorNo = a.EdorNo and EdorType = a.EdorType and PolNo = a.PolNo) " +
	          " order by RiskSeqNo";
	turnPage2.pageLineNum = 100 ;          
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, PolGrid);
	//ContNo和PrtNo保存第一个保单的信息
	fm.ContNo.value = PolGrid.getRowColData(0, 3); 
	fm.PrtNo.value = PolGrid.getRowColData(0, 4); 
}

//显示保全信息
function showEdorInfo()
{
	document.all("EdorInfo").src = "./ShowEdorInfo.jsp?EdorNo=" + mEdorNo + "&Operate=PRINT";
	if(fm.EdorType.value=='XB')
	{
	  document.all("EdorInfo").height = "0";
	}
	else
	{
	  document.all("EdorInfo").height = "200";
	}
}

function initUWInfo()
{
	document.all("trAddCondition").style.display = "none";
	document.all("trAddFee").style.display = "none";
	document.all("trAddSpec").style.display = "none";
	document.all("trSubMult").style.display = "none";
	document.all("trSubAmnt").style.display = "none";
  document.all("tdDisagreeDeal1").style.display = "none";
	document.all("tdDisagreeDeal2").style.display = "none";
	fm.DisagreeDeal.verify = "";
  fm.AddFeeFlag.checked = false;
  fm.SpecFlag.checked = false;
  fm.SubMultFlag.checked = false;
  fm.SubAmntFlag.checked = false;
  fm.PassFlag.value = "1";
  fm.UWIdea.value = "";
}

//查询核保信息，选择每个项目时触发
function queryUWInfo()
{
  initUWInfo(); 
	var selNo = PolGrid.getSelNo() - 1;
	this.mEdorNo = fm.EdorNo.value;
  this.mPolNo = PolGrid.getRowColData(selNo, 1);
	this.mEdorType = PolGrid.getRowColDataByName(selNo, "EdorType");

	var sql = "select PassFlag, UWIdea, DisagreeDeal, AddPremFlag, " +
	          "       SpecFlag, SubMultFlag, SubAmntFlag, Mult, Amnt " +
	          "from LPUWMaster " +
	          "where EdorNo = '" + mEdorNo + "' " +
	          "and EdorType = '" + mEdorType + "' " +
	          "and PolNo = '" + mPolNo + "' " +
	          "and AutoUWFlag = '2'";
	var result = easyExecSql(sql);
	if (result)
	{
		try 
		{
			fm.PassFlag.value = result[0][0];
			fm.UWIdea.value = result[0][1];
			fm.DisagreeDeal.value = result[0][2];
			var addPremFlag = result[0][3];
			var specFlag = result[0][4];
			var subMultFlag = result[0][5];
			var subAmntFlag = result[0][6];
			fm.InputMult.value = result[0][7];
			fm.InputAmnt.value = result[0][8];
			if (fm.PassFlag.value == "4")
			{
				document.all("trAddCondition").style.display = "";
        document.all("tdDisagreeDeal1").style.display = "";
        document.all("tdDisagreeDeal2").style.display = "";
        fm.DisagreeDeal.verify = "客户不同意处理|notnull&code:DisagreeDeal";
				if (addPremFlag == "1")
				{
					queryPrem();
					fm.AddFeeFlag.checked = true;
        	document.all("trAddFee").style.display = "";
				}
				if (specFlag == "1")
				{
					querySpec();
					document.all("trAddSpec").style.display = "";
	        fm.SpecFlag.checked = true;
				}
				if (subMultFlag == "1")
				{
				  showSubMult();
					document.all("trSubMult").style.display = "";
					fm.SubMultFlag.checked = true;
				}
				if (subAmntFlag == "1")
				{
				  showSubAmnt();
					document.all("trSubAmnt").style.display = "";
					fm.SubAmntFlag.checked = true;
				}
			}
		}
		catch (ex){}
	}
	fm.EdorNo.value = mEdorNo;
	fm.EdorType.value = mEdorType;
	fm.ContNo.value = PolGrid.getRowColData(selNo, 3);
	fm.PolNo.value = mPolNo;
	fm.InsuredNo.value = PolGrid.getRowColData(selNo, 7);
	queryImapart();
	showAllCodeName();
}

//查询加费信息
function queryPrem()
{
	var strSQL = "select Prem, Rate, PayStartDate, PayEndDate from LPPrem " + 
	          "where EdorNo = '" + mEdorNo + "' " +
	          "and EdorType = '" + mEdorType + "' " +
	          "and PolNo = '" + mPolNo + "' " +
	          "and PayPlanCode like '000000%%' ";
	//turnPage3.queryModal(sql, AddFeeGrid);
	turnPage3.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	
	//判断是否查询成功
	if (!turnPage3.strQueryResult) 
	{
	    if(fm.EdorType.value == "XB")
	    {
			var sql = "select CValiDate, EndDate - 1 days from LCPol where PolNo = '" + mPolNo + "' ";
			var result = easyExecSql(sql);
  			if (result)
			{
				try 
				{
					fm.AddFeeGrid3.value = result[0][0];
					fm.AddFeeGrid4.value = result[0][1];
					fm.AddFeeGrid3.disabled = true;
					fm.AddFeeGrid4.disabled = true;
				}
				catch (ex){}
			}
	    }
		return "";
	}
	//查询成功则拆分字符串，返回二维数组
	turnPage3.arrDataCacheSet = decodeEasyQueryResult(turnPage3.strQueryResult);
	
	//设置初始化过的MULTILINE对象
	turnPage3.pageDisplayGrid = AddFeeGrid;    
	//保存SQL语句
	turnPage3.strQuerySql = strSQL; 
	
	//设置查询起始位置
	turnPage3.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage3.arrDataCacheSet, turnPage3.pageIndex, MAXSCREENLINES);
	
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage3.pageDisplayGrid);
	//少儿险核保加费起止日期为保单续保后的生效日到终止日
	if(fm.EdorType.value == "XB")
	{
		var sql = "select CValiDate, EndDate - 1 days from LCPol where PolNo = '" + mPolNo + "' ";
		var result = easyExecSql(sql);
  		if (result)
		{
			try 
			{
				fm.AddFeeGrid3.value = result[0][0];
				fm.AddFeeGrid4.value = result[0][1];
				//fm.AddFeeGrid3.disabled = true;
				//fm.AddFeeGrid4.disabled = true;
			}
			catch (ex){}
		}
	}
}

//查询免责信息
function querySpec()
{
	var sql = "select specCode, specContent, specStartDate, specEndDate, SerialNo " +
	          "from LPSpec " + 
	          "where EdorNo = '" + mEdorNo + "' " +
	          "and EdorType = '" + mEdorType + "' " +
	          "and PolNo = '" + mPolNo + "' ";
	turnPage4.queryModal(sql, SpecGrid);
}

function queryMultAndAmnt()
{	          
  var sql = "select Mult, Amnt from LPPol " + 
	          "where EdorNo = '" + mEdorNo + "' " +
	          "and EdorType = '" + mEdorType + "' " +
	          "and PolNo = '" + mPolNo + "' ";
	var result = easyExecSql(sql);
	if (result)
	{
	  fm.InputMult.value = result[0][0];
	  fm.InputAmnt.value = result[0][1];
	}
}

//体检录入
function healthInput()
{
	//var url = "../uw/UWManuHealthMain.jsp?" +
	//          "MissionID=" + fm.MissionId.value + 
	//          "&SubMissionID=" + fm.SubMissionId.value +
	//          "&ContNo1=" + fm.ContNo.value +
	//          "&PrtNo=" + fm.PrtNo.value;
	var url = "./PEdorUWManuHealthMain.jsp?" +
	        "EdorNo=" + fm.EdorNo.value + 
	        "&AppntNo=" + fm.AppntNo.value +
	        "&ContNo=" + fm.ContNo.value;
	window.open(url);
}

//体检回销
function healthBack()
{
	//var url = "../uw/UWManuHealthQMain.jsp?" +
  //        "MissionID=" + fm.MissionId.value + 
  //        "&SubMissionID=" + fm.SubMissionId.value +
  //        "&ContNo=" + fm.ContNo.value +
  //        "&PrtNo=" + fm.PrtNo.value;
  var prtSeq;
	var sql = "select prtSeq from LPPENotice " +
	          "where EdorNo = '" + fm.EdorNo.value + "' ";
	var result = easyExecSql(sql);
	if (result != null)
	{
		prtSeq = result[0][0];
	}
  var url = "./PEdorUWManuHealthQMain.jsp?" +
	        "EdorNo=" + fm.EdorNo.value + 
	        "&AppntNo=" + fm.AppntNo.value +
	        "&ContNo=" + fm.ContNo.value +
	        "&PrtNo=" + prtSeq;
	window.open(url);
}

//契调录入
function reportInput()
{
	//var url = "../uw/UWManuRReportMain.jsp?" +
  //        "MissionID=" + fm.MissionId.value + 
  //        "&SubMissionID=" + fm.SubMissionId.value +
  //        "&ContNo=" + fm.ContNo.value +
  //        "&PrtNo=" + fm.PrtNo.value +
  //        "&Flag=1";
	var url = "./PEdorUWManuRReportMain.jsp?" +
        "EdorNo=" + fm.EdorNo.value + 
        "&AppntNo=" + fm.AppntNo.value +
        "&ContNo=" + fm.ContNo.value;
	window.open(url);
}

//契调回销
function reportBack()
{
	//var url = "../uw/RReportQueryMain.jsp?" +
  //        "MissionID=" + fm.MissionId.value + 
  //        "&SubMissionID=" + fm.SubMissionId.value +
  //        "&ContNo=" + fm.ContNo.value +
  //        "&PrtNo=" + fm.PrtNo.value;
	var url = "./PEdorRReportQueryMain.jsp?" +
        "EdorNo=" + fm.EdorNo.value + 
        "&AppntNo=" + fm.AppntNo.value +
        "&ContNo=" + fm.ContNo.value;
	window.open(url);
}

       
//问题件录入
function questionInput()
{
	var url = "./PEdorUWManuQuestMain.jsp?" +
        "EdorNo=" + fm.EdorNo.value + 
        "&AppntNo=" + fm.AppntNo.value +
        "&ContNo=" + fm.ContNo.value;
	window.open(url);
}

//问题件回销
function questionBack()
{
	var prtSeq;
	var BQ04_docid;
	var sql = "select max(prtSeq) from LPIssuePol " +
	          "where EdorNo = '" + fm.EdorNo.value + "' ";
	var result = easyExecSql(sql);
	if (result != null)
	{
		prtSeq = result[0][0];
	}
	sql = "select DocID from es_doc_main where DocCode = '" + prtSeq + "' and BussType = 'BQ' and SubType = 'BQ04'";
	result = easyExecSql(sql);
	if (result != null)
	{
		BQ04_docid = result[0][0];
	}   
	var url = "./PEdorUWManuQuestQMain.jsp?" +
        "ContNo=" + fm.ContNo.value +
        "&PrtNo=" + prtSeq +
        "&Docid=" + BQ04_docid +
        "&Flag=1";
	window.open(url);
}


/*********************************************************************
 *  投保书查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function scanQuery()
{
	var arrReturn = new Array();
	var arrReturn1 = new Array();
	var tBussNoType = "";
	var tBussType = "";
    var tSubType = "";
	//获的个人保单印刷号
    
  var tContNo = fm.ContNo.value;
  if (tContNo == "")
  {
    tContNo= trim(fm.all('ContNo').value);
	}
	if (tContNo == "")
	{
	  alert("请选择一个保单！");
	  return false;
	}
		 
  //查询团单记录(撤保或退保)
    var strSql = "select PrtNo from LCGrpCont where GrpContNo='" + tContNo + "'";
  var arrReturn = new Array();
  arrReturn = easyExecSql(strSql);
  
  //查询团单记录(撤保或退保)
    var strSql1 = "select PrtNo from LBGrpCont where GrpContNo='" + tContNo + "'";
  var arrReturn1 = new Array();
  arrReturn1 = easyExecSql(strSql1);
  
  if (arrReturn == null && arrReturn1 ==null ) {
  
        var arrReturn2 = new Array();
  	var strSql2 = "select PrtNo from LCCont where ContNo='" + tContNo + "'";
  	arrReturn2 = easyExecSql(strSql2);
  
          //查询团单记录(撤保或退保)
  	var strSql3 = "select PrtNo from LBCont where ContNo='" + tContNo + "'";
  	var arrReturn3 = new Array();
  	arrReturn3 = easyExecSql(strSql3);
          
  	//ContLoadFlag: 1 个单; 2 团单
  	//ContType: 1 签单; 2 撤保/退保
  	if (arrReturn2 == null && arrReturn3 ==null ) {
  		alert("没有该合同号，请重新输入");
  	} else if(arrReturn3 == null ){	
  		
  		var tProNo = arrReturn2[0][0];
              window.open("../sys/LCProposalScanQuery.jsp?prtNo="+ tProNo + "&SubType=TB1001&BussType=TB&BussNoType=11" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");					
  
  	}else if(arrReturn2 == null ){	
  
  		var tProNo = arrReturn3[0][0];
              window.open("../sys/LCProposalScanQuery.jsp?prtNo="+ tProNo + "&SubType=TB1001&BussType=TB&BussNoType=11" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");				
  	}
  
    } else if(arrReturn == null){
  
  	var tProNo = arrReturn1[0][0];
          window.open("../sys/LCProposalScanQuery.jsp?prtNo="+ tProNo + "&SubType=TB1002&BussType=TB&BussNoType=12" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");				
  }else if(arrReturn1 == null ){	
  	var tProNo = arrReturn[0][0];
          window.open("../sys/LCProposalScanQuery.jsp?prtNo="+ tProNo + "&SubType=TB1002&BussType=TB&BussNoType=12" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");				
  }
}

//显示保全扫描件
function showEdorScan()
{
  var url = "./EasyScanQueryMain.jsp?EdorNo=" + mEdorNo;
	window.open(url, "", "status:no;help:0;close:0");
}

//显示既往核保信息
function showUWInfo()
{
	var selNo = UWErrGrid.getSelNo();
	if (selNo == 0)
	{
		alert("请选择一个保单！");
		return false;
	}
	fm.PrtNo.value = UWErrGrid.getRowColData(selNo - 1, 3);
	var sql = "select ProposalContNo from LCCont " + 
	          "where ContNo = '" + UWErrGrid.getRowColDataByName(selNo - 1, 'contNo') + "' ";
	var result = easyExecSql(sql);
	if (!result)
	{
		alert("未找到合同信息！");
		return false;
	}
	var contNo = result[0][0];
	var url = "../uw/ManuUWInputMain.jsp?ContNo=" + contNo + "&MissionID=null&SubMissionID=null" +
	          "&PrtNo=" + fm.PrtNo.value + "&ActivityID=null&LoadFlag=1";
	window.open(url);
}

//显示保全批单信息
function showEdorPrint()
{
	var url = "./ShowEdorPrint.jsp?EdorAcceptNo=" + mEdorNo;
	window.open(url, "", "status:no;help:0;close:0");
}

//显示保单信息
function showContInfo()
{
	var url = "../app/ProposalEasyScan.jsp?" +
	      "ContNo=" + fm.ContNo.value +
	      "&prtNo=" + fm.PrtNo.value;  //这里&prtNo中p是小写
        "&LoadFlag=6";
	window.open(url, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}

//显示既往投保信息
function historyContInfo()
{
	var url = "../uw/UWAppMain.jsp?" +
				"ContNo=" + fm.ContNo.value +
				"&CustomerNo=" + fm.AppntNo.value +
				"&type=1";
  window.open(url);
}

//显示既往保全信息
function historyEdorInfo()
{
	var url = "../uw/UWEdorApp.jsp?" +
				"ContNo=" + fm.ContNo.value +
				"&CustomerNo=" + fm.AppntNo.value + 
				"&type=2";
	window.open(url);
}

//显示既往理赔信息
function historyClaimInfo()
{
}

function healthImpart()
{
	var selNo = PolGrid.getSelNo() - 1;
	if (selNo == -1)
	{
		alert("请选择一个被保人！");
		return false;
	}
	var edorType = PolGrid.getRowColDataByName(selNo, "EdorType");
	var contNo = PolGrid.getRowColData(selNo, 3);
	var insuredNo = PolGrid.getRowColData(selNo, 7); 
	var url = "./PEdorHealthImpartMain.jsp?" +
				"EdorNo=" + fm.EdorNo.value +
				"&EdorType=" + edorType +
 				"&ContNo=" + fm.ContNo.value +
				"&InsuredNo=" + insuredNo + 
				"&Flag=2";  //只是查看
	window.open(url);
}

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  if(!checkXB())
  {
    return false;  
  }
  if (fm.all("PassFlag").value == "4")
  {
    if ((fm.AddFeeFlag.checked == false) && (fm.SpecFlag.checked == false) &&
        (fm.SubMultFlag.checked == false) && (fm.SubAmntFlag.checked == false))
    {
      alert("请录入附加条件！");
      return false;
    }
    if (fm.AddFeeFlag.checked == true)
    {
      var prem = fm.AddFeeGrid1.value;
      var rate = fm.AddFeeGrid2.value;
      var startDate = fm.AddFeeGrid3.value;
      var endDate = fm.AddFeeGrid4.value;
      if ((prem == "") && (rate == ""))
      {
        alert("请录入加费金额或加费比例！");
        return false;
      }
      if ((prem != "") && (!isNumeric(prem)))
      {
        alert("加费金额应该是正数！");
        return false;
      }
      if ((rate != "") && (!isNumeric(rate)))
      {
        alert("加费比例应该是正数！");
        return false;
      }
      if (startDate == "")
      {
        alert("请录入加费起始日期！");
        return false;
      }
      if (!isDate(startDate))
      {
        alert("加费起始日期格式不正确！");
        return false;
      }
//      if (endDate == "")
//      {
//        alert("请录入加费终止日期！");
//        return false;
//      }
      if ((endDate != "") && (!isDate(endDate)))
      {
        alert("加费终止日期格式不正确！");
        return false;
      }
    }
    if (fm.SpecFlag.checked == true)
    {
      
    }
    if (fm.SubMultFlag.checked == true)
    {
      var mult = fm.InputMult.value;
      if (mult == "")
      {
        alert("请录入降低后档次！");
        return false;
      }
      if ((!isInteger(mult)) || (Number(mult) < 0))
      {
        alert("档次应该是正整数！");
        return false;
      }
    }
    if (fm.SubAmntFlag.checked == true)
    {
      var amnt = fm.InputAmnt.value;
      if (amnt == "")
      {
        alert("请录入降低后保额！");
        return false;
      }
      if ((!isNumeric(amnt)) || (Number(amnt) < 0))
      {
        alert("保额应该是整数！");
        return false;
      }
    }
  }
  return true;
}

//对续保核保结论进行校验
function checkXB()
{
  if(fm.EdorType.value == "XB")
  {
    if(fm.PassFlag.value == "2")
    {
      alert("续保核保不能下发\"终止申请\"核保结论");
      return false;
    }
    if(fm.PassFlag.value == "3")
    {
      alert("续保核保不能下发\"险种解约\"核保结论");
      return false;
    }
    if(fm.PassFlag.value == "4")
    {
      if(fm.DisagreeDeal.value == "1")
      {
        alert("续保核保客户不同意处理不能为\"终止申请\"");
        return false;
      }
      if(fm.DisagreeDeal.value == "2")
      {
        alert("续保核保客户不同意处理不能为\"险种解约\"");
        return false;
      }
      if(fm.SubMultFlag.checked) 
      {
        alert("续保核保附加条件不能为\"降低档次\"");
        return false;
      }
      if(fm.SubAmntFlag.checked)
      {
        alert("续保核保附加条件不能为\"降低保额\"");
        return false;
      }
    }
  }
  
  return true;
}

//保存核保结论
function saveDecision()
{
	var tRow = PolGrid.getSelNo();	
	if (tRow==0)
	{
		alert("请选择需要核保的险种！");
		return false ;
	} 
	//add by fuxin 2009-1-8 万能险不能下：附加条件的核保结论
	if (fm.PassFlag.value =="4")
	{
		var tRow = PolGrid.getSelNo() - 1;	
		var tRiskCode = PolGrid.getRowColData(tRow,6); 
		var SQL =" select 1 From lmriskapp where risktype4='4' and riskcode ='"+tRiskCode+"'"
						+" union "
						+" select 1 From lmriskapp where riskcode in (select code1 from ldcode1 where code ='"+tRiskCode+ "') and risktype4='4'" 
						;
		var arrResult = easyExecSql(SQL, 1, 0);
		if(arrResult)
		{
			alert("万能险不能下：附加条件的核保结论！");
			return false ;
		}
	}
	
	if (fm.PassFlag.value =="5")
	{
		var tRow = PolGrid.getSelNo() - 1;	
		var tRiskCode = PolGrid.getRowColData(tRow,6); 
		var SQL =" select 1 From lmriskapp where risktype4='4' and riskcode ='"+tRiskCode+"'"
						+" union "
						+" select 1 From lmriskapp where riskcode in (select code1 from ldcode1 where code ='"+tRiskCode+ "') and risktype4='4'" 
						;
		var arrResult = easyExecSql(SQL, 1, 0);
		if(arrResult)
		{
			alert("万能险不能下：终止续保的核保结论！");
			return false ;
		}
	}
	
  if (!beforeSubmit())
  {
    return false;
  }
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PEdorUWManuSave.jsp";
	fm.submit(); //提交
}

//确认核保完毕
function confirmDecision()
{
  if (!confirm("确认核保完毕？"))
  {
    return false;
  }
  
  if(fm.EdorType.value == "XB")
  {
    var sql = "select count(1) from LPUWMaster where EdorNo='" + mEdorNo +　"' and passflag = '5' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      if(rs[0][0]== 1)
      {
        alert("续保核保不能只对一个险种终止续保!");
        return false;
      }
    }
  }
  
  mFlag = 1;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PEdorUWManuConfirmSave.jsp";
	fm.submit(); //提交
}

//提交数据后执行的操作
function afterSubmit(flag, content)
{
	showInfo.close();
	window.focus();
	if (flag == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	if (mFlag == 0)
	{
	  queryUWPol(); //更新显示核保状态
	}
  else
  {
  	if (flag == "Succ")
  	{
	  	top.window.close();
	    top.opener.window.focus();
	    top.opener.window.location.reload();
	  }
  }
}

//创建工作流节点
function makeWorkFlow()
{
	fm.action = "./PEdorUWManuWorkFlow.jsp";
  fm.submit();
}

//查询录入告知				
function queryImapart()
{
	var sql = "select ImpartContent from LPCustomerImpart " +
	          "where EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value + "' " +
	         	"and ContNo = '" + fm.ContNo.value + "' " +
	         	"and CustomerNo = '" + fm.InsuredNo.value + "' " +
	          "and ImpartVer = '101' ";
	var arrResult = easyExecSql(sql);
	if (arrResult)
	{
		for (i = 0; i < arrResult.length; i++)
		{
			fm.ImpartContent[i].value = arrResult[i][0];
		}
		document.all("divImpart").style.display = "";
		document.all("impartImg").src = "../common/images/butExpand.gif";
	}
	else
	{
		document.all("divImpart").style.display = "none";
		document.all("impartImg").src = "../common/images/butCollapse.gif";
	}
	
	var sql = "select ImpartParam from LPCustomerImpartParams " +
          "where EdorNo = '" + fm.EdorNo.value + "' " +
          "and EdorType = '" + fm.EdorType.value + "' " +
         	"and ContNo = '" + fm.ContNo.value + "' " +
          "and ImpartVer = '101' " +
          "and CustomerNo = '" + fm.InsuredNo.value + "' " +
          "order by ImpartCode, ImpartParamNo";
  var arrResult = easyExecSql(sql);
  if (arrResult)
  {
		fm.Detail1[arrResult[0][0]].checked = true;
		fm.Detail2[0].value = arrResult[1][0];
		fm.Detail2[parseInt(arrResult[2][0]) + 1].checked = true;
		fm.Detail3[arrResult[3][0]].checked = true;
		fm.Detail4[arrResult[4][0]].checked = true;
		fm.Detail5[arrResult[5][0]].checked = true;
		fm.Detail6[arrResult[6][0]].checked = true;
  }
}

//查询人工核保结论
function queryManuUW() 
{
	var sql = "select EdorType, EdorType, ContNo, PolNo, " +
	          "       (select RiskSeqNo from LCPol where PolNo = LPUWMaster.PolNo) as RiskSeqNo, " +
	          "       (select riskCode from LCPol where PolNo = LPUWMaster.PolNo), InsuredNo, InsuredName, " +
	          "       SugUWIdea, AddPremFlag, SpecFlag, SubMultFlag, UWIdea, MakeDate, " +
	          "       case when DisagreeDeal = '1' then '终止申请' when DisagreeDeal = '2' then '终止险种效力' end , " +
	          "       case when CustomerReply = '1' then '同意' when CustomerReply = '2' then '不同意' end "+
						"from LPUWMaster " +
						"where EdorNo = '" + mEdorNo + "' " +
						"and AutoUWFlag = '2'" +
						"order by RiskSeqNo";
	turnPage5.pageLineNum = 100 ; 
	turnPage5.pageDivName = "divPage5";
	turnPage5.queryModal(sql, ManuUWGrid);
}

//续保核保暂时不显示按钮层
function showButtonDiv()
{
	if(fm.EdorType.value=='XB')
	{
	  document.all("divButton").style.display = "none";
	}
	else
	{
	  document.all("divButton").style.display = "";
	}
}
