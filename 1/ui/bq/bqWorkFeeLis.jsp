<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：bqWorkFeeLis.jsp
//程序功能：保单管理收付清单
//创建日期：2005-10-10
//创建人  ：yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   
<script>
	var comCode = "<%=tG.ComCode%>";
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="bqWorkFeeLis.js"></SCRIPT> 

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>保单管理收付清单</title>
</head>
<body onload="initElementtype()">   
	<table>
			<tr> 
			  <td class= common> 
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, bqWorkFeeLis);"> 
				</td>
				<td class= titleImg>保单管理收付清单</td>
			</tr>
	</table>
	<Div  id= "bqWorkFeeLis" style= "display: ''" >  
			<form action="./bqWorkFeeLisPrint.jsp" method=post name=fm target="f1print">
				<INPUT VALUE="查  询" class="cssButton" TYPE="hidden" onclick="submitForm()">&nbsp;
				<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()"> <br><br><font size="2" color="#ff0000">统计起期和统计止期均按照保全结案日期进行统计</font><br><br> 
				<table class= common border=0 width=100%>		
		       <TR  class= common> 

								<TD  class= title> 机构</TD><TD  class= input>
								<Input class= "codeno"  name=organcode  readonly ondblclick="return showCodeList('comcode',[this,organname],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,organname],[0,1],null,null,null,1);" ><Input class=codename  name=organname  ></TD>
								
								<TD  class= title> 小组</TD><TD  class= input>
								<Input class= "codeno"  name=Groupno  readonly ondblclick="return showCodeList('acceptcom',[this,Groupname],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('acceptcom',[this,Groupname],[0,1],null,null,null,1);" ><Input class=codename  name=Groupname  ></TD>

								<TD  class= title> 组员 </TD><TD  class= input>
								<Input class= "codeno"  name=usercode  readonly ondblclick="return showCodeList('groupmember',[this,username],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('groupmember',[this,username],[0,1],null,null,null,1);" ><Input class=codename  name=username  ></TD>
		         </TR>
		         
		      	<TR  class= common>						
						<TD  class= title8>收付类型</TD>
						<TD  class= input8> <input class=codeno name=feeType   CodeData="0|^1|收费^0|付费" ondblclick="return showCodeListEx('feeType',[this,feeTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('feeType',[this,feeTypeName],[0,1],null,null,null,1);"><input class=codename name=feeTypeName></TD>


						<TD  class= title8>收付状态</TD>
						<TD  class= input8> <input class=codeno name=feeState   readonly CodeData="0|^1|未完成^0|结束" ondblclick="return showCodeListEx('feeState',[this,feeStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('feeState',[this,feeStateName],[0,1],null,null,null,1);"><input class=codename name=feeStateName></TD>
							
						</TR>		
						<TR  class= common>	
 							
							<TD  class= title>统计起期</TD>
								<TD  class= input> 
								<Input name=StartDate  style="width:160" class='coolDatePicker' dateFormat='short' verify="统计起期|Date"  elementtype=nacessary  > </TD> 
								<TD  class= title>统计止期</TD>
								<TD  class= input > 
								<Input name=EndDate style="width:160" class='coolDatePicker' dateFormat='short' verify="统计止期|Date" elementtype=nacessary  > </TD> 
							<TD colspan=2> 
    						 <INPUT VALUE="重 置" TYPE=button style="width:100"Class="cssButton" name="query" onclick="lcReload();">  
   							 </TD>	 	
						</TR>
			</table>
			  <input type="hidden" name=op value="">
			  <input type="hidden" name=sql value="">
			</form>
		 </Div>  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 