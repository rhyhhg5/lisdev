<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
	<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">
	<%@include file="LLWSCertifyPInit.jsp"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>	
	<script src="LLWSCertifyP.js"></script> 
	
<title>卡折被保人实名化</title>
</head>
<body  onload="initForm();">
  <form action="" method=post name=fm target="fraSubmit">
	<table>
            <tr>
                <td class="titleImg">保单信息</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">管理机构</td>
                    <td class="input8"><input class="codeNo" name="ManageCom" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><input class="codename" name="ManageComName" readonly="readonly" ></td>
                    <td class="title8">保单号</td>
                    <td class="input8"><input class="common" name="ContNo" readonly></td>
                </tr>
                <tr class="common">
                    <td class="title8">主被保人姓名</td>
                    <td class="input8"><input class="common" name="InsuredName" readonly></td>
                    <td class="title8">主被保人性别</td>
                    <td class="input8"><input class="common" name="InsuredSex" readonly></td>
                    <td class="title8">主被保人出生日期</td>
                    <td class="input8"><input class="common" name="InsuredBirthDay" readonly></td>
                </tr>
                <tr class="common">
                    <td class="title8">证件类型</td>
                    <td class="input8"><Input  onClick="showCodeList('idtype',[this,tIDTypeName],[0,1]);" onkeyup="showCodeListKeyEx('idtype',[this,tIDTypeName],[0,1]);" class=codeno name="tIDType" readonly><Input class= codename name=tIDTypeName ></td>
                    <td class="title8">证件号码</td>
                    <td class="input8"><Input class=common name="tIDNo" readonly></td>
                </tr>
            </table>
        </div>
	 <br>
   <table>
     <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
      </td>
      <td class= titleImg>
        新增被保人清单
      </td>
    </tr>
   </table>
    <Div id= "divLPInsured" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanInsuredListGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage" align=center style="display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
      </Div>	
  	</div>	 
  	<br>
  <table>
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divFail);">
      </td>
      <td class= titleImg>无效数据</td>
    </tr>
   </table>
    <Div id= "divFail" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanFailGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage2" align=center style= "display: 'none'">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
      </Div>	
  	</div>
  	<br>
	<Div  id= "divSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="doImport" class=cssButton value="磁盘导入"  onclick="importInsured();">
	   		 <input type=Button name="save" class= cssButton value="确  认" onclick="saveEdor();">
	   		 <input type=Button name="goBack" class= cssButton value="返  回" onclick="returnParent();">
			</TR>
	 	</table> 
	</Div>
	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="ContType" name="ContType">
	<input type=hidden id="ContNo" name="ContNo">
	<input type=hidden id="EdorValiDate" name="EdorValiDate">
	<input type=hidden id="InsuredId" name="InsuredId">
  </form>
</body>
</html>