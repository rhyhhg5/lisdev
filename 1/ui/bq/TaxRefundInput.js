var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  easyQueryClick();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

//提交前的校验、计算  
function beforeSubmit()
{
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{ 
  var year = fm.Year.value;
  var year2 = Number(year) + 1;
  var   strSQL = "select a.AppntNo,a.ContNo,"+year+",manageCom," 
               + "a.CValiDate,a.CInValiDate,"
               + "nvl((select sum(money) from LCInsureAccTrace where Contno=a.Contno and MoneyType='BR' and year(PayDate)='"+year2+"'),0) "
               + "from Lccont a " 
               + "where 1=1 and appFlag='1' and contType='1' " 
               + "and ManageCom like '"+ fm.ManageCom.value + "%' "
               + "and exists (select 1 from LCPol lcp where contno=a.contno and exists (select RiskCode from LMRiskApp where RiskCode=lcp.RiskCode and TaxOptimal='Y'))"
  			   + getWherePart( 'a.AppntNo','CustomerNo') 
  			   + getWherePart( 'a.ContNo','ContNo') 
               + " with ur";
             
  turnPage1.queryModal(strSQL, TaxRefundGrid); 
}

//点击查询
function queryClick()
{
	//得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构!");
	  return false;
	}
	//校验保单号不能为空
	if(fm.ContNo.value == "" && fm.CustomerNo.value == "") {
		alert("请输入保单号或者客户号!");
		return false;
	}
	//校验年度
	if(fm.Year.value == "") {
		alert("年度不能为空!");
		return false;
	}
	//校验年度是否为四位数字
	var reg = /^\d{4}$/;
	if(reg.test(fm.Year.value)==false) {
		alert("请输入四位整数的年度!");
		return false;
	}
	//查询SQL
	easyQueryClick();
	//如果没有查询记录给用户提示
   if(TaxRefundGrid.mulLineCount == 0)
   {
     alert("没有符合条件的查询信息!");
     return false;
   }
}

//点击打印
function printClick()
{
	//MulLine中的行数
	 var tSel = TaxRefundGrid.getSelNo();
	 if (tSel == 0 || tSel == null) {
		alert("请先选择一条记录再打印!");
	}else{//单打印
		//选中的某一行的行号
		var tRow = TaxRefundGrid.getSelNo() - 1;
		//选中记录中的保单号
		var tContNo = TaxRefundGrid.getRowColData(tRow,2);
		//选中记录中的年度
		var tPolYear = TaxRefundGrid.getRowColData(tRow,3);
		//选中记录中的返还金额
	    var tReturnMoney = TaxRefundGrid.getRowColData(tRow,7);
	    //校验如果返还金额为0则不允许打印
	    if (tReturnMoney == 0) {
			alert("该保单本年度没有返还,不能打印！");
		}else{
			var showStr="正在准备打印数据，请稍后...";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.target = "fraSubmit";
			fm.action="./TaxRefundPrintSave.jsp?Code=SY001&StandbyFlag2="+tContNo+"&StandbyFlag1="+tPolYear;
			fm.submit();
		}
	}
}
