<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DiskImportZT.jsp
//程序功能：磁盘导入
//创建日期：2005-10-08
//创建人 ：Yang Yalin
//更新记录： 更新人  更新日期   更新原因/内容
%>
<html>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="./DiskImportZT.js"></SCRIPT>
<%@include file="DiskImportZTInit.jsp"%>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>磁盘导入</title>
</head>
<body onload="initForm();">
	<form name="fm" action="./DiskImportZTSave.jsp?edorNo=<%=request.getParameter("EdorNo")%>&grpContNo=<%=request.getParameter("GrpContNo")%>&edorType=<%=request.getParameter("EdorType")%>" method=post target="fraSubmit" ENCTYPE="multipart/form-data">
		<input type="hidden" name="edorNo">
		<input type="hidden" name="grpContNo">
		<input type="hidden" name="edorType">
		<br><br>
		<table class=common>
			<TR>
				<TD width='15%' style="font:9pt">文件名</TD>
				<TD width='85%'>
					<Input type="file" width="100%" name="FileName">
					<INPUT VALUE="导  入" class="cssButton" TYPE=button onclick="importFile();">
				</TD>
			</TR>
		</table>
		<br><br>
		<table class=common>
			<TR align="center">
				<TD id="Info" width='100%' style="font:10pt"></TD>
			</TR>
		</table>
	</form>
</body>
</html>