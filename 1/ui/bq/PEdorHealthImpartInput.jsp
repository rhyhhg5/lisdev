<html> 
<%   
//程序名称：
//程序功能：保障保费调整
//创建日期：2005-12-30
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorHealthImpart.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorHealthImpartInit.jsp"%>
  <title>保障保费调整</title>
</head>
<body onload="initForm();">
<span id="show"></span>
  <form action="./PEdorHealthImpartSave.jsp" method=post name=fm target="fraSubmit">
     <table class=common>
   		<tr class= common>
	      <td class="title"> 受理号 </td>
	      <td class=input>
	        <input class="readonly" type="text" readonly name=EdorNo >
	      </td>
	      <td class="title"> 批改类型 </td>
	      <td class="input">
	      	<input class="readonly" type="hidden" readonly name=EdorType>
	      	<input class="readonly" readonly name=EdorTypeName>
	      </td>
	      <td class="title"> 保单号 </td>
	      <td class="input">
	      	<input class = "readonly" readonly name=ContNo>
	      </td>   
    	</tr>
  	</table>
  	<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divInsured);">
	      </td>
	      <td class= titleImg>
	        被保人信息
	      </td>
		  </tr>
		</table>
		<div id= "divInsured" style= "display: ''">
	      <table  class= common>
	      	<tr  class= common>
	      		<td  class= title>
	            客户号
	          </td>
	          <td class= input>
	            <Input class= "readonly" readonly name=InsuredNo>
	          </td>
	          <td  class= title>
	            姓名
	          </td>
	          <td class= input>
	            <Input class= "readonly" readonly name=InsuredName>
	          </td>
			      <td class= title>
			        性别
			      </td>
			      <td  class= input>
			         <Input class= "readonly" name=Sex readonly ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
			      </td>
		      </tr>
			    <tr  class= common>
			    	<td  class= title>
			        出生日期
			      </td>
			      <td  class= input>
			      	<Input class= "readonly" readonly name=Birthday>
			      </td>
			      <td  class= title>
		           证件类型
		        </td>
		        <td  class= input>
		          <Input class= "readonly" readonly name="IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
		        </td>
		        <td  class= title>
		           证件号码
		        </td>
		        <td  class= input>
		          <Input class= "readonly" readonly name="IDNo"  >
		        </td>  
		      </tr>
		    </table>
	   </div>
  	<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divImpart);">
	      </td>
	      <td class= titleImg>
	        健康告知
	      </td>
		  </tr>
		</table>
		<div id= "divImpart" style= "display: ''">
      <table width="942" border="0" align="center" cellpadding="0" cellspacing="0" class="muline">
        <tr>
					<td colspan="2" class="mulinetitle2" height="20">问卷</td>
				</tr>
  			<tr height="25"> 
			    <td class="mulinetitle3">
			    	<input type="hidden" name="ImpartCode" value="001">
			    	<input type="hidden" name="ImpartParamName1" value="PayMode">
			    	&nbsp;1. 目前您医疗费用支付方式：
			    	<input type="radio" name="Detail1" value="0">社会医疗保险
			    	<input type="radio" name="Detail1" value="1">商业医疗保险
			    	<input type="radio" name="Detail1" value="2">自费
			    	<input type="radio" name="Detail1" value="3">其他（请详细说明）
			    </td>
			    <td class="mulinetitle3" width="300">
			    	详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="002">
			  		<input type="hidden" name="ImpartParamName2" value="Income">
			  		<input type="hidden" name="ImpartParamName2" value="Source">
			  		&nbsp;2.您目前收入<input name="Detail2" class = "common66">万元，主要来源为：
			  		<input type="radio" name="Detail2" value="0">薪资
			  		<input type="radio" name="Detail2" value="1">营业收入
			  		<input type="radio" name="Detail2" value="2">房屋出租
			  		<input type="radio" name="Detail2" value="3">证券投资
			  		<input type="radio" name="Detail2" value="4">银行利息
			  		<input type="radio" name="Detail2" value="5">其他（请详细说明）
			  	</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="003">
						&nbsp;3.（<input type="radio" name="Detail3" value="0">是<input type="radio" name="Detail3" value="1">否）  您曾否投保过医疗险、重大疾病险、意外险或寿险？ 如“是”，请就投保公司、险种、保额作详细说明。
			  	</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="004">
						&nbsp;4.（<input type="radio" name="Detail4" value="0">是<input type="radio" name="Detail4" value="1">否）  您是否有投保本保单前未告知的疾病、残疾及器官缺失或功能不全？如有，请详细说明。
					</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="005">
						&nbsp;5.（<input type="radio" name="Detail5" value="0">是<input type="radio" name="Detail5" value="1">否）  自投保至今，您是否有身体不适、检查结果异常、就诊或被诊断为某种疾病？如有，清详细说明。
					</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			  <tr height="25"> 
			  	<td class="mulinetitle3">
			  		<input type="hidden" name="ImpartCode" value="006">
						&nbsp;6.（<input type="radio" name="Detail6" value="0">是<input type="radio" name="Detail6" value="1">否）  自投保至今您是否发生过保险赔付（包括其他公司），或正准备申请赔付？如有，请详细说明。
					</td>
			  	<td class="mulinetitle3" width="300">
			  		详细说明<input name="ImpartContent" class = "common66" style="width:250">
			    </td>
			  </tr>
			</table>
		</div>
		<br><br>
		<%
			String flag = request.getParameter("Flag");
			if ((flag != null) && (flag.equals("1")))
			{
		%>
		<div  id= "divSubmit" style= "display:''">
		  <table class = common>
				<tr class= common>&nbsp;&nbsp;
		   	 	<input type=Button name="save" class= cssButton value="保  存" onclick="saveImpart();">
		   		<input type=Button name="goBack" class= cssButton value="返  回" onclick="returnParent();">
				</tr>
		 	</table> 
		</div>
		<%
			}
		%>
  </form>
</body>
</html>