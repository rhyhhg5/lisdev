<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：CashValuePrintSave.jsp
//程序功能：现金价值表打印
//创建日期：2005-09-27 16:20:22
//创建人  ：liurx
//更新记录：  更新人    更新日期      更新原因/内容 
%>
<!--用户校验类-->
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.io.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="org.jdom.input.DOMBuilder"%>
<%@page import="org.jdom.Document"%>
<%@page import="com.f1j.util.F1Exception"%>
<%@page import="com.f1j.ss.BookModelImpl"%>
<%
  System.out.println("开始执行打印操作");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  String ContNo=request.getParameter("ContNo");	
  
if(ContNo == null || ContNo.trim().equals(""))
{
    FlagStr = "Fail";
    Content = "请输入保单号";  
}
else
{
  LCPolSet tLCPolSet = new LCPolSet();
  LCPolDB tLCPolDB = new LCPolDB();
  tLCPolDB.setContNo(ContNo);
  tLCPolDB.setAppFlag("1");
  tLCPolSet = tLCPolDB.query();
  if (tLCPolSet == null || tLCPolSet.size() < 1)
  {
      FlagStr = "Fail";
      Content = "无保单险种信息！";  
  }
  else
  {
    //Get the path of VTS file from LDSysVar table	
    LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    //获取临时文件名
    String strSql1 = "select * from ldsysvar where Sysvar='VTSFilePath'";
    LDSysVarSet tLDSysVarSet = tLDSysVarDB.executeQuery(strSql1);    
    LDSysVarSchema tLDSysVarSchema = tLDSysVarSet.get(1);
    String strFilePath = tLDSysVarSchema.getV("SysVarValue");
    String strVFFileName2 = strFilePath + FileQueue.getFileName()+".vts";
    String strVFFileName1 = strFilePath + FileQueue.getFileName();
    
    //获取存放临时文件的路径
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strName = strRealPath + strVFFileName2;
    
    String strTemplatePath = "";

    int tCount = tLCPolSet.size();
    String[] strVFPathName = new String[2*tCount];
    int len = 0;
    //主险现金价值
    for(int i = 1;i<=tLCPolSet.size();i++)
    {
    	 if(tLCPolSet.get(i).getPolNo().trim().equals(tLCPolSet.get(i).getMainPolNo().trim()))
    	 {
             CashValueTablePrintUI tCashValueTablePrintUI = new CashValueTablePrintUI();            
             TransferData tTransferData = new TransferData();
             tTransferData.setNameAndValue("ContNo",ContNo);
             tTransferData.setNameAndValue("Len",String.valueOf(len));
             tTransferData.setNameAndValue("PolNo",tLCPolSet.get(i).getPolNo());
             VData tVData = new VData();
             tVData.add(tG);
             tVData.add(tTransferData);
    		 if(tCashValueTablePrintUI.submitData(tVData,"MAIN&CASH"))
    		 {
    		     CombineVts tcombineVts=null;
    		     VData mResult = tCashValueTablePrintUI.getResult();	
	             XmlExport txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	             if(txmlExport!=null)
	             {
                       //合并VTS文件 
                      strTemplatePath = application.getRealPath("f1print/NCLtemplate/") + "/";
                      tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
                      ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                      tcombineVts.output(dataStream);
                      
                       //把dataStream存储到磁盘文件
                      strVFPathName[len] = "CashValueTable"+String.valueOf(len) + ".vts";
                      AccessVtsFile.saveToFile(dataStream,strVFPathName[len]);
                      len++;  
                 }
    		  }
    	   }
    }
    //附件险现金价值
    for(int i = 1;i<=tLCPolSet.size();i++)
    {
    	 if(!tLCPolSet.get(i).getPolNo().trim().equals(tLCPolSet.get(i).getMainPolNo().trim()))
    	 {
             CashValueTablePrintUI tCashValueTablePrintUI = new CashValueTablePrintUI();             
             TransferData tTransferData = new TransferData();
             tTransferData.setNameAndValue("ContNo",ContNo);
             tTransferData.setNameAndValue("Len",String.valueOf(len));
             tTransferData.setNameAndValue("PolNo",tLCPolSet.get(i).getPolNo());
             VData tVData = new VData();
             tVData.add(tG);
             tVData.add(tTransferData);
    		 if(tCashValueTablePrintUI.submitData(tVData,"SUB&CASH"))
    		 {
    		     CombineVts tcombineVts=null;
    		     VData mResult = tCashValueTablePrintUI.getResult();	
	             XmlExport txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	             if(txmlExport!=null)
	             {
                       //合并VTS文件 
                      strTemplatePath = application.getRealPath("f1print/NCLtemplate/") + "/";
                      tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
                      ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                      tcombineVts.output(dataStream);
                      
                       //把dataStream存储到磁盘文件
                      strVFPathName[len] = "CashValueTable"+String.valueOf(len) + ".vts";
                      AccessVtsFile.saveToFile(dataStream,strVFPathName[len]);
                      len++;  
                 }
    		  }
    	   }
      }
    //主险红利保额现金价值
    for(int i = 1;i<=tLCPolSet.size();i++)
    {
    	 if(tLCPolSet.get(i).getPolNo().trim().equals(tLCPolSet.get(i).getMainPolNo().trim()))
    	 {
             CashValueTablePrintUI tCashValueTablePrintUI = new CashValueTablePrintUI();            
             TransferData tTransferData = new TransferData();
             tTransferData.setNameAndValue("ContNo",ContNo);
             tTransferData.setNameAndValue("Len",String.valueOf(len));
             tTransferData.setNameAndValue("PolNo",tLCPolSet.get(i).getPolNo());
             VData tVData = new VData();
             tVData.add(tG);
             tVData.add(tTransferData);
    		 if(tCashValueTablePrintUI.submitData(tVData,"MAIN&BONUS"))
    		 {
    		     CombineVts tcombineVts=null;
    		     VData mResult = tCashValueTablePrintUI.getResult();	
	             XmlExport txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	             if(txmlExport!=null)
	             {
                       //合并VTS文件 
                      strTemplatePath = application.getRealPath("f1print/NCLtemplate/") + "/";
                      tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
                      ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                      tcombineVts.output(dataStream);
                      
                       //把dataStream存储到磁盘文件
                      strVFPathName[len] = "CashValueTable"+String.valueOf(len) + ".vts";
                      AccessVtsFile.saveToFile(dataStream,strVFPathName[len]);
                      len++;  
                 }
    		  }
    	   }
    }
    //附件险红利保额现金价值
    for(int i = 1;i<=tLCPolSet.size();i++)
    {
    	 if(!tLCPolSet.get(i).getPolNo().trim().equals(tLCPolSet.get(i).getMainPolNo().trim()))
    	 {
             CashValueTablePrintUI tCashValueTablePrintUI = new CashValueTablePrintUI();             
             TransferData tTransferData = new TransferData();
             tTransferData.setNameAndValue("ContNo",ContNo);
             tTransferData.setNameAndValue("Len",String.valueOf(len));
             tTransferData.setNameAndValue("PolNo",tLCPolSet.get(i).getPolNo());
             VData tVData = new VData();
             tVData.add(tG);
             tVData.add(tTransferData);
    		 if(tCashValueTablePrintUI.submitData(tVData,"SUB&BONUS"))
    		 {
    		     CombineVts tcombineVts=null;
    		     VData mResult = tCashValueTablePrintUI.getResult();	
	             XmlExport txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	             if(txmlExport!=null)
	             {
                       //合并VTS文件 
                      strTemplatePath = application.getRealPath("f1print/NCLtemplate/") + "/";
                      tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
                      ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                      tcombineVts.output(dataStream);
                      
                       //把dataStream存储到磁盘文件
                      strVFPathName[len] = "CashValueTable"+String.valueOf(len) + ".vts";
                      AccessVtsFile.saveToFile(dataStream,strVFPathName[len]);
                      len++;  
                 }
    		  }
    	   }
      }
      strVFPathName = BqNameFun.getNotNullArr(strVFPathName);
      if(len>0)
      {
            VtsFileCombine vtsfilecombine = new VtsFileCombine();
            BookModelImpl tb = vtsfilecombine.dataCombine(strVFPathName);
            try
            {  
               System.out.println("=================" + strName);
               vtsfilecombine.write(tb, strName);
            }
            catch (IOException ex)
            {
            }
            catch (F1Exception ex)
            {
            }
            session.putValue("RealPath", strName);
            //response.sendRedirect("../f1print/GetF1PrintJ1.jsp");
            request.getRequestDispatcher("../f1print/GetF1PrintJ1.jsp").forward(request,response);
      }
      else
      {
           FlagStr = "Fail";
           Content = "打印失败！";   
      }
  }
}
	if( !Content.equals("") ) {
		System.out.println("outputStream object has been open");
%>
<html>
<script language="javascript">
	alert("<%=Content%>");
	top.opener.focus();
	top.close();
</script>
</html>
<%
	}
%>