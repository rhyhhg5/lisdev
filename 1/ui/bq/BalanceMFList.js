var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}

//打印按钮对应操作
function submitForm()
{
  if(fm.Sql.value == "")
  {
    alert("请先查询!");
    return false;
  }
  if(AccMFGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据!");
    return false;
  }
  fm.action = "BalanceMFListSave.jsp";
  fm.submit(); //提交
}

//下载按钮对应操作
function downloadClick()
{
  if(fm.Sql.value == "")
  {
    alert("请先查询!");
    return false;
  }
  if(AccMFGrid.mulLineCount == 0)
  {
    alert("没有需要下载的数据!");
    return false;
  }
  fm.action = "BalanceDownLoad.jsp";
  fm.submit(); //提交
}   

function  queryList() 
{
  if(!checkData())
  {
    return false;
  }
  var tStartDate = formatMonth(trim(fm.PolMonthStart.value));
  if (!formatTime(tStartDate))
  {
    alert("结算日期起录入错误！");
    return false;
  }
  var tEndDate = formatMonth(trim(fm.PolMonthEnd.value));
  if (!formatTime(tStartDate))
  {
    alert("结算日期止录入错误！");
    return false;
  }
  var strSql = "select (select name from ldcom where comcode = b.ManageCom), "
             + "   b.ContNo, b.AppntName, year(DueBalaDate - 1 days), month(DueBalaDate - 1 days), "
             + "   nvl((select abs(sum(Money)) from LCInsureAccTrace where OtherType = '6' and MoneyType = '" + tLoadFlag + "' and OtherNo = c.SequenceNo),0), "
             + "   (c.DueBalaDate - 1 day), e.Name,getUniteCode(e.AgentCode), (select Name from LABranchGroup where AgentGroup = b.AgentGroup) bragrp, "
             + "   (select min(Mobile) from LCAppnt x, LCAddress y where x.AppntNo = y.CustomerNo and x.AddressNo = y.AddressNo and x.ContNo = b.ContNo) "
             + "from LCCont b, LCInsureAccBalance c, LAAgent e " 
             + "where 1 = 1 "
             + "and b.managecom like '" + fm.ManageCom.value + "%' "
             + getWherePart('c.RunDate', 'RunDateStart','>=')
             + getWherePart('c.RunDate', 'RunDateEnd','<=')
             + getWherePart('b.SaleChnl', 'SaleChnl','=')
             + getWherePart('b.ContNo', 'ContNo')
             + getWherePart('b.AppntName', 'AppntName', '=')
             + "   and b.ContNo = c.ContNo "
             + "   and b.AgentCode = e.AgentCode "
             + "   and DueBalaDate - 1 month between '"+tStartDate+"' and '"+tEndDate+"' " 
             + "   and c.BalaCount > 0 order by b.ManageCom ,bragrp, e.AgentCode";
               
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(strSql, AccMFGrid);
  fm.Sql.value = strSql;
  
  if(AccMFGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }
  return true;
}

function checkData()
{ 
  if(!verifyInput2())
  {
    return false;
  }
  
  return true;
}

//回车查询
function queryByKeyDown()
{
  if(event.keyCode == "13")
  {
	queryList();
  }
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

function formatMonth(date)
{
  var baladate = "";
  var year = date.substring(0,4);
  var month = date.substring(4,6);
  var baladate = year+"-"+month+"-01";
  return baladate;
}

function formatTime(str)
{
  var r = str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);     
  if(r==null) return false;     
  var d= new Date(r[1],r[3]-1,r[4]);     
  return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);   
}


