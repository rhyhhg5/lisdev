<%
//程序名称：LGGroupTestQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-22 17:32:49
//创建人  ：hyx
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    
<%
   	GlobalInput tGI = (GlobalInput) session.getValue("GI");	
%>              
<script language="JavaScript">
function initInpBox()
{     
    fm.all('GroupNo').value = "";
    fm.all('GroupName').value = "";
    fm.all('GroupInfo').value = "";
    fm.all('WorkTypeNo').value = ""; 
}       
                             
function initForm() 
{
  try 
  {
 // initInpBox();
    initInureGrid();  
 // queryNoInureCont();
  }
  catch(re) 
  {
    alert("操作错误！请关掉上次登陆打开的页面");
  }
}

//领取项信息列表的初始化
//var InureGrid;
function initInureGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="0px";         		
    iArray[0][3]=3;         		    
    iArray[0][4]="station"; 
    
    iArray[1]=new Array();
    iArray[1][0]="团体保单号";         	  //列名
    iArray[1][1]="50px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="保单号";         	
    iArray[2][1]="100px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="投保人姓名";         	  //列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="保单生效日";      //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[5]=new Array();
    iArray[5][0]="保单满期日";              //列名
    iArray[5][1]="80px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="保单类型";         	
    iArray[6][1]="10px";            	           		 
    iArray[6][3]=3; 
  
    InureGrid = new MulLineEnter("fm", "InureGrid"); 
    //设置Grid属性
    InureGrid.mulLineCount = 0;
    InureGrid.displayTitle = 1;
    InureGrid.locked = 1;
    InureGrid.canSel = 1;
    InureGrid.canChk = 0;
    InureGrid.hiddenSubtraction = 1;
    InureGrid.hiddenPlus = 1;
    InureGrid.selBoxEventFuncName = "selReadio";
    InureGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
