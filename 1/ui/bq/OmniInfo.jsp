<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：OmniInfo.jsp
//程序功能：万能保单事项界面
//创建日期：2009-6-15
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<%
  String tContNo = request.getParameter("ContNo");
  String tPolNo = request.getParameter("PolNo");
  String tInsuAccNo = request.getParameter("InsuAccNo");
  String tRiskCode = request.getParameter("RiskCode");
%>
<script>
var mContNo = "<%=tContNo%>"; 
var mPolNo = "<%=tPolNo%>"; 
var mInsuAccNo = "<%=tInsuAccNo%>";
var mRiskCode = "<%=tRiskCode%>";
function returnParent()
{
  try
  {
    if(top.opener != undefined)
    {
      top.opener.focus();
      top.close();
    }
  }
  catch(ex)
  {}
}

function initForm()
{
  fm.all('ContNo').value = mContNo;
  var sql = "select polapplydate from lccont where contno = '" + mContNo + "' ";
  var result = easyExecSql(sql);
  if(result && result[0][0] != "null")
  {
    fm.all('PolApplyDate').value = result[0][0];
  }
  var sql1 = "select insuaccbala from lcinsureacc where polno = '" + mPolNo + "' and insuaccno = '" + mInsuAccNo + "' ";
  var result1 = easyExecSql(sql1);
  if(result1 && result1[0][0] != "null")
  {
    fm.all('InsuAccBala').value = result1[0][0];
  }
  var sql2 = "select ltrim(rtrim(char(year(DueBalaDate - 1 day))))||'-'||ltrim(rtrim(char(month(DueBalaDate - 1 day)))) "
           + "from LCInsureAccBalance where polno = '" + mPolNo + "' and insuaccno = '" + mInsuAccNo 
           + "' order by DueBalaDate desc ";
  var result2 = easyExecSql(sql2);
  if(result2 && result2[0][0] != "null")
  {
    fm.all('BalaMonth').value = result2[0][0];
  }
  var sql3 = "select rate,ltrim(rtrim(char(year(startbaladate))))||'-'||ltrim(rtrim(char(month(startbaladate)))) "
           + "from LMInsuAccRate where riskcode = '" + mRiskCode + "' and insuaccno = '" + mInsuAccNo 
           + "' order by startbaladate desc ";
  var result3 = easyExecSql(sql3);
  if(result3)
  {
    fm.all('Rate').value = result3[0][0];
    fm.all('StartBalaMonth').value = result3[0][1];
  }
}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!—页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <title>万能保单事项</title>
</head>

<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
<br>
<br>
  <form name=fm target="fraSubmit">
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>保单号</td>
        <td  class= input><Input class=common  name=ContNo readonly=true></td>
        <td  class= title>投保日期</td>
        <td  class= input><input class=common name=PolApplyDate readonly=true></td>
        <td  class= title>当前账户价值</td>
        <td  class= input><input class=common name=InsuAccBala  readonly=true></td>
      </tr>
      <tr  class= common>
        <td class= title>最近结算月份</td>
        <td  class= input><input class=common name=BalaMonth  readonly=true></td>
        <td class= title>最近公布收益率</td>
        <td  class= input><input class=common name=Rate  readonly=true></td>
        <td class= title>对应月度</td>
        <td  class= input><input class=common name=StartBalaMonth  readonly=true></td>
      </tr>
      <tr  class= common>
        <td><Input type =button class=cssButton value="返  回" onclick="returnParent()"></td>
      </tr>
    </table>
  </form>
<!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
