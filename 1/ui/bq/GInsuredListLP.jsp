<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListLP.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  XmlExport txmlExport = null;   
  
  String tZipFile="";
  String outname="";
  String outpathname="";
  
  String edorAcceptNo = request.getParameter("edorAcceptNo");
  LPEdorAppSchema mLPEdorAppSchema = new LPEdorAppSchema();
  mLPEdorAppSchema.setEdorAcceptNo(edorAcceptNo);
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("edorType", request.getParameter("edorType"));
  
  VData tVData = new VData();
  tVData.addElement(mLPEdorAppSchema);
  tVData.addElement(tG);
  tVData.addElement(tTransferData);
         
  PrtGrpInsuredListLPUI tPrtGrpInsuredListLPUI = new PrtGrpInsuredListLPUI(); 
  if(!tPrtGrpInsuredListLPUI.submitData(tVData,"PRINT"))
  {          
     	operFlag = false;
     	Content = tPrtGrpInsuredListLPUI.mErrors.getFirstError(); 
  }
  else
  {             
    VData mResult = tPrtGrpInsuredListLPUI.getResult();			
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    
    if(txmlExport==null)
    {
    	operFlag=false;
    	Content="没有得到要显示的数据文件";	  
    }
  }
	
	if (operFlag==true)
	{
//	  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
//    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
//    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
//    tcombineVts.output(dataStream);
//    session.putValue("PrintVts", dataStream);
//	  
//		session.putValue("PrintStream", txmlExport.getInputStream());
//		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
		System.out.println("处理报表");
	    Readhtml rh=new Readhtml();
	    rh.XmlParse(txmlExport.getInputStream());
	    String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
		String temp=realpath.substring(realpath.length()-1,realpath.length());
		if(!temp.equals("\\"))
		{
			realpath=realpath+"/"; 
		}
		String templatename=rh.getTempLateName();//模板名字
		String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
		System.out.println("*********************templatepathname= " + templatepathname);
		System.out.println("************************realpath="+realpath);

		String date=PubFun.getCurrentDate().replaceAll("-","");  
		String time=PubFun.getCurrentTime3().replaceAll(":","");
		
		outname="GInsuredListLP"+tG.Operator+date+time+".xls";
		outpathname=realpath+"vtsfile\\"+outname;
		
		rh.setReadFileAddress(templatepathname);
		rh.setWriteFileAddress(outpathname);
		rh.start("vtsmuch");
		
		try{
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
		  	outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
		  	outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	    }catch(Exception ex)
	    {
	     	ex.printStackTrace();
	    }
	    String[] InputEntry = new String[1];
	    InputEntry[0] = outpathname;
	    //tZipFile = realpath+"vtsfile\\"+ StrTool.replace(outname,".xls",".zip");
		System.out.println("tZipFile == " + outpathname);
	    //rh.CreateZipFile(InputEntry, tZipFile);
	}
	else
	{
    	FlagStr = "Fail";
    	%>
  		<%=Content%>  
  		<%
	}
	
%>
<html>
<a href="../f1print/download.jsp?filename=<%=outname%>&filenamepath=<%=outpathname%>">点击下载</a>
 
</html>