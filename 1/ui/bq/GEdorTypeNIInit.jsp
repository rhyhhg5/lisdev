<%
//程序名称：GEdorTypeNIInput.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script>
	
function initForm() 
{
  try
  {
  	initGrpEdor();
  	initInsuredListGrid();
  	initQuery();
  	querySpecialData();
  	showSpecialInfo();
  	showAllCodeName();
  }
  catch(ex)
  {
    alert("初始化页面错误！");
  }
}

function initGrpEdor()
{
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
	var flag;
	try
	{
		flag = top.opener.fm.all('loadFlagForItem').value;
	}
	catch(ex)
	{
		flag = "";	
	}
	
	if(flag == "TASK")
	{
	  fm.doImport.style.display = "none";
	  fm.del.style.display = "none";
		fm.save.style.display = "none";
		fm.goBack.style.display = "none";
	}
	
	fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
	fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
	fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
	fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
	fm.all('ContNo').value = "";
	
	showOneCodeName("EdorCode", "EdorTypeName");
}


//新增被保人列表
function initInsuredListGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="被保人编号";        //列名
		iArray[1][1]="50px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="状态";         			//列名
		iArray[2][1]="60px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="姓名";         			//列名
		iArray[3][1]="50px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="性别";         			//列名
		iArray[4][1]="30px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="出生日期";         			//列名
		iArray[5][1]="80px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="证件类型";         			//列名
		iArray[6][1]="40px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="证件号码";         			//列名
		iArray[7][1]="120px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="保险计划";         			//列名
		iArray[8][1]="50px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[9]=new Array();
		iArray[9][0]="职业类别";         			//列名
		iArray[9][1]="50px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[10]=new Array();
		iArray[10][0]="理赔银行";         			//列名
		iArray[10][1]="120px";            		//列宽
		iArray[10][2]=100;            			//列最大值
		iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[11]=new Array();
		iArray[11][0]="户名";         			//列名
		iArray[11][1]="60px";            		//列宽
		iArray[11][2]=100;            			//列最大值
		iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[12]=new Array();
		iArray[12][0]="帐号";         			//列名
		iArray[12][1]="100px";            		//列宽
		iArray[12][2]=100;            			//列最大值
		iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[13]=new Array();
		iArray[13][0]="期交保费";         			//列名
		iArray[13][1]="60px";            		//列宽
		iArray[13][2]=100;            			//列最大值
		iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
	  iArray[14]=new Array();
		iArray[14][0]="导入金额";         			//列名
		iArray[14][1]="60px";            		//列宽
		iArray[14][2]=100;            			//列最大值
		iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[15]=new Array();
		iArray[15][0]="期交保费";         			//列名
		iArray[15][1]="60px";            		//列宽
		iArray[15][2]=100;            			//列最大值
		iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[16]=new Array();
		iArray[16][0]="保全生效日期";         			//列名
		iArray[16][1]="80px";            		//列宽
		iArray[16][2]=100;            			//列最大值
		iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[17]=new Array();
		iArray[17][0]="社保号";         			//列名
		iArray[17][1]="80px";            		//列宽
		iArray[17][2]=100;            			//列最大值
		iArray[17][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[18]=new Array();
		iArray[18][0]="联系电话";         			//列名
		iArray[18][1]="80px";            		//列宽
		iArray[18][2]=100;            			//列最大值
		iArray[18][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[19]=new Array();
		iArray[19][0]="学校";         			//列名
		iArray[19][1]="60px";            		//列宽
		iArray[19][2]=100;            			//列最大值
		iArray[19][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[20]=new Array();
		iArray[20][0]="班级";         			//列名
		iArray[20][1]="40px";            		//列宽
		iArray[20][2]=100;            			//列最大值
		iArray[20][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[21]=new Array();
		iArray[21][0]="投保人姓名";         			//列名
		iArray[21][1]="50px";            		//列宽
		iArray[21][2]=100;            			//列最大值
		iArray[21][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[22]=new Array();
		iArray[22][0]="投保人性别";         			//列名
		iArray[22][1]="40px";            		//列宽
		iArray[22][2]=100;            			//列最大值
		iArray[22][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[23]=new Array();
		iArray[23][0]="投保人出生日期";         			//列名
		iArray[23][1]="70px";            		//列宽
		iArray[23][2]=100;            			//列最大值
		iArray[23][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[24]=new Array();
		iArray[24][0]="投保人证件类型";         			//列名
		iArray[24][1]="40px";            		//列宽
		iArray[24][2]=100;            			//列最大值
		iArray[24][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[25]=new Array();
		iArray[25][0]="投保人证件号码";         			//列名
		iArray[25][1]="100px";            		//列宽
		iArray[25][2]=100;            			//列最大值
		iArray[25][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		InsuredListGrid = new MulLineEnter("fm", "InsuredListGrid"); 
		//这些属性必须在loadMulLine前
		InsuredListGrid.mulLineCount = 0;   
		InsuredListGrid.displayTitle = 1;
    InsuredListGrid.canSel = 0;
    InsuredListGrid.canChk = 1;
		InsuredListGrid.hiddenPlus = 1;
		InsuredListGrid.hiddenSubtraction = 1;
		InsuredListGrid.selBoxEventFuncName ="onInsuredGridSelected";
		InsuredListGrid.loadMulLine(iArray);  
	}
	catch (ex)
	{
	  alert(ex);
	}
}

</script>