<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：
//创建日期：2005-11-28
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="GEdorTypeTA.js"></SCRIPT>
	<%@include file="GEdorTypeTAInit.jsp"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<title>团体帐户资金分配 </title>
</head>
<body onload="initForm();" >
  <form action="./GEdorTypeTASubmit.jsp" method=post name=fm target="fraSubmit">
  <table class="common">
    <tr class="common"> 
      <td class="title">受理号</TD>
      <td class="input"> 
        <input class="readonly" readonly name="EdorNo">
      </td>
      <td class="title">批改类型</TD>
      <td class="input">
      	<input class="readonly" readonly name="EdorType" type="hidden">
      	<input class="readonly" readonly name="EdorTypeName">
      </td>
      <td class="title">集体保单号</TD>
      <td class="input">
      	<input class="readonly" readonly name="GrpContNo">
      </td>
    </tr>
  </table>
  <!-- lx -->
  
  <Div  id= "divOtherSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="downButton" class=cssButton value="被保险人清单查询"  onclick="grpInsuList();">
	   		 <input type=Button name="queryButton" class= cssButton value="万能险账户查询" onclick="GPublicAccQuery();">
			</TR>
	 	</table>
	</Div>
  
  <%@include file="SpecialInfoCommon.jsp"%>
  <table>
    <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroup);">
      </td>
      <td class= titleImg>公共账户当前金额
      </td>
    </tr>
  </table>
  <div id= "divGroup" style= "display: ''">
    <table class= common>
<tr class= common>
<td>
<span id="spanPolGrid" >
</span>
</td>
</tr>
</table>
<div id="divPage333" align=center style="display: 'none' ">
      <input class=cssButton value="首  页" type=button onclick="turnPage3.firstPage();"> 
      <input class=cssButton value="上一页" type=button onclick="turnPage3.previousPage();">  
      <input class=cssButton value="下一页" type=button onclick="turnPage3.nextPage();"> 
      <input class=cssButton value="尾  页" type=button onclick="turnPage3.lastPage();">
    </div> 
  </div>
  <!-- lx -->
  <table>
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLPInsured);">
      </td>
      <td class= titleImg>导入清单</td>
    </tr>
   </table>
    <Div id= "divLPInsured" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanInsuredListGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage" align=center style="display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div>	
  	</div>	 
   <table>
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divFail);">
      </td>
      <td class= titleImg>无效数据</td>
    </tr>
   </table>
    <Div id= "divFail" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanFailGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage2" align=center style="display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
      </Div>	
  	</div>
    <br>
      <Div style="color:red; ">
   *除特殊情况外，在每月10日之后办理<br/>
   </Div>
	<Div  id= "divSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="importButton" class=cssButton value="导  入"  onclick="importInsured();">
	   		 <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
	   		 <input type=Button name="returnButton" class= cssButton value="返  回" onclick="returnParent();">
			</TR>
	 	</table>
	</Div>
	<input type=hidden id="fmtransact" name="fmtransact"><p align="right"></p>
	<input type=hidden id="ContType" name="ContType">
	<input type=hidden id="ContNo" name="ContNo">
	<input type=hidden id="EdorValiDate" name="EdorValiDate">
	<input type=hidden id="InsuredId" name="InsuredId">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>