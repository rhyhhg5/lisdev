//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 
var turnPageItem = new turnPageClass(); 
var mainState="0";
//var cEdorNo;
var cPolNo;
var alertflag = "0"; //10-29
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
function easyQueryClick()
{
	
//	alert("here");
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";			
	  strSQL=" select a.edorNo,a.contno,a.edorState,a.makedate,a.maketime from lpedorMain a,LPEdorApp b where a.EdorAcceptNo=b.EdorAcceptNo and a.edorstate<>'0' and b.OtherNoType in ('1','3') "
         + getWherePart( 'a.EdorNo','EdorNo' )
         + getWherePart( 'a.ContNo','ContNo')
         + getWherePart( 'a.EdorState','EdorState')
         + getWherePart( 'a.ManageCom','ManageCom' )
         +"order by a.MakeDate desc,a.MakeTime desc";
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
//  alert(turnPage.strQueryResult);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
       if(alertflag!="1")     //
          alert("查询失败,数据库中无此记录！"); //10-29  yz
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = MainGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
//  alert("here");
}


//查询按钮
function QueryClick()
{
	initPolGrid();
	fm.all('Transact').value ="QUERY|EDOR";
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
}


function getItemDetail()
{
	
	initPolGrid();
	var tsel=MainGrid.getSelNo();


	fm.all('EdorNo').value=MainGrid.getRowColData(tsel-1,1);
        mainState=MainGrid.getRowColData(tsel-1,3);
        itemMainNo=MainGrid.getRowColData(tsel-1,1);
 
  
  var strSQL = "";
	
	
  strSQL=" select distinct a.edorNo,a.contno,a.edortype,a.edorstate,a.MakeDate,a.MakeTime from lpedorItem a where a.edorNo='" +fm.all('EdorNo').value+ "' and a.edorstate<>'0'  order by a.MakeDate desc,a.MakeTime desc"
  
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  //turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);	


turnPageItem.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

  //判断是否查询成功
  if (!turnPageItem.strQueryResult) {

    alert("该批单下已无批改项目！");
    return false;
  }

  turnPageItem.arrDataCacheSet = clearArrayElements(turnPageItem.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPageItem.arrDataCacheSet = decodeEasyQueryResult(turnPageItem.strQueryResult,0,0,turnPageItem);
  
  //设置初始化过的MULTILINE对象
  turnPageItem.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPageItem.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPageItem.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet2 = turnPageItem.getData(turnPageItem.arrDataCacheSet, turnPageItem.pageIndex, turnPageItem.pageLineNum);
  arrGrid = arrDataSet2;
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet2, turnPageItem.pageDisplayGrid,turnPageItem);

 }

// 项目明细查询
function ItemQuery()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击明细查询按钮。" );
	else
	{
	    var cEdorNo = PolGrid.getRowColData(tSel - 1,1);	
	    var cSumGetMoney = 	PolGrid.getRowColData(tSel - 1,6);			
//		parent.VD.gVSwitch.deleteVar("PayNo");				
//		parent.VD.gVSwitch.addVar("PayNo","",cPayNo);
		
		if (cEdorNo == "")
		    return;
		    
				window.open("../sys/AllPBqItemQuery.jsp?EdorNo=" + cEdorNo + "&SumGetMoney=" + cSumGetMoney);		
							
	}
}

function CancelEdorMain()

{
	var arrReturn = new Array();
				
		var tSel = MainGrid.getSelNo();
	
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
		else
		{
			arrReturn = getQueryResult();
	
			fm.all('EdorNo').value = MainGrid.getRowColData(tSel-1,1);
			fm.all('ContNo').value=MainGrid.getRowColData(tSel-1,2);
                        fm.all('EdorState').value=MainGrid.getRowColData(tSel-1,3);			
			fm.all("Transact").value = "DELETE||EDOR";
	                fm.all("DelFlag").value="2";     // flag 为2 删除批单 
    
			// showSubmitFrame(mDebug);

		    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
			fm.submit();
		  
		}
}

	//撤销批单
function CancelEdor()
{
	var arrReturn = new Array();
				
		var tSel = PolGrid.getSelNo();
	
			
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
		else if(mainState=="1")
		{
			arrReturn = getQueryResult();
		        fm.all('EdorNo').value = PolGrid.getRowColData(tSel-1,1);
			fm.all('ContNo').value=PolGrid.getRowColData(tSel-1,2);
			fm.all('EdorType').value=PolGrid.getRowColData(tSel-1,3);
			fm.all('EdorState').value=PolGrid.getRowColData(tSel-1,4);
			fm.all('MakeDate').value=PolGrid.getRowColData(tSel-1,5);
			fm.all('MakeTime').value=PolGrid.getRowColData(tSel-1,6);			
			fm.all("Transact").value = "DELETE||EDOR";
	                fm.all("DelFlag").value="1";  //flag 为1 删除项目
	
			// showSubmitFrame(mDebug);
		    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

		    fm.submit();  

	}
	else {
		alert("该批单项目已申请确认，不可撤销！");
		return false;
		}
}


function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	
	//********dingzhong*************
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	//*******************************
/*	
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	//alert(arrSelected[0][0]);
*/	
	return arrSelected;
}


function afterSubmit( FlagStr, content,Result )
{

  showInfo.close();
  
  if (FlagStr == "Fail" )
  {   
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	         
				fm.all('EdorNo').value ="";
				fm.all('ContNo').value ="";	  
                                fm.all('EdorState').value="";
				if(fm.all("DelFlag").value=="2") //删主批单
				{ 	
				initForm();				
				easyQueryClick();
			 }
			 if(fm.all("DelFlag").value=="1") //删项目
				{ 				
				getItemDetail();
			 }
			 				
	
			    
			    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
  }
  	
  
}
