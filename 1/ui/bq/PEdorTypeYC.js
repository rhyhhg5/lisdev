//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeBCReturn()
{
   initForm();
}

function verify() {
  if(fm.all('GetYear').value != "")
{
   var year = parseInt(fm.all('GetYear').value);
   if(isNaN(year)){
   	alert("请输入正确的领取年龄!");
   	fm.all('GetYear').value = "";
   	return false;
   	}
   else
   	{
   		fm.all('GetYear').value = year;
   		return true;
    }
}else
	return false;
}

function edorTypeYCSave()
{
  if (!verify()) return false;
  else{
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   fm.all('fmtransact').value = "INSERT||MAIN";
   fm.submit();
  }
}


//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initLCBnfGrid();
 //  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact=fm.all('fmtransact').value;
//  	alert(tTransact);
	if (tTransact=="QUERY||MAIN")
	{
	    var iArray;
	    //alert(Result);
	    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  	    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	    //保存查询结果字符串
 	    turnPage.strQueryResult  = Result;
  	    //使用模拟数据源，必须写在拆分之前
  	    turnPage.useSimulation   = 1;  
            //查询成功则拆分字符串，返回二维数组
  	    var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
      	    if (tArr != null) {	
                turnPage.arrDataCacheSet =chooseArray(tArr,[0,,1,,2,3]);			
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = LCDutyGrid;    
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
	  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
		//调用MULTILINE对象显示查询结果
	   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
                  //xie zhong wen
                displayName();
	    } else {
		LCDutyGrid.delBlankLine("LCDutyGrid");
	    }
	 }
	 else {
             var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
             showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   	     initForm();
         }
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}


function returnParent()
{
	top.close();
}

function displayName()
{
    	
    var rowNum=LCDutyGrid.mulLineCount ; //行数	

    for(var i=0; i<rowNum; i++ ) {
         var strSqla = "select DutyName from LMDuty where Dutycode='" + LCDutyGrid.getRowColData(i,1) + "'" ;
         //查询SQL，返回结果字符串
         var tDutyName  = easyQueryVer3(strSqla, 1, 1, 1);         	
         if(tDutyName != '')
         {
            var tname = tDutyName.substring(4);
            LCDutyGrid.setRowColData(i,2,tname);
         }
         var strSqlb = "select getDutyName from lmdutygetalive where getDutycode='" + LCDutyGrid.getRowColData(i,3) + "' and getdutykind = '"+ LCDutyGrid.getRowColData(i,5)+"'" ;
         //查询SQL，返回结果字符串
         var tgetDutyName  = easyQueryVer3(strSqlb, 1, 1, 1);         	
         if(tgetDutyName != '')
         {   var tgetname = tgetDutyName.substring(4);
             LCDutyGrid.setRowColData(i,4,tgetname);    
         }
    }
}