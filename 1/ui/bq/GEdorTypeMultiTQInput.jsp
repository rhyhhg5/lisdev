<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<% 
//程序名称：GEdorTypeMultiDetailInput.jsp
//程序功能：团体保全明细总页面
//创建日期：2003-12-03 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>   
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeMultiTQ.js"></SCRIPT>
  <%@include file="GEdorTypeMultiTQInit.jsp"%>
  <script type="text/javascript">
	 var tsql=" 1 and code in (select code from ldcode where codetype=#paymode# and code in(#1#,#2#,#3#,#4#,#9#))";
	</script>
  <title>团体保全明细总页面</title> 
</head>

<body  onload="initForm();" >
  <form action="./GEdorTypeMultiTQSubmit.jsp" method=post name=fm target="fraSubmit">    
  <input type=hidden readonly name=EdorAcceptNo >
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 批单号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 批改类型 </TD>
        <TD class = input >
        	<input class = "readonly" type="hidden" readonly name=EdorType>
      		<input class = "readonly" readonly name=EdorTypeName>
        </TD>
        <TD class = title > 集体保单号 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=GrpContNo>
        </TD>   
      </TR>
    </TABLE> 
      <%@include file="SpecialInfoCommon.jsp"%>
    <br><hr>
    
    <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
        </td>
        <td class= titleImg>
          被保人信息(最多显示1000条，若有必要，请查询)
        </td>
      </tr>
    </table>
    
    <table class = common>
      <tr class = common>
      	<td class = title>
      		证件号码
      	</td>
      	<td class = input>
      		<input class = common  name=IDNo>   
        </TD>
        <td class = title>
      		个人客户号
        </td>
      	<td class = input>
      		<input class = common  name=CustomerNo>
        </TD>
        <td class = title>
      		客户姓名
      	</td>
      	<td class = input>
      		<input class = common  name=Name>
        </TD>  
        <td class = title>
      		其他号码
      	</td>
      	<td class = title>
      		<input class = common  name=OthIDNo>
      	</td>
      </tr>
    </table>
    <INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="queryClick();"> 
    <br><br>
    <Div  id= "divLPInsured" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsuredGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage" align=center style= "display: 'none' ">
        <INPUT class=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage(); changeNullToEmpty();"> 					
        <INPUT class=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage(); changeNullToEmpty();"> 			
      </Div>		
  	</div>
	  <br><hr> 
	  <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
        </td>
        <td class= titleImg>
          进行离职领取的被保人信息(最多显示1000条，若有必要，请查询)
        </td>
      </tr>
    </table>
    <table class = common>
      <tr class = common>
      	<td class = title>
      		证件号码
      		</td>
      	<td class = input>
      		<input class = common  name=IDNo3>   
          </TD>
        <td class = title>
      		个人客户号
      		</td>
      	<td class = input>
      		<input class = common  name=CustomerNo3>
          </TD>
        <td class = title>
      		客户姓名
      		</td>
      	<td class = input>
      		<input class = common  name=Name3>
        </TD>  
        <td class = title>
      		其他号码
      	</td>
      	<td class = title>
      		<input class = common  name=OthIDNo3>
      	</td>
      </tr>
    </table>
    <INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="querySelectedInsured();"> 
	  <Div  id= "divLPInsured2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsured2Grid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage2" align=center>
        <INPUT class=cssButton VALUE="首页" TYPE=button onclick="turnPage2.firstPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage(); changeNullToEmpty();"> 					
        <INPUT class=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage(); changeNullToEmpty();">					
      </Div>
  	</div>
  <br><hr> 
  	<div id="calTypeDiv" style="display: 'none'">&nbsp;
	    <input class="codeNo" name="CalType" value="1" 
	        CodeData="0|^1|条款规定算法^2|月比例算法"  
	        ondblclick="return showCodeListEx('CalType',[this,CalTypeName],[0,1]);" 
	        onkeyup="return showCodeListEx('CalType',[this,CalTypeName],[0,1]);"><Input class="codeName" name="CalTypeName" readonly>
	</div>
	<!-- 刘鑫  -->
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDWFee);">
    		</td>
    		<td class= titleImg>
    			 个人给付信息部分
    		</td>
    	</tr>
    	</table>
    	<Div  id= "divDWFee" style= "display: ''">
	      <table class=common> 
	        <TR  class= common>
	          <TD  class= title8>
	            付费方式
	          </TD>
	          <TD  class= input8>
	          <Input class=codeNo name=PayMode  VALUE="1" MAXLENGTH=1 verify="付费方式|notnull&code:PayMode" ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);" onkeyup="return showCodeListKey('GrpPayMode',[this,PayModeName],[0,1],null,tsql,'1',1);"><input class=codename name=PayModeName value ='现金' readonly=true elementtype=nacessary>
	          </TD>
	          <TD  class= title8>    
	          </TD>
	          <TD  class= input8>
	          </TD>
	          <TD  class= title8>    
	          </TD>
	          <TD  class= input8>
	          </TD>
	        </TR>
	        <tr>
	          <table class=common id='divBankInfo' style= "display: 'none'" >
	          	<tr class=common>
	          		<TD  class= title8>
	            	开户银行
	          		</TD>
	          		<TD  class= input8>
	            		<Input class=codeNo  name=BankCode style="display:''"  MAXLENGTH=1 verify="开户银行|code:bank&len<=24" ondblclick="return showCodeList('Bank',[this,BankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName],[0,1],null,null,null,1);"><input class=codename name=BankCodeName style="display:''" readonly=true >
	          		</TD>
	          		<TD  class= title8>
	           		 账&nbsp;&nbsp;&nbsp;号
	          		</TD>
	          		<TD  class= input8>
	            		<Input class= common8 name=BankAccNo style="display:''" verify="帐号|len<=40">
	          		</TD>       
 
	          		<TD  class= title8>
	            		户&nbsp;&nbsp;&nbsp;名
	          		</TD>
	          		<TD  class= input8>
	            		<Input class= common8 name=AccName style="display:''" verify="户名|len<=40">
	          		</TD>  
	        	</TR>
			</table>
		</TR> 
	  </table>   
 		</div>
	<!-- 刘鑫  -->	
	<br>
	<hr> 
	  <table class=common> 
	   <TR  class= common>
	   <TD  class= common>
	   
	  <Input type=Button name="goIntoItem" value="进入个人保全" class=cssButton onclick="pEdorMultiDetail()">
	  <Input type=hidden name="goDiskImport" value="磁盘导入" id="goDiskImport" class=cssButton onclick="diskImport()">
	  <Input type=Button name="cancel" value="撤销个人保全" class=cssButton onclick="cancelPEdor()"> 
	 <!-- <Input type=Button value="保  存" class=cssButton onclick="saveEdor()"> --->
	 
	 <!--将原来的返回改成保存并返回，主要目的为更新edorState 为1-录入完成。------------>
	  <Input type=Button name="save" value="保存申请" class=cssButton onclick="returnParent()">
	  <Input type=hidden  value="修改归属比例" class=cssButton onclick="return submitForm();">
	  <input type=hidden id="ContNo" name="ContNo">
	  <!-- add ContType for PEdor GT -->
	  <input type=hidden id="ContType" name="ContType"> 
	  <input type=hidden id="Transact" name="Transact">
	  <div id="test"></div>
	  <div id="ErrorsInfo"></div> 
	  </TD>
	  </TR>
	  </table>
	  <hr>
	  
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  window.focus();
</script>
