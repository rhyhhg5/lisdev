<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorBodyCheckPrintInit.jsp
//程序功能：保全人工核保体检通知书打印
//创建日期：2006-02-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initHealthGrid();
    queryHealthPrint();
  }
  catch(ex) 
  {
    alert("页面初始化错误，请重新登陆！");
  }
}

function initHealthGrid()
{                              
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="体检通知书号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="100px";         			//列宽
    iArray[1][2]=10;          			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="保全受理号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="100px";         			//列宽
    iArray[2][2]=10;          			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="体检人客户号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="80px";         			//列宽
    iArray[3][2]=10;          			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="体检人姓名";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="80px";         			//列宽
    iArray[4][2]=10;          			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="投保人姓名";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[5][1]="80px";         			//列宽
    iArray[5][2]=10;          			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="管理机构";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[6][1]="80px";         			//列宽
    iArray[6][2]=10;          			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="打印状态";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[7][1]="80px";         			//列宽
    iArray[7][2]=10;          			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    HealthGrid = new MulLineEnter("fm", "HealthGrid"); 
    //这些属性必须在loadMulLine前                            
    HealthGrid.mulLineCount = 0;
    HealthGrid.displayTitle = 1;
    HealthGrid.hiddenPlus = 1;   
    HealthGrid.hiddenSubtraction = 1;
    HealthGrid.canSel =1;
    HealthGrid.canChk = 0;
    HealthGrid.selBoxEventFuncName ="onClickedPrint" ;
    HealthGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>