//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";

// added by Zhengwf, for:遗失补发的收费
function BJDetailApplyConfirm()
{
	var num = fm.all("EdorValue").value;
	if(num<=0){
		alert("输入的数值必须大于0："+num);
		return false;
	}
	//下面需要对输入的金额进行校验
	//保单返还总金额（本次录入金额和之前操作的结余返还金额的合计）不能大于保单实收保费（包括首期、续期和保全收付费的合计）。
	
	//之前操作的结余返还金额的合计
	var sql_back = "select nvl(sum(-getmoney),0) from "
					+" ljagetendorse where feeoperationtype='BJ' and grpcontno='"+fm.all('GrpContNo').value+"'";

	//保单实收保费,下面两者之和 
	var sql_get1 = "select nvl(sum(sumactupaymoney),0) from "
					+" ljapay where incomeno='"+fm.all('GrpContNo').value+"' and incometype='1'" ;
							
	var sql_get2 = 	"select nvl(sum(getmoney),0) from "
					+"ljagetendorse where feeoperationtype!='BJ' and grpcontno='"+fm.all('GrpContNo').value+"'";
	//执行上述三条语句，然后获取我们的要的结果，进行判断
	var back_result = easyExecSql(sql_back);
	var get1_result = easyExecSql(sql_get1);
	var get2_result = easyExecSql(sql_get2);
	var math_back = parseFloat(back_result[0][0]);
	var math_get1 = parseFloat(get1_result[0][0]);
	var math_get2 = parseFloat(get2_result[0][0]);
	if((parseFloat(num)+math_back)>(math_get1+math_get2)){
		alert("本次录入金额应不能大于"+(math_get1+math_get2-math_back));
		return false;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.all('fmtransact').value = "INSERT";
	fm.submit();
}

function edorTypeBJReturn()
{
	top.opener.focus();
	top.opener.getGrpEdorItem();
	top.close();
}
function edorTypeLRQuery()
{
	alert("Wait...");
}
function edorTypeLRSave()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
 // showSubmitFrame(mDebug);
  //fm.submit();
  //showSubmitFrame(mDebug);

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*得到补发次数
 * 可以用这个得到保费回补嘛？
 * */
function getTimes()
{
	var ret = "";
	var sql = "select LostTimes from lcGrpcont where grpContNo = '" + fm.all('GrpContNo').value + "'";
	var arrResult = easyExecSql(sql);
	if (arrResult)
	{
		ret = arrResult[0][0];
	}
	return ret;
}