
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
  if(!checkSubmit(fm.ywtype.value))
  {
    return false;
  }
  if(!beforeSubmit())
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  fm.submit(); //提交
}

function beforeSubmit()
{
  fm.all('Drawer1').value = trim(fm.all('Drawer1').value);
  fm.all('DrawerID1').value = trim(fm.all('DrawerID1').value);
  fm.all('BankAccNo1').value = trim(fm.all('BankAccNo1').value);
  fm.all('AccName1').value = trim(fm.all('AccName1').value);
  fm.all('Drawer2').value = trim(fm.all('Drawer2').value);
  fm.all('DrawerID2').value = trim(fm.all('DrawerID2').value);
  fm.all('BankAccNo2').value = trim(fm.all('BankAccNo2').value);
  fm.all('AccName2').value = trim(fm.all('AccName2').value);
  return true;
}
//提交后操作,服务器数据返回后执行的操作
//业务类型，BQPAY保全收费，BQGET保全付费，XQPAY续期收费，XQGET续期付费(满期给付)
function afterSubmit(FlagStr, content, transact)
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    easyQueryClick(transact);
    resetDisplay(transact); 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  
}

//Click事件，当点击“查询”按钮时触发该函数
function queryClick(flag)
{ 
  if(!beforeQuery(flag))
  {
    return false;
  }
  if(!easyQueryClick(flag))
  {
    return false;
  }
  resetDisplay(flag);
  if(!returnQuery(flag))
  {
    return false;
  }
}

function easyQueryClick(flag) 
{
  if(flag=="BQPAY")
  {
    initLJSPayBQGrid();
    var strSql = "select (select Name from LDCom where ComCode = a.ManageCom),  "
               + "OtherNo, GetNoticeNo, SumDuePayMoney, AccName, "
               + "(select BankName from LDBank where BankCode = a.BankCode), "
               + "(select max(SendDate) from LYReturnFromBankB where PayCode=a.GetNoticeNo), SendBankCount" +
               		" ,(select bank_reason(GetNoticeNo) from dual)  "
               + "from LJSPay a "
               + "where 1=1 "
               + getWherePart('ManageCom','ManageCom1','like')
	           + getWherePart('OtherNo','WorkNo1','=')
               + getWherePart('GetNoticeNo', 'GetNoticeNo1','=')
               + " and CanSendBank = '1' and OtherNoType = '10' and SendBankCount >= 1 "
               + "union all "
               + "select (select Name from LDCom where ComCode = a.ManageCom),  "
               + "OtherNo, GetNoticeNo, SumDuePayMoney, AccName, "
               + "(select BankName from LDBank where BankCode = a.BankCode), "
               + "(select max(SendDate) from LYReturnFromBankB where PayCode=a.GetNoticeNo), SendBankCount" +
               		" ,(select bank_reason(GetNoticeNo) from dual) "
               + "from LJSPay a "
               + "where 1=1 "
               + getWherePart('ManageCom','ManageCom1','like')
	           + getWherePart('OtherNo','WorkNo1','=')
               + getWherePart('GetNoticeNo', 'GetNoticeNo1','=')
               + " and OtherNoType = '10' and PayDate < current date and bankcode is not null "
               + "and bankcode <> '' and (cansendbank in ('0','2','3','') or cansendbank is null ) "
               + "order by GetNoticeNo with ur";
    turnPage1.pageDivName = "divPage1";
    turnPage1.queryModal(strSql, LJSPayBQGrid);
  }
  else if(flag=="BQGET")
  {
    initLJAGetBQGrid();
    var strSql = "select (select Name from LDCom where ComCode = a.ManageCom),   "
               + "OtherNo, ActuGetNo, SumGetMoney, AccName, "
               + "(select BankName from LDBank where BankCode = a.BankCode), "
               + "(select max(SendDate) from LYReturnFromBankB where PayCode=a.ActuGetNo), SendBankCount," +
               		" (select bank_reason(ActuGetNo) from dual) "
               + "from LJAGet a "
               + "where 1=1 "
               + getWherePart('ManageCom','ManageCom2','like')
	           + getWherePart('OtherNo','WorkNo2','=')
               + getWherePart('ActuGetNo', 'ActuGetNo2','=')
               + " and CanSendBank = '1' and SendBankCount >= 1 and PayMode = '4' "
               //增加余额领取付费信息修改功能
               + " and (OtherNoType='10' or (a.OtherNoType='12' "
               + " and exists (select 1 from lcappaccgettrace where customerno = a.otherno and noticeno = a.actugetno)))"
               + "order by ActuGetNo with ur";
    turnPage2.pageDivName = "divPage2";
    turnPage2.queryModal(strSql, LJAGetBQGrid);
  }
  else
  {
    alert("业务类型错误！")
    return false;
  }
  return true;
}

function returnQuery(flag)
{
  if(flag=="BQPAY")
  {
    if(LJSPayBQGrid.mulLineCount == 0)
    {
      alert("没有查询到数据");
      return false;
    }
  }
  else if(flag=="BQGET")
  {
    if(LJAGetBQGrid.mulLineCount == 0)
    {
      alert("没有查询到数据");
      return false;
    }
  }
  else
  {
    alert("业务类型错误！")
    return false;
  }
  return true;
}

function selectOne1() 
{
  var arrReturn = getQueryResult1();
  afterQuery1(arrReturn);	          		  
}

function getQueryResult1()
{
  var arrSelected = null;
  tRow = LJSPayBQGrid.getSelNo();
  if( tRow == 0 || tRow == null )
  {
    return arrSelected;
  }
  arrSelected = new Array();
  //设置需要返回的数组
  arrSelected[0] = new Array();
  arrSelected[0] = LJSPayBQGrid.getRowData(tRow-1);
  return arrSelected;
}

function afterQuery1( arrQueryResult )
{
  var arrResult = new Array();	
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.GetNoticeNoR1.value = arrResult[0][2];
    fm.WorkNoR1.value = arrResult[0][1];
    fm.GetMoney1.value = arrResult[0][3];
    fm.BankCodeName1.value = arrResult[0][5];
  }
  var sql = "select (select bankunsuccreason from LYReturnFromBankB where paycode = a.getnoticeno " 
          + "order by SendDate desc fetch first 1 row only), "
          + "(case when bankaccno is null or bankaccno = '' then '1' else '4' end), " 
          + "(select name from ldperson where customerno = a.appntno), "
          + "(select idno from ldperson where customerno = a.appntno), "
          + "bankcode,bankaccno, accname "
          + "from ljspay a where "
          + "getnoticeno = '" +arrResult[0][2]+ "' with ur";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.all('Reason1').value = rs[0][0];
    fm.all('PayMode1').value = rs[0][1];
    fm.all('Drawer1').value = rs[0][2];
    fm.all('DrawerID1').value = rs[0][3];
    fm.all('BankCode1').value = rs[0][4];
    fm.all('BankAccNo1').value = rs[0][5];
    fm.all('AccName1').value = rs[0][6];			
  }
  showAllCodeName();
}

function selectOne2() 
{
  var arrReturn = getQueryResult2();
  afterQuery2(arrReturn);	          		  
}

function getQueryResult2()
{
  var arrSelected = null;
  tRow = LJAGetBQGrid.getSelNo();
  if( tRow == 0 || tRow == null )
  {
    return arrSelected;
  }
  arrSelected = new Array();
  //设置需要返回的数组
  arrSelected[0] = new Array();
  arrSelected[0] = LJAGetBQGrid.getRowData(tRow-1);
  return arrSelected;
}

function afterQuery2( arrQueryResult )
{
  var arrResult = new Array();	
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.ActuGetNoR2.value = arrResult[0][2];
    fm.WorkNoR2.value = arrResult[0][1];
    fm.GetMoney2.value = arrResult[0][3];
    fm.BankCodeName2.value = arrResult[0][5];
  }
  var sql = "select (select bankunsuccreason from LYReturnFromBankB where paycode = a.actugetno " 
          + "order by SendDate desc fetch first 1 row only), "
          + "paymode,drawer,drawerid,bankcode,bankaccno, accname "
          + "from ljaget a where "
          + "actugetno = '" +arrResult[0][2]+ "' with ur";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.all('Reason2').value = rs[0][0];
    fm.all('PayMode2').value = rs[0][1];
    fm.all('Drawer2').value = rs[0][2];
    fm.all('DrawerID2').value = rs[0][3];
    fm.all('BankCode2').value = rs[0][4];
    fm.all('BankAccNo2').value = rs[0][5];
    fm.all('AccName2').value = rs[0][6];			
  }
  showAllCodeName();
}

function beforeQuery(flag)
{ 
  if(flag == "BQPAY")
  {
    var tManageCom1 = trim(fm.ManageCom1.value);
    var tWorkNo1 = trim(fm.WorkNo1.value);
    var tGetNoticeNo1 = trim(fm.GetNoticeNo1.value); 
    if(tManageCom1=="" && tWorkNo1=="" && tGetNoticeNo1=="")
    {
      alert("至少一个查询条件不能为空!");
      return false;
    } 
  }
  else if(flag == "BQGET")
  {
    var tManageCom2 = trim(fm.ManageCom2.value);
    var tWorkNo2 = trim(fm.WorkNo2.value);
    var tActuGetNo2 = trim(fm.ActuGetNo2.value); 
    if(tManageCom2=="" && tWorkNo2=="" && tActuGetNo2=="")
    {
      alert("至少一个查询条件不能为空!");
      return false;
    } 
  }
  else 
  {
    alert("业务类型错误！")
    return false;
  }
  return true;
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

//进行两次输入的校验
var theFirstValue="";
var theSecondValue="";
function confirmBankAccNo(aObject)
{ 
  if(theFirstValue!="")
  {
    theSecondValue = aObject.value;
    if(theSecondValue=="")
    {
      alert("请再次录入银行帐号！");
      aObject.value="";
      aObject.focus();
      return;
    }
    if(theSecondValue==theFirstValue)
    {
      aObject.value = theSecondValue;
      theSecondValue="";
      theFirstValue="";
      return;
    }
    else
    {
      alert("两次录入帐号不符，请重新录入！");
      theFirstValue="";
      theSecondValue="";
      aObject.value="";
      aObject.focus();
      return;
    }
  }
  else
  {
    theFirstValue = aObject.value;
    theSecondValue="";
    if(theFirstValue=="")
    {
      return;
    }
    alert("请重新录入银行帐号！");
    aObject.value="";
    aObject.focus();
    return;
  }
}

//获取财务类型Type2，0-收费，1-付费
//content 3-收费，4-付费
function getType(content)
{
  if(content=="3")
  {
      document.all("ywtype").value = "BQPAY";
      document.all("divBQPay").style.display = "";
      document.all("divBQGet").style.display = "none";
  }
  else
  {
      document.all("ywtype").value = "BQGET";
      document.all("divBQPay").style.display = "none";
      document.all("divBQGet").style.display = "";
  }
  resetDisplay(fm.ywtype.value);
  initLJSPayBQGrid(); 
  initLJAGetBQGrid();
}

function resetDisplay(flag)
{
  if(flag=="BQPAY")
  {
    fm.GetNoticeNoR1.value = "";
    fm.WorkNoR1.value = "";
    fm.GetMoney1.value = "";
    fm.Reason1.value = "";
    fm.PayMode1.value = "";
    fm.PayModeName1.value = "";
    fm.Drawer1.value = "";
    fm.DrawerID1.value = "";
    fm.BankCode1.value = "";
    fm.BankCodeName1.value = "";
    fm.BankAccNo1.value = "";
    fm.AccName1.value = "";
  }
  else if(flag=="BQGET")
  {
    fm.ActuGetNoR2.value = "";
    fm.WorkNoR2.value = "";
    fm.GetMoney2.value = "";
    fm.Reason2.value = "";
    fm.PayMode2.value = "";
    fm.PayModeName2.value = "";
    fm.Drawer2.value = "";
    fm.DrawerID2.value = "";
    fm.BankCode2.value = "";
    fm.BankCodeName2.value = "";
    fm.BankAccNo2.value = "";
    fm.AccName2.value = "";
  }	
}

function checkSubmit(flag)
{
  if(flag=="BQPAY")
  {
    if(LJSPayBQGrid.mulLineCount == 0)
    {
      alert("请先查询！");
      return false;
    }
    if(trim(fm.GetNoticeNoR1.value) == "")
    {
      alert("请选择需要修改的数据!");
      return false;
    }
    if(fm.PayMode1.value == "4")
    {
      if(trim(fm.Drawer1.value) == "")
      {
        alert("缴费方式为银行转账时对方姓名不能为空!");
        fm.Drawer1.focus();
        return false;
      }
      if(trim(fm.DrawerID1.value) == "")
      {
        alert("缴费方式为银行转账时对方身份证号不能为空!");
        fm.DrawerID1.focus();
        return false;
      }
      if(trim(fm.BankCode1.value) == "")
      {
        alert("缴费方式为银行转账时对方开户银行不能为空!");
        fm.BankCode1.focus();
        return false;
      }
      if(trim(fm.BankAccNo1.value) == "")
      {
        alert("缴费方式为银行转账时对方银行帐号不能为空!");
        fm.BankAccNo1.focus();
        return false;
      }
      if(trim(fm.AccName1.value) == "")
      {
        alert("缴费方式为银行转账时对方帐户名不能为空!");
        fm.AccName1.focus();
        return false;
      }
    }
  }
  else if(flag=="BQGET")
  {
    if(LJAGetBQGrid.mulLineCount == 0)
    {
      alert("请先查询！");
      return false;
    }
    if(trim(fm.ActuGetNoR2.value) == "")
    {
      alert("请选择需要修改的数据!");
      return false;
    }
    if(fm.PayMode2.value == "4")
    {
      if(trim(fm.Drawer2.value) == "")
      {
        alert("付费方式为银行转账时对方姓名不能为空!");
        fm.Drawer2.focus();
        return false;
      }
      if(trim(fm.DrawerID2.value) == "")
      {
        alert("付费方式为银行转账时对方身份证号不能为空!");
        fm.DrawerID2.focus();
        return false;
      }
      if(trim(fm.BankCode2.value) == "")
      {
        alert("付费方式为银行转账时对方开户银行不能为空!");
        fm.BankCode2.focus();
        return false;
      }
      if(trim(fm.BankAccNo2.value) == "")
      {
        alert("付费方式为银行转账时对方银行帐号不能为空!");
        fm.BankAccNo2.focus();
        return false;
      }
      if(trim(fm.AccName2.value) == "")
      {
        alert("付费方式为银行转账时对方帐户名不能为空!");
        fm.AccName2.focus();
        return false;
      }
    }
  }	
  else 
  {
    alert("业务类型错误！")
    return false;
  }
  return true;
}

function payModeHelp(){
	window.open("../finfee/PayModeHelp.jsp");
}

function afterCodeSelect(cCodeName, Field)
{
  try	
  {
    if(cCodeName == "PayMode")	
    {
      if(fm.PayMode1.value!="4")
      {
        fm.Drawer1.value = "";
        fm.DrawerID1.value = "";
        fm.BankCode1.value = "";
        fm.BankCodeName1.value = "";
        fm.BankAccNo1.value = "";
        fm.AccName1.value = "";
      }
    }
  }
  catch(ex){}
}