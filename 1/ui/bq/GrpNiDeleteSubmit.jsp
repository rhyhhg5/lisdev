<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpNiDeleteSubmit.jsp
//程序功能：删除导入清单中的被保人
//创建日期：2005-08-02
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");	
	
	String grpContNo = request.getParameter("GrpContNo");
	String edorNo = request.getParameter("EdorNo");
	String[] insuredIdList = request.getParameterValues("InsuredListGridChk");
	
	LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
	for (int i = 0; i < insuredIdList.length; i++)
	{
		LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
		tLCInsuredListSchema.setGrpContNo(grpContNo);
		tLCInsuredListSchema.setEdorNo(edorNo);
		tLCInsuredListSchema.setInsuredID(insuredIdList[i]);
		tLCInsuredListSet.add(tLCInsuredListSchema);
	}
		
	//输入参数
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tLCInsuredListSet);
	
	DeleteInsuredListUI tDeleteInsuredListUI = new DeleteInsuredListUI();
	if (!tDeleteInsuredListUI.submitData(tVData, "DELETE"))
	{
		FlagStr = "Fail";
		Content = tDeleteInsuredListUI.mErrors.getFirstError();
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据删除成功！";
	}
%> 

<html>
<script language="javascript">
	parent.fraInterface.initForm();
</script>
</html>

