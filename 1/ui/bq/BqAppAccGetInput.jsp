<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2006-2-20 
//创建人  ：mojiao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>   
<Script>
var comCode = <%=tG.ComCode%>
</Script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="BqAppAccGetInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BqAppAccGetInputInit.jsp"%>
  <title>保全查询 </title>
</head>
<body  onload="initForm();">
  <form action="BqAppAccGetSave.jsp" method=post name=fm target="fraSubmit" >
  <Div id="divTaskInfo" style="display: ''"> 
	  <table class=common>
	    <tr class=common>
	      <TD class= title> 客户号码 </TD>
	      <TD class= input>
		    	<Input class=common name="CustomerNo" value='' style="width:100">
		    	<input type="button" Class="cssButton" name="search" value="个人" onclick="queryCustomerNo();" >
		    	<input type="button" Class="cssButton" name="searchGrp" value="团体" onclick="queryGrpCustomerNo();" > 
		    </TD>
		  </TR>
		</table>
	</Div>
	
	<table id="CustomerTable" style="display: 'none'">
    <tr>
      <td  class=common>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCustomerInfo)">
      </td>
    <td class= titleImg>客户基本信息&nbsp;</td>
    </tr>
  </table>
  <Div id="divCustomerInfo" style="display: 'none'"> 
	  <table  class= common>
	  	<TR  class= common>           
		    <TD  class= title>
		      姓名
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=Name readonly >
		    </TD>
		    <TD  class= title>
		      性别
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=Sex readOnly >
		    </TD>
		    <TD  class= title>
		      出生日期
		    </TD>
		    <TD  class= input>
		      <Input class="readonly"  name=Birthday readonly >
		    </TD>
			</TR>
     	<TR class= common>
		    <TD  class= title>
		      职业
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=Occupation readonly >
		    </TD>
		    <TD  class= title>
		      证件类型
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=IDType readOnly >
		    </TD>
		    <TD  class= title>
		      证件号码
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=IDNo2 readonly >
		    </TD>
		 	</TR>
	 		<TR  class= common style="display:'none'">
				<TD  class= title>
		      家庭地址
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=HomeAddress readonly >
		    </TD>
		    <TD  class= title>
		      邮编
		    </TD>
	   		<TD  class= input>
		      <Input class="readonly" name=ZipCode readonly >
		    </TD>
		    <TD  class= title>
		      通讯地址
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=PostalAddress readonly >
		    </TD>
		 </TR>
		 <TR style="display:'none'">
		    <TD  class= title>
		      住宅电话
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=HomePhone readonly >
		    </TD>
		    <TD  class= title>
		      单位编码
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=GrpNo readonly >
		    </TD>
		    <TD  class= title>
		      单位地址
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=CompanyAddress readonly >
		    </TD>
	  	</TR>
	  	<TR style="display:'none'"> 
		    <TD  class= title>
		      单位电话
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=CompanyPhone readonly >
		    </TD>
		    <TD  class= title>
		      e_mail
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=EMail readonly >
		    </TD>
		    <TD  class= title>
		      移动电话
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=Mobile readonly >
		    </TD>
			</TR>
			<TR style="display:none;">
		   	<TD  class= title>
		      婚姻状况
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=Marriage ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);" >
		    </TD>
		    <TD  class= title>
		      职业代码
		    </TD>
		    <TD  class= input>
					<Input class="readonly" name=OccupationCode ondblclick="return showCodeList('occupationcode',[this]);" onkeyup="return showCodeListKey('occupationcode',[this]);">
		    </TD>
		    <TD  class= title>
		      户籍所在地
		    </TD>
		    <TD  class= input>
		      <Input class="readonly" name=RgtAddress ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);" >
		    </TD>
	  	</TR>
		</table>
  </Div>
  <%@ include file="../task/GroupInfo.jsp"%>  
  <INPUT VALUE="查看帐户信息" TYPE=button Class="cssButton" name="view" onclick="viewAppAccInfo();"> 
  	
	<table>
  	<tr> 
      <td width="17"> 
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divAppAccList);"> 
      </td>
      <td width="185" class= titleImg>帐户信息 </td>
    </tr>
	</table>
	<Div  id= "divAppAccList" style= "display: ''">  
		<table  class= common>
	 		<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanAppAccGrid" >
				</span> 
		  	</td>
			</tr>
		</table>
		<Div id= "divPage" align=center style= "display: 'none' ">
		  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
		  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
		  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
		  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 
	 	</Div>
	</Div>
	
	<table>
    <tr> 
      <td width="17"> 
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divAppAccTraceList);"> 
      </td>
      <td width="185" class= titleImg>帐户轨迹信息</td>
    </tr>
	</table>
	<Div  id= "divAppAccTraceList" style= "display: ''">  
		<table  class= common>
	 		<tr  class= common>
	  		<td text-align: left colSpan=1>
					<span id="spanAppAccTraceGrid" >
					</span> 
	  		</td>
			</tr>
		</table>
		<Div id= "divPage1" align=center style= "display: 'none' ">
		  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
		  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();"> 
	 	</Div>
	</Div>
	
	<DIV id=AppAccGetInfo style= "display: 'none'" >
		<table>
	    <tr> 
	      <td width="17"> 
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divAppAccGetTraceList);"> 
	      </td>
	      <td width="185" class= titleImg>帐户领取信息</td>
	    </tr>
		</table>
		<Div  id= "divAppAccGetTraceList" style= "display: ''">  
			<table  class= common>
		 		<tr  class= common>
		  		<td text-align: left colSpan=1>
						<span id="spanAppAccGetTraceGrid" >
						</span> 
		  		</td>
				</tr>
			</table>
			<Div id= "divPage2" align=center style= "display: 'none' ">
			  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
			  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
			  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
			  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
		 	</Div>
		</Div>
	</DIV>
	<table>
		 <tr>
		    <td>
		      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAccGetInfo);">
		    </td>
		    <td class= titleImg>
		      领取人信息
		    </td>
		 </tr>
  </table>
  <Div  id= "divAccGetInfo" style= "display: ''">
		<table  class= common>
			<TR>
				<TD  class= title>
		  		领款人姓名
				</TD>
				<TD  class= input>
			  	<Input class= common name=AccGetName verify="领款人姓名|notnull" elementtype=nacessary>
				</TD>		
				<TD  class= title>
	        证件类型
	      </TD>
	      <TD  class= input>
	          <Input class= codeNo name="GetIDType"  verify="证件类型|notnull&code:IDType" ondblclick="return showCodeList('IDType',[this,GetIDTypeName], [0,1]);" onkeyup="return showCodeListKey('IDType',[this,GetIDTypeName], [0,1]);"><Input class= codeName name="GetIDTypeName" elementtype=nacessary readonly >
	     </TD>
	      <TD  class= title>
	        证件号码
	      </TD>
	      <TD  class= input>
	        <Input class= common name=GetIDNo verify="证件号码|notnull" elementtype=nacessary>
	     </TD>
	    </TR>
	    <TR CLASS=common>
	     <TD CLASS=title>
		      领取金额
		    </TD>
		    <TD CLASS=input COLSPAN=1  >
		      <Input class= common name=AccGetMoney verify="领取金额|notnull&NUM" elementtype=nacessary>
		    </TD>		
		    <TD CLASS=title  >
		      领取方式 
		    </TD>
		    <TD CLASS=input COLSPAN=1 >
		      <Input class= codeNo name="PayMode" CodeData="0|^1|现金^4|转帐" verify="领取方式|notnull&code:paymode" ondblclick="return showCodeListEx('paymode',[this,PayModeName], [0,1]);" onkeyup="return showCodeListKeyEx('paymode',[this,PayModeName], [0,1]);"><Input class= codeName name="PayModeName" elementtype=nacessary readonly >
		    </TD>
		  </TR>
		  <TR CLASS=common id='ForTransfer1' style= "display: 'none'">
		    <TD CLASS=title  >
		      开户银行 
		    </TD>
		    <TD CLASS=input COLSPAN=1 >
		      <Input NAME=BankCode VALUE="" CLASS="codeNo" MAXLENGTH=20 verify="开户银行|code:bank" ondblclick="return showCodeList('bank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('bank',[this,BankCodeName],[0,1]);" ><Input class= codeName name="BankCodeName" elementtype=nacessary readonly >
		    </TD>
		    <TD CLASS=title >
		      户名 
		    </TD>
		    <TD CLASS=input COLSPAN=1  >
		      <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20>
		    </TD>
	      <TD CLASS=title width="109" >
		      账号</TD>
		    <TD CLASS=input COLSPAN=1  >
		      <Input NAME=BankAccNo  CLASS=common >
		    </TD>
		  </TR>
		 	<TR CLASS=common id='ForTransfer2' style= "display: 'none'">
		    <TD CLASS=title width="109" >
		      转帐日期</TD>
		    <TD CLASS=input COLSPAN=1  >
		      <Input class="coolDatePicker" dateFormat="short" name=TransferDate verify="转帐日期|date">
		    </TD>
		  </TR>
		</Table>
  </Div>
  <br>
  <hr>
  <INPUT VALUE="确    认" TYPE=button Class="cssButton" name="confirm" onclick="getAppAcc();">
  <INPUT VALUE="记录清单打印" TYPE=button Class="cssButton" name="confirm" onclick="printAppAccTraceList();">
  <INPUT VALUE="领取单打印" TYPE=button Class="cssButton" name="confirm" onclick="printAppAccGet();">  
  <INPUT VALUE="取    消" TYPE=button Class="cssButton" name="confirm" onclick="clearForm();">
  <INPUT TYPE=hidden name="ContType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
