//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var iCount=1;
var typeUW = false;
var mainUW = false;
//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initPGrpEdorMainGrid();
  fm.submit(); //提交
}

//批单总单结论
function grpEdorMainManuUW()
{
	var tGrpContNo ;
	var tEdorNo;
	
	tGrpContNo = fm.all('GrpContNo').value;
	tEdorNo = fm.all('EdorNo').value;
	
	if ((tGrpContNo==''||tGrpContNo==null)&& (tEdorNo ==''||tEdorNo==null))
		alert("请先选择待核保的申请批单!");
	else
	{
		if (fm.all("MainUWState").value == null || fm.all("MainUWState").value == "" )
		{
			alert("请先选择一个核保结论！");
			return;
		}
		if (window.confirm("是否核保本次申请?"))
		{
			mainUW = true;
			var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fm.all('fmtransact').value = "";
			fm.action = "./GEdorMainManuUWSubmit.jsp";
			fm.submit();
		}
	}
}

//批单项目结论
function grpEdorTypeManuUW()
{
	if (fm.all("EdorType").value == null || fm.all("EdorType").value == "" )
	{
		alert("请先选择一个批改项目！");
		return;
	}
	if (fm.all("TypeUWState").value == null || fm.all("TypeUWState").value == "" )
	{
		alert("请先选择一个核保结论！");
		return;
	}
	if (window.confirm("是否核保本次申请?"))
	{
		typeUW = true;
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.action = "./GEdorTypeManuUWSubmit.jsp";
		fm.submit();
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  if (typeUW)
  {
	  initPGrpEdorTypeGrid();
	  divGrpEdorMainManuUW.style.display = "none";
	  divGrpEdorTypeManuUW.style.display = "none";
	  fm.all("TypeUWState").value = "";
	  fm.all("ManuUWIdea").value = "";
	  queryEdorType();
	  typeUW = false;
  }
  else if (mainUW)
  {
	  goBack();
	  initPGrpEdorMainGrid();
	  fm.all("MainUWState").value = "";
	  easyQueryClick();
	  mainUW = false;
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

// 查询按钮
function easyQueryClick()
{
	var tEdorState;
	var tUWGrade;	
	tEdorState='2';
	
	tUWGrade = fm.all('uwGradeFlag').value;
	
	// 初始化表格
	initPGrpEdorMainGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var tUWGradeFlag;
	var tReturn = parseManageComLimit();

	tUWGradeFlag = fm.all('uwGradeFlag').value;
	iCount++;	
	strSQL = "select EdorNo,GrpContNo,EdorValiDate,EdorAppDate,ChgPrem,GetMoney from LPGrpEdorMain where EdorState = '"+ tEdorState + "' and uwstate in ('5','6')"//+ " and " + tReturn
				 + getWherePart( 'GrpContNo', 'QGrpContNo' )
				 + getWherePart('EdorNo', 'QEdorNo');
	
	if (tUWGradeFlag==null||tUWGradeFlag=='')
	{
			strSQL = strSQL + " and appgrade <= '"+tUWGrade+"'";
			fm.all('uwGradeFlag').value = 'A';
	}
	else
	{
		if (tUWGradeFlag=='P')
				strSQL = strSQL + " and appgrade='appgrade ='"+tUWGrade+"'";
		else
				strSQL = strSQL + " and appgrade<='"+tUWGrade+"'";
	}
	  
	  //查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  
	  //判断是否查询成功
	  if (turnPage.strQueryResult) 
	  {
		  //查询成功则拆分字符串，返回二维数组
		  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  
		  arrGrid = turnPage.arrDataCacheSet;
	  
		  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		  turnPage.pageDisplayGrid = PGrpEdorMainGrid;    
	          
		  //保存SQL语句
		  turnPage.strQuerySql     = strSQL; 
	  
		  //设置查询起始位置
		  turnPage.pageIndex       = 0;  
	  
		  //在查询结果数组中取出符合页面显示大小设置的数组
		  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	  
		  //调用MULTILINE对象显示查询结果
		  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);		 
	  }
}

//查询明细信息
function detailEdor()
	{
		var arrReturn = new Array();
				
		var tSel = PGrpEdorMainGrid.getSelNo();
		
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击查看按钮。" );
		else
		{
			arrReturn = getQueryResult();
			fm.all('EdorType').value =arrReturn[0][2];
			fm.all('EdorNo').value = arrReturn[0][0];
			fm.all('GrpContNo').value=arrReturn[0][1];
			//top.close();
			detailEdorType();
		}
	}
	
function getQueryResult()
{
	var arrSelected = null;
	tRow = PGrpEdorMainGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	//alert(arrSelected[0][0]);
	return arrSelected;
}
//显示自动核保信息
function edorClicked()
{
	var tSel = PGrpEdorMainGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条申请批单！" );
	else
	{
		if (PGrpEdorMainGrid.getRowColData(tSel-1,1)==null||PGrpEdorMainGrid.getRowColData(tSel-1,1)=='')
		{
			alert("请单击查询按钮!");
			divEdorManuUWAct.style.display ='none';
			return;
		}

		fm.all("EdorNo").value = PGrpEdorMainGrid.getRowColData(tSel-1,1);
		fm.all("GrpContNo").value = PGrpEdorMainGrid.getRowColData(tSel-1,2);
		fm.all("EdorValiDate").value = PGrpEdorMainGrid.getRowColData(tSel-1,3);
		fm.all("EdorAppDate").value = PGrpEdorMainGrid.getRowColData(tSel-1,4);
		fm.all("ChgPrem").value = PGrpEdorMainGrid.getRowColData(tSel-1,5);
		fm.all("GetMoney").value = PGrpEdorMainGrid.getRowColData(tSel-1,6);

		divGrpEdorSelect.style.display = "none";
		divEdorManuUWAct.style.display ='';
		divGrpEdorMainManuUW.style.display = "none";
		divGrpEdorTypeManuUW.style.display = "none";

		queryEdorType();
	}
}

function edorTypeGridClick()
{
	var tSel = PGrpEdorTypeGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条申请批单！" );
	else
	{
		if (PGrpEdorTypeGrid.getRowColData(tSel-1,1)==null||PGrpEdorTypeGrid.getRowColData(tSel-1,1)=='')
		{
			return;
		}
		fm.all("EdorType").value = PGrpEdorTypeGrid.getRowColData(tSel-1,2);
	}

}

function queryEdorType()
{
	var tEdorNo = fm.all("EdorNo").value;
			 
	strSQL = "select EdorNo,EdorType,EdorValiDate,EdorAppDate,UWFlag from LPGrpEdorItem where EdorState = '2' and UWFlag in ('5','6') and EdorNo = '" + tEdorNo + "'";

	  
	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	//判断是否查询成功
	if (turnPage.strQueryResult) 
	{
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

		arrGrid = turnPage.arrDataCacheSet;

		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = PGrpEdorTypeGrid;    
		  
		//保存SQL语句
		turnPage.strQuerySql     = strSQL; 

		//设置查询起始位置
		turnPage.pageIndex       = 0;  

		//在查询结果数组中取出符合页面显示大小设置的数组
		var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);//, MAXSCREENLINES);

		//调用MULTILINE对象显示查询结果
		displayMultiline(arrDataSet, turnPage.pageDisplayGrid);		 
		divGrpEdorTypeManuUW.style.display = "";
	}
	else
	{
		divGrpEdorMainManuUW.style.display = "";
	}
}

function detailUWInfo(tEdorNo,tEdorType,tGrpContNo)
{
/*
	var i,m,n,j;
	var tReturn;
	var strSql;
	n=0;	
	
	strSql = "select EdorNo, GrpContNo, AppDate, EdorAppDate, EdorValidate, ChgPrem, GetMoney from LPGrpEdorMain where EdorNo='" + tEdorNo + "' and edortype='"+ tEdorType+ "' and GrpContNo='" +tGrpContNo+ "' order by modifydate";

	
	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);  
	  
	//判断是否查询成功
	if (turnPage.strQueryResult)
	{
		//查询成功则拆分字符串，返回二维数组
	 	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);	
	 	n = turnPage.arrDataCacheSet.length;
	}
	else
	{
		return;
	}
 

	fm.all('EdorNo').value = tEdorNo;
	fm.all('EdorType').value = tEdorType;
	fm.all('GrpContNo').value = tGrpContNo;
	fm.all('UWstate').value = "";
	fm.all('ManuUWIdea').value = "";
*/

  var cEdorNo=fm.EdorNo.value;
  if(cEdorNo==""||cEdorNo==null)
  {
		alert("请先选择一个团体投保单!");
		return ;
  }
  window.open("../f1print/EndorsementF1P.jsp?EdorNo=" + cEdorNo,"window1");
}

/*********************************************************************
 *  查询团体批单核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGUWErr()
{
  var cGrpContNo=fm.GrpContNo.value;
  if(cGrpContNo==""||cGrpContNo==null)
  {
		alert("请先选择一个团体投保单!");
		return ;
  }
  window.open("./UWGrpEdorErrMain.jsp?GrpContNo="+cGrpContNo,"window1");
}

// 数据返回父窗口
function returnParent()
{
	alert("请选择其他菜单操作!");	
}

function goBack()
{
	divGrpEdorSelect.style.display = "";
	divEdorManuUWAct.style.display = "none";
	divGrpEdorMainManuUW.style.display = "none";
	divGrpEdorTypeManuUW.style.display = "none";
}