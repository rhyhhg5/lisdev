<%
//程序名称：PEdorTypeBFSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  String flag = "";
  String content = "";
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  
  String operator = request.getParameter("Operator");
  String edorNo = request.getParameter("EdorNo");
  String edorType = request.getParameter("EdorType");
  String contNo = request.getParameter("ContNo");
  String[] chkPolNo = request.getParameterValues("PolGridChk");
  //String[] polNo = request.getParameterValues("PolGrid2");
  String[] chkNewPolNo = request.getParameterValues("NewPolGridChk");
  String[] chkNo = request.getParameterValues("InpNewPolGridChk");
  String[] newPolNo = request.getParameterValues("NewPolGrid2");
  String[] insuredNo = request.getParameterValues("NewPolGrid6");

  String[] amnt = request.getParameterValues("NewPolGrid9");
  String[] mult = request.getParameterValues("NewPolGrid11");
	String[] validateTime = request.getParameterValues("NewPolGrid12");
	
	try
	{
	  LCPolSet tLCPolSet = new LCPolSet();
	  if (operator.equals("ADD"))
	  {
	  	if (chkPolNo != null) 
	  	{ 
		    for (int i = 0; i < chkPolNo.length; i++)
		    {
		      LCPolSchema tLCPolSchema = new LCPolSchema();
		      tLCPolSchema.setContNo(contNo);
		      tLCPolSchema.setPolNo(chkPolNo[i]);
		      tLCPolSet.add(tLCPolSchema);
		    }
		  }
	  }
	  else if (operator.equals("DEL"))
	  {
	  	if (chkNo != null)
			{
		    for (int i = 0; i < chkNo.length; i++)
		    {
		    	if (chkNo[i].equals("1"))
		    	{
		    		    	System.out.println("newPolNo:" + newPolNo[i]);
			      LCPolSchema tLCPolSchema = new LCPolSchema();
			      tLCPolSchema.setContNo(contNo);
			      tLCPolSchema.setPolNo(newPolNo[i]); 
			      tLCPolSchema.setInsuredNo(insuredNo[i]); 
			      tLCPolSet.add(tLCPolSchema);
			    }
		    }
		  }
	  }
		else if (operator.equals("SAVE"))
		{
			if (newPolNo != null)
			{
				for (int i = 0; i < newPolNo.length; i++)
		    {
		      LCPolSchema tLCPolSchema = new LCPolSchema();
		      tLCPolSchema.setContNo(contNo);
		      tLCPolSchema.setPolNo(newPolNo[i]);
		      tLCPolSchema.setInsuredNo(insuredNo[i]);
		      tLCPolSchema.setAmnt(amnt[i]);
		      tLCPolSchema.setMult(mult[i]);
		      tLCPolSet.add(tLCPolSchema);
		    }
		  }
		}
	
		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
		tLPEdorItemSchema.setEdorNo(edorNo);
		tLPEdorItemSchema.setEdorType(edorType);
		tLPEdorItemSchema.setContNo(contNo);
		
		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(tLPEdorItemSchema);
		if (newPolNo != null)
		{
			for (int i = 0; i < newPolNo.length; i++)
			{
				tSpecialData.setPolNo(newPolNo[i]);
				tSpecialData.add("ValiDateTime", validateTime[i]);
			}
		}
	  VData data = new VData();
	  data.add(gi);
	  data.add(tLCPolSet);
	  data.add(tLPEdorItemSchema);
	  data.add(tSpecialData);
	  PEdorBFDetailUI tPEdorBFDetailUI = new PEdorBFDetailUI();
	  if (!tPEdorBFDetailUI.submitData(data, operator))
	  {
	    flag = "Fail";
	    content = "数据保存失败！原因是：" + tPEdorBFDetailUI.getError();
	  }
	  else
	  {
	    flag = "Succ";
	    content = "数据保存成功！";
	  }
  }
	catch (Exception e)
	{
		e.printStackTrace();
		content = "程序异常！" + e.toString();
	}
  content = PubFun.changForHTML(content);
  System.out.println(flag);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>", "<%=operator%>");
</script>
</html>