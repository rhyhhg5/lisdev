<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GEdorTypeTZListPrint.jsp
//程序功能：
//创建日期：2008-02-13
//创建人  ：Zhanggm modify by xp 改造成EXCEL下载方式
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "增人导入清单_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    
      String sql = "set current query optimization=5";
        
      ExeSQL tExeSQL = new ExeSQL();
      tExeSQL.execUpdateSQL(sql);
      
    String edorno = request.getParameter("edorno");
    System.out.println("edorno:"+edorno);
	String tSQL = "select insuredid,employeename,insuredno,sex,birthday,idtype,idno,position,getyearflag,getyear,edorvalidate,perbankcode,peraccname,perbankaccno  from lcinsuredlist where edorno='"+edorno+"'";
	System.out.println("打印查询:"+tSQL);
	
    //设置表头
    String[][] tTitle = {{"序号","姓名","客户号","性别","出生日期","证件类型","证件号码","级别","老年护理金领取类型","老年护理金领取开始日期","生效日期","个人直接交费转账银行","户名","帐号"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(tSQL,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>