<% 
//程序名称：GEdorTypeWZInit.jsp
//程序功能：无名单增人
//创建日期：20060623
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String edorNo = request.getParameter("EdorNo");
  String grpContNo = request.getParameter("GrpContNo");
  String edorType = request.getParameter("EdorType");
  String currentDate = PubFun.getCurrentDate();
%>

<script language="JavaScript">
var currentDate = "<%=currentDate%>";

//初始化页面信息
function initForm()
{
  fm.EdorValiDate.value = currentDate;
  
  initEdorInfo();
  initGrpContInfo();
  initLCContPlan();
  initLCContPlanRisk();
  queryLCContPlan();
}

//初始化项目信息
function initEdorInfo()
{
  fm.EdorNo.value = "<%=edorNo%>";
  fm.EdorType.value = "<%=edorType%>";
  fm.GrpContNo.value = "<%=grpContNo%>";
  fm.GrpContNo2.value = "<%=grpContNo%>";
  
  var sql = "  select edorName "
            + "from LMEdorItem "
            + "where edorCode = '" + fm.EdorType.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    fm.EdorTypeName.value = result[0][0];
  }
}

//初始化保障计划信息列表
function initLCContPlan()
{
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保障计划";         		//列名
    iArray[1][1]="30px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21] = "contPlanCode";
    
    iArray[2]=new Array();
    iArray[2][0]="人员类别";         		//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21] = "contPlanName";
    
    iArray[3]=new Array();
    iArray[3][0]="被保人数";         		//列名
    iArray[3][1]="50px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21] = "peoples2";
    
    iArray[4]=new Array();
    iArray[4][0]="保费";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21] = "prem";
    
    iArray[5]=new Array();
    iArray[5][0]="保额";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21] = "amnt";
    
    iArray[6]=new Array();
    iArray[6][0]="保单号";         		//列名
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21] = "contNo";
    
    iArray[7]=new Array();
    iArray[7][0]="客户号";         		//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21] = "insuredNo";
      
    LCContPlan = new MulLineEnter( "fm" , "LCContPlan" ); 
    //这些属性必须在loadMulLine前
    LCContPlan.mulLineCount = 0;
    LCContPlan.displayTitle = 1;
    LCContPlan.locked = 1;
    LCContPlan.canSel = 1;
    LCContPlan.canChk = 0;
    LCContPlan.hiddenSubtraction = 1;
    LCContPlan.hiddenPlus = 1;
    LCContPlan.selBoxEventFuncName  = "queryLCContPlanRisk";
    LCContPlan.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex.message);
  }
}

//保障计划险种信息
function initLCContPlanRisk()
{
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保障计划";         		//列名
    iArray[1][1]="30px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21] = "contPlanCode";
    
    iArray[2]=new Array();
    iArray[2][0]="人员类别";         		//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21] = "contPlanName";
    
    iArray[3]=new Array();
    iArray[3][0]="险种代码";         		//列名
    iArray[3][1]="30px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21] = "riskCode";
    
    iArray[4]=new Array();
    iArray[4][0]="险种名称";         		//列名
    iArray[4][1]="120px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21] = "riskName";
    
      
    LCContPlanRisk = new MulLineEnter( "fm" , "LCContPlanRisk" ); 
    //这些属性必须在loadMulLine前
    LCContPlanRisk.mulLineCount = 0;
    LCContPlanRisk.displayTitle = 1;
    LCContPlanRisk.locked = 1;
    LCContPlanRisk.canSel = 0;
    LCContPlanRisk.canChk = 0;
    LCContPlanRisk.hiddenSubtraction = 1;
    LCContPlanRisk.hiddenPlus = 1;
    LCContPlanRisk.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex.message);
  }
}
  
</script>