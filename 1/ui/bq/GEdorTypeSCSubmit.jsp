<%
//程序名称：GEdorTypeSCSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----SCsubmit---");
  LPGrpPolSchema tLPGrpPolSchema   = new LPGrpPolSchema();
  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
  PGrpEdorSCDetailUI tPGrpEdorSCDetailUI   = new PGrpEdorSCDetailUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("------transact:"+transact);
  GlobalInput tG = new GlobalInput();
  System.out.println("------------------begin ui");
  tG = (GlobalInput)session.getValue("GI");
  
  //个人保单批改信息
  tLPGrpEdorMainSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
  tLPGrpEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPGrpEdorMainSchema.setEdorType(request.getParameter("EdorType"));
	
  tLPGrpPolSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
  tLPGrpPolSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPGrpPolSchema.setEdorType(request.getParameter("EdorType"));
  
  String Path = application.getRealPath("config//Conversion.config");	
  tLPGrpPolSchema.setGrpSpec(StrTool.Conversion(request.getParameter("GrpSpec"),Path));  
  //tLPGrpPolSchema.setGrpSpec(request.getParameter("GrpSpec")); 
  System.out.println(tLPGrpPolSchema.getGrpSpec());
try
  {
    // 准备传输数据 VData
    VData tVData = new VData();  
    //保存个人保单信息(保全)	
    tVData.addElement(tG);
    tVData.addElement(tLPGrpEdorMainSchema);
    tVData.addElement(tLPGrpPolSchema);
    tPGrpEdorSCDetailUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPGrpEdorSCDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))
    	{
    	if (tPGrpEdorSCDetailUI.getResult()!=null&&tPGrpEdorSCDetailUI.getResult().size()>0)
    	{
    		Result = (String)tPGrpEdorSCDetailUI.getResult().get(0);
    		if (Result==null||Result.trim().equals(""))
    		{
    			FlagStr = "Fail";
    			Content = "提交失败!!";
    		}
    	}
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>

<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

