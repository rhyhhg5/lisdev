<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：GEdorTypeMultiDetailSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
    String transact=(String)request.getParameter("Transact");
    
    String FlagStr = "";
    String Content = "";
    String Result="";
    
	String grpPolNo = request.getParameter("GrpPolNo");
	String grpContNo = request.getParameter("GrpContNo");
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	
	System.out.println("----------------"+tG.Operator+"---------------------");
	System.out.println("grpPolNo:"+grpPolNo);
	System.out.println("grpContNo:"+grpContNo);
	System.out.println("edorAcceptNo:"+edorAcceptNo);
	System.out.println("edorNo:"+edorNo);
	System.out.println("edorType:"+edorType);
	System.out.println("transact:"+transact);
	

    LPGrpEdorItemSchema tLPGrpEdorItemSchema= new LPGrpEdorItemSchema();
    LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
    
    tLPGrpEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
    tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
    tLPGrpEdorItemSchema.setEdorNo(edorNo);
    tLPGrpEdorItemSchema.setEdorType(edorType);
    tLPGrpEdorItemSchema.setEdorState("1");
	if(transact.trim().equals("INSERT||EDOR")){
	    String tContNo[] = request.getParameterValues("LCInsuredGrid1");    //合同号
	    String tInsuredNo[] = request.getParameterValues("LCInsuredGrid2"); //客户号
	    String tChk[] = request.getParameterValues("InpLCInsuredGridChk");
	    String tPolNo[] = request.getParameterValues("LCInsuredGrid8");
	    
	    for(int i=0;i<tContNo.length;i++){
	    	System.out.println("tPolNo[]:"+tPolNo[i]);
	    	System.out.println("tInsuredNo[]:"+tInsuredNo[i]);
	    	System.out.println("tChk[]:"+tChk[i]);
	    }
        for(int index=0;index<tChk.length;index++)
        {
            if(tChk[index].equals("1"))
            {
                LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
                tLPEdorItemSchema.setEdorNo(edorNo);
	            tLPEdorItemSchema.setContNo(tContNo[index]);
	            tLPEdorItemSchema.setInsuredNo(tInsuredNo[index]);
	            tLPEdorItemSchema.setEdorType(edorType);
	            tLPEdorItemSchema.setGrpContNo(grpContNo);
	            tLPEdorItemSchema.setPolNo(tPolNo[index]);
				tLPEdorItemSchema.setEdorState("1");
                tLPEdorItemSet.add(tLPEdorItemSchema);
            }           
        }
     }else if(transact.trim().equals("DELETE||EDOR")){
     	
	    String tContNo[] = request.getParameterValues("LCInsured2Grid1");    //合同号
	    String tInsuredNo[] = request.getParameterValues("LCInsured2Grid2"); //客户号
	    String tChk[] = request.getParameterValues("InpLCInsured2GridChk");
	    String tPolNo[] = request.getParameterValues("LCInsured2Grid8");
	    for(int i=0;i<tContNo.length;i++){
	    	System.out.println("tContNo[]:"+tContNo[i]);
	    	System.out.println("tInsuredNo[]:"+tInsuredNo[i]);
	    	System.out.println("tChk[]:"+tChk[i]);
	    	System.out.println("tPolNo[]:"+tPolNo[i]);
	    }
        for(int index=0;index<tChk.length;index++)
        {
            if(tChk[index].equals("1"))
            {
                LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
                tLPEdorItemSchema.setEdorNo(edorNo);
	            tLPEdorItemSchema.setContNo(tContNo[index]);
	            tLPEdorItemSchema.setInsuredNo(tInsuredNo[index]);
	            tLPEdorItemSchema.setEdorType(edorType);
	            tLPEdorItemSchema.setGrpContNo(grpContNo);
	            tLPEdorItemSchema.setPolNo(tPolNo[index]);
				tLPEdorItemSchema.setEdorState("1");
                tLPEdorItemSet.add(tLPEdorItemSchema);
            }           
        }
     }

    
  GEdorRTDetailUI tGEdorRTDetailUI   = new GEdorRTDetailUI();
  try {
  		VData tVData = new VData();  
	 	tVData.addElement(tG);
	 	tVData.addElement(tLPGrpEdorItemSchema);
	 	tVData.addElement(tLPEdorItemSet);
	 	
    	tGEdorRTDetailUI.submitData(tVData, transact);
	}
	catch(Exception ex) {
		Content = transact+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}			
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="") {
    CErrors tError = new CErrors(); 
    tError = tGEdorRTDetailUI.mErrors;
    
    if (!tError.needDealError()) {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	
    }
    else {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
     
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>");
</script>
</html>

