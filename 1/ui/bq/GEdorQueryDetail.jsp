<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：PEdorQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：LH
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  //保单信息部分
  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
  tLPGrpEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPGrpEdorMainSchema.setEdorType(request.getParameter("EdorType"));
  tLPGrpEdorMainSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
  
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLPGrpEdorMainSchema);
  // 数据传输
  PGrpEdorMainUI tPGrpEdorMainUI   = new PGrpEdorMainUI();
	if (!tPGrpEdorMainUI.submitData(tVData,"QUERY||EDOR"))
	{
      Content = " 查询失败，原因是: " + tPGrpEdorMainUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tPGrpEdorMainUI.getResult();
		// 显示
		// 保单信息
		LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema(); 
		LPGrpEdorMainSet mLPGrpEdorMainSet=new LPGrpEdorMainSet();
		LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

		mLPGrpEdorMainSet=(LPGrpEdorMainSet)tVData.getObjectByObjectName("LPGrpEdorMainBLSet",0);
		mLPGrpEdorMainSchema=mLPGrpEdorMainSet.get(1);
		mLCGrpPolSchema = (LCGrpPolSchema)tVData.getObjectByObjectName("LCGrpPolSchema",0);
		%>
    	<script language="javascript">
    		
    		parent.fraInterface.fm.all("GrpPolNo").value = "<%=mLCGrpPolSchema.getGrpPolNo()%>";
    		parent.fraInterface.fm.all("ContNo").value = "<%=mLCGrpPolSchema.getContNo()%>";
    	 	parent.fraInterface.fm.all("Name").value="<%=mLCGrpPolSchema.getGrpName()%>";
    	 	parent.fraInterface.fm.all("LinkMan").value="<%=mLCGrpPolSchema.getLinkMan1()%>";
    	 	parent.fraInterface.fm.all("SignDate").value="<%=mLCGrpPolSchema.getSignDate()%>";
    	 	parent.fraInterface.fm.all("PaytoDate").value="<%=mLCGrpPolSchema.getPaytoDate()%>";
    	 	parent.fraInterface.fm.all("Prem").value="<%=mLCGrpPolSchema.getPrem()%>";
    	 	parent.fraInterface.fm.all("Operator").value="<%=mLCGrpPolSchema.getOperator()%>";
				parent.fraInterface.fm.all("RiskCode").value = "<%=mLCGrpPolSchema.getRiskCode()%>";

    	
    		parent.fraInterface.fm.all("EdorNo").value = "<%=mLPGrpEdorMainSchema.getEdorNo()%>";
    	 	parent.fraInterface.fm1.all("GrpPolNo1").value = "<%=mLPGrpEdorMainSchema.getGrpPolNo()%>";
    	 	//parent.fraInterface.fm.all("ContNo").value = "<%=mLPGrpEdorMainSchema.getContNo()%>";
    	 	parent.fraInterface.fm.all("EdorType").value = "<%=mLPGrpEdorMainSchema.getEdorType()%>";
    	 	parent.fraInterface.fm.all("EdorValiDate").value = "<%=mLPGrpEdorMainSchema.getEdorValiDate()%>";
    	 	parent.fraInterface.fm.all("ChgPrem").value = "<%=mLPGrpEdorMainSchema.getChgPrem()%>";
    	 	parent.fraInterface.fm.all("GetMoney").value = "<%=mLPGrpEdorMainSchema.getGetMoney()%>";
    	 	parent.fraInterface.fm.all("Operator1").value = "<%=mLPGrpEdorMainSchema.getOperator()%>";
    	 	parent.fraInterface.fm.all("EdorAppDate").value = "<%=mLPGrpEdorMainSchema.getEdorAppDate()%>";
    	</script>
		<%		
	}
 // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tPGrpEdorMainUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
//out.println("<script language=javascript>");;
//out.println("top.close();");
//out.println("</script>");
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>