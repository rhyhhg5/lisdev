//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function verify()
{
  for (i=0; i<LoanGrid.mulLineCount; i++)
  {
    if (LoanGrid.getRowColData(i, 5) == "")
    {
      continue; 
    }
    else
    {
      if (parseFloat(LoanGrid.getRowColData(i, 5)) > LoanGrid.getRowColData(i, 2))
      {
        alert("第" + (i+1) + "行还款大于借款，请确认金额！");
        return false;
      }
    }
  }
  
  return true;
}

function edorTypeRFSave()
{
  if (!LoanGrid.checkValue()) 
  {
    return false; 
  }
  
  if (!verify()) 
  {
    return false; 
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result, loanResult)
{ 
  try { showInfo.close(); } catch (e) {}
  
  var tTransact=fm.all('fmtransact').value;
	if (FlagStr=="Success" && tTransact=="QUERY||MAIN")
	{
		var tArr = decodeEasyQueryResult(Result, 0);
		
		fm.all('RiskCode').value = tArr[0][7];
		fm.all('GrpNo').value = tArr[0][15];
		fm.all('GrpName').value = tArr[0][17];
		fm.all('CValidate').value = tArr[0][53];
		fm.all('PayToDate').value = tArr[0][58];
		fm.all('Prem').value = tArr[0][62];
		fm.all('Amnt').value = tArr[0][63];
    
		var iArray;
		//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
		//保存查询结果字符串
	 	turnPage.strQueryResult  = loanResult;
		//使用模拟数据源，必须写在拆分之前
		turnPage.useSimulation   = 1;  
  
		//查询成功则拆分字符串，返回二维数组
		var tArr = decodeEasyQueryResult(turnPage.strQueryResult, 0);
		
		//如果返回无数据
		if (tArr == null) {
		  LoanGrid.delBlankLine();  
		  return null;
		}
		
		turnPage.arrDataCacheSet =chooseArray(tArr,[1, 16, 6, 11, 8]);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	 	turnPage.pageDisplayGrid = LoanGrid; 
	  
	  //设置查询起始位置
	 	turnPage.pageIndex       = 0;
	 	//在查询结果数组中取出符合页面显示大小设置的数组
  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
		//调用MULTILINE对象显示查询结果
   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	}
	else
	{  
           var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
           showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
}

function returnParent()
{
	top.close();
}


