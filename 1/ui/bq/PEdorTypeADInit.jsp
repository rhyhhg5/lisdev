<%
//PEdorTypeACInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">  
//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divLPAppntIndDetail.style.left=ex;
  	divLPAppntIndDetail.style.top =ey;
   	detailQueryClick();
}

function initInpBox()
{ 
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
  	
  	if(flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  		fm.cancel.style.display = "none";
  	}

    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    //fm.all('CustomerNo').value = '';
    fm.all('CustomerNo').value = top.opener.fm.all('CustomerNoBak').value;
    fm.all('Name').value = '';
    fm.all('IDType').value = '';
    fm.all('IDNo').value = '';
    fm.all('Sex').value ='';
    fm.all('Birthday').value = '';

    //制定汉化
    showOneCodeName("EdorCode", "EdorTypeName");    
}
function initPostalAddress(){
	var ContNo = top.opener.fm.all('ContNo').value;
	var EdorNo = top.opener.fm.all('EdorNo').value;
	var CustomerNo = top.opener.fm.all('CustomerNoBak').value;
	var isInsuFlag = false;
	var AddressNo;
    var EdorType = top.opener.fm.all('EdorType').value;
	var isInsuSql = "select 1 from LCCont where ContNo='"+ ContNo +"' and appntno='"+ CustomerNo + "' ";
	var isAppnt = easyExecSql(isInsuSql, 1, 0);
	if(isAppnt){//投保人
		//先查P表
		var LPAppntSql = "select AddressNo from LPAppnt where ContNo = '" + ContNo +"' and EdorNo = '" + EdorNo + "' and EdorType = '"+ EdorType +"'";
		AddressNo = easyExecSql(LPAppntSql, 1, 0);
		if(AddressNo == '' || AddressNo == null){
			//P表没有再查C表
			var LCAppntSql = "select AddressNo from LCAppnt where ContNo = '" + ContNo +"'";
			AddressNo = easyExecSql(LCAppntSql, 1, 0);
		}
	}else{//被保险人
		//先查P表
		var LPInsuredSql = "select AddressNo from LPInsured where ContNo = '" + ContNo +"' and EdorNo = '" + EdorNo + "' and EdorType = '"+ EdorType +"'";
		AddressNo = easyExecSql(LPInsuredSql, 1, 0);
		if(AddressNo == '' || AddressNo == null){
			//P表没有再查C表
			var LCInsuredSql = "select AddressNo from LCInsured where ContNo = '" + ContNo +"'";
			AddressNo = easyExecSql(LCInsuredSql, 1, 0);
		}
	}
	//先查P表
	var lpaddressSql = "select PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity,PostalAddress " 
			+ "from LPAddress where CustomerNo = '" + CustomerNo + "' and EdorNo = '"+ EdorNo +"' and EdorType = '"+ EdorType 
			+ "' and AddressNo = '" + AddressNo + "'";
	var addressResult = easyExecSql(lpaddressSql, 1, 0);
	if(addressResult == '' || addressResult == null){
		//P表为空再查C表
		var lcaddressSql = "select PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity,PostalAddress " 
			+ "from LCAddress where CustomerNo = '" + CustomerNo 
			+ "' and AddressNo = '" + AddressNo + "'";
		addressResult = easyExecSql(lcaddressSql, 1, 0);
	}
	if(addressResult != '' && addressResult != null){		
		var provinceSql = "select CodeName from LDCode1 where codetype = 'province1' and code = '" + addressResult[0][0] + "'";
		var citySql = "select CodeName from LDCode1 where codetype = 'city1' and code = '" + addressResult[0][1] + "' and code1 = '" + addressResult[0][0] + "'";
		var countySql = "select CodeName from LDCode1 where codetype = 'county1' and code = '" + addressResult[0][2] + "' and code1 = '" + addressResult[0][1] + "'";
		var postalProvince = easyExecSql(provinceSql, 1, 0);
		var postalCity = easyExecSql(citySql, 1, 0);
		var postalCounty = easyExecSql(countySql, 1, 0);
		fm.all('POtherAddress').value = addressResult[0][5];
		if(postalProvince != null && postalCity != null && postalCounty != null){
			fm.all('Province3').value = addressResult[0][0];
			fm.all('City3').value = addressResult[0][1];
			fm.all('County3').value = addressResult[0][2];
			fm.all('appnt_PostalProvince3').value = postalProvince;
			fm.all('appnt_PostalCity3').value = postalCity;
			fm.all('appnt_PostalCounty3').value = postalCounty;
			fm.all('appnt_PostalStreet3').value = addressResult[0][3];
			fm.all('appnt_PostalCommunity3').value = addressResult[0][4];
		}
	}

	
}                               

function initForm()
{
  try
 {
    initInpBox();
    initLCContGrid();
    initQuery();  
    showAllCodeName();
    initPostalAddress();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

// 信息列表的初始化
function initLCAppntIndGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";    	//列名1
    iArray[1][1]="150px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="客户号";         			//列名2
    iArray[2][1]="150px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="客户姓名";         			//列名8
    iArray[3][1]="100px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="投保人级别";         		//列名5
    iArray[4][1]="100px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
    
    LCAppntIndGrid = new MulLineEnter( "fm" , "LCAppntIndGrid" ); 
    //这些属性必须在loadMulLine前
    LCAppntIndGrid.mulLineCount = 10;   
    LCAppntIndGrid.displayTitle = 1;
    LCAppntIndGrid.canSel=1;
    LCAppntIndGrid.hiddenPlus = 1;
    LCAppntIndGrid.hiddenSubtraction = 1;
    LCAppntIndGrid.selBoxEventFuncName ="reportDetailClick";
    LCAppntIndGrid.loadMulLine(iArray);  
    LCAppntIndGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert(ex);
  }
}

function initLCContGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";    	//列名1
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();                                                       
    iArray[2][0]="联系地址";    	//列名1                                              
    iArray[2][1]="200px";            		//列宽                                   
    iArray[2][2]=100;            			//列最大值                               
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许    
                                                                             
    iArray[3]=new Array();                                                       
    iArray[3][0]="邮政编码";    	//列名1                                              
    iArray[3][1]="50px";            		//列宽                                   
    iArray[3][2]=100;            			//列最大值                               
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    LCContGrid = new MulLineEnter( "fm" , "LCContGrid" ); 
    //这些属性必须在loadMulLine前
    LCContGrid.mulLineCount = 0;   
    LCContGrid.displayTitle = 1;
    LCContGrid.canChk=1;
    LCContGrid.hiddenPlus = 1;
    LCContGrid.hiddenSubtraction = 1;
    LCContGrid.selBoxEventFuncName ="reportDetailClick";
    LCContGrid.loadMulLine(iArray);  
    LCContGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert(ex);
  }
}


function detailQueryClick()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
	fm.all('fmtransact').value = "QUERY||DETAIL";
	
	var tSel=LCAppntIndGrid.getSelNo();
	
	if (tSel==0||tSel==null)
		alert("不能是空记录!");
	else
	{
		var tCustomerNo =LCAppntIndGrid.getRowColData(tSel-1,2);
//		alert(tCustomerNo);
		fm.all('CustomerNo').value =tCustomerNo;
		//showSubmitFrame(mDebug);
		fm.submit();
	}
}
function initDiv()
{
  divLPAppntIndDetail.style.display ='none';
  divDetail.style.display='none';
}

</script>