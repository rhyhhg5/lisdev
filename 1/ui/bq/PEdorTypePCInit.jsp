<%
//PEdorTypeIAInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<script language="JavaScript">  
function initForm()
{
  try
  {
    initInpBox();
    initPolGrid();
    queryPolInfo();
  }
  catch (ex)
  {
    alert("初始化界面错误!");
  }
}

function initInpBox()
{ 
  fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
  fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
  fm.all('ContNo').value = top.opener.fm.all('ContNoBak').value;
  fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
  showOneCodeName("EdorCode", "EdorTypeName");  
}    

function initPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            	//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="险种号";
    iArray[1][1]="60px";
    iArray[1][2]=100;
    iArray[1][3]=3;
    
    iArray[2]=new Array();
    iArray[2][0]="险种序号";
    iArray[2][1]="60px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="险种代码";
    iArray[3][1]="60px";
    iArray[3][2]=100;
    iArray[3][3]=0;
  
    iArray[4]=new Array();
    iArray[4][0]="险种名称";
    iArray[4][1]="250px";
    iArray[4][2]=100;
    iArray[4][3]=0;
    iArray[4][4]="riskcode";
    
    iArray[5]=new Array();
    iArray[5][0]="期交保费";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=0;        
    
    iArray[6]=new Array();
    iArray[6][0]="生效日期";
    iArray[6][1]="80px";
    iArray[6][2]=100;
    iArray[6][3]=0;
    
    iArray[7]=new Array();
    iArray[7][0]="交至日期";
    iArray[7][1]="80px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    
    iArray[8]=new Array();
    iArray[8][0]="交费频次";
    iArray[8][1]="80px";
    iArray[8][2]=100;
    iArray[8][3]=2;
    iArray[8][4]="payintv";
    
    PolGrid = new MulLineEnter("fm", "PolGrid"); 
    PolGrid.mulLineCount = 0;   
    PolGrid.displayTitle = 1;
    PolGrid.canChk = 0;
    PolGrid.hiddenPlus = 1;
    PolGrid.hiddenSubtraction = 1;
    PolGrid.loadMulLine(iArray);
  }
  catch (ex)
  {
    alert(ex);
  }
}
</script>