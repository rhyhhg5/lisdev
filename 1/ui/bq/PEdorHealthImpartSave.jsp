<%
//程序名称：PEdorHealthImpartSave.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  String flag = "";
  String content = "";
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  
  String edorNo = request.getParameter("EdorNo");
  String edorType = request.getParameter("EdorType");
  String contNo = request.getParameter("ContNo");
  String insuredNo = request.getParameter("InsuredNo");
  
  String[] impartCode = request.getParameterValues("ImpartCode");   
  String[] impartContent = request.getParameterValues("ImpartContent");  

	try
	{
		LPCustomerImpartSet tLPCustomerImpartSet = new LPCustomerImpartSet();
		for (int i = 0; i < impartCode.length; i++)
		{
			LPCustomerImpartSchema tLPCustomerImpartSchema = new LPCustomerImpartSchema();
			tLPCustomerImpartSchema.setImpartCode(impartCode[i]);
			tLPCustomerImpartSchema.setImpartContent(impartContent[i]);
			tLPCustomerImpartSet.add(tLPCustomerImpartSchema);
		}
	
		LPCustomerImpartParamsSet tLPCustomerImpartParamsSet = new LPCustomerImpartParamsSet();
		for (int i = 0; i < impartCode.length; i++)
		{
			String[] detail = request.getParameterValues("Detail" + (i + 1));
			String[] impartParamName = request.getParameterValues("ImpartParamName" + (i + 1));
			for (int j = 0; j < detail.length; j++)
			{
			
				String paramName = null;
				if ((impartParamName != null) && (impartParamName.length > j))
				{
					paramName = impartParamName[j];
				}
				LPCustomerImpartParamsSchema tLPCustomerImpartParamsSchema = new LPCustomerImpartParamsSchema();
				tLPCustomerImpartParamsSchema.setImpartCode(impartCode[i]);
				tLPCustomerImpartParamsSchema.setImpartParamNo(String.valueOf(j + 1));
				tLPCustomerImpartParamsSchema.setImpartParamName(paramName);
				tLPCustomerImpartParamsSchema.setImpartParam(detail[j]);
				tLPCustomerImpartParamsSet.add(tLPCustomerImpartParamsSchema);
			}
		}
		
		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
		tLPEdorItemSchema.setEdorNo(edorNo);
		tLPEdorItemSchema.setEdorType(edorType);
		tLPEdorItemSchema.setContNo(contNo);
		tLPEdorItemSchema.setInsuredNo(insuredNo);
		
	  VData data = new VData();
	  data.add(gi);
	  data.add(tLPCustomerImpartSet);
	  data.add(tLPCustomerImpartParamsSet);
	  data.add(tLPEdorItemSchema);
	  EdorCustomerImpartUI tEdorCustomerImpartUI = new EdorCustomerImpartUI();
	  if (!tEdorCustomerImpartUI.submitData(data))
	  {
	    flag = "Fail";
	    content = "数据保存失败！原因是：" + tEdorCustomerImpartUI.getError();
	  }
	  else
	  {
	    flag = "Succ";
	    content = "数据保存成功！";
	  }
	}
	catch (Exception e)
	{
		e.printStackTrace();
		content = "程序异常！" + e.toString();
	}
	content = PubFun.changForHTML(content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>