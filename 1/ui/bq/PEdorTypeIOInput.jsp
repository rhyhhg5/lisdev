<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeIO.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeIOInit.jsp"%>
  
  <title>被保人重要资料变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeIOSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 申请批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>
     
      <TD class = title > 合同保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE> 
<!--
   <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
      </td>
      <td class= titleImg>
        被保人信息
      </td>
   </tr>
   </table>
	    	<table class = common>
  <tr class = common>
  	<td class= title>
        可按输入条件查询:
     </td> 
  	<td class = title>
  		个人保单号
  		</td>
  	<td class = input>
  		<input class = common  name= ContNo>
      </TD>
     <td class = title>
  		个人客户号
  		</td>
  	<td class = input>
  		<input class = common  name= CustomerNo1>
      </TD>
    <td class = input>
    	      <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="CondQueryClick();"> 
		</td>  
     <tr>
     </table>
    <Div  id= "divLPInsured" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsuredGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class = cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class = cssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
 --> 
 <Div  id= "divLPInsuredDetail" style= "display:''">
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      </td>
      <td class= titleImg>
        被保人详细信息
      </td>
   </tr>
   </table>
  	<Div  id= "divDetail" style= "display: none">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
          <TD  class= title>
            客户姓名
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Name >
            <Input class= hidden type=hidden readonly name=NameBak >
          </TD>
		</TR>
         <TR  class= common>
          <TD CLASS=title>
			  职业代码 
			</TD>
			<TD CLASS=input COLSPAN=1>
			  <Input NAME=OccupationCode VALUE="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('OccupationCode', [this,OccupationType],[0,2]);" onkeyup="return showCodeListKey('OccupationCode', [this,OccupationType],[0,2]);" verify="职业代码|code:OccupationCode" >
			  <Input NAME=OccupationCodeBak class= hidden type=hidden>
			</TD>
			<TD CLASS=title>
			  职业类别 
			</TD>
			<TD CLASS=input COLSPAN=1>
			  <Input NAME=OccupationType VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=10 verify="被保人职业类别|code:OccupationType" >
			  <Input NAME=OccupationTypeBak class= hidden type=hidden>
			</TD>
          </TR>
          
      </table>
      
      <Div  id= "divDetail2" style= "display: none">
      	<TR  class= common>
		  <TD  class= title>
			性别
		  </TD>
		  <TD  class= input>
			 <Input class="code" name=Sex verify="被保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
            <Input class= hidden type=hidden readonly name=SexBak >
		  </TD>
		  <TD  class= title>
		   出生日期
		  </TD>
		  <TD  class= input>
			<Input class= common name=Birthday >
            <Input class= hidden type=hidden readonly name=BirthdayBak >
		  </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class= code name="IDType" verify="被保险人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
            <Input class= hidden type=hidden readonly name=IDTypeBak >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name=IDNo >
            <Input class= hidden type=hidden readonly name=IDNoBak >
         </TD>
        </TR>       
          <TR class = common>
           <TD  class= title>
            户籍
          </TD>
          <TD  class= input>
          <input class="code" name="NativePlace" verify="被保人户籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('nativePlace',[this]);">
          </TD>
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
 						<Input class="code" name="Marriage"  ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);" >
          </TD>
         </TR> 
      </Div>
      
      <table class = common>
		<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= cssButton type=Button value="保存申请" onclick="edorTypeIOSave()">
       		 <Input class= cssButton type=Button value="重  置" onclick="edorTypeIOReturn()">
     	 </TD>
		 <!--TD  class= input width="26%"> 
			<Div id ="divGetEndorse" style="display:''">
				<Input class = common type=Button value="费用明细" onclick="GetEndorseQuery()">
	    	</Div>
	     </TD-->
     	 </TR>
     	</table>
    </Div>
	</Div>
	  
	  <br>
	  <hr></hr>
		<Input type=Button value="返  回" class = cssButton onclick="returnParent()">
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="ContType" name="ContType">
		<input type=hidden name="EdorAcceptNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
