<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeIA.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeIAInit.jsp"%>
  
  <title>投保人变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeIASubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
     
      <TD class = title > 集体保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpPolNo>
      </TD> 
    </TR>
  </TABLE> 

   <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPAppntInd);">
      </td>
      <td class= titleImg>
        原投保人信息
      </td>
   </tr>
   </table>
	 

  	<Div  id= "divPreDetail" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            单位编码
          </TD>
          <TD  class= input>
            <Input readonly class= "readonly" readonly name=	GrpNo   >
          </TD>
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input readonly class= "readonly"  readonly name= GrpName >
          </TD>
        </TR>
         </table>
       <table class = common> 
	       <TR  class= common>
	          <TD  class= title>
	            单位地址编码	
	          </TD>
	          <TD  class= input>
								 <Input readonly class= common name=GrpAddressCode  >
	          </TD>
	          <TD  class= title>
	           单位地址	
	          </TD>
	          <TD  class= input>
	            <Input readonly class= common name=GrpAddress  >
	          </TD>
	           <TD  class= title>
            客户组号码	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=GrpGroupNo >
          </TD>
	        </TR>
	        </table>
       <table class = common> 
	      <TR  class= common>
          <TD  class= title>
            单位邮编	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=GrpZipCode >
          </TD>
          <TD  class= title>
            行业分类	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=BusinessType >
          </TD>
           <TD  class= title>
             单位性质		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=GrpNature >
          </TD>
       </TR>
      
        <TR  class= common>
          <TD  class= title>
            单位总人数	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=Peoples2 >
          </TD>
          <TD  class= title>
            单位传真	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=Fax >
          </TD>
          <TD  class= title>
            单位电话		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=Phone >
          </TD>
          
       </TR>
        
       <TR class = common>
           <TD  class= title>  
							注册资本	
          </TD>
          <TD  class= input>
          <input readonly class=common name=RgtMoney >
          </TD>
          <TD  class= title>
            资产总额	
          </TD>
          <TD  class= input>
          <input readonly class=common name=Asset >
          </TD>
          <TD  class= title>
            净资产收益率	
          </TD>
          <TD  class= input>
          <input readonly class=common name=NetProfitRate >
          </TD>
       </TR> 
       
       <TR  class= common>
          <TD  class= title>
            主营业务	
          </TD>
          <TD  class= input>
            <Input readonly class=common name=MainBussiness >
          </TD>
          <TD  class= title>
            法人	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=Corporation >
          </TD>
          <TD  class= title>
            机构分布区域	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=ComAera >
          </TD>
       </TR>
       
        <TR  class= common>
          <TD  class= title>
            成立日期		
          </TD>
          <TD  class= input>
            <Input readonly class=common name=FoundDate >
          </TD>
          <TD  class= title>
            负责人	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=Satrap >
          </TD>
           <TD  class= title>
            公司Email	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=EMail >
          </TD>
        </TR>
        
        <TR  class= in>
          <TD  class= title>
            付款方式	
          </TD>
          <TD  class= input>
            <Input readonly class=common name=GetFlag >
          </TD>
          <TD  class= title>
            银行编码	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=BankCode >
          </TD>
          <TD  class= title>
            银行帐号	
          </TD>
          <TD  class= input>
            <Input readonly class=common name=BankAccNo>
          </TD>
         
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            联系人1		
          </TD>
          <TD  class= input>
            <Input readonly class=common name=LinkMan1 >
          </TD>
          <TD  class= title>
            所在部门		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=Department1 >
          </TD>
           <TD  class= title>
            职务	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=HeadShip1 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系电话			
          </TD>
          <TD  class= input>
            <Input readonly class=common name=Phone1 >
          </TD>
          <TD  class= title>
            E_Mail			
          </TD>
          <TD  class= input>
            <Input readonly class= common name=E_Mail1 >
          </TD>
           <TD  class= title>
            传真		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=Fax1 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系人2		
          </TD>
          <TD  class= input>
            <Input readonly class=common name=LinkMan2 >
          </TD>
          <TD  class= title>
            所在部门		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=Department2 >
          </TD>
           <TD  class= title>
            职务	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=HeadShip2 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系电话			
          </TD>
          <TD  class= input>
            <Input readonly class=common name=Phone2 >
          </TD>
          <TD  class= title>
            E_Mail			
          </TD>
          <TD  class= input>
            <Input readonly class= common name=E_Mail2 >
          </TD>
           <TD  class= title>
            传真		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=Fax2 >
          </TD>
        </TR>
        
      </table>
        	
  	</div>
    
    <div id= "divQuery" style= "display: ''">
	  <table>
	   <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
	      </td>
	      <td class= titleImg>
	        变更投保人查询
	      </td>
	   </tr>
	   </table>
	   <Div id= "divQuery" style = "display: ''">
	     <table>
	     <tr class = common>
	         <td class= title>
	            集体号码
	         </td>
	         <td class= input>
	            <Input readonly class= common  name=QueryGrpNo >
	         </td>
	     </tr>
	     <tr class = common>    
	         <td >
	         <Input class= common type=Button value="查询" onclick="GrpDetailQuery()">
	         </td>
	         <td >
	         <Input class= common type=Button value="新 增" onclick="GrpPersonNew()"> 
	         </td>
	         
	     </tr>
	     </table>
	   </div>
    </div>
    
       
 	<Div  id= "divPostDetail" style= "display: none">
	<TABLE class=common>
      	<TR  class= common>
          <TD  class= title>
            单位编码
          </TD>
          <TD  class= input>
            <Input readonly  class= "readonly" readonly name=	PostGrpNo   >
          </TD>
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input readonly  class= "readonly"  readonly name= PostGrpName >
          </TD>
        </TR>
   </table>
   <table class = common> 
	       <TR  class= common>
	          <TD  class= title>
	            单位地址编码	
	          </TD>
	          <TD  class= input>
								 <Input class= common name=PostGrpAddressCode  >
	          </TD>
	          <TD  class= title>
	           单位地址	
	          </TD>
	          <TD  class= input>
	            <Input readonly  class= common name=PostGrpAddress  >
	          </TD>
	           <TD  class= title>
            客户组号码	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostGrpGroupNo >
          </TD>
	        </TR>
	        </table>
       <table class = common> 
	      <TR  class= common>
          <TD  class= title>
            单位邮编	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostGrpZipCode >
          </TD>
          <TD  class= title>
            行业分类	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostBusinessType >
          </TD>
           <TD  class= title>
             单位性质		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostGrpNature >
          </TD>
       </TR>
      
        <TR  class= common>
          <TD  class= title>
            单位总人数	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostPeoples2 >
          </TD>
          <TD  class= title>
            单位传真	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostFax >
          </TD>
          <TD  class= title>
            单位电话		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostPhone >
          </TD>
          
       </TR>
        
       <TR class = common>
           <TD  class= title>  
							注册资本	
          </TD>
          <TD  class= input>
          <input readonly class=common name=PostRgtMoney >
          </TD>
          <TD  class= title>
            资产总额	
          </TD>
          <TD  class= input>
          <input readonly class=common name=PostAsset >
          </TD>
          <TD  class= title>
            净资产收益率	
          </TD>
          <TD  class= input>
          <input readonly class=common name=PostNetProfitRate >
          </TD>
       </TR> 
       
       <TR  class= common>
          <TD  class= title>
            主营业务	
          </TD>
          <TD  class= input>
            <Input readonly class=common name=PostMainBussiness >
          </TD>
          <TD  class= title>
            法人	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostCorporation >
          </TD>
          <TD  class= title>
            机构分布区域	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostComAera >
          </TD>
       </TR>
       
        <TR  class= common>
          <TD  class= title>
            成立日期		
          </TD>
          <TD  class= input>
            <Input readonly class=common name=PostFoundDate >
          </TD>
          <TD  class= title>
            负责人	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostSatrap >
          </TD>
           <TD  class= title>
            公司Email	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostEMail >
          </TD>
        </TR>
        
        <TR  class= in>
          <TD  class= title>
            付款方式	
          </TD>
          <TD  class= input>
            <Input readonly class=common name=PostGetFlag >
          </TD>
          <TD  class= title>
            银行编码	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostBankCode >
          </TD>
          <TD  class= title>
            银行帐号	
          </TD>
          <TD  class= input>
            <Input readonly class=common name=PostBankAccNo>
          </TD>
         
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            联系人1		
          </TD>
          <TD  class= input>
            <Input readonly class=common name=PostLinkMan1 >
          </TD>
          <TD  class= title>
            所在部门		
          </TD>
          <TD  class= input>
            <Input class= common name=PostDepartment1 >
          </TD>
           <TD  class= title>
            职务	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostHeadShip1 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系电话			
          </TD>
          <TD  class= input>
            <Input readonly class=common name=PostPhone1 >
          </TD>
          <TD  class= title>
            E_Mail			
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostE_Mail1 >
          </TD>
           <TD  class= title>
            传真		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostFax1 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系人2		
          </TD>
          <TD  class= input>
            <Input readonly class=common name=PostLinkMan2 >
          </TD>
          <TD  class= title>
            所在部门		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostDepartment2 >
          </TD>
           <TD  class= title>
            职务	
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostHeadShip2 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系电话			
          </TD>
          <TD  class= input>
            <Input readonly class=common name=PostPhone2 >
          </TD>
          <TD  class= title>
            E_Mail			
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostE_Mail2 >
          </TD>
           <TD  class= title>
            传真		
          </TD>
          <TD  class= input>
            <Input readonly class= common name=PostFax2 >
          </TD>
        </TR>
		
		
	  </TABLE>
		
		
      <table class = common>
			<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeIASave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="取消" onclick="edorTypeIAReturn()">
     	 </TD>
     	 </TR>
     	</table>
    </Div>
	</Div>
	
	  <Input type=Button value="返回" onclick="returnParent()">
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
