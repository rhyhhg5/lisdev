<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LMInsuAccRateSave.jsp
//程序功能：万能险帐户结算利率录入
//创建日期：2007-12-12
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  
  transact = request.getParameter("fmtransact");
  
  LMInsuAccRateSchema tLMInsuAccRateSchema = new LMInsuAccRateSchema();
  tLMInsuAccRateSchema.setRiskCode(request.getParameter("RiskCode"));
  tLMInsuAccRateSchema.setInsuAccNo(request.getParameter("InsuAccNo"));
  tLMInsuAccRateSchema.setRateType(request.getParameter("RateType"));
  tLMInsuAccRateSchema.setRateIntv(request.getParameter("RateIntv"));
  tLMInsuAccRateSchema.setRateIntvUnit(request.getParameter("RateIntvUnit"));
  tLMInsuAccRateSchema.setRate(request.getParameter("Rate"));
  String tBalaMonth = request.getParameter("BalaMonth");//BalaMonth
  
  LMInsuAccRateSchema dLMInsuAccRateSchema = new LMInsuAccRateSchema();
  dLMInsuAccRateSchema.setRiskCode(request.getParameter("DRiskCode"));
  dLMInsuAccRateSchema.setInsuAccNo(request.getParameter("DInsuAccNo"));
  dLMInsuAccRateSchema.setRateType(request.getParameter("DRateType"));
  dLMInsuAccRateSchema.setRateIntv(request.getParameter("DRateIntv"));
  dLMInsuAccRateSchema.setRateIntvUnit(request.getParameter("DRateIntvUnit"));
  dLMInsuAccRateSchema.setRate(request.getParameter("DRate"));
  String dBalaMonth = request.getParameter("DBalaMonth");//BalaMonth
  LMInsuAccRateUI tLMInsuAccRateUI = new LMInsuAccRateUI();
  try
  {
  // 准备传输数据 VData
    VData tVData = new VData();
  	TransferData tTransferData = new TransferData();
  	tVData.add(tG);
  	tTransferData.setNameAndValue("tLMInsuAccRateSchema",tLMInsuAccRateSchema);
  	tTransferData.setNameAndValue("tBalaMonth",tBalaMonth);
  	System.out.println("tBalaMonth=" + tBalaMonth);
  	tTransferData.setNameAndValue("dLMInsuAccRateSchema",dLMInsuAccRateSchema);
  	tTransferData.setNameAndValue("dBalaMonth",dBalaMonth);
  	System.out.println("dBalaMonth=" + dBalaMonth);
  	tVData.add(tTransferData);
    tLMInsuAccRateUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "")
  { 
    tError = tLMInsuAccRateUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = "操作成功! ";
      FlagStr = "Success";
    }
    else                                                                           
    {
      Content = "操作失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "<%=transact%>");
</script>
</html>
