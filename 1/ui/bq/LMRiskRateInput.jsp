<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2016-05-16
		//创建人  ：
		//更新记录：  更新人    更新日期     更新原因/内容
		String tProjectNo = request.getParameter("ProjectNo");
		String transact = request.getParameter("transact");
		String tLookFlag = request.getParameter("LookFlag"); 
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="LMRiskRate.js"></script>
		<%@include file="LMRiskRateInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>"; 
  		var tProjectNo = "<%=tProjectNo%>";
		var transact = "<%=transact%>";
		var tLookFlag = "<%=tLookFlag%>";
  		</script>
	</head>
<body onload="initForm();">
		<form action="LMRiskRateSave.jsp" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td class=titleImg>
						风险综合评级录入
					</td>
				</tr>
			</table>
				<tr class=common>
					<td class="title">年度</td>
					<td class="input"><Input class=common id=Year name="Year"  onblur="checkYear(this)" ><font style=color:red;valign:right> *</font> </td>
					<td class="title">季度</td>
					<td class="input"><input class="codeNo" name="Quarter"  CodeData="0|请选择^1|第一季度^2|第二季度^3|第三季度^4|第四季度" readOnly
						ondblclick="return showCodeListEx('Quarter',[this,getQuarterName],[0,1]);"  
						onkeyup="return showCodeListEx('Quarter',[this,getQuarterName],[0,1]);"><Input class="codeName" name="getQuarterName" readonly>
						<font style=color:red;valign:right> *</font>
					</td>
					<td class="title">风险综合评级</td>
					<td class="input"><input class="codeNo" name="RiskRate" value="" CodeData="0|请选择^1|A^2|B^3|C^4|D" readOnly
						ondblclick="return showCodeListEx('RiskRate',[this,getRiskRateName],[0,1]);"  
						onkeyup="return showCodeListEx('RiskRate',[this,getRiskRateName],[0,1]);"><Input class="codeName" name="getRiskRateName"  readonly>
						<font style=color:red;valign:right> *</font>
					</td>
				</tr><br/><br/>

			 <table>
				<input type=hidden id="fmtransact" name="fmtransact">
				<td class=button>
					<input type="button" class=cssButton value="保存" id = "AddID" name="AddID" onclick="submitForm()">&nbsp;&nbsp;&nbsp;
				</td>

			</table><br/>

			<table>
				<tr>
					<td class=titleImg>风险综合评级信息</td>
				</tr>
			</table>

			<Div id="divRiskRateGrid" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1><span id="spanRiskRateGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">
				<table>
					<tr align=center>
						<td class=button width="10%">
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
						</td>
					</tr>
				</table>
			</Div><br/>
			<table>
				<tr>
					<td class=titleImg>风险综合评级信息修改</td>
				</tr>
			</table>
			<Div>
				<tr class=common>
					<td class="title">年度</td><td class="input"><Input class=readonly readonly name="mYear" ></td>
					<td class="title">季度</td><td class="input"><input class=readonly readonly name="mQuarter"  ></td>
					<td class="title">风险综合评级</td>
					<td class="input">
						<input class="codeNo" name="mRiskRate"  CodeData="0|请选择^1|A^2|B^3|C^4|D" readOnly
						ondblclick="return showCodeListEx('mRiskRate',[this,RiskRateName],[0,1]);"  
						onkeyup="return showCodeListEx('mRiskRate',[this,RiskRateName],[0,1]);"><Input class="codeName" name="RiskRateName" readonly>
					</td>
				</tr>
			</Div><br/>
				<td class=button>
			 	 <input type="button" class=cssButton value="修改" id = "UpdateID" name="UpdateID" onclick="updateClick()">
				</td><br/>
			<INPUT TYPE=hidden name=tYear value="">
 			<INPUT TYPE=hidden name=tQuarter value="">
 			<INPUT TYPE=hidden name=tRiskRateName value="">
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
