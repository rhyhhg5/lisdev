<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/currentTime.jsp"%>
<html>    
<%
//程序名称：bqWTLis.jsp
//程序功能：保全犹豫期退保统计
//创建日期：2006-2-8
//创建人  ：yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   
<script>

	var comCode = "<%=tG.ComCode%>";
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="bqWTLis.js"></SCRIPT> 

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>犹豫期撤保险种分析</title>
</head>
		<body>   
			<table>
					<tr> 
					<td class= common> 
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divbqWorkLis);"> 
					</td>
					<td class= titleImg>犹豫期撤保险种分析</td>
					</tr>
			</table>
					<Div  id= "divbqWorkLis" style= "display: ''" >  
					<form action="./bqWTLisSub.jsp" method=post name=fm target="f1print">
						<INPUT VALUE="生成报表" class="cssButton" TYPE="button" onclick="submitForm()">&nbsp;

						
							<table class= common border=0 width=100%>		
							<TR  class= common>	
							<TD class= title> 机构 </TD>
							<TD class= input>
							<input class="code" name="organcode" type="hidden">
							<input class="code" name="organname" readonly ondblclick="return showCodeList('comcode', [this,organcode], [1,0]);" onkeyup="return showCodeListKey('comcode', [this,organcode], [1,0]);" verify="机构|&code:comcode">
							</TD>
							<TD colspan=3> 
    					<INPUT VALUE="重 置" TYPE=button style="width:100"Class="cssButton" name="query" onclick="clearData();">  
   						</TD>
							</TR>
							<TR  class= common>			
							<TD  class= title>承保起期</TD>
							<TD  class= input> 
							<Input name=StartDate  style="width:160" class='coolDatePicker' dateFormat='short' verify="统计起期|Date" > </TD> 
							<TD  class= title>承保止期</TD>
							<TD  class= input > 
							<Input name=EndDate style="width:160" class='coolDatePicker' dateFormat='short' verify="统计止期|Date" > </TD> 
							
							</TR>
							   
						</table>

				<input type="hidden" name=op value="">
			</form>
		</Div>  
		


  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 