<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BqUpdateGetdateInit.jsp
//程序功能：保全应付日期修改
//创建日期：20170426
//创建人  ：ys
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）


  function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('EdorAcceptNo').value = '';
  }
  catch(ex)
  {
    alert("在BqUpdateGetdateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在BqUpdateGetdateInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                         

function initForm()
{
  try
  {
    initInpBox();   
    initSelBox();   
    initBQGrid();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initBQGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="给付凭证号";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="保全受理号";         		//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="业务类型";         		//列名
      iArray[3][1]="20px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="申请日期";         		//列名
      iArray[4][1]="30px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[5]=new Array();
      iArray[5][0]="结案日期";         		//列名
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="退费金额";         		//列名
      iArray[6][1]="40px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="应付日期";         		//列名
      iArray[7][1]="30px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="实付日期";         		//列名
      iArray[8][1]="30px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="银行在途";         		//列名
      iArray[9][1]="30px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      BQGrid = new MulLineEnter( "fm" , "BQGrid" ); 
      //这些属性必须在loadMulLine前
      BQGrid.mulLineCount =0;   
      BQGrid.displayTitle = 1;
      BQGrid.locked = 1;
      BQGrid.canSel = 1;
      BQGrid.hiddenPlus = 1;
      BQGrid.hiddenSubtraction = 1;
      BQGrid.canChk = 0;
      BQGrid.selBoxEventFuncName = "BQinit";
      BQGrid.loadMulLine(iArray);  
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>