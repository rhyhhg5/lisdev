<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeAC.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeACInit.jsp"%>
  <title>投保人重要资料变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeACSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class= "readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 集体保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpContNo>
      </TD>   
    </TR>
  </TABLE> 
  <br>
		<table>
		  <tr>
		    <td>
		    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpInfo);">
		    </td>
				<td class= titleImg>
					 投保单位资料
				</td>
		  </tr>
		</table>
    <Div  id= "divGrpInfo" style= "display: ''">
      <table  class= common>
       <TR>
          <TD  class= title8>
            单位客户号
          </TD>
          <TD  class= input8>
            <Input class="readonly" name=GrpNo readonly >
          </TD>  
          <TD class= title8>
            投保团体名称
          </TD>
          <TD  class= input8>
            <Input class=common name=GrpName elementtype=nacessary verify="投保团体名称|notnull&len<=150">
          </TD>   
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8  name=Phone elementtype=nacessary verify="联系电话|notnull&len<=18">
          </TD>  
        </TR>
        <TR>
          <TD class= title8>
            投保单位地址
          </TD>
          <TD  class= input8 colspan="7">
			<Input class="codeno" name=Province id=Province verify="投保单位地址|notnull"  ondblclick="return showCodeList('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1),changeAddress(1)" onkeyup="return showCodeListKey('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1),changeAddress(1);" onblur="return chagePostalAddress()" readonly><input class=codename style="width:90px" name=appnt_PostalProvince readonly=true elementtype=nacessary >省（自治区直辖市）
			<Input class="codeno" name=City verify="投保单位地址|notnull" ondblclick="return showCodeList('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1),changeAddress(2)" onkeyup="return showCodeListKey('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1),changeAddress(2);" onblur="return chagePostalAddress()" readonly><input class=codename style="width:90px" name=appnt_PostalCity readonly=true elementtype=nacessary>市
			<Input class="codeno" name=County verify="投保单位地址|notnull" ondblclick="return showCodeList('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1),changeAddress(3)" onkeyup="return showCodeListKey('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1),changeAddress(3);" onblur="return chagePostalAddress()" readonly><input class=codename style="width:90px" name=appnt_PostalCounty readonly=true elementtype=nacessary>县（区）
			<input  class= common8 style="width:300px" elementtype=nacessary name=appnt_DetailAdress verify="投保单位地址|notnull" onblur="return chagePostalAddress();"> 详细地址
	      </TD>	    
        </TR>
        <TR  class= common id="AppntPostalAddressID" style= "display: ''">
        	<TD  class= title>
            联系地址
          </TD>
          <TD class= input8 colspan="3">
            <Input class=common8 name=GrpAddress  elementtype=nacessary  verify="投保单位地址|notnull&len<=150" style="width:360pt" readonly>
          </TD>
          <TD class= title8>
 
          </TD>
          <TD class= input8 colspan="3">
          
          </TD> 
        </TR>
        <TR class= common>
           <TD  class= title8>
           		 邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8  name=GrpZipCode elementtype=nacessary  verify="邮政编码|notnull&zipcode">
          </TD>
          <TD  class= title8>
            	税务登记证号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=TaxNo>
          </TD>
          	  <TD  class= title8>
	          </TD>
	          <TD  class= input8>
	          </TD>  
        </TR>
        <TR class= common>
	          <TD  class= title8>
	            	法人姓名
	          </TD>
	          <TD  class= input8>
	            <Input class= common8  name=LegalPersonName>
	          </TD>     
	          <TD  class= title8>
	           	 法人身份证号
	          </TD>
	          <TD  class= input8>
	             <Input class= common8 name=LegalPersonIDNo>
	          </TD>
	         <TD  class= title8>
	          </TD>
	          <TD  class= input8>
	          </TD>
        </TR>
        <TR  class= common>
        	<TD  class= title8>
            行业性质
          </TD>
          <TD  class= input8>
            <Input class="codeNo" name=BusinessType verify="行业性质|notnull&code:BusinessType&len<=20" ondblclick="return showCodeList('BusinessType',[this,BusinessTypeName],[0,1]);" onkeyup="return showCodeListKey('BusinessType',[this,BusinessTypeName],[0,1]);"><Input class="codeName"  elementtype=nacessary  name=BusinessTypeName readonly >
          </TD>
          <TD  class= title8>
            企业类型
          </TD>
          <TD  class= input8>
            <Input class=codeNo name=GrpNature verify="企业类型|notnull&code:GrpNature&len<=10" ondblclick="showCodeList('GrpNature',[this,GrpNatureName],[0,1]);" onkeyup="showCodeListKey('GrpNature',[this,GrpNatureName],[0,1]);"><Input class="codeName" name=GrpNatureName readonly elementtype=nacessary>
          </TD>
          <TD  class= title8>
            员工总人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Peoples  elementtype=nacessary verify="单位总人数|notnull&int&len<=6">
          </TD>       
        </TR>
        <TR  class= common>
        	<TD  class= title8>
            在职人数
          </TD>
          <TD  class= input8>
            <Input class="common8" name=OnWorkPeoples  elementtype=nacessary verify="在职人数|notnull&int&len<=6">
          </TD>
          <TD  class= title8>
            退休人数
          </TD>
          <TD  class= input8>
            <Input class="common8" name=OffWorkPeoples verify="退休人数|int&len<=6">
          </TD>
          <TD  class= title8>
            其他人员数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=OtherPeoples verify="其他人员数|int&len<=6">
          </TD>       
        </TR>
        <TR  class= common>
          <TD  class= title8>
            联系人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8  name=LinkMan1  elementtype=nacessary verify="联系人姓名|notnull&len<=60">
          </TD>
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone1  elementtype=nacessary verify="联系人联系电话|notnull&len<=30">
          </TD>
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8  name=Fax1  verify="联系人传真|len<=30&Fax1">
          </TD> 
         </TR>
         <TR  class= common>    
          <TD  class= title8>
            电子邮箱
          </TD>
          <TD  class= input8 >
            <Input class= common8  name=E_Mail1 verify="联系人电子邮箱|len<=60&Email">
          </TD>           
          <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class=codeno name=IDType  CodeData="0|1^0|身份证^4|其他" ondblclick="return showCodeListEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);"><input class=codename name=IDTypeName readonly=true >
          </TD>
          <TD  class= title8>
            联系人证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=IDNo verify="联系人证件号码|NUM&len<=20" >
          </TD>                      
        </TR>
        <TR  class= common>
          <TD  class= title8>
            证件生效日期
          </TD>                  
          <TD  class= input8>            
            <Input class=coolDatePicker dateFormat="short" name=IDStartDate  verify="证件生效日期|date">        
          </TD>        
          <TD  class= title8>
            证件失效日期
          </TD>
          <TD  class= input8>
            <Input class=coolDatePicker dateFormat="short" name=IDEndDate  verify="证件失效日期|date"> 
            <input type="checkbox" name="IdNoValidity" class="box"  onclick="setIDLongEffFlag();">&nbsp;&nbsp;&nbsp;&nbsp;长期有效
            <Input class= common8  name=IDLongEffFlag type=hidden>
          </TD>
          <TD  class= title8>
            手机
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Mobile1 >
          </TD>
        </TR>
      </table>
      <br>
    </Div>
		<table class = common>
			<TR class =common>
				<TD width="26%"> 
				 <Input type=button class=cssButton name="save" value="保  存" onclick="edorTypeACSave()">
				 <Input type=button class=cssButton name="cancel" value="取  消" onclick="edorTypeACReturn()">
				 <Input type=Button class=cssButton name="goBack" value="返  回" onclick="returnParent()">
				</TD>
			</TR>
		</table>
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	 <input type=hidden id="addrFlag" name="addrFlag">
	 <Input type=hidden id="GrpAddressNo" name="GrpAddressNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
