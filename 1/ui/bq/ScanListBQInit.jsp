<%
//程序名称：ScanLisInit.jsp
//程序功能：扫描件查询
//创建日期：2006-11-20 9:14
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
  
<%

  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
  String curDate = CommonBL.decodeDate(PubFun.getCurrentDate());
%>       
<script language="JavaScript">
var operator = "<%=tGI.Operator%>";  //操作员编号
var manageCom = "<%=tGI.ManageCom%>";
var curDate = "<%=curDate%>";
var tContNo = "<%=request.getParameter("ContNo")%>";
var tPrtNo = "<%=request.getParameter("PrtNo")%>";
var tAppObj = "<%=request.getParameter("AppObj")%>";	//团个标记: I个,G团

//表单初始化
function initForm()
{
	try
	{
		initScanGrid();
		initInpBox();
		initElementtype();
	}
	catch(re)
	{
		alert("操作错误，请关闭上次登陆打开的页面后重试。");
	}
}

//扫描件列表初始化
function initScanGrid()
{                         
	var iArray = new Array();
	try
	{
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30";            		//列宽
    iArray[0][2]=200;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="扫描流水号";  
    iArray[1][1]="50px";   
    iArray[1][2]=200;     
    iArray[1][3]=0;
    iArray[1][21]="DocID";
    
    iArray[2]=new Array();
    iArray[2][0]="页数";  
    iArray[2][1]="25px";   
    iArray[2][2]=200;     
    iArray[2][3]=0;		
    iArray[2][21]="PageCode";		
    
    iArray[3]=new Array();
    iArray[3][0]="受理机构";         	  //列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="ManageCom";
    
    iArray[4]=new Array();
    iArray[4][0]="业务受理号";              //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="BussNo";
    
    iArray[5]=new Array();
    iArray[5][0]="扫描日期";              //列名
    iArray[5][1]="50px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="MakeDate";
    
    iArray[6]=new Array();
    iArray[6][0]="扫描时间";         	//列名
    iArray[6][1]="50px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="MakeTime";
    
    iArray[7]=new Array();
    iArray[7][0]="扫描人";         	//列名
    iArray[7][1]="50px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]= "Operator";
    
    iArray[8]=new Array();
    iArray[8][0]="扫描人机构";         	//列名
    iArray[8][1]="100px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]= "OperatorCom";
    
    iArray[9]=new Array();
    iArray[9][0]="投保人姓名";         	//列名
    iArray[9][1]="100px";            	//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[9][21]= "AppntName";
    
    
    iArray[10]=new Array();
    iArray[10][0]="保全类型";         	//列名
    iArray[10][1]="100px";            	//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[10][21]= "EdorType";
    
    ScanGrid = new MulLineEnter("fm", "ScanGrid"); 
    //这些属性必须在loadMulLine前
    ScanGrid.mulLineCount = 0;
    ScanGrid.displayTitle = 1;
    ScanGrid.locked = 1;
    ScanGrid.canSel = 0;
    ScanGrid.canChk = 0;
    ScanGrid.hiddenSubtraction = 1;
    ScanGrid.hiddenPlus = 1;
    ScanGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex.message);
	}
}

</script>