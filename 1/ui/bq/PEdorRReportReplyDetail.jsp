<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：PEdorRReportReplyDetail.jsp
//程序功能：生存调查报告回复
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
 	String tPolNo = "";
 	String tEdorNo = "";
	String tSerialNo = "";
	String tMissionID = "";
	String tSubMissionID = "";
	String tPrtSeq = "";
	String tContNo = "00000000000000000000";
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	try
	{
		tPolNo = request.getParameter( "PolNo" );
		tEdorNo = request.getParameter( "EdorNo" );
		tSerialNo = request.getParameter( "SerialNo" );
		tMissionID = request.getParameter( "MissionID" );
		tSubMissionID = request.getParameter( "SubMissionID" );
		tPrtSeq = request.getParameter( "PrtSeq" );
		System.out.println("---Exception:"+tPolNo+tSerialNo);
	}
	catch( Exception e1 )
	{
			System.out.println("---Exception:"+e1);

	}
%>
<script>
	var PolNo = "<%= tPolNo%>";
	var EdorNo = "<%= tEdorNo%>";
	var SerialNo = "<%= tSerialNo%>";
	var MissionID = "<%= tMissionID%>";
	var SubMissionID = "<%= tSubMissionID%>";
	var PrtSeq = "<%= tPrtSeq%>";
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head >
<title>生调件回复 </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="PEdorRReportReplyDetail.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PEdorRReportReplyDetailInit.jsp"%>
  
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./PEdorRReportReplyChk.jsp">
    <!-- 保单查询条件 -->
     <table>
    	<tr>
        	<td class=common>  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);"></td>
    		<td class= titleImg> 生存调查报告内容：</td>
    	</tr>
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanQuestGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    </div>
    <br> </br>
  <table width="121%" height="37%">
    <TR  class= common> 
      <TD width="100%" height="13%"  class= title> 报告内容 </TD>
    </TR>
    <TR  class= common>
      <TD height="87%"  class= title><textarea name="Content" cols="135" rows="10" class="common" readonly></textarea></TD>
    </TR>
  </table>
  <table width="121%" height="37%">
    <TR  class= common> 
      <TD width="100%" height="13%"  class= title> 回复内容 </TD>
    </TR>
    <TR  class= common>
      <TD height="87%"  class= input><textarea name="ReplyResult" cols="135" rows="10" class="common"></textarea></TD>
    </TR>
  </table>
  <p> 
    <!--读取信息-->
    <input type= "button" class= common name= "Reply" value="回复" onClick= "submitForm()">
    <input type= "hidden" name= "PolNo" value="">
    <input type= "hidden" name= "EdorNo" value="">
    <input type= "hidden" name= "SerialNo" value="">
    <input type= "hidden" name= "MissionID" value="">
    <input type= "hidden" name= "SubMissionID" value="">
    <input type= "hidden" name= "PrtSeq" value="">
    <input type= "hidden" name= "Type" value="">
    
  </p>
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>