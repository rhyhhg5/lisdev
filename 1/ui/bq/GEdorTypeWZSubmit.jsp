<% 
//程序名称：GEdorTypeWZSubmit.jsp
//程序功能：无名单增人保存
//创建日期：20060625
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  String flag;
  String content;
  String edorNo = request.getParameter("EdorNo");
  GlobalInput gi = (GlobalInput)session.getValue("GI");
  
  //集体批改信息
  LPGrpEdorItemSchema tLPGrpEdorItemSchema  = new LPGrpEdorItemSchema();
  tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
  tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
  tLPGrpEdorItemSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
  
  TransferData td = new TransferData();
  td.setNameAndValue("prem", request.getParameter("SumPremWZ"));
  td.setNameAndValue("peoples2", request.getParameter("Peoples2WZ"));
  td.setNameAndValue("contPlanCode", request.getParameter("ContPlanCode"));
  td.setNameAndValue("contNo", request.getParameter("ContNo"));
  
  VData data = new VData();
  data.add(gi);
  data.add(td);
  data.add(tLPGrpEdorItemSchema);
  
  GEdorWZDetailUI ui = new GEdorWZDetailUI();
  if(!ui.submitData(data, ""))
  {
    flag = "Fail";
    content = ui.mErrors.getErrContent();
  }
  else
  {
    flag = "Succ";
    content = "保存成功";
  }
  
  content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>