<%
//程序名称：PEdorTypeWTSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%             
    String FlagStr = "";
    String Content = "";
  
    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    String transact = request.getParameter("fmtransact");
  	GlobalInput tGlobalInput = (GlobalInput)session.getValue("GI");
  	
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String contNo = request.getParameter("ContNo");
	String reason = request.getParameter("reason_tb");
	
    String tContNo[] = request.getParameterValues("PolGrid1");
    String tInsuredNo[]= request.getParameterValues("PolGrid1");
    String tPolNo[]= request.getParameterValues("PolGrid2");
    String tChk[] = request.getParameterValues("InpPolGridChk");
    
    LPPolSet mLPPolSet= new LPPolSet();
    for (int i=0;i<tChk.length;i++)
    {
        if (tChk[i].equals("1"))
        {
            LPPolSchema tLPPolSchema=new LPPolSchema();
            tLPPolSchema.setEdorNo(edorNo);
            tLPPolSchema.setEdorType(edorType);
            tLPPolSchema.setContNo(contNo);
            tLPPolSchema.setInsuredNo(tInsuredNo[i]);
            tLPPolSchema.setPolNo(tPolNo[i]);
            mLPPolSet.add(tLPPolSchema);
        }
    }
	
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setContNo(contNo);
	tLPEdorItemSchema.setEdorType(edorType);
	//tLPEdorItemSchema.setGetMoney(getMoney);
	tLPEdorItemSchema.setReasonCode(reason);
	
	//体检费信息
	LPPENoticeSet tLPPENoticeSet = null;
	String[] tChks = request.getParameterValues("InpTestGridChk");
	String[] proposalContNos = request.getParameterValues("TestGrid1");
	String[] prtSeqs = request.getParameterValues("TestGrid2");
	String[] customers = request.getParameterValues("TestGrid3");
    if(tChks != null)
    {
        tLPPENoticeSet = new LPPENoticeSet();
    	for(int i = 0; i < tChks.length; i++)
    	{
    	    if(tChks[i].equals("1"))
    	    {
    	        LPPENoticeSchema schema = new LPPENoticeSchema();
    	        schema.setEdorAcceptNo(edorNo);
    	        schema.setEdorNo(edorNo);
    	        schema.setEdorType(edorType);
    	        schema.setCustomerNo(customers[i]);
    	        schema.setProposalContNo(proposalContNos[i]);
    	        schema.setPrtSeq(prtSeqs[i]);
    	        tLPPENoticeSet.add(schema);
    	    }
    	}
	}
	
	//工本费
	LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = null;
	String costCheck = request.getParameter("costCheck");
	if(costCheck != null && costCheck.equals("1"))
	{
	    tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
	    tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
	    tLPEdorEspecialDataSchema.setEdorAcceptNo(edorNo);
	    tLPEdorEspecialDataSchema.setEdorNo(edorNo);
	    tLPEdorEspecialDataSchema.setEdorType(edorType);
	    tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
	    tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_GB);
	    tLPEdorEspecialDataSchema.setEdorValue(request.getParameter("cost"));
	}
	
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPEdorItemSchema);
	tVData.add(mLPPolSet);
	tVData.add(tLPPENoticeSet);
	tVData.add(tLPEdorEspecialDataSchema);
	
    PEdorWTDetailUI tPEdorWTDetailUI   = new PEdorWTDetailUI();  
    if (!tPEdorWTDetailUI.submitData(tVData, ""))
	{
		VData rVData = tPEdorWTDetailUI.getResult();
		System.out.println("Submit Failed! " + tPEdorWTDetailUI.mErrors.getErrContent());
		Content = transact + "失败，原因是:" + tPEdorWTDetailUI.mErrors.getFirstError();
		FlagStr = "Fail";
	}
	else 
	{
		Content = "保存成功";
		FlagStr = "Success";
	} 

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

