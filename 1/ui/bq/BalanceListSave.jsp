<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BalanceListSave.jsp
//程序功能：万能保单月度结算清单查询
//创建日期：2007-12-14
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%
  String FlagStr="";      
  String Content = "";    
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  TransferData tTransferData = new TransferData();
  
  String templateName = "BalanceList.vts";
  String sql = request.getParameter("Sql");
  tTransferData.setNameAndValue(PrtCoreBL.TemplateName, templateName);
  tTransferData.setNameAndValue(PrtCoreBL.SQL, sql);
  
  TextTag tTextTag = new TextTag();
  tTextTag.add("ManageCom", request.getParameter("ManageCom"));
  tTextTag.add("ManageComName", StrTool.unicodeToGBK(request.getParameter("ManageComName")));
  tTextTag.add("PolYear", request.getParameter("PolYear"));
  tTextTag.add("PolMonth", request.getParameter("PolMonth"));
  tTextTag.add("RunDateStart", request.getParameter("RunDateStart"));
  tTextTag.add("RunDateEnd", request.getParameter("RunDateEnd"));
  tTextTag.add("SaleChnl", request.getParameter("SaleChnl"));
  tTextTag.add("SaleChnlName", StrTool.unicodeToGBK(request.getParameter("SaleChnlName")));
  
  VData tVData = new VData();
  tVData.add(tG);
  tVData.add(tTransferData);
  tVData.add(tTextTag);
  
  //xml文件输出路径
  String sOutXmlPath = application.getRealPath("") + "/printdata/data/brief/";	
  String tPrintServerInterface = (new ExeSQL()).getOneValue("select sysvarvalue from LDSYSVAR where sysvar='PrintServerInterface'");
  System.out.println("数据路径sOutXmlPath + " + sOutXmlPath);
  
  //生成xml文件名
  Calendar cal = new GregorianCalendar();
  String year = String.valueOf(cal.get(Calendar.YEAR));
  String month=String.valueOf(cal.get(Calendar.MONTH)+1);
  String date=String.valueOf(cal.get(Calendar.DATE));
  String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String now = year + month + date + hour + min + sec + "_" ;
  String millis = String.valueOf(System.currentTimeMillis());
  String tFileName = "B001-" + now + millis.substring(millis.length()-3, millis.length());
  
  //生成xml文件   
  PrtCoreUI ui = new PrtCoreUI();
  XmlExport txmlExport = ui.getXmlExport(tVData, "");
  if(txmlExport == null)
  {
    FlagStr = "Fail";
    Content = ui.mErrors.getFirstError().toString();                 
  }
  txmlExport.outputDocumentToFile(sOutXmlPath, tFileName);
  
  //打印
  String url=tPrintServerInterface+"&filename=" + tFileName + "&action=preview";
  System.out.println(url);
  response.sendRedirect(url);
  FlagStr = "Succ";
  Content = "打印成功! ";	
%>                      
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
