<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
 <%
     GlobalInput tG = new GlobalInput();
     tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
     System.out.println("-----"+tG.ManageCom);
  %>    
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./GEdorManuUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorManuUWInit.jsp"%>
  <%@include file = "ManageComLimit.jsp"%>

  <title>保全集体单人工核保 </title>
</head>
<body  onload="initForm();" >

  <form action="./GEdorManuUWSubmit.jsp" method=post name=fm target="fraSubmit">
	<DIV id = "divGrpEdorSelect" style = "display: ''">
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPQuery);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLPQuery" style= "display: ''">
      <table  class= common>
       	 <TR class=common>
          <TD  class= title>
            集体合同保单号
          </TD>
          <TD  class= input>
            <Input class= common name=QGrpContNo >
          </TD>
        </TR>
        <TR class=common>
          <TD  class= title>
            申请批单号
          </TD>
          <TD  class= input>
            <Input class= common name=QEdorNo >
          </TD>
        </TR>
      </table>
    </Div>
          <INPUT VALUE="查  询" TYPE=button class=cssButton onclick="easyQueryClick();"> 
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEdorManuUW);">
    		</td>
    		<td class= titleImg>
    			 集体批改信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divEdorManuUW" style= "display: ''">
      	<table  class= common>
       	<tr  class= common>
      	  	<td text-align: left colSpan=1>
  				<span id="spanPGrpEdorMainGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button class=cssButton onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button class=cssButton onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button class=cssButton onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" TYPE=button class=cssButton onclick="getLastPage();"> 	
      	
      	<table class = common>
  		<TR>
  	 
  	 	<!--td class = titleImg width = 26%>
    		当前核保员级别为&nbsp<Input class = readonly readonly name=uwgrade >&nbsp级。
    	</td-->
		</TR>
    	</table>
  </div>
  </DIV>
  <div id="divEdorManuUWAct" style="display : none">

	<table>
      <tr>
       <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
       </td>
       <td class= titleImg>
        保全信息
       </td>
      </tr>
	</table>
	<Div  id= "divGrpEdorMain" style= "display: ''">
    <table  class= common>
        <tr class=common>
            <td class=title>
                团体批单号
            </td>
            <td class= input>
                <Input class="readonly" readonly name=EdorNo>
            </td>
            <TD  class= title>
              团体保单号
            </TD>
            <TD  class= input>
              <Input class="readonly" readonly name=GrpContNo >
            </TD>
            <TD  class= title>
                保全申请日期
            </TD>
            <TD  class= input>
                <Input class= "readonly" readonly dateFormat="short" name=EdorAppDate >
            </TD>
        </tr>
        <tr  class= common>
             <TD  class= title>
                保全生效日期
             </TD>
             <TD  class= input>
                <Input class= "readonly" readonly dateFormat="short" name=EdorValiDate >
             </TD>
   	  	     <TD  class= title>
            		变动保费
          	 </TD>
          	 <TD  class= input>
            		<Input class= "readonly" readonly name=ChgPrem >
          	 </TD>
          	 <TD  class= title>
            		补/退费金额
          	 </TD>
          	 <TD  class= input>
            		<Input class= "readonly" readonly name=GetMoney >
          	 </TD>
		</tr>
    </table>
	<table  class= common>
	<tr  class= common>
		<td text-align: left colSpan=1>
			<span id="spanPGrpEdorTypeGrid" >
			</span> 
		</td>
	</tr>
	</table>
	</div>      
	<hr></hr>
	<INPUT VALUE = "自动核保信息" Class="cssButton" TYPE=button onclick= "showGUWErr();">
	<INPUT VALUE = "保全批单明细" Class="cssButton" TYPE=button onclick= "detailUWInfo();">
	<hr></hr>
  </div>

	<DIV id = "divGrpEdorTypeManuUW" style="display : none">
		<table class = common>
		<tr><td class = titleImg>
			团险保全项目人工核保
		</td></tr>	
		<TR >
			<TD  class= title> 核保结论: </TD>
		</tr>
		<tr>
			<TD  class= input>
				<Input class="code" name=TypeUWState ondblclick="return showCodeList('edoruwstate',[this]);" onkeyup="return showCodeListKey('edoruwstate',[this]);">
			</TD>
		</tr>
		<tr>
			<TD  class= title>
				人工核保意见:
			</TD>
		</tr>
		<tr>
		<td class = input>
			<textarea name="ManuUWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea>
		</td>
		</TR>	
		</table>
		<Input class = cssButton type=Button value= "核保确认" onclick ="grpEdorTypeManuUW();">
		<Input class = cssButton type=Button value= "返  回" onclick ="goBack();">
	</DIV>
	<DIV id = "divGrpEdorMainManuUW" style="display : none">
		<table class = common>
		<tr><td class = titleImg>
			团险保全申请批单人工核保
		</td></tr>	
		<TR >
			<TD  class= title> 核保结论: </TD>
		</tr>
		<tr>
			<TD  class= input>
				<Input class="code" name=MainUWState ondblclick="return showCodeList('edoruwstate',[this]);" onkeyup="return showCodeListKey('edoruwstate',[this]);">
			</TD>
		</tr>
		<!--tr>
			<TD  class= title>
				人工核保意见:
			</TD>
		</tr>
		<tr>
		<td class = input>
			<textarea name="ManuUWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea>
		</td>
		</TR -->	
		</table>
		<Input class = cssButton type=Button value= "核保确认" onclick ="grpEdorMainManuUW();">
		<Input class = cssButton type=Button value= "返  回" onclick ="goBack();">
		<!--Input class = cssButton type=Button value= "进入个单核保确认" class=cssButton onclick ="edorManuUW();"-->
	</DIV>
	
  	<input type=hidden id="EdorType" name="EdorType">
  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="uwGradeFlag" name="uwGradeFlag">
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>
