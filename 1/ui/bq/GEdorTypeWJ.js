//GEdorTypeWJ.js
var showInfo;

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//初始化保单信息
function initGrpContInfo()
{
  var sql = "  select appntNo, grpName, handlerDate, cValiDate, "
            + "   (select min(payToDate) "
            + "   from LCCont "
            + "   where grpContNo = a.grpContNo), "
            + "   peoples2, prem, "
            + "   (select AccBala "
            + "   from LCAppAcc "
            + "   where customerNo = a.appntNo), "
            + "   (select name "
            + "   from LDCom "
            + "   where comCode = a.signCom) "
            + "from LCGrpCont a "
            + "where grpContNo = '" + fm.GrpContNo.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    fm.AppntNo.value = result[0][0];
    fm.GrpName.value = result[0][1];
    fm.HandlerDate.value = result[0][2];
    fm.CValiDate.value = result[0][3];
    fm.PayToDate.value = result[0][4];
    fm.Peoples2.value = result[0][5];
    fm.Prem.value = result[0][6];
    fm.AccBala.value = result[0][7];
    fm.SignCom.value = result[0][8];
    
    if(fm.AccBala.value == null || fm.AccBala.value == "" || fm.AccBala.value == "null")
    {
      fm.AccBala.value = "0";
    }
  }
  else
  {
    alert("没有查询保单号为" + fm.GrpContNo.value + "的保单信息");
  }
}

//查询保障计划
function queryLCContPlan()
{
  var sql = "  select distinct a.contPlanCode, a.contPlanName, a.peoples2, "
            + "   (select sum(prem) "
            + "   from LCPol "
            + "   where grpContNo = a.grpcontNo "
            + "       and contPlanCode = a.contPlanCode "
            + "       and polTypeFlag = '1' ), "
            + "   (select sum(amnt) "
            + "   from LCPol "
            + "   where grpContNo = a.grpContNo "
            + "       and contPlanCode = a.contPlanCode "
            + "       and polTypeFlag = '1' ), b.contNo, b.insuredNo "
            + "from LCContPlan a , LCInsured b, LCCont c "
            + "where a.grpContNo = b.grpContNo "
            + "   and a.contPlanCode = b.contPlanCode "
            + "   and b.contNo = c.contNo "
            + "   and c.polType = '1' "  //无名单
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
            + "   and a.contPlanCode != '11' ";
  turnPage.pageLineNum = 50;
	turnPage.queryModal(sql, LCContPlan);
}

//查询保障计划的险种信息
function queryLCContPlanRisk()
{
  var rows = LCContPlan.getSelNo() - 1;
  var contPlanCode = LCContPlan.getRowColDataByName(rows, "contPlanCode");
  var contNo = LCContPlan.getRowColDataByName(rows, "contNo");
  
  if(contPlanCode == null || contPlanCode == "")
  {
    alert("请选择保障计划信息");
    return false;
  }
  fm.ContPlanCode.value = contPlanCode;
  fm.ContNo.value = contNo;
  
  var sql = "  select a.contPlanCode, a.contPlanName, a.riskCode, b.riskName "
            + "from LCContPlanRisk a, LMRisk b "
            + "where a.riskCode = b.riskCode "
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
            + "   and a.contPlanCode = '" + contPlanCode + "' ";
  turnPage2.pageLineNum = 50;
	turnPage2.queryModal(sql, LCContPlanRisk);
	
	//查询本期应交日期和交至日期
	sql = "  select distinct lastPayToDate, curPayToDate, payCount "
	      + "from LJAPayPerson "
	      + "where contNo = '" + contNo + "' "
	      + "order by payCount desc ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.LastPayToDate.value = rs[0][0];
    fm.CurPayToDate.value = rs[0][1];
  }
	
	showContPlanWJInfo(contPlanCode);
}

//显示录入的保障计划减人信息
function showContPlanWJInfo(contPlanCode)
{
  var sql = "  select abs(int(a.noNamePeoples)), abs(double(a.edorPrem)), a.edorValiDate "
            + "from LCInsuredList a, LPCont b "
            + "where a.contNo = b.contNo "
            + "   and a.edorNo = b.edorNo "
            + "   and a.contPlanCode = '" + fm.ContPlanCode.value + "' "
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
            + "   and a.edorNo = '" + fm.EdorNo.value + "' "
            + "   and a.contNo = '" + fm.ContNo.value + "' "
            + "   and b.edorType = '" + fm.EdorType.value + "' ";
            
  var turnPage3 = new turnPageClass();
  var result = easyExecSql(sql);
  if(result)
  {
    fm.Peoples2WJ.value = result[0][0];
    fm.SumPremWJ.value = result[0][1];
    fm.EdorValiDate.value = result[0][2];
    fm.PremWJ.value = pointTwo(parseFloat(fm.SumPremWJ.value) / parseInt(fm.Peoples2WJ.value));
  }
  //若为录入，计算出保障计划下的人均保费
  else
  {
    try
    {
      var rows = LCContPlan.getSelNo() - 1;
      var sumPrem = LCContPlan.getRowColDataByName(rows, "prem");  //保障计划总保费
      var peoples2 = LCContPlan.getRowColDataByName(rows, "peoples2");  //保障计划总人数
      
      fm.PremWJ.value = pointTwo(parseFloat(sumPrem) / parseInt(peoples2));
      fm.AvgPrem.value = fm.PremWJ.value;
      fm.Peoples2WJ.value = "";
      fm.SumPremWJ.value = "";
    }
    catch(ex)
    {}
  }
}

//响应人均保费变更事件
//若人数不为空，则本次应收保费=本次应收保费*人数
function onPremWJChange()
{
  var premWJ = fm.PremWJ.value;
  var peoples2WJ = fm.Peoples2WJ.value;
  
  if(!isNumeric(premWJ))
  {
    alert("人均期交保费必须为非负数");
    fm.PremWJ.value = "";
    return false;
  }
  
  if(peoples2WJ != "" && isInteger(peoples2WJ))
  {
    var sumPremWJ = pointTwo(parseFloat(premWJ) * parseInt(peoples2WJ));
    fm.SumPremWJ.value = sumPremWJ;
  }
}

//响应新减被保人数变更事件
//若本次应收保费不为空，则人均期交保费=本次应收保费/人数
//若本次应收保费为空，且人均期交保费不为空，则本次应收保费=本次应收保费*人数
function onPeoples2WJChange()
{
  var premWJ = fm.PremWJ.value;
  var peoples2WJ = fm.Peoples2WJ.value;
  var sumPremWJ = fm.SumPremWJ.value;
  
  if(!isInteger(peoples2WJ) || parseInt(peoples2WJ) == 0)
  {
    alert("减少被保险人数应为正整数");
    fm.Peoples2WJ.value = "";
    return false;
  }
  
  if(!checkPeoples2Input())
  {
    return false;
  }
  
  //若本次应收保费不为空，则人均期交保费=本次应收保费/人数
  if(isNumeric(sumPremWJ))
  {
    fm.PremWJ.value = pointTwo(parseFloat(sumPremWJ) / parseInt(peoples2WJ));
  }
  //若本次应收保费为空，则本次应收保费=本次应收保费*人数
  else if(isNumeric(premWJ))
  {
    var sumPremWJ = pointTwo(parseFloat(premWJ) * parseInt(peoples2WJ));
    fm.SumPremWJ.value = sumPremWJ;
  }
}

//录入人数不能大于对应保障计划人数
function checkPeoples2Input()
{
  var rows = LCContPlan.getSelNo() - 1;
  if(rows < 0)
  {
    alert("请选择保障计划");
    return false;
  }
    
  var planPeoples2 = LCContPlan.getRowColDataByName(rows, "peoples2");
  if(parseInt(fm.Peoples2WJ.value) > parseInt(planPeoples2))
  {
    alert("人数不能大于保障计划" + fm.ContPlanCode.value + "人数");
    fm.Peoples2WJ.value = "";
    
    return false;
  }
  return true;
}

//响应本次应收保费变更事件
//若人数存在，则自动计算人均期交保费
function onSumPremWJChange()
{
  var peoples2WJ = fm.Peoples2WJ.value;
  var sumPremWJ = fm.SumPremWJ.value;
  
  if(!isNumeric(sumPremWJ))
  {
    alert("本次应收保费应为非负数");
    fm.SumPremWJ.value = "";
    return false;
  }
  if(isInteger(peoples2WJ))
  {
   fm.PremWJ.value = pointTwo(parseFloat(sumPremWJ) / parseInt(peoples2WJ));
  }
}

//响应生效日期 变更事件
//本次减人生效日期必须在保单生效日和交至日之间
function onEdorValiDateChange()
{
  var edorValiDate = fm.EdorValiDate.value;
  if(!isDate(edorValiDate))
  {
    alert("生效日期必须为日期格式'yyyy-mm-dd'");
    fm.EdorValiDate.value = currentDate;
    return false;
  }
  var sql = "  select 1 "
            + "from dual "
            + "where days('" + fm.LastPayToDate.value + "') > days('" + edorValiDate + "') "
            + "   or days('" + fm.CurPayToDate.value + "') < days('" + edorValiDate + "') ";
  var turnPage3 = new turnPageClass();
  var result = easyExecSql(sql, null, null, null, null, null, turnPage3);

  if(result)
  {
    alert("本次减人生效日期必须在本交费期的应交日期和交至日之间。");
    fm.EdorValiDate.value = currentDate;
    return false;
  }
  return true;
}

//提交时:
//人均期交保费不能为空，必须>=0
//被保人数被保人数不能为空，必须>0
//总保费不能为空,必须>=0
//生效日必须在保单生效日和交至日之间
function checkAllInput()
{
  var premWJ = fm.PremWJ.value;  //人均期交保费
  var peoples2WJ = fm.Peoples2WJ.value;  //总人数
  var sumPremWJ = fm.SumPremWJ.value;  //总保费
  
  var rows = LCContPlan.getSelNo() - 1;
  if(rows < 0)
  {
    alert("请选择保障。");
    return false;
  }
  var contPrem = LCContPlan.getRowColDataByName(rows, "prem");  //保障计划总保费
  var contPeoples2 = LCContPlan.getRowColDataByName(rows, "peoples2");  //保障计划总人数
  
  if(!isNumeric(premWJ) || !isNumeric(sumPremWJ))
  {
    alert("人均期交保费、减少被保险人数、本次应收保费均应为非负数");
    return false;
  }
  if(parseFloat(contPrem) < parseFloat(sumPremWJ))
  {
    alert("录入总保费不能大于保障计划保费。");  
    return false;
  }
  if(!isInteger(peoples2WJ) || parseFloat(peoples2WJ) == 0)
  {
    alert("减少被保险人数应为正整数");
    return false;
  }
  if(parseInt(contPeoples2) < parseInt(peoples2WJ))
  {
    alert("录入人数不能大于保障计划人数。");  
    return false;
  }
  if(!onEdorValiDateChange())
  {
    return false;
  }
  if(!checkPeoples2Input())
  {
    return false;
  }
  if(!checkPeoples2Input2())
  {
    return false;
  }
  if(premWJ != fm.AvgPrem.value)
  {
    return window.confirm("您录入的人均期交保费与该保障计划下人均期交保费不等，要继续操作么?");
  }
  return true;
}

//提交操作
function submitData()
{
  if(!checkAllInput())
  {
    return false;
  }
  controlSubmitButton(true);
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

//控制保存按钮的显示与否
function controlSubmitButton(flag)
{
  document.all.submitButton.disabled=flag;
}

function afterSubmit(flag, content)
{
  controlSubmitButton(false);
  
  showInfo.close(); 
  window.focus();
    
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  if(flag == "Succ")
  {
    top.opener.getGrpEdorItem();
  }
}

//撤销录入
function cancel()
{
  fm.PremWJ.value = "";
  fm.Peoples2WJ.value = "";
  fm.SumPremWJ.value = "";
  fm.EdorValiDate.value = currentDate;
}

function returnParent()
{
  try
  {
    top.close();
    top.opener.focus();
  }
  catch(ex)
  {
    alert(ex.message);
  }
  
}

function getLCContPlan()
{
  var win = window.open("../sys/ContPlan.jsp?GrpContNo=" + fm.GrpContNo.value + "&LoadFlag=1&ContType=1&LoadFrame=TASK", "contPlan");
  win.focus();
}

//20080527 add zhanggm 减人后的保障计划人数不能少于已经承保人数
function checkPeoples2Input2()
{
  var rows = LCContPlan.getSelNo() - 1;
  if(rows < 0)
  {
    alert("请选择保障计划");
    return false;
  }
  var planPeoples2 = LCContPlan.getRowColDataByName(rows, "peoples2");  
  var sql = "select count(distinct a.ContNo) from LCPol a ,LCCont b "
          + "    where a.GrpContNo = '" + fm.GrpContNo.value + "' "
          + "        and exists (select 1 from LCPol c " 
          + "                    where c.ContNo = b.ContNo  and c.PolNo = a.MasterPolNo "
          + "                    and a.stateflag = '1') "
          + "        and b.conttype ='2' and b.PolType = '1' and a.PolTypeFlag <> '1' "
          + "        and a.ContPlanCode = '" + fm.ContPlanCode.value + "' with ur ";
  var ContPeoples = "0";   
  var result = easyExecSql(sql);
  if(result)
  {
    ContPeoples = result[0][0];
  }
  if(parseInt(planPeoples2)-parseInt(fm.Peoples2WJ.value) < parseInt(ContPeoples))
  {
    var alertInfo = "减人后保障计划" + fm.ContPlanCode.value + "人数不能少于已经被实名化的人数,\n"
                  + "保障计划" + fm.ContPlanCode.value + "已经实名化" + ContPeoples + "人!";
    alert(alertInfo);
    fm.Peoples2WJ.value = "";
    return false;
  }
  return true;
}