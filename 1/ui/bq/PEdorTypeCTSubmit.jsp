<%
//程序名称：PEdorTypeWTSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  LPPolSet mLPPolSet=new LPPolSet();
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

	transact = request.getParameter("fmtransact");
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String contNo = request.getParameter("ContNo");
	String reason = request.getParameter("reason_tb");
	String taxMoney = request.getParameter("TaxMoney");
	System.out.println("++++++++++++++++"+taxMoney);
	
	String tContNo[] = request.getParameterValues("PolGrid8");             
    String tGrpContNo[]= request.getParameterValues("PolGrid9");
    String tInsuredNo[]= request.getParameterValues("PolGrid1");
    String tPolNo[]= request.getParameterValues("PolGrid3");
    String tRiskCode[]= request.getParameterValues("PolGrid4");
    String tChk[] = request.getParameterValues("InpPolGridChk");
    for (int i=0;i<tChk.length;i++)
    {
        System.out.println("$$$$$$$$$$$44"+tChk[i]);
        if (tChk[i].equals("1"))
        {
            LPPolSchema tLPPolSchema=new LPPolSchema();
            tLPPolSchema.setEdorNo(edorNo);
            tLPPolSchema.setEdorType(edorType);
            tLPPolSchema.setContNo(contNo);
            tLPPolSchema.setInsuredNo(tInsuredNo[i]);
            tLPPolSchema.setPolNo(tPolNo[i]);
            System.out.println("++++++++++++++++"+tPolNo[i]);
            mLPPolSet.add(tLPPolSchema);
        }
    }
                
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setContNo(contNo);
	tLPEdorItemSchema.setEdorType(edorType);
	tLPEdorItemSchema.setInsuredNo("000000");
	tLPEdorItemSchema.setPolNo("000000");
	//tLPEdorItemSchema.setGetMoney(getMoney);
	tLPEdorItemSchema.setReasonCode(reason);
	
	
	
	LPContSchema tLPContSchema = new LPContSchema();
  tLPContSchema.setEdorNo(edorNo);
  tLPContSchema.setEdorType(edorType);
  tLPContSchema.setContNo(contNo);
  tLPContSchema.setBankCode(request.getParameter("BankCode"));
  tLPContSchema.setBankAccNo(request.getParameter("BankAccNo"));
  tLPContSchema.setAccName(request.getParameter("AccName"));

  
    VData tVData = new VData();
    tVData.add(tGlobalInput);
	tVData.add(tLPEdorItemSchema);
	tVData.add(mLPPolSet);
	tVData.add(tLPContSchema);
	
    if (!"".equals(taxMoney))
    {
      LPEdorEspecialDataSchema tLPEdorEspecialDataSchema=new LPEdorEspecialDataSchema();
      tLPEdorEspecialDataSchema.setEdorAcceptNo(edorNo);
      tLPEdorEspecialDataSchema.setEdorNo(edorNo);
      tLPEdorEspecialDataSchema.setEdorType(edorType);
      tLPEdorEspecialDataSchema.setDetailType("TAXMONEY");
      tLPEdorEspecialDataSchema.setEdorValue(taxMoney);
      tLPEdorEspecialDataSchema.setPolNo(tPolNo[0]);
      tVData.add(tLPEdorEspecialDataSchema);
    }
    

    PEdorCTDetailUI tPEdorCTDetailUI   = new PEdorCTDetailUI();  
if (!tPEdorCTDetailUI.submitData(tVData, ""))
	{
		VData rVData = tPEdorCTDetailUI.getResult();
		System.out.println("Submit Failed! " + tPEdorCTDetailUI.mErrors.getErrContent());
		Content = transact + "失败，原因是:" + tPEdorCTDetailUI.mErrors.getFirstError();
		FlagStr = "Fail";
	}
	else 
	{
		Content = "保存成功";
		FlagStr = "Success";
	} 

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

