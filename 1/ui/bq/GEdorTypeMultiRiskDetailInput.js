//该文件中包含客户端需要处理的函数和事件
var showInfo;
var pEdorFlag = true;                        //用于实时刷新处理过的数据的列表

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();  
var turnPage0 = new turnPageClass();       //使用翻页功能，必须建立为全局变量
window.onfocus = initFocus;                  //重定义获取焦点处理事件
var arrList1 = new Array();                  //选择的记录列表
var grpPolNo;

/**
 * 查询按钮
 */
function queryClick() {
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
//	var strSql = "select ContNo, InsuredNo, InsuredName, InsuredSex, InsuredBirthday, InsuredIDType, InsuredIDNo ,"
//				+"(Select b.polno From lcpol b Where b.contno=a.contno And b.grpcontno='"+fm.GrpContNo.value+"' And b.grppolno='"+grpPolNo+"' and b.grpContno='"+fm.GrpContNo.value+"')"
//				+"from lccont a where grpcontno = '"
//	           + fm.GrpContNo.value + "'"
//	           + " and InsuredNo in (select InsuredNo from LCPol where GrpPolNo='"+grpPolNo+"' and GrpContNo='"+fm.GrpContNo.value+"')";
	var strSql = "Select b.contno,b.InsuredNo, b.InsuredName, b.InsuredSex, b.InsuredBirthday,t.InsuredIDType, t.InsuredIDNo ,polno"
	+" From lcpol b,lccont t Where b.grpcontno=t.grpcontno And  t.grpcontno='"+fm.GrpContNo.value+"' And b.contno = t.contno and b.grppolno='"+grpPolNo+"'"
	+"and t.contno not in (select ContNo from lpedoritem where edorno='" + fm.EdorNo.value + "' and edortype='"+fm.EdorType.value+"')";

	turnPage.queryModal(strSql, LCInsuredGrid); 	
	showInfo.close();
	

}

/**
 * 单击MultiLine的复选按钮时操作
 */
function reportDetailClick(parm1, parm2) {	
  if (fm.all(parm1).all('LCInsuredGridChk').checked) {
    arrList1[fm.all(parm1).all('LCInsuredGridNo').value] = fm.all(parm1).all('LCInsuredGrid2').value;

  }
  else {
    arrList1[fm.all(parm1).all('LCInsuredGridNo').value] = null;
  }
  
  GrpBQ = false;
}

/**
 * 进入多个人保全
 */
var arrP = 0;
function pEdorMultiDetail() {
  //校验是否选择
    var i = 0;
    var chkFlag=false;
    for (i=0;i<LCInsuredGrid.mulLineCount;i++ )
    {
        if (LCInsuredGrid.getChkNo(i))
        {
          chkFlag=true;
          break;
        }
        
    }
    if (chkFlag)
    {
        var showStr = "正在申请集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        //showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        PEdorDetail();
		//showInfo.close();
    }
    else
    {
        alert("请选择需要操作的记录!");
    }     
  
}

/**
 * 进入个人保全
 */
function PEdorDetail() {  
    fm.all("Transact").value = "INSERT||EDOR";
    var showStr = "正在申请集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit();
    showInfo.close();
    queryClick();
    queryPEdorList();

}

/**
 * 提交后操作,服务器数据返回后执行的操作
 */
function afterSubmit(FlagStr, content, Result) 
{
  showInfo.close();
  if (FlagStr == "Fail") 
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else 
  { 
    pEdorFlag = true;
    if (fm.Transact.value=="DELETE||EDOR") {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    }
    else {
		queryPEdorList();
    }
  }
}

/**
 * 处理获取焦点事件
 */
function initFocus() {
  if (pEdorFlag) {   
    pEdorFlag = false;
    queryClick0();
    //queryPEdorList();
  }
}

var GrpBQ = false;
var GTArr = new Array();
/**
 * 打开个人保全的明细界面
 */
function openPEdorDetail() {
	
	  if (trim(fm.all('EdorType').value)=="PT") {
		  window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp", "", 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		}
		if (trim(fm.all('EdorType').value)=="GT") {
		  window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp", "", 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		}
		if (trim(fm.all('EdorType').value)=="ZT") {
		  PEdorDetail();
		}

		try { showInfo.close();	} catch(e) {}

}

/**
 * 查询出申请后的个人保全列表
 */
function queryPEdorList() {
  var strSql = "select a.ContNo, a.InsuredNo, a.InsuredName, a.InsuredSex, a.InsuredBirthday, a.InsuredIDType, a.InsuredIDNo,b.polno from lccont a ,lcpol b where"
  				+"a.contno=b.contno and  a.ContNo in "
	           + "(select ContNo from lpedoritem where edorno='" + fm.EdorNo.value + "' and edortype='"+fm.EdorType.value+"')";
  
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(strSql, LCInsured2Grid); 	 
  showInfo.close();
}

/**
 * 单击MultiLine的单选按钮时操作
 */
function reportDetail2Click(parm1, parm2) {	
  if (fm.all(parm1).all('LCInsured2GridChk').value=='on' || fm.all(parm1).all('LCInsured2GridChk').value=='1') {
    fm.ContNo.value = fm.all(parm1).all('LCInsured2Grid1').value;
  }
}

/**
 * 撤销集体下个人保全
 */
function cancelPEdor() 
{
    var i = 0;
    var chkFlag=false;
    for (i=0;i<LCInsured2Grid.mulLineCount;i++ )
    {
        if (LCInsured2Grid.getChkNo(i))
        {
          chkFlag=true;
          break;
        }
        
    }
    if (chkFlag)
    {
        fm.all("Transact").value = "DELETE||EDOR"
     //   var showStr = "正在撤销集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
     //   var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    //    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.submit();
    //    showInfo.close();
        queryClick();
        queryPEdorList();
        
    }
    else
    {
        alert("请选择需要操作的记录!");
    }     
 
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent() {
	top.close();
}
//初始化险种表
function queryClick0()
{	
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	var strSql = "select grppolno,riskcode,(select riskname from lmrisk where riskcode=a.riskcode),'','','未申请'"
	           + "from lcgrppol a where grpcontno='"
	           + fm.all('GrpContNo').value+"'";
////下面两行注释应该加上
//	           + " and riskcode IN(select riskcode from lmriskedoritem where edorcode='"
//	           + fm.all('EdorNo').value+"')";
	turnPage0.queryModal(strSql, LCInsured0Grid);
	showInfo.close();	 
}

//单击MultiLine的单选按钮时操作
function reportDetail0Click(parm1, parm2)
{	
  tRow=LCInsured0Grid.getSelNo();
  grpPolNo = LCInsured0Grid.getRowColData(tRow-1,1);
  fm.GrpPolNo.value = grpPolNo;
  queryClick();
  queryPEdorList();
}