<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BqBalanceAdvanceAskSave.jsp
//程序功能：提前结算数据保存页面
//创建日期：2005-03-11 14:27:43
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
  
<%
	String FlagStr;
	String Content;
	String tWorkNo = "";
	String tAccpetNo = "";
	String tDetailWorkNo = "";

	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	String sql ="select appntno,grpname ,managecom from lcgrpcont where grpcontno ='"+request.getParameter("grppolno")+"'";
	ExeSQL tExeSQL = new ExeSQL();
	SSRS tSSRS = tExeSQL.execSQL(sql);
	String tCustomerNO = tSSRS.GetText(1,1);
	String tCustomerName = tSSRS.GetText(1,2);
	String tManageCom = tSSRS.GetText(1, 3);	
	
	//输入参数
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	tLGWorkSchema.setCustomerNo(tCustomerNO);
	tLGWorkSchema.setCustomerName(tCustomerName);
	tLGWorkSchema.setStatusNo("3"); //处理状态
	tLGWorkSchema.setTypeNo("0601"); //业务类型
	tLGWorkSchema.setAcceptWayNo("8");
	tLGWorkSchema.setContNo(request.getParameter("grppolno"));
	
	VData tVData = new VData();
	tVData.add(tLGWorkSchema);
	tVData.add(tGI);
	String tsql = "select target from lgautodeliverrule where  sourcecomcode = '" +
                    tManageCom + " ' and goaltype = '6' ";
  tSSRS = tExeSQL.execSQL(tsql);
  if (tSSRS.getMaxRow() == 0) {
			FlagStr = "Fail";
			Content = "数据保存失败！请在定期结算查询界面生成提前结算任务！";
  }
	else
	{
	  tVData.add(tSSRS.GetText(1,1));
	            
		LCGrpBalPlanBL tLCGrpBalPlanBL = new LCGrpBalPlanBL();
		if(!tLCGrpBalPlanBL.submitData(tVData, ""))
		{
			FlagStr = "Fail";
			Content = "数据保存失败！请在定期结算查询界面生成提前结算任务！";
		}
		else
		{		
			FlagStr = "Succ";
			Content = "数据保存成功!"; //+ tAccpetNo;
		}
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmitAsk("<%=FlagStr%>","<%=Content%>");
</script>
</html>

