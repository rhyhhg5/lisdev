<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：
//创建日期：2015-02-27
//创建人  ：lizy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="GEdorTypeZG.js"></SCRIPT>
	<%@include file="GEdorTypeZGInit.jsp"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<title>追加管理费 </title>
</head>
<body onload="initForm();">
  <form action="./GEdorTypeZGSubmit.jsp" method=post name=fm target="fraSubmit">
  <table class="common">
    <tr class="common"> 
      <td class="title">受理号</TD>
      <td class="input"> 
        <input class="readonly" readonly name="EdorNo">
      </td>
      <td class="title">批改类型</TD>
      <td class="input">
      	<input class="readonly" readonly name="EdorType" type="hidden">
      	<input class="readonly" readonly name="EdorTypeName">
      </td>
      <td class="title">集体保单号</TD>
      <td class="input">
      	<input class="readonly" readonly name="GrpContNo">
      </td>
    </tr>
  </table>
  <%@include file="SpecialInfoCommon.jsp"%>
  <table>
    <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroup);">
      </td>
      <td class= titleImg>团体账户管理费追加金额
      </td>
    </tr>
  </table>
  <div id= "divGroup" style= "display: ''">
    <table class= common>
			<tr class= common>
				<td>
					<span id="spanPolGrid" >
					</span>
				</td>
			</tr>
		</table>
		<div id="divPage3" align=center style="display: 'none' ">
      <input class=cssButton value="首  页" type=button onclick="turnPage3.firstPage();"> 
      <input class=cssButton value="上一页" type=button onclick="turnPage3.previousPage();"> 					
      <input class=cssButton value="下一页" type=button onclick="turnPage3.nextPage();"> 
      <input class=cssButton value="尾  页" type=button onclick="turnPage3.lastPage();">
    </div>	
  </div>
   <br>
	<Div  id= "divSubmit" style="display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
	   		 <input type=Button name="returnButton" class= cssButton value="返  回" onclick="returnParent();">
			</TR>
	 	</table>
	</Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>