<%
//程序名称：PEdorTypeTFInit.jsp
//程序功能：
//创建日期：2008-06-02
//创建人  ：Cz
//更新记录：  更新人    更新日期     更新原因/内容
%>                         

<script language="JavaScript">  

function initForm()
{
  try
  {
    initInpBox();
    initPolGrid();
    initGrpPolGrid();   
    getGrpContInfo(fm.all('GrpContNo').value);
    //checkSelectPol();
  }
  catch(re)
  {
    alert("PEdorTypeTFInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
{                               
    var iArray = new Array();
      
    try
    {
       iArray[0]=new Array();
       iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      	iArray[0][1]="0px";            		//列宽
    		iArray[0][2]=10;            			//列最大值
  			iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="投保人名称";
        iArray[1][1]="200px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="生效日期";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="缴费频次";
        iArray[3][1]="60px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="终止日期";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        
	    PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
	    //这些属性必须在loadMulLine前
	    PolGrid.mulLineCount = 0;   
	    PolGrid.displayTitle = 1;
	    PolGrid.canChk=0;
	    PolGrid.canSel = 1;
	    PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
	    PolGrid.hiddenSubtraction=1;
	    PolGrid.loadMulLine(iArray);
	    PolGrid.selBoxEventFuncName ="getpoldetail"; 
	    //这些操作必须在loadMulLine后面
    }
    catch(ex)
    {
      alert(ex);
    }
}

function initGrpPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      
      iArray[2]=new Array();
      iArray[2][0]="险种号";   		//列名
      iArray[2][1]="90px";            		//列宽
      iArray[2][2]=100;            		//列最大值
      iArray[2][3]=3;              		//是否允许输入,1表示允许，

	  	iArray[3]=new Array();
      iArray[3][0]="被保人客户号";		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="被保人姓名";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许

	  	iArray[5]=new Array();
      iArray[5][0]="保障计划";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="缴费频次";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="生效日期";         		//列名
      iArray[7][1]="90px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="交至日期";         		//列名
      iArray[8][1]="90px";            		//列宽
      iArray[8][2]=200;            	        //列最大值
      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      //iArray[9]=new Array();
      //iArray[9][0]="满期日期";         		//列名
      //iArray[9][1]="90px";            		//列宽
      //iArray[9][2]=200;            	        //列最大值
      //iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      GrpPolGrid = new MulLineEnter( "fm" , "GrpPolGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPolGrid.mulLineCount =0;   
      GrpPolGrid.displayTitle = 1;
      GrpPolGrid.hiddenPlus = 1;
      GrpPolGrid.hiddenSubtraction = 1;
      GrpPolGrid.locked = 1;
      GrpPolGrid.canChk = 1;
//      GrpPolGrid.canSel = 1;
      GrpPolGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert(ex);
      }
}
            
</script>