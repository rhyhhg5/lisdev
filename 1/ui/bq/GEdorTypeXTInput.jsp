<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2005.3.31 20:00:00
//创建人  ：LHS
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeXT.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeXTInit.jsp"%>
  
<title>投保人协议退保！</title> 
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeXTSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 批单号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 批改类型 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=EdorType type=hidden>
        	<input class = "readonly" readonly name=EdorTypeName>
        </TD>
       
        <TD class = title > 集体保单号 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=GrpContNo>
        </TD>   
      </TR>
    </TABLE> 
  
    <Div  id= "divLPAppntGrpDetail" style= "display: ''">
      <table>
        <tr>
        	<td class= titleImg>
        		 集体保单资料
        	</td>
        </tr>
      </table>
      
      <Div  id= "divGroupPol2" style= "display: ''">
        <table  class= common>
          <TR>
            <TD  class= title8>
              集体客户号
            </TD>
            <TD  class= input8>
              <Input class="readonly" name=AppntNo readonly >
            </TD>  
            <TD  class= title8>
              集体保单号
            </TD>
            <TD  class= input8>
              <Input class="readonly" name=GrpContNo2 readonly >
            </TD> 
            <TD  class= title8>
              单位名称
            </TD>
            <TD  class= input8>
              <Input class="readonly" name=GrpName readonly >
            </TD>     
          </TR>               
          <TR  class= common>
            <TD  class= title8>
              投保日期
            </TD>
            <TD  class= input8>
              <Input class="readonly" name=PolApplyDate readonly >
            </TD>
            <TD  class= title8>
              生效日期
            </TD>
            <TD  class= input8>
              <Input class="readonly" name=CValiDate readonly >
            </TD>
            <TD  class= title8>
              交至日期
            </TD>
            <TD  class= input8>
              <Input class="readonly" name=PayToDate readonly >
            </TD>   
          </TR>
          <TR  class= common>
            <TD  class= title8>
              签单机构
            </TD>
            <TD  class= input8>
              <Input class="readonly" name=SignCom readonly >
            </TD>   
            <TD  class= title8>
              投保人数
            </TD>
            <TD  class= input8>
              <Input class="readonly"  name=Peoples2 readonly >
            </TD>
      
            <TD  class= title8>
              期交保费
            </TD>
            <TD  class= input8>
              <Input class="readonly" name=Prem readonly >
            </TD>
          </TR>
          <TR>
            <TD  class= title8>
              总保额
            </TD>
            <TD  class= input8>
              <Input class="readonly" name=Amnt readonly >
            </TD>
          </TR>
          <TR  class= common> 
  	  		  <TD class= title> 退保原因 </TD>
            <TD class= input>
          	  <input class="codeNo" name="reason_tb" readonly ondblclick="return showCodeList('reason_tb', [this,reason_tbName], [0,1]);" onkeyup="return showCodeListKey('reason_tb', [this,reason_tbName],[0,1]);" verify="退保原因|not null"><input class="codeName" name="reason_tbName" readonly elementtype="nacessary">
            </TD>
          </TR>
        </table>
        
        <!--险种信息-->   
        <Div  id= "divPolInfo" style= "display: ''">
          <table>
            <tr>
              <td>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpPolGrid);">
              </td>
              <td class= titleImg>
                险种选择
              </td>
            </tr>
          </table>
	        <Div  id= "divLPGrpPolGrid" style= "display: ''">
            <table  class= common>
              <tr  class= common>
                <td text-align: left colSpan=1>
  	      		    <span id="spanLCGrpPolGrid" ></span> 
  	      	    </td>
  	      	  </tr>
            </table>
            <Div id= "divPage2" align="center" style= "display: 'none' ">
      	      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
      	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
      	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
      	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
	          </Div>				
  	      </div>
        </div>
        <Input type =button name=budgetButton class=cssButton value="退保试算" onclick="edorCalZTTest()">
 		    <Input name="cancel" type =button class=cssButton value="算  费" onclick="calFee()">
 		    <Input name="save" type =button class=cssButton value="保  存" onclick="edorTypeXTSave()">
 		    <Input name="goBack" type =button class=cssButton value="返  回" onclick="returnParent()">
      </Div>
	  </Div>
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="ContType" name="ContType">
    <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
    <input type=hidden id="EdorValiDate" name="EdorValiDate">
    <input type=hidden id="addrFlag" name="addrFlag">
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
