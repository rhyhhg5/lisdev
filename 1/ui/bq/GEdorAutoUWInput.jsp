<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./GEdorAutoUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorAutoUWInit.jsp"%>
  <%@include file = "ManageComLimit.jsp"%>

  <title>保全集体单自动核保 </title>
</head>
<body  onload="initForm();" >

  <form action="./GEdorAutoUWSubmit.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPQuery);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLPQuery" style= "display: ''">
      <table  class= common>
       	 <TR class=common>
          <TD  class= title>
            集体保单号
          </TD>
          <TD  class= input>
            <Input class= common name=GrpContNo >
          </TD>
        </TR>
        <TR class=common>
          <TD  class= title>
            申请批单号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
          </TD>
        </TR>
      </table>
    </Div>
          <INPUT class = cssButton VALUE="查  询" TYPE=button onclick="easyQueryClick();"> 
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEdorAutoUW);">
    		</td>
    		<td class= titleImg>
    			 个人批改信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divEdorAutoUW" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanPGrpEdorMainGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
      <INPUT class = cssButton VALUE="首  页" TYPE=button onclick="getFirstPage();"> 
      <INPUT class = cssButton VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT class = cssButton VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT class = cssButton VALUE="尾  页" TYPE=button onclick="getLastPage();"> 	
      <hr/>
      	<table class = common>
		<TR>
  	 	  <INPUT class = cssButton type=button value= "自动核保" onclick ="edorAutoUW();">
		</TR>
    	</table>			
  </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
