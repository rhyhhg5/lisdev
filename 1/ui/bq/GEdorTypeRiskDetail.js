//该文件中包含客户端需要处理的函数和事件
var showInfo;
var GEdorFlag = true;
var selGridFlag = 0;            //标识当前选中记录的GRID

//该文件中包含客户端需要处理的函数和事件
var turnPage0 = new turnPageClass();       
var turnPage = new turnPageClass();           //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//重定义获取焦点处理事件
window.onfocus = initFocus;

//查询按钮
function queryClick()
{
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	var strSql = "select ContNo, InsuredNo, InsuredName, InsuredSex, InsuredBirthday, (select IDType from LCInsured where contno=a.contno and insuredno=a.insuredno),(select IDNo from LCInsured where contno=a.contno and insuredno=a.insuredno), PolNo"
	           + " from LCPol a"
	           + " where GrpContNo='"+fm.GrpContNo.value+"'"
	           + " and GrpPolNo='"+fm.thegrppolno.value+"'"
	           + " and ContNo not in(select ContNo from LPEdorItem where edortype='" + fm.EdorType.value + "' and edorno='" + fm.EdorNo.value + "')"
	           + getWherePart('ContNo', 'ContNo2')
	           + getWherePart('InsuredNo', 'CustomerNo2')
	           + getWherePart('InsuredName', 'Name2', 'like', '0');
  
	turnPage.queryModal(strSql, LCInsuredGrid);
	if (selGridFlag == 1)
	{
		selGridFlag = 0;
	}

	showInfo.close();	 
}
function queryClick0()
{
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	var strSql = "select grppolno,riskcode,(select riskname from lmrisk where riskcode=a.riskcode),'','','未申请'"
	           + "from lcgrppol a where grpcontno='"
	           + fm.all('GrpContNo').value+"'";
	           //+ "and riskcode IN(select riskcode from lmriskedoritem where edorcode='"
	           //+ fm.all('EdorNo').value+"'";

	turnPage0.queryModal(strSql, LCInsured0Grid);
	if (selGridFlag == 1)
	{
		selGridFlag = 0;
	}

	showInfo.close();	 
}

//查询按钮
function queryClick2()
{
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	var strSql = "select ContNo, InsuredNo, InsuredName, InsuredSex, InsuredBirthday, (select IDType from LCInsured where contno=a.contno and insuredno=a.insuredno),(select IDNo from LCInsured where contno=a.contno and insuredno=a.insuredno), PolNo"
	           + " from LCPol a"
	           + " where GrpContNo='"+fm.GrpContNo.value+"'"
	           + " and GrpPolNo='"+fm.thegrppolno.value+"'"
	           + " and ContNo in(select ContNo from LPEdorItem where edortype='" + fm.EdorType.value + "' and edorno='" + fm.EdorNo.value + "')"
			       + getWherePart('ContNo', 'ContNo3')
			       + getWherePart('InsuredNo', 'CustomerNo3')
			       + getWherePart('InsuredName', 'Name3', 'like', '0');
  //document.write(strSql);
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, LCInsured2Grid);
	if (selGridFlag == 2)
	{
		selGridFlag = 0;
	}

	showInfo.close();	 
}

//单击MultiLine的单选按钮时操作
function reportDetail0Click(parm1, parm2)
{	
	fm.thegrppolno.value = LCInsured0Grid.getRowColData(LCInsured0Grid.getSelNo()-1, 1);
	fm.theriskcode.value = LCInsured0Grid.getRowColData(LCInsured0Grid.getSelNo()-1, 2);
	queryClick0();
	selGridFlag = 1;	
  queryClick();
  queryGEdorList();   
}

//单击MultiLine的单选按钮时操作
function reportDetailClick(parm1, parm2)
{	
	fm.ContNo.value     = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 1);
	fm.PolNo.value      = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 8);
	fm.CustomerNo.value = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 2);
	queryGEdorList();
	selGridFlag = 1;
}

//进入个人保全
function GEdorDetail()
{
	if (selGridFlag == 0)
	{
		alert("请先选择一个被保人！");
		return;
	}
	var showStr = "正在申请集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	if (selGridFlag == 1)
	{
		fm.all("Transact").value = "INSERT||EDOR";
	}
	fm.submit();
	fm.all("Transact").value = "";
}

//打开个人保全的明细界面
function openGEdorDetail() 
{
	if (fm.ContNo.value=="")
	{
		alert("不能是空记录!");
	}
	else
	{
		fm.CustomerNoBak.value = fm.CustomerNo.value;
		fm.ContNoBak.value = fm.ContNo.value;
		window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp");
		showInfo.close();	 
	}
	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result)
{
	showInfo.close();

	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		GEdorFlag = true;

		if (fm.Transact.value=="DELETE||EDOR")
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		}
		else
		{
			openGEdorDetail();
		}
	}
}

//处理获取焦点事件
function initFocus() 
{
  if (GEdorFlag)
  {   
    GEdorFlag = false;
    queryClick0();
    //queryClick();
    //queryGEdorList();
 	selGridFlag = 0;
  }
}

//查询出申请后的个人保全列表 已经作过保全项目的 
function queryGEdorList() 
{
	var strSql = "select ContNo, InsuredNo, InsuredName, InsuredSex, InsuredBirthday, (select IDType from LCInsured where contno=a.contno and insuredno=a.insuredno),(select IDNo from LCInsured where contno=a.contno and insuredno=a.insuredno), PolNo"
	           + " from LCPol a"
	           + " where GrpContNo='"+fm.GrpContNo.value+"'"
	           + " and GrpPolNo='"+fm.thegrppolno.value+"'"
	           + " and ContNo in(select ContNo from LPEdorItem where edortype='" + fm.EdorType.value + "' and edorno='" + fm.EdorNo.value + "')";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, LCInsured2Grid); 	 
	if (selGridFlag == 2)
	{		
		selGridFlag = 0;
	}
}

//单击MultiLine的单选按钮时操作
function reportDetail2Click(parm1, parm2)
{	
	fm.ContNo.value     = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 1);
	fm.PolNo.value      = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 8);
	fm.CustomerNo.value = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 2);
	selGridFlag = 2;
	queryClick();
}

//撤销集体下个人保全
function cancelGEdor()
{
	fm.all("Transact").value = "DELETE||EDOR"

	var showStr = "正在撤销集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	var strSql = "select MakeDate, MakeTime, EdorState from LPEdorItem where EdorNo='" + fm.EdorNo.value + "' and EdorType = '" + fm.EdorType.value + "'";
	var arrResult = easyExecSql(strSql);     //执行SQL语句进行查询，返回二维数组

	if (arrResult != null)
	{
		var tMakeDate = arrResult[0][0];
		var tMakeTime = arrResult[0][1];
		var tEdorState = arrResult[0][2];
		var tInusredNo = fm.CustomerNo.value;
		var tPolNo = fm.PolNo.value;
		fm.action = "./PEdorAppCancelSubmit.jsp?DelFlag=1&InsuredNo=" + tInusredNo + "&MakeDate=" + tMakeDate + "&MakeTime=" + tMakeTime + "&EdorState=" + tEdorState+"&PolNo="+tPolNo;
		fm.submit();
		fm.action = "./GEdorTypeRiskDetailSubmit.jsp";
	}
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent() {
	top.close();
}
