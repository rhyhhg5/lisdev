<%
//GEdorTypeMultiDetailInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>                       

<script language="JavaScript">  

function initInpBox() { 
  try 
  {
  	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
  	
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
  	
  	if(flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goIntoItem.style.display = "none";
  		fm.cancel.style.display = "none";
  	}
  
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;

  }
  catch(ex) 
  {
    alert("操作错误，请关闭上次登陆打开的页面后重试。");
  }      
   //   setCalType();
}   

//显示算法类型
function setCalType()
{
	var sql = "select DetailType, EdorValue from LPEdorESpecialData " +
	          "where EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value + "' " +
	          "and PolNo = '000000'";
	var result = easyExecSql(sql);
	if (result == null)
	{
	  return false;
	}
	for (var i = 0; i < result.length; i++)
	{
	  var detailType = result[i][0];
	  var edorValue = result[i][1];
    if (detailType == "CALTIME")
  	{
  		if (edorValue == "1")
  		{
  			fm.CalTime[0].checked = "true";
  		}
  		else if (edorValue == "2")
  		{
  			fm.CalTime[1].checked = "true";
  		}
  	}
	  else if (detailType == "FEERATE")
	  {
	    fm.FeeRate.value = parseFloat(edorValue) * 100;
	  }
	}       
}

//控制按钮的显示与否
function initDisplay()
{

}                                  

function initForm() {
  try 
  {

    initLCInsuredGrid();
    initLCInsured2Grid();
    
    initInpBox();
    
    initDisplay();
    
    //showAllCodeName();
    queryPEdorList();
    //checkClaimInfo();
    
  }
  catch(re) 
  {
    alert("操作错误，请关闭上次登陆打开的页面后重试。");
  }
}



// 信息列表的初始化
function initLCInsuredGrid() {
  var iArray = new Array();  
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号";    	//列名1
    iArray[1][1]="150px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="客户号";         			//列名2
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="姓名";         			//列名8
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="性别";         		//列名5
    iArray[4][1]="50px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

    iArray[5]=new Array();
    iArray[5][0]="出生日期";         		//列名6
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="证件类型";         		//列名6
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="证件号码";         		//列名6
    iArray[7][1]="150px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="个人帐户单位交费余额";         
    iArray[8][1]="150px";            
    iArray[8][2]=100;            		
    iArray[8][3]=0;       
    
    iArray[9]=new Array();
    iArray[9][0]="个人帐户个人交费余额";         
    iArray[9][1]="150px";            
    iArray[9][2]=100;            		
    iArray[9][3]=0;
    
    iArray[10]=new Array();
    iArray[10][0]="个人帐户余额";         
    iArray[10][1]="150px";            
    iArray[10][2]=100;            		
    iArray[10][3]=0;         		
    
    LCInsuredGrid = new MulLineEnter( "fm" , "LCInsuredGrid" ); 
    //这些属性必须在loadMulLine前
    LCInsuredGrid.mulLineCount = 0;    
    LCInsuredGrid.displayTitle = 1;
    LCInsuredGrid.canChk = 1;
    LCInsuredGrid.hiddenPlus = 1; 
    LCInsuredGrid.hiddenSubtraction = 1;
    LCInsuredGrid.chkBoxEventFuncName = "reportDetailClick";
    LCInsuredGrid.detailInfo = "单击显示详细信息";
    LCInsuredGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
}

// 信息列表的初始化
function initLCInsured2Grid() {
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号";    	//列名1
    iArray[1][1]="150px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="客户号";         			//列名2
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="姓名";         			//列名8
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="性别";         		//列名5
    iArray[4][1]="50px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用


    iArray[5]=new Array();
    iArray[5][0]="证件号码";         		//列名6
    iArray[5][1]="150px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="生效日期";         
    iArray[6][1]="150px";            
    iArray[6][2]=100;            		
    iArray[6][3]=3; 
    iArray[6][9]="生效日期|DATE&NOTNULL";
    iArray[6][10]="edorvalidate"; 
    
    iArray[7]=new Array();
    iArray[7][0]="单位交费归属比例";         
    iArray[7][1]="150px";            
    iArray[7][2]=100;            		
    iArray[7][3]=3; 
    
    iArray[8]=new Array();
    iArray[8][0]="个人账户单位交费余额";         
    iArray[8][1]="150px";            
    iArray[8][2]=100;            		
    iArray[8][3]=0; 
    
    iArray[9]=new Array();
    iArray[9][0]="个人账户个人交费余额";         
    iArray[9][1]="150px";            
    iArray[9][2]=100;            		
    iArray[9][3]=0; 
    
    iArray[10]=new Array();
    iArray[10][0]="个人账户余额";         
    iArray[10][1]="150px";            
    iArray[10][2]=100;            		
    iArray[10][3]=0;


    LCInsured2Grid = new MulLineEnter( "fm" , "LCInsured2Grid" ); 
    //这些属性必须在loadMulLine前
   	LCInsured2Grid.mulLineCount = 0;    
    LCInsured2Grid.displayTitle = 1;
    LCInsured2Grid.canChk = 1;
    LCInsured2Grid.hiddenPlus = 1; 
    LCInsured2Grid.hiddenSubtraction = 1;
    LCInsured2Grid.detailInfo = "单击显示详细信息";
    LCInsured2Grid.loadMulLine(iArray);       
  }
  catch(ex) 
  {
    alert(ex);
  }
}

</script>