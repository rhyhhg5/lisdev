<%
//程序名称：PEdorTypeCTTestSubmit.jsp
//程序功能：
//创建日期：2005-08-02 16:49:22
//创建人  ：zhangtao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息

  CErrors tError = null;
  //后面要执行的动作：添加，修改
              
  String FlagStr = "";
  String Content = "";
  String transact = "";
  LPPolSet mLPPolSet=new LPPolSet();
  
  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

	String sContNo = request.getParameter("ContNo");
	String sPEdorZTDate = request.getParameter("EdorItemAppDate");
	
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setContNo(sContNo);
	tLPEdorItemSchema.setEdorAppDate(sPEdorZTDate);	
	tLPEdorItemSchema.setEdorType("CT");

	String tRiskCode[]= request.getParameterValues("PolGrid1");
	String tRiskName[]= request.getParameterValues("PolGrid2");
    String tPolNo[]= request.getParameterValues("PolGrid8");
    String tChk[] = request.getParameterValues("InpPolGridChk");
    
    int iPolCont = 0;
    double dZTMoney = 0.0;
    LJSGetEndorseSet mLJSGetEndorseSet = new LJSGetEndorseSet();
    LJSGetEndorseSet tLJSGetEndorseSet = new LJSGetEndorseSet();
    String StrCTFeePol = "";
    String StrCTFeeDetail = "";
    for (int i = 0; i < tChk.length; i++)
    {
        if (tChk[i].equals("1"))
        {
            iPolCont++;
            tLPEdorItemSchema.setPolNo(tPolNo[i]);
       
			EdorCalZT tEdorCalZT = new EdorCalZT(tGlobalInput);
		    double dPolZTMoney = tEdorCalZT.calZTData(tLPEdorItemSchema);
		    System.out.println("*******************" + dPolZTMoney);
		    if (dPolZTMoney == -1)
		    {
		          tError = tEdorCalZT.mErrors;
			      Content = "试算失败，原因是:" + tError.getFirstError();
			      FlagStr = "Fail";
			      System.out.println(Content);
			      break;
		    }
		    else
		    {
		    	dZTMoney += dPolZTMoney;
		        tLJSGetEndorseSet = tEdorCalZT.getLJSGetEndorseSet();
		        mLJSGetEndorseSet.add(tLJSGetEndorseSet);
		        
		        String sql = " select RiskName from LMRisk  where RiskCode = '" + 
		        			tRiskCode[i] + "' ";
		        ExeSQL tExeSQL = new ExeSQL();
		        String sRiskName = tExeSQL.getOneValue(sql);
		        if (tExeSQL.mErrors.needDealError())
		        {
		            sRiskName = "";
		        }
		        
		        StrCTFeePol += "^" + tPolNo[i] + "|" + tRiskCode[i] + "|" + sRiskName + "|" + -dPolZTMoney;
		    }            
        }
    }
    
    dZTMoney = -dZTMoney;  //页面负号显示退费
    
    StrCTFeePol = "0|" + String.valueOf(iPolCont) + StrCTFeePol;
    
	System.out.println("===StrCTFeePolGrid===" + StrCTFeePol);
	
    if (iPolCont < 1)
    {
	      Content = "请选择要退保试算的保单!";
	      FlagStr = "Fail";
    }  

    if ("".equals(FlagStr))
    { 
		if (mLJSGetEndorseSet != null && mLJSGetEndorseSet.size() > 0)
		{
		    for (int i = 1; i<= mLJSGetEndorseSet.size(); i++)
		    {
		        String sql = " select RiskName from LMRisk  where RiskCode = '" + 
		        			mLJSGetEndorseSet.get(i).getRiskCode() + "' ";
		        ExeSQL tExeSQL = new ExeSQL();
		        String sRiskName = tExeSQL.getOneValue(sql);
		        if (tExeSQL.mErrors.needDealError())
		        {
		            sRiskName = "";
		        }
		        
		        sql = " select codename from ldcode where trim(codetype) = 'BQSubFeeType' " +
		        	" and trim(code) = '" + mLJSGetEndorseSet.get(i).getSubFeeOperationType() + "' ";
		        tExeSQL = new ExeSQL();
		        String sFeeName = tExeSQL.getOneValue(sql);
		        if (tExeSQL.mErrors.needDealError())
		        {
		            sFeeName = "";
		        }
		        
		    	String sFeeType = "";
		        sql = " select codename from ldcode where codetype = 'finfeetype' and code = '" + mLJSGetEndorseSet.get(i).getFeeFinaType() + "' ";
		        tExeSQL = new ExeSQL();
		        sFeeType = tExeSQL.getOneValue(sql);
		        if (tExeSQL.mErrors.needDealError())
		        {
		            sFeeType = "";
		        }
		    
		         StrCTFeeDetail += "^" + mLJSGetEndorseSet.get(i).getPolNo() + "|" +
		         				mLJSGetEndorseSet.get(i).getRiskCode() + "|" + 
		         				sRiskName + "|" + 
		         				sFeeName + "|" + 
		         				mLJSGetEndorseSet.get(i).getGetMoney() + "|" + 
		         				sFeeType;
		         				
		    }
		    
		    StrCTFeeDetail = "0|" + mLJSGetEndorseSet.size() + StrCTFeeDetail;
		    System.out.println("===StrCTFeeDetail===" + StrCTFeeDetail);
		}
		else
		{
			StrCTFeeDetail = "null";
		}
		
	      Content = "退保试算成功!";
	      FlagStr = "Succ";
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.fm.GetMoney.value = "<%=dZTMoney%>";
	parent.fraInterface.fm.StrCTFeePol.value = "<%=StrCTFeePol%>";
	parent.fraInterface.fm.StrCTFeeDetail.value = "<%=StrCTFeeDetail%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

