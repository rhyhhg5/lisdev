//程序功能：
//创建日期：2012/8/31
//创建人  ：LCY
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus; 
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
//var RelaGridTurnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	 if(showInfo!=null)
	 {
		   try
		   {
		     showInfo.focus();  
		   }
		   catch(ex)
		   {
		     showInfo=null;
		   }
	 }
}
function querySolvencyGrid() {
	var strSql = "select year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  "
        + " from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc  "
	    + " with ur";

	var strResult = easyExecSql(strSql);
	if(!strResult){
		return true;
	}
	turnPage.pageLineNum = 10;
	turnPage.queryModal(strSql,SolvencyGrid);

}

function queryImportGrid() {
	var strSql = "select Year,quarter,solvency||'%',case when flag='1' then '达到' else '未达到' end  "
        + "from  ldriskrate where codetype='CFNL' and state='2' "
	    + " with ur";
	var strResult = easyExecSql(strSql);
	if(!strResult){
		return true;
	}
	turnPage1.queryModal(strSql,ImportGrid);
//	fm.tYear.value=strResult[0][0];
//	fm.tQuarter.value=strResult[0][1];
//	fm.tSolvency.value=strResult[0][2];
//	fm.tFlag.value=strResult[0][3];
}

//磁盘导入 
function importInsured()
{
	var url = "./SolvencyImportMain.jsp";
	var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
	window.open(url, "偿付能力充足率导入", param);
}



//修改
function LMSolvencyUpdate(){

	var arrReturn = new Array();
	var tSel = SolvencyGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录。" );
		return false;
	}
	var result = new Array();
	result[0] = new Array();
	result[0] = SolvencyGrid.getRowData(tSel-1);
	fm.all("tYear").value = fm.all("Year").value;
	fm.all("tQuarter").value = fm.all("Quarter").value;
	fm.all("tSolvency").value = fm.all("CFNLRate").value;
	if(fm.all("getCode").value=="2"){
		fm.all("tFlag").value = "0";
	}else{
		fm.all("tFlag").value = "1";
	}
	if(fm.all("tSolvency").value==""){
		alert( "偿付能力充足率不能为空！" );
		return false;
	}else if( /^[0-9]+(.[0-9]{1,7})?$/.test(fm.all("tSolvency").value)){

	}else{
		alert("偿付能力充足率录入格式错误！");
		return false;
	}
	
	if (confirm("您确实想修改该记录吗?"))
	  {
	  var i = 0;
	  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.fmtransact.value = "UPDATE||MAIN";
	  fm.action = "LMSolvencySave.jsp";
	  fm.submit(); //提交
	  }
	  else
	  {
	    alert("您取消了修改操作！");
	  }
}

//保存
function submitForm(){
	var strSql = "select Year,quarter,solvency,flag  "
        + "from  ldriskrate where codetype='CFNL' and state='2' "
	    + " with ur";
	var result = easyExecSql(strSql);
	fm.tYear.value=result[0][0];
	fm.tQuarter.value=result[0][1];
	fm.tSolvency.value=result[0][2];
	fm.tFlag.value=result[0][3];
	
	if(fm.tYear.value==""|| fm.tQuarter.value=="" || fm.tSolvency.value=="" || fm.tFlag.value==""){
		alert("请检查导入模板，录入信息是否完整！");
		return false;
	}

	if( /^[0-9]+(.[0-9]{1,6})?$/.test(fm.tSolvency.value)){

	}else{
		alert("偿付能力充足率录入格式错误！");
		return false;
	}
	var Sql = "select Year,quarter "
        + "from  ldriskrate where  codetype='CFNL' and state='1' order by Year desc,quarter desc fetch first row only  "
	    + " with ur";
	var strResult = easyExecSql(Sql);
	if(strResult){
	
			if(strResult[0][0]==fm.tYear.value){
				if(strResult[0][1]=="4"){
					alert("请按照时间顺序依次录入年度");
					return false;
				}else if((strResult[0][1]=="1" && fm.tQuarter.value=="2") || (strResult[0][1]=="2" && fm.tQuarter.value=="3") || (strResult[0][1]=="3" && fm.tQuarter.value=="4"))
					{
					}else{
						alert("请按照时间顺序依次录入季度");
						return false;
					}
			}else{
				var lastYear=parseInt(strResult[0][0]);
				var newYear=parseInt(fm.tYear.value);
				if(newYear - lastYear == 1 && fm.tQuarter.value=="1" && strResult[0][1]=="4"){
				
				}else {
					alert("请按照时间顺序依次录入年度");
					return false;

				}
			}
	}
	if(confirm("确认保存吗？")){	
		fm.fmtransact.value="INSERT||MAIN";
		var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.action = "LMSolvencySave.jsp";
		fm.submit();
	}else{
		alert("取消保存！");
	}
	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr,content,transact){

	if (transact == "INSERT||MAIN")
	{
	    if (FlagStr != "Fail" )
	    {
	        content = "保存成功";
	    }
	}
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		//执行下一步操作
	}
	initForm();
}
function onQuerySelected(parm1,parm2)
{ 
    fm.all("Year").value = SolvencyGrid.getRowColData(SolvencyGrid.getSelNo() - 1, 1);
	fm.all("Quarter").value = SolvencyGrid.getRowColData(SolvencyGrid.getSelNo() - 1, 2);	
    var Str = SolvencyGrid.getRowColData(SolvencyGrid.getSelNo() - 1, 3);

    fm.all("CFNLRate").value=Str.replace(/\%/g, '');
    if(SolvencyGrid.getRowColData(SolvencyGrid.getSelNo() - 1, 4)=="未达到"){
    	fm.all("getCode").value = "2";
    } else if(SolvencyGrid.getRowColData(SolvencyGrid.getSelNo() - 1, 4)=="达到"){
    	fm.all("getCode").value = "1";
    }
    fm.all("getCodeName").value=SolvencyGrid.getRowColData(SolvencyGrid.getSelNo() - 1, 4);
}


