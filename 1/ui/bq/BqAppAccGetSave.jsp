<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LDPersonSave.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT language="javascript">

</SCRIPT>
<%
  //接收信息，并作校验处理。
  //输入参数
  
  LCAppAccGetTraceSchema tLCAppAccGetTraceSchema   = new LCAppAccGetTraceSchema();
  LCAppAccGetUI tLCAppAccGetUI=new LCAppAccGetUI();
  //输出参数
  String FlagStr = "";
  String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    CErrors tError = null;
    //后面要执行的动作：添加，修改，删除
    String fmAction=request.getParameter("ContType");
    System.out.println("fmAction:"+fmAction); 

    tLCAppAccGetTraceSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLCAppAccGetTraceSchema.setName(request.getParameter("AccGetName"));
    tLCAppAccGetTraceSchema.setIDType(request.getParameter("GetIDType"));
    tLCAppAccGetTraceSchema.setIDNo(request.getParameter("GetIDNo"));
    tLCAppAccGetTraceSchema.setAccGetMode(request.getParameter("PayMode"));
    tLCAppAccGetTraceSchema.setBankCode(request.getParameter("BankCode"));
    tLCAppAccGetTraceSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLCAppAccGetTraceSchema.setAccName(request.getParameter("AccName"));
    tLCAppAccGetTraceSchema.setTransferDate(request.getParameter("TransferDate"));
    tLCAppAccGetTraceSchema.setAccGetMoney(request.getParameter("AccGetMoney"));
    
    try
    {
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add(tLCAppAccGetTraceSchema);
			tVData.add(tGI);
       //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
      if ( !tLCAppAccGetUI.submitData(tVData,fmAction))
      {
      	Content = "保存失败，原因是:" + tLCAppAccGetUI.mErrors.getFirstError() ;
        FlagStr = "Fail";
      }
      else
      {
      	FlagStr = "Succ";
				Content = "数据保存成功。";
      }
    }
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
	}
%>                                       
<html>
<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

