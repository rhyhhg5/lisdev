<%
//程序名称：GEdorCalInterest.jsp
//程序功能：
//创建日期：2006-01-06
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%         
  GlobalInput tGlobalInput = (GlobalInput) session.getValue("GI");
  
  String flag = "";
  String content = "";
  String interestGrp = "0";
  String interestInsured = "0";
  
  String edorNo = request.getParameter("EdorNo");
  String edorType = request.getParameter("EdorType");
  String grpPolNo = request.getParameter("grpPolNo");
  
  EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(edorNo, edorType);
  tEdorItemSpecialData.setPolNo(grpPolNo);
  tEdorItemSpecialData.add(BQ.DETAILTYPE_ACCRATE, request.getParameter("accRate"));
  tEdorItemSpecialData.add(BQ.DETAILTYPE_ENDTIME_RATETYPE, request.getParameter("rateType"));
  System.out.println("\n\n\n\n" + request.getParameter("accRate") + " "
    + request.getParameter("rateType"));
  
  LPGrpPolSchema tLPGrpPolSchema = new LPGrpPolSchema();
  tLPGrpPolSchema.setEdorNo(edorNo);
  tLPGrpPolSchema.setEdorType(edorType);
  tLPGrpPolSchema.setGrpPolNo(grpPolNo);
  
  VData data = new VData();
  data.add(tGlobalInput);
  data.add(tLPGrpPolSchema);
  data.add(tEdorItemSpecialData);
  
  GEdorMJDetailUI tGEdorMJDetailUI = new GEdorMJDetailUI();
  if(!tGEdorMJDetailUI.submitData(data, ""))
  {
    flag = "Fail";
    content = tGEdorMJDetailUI.mErrors.getLastError();
  }
  else
  {
    flag = "Succ";
    content = "满期理算成功";
  }
%>    
<html>
<script language="javascript">
	parent.fraInterface.afterCalculate("<%=flag%>", "<%=content%>", "<%=interestGrp%>", "<%=interestInsured%>");
</script>
</html>
  
  
  
  
  
  
  
  
      
      
      
      
      
      