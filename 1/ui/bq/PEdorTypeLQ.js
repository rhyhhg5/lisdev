//               该文件中包含客户端需要处理的函数和事件

var showInfo1;
var mDebug="1";

var turnPage = new turnPageClass();    //使用翻页功能，必须建立为全局变量 
var turnPage1 = new turnPageClass();   

function returnParent()
{
	try
	{
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {}
}

function getPolInfo(tContNo)
{
    var strSQL ="select InsuredNo,InsuredName,PolNo,RiskCode,Prem,Amnt,CValiDate,contno,grpcontno, to_char(LCPol.mult) from LCPol where ContNo='"+tContNo+"'";
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    for(var i = 0; i < PolGrid.mulLineCount; i++)
    {
    	if(PolGrid.getRowColData(i, 10) == "0")
    	{
    		PolGrid.setRowColData(i, 10, "");
    	}
    }
}

function chkPol()
{
    var tContno=fm.all('ContNo').value;
    var tEdorNo=fm.all('EdorNo').value;
    var tEdorType=fm.all('EdorType').value;
    var strSQL="select polno from lppol where edorno='"+tEdorNo+"' and edortype='"+tEdorType+"' and contno='"+tContno+"'";        
    var arrResult2=easyExecSql(strSQL);
    var m=0;
  	var n=0;
  	
  	if(arrResult2!=null)
  	{
  		var q=arrResult2.length;
  		for(m=0;m<PolGrid.mulLineCount;m++)
	  	{
	  		
	  		for(n=0;n<q;n++)
	  	   {
	  			if(PolGrid.getRowColData(m,3)==arrResult2[n][0])
	  			{
	  				PolGrid.checkBoxSel(m+1);
	  			}
	  		}
	  	}				
  	}
}

function saveDate()
{
	var tsql="select riskcode,polno,insuredno from lcinsureacc where contno='"+fm.all('ContNo').value+"' with ur";
	var arrResult=easyExecSql(tsql);
	fm.all("PolNo").value = arrResult[0][1];
	fm.all("InsuredNo").value= arrResult[0][2];
	if("332301"==arrResult[0][0] || arrResult[0][0]=="334801"){
		var tSel = InsuAccGrid.getSelNo();
		if( tSel == 0 || tSel == null ){
			alert( "请先选择一条记录。" );
			return false;
		}
		fm.all("PolNo").value=InsuAccGrid.getRowColData(InsuAccGrid.getSelNo() - 1, 1);
		fm.all("InsuredNo").value=InsuAccGrid.getRowColData(InsuAccGrid.getSelNo() - 1, 2);
	}
		
	
	if (!beforeSubmit())
	{
		return false;
	}
	if(!checkAppMoney())
	{
		return false;
	}

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "PEdorTypeLQSubmit.jsp";
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  returnParent();
	}
}

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  return true;
}           

//明细中查询是否已经录入领取金额
function queryMoney()
{
  var strSQL="select edorvalue from lpedorespecialdata where edorno='"+fm.all('EdorNo').value+"' and edortype='LQ' and detailtype='AppMoney'";
  var arrResult=easyExecSql(strSQL);
  if(arrResult!=null)
  {
  fm.all('AppMoney').value=arrResult[0][0];
  }
  else
  {
  fm.all('AppMoney').value="500";
  }
}          

function queryInsuAcc(){


	var strSql=" select distinct a.polno,b.insuredno,a.polapplydate,b.insuaccbala, "
			  +" (select ltrim(rtrim(char(year(DueBalaDate - 1 day))))||'-'||ltrim(rtrim(char(month(DueBalaDate - 1 day)))) "
			  +" from LCInsureAccBalance where polno=b.polno and InsuAccNo = b.InsuAccNo order by DueBalaDate desc fetch first 1 rows only), "
			  +" (select Rate from LMInsuAccRate where riskcode=a.riskcode and BalaDate<=b.BalaDate and RateType = 'C' "
			  +" and  exists (select 1 from LMRiskApp where RiskType4 = '4' and riskcode = a.RiskCode)" 
			  +" and Rateintv = 1 and RateIntvUnit = 'Y' order by BalaDate desc  fetch first 1 rows only)"
			  +" from lcpol a,lcinsureacc b "
			  +" where a.polno=b.polno "
			  +" and a.contno='"+fm.all('ContNo').value+"'"
			  +" with ur ";
	var arrResult=easyExecSql(strSql);
		turnPage1.pageLineNum = 10;
		turnPage1.queryModal(strSql,InsuAccGrid);

}

function queryInsuAccList(){

	  var mSql="select edorstate,polno from lpedoritem where edorno='"+fm.all('EdorNo').value+"' with ur";
	  var arrResult=easyExecSql(mSql);
	  var mEdorState = arrResult[0][0];
	  var mPolNo = arrResult[0][1];
	  if(arrResult[0][1] == InsuAccGrid.getRowColData(0, 1) ||"3" == mEdorState ){
		  InsuAccGrid.radioBoxSel(1);
	  }else {
		  InsuAccGrid.radioBoxSel(2); 
	  }
	
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function checkAppMoney()
{
  var riskCode="select riskcode,insuaccbala from lcinsureacc where polno='"+fm.all('PolNo').value+"' with ur";
  var arrResult=easyExecSql(riskCode);
  var InsuAccBala = arrResult[0][1];
  var appMoney = fm.AppMoney.value;
  if("332301"!=arrResult[0][0] && arrResult[0][0]!="334801"){

  	if (appMoney < 500)
  	{
	  alert("领取金额不能低于500元！")
      return false;
  	} 
  	else if ((appMoney % 100)!= 0)
  	{
  		alert("领取金额必须为100元的整数倍!")
    	return false;
    
  	} 
//  	if((fm.InsuAccBala.value-appMoney)<0)
  	if((InsuAccBala-appMoney)<0)
  	{
	  	alert("录入领取金额不能多于帐户余额,请重新录入！");
      	return false;
  	}
//  	else if((fm.InsuAccBala.value-appMoney)<1000)
  	else if((InsuAccBala-appMoney)<1000)
  	{
  		alert("领取后账户金额低于1000元，请重新录入！")
    	return false;
  	} 
  }else {
	  
	  	if (appMoney < 500)
	  	{
		  alert("领取金额不能低于500元！")
	      return false;
	  	} 
	  	if((InsuAccBala-appMoney)<0)
	  	{
		  	alert("录入领取金额不能多于帐户余额,请重新录入！");
	      	return false;
	  	}
  }
  return true;
}
