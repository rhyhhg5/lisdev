<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：LCGrpPolDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  //保单信息部分
  LCGrpPolSchema tLCGrpPolSchema   = new LCGrpPolSchema();
  tLCGrpPolSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLCGrpPolSchema);
  // 数据传输
  CGrpPolUI tCGrpPolUI   = new CGrpPolUI();
	if (!tCGrpPolUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tCGrpPolUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tCGrpPolUI.getResult();
		System.out.println(tVData.size());
		// 显示
		// 保单信息
		LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema(); 
		LCGrpPolSet mLCGrpPolSet=new LCGrpPolSet();
		mLCGrpPolSet=(LCGrpPolSet)tVData.getObjectByObjectName("LCGrpPolBLSet",0);
		mLCGrpPolSchema=mLCGrpPolSet.get(1);
		%>
    	<script language="javascript">
    		top.opener.fm1.all("GrpPolNo1").value = "<%=mLCGrpPolSchema.getGrpPolNo()%>";
    	 	top.opener.fm.all("GrpPolNo").value = "<%=mLCGrpPolSchema.getGrpPolNo()%>";
    	 	top.opener.fm.all("ContNo").value = "<%=mLCGrpPolSchema.getContNo()%>";
    	 	top.opener.fm.all("Name").value = "<%=mLCGrpPolSchema.getGrpName()%>";
    	 	top.opener.fm.all("LinkMan").value = "<%=mLCGrpPolSchema.getLinkMan1()%>";
    	 	top.opener.fm.all("SignDate").value = "<%=mLCGrpPolSchema.getSignDate()%>";
    	 	top.opener.fm.all("PaytoDate").value = "<%=mLCGrpPolSchema.getPaytoDate()%>";
    	 	top.opener.fm.all("Prem").value = "<%=mLCGrpPolSchema.getPrem()%>";
    	 	top.opener.fm.all("Operator").value = "<%=mLCGrpPolSchema.getOperator()%>";
    	 	top.opener.fm.all("RiskCode").value = "<%=mLCGrpPolSchema.getRiskCode()%>";

    	</script>
		<%		
	}
 // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tCGrpPolUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  out.println("top.close();");
  out.println("</script>");
%>
