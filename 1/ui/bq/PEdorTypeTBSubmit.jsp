<%
//程序名称：PEdorTypeBFSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  String flag = "";
  String content = "";
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  
  String operator = request.getParameter("Operator");
  String edorNo = request.getParameter("EdorNo");
  String edorType = request.getParameter("EdorType");
  String contNo = request.getParameter("ContNo");
  String signNameFlag = request.getParameter("SignNameFlag");
  String impartFlag = request.getParameter("ImpartFlag");
  String conditionFlag = request.getParameter("ConditionFlag");
  String remarkFlag = request.getParameter("RemarkFlag");
  String signName = request.getParameter("SignName");
  String impart = request.getParameter("Impart");
  String condition = request.getParameter("Condition");
  String remark = request.getParameter("Remark");

	try
	{
		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
		tLPEdorItemSchema.setEdorNo(edorNo);
		tLPEdorItemSchema.setEdorType(edorType);
		tLPEdorItemSchema.setContNo(contNo);
		
		LPTbInfoSchema tLPTbInfoSchema = new LPTbInfoSchema();
		tLPTbInfoSchema.setEdorNo(edorNo);
		tLPTbInfoSchema.setEdorType(edorType);
		tLPTbInfoSchema.setContNo(contNo);
		if ((signNameFlag != null) && (signNameFlag.equals("1")))
		{
			tLPTbInfoSchema.setSignName(signName);
		}
	  if ((impartFlag != null) && (impartFlag.equals("1")))
		{
			tLPTbInfoSchema.setImpart(impart);
		}
		if ((conditionFlag != null) && (conditionFlag.equals("1")))
		{
			tLPTbInfoSchema.setCondition(condition);
		}

		if ((remarkFlag != null) && (remarkFlag.equals("1")))
		{
			tLPTbInfoSchema.setRemarkFlag(remarkFlag);
			tLPTbInfoSchema.setRemark(remark);
		}
		
	  VData data = new VData();
	  data.add(gi);
	  data.add(tLPTbInfoSchema);
	  data.add(tLPEdorItemSchema);
	  PEdorTBDetailUI tPEdorTBDetailUI = new PEdorTBDetailUI();
	  if (!tPEdorTBDetailUI.submitData(data))
	  {
	    flag = "Fail";
	    content = "数据保存失败！原因是：" + tPEdorTBDetailUI.getError();
	  }
	  else
	  {
	    flag = "Succ";
	    content = "数据保存成功！";
	  }
  }
	catch (Exception e)
	{
		e.printStackTrace();
		content = "程序异常！" + e.toString();
	}
  content = PubFun.changForHTML(content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>", "<%=operator%>");
</script>
</html>