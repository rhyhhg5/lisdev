<%
//程序名称：LLWSCertifySubmit.jsp
//程序功能：
//创建日期：2010-01-04
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String flag;
  String content;
  
  GlobalInput gi = (GlobalInput)session.getValue("GI");
  String edorNo = request.getParameter("EdorNo");
  String grpContNo = request.getParameter("GrpContNo");
  
  LLWSCertifyUI tLLWSCertifyUI = new LLWSCertifyUI(gi, edorNo, grpContNo);
  if (!tLLWSCertifyUI.submitData())
  {
  	flag = "Fail";
  	content = "数据保存失败！原因是:" + tLLWSCertifyUI.getError();
  }
  else 
  {
  	flag = "Succ";
  	content = "数据保存成功。";
  	String message = tLLWSCertifyUI.getMessage();
  	if (message != null)
  	{
  	  flag = "Fail";
  	  content = message;
  	}
  }
  content = PubFun.changForHTML(content);
%>   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>