<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：EdorUWManuHealth.jsp
//程序功能：保全人工核保生存调查报告录入
//创建日期：2006-3-17
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="PEdorUWManuRReport.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title> 新契约生存调查报告 </title>
  <%@include file="PEdorUWManuRReportInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./PEdorUWManuRReportChk.jsp">
    <input type="hidden" name="EdorNo">
    <input type="hidden" name="AppntNo">
    <input type="hidden" name="ContNo">
    <input type="hidden" name="InsuredNo">
    <input type="hidden" name="InsuredName">
    <input type="hidden" name="PrtSeq">
   	<table>
      <tr>
        <td><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divInsured);"></td>
        <td class= titleImg>被保人信息</td>
        <td class= titleImg><!--input type=Button class=cssButton value="被保人明细" onclick=""--></td>
      </tr>
    </table>
    <div id= "divInsured" style= "display: ''">
      <table class= common>
        <tr class= common>
        	<td text-align: left colSpan=1>
          	<span id="spanLCInsuredGrid" >
          	</span> 
        	</td>
        </tr>
      </table>
      <div id= "divPage" align=center style= "display: 'none' ">
        <input class=cssButton value="首  页" type=button onclick="turnPage.firstPage();"> 
        <input class=cssButton value="上一页" type=button onclick="turnPage.previousPage();"> 					
        <input class=cssButton value="下一页" type=button onclick="turnPage.nextPage();"> 
        <input class=cssButton value="尾  页" type=button onclick="turnPage.lastPage();"> 			
      </div>		
    </div>
     <table>
    	<tr>
      <td class=common>    
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);"></td>
    		<td class= titleImg>契调项目录入</td>                            
    	</tr>	
    </table>
    <Div  id= "divUWSpec" style= "display: ''">
      <table  class= common>
      	<tr  class= common>
      		<td text-align: left colSpan=1 >
    				<span id="spanInvestigateGrid"></span> 
    			</td>
      	</tr>
      </table>
      <div id= "divPage2" align=center style= "display: 'none' ">
        <input class=cssButton value="首  页" type=button onclick="turnPage2.firstPage();"> 
        <input class=cssButton value="上一页" type=button onclick="turnPage2.previousPage();"> 					
        <input class=cssButton value="下一页" type=button onclick="turnPage2.nextPage();"> 
        <input class=cssButton value="尾  页" type=button onclick="turnPage2.lastPage();"> 			
      </div>	
    </div>
      <table class=common>
         <TR  class= common> 
           <TD  class= common> 其他契调信息 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Contente" cols="120" rows="3" class="common" ></textarea>
           </TD>
         </TR>
      </table>
      <INPUT type= "button" name= "sure" value="确 认" class= cssButton onclick="submitForm()">			
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>