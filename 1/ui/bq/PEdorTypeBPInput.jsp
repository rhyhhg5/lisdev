<html> 
<% 
//程序名称：PEdorTypeBPInput.jsp
//程序功能：
//创建日期：2009-07-3
//创建人  zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="./PEdorTypeBP.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="./PEdorTypeBPSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
    	<tr class= common> 
        <td class="title"> 受理号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="EdorNo" >
        </td>
        <td class="title"> 批改类型 </td>
        <td class="input">
        	<input class="readonly" type="hidden" readonly name="EdorType">
        	<input class="readonly" readonly name="EdorTypeName">
        </td>
        <td class="title"> 保单号 </td>
        <td class="input">
          <input class="readonly" type="text" readonly name="ContNo" >
        </td>
    	</tr>
    </table>  
    <%@include file="ULICommon.jsp"%>
    <Div  id= "divPolInfo" style= "display: ''">
    <table>
      <tr>
       <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolBase);"></td>
       <td class= titleImg>基本信息</td>
      </tr>
     
    </table>
  </Div>
   <Div id="divPolBase" style= "display: '' ">
   <table class = "common"> 
   	 <tr  class= "common"> 
      <td class="title">保单生效日期</td>
      <td class= input><input class="readonly" type="text" readonly name="CvaliDate" ></td>
      <td class="title">基本保额</td>
      <td class= input><input class="readonly" type="text" readonly name="Amnt" ></td>
      <td class="title">交费状态</td>
      <td class= input><input class="readonly" type="text" readonly name="PayState" ></td>
    </tr>
  </table>
   </Div>
   <table class = "common"> 
    <tr  class= "common"> 
      <td class="title">险种代码</td>
      <td class= input><input class="readonly" type="text" readonly name="RiskCode" ></td>
      <td class="title">当前保费</td>
      <td class= input><input class="readonly" type="text" readonly name="Prem" ></td>
      <td class="title">变更后保费</font></td>
      <td class= input><Input class= "common" name="NewPrem"  verify="变更后保费|notnull&len<13" style="width:120"> 元 <font color="#FF0000">（录入）</font></td>
    </tr>
  </table>
  <div  id= "divUliInfo" style= "display: 'none'">
      <table  class= common>
       <tr class= common>
          <td  class= common >
            <input class= "readonly" name="UliInfo" value = "" readonly style = "width:100%">
          </td>  
        </tr>
       </table>
  </div>
  <Div  id= "divSubmit" style="display:''">
    <table class = common>
      <TR class= common>
        <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
        <input type=Button name="returnButton" class= cssButton value="返  回" onclick="returnParent();">
       <!-- <input type="button" Class="cssButton"  value="测试" onclick="isLoanState();" >  --> 
      </TR>
    </table>
  </Div>
  <br>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="ContType" name="ContType">
  <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
  <input type=hidden id="EdorValiDate" name="EdorValiDate">
  <input type=hidden id="EdorAppDate" name="EdorAppDate">
  <input type=hidden id="SumPrem" name="SumPrem">
  <input type=hidden id="PayToDate" name="PayToDate">
  <input type=hidden id="PayStartDate" name="PayStartDate">
  <input type=hidden id="PolNo" name="PolNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
