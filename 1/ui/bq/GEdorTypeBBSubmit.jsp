<%
//程序名称：GEdorTypeBBSubmit.jsp
//程序功能：
//创建日期：2002-10-9 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 	<%@page import="com.sinosoft.lis.bq.*"%>
 	<%@page import="com.sinosoft.lis.pubfun.*"%>
 	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。 
 
  
  CErrors tError = null; 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";  

  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

	 transact = request.getParameter("fmtransact");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String edorValue = request.getParameter("EdorValue");
	
	LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	tLPGrpEdorItemSchema.setEdorNo(edorNo);
	//tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
	tLPGrpEdorItemSchema.setGetMoney(edorValue);
	tLPGrpEdorItemSchema.setEdorType(edorType);

  // 准备传输数据 VData
  		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
  		tLPEdorEspecialDataSchema.setEdorNo(edorNo);
  		tLPEdorEspecialDataSchema.setEdorAcceptNo(edorNo);
  		
  		tLPEdorEspecialDataSchema.setEdorType(edorType);
  		tLPEdorEspecialDataSchema.setEdorValue(edorValue);
  		
  		
	    tLPEdorEspecialDataSchema.setDetailType("BBMONEY");
	    tLPEdorEspecialDataSchema.setPolNo("000000");
	
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPGrpEdorItemSchema);
	tVData.add(tLPEdorEspecialDataSchema);
	//add by hyy

	GEdorBBDetailUI tGEdorBBDetailUI = new GEdorBBDetailUI();
	if (!tGEdorBBDetailUI.submitData(tVData, transact))
	{
		VData rVData = tGEdorBBDetailUI.getResult();
		System.out.println("Submit Failed! " + tGEdorBBDetailUI.mErrors.getErrContent());
		Content = transact + "失败，原因是:" + tGEdorBBDetailUI.mErrors.getFirstError();
		FlagStr = "Fail";
	}
	else 
	{
		Content = "保存成功";
		FlagStr = "Success";
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

