<%
//程序名称：PEdorTypePYSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----PYsubmit---");
  LPPolSchema tLPPolSchema   = new LPPolSchema();
  LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
  PEdorPYDetailUI tPEdorPYDetailUI   = new PEdorPYDetailUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("------transact:"+transact);
	GlobalInput tG = new GlobalInput();
	    System.out.println("------------------begin ui");
	tG = (GlobalInput)session.getValue("GI");
  
  //个人保单批改信息
	tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));
  	tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));
	
	tLPPolSchema.setPolNo(request.getParameter("PolNo"));
  	tLPPolSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPPolSchema.setEdorType(request.getParameter("EdorType"));
	String tNewPayYears=request.getParameter("PayYearsNew");
	System.out.println("tNewPayYears:"+tNewPayYears);
  try
  {

	if (tNewPayYears!=null && tNewPayYears.length()!=0)
	{
		System.out.println("begin....");
		int index=tNewPayYears.indexOf("*");
		String tx=tNewPayYears.substring(index+1);
    		System.out.println("tx="+tx);
    		tNewPayYears=tNewPayYears.substring(0,index);
    		System.out.println("tNewPayYears="+tNewPayYears);
    		System.out.println("aaa="+String.valueOf(tNewPayYears.charAt(0)));
		tLPPolSchema.setPayEndYearFlag(String.valueOf(tNewPayYears.charAt(0)));
		System.out.println("bbb="+tNewPayYears.substring(1));
		tLPPolSchema.setPayEndYear(tNewPayYears.substring(1));
		//tLPPolSchema.setPayIntv(tx);
		System.out.println("PayEndYearFlag:"+tLPPolSchema.getPayEndYearFlag());
		System.out.println("PayEndYear:"+tLPPolSchema.getPayEndYear());
		System.out.println("PayIntv:"+tLPPolSchema.getPayIntv());
		
	}
  	// 准备传输数据 VData
  	VData tVData = new VData();  
   	//保存个人保单信息(保全)	
	tVData.addElement(tG);
	tVData.addElement(tLPEdorMainSchema);
  	tVData.addElement(tLPPolSchema);
    	tPEdorPYDetailUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
	Content = transact+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
  }		
		
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorPYDetailUI.mErrors; 
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))
    	{
      	if (tPEdorPYDetailUI.getResult()!=null&&tPEdorPYDetailUI.getResult().size()>0)
      	{
      		Result = (String)tPEdorPYDetailUI.getResult().get(0);
      		System.out.println("Result:"+Result);
      		if (Result==null||Result.trim().equals(""))
      		{
      			FlagStr = "Fail";
      			Content = "提交失败!!";
      		}
      	}
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

