var PassFlag = '0';
var ComLength= 8;
var ScreenWidth=640;
var ScreenHeight=480;
//var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//根据不同的保全项目进入不同的保全明细
function detailEdorType()
{
	if (!needDetail())
	    return;
	detailQueryClick();
	var newWin; 
	if (PassFlag=='1')
	{
	    var itemDetailUrl = "";
		switch(fm.all('EdorType').value) 
		{ 
			case "WT":
				if (fm.all('ContType').value =='1')
				{
					//newWin = window.open("../bq/PEdorTypeWT.html","PEdorTypeWT",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
					itemDetailUrl = "PEdorTypeWT.jsp";
				}
				else
				{
					//newWin = window.open("../bq/GEdorTypeWT.html","GEdorTypeWT",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
					itemDetailUrl = "GEdorTypeWT.jsp";
				}
				break;
			default:
			  //打开默认个体保全项目的明细界面
				if (fm.all('ContType').value =='1')
				{
			 		//newWin = window.open("../bq/PEdorType" + trim(fm.all('EdorType').value) + ".jsp", "PEdorType" + trim(fm.all('EdorType').value), 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
			 		itemDetailUrl = "PEdorType" + trim(fm.EdorType.value) + "Input.jsp";
			 	}
			 	//打开团险保全明细
				else {
				  //打开团险单选保全明细综合页面
				  if (fm.all('EdorType').value=="BC"  //fm.all('EdorType').value=="BB" 修改
				  || fm.all('EdorType').value=="IC"
				  || fm.all('EdorType').value=="CM"
				  || fm.all('EdorType').value=="GT" 
				  || fm.all('EdorType').value=="IO"
				  || fm.all('EdorType').value=="GC"
				  || fm.all('EdorType').value=="YC"
				  || fm.all('EdorType').value=="PT"
				  || fm.all('EdorType').value=="HL"
				  //|| fm.all('EdorType').value=="SG"
				  ) 
				  {
				    //window.open("../bq/GEdorTypeDetail.jsp", "PEdorType" + trim(fm.all('EdorType').value), 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
				    itemDetailUrl = "GEdorTypeDetailInput.jsp";
				  }
				  //打开团险复选保全明细综合页面
				  else if (fm.all('EdorType').value=="ZT" ) 
				  {
				    //newWin = window.open("../bq/GEdorTypeMultiDetail.jsp", "PEdorType" + trim(fm.all('EdorType').value), 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
				    itemDetailUrl = "GEdorTypeMultiDetailInput.jsp";
				  }
				  //打开团险复选保全明细综合页面
				  else if (fm.all('EdorType').value=="LT" ) 
				  {
				    //newWin = window.open("../bq/GEdorTypeMultiRisk.jsp", "PEdorType" + trim(fm.all('EdorType').value), 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
				    itemDetailUrl = "GEdorTypeMultiRiskInput.jsp";
				  }
				  else if (fm.all('EdorType').value=="TY" ){				  
					  itemDetailUrl = "GEdorTypeTYInput.jsp";
				  }
				  else if (fm.all('EdorType').value=="TQ" ) 
				  {
				    //newWin = window.open("../bq/GEdorTypeMultiDetail.jsp", "PEdorType" + trim(fm.all('EdorType').value), 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
				    itemDetailUrl = "GEdorTypeMultiTQInput.jsp";
				  }
				  //liuxin  SG  模仿TQ
				  else if (fm.all('EdorType').value=="SG" ) 
				  {
				    //newWin = window.open("../bq/GEdorTypeMultiDetail.jsp", "PEdorType" + trim(fm.all('EdorType').value), 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
				    itemDetailUrl = "GEdorTypeMultiSGInput.jsp";
				  }
				   else if (fm.all('EdorType').value=="MC" ){				  
						  itemDetailUrl = "GEdorTypeMCDetailMain.jsp";
					  }
				   //liuxin  JY  模仿TQ
				  else if (fm.all('EdorType').value=="JY" ) 
				  {
				    //newWin = window.open("../bq/GEdorTypeMultiDetail.jsp", "PEdorType" + trim(fm.all('EdorType').value), 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
				    itemDetailUrl = "GEdorTypeJYInput.jsp";
				  }
				  //打开默认团险保全项目的明细界面
				  else {				  
					  //newWin = window.open("../bq/GEdorType" + trim(fm.all('EdorType').value) + ".jsp", "PEdorType" + trim(fm.all('EdorType').value), 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
					  itemDetailUrl = "GEdorType" + trim(fm.EdorType.value) + "Input.jsp";
				  }
			  }
				break;
		}
		newWin = window.open("../bq/EdorItemDetailMain.jsp?edorAcceptNo=" 
		                        + fm.EdorAcceptNo.value + "&itemDetailUrl=" + itemDetailUrl 
		                        + "&GrpContNo=" + fm.GrpContNo.value + "&ContNo=" + fm.ContNo.value + "&EdorType=" + fm.EdorType.value, 
		                    itemDetailUrl.substring(0, itemDetailUrl.indexOf(".jsp")),
		                    'width=' + screen.availWidth + ',height=' + screen.availHeight 
		                        + ',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,'
		                        + 'resizable=1,status=0');
	}
	if (newWin != null)
	{
		newWin.focus();
	}
}
function GetEndorseQuery()
{
	window.open("../bq/LJSGetEndorse.html");
}
function LPPolQuery()
{
		window.open("../bq/LPPolQuery.jsp");
}
// 查询按钮
function detailQueryClick()
{
    var tEdorNo = 	fm.all('EdorNo').value;
    var tEdorType = fm.all('EdorType').value;
    var tContType = fm.all('ContType').value;
    
    var strSQL = "";
    if (tContType=='1')
    {
    	strSQL = "  select count(*) "
    	         + "from LPEdorItem "
    	         + "where edorno = '" + tEdorNo + "' "
    	         + "    and edortype = '" + tEdorType + "' ";
    }
    else
    {
        strSQL = "  select count(*) "
                 + "from LPGrpEdorItem "
                 + "where edorno = '" + tEdorNo + "' "
                 + "    and edortype = '" + tEdorType + "' ";
    }
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
      
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        alert("查询失败！");
        PassFlag='0';
        return;
    }
    else
    {
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  	  
        if (turnPage.arrDataCacheSet[0][0]<=0)
        {
            //alert("请保存申请批改项目！");
            PassFlag='0';
            return;
        }
        PassFlag ='1';
    }
}
//控制费用明细按钮
function ctrlGetEndorse()
{
	try
	{
		var tEdorNo = 	fm.all('EdorNo').value;
		var tEdorType = fm.all('EdorType').value;
		
		strSQL = "select count(*) from LJSGetEndorse where EndorsementNo='"+ tEdorNo + "' and FeeOperationType='" + tEdorType+"'";
		
		//查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  
	  //判断是否查询成功
	  if (!turnPage.strQueryResult) 
	  {
	    alert("查询失败！");
	    return;
	  }
	  else
	  {
		  //查询成功则拆分字符串，返回二维数组
		  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  	  
			if (turnPage.arrDataCacheSet[0][0]<=0)
			{
				 divGetEndorse.style.display='none';
				 return;
			}
		}
	}
	catch(ex)
	{}
}
/*
function getScreenSize()
{
	ScreenWidth = screen.availWidth;
	ScreenHeight = screen.availHeight;
}
*/
//校验该项目是否需要明细
function needDetail()
{   
    var strSQL = "  select NeedDetail "
                 + "from LMEdorItem "
                 + "where edorcode = '" + fm.all('EdorType').value + "' ";
    if (fm.all('ContType').value == '1') 
    {
        strSQL=strSQL+" and APPOBJ='I'"
    }
    else
    {
        strSQL=strSQL+" and APPOBJ='G'"
    }
    var arrResult = easyExecSql(strSQL);
    if (arrResult != null) 
    {
        if (arrResult[0][0]==0)
        {
            alert("该项目不需要录入明细信息！")
            return false;
         }
        else
            return true;
    }
    else
    {
        alert("该项目定义不完整！")
        return false;
    }
        
}

function showEdor()
{
	var arrReturn = new Array();
				
		var tSel = EdorItemGrid.getSelNo();
			
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击查看按钮。" );
		else
		{
			var state =EdorItemGrid. getRowColData(tSel-1,15) ;
	        
			if (state=="理算完成" || state=="保全确认")
			{
					var EdorAcceptNo=EdorItemGrid. getRowColData(tSel-1,1) ;
					window.open("../f1print/AppEndorsementF1PJ1.jsp?EdorAcceptNo="+EdorAcceptNo+"");
			}
			else{
          alert ("所选批单处于正在申请状态，不能打印！");
			}
		}
}

function showGrupEdor()
{   
	 
	  
    //var arrGrpReturn = new Array();
				
		var tGrpSel = GrpEdorItemGrid.getSelNo();
		if( tGrpSel == null || tGrpSel == 0)
		{
				alert( "请先选择一条记录，再点击查看按钮。" );
		}
		else
		{
			var grpState=GrpEdorItemGrid.getRowColData(tGrpSel-1,7);
			if (grpState=="保全确认" || grpState=="理算完成")
			{
				//fm.all('EdorNo').value = GrpEdorItemGrid.getRowColData(tGrpSel-1,2);
				var Edorno = GrpEdorItemGrid.getRowColData(tGrpSel-1,9);
				//alert(Edorno);
				//alert(GrpEdorItemGrid.getRowColData(tGrpSel-1,1));
				//alert(GrpEdorItemGrid.getRowColData(tGrpSel-1,9));
				//fm.all('GrpContNo').value = GrpEdorItemGrid.getRowColData(tGrpSel-1,2);
				parent.fraInterface.fm.action = "../f1print/GrpEndorsementF1PJ1.jsp?EdorNo=" + Edorno;
				parent.fraInterface.fm.target="f1print";		 	
				fm.submit();
			}	
			else
			{
				alert ("所选批单处于正在申请状态,不能打印批单！");
			}
		}
}

function showInsured(){
	
	
	//var sql = "select EdorType from LPGrpEdorItem " +
	          //"where EdorNo = '" + fm.all('AcceptNo').value + "' ";
	          
	var sql="select EdorType from lpgrpedoritem where "
	       +"edorno=(select workno from lgwork where  workno='" 
	       + fm.all('AcceptNo').value + "') "
	       +"and (edorstate in ('2','10') or '4' in "
	       +"(select statusno from lgwork where workno='" 
	       + fm.all('AcceptNo').value + "'))";
	var arrInsuredResult = easyExecSql(sql, 1, 0);
	
	if (!arrInsuredResult)
	{
		alert("没有找到保全信息！");
		return false;
	}
	var hasNI = false;
	var hasZT = false;
	var hasCT = false;
	var hasLP = false;
	var hasZB = false;
	var hasGA = false;
	var hasWS = false;
	var hasWD = false;
	var hasWT = false;
	
	for (var i = 0; i < arrInsuredResult.length; i++)
	{
		if (arrInsuredResult[i][0] == "NI")
		{
			hasNI = true;
		}
		if (arrInsuredResult[i][0] == "ZT")
		{
			hasZT = true;
		}
		if(arrInsuredResult[i][0] == "CT")
		{
		  hasCT = true;
		}
		if(arrInsuredResult[i][0] == "LP")
		{
		  hasLP = true;
		}
		if(arrInsuredResult[i][0] == "ZB")
		{
		  hasZB = true;
		}
		if(arrInsuredResult[i][0] == "GA")
		{
		  hasGA = true;
		}
		if(arrInsuredResult[i][0] == "WS")
		{
		  hasWS = true;
		}
		if(arrInsuredResult[i][0] == "WD")
		{
		  hasWD = true;
		}
		if(arrInsuredResult[i][0] == "WT")
		{
		  hasWT = true;
		}
		if(hasNI && hasZT && hasCT && hasLP && hasZB && hasGA && hasWS && hasWD)
		{
			break;
		}
	}
	
	if (hasNI == true)
	{
		window.open("../bq/ShowInsuredList.jsp?EdorAcceptNo="+fm.AcceptNo.value);
	}
	if(hasZT == true)
	{
		window.open("../bq/GInsuredListZTMain.jsp?edorType=ZT&edorAcceptNo=" + fm.AcceptNo.value);
	}
	if(hasCT == true)
	{
		window.open("../bq/GInsuredListZTMain.jsp?edorType=CT&edorAcceptNo=" + fm.AcceptNo.value);
	}
	if(hasLP == true)
	{
		window.open("../bq/GInsuredListLPMain.jsp?edorType=LP&edorAcceptNo=" + fm.AcceptNo.value);
	}
	if(hasZB == true)
	{
		window.open("../bq/GInsuredListZBMain.jsp?edorType=ZB&edorAcceptNo=" + fm.AcceptNo.value);
	}
	if(hasGA == true)
	{
		window.open("../bq/GInsuredListGAMain.jsp?edorType=GA&edorAcceptNo=" + fm.AcceptNo.value);
	}
	if(hasWS == true)
	{
		window.open("../bq/GInsuredListWSMain.jsp?edorType=WS&edorAcceptNo=" + fm.AcceptNo.value);
	}
	if(hasWD == true)
	{
		window.open("../bq/GInsuredListWDMain.jsp?edorType=WD&edorAcceptNo=" + fm.AcceptNo.value);
	}
	if(hasWT == true)
	{
		window.open("../bq/GInsuredListWTMain.jsp?edorType=WD&edorAcceptNo=" + fm.AcceptNo.value);
	}
	if(hasNI == false && hasZT == false && hasCT == false&& hasLP == false && hasZB == false && hasGA == false && hasWS == false && hasWD == false && hasWT == false)
	{
		alert("该受理没有被保人清单！");
	}
}