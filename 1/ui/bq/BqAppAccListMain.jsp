<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：GInsuredListZTMain.jsp
//程序功能：保全确认按钮
//创建日期：2005-09-14
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
<title>显示投保人帐户历史轨迹清单 </title>
<%
	String customerNo = request.getParameter("CustomerNo");
	String contType = request.getParameter("ContType");

%>
<script language="javascript">
	var intPageWidth = screen.availWidth;
	var intPageHeight = screen.availHeight;
	window.resizeTo(intPageWidth, intPageHeight);
	window.moveTo(-1, -1);
	window.focus();
</script>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" frameborder="no" border="1" framespacing="0" cols="*">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="yes" noresize src="about:blank" >
	<frame name="fraTitle"  scrolling="no" noresize src="about:blank" >
	<frameset name="fraSet" rows="0,*" frameborder="no" border="1" framespacing="0" rows="*">
		<frame name="fraMenu" scrolling="yes" noresize src="about:blank">
		<frame id="fraPrint" name="fraPrint"" scrolling="auto" src="BqAppAccList.jsp?ContType=<%=contType%>&CustomerNo=<%=customerNo%>">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>