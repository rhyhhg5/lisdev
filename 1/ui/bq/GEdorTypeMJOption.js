var showInfo;

//业务类型改变时显示相应的信息录入页面
function afterCodeSelect(cCodeName, Field)
{
	//缴费方式变更
	if (cCodeName == "PayMode")
	{
		var payMode = fm.all("PayMode").value
		if (payMode == "1") //自行缴费
		{
			if (fm.fmtransact.value == "1") //交费
			{
				document.all("trEndDate").style.display = "";
			}
		  else //退费不设日期
		  {
		  	document.all("trEndDate").style.display = "none";
		  }
			document.all("trPayDate").style.display = "none";
			document.all("trBank").style.display = "none";
			document.all("trAccount").style.display = "none";
		}
		if (payMode == "4") //银行转帐
		{
			document.all("trEndDate").style.display = "none";
			document.all("trPayDate").style.display = "";
			document.all("trBank").style.display = "";
			document.all("trAccount").style.display = "";
		}
	}
}

//提交后操作,服务器数据返回后执行的操作
function AfterEndTimeDeal(flagStr, content)
{
	showInfo.close();
	window.focus();
	
	if (flagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    if (fm.all("printMode")[0].checked)
  	{
  		window.open("GEdorTypeMJCalList.jsp?edorType=MJ&prtType=MJDeal"
  		  + "&edorAcceptNo=" + mEdorAcceptNo, "MJPrint");
  	}   
  	else if (fm.all("printMode")[1].checked) //批量打印 暂时没有实现
  	{   
  	}
  	else if (fm.all("printMode")[2].checked)
  	{   
  	}
  	
  	top.opener.top.opener.location.reload();
  	top.close();
  }
}

function boforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  var getMoney = getGetMoney(mEdorAcceptNo);
  if (getMoney != 0)
  {
    if (fm.PayMode.value == "")
    {
        alert("交费/付费方式不能为空。");
        return false;
    }
    if (fm.PayMode.value == "4")
    {
        if(fm.PayDate.value == "")
        {
            alert("转帐日期不能为空。");
            return false;
        }
        if(!isDate(fm.PayDate.value))
        {
            alert("转帐日期录入有误。");
            return false;
        }
    }
    if (getMoney > 0)
    {
        if (fm.PayMode.value == "1" || fm.PayMode.value == "2") 
        {
          if (fm.EndDate.value == "")
          {
            alert("截止日期不能为空");
            return false;
          }
          if (!isDate(fm.EndDate.value))
          {
            alert("截止日期录入有误。");
            return false;
          }
        }
    }
  }
  
  if (fm.PayMode.value == "4")
  {
    if (fm.Bank.value == "")
    {
      alert("转帐银行为空不能选择银行转帐！");
      return false;
    }
    if (fm.BankAccno.value == "")
    {
      alert("转帐帐号为空不能选择银行转帐！");
      return false;
    }
  }
  return true;
}

//提交
function submitForm()
{
  if (!boforeSubmit())
  {
    return false;
  }
  
  showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
	fm.submit();
}

function getGetMoney(edorAcceptNo)
{
	return fm.sumGetMoney.value;
}

//费用为0则不设交费方式
function queryMenoy(edorAcceptNo)
{
	var getMoney = getGetMoney(edorAcceptNo);
	if (getMoney == null)
	{
		return false;
	}
	if (getMoney == 0)
	{
		trPayMode.style.display = "none";
	}
	else if (getMoney > 0) //交费
	{
		document.all("FeeFlag").style.display = "";
		fm.fmtransact.value = "1";
	}
	else //退费
	{
		fm.fmtransact.value = "0";
	}
}

//得到交费银行和帐号
function queryAccount()
{
	var strSql = "select a.BankCode, a.BankAccno, a.AccName " +
				 "from LCGrpCont a, LPGrpEdorMain b " +
				 "where a.GrpContNo = b.GrpContNo " +
				 "and   b.EdorAcceptNo = '" + mEdorAcceptNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.all("Bank").value = arrResult[0][0];
		fm.all("BankName").value = arrResult[0][0];
		fm.all("BankAccno").value = arrResult[0][1];
		fm.all("AccName").value = arrResult[0][2];
		showOneCodeName("Bank", "BankName");  
	}
}

function cancel()
{
    try
    {
        //从个人信箱直接到批单页面时找不到top.opener.top.opener.top.opener，将会抛出异常
        top.opener.top.opener.top.opener.focus();
        top.opener.top.opener.top.opener.location.reload();
        top.opener.top.opener.top.close();
    }
    catch(e)
    {
        top.opener.top.opener.focus();
        top.opener.top.opener.location.reload();
    }
    
    top.opener.top.close();
    top.close();
}

function controlButton()
{
}