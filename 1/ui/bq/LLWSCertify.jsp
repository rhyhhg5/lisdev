<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：激活卡增加连带被保人
//程序功能：激活卡增加连带被保人
//创建日期：2010-01-05 
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="LLWSCertify.js"></script>
    <%@include file="LLWSCertifyInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">查询条件</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="codeNo" name="ManageCom" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" />
                    </td>
                    <td class="title8">生效日期起</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="StartCValidate" verify="生效日期起|date" />
                    </td>
                    <td class="title8">生效日期止</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="EndCValidate" verify="生效日期止|date" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title8">保险卡号</td>
                    <td class="input8">
                        <input class="common" name="CardNo" verify="保险卡号|len>=11&len<=12" maxlength="12" />
                    </td>
                    <td class="title8">结算单号</td>
                    <td class="input8">
                        <input class="common" name="PrtNo" verify="结算单号|len=11" maxlength="11" />
                    </td>
                    <TD  class= title8>客户姓名</TD>
                    <TD  class= input8>
                        <Input class=common name="CustomerName" >
                    </TD>
                </tr>
                <tr class="common">
                    <td class="title8">证件类型</td>
                    <td class="input8">
                        <Input  onClick="showCodeList('idtype',[this,tIDTypeName],[0,1]);" onkeyup="showCodeListKeyEx('idtype',[this,tIDTypeName],[0,1]);" class=codeno name="tIDType" value="0"><Input class= codename name=tIDTypeName value="身份证">
                    </td>
                    <td class="title8">证件号码</td>
                    <td class="input8">
                        <Input class=common name="tIDNo" >
                    </td>
                    <TD  class= title8></TD>
                    <TD  class= input8></TD>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryCertifyList();" /> 	
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportBatchListGrid);" />
                </td>
                <td class="titleImg">卡折清单列表</td>
            </tr>
        </table>
        <div id="divCertifyListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertifyListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divCertifyListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>        
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnWSCertify" value="增加连带被保险人" onclick="addPeoples();" />   
                </td>
            </tr>
        </table>
<input type="hidden" name="BCardNo" >
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
