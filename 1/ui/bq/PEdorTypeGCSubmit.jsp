<%
//程序名称：PEdorTypeGCSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
  //个人批改信息
  System.out.println("---GC submit---");
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = request.getParameter("fmtransact");
  System.out.println("transact: " + transact);
  
  //全局变量
  GlobalInput tGlobalInput = new GlobalInput();
  tGlobalInput = (GlobalInput)session.getValue("GI");
  
  //批改信息
  LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
  tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));
  tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));
  
  LPGetSchema tLPGetSchema = new LPGetSchema();
  tLPGetSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPGetSchema.setEdorType(request.getParameter("EdorType"));
  tLPGetSchema.setPolNo(request.getParameter("PolNo"));
  tLPGetSchema.setDutyCode(request.getParameter("DutyCode"));
  tLPGetSchema.setGetDutyCode(request.getParameter("GetDutyCode"));
  tLPGetSchema.setGetDutyKind(request.getParameter("GetDutyKind"));
  //tLPGetSchema.setGetIntv(request.getParameter("GetIntv"));
  
  //准备传输数据
  VData tVData = new VData();  
  tVData.add(tGlobalInput);
  tVData.add(tLPEdorMainSchema); 
  tVData.add(tLPGetSchema); 
      
  CErrors tError = null;                 
  String FlagStr = "";
  String Content = "";
  String Result = ""; 

  PEdorGCDetailUI tPEdorGCDetailUI = new PEdorGCDetailUI();
  if (!tPEdorGCDetailUI.submitData(tVData, transact)) {
    FlagStr = "Fail"; 
    Content = "保存失败!";
    
    VData rVData = tPEdorGCDetailUI.getResult();
    System.out.println("Submit Failed! " + (String)rVData.get(0));
  }
  else {
  	FlagStr = "Success";
        Content = "保存成功!";
        if (transact.equals("QUERY||MAIN")) {
            Content = "查询成功!";
            if (tPEdorGCDetailUI.getResult()!=null && tPEdorGCDetailUI.getResult().size()>0) {
  	        Result = (String)tPEdorGCDetailUI.getResult().get(0);
  		if (Result==null || Result.trim().equals("")) {
  		     FlagStr = "Fail"; 
  		     Content = "查询失败!";
  		}
  	    }
	}
  		
    System.out.println("Submit Succed!");
  }
	
  //添加各种预处理
  System.out.println("------------Result is------------\n" + Result);
%>   
                   
<html>
<script language="javascript">

	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>");
</script>
</html>

