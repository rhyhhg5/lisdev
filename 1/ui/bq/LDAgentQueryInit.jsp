<%
//程序名称：LDBankQueryInit.jsp
//程序功能：
//创建日期：2007-10-14 12:57
//创建人  ：qulq程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
     GlobalInput tG1 = (GlobalInput)session.getValue("GI");
     String comcode = tG1.ComCode;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
		fm.all('comcode').value = '<%=comcode%>';
    fm.all('agentCode').value = '';
    fm.all('agentName').value = '';
    
  }
  catch(ex)
  {
    alert("在LDAgentQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      

function initForm()
{
  try
  {
    initInpBox();  
    initAgentGrid();
  }
  catch(re)
  {
    alert("在LDAgentQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initAgentGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="业务员代码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="业务员名字";        //列名
      iArray[2][1]="120px";            	//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      //是否允许输入,1表示允许，0表示不允许      

      AgentGrid = new MulLineEnter( "fm" , "AgentGrid" ); 
      //这些属性必须在loadMulLine前
      AgentGrid.mulLineCount = 10;   
      AgentGrid.displayTitle = 1;
      AgentGrid.locked = 1;
      AgentGrid.canSel = 1;
      AgentGrid.hiddenPlus=1;
      AgentGrid.hiddenSubtraction=1;
      AgentGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>