<%
//程序名称：PEdorTypeBASubmit.jsp
//程序功能：
//创建日期：2008-04-10
//创建人  ：pst
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

	transact = request.getParameter("fmtransact");
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String contNo = request.getParameter("ContNo");
	
	String tGridNo[] = request.getParameterValues("PolGridNo");  //得到序号列的所有值
	String tGrid2 [] = request.getParameterValues("PolGrid2"); //PolNo
	String tGrid10 [] = request.getParameterValues("PolGrid10"); //AppMoney
    String tGrid11 [] = request.getParameterValues("PolGrid11"); //DetailType
    
    LPEdorEspecialDataSet tLPEdorEspecialDataSet = new LPEdorEspecialDataSet();
    
    int tCount = tGridNo.length; //得到接受到的记录数
	for(int index=0;index< tCount;index++)
    {
      LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
      tLPEdorEspecialDataSchema.setEdorAcceptNo(edorAcceptNo);
      tLPEdorEspecialDataSchema.setEdorNo(edorNo);
      tLPEdorEspecialDataSchema.setEdorType(edorType);
      tLPEdorEspecialDataSchema.setPolNo(tGrid2[index]);
      tLPEdorEspecialDataSchema.setEdorValue(tGrid10[index]);
      tLPEdorEspecialDataSchema.setDetailType(tGrid11[index]);
      tLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);
    }
	
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setContNo(contNo);
	tLPEdorItemSchema.setEdorType(edorType);
	tLPEdorItemSchema.setInsuredNo("000000");
	tLPEdorItemSchema.setPolNo("000000");
    // 准备传输数据 VData		
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPEdorItemSchema);
    tVData.add(tLPEdorEspecialDataSet);
    PEdorBADetailUI tPEdorBADetailUI   = new PEdorBADetailUI();  
    if (!tPEdorBADetailUI.submitData(tVData, ""))
	{
		VData rVData = tPEdorBADetailUI.getResult();
		System.out.println("Submit Failed! " + tPEdorBADetailUI.mErrors.getErrContent());
		Content = transact + "失败，原因是:" + tPEdorBADetailUI.mErrors.getFirstError();
		FlagStr = "Fail";
	}
	else 
	{
		Content = "保存成功";
		FlagStr = "Success";
	} 

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>