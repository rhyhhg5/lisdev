//程序名称：BalanceList.js
//程序功能：万能保单月度结算清单查询
//创建日期：2007-12-14
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}
function afterSubmit(FlagStr, content) 
{
  try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  if (FlagStr == "Fail" ) 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//打印按钮对应操作
function print()
{
  if(fm.Sql.value == "")
  {
    alert("请先查询!");
    return false;
  }
  if(LCInsureAccBalanceGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据!");
    return false;
  }
  fm.action = "BalanceListSave.jsp";
  fm.submit(); //提交
}

//下载按钮对应操作
function download()
{
  if(fm.Sql.value == "")
  {
    alert("请先查询!");
    return false;
  }
  if(LCInsureAccBalanceGrid.mulLineCount == 0)
  {
    alert("没有需要下载的数据!");
    return false;
  }
  
  fm.action = "BalanceDownLoad.jsp";
  fm.submit(); 
}   

function easyQueryClick() 
{
  var strSql = "select (select name from ldcom where comcode = b.ManageCom), "
             + "   b.ContNo, b.AppntName, year(DueBalaDate - 1 days), month(DueBalaDate - 1 days), "
             + "   nvl((select sum(Money) from LCInsureAccTrace where OtherType = '6' and MoneyType = 'LX' and OtherNo = c.SequenceNo),0), "
             + "   nvl((select abs(sum(Money)) from LCInsureAccTrace where OtherType = '6' and MoneyType = 'MF' and OtherNo = c.SequenceNo),0), "
             + "   nvl((select abs(sum(Money)) from LCInsureAccTrace where OtherType = '6' and MoneyType = 'RP' and OtherNo = c.SequenceNo),0), "
             + "   nvl((select abs(sum(Money)) from LCInsureAccTrace where OtherType = '6' and MoneyType = 'B' and OtherNo = c.SequenceNo),0), "
             + "   cast(round(c.InsuAccBalaAfter,2) as  decimal(20,2)), "
             + "   (c.DueBalaDate - 1 day), e.Name, getUniteCode(e.AgentCode), (select Name from LABranchGroup where AgentGroup = b.AgentGroup), "
             + "   (select min(Mobile) from LCAppnt x, LCAddress y where x.AppntNo = y.CustomerNo and x.AddressNo = y.AddressNo and x.ContNo = b.ContNo) "
             + "from LCCont b, LCInsureAccBalance c, LAAgent e " 
             + "where 1 = 1 "
             + "and b.managecom like '" + fm.ManageCom.value + "%' "
             + getWherePart('c.RunDate', 'RunDateStart','>=')
             + getWherePart('c.RunDate', 'RunDateEnd','<=')
             + getWherePart('b.SaleChnl', 'SaleChnl','=')
             + getWherePart('b.ContNo', 'ContNo')
             + getWherePart('b.AppntName', 'AppntName', '=')
             + "   and b.ContNo = c.ContNo "
             + "   and b.AgentCode = e.AgentCode "
             + "   and year(DueBalaDate - 1 days) = " + fm.PolYear.value
             + "   and month(DueBalaDate - 1 days) = " + fm.PolMonth.value
             + "   and c.BalaCount > 0 with ur";
             //+ "group by  b.ManageCom, b.ContNo, b.AppntName, c.PolYear, c.PolMonth, c.InsuAccBalaAfter,c.SequenceNo,c.DueBalaDate, e.Name, e.AgentCode,b.AgentGroup ";
               
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(strSql, LCInsureAccBalanceGrid);
  fm.Sql.value = strSql;
  return true;
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{ 
  if(!verifyInput2())
  {
    return false;
  }
  if(!easyQueryClick())
  {
    return false;
  }
  if(LCInsureAccBalanceGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }
}

//回车查询
function queryByKeyDown()
{
  if(event.keyCode == "13")
	{
	  queryClick();
	}
}

//打印所选帐户轨迹
function printAcc()
{
  if(LCInsureAccBalanceGrid.mulLineCount == 0)
  {
    alert("请先查询账户");
    return false;
  }
  
  var row = LCInsureAccBalanceGrid.getSelNo() - 1;
  if(row < 0)
  {
    alert("请选择需要打印的账户");
    return false;
  }
  
  var sql = "select PolNo, InsuAccNo, ContNo, (select AppntName from LCCont where ContNo = a.ContNo) "
          + "from LCInsureAcc a "
          + "where ContNo = '" + LCInsureAccBalanceGrid.getRowColDataByName(row, "ContNo") + "' ";
  var rs = easyExecSql(sql);
  if(rs == null || rs[0][0] == null || rs[0][0] == "")
  {
    alert("没有查询到账户信息");
    return false;
  }
  
  var tPolNo = rs[0][0];
  var tInsuAccNo = rs[0][1];
  var tContNo = rs[0][2];
  
  fm.action = "BalanceListPrint.jsp?ContNo=" + tContNo + "&PolNo=" + tPolNo + "&InsuAccNo=" + tInsuAccNo;
	fm.target = "_blank";
	fm.submit();
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}