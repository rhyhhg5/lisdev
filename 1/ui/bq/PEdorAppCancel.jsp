<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="PEdorAppCancel.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PEdorAppCancelInit.jsp"%>
  <title>保全查询 </title>
</head>
<body  onload="initForm();">
  <form action="./PEdorAppCancelSubmit.jsp" method=post name=fm target="fraSubmit" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入个人保全查询条件：
			</td>
	</tr>
	</table>
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title> 管理机构 </td>
        <td  class= input>
          <input class="readonly" name=ManageCom readonly>
        </td>
        <td  class= title> 保全受理号 </td>
        <td  class= input>
          <input class= common name=EdorAcceptNo >
        </td>
        <td  class= title> 客户号  </td>
        <td  class= input>
          <input class= common name=OtherNo >
        </td>
      </tr>
      <tr class= common style="display:none">
        <td  class= title> 申请号码类型 </td>
        <td  class= input>
          <input class= common name=OtherNoType >
        </td>
        <td  class= title> 批改状态 </td>
        <td  class= input>
          <input class="common" name=EdorAppState >
        </td>
      </tr>
    </table>
    <table class= common align=center>
          <INPUT class = cssbutton VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
    </table>
<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 保全申请信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorApp" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanEdorAppGrid" >
  					</span> 
  			  	</td>
  			</tr> 		
	
    	</table>
      <INPUT class = cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class = cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class = cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class = cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">	 

      
   	<table align=center cellpadding="2" class= common>
    		<tr class= common>
    			<td width="2%" height="24" valign="middle" class="input" ><div align="left">撤销原因
                </div></td>
   			    <td width="98%" valign="middle">	<input type="input" class="code" name='CancelAppReasonCode' readOnly elementtype="nacessary" ondblclick="showCodeList('CancelEdorReason', [this, delAppReason], [0, 1]);" onkeyup=" showCodeListKey('CancelEdorReason', [this, delAppReason] , [0, 1]);"></td>
    		</tr>
    		<tr class= input>
    		  <td valign="middle" class="input" ><div align="left">详细情况</div></td>
  		      <td width="98%" valign="middle"><textarea name="delAppReason" cols="60" rows="3" class="common" id="delAppReason"></textarea>
	          <input name="button2" type=button class= cssbutton onClick="CancelEdorApp();" value="申请撤销"></td>
   		  </tr>
   	</table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 保全批单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanEdorMainGrid" >
  					</span> 
  			  	</td>
  			</tr> 		
	
    	</table>
      <INPUT class = cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class = cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class = cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class = cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">	 

      
   	<table class= common align=center>
    		<tr class= common>
    			<td width="2%" class="input" >
    			撤销原因 </td>
   			    <td width="98%">	<input type="input" class="code" name='CancelMainReasonCode' readOnly elementtype="nacessary" ondblclick="showCodeList('CancelEdorReason', [this, delMainReason], [0, 1]);" onkeyup=" showCodeListKey('CancelEdorReason', [this, delMainReason] , [0, 1]);"></td>
    		</tr>
    		<tr class= common>
    		  <td class="input" >详细情况</td>
  		      <td width="98%"  ><textarea name="delMainReason" cols="60" rows="3" class="common"></textarea>
	          <input name="button3" type=button class= cssbutton onClick="CancelEdorMain();" value="批单撤销"></td>
   		  </tr>
   	</table>
    	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpEdorItem);">
    		</td>
    		<td class= titleImg>
    			 保全项目信息
    		</td>
    	</tr>
    </table>
  	
  	<Div  id= "divLPEdorItem" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanEdorItemGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>


   	<table align=center bgcolor="f9f9f9" class= common>
    		<tr class= common>
    			<td width="2%" class="input">撤销原因 </td>
   			    <td width="98%" >
		          <input type="input" class="code" name='CancelItemReasonCode' readOnly elementtype="nacessary" ondblclick="showCodeList('CancelEdorReason', [this, delItemReason], [0, 1]);" onkeyup=" showCodeListKey('CancelEdorReason', [this, delItemReason] , [0, 1]);"></td>
    		</tr>
    		<tr class= common>
    		  <td class="input">详细情况</td>
  		      <td width="98%" ><textarea name="delItemReason" cols="60" rows="3" class="common"></textarea>
	          <input name="button" type=button class= cssbutton onClick="CancelEdorItem();" value="申请项目撤销"></td>
   		  </tr>
   	  </table>        			
  	</div>
  	<input type=hidden name=Transact >
  	<input type=hidden name=DelFlag >
  	<input type=hidden name=EdorNo >
  	<input type=hidden name=ContNo >
  	<input type=hidden name=EdorMainState >
  	<input type=hidden name=EdorItemState >
  	<input type=hidden name=EdorType >
  	<input type=hidden name=MakeDate>
  	<input type=hidden name=MakeTime>
  	 <input type=hidden name=InsuredNo>
  	<input type=hidden name=PolNo>
  	<input type=hidden name=hEdorAcceptNo>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
