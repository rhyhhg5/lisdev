//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var mFlag;
var addrFlag= "MOD";

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();  

function verifyPage()
{ 
	//以年龄和性别校验身份证号
	if (fm.all("IDType").value == "0") 
	var strChkIdNo = chkIdNo(fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);

	if (strChkIdNo!="" && strChkIdNo!=null)
	{
		//alert(strChkIdNo);
		return false;
	}

	return true;
}

function edorTypeICReturn()
{
	initForm();
}

function edorTypeICSave()
{
	
	fm.all('addrFlag').value=addrFlag;
	if (!verifyPage()) return;

/*
	if (fm.Name.value == fm.NameBak.value && fm.Sex.value == fm.SexBak.value &&
		fm.Birthday.value == fm.BirthdayBak.value && fm.IDType.value == fm.IDTypeBak.value &&
		fm.IDNo.value == fm.IDNoBak.value)
	{
		alert('被保人资料未发生变更！');
		return;
	}
	*/

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmtransact').value = "INSERT||MAIN";
	fm.submit();

}

function customerQuery()
{	
	window.open("./LCInsuredQuery.html");
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initLCInsuredGrid();
 //  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		var tTransact=fm.all('fmtransact').value;
  	//alert(tTransact);
		if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
	  		//使用模拟数据源，必须写在拆分之前
  			turnPage.useSimulation   = 1;  
    
  			//查询成功则拆分字符串，返回二维数组
	  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
			
			turnPage.arrDataCacheSet =chooseArray(tArr,[3,4,14,15,16,11]);
			//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		 	turnPage.pageDisplayGrid = LCInsuredGrid;    
		  
			//设置查询起始位置
		 	turnPage.pageIndex       = 0;
		 	//在查询结果数组中取出符合页面显示大小设置的数组
		  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
			//调用MULTILINE对象显示查询结果
		   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
		}
		else if (tTransact=="QUERY||DETAIL")
		{
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
			turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
			//	alert(Result);
			turnPage.strQueryResult  = Result;
			//使用模拟数据源，必须写在拆分之前
			turnPage.useSimulation   = 1;  

			//查询成功则拆分字符串，返回二维数组
			var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,tTransact);
			turnPage.arrDataCacheSet =chooseArray(tArr,[4,14,15,16,17,18,36,37,19,22]);

			fm.all('CustomerNo').value = turnPage.arrDataCacheSet[0][0];
			fm.all('Name').value = turnPage.arrDataCacheSet[0][1];
			fm.all('NameBak').value = turnPage.arrDataCacheSet[0][1];
			fm.all('Sex').value = turnPage.arrDataCacheSet[0][2];
			fm.all('SexBak').value = turnPage.arrDataCacheSet[0][2];
			fm.all('Birthday').value = turnPage.arrDataCacheSet[0][3];
			fm.all('BirthdayBak').value = turnPage.arrDataCacheSet[0][3];
			fm.all('IDType').value = turnPage.arrDataCacheSet[0][4];
			fm.all('IDTypeBak').value = turnPage.arrDataCacheSet[0][4];
			fm.all('IDNo').value = turnPage.arrDataCacheSet[0][5];
			fm.all('IDNoBak').value = turnPage.arrDataCacheSet[0][5];
			fm.all('OccupationType').value = turnPage.arrDataCacheSet[0][6];
			fm.all('OccupationCode').value = turnPage.arrDataCacheSet[0][7];
			fm.all('NativePlace').value = turnPage.arrDataCacheSet[0][8];
			fm.all('Marriage').value = turnPage.arrDataCacheSet[0][9];
			divLPInsuredDetail.style.display ='';
			divDetail.style.display='';
			parent.focus();
			queryAddress();
			queryBank();
			

		}
		else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
			edorTypeICReturn();

	 }
	 
	}
	
	
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		/**
		alert("aa:"+arrResult[0][5]);
		fm.all('Nationality').value = arrResult[0][5];
		fm.all('Marriage').value=arrResult[0][6];
		fm.all('Stature').value=arrResult[0][7];
		fm.all('Avoirdupois').value=arrResult[0][8];
		fm.all('ICNo').value=arrResult[0][9];
		fm.all('HomeAddressCode').value=arrResult[0][10];
		fm.all('HomeAddress').value=arrResult[0][11];
		fm.all('PostalAddress').value=arrResult[0][12];
		fm.all('ZipCode').value=arrResult[0][13];
		fm.all('Phone').value=arrResult[0][14];
		fm.all('Mobile').value=arrResult[0][15];
		fm.all('EMail').value=arrResult[0][16];
		*/
		// 查询保单明细
		queryInsuredDetail();
	}
}
function queryInsuredDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	//alert(tEdorNo);
	tEdorType=fm.all('EdorType').value;
	//alert(tEdorType);
	tPolNo=fm.all('PolNo').value;
	//alert(tPolNo);
	tCustomerNo = fm.all('CustomerNo').value;
	//alert(tCustomerNo);
	//top.location.href = "./InsuredQueryDetail.jsp?EdorNo=" + tEdorNo+"&EdorType="+tEdorType+"&PolNo="+tPolNo+"&CustomerNo="+tCustomerNo;
	parent.fraInterface.fm.action = "./InsuredQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorTypeICSubmit.jsp";
}
function returnParent()
{
	top.close();
}





////-----------------------------------------------------------------------------------------
////add by lanjun 2005-5-24 16:16 合并基本资料和重要资料变更

function queryAddress()
{
	  var strSQL= "select homeaddress,postaladdress,zipcode,phone,mobile,addressno from lpaddress where  customerno= '" + fm.CustomerNo.value + "' and addressno='" + fm.AddressNo.value +"'  and EdorType='" + fm.EdorType.value + "' ";
    var arrResult1 = easyExecSql(strSQL,1,0);
   // alert(arrResult1);
    if(arrResult1 == null)
    {
    	strSQL1 = " select  homeaddress,postaladdress,zipcode,phone,mobile,addressno from lpaddress where  customerno= '" + fm.CustomerNo.value + "' union select  homeaddress,postaladdress,zipcode,phone,mobile,addressno from lcaddress where  customerno= '" + fm.CustomerNo.value + "' ";  
      //alert(strSQL1);
      arrResult1 = easyExecSql(strSQL1,1,0);
      if(arrResult1 == null)
      {
      	//alert('未查询到被保人地址信息');
      }
      else
      {
      	addrFlag="NEW";
        afterQuery1(arrResult1);
        setReadOnly('Y');	
      }  
    }  	    
    else
    {
    	addrFlag="MOD";    
      afterQuery1(arrResult1);
      setReadOnly('N');	
    }  

}




function getaddresscodedata()
{
    var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select AddressNo,HomeAddress from LCAddress where CustomerNo ='"+fm.CustomerNo.value+"'"
    + " union select AddressNo,HomeAddress from LpAddress where CustomerNo ='"+fm.CustomerNo.value+"'  and EdorNo='"+fm.EdorNo.value+"'";
    //alert("strsql :" + strsql);
    turnPage1.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
    if (turnPage1.strQueryResult != "")
    {
    	turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
    	m = turnPage1.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		tCodeData = tCodeData + "^" + turnPage1.arrDataCacheSet[i][0] + "|" + turnPage1.arrDataCacheSet[i][1];
    	}
    }
    //alert ("tcodedata : " + tCodeData);
    //return tCodeData;
    fm.all("AddressNo").CodeData=tCodeData;
}

function setAddr()
{

    if (fm.AddressNo.value=='')
    {
        addrFlag="MOD";
        setReadOnly('N');        
    }
}    


function setReadOnly(flag)
{
   
    var tflag=flag
    if (flag=='Y')
    {
        

        try {fm.all('HomeAddress').readOnly= true; } catch(ex) { };
        
        try {fm.all('PostalAddress').readOnly= true; } catch(ex) { };
        try {fm.all('ZipCode').readOnly= true; } catch(ex) { };

        try {fm.all('Phone').readOnly= true; } catch(ex) { };
        try {fm.all('Mobile').readOnly= true; } catch(ex) { };
        
    }
    else
    {

        try {fm.all('HomeAddress').readOnly= false; } catch(ex) { };
        
        try {fm.all('PostalAddress').readOnly= false; } catch(ex) { };
        try {fm.all('ZipCode').readOnly= false; } catch(ex) { };

        try {fm.all('Phone').readOnly= false; } catch(ex) { };
        try {fm.all('Mobile').readOnly= false; } catch(ex) { };
 

    }
}

function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="GetAddressNo")
 {
     var arrResult1;
     var strSQL;
     strSQL="select b.AddressNo,b.HomeAddress,b.PostalAddress,b.ZipCode,b.Phone,b.mobile from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.CustomerNo.value+"'";
   
     arrResult1 = easyExecSql(strSQL);
     //alert(arrResult1[0]);
     
     if (arrResult1 == null)
     {
        addrFlag="MOD";
        strSQL="select b.AddressNo,b.HomeAddress,b.PostalAddress,b.ZipCode,b.Phone,b.mobile from LPAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.CustomerNo.value+"'";
        arrResult1 = easyExecSql(strSQL);
        
     }
     else
    {
        addrFlag="NEW";
    }
     
   
     try {fm.all('AddressNo').value= arrResult1[0][0]; } catch(ex) { };
     
     try {fm.all('HomeAddress').value= arrResult1[0][1]; } catch(ex) { };
     try {fm.all('PostalAddress').value= arrResult1[0][2]; } catch(ex) { };
     try {fm.all('ZipCode').value= arrResult1[0][3]; } catch(ex) { };
     try {fm.all('Phone').value= arrResult1[0][4]; } catch(ex) { };
     try {fm.all('Mobile').value= arrResult1[0][5]; } catch(ex) { };

     if (addrFlag=="NEW")
        setReadOnly('Y');
     else if (addrFlag=="MOD")
        setReadOnly('N');
     
  }
}

function afterQuery1( arrQueryResult1 )
{
	if( arrQueryResult1 != null )
	{
		fm.all('HomeAddress').value=arrQueryResult1[0][0];
		fm.all('PostalAddress').value=arrQueryResult1[0][1];
		fm.all('ZipCode').value=arrQueryResult1[0][2];
		fm.all('Phone').value=arrQueryResult1[0][3];
		fm.all('Mobile').value=arrQueryResult1[0][4];		
		fm.all('addressno').value= 	arrQueryResult1[0][5];	
		//alert(arrQueryResult1[0][5]);
	}
}
//------------------------------------------------------------------------------




//add by lanjun 2005-5-24 16:15 修改理赔金帐户
//-------------------------------------------------------------------------

function queryBank()
 {
    var i = 0;
    var tCustomerNo = fm.all('CustomerNo').value;
    var tContNo = fm.all('ContNo').value; 
    var showStr;
    var urlStr;
//    alert(tContNo1)	;
    if (tCustomerNo==null||tCustomerNo =='') 
    {
    	//alert("请输入申请号码以及申请号码类型!");
    }
    else
    {
         var strSQL= "select bankcode,AccName,BankAccNo from lpinsured where insuredno='"+ tCustomerNo + "' and  contno='"+tContNo +"' and edorno='"+fm.EdorNo.value+"' and edortype='"+fm.EdorType.value+"'";		
          //alert("strSQL"+strSQL);        
 	       var strQueryResult  = easyExecSql(strSQL, 1, 0);
            //判断是否查询成功
         if (strQueryResult == null) 
         {
             var strSQL= "select bankcode,AccName,BankAccNo from lcinsured where insuredno='"+ tCustomerNo + "' and  contno='"+tContNo +"'";		
             strQueryResult  = easyExecSql(strSQL, 1, 0); 
         }
         
         
         
         var sqlc= "select bankcode,AccName,BankAccNo from lccont where  contno='"+tContNo +"'";		
         resultc  = easyExecSql(sqlc, 1, 0); 

         
         if(strQueryResult != null)
         {
         	if (strQueryResult[0][2] != ""&&resultc[0][2]== strQueryResult[0][2])
         	{ 
         		fm.AccountNo.value = 1;
         	}
          else  fm.AccountNo.value = 2;
         	fm.BankCode.value = strQueryResult[0][0];
         	//alert(strQueryResult[0][0]);
         	fm.AccName.value = strQueryResult[0][1];
         	fm.BankAccNo.value = strQueryResult[0][2];
         }
    }
}


function getdetailaccount()
{

	if(fm.AccountNo.value=="1")
	{
		     var tContNo = fm.all('ContNo').value; 
		     var strSQL= "select bankcode,AccName,BankAccNo from lccont where  contno='"+tContNo +"'";		
         strQueryResult  = easyExecSql(strSQL, 1, 0); 
         
         if(strQueryResult != null)
         {
         	fm.BankCode.value = strQueryResult[0][0];
         	fm.AccName.value = strQueryResult[0][1];
         	fm.BankAccNo.value = strQueryResult[0][2];
         	
         	fm.BankCodeBak.value = strQueryResult[0][0];
         	fm.AccNameBak.value = strQueryResult[0][1];
         	fm.BankAccNoBak.value = strQueryResult[0][2];
         }
	}
	if(fm.AccountNo.value=="2")
	{
		/*
           fm.all('BankAccNo').value="";
           fm.all('BankCode').value="";
           fm.all('AccName').value="";		
           	
           fm.all('BankAccNoBak').value="";
           fm.all('BankCodeBak').value="";
           fm.all('AccNameBak').value="";		
    */	
	}
	
}
function getdetail()
{
	/*
var strSql = "select BankCode,AccName from LCAccount where BankAccNo='" + fm.BankAccNo.value+"'";
arrResult = easyExecSql(strSql);
if (arrResult != null) {
      fm.BankCode.value = arrResult[0][0];
      fm.AccName.value = arrResult[0][1];
      
      fm.BankCodeBak.value = arrResult[0][0];
      fm.AccNameBak.value = arrResult[0][1];
    }
	*/
}

