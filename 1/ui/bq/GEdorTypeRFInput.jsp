<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2003-01-14 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <SCRIPT src="./GEdorTypeRF.js"></SCRIPT>
  <%@include file="GEdorTypeRFInit.jsp"%> 
</head>

<body  onload="initForm();" >
  <form action="./GEdorTypeRFSubmit.jsp" method=post name=fm target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= title > 
        批单号
      </TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 
        批改类型 
      </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>    
      <TD class = title >  
        集体保单号 
      </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpPolNo>
      </TD>   
    </TR>
  </TABLE> 
  
  <table>
   	<tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
      </td>
      <td class= titleImg>
        投保单位详细信息
      </td>
   	</tr>
  </table> 
   
  <Div  id= "divLCPol" style= "display: ''"> 
  <table  class= common>
   	<TR  class= common>
  	  <TD class = title>
  	     险种编码
  	  </TD>
  	  <TD class= input>
  	    <Input class= "readOnly" readonly  name=RiskCode >
  	  </TD>      
    </TR>
       
    <TR>
      <TD class = title>
         单位编码
      </TD>
      <TD class= input>
        <Input class= "readOnly" readonly  name=GrpNo >
      </TD>     
      <TD class= title>
       单位名称
      </TD>
  	  <TD class= input>
  	    <Input class= "readOnly" readonly  name=GrpName >
  	  </TD>      
    </TR> 
    	
    <TR>
		  <TD class= title>
		    生效日期
		  </TD>
		  <TD class= input>
		    <Input class= "readOnly" readonly  name=CValidate >
		  </TD>      
      <TD class= title>
		    交至日期
		  </TD>
		  <TD class= input>
		    <Input class= "readOnly" readonly  name=PayToDate >
		  </TD>	 
		</TR>
      
    <TR>
       <TD class= title>
       总保费
       </TD>
       <TD class= input>
         <Input class= "readOnly" readonly  name=Prem >
       </TD>         
       <TD class= title>
       总保额
       </TD>
       <TD class= input>
         <Input class= "readOnly" readonly  name=Amnt >
       </TD>       
     </TR>

    </Table>
    </Div>
    
    <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLoan);">
      </td>
      <td class= titleImg>
        借款信息
      </td>
   </tr>
   </table>
	 
    <Div  id= "divLoan" style= "display: ''">
    	<table  class= common>
     		<tr  class= common>
    	  	<td text-align: left colSpan=1>
					<span id="spanLoanGrid" >
					</span> 
			  	</td>
  			</tr>
    	</table>
      
      <INPUT class= common VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT class= common VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT class= common VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT class= common VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
    
    <table class= common>
		<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeRFSave()">
	     	 </TD>
	     	 <TD  class= input width="26%"> 
	       	 <Input class= common type=Button value="返回" onclick="returnParent()">
	     	 </TD>
	    </TR>
    </table>
	
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
	 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
