
<%
	//程序名称：LMinterestRateSave.jsp
	//程序功能：
	//创建日期：2011-11-23 
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息

	CErrors tError = null;
	//后面要执行的动作：添加，修改
	String FlagStr = "";
	String Content = "";
	String transact = "";
	
	//执行动作：insert 添加纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");
	transact = request.getParameter("fmtransact");
	String RiskCode = request.getParameter("RiskCode");
	String rateStartDate = request.getParameter("rateStartDate");
	String rateEndDate = request.getParameter("rateEndDate");
	String Rate = request.getParameter("Rate");
	String DRiskCode = request.getParameter("DRiskCode");
	String DrateStartDate = request.getParameter("DrateStartDate");
	String DrateEndDate = request.getParameter("DrateEndDate");
	String aManageCom = request.getParameter("aManageCom");
	String MinPremLimit = request.getParameter("MinPremLimit");
	String MaxPremLimit = request.getParameter("MaxPremLimit");
	String DManageCom = request.getParameter("DManageCom");
	
	java.util.Date udate = new java.util.Date();
	java.sql.Date sdate = new java.sql.Date(udate.getTime());
	
	LMInterestRateSchema tLMInterestRateSchema = new LMInterestRateSchema();
	tLMInterestRateSchema.setRiskCode(RiskCode);
	tLMInterestRateSchema.setStartDate(rateStartDate);
	tLMInterestRateSchema.setEndDate(rateEndDate);
	tLMInterestRateSchema.setComCode(aManageCom);
	tLMInterestRateSchema.setMinPremLimit(MinPremLimit);
	tLMInterestRateSchema.setMaxPremLimit(MaxPremLimit);
	tLMInterestRateSchema.setBussType("L");
	tLMInterestRateSchema.setARateDate(sdate);
	tLMInterestRateSchema.setRateType("C");
	tLMInterestRateSchema.setRateIntv("1");
	tLMInterestRateSchema.setRateIntvUnit("Y");
	tLMInterestRateSchema.setRate(Rate);
	
	LMInterestRateSchema DLMInterestRateSchema = new LMInterestRateSchema();
	DLMInterestRateSchema.setComCode(DManageCom);
	DLMInterestRateSchema.setRiskCode(DRiskCode);
	DLMInterestRateSchema.setStartDate(DrateStartDate);
	DLMInterestRateSchema.setEndDate(DrateEndDate);
	DLMInterestRateSchema.setBussType("L");
	DLMInterestRateSchema.setRateType("C");
	DLMInterestRateSchema.setRateIntv("1");
	DLMInterestRateSchema.setRateIntvUnit("Y");
	LMInterestRateUI tLMInterestRateUI = new LMInterestRateUI();
	try{
		// 准备传输数据 VData	
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tVData.add(tGlobalInput);
		tTransferData.setNameAndValue("tLMInterestRateSchema",tLMInterestRateSchema);
		tTransferData.setNameAndValue("dLMInterestRateSchema",DLMInterestRateSchema);
		tVData.add(tTransferData);
		tLMInterestRateUI.submitData(tVData,transact);
	}catch(Exception ex){
		Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
	if (FlagStr == "")
  { 
    tError = tLMInterestRateUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = "操作成功! ";
      FlagStr = "Success";
    }
    else                                                                           
    {
      Content = "操作失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=transact%>");
</script>
</html>

