<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeBC.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeBCInit.jsp"%>
  
  <title>受益人资料变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeBCSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
     
      <TD class = title > 集体保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpPolNo>
      </TD>   
    </TR>
  </TABLE> 
  
  <div id = "divPolInfo" style = "display:''">
   <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPPol);">
      </td>
      <td class= titleImg>
        集体下个单信息
      </td>
   </tr>
   </table>
	 
    <Div  id= "divLPPol" style= "display: ''">
    	<table class = common>
  <tr class = common>
  	<td class= title>
        可按输入条件查询:
     </td> 
  	<td class = title>
  		个人保单号
  		</td>
  	<td class = input>
  		<input class = common  name= PolNo>
      </TD>
     <td class = title>
  		个人客户号
  		</td>
  	<td class = input>
  		<input class = common  name= CustomerNo>
      </TD>
    <td class = input>
    	      <INPUT VALUE="查询" TYPE=button onclick="CondQueryClick();"> 
		</td>  
     <tr>
     </table>
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
  	</div>
	<div id = "divBnfInfo" style = "display:'none'">
   <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPBnf);">
      </td>
      <td class= titleImg>
        受益人信息
      </td>
   </tr>
   </table>
	 
    <Div  id= "divLPBnf" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCBnfGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
  
 	<Div  id= "divSubmit" style= "display:''">
      <table class = common>
			<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common name="saveButton" type=Button value="保存申请" onclick="edorTypeBCSave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= common name="cancelButton" type=Button value="取消" onclick="edorTypeBCReturn()">
     	 </TD>
     	 </TR>
     	</table>
    </Div>
	</Div>
	  <Input type=Button name="returnButton" value="返回" onclick="returnParent()">
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
