<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ProposalSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	String tAction = "";
	String tOperate = "";

	LCPolSchema mLCPolSchema = new LCPolSchema(); 
	try	{
		ProposalUI tProposalUI  = new ProposalUI();
		
		GlobalInput tG = new GlobalInput();	
		tG = ( GlobalInput )session.getValue( "GI" );
	
		VData tVData = new VData();
		tAction = request.getParameter( "fmAction" );
    System.out.println("用户选择的操作为tAction:"+tAction);
	 
		if( tAction.equals( "DELETE" ))	{ 	//选择删除操作的处理			 
			LCPolSchema tLCPolSchema  = new LCPolSchema();
			tLCPolSchema.setProposalNo( request.getParameter( "ProposalNo" ));
		
			// 准备传输数据 VData
			tVData.addElement( tLCPolSchema );
			tVData.addElement( tG );
		}	
		else { 						//接收信息
            
			// 保单信息部分
			LCPolSchema tLCPolSchema = new LCPolSchema();
			LCDutySchema tLCDutySchema = new LCDutySchema();
		 
			tLCPolSchema.setProposalNo(request.getParameter("ProposalNo"));
			tLCPolSchema.setPrtNo(request.getParameter("PrtNo"));
			tLCPolSchema.setManageCom(request.getParameter("ManageCom"));
			tLCPolSchema.setSaleChnl(request.getParameter("SaleChnl"));
			tLCPolSchema.setAgentCom(request.getParameter("AgentCom"));
			tLCPolSchema.setAgentCode(request.getParameter("AgentCode"));
			tLCPolSchema.setAgentGroup(request.getParameter("AgentGroup"));
			tLCPolSchema.setHandler(request.getParameter("Handler"));
			tLCPolSchema.setAgentCode1(request.getParameter("AgentCode1"));
	
			tLCPolSchema.setContNo(request.getParameter("ContNo"));
			tLCPolSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
			tLCPolSchema.setMainPolNo(request.getParameter("MainPolNo"));
			tLCPolSchema.setFirstPayDate(request.getParameter("FirstPayDate"));
			tLCPolSchema.setLang(request.getParameter("Lang"));
			tLCPolSchema.setCurrency(request.getParameter("Currency"));
			tLCPolSchema.setDisputedFlag(request.getParameter("DisputedFlag"));
			tLCPolSchema.setAgentPayFlag(request.getParameter("AgentPayFlag"));
			tLCPolSchema.setAgentGetFlag(request.getParameter("AgentGetFlag"));
			tLCPolSchema.setRemark(request.getParameter("Remark"));
      System.out.println("end set schema 保单信息...");
	
			// 险种信息部分
			tLCPolSchema.setRiskCode(request.getParameter("RiskCode"));
			tLCPolSchema.setRiskVersion(request.getParameter("RiskVersion"));
			tLCPolSchema.setCValiDate(request.getParameter("CValiDate"));             //保单生效日期
			tLCPolSchema.setHealthCheckFlag(request.getParameter("HealthCheckFlag")); //是否体检件
			tLCPolSchema.setOutPayFlag(request.getParameter("OutPayFlag"));           //溢交处理方式
			tLCPolSchema.setPayLocation(request.getParameter("PayLocation"));         //收费方式   
			tLCPolSchema.setBankCode(request.getParameter("BankCode"));               //开户行
			tLCPolSchema.setBankAccNo(request.getParameter("BankAccNo"));             //银行帐号 
			tLCPolSchema.setAccName(request.getParameter("AccName"));             	//户名 
			tLCPolSchema.setLiveGetMode(request.getParameter("LiveGetMode"));         //生存保险金领取方式                                                
			                                                                          //领取期限，通过TRANSFERDATA传递
			                                                                          //领取方式，通过TRANSFERDATA传递
			tLCPolSchema.setBonusGetMode(request.getParameter("BonusGetMode"));       //红利领取方式
			tLCPolSchema.setMult(request.getParameter("Mult"));                       //份数
			tLCPolSchema.setPrem(request.getParameter("Prem"));                       //保费
			tLCPolSchema.setAmnt(request.getParameter("Amnt"));                       //保额
			tLCPolSchema.setRnewFlag(request.getParameter("RnewFlag"));               //保额
			tLCPolSchema.setSpecifyValiDate(request.getParameter("SpecifyValiDate")); //是否指定生效日期
	  
			//以下页面信息未整理
			tLCDutySchema.setPayIntv(request.getParameter("PayIntv"));                 //交费方式
			tLCDutySchema.setInsuYear(request.getParameter("InsuYear"));              //保险期间
			tLCDutySchema.setInsuYearFlag(request.getParameter("InsuYearFlag"));
			tLCDutySchema.setPayEndYear(request.getParameter("PayEndYear"));          //交费年期
			tLCDutySchema.setPayEndYearFlag(request.getParameter("PayEndYearFlag"));
			tLCDutySchema.setGetYear(request.getParameter("GetYear"));		          //年金开始领取年龄
			tLCDutySchema.setGetYearFlag(request.getParameter("GetYearFlag"));
			tLCDutySchema.setGetStartType(request.getParameter("GetStartType"));
      System.out.println("end set schema 险种信息...");
	
			// 投保人信息部分
			// 个人投保人
			LCAppntIndSchema tLCAppntIndSchema = new LCAppntIndSchema();
			
			tLCAppntIndSchema.setCustomerNo(request.getParameter("AppntCustomerNo"));  //客户号
			tLCAppntIndSchema.setName(request.getParameter("AppntName"));              //姓名 
			tLCAppntIndSchema.setSex(request.getParameter("AppntSex"));                //性别
			tLCAppntIndSchema.setBirthday(request.getParameter("AppntBirthday"));      //出生日期 
			                                                                         //年龄，不提交 
			tLCAppntIndSchema.setIDType(request.getParameter("AppntIDType"));          //证件类型
			tLCAppntIndSchema.setIDNo(request.getParameter("AppntIDNo"));              //证件号码 
			tLCAppntIndSchema.setNativePlace(request.getParameter("AppntNativePlace"));//国籍
			tLCAppntIndSchema.setRgtAddress(request.getParameter("AppntRgtAddress"));  //户口所在地
			tLCAppntIndSchema.setMarriage(request.getParameter("AppntMarriage"));      //婚姻状况
			tLCAppntIndSchema.setNationality(request.getParameter("AppntNationality"));//民族
			tLCAppntIndSchema.setDegree(request.getParameter("AppntDegree"));          //学历
			tLCAppntIndSchema.setRelationToInsured(request.getParameter("AppntRelationToInsured")); //与被保险人关系
			tLCAppntIndSchema.setPostalAddress(request.getParameter("AppntPostalAddress"));         //联系地址
			tLCAppntIndSchema.setZipCode(request.getParameter("AppntZipCode"));        //邮政编码
			tLCAppntIndSchema.setPhone(request.getParameter("AppntPhone"));            //家庭电话  
			tLCAppntIndSchema.setMobile(request.getParameter("AppntMobile"));          //移动电话
			tLCAppntIndSchema.setEMail(request.getParameter("AppntEMail"));            //电子邮箱
			tLCAppntIndSchema.setGrpName(request.getParameter("AppntGrpName"));        //工作单位
			tLCAppntIndSchema.setGrpPhone(request.getParameter("AppntGrpPhone"));      //单位电话
			tLCAppntIndSchema.setGrpAddress(request.getParameter("AppntGrpAddress"));  //单位地址
			tLCAppntIndSchema.setGrpZipCode(request.getParameter("AppntGrpZipCode"));  //单位邮政编码
			tLCAppntIndSchema.setWorkType(request.getParameter("AppntWorkType"));      //职业（工种）
			tLCAppntIndSchema.setPluralityType(request.getParameter("AppntPluralityType"));        //兼职（工种）
			tLCAppntIndSchema.setOccupationType(request.getParameter("AppntOccupationType"));      //职业类别
			tLCAppntIndSchema.setOccupationCode(request.getParameter("AppntOccupationCode"));      //职业代码
			tLCAppntIndSchema.setSmokeFlag(request.getParameter("AppntSmokeFlag"));    //是否吸烟
			
			// 集体投保人 
			LCAppntGrpSchema tLCAppntGrpSchema = new LCAppntGrpSchema();
	
	    tLCAppntGrpSchema.setGrpNo(request.getParameter("AppGrpNo"));
	    tLCAppntGrpSchema.setGrpName(request.getParameter("AppGrpName"));	    
	    tLCAppntGrpSchema.setGrpAddress(request.getParameter("AppGrpAddress"));
	    tLCAppntGrpSchema.setGrpZipCode(request.getParameter("AppGrpZipCode"));
	    tLCAppntGrpSchema.setGrpNature(request.getParameter("GrpNature"));
	    tLCAppntGrpSchema.setBusinessType(request.getParameter("BusinessType"));
	    tLCAppntGrpSchema.setPeoples(request.getParameter("Peoples"));
	    tLCAppntGrpSchema.setRgtMoney(request.getParameter("RgtMoney"));
	    tLCAppntGrpSchema.setAsset(request.getParameter("Asset"));
	    tLCAppntGrpSchema.setNetProfitRate(request.getParameter("NetProfitRate"));
	    tLCAppntGrpSchema.setMainBussiness(request.getParameter("MainBussiness"));
	    tLCAppntGrpSchema.setCorporation(request.getParameter("Corporation"));
	    tLCAppntGrpSchema.setComAera(request.getParameter("ComAera"));
	    //保险联系人一
	    tLCAppntGrpSchema.setLinkMan1(request.getParameter("LinkMan1"));
	    tLCAppntGrpSchema.setDepartment1(request.getParameter("Department1"));
	    tLCAppntGrpSchema.setHeadShip1(request.getParameter("HeadShip1"));
	    tLCAppntGrpSchema.setPhone1(request.getParameter("Phone1"));
	    tLCAppntGrpSchema.setE_Mail1(request.getParameter("E_Mail1"));
	    tLCAppntGrpSchema.setFax1(request.getParameter("Fax1"));
	    //保险联系人二
	    tLCAppntGrpSchema.setLinkMan2(request.getParameter("LinkMan2"));
	    tLCAppntGrpSchema.setDepartment2(request.getParameter("Department2"));
	    tLCAppntGrpSchema.setHeadShip2(request.getParameter("HeadShip2"));
	    tLCAppntGrpSchema.setPhone2(request.getParameter("Phone2"));
	    tLCAppntGrpSchema.setE_Mail2(request.getParameter("E_Mail2"));
	    tLCAppntGrpSchema.setFax2(request.getParameter("Fax2"));
	    tLCAppntGrpSchema.setGetFlag(request.getParameter("GetFlag"));
	    tLCAppntGrpSchema.setBankCode(request.getParameter("GrpBankCode"));
	    tLCAppntGrpSchema.setBankAccNo(request.getParameter("GrpBankAccNo"));
      System.out.println("end set schema 投保人信息...");
		
			// 被保人信息
			LCInsuredSet tLCInsuredSet = new LCInsuredSet();
			
			// 主被保人
			LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
			tLCInsuredSchema.setInsuredGrade("M"); 
			
			tLCInsuredSchema.setCustomerNo(request.getParameter("CustomerNo"));  //客户号
			tLCInsuredSchema.setName(request.getParameter("Name"));              //姓名 
			tLCInsuredSchema.setSex(request.getParameter("Sex"));                //性别
			tLCInsuredSchema.setBirthday(request.getParameter("Birthday"));      //出生日期 
			                                                                   //年龄，不提交 
			tLCInsuredSchema.setIDType(request.getParameter("IDType"));          //证件类型
			tLCInsuredSchema.setIDNo(request.getParameter("IDNo"));              //证件号码 
			tLCInsuredSchema.setNativePlace(request.getParameter("NativePlace"));//国籍
			tLCInsuredSchema.setRgtAddress(request.getParameter("RgtAddress"));  //户口所在地
			tLCInsuredSchema.setMarriage(request.getParameter("Marriage"));      //婚姻状况
			tLCInsuredSchema.setNationality(request.getParameter("Nationality"));//民族
			tLCInsuredSchema.setDegree(request.getParameter("Degree"));          //学历
			tLCInsuredSchema.setSmokeFlag(request.getParameter("SmokeFlag"));    //是否吸烟
			tLCInsuredSchema.setPostalAddress(request.getParameter("PostalAddress"));         //联系地址
			tLCInsuredSchema.setZipCode(request.getParameter("ZipCode"));        //邮政编码
			tLCInsuredSchema.setPhone(request.getParameter("Phone"));            //家庭电话  
			tLCInsuredSchema.setMobile(request.getParameter("Mobile"));          //移动电话
			tLCInsuredSchema.setEMail(request.getParameter("EMail"));            //电子邮箱
			tLCInsuredSchema.setGrpName(request.getParameter("GrpName"));        //工作单位
			tLCInsuredSchema.setGrpPhone(request.getParameter("GrpPhone"));      //单位电话
			tLCInsuredSchema.setGrpAddress(request.getParameter("GrpAddress"));  //单位地址
			tLCInsuredSchema.setGrpZipCode(request.getParameter("GrpZipCode"));  //单位邮政编码
			tLCInsuredSchema.setWorkType(request.getParameter("WorkType"));      //职业（工种）
			tLCInsuredSchema.setPluralityType(request.getParameter("PluralityType"));         //兼职（工种）
			tLCInsuredSchema.setOccupationType(request.getParameter("OccupationType"));       //职业类别
			tLCInsuredSchema.setOccupationCode(request.getParameter("OccupationCode"));       //职业代码
			
			//以下页面信息未整理
			tLCInsuredSchema.setHealth(request.getParameter("Health"));
	
			tLCInsuredSet.add(tLCInsuredSchema);
      System.out.println("end set schema 被保人信息...");
			    
			// 连带被保险人(未整理)
			String tInsuredNum[] = request.getParameterValues("SubInsuredGridNo");
			String tInsuredCustomerNo[] = request.getParameterValues("SubInsuredGrid1");
			String tInsuredName[] = request.getParameterValues("SubInsuredGrid2");
			String tInsuredSex[] = request.getParameterValues("SubInsuredGrid3");
			String tInsuredBirthday[] = request.getParameterValues("SubInsuredGrid4");
			String tRelationToInsured[] = request.getParameterValues("SubInsuredGrid5");
			
			int InsuredCount = 0;
			if (tInsuredNum != null) InsuredCount = tInsuredNum.length;
			
			for (int i = 0; i < InsuredCount; i++) {
				if(tInsuredCustomerNo[i]==null || tInsuredCustomerNo[i].equals("")) break;
				
				tLCInsuredSchema = new LCInsuredSchema();
				tLCInsuredSchema.setInsuredGrade("S");
				
				tLCInsuredSchema.setCustomerNo(tInsuredCustomerNo[i]);
				tLCInsuredSchema.setName(tInsuredName[i]);
				tLCInsuredSchema.setSex(tInsuredSex[i]);
				tLCInsuredSchema.setBirthday(tInsuredBirthday[i]);
				tLCInsuredSchema.setRelationToInsured(tRelationToInsured[i]);
				
				tLCInsuredSet.add(tLCInsuredSchema);
			}
	
			// 受益人信息
			LCBnfSet tLCBnfSet = new LCBnfSet();
			
			String tBnfNum[]   = request.getParameterValues("BnfGridNo");
			
			String tBnfType[]  = request.getParameterValues("BnfGrid1");
			String tName[]     = request.getParameterValues("BnfGrid2");
			String tIDType[]   = request.getParameterValues("BnfGrid3");
			String tIDNo[]     = request.getParameterValues("BnfGrid4");
			String tBnfRelationToInsured[] = request.getParameterValues("BnfGrid5");
			String tBnfLot[]   = request.getParameterValues("BnfGrid6");
			String tBnfGrade[] = request.getParameterValues("BnfGrid7");
			String tAddress[]  = request.getParameterValues("BnfGrid8");
			
			int BnfCount = 0;
			if (tBnfNum != null) BnfCount = tBnfNum.length;
	  
			for (int i = 0; i < BnfCount; i++) {
				if( tName[i] == null || tName[i].equals( "" )) break;
							
				LCBnfSchema tLCBnfSchema = new LCBnfSchema();
				
				tLCBnfSchema.setBnfType(tBnfType[i]);
				tLCBnfSchema.setName(tName[i]);
				//tLCBnfSchema.setSex(tSex[i]);
				tLCBnfSchema.setIDType(tIDType[i]);
				tLCBnfSchema.setIDNo(tIDNo[i]);
				//tLCBnfSchema.setBirthday(tBirthday[i]);
				tLCBnfSchema.setRelationToInsured(tBnfRelationToInsured[i]);
				tLCBnfSchema.setBnfLot(tBnfLot[i]);
				tLCBnfSchema.setBnfGrade(tBnfGrade[i]);
				tLCBnfSchema.setAddress(tAddress[i]);
				//tLCBnfSchema.setZipCode(tZipCode[i]);
				//tLCBnfSchema.setPhone(tPhone[i]);
				tLCBnfSet.add(tLCBnfSchema);
			}	
      System.out.println("end set schema 受益人信息...");
		
			// 告知信息
			LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
			
			String tImpartNum[] = request.getParameterValues("ImpartGridNo");
			String tImpartVer[] = request.getParameterValues("ImpartGrid1");            //告知版别
			String tImpartCode[] = request.getParameterValues("ImpartGrid2");           //告知编码
			String tImpartContent[] = request.getParameterValues("ImpartGrid4");        //填写内容
			String tImpartCustomerNoType[] = request.getParameterValues("ImpartGrid5"); //告知客户类型
			
			int ImpartCount = 0;
			if (tImpartNum != null) ImpartCount = tImpartNum.length;
	
			for (int i = 0; i < ImpartCount; i++)	{
				if( !(tImpartCustomerNoType[i].trim().equals( "A" ) || tImpartCustomerNoType[i].trim().equals( "I" ))) 
					break;
	    
				LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
				
				if( tImpartCustomerNoType[i].trim().equals( "A" ))
					tLCCustomerImpartSchema.setCustomerNo( request.getParameter( "AppntCustomerNo" ));
				if( tImpartCustomerNoType[i].trim().equals( "I" ))
					tLCCustomerImpartSchema.setCustomerNo( request.getParameter( "CustomerNo" ));
				
				tLCCustomerImpartSchema.setCustomerNoType( tImpartCustomerNoType[i] );
				tLCCustomerImpartSchema.setImpartCode(tImpartCode[i]);
				tLCCustomerImpartSchema.setImpartContent(tImpartContent[i]);
				tLCCustomerImpartSchema.setImpartVer(tImpartVer[i]) ;
				tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
			}
      System.out.println("end set schema 告知信息...");
		
			// 特别约定
			LCSpecSet tLCSpecSet = new LCSpecSet();
			
			String tSpecNum[] = request.getParameterValues("SpecGridNo");
			String tSpecType[] = request.getParameterValues("SpecGrid1");
			String tSpecCode[] = request.getParameterValues("SpecGrid2");
			String tSpecContent[] = request.getParameterValues("SpecGrid3");
			
			int SpecCount = 0;
			if (tSpecNum != null) SpecCount = tSpecNum.length;
			for (int i = 0; i < SpecCount; i++)	{
				if(tSpecCode[i]==null || tSpecCode[i].equals("")) break;
				
				LCSpecSchema tLCSpecSchema = new LCSpecSchema();
				
				tLCSpecSchema.setSpecCode(tSpecCode[i]);
				tLCSpecSchema.setSpecType(tSpecType[i]);
				tLCSpecSchema.setSpecContent(tSpecContent[i]);
				
				tLCSpecSet.add(tLCSpecSchema);
			}
      System.out.println("end set schema 特约信息...");
	  
			//是否有责任项
			String tNeedDutyGrid=request.getParameter("inpNeedDutyGrid");
			LCDutySet tLCDutySet=new LCDutySet();
			LCDutySchema tLCDutySchema1=new LCDutySchema();
			
			if (tNeedDutyGrid.equals("1")) { 	   //有责任项 
				tLCDutySchema1 = new LCDutySchema();
				tLCDutySet=new LCDutySet();
				
				String tDutyCode[] =request.getParameterValues("DutyGrid1");
				String tDutyPrem1[] =request.getParameterValues("DutyGrid3");
				String tDutyGet1[] =request.getParameterValues("DutyGrid4");
				String tDutyPayEndYear[] =request.getParameterValues("DutyGrid5");
				String tDutyGetYear[] =request.getParameterValues("DutyGrid6");
				String tDutyChk[] = request.getParameterValues("InpDutyGridChk");
				
				int DutyCount = 0;
				if (tDutyCode != null) DutyCount = tDutyCode.length;
				for (int i = 0; i < DutyCount; i++)	{
					if(tDutyCode[i]==null || tDutyCode[i].equals("")) break;
					
					tLCDutySchema1= new LCDutySchema();
					
					if(!tDutyCode[i].equals("") && tDutyChk[i].equals("1"))	{
						tLCDutySchema1.setDutyCode(tDutyCode[i]);
						tLCDutySchema1.setPrem(tDutyPrem1[i]);
						tLCDutySchema1.setAmnt(tDutyGet1[i]);
						tLCDutySchema1.setPayEndYear(tDutyPayEndYear[i]);
						tLCDutySchema1.setGetYear(tDutyGetYear[i]);
						tLCDutySet.add(tLCDutySchema1);
					} // end of if
				} // end of for
			} // end of if
      System.out.println("end set schema 责任信息...");
	
			//传递非SCHEMA信息
			TransferData tTransferData = new TransferData();
	
			tTransferData.setNameAndValue("getIntv", request.getParameter("getIntv"));                //领取间隔（方式）
			tTransferData.setNameAndValue("GetDutyKind", request.getParameter("GetDutyKind"));        //年金开始领取年龄
	
			if (request.getParameter("SamePersonFlag") == null)
				tTransferData.setNameAndValue("samePersonFlag", "0"); //投保人同被保人标志
			else
				tTransferData.setNameAndValue("samePersonFlag", "1"); //投保人同被保人标志
      System.out.println("\ntTransferData:");
	  
			// 准备传输数据 VData
			tVData.addElement(tLCPolSchema);
			tVData.addElement(tLCAppntIndSchema);
			tVData.addElement(tLCAppntGrpSchema);
			tVData.addElement(tLCInsuredSet);
			tVData.addElement(tLCBnfSet);
			tVData.addElement(tLCCustomerImpartSet);
			tVData.addElement(tLCSpecSet);
			tVData.addElement(tG);
			tVData.addElement(tTransferData);
	  
			if (tNeedDutyGrid.equals("1"))
				tVData.addElement(tLCDutySet);
			else
				tVData.addElement(tLCDutySchema);
		} // end of if
	  
		if( tAction.equals( "INSERTPERSON" )) tOperate = "INSERT||PROPOSAL";
		if( tAction.equals( "INSERTGROUP" )) tOperate = "INSERT||PROPOSAL";
		if( tAction.equals( "UPDATEPERSON" )) tOperate = "UPDATE||PROPOSAL";
		if( tAction.equals( "UPDATEGROUP" )) tOperate = "UPDATE||PROPOSAL";
		if( tAction.equals( "DELETE" )) tOperate = "DELETE||PROPOSAL";
	
		// 数据传输
		if( !tProposalUI.submitData( tVData, tOperate )) {
			Content = " 保存失败，原因是: " + tProposalUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		else {
			Content = "*************Proposal 保存成功!*************";
			FlagStr = "Succ";
	  
			tVData.clear();
			tVData = tProposalUI.getResult();
	  
			// 显示
			// 保单信息
			mLCPolSchema.setSchema(( LCPolSchema )tVData.getObjectByObjectName( "LCPolSchema", 0 ));
		}	
	} // end of try
	catch( Exception e1 )	{
		Content = " 保存失败，原因是: " + e1.toString().trim();
		FlagStr = "Fail";
	}

	if( FlagStr.equals( "Succ" ))	{
%>
    	<script language="javascript">
    	try 
    	{
    	 	parent.fraInterface.fm.all("ContNo").value = "<%=mLCPolSchema.getContNo()%>";
    	 	parent.fraInterface.fm.all("GrpPolNo").value = "<%=mLCPolSchema.getGrpPolNo()%>";
    	 	parent.fraInterface.fm.all("ProposalNo").value = "<%=mLCPolSchema.getProposalNo()%>";
    	 	parent.fraInterface.fm.all("Prem").value = "<%=mLCPolSchema.getPrem()%>";
    	 	parent.fraInterface.fm.all("Amnt").value = "<%=mLCPolSchema.getAmnt()%>";
    	} catch(ex) { 
			//alert("after Save but happen err:" + ex);
    	}
    	</script>

<%
	} // end of if
Content.replace('"', ' ');
System.out.println(Content);
System.out.println(FlagStr);
%>    
                  
<html>
<script language="javascript">
	try 
	{
		parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
	}	catch(ex) { 
		//alert("after Save but happen err:" + ex);
	}
</script>
<%
	if(( tAction.equals( "INSERTPERSON" ) || tAction.equals( "UPDATEPERSON" )) && FlagStr.equals( "Succ" ))
	{
%>
<script language="javascript">
	try {
	  var prtNo = "<%=mLCPolSchema.getPrtNo()%>";
	  if (prtNo.substring(2, 4) == "11") {
	    parent.fraPic.goToPic(3); top.fraPic.scrollTo(0, 0);
	  }
	  else if (prtNo.substring(2, 4) == "15") {
	    parent.fraPic.goToPic(0); top.fraPic.scrollTo(300, 2000);
	  }
	  parent.fraInterface.showFee();
	} 
	catch(ex) {}
</script>
<%
    } // end of if
%>
</html>

