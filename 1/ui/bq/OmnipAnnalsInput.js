//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass();
turnPage.pageLineNum = 20;       //根据需求，显示20行
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var theFirstValue="";
var theSecondValue="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示


function EasyExecSql()
{
	if( verifyInput2() == false ){
         return false;
    }
	
	var strSQL = "select b.ManageCom,    b.ContNo, b.AppntName,(select insuredname from lcpol where polno=c.polno), signdate,polmonth"
             + " ,nvl((select sum(money) From lcinsureacctrace where polno = c.PolNo and paydate <= c.DueBalaDate),0)"
             + " ,e.Name, getUniteCode(e.AgentCode)"
             + " ,(select Name from LABranchGroup where AgentGroup = b.AgentGroup)"
             + " ,(select min(Mobile) from LCAppnt x, LCAddress y where x.AppntNo = y.CustomerNo and x.AddressNo = y.AddressNo and x.ContNo = b.ContNo) "
             + " ,(select CodeAlias from LDCode1 where CodeType = 'salechnl' and Code=b.SaleChnl) "
             + " ,c.polno "
             + " from LCCont b, LCInsureAccBalance c, LAAgent e "
             + "where 1 = 1 "
             + getWherePart('c.RunDate', 'RunDateStart','>=')
             + getWherePart('c.RunDate', 'RunDateEnd','<=')
             + getWherePart('b.SaleChnl', 'SaleChnl','=')
             
             + getWherePart('b.ContNo', 'ContNo')
             + "   and b.ContNo = c.ContNo "
             + "   and b.AgentCode = e.AgentCode and b.ManageCom like '"+fm.all('ManageCom').value+"%'"
             + "   and year(DueBalaDate - 1 days) = " + fm.PolYear.value
             + "   and polmonth = " + fm.PolMonth.value
             + "   and c.BalaCount > 0 "
             + "   and exists (select 1 from lmriskapp l,lcpol p where p.polno=c.polno and  l.riskcode=p.riskcode and l.risktype4='4' and (TaxOptimal is null or TaxOptimal<>'Y') union select 1 from lmriskapp l, lbpol pb where pb.polno = c.polno and l.riskcode = pb.riskcode and l.risktype4 = '4' and (TaxOptimal is null or TaxOptimal<>'Y')) "
             + " group by  b.ManageCom, b.ContNo, b.AppntName, signdate, c.PolMonth, c.InsuAccBalaAfter,c.SequenceNo,c.DueBalaDate, e.Name, e.AgentCode,b.AgentGroup,b.SaleChnl,c.polno  "
             + " fetch first 2000 rows only with ur "
             ;
  fm.tempSQL.value =strSQL;
  turnPage.queryModal(strSQL, GetModeGrid);  
  if(GetModeGrid.mulLineCount == 0)
	{
	    alert("没有查询到保单信息！");
	}     
}

function queryAll()
{
	initGrpContGrid();
	var sql = "select grpcontno,contno,insuredname,cvalidate,cinvalidate from lccont where grpcontno ='"+fm.tGrpContNo.value+"' with ur ";
	turnPage2.queryModal(sql, GrpContGrid);
	return false ;
}

function save()
{
	if (GrpContGrid.mulLineCount ==0)
	{
		alert("没有保单明细信息，不能修改！");
		return false;
	}	
	
	if (!CheckData(fm.tCinvaliDate.value))
	{
		return false ;
	}
	
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

/**
*  日期格式校验，防止录入错误格式日期。
*/


function CheckData(cform){
	
 if (fm.tCinvaliDate.value=="" || fm.tCinvaliDate.value==null)
 {
    alert("截止日期不能为空，请输入！");
 		return false;
 } 
 if (!formatTime(fm.tCinvaliDate.value))
 {
    alert("截至日期格式错误！");
    return false;
 } 
   return true;
}

function formatTime(str)
{
  var   r   =   str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);     
  if(r==null) return   false;     
  var  d=  new  Date(r[1],   r[3]-1,   r[4]);     
  return  (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);   
}

// 清单打印
function PrintList()
{
    if(!printAlert())
	{
	  return false;
	}
	if(GetModeGrid.mulLineCount==0)
	{
		alert("没有查询列表，不可以打印清单！");
		return false;
	}
//	window.open("../bq/OmnipAnnalsPrtList.jsp?tempSQL="+fm.tempSQL.value);
    fm.action = "../bq/OmnipAnnalsPrtList.jsp";
    fm.submit();
}


function printOneNoticeNew()
{
	if(!printAlert())
	{
	  return false;
	}
	if (GetModeGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GetModeGrid.mulLineCount; i++)
	{
		if(GetModeGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印");
		return false;	
	}

	var tContNo=GetModeGrid.getRowColData(tChked[0],2);
	//20161107含有多个万能账户的调整
	var tPolNo=GetModeGrid.getRowColData(tChked[0],13);
	//var tGetNoticeNo=GetModeGrid.getRowColData(tChked[0],16);
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=OM001&OtherNo="+tPolNo+"&StandbyFlag3="+fm.PolYear.value+"&StandbyFlag1="+fm.PolMonth.value
					 +"&StandbyFlag2="+fm.RunDateStart.value+"&StandbyFlag4="+fm.RunDateEnd.value	;
	fm.submit();
}

//批量打印
function printAllNotice()
{
	if(!printAlert())
	{
	  return false;
	}
	if (GetModeGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GetModeGrid.mulLineCount; i++)
	{	
		if(GetModeGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}


	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.target = "fraSubmit";
		fm.action="../bq/OmnipAnnalsAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.target = "fraSubmit";
		fm.action="../bq/OmnipAnnalsInsForBatPrt.jsp";
		fm.submit();
	}

}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

//add by fuxin  2009-10-9 只有总公司才能打印“万能年报”--IT负责人：丁曙光
//modify by zhanggm 20091203 只有登录机构为总公司的才能打印，但是可以按页面录入机构查询并打印。
function printAlert()
{
//  if("86" != comCode && "86000000" != comCode )
//  {
//    alert ("对不起，您所在的机构是“"+comCode+"”，如需打印请联系总公司！");
//    return false ;	
//  }
  return true;
}

//去空格
function trim(s)
{
  var l = s.replace( /^\s*/, ""); //去左空格
  var r = l.replace( /\l*$/, ""); //去右空格
  return r;
}