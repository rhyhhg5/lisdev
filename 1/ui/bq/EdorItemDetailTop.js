//程序名称：EdorItemDetailTop.js
//程序功能：工单管理查看顶部操作按钮
//创建日期：2005-09-28
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

var viewScanFlag = false;

function initForm()
{
    
}

/* 查看扫描 */
function viewScan()
{
	if(viewScanFlag == false)
	{
		parent.fraSet.rows = "20,100,300,*,0";
		viewScanFlag = true;
	}
	else
	{
		parent.fraSet.rows = "20,0,0,*,0";
		viewScanFlag = false;
	}
}
