
<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>

<html>
<%
//程序名称：PContPauseInput.jsp
//程序功能：保单失效清单
//创建日期：2010-3-31
//创建人  ：WangHongLiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="PEdorYBTQueryWTInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="PEdorYBTQueryWTInit.jsp"%>
	</head>

	<body  onload="initForm();" >
  	<form action="./PEdorYBTPrintWT.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
    		
    	<Div  id= "divLLReport1" style= "display: ''">    	
      	<Table  class= common>
	    	<tr>
				<td class= title>申请日期起期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=StartDate elementtype=nacessary verify="失效日期起期|NOTNULL">
				</td class=input>
				<td class= title>申请日期止期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=EndDate elementtype=nacessary verify="失效日期止期|NOTNULL">
				</td>	    	
	    		<td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName elementtype=nacessary>
				</td>	    		
			</tr>
	    	<tr>
	    		<td class= title>保单号</td>
				<td class=input>
					<input class=common name=ContNo>
				</td>
				<td class= title>当前申请状态</td>
				<td class=input>				
					<Input class= "codeno" name=nowState CodeData="0|^0|请选择类型^1|已提交申请^2|工单已结案^3|全部状态" ondblclick="return showCodeListEx('nowState',[this,nowStateName],[0,1]);" onkeyup="return showCodeListKeyEx('nowState',[this,nowStateName],[0,1]);" ><Input class=codename name=nowStateName elementtype=nacessary  readonly  >
				</td>
				<td class= title>保全项目</td>
				<td class=input>				
					<Input class= "codeno" name=edorType value='0' CodeData="0|^0|全部^WT|犹豫期退保^CT|解约" ondblclick="return showCodeListEx('edorType',[this,edorTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('edorType',[this,edorTypeName],[0,1]);" ><Input class=codename name=edorTypeName elementtype=nacessary  readonly  >
				</td>				
	    	</tr>
		</Table>
      <table  class= common>
	  <input class=cssButton type=button value="查  询" onclick="easyQuery();">  
		<input class=cssButton type=button value="下  载" onclick="exportList();">
<%--	  <input class=cssButton type=button value="导出结果集" onclick="exportList();">  --%>
	  </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 申请清单
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanContPauseGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		  <Div  id= "divPage2" align=center style= "display: '' ">     
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
		 </Div>
  	</Div>
		
		

		 
		<input type=hidden id="sql" name="sql" value="">

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </form>
</body>
</html>
