<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK"> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeLP.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeLPInit.jsp"%>
  <title>客户基本资料变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeLPSubmit.jsp" method=post name=fm target="fraSubmit"> 
  <table class=common>
    <TR class= common> 
      <TD class= title > 受理号 </TD>
      <TD class= input>
        <input class="readonly" type="text" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" type="hidden" readonly name=EdorType>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 合同保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE>
 <Div  id= "divLPInsuredDetail" style= "display:''">
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      </td>
      <td class= titleImg>
        客户详细信息
      </td>
   </tr>
   </table>
  	<Div  id= "divDetail" style= "">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= readonly readonly name=Name verify="姓名|notnull&len<=60"  elementtype=nacessary >
            <Input class= hidden type=hidden readonly name=NameBak >
          </TD>
				  <TD  class= title>
						性别
				  </TD>
				  <TD  class= input>
					 	<Input class="codeNo" readonly  type=hidden name=Sex verify="性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName], [0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName], [0,1]);"><Input class="readonly" name=SexName elementtype=nacessary readonly >
		        <Input class= hidden type=hidden readonly name=SexBak >
				  </TD>
				</TR>
      	<TR  class= common>
				  <TD  class= title>
				   	出生日期
				  </TD>
				  <TD  class= input>
						<Input class= readonly readonly name=Birthday elementtype=nacessary verify="出生日期|notnull&data" >
		        <Input class= hidden type=hidden readonly name=BirthdayBak >
				  </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class= codeNo readonly  type=hidden name="IDType"   ondblclick="return showCodeList('IDType',[this,IDTypeName], [0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName], [0,1]);"><Input class= readonly name="IDTypeName" elementtype=nacessary readonly >
            <Input class= hidden type=hidden readonly name=IDTypeBak >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= readonly readonly name=IDNo elementtype=nacessary verify="证件号码|len<20">
            <Input class= hidden type=hidden readonly name=IDNoBak >
         </TD>
        </TR>       
       </table>
      </Div>

	<DIV id=DivClaimHead STYLE="display:''">   
	<table>
			<tr>
				<td>
				<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivClaim);">
				</td>
				<td class="titleImg">理赔金帐户
				</td>
			</tr>
	</table>
	</Div>      

<DIV id=DivClaim STYLE="display:''"> 
	<table  class= common>
		<TR CLASS=common>
			<TD CLASS=title>开户银行</TD>
			<TD CLASS=input>
				<Input NAME=BankCode VALUE="" CLASS="codeNo" MAXLENGTH=20 ondblclick="return showCodeList('bank',[this,BankCodeName], [0,1], null, null, null, 1);" onkeyup="return showCodeListKey('bank',[this,BankCodeName], [0,1], null, null, null, 1);" ><Input NAME=BankCodeName VALUE="" CLASS="codeName" readonly >
				<input name="BankCodeBak" type=hidden>
			</TD>
			<TD CLASS=title >户名</TD>
			<TD CLASS=input>
				<Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20 verify="户名|len<=20" >
				<input name="AccNameBak" type=hidden>
			</TD>
			<TD CLASS=title width="109" >账号</TD>
			<TD CLASS=input>
				<Input NAME=BankAccNo VALUE="" CLASS=common verify="银行帐号|len<=40">
				<input name="BankAccNoBak" type=hidden>
			</TD>
		</TR>   
		<TR CLASS=common>
				<TD CLASS=title>开户银行</TD>
				<TD CLASS=input>
					<Input NAME=BankCode1 VALUE="" CLASS="codeNo" MAXLENGTH=20 ondblclick="return showCodeList('bank',[this,BankCodeName1], [0,1], null, null, null, 1);" onkeyup="return showCodeListKey('bank',[this,BankCodeName1], [0,1], null, null, null, 1);" ><Input NAME=BankCodeName1 VALUE="" CLASS="codeName" readonly >
				</TD>
				<TD CLASS=title >户名</TD>
				<TD CLASS=input>
					<Input NAME=AccName1 VALUE="" CLASS=common MAXLENGTH=20 verify="户名|len<=20" >
				</TD>
				<TD CLASS=title width="109" >账号</TD>
				<TD CLASS=input>
					<Input NAME=BankAccNo1 VALUE="" CLASS=common verify="银行帐号|len<=40">
				</TD>
		 </TR> 
		 <TR><TD><input type=button value="银行查询" class=cssButton onclick="querybank();"></TD></TR>   
	</Table> 	          
</DIV> 
 
	</Div>
	  <br>
	  <hr>
		<Input class= cssButton type=Button value="保  存" onclick="edorTypeLPSave()">
		<Input class= cssButton type=Button value="取  消" onclick="initForm()">
		<Input type=Button value="返  回" class = cssButton onclick="returnParent()">
		<input type=hidden id="TypeFlag" name="TypeFlag">
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="ContType" name="ContType">
		<input type=hidden name="EdorAcceptNo">
		<input type=hidden name="GrpContNo">
		<input type=hidden name="SerialNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>