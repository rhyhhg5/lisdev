<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QuestInputInit.jsp
//程序功能：问题件录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
<%
  String tContNo = "";
  String tFlag = "";
  String tAppFlag = "";

  tContNo = request.getParameter("ContNo");
  tFlag = request.getParameter("Flag");
  tAppFlag = request.getParameter("AppFlag");

  System.out.println("ContNo:"+tContNo);
  System.out.println("Flag:"+tFlag);
  System.out.println("tAppFlag:"+tAppFlag);

%>                            

<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
try
  {                            
  	fm.EdorNo.value = "<%=request.getParameter("EdorNo")%>";
    fm.ContNo.value = "<%=request.getParameter("ContNo")%>";       
    fm.all('BackObj').value = '';
    fm.all('Quest').value = '';
  }
  catch(ex)
  {
    alert("在UWManuDateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }   
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在UWSubInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm(tContNo,tFlag)
{
	var cAppFlag=<%=tAppFlag%>
  try
  {
	initInpBox();

	//initQuestGrid();

	initHide(tContNo,tFlag);
	initIssueGrid();
	easyQueryClick();
	//QueryCont(tContNo,tFlag);
	initCodeDate(tContNo,tFlag);
	hiddenGroup();
	/*
	if(cAppFlag==2){
	var arr =easyExecSql("select insuredname,insuredno from lcpol where contno='"+tContNo+"'");
  	if(arr)
  	{
  		fm.QuestionObj.value = arr[0][0];
  		fm.all('QuestionObj').readOnly=true;
  		fm.all('QuestionObj').ondblclick="";
  		fm.all('QuestionObj').onkeyup="";
  		fm.all('QuestionObj').onfocus="";
  		fm.QuestionObjNo.value = arr[0][1];
  	}
  }
  */
}
  catch(re)
  {
    alert("UWSubInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 责任信息列表的初始化
function initQuestGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="问题代码";    	//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="问题名称";         			//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="问题内容";         			//列名
      iArray[3][1]="300px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
                           

      QuestGrid = new MulLineEnter( "fm" , "QuestGrid" ); 
      //这些属性必须在loadMulLine前                            
      QuestGrid.mulLineCount = 1;
      QuestGrid.canSel = 1;
      QuestGrid.displayTitle = 1;
      QuestGrid.canChk = 1;
      QuestGrid.loadMulLine(iArray);  
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initHide(tContNo,tFlag)
{
	fm.all('ContNo').value=tContNo;
	fm.all('Flag').value=tFlag;
	//alert("pol:"+tContNo);
}

function initCodeDate(tContNo,tFlag)
{
	if (tFlag == "0")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|业务员^3|保户^4|机构";
		//fm.all('BackObj').value = "1";
	}
else	if (tFlag == "4")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|业务员^3|保户^4|机构";
		//fm.all('BackObj').value = "1";
	}
else	if (tFlag == "5")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|业务员^3|保户^4|机构";
		//fm.all('BackObj').value = "1";
	}
else	if (tFlag == "1")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|业务员^3|保户^4|机构";
	}
else	if (tFlag == "23")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|业务员^3|保户^4|机构";
	}
else	if (tFlag == "25")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|业务员^3|保户^4|机构";
	}
else
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|业务员^3|保户^4|机构";
	}	
}

function initIssueGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="问题对象";    	//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="问题字段";         			//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="原填写内容";         			//列名
      iArray[3][1]="200px";            		//列宽
      iArray[3][2]=150;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="问题件内容";         			//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=150;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许                           

      iArray[5]=new Array();
      iArray[5][0]="问题件回复";         			//列名
      iArray[5][1]="200px";            		//列宽
      iArray[5][2]=150;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="流水号";         			//列名
      iArray[6][1]="200px";            		//列宽
      iArray[6][2]=150;            			//列最大值
      iArray[6][3]=3 ;              			//是否允许输入,1表示允许，0表示不允许
      
      IssueGrid = new MulLineEnter( "fm" , "IssueGrid" ); 
      //这些属性必须在loadMulLine前                            
      IssueGrid.mulLineCount = 0;
      IssueGrid.displayTitle = 1;
      IssueGrid.canChk = 0;
      IssueGrid.canSel = 1;
      IssueGrid.hiddenPlus=1;
      IssueGrid.hiddenSubtraction=1;
      IssueGrid.loadMulLine(iArray);  
      
      }
      catch(ex)
      {
        alert(ex);
      }
}
function hiddenGroup()
{
//	if(fm.all('Flag').value==2)
//	{
//		Obj1.style.display='none';
//		Obj2.style.display='none';
//	}
}
</script>


