<html> 
<% 
//程序名称：
//程序功能：退保试算 
//创建日期：2005-08-02 16:49:22
//创建人  ：zhangtao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
  <%
     %>    

<head >
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  
  <SCRIPT src="./PEdorTypeCTTest.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeCTTestInit.jsp"%>
  
  
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeCTTestSubmit.jsp" method=post name=fm target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD class = title > 保单号 </TD>
      <td class= input><Input class="common" name=ContNo></td>
	  <TD  class= title> 退保日期 </TD>
	  <TD  class= input><Input class= "coolDatePicker" dateFormat="short" name=EdorItemAppDate ></TD>
	  <td class=title>  </td>
	  <td class= input> </td>
    </TR>
  </TABLE> 
  
  <BR>
  
   <INPUT class=cssButton VALUE="退保试算"  TYPE=button onclick="DisplayContInfo();">
   <INPUT class=cssButton VALUE="保单查询"  TYPE=button onclick="contQuery();">
   
 <Div  id= "divZTTest" style= "display: 'none'"> 
 
        <table>
            <tr>
                <td>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContInfo);">
                </td>
                <td class= titleImg>
                    保单详细信息
                </td>
            </tr>
        </table>
        <Div  id= "divContInfo" style= "display: ''">
          <table  class= common>            
	        <TR class = common>
				<TD  class= title > 保单生效日期 </TD>
				<TD  class= input ><input class="readonly" readonly name=CvaliDate ></TD>
				<TD  class= title > 客户签收日期 </TD>
				<TD  class= input ><input class="readonly" readonly name=CustomGetPolDate ></TD>
				<TD  class= title > 交费对应日 </TD>
				<TD  class= input ><input class="readonly" readonly name=PayToDate ></TD>
	        </TR>
	      	<TR class = common >
				<TD  class= title > 已满保单年度 </TD>
				<TD  class= input ><input class="readonly" readonly name=Inteval ></TD>
	            <TD  class= title > </TD>
	            <TD  class= title > </TD>
	            <TD  class= title > </TD>
	            <TD  class= title > </TD>
	        </TR>
            
          </table>  
        </Div>
 
	<table>
		<tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerGrid);">
	      </td>
	      <td class= titleImg> 客户基本信息 </td>
		</tr>
   </table> 
  <Div  id= "divCustomerGrid" style= "display: ''">
        <table  class= common>
        	<tr  class= common>
          		<td text-align: left colSpan=1>
        			<span id="spanCustomerGrid" >
        			</span> 
        	  	</td>
        	</tr>
        </table>					
    </Div>
        <table>
            <tr>
                <td>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
                </td>
                <td class= titleImg>
                    保单险种信息
                </td>
            </tr>
        </table>
    <Div  id= "divLCGrpPol" style= "display: ''">
        <table  class= common>
        	<tr  class= common>
          		<td text-align: left colSpan=1>
        			<span id="spanPolGrid" >
        			</span> 
        	  	</td>
        	</tr>
        </table>	
			
    </DIV>
	
	<BR>
	
     <Input  class= cssButton type=Button value="计  算" onclick="edorTypeCTSave()">
 </DIV>	
 
	<BR>
		
 <Div  id= "divZTDetail" style= "display: 'none'">	
 
    <table>
            <tr>
                <td>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCTFeeInfo);">
                </td>
                <td class= titleImg> 退费合计 </td>
            </tr>
	</table>
        
	<Div  id= "divCTFeeInfo" style= "display: ''">
      <table  class= common>       
        <TR class = common>
            <TD  class= title width="5%"> 退费合计金额 </TD>
			<TD  class= input width="20%"><Input class= "readonly" readonly name=GetMoney ></TD>
            <TD  class= title > </TD>
            <TD  class= title > </TD>
            <TD  class= title > </TD>
            <TD  class= title > </TD>
       </TR>
     </table>
	</DIV>
 
    <table>
            <tr>
                <td>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCTFeePolDetail);">
                </td>
                <td class= titleImg> 险种退费合计 </td>
            </tr>
	</table>
        
	<Div  id= "divCTFeePolDetail" style= "display: ''">
        <table  class= common>
        	<tr  class= common>
          		<td text-align: left colSpan=1>
        			<span id="spanCTFeePolGrid" >
        			</span> 
        	  	</td>
        	</tr>
        </table>	
	</Div>
		
    <table>
            <tr>
                <td>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCTFeeDetail);">
                </td>
                <td class= titleImg> 退保费用明细 </td>
            </tr>
	</table>
        
	<Div  id= "divCTFeeDetail" style= "display: ''">
        <table  class= common>
        	<tr  class= common>
          		<td text-align: left colSpan=1>
        			<span id="spanCTFeeDetailGrid" >
        			</span> 
        	  	</td>
        	</tr>
        </table>	
	</Div>
 </DIV>
	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="ContType" name="ContType">
	<input type=hidden id="StrCTFeePol" name="StrCTFeePol">
	<input type=hidden id="StrCTFeeDetail" name="StrCTFeeDetail">
	<input type=hidden name="EdorNo">
	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
