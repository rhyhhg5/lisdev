//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
function easyQueryClick()
{          
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select LPGrpEdorMain.EdorNo,min(LPGrpEdorMain.GrpPolNo),min(LCGrpPol.RiskCode),min(LPGrpEdorMain.EdorType),min(LPGrpEdorMain.InsuredNo),min(LPGrpEdorMain.InsuredName),sum(LPGrpEdorMain.GetMoney),min(LPGrpEdorMain.EdorAppDate),min(LPGrpEdorMain.EdorValiDate),decode(max(LPGrpEdorMain.EdorState)||min(LPGrpEdorMain.EdorState),'00','保全确认','11','正在申请','22','申请确认','21','部分申请确认','20','部分保全确认') from LPGrpEdorMain,LCGrpPol where LPGrpEdorMain.GrpPolNo=LCGrpPol.GrpPolNo and LPGrpEdorMain.EdorState <> '0'"			 
				 + getWherePart( 'LPGrpEdorMain.EdorNo','EdorNo' )
				 + getWherePart( 'LPGrpEdorMain.GrpPolNo','GrpPolNo' )
				 + getWherePart( 'LPGrpEdorMain.EdorType','EdorType' )
				 + getWherePart( 'LCGrpPol.RiskCode','RiskCode' )
				 + getWherePart( 'LPGrpEdorMain.InsuredNo','InsuredNo' )
				 + getWherePart( 'LPGrpEdorMain.EdorAppDate','EdorAppDate' )
				 //+ getWherePart( 'LPGrpEdorMain.ManageCom','ManageCom' );	
				 + getWherePart( 'LPGrpEdorMain.ManageCom','ManageCom','like' )
				 + " group by LPGrpEdorMain.EdorNo";				
	
	//alert(strSQL);
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有保全核保不通过的保全申请！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function CancelEdor()
{

	var arrReturn = new Array();
				
		var tSel = PolGrid.getSelNo();
	
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
		else
		{
			arrReturn = getQueryResult();
			fm.all('EdorNo').value = PolGrid.getRowColData(tSel-1,1);
			fm.all('GrpPolNo').value=PolGrid.getRowColData(tSel-1,2);	

			fm.all("Transact").value = "DELETE||EDOR"
			// showSubmitFrame(mDebug);

		    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			
			fm.submit();
		}
}


function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	
	//********dingzhong*********
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	//**************************
	
/*	
	arrSelected[0] = arrGrid[tRow-1];
	//alert(arrSelected[0][0]);
*/	
	return arrSelected;
}

function afterSubmit( FlagStr, content,Result )
{

  showInfo.close();
  if (FlagStr == "Fail" )
  {             
  	fm.all('EdorNo').value ="";
	fm.all('GrpPolNo').value="";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	fm.all('EdorNo').value ="";
	fm.all('GrpPolNo').value="";	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    easyQueryClick();
  }
  	
  
}