//该文件中包含客户端需要处理的函数和事件
var showInfo;
var pEdorFlag = true;                        //用于实时刷新处理过的数据的列表

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();         //使用翻页功能，必须建立为全局变量
window.onfocus = initFocus;                  //重定义获取焦点处理事件
var arrList1 = new Array();                  //选择的记录列表

/**
 * 查询按钮
 */
function queryClick() {
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var compareAccContNo = "";
  var sql = "  select contNo "
            + "from LCCont "
            + "where grpContNo = '" + fm.GrpContNo.value + "' "
            + "   and polType = '2' "
	          + "with ur ";
  var result = easyExecSql(sql);
  if(result)
  {
    compareAccContNo = " and not exists (select 1 from LCCont where  contno = b.contno   and polType = '2') ";
  }
	var strSql = "  select b.contNo, b.insuredNo, b.name, b.sex, b.birthday, "
	             + "  b.IDType, b.IDNo, '', othIDNo "
	             + "from LCInsured b "
	             + "where 1 = 1 "
	             + getWherePart('b.IDNo','IDNo','like', '0')
	             + getWherePart('b.InsuredNo','CustomerNo','like', '0')
	             + getWherePart('b.Name','Name','like', '0')
	             + getWherePart('b.OthIDNo','OthIDNo','like', '0')
	             + "    and grpContNo = '" + fm.GrpContNo.value + "' "
	             + compareAccContNo
	             + "    and b.insuredNo not in "
	             + "      (select insuredNo "
	             + "      from LPInsured "
	             + "      where edorno = '" + fm.EdorNo.value + "' "
	             + "      and edortype = '" + fm.EdorType.value + "' "
	             + "      and grpContNo = '" + fm.GrpContNo.value + "') "
	             + "      and exists ( select 'x' from lccont where contno=b.contno  and  appflag='1')"
	             + "order by b.insuredNo "
	             + "with ur ";
	turnPage.pageDivName = "divPage";    
	turnPage.queryModal(strSql, LCInsuredGrid); 

	//document.all("test").innerHTML = strSql;
	
	changeNullToEmpty();
	showInfo.close();	 
}

//将账户余额一列的"null"转为""，并计算被保人的帐户余额
function changeNullToEmpty()
{
  //原被保人
  for(var i = 0; i < LCInsuredGrid.mulLineCount; i++)
	{
	  if(LCInsuredGrid.getRowColData(i, 8) == "null")
	  {
	    LCInsuredGrid.setRowColData(i, 8, "");
	  }
	  
	  //帐户余额
	  var insuAccBala = queryInsuAccBala(LCInsuredGrid.getRowColData(i, 1),
	        LCInsuredGrid.getRowColData(i, 2));
	  LCInsuredGrid.setRowColData(i, 8, insuAccBala);
	}
	
	//欲减掉的被保人
	for(var i = 0; i < LCInsured2Grid.mulLineCount; i++)
	{
	  if(LCInsured2Grid.getRowColData(i, 8) == "null")
	  {
	    LCInsured2Grid.setRowColData(i, 8, "");
	  }
	  var insuAccBala = queryInsuAccBala(LCInsured2Grid.getRowColData(i, 1),
	        LCInsured2Grid.getRowColData(i, 2));
	  LCInsured2Grid.setRowColData(i, 8, insuAccBala);
	}
}

//查询帐户余额
function queryInsuAccBala(contNo, insuredNo)
{
  var sql = "  select sum(InsuAccBala) "
            + "from LCInsureAcc "
            + "where contNo = '" + contNo + "' "
            + "   and insuredNo = '" + insuredNo + "' "
            + "with ur ";
  var turnPage3 = new turnPageClass();
  var result = easyExecSql(sql, null, null, null, null, null, turnPage3);
  if(result && result[0][0] != "null")
  {
    return result[0][0];
  }
  else
  {
    return "";
  }
}

/**
 * 单击MultiLine的复选按钮时操作
 */
function reportDetailClick(parm1, parm2) {	
  if (fm.all(parm1).all('LCInsuredGridChk').checked) {
    arrList1[fm.all(parm1).all('LCInsuredGridNo').value] = fm.all(parm1).all('LCInsuredGrid1').value;
  }
  else {
    arrList1[fm.all(parm1).all('LCInsuredGridNo').value] = null;
  }
  
  GrpBQ = false;
}

/**
 * 进入多个人保全
 */
var arrP = 0;
function pEdorMultiDetail() {
  //校验是否选择
    var i = 0;
    var chkFlag=false;
    for (i=0;i<LCInsuredGrid.mulLineCount;i++ )
    {
        if (LCInsuredGrid.getChkNo(i))
        {
          chkFlag=true;
          break;
        }
        
    }
    if (chkFlag)
    {
    	fm.all("ErrorsInfo").innerHTML =""; 
        var showStr = "正在申请集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        PEdorDetail();
    }
    else
    {
        alert("请选择需要操作的记录!");
    }     
  
}

//减人的磁盘导入
function diskImport()
{
	fm.all("ErrorsInfo").innerHTML =""; 
	var strUrl = "DiskImportZTMain.jsp?EdorNo=" + fm.all("EdorNo").value 
	    + "&GrpContNo=" + fm.all("GrpContNo").value + "&EdorType=" + fm.all("EdorType").value;
	window.open(strUrl, "被保人清单导入", "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=yes");
}

/**
 * 进入个人保全
 */
function PEdorDetail() {  
      
 
    fm.all("Transact").value = "INSERT||EDOR";
    fm.action = "GEdorTypeMultiDetailSubmit.jsp";
    fm.submit();
 
}

/**
 * 提交后操作,服务器数据返回后执行的操作
 */
function afterSubmit(FlagStr, content, Result) 
{
  showInfo.close();
  window.focus();

  if (FlagStr == "Fail") 
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else 
  { 
    pEdorFlag = true;
    
    if (fm.Transact.value=="DELETE||EDOR") {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else {
      //openPEdorDetail();
      //PEdorDetail();
    }
  }
}

/**
 * 处理获取焦点事件
 */
function initFocus() {
  if (pEdorFlag) {   
    pEdorFlag = false;
        
    queryPEdorList();
  }
}

var GrpBQ = false;
var GTArr = new Array();
/**
 * 打开个人保全的明细界面
 */
function openPEdorDetail() {
	
	  if (trim(fm.all('EdorType').value)=="PT") {
		  window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp", "", 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		}
		if (trim(fm.all('EdorType').value)=="GT") {
		  window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp", "", 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		}
		if (trim(fm.all('EdorType').value)=="ZT") {
		  PEdorDetail();
		}

		try { showInfo.close();	} catch(e) {}

}

/**
 * 查询出申请后的个人保全列表
 */
function queryPEdorList() {
  
	querySelectedInsured();
	queryClick();
}

//查询选择了的被保人信息
function querySelectedInsured(){

  var strSql = "  select ContNo, InsuredNo, Name, Sex, Birthday, IDType, IDNo, "
               + "    (select sum(InsuAccBala) "
  	           + "        from LCInsureAcc "
  	           + "        where grpContNo = '" + fm.GrpContNo.value + "' "
  	           + "            and contNo = LPInsured.contNo "
  	           + "            and insuredNo = LPInsured.insuredNo), "
  	           + "    othIDNo "
               + "from LPInsured "
               + "where grpContNo = '" + fm.GrpContNo.value + "' "
               + "    and edorNo = '" + fm.EdorNo.value + "' "
               + "    and edorType = '" + fm.EdorType.value + "' "
               + getWherePart('IDNo','IDNo3','like', '0')
	             + getWherePart('InsuredNo','CustomerNo3','like', '0')
	             + getWherePart('Name','Name3','like', '0')
	             + getWherePart('OthIDNo','OthIDNo3','like', '0')
	             + "order by InsuredNo "
	             + "with ur ";
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, LCInsured2Grid); 	
	changeNullToEmpty();
}


/**
 * 单击MultiLine的单选按钮时操作
 */
function reportDetail2Click(parm1, parm2) {	
  if (fm.all(parm1).all('LCInsured2GridChk').value=='on' || fm.all(parm1).all('LCInsured2GridChk').value=='1') {
    fm.ContNo.value = fm.all(parm1).all('LCInsured2Grid1').value;
  }
}

/**
 * 撤销集体下个人保全
 */
function cancelPEdor() 
{
    
    var i = 0;
    var chkFlag=false;
    for (i=0;i<LCInsured2Grid.mulLineCount;i++ )
    {
        if (LCInsured2Grid.getChkNo(i))
        {
          chkFlag=true;
          break;
        }
        
    }
    if (chkFlag)
    {
    		fm.all("ErrorsInfo").innerHTML =""; 
        fm.all("Transact").value = "DELETE||EDOR"
        var showStr = "正在撤销集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.action = "GEdorTypeMultiDetailSubmit.jsp";
        fm.submit();
    }
    else
    {
        alert("请选择需要操作的记录!");
    }     
 
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent() 
{
	if(!confirm("您选择的扣除手续费比例为："+fm.all("FeeRate").value+"%，是否继续？"))
	{
	return false;
	}
    checkPeoples2();
    
    //若是减人，则需要存储算法类别
    if(fm.EdorType.value == "ZT")
    {
        doZT();
    }
    else
    {
        goBack();
    }
}

//校验保单操作后的人数是否小于8，若小于8则给出提示
function checkPeoples2()
{
    if(getPeoples2() < 8)
    {
        alert("保单总人数已小于8人。");
    }
}

//查询保单操作后的人数
function getPeoples2()
{
//    var sql = "  select count(1) "
//              + "from LCInsured "
//              + "where grpContNo = '" + fm.GrpContNo.value + "' "
//              + "    and contNo not in "
//              + "       (select contNo "
//              + "       from LPEdorItem "
//              + "       where edorNo = '" + fm.EdorNo.value + "' "
//              + "           and edorType = '" + fm.EdorType.value + "' "
//              + "           and grpContNo = '" + fm.GrpContNo.value + "')";
    var sql = "  select (select count(1) "
              + "       from LCInsured  m"
              + "       where grpContNo = '" + fm.GrpContNo.value + "'  "
              + "    and exists (select 'x' from lccont where contno=m.contno and appflag='1')) "
	            + "   -   (select count(1) "
	            + "       from LPInsured "
	            + "       where grpContNo = '" + fm.GrpContNo.value + "' "
	            + "           and edorType = '" + fm.EdorType.value + "' "
	            + "           and edorNo = '" + fm.EdorNo.value + "') "
              + "from dual ";
    var result = easyExecSql(sql);

    if(result)
    {
        return parseInt(result[0][0]);
    }
    else
    {
        return -1;
    }
}

//减人操作
function doZT()
{
	 fm.all("ErrorsInfo").innerHTML =""; 
    if(!checkInput())
    {
        return false;
    }
    
    fm.action = "GEdorTypeZTCaltypeSave.jsp";
    
    var showStr = "正在保存集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); 
}

//校验录入的合法性
function checkInput()
{
    var sql = "  select edorAcceptNo "
              + "from LPEdorItem "
              + "where edorAcceptNo = '" + fm.EdorNo.value + "' "
              + "    and edorType = '" + fm.EdorType.value + "' ";
    var result = easyExecSql(sql);
    if(result == null)
    {
        alert("您还没有录入需退保的被保人");
        return false;
    }
    
    return true;
}

//返回父目录
function goBack()
{
    top.opener.focus();
    top.opener.getGrpEdorItem();
    top.close();
}

//存储算类别择后的操作
function afterSubmitCalType(flag, content)
{
    showInfo.close();
    window.focus();
    //此处content可能较长，使用showModalDialog可能有问题，现将错误显示到页面
    if (flag == "Fail" )
    {
        fm.all("ErrorsInfo").innerHTML = content;  
        content = "保存数据失败！发生的错误信息见页面底部";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
	else
	{
		content = "保存数据成功！";
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	    goBack();
	}
}

function saveEdor() {
  if (LCInsured2Grid.mulLineCount == 0)
  {
    alert("必须修改后才能保存申请！");
    return;
  } 
  
   top.opener.getGrpEdorItem();

   var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=保存成功"   ;  
   showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   
	
}
