<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DiskImport.jsp
//程序功能：磁盘导入
//创建日期：2005-7-7
//创建人 ：QiuYang
//更新记录： 更新人  更新日期   更新原因/内容
%>
<html>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="./DiskImport.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>磁盘导入</title>
<script>
	var edorNo = "<%=request.getParameter("EdorNo")%>";
	var grpContNo = "<%=request.getParameter("GrpContNo")%>";
	
	function initForm()
	{
		fm.all("EdorNo").value = edorNo;
		fm.all("GrpContNo").value = grpContNo;
	}
</script>
</head>
<body onload="initForm();">
	<form name="fm" action="./DiskImportSave.jsp?EdorNo=<%=request.getParameter("EdorNo")%>&GrpContNo=<%=request.getParameter("GrpContNo")%>" method=post target="fraSubmit" ENCTYPE="multipart/form-data">
		<input type="hidden" name="EdorNo">
		<input type="hidden" name="GrpContNo">
		<br><br>
		<table class=common>
			<TR>
				<TD width='15%' style="font:9pt">文件名</TD>
				<TD width='85%'>
					<Input type="file" width="100%" name="FileName">
					<INPUT VALUE="导  入" class="cssButton" TYPE=button onclick="importFile();">
				</TD>
			</TR>
		</table>
		<br><br>
		<table class=common>
			<TR align="center">
				<TD id="Info" width='100%' style="font:10pt"></TD>
			</TR>
		</table>
	</form>
</body>
</html>