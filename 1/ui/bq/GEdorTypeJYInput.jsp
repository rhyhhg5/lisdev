<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人  qulq  更新日期   更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeJY.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeJYInit.jsp"%>
  <title>解约</title>
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeJYSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 集体保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpContNo>
      </TD>   
    </TR>
  </TABLE> 
  <Div  id= "divOtherSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="downButton" class=cssButton value="被保险人清单查询"  onclick="grpInsuList();">
	   		 <input type=Button name="queryButton" class= cssButton value="万能险账户查询" onclick="GPublicAccQuery();">
			</TR>
	 	</table>
	</Div>
  
 	<Div id= "divLPAppntGrpDetail" style= "display: ''">
  <table>
    	<tr>
    		<td class= titleImg>
    			 集体保单资料
    		</td>
    	</tr>
    </table>
    <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
       <TR>
          <TD  class= title>
            单位客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" name=AppntNo readonly >
          </TD>  
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input class="readonly" name=GrpName readonly >
          </TD>     
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            签单日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=SignDate readonly >
          </TD>
      
          <TD  class= title>
            生效日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=CValiDate readonly >
          </TD>
         </TR>
         <TR  class= common>
          <TD  class= title>
            签单机构
          </TD>
          <TD  class= input>
            <Input class="readonly" name=SignCom readonly >
          </TD>       
          <TD  class= title>
            投保人数
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=Peoples2 readonly >
          </TD>
      	</TR>
      	 <TR  class= common>
          <TD  class= title>
            总保费
          </TD>
          <TD  class= input>
            <Input class="readonly" name=Prem readonly >
          </TD>
          <TD  class= title8>
            总保额
          </TD>
          <TD  class= input>
            <Input class="readonly" name=Amnt readonly >
          </TD>       
        </TR>
        
       <TR  class= common>
        <td class = title>
        投保日期
        </td>
        <TD  class= input > 
        <input class="readonly" readonly name=InsueredDate >
     		 </TD>
     	<TD class = title > 生效日期 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=CValiDate1>
      </TD>
        </tr>
        <TR class= common>
          <TD class= title colspan=6>
            <table>
              <tr>
                <td class=common>
                  <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCGrpPol);">
                </td>
                <td class=titleImg>险种选择</td>
              </tr>
            </table>
            <Div id="divLCGrpPol" style="display: ''">
              <table class=common>
                <tr class=common>
                  <td text-align: left colSpan=1>
                    <span id="spanLCGrpPolGrid">  </span>
                  </td>
                </tr>
              </table>
            </div>
          </TD>    
        </TR>
        <TR class= common>
          <TD class= title>
            退保原因
          </TD>
          <TD class= input colspan=5>
			      <input class="codeNo" name="reason_tb" readOnly verify="退保原因|notnull&code:reason_tb&len<=10" ondblclick="showCodeList('reason_tb',[this,reason_tbName],[0,1]);" onkeyup="showCodeListKey('reason_tb',[this,reason_tbName],[0,1]);"><Input class="codeName" name=reason_tbName readonly elementtype=nacessary>
          </TD>     
        </TR>
        <TR class= common style="display: 'none'">
          <TD class= title>
            解约后保费
          </TD>
          <TD class= input colspan=5>
			      <Input class="readonly" name="leavingPrem" readonly >
          </TD>     
        </TR>
      </table>
		<%@include file="GrpCTRateInput.jsp"%> 
		<%@include file="SpecialInfoCommon.jsp"%> 
      <br>
   	  <Input type =button class=cssButton value="保  存" onclick="edorTypeCTSave()">
   	  <Input type =button class=cssButton value="返  回" onclick="returnParent()">
    </Div>
	</Div>

	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	 <input type=hidden id="addrFlag" name="addrFlag">
	 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


