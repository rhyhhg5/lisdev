<html> 
<%   
//程序名称：
//程序功能：预收保费
//创建日期：
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdorTypeYS.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeYSInit.jsp"%>
  <title>预收保费</title> 
</head>
<body onload="initForm();">
  <form action="./PEdorTypeYSSave.jsp" method=post name=fm target="fraSubmit">  
   <table class=common>
   		<tr class= common>
	      <td class="title"> 受理号 </td>
	      <td class=input>
	        <input class="readonly" type="text" readonly name=EdorNo >
	      </td>
	      <td class="title"> 批改类型 </td>
	      <td class="input">
	      	<input class="readonly" type="hidden" readonly name=EdorType>
	      	<input class="readonly" readonly name=EdorTypeName>
	      </td>
	      <td class="title"> 保单号 </td>
	      <td class="input">
	      	<input class = "readonly" readonly name=ContNo>
	      </td>   
    	</tr>
  	</table>
   <table class=common>
   		<tr class= common>
	      <td class="title"> 客户号码 </td>
	      <td class=input>
	        <Input class="readonly" type="text" readonly name="CustomerNo">
	    		<!--input type="button" Class="cssButton" name="search" value="个人" onclick="queryCustomerNo();" -->
	      </td>
	      <td class="title"> 客户名称 </td>
	      <td class="input">
	      	<input class="readonly" type="text" readonly name="CustomerName">
	      </td>
	      <td class="title"> 帐户余额 </td>
	      <td class="input">
	     		<Input type="text" class="readonly" readonly name=AccBala>
	      </td>   
    	</tr>
  	</table>
  	<br>
		<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCont);">
	      </td>
	      <td class= titleImg>
	        本次预交针对保单
	      </td>
		  </tr>
		</table>
	  <div id= "divCont" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanContGrid">
						</span> 
					</td>
				</tr>
			</table>			
	  	<div id= "divPage" align=center style= "display: 'none' ">
				<input value="首  页" class=cssButton type=button onclick="turnPage.firstPage();"> 
				<input value="上一页" class=cssButton type=button onclick="turnPage.previousPage();"> 					
				<input value="下一页" class=cssButton type=button onclick="turnPage.nextPage();"> 
				<input value="尾  页" class=cssButton type=button onclick="turnPage.lastPage();"> 
			</div>		
		</div>
    <br>
    <table class=common>
   		<tr class= common>
	      <td class="title"> 本次预交保费 </td>
	      <td class=input>
	        <input class="common" type="text" name="AccFee" >
	      </td>
	     	<!--
	      <td class="title"> 帐户余额合计 </td>
	      <td class="input">
	      	<input class="readonly" type="text" readonly name="AccBala">
	      </td>
	      -->
    	</tr>
  	</table> 
		<br>
	 <Input class=cssButton type=Button value="保  存" onclick="save();">
	 <Input class=cssButton type=Button value="取  消" onclick="initForm();">
	 <Input class=cssButton type=Button value="返  回" onclick="returnParent();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>