<%
//程序名称：GEdorTypeGAInput.jsp
//程序功能：
//创建日期：2006-01-13
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script>
function initForm() 
{
  try
  {
  	initGrpEdor();
  	initPolGrid();
  	initInsuredListGrid();
  	initFailGrid();
  	queryPolInfo();
  	queryImportData();
  	queryFailData();
  }
  catch (ex)
  {
    alert("页面初始化错误！");
  }
}

function initGrpEdor()
{
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
	var flag;
	try
	{
		flag = top.opener.fm.all('loadFlagForItem').value;
	}
	catch(ex)
	{
		flag = "";	
	}
	
	if(flag == "TASK")
	{
		fm.add.style.display = "none";
		fm.sub.style.display = "none";
		fm.save.style.display = "none";
		fm.goBack.style.display = "none";
	}
	
	fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
	fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
	fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
	fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
	fm.all('ContNo').value = "";
	showOneCodeName("EdorCode", "EdorTypeName");
}

function initPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            	//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="险种号";
    iArray[1][1]="60px";
    iArray[1][2]=100;
    iArray[1][3]=3;
    
    iArray[2]=new Array();
    iArray[2][0]="险种序号";
    iArray[2][1]="60px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="险种代码";
    iArray[3][1]="60px";
    iArray[3][2]=100;
    iArray[3][3]=0;
  
    iArray[4]=new Array();
    iArray[4][0]="险种名称";
    iArray[4][1]="250px";
    iArray[4][2]=100;
    iArray[4][3]=0;
    iArray[4][4]="riskcode";   
    
    iArray[5]=new Array();
    iArray[5][0]="团体账户领取金额";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=1;
    iArray[5][9]="团体账户领取金额|notnull&num&value>=0";
    
    iArray[6]=new Array();
    iArray[6][0]="团体固定账户领取金额";
    iArray[6][1]="100px";
    iArray[6][2]=100;
    iArray[6][3]=3;
    iArray[6][9]="团体固定账户领取金额|notnull&num&value>=0";
    
    PolGrid = new MulLineEnter("fm", "PolGrid"); 
    PolGrid.mulLineCount = 0;   
    PolGrid.displayTitle = 1;
    PolGrid.canChk = 0;
    PolGrid.hiddenPlus = 1;
    PolGrid.hiddenSubtraction = 1;
    PolGrid.loadMulLine(iArray);
  }
  catch (ex)
  {
    alert(ex);
  }
}

//导入帐户清单
function initInsuredListGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="被保人编号";        //列名
		iArray[1][1]="60px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="状态";         			//列名
		iArray[2][1]="60px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="客户号";         //列名
		iArray[3][1]="60px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="姓名";         //列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="性别";         			//列名
		iArray[5][1]="30px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="出生日期";         			//列名
		iArray[6][1]="90px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="证件类型";         			//列名
		iArray[7][1]="50px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="证件号码";         			//列名
		iArray[8][1]="150px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[9]=new Array();
		iArray[9][0]="领取金额";         			//列名
		iArray[9][1]="50px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		InsuredListGrid = new MulLineEnter("fm", "InsuredListGrid"); 
		//这些属性必须在loadMulLine前
		InsuredListGrid.mulLineCount = 0;   
		InsuredListGrid.displayTitle = 1;
    InsuredListGrid.canSel = 0;
    InsuredListGrid.canChk = 0;
		InsuredListGrid.hiddenPlus = 1;
		InsuredListGrid.hiddenSubtraction = 1;
	//	InsuredListGrid.selBoxEventFuncName ="onInsuredGridSelected";
		InsuredListGrid.loadMulLine(iArray);  
	}
	catch (ex)
	{
	  alert(ex);
	}
}

function initFailGrid()
{
  var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="被保人编号";        //列名
		iArray[1][1]="60px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="状态";         			//列名
		iArray[2][1]="60px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="客户号";         //列名
		iArray[3][1]="60px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="姓名";         //列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="性别";         			//列名
		iArray[5][1]="30px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="出生日期";         			//列名
		iArray[6][1]="90px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="证件类型";         			//列名
		iArray[7][1]="50px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="证件号码";         			//列名
		iArray[8][1]="150px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[9]=new Array();
		iArray[9][0]="领取金额";         			//列名
		iArray[9][1]="80px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
	  iArray[10]=new Array();
		iArray[10][0]="无效原因";         			//列名
		iArray[10][1]="200px";            		//列宽
		iArray[10][2]=100;            			//列最大值
		iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		FailGrid = new MulLineEnter("fm", "FailGrid"); 
		//这些属性必须在loadMulLine前
		FailGrid.mulLineCount = 0;   
		FailGrid.displayTitle = 1;
    FailGrid.canSel = 0;
    FailGrid.canChk = 0;
		FailGrid.hiddenPlus = 1;
		FailGrid.hiddenSubtraction = 1;
		FailGrid.loadMulLine(iArray);  
	}
	catch (ex)
	{
	  alert(ex);
	}
}
</script>