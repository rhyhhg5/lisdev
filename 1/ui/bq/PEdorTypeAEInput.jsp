<%@include file="../common/jsp/UsrCheck.jsp"%>
<% 
//程序名称：个险投保人变更
//程序功能：
//创建日期：2005-09-22
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeAE.js"></SCRIPT>
  <%@include file="PEdorTypeAEInit.jsp"%>
  <link href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <link href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>投保人变更 </title> 
</head>
<body onload="initForm();initElementtype();">
	<form action="./PEdorTypeAESubmit.jsp" method=post name=fm target="fraSubmit">  
   <table class=common>
   		<tr class= common> 
	      <td class="title"> 受理号 </td>
	      <td class=input>
	        <input class="readonly" type="text" readonly name=EdorNo >
	      </td>
	      <td class="title"> 批改类型 </td>
	      <td class="input">
	      	<input class="readonly" type="hidden" readonly name=EdorType>
	      	<input class="readonly" readonly name=EdorTypeName>
	      </td>
	      <td class="title"> 保单号 </td>
	      <td class="input">
	      	<input class = "readonly" readonly name=ContNo>
	      </td>   
    	</tr>
  	</table> 
  	<table>
	    <tr>
	      <td><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCustomerInfo);"></td>
	      <td class= titleImg>新投保人信息</td>
	    </tr>
   	</table>
		<Div id="divCustomerInfo" style="display: ''">
       <table  class= common>
      	<tr  class= common>
          <td class= title>
            姓名
          </td>
          <td class= input>
            <Input class= common name=Name verify="姓名|notnull" elementtype="nacessary" >
          </td>
				  <td class= title>
						性别
				  </td>
				  <td class= input>
					 	<Input class="codeNo" name=Sex verify="性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName], [0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName], [0,1]);"><Input class="codeName" name=SexName elementtype="nacessary" readonly >
				  </td>
				  <td class= title>
				   	出生日期
				  </td>
				 
				<td class= input>
				    <Input class="coolDatePicker" dateFormat="short" name=Birthday verify="出生日期|notnull"  elementtype="nacessary" readonly>
				</td>
			  </tr>
      	<tr class= common>
          <td class= title>
            证件类型
          </td>
          <td class= input>
            <Input class= codeNo name="IDType" verify="证件类型|notnull&code:IDType" ondblclick="return showCodeList('IDType',[this,IDTypeName], [0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName], [0,1]);"><Input class= codeName name="IDTypeName" elementtype="nacessary" readonly >
          </td>
          <td class= title>
            证件号码
          </td>
          <td class= input>
            <Input class= common name=IDNo verify="证件号码|notnull"  elementtype="nacessary" >
          </td>
       	</tr>
				<tr class= common>
				  <td class=title>
					  职业代码 
					</td>
					<td class=input>
					 	<Input class="codeNo" name=OccupationCode ondblclick="return showCodeList('OccupationCode', [this,OccupationCodeName,OccupationType],[0,1,2],null,null,null,1,300);" onkeyup="return showCodeListKey('OccupationCode', [this,OccupationCodeName,OccupationType],[0,1,2],null,null,null,1,300);" verify="职业代码|code:OccupationCode"><Input name=OccupationCodeName class="codeName" readonly >
					</td>
					<td class=title>
					  职业类别 
					</td>
					<td class=input>
					  <Input class="readonly" name="OccupationType" verify="职业类别|code:OccupationType" readonly>
					</td>
					<td class=title>
					  联系电话 
					</td>
					<td class=input>
					 	<Input class="common" name="Phone">
					</td>
				</tr>
				<tr class= common>
					<td class=title>
					  保单联系地址 
					</td>
					<td class=input colspan="3">
					 	<Input class="common" name="Address" style="width:480">
					</td>
					<td class=title>
					  邮政编码 
					</td>
					<td class=input>
					 	<Input class="common" name="ZipCode">
					</td>
				</tr>
				<tr class= common>
					<td class= title>
					  交费方式
					</td>    
					<td class= input>
					  <Input class="codeNo" name="PayMode" CodeData="0|^1|现金^2|支票^4|银行^9|其它" verify="交付费方式|notnull&code:PayMode"  ondblclick="return showCodeListEx('PayMode1',[this,PayModeName],[0,1]);" onkeyup="return showCodeListEx('PayMode1',[this,PayModeName],[0,1]);"><Input class="codeName" name="PayModeName"  elementtype="nacessary" readonly>
					</td>	
					<td class= title>
						转帐银行
					</td>    
					<td class= input>
						<Input class="codeNo" name=BankCode ondblclick="return showCodeList('Bank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName],[0,1]);"><Input class="codeName" name=BankCodeName readonly >
					</td>	
					<td class= title>
					  转账帐号
					</td>     
					<td class= input>
					  <Input class= "common" name=BankAccNo >
					</td>		      	       
				</tr>
				<tr class= common>
					<td class= title>
					  账户名
					</td>    
					<td class= input colspan="5">
					  <Input class= "common" name=AccName >
					</td>	
				</tr>
       </table>
		</Div>
	 <br>
	 
	 <table>
      <tr>
        <td class=common>
          <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divRelationToAppntInsured);">
        </td>
  	    <td class=titleImg>此保单下全部被保人信息（可以在此修改被保人与投保人的关系）</td>
  	    <p><font color="#ff0000"><br>请将要修改的记录选中，否则修改不会生效。</font></p>
  	  </tr>
    </table>
<div id="divRelationToAppntInsured" style="display:''" >
<table class=common>
      <tr class=common>
  	    <td text-align=left colSpan=1>
  	      <span id="spanRelationToAppntInsuredGrid">
  	      </span>
  	    </td>
  	  </tr>
  	</table>			
 </div>
 
<div  id= "divPage" align=center style= "display: '' ">
      <input CLASS=CssButton VALUE="首  页" class= CssButton TYPE=button onclick="turnPage.firstPage();"> 
      <input CLASS=CssButton VALUE="上一页" class= CssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <input CLASS=CssButton VALUE="下一页" class= CssButton TYPE=button onclick="turnPage.nextPage();"> 
      <input CLASS=CssButton VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage.lastPage();">
</div> 	

	 <Div>
		<input class= cssButton type=Button value="保  存" onclick="save();">
		<input class= cssButton type=Button value="取  消" onclick="initForm();">
		<input class= cssButton type=Button value="返  回" onclick="">
	 </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>