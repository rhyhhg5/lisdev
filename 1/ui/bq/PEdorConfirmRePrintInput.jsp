<%@page contentType="text/html;charset=GBK" %>
<html>
<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
  String ManageCom = tGI1.ManageCom;  
%>    
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./PEdorConfirmRePrint.js"></SCRIPT>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file = "ManageComLimit.jsp"%>

  <title>个人批改查询 </title>
</head>
<body>

  <form action="./PEdorConfirmRePrintSubmit.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor);">
      </td>
      <td class= titleImg>
        请您输入重新生成保全确认打印数据的批单申请号（个单/团单）：
      </td>
    	</tr>
    </table>

    <Div  id= "divLPEdor" style= "display: ''">
      <table  class= common>
       	<TR class=common>    
          <TD  class= title>
            批单申请号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
          </TD>    
        </TR>
        <TR class=common> 

          <TD  class= input>
            <Input class= common style="display: none"  name=ManageCom value=<%=ManageCom%> >
            
          </TD>
        </TR>
      </table>
      <Input class = common type=Button value= "重新生成打印数据" onclick ="ReMakePrintData()">
      <Input class = common type=Button value= "重新生成团单打印数据" onclick ="ReMakePrintGrpData()">
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="PolNo" name="PolNo">
    
     
     <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorApp);">
      </td>
      <td class= titleImg>
        请您输入需要打印的批单号：
      </td>
    	</tr>
    </table>

    <Div  id= "divLPEdorApp" style= "display: ''">
      <table  class= common>
       	<TR class=common>   
          <TD  class= title>
            批单号/批单申请号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorAppNo >
          </TD>    
        </TR>
      </table>
      <Input class = common type=Button value= "打印批单" onclick ="PrintData()">
    </Div> 
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
