var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

//简单查询
function easyQuery()
{
//	应该不用，不让选择时间
//	var startDate = fm.all('StartDate').value;
//	结束时间取当前时间
//	var endDate = new Date();
	var tManageCom = fm.all('ManageCom').value;//管理机构
	var tContNo = fm.all('ContNo').value;//保单号
	var tAgentCode = fm.all('AgentCode').value;//业务员代号
//	var tCustomerno = fm.all('Customerno').value;//投保人客户号
	
	//管理机构校验
	if(tManageCom == "" || tManageCom == null)
	{
		alert("请选择管理机构！");
		return false;
	}
	if(tManageCom == "86")
    {
      if(!confirm("管理机构选择“总公司”，查询速度会很慢，是否继续？"))
      {
        return false;
      }
    }
    
	//失效日期起期校验
//	if (startDate == "")
//	{
//		alert("请输入失效日期起期！");
//		return;
//	}

//    var t2 = endDate.replace(/-/g,"\/").getTime();
//    alert("完全测试使用,到时候看需不需要这个："+t2);

    var wherePar="";
    //限制管理机构
    if(!(tManageCom == "" || tManageCom == null))
    {
      wherePar+="a.ManageCom like '"+tManageCom+"%' and " ; 
    }

    //限制保单号
    if( !(tContNo == "" ||  tContNo == null))
    {
       wherePar+="a.ContNo='"+tContNo+"' and " ;               
    }
    //限制业务员代号 
     if( !(tAgentCode == "" ||  tAgentCode == null))
    {
//       wherePar+= "a.AgentCode=getAgentCode('"+tAgentCode+"') and ";  
    	 wherePar+= "a.AgentCode = getAgentcode('"+tAgentCode+"') and ";  
    }
   alert("wherePar:"+wherePar);     
   var strSQL =  "select distinct "
	   + "contno,stateflag,customerno,cname,nature,managecom,idtype,idno,idstartdate,idenddate,agentcode,aname,aphone "
	   + "from " 
	   + "( "	   
	   + "select " 
	   + "a.contno, "
	   + "(select codename from ldcode where codetype = 'stateflag' and code = a.stateflag) stateflag, "
	   + "c.appntno customerno, "
	   + "c.appntname cname , "
	   + "'投保人' nature, "
	   + "(select Name from ldcom where comcode=a.ManageCom) managecom, "
	   + "getUniteCode(a.AgentCode) agentcode, "
	   + "(select Name from laagent where AgentCode=a.AgentCode) aname, "
	   + "(select codename from ldcode where codetype = 'idtype' and code = c.idtype) idtype , "
	   + "c.idno,c.idstartdate,c.idenddate, " 
	   + "(select Mobile from laagent where AgentCode=a.AgentCode) aphone "
	   + "from lccont a,laagent b,lcappnt c "
	   + "where " 
	   + "a.appntno = a.insuredno and "
	   + "a.contno = c.contno and " 
	   + "a.agentcode = b.agentcode and "
	   + "c.idenddate is not null and "
	   + "idenddate < current date and " 
	   + "a.appflag = '1' and " 
	   + wherePar 
	   + "a.conttype = '1' " 
	   + getWherePart("c.appntno", "Customerno")
	   + "union all "
	   + "select " 
	   + "a.contno, "
	   + "(select codename from ldcode where codetype = 'stateflag' and code = a.stateflag) stateflag, "
	   + "c.appntno customerno, "
	   + "c.appntname cname , "
	   + "'投保人' nature, "
	   + "(select Name from ldcom where comcode=a.ManageCom) managecom, "
	   + "getUniteCode(a.AgentCode) agentcode, "
	   + "(select Name from laagent where AgentCode=a.AgentCode) aname, "
	   + "(select codename from ldcode where codetype = 'idtype' and code = c.idtype) idtype , "
	   + "c.idno,c.idstartdate,c.idenddate, " 
	   + "(select Mobile from laagent where AgentCode=a.AgentCode) aphone "
	   + "from lccont a,laagent b,lcappnt c "
	   + "where " 
	   + "a.appntno <> a.insuredno and "
	   + "a.contno = c.contno and " 
	   + "a.agentcode = b.agentcode and "
	   + "c.idenddate is not null and "
	   + "idenddate < current date and " 
	   + "a.appflag = '1' and " 
	   + wherePar 
	   + "a.conttype = '1' " 
	   + getWherePart("c.appntno", "Customerno")
	   + "union all "
	   + "select " 
	   + "a.contno, "
	   + "(select codename from ldcode where codetype = 'stateflag' and code = a.stateflag) stateflag, "
	   + "c.insuredno customerno, "
	   + "c.name cname , "
	   + "'被保人' nature, "
	   + "(select Name from ldcom where comcode=a.ManageCom) managecom, "
	   + "getUniteCode(a.AgentCode) agentcode, "
	   + "(select Name from laagent where AgentCode=a.AgentCode) aname, "
	   + "(select codename from ldcode where codetype = 'idtype' and code = c.idtype) idtype , "
	   + "c.idno,c.idstartdate,c.idenddate, " 
	   + "(select Mobile from laagent where AgentCode=a.AgentCode) aphone "
	   + "from lccont a,laagent b,lcinsured c "
	   + "where " 
	   + "a.appntno <> a.insuredno and "
	   + "a.contno = c.contno and " 
	   + "a.agentcode = b.agentcode and "
	   + "c.idenddate is not null and "
	   + "idenddate < current date and " 
	   + "a.appflag = '1' and " 
	   + wherePar
	   + "a.conttype = '1' " 
	   + getWherePart("c.insuredno", "Customerno")
	   + ") x " 
	   + "order by idenddate,contno ";
   alert(strSQL);
   turnPage2.queryModal(strSQL, IDOverdueGrid);  
   if(IDOverdueGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}	
	fm.sql.value =  strSQL;
	showCodeName(); 
}
//下载清单 符合条件
function easyPrint()
{	
	if( IDOverdueGrid.mulLineCount == 0)
	{
		alert("没有需要打印的信息");
		return false;
	}
	alert(1);
	fm.submit();
}

//批量打印 
function printAllNotice()
{
	if (PContTerminateGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PContTerminateGrid.mulLineCount; i++)
	{	
		if(PContTerminateGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}

	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.action="../bq/PContTerminateInputPrintAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.action="../bq/PContTerminateInputPrintForBatPrt.jsp";
		fm.submit();
	}

}
function afterSubmit2(FlagStr,Content){
    alert(Content);
}















