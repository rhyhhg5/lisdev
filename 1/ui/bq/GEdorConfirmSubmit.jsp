<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PEdorConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.task.*"%>
<%
    String flag = "";
    String content = "";
    String edorAcceptNo = request.getParameter("EdorAcceptNo");
    String fmtransact = request.getParameter("fmtransact");
		String payMode = request.getParameter("PayMode");
		String balanceMethodValue = request.getParameter("BalanceMethodValue");
		String customerNo = request.getParameter("CustomerNo");
		String accType = request.getParameter("AccType");
		String destSource=request.getParameter("DestSource");
		String contType=request.getParameter("ContType");
	  String fmtransact2 = request.getParameter("fmtransact2");
    GlobalInput gi = (GlobalInput)session.getValue("GI");
    
    String failType = null;
		String autoUWFail = null;
		
		System.out.println("fmtransact2:" + fmtransact2);
		
		//若使用账户冲抵保全收退费，则默认为即时结算
		if(!"NOTUSEACC".equals(fmtransact2)){
			balanceMethodValue="1";
		}
		//qulq 2007-4-5 
		String 	checkSQL = "select 1 from LPGrpEdorItem " +
            					"where EdorNo = '" + edorAcceptNo + "' " +
            					"and EdorType in ('LQ','ZB','ZA','ZE')";
  	SSRS rs =new ExeSQL().execSQL(checkSQL);
  	if(rs!= null && rs.getMaxRow() > 0 )
  	{
	  	if(balanceMethodValue.equals("0"))
  		{
  			flag = "Fail";
				content = "部分领取、追加保费、管理式医疗减少保额项目不得进行定期结算，请单独操作并进行即时结算";
  		}
  	}
  
  	//xiep 2008-12-1 增加做NI项目时会增人重复的校验
		String 	checkNI = "select 1 from LPGrpEdorItem " +
            					"where EdorNo = '" + edorAcceptNo + "' " +
            					"and EdorType = 'NI' ";
  	SSRS mssrs =new ExeSQL().execSQL(checkNI);
  	if(mssrs!= null && mssrs.getMaxRow() > 0 )
  	{
  		//增加理算后对于lcinsured是否缺失的校验 add by xp 100604
  		String 	checkInsured = " select 1 from lccont where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"+
  								edorAcceptNo+"' fetch first 1 row only) and contno not in "+
  								"(select distinct contno from lcinsured where grpcontno=(select grpcontno from lpgrpedoritem where edorno='"+
  								edorAcceptNo+"' fetch first 1 row only) ) and contno in (select contno from lcinsuredlist where edorno='"+edorAcceptNo+"') ";
  	  SSRS mssrs1 =new ExeSQL().execSQL(checkInsured); 
  	  if(mssrs1!= null && mssrs1.getMaxRow() > 0 )  
 	   	  {
  			 flag = "Fail";
			content = "保全项目增人时出现lcinsured丢失的问题,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
  		  }     					
       //------------------insured校验结束-------------------  
          					
  			String sql1 = "select getmoney from lpgrpedoritem "+
            					"where EdorNo = '" + edorAcceptNo + "' " +
            					"and EdorType = 'NI' ";
            SSRS tSSRS1 =new ExeSQL().execSQL(sql1);
            String itemmoney = tSSRS1.GetText(1,1);
            String sql2 = "select sum(getmoney) from ljsgetendorse "+
            					" where endorsementno = '" + edorAcceptNo + "' " +
            					" and feeoperationtype='NI' and feefinatype='BF' with ur";
            SSRS tSSRS2 =new ExeSQL().execSQL(sql2);
            String endorsemoney = tSSRS2.GetText(1,1);
  	
  		if(!itemmoney.equals(endorsemoney))
	  	{
  			flag = "Fail";
			content = "保全项目增人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[NI]项目明细重试";
  		}
        //-----------------------关于增人总数和实际的理算的人数的比较-----------------------
           String sql_check_ljagetendorse = 
                      "  select count(distinct a.contno),count(distinct b.contno) "+
                           "  from ljsgetendorse a, lcinsuredlist b "+
                           "  where a.grpcontno = b.grpcontno "+
                           "  and a.endorsementno = '"+edorAcceptNo+"'"+
                           "  and a.feeoperationtype = 'NI' "+
                           "  and a.endorsementno=b.edorno "+
                           "  and b.state='1' "+
                           "  with ur";
                  //---------------------------------------------------------
                    SSRS t_check_ljagendorse_SSRS1 =new ExeSQL().execSQL(sql_check_ljagetendorse);
                 //---------------------------------------------------------
                    System.out.println("sql_check_ljagetendorse:"+sql_check_ljagetendorse);
                    int the_number_of_1 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1,1));
                    int the_number_of_2 = Integer.parseInt(t_check_ljagendorse_SSRS1.GetText(1,2));
                    System.out.println("the_number_of_1:"+the_number_of_1);
                    System.out.println("the_number_of_2:"+the_number_of_2);
                 if(the_number_of_1!=the_number_of_2)
                 {
                     flag = "Fail";
                     content = "实际理算完成的人数和模板录入的人数不一致，请点击[重复理算]回退到保全明细录入页面，并重新录入[NI]项目明细";
                 }
  			
  	}
  	
  	//xiep 2010-4-21 增加做ZT项目时会错误的校验
		String 	checkZT = "select 1 from LPGrpEdorItem " +
            					"where EdorNo = '" + edorAcceptNo + "' " +
            					"and EdorType = 'ZT' ";
  	SSRS mssrs1 =new ExeSQL().execSQL(checkZT);
  	if(mssrs1!= null && mssrs1.getMaxRow() > 0 )
  	{
  			String sql3 = "select sum(getmoney) from lpedoritem "+
            					"where EdorNo = '" + edorAcceptNo + "' " +
            					"and EdorType = 'ZT' ";
            SSRS tSSRS3 =new ExeSQL().execSQL(sql3);
            String itemmoneyZT = tSSRS3.GetText(1,1);
            String sql4 = "select case when sum(getmoney) is null then 0 else sum(getmoney) end from ljsgetendorse "+
            					" where endorsementno = '" + edorAcceptNo + "' " +
            					" and feeoperationtype='ZT'  with ur";
            SSRS tSSRS4 =new ExeSQL().execSQL(sql4);
            String endorsemoneyZT = tSSRS4.GetText(1,1);
  	
  		if(!itemmoneyZT.equals(endorsemoneyZT))
	  	{
  			flag = "Fail";
			content = "保全项目减人时出现金额错误,请点击[重复理算]回退到保全明细并重新添加[ZT]项目明细重试";
  		}
  	}
  	
  	//xiep 2009-6-24 增加做WS项目时会实名化重复的校验
		String 	checkWS = "select 1 from LPGrpEdorItem " +
            					"where EdorNo = '" + edorAcceptNo + "' " +
            					"and EdorType = 'WS' ";
  	SSRS mrs =new ExeSQL().execSQL(checkWS);
  	if(mrs!= null && mrs.getMaxRow() > 0 )
  	{
  			String wssql = "select 1 from lpcont where edorno='" + edorAcceptNo + "' group by insuredno having count(contno)>1";
            SSRS wsSSRS =new ExeSQL().execSQL(wssql);
        if(wsSSRS!= null && wsSSRS.getMaxRow() > 0 )
  		{
  			flag = "Fail";
			content = "无名单实名化时增人出现重复数据,请点击[重复理算]回退到保全明细后重新添加[WS]项目明细";
		}
  	}

	if(!flag.equals("Fail"))
		{
		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorAcceptNo, "YE");
		tSpecialData.add("CustomerNo", customerNo);
		tSpecialData.add("AccType", accType);
		tSpecialData.add("OtherType", "3");
		tSpecialData.add("OtherNo", edorAcceptNo);
		tSpecialData.add("DestSource", destSource);
		tSpecialData.add("ContType", contType);
	  tSpecialData.add("Fmtransact2", fmtransact2);
    
   	LCAppAccTraceSchema tLCAppAccTraceSchema=new LCAppAccTraceSchema();
	  tLCAppAccTraceSchema.setCustomerNo(customerNo);
	  tLCAppAccTraceSchema.setAccType(accType);
	  tLCAppAccTraceSchema.setOtherType("3");//团单
	  tLCAppAccTraceSchema.setOtherNo(edorAcceptNo);
	  tLCAppAccTraceSchema.setDestSource(destSource);
	  tLCAppAccTraceSchema.setOperator(gi.Operator);
	  
    VData tVData = new VData();
    tVData.add(tLCAppAccTraceSchema);
    tVData.add(tSpecialData);
		tVData.add(gi);
		PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
		if (!tPEdorAppAccConfirmBL.submitData(tVData, "INSERT||Param"))
		{
			flag = "Fail";
			content = "处理帐户余额失败！";
		}
  	else
  	{
	    BqConfirmUI tBqConfirmUI = new BqConfirmUI(gi, edorAcceptNo, BQ.CONTTYPE_G,balanceMethodValue);
	    if (!tBqConfirmUI.submitData())
	    {
	      flag = "Fail";
	      content = tBqConfirmUI.getError();
	    }
	    else
	    {
	    	System.out.println("交退费通知书" + edorAcceptNo);
				TransferData tTransferData = new TransferData();
				tTransferData.setNameAndValue("payMode", payMode);	
				tTransferData.setNameAndValue("endDate", request.getParameter("EndDate"));
				tTransferData.setNameAndValue("payDate", request.getParameter("PayDate"));
				tTransferData.setNameAndValue("bank", request.getParameter("Bank"));
				tTransferData.setNameAndValue("bankAccno", request.getParameter("BankAccno"));
				tTransferData.setNameAndValue("accName", request.getParameter("AccName"));
	  		
	  		//生成交退费通知书
				FeeNoticeGrpVtsUI tFeeNoticeGrpVtsUI = new FeeNoticeGrpVtsUI(edorAcceptNo);
				if (!tFeeNoticeGrpVtsUI.submitData(tTransferData))
				{
					flag = "Fail";
					content = "生成批单失败！原因是：" + tFeeNoticeGrpVtsUI.getError();
				} 		
				
				VData data = new VData();
				data.add(gi);
				data.add(tTransferData);
				SetPayInfo spi = new SetPayInfo(edorAcceptNo);
				if (!spi.submitDate(data, fmtransact))
				{
					System.out.println("设置转帐信息失败！");
					flag = "Fail";
					content = "设置收退费方式失败！原因是：" + spi.mErrors.getFirstError();
				}
	  		flag = "Succ";
				content = "保全确认成功！";
				String message = tBqConfirmUI.getMessage();
				if ((message != null) && (!message.equals("")))
				{
				  content += "\n" + tBqConfirmUI.getMessage();
				}
	    }
		  content = PubFun.changForHTML(content);
	    failType = tBqConfirmUI.getFailType();  //是否审批通过
	    autoUWFail = tBqConfirmUI.autoUWFail();  //是否自核通过
	    autoUWFail = autoUWFail == null ? "" : autoUWFail;
	  }
	  }
	  System.out.println("end");
%>                      
<html>
<script language="javascript">
	if("<%=failType%>" == "1")
	{
		//自动审批未通过
		parent.fraInterface.fm.failType.value = 1;
	}
  else
  {
    //自动核保失败
    if("<%=autoUWFail%>" == 0)
    {
		  parent.fraInterface.fm.autoUWFlagFail.value = 1;
    }
  }
  parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>", "<%=edorAcceptNo%>");
</script>
</html>

