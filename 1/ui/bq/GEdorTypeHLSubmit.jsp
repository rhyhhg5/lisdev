<%
//程序名称：PEdorTypeCMSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
  <%
  String flag = "";
  String content = "";
  String typeFlag = request.getParameter("TypeFlag");
  String PayMode = request.getParameter("PayMode");
  String PerPayMode = request.getParameter("PerPayMode");
  String Money = request.getParameter("Money");
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  //提取领取方式

  LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
  tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
  tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
  tLPEdorItemSchema.setInsuredNo(request.getParameter("InsuredNo"));
 
  VData data = new VData();
  data.add(typeFlag);
  data.add(PayMode);
  data.add(PerPayMode);
  data.add(Money);
  data.add(gi);
  data.add(tLPEdorItemSchema);

  PEdorHLDetailUI tPEdorHLDetailUI = new PEdorHLDetailUI();
  if (!tPEdorHLDetailUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tPEdorHLDetailUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！";
  }
  content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>