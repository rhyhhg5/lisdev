<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./GEdorConfirmQuery.js"></SCRIPT>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorConfirmQueryInit.jsp"%>
    <%@include file = "ManageComLimit.jsp"%>

  <title>集体批改查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./GEdorConfirmSubmit.jsp" method=post name=fm target="fraSubmit">
    <!-- 集体信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLPEdor1" style= "display: ''">
      <table  class= common>
       	 <TR class=common>
          <TD  class= title>
            集体保单号
          </TD>
          <TD  class= input>
            <Input class= common name=GrpContNo >
          </TD>
          <TD  class= title>
            集体批单号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
          </TD>        
        </TR>
      </table>
    </Div>
          <INPUT VALUE="查询" class=cssButton TYPE=button onclick="easyQueryClick();">           
                  					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor2);">
    		</td>
    		<td class= titleImg>
    			 集体批改信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdor2" style= "display: ''" align = "center">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanGEdorMainGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<input type=hidden id="EdorType" name="EdorType">
        <!--<input type=hidden id="EdorNo" name="EdorNo">-->
  	<input type=hidden id="ContType" name="ContType">
  	<!--<table class=common>-->
  	<!--<tr class=common>-->
     	 <!--<TD class = input >-->
     	 	
     	 <!--</TD>-->
    	<!--</tr>-->
    	<!--</table>-->
        <hr>
        <Input class=cssButton type=Button value="保全确认" onclick="edorConfirm()">
        <Input class=cssButton type=Button value="查看批改明细" onclick="PrtEdor()">
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
