<%@page contentType="text/html;charset=GBK"%>
<%@page pageEncoding="GBK"%>
<%
	request.setCharacterEncoding("GBK");
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/Download.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.utility.*"%>

<%
	boolean errorFlag = false;

	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");

	//生成文件名
	Calendar cal = new GregorianCalendar();
	String min = String.valueOf(cal.get(Calendar.MINUTE));
	String sec = String.valueOf(cal.get(Calendar.SECOND));
	String downLoadFileName = "分红派发清单_" + tG.Operator + "_" + min + sec
			+ ".xls";
	String filePath = application.getRealPath("/");
	System.out.println("filepath................" + filePath);
	String tOutXmlPath = filePath + File.separator + downLoadFileName;
	System.out.println("OutXmlPath:" + tOutXmlPath);

	String tManageCom = request.getParameter("organcode");
	String tqueryType = request.getParameter("queryType");
	String tgetType = request.getParameter("getType");
	String tStartDate = request.getParameter("StartDate");
	String tEndDate = request.getParameter("EndDate");

	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("StartDate", tStartDate);
	tTransferData.setNameAndValue("EndDate", tEndDate);
	tTransferData.setNameAndValue("ManageCom", tManageCom);
	tTransferData.setNameAndValue("queryType", tqueryType);
	System.out.println("tqueryType:" + tqueryType);
	VData tData = new VData();
	tData.add(tG);
	tData.add(tTransferData);

	BonusNoticeQueryBL tbl = new BonusNoticeQueryBL();
	CreateExcelList tCreateExcelList = new CreateExcelList();
	tCreateExcelList = tbl.getsubmitData(tData, tgetType);
	if (tCreateExcelList == null) {
		errorFlag = true;
		System.out.println("EXCEL生成失败！");
	} else {
		errorFlag = false;
	}
	if (!errorFlag) {
		//写文件到磁盘
		try {
			tCreateExcelList.write(tOutXmlPath);
		} catch (Exception e) {
			errorFlag = true;
			System.out.println(e);
		}
	}
	System.out.println(errorFlag);
	//返回客户端
	if (!errorFlag) {
		downLoadFile(response, filePath, downLoadFileName);
	}
	out.clear();
	out = pageContext.pushBody();
	if (errorFlag) {
	}
%>

<html>
<script language="javascript">
	alert("打印失败");
	top.close();
</script>
</html>
