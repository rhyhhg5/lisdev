<%
//程序名称：ReportConfirmQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//从工单接收数据
var mLoadFlag = "<%=request.getParameter("LoadFlag")%>";
var mEdorNo = "<%=request.getParameter("DetailWorkNo")%>"; //保全号

//工单接口函数
function initTaskInterface()
{
    if (mLoadFlag == "TASK")
    {
    	fm.all("QEdorNo").value = mEdorNo;
    	easyQueryClick();
    }
}

//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divEdorManuUWAct.style.left=ex;
  	divEdorManuUWAct.style.top =ey;

    edorClicked();
}

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	fm.all('EdorNo').value='';
	fm.all('GrpContNo').value = '';
	fm.all('uwGradeFlag').value = '';

  }
  catch(ex)
  {
    alert("在PEdorConfirmQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在PEdorConfirmQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initPGrpEdorMainGrid();
	initPGrpEdorTypeGrid();
	initQuery();
	initTaskInterface();
 	divGrpEdorSelect.style.display = "";
	divEdorManuUWAct.style.display = "none";
	divGrpEdorMainManuUW.style.display = "none";
	divGrpEdorTypeManuUW.style.display = "none";
 }
  catch(re)
  {
    alert("PEdorConfirmQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPGrpEdorMainGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="申请批单号";    	        //列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="集体合同保单号"; 		//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      iArray[3]=new Array();
      iArray[3][0]="生效日期";         		//列名
      iArray[3][1]="90px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[4]=new Array();
      iArray[4][0]="申请日期";         		//列名
      iArray[4][1]="90px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="变动保费";         	//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="补退费金额";         	//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      PGrpEdorMainGrid = new MulLineEnter( "fm" , "PGrpEdorMainGrid" ); 
      //这些属性必须在loadMulLine前
      PGrpEdorMainGrid.mulLineCount = 10;   
      PGrpEdorMainGrid.displayTitle = 1;
      PGrpEdorMainGrid.canSel=1;
      PGrpEdorMainGrid.hiddenPlus = 1;
      PGrpEdorMainGrid.hiddenSubtraction=1;
      
//      PGrpEdorMainGrid.detailInfo="单击显示详细信息";
      PGrpEdorMainGrid.selBoxEventFuncName="reportDetailClick";
//      PGrpEdorMainGrid.detailClick=reportDetailClick;
      PGrpEdorMainGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initPGrpEdorTypeGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="申请批单号";    	        //列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="批改项目"; 		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      iArray[3]=new Array();
      iArray[3][0]="生效日期";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[4]=new Array();
      iArray[4][0]="申请日期";         		//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="核保结论";         	    //列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=2;            			//列最大值
      iArray[5][4]="edoruwstate";              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][5]=5;
      iArray[5][6]=0;

      PGrpEdorTypeGrid = new MulLineEnter( "fm" , "PGrpEdorTypeGrid" ); 
      //这些属性必须在loadMulLine前
      PGrpEdorTypeGrid.displayTitle = 1;
      PGrpEdorTypeGrid.canSel=1;
      PGrpEdorTypeGrid.hiddenPlus = 1;
      PGrpEdorTypeGrid.hiddenSubtraction=1;
      
      PGrpEdorTypeGrid.selBoxEventFuncName="edorTypeGridClick";
      PGrpEdorTypeGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initQuery()
{
	
	
	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select UWPopedom from lduser where usercode = '<%=tG.Operator%>'";
	
	//alert(strSQL);
				 
	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  
	  //判断是否查询成功
	if (turnPage.strQueryResult) 
	{
	    //查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		fm.all('uwGradeFlag').value = turnPage.arrDataCacheSet[0][0];	 	 
	}
}

</script>