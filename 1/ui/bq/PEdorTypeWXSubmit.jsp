
<%
	//程序名称：PEdorTypeWXSubmit.jsp
	//程序功能：
	//创建日期：2011-08-04
	//创建人  ：xp
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息
	CErrors tError = null;
	//后面要执行的动作：添加，修改

	String FlagStr = "";
	String Content = "";

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");

	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String contNo = request.getParameter("ContNo");
	String Fee = request.getParameter("Fee");
	String feemode = request.getParameter("feemode");
	String getnoticeno = request.getParameter("getnoticeno");

	System.out.println("Fee:"+Fee);
	System.out.println("feemode:"+feemode);
	System.out.println("getnoticeno:"+getnoticeno);
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setContNo(contNo);
	tLPEdorItemSchema.setEdorType(edorType);
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("feemode",feemode);
	tTransferData.setNameAndValue("Fee",Fee);
	tTransferData.setNameAndValue("getnoticeno",getnoticeno);
	// 准备传输数据 VData		
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tTransferData);
	tVData.add(tLPEdorItemSchema);
	PEdorWXDetailUI tPEdorWXDetailUI = new PEdorWXDetailUI();
	if (!tPEdorWXDetailUI.submitData(tVData, ""))
	{
		System.out.println("Submit Failed! " + tPEdorWXDetailUI.mErrors.getErrContent());
		Content = "数据处理失败，原因是:" + tPEdorWXDetailUI.mErrors.getFirstError();
		FlagStr = "Fail";
	} else {
		Content = "保存成功";
		FlagStr = "Success";
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
