var turnPage = new turnPageClass();
//计算每行金额
function getMoney()
{
  
   LADiscountGrid.delBlankLine();//删除空行
   var rowNum=LADiscountGrid.mulLineCount;
   for(i=0;i<rowNum;i++)
   {
     var serclass1 = LADiscountGrid.getRowColDataByName(i, "serclass1");
     var serclass2 = LADiscountGrid.getRowColDataByName(i, "serclass2");
     var serclass3 = LADiscountGrid.getRowColDataByName(i, "serclass3");
     var Address = LADiscountGrid.getRowColDataByName(i, "Address");
     if(serclass1==""||serclass1=="null"||serclass1==null)
      {
        alert("请先录入第"+(i+1)+"行的 服务项目");
        return ; 
      }
      if(serclass2==""||serclass2=="null"||serclass2==null)
      {
        alert("请先录入第"+(i+1)+"行的 具体项目");
        return ; 
      }
      if(Address==""||Address=="null"||Address==null)
      {
        alert("请先录入第"+(i+1)+"行的 地点");
        return ; 
      }
   /*   else if(Address=="北京")
      {
          Address="8611";
      }
      else if(Address=="辽宁")
      {
          Address="8621";
      }
      else if(Address=="上海")
      {
          Address="8631";
      }
      else if(Address=="天津")
      {
          Address="8612";
      }
      else if(Address=="浙江")
      {
          Address="8633";
      }
      */
     var sql="select "
            +"a.seqno,"
            +"a.amount,"
            +"(select codename from ldcode where codetype='quantifiers' and code=a.quantifiers) "
            +"from ldzxhealth a where "
            +"a.serclass1='"+serclass1+"' "
            +"and a.serclass2='"+serclass2+"' "
            +"and a.serclass3='"+serclass3+"' "
            +"and comcode='"+Address+"' ";
     var rs = easyExecSql(sql);
     if(rs)
     {
       LADiscountGrid.setRowColData(i,5,rs[0][1]);
       LADiscountGrid.setRowColData(i,6,rs[0][2]);
       LADiscountGrid.setRowColData(i,11,rs[0][0]);//唯一标识
     }
    /* else
     {
        alert("");
     }*/
   }

}
//计算总金额
function sumGetMoney()
{
   LADiscountGrid.delBlankLine();//删除空行
   var rowNum=LADiscountGrid.mulLineCount;
   var sumMoney=0;
    for(i=0;i<rowNum;i++)
   {
      var money = LADiscountGrid.getRowColDataByName(i, "money");
      var count = LADiscountGrid.getRowColDataByName(i, "count");
      if(money==""||money=="null"||money==null)
      {
        alert("请先点击 金额计算 按钮计算单行金额");
        return ; 
      }
      if(count==""||count=="null"||count==null)
      {
        alert("请录入第"+(i+1)+"行的 份数");
        return ; 
      }
      sumMoney+=money*count;
   }
   fm.all("ZongJinEr").value=sumMoney;
}

function queryList()
{
  var strSQL="select "
            +"db2inst1.codename('serclass1',a.serclass1), "
            +"db2inst1.codename('serclass2',a.serclass2), "
            +"a.comcode, "
            +"db2inst1.codename('serclass3',a.serclass3), "
            +"a.amount, "
            +"db2inst1.codename('quantifiers',a.quantifiers), "
            +"(select edorvalue  from lpedorespecialdata where edorno='"+fm.all('EdorNo').value+"' and edortype='ZX' and detailtype='ZXFWXM' and polno=char(a.seqno)),"
            +"a.serclass1, "
            +"a.serclass2, "
            +"a.serclass3, "
            +"a.seqno "
            +"from ldzxhealth a where seqno "
            +"in (select int(polno) "
            +"from lpedorespecialdata  "
            +"where edorno='"+fm.all('EdorNo').value+"' "
            +"and edortype='ZX' "
            +"and detailtype='ZXFWXM') "
            ;        
  turnPage.queryModal(strSQL, LADiscountGrid); 
  //showCodeName();
}


//明细中查询是否已经录入领取金额
function queryMoney()
{
  var strSQL="select edorvalue from lpedorespecialdata where edorno='"+fm.all('EdorNo').value+"' and edortype='ZX' and detailtype='AppMoney'";
  var arrResult=easyExecSql(strSQL);
  if(arrResult!=null)
  {
  fm.all('ZongJinEr').value=arrResult[0][0];
  }
  else
  {
  fm.all('ZongJinEr').value="";
  }
}     

function edorTypeZXSave()
{
  if(!checkZhangHuYuEr())
  {
    return;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}

function checkZhangHuYuEr()
{
       
        var accBala=0;
		var sql = "select accbala from lcappacc where customerno in (select appntno from lccont where contno='" + fm.all('ContNo').value + "') ";
			var arrResult = easyExecSql(sql);
			if (arrResult)
			{
				accBala = arrResult[0][0];
			}
	   if((accBala-fm.all("ZongJinEr").value)<1000)
	   {
	     alert("换取健康尊享服务后剩余的个人账户价值不得低于人民币1000元。");
	     return false;
	   }
			
}




//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

	try { 
		showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{

	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  returnParent();
	}
}

function returnParent()
{
	try
	{
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {}
}