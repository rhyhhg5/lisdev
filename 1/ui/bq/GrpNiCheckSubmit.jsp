<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpNiCheckSubmit.jsp
//程序功能：把导入清单中的被保人设为无效
//创建日期：2005-08-2
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<html>
<script language="javascript">
var paramArray = new Array;
<%
	String flag = "Succ";
	String param1 = "";
	String param2 = "";
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");	
	
	String grpContNo = request.getParameter("GrpContNo");
	String edorNo = request.getParameter("EdorNo");
	String fmtransact = request.getParameter("fmtransact");
	
	//输入参数
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(grpContNo);
	tVData.add(edorNo);
	
	GrpEdorNICheckUI tGrpEdorNICheckUI = new GrpEdorNICheckUI();
	if (!tGrpEdorNICheckUI.submitData(tVData, fmtransact))
	{
		VData result = tGrpEdorNICheckUI.getResult();
		flag = (String) result.get(0);
		if (flag.equals("CONTPLAN"))
		{
			List list = (List) result.get(1);
			for (int i=0; i<list.size(); i++)
			{
			   TransferData tTransferData = (TransferData)list.get(i);
%>
         paramArray[<%=i%>] = new Array();
         paramArray[<%=i%>][0] = "<%=(String) tTransferData.getValueByName("INSUREDNO")%>";
         paramArray[<%=i%>][1] = "<%=(String) tTransferData.getValueByName("INSUREDNAME")%>";
         paramArray[<%=i%>][2] = "<%=(String) tTransferData.getValueByName("IDTYPE")%>";
         //paramArray[<%=i%>][3] = "<%=(String) tTransferData.getValueByName("IDNO")%>";
         paramArray[<%=i%>][3] = "<%=(String) tTransferData.getValueByName("Sex")%>";
         paramArray[<%=i%>][4] = "<%=(String) tTransferData.getValueByName("Birthday")%>";
<%
			}
		}
		else if (flag.equals("WAITPERIOD"))
		{
			List list = (List) result.get(1);
			for (int i=0; i<list.size(); i++)
			{
			   TransferData tTransferData = (TransferData)list.get(i);
%>
         paramArray[<%=i%>] = new Array();
         paramArray[<%=i%>][0] = "<%=(String) tTransferData.getValueByName("RISKCODE")%>";
         paramArray[<%=i%>][1] = "<%=(String) tTransferData.getValueByName("RISKNAME")%>";
<%
			}
		}
		else if (flag.equals("RELATION"))
		{
			List list = (List) result.get(1);
			for (int i=0; i<list.size(); i++)
			{
			   TransferData tTransferData = (TransferData)list.get(i);
%>
         paramArray[<%=i%>] = new Array();
         paramArray[<%=i%>][0] = "<%=(String) tTransferData.getValueByName("INSUREDNO")%>";
         paramArray[<%=i%>][1] = "<%=(String) tTransferData.getValueByName("INSUREDNAME")%>";
<%
			}
		}
	}
%> 
	parent.fraInterface.afterChecked("<%=flag%>", paramArray);
</script>
</html>


