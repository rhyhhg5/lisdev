<%
//程序名称：OmnipAnnalsInputInit.jsp
//程序功能：
//创建日期：2009-4-29 18:23:59
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
 <%@page import="java.util.*" pageEncoding="GBK"%> 
 <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  	var operator = "<%=tGI.Operator%>"; //记录当前登录人
	var tCurrentDate = "<%=tCurrentDate%>";
  	var tCurrentTime = "<%=tCurrentTime%>";



// 下拉框的初始化


function initForm()
{
  try
  {
	  	fm.all('ManageCom').value = manageCom;
		initGetModeGrid();
		initElementtype();

  }
  catch(re)
  {
    alert("LLAccModifyInput.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 产品查询信息列表的初始化
function initGetModeGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="团体保单号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[2]=new Array();                                                 
      iArray[2][0]="分单号";         		//列名                     
      iArray[2][1]="70px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();                                                 
      iArray[3][0]="被保险人客户号";         		//列名                     
      iArray[3][1]="60px";            		//列宽                             
      iArray[3][2]=100;            			//列最大值                           
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      iArray[4]=new Array();
      iArray[4][0]="被保险人姓名";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      iArray[5]=new Array();
      iArray[5][0]="性别";         		//列名
      iArray[5][1]="40px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      iArray[6]=new Array();
      iArray[6][0]="出生日期";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      iArray[7]=new Array();
      iArray[7][0]="老年护理金领取年龄";         		//列名
      iArray[7][1]="90px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="老年护理金起领日期";         		//列名
      iArray[8][1]="90px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      iArray[9]=new Array();
      iArray[9][0]="状态";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
              

      GetModeGrid = new MulLineEnter( "fm" , "GetModeGrid" ); 
      //这些属性必须在loadMulLine前
      //GetModeGrid.mulLineCount = 5;   
      GetModeGrid.displayTitle = 1;
      GetModeGrid.locked = 1;
      //GetModeGrid.canSel = 1;
      GetModeGrid.canChk = 1;
      GetModeGrid.hiddenPlus = 1;
      GetModeGrid.hiddenSubtraction = 1;
      GetModeGrid.loadMulLine(iArray);
      //GetModeGrid.selBoxEventFuncName ="queryAll";
  
      
      //这些操作必须在loadMulLine后面
      //ContGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}





</script>