<%
//程序名称：PEdorTypeCCSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("---AP submit---");
  LPPolSchema tLPPolSchema = new LPPolSchema();
  //LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
    
  //后面要执行的动作：添加，修改
  CErrors tError = null;                 
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("---transact: "+transact);
  System.out.println("\nFreeFlag:" + request.getParameter("FreeFlag"));
  System.out.println("FreeRate:" + request.getParameter("FreeRate"));
  
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
  
  //个人保单批改信息
  //tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));
  //tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
  //tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));
	
  //if (transact.equals("INSERT||MAIN")) {
    tLPPolSchema.setPolNo(request.getParameter("PolNo"));
    tLPPolSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPPolSchema.setEdorType(request.getParameter("EdorType"));
    if (request.getParameter("FreeFlag").equals(""))
    {
      tLPPolSchema.setFreeFlag("");
    }
    else 
    {
      tLPPolSchema.setFreeFlag(request.getParameter("FreeFlag"));
    }
    if (request.getParameter("FreeRate").equals(""))
    {
      tLPPolSchema.setFreeRate("-1");
    }
    else
    {
      tLPPolSchema.setFreeRate(request.getParameter("FreeRate"));
    }
    
  //}
  System.out.println("tLPPolSchema:" + tLPPolSchema.encode());
  
  PEdorAPDetailUI tPEdorAPDetailUI = new PEdorAPDetailUI();

  try 
  {
    // 准备传输数据 VData
    VData tVData = new VData();  
  	
    //保存个人保单信息(保全)	
    tVData.addElement(tG);
    //tVData.addElement(tLPEdorMainSchema);
    tVData.addElement(tLPPolSchema);
    
    boolean tag = tPEdorAPDetailUI.submitData(tVData,transact);
    System.out.println("tag is " + tag);
  } 
  catch(Exception ex) 
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "") 
  {
  	tError = tPEdorAPDetailUI.mErrors;
  	if (!tError.needDealError()) 
  	{                          
      Content = " 保存成功";
  		FlagStr = "Success";
  		
  		if (transact.equals("QUERY||MAIN")) 
  		{
    		if (tPEdorAPDetailUI.getResult()!=null && tPEdorAPDetailUI.getResult().size()>0)
    	  {
    			Result = (String)tPEdorAPDetailUI.getResult().get(0);
    		  if (Result==null || Result.trim().equals("")) 
    		  {
    				FlagStr = "Fail"; 
    				Content = "提交失败!!";
    		  }
    	  }
  	  }
    } 
    else  
    {
  		Content = " 保存失败，原因是:" + tError.getFirstError();
  		FlagStr = "Fail";
  	}
	}
	
  //添加各种预处理
  System.out.println("------------Result is------------\n" + Result);
%>   
                   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

