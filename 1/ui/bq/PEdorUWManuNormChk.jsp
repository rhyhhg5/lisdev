<%
//程序名称：PEdorUWManuNornChk.jsp
//程序功能：保全人工核保确认
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.bq.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
 //接收信息，并作校验处理。
	String FlagStr = "Fail";
	String Content = "";
	String transact = "";
	CErrors tError = null;
	boolean flag = true ;

	String tPolNo = request.getParameter("PolNo");
	String tContNo = request.getParameter("ContNo");
	String tEdorNo = request.getParameter("EdorNo");
	String tEdorType = request.getParameter("EdorType");
	String tInsuredNo = request.getParameter("InsuredNo");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
	String tUWIdea = request.getParameter("UWIdea");
	String tUWState= request.getParameter("EdorUWState");
	String tUWDelay = request.getParameter("UWDelay");
	String tAppUser= request.getParameter("UWPopedomCode");
	String UWConculsionType = request.getParameter("UWConculsionType");
	   
	LPUWMasterMainSchema tLPUWMasterMainSchema = new LPUWMasterMainSchema();		
	LPCUWMasterSchema tLPCUWMasterSchema = new LPCUWMasterSchema();		
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	TransferData tTransferData = new TransferData();
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");
  
	if (tUWState == null || tUWState.equals(""))
	{
		Content = "条件录入不完整!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{	    	
		// 准备传输工作流数据 VData
		tLPUWMasterMainSchema.setUWIdea(tUWIdea);
		if(tUWDelay != null && !tUWDelay.equals(""))
		tLPUWMasterMainSchema.setPostponeDay(tUWDelay);
		tLPUWMasterMainSchema.setPassFlag(tUWState);

		tLPCUWMasterSchema.setUWIdea(tUWIdea);
		if(tUWDelay != null && !tUWDelay.equals(""))
		tLPCUWMasterSchema.setPostponeDay(tUWDelay);
		tLPCUWMasterSchema.setPassFlag(tUWState);

		tLPEdorItemSchema.setEdorNo(tEdorNo);
		tLPEdorItemSchema.setContNo(tContNo);
		tLPEdorItemSchema.setInsuredNo(tInsuredNo);
		tLPEdorItemSchema.setPolNo(tPolNo);
		tLPEdorItemSchema.setEdorType(tEdorType);

		// 准备传输工作流数据 VData
		tTransferData.setNameAndValue("EdorNo",tEdorNo);
		tTransferData.setNameAndValue("ContNo",tContNo);
		tTransferData.setNameAndValue("PolNo",tPolNo);
		tTransferData.setNameAndValue("InsuredNo",tInsuredNo);
		tTransferData.setNameAndValue("EdorType",tEdorType);
		tTransferData.setNameAndValue("AppUser",tAppUser);
		tTransferData.setNameAndValue("MissionID",tMissionID);
		tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
		tTransferData.setNameAndValue("LPUWMasterMainSchema",tLPUWMasterMainSchema);
		tTransferData.setNameAndValue("LPEdorItemSchema",tLPEdorItemSchema);
		flag = true;
	}	 	

	if (flag == true)
	{
		// 准备传输数据 VData  
		VData tVData = new VData();   
		tVData.add(tGlobalInput);
		tVData.add(tTransferData);	
		tVData.add(tLPEdorItemSchema);
		tVData.add(tLPCUWMasterSchema);
		tVData.add(tLPUWMasterMainSchema);
		// 数据传输
		System.out.println(UWConculsionType);
		if (UWConculsionType.equals("EdorMain"))
		{
			EdorWorkFlowUI tEdorWorkFlowUI   = new EdorWorkFlowUI();
			if (tEdorWorkFlowUI.submitData(tVData,"0000000010") == false)
			{
				int n = tEdorWorkFlowUI.mErrors.getErrorCount();
				Content = " 保全核保确认失败，原因是:";
				Content = Content + tEdorWorkFlowUI.mErrors.getError(0).errorMessage;
				FlagStr = "Fail";
			}
			else
			{                          
				Content = " 保全核保确认成功! ";
				FlagStr = "Succ";
			}
		}
		else if (UWConculsionType.equals("EdorItem"))
		{
			PEdorItemManuUWUI tPEdorItemManuUWUI   = new PEdorItemManuUWUI();
			if (tPEdorItemManuUWUI.submitData(tVData,"") == false)
			{
				int n = tPEdorItemManuUWUI.mErrors.getErrorCount();
				Content = " 保全项目核保确认失败，原因是:";
				Content = Content + tPEdorItemManuUWUI.mErrors.getError(0).errorMessage;
				FlagStr = "Fail";
			}
			else
			{                          
				Content = " 保全核保确认成功! ";
				FlagStr = "Succ";
			}
		}
		else
		{
			Content = " 保全项目核保确认失败!";
			FlagStr = "Fail";
		}
	}

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>");
</script>
</html>

