<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：CollectivityClientInput.jsp
//程序功能： 
//创建日期：2003-1-7 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="LPTypeIAGrpPersonNew.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LPTypeIAGrpPersonInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LPTypeIAGrpPersonSave.jsp" method=post name=fm target="fraSubmit">
    <table>
         <td >
         <Input class= common type=Button value="保 存" onclick="insertClick()">
         </td>
         <td >
         <Input class= common type=Button value="返 回" onclick="returnParent()">      
         </td>
         <td >
         <Input class= common type=Button value="关 闭" onclick="closeCurrentWindow()">      
         </td>
    </table>

        <!-- 显示或隐藏CollectivityClient1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCollectivityClient1);">
      </td>
      <td class= titleImg>
        集体客户信息 
      </td>
    	</tr>
    </table>
    <Div  id= "divCollectivityClient1" style= "display: ''">
      <table  class= common>
         <TR  class= common>
          <TD  class= title>
            单位编码 
          </TD>
          <TD  class= input>
            <Input class="readonly" name=GrpNo readonly > 
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            密码 
          </TD>
          <TD  class= input>
            <Input class= common name=Password > 
          </TD>
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input class= common name=GrpName verify="单位名称|NOTNULL">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单位地址编码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpAddressCode >
          </TD>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input>
            <Input class= common name=GrpAddress >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单位邮编
          </TD>
          <TD  class= input>
            <Input class= common name=GrpZipCode >
          </TD>
          <TD  class= title>
            行业分类
          </TD>
          <TD  class= input>
            <Input class="code" name=BusinessType ondblclick="return showCodeList('BusinessType',[this]);" onkeyup="return showCodeListKey('BusinessType',[this]);">                                                                                                        
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单位性质
          </TD>
          <TD  class= input>
            <Input class="code" name=GrpNature ondblclick="return showCodeList('GrpNature',[this]);" onkeyup="return showCodeListKey('GrpNature',[this]);">                                                                                  
          </TD>
          <TD  class= title>
            总人数
          </TD>
          <TD  class= input>
            <Input class= common name=Peoples >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            注册资本
          </TD>
          <TD  class= input>
            <Input class= common name=RgtMoney >
          </TD>
          <TD  class= title>
            资产总额
          </TD>
          <TD  class= input>
            <Input class= common name=Asset >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            净资产收益率
          </TD>
          <TD  class= input>
            <Input class= common name=NetProfitRate >
          </TD>
          <TD  class= title>
            主营业务
          </TD>
          <TD  class= input>
            <Input class= common name=MainBussiness >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            法人
          </TD>
          <TD  class= input>
            <Input class= common name=Corporation >
          </TD>
          <TD  class= title>
            机构分布区域
          </TD>
          <TD  class= input>
            <Input class= common name=ComAera >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系人1
          </TD>
          <TD  class= input>
            <Input class= common name=LinkMan1 >
          </TD>
          <TD  class= title>
            部门1
          </TD>
          <TD  class= input>
            <Input class= common name=Department1 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            职务1
          </TD>
          <TD  class= input>
            <Input class= common name=HeadShip1 >
          </TD>
          <TD  class= title>
            联系电话1
          </TD>
          <TD  class= input>
            <Input class= common name=Phone1 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            E_Mail1
          </TD>
          <TD  class= input>
            <Input class= common name=E_Mail1 >
          </TD>
          <TD  class= title>
            传真1
          </TD>
          <TD  class= input>
            <Input class= common name=Fax1 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系人2
          </TD>
          <TD  class= input>
            <Input class= common name=LinkMan2 >
          </TD>
          <TD  class= title>
            部门2
          </TD>
          <TD  class= input>
            <Input class= common name=Department2 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            职务2
          </TD>
          <TD  class= input>
            <Input class= common name=HeadShip2 >
          </TD>
          <TD  class= title>
            联系电话2
          </TD>
          <TD  class= input>
            <Input class= common name=Phone2 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            E_Mail2
          </TD>
          <TD  class= input>
            <Input class= common name=E_Mail2 >
          </TD>
          <TD  class= title>
            传真2
          </TD>
          <TD  class= input>
            <Input class= common name=Fax2 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单位传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax >
          </TD>
          <TD  class= title>
            单位电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone >
          </TD>
        </TR>      
        <TR  class= common>
          <TD  class= title>
            付款方式
          </TD>
          <TD  class= input>
            <Input class= common name=GetFlag >
          </TD>
          <TD  class= title>
            负责人
          </TD>
          <TD  class= input>
            <Input class= common name=Satrap >
          </TD>
        </TR>       
        <TR  class= common>
          <TD  class= title>
            公司e_mail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
          <TD  class= title>
            成立日期
          </TD>
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="FoundDate" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            银行编码
          </TD>
          <TD  class= input>
            <Input class= common name=BankCode >
          </TD>
          <TD  class= title>
            银行帐号
          </TD>
          <TD  class= input>
            <Input class= common name=BankAccNo >
          </TD>
        </TR>
        <TR  class= common>          
          <TD  class= title>
            客户组号码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpGroupNo >
          </TD>
          <TD  class= title>
            状态
          </TD>
          <TD  class= input>
            <Input class= common name=State >
          </TD>
        </TR>
        <TR  class= common>          
          <TD  class= title>
            备注
          </TD>
          <TD  class= input>
            <Input class= common name=Remark >
          </TD>
          <TD  class= title>
            黑名单标记
          </TD>
          <TD  class= input>
            <Input class= common name=BlacklistFlag >
          </TD>
        </TR>
            <input type=hidden name=Transact >
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
