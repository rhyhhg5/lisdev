<%
//GEdorTypeACInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类--> 

<script language="JavaScript">  
	var cancelCount = 0;
	var zh = new Array();
//单击时查询

function reportDetailClick(parm1,parm2)
{
  var ex,ey;
  ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  divLPAppntGrpDetail.style.left=ex;
  divLPAppntGrpDetail.style.top =ey;
  detailQueryClick();
}

function initInpBox()
{      
  fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
  fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
  fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
  fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
  fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
  fm.all('ContType').value ='2';
  
  try {fm.all('AppntNo').value= ''; } catch(ex) { }; //客户号码
  try {fm.all('Peoples2').value= ''; } catch(ex) { }; //投保总人数
  try {fm.all('GrpName').value= ''; } catch(ex) { }; //单位名称
  try {fm.all('SignCom').value= ''; } catch(ex) { }; //签单机构
  try {fm.all('SignDate').value= ''; } catch(ex) { }; //签单日期
  try {fm.all('CValiDate').value= ''; } catch(ex) { }; //保单生效日期
  try {fm.all('Prem').value= ''; } catch(ex) { }; //总保费
  try {fm.all('Amnt').value= ''; } catch(ex) { }; //总保额
  
  setCTReason();
}                                   

//显示退保原因
function setCTReason()
{
  var sql = "  select a.reasonCode, b.codeName "
            + "from LPGrpEdorItem a, LDCode b "
            + "where a.reasonCode = b.code "
            + "   and b.codeType = 'reason_tb' "
            + "   and edorNo = '" + fm.EdorNo.value + "' "
            + "   and edorType = '" + fm.EdorType.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    fm.reason_tb.value = result[0][0];
    fm.reason_tbName.value = result[0][1];
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initQuery();  
    initLCGrpPolGrid();
    queryLCGrpPolGrid(); 
    //checkSelectContPlanRisk();
    //showOneCodeName("EdorCode", "EdorTypeName"); 
    if(cancelCount == 0)
    {
      initElementtype();
      cancelCount = cancelCount + 1;
    }
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

function initLCGrpPolGrid()
{
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保障计划";         		//列名
    iArray[1][1]="30px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="险种序号";         		//列名
    iArray[2][1]="30px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21] = "riskSeqNo";
    
    iArray[3]=new Array();
    iArray[3][0]="险种代码";         		//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21] = "riskCode";
    
    iArray[4]=new Array();
    iArray[4][0]="险种名称";         		//列名
    iArray[4][1]="150px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21] = "riskName";
    
    iArray[5]=new Array();
    iArray[5][0]="被保人数";         		//列名
    iArray[5][1]="30px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21] = "insurdCount";
    
    iArray[6]=new Array();
    iArray[6][0]="总保费";         		//列名
    iArray[6][1]="40px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21] = "prem";
    
    iArray[7]=new Array();
    iArray[7][0]="集体投保单号";         		//列名
    iArray[7][1]="20px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21] = "proposalGrpContNo";
    
    iArray[8]=new Array();
    iArray[8][0]="主险险种编码";         		//列名
    iArray[8][1]="20px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21] = "mainRiskCode";
    
    iArray[9]=new Array();
    iArray[9][0]="计划类别";         		//列名
    iArray[9][1]="20px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
    LCGrpPolGrid = new MulLineEnter( "fm" , "LCGrpPolGrid" ); 
    //这些属性必须在loadMulLine前
    LCGrpPolGrid.mulLineCount = 0;
    LCGrpPolGrid.displayTitle = 1;
    LCGrpPolGrid.locked = 1;
    LCGrpPolGrid.canSel = 0;
    LCGrpPolGrid.canChk = 1;
    LCGrpPolGrid.hiddenSubtraction = 1;
    LCGrpPolGrid.hiddenPlus = 1;
    LCGrpPolGrid.chkBoxEventFuncName  = "calLeavingPrem";
    LCGrpPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
function initQuery()
{	
	getGrpCont();
}
</script>