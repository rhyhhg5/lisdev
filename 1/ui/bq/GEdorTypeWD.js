var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 

//磁盘导入 
function importInsured()
{
	var url = "./BqDiskImportMain.jsp?EdorNo=" + fm.all("EdorNo").value + 
	          "&EdorType=" + fm.all("EdorType").value +
	          "&GrpContNo=" + fm.all("GrpContNo").value;
	var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
	window.open(url, "无名单实名化导入", param);
}

//查询保障计划
function queryPlan()
{
  var planUrl = "../app/ContPlan.jsp?GrpContNo=" + fm.all("GrpContNo").value + 
            "&LoadFlag=3&ContType=4";
  window.open(planUrl);
}

//初始化险种信息和已录入信息
function initQuery()
{	
  var sql = "select SerialNo, State, InsuredName, Sex, Birthday, IdType, IDNo, ContPlanCode, " +
           "    OccupationType, BankCode, AccName, BankAccno, EdorValiDate, OthIdNo " +
           "from LPDiskImport " +
           "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
           "and EdorNo = '" + fm.all("EdorNo").value + "' " +
           "and State = '1' " +
           "order by Int(SerialNo) ";
	turnPage.queryModal(sql, InsuredListGrid); 
}

//查询导入无效的数据
function queryFailData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, BankCode,AccName,BankAccNo, ErrorReason " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '0' " +
	             "order by Int(SerialNo) ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, FailGrid); 
}

//查询按钮
function queryClick()
{
	var sql = "select a.ContNo, a.InsuredNo, a.Name, a.Sex, a.Birthday, " +
	     " a.IDType, a.IDNo, a.OccupationCode, a.OccupationType " +
	     "from LCInsured a, LCCont b " +
	     "where a.ContNo = b.ContNo " +
	     "and b.PolType <> '1' " +
		   "and not exists (select * from LPDiskImport " +
		   "    where InsuredNo = a.InsuredNo " +
		   "    and GrpContNo = '" + fm.GrpContNo.value + "' " +
		   "    and EdorType = '" + fm.EdorType.value + "' " +
		   "    and EdorNo = '" + fm.EdorNo.value + "')" +
		   "and b.AppFlag = '1' " +
		   "and a.GrpContNo = '" + fm.GrpContNo.value + "' " +
		   getWherePart('a.InsuredNo', 'InsuredNo1') +                  
		   getWherePart('a.Name', 'Name', 'like', '0') +
		   getWherePart('a.IdNo', 'IdNo') + 
		   getWherePart('a.OthIdNo', 'OthIdNo') + 
		   "order by InsuredNo"; +
	turnPage.queryModal(sql, LCInsuredGrid);
}

//数据提交后的操作
function afterSubmit(flag, content, operator)
{
  try 
  { 
    showInfo.close();
	  window.focus();
	}
	catch (ex) {}
	if (operator == "ADD" || operator == "DEL")
	{
		initForm();
	}
	else if (operator == "SAVE")
	{
		if (flag == "Fail")
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
			showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
			initForm();
		}
		else
		{
			var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			returnParent();
		}
	}
}

function addEdor()
{
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "GEdorTypeWDSubmit.jsp?Operator=ADD";
	fm.submit();
}

function delEdor()
{
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "GEdorTypeWDSubmit.jsp?Operator=DEL";
	fm.submit();
}


//保存申请
function saveEdor()
{
	if (InsuredListGrid.mulLineCount == 0)
	{
		alert("没有有效的导入数据！");
		return false;
	}
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "GEdorTypeWDSubmit.jsp?Operator=SAVE";
	fm.submit();
}

//返回父窗口
function returnParent()
{
	try
	{
		top.opener.getGrpEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {};
}