<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListLP.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  String outname = "";
  String outpathname = "";
  XmlExport txmlExport = null;   
    
  String customerNo = request.getParameter("CustomerNo");
  String contType = request.getParameter("ContType");
  LCAppAccSchema mLCAppAccSchema = new LCAppAccSchema();
  mLCAppAccSchema.setCustomerNo(customerNo);
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  VData tVData = new VData();
  tVData.add(mLCAppAccSchema);
  tVData.add(tG);
  
  
  PrtAppAccListUI tPrtAppAccListUI = new PrtAppAccListUI(); 
  if(!tPrtAppAccListUI.submitData(tVData,contType))
  {          
     	operFlag = false;
     	Content = tPrtAppAccListUI.mErrors.getFirstError(); 
  }
  else
  {             
    VData mResult = tPrtAppAccListUI.getResult();			
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    
    if(txmlExport==null)
    {
    	operFlag=false;
    	Content="没有得到要显示的数据文件";	  
    }
  }
	
	if (operFlag==true)
	{
//	  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
//    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
//    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
//    tcombineVts.output(dataStream);
//    session.putValue("PrintVts", dataStream);
	  
//		session.putValue("PrintStream", txmlExport.getInputStream());
//		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");

	          Readhtml rh=new Readhtml();
	          System.out.println(txmlExport.getInputStream());
			  rh.XmlParse(txmlExport.getInputStream()); //相当于XmlExport.getInputStream();
			  
			  String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
			  String temp=realpath.substring(realpath.length()-1,realpath.length());
			  if(!temp.equals("/"))
			  {
				  realpath=realpath+"/"; 
			  }
			  String templatename=rh.getTempLateName();//模板名字
			  String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
			  System.out.println("*********************templatepathname= " + templatepathname);
			  System.out.println("************************realpath="+realpath);
			  String date=PubFun.getCurrentDate().replaceAll("-","");
			  String time=PubFun.getCurrentTime().replaceAll(":","");
			  outname=tG.Operator+date+time+".xls";
			  outpathname=realpath+"vtsfile/"+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
				System.out.println("outpathname"+outpathname);
				System.out.println("templatepathname为空？"+templatepathname);
				System.out.println("**++**"+realpath+"f1print/picctemplate/");
				System.out.println("templatename："+templatename);
			  rh.setReadFileAddress(templatepathname);
			  rh.setWriteFileAddress(outpathname);
			  rh.start("vtsmuch");
			  
	}
	else
	{
    	FlagStr = "Fail";
  	}
%>
<a href="../f1print/download.jsp?filename=<%=outname%>&filenamepath=<%=outpathname%>">点击下载</a>