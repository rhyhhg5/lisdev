//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeCCReturn()
{  
	top.close();
}


function verify() {
  if (trim(fm.BankCode.value)=="0101") {
    if (trim(fm.BankAccNo.value).length!=19 || !isInteger(trim(fm.BankAccNo.value))) {
      alert("工商银行的账号必须是19位的数字，最后一个星号（*）不要！");
      return false;
    }
  }
  // 20170424 lichang 校验账户名称是否相同
  var strSQL2 = "  select appntname from lccont where contno = '"+fm.ContNo.value+"' ";
  var arrResult2 = easyExecSql(strSQL2);
  if(trim(fm.AccName.value) != arrResult2){
	  alert("修改的银行账户名与投保人不同，请重新录入！");
      return false;
  }
  return true;
}

function beforeSubmit()
{
  if (fm.PayMode.value == "4" || fm.PayMode.value == "6")
  {
    if (fm.BankCode.value == "")
    {
      alert("请输入转帐银行！");
      fm.BankCode.focus();
      return false
    }
    if (fm.BankAccNo.value == "")
    {
      alert("请输入转帐帐号！");
      fm.BankAccNo.focus();
      return false
    }
    if (fm.AccName.value == "")
    {
      alert("请输入帐户名！");
      fm.AccName.focus();
      return false
    }
  }
  if (!verifyInput2())
  {
    return false;
  }
  if (!verify()) 
  {
    return false;
  }
  return true;
}

function edorTypeCCSave()
{
  if (!beforeSubmit())
  {
    return false;
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}


//提交，保存按钮对应操作
function submitForm()
{
	
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
  try
  {
    showInfo.close();
    window.focus();
  }
  catch(ex) {}
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  {
  	var tTransact=fm.all('fmtransact').value;
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		returnParent();
  }
}

function returnParent()
{ 
	  try
	  {
		  top.opener.getEdorItem();
	  }
	  catch(ex) { }
	  
	  top.opener.focus();
		top.close();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
        
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];


		// 查询保单明细
		queryInsuredDetail();
	}
}

function queryInsuredDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	//alert(tEdorNo);
	tEdorType=fm.all('EdorType').value;
	//alert(tEdorType);
	tPolNo=fm.all('PolNo').value;
	//alert(tPolNo);
	tCustomerNo = fm.all('CustomerNo').value;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	parent.fraInterface.fm.action = "./InsuredQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorTypeBBSubmit.jsp";
}


function afterCodeSelect(cCodeName, Field)
{
	if (cCodeName == "PayMode")
	{
	/*	if (fm.PayMode.value == "4")
		{
			document.all("TrBank").style.display = "";
		}
		else
		{
			document.all("TrBank").style.display = "none";
		}
	*/
		if((fm.hPayIntv.value =='1'||fm.hPayIntv.value =='3'||fm.hPayIntv.value =='6')&& Field.value !='4')
		{
			alert("月交、季交和半年交件只能选择银行转账方式");
			return;
		}
		if(Field.value=='4')
		{
			DivPayMode.style.display='';
		}
		else
		{
			DivPayMode.style.display='none';
			fm.BankCode.value='';
			fm.BankAccNo.value='';
			fm.AccName.value='';
			fm.BankCodeName.value='';
		}
	}
}

function getContDetail()
{
	  var isModify = true;
	  var strSQL = "select appntno,appntname,cvalidate,paytodate,prem,amnt,"
	         +"case PayIntv when 0 then '趸交' when 1 then '月交' when 3 then '季交' when 6 then '半年交' when 12 then '年交' when -1 then '不定期' end,"
	         +" paymode,bankcode,bankaccno,accname,payintv,managecom from  lpcont where contNo='"+fm.ContNo.value  + "' and edortype='CC' and edorno='"+fm.EdorNo.value+"'";
    arrResult = easyExecSql(strSQL,1,0);
    

    if(arrResult == null)
    {
    	isModify = false;
		  var strSQL = "select appntno,appntname,cvalidate,paytodate,prem,amnt,"
		         +"case PayIntv when 0 then '趸交' when 1 then '月交' when 3 then '季交' when 6 then '半年交' when 12 then '年交' when -1 then '不定期' end,"
		         +" paymode,bankcode,bankaccno,accname,payintv,managecom from  lccont where contNo='"+fm.ContNo.value  + "'";
	    arrResult = easyExecSql(strSQL,1,0);
	  }
		if( arrResult != null )
		{
			fm.AppntNo.value = arrResult[0][0];
			fm.AppntName.value = arrResult[0][1];
			fm.CValidate.value = arrResult[0][2];
			fm.PayToDate.value = arrResult[0][3];
			fm.Prem.value = arrResult[0][4];
			fm.Amnt.value = arrResult[0][5];
			//fm.PayIntv.value = arrResult[0][6];
			fm.PayMode.value = arrResult[0][7];
			fm.BankCode.value = arrResult[0][8];
			fm.BankAccNo.value = arrResult[0][9];
			fm.AccName.value = arrResult[0][10];
			fm.hPayIntv.value = arrResult[0][11];
			fm.ManageCom.value = arrResult[0][12];
	/*	if (fm.PayMode.value == "4")
			{
				document.all("TrBank").style.display = "";
			}
			else
			{
				document.all("TrBank").style.display = "none";
			}
	*/
		}
}

function afterQuery( arrReturn )
{
	fm.all('BankCode').value = arrReturn[0];
	fm.all('BankCodeName').value = arrReturn[1];	
}
function querybank()
{
	showInfo = window.open("LDBankQueryMain.jsp");
}