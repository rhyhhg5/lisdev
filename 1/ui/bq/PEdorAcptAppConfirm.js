//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量    
var targetWin = null;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initPEdorAcptAppConfirmGrid();
  // showSubmitFrame(mDebug);
  fm.submit(); //提交
}

function  edorAcptAppConfirm()
{
	//fm.all("EdorNo").value = PEdorAppConfirmGrid.getRowColData(0,1);
	//OtherNo在打印保全交费表时使用
	//fm.all("OtherNo").value = PEdorAppConfirmGrid.getRowColData(0,1);
	fm.all("fmtransact").value = "INSERT||EDORAPPCONFIRM";
	
	if (window.confirm("是否确认本次申请?"))
	{
		
		//alert("申请确认中...");
		var showStr="正在计算数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		//showSubmitFrame(mDebug);
		fm.submit();
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,prtParams )
{
  //alert("prtParams : " + prtParams);
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    easyQueryClick();
    
    if (fm.all('EdorAcceptNo').value==''||fm.all('EdorAcceptNo').value==null)
    {
        alert('保全受理号错误!');
        return;
    }
    if (fm.all('fmtransact').value != 'EDORACPTCONFIRM')
    { 
    	if (window.confirm("是否查看本次保全变更结果?"))
    	{
    	    PrtEdor();
        }
        
        
        var strSQL = "";
	    strSQL = "select SumDuePayMoney from LJSPay where 1=1 and OtherNo='"+ fm.all('EdorAcceptNo').value +"' ";
        
        //alert(strSQL); 
        
	    var strQueryResult  = easyExecSql(strSQL);
        //判断是否查询成功
        if (strQueryResult!=null) //需要交费
        {
            //if (window.confirm("是否打印本次费用通知书?"))
            //{
        	//    PrePrtPay(prtParams);
            //}
        }
        else //不需要交费
        {
            if (window.confirm("是否确认本次保全?"))
            {
        	    EdorAcptConfirm();
            }
        }

    }
    else
    {
        if (window.confirm("是否打印批单?"))
    	{
    	    PrtEdor();
        }
        //var strSQL = "";
	    //strSQL = "select SumActuPayMoney from LJAPay where 1=1 and IncomeNo='"+ fm.all('EdorAcceptNo').value +"' ";
        //    
        //    //alert(strSQL); 
        //    
	    //var strQueryResult  = easyExecSql(strSQL);
        //    //判断是否查询成功
        //if (strQueryResult!=null) //需要退费
        //if (window.confirm("是否打印给付凭证?"))
    	//{
    	//    PrtEdor();
        //}
    }     
        

//    top.close();
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

// 查询按钮
function easyQueryClick()
{
	var EdorState;
	
	tEdorState=top.opener.fm.all('EdorState').value;
		
	// 初始化表格
	initPEdorAcptAppConfirmGrid();
	
	var tReturn = parseManageComLimit(); 
	//alert(tReturn);

	// 书写SQL语句,GetMoney
	var strSQL = "";
	strSQL = "select EdorNo,ContNo,EdorType,EdorValiDate,EdorAppDate from LPEdorItem where EdorState='"+ tEdorState//+" and"+tReturn
				 +"' "+ getWherePart( 'EdorAcceptNo' )
  
//    alert(strSQL);
  
	//execEasyQuery( strSQL );
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
//    alert(turnPage.strQueryResult);
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PEdorAcptAppConfirmGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	
}

// 数据返回父窗口
function returnParent()
{

	top.close();
		
}

//打印批单
function PrtEdor(prtParams)
{

	if (fm.all("EdorAcceptNo").value == ""||fm.all("EdorAcceptNo").value ==null) {
		alert("由于数据出错，无法打印批单。");
		return;
	}
	else
	{
	    window.open("../f1print/AppEndorsementF1PJ1.jsp?EdorAcceptNo="+fm.all("EdorAcceptNo").value+"");
	}
}

//打印保全交费
function PrePrtPay(prtParams)
{
    /**fm.all('prtParams').value = prtParams;
    parent.fraInterface.fm.action = "../f1print/EdorFeeF1PSave.jsp";
    fm.all('fmtransact').value = 'PRINT';
    parent.fraInterface.fm.target="fraSubmit";
    fm.submit();*/
    if (fm.all('EdorAcceptNo').value==''||fm.all('EdorAcceptNo').value==null)
        {
            alert('保全受理号错误!');
            return;
        }
        
    parent.fraInterface.fm.action = "../f1print/EdorAppFeeF1PSave.jsp?OtherNo='"+fm.all('EdorAcceptNo').value+"'";
    fm.all('fmtransact').value = 'PRINT';
    parent.fraInterface.fm.target="fraSubmit";
    fm.submit();    
    parent.fraInterface.fm.action = "./PEdorAppConfirmSubmit.jsp";
    parent.fraInterface.fm.target="fraSubmit";
 

}

//保全确认
function EdorAcptConfirm()
{
    parent.fraInterface.fm.action = "./PEdorAcptConfirmSubmit.jsp";
    fm.all('fmtransact').value = "EDORACPTCONFIRM";
    parent.fraInterface.fm.target="fraSubmit";
    fm.submit();    
    parent.fraInterface.fm.action = "./PEdorAppConfirmSubmit.jsp";
    parent.fraInterface.fm.target="fraSubmit";
     

}

function PrtPay(GetNoticeNo,AppntNo,SumdeupayMoney)
{

    fm.all('GetNoticeNo').value = GetNoticeNo;
    fm.all('AppntNo').value = AppntNo;
    fm.all('SumDuePayMoney').value = SumdeupayMoney;
/*    
    var urlStr="../f1print/EdorFeeF1PSave.jsp?OtherNo=" + fm.all("OtherNo").value
       +"&GetNoticeNo=" + fm.all("GetNoticeNo").value
       + "&AppntNo=" + fm.all("AppntNo").value
       +"&SumDuePayMoney=" + fm.all("SumDuePayMoney").value
       +"&fmtransact=" + fm.all("fmtransact").value;  

    alert(urlStr);
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:950px;dialogHeight:600px");   	

*/
    parent.fraInterface.fm.action = "../f1print/EdorFeeF1PSave.jsp";
    parent.fraInterface.fm.target="f1print2";
    fm.submit();
    parent.fraInterface.fm.action = "./PEdorAppConfirmSubmit.jsp";
    parent.fraInterface.fm.target="fraSubmit";
   
}

function QueryUWDetailClick()
{
  //var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  var cEdorNo = fm.all("EdorNo").value;
  var cContNo = fm.all("ContNo").value;		
  if (cContNo != "" && cEdorNo !="" )
  {
  	window.open("./PEdorAppUWErrMain.jsp?ProposalNo1="+cContNo+"&EdorNo="+cEdorNo,"window1");
  	//showInfo.close();
  }
  else
  {
  	//showInfo.close();
  	alert("请先进行申请确认!");  
  }
	
}