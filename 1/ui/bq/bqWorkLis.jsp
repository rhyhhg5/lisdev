<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/CurrentTime.jsp"%>
<html>    
<%
//程序名称：bqWorkLis.jsp
//程序功能：保单管理业务清单
//创建日期：2005-10-10
//创建人  ：yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   
<script>

	var comCode = "<%=tG.ComCode%>";
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="bqWorkLis.js"></SCRIPT> 

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>保单管理业务清单</title>
</head>
		<body>   
			<table>
					<tr> 
					<td class= common> 
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divbqWorkLis);"> 
					</td>
					<td class= titleImg>保单管理业务清单</td>
					</tr>
			</table>
					<Div  id= "divbqWorkLis" style= "display: ''" >  
					<form action="./bqWorkLisSub.jsp" method=post name=fm target="f1print">
						<INPUT VALUE="查  询" class="cssButton" TYPE="button" onclick="return chenkDate();submitForm();">&nbsp;
						<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="return chenkDate();submitForm();">&nbsp;
						
								<table class= common border=0 width=100%>
								<td  class= title>操作类型</td>
								<td  class= input colspan=5>
								<input class="code" name="operateType" type="hidden" value="">
								<input class="code" name="operateTypeName" readonly CodeData="0|^1|受理件^0|经办件" ondblclick="return showCodeListEx('operateType1', [this,operateType], [1,0]);" onkeyup="return showCodeListKey('operateType1', [this,operateType],[1,0]);">
								</td>								
					  </TR> 
		        
		         <TR  class= common> 
								
								<TD class= title> 机构 </TD>
								<TD class= input>
								<input class="code" name="organcode" type="hidden">
								<input class="code" name="organname" readonly ondblclick="return showCodeList('comcode', [this,organcode], [1,0]);" onkeyup="return showCodeListKey('comcode', [this,organcode], [1,0]);" verify="机构|&code:comcode">
								</TD>
                 <TD class= title> 业务性质 </TD>
						 <TD class= input>
								<input class="code" name="StatusKindNo" type="hidden">
								<input class="code" name="StatusKindName" readonly ondblclick="return showCodeList('StatusKind', [this,StatusKindNo], [1,0]);" onkeyup="return showCodeListKey('StatusKind', [this,StatusKindNo], [1,0]);">
						 </TD>
						 <TD class= title> 业务类型 </TD>
						 <TD class= input>
								<input class="code" name="TypeNo" type="hidden">
								<input class="code" name="TypeNoname" readonly ondblclick="return showCodeList('TaskTypeNo', [this,TypeNo], [1,0], null, '03', 'SuperTypeNo', 1);" onkeyup="return showCodeListKey('TaskTypeNo', [this,TypeNo], [1,0], null, '03', 'SuperTypeNo', 1);">
						 </TD>
						</TR>
						<TR class=common>
								<TD class= title> 小组 </TD>
								<TD class= input>
								<input class="code" name="Groupno" type="hidden">
								<input class="code" name="Groupname"  readonly ondblclick="return showCodeList('acceptcom', [this,Groupno], [1,0]);" onkeyup="return showCodeListKey('acceptcom', [this,Groupno], [1,0]);" verify="受理机构|&code:acceptcom">
								</TD>
								<TD class= title> 组 员 </TD>
								<TD class= input>
								<input class="code" name="usercode" type="hidden">
								<input class="code" name="username" readonly ondblclick="return showCodeList('groupmember', [this,usercode], [1,0],null,fm.all('Groupno').value, 'Groupno', 1);" onkeyup="return showCodeListKey('groupmember', [this,usercode], [1,0],null,fm.all('Groupno').value, 'Groupno', 1);" >
 								
		         
		      	  	<TD class= title> 业务状态 </TD>
								<TD class= input>
								<input class="code" name="Statuscode" type="hidden">
								<input class="code" name="Statusname" readonly ondblclick="return showCodeList('TaskStatusNo', [this,Statuscode], [1,0]);" onkeyup="return showCodeListKey('TaskStatusNo', [this,Statuscode], [1,0]);" verify="机构|&code:TaskStatusNo">
								</TD>
								</TR>
		       
		      	<TR  class= common>	
								<TD class= title>保全状态 </TD>
								<TD class= input >
								<input class="code" name="bqStatecode" type="hidden">
								<input class="code" name="bqStatename" readonly ondblclick="return showCodeList('appedorstate', [this,bqStatecode],[1,0]);" onkeyup="return showCodeListKey('appedorstate', [this,bqStatecode], [1,0]);" verify="保全状态|&code:appedorstate">
								</TD> 

								<td  class= title> 排序方式 </td>
								<td  class= input colspan=5>
								<input class="code" name="compositorcode" type="hidden" value="">
								<input class="code" name="compositorname" readonly CodeData="0|^0|受理号|^1|受理机构|^2|受理人|^3|经办人|^4|结案日期" ondblclick="return showCodeListEx('compositorcode1', [this,compositorcode], [1,0]);" onkeyup="return showCodeListKey('compositorcode1', [this,compositorcode1],[1,0]);">
								</td>



						</TR>		
						<TR  class= common>	
								<TD  class= title>统计起期</TD>
								<TD  class= input> 
								<Input name=StartDate  style="width:160" class='coolDatePicker' dateFormat='short' verify="统计起期|Date" > </TD> 
								<TD  class= title>统计止期</TD>
								<TD  class= input> 
								<Input name=EndDate style="width:160" class='coolDatePicker' dateFormat='short' verify="统计止期|Date" > </TD> 
								 <TD colspan=2> 
    						 <INPUT VALUE="重 置" TYPE=button style="width:100"Class="cssButton" name="query" onclick="lcReload();">  
   							 </TD>
						</TR>
						<TR>
								<TD  class= title>说  明</TD>
								<TD colspan="6"  class= input>
								<textarea class="common" name="Remark"  cols="90%" rows="2"></textarea> 
								</TD>
		        </TR>		        
						</table>
				<input type="hidden" name=op value="">
			</form>
		</Div>  
		


  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 