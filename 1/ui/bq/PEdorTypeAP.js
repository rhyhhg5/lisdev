//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeAPSave()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result)
{ 
  try { showInfo.close(); } catch (e) {}
  
  var tTransact=fm.all('fmtransact').value;
	if (tTransact=="QUERY||MAIN")
	{
		var tArr = decodeEasyQueryResult(Result, 0);
		
		fm.all('RiskCode').value = tArr[0][11];
		fm.all('InsuredNo').value = tArr[0][20];
		fm.all('InsuredName').value = tArr[0][21];
		fm.all('CValidate').value = tArr[0][31];
		fm.all('PayToDate').value = tArr[0][36];
		fm.all('Prem').value = tArr[0][42];
		fm.all('Amnt').value = tArr[0][45];
		
    //[117,118];
    fm.all("LPFreeFlag").value = tArr[0][117];
    fm.all("LPFreeRate").value = tArr[0][118];
	}
	else
	{  
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
}

function returnParent()
{
	top.close();
}



