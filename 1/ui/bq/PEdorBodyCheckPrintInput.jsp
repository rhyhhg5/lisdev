<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="PEdorBodyCheckPrintInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PEdorBodyCheckPrintInit.jsp"%>
  <title>打印体检通知书 </title>   
</head>
<body  onload="initForm();" >
  <form action="./PEdorBodyCheckPrintQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
        <tr  class= common>
          <td  class= title>体检通知书号</td>
          <td  class= input>  
            <Input class= common name=PrtNo > 
          </td>
          <td  class= title>保全受理号</td>
          <td  class= input>  
            <Input class= common name=EdorNo >
          </td>
          <td  class= title>被保人客户号</td>
          <td  class= input>  
            <Input class= common name=InsuredNo >  
          </td>   
        </tr> 
        <tr  class= common>
          <td  class= title>管理机构</td>
          <td  class= input>  
            <Input class="codeNo" name=ManageCom ondblclick="return showCodeList('station',[this,ManageComName], [0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName], [0,1]);"><Input class="codeName" name=ManageComName readonly >
          </td> 
          <td  class= title>打印状态</td>
          <td  class= input>  
            <Input class="codeNo" name=HealthPrintFlag CodeData="0|^1|未打印^2|已打印" value="1" ondblclick="return showCodeListEx('HealthPrintFlag',[this,HealthPrintFlagName], [0,1]);" onkeyup="return showCodeListEx('HealthPrintFlag',[this,HealthPrintFlagName], [0,1]);"><Input class="codeName" name=HealthPrintFlagName readonly value="未打印">   
          </td>
        </tr>     
    </table>
    <input value="查  询" class= cssButton type=button onclick="queryHealthPrint();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHealth);">
    		</td>
    		<td class= titleImg>
    			 体检通知书信息
    		</td>
    	</tr>
    </table>
  	<div id="divHealth" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanHealthGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<div id= "divPage" align=center style= "display: 'none' ">
  			<input value="首  页" class=cssButton type=button onclick="getFirstPage();"> 
        <input value="上一页" class=cssButton type=button onclick="getPreviousPage();"> 					
        <input value="下一页" class=cssButton type=button onclick="getNextPage();"> 
        <input value="尾  页" class=cssButton type=button onclick="getLastPage();"> 
      </div>					
  	</div>
  	<p>
      <input value="打印保全体检通知书" class=cssButton type=button onclick="printNotice();"> 
  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
