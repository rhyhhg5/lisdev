<%
//程序名称：PEdorTypeRFSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
    
  //后面要执行的动作：添加，修改
  CErrors tError = null;                 
  String FlagStr = "";
  String Content = "";
  String transact = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("---transact: " + transact);  
  
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");

	// 准备传输数据 VData		
	VData tVData = new VData();
	tVData.add(tG);
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("EdorNo", edorNo);
	tTransferData.setNameAndValue("EdorType", edorType);
	tVData.add(tTransferData);
  
  PEdorRFDetailUI tPEdorRFDetailUI = new PEdorRFDetailUI();

  try 
  {    
    if (!tPEdorRFDetailUI.submitData(tVData,transact)) 
    {
      FlagStr = "Fail";
      Content = "操作失败!!";
    }
  } 
  catch(Exception ex) 
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "") 
  {
  	tError = tPEdorRFDetailUI.mErrors;
  	if (!tError.needDealError()) 
  	{                          
      Content = "保存成功";
  		FlagStr = "Success";
    } 
    else  
    {
  		Content = " 保存失败，原因是:" + tError.getFirstError();
  		FlagStr = "Fail";
  	}
	}
%>   
                   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>

