<%
//程序名称：
//程序功能：
//创建日期：2006-10-30 20:22
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
%>                         
<%
	String edorAcceptNo = request.getParameter("edorAcceptNo");
	String actuGetNo = request.getParameter("actuGetNo");

%>
<script language="JavaScript">  
//初始化输入框

function initForm()
{
  try
  {
  	fm.edorAcceptNo.value = <%=edorAcceptNo%>;
		fm.actuGetNo.value = <%=actuGetNo%>;
		initGetGrid();   
    initPayGrid();  
    queryGet();
    queryPay();   
  }
  catch(ex)
  {
    alert("workQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGetGrid()
{                               
    var iArray = new Array();
      
    try
    {
       	iArray[0] =new Array();
       	iArray[0][0]="";         			//列名（此列为顺序号，列名无意义，而且不显示）
      	iArray[0][1]="0px";            		//列宽
    		iArray[0][2]=10;            			//列最大值
  			iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
        iArray[1]=new Array();
        iArray[1][0]="收费记录号";
        iArray[1][1]="80px";
        iArray[1][2]=100;
        iArray[1][3]=0;
              
        iArray[2]=new Array();
        iArray[2][0]="收费金额";
        iArray[2][1]="60px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
				iArray[3]=new Array();
        iArray[3][0]="收付方式";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
				iArray[4]=new Array();
        iArray[4][0]="转帐银行";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="转帐帐号";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="现金缴费期";
        iArray[6][1]="60px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="银行转帐预设时间";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;    
        
        iArray[8]=new Array();
        iArray[8][0]="实际收费时间";
        iArray[8][1]="90px";
        iArray[8][2]=100;
        iArray[8][3]=0;  
        




			//初始化MulLine
	    getGrid = new MulLineEnter("fm","getGrid"); 
	    //这些属性必须在loadMulLine前
	    getGrid.mulLineCount = 0;   
	    getGrid.displayTitle = 1;
//	    getGrid.canChk=1;        //1显示checkbox
	    getGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
	    getGrid.hiddenSubtraction=1;//是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
	    getGrid.loadMulLine(iArray);
	    //这些操作必须在loadMulLine后面
	  //  getGrid.selBoxEventFuncName ="*****";//单击checkBox或radio 后的事件处理方法，填写方法名    

    }
    catch(ex)
    {
      alert(ex);
    }
}
function initPayGrid()
{                               
    var iArray1 = new Array();
      
    try
    {
       	iArray1[0]=new Array();
      	iArray1[0][0]="";         			//列名（此列为顺序号，列名无意义，而且不显示）
      	iArray1[0][1]="0px";            		//列宽
    		iArray1[0][2]=10;            			//列最大值
  			iArray1[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray1[1]=new Array();
        iArray1[1][0]="付费记录号";
        iArray1[1][1]="80px";
        iArray1[1][2]=100;
        iArray1[1][3]=0;
        
        
        
        iArray1[2]=new Array();
        iArray1[2][0]="付费金额";
        iArray1[2][1]="60px";
        iArray1[2][2]=100;
        iArray1[2][3]=0;
        
				iArray1[3]=new Array();
        iArray1[3][0]="收付方式";
        iArray1[3][1]="100px";
        iArray1[3][2]=100;
        iArray1[3][3]=0;
        
				iArray1[4]=new Array();
        iArray1[4][0]="转帐银行";
        iArray1[4][1]="80px";
        iArray1[4][2]=100;
        iArray1[4][3]=0;
        
        iArray1[5]=new Array();
        iArray1[5][0]="转帐帐号";
        iArray1[5][1]="80px";
        iArray1[5][2]=100;
        iArray1[5][3]=0;
        
        iArray1[6]=new Array();
        iArray1[6][0]="银行转帐预设时间";
        iArray1[6][1]="80px";
        iArray1[6][2]=100;
        iArray1[6][3]=0;
        
        iArray1[7]=new Array();
        iArray1[7][0]="实际收费时间";
        iArray1[7][1]="80px";
        iArray1[7][2]=100;
        iArray1[7][3]=0;  
        
			//初始化MulLine
	    payGrid = new MulLineEnter("fm","payGrid"); 
	    //这些属性必须在loadMulLine前
	    payGrid.mulLineCount = 0;   
	    payGrid.displayTitle = 1;
//	    payGrid.canChk=1;        //1显示checkbox
	    payGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
	    payGrid.hiddenSubtraction=1;//是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
	    payGrid.loadMulLine(iArray1);
	    //这些操作必须在loadMulLine后面
//	    *****Grid.selBoxEventFuncName ="*****";//单击checkBox或radio 后的事件处理方法，填写方法名    

    }
    catch(ex)
    {
      alert(ex);
    }
}


        
</script>