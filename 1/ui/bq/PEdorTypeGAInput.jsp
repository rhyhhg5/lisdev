<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeGA.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeGAInit.jsp"%>
  <title>生存给付申请 </title>
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeGASubmit.jsp" method=post name=fm target="fraSubmit">
   	     
    <Div  id= "divEdorHead" style= "display: ''">
      <table  class= common>
       	 <TR class=common>

            <Input type=hidden class= readonly name=ContNo >

          <TD  class= title>
            被保人客户号
          </TD>
          <TD  class= input>
            <Input class= readonly name=InsuredNo >
          </TD>
          <TD  class= title>
            申请批单号
          </TD>
          <TD  class= input>
            <Input class= readonly name=EdorNo >
          </TD>
          <td class = title>
	    		保全项目
	    	</td>
	    	<TD  class= input>
	            <Input class= readonly  name=EdorType >
	        </TD>	
        </TR>
        <TR>
          
	          
        </TR>
      </table>
    </Div>
    <table>
	    <tr class=common>
		    <td class=common>
		    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
		    </td>
		    <td class= titleImg>年金转换</td>
		</tr>
    </table>
	<Div  id= "divLPAccMove" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				    <span id="spanLPAccMoveGrid" >
  				    </span> 
  			    </td>
  		    </tr>
    	</table>	
    </div>
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGet);">
    		</td>
    		<td class= titleImg>
    			 生存给付项信息
    		</td>
    	</tr>
    </table>
    <TR>
        <TD  class= title>
          险种
        </TD>
        <TD  class= input>
          <Input class="code" name=RiskCode ondblclick="showCodeListEx('RiskCode',[this],[1],'', '', '', true);" onkeyup="showCodeListKeyEx('RiskCode',[this],[1],'', '', '', true);">
        </TD>
        <TD>
            <Input VALUE="生存领取查询" class=cssButton TYPE=button onclick="QueryGetClick();">
        </TD>
    </TR>
    <Div  id= "divLPGet" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanLPGetGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>	
  </div>

  <div id="DivGetAcc" style="display : ">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGet);">
    		</td>
    		<td class= titleImg>
    			 帐户分类表
    		</td>
    	</tr>
 </table>
  <div id="DivLCInsureAcc" style="display : ">
    <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanLCInsureAccClassGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
	
  </div>
  </div>
  <TD  class= input width="26%"> 
    <Input class=cssButton type=Button value="添加年金转换" onclick="addAccMove()">
   </TD>
  <!--div id="DivGetAccManage" style="display : none">
  <table class = common>
  <tr class = common >
  <td class = title > <input name="annuityFlag1" type="checkbox" dir="ltr" onclick="setAnnuityFlag()" >&nbsp年金转换 </td><td class = title ><input name="cashFlag1" type="checkbox" onclick="setCashFlag()" >&nbsp现金转出 </td>
  </tr>
  <tr class = common >
  <td class = Input width = 26%>转年金金额：&nbsp<Input class= common name=CAnnuity onblur="verifyCAnnuity()">&nbsp元 </td><td class = title width = 26%>转出金额：&nbsp<Input class= common name=CCash onblur="verifyCCash()">&nbsp元 </td>
  </tr>
  <tr class = common>
  <td class = Input width = 26%>转年金比例：&nbsp<Input class= common name=RAnnuity onblur="verifyRAnnuity()">&nbsp%</td><td class = title width = 26%>转出比例：&nbsp<Input class= common name= RCash onblur="verifyRCash()">&nbsp%</td>
  </tr>
  </table>
  </div-->
	
    <table class = common>
		<tr class = common>
		    <TD  class= input width="26%"> 
			    <Div id ="divGetEndorse" style="display:''">
	       		    <Input class=cssButton type=Button value="费用明细" onclick="GetEndorseQuery()">
	    		</Div>
	    	</TD>
    <TD  class= input width="26%"> 
    			<div id="DivGetSave" style="display : ''">		
       		 			<Input class=cssButton type=Button value="保存申请" onclick="submitForm()">
       		</div>
    </TD>
    <TD  class= input width="26%"> 
       		 <Input class=cssButton type=Button value="返回" onclick="returnParent()">
    </TD>
    </tr>
    </table>
 
  <input type=hidden id="ContType" name="ContType">
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="annuityFlag" name="annuityFlag">
  <input type=hidden id="cashFlag" name="cashFlag">


</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>
