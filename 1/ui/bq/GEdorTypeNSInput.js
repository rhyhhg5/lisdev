//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var GrpPolNo = "";
var prtNo = "";

//
function showInsureList() {
  window.open("../f1print/ShowGrpInsuredList.jsp");
}

//初始化险种信息和已录入信息
function initQuery() {	
  var strSql = "select prtno from lcgrppol where GrpPolNo='" + fm.all('GrpPolNo').value + "'";
	prtNo = easyExecSql(strSql);	
  
	var strSql = "select riskcode, riskname from lmriskapp where SubRiskFlag='S' and riskprop='G' and riskcode not in (select riskcode from lcgrppol where PrtNo='" + prtNo + "')";
	var strResult = easyQueryVer3(strSql);
	if (strResult) {	
	  fm.RiskCode.CodeData = easyQueryVer3(strSql);	
	}
	else {
	  fm.RiskCode.CodeData = "0|1^000000|没有可以增加的附加险！";	
	}
	
  LPInsuredGrid.clearData("LPInsuredGrid");
	var strSql = "select distinct riskcode, GrpProposalNo from lcgrppol where PrtNo='" + prtNo
	           + "' and riskcode in (select riskcode from lmriskapp where SubRiskFlag='S' and riskprop='G') and appflag='2' order by grpproposalno";
	turnPage.pageLineNum = 500;
	turnPage.queryModal(strSql, LPInsuredGrid);  
  	
	GrpPolNo = fm.all('GrpPolNo').value;
}

//打开险种录入界面
function afterCodeSelect(cCodeName, Field) {
  if (LPInsuredGrid.mulLineCount >= 500) {
    alert("超出该批次最大处理范围，请重新增加一个保全项目！");
    return;
  }
  
  if (cCodeName == "EdorRisk") {
		//showInfo = window.open("./ProposalMain.jsp?risk="+Field.value+"&prtNo="+prtNo+"&loadFlag=8"+"&polNo="+fm.PolNo.value);
		showInfo = window.open("./GrpProposalMain.jsp?type=noScan&prtNo=" + prtNo + "&riskCode=" + Field.value);
  }
}

//保存申请
function edorSave() {
  if (LPInsuredGrid.mulLineCount == 0) {
    alert("必须录入后才能保存申请！");
    return;
  }
  
  for (i=0; i<LPInsuredGrid.mulLineCount; i++) {
    if (LPInsuredGrid.getRowColData(i, 2) == "") {
      alert("必须完成所有的险种录入才能保存申请！");
      return;
    }
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.all('fmtransact').value = "INSERT";
  fm.submit();
}

//显示提交结果
function afterSubmit( FlagStr, content ) {
	showInfo.close();
           
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	showInfo = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
}

//返回，关闭当前窗口
function returnParent() {
  top.close();
}
