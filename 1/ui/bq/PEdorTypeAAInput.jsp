<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<% 
//程序名称：PEdorTypeAAInput.jsp
//程序功能：团单增额
//创建日期：2005-04-25 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>   
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeAA.js"></SCRIPT>
  <%@include file="PEdorTypeAAInit.jsp"%>
  
  <title>增额明细总页面</title> 
</head>

<body  onload="initForm();" >
  <form action="./PEdorTypeAASubmit.jsp" method=post name=fm target="fraSubmit">  
  <!--------------------------------------------------->

<input type="hidden" readonly name="InsuredNo">
<input type="hidden" readonly name="EdorAcceptNo">
<input type="hidden" readonly name="ContType">
<input type="hidden" readonly name="fmtransact">
<input type="hidden" readonly name="PolNo"> 
<!--------------------------------------------------->  
      <table class=common>
      <TR  class= common> 
        <TD  class= title > 批单号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 批改类型 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=EdorType>
        </TD>       
        <TD class = title > 保单号 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=ContNo>
        </TD>   
      </TR>
    </TABLE>    
        
    <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCDuty);">
        </td>
        <td class= titleImg>
          责任信息
        </td>
      </tr>
    </table>       
    
    <Div  id= "divLCDuty" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCDutyGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	
  	</div>
  <Div  id= "divLPAppntGrpDetail" style= "display: ''">
  <table>
    	<tr>
    		<td class= titleImg>
    			 请输入增额或增额比例
    		</td>
    	</tr>
   </table>
    
   <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
       <TR>       
       <TD class=title8>
        增额
        </TD>
        <TD>
        	<Input class=common name=GetMoney onChange="changeValue1()" onFocus="">
        </TD>
        <TD class=title8>
        增额比例
        </TD>
        <TD>
        	<Input class=common name=GetMoneyPro onChange="changeValue2()" onFocus="">
        </TD>
         <TD  class= title8 width="26%"> 
       		 %
     	 </TD>    
      </table>     
    </Div>
	

	  <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPDuty);">
        </td>
        <td class= titleImg>
         增额后责任信息
        </td>
      </tr>
    </table>
	  <Div  id= "divLPDuty" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLPDutyGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>   	
      
  	</div>
	 	 	  
	  <Input type=Button value="保存申请" class=cssButton onclick="PEdorSave()">  
	  <Input type=Button value="返回" class=cssButton onclick="returnParent()">	 
	  <input type=hidden id="Prem_bak" name="Prem_bak"> 
	  <input type=hidden id="Transact" name="Transact">	 
	  
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>

<script>
  window.focus();
</script>
