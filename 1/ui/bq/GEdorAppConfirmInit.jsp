<%
//程序名称：ReportQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->                     

<script language="JavaScript">
var mEdorAcceptNo = "<%=request.getParameter("EdorNo")%>"; //保全号
var mEdorNo =  "<%=request.getParameter("EdorNo")%>";//保全号
var mGetMoney = 0;
function initForm()
{
  try
  {
  	fm.EdorNo.value = mEdorAcceptNo;
  	fm.EdorAcceptNo.value = mEdorAcceptNo;
  	mGetMoney = getGetMoney(mEdorAcceptNo);
		queryAccount();
		getNoticeNo();
		var getMoney = HaveFee(mEdorAcceptNo);
		var mEdorType = getEdorType();
		if (getMoney != 0 && mEdorType != "TY" && mEdorType != "TZ" && mEdorType != "TL" && mEdorType != "TQ" && mEdorType != "SG" && mEdorType != "JY")
		{
			document.all("AccFee").style.display = "";
			document.all("FeeFlag").style.display = "none";                    
			document.all("PayDiv").style.display = "none";
			getAppAccInfo();
			notUseAcc();
			if (getMoney > 0)
			{
				document.all("AccTypeStr").innerText = "是否使用帐户余额抵扣?";
				fm.AccType.value = "0";
				fm.DestSource.value = "01";
			}
			else
			{
				document.all("AccTypeStr").innerText = "本次退费是否转到帐户余额?";
				fm.AccType.value = "1";
				fm.DestSource.value = "11";
			}
		}
//		if(mEdorType=='TY' && getMoney!=0){
//			document.all("FeeFlag").style.display = 'none';                    
//			document.all("PayDiv").style.display = 'none';
//		}
		if(mEdorType=='TQ' && getMoney!=0){
			document.all("FeeFlag").style.display = 'none';                    
			document.all("PayDiv").style.display = '';
			document.all("trEndDate").style.display = '';
			initTQPayInfo(mEdorType);
		}
		if(mEdorType=='SG' && getMoney!=0){
			document.all("FeeFlag").style.display = 'none';                    
			document.all("PayDiv").style.display = '';
			document.all("trEndDate").style.display = '';
			initTQPayInfo(mEdorType);
		}
		if(mEdorType=='JY' && getMoney!=0){
			document.all("FeeFlag").style.display = 'none';                    
			document.all("PayDiv").style.display = '';
			document.all("trEndDate").style.display = '';
			//initTQPayInfo(mEdorType);
		}
		
		initElementtype();
		checkOverFlow(mEdorAcceptNo);
		CtrlBalanceSel(mEdorAcceptNo);
  	ctrlBalanceInfo();
  	ctrlButton();
  	setRemak();
  }
  catch(ex)
  {
    alert("GEdorAppConfirmInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>