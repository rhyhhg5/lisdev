  
<%
	//程序名称：PEdorTypeZBSubmit.jsp
	//程序功能：
	//创建日期：2008-04-10
	//创建人  ：pst
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息
	CErrors tError = null;
	//后面要执行的动作：添加，修改

	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String transact = "";

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");

	transact = request.getParameter("fmtransact");
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String contNo = request.getParameter("ContNo");
	String appMoney = request.getParameter("AppMoney");
	String tZBFLag = request.getParameter("ZBFlag");

	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setContNo(contNo);
	tLPEdorItemSchema.setEdorType(edorType);
	tLPEdorItemSchema.setInsuredNo("000000");
	tLPEdorItemSchema.setPolNo("000000");
	LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
	tLPEdorEspecialDataSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorEspecialDataSchema.setEdorNo(edorNo);
	tLPEdorEspecialDataSchema.setEdorType(edorType);
	tLPEdorEspecialDataSchema.setDetailType(tZBFLag);
	tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
	tLPEdorEspecialDataSchema.setEdorValue(appMoney);
	// 准备传输数据 VData		
	VData tVData = new VData();
	PEdorZBDetailUI tPEdorZBDetailUI = new PEdorZBDetailUI();
	//初始化
	if ("init".equals(transact)) {
		tVData.add(tGlobalInput);
		tVData.add(tLPEdorItemSchema);
		if (!tPEdorZBDetailUI.submitData(tVData, transact)) {
			VData rVData = tPEdorZBDetailUI.getResult();
			System.out.println("Submit Failed! "
			+ tPEdorZBDetailUI.mErrors.getErrContent());
			Content = "初始化失败，原因是:"
			+ tPEdorZBDetailUI.mErrors.getFirstError();
			FlagStr = "Fail";
		} else {
			Content = "初始化成功";
			FlagStr = "Success";
		}
	} else {
		tVData.add(tGlobalInput);
		tVData.add(tLPEdorItemSchema);
		tVData.add(tLPEdorEspecialDataSchema);

		if (!tPEdorZBDetailUI.submitData(tVData, transact)) {
			VData rVData = tPEdorZBDetailUI.getResult();
			System.out.println("Submit Failed! "
			+ tPEdorZBDetailUI.mErrors.getErrContent());
			Content = "失败，原因是:"
			+ tPEdorZBDetailUI.mErrors.getFirstError();
			FlagStr = "Fail";
		} else {
			Content = "保存成功";
			FlagStr = "Success";
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=transact%>");
</script>
</html>
