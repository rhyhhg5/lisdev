//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initCollectivityGrid();
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select GrpNo,GrpName,GrpAddress,Satrap from LDGrp where 1=1 "
				 + getWherePart( 'GrpNo' )
				 + getWherePart( 'GrpName' )
				 + getWherePart( 'GrpNature' )
				 + getWherePart( 'BusinessType' );
//alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initCollectivityGrid();
		CollectivityGrid.recordNo = (currPageIndex - 1) * MAXSCREENLINES;
		CollectivityGrid.loadMulLine(CollectivityGrid.arraySave);
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				CollectivityGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//CollectivityGrid.delBlankLine();
	} // end of if
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = CollectivityGrid.getSelNo();
	//alert(tSel);
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			//alert("hehe");
			top.opener.afterGrpDetailQuery( arrReturn );
			top.close();
		}
		catch(ex)
		{
			alert( "请先选择一条非空记录，再点击返回按钮。");
			//alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = CollectivityGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

