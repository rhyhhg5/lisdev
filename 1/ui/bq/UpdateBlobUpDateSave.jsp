<%

%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%>
  <%@page import="java.io.*"%>
  <%@page import="java.text.SimpleDateFormat" %>
    <%@page import="org.apache.commons.fileupload.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行合同保存Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  //select * from LDSYSVAR
  CErrors tError = null;
  String Content = "";
  String FlagStr = "";
  String mEdorno = request.getParameter("Edorno");
  String mContType = request.getParameter("ContType");
  String mFileType = request.getParameter("FileType");
  String fileName = "";
  String tableName = "";
  String fieldName = "";
  
  //先上传再转到数据库
  		//临时保存解析出的数据的保存路径
		String path = application.getRealPath("").replace('\\','/')+'/';
		String importPath = path + "temp/upload";
		System.out.println(importPath);

		DiskFileUpload fu = new DiskFileUpload();
		//设置请求消息实体内容的最大允许大小
		fu.setSizeMax(100 * 1024 * 1024);
		//设置是否使用临时文件保存解析出的数据的那个临界值
		fu.setSizeThreshold(1 * 1024 * 1024);
		//设置setSizeThreshold方法中提到的临时文件的存放目录
		fu.setRepositoryPath(importPath);
		//判断请求消息中的内容是否是“multipart/form-data”类型
		//fu.isMultipartContent(request);
		//设置转换时所使用的字符集编码
		//fu.setHeaderEncoding(encoding);
		
		List fileItems = null;
		try
		{
			//解析出FORM表单中的每个字段的数据，并将它们分别包装成独立的FileItem对象
			//然后将这些FileItem对象加入进一个List类型的集合对象中返回
			fileItems = fu.parseRequest(request);
		}
		catch(Exception ex)
		{
			fileItems = new ArrayList();
			ex.printStackTrace();
		}

			System.out.println(fileItems.size());
		//依次处理每个上传的文件
		Iterator iter = fileItems.iterator();

		HashMap hashMap = new HashMap();
		while (iter.hasNext()) 
		{
			FileItem item = (FileItem) iter.next();
			
			//判断FileItem类对象封装的数据是否属于一个普通表单字段，还是属于一个文件表单字段
			if(item.isFormField())
			{
				//返回表单字段元素的name属性值、将FileItem对象中保存的主体内容作为一个字符串返回
				hashMap.put(item.getFieldName(), item.getString());
				//System.out.println(item.getFieldName() + " : " + item.getString());
			}
		}
		if(mEdorno==null||"".equals(mEdorno)){
			 mEdorno = (String)hashMap.get("Edorno");
		}
		if(mContType==null||"".equals(mContType)){
			 mContType = (String)hashMap.get("ContType");
		}
  		if(mFileType==null||"".equals(mFileType)){
			 mFileType = (String)hashMap.get("FileType");
		}	
     		 
		iter = fileItems.iterator();
		while (iter.hasNext()) 
		{
			FileItem item = (FileItem) iter.next();
			
			//判断FileItem类对象封装的数据是属于一个普通表单字段(true)，还是属于一个文件表单字段
			if(!item.isFormField())
			{
				//获得文件上传字段中的文件名
				String name = item.getName();
				System.out.println("name : " + name);
				long size = item.getSize();
				if((name==null||name.equals("")) && size==0)
				{
					continue;
				}

				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
       			 String a = sdf.format(new Date());
				fileName =path + "temp/qy/update"+"/"+a+name.substring(name.lastIndexOf("\\") + 1);
				System.out.println("file name : "+fileName + " size : " + size);
				//Random rand = new Random();
				//int tSelect = rand.nextInt(999);
				//String filePath = "qy";
				
				System.out.println("filePath : " +   fileName);
				File file = new File(path+"temp/qy/update" );   
				if (!file.exists()) 
				{    
					file.mkdirs();
				}
				
				//保存上传的文件到指定的目录
				try 
				{
					item.write(new File( fileName));
						FlagStr = "succ";
						Content = "文件上载成功.";

				} catch(Exception e) 
				{
					e.printStackTrace();
					System.out.println("upload file error ...");
					FlagStr = "Fail";
					Content = fileName + "上载失败.";
					break;
				}
			}
		}
		System.out.println("run succ...");
  

  if("1".equals(mContType)){
	  
    tableName = "LPEdorAppPrint";//个单
    fieldName = "EdorAcceptNo";//个单
  }
  else if("2".equals(mContType)){
	  fieldName = "EdorNo";//团单
	  
	  if("1".equals(mFileType)){
		  
		  tableName = "LPEdorPrint";//团单
	      
	  }
	  else{
		  tableName = "LPEdorPrint2";//团单增人清单
		  
	  }
	    
	  } 
  
  UpdateBlobBL mUpdateBlobBL = new UpdateBlobBL(fileName,tableName,fieldName,mEdorno);
  VData tVData = new VData();
  try
  {
	  mUpdateBlobBL.importBlob();
	  FlagStr = "Succ";
		Content = "数据保存成功" ;
  }
  catch(Exception ex)
  {
	
    Content = "失败" + ex.toString();
    FlagStr = "Fail";
  }
  
 
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>