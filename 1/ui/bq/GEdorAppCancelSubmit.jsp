<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-20
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>

<SCRIPT language="javascript">
var mLoadFlag = "<%=request.getParameter("LoadFlag")%>";
</SCRIPT>
<%
  //接收信息，并作校验处理。
  //输入参数
  LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
  LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
  PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
  TransferData tTransferData = new  TransferData(); 

  //输出参数
  String FlagStr = "";
  String Content = "";
  GlobalInput tGI = new GlobalInput(); 
  tGI=(GlobalInput)session.getValue("GI");  
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  
 {
			String Operator  = tGI.Operator ;  //保存登陆管理员账号
			String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom内存中登陆区站代码

		  CErrors tError = null;
			String tBmCert = "";
			String delReason = "";
			String reasonCode = "";

			//后面要执行的动作：添加，修改，删除
		  String transact=request.getParameter("Transact");
		  //System.out.println("transact:"+transact);

      String DelFlag= request.getParameter("DelFlag");
      //System.out.println("Delflag:"+DelFlag);
    
    
    if(DelFlag.equals("1")){        //删除项目
			
			tLPGrpEdorItemSchema.setEdorNo(request.getParameter("hEdorNo"));
			tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("hGrpContNo"));
			tLPGrpEdorItemSchema.setEdorType(request.getParameter("hEdorType"));
			tLPGrpEdorItemSchema.setEdorState(request.getParameter("hEdorState"));
			tLPGrpEdorItemSchema.setMakeDate(request.getParameter("MakeDate"));
      tLPGrpEdorItemSchema.setMakeTime(request.getParameter("MakeTime"));
      delReason = request.getParameter("delItemReason"); 
      reasonCode = request.getParameter("CancelItemReasonCode"); 
      
      tTransferData.setNameAndValue("DelReason",delReason);
      tTransferData.setNameAndValue("ReasonCode",reasonCode);
      System.out.println("页面中的数据"+delReason);   
     try
     {
    // 准备传输数据 VData
     VData tVData = new VData();
     tVData.addElement(tGI);
     tVData.addElement(tLPGrpEdorItemSchema);
     tVData.addElement(tTransferData);
     //tVData.addElement(reasonCode);
     
     

     //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
      tPGrpEdorCancelUI.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
  if (FlagStr=="")
  {
    tError = tPGrpEdorCancelUI.mErrors;
    if (!tError.needDealError())
    {
      Content ="撤销成功！";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = "撤销失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
   }
    
  
  
  }
    if (DelFlag.equals("2")){  //删除批单
      
     tLPGrpEdorMainSchema.setEdorNo(request.getParameter("hEdorNo"));
     tLPGrpEdorMainSchema.setEdorState(request.getParameter("EdorState"));     
     tLPGrpEdorMainSchema.setGrpContNo(request.getParameter("hGrpContNo"));     
     delReason = request.getParameter("delMainReason");
     reasonCode = request.getParameter("CancelMainReasonCode");  
     
     tTransferData.setNameAndValue("DelReason",delReason);
     tTransferData.setNameAndValue("ReasonCode",reasonCode);  
      System.out.println("页面中的数据"+delReason);  
     try
     {
    // 准备传输数据 VData
     VData tVData = new VData();
     tVData.addElement(tGI);
     tVData.addElement(tLPGrpEdorMainSchema);
     tVData.addElement(tTransferData);//撤销申请原因
     //tVData.addElement(reasonCode);   
     //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
      tPGrpEdorCancelUI.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
   if (FlagStr=="")
   {
    tError = tPGrpEdorCancelUI.mErrors;
    if (!tError.needDealError())
    {
      Content ="撤销成功！";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = "撤销失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
   }
  
  
  }  
   
}
%>
<html>
	<script language="javascript">
	if (mLoadFlag == "TASKFRAME")
	{
		parent.fraInterface.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
	else
	{
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
</script>
</html>

