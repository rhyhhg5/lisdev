<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.config.*"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息

	CErrors tError = null;
	//后面要执行的动作：添加，修改
	String FlagStr = "";
	String Content = "";
	String transact = "";

	//执行动作：insert 添加纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");
	transact = request.getParameter("fmtransact");
	String Year = request.getParameter("tYear");
	String Quarter = request.getParameter("tQuarter");
	String RiskRate = request.getParameter("tRiskRateName");

	LDRiskRateSet tLDRiskRateSet=new LDRiskRateSet();
	LDRiskRateSchema tLDRiskRateSchema = new LDRiskRateSchema();
	tLDRiskRateSchema.setYear(Year);
	tLDRiskRateSchema.setQuarter(Quarter);
	tLDRiskRateSchema.setRiskRate(RiskRate);
	tLDRiskRateSet.add(tLDRiskRateSchema);

	LMRiskRateUI tLMRiskRateUI = new LMRiskRateUI();
	try{
		// 准备传输数据 VData	
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tVData.add(tGlobalInput);
		tVData.addElement(tLDRiskRateSet);
		tTransferData.setNameAndValue("tLDRiskRateSchema",tLDRiskRateSchema);
		tVData.add(tTransferData);
		tLMRiskRateUI.submitData(tVData,transact);
	}catch(Exception ex){
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	if (FlagStr == "")
	{ 
		tError = tLMRiskRateUI.mErrors;
		if (!tError.needDealError())
		{                        

 		 Content = "操作成功! ";
  		FlagStr = "Success";
		}
		else                                                                           
		{
 		 Content = "操作失败，原因是:" + tError.getFirstError();
  		FlagStr = "Fail";
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
