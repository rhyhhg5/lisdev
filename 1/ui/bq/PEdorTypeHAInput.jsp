<html> 
<% 
//程序名称：分红险满期领取
//程序功能：个人保全
//创建日期：2016-8-25 
//创建人  ：【OoO?】hehongwei
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeHA.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeHAInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeHASubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR class= common> 
      <TD class= title > 受理号 </TD>
       <TD class= input>
        <input class="readonly" type="text" readonly name=EdorNo >
       </TD>
      <TD class = title > 批改类型 </TD>
       <TD class = input >
      	<input class = "readonly" type="hidden" readonly name=EdorType>
      	<input class = "readonly" readonly name=EdorTypeName>
       </TD>
      <TD class = title > 合同保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE>
  <br>
  <table>
   	<tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
      </td>
      <td class= titleImg>
        保单信息
      </td>
   	</tr>
  </table> 
	<Div  id= "divLCPol" style= "display: ''"> 
  <table  class= common>    
		<TR>
			<TD class = title>
			 	投保人客户号
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=AppntNo >
			</TD>
			<TD class= title>
				投保人姓名
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=AppntName >
			</TD>
						<TD class = title>
			 	被保人客户号
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=InsuredNo >
			</TD>
		</TR> 
		<TR>
			<TD class= title>
				被保人姓名
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=InsuredName >
			</TD>
			<TD class= title>
				生效日期
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=CValidate >
			</TD>      

			<TD class= title>
				交至日期
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=PayToDate >
			</TD>
		</TR> 
		<TR>
			<TD class= title>
				总保费
			</TD>
			<TD class= input>
			 <Input class= "readOnly" readonly  name=Prem >
			</TD>   
			<TD class= title>
				总保额
			</TD>
			<TD class= input>
			 <Input class= "readOnly" readonly  name=Amnt >
			</TD>

			<TD class= title>
				红利领取方式
			</TD>
			<TD class= input>
			 <Input class= "readOnly" readonly  name=BonusGetMode1 >
			</TD>
		</TR> 
		<TR>
			<TD class= title>
				累计生息账户余额
			</TD>
			<TD class= input>
			 <Input class= "readOnly" readonly  name=InsuAccBala style= "display: 'none'" >
			</TD>
		</TR>	  

	</Table>
	</div>
	<div  id= "getCustomer" style= "display: 'none'">
			<TD class= title>
			  给付对象
			</TD>    
			<TD class= input >
			  <Input class="codeNo" name="PayType" value="1" CodeData="0|^1|请选择给付对象^2|投保人^3|被保人" 
			  ondblClick="showCodeListEx('PayType',[this,PayTypeName],[0,1]);" 
			  onkeyup="showCodeListKeyEx('PayType',[this,PayTypeName],[0,1]);" ><Input class="codeName" name="PayTypeName"  readonly>
			</TD> 
	</div>
    <br>
		<div>
			<Input class= cssButton type=Button value="保  存" onclick="edorTypeHASave()">
			<Input class= cssButton type=Button value="取  消" onclick="initForm()">
			<Input class= cssButton type=Button value="返  回" onclick="returnParent()">
		</div>
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="PayType1" name="PayType1">
	 <input type=hidden id="BonusGetMode" name="BonusGetMode">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
