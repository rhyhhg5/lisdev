//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var theFirstValue="";
var theSecondValue="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示


function EasyExecSql()
{
	if(fm.tGrpContNo.value=="" || fm.tGrpContNo.value == null)
	{
		alert("请录入保单号后再次查询！");
		return false ;
	}
	var strSQL = " select a.GrpContNo,a.GrpName,a.cvalidate,a.paytodate "
	      + " from LCGrpPol a,LMRiskApp b where RiskType3='7' and a.RiskCode = b.RiskCode "
          + " and a.GrpContNo ='"+fm.tGrpContNo.value+"' with ur "
          ;
  turnPage.queryModal(strSQL, GetModeGrid);  
  if(GetModeGrid.mulLineCount == 0)
	{
	    alert("没有查询到保单信息！");
	}     
}

function queryAll()
{
  tRow = GetModeGrid.getSelNo();
  if( tRow == 0 || tRow == null )
  {
    return false;
  }
  var arrSelected = new Array();
  arrSelected = GetModeGrid.getRowData(tRow-1);
  initGrpContGrid();
  var sql = "select grpcontno,contno,insuredname,cvalidate,cinvalidate from lccont where grpcontno ='"+arrSelected[0]+"' with ur ";
  turnPage2.queryModal(sql, GrpContGrid);
  fm.tCinvaliDate.value = arrSelected[3];
  return false ;
}

function save()
{
	if (GrpContGrid.mulLineCount ==0)
	{
		alert("没有保单明细信息，不能修改！");
		return false;
	}	
	
	if (!CheckData(fm.tCinvaliDate.value))
	{
		return false ;
	}
	document.all.saveButton.disabled=true;
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	EasyExecSql();
	initGrpContGrid();
	document.all.saveButton.disabled=false;
}

/**
*  日期格式校验，防止录入错误格式日期。
*/


function CheckData(cform){
	
 if (fm.tCinvaliDate.value=="" || fm.tCinvaliDate.value==null)
 {
    alert("保单终止日期不能为空，请输入！");
 		return false;
 } 
 if (!formatTime(fm.tCinvaliDate.value))
 {
    alert("保单终止日期式错误！");
    return false;
 }
  var sql = "select 1 from lcgrppol where grpcontno = '" +fm.tGrpContNo.value+ "' "
          + "and paytodate >= '"+fm.tCinvaliDate.value+"' with ur";
  var rs = easyExecSql(sql);
  if(rs)
  {
    alert("修改后保单终止日期必须大于原保单终止日期!");		
    return false;
  } 
   return true;
}

function formatTime(str)
{
  var   r   =   str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);     
  if(r==null) return   false;     
  var  d=  new  Date(r[1],   r[3]-1,   r[4]);     
  return  (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);   
}