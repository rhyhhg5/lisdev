<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：PEdorQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  //保单信息部分
  LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
  tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));
  tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));

  System.out.println("------edorno:"+request.getParameter("EdorNo"));
  System.out.println("------Polno:"+request.getParameter("PolNo"));
  System.out.println("------edortype:"+request.getParameter("EdorType"));

  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLPEdorMainSchema);
  // 数据传输
  PEdorMainUI tPEdorMainUI   = new PEdorMainUI();
	if (!tPEdorMainUI.submitData(tVData,"QUERY||EDOR"))
	{
      Content = " 查询失败，原因是: " + tPEdorMainUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tPEdorMainUI.getResult();
		System.out.println("-----size:"+tVData.size());
		// 显示
		// 保单信息
		LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema(); 
		LPEdorMainSet mLPEdorMainSet=new LPEdorMainSet();
		LCPolSchema mLCPolSchema = new LCPolSchema();
		
		mLPEdorMainSet=(LPEdorMainSet)tVData.getObjectByObjectName("LPEdorMainBLSet",0);
		mLPEdorMainSchema=mLPEdorMainSet.get(1);
		mLCPolSchema = (LCPolSchema)tVData.getObjectByObjectName("LCPolSchema",0);
		
		%>
    	<script language="javascript">
    	
    		parent.fraInterface.fm.all("PolNo").value = "<%=mLCPolSchema.getPolNo()%>";
    		
    	 	parent.fraInterface.fm.all("InsuredName").value="<%=mLCPolSchema.getInsuredName()%>";
    	 	parent.fraInterface.fm.all("AppntName").value="<%=mLCPolSchema.getAppntName()%>";
    	 	parent.fraInterface.fm.all("ValiDate").value="<%=mLCPolSchema.getCValiDate()%>";
    	 	parent.fraInterface.fm.all("PaytoDate").value="<%=mLCPolSchema.getPaytoDate()%>";
    	 	parent.fraInterface.fm.all("Prem").value="<%=mLCPolSchema.getPrem()%>";
    	 	parent.fraInterface.fm.all("AgentCode").value="<%=mLCPolSchema.getAgentCode()%>";
			parent.fraInterface.fm.all("GetPolDate").value = "<%=mLCPolSchema.getGetPolDate()%>";
			
				parent.fraInterface.fm.all("RiskCode").value = "<%=mLCPolSchema.getRiskCode()%>";

				parent.fraInterface.fm1.all("PolNo").value = "<%=mLPEdorMainSchema.getPolNo()%>";
    	 	parent.fraInterface.fm.all("EdorNo").value = "<%=mLPEdorMainSchema.getEdorNo()%>";
    	 	parent.fraInterface.fm.all("EdorType").value = "<%=mLPEdorMainSchema.getEdorType()%>";
    	 	parent.fraInterface.fm.all("EdorValiDate").value = "<%=mLPEdorMainSchema.getEdorValiDate()%>";
    	 	//parent.fraInterface.fm.all("ChgPrem").value = "<%=mLPEdorMainSchema.getChgPrem()%>";
    	 	//parent.fraInterface.fm.all("ChgAmnt").value = "<%=mLPEdorMainSchema.getChgAmnt()%>";
    	 	//top.opener.fm.all("Operator").value = "<%=mLPEdorMainSchema.getOperator()%>";
    	 	parent.fraInterface.fm.all("EdorAppDate").value = "<%=mLPEdorMainSchema.getEdorAppDate()%>";
    	 	
    	 	parent.fraInterface.fm.all("PayMode").value = "<%=mLCPolSchema.getPayMode()%>";
    	</script>
		<%		
	}
 // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tPEdorMainUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>