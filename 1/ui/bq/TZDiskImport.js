var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var GrpContNo = "";
var mSwitch = parent.VD.gVSwitch;
var ImportPath;
var arrResult;
var specialRiskFlag = false;

//磁盘导入
function importFile()
{
  if (fm.FileName.value == "")
  {
    alert("请选择要导入的文件！");
    return false;
  }
	document.all("Info").innerText = "上传文件导入中，请稍后...";
	fm.submit();
}

function afterSubmit(FlagStr, content)
{
	document.all("Info").innerText = content;
	if (FlagStr == "Succ")
	{
		top.opener.initForm();
		top.close();
	}
}

function getImportPath () {
 // 书写SQL语句
  var strSQL = "";

  strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未找到上传路径");
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  ImportPath = turnPage.arrDataCacheSet[0][0];

}

function InsuredListUpload() {
  getImportPath();
  ImportFile = fm1.all('FileName').value;
  var prtno = edorNo;
  var tprtno = ImportFile;

  if ( tprtno.indexOf("\\")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("\\")+1);
  if ( tprtno.indexOf("/")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("/")+1);
  if ( tprtno.indexOf("_")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("_"));
  if ( tprtno.indexOf(".")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("."));

  if ( prtno!=tprtno ) {
    alert("文件名与工单号不一致,请检查文件名!");
    return ;
  } else {
    var showStr="正在上载数据……";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm1.action = "../bq/BQDiskApplySave.jsp?ImportPath="+ImportPath+"&GrpContNo="+grpContNo+"&EdorNo="+edorNo;
    fm1.submit(); //提交
  }
}