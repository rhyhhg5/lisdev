<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "保单管理收付清单_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    String StartDate = request.getParameter("StartDate");
   	String EndDate = request.getParameter("EndDate");	
	String feeState = request.getParameter("feeState");	  
	String feeType = request.getParameter("feeType");	  
	String feeTypeName = request.getParameter("feeTypeName");	 
	String organcode = request.getParameter("organcode");	  
	String organname = request.getParameter("organname");	  
	String Groupno = request.getParameter("Groupno");	  
	String Groupname = request.getParameter("Groupname");	  
	String usercode = request.getParameter("usercode");	 
	String username = request.getParameter("username");
    
    String flag = "0";
    String FlagStr = "";
    String Content = "";
    String state1 = " ";
    String state2 = " ";
    String feeType1 = " ";
    String feeType2 = " ";
    String com = " ";
    String group1 = " ";
    String group2 = " ";
    String usercode1 = " ";
    String usercode2 = " ";

    if (!(feeState == null || feeState.equals(""))) 
    {
	    state1 = "and '" + feeState + "'='1'";
	    state2 = "and '" + feeState + "'='0'";
    }
    if (!(feeType == null || feeType.equals(""))) 
    {
	    feeType1 = "and '" + feeType + "'='1'";
	    feeType2 = "and '" + feeType + "'='0'";
    }
    if (!(organcode == null || organcode.equals(""))) 
    {
	    String tManage = "";
	    if (organcode.length() >= 4) 
	    {
		      tManage = organcode.substring(0, 4) + "0000";
		      System.out.println("tManage=" + tManage);
		      System.out.println("organcode=" + organcode);
        }
	    else 
	    {
	          tManage = organcode + "0000";
	    }
	    if (organcode.equals(tManage)) 
	    {
		      if (organcode.equals("86000000")) 
		      {
		          com = "and lp.ManageCom =substr('" + organcode + "',1,2)";
		      }
		      else
		      {
		          com = "and lp.ManageCom =substr('" + organcode + "',1,4)";
		      }
	        
	    }
	    else 
	    {
	        com = "and lp.ManageCom like '" + organcode + "%'";
	    }
  }
  if (!(Groupno == null || Groupno.equals(""))) 
  {
	    group1 = "and (select acceptorno from lgwork where workno=sp.otherno) in (select memberno from lggroupmember where groupno='" + Groupno + "')";
	    group2 = "and (select acceptorno from lgwork where workno=incomeno) in (select memberno from lggroupmember where groupno='" + Groupno + "')";
  }

  if (!(usercode == null || usercode.equals(""))) 
  {
	    usercode1 = "and (select acceptorno from lgwork where workno=sp.otherno)= '" + usercode + "'";
	    usercode2 = "and (select acceptorno from lgwork where workno=incomeno)='" + usercode + "'";
  } 
  
    String date=" and sp.makedate between '"+StartDate+"' and '"+EndDate+"'"; 

    
    
    String tSQL   = "select sp.otherno 受理号,  " 
					+"(select name from ldcom where comcode=lp.managecom) 受理机构,   "
					+"(select username from lduser where usercode=(select acceptorno from lgwork where workno=sp.otherno)) 受理人, "
					+"(select workTypeName from LGworkType where workTypeNo=(select typeno from lgwork where workno=sp.otherno)) 业务类型, " 
					+"(select username from lduser where usercode=(select operator from lgwork where workno=sp.otherno)) 处理人,  "
					+" sp.MakeDate 提交日期, "
					+"sp.sumduepaymoney 应收金额, "
					+"0 付费金额, "
					+"0 实收金额, "
					+"'' 收付方式, "
					+"(select bankname from ldbank where  bankcode=sp.BankCode) 转帐银行, "
					+" sp.bankaccno 账号,"
					+" sp.accname 帐户所有人 ,"
					+"char(paydate) 收费期限, "
					+"'未完成'  收付状态  "
					+"from ljspay sp,lpedorapp lp   "
					+"where sp.othernotype in ('10','3','13') and sp.otherno=lp.edoracceptno   "
					+"and not exists(select 1 from LGWork where workno = lp.EdorAcceptNo and statusNo = '8') "
					+date+" " 
					+state1+" " 
					+feeType1+" " 
					+com+" " 
					+group1+" "
					+usercode1+" "                                                                    
					+"union    "
					+"select incomeno 受理号,   "
					+"(select name from ldcom where comcode=lp.managecom) 受理机构, "
					+"(select username from lduser where usercode=(select acceptorno from lgwork where workno=incomeno)) 受理人,  "
					+"(select workTypeName from LGworkType where workTypeNo=(select typeno from lgwork where workno=incomeno)) 业务类型, "
					+"(select username from lduser where usercode=(select operator from lgwork where workno=incomeno)) 处理人,  "
					+"sp.MakeDate 提交日期,   "
					+"sumactupaymoney 应收金额,  "
					+"0 付费金额, "
					+"cp.Paymoney 实收金额, "
					+"(select codename from ldcode where codetype='paymode' and code = cp.PayMode) 收付方式,    "
					+"(select bankname from ldbank where  bankcode=sp.BankCode) 转帐银行,    "
					+" sp.bankaccno 账号,"
					+" sp.accname 帐户所有人 ,"
					+"char(cp.paydate) 收费期限,   "
					+"'结束' 收付状态 "
					+"from ljapay sp,lpedorapp lp,LJTempfeeClass cp   "
					+"where sp.incometype in ('10','3','13') and sp.incomeno=lp.edoracceptno   and sp.GetNoticeNo = cp.TempFeeNo  "
					+date+" " 
					+state2+" " 
					+feeType1+" " 
					+com+" " 
					+group2+" "
					+usercode2+" "                                                                   
					+"union   "
					+"select sp.otherno 受理号,  "
					+"(select name from ldcom where comcode=lp.managecom) 受理机构, "
					+" (select username from lduser where usercode=(select acceptorno from lgwork where workno=sp.otherno)) 受理人,  "
					+" (select workTypeName from LGworkType where workTypeNo=(select typeno from lgwork where workno=sp.otherno)) 业务类型,"
					+" (select username from lduser where usercode=(select operator from lgwork where workno=sp.otherno)) 处理人, "
					+"sp.MakeDate 提交日期, "
					+"0 应收金额, "
					+"abs(SumGetMoney) 付费金额, "
					+"0 实收金额,  "
					+"(select codename from ldcode where codetype='paymode'and code=paymode) 收付方式,  "
					+"(select bankname from ldbank where  bankcode=sp.BankCode) 转帐银行,   "
					+" sp.bankaccno 账号,"
					+" sp.accname 帐户所有人 ,"
					+"  '' 收费期限,   "
					+"'未完成' 收付状态  "
					+"from ljsget sp,lpedorapp lp   "
					+"where sp.othernotype in ('3','10') and sp.PayMode not in ('B','J') and sp.otherno=lp.edoracceptno  "
					+" and not exists(select 1 from LGWork where workno = lp.EdorAcceptNo and statusNo = '8') "
					+date+" " 
					+state1+" " 
					+feeType2+" " 
					+com+" " 
					+group1+" "
					+usercode1+" "                                                                    
					+"union   "
					+"select sp.otherno 受理号, "
					+"(select name from ldcom where comcode=lp.managecom) 受理机构, "
					+"(select username from lduser where usercode=(select acceptorno from lgwork where workno=sp.otherno)) 受理人,"
					+"(select workTypeName from LGworkType where workTypeNo=(select typeno from lgwork where workno=sp.otherno)) 业务类型, " 
					+"(select username from lduser where usercode=(select operator from lgwork where workno=sp.otherno)) 处理人, "
					+"sp.MakeDate 提交日期,"
					+"0 应收金额,"
					+"abs(SumGetMoney) 付费金额, "
					+"0 实收金额, "
					+"(select codename from ldcode where codetype='paymode'and code=PayMode) 收付方式,  "
					+"(select bankname from ldbank where  bankcode=sp.BankCode) 转帐银行,  "
					+" sp.bankaccno 账号,"
					+" sp.accname 帐户所有人 ,"
					+" '' 收费期限, "
					+"'结束' 收付状态 "
					+"from ljaget sp,lpedorapp lp where sp.othernotype in ('3','10') and sp.PayMode not in ('B','J')  "
					+"and sp.otherno=lp.edoracceptno  "
					+date+" " 
					+state2+" " 
					+feeType2+" " 
					+com+" " 
					+group1+" "
					+usercode1+" "                                                                    
					+"order by 受理号  "
					;
				  
	System.out.println("打印查询:"+tSQL);
	
    //设置表头
    String[][] tTitle = {{"受理号", "受理机构", "受理人", "业务类型", "处理人", "提交日期", "应收金额", "付费金额", "实收金额", "收付方式", "转帐银行","账号","帐户所有人", "收费期限", "收付状态"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(tSQL,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>