<% 
//程序名称：
//程序功能：个人保单迁移
//创建日期：2007-9-28 10:52
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK"> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./PEdorTypePR.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypePRInit.jsp"%>
  <title>个人保单迁移 </title> 
</head>
<script>
var comsql = "1  and (comgrade=#03# or ((comgrade is null or comgrade =##) and length(trim(comcode))=#8#)) and comcode<>#86000000# ";
</script>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit"> 
  <table class=common>
    <TR class= common> 
      <TD class= title > 投保日期 </TD>
      <TD class= input>
        <input class="readonly" type="text" readonly name=PolApplyDate >
      </TD>
      <TD class = title > 生效日期 </TD>
      <TD class = input >
      	<input class = "readonly" type="text" readonly name=CValiDate>
      </TD>
      <TD class = title > 交至日期 </TD>
      <TD class = input >
      	<input class = "readonly" type="text" readonly name=PaytoDate>
      </TD>   
    </TR>
    <TR class= common> 
    	  <TD class= title > 迁移机构选择 </TD>
 		</TR>
 		<TR class= common>
    	 <TD class= title > 迁入机构 </TD>
      <TD class= input>
        <!--  <Input class=codeNo name="InCom" ondblclick="return showCodeList('comcodeallsign',[this,ManageComName],[0,1],null,'03','comgrade',1);" onkeyup="return showCodeListKey('comcodeallsign',[this,ManageComName],[0,1],null,'03','comgrade',1); " readonly><input class=codename name=ManageComName readonly=true >-->
        <Input class=codeNo name="InCom" ondblclick="return showCodeList('comcodeallsign',[this,ManageComName],[0,1],null,comsql,'1',1);" onkeyup="return showCodeListKey('comcodeallsign',[this,ManageComName],[0,1],null,comsql,'1',1); " readonly><input class=codename name=ManageComName readonly=true >  
      </TD>
    </TR>
    <TR>
    	<TD class= title>迁移后保单联系地址</TD>
    	<TD class= input colspan=3><Input class= common3 name=Postaladdress ></TD>
    	</TR>
    	 <TR>
    	<TD class= title>联系地址邮编</TD>
    	    <TD  class= input>
            <Input class= common name="ZipCode" verify="投保人联系邮政编码|zipcode" >
          </TD>  
          <TD class= title>联系电话</TD>
    	    <TD  class= input>
            <input class= common name="Phone" verify="投保人联系电话|NUM&len<=18" >
          </TD>
    	</TR>
    	  </TABLE>
    	  
    	  <DIV id= "DivInInfo">
				<table class=common>
				 <TR class= common> 
    	      <TD class= input > 机构确认 </TD>
      			<TD class= input>
        		<Input class=codeNo name=ConfManageCom ondblclick="return showCodeList('comcode',[this,ConfManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ConfManageComName],[0,1],null,null,null,1);"><input class=codename name=ConfManageComName readonly=true >  
      			</TD>
   				</TR>
    			<TR class= common> 
    	      <TD class= input > 服务业务员指定 </TD>
      			<TD class= input>
        			<Input class=codeNo name=AgentCode ondblclick="return showCodeList('agentCode',[this,AgentCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('agentCode',[this,AgentCodeName],[0,1],null,null,null,1);"><input class=codename name=AgentCodeName readonly=true ><Input class= cssButton type=Button value="查  询" onclick="queryAgent()">
       			</TD>
    		</TR>
  			</TABLE>
			</DIV>
	  <hr>
		<Input class= cssButton type=Button id='save'  value="保  存" onclick="saveButton()">
		<Input class= cssButton type=Button id='sendLetter'  value="函件下发" onclick="letter()">
		<Input class= cssButton type=Button id='removeOut' value="迁出处理" onclick="removeOutButton()">
		<Input class= cssButton type=Button id='removeIn' value="迁入处理" onclick="removeInButton()">
		<Input class= cssButton type=Button id='hosList'  value="迁入机构定点医院清单" onclick="printHospitalListButton()">
		<input type=hidden id="TypeFlag" name="TypeFlag">
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="ContType" name="ContType">
		<input type=hidden name="EdorAcceptNo">
		<input type=hidden name="EdorType">
		<input type=hidden name="ContNo">
		<input type=hidden name="OutCom">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>