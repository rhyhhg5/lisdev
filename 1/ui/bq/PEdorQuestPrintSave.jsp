<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2006-02-20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<script>
<%
  String flag = "";
  String content = "";
  
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  String prtSeq = request.getParameter("PrtSeq");
	System.out.println("prtSeq" + prtSeq);
  PEdorUWF1PUI tPEdorUWF1PUI = new PEdorUWF1PUI(gi, prtSeq);
  if (!tPEdorUWF1PUI.submitData())
  {
    out.print("alert('通知书显示错误!原因是" + tPEdorUWF1PUI.getError() +"');");
  }
	
  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
  ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
  CombineVts combineVts = new CombineVts(tPEdorUWF1PUI.getInputStream(), templatePath);
  combineVts.output(dataStream);
  session.putValue("PrintVts", dataStream);
	session.putValue("PrintStream", tPEdorUWF1PUI.getInputStream());
	response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
%>
</script>