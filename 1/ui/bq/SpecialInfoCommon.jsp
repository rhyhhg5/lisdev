<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.text.*"%>

<%
  boolean specialRiskFlag = false;
  String edorNo = request.getParameter("EdorNo");
  String edorType = request.getParameter("EdorType"); 
  String grpContNo = request.getParameter("GrpContNo");
  if ((grpContNo != null) && (!grpContNo.equals("")))
  {
      specialRiskFlag  = CommonBL.hasSpecialRisk(grpContNo);
      //判断合同下是否有特需的险种
      if (specialRiskFlag)
      {
      System.out.println("Specialrisk start!");
          //得到帐户信息
          String rate = "";
          String cValiDate = "";
          String accNum = null;
          String accMoney = null;
          String grpAccMoney = null;
          String fixedAccMoney = null;
          String remark = null;
        
          LCGrpContDB tLCGrpContDB = new LCGrpContDB();
          tLCGrpContDB.setGrpContNo(grpContNo);
          if (tLCGrpContDB.getInfo())
          {
            cValiDate = StrTool.cTrim(tLCGrpContDB.getCValiDate());
            remark = StrTool.cTrim(tLCGrpContDB.getRemark());
          }						
          String mngFeeRateSql = "  select b.feeName, a.feeValue "
                              + "from LCGrpFee a, LMRiskFee b "
                              + "where a.feeCode = b.feeCode "
                              + "   and a.insuAccNo = b.insuAccNo "
                              + "   and a.payPlanCode = b.payPlanCode "
                              + "   and a.grpContNo = '" + grpContNo + "' "
                              + "order by a.feeCode ";
           ExeSQL tExeSQL = new ExeSQL();
          SSRS feeRate = tExeSQL.execSQL(mngFeeRateSql);
          if(feeRate != null && feeRate.getMaxRow() > 0)
          {
            for(int i = 1; i <= feeRate.getMaxRow(); i++)
            {
              rate += feeRate.GetText(i, 1) + "率：" 
                + Double.parseDouble(feeRate.GetText(i, 2)) + " ";
            }
          }
          String specialriskcodeSQL = "select b.riskcode,b.risktype3 from lcgrppol a,lmriskapp b where grpcontno = '"
          							+ grpContNo +"' and a.riskcode = b.riskcode and (risktype3='7' or risktype8='6') with ur ";	
          SSRS specialRiskcode =  tExeSQL.execSQL(specialriskcodeSQL);
			System.out.println("查询险种:"+specialriskcodeSQL);      					
          //得到人账户总金额及账户数目
          String sql = "select count(*), sum(insuAccBala) from LCPol a, LCInsureAcc b " +
                       "where a.polNo = b.polNo " + 
                       "and a.grpContNo = b.grpContNo " + 
                       "and a.polTypeFlag != '" + BQ.POLTYPEFLAG_PUBLIC + "' " +
                       "and InsuAccNo = '" + CommonBL.getInsuAccNo(BQ.ACCTYPE_INSURED,specialRiskcode.GetText(1,1)) +"' " +
                       "and a.GrpContNo = '" + grpContNo + "'";
                       System.out.println("得到人账户总金额及账户数目:"+sql);
          SSRS tSSRS = tExeSQL.execSQL(sql);
          if(tSSRS==null||tSSRS.getMaxRow()<1)
          {
          	accNum ="0";
          	accMoney ="null";
          }else{
          accNum = StrTool.cTrim(tSSRS.GetText(1, 1));
          accMoney = StrTool.cTrim(tSSRS.GetText(1, 2));
          }
          
          if(accMoney.equals("null")||accMoney.equals(""))
          {
            accMoney = "0";
          }
          //得到团体账户金额
          sql = "select insuAccBala from LCPol a, LCInsureAcc b " +
                "where a.PolNo = b.PolNo " +
                "and a.GrpContNo = b.GrpContNo " +
                "and a.PolTypeFlag = '" + BQ.POLTYPEFLAG_PUBLIC + 
                "' and a.acctype = '" + BQ.ACCTYPE_PUBLIC + 
                "' and b.InsuAccNo = '" + CommonBL.getInsuAccNo(BQ.ACCTYPE_GROUP,specialRiskcode.GetText(1,1)) + "' " +
                "and a.grpContNo = '" + grpContNo + "' ";
          tExeSQL = new ExeSQL();
          grpAccMoney = tExeSQL.getOneValue(sql);
          
          if(grpAccMoney==null||grpAccMoney.equals("null")||grpAccMoney.equals(""))
          {
            grpAccMoney = "0";
          }
          //得到团体固定帐户金额
          
          String tRiskType3 = specialRiskcode.GetText(1,2);
          String acctypePart = null;
          if(tRiskType3.equals("7"))
          {
              acctypePart = " and a.acctype = '"+BQ.ACCTYPE_PUBLIC+"' ";
          }
          else
          {
              acctypePart = " and (a.acctype <> '"+BQ.ACCTYPE_PUBLIC+"' or a.acctype is null) ";
          }
          sql = "select insuAccBala from LCPol a, LCInsureAcc b " +
                "where a.PolNo = b.PolNo " +
                "and a.GrpContNo = b.GrpContNo " +
                "and a.PolTypeFlag = '" + BQ.POLTYPEFLAG_PUBLIC + "' " + 
                acctypePart +
                "and b.InsuAccNo = '" + CommonBL.getInsuAccNo(BQ.ACCTYPE_FIXED,specialRiskcode.GetText(1,1)) + "' " +
                "and a.GrpContNo = '" + grpContNo + "' ";
          tExeSQL = new ExeSQL();
          fixedAccMoney = tExeSQL.getOneValue(sql);
          
          if(fixedAccMoney==null||fixedAccMoney.equals("null")||fixedAccMoney.equals(""))
          {
            fixedAccMoney = "0";
          }
          DecimalFormat tDecimalFormat = new DecimalFormat("0.00");
//          System.out.println(Double.parseDouble(grpAccMoney));	
          String totalAccMoney = tDecimalFormat.format(
              Double.parseDouble(String.valueOf((Double.parseDouble(accMoney) 
                + Double.parseDouble(grpAccMoney) 
                + Double.parseDouble(fixedAccMoney)))));
          System.out.println("qulq sdkfjsdklfjdsaklfjsadlk;fjafkjasf;jdkfas;");	
          if(totalAccMoney==null||totalAccMoney.equals("null")) 
          {
          	totalAccMoney = "0";
          }
          System.out.println("\n\n\n\n\n\n" + totalAccMoney);
          String s = (new DecimalFormat("0.00")).format(Double.parseDouble(totalAccMoney)); 
          System.out.println("\n\n\n\n\nfffff: "+s);
            
        //得到特需险种号grppolno
                   
          String  polnosql="select grppolno "
          									+	"from lcgrppol "
          									+ "where riskcode='"
          									+specialRiskcode.GetText(1,1) 
													  + "' and grpContNo = '" + grpContNo + "' ";
				
				 String GrpPolNo=tExeSQL.getOneValue(polnosql);    
        %>
        <script>
          function insuredList()
          {
            window.open("../sys/GrpInsuredInput.jsp?ContNo=<%=grpContNo%>&ModeFlag=0&ContLoadFlag=2&ContType=1");
          }
        </script>
       
    <script>    
       	 function grpSpecQuery()
					{
													
						window.open("../sys/GrpSpecialAccQuery.jsp?ContNo=<%=grpContNo%>&ContType=1&RiskCode=<%=specialRiskcode.GetText(1,1)%>&GrpPolNo=<%=GrpPolNo%>");
					}
			</script>
         <table>
            <tr>
              <td><IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSpecialInfo);"></td>
              <td class= titleImg>特需医疗帐户信息</td>
              <td><INPUT class=CssButton VALUE="特需医疗事项查询" TYPE=button onclick="grpSpecQuery();"> </td>
              <td><input type=Button class=cssButton value="被保人清单" onclick="insuredList();"></td>
            </tr>
          </table>
          <div id="divSpecialInfo" style="display: '' ">
            <table class="common">
              <tr class="common">
                <td class="title">账户管理费率</td>
                <td class="input" colspan=5>
                  <%=rate%><!--input class="readonly" readonly name="Rate" style="display: 500" value="<%=rate%>"-->
                </td>
              </tr>
              <tr class="common">
                <td class="title">投保日期</td>
                <td class="input">
                  <input class="readonly" readonly name="CValiDate" value="<%=cValiDate%>">
                </td>
                <td class="title">个人账户总数</td>
                <td class="input" colspan=3>
                  <input class="readonly" readonly name="AccNum" value="<%=accNum%>">
                </td>
              </tr>
              <tr class="common">
                <td class="title">账户总金额</td>
                <td class=input colspan=5>
                  <input class="readonly" readonly name="TotalAccMoney" value="<%=totalAccMoney%>">
                </td>
              </tr>
              
              <tr class="common">
                <td class="title">个人账户总金额</td>
                <td class="input">
                  <input class="readonly" readonly name="AccMoney" value="<%=accMoney%>">
                </td>
                <td class="title">团体账户金额</td>
                <td class="input">
                  <input class="readonly" readonly name="GrpAccMoney" value="<%=grpAccMoney%>">
                </td>
                <td class="title">团体固定帐户金额</td>
                <td class="input">
                  <input class="readonly" readonly name="FixedAccMoney" value="<%=fixedAccMoney%>">
                </td>
              </tr>
              <tr class="common">
                <td class="title">特别约定</td>
                <td class="input" colspan="7">
                  <textarea class="readonly" readonly style="border: none; overflow: hidden; background-color: #F7F7F7;" name="Remark" value="<%=remark%>" cols="90%" rows="3"></textarea> 
                </td>
              </tr>
            </table>
          </div>
          
<%
            if ((edorType != null) && (edorType.equals(BQ.EDORTYPE_NI)))
            {
%>
        <script>
        function afterCodeSelect(cCodeName, Field) 
        {
          if (cCodeName == "CreateAccFlag")
          {
            if (fm.CreateAccFlag.value == "1")
            {
               document.all("trSetAcc").style.display = "";
            }
            else if (fm.CreateAccFlag.value == "2")
            {
              document.all("trSetAcc").style.display = "none";
              fm.InputType.value = "";
              fm.InputTypeName.value = "";
              fm.Source.value = "";
              fm.SourceName.value = "";
              fm.Prem.value = "";
            }
          }
          if (cCodeName == "InputType")
          {
            if (fm.InputType.value == "1")
            {
              document.all("tdPrem1").style.display = "";
              document.all("tdPrem2").style.display = "";
            }
            else if (fm.InputType.value == "2")
            {
              document.all("tdPrem1").style.display = "none";
              document.all("tdPrem2").style.display = "none";
              fm.Prem.value = "";
            }
          }
        } 
        </script>
           <table>
              <tr>
                <td><IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divPremSet);"></td>
                <td class= titleImg>个人账户资金设置</td>
              </tr>
            </table>
            <div id="divPremSet">
              <table class="common">
                <tr class="common">
                  <td class="title">是否设置个人账户</td>
                  <td class="input" colspan="7">
                    <input class="codeNo" name="CreateAccFlag" value="1" CodeData="0|^1|是^2|否"  ondblclick="return showCodeListEx('CreateAccFlag',[this,CreateAccFlagName],[0,1]);" onkeyup="return showCodeListEx('CreateAccFlag',[this,CreateAccName],[0,1]);"><Input class="codeName" name="CreateAccFlagName" readonly>
                  </td>
                </tr>
                <tr class="common" id="trSetAcc">
                  <td class="title">个人账户资金设置方式</td>
                  <td class="input">
                    <input class="codeNo" name="InputType" value="1" CodeData="0|^1|统一设置^2|清单导入"  ondblclick="return showCodeListEx('InputType',[this,InputTypeName],[0,1]);" onkeyup="return showCodeListEx('InputType',[this,InputTypeName],[0,1]);"><Input class="codeName" name="InputTypeName" readonly>
                  </td>
                  <td class="title" id="tdSource1">个人账户资金来源</td>
                  <td class="input" id="tdSource2">
                    <input class="codeNo" name="Source" value="1" CodeData="0|^1|追加保费^2|团体账户转入"  ondblclick="return showCodeListEx('Source',[this,SourceName],[0,1]);" onkeyup="return showCodeListEx('Source',[this,SourceName],[0,1]);"><Input class="codeName" name="SourceName" readonly>
                  </td>
                  <td class="title" id="tdPrem1">每人转入保费金额</td>
                  <td class="input" id="tdPrem2">
                    <input class="common" name=Prem>
                  </td>
                </tr>
              </table>
            </div>
<%
            }
            if ((edorType != null) && (edorType.equals(BQ.EDORTYPE_ZT)))
            {
%>
              <div div="divZT">
                <table class=common>
                  <tr class=common>
                    <td class=title>余额退还方式
                    </td>
                    <td class=common colspan="4">
                      <input class="codeNo" name="BalaPayWay" readOnly value="1" CodeData="0|^1|转入团体帐户^2|退还投保单位"  ondblclick="return showCodeListEx('InputType',[this,BalaPayWayName],[0,1]);" onkeyup="return showCodeListEx('InputType',[this,BalaPayWayName],[0,1]);"><Input class="codeName" name="BalaPayWayName" readonly value="转入团体帐户">
                    </td>
                  </tr>
                </table>
              </div>
              <script language = "javascript">
                var sql = "  select edorValue "
                          + "from LPEdorEspecialData "
                          + "where edorNo = '<%=edorNo%>' "
                          + "   and edorType = '<%=BQ.EDORTYPE_ZT%>' "
                          + "   and detailType = '<%=BQ.DETAILTYPE_BALAPAYTYPE%>' ";
                var result = easyExecSql(sql);
                if(result)
                {
                  fm.BalaPayWay.value = result[0][0];
                  fm.BalaPayWayName.value = (result[0][0] == "1" ? "转入团体帐户" : "退还投保单位");
                }
              </script>
<%
            }  //(edorType != null) && (edorType.equals(BQ.EDORTYPE_ZT))
        } //CommonBL.hasSpecialRisk(grpContNo)
    } //grpContNo != null && !grpContNo.equals("")
%>