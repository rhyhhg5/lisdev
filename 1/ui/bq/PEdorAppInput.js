var showInfo;
var mDebug="1";
var aEdorFlag='0';
var mEdorType;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPageCont = new turnPageClass(); 
var turnPagePol = new turnPageClass();
var turnPageEdor = new turnPageClass();
var turnPageIns = new turnPageClass();
var queryFlag = "N";


function initEdorType(cObj)
{
	var tOtherNo = fm.all('OtherNo').value;
	var tOtherNoType = fm.all('OtherNoType').value;
	
	if (LCContGrid.getSelNo() == 0)
	{
	  alert("请先选择一个保单！");
	  return false;
	}
	
	//mEdorType = " 1 and AppObj=#I#";
	//if (tOtherNoType=='3')//保单号申请
	//{
	//	mEdorType=mEdorType+" and riskcode in (select riskcode from lcpol where ContNo=#"+tOtherNo+"# ";
	//	mEdorType=mEdorType+" and SaleChnl in(select #01# from dual where #@#>=#D# union select #02# from dual union select #03# from dual)) "; 
	//	mEdorType=mEdorType+" and (EdorPopedom is not null and  EdorPopedom<=#@#)";
  //}
//	else if (tOtherNoType=='1')//客户号申请
//	{
//		mEdorType=mEdorType+" and riskcode in (select riskcode from lcpol where AppntNo=#"+tOtherNo+"# ";
//		mEdorType=mEdorType+" and SaleChnl in(select #01# from dual where #@#>=#D# union select #02# from dual union select #03# from dual)) "; 
//		mEdorType=mEdorType+" and (EdorPopedom is not null and  EdorPopedom<=#@#)";
//	}
	showCodeList('EdorCode',[cObj, fm.EdorTypeName], [0,1], null, fm.ContNo1.value, "1");
}
 
/*********************************************************************
 *  选择批改项目后的动作
 *  参数： codeName, feild
 *********************************************************************
 */
function afterCodeSelect(codeName, feild)
{
	if (codeName == "EdorCode")	
	{
		var edorType = feild.value;
		var strSQL = "select DisplayFlag from LMEdorItem " +
		             "where EdorCode = '" + edorType + "' " +
		             "and AppObj = 'I'";
		var tDisplayType = easyExecSql(strSQL);
		if ((tDisplayType == null) || (tDisplayType == ""))
		{
	    alert("未查到该保全项目显示级别！");    
	    return;
		}
		
		fm.all("DisplayType").value = tDisplayType;
		if (tDisplayType == "1")//只显示保单
		{
	    divInsuredInfo.style.display='none';
	    divPolInfo.style.display='none';  
		}
		else if (tDisplayType == "2")//需要显示客户
		{
	    initInusredGrid();
	    divInsuredInfo.style.display='';
	    var tSelNo=LCContGrid.getSelNo();
	    getInsuredInfo(LCContGrid.getRowColData(tSelNo-1, 1));
		}
		else if (tDisplayType == "3")//需要显示险种
		{
	    initPolGrid();
	    divInsuredInfo.style.display='';
	    divPolInfo.style.display='';  
		}
		
		//设置保全生效日期 
		if (edorType == "AC" || edorType == "BB" || edorType == "BC" || edorType == "IA" || edorType == "CC") 
		{
		    fm.all('EdorValiDate').value = fm.all('dayAfterCurrent').value;
		} 
		else if (edorType == "WT" || edorType == "ZT" || edorType == "GT") 
		{
		    fm.all('EdorValiDate').value = fm.all('currentDay').value;
		} 
		else 
		{
			fm.all('EdorValiDate').value = fm.all('currentDay').value;
		}	
	}
}

/*********************************************************************
 *  查看保单明细
 *********************************************************************
 */       
function viewContInfo()
{
	var tSelNo = LCContGrid.getSelNo();
	if (tSelNo == 0)
	{
		alert("请选择需要查询的保单!");
	}
	else
	{
		var tContNo = LCContGrid.getRowColData(tSelNo - 1, 1);
		var state = LCContGrid.getRowColDataByName(tSelNo - 1, 'state');
		var win = window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + tContNo + "&IsCancelPolFlag=0&LoadFlag=TASK&ContType="+state);
		win.focus();
	}
}

function CtrlEdorType(tEdorType)
{
	tEdorNo=fm.all('EdorNo').value;
	if (tEdorNo!=null&&tEdorNo!='')
	{
		divedortype.style.display = '';
	}
	else
	{
		divedortype.style.display = 'none';
	}
	easyQueryClick(tEdorType);
}

//提交，保存按钮对应操作
function submitForm()
{
	var tOtherNo = fm.all('OtherNo').value;
	var tOtherNoType = fm.all('OtherNoType').value;
	fm.fmtransact.value = "INSERT||EDORAPP";
	                 
	if (tOtherNo==null||tOtherNo ==''||tOtherNoType==null||tOtherNoType =='')
	{
	  alert('请输入申请号码类型及申请号码！');
	  return;    
	}                                                         
	if (fm.all('EdorAcceptNo').value!='')
	{
	  alert('该保全受理已经申请，不能重复申请！');
	  return;    
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PEdorAppSave.jsp";
	fm.submit(); //提交
}

function afterSubmit1(FlagStr, content)
{
  try{ showInfo.close(); } catch (e) {};
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
		emptyUndefined();	//将所有值为undefined的输入框置为""
  	var tTransact = fm.all('fmtransact').value;
  	
		divappconfirm.style.display ='';
		if (tTransact=="QUERY||EDOR")
		{
			 var tEdorType = fm.all('EdorType').value;
			 CtrlEdorType(tEdorType);
		}
		else
		{
		}
  }
}

//打印批单
function PrtEdor(prtParams)
{
  var url = "../bq/PEdorConfirmMain.jsp?LoadFlag=EDORAPPCONFIRM&EdorAcceptNo=" + fm.all("EdorAcceptNo").value;
	top.document.all("fraInterface").src = url;
	//top.document.all("fraInterface").src = url;
	
	//var url = "./PEdorConfirmMain.jsp?LoadFlag=EDORAPPCONFIRM&EdorAcceptNo=" + fm.all("EdorAcceptNo").value;
	//window.open(url);
	//top.close();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
  try {showInfo.close();} catch(re) {}
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    getEdorItem();
    if (fm.all('fmtransact').value == 'INSERT||EDORAPP')
    {
    	queryLCCont();
    }
    if (fm.all('fmtransact').value == 'INSERT||EDORAPPCONFIRM')
    {  
    	PrtEdor();
    }
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr, content)
{
	try {showInfo.close();} catch(re) {}
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
		var getMoney=HaveFee(fm.EdorAcceptNo.value);

		if(getMoney=="")//查询出错
		{
			return;
		}

		//if(Number(getMoney)==0)
		//{
			edorAppAccConfirm();
		//	return;
		//}else
		{
		//	var ret=AppAccExist(fm.OtherNo.value);
		//	if(ret=="")
		//	{
		//		return;
		//	}
		//	if(ret=="No")
		//	{
		//		edorAppAccConfirm();
		//		return;
		//	}
		//	if(ret=="Yes")
		//	{
		//		if(Number(getMoney)>0)
		//		{
		//			var accMoney=AppAccHaveGetFee(fm.OtherNo.value);
		//			if(accMoney=="")
		//			{
		//				return;
		//			}
		//			if(Number(accMoney)>0)
		//			{
		//				var urlStr="BqAppAccConfirmMain.jsp?ContType=0&AccType=0&EdorAcceptNo="+fm.EdorAcceptNo.value+"&CustomerNo="+fm.OtherNo.value+"&DestSource=01";
		//				showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:200px");	
		//			}
		//			edorAppAccConfirm();
		//			return;
		//		}
		//		if(Number(getMoney)<0)
		//		{
		//			var urlStr="BqAppAccConfirmMain.jsp?ContType=0&AccType=1&EdorAcceptNo="+fm.EdorAcceptNo.value+"&CustomerNo="+fm.OtherNo.value+"&DestSource=11";
		//			showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:200px");	
		//			edorAppAccConfirm();
		//			return;
		//		}	
		//	}
		}		
	}
}

//是否有补退费
function HaveFee(EdorAcceptNo)
{
	var strSQL ="select getmoney from LPEdorApp where EdorAcceptNo='"+EdorAcceptNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询保全申请主表时出错，保全受理号为"+EdorAcceptNo+"！");
		return "";
	}
	return arrResult[0][0];
}

//帐户余额是否有可领金额是否大于0
function AppAccHaveGetFee(customerNo)
{
	var strSQL ="select accgetmoney from lcappacc where customerNo='"+customerNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询投保人帐户表时出错，客户号为"+customerNo+"！");
		return "";
	}
	return arrResult[0][0];
}

//投保人帐户是否存在
function AppAccExist(customerNo)
{
	var strSQL ="select count(1) from lcappacc where customerNo='"+customerNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询投保人帐户表时出错！");
		return "";
	}
	if(Number(arrResult[0][0])==0)
	{
		return 'No';
	}
	else
	{
		return 'Yes';
	}
}

//保全理算后打印
function edorAppAccConfirm()
{
	
	fm.all("fmtransact").value = "INSERT||EDORAPPCONFIRM";	
	fm.action = "./BqAppAccAfterConfirmSubmit.jsp";
	fm.submit();
}

//保全理算
function edorAppConfirm()
{
	//add by chenlei 2008-4-24 未添加保全项目不能进行理算操作
	var strSQL=" select * from lpedoritem where edoracceptno='"+fm.all('EdorAcceptNo').value+"'";
	var arrResult = easyExecSql(strSQL);
	if(!arrResult)
	{
	  alert("未添加保全项目，不能进行保全理算！");	
	  return false;
	}
	//add by chenlei 2008-4-24 本次保全申请，若有保全项目未录入完毕，不能进行保全理算
	var strSQL=" select 'x' from lpedoritem where edoracceptno='"+fm.all('EdorAcceptNo').value+"' and EdorState='3'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult)
	{
	  alert("本次申请，有保全项目处于未录入状态，不能进行保全理算！");	
	  return false;
	}
	fm.all("fmtransact").value = "INSERT||EDORAPPCONFIRM";
	if (window.confirm("是否理算本次保全申请?"))
	{
		var showStr="正在计算数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.action = "./PEdorAppConfirmSubmit.jsp";
		fm.submit();
	}
}    
      
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv, cShow)
{
  if (cShow == "true")
  {
    cDiv.style.display = "";
  }
  else
  {
    cDiv.style.display = "none";  
  }
}


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：arrQueryResult  查询返回的二维数组
 *********************************************************************
 */
function afterQuery(arrQueryResult)
{
}

// 查询按钮
function easyQueryClick(tEdorType)
{
	tRiskCode = fm.all('RiskCode').value;
	//tEdorType = fm.all('EdorType').value;
	
	//verifyInput();
	if (tEdorType==null||tEdorType=='')
	{
				alert("请选择批改项目!");
				return;
	}
	if (tRiskCode==null||tRiskCode=='')
	{
				alert("险种为空,请重新查询!");
				return;
	}
	else
	{
		// 书写SQL语句
		var strSQL = "";
		strSQL = "select count(*) from lmriskedoritem where riskcode ='"+tRiskCode+"' and edorcode='"+tEdorType+"' and AppObj='I' and needdetail='1'";		 
		//查询SQL，返回结果字符串
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
		
		//判断是否查询成功
		if (!turnPage.strQueryResult) 
		{
		  alert("查询失败！");
		}
		else
		{
		  //查询成功则拆分字符串，返回二维数组
		  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  	  
			if (turnPage.arrDataCacheSet[0][0]>0)
			{
				divdetail.style.display = '';
			}
			else
			{
				divdetail.style.display = 'none';
			}
		}
	}
}

/*********************************************************************
 *  查询客户投保信息
 *********************************************************************
 */
function queryLCCont()
{
	var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
	var tOtherNo = fm.all('OtherNo').value;
	var tOtherNoType = fm.all('OtherNoType').value;

	if (tOtherNo == null || tOtherNo =='' || tOtherNoType==null || tOtherNoType =='') 
	{
		if (tEdorAcceptNo != null)
		{
			var strSQL = "select OtherNo, OtherNoType from LPEdorApp " +
			             "where EdorAcceptNo = '" + tEdorAcceptNo + "' ";
			var arrResult = easyExecSql(strSQL);
			tOtherNo = arrResult[0][0];
			tOtherNoType = arrResult[0][1];
		}
	}

  if (tOtherNoType=='3')
  { 
    var strSQL= "select contno,state,AppntName,InsuredName,CValiDate,PaytoDate,PayIntv,prem,getUniteCode(a.agentcode),grpcontno,laagent.name"
            +" ,codename('stateflag',a.stateflag) "
            + " from lccont a, laagent "
            + " where contno='"+ tOtherNo + "' "
            + " and appflag='1' "
            + " and a.agentcode=laagent.agentcode "
            + " and (a.StateFlag is null or a.StateFlag not in('0')) ";		
  }
  else if (tOtherNoType=='1')
  {
    var strSQL= "select contno,'1',AppntName,InsuredName,CValiDate,PaytoDate,PayIntv,prem,getUniteCode(a.agentcode),grpcontno,(select laagent.name from laagent where a.agentcode=laagent.agentcode) "
            +" ,codename('stateflag',a.stateflag) "
            + " from lccont a "
            + " where appntno='"+ tOtherNo + "' "
            + " and appflag='1' and conttype ='1' "
            + "  and (a.StateFlag is null or a.StateFlag not in('0')) "
            + " union "
						+ " select contno,'2',AppntName,InsuredName,CValiDate,PaytoDate,PayIntv,prem,getUniteCode(a.agentcode),grpcontno,(select laagent.name from laagent where a.agentcode=laagent.agentcode) "
						+" ,codename('stateflag',a.stateflag) "
						+ " from lbcont a "
						+ " where appntno='"+ tOtherNo + "' "
						+ " and conttype ='1' and (a.StateFlag is null or a.StateFlag not in('0')) "
						+ " and exists ( "
						+ " select 1 from lpedoritem where edortype in ('WT','CT','XT') and edorno = a.edorno and contno = a.contno ) "
            ;		
  }
  if (tOtherNo)
  {
    turnPageCont.pageLineNum = 10;
  	turnPageCont.queryModal(strSQL, LCContGrid);
  }
}

/*********************************************************************
 *  查询团体保全申请信息
 *********************************************************************
 */
function queryEdorAppInfo()
{
    //查询险种信息
    queryLCCont();
    //查询保全项目
    if (getEdorItem())
    {
    	divappconfirm.style.display = '';
    }
}

/*********************************************************************
 *  查询团体保全申请信息
 *********************************************************************
 */
function queryEdorApp()
{
  try
  {
    var i = 0;
    var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
    var tContNo = fm.all('ContNo').value;
    var showStr;
    var urlStr;
    	
    
    //查询保全申请信息
    var strSQL= "select * from LPEdorApp where EdorAcceptNo='"
                + tEdorAcceptNo+"'";		
    //alert("strSQL"+strSQL);        
    
    arrResult = easyExecSql(strSQL);    
    //alert("arrResult"+arrResult);
    if (arrResult == null) 
    {
                alert("未查到保全申请号为"+tEdorAcceptNo+"的保全申请信息！");
    } 
    else
    {
        try {fm.all('EdorAcceptNo').value= arrResult[0][0]; } catch(ex) { }; //保全受理号
        try {fm.all('OtherNo').value= arrResult[0][1]; } catch(ex) { }; //申请号码
        try {fm.all('OtherNoType').value= arrResult[0][2]; } catch(ex) { }; //申请号码类型
        try {fm.all('EdorAppName').value= arrResult[0][3]; } catch(ex) { }; //申请人名称
        try {fm.all('AppType').value= arrResult[0][4]; } catch(ex) { }; //申请方式
    }
  }
  catch(ex)
  {
      alert("在GEdorInput.js-->queryEdorApp函数中发生异常:"+ex);
  }
}  
/*********************************************************************
 *  查询批改主表信息
 *********************************************************************
 */
 function queryEdorMain()
 { 
    var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
    var tEdorNo = fm.all('EdorNo').value;
    
    //查询批改主表信息
    var strSQL= "select ChgPrem,GetMoney,EdorAppDate,EdorValiDate,Operator from LPEdorMain where EdorAcceptNo='"
                + tEdorAcceptNo + "' and EdorNo='"+tEdorNo+"'";		      
    
    arrResult = easyExecSql(strSQL);    
    if (arrResult == null) 
    {
    } 
    else
    {
        try {fm.all('ChgPrem').value= arrResult[0][0]; } catch(ex) { }; //变动的保费
        try {fm.all('GetMoney').value= arrResult[0][1]; } catch(ex) { }; //补/退费金额
        try {fm.all('EdorAppDate').value= arrResult[0][2]; } catch(ex) { }; //批改申请日期
        try {fm.all('EdorValiDate').value= arrResult[0][3]; } catch(ex) { }; //批改生效日期
        try {fm.all('Operator').value= arrResult[0][4]; } catch(ex) { }; //操作员
    }
}

/*********************************************************************
 *  查询保全项目，写入MulLine
 *********************************************************************
 */       
function getEdorItem()
{
  var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
  var tContNo = fm.all('ContNo').value;
  var tedorType = fm.EdorType.value;
  //险种信息初始化
  if (tedorType=="RB")
  {
  		var strSQL =" select a.EdorAcceptNo, a.EdorNo, a.EdorType, a.ContNo, (select appntno  from lbappnt where contno=a.contno), a.InsuredNo " 
  							 +" ,a.EdorValiDate, case a.EdorState when '1' then '录入完成' when '3' then '待录入' when '0' then '已提交' when '4' then '试算完成' when '2' then '理算完成'  end,a.polno "
  							 +" from lpedoritem a where edorno ='"+tEdorAcceptNo+"'"
  							 +" union "
		             +" select a.EdorAcceptNo, a.EdorNo, a.EdorType, a.ContNo, (select appntno  from lbappnt where contno=a.contno), a.InsuredNo " 
  							 +" ,a.EdorValiDate, case a.EdorState when '1' then '录入完成' when '3' then '待录入' when '0' then '已提交' when '4' then '试算完成' when '2' then '理算完成'  end,a.polno "
  							 +" from lpedoritem a where edorno ='"+tEdorAcceptNo+"'"
  							 ;
			turnPageEdor.queryModal(strSQL, EdorItemGrid);       							 
  }else
  	{
  		
		  if (tEdorAcceptNo != null)
		  {
		      var strSQL = "select a.EdorAcceptNo, a.EdorNo, a.EdorType, a.ContNo, b.AppntNo, a.InsuredNo, " +
		                 //  "      case a.InsuredNo when '000000' then '' else a.InsuredNo end,  " + 
		                   "      a.EdorValiDate, case a.EdorState when '1' then '录入完成' when '3' then '待录入' when '0' then '已提交' when '4' then '试算完成' when '2' then '理算完成'  end, " +
		                   "      a.PolNo " +
		                   "from LPEdorItem a, LCAppnt b " +
		                   "where a.ContNo = b.ContNo " +
		                   "and a.EdorAcceptNo = '" + tEdorAcceptNo + "' "
		                 +" union "
				             +" select a.EdorAcceptNo, a.EdorNo, a.EdorType, a.ContNo, (select appntno  from lbappnt where contno=a.contno), a.InsuredNo " 
		  							 +" ,a.EdorValiDate, case a.EdorState when '1' then '录入完成' when '3' then '待录入' when '0' then '已提交' when '4' then '试算完成' when '2' then '理算完成'  end,a.polno "
		  							 +" from lpedoritem a, LBAppnt b where 1=1 and a.ContNo = b.ContNo and a.EdorAcceptNo ='"+tEdorAcceptNo+"'"
  							 			;
		               //    +" select a.EdorAcceptNo, a.EdorNo, a.EdorType, a.ContNo, b.AppntNo, a.InsuredNo, a.EdorValiDate "
									 //		 +", case a.EdorState when '1' then '录入完成' when '3' then '待录入' when '0' then '已提交' when '4' then '试算完成' when '2' then '理算完成'  end,a.PolNo "
									 //		 +" from LPEdorItem a, LBAppnt b where '1205380064000'='1205380064000' and  a.ContNo = b.ContNo "
									 //		 +" and a.ContNo = '"+tContNo+"'  fetch first 3000 rows only "
		               //    ;
					turnPageEdor.queryModal(strSQL, EdorItemGrid);     
		  }
		}
  return true;
}

function checkClaim()
{
	var edorType = fm.EdorType.value;
	var contNo = fm.ContNo1.value;
	if (edorType == "TB") //投保事项变更
	{
		var sql = "select * from LCInsured a " +
		          "where exists (select * from LLCase " +
		          "     where CustomerNo = a.InsuredNo " +
		          "     and rgtstate not in ('11', '12', '14')) " +
		          "and ContNo = '" + contNo + "' ";
		var result = easyExecSql(sql);
		if (result)
		{
			if (!confirm("该保单正在理赔，是否继续操作！"))
			{
				return false;
			}
		}
	}
	return true;
}

/*********************************************************************
 *  添加保全项目
 *********************************************************************
 */
function addRecord()
{
	 

	  if(!checkState())
	  {
	    return false;
	  }
		if (fm.all('EdorValiDate').value==null || fm.all('EdorValiDate').value=="")
	  {
	    alert("保全生效日期不能为空!");
	    return false;
	  }
		
		
		if(fm.all('EdorType').value!=""){
		//校验保全项目
		var bqSQL = "select edoracceptno from lpedoritem where contno='" + fm.all('ContNo').value + "' and exists ( " +
				" select 1 from lpedorapp where edoracceptno=lpedoritem.edoracceptno and edorstate!='0' ) " +
//				" and exists (select 1 from lgwork where workno=lpedoritem.edoracceptno and (innersource != '"+fm.all('EdorAcceptNo').value+"' or innersource is null)) " +
				" and exists (select 1 from lgwork where workno='"+fm.all('EdorAcceptNo').value+"' and (innersource != lpedoritem.edoracceptno or innersource is null)) ";
		var rsBQ = easyExecSql(bqSQL);
		if(rsBQ){
			alert("保单存在未结案的保全项目，工单号为：'"+rsBQ[0][0]+"',请先将该保全结案或者撤销后再重新申请保全工单");
			return false;
		}
		}		
		
		
		if(fm.all('EdorType').value=="CT" || fm.all('EdorType').value=="XT" || fm.all('EdorType').value=="WT")
		{
			var sqllr = " select count(1),max(edorvalidate) from lpedoritem a "
						+ " where edortype= 'LR' "
						+ "	and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate='0') and contno ='"+fm.ContNo1.value+"'";
			var resultlr = easyExecSql(sqllr);
			if(resultlr[0][0]!="0" && resultlr[0][1]!=null && resultlr[0][1]!=""){
				if(!confirm("该保单已补发过"+resultlr[0][0]+"次，最后一次补发时间为"+resultlr[0][1]+"，是否继续处理？"))
				{
					return false;
				}
			}		
			var sqlPrint = " select a.contno from lccont a,lccontprint b where a.contno=b.otherno and  a.cardflag in ('c','d')  "
				+ "and a.conttype='1' and a.appflag='1' and a.contno ='"+fm.ContNo1.value+"'";
			var resultPrint = easyExecSql(sqlPrint);
			if(resultPrint){
				 if(confirm("该保单为银保网银/自助机具出单，除了打印电子保单外，还打印过纸质保单，请确认已向客户收回打印的纸质保单方可进行下一步操作")==false)
					{
						return false;
					}
				 }
			}
		
		if(fm.all('EdorType').value=="NS"){
			 var sql1 = "select 1 "
			        + "from lcpol "
			        + "where contno= '" + fm.all('ContNo').value + "' and riskcode in ('340101','340301','340401','335301','335302','730503') and appflag='1' and stateflag='1' with ur";
			    var rs1 = easyExecSql(sql1);
			    
			    if(rs1){
			    
			    	 if(fm.all('EdorValiDate').value != fm.all('currentDay').value)
			         {
//			           alert("金生无忧险种或美满今生险种或美满一生险种仅能添加附加万能险种且保全生效日期必须为当前日期");
			    	   alert("新增险种保全生效日期必须为当前日期");
			           return false;
			         }
			        
			    }
		}
//---------------- 犹豫期个单解约 杨天政 20110624 --------------- 

  var ExeSql="select customgetpoldate from lccont where contno = '"+ fm.ContNo1.value +"'";
  //获取保单的犹豫期
//  var WTPeriod = getWTPeriod(fm.ContNo1.value);
  var result_20110624 = easyExecSql(ExeSql);
  if(result_20110624 != "null" && result_20110624 != "" && result_20110624 != null)
  {
  var EdorValiDate_0624=fm.all('EdorValiDate').value;
  //-----------计算回执签收之日-----------------------
  var t1 = new Date(result_20110624[0][0].replace(/-/g,"\/")).getTime();
  //-----------计算保全生效之日-----------------------
  var t2 = new Date(EdorValiDate_0624.replace(/-/g,"\/")).getTime();
  //-----------计算保全生效日期和回执签收之日的差值-------
      var tfixdate_0624 = (t2-t1)/(24*60*60*1000);  
  //-----------只有是个险解约的时候才会进行校验----------
      
      
    
      
     if(fm.all('EdorType').value=="CT" && tfixdate_0624<15)
     {
     if(confirm("该保单尚在犹豫期内，是否确定进行个单解约？")==false)
		{
			return false;
		}
     }
     if(fm.all('EdorType').value=="LN" && tfixdate_0624<=15)
     {
     		alert("保单贷款需在保单过了犹豫期之后操作!");
			return false;
     }
  }
  else
  {
   var ExeSql="select cvalidate from lccont where contno = '"+ fm.ContNo1.value +"'";
   var result_20110624_c = easyExecSql(ExeSql);    
     if(result_20110624_c != "null" && result_20110624_c != "" && result_20110624_c != null)
     {
       var EdorValiDate_0624_c=fm.all('EdorValiDate').value;
        //-----------计算回执签收之日-----------------------
       var t1_c = new Date(result_20110624_c[0][0].replace(/-/g,"\/")).getTime();
       //-----------计算保全生效之日-----------------------
       var t2_c = new Date(EdorValiDate_0624_c.replace(/-/g,"\/")).getTime();
       //-----------计算保全生效日期和保单生效之日的差值-------
       var tfixdate_0624_c = (t2_c-t1_c)/(24*60*60*1000);  
       //-----------只有是个险解约的时候才会进行校验----------
     if(fm.all('EdorType').value=="CT" && tfixdate_0624_c<=30)
     {
     if(confirm("该保单尚在犹豫期内，是否确定进行个单解约？")==false)
		{
			return false;
		}
     }
   }
  }
  
  //--------------------税优转入保单无法犹豫期退保的校验
  if(fm.all('EdorType').value=="WT" ){
	  var SYSql="select 1 from lmriskapp where taxoptimal='Y' and riskcode in (select riskcode from lcpol where contno= '"+ fm.ContNo1.value +"')";
	  var resultSY = easyExecSql(SYSql);	  
   	  if(resultSY != "null" && resultSY != "" && resultSY != null){
   		  // 转入保单
   		  var ZRSql = "select 1 from lstransinfo where prtno in (select prtno from lccont where contno='"+ fm.ContNo1.value +"')";
   		  var resultZR = easyExecSql(ZRSql);
   		  if(resultZR != "null" && resultZR != "" && resultZR != null){
   			alert("该保单为税优转入保单，不能进行犹豫期退保");
            return false;
   		  }
   		  
	  }
  }
  
  
  //--------------------税优保单解约，保全生效日期不能超过当前时间的校验
  if(fm.all('EdorType').value=="CT" ){
	  var SYSql="select 1 from lmriskapp where taxoptimal='Y' and riskcode in (select riskcode from lcpol where contno= '"+ fm.ContNo1.value +"')";
	  var resultSY = easyExecSql(SYSql);
   	  if(resultSY != "null" && resultSY != "" && resultSY != null){
   		  var tDayDiff=dateDiff(fm.all('currentDay').value,fm.all('EdorValiDate').value,"D");          
   		  if(tDayDiff>0)                                          
   		  {
	    	  alert("税优保单解约时，保全生效日期不能超过当前日期");
	    	  return false;
   		  }
	  }
  }
  
  
  
//----------------- 犹豫期个单解约 ----the end ----to be continue ---- 
  if(!checkXB())
  {
    return false;
  }
  
	if( fm.all('EdorType').value == "PC")
  	{
 		if(!checkPC())
 		{    
 			return false;
 		}		
  	}
  
  if(fm.all('EdorType').value=="CM" || fm.all('EdorType').value=="AE"||fm.all('EdorType').value=="BF")
  {
	  if(!checkLPXB())
	  {
	    return false;
	  }
  }
  
  if (fm.all('EdorAcceptNo').value==null||fm.all('EdorAcceptNo').value=="")
  {
  		//EdorAcceptNo即是detaiWorkNo,也是workNo
      alert("请先保存保全受理，再添加保全项目!");
      return false;    
  }
  if (fm.all('EdorType').value==null||fm.all('EdorType').value=="")
  {
  		//EdorType即是AD、BB等
      alert("请选择需要添加的保全项目");
      return false;    
  }
  
  //added by suyanlin at 2007-11-19 14:05 start
    //撤销犹豫期撤保处理增加限制提示
    var mySql = "select customgetpoldate from LCCont where ContNo = '" + fm.ContNo1.value + "'" ;
    var result = easyExecSql(mySql);
    if(result != "null" && result != "" && result != null)
    {
      var t1 = new Date(result[0][0].replace(/-/g,"\/")).getTime();
      //alert("t1="+t1);
      var t2 = new Date(fm.EdorValiDate.value.replace(/-/g,"\/")).getTime();
      var tMinus = (t2-t1)/(24*60*60*1000);  
		//选择保全生效日期已经过了保单生效日期的反悔期（>=10天）
      if(fm.all('EdorType').value=="WT" && tMinus>=15 )
      {
		if(confirm("已过15天反悔期，是否继续做撤保处理?")==false)
		{
			return false;
		}
      } 
      //wanghl  2011-2-18 部分领取必须是犹豫期满才能进行申请
      if(fm.all('EdorType').value=="LQ" && tMinus<15) 
      {
         alert("犹豫期未满，不能进行部分领取");
         return false;
      }
    }
    else
    {
    	if(confirm("保单未进行回执回销，是否继续对保单继续做保全项目？")==false)
    	{
    		return false;
    	}
    }  
  //added by suyanlin at 2007-11-19 14:05 end
  
  // added by suyanlin at 2007-11-14 14:11 start
  //个险在保单失效及满期终止状态下继续添加保全项目需要提示确认
  var sql = "select * from lccont "
  			  + "where contno = '" + fm.ContNo1.value + "' "
  			  + "and stateflag in('2','3') ";
  var result = easyExecSql(sql);
  if(result)
  {
	 if(fm.all('EdorType').value != "FX" || fm.all('EdorType').value != "MF")  //modify by lc 添加免息复效
	 {
			if(hasULIRisk(fm.ContNo.value) && !hasSubULIRisk(fm.ContNo.value) && !hasSYRisk(fm.ContNo.value))
			{
				alert("万能险失效保单只能进行保单复效或者免息复效保全项目!");
  				return false;
			}	
  	}
  	if(fm.all('EdorType').value == "LN" )
  	{
		alert("该保单已失效，无法添加保单贷款的保全项目!");
  		return false;
  	}
  	if(fm.all('EdorType').value == "DD" )
  	{
		alert("该保单已失效或者终止，不能添加红利领取方式变更的保全项目!");
  		return false;
  	}
  	if(fm.all('EdorType').value == "CF" )
  	{
		alert("该保单已失效或者终止，不能添加保单拆分的保全项目!");
  		return false;
  	}

  	if(confirm("该保单已经失效，是否继续操作？")==false)
  	{
  		return false;
    }  	
  }
    
  if(!checkTaxFX(fm.all('EdorType').value)){
	  return false;
  }  
//校验万能险
  if(hasULIRisk(fm.ContNo.value))
  {
  	if(!checkULIRisk())
  	{
  		return false;
  	}
  	
  	//20101230 临时增加 ，万能附加重疾，先把保全项目控制住，完成一个项目放开一个。
//  	if(!checkULI231001())
//  	{
//  		return false;
//  	}
  	
	//20120130 万能险解约的生效日期必须大于最后一次给付持续奖金的时间。
  	if(!checkULIVali())
  	{
  		return false;
  	}
  }
  //分红险保单满期日之后，失效中止的分红保单不能操作复效或特权复效
  if(!hasFHRisk()){
	  return false;
  }
  
  
  //一次保全申请不能做多个AD
  if(fm.EdorType.value == "AD")
  {
  	var sql = 	"select * "
  				+ "from LPEdorItem "
  				+ "where edorAcceptNo='" + fm.EdorAcceptNo.value + "' "
  				+ "		and edorType='" + fm.EdorType.value + "' ";
  	var result = easyExecSql(sql);
  	
  	if(result)
  	{
  		alert("AD项目已存在，不能再添加");
  		return false;
  	}
  }
  
  //由于AD和PR涉及到客户地址表lcaddress,同时做保全会产生相同客户地址号的保全P表,增加校验做这两个项目时禁止同一客户进行其他的保全项目

  	var sql = 	"select 1 "
  				+ "from LPEdorItem a,LPEdorApp b "
  				+ "where  1=1 and a.edorno=b.edoracceptno and b.OtherNo='" + fm.OtherNo.value + "' "
  				+ "		and b.edorstate<>'0' and a.edortype in ('AD','PR') ";
  	var result = easyExecSql(sql);
  	
  	if(result)
  	{
  		alert("同一客户号下已经有其他[AD](联系方式变更)或[PR](个单迁移)项目，不能再添加保全项目");
  		return false;
  	}
  
  
  
  //简易保单重打提示
  if(fm.EdorType.value == "LR")
  {
	  var sqllr = " select count(1),max(edorvalidate) from lpedoritem a "
			+ " where edortype= 'LR' "
			+ "	and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate='0') and contno ='"+fm.ContNo1.value+"'";
	var resultlr = easyExecSql(sqllr);
	if(resultlr){
		if(!confirm("该保单已补发过"+resultlr[0][0]+"次，最后一次补发时间为"+resultlr[0][1]+"，是否继续处理？")){
			return false;
		}
	}	
	if(result)
	{
	alert("该保单是简易平台保单");
	}
	  
  	var sql = " select 1 from lccont "
	  				+ " where conttype = '1' "
  					+ "	and cardflag in ('1','2','6') and contno ='"+fm.ContNo1.value+"'";
  	var result = easyExecSql(sql);
  	
  	if(result)
  	{
  		alert("该保单是简易平台保单");
  	}
  }
  if (fm.all('DisplayType').value=='1')
  {
  		//displayType="1"表示选中AD是的显示模式
      var chkflag=false;  
      if (LCContGrid.getSelNo()==0)
      {//在客户投保信息列表中选择
          alert("请选择需要添加的保全项目的保单!");
          return false; 
      }
  }
  if (fm.all('DisplayType').value=='2')
  {
  }
  
  //判断险种是否有红利险种
  var riskSql = "SELECT 1 FROM LCPOL WHERE CONTNO='"+fm.ContNo1.value+"'"+
   				"and riskcode in ("+
   				"select code from ldcode where codetype='ctcheck'"+
   				")  with ur";
  var riskResult = easyExecSql(riskSql);
  if(riskResult){
	  //判断解约生效日时的年金是否存在未领取的年金
	  var vidateSql = "select min(gettodate) from lcget where contno = '"+fm.ContNo1.value+"' and getdutykind is not null with ur";
	  var vidateResult = easyExecSql(vidateSql);
	  if(vidateResult){
		  var vidate = new Date((vidateResult[0]+"").replace(/-/g,"\/")).getTime();
		  var  edorValidate = new Date().getTime();
		  if(vidate<=edorValidate){
			  alert("保单申请解约时须领取完截至解约生效时的年金后才可以进行解约!");
			  return false;
		  }
	  }
  }
  
  //20081009 zhanggm 万能险种解约后不能进行保全回退操作。
  //20090305 zhanggm 限制常无忧B('330501','530301')不能进行解约回退。
  if(!checkRB())
  {
  	return false;
  }
  
    //20111104 xp 保单贷款添加对保单的校验。
  if(!checkLN())
  {
  	return false;
  }
  
      //20111118 xp 保单还款添加对保单的校验。
  if(!checkRF())
  {
  	return false;
  }
  
    //20111104 xp 限制贷款状态下的保单只能操作特定的保全项目。
  if(!HaveLNcheck())
  {
  	return false;
  }
//不再单独校验
//   if(!checkDD_CF())
//  {
//  	return false;
//  }
   if(!checkLjagetDrawinfo())
  {
  	return false;
  }
  //20081106 zhanggm 做过LQ、ZB项目的保单不能做犹豫期退保。
  //20101228 zhanggm 改在PEdorWTDetailBL.java里校验。
  /*
  if(fm.all('EdorType').value=="WT")
  {
  	var sql = "select 1 from LPEdorItem where Contno = '" + fm.ContNo.value + "' "
            + "and EdorType in ('LQ','ZB') " ;
	var result = easyExecSql(sql);
    if(result)
    {
      alert("保单"+ fm.ContNo.value + "做过保全项目(部分领取或追加保费)，不能进行犹豫期退保操作！");
      return false;
    }
  }
  */
  //20140825正在做理赔的保单不能做任何保全
//  if (!checkClaim())
//  {
//  	return false;
//  }
  if(!checkDueFee())
  {
    return false;  
  }
  //  #1339,在保全申请时实现校验客户身份证件有效期校验的功能
  if(fm.all('EdorType').value !="CM"){
		if(!checkID()){
  	      return false;
        }
  }
  
  divappconfirm.style.display='';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value="INSERT||EDORITEM";
  fm.action = "../bq/PEdorAppItemSave.jsp"
  fm.submit();
}

function checkID(){
	var contNo = fm.ContNo1.value;
//	alert(contNo);
	var sql="select idstartdate,idenddate,current date from lcappnt where contno='"+contNo+"' union select idstartdate,idenddate,current date from lbappnt where contno='"+contNo+"'";
	var re = easyExecSql(sql);
    if(re==null){
    	alert("客户信息缺失！");
		return false;
    }
	
//	alert("idstartdate,idenddate,current date:  "+re[0][0]+", "+re[0][1]+", "+re[0][2]);
	var startdate = toDate(re[0][0]);
	var enddate = toDate(re[0][1]);
	var today = toDate(re[0][2]);
	if((startdate!=null && startdate!='') && (enddate==null || enddate=='' )){
//		alert("111111");
		if(today<startdate){
			alert("客户证件已过有效期，请进行客户资料变更！");
			return false;
		}
	}else if((startdate==null || startdate=='' ) && (enddate!=null && enddate!='' )){
//		alert("22222");
		if(enddate<today){
			alert("客户证件已过有效期，请进行客户资料变更！");
			return false;
		}
	}else if((startdate!=null && startdate !='' ) && (enddate !=null  && enddate !='' )){
//		alert("33333");
		if(startdate>today || today>enddate){
			alert("客户证件已过有效期，请进行客户资料变更！");
			return false;
		}	
	}else{
//		alert("4444");
		return true;
	}
	return true;
}

function toDate(str){   
      var arys= new Array();   
   if (str != null && str != '') {
   	arys = str.split('-');
   	var sdate = new Date(arys[0], parseInt(arys[1] - 1), arys[2]);
   	return sdate;
   }else{
   	return '';
   }      
}
//校验续保项目
function checkXB()
{
  var sql = "  select edorType "
            + "from LPEdorItem "
            + "where edorNo = '" + fm.EdorAcceptNo.value + "' "
            + "   and edorType = 'XB' ";
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] != null && rs[0][0] != "null")
  {
    alert("保全受理为续保续期核保类型，不能添加项目。");
    return false;
  }
  
  return true;
}

function checkLPXB()
{
  var sql = "  select 1 "
            + "from LGWork a "
            + " where  contno= '" + fm.ContNo1.value + "' "
            + "   and TypeNo = '070001' and StatusNo<>'5' " 
            		+" and exists (select 1 from LPUWMaster where contno=a.contno and edorno=a.workno) and InnerSource='070002'";
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] != null && rs[0][0] != "null")
  {
    alert("保单尚处在理赔续保核保阶段，并且核保结论已经保存，不能做对保单保费或保额有修改的保全项目");
    return false;
  }
  
  return true;
}


/**
校验续期续保应收状态
a)	若续保续期已收费（待核销状态），则不得再进行保全的正常操作，系统提示“该保单有续期续保应收记录待核销，请核销后操作或先作废原应收记录后再操作”。
b)	若续保续期已抽档但未收费，系统提示“该保单有续期续保应收记录，是否继续？”，若继续操作则暂停原续期续保应收记录的收费操作，在保全结案的同时执行续期续保应收作废、抽档操作（系统自动运行），使用原来的应收号，原应收不用记录历史信息；

*/
function checkDueFee()
{
  var contNo = fm.ContNo1.value;
  
  var sql = "  select dealState "
            + "from LJSPayB "
            + "where otherNo = '" + contNo + "' "
            + "   and otherNoType = '2' "  //个单
            + "   and dealState not in ('1', '2', '3','6') ";　//未结案,未作废,未撤销,未实收保费转出
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] != "" && rs[0][0] != "null")
  {
    if(rs[0][0] == "4")  // 待核销
    {
      alert("该保单有续期续保应收记录待核销，请核销后操作或先作废原应收记录后再操作");
      return false;
    }
    else
    {
    	if(fm.EdorType.value == "CF")
    	{
    	  if(rs[0][0] == "0")  // 待缴费
          {
           alert("该保单有续期续保应收记录，请核销后操作或先作废原应收记录后再操作");
           return false;
          }
    	}
    	else
    	{
        return confirm("该保单有续期续保应收记录，是否继续？");
    	}
    }
  }
  
  return true;
}

/*********************************************************************
 *  添加保单保全申请
 *********************************************************************
 */
function addEdorMain()
{
  if (fm.all('EdorAcceptNo').value==null||fm.all('EdorAcceptNo').value=="")
  {
       alert("请先保存保全受理，再添加保单保全申请!");
      return false;    
  }
  fm.all('fmtransact').value="INSERT||EDOR";
  fm.action = "../bq/PEdorAppMainSave.jsp"
  fm.submit();
}

/*********************************************************************
 *  删除未录入保全项目
 *********************************************************************
 */
function delRecord()
{
	if(EdorItemGrid.mulLineCount==0)
	{
		alert("没有批改项目！");
		return false;
	}

	var tSelNo = EdorItemGrid.getSelNo()-1;
	if (tSelNo < 0)
	{
		alert("请选择需要删除的项目！");
		return false;
	}
	var state= EdorItemGrid.getRowColData(tSelNo,8);
  if(state == "已提交")
	{
	  alert("请选择未提交的项目!");
	  return false;
	}
  else
  {
  	if(confirm("确认删除吗？"))
  	{
	  	var EdorAcceptNo = EdorItemGrid.getRowColData(tSelNo,1);
	  	var EdorNo = EdorItemGrid.getRowColData(tSelNo,2);
	  	var ContNo = EdorItemGrid.getRowColData(tSelNo,4);
	  	var InsuredNo = EdorItemGrid.getRowColData(tSelNo,6);
	  	var PolNo = EdorItemGrid.getRowColData(tSelNo,9);
	  	fm.all("EdorType").value = EdorItemGrid.getRowColData(tSelNo,3);
	  	fm.all('Transact').value="I&EDORITEM";
	  	fm.all("DelFlag").value="1";
	  	fm.action="./PEdorAppCancelSubmit.jsp?hEdorAcceptNo="+EdorAcceptNo+"&EdorNo="+EdorNo+"&"
	  	          +"ContNo="+ContNo+"&InsuredNo="+InsuredNo+"&PolNo="+PolNo;
	  	fm.submit();
	  	fm.all('fmtransact').value="";
	  	fm.all('EdorType').value="";
  	}
  }
}

/*********************************************************************
 *  MulLine的RadioBox点击事件，显示项目明细按钮
 *  参数  ：  parm1,parm2
 *********************************************************************
 */       
function getEdorItemDetail(parm1, parm2)
{
  fm.EdorNo.value=fm.all(parm1).all('EdorItemGrid2').value 
  fm.EdorType.value=fm.all(parm1).all('EdorItemGrid3').value
  fm.EdorTypeName.value = getEdorTypeName(fm.EdorType.value);
  fm.ContNo.value=fm.all(parm1).all('EdorItemGrid4').value
  fm.ContNoBak.value=fm.all(parm1).all('EdorItemGrid4').value;
  fm.AppntNo.value=fm.all(parm1).all('EdorItemGrid5').value;
  fm.CustomerNoBak.value=fm.all(parm1).all('EdorItemGrid6').value;
  fm.EdorValiDate.value=fm.all(parm1).all('EdorItemGrid7').value;
}

//得到项目名
function getEdorTypeName(edorType)
{
	var sql = "  select edorName "
	          + "from LMEdorItem "
	          + "where appObj = 'I' "
	          + "   and edorCode = '" + edorType + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    return rs[0][0];
  }
  else
  {
    return "";
  }
}

/*********************************************************************
 *  MulLine的RadioBox点击事件，显示保单明细，险种，和批改项目
 *  参数  ：  parm1, parm2
 *********************************************************************
 */       
function getLCContDetail(parm1, parm2)
{
  var tContNo = fm.all(parm1).all('LCContGrid1').value;
  fm.all('ContNo').value = tContNo;
  getCValiDate();//added by suyanlin 2007-11-19 14:58
  getLCContDetailInfo(tContNo);
  getInsuredPolInfo(tContNo);
  getEdorItem();
  showOmAccButton();
}

//updated by suyanlin at 2007-11-19 14:55 start\
//取得所选保单生效日期
function getCValiDate()
{
    var result = new Array();
		tRow = LCContGrid.getSelNo();	
		//选择行的数组
		result[0] = LCContGrid.getRowData(tRow-1);
		//保单生效日期
		fm.CValiDate.value=result[0][4];
}
//updated by suyanlin at 2007-11-19 14:55 end
  
/*********************************************************************
 *  显示保单详细信息
 *  参数  ：  contNo 保单号
 *********************************************************************
 */       
function getLCContDetailInfo(contNo)
{ 
  	var strSQL= "select contno,AppntName,appntno,AppntSex,AppntIDType,AppntIDNo," +
			        "PayMode,BankCode,BankAccNo,AccName " + 
              "from lccont " + 
              "where contno='"+ contNo + "' and appflag='1'";	      
  	var arrResult = easyExecSql(strSQL);    
  	if (arrResult == null) 
  	{
  	//退保保单
		strSQL= " select contno,AppntName,appntno,AppntSex,AppntIDType,AppntIDNo," +
			        " PayMode,BankCode,BankAccNo,AccName " + 
              " from lbcont a" + 
              " where contno='"+ contNo + "' and exists ( "+
					 		" select 1 from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','XT') and edorno = a.edorno)"
              ;	      
  		arrResult = easyExecSql(strSQL);   
		} 

    try {fm.all('contno1').value= arrResult[0][0]; } catch(ex) { };
    try {fm.all('AppntName').value= arrResult[0][1]; } catch(ex) { };
    try {fm.all('Appntno1').value= arrResult[0][2]; } catch(ex) { };
    try {fm.all('AppntSex').value= arrResult[0][3]; } catch(ex) { };
    try {fm.all('AppntIDType').value= arrResult[0][4]; } catch(ex) { };
    try {fm.all('AppntIDNo').value= arrResult[0][5]; } catch(ex) { };  
    try {fm.all('PayMode').value= arrResult[0][6]; } catch(ex) { };  
    try {fm.all('BankCode').value= arrResult[0][7]; } catch(ex) { };  
    try {fm.all('BankAccNo').value= arrResult[0][8]; } catch(ex) { };  
    try {fm.all('AccName').value= arrResult[0][9]; } catch(ex) { };  

  	strSQL= "select b.Phone,b.Mobile,b.EMail,b.PostalAddress "
          + "from lccont a, LCAddress b "
          + "where b.CustomerNo=a.appntno "
          + "and   a.contno ='" + contNo + "'"
          + "and   a.appflag='1' "
          + "and   b.AddressNo="
          + "(select AddressNo from lcappnt where contno='" + contNo + "')";   

  	arrResult = easyExecSql(strSQL);

  	if (arrResult == null) 
  	{
		  strSQL= "select b.Phone,b.Mobile,b.EMail,b.PostalAddress "
          + "from lbcont a, LCAddress b "
          + "where b.CustomerNo=a.appntno "
          + "and   a.contno ='" + contNo + "'"
          + "and   a.appflag='1' "
          + "and   b.AddressNo="
          + "(select AddressNo from lbappnt where contno='" + contNo + "')";
      arrResult = easyExecSql(strSQL);
  	} 

    try {fm.all('Phone').value= arrResult[0][0]; } catch(ex) { };
    try {fm.all('Mobile').value= arrResult[0][1]; } catch(ex) { };
    try {fm.all('EMail').value= arrResult[0][2]; } catch(ex) { };
    try {fm.all('PostalAddress').value= arrResult[0][3]; } catch(ex) { };
    
    
    //制定汉化
    showOneCodeName("Sex", "AppntSex"); 
    showOneCodeName("IDType", "AppntIDType"); 
    showOneCodeName("PayMode", "PayMode"); 
    showOneCodeName("Bank", "BankCode");    
}

/*********************************************************************
 *  MulLine的RadioBox点击事件，显示被保险人下险种
 *  参数  ：  parm1, parm2
 *********************************************************************
 */       
function getInsuredDetail(parm1,parm2)
{
  var tContNo = fm.all(parm1).all('InusredGrid7').value;
  getInsuredPolInfo(tContNo);    
}

/*********************************************************************
 *  查询保单的所有投保人和被保人信息
 *  参数：contNo 合同号
 *********************************************************************
 */
function getInsuredInfo(contNo)
{
  //var strSQL ="select distinct AppntNo, AppntName, AppntSex, AppntBirthday, IdNo, ContNo, GrpContNo " +
  //      "from LCAppnt " +
  //      "where ContNo = '" + contNo + "' " +
  //      "union " +
  //      "select distinct InsuredNo, Name, Sex, Birthday, IdNo, ContNo, GrpContNo " +
  //      "from LCInsured " +
  //      "where ContNo = '" + contNo + "' ";
  var strSQL ="select distinct a.CustomerNo,a.Name,a.Sex,a.Birthday,a.IdNo,b.contno,b.grpcontno " +
              "from LDPerson a,LCCont b " +
              "where b.ContNo = '" + contNo + "' " +
              "and a.CustomerNo in (select appntno from lccont where ContNo='"+contNo+"' union select insuredno from LCInsured where ContNo='"+contNo+"')";
  turnPageIns.queryModal(strSQL, InsuredGrid);
}

/*********************************************************************
 *  查询险种信息
 *  参数：contNo 合同号
 *********************************************************************
 */       
function getInsuredPolInfo(contNo)
{
	var strSQL ="select distinct RiskSeqNo, InsuredNo, InsuredName, LCPol.RiskCode, LMRiskApp.RiskName, " +
	            "       Amnt,LCPol.mult, prem, CValiDate, PayToDate, ContNo,  LCPol.GrpContNo ,codename('stateflag',stateflag),InsuredBirthday " +
	            "from LCPol INNER JOIN lMRiskApp ON  LCPol.RiskCode=LMRiskApp.RiskCode" +" "+
	            "left join LCContPlanRisk ON LCPol.riskcode=LCContPlanRisk.riskcode"+" "+
	            "where ContNo = '" + contNo + "' " +
	            "and appflag = '1'"
	            +" union "
	            +" select distinct RiskSeqNo, InsuredNo, InsuredName, LBPol.RiskCode, LMRiskApp.RiskName "
							+" , Amnt,LBPol.mult, prem, CValiDate, PayToDate, ContNo,  LBPol.GrpContNo ,codename('stateflag',stateflag),InsuredBirthday "
							+" from LBPol INNER JOIN lMRiskApp ON  LBPol.RiskCode=LMRiskApp.RiskCode "
							+" left join LCContPlanRisk ON LBPol.riskcode=LCContPlanRisk.riskcode "
							+" where ContNo = '" + contNo + "' " 
							;
	turnPagePol.pageLineNum = 100;
	turnPagePol.queryModal(strSQL, PolGrid);
	
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getRowColData(i, 7) == "0")
		{
			PolGrid.setRowColData(i, 7, "");
		}
	}
}

function detailEdor()
{
  if (EdorItemGrid.getSelNo() == 0)
  {
    alert("请选择一个批改项目！");
    return false;
  }
  
  if(fm.EdorType.value == "XB")
  {
    alert("续期续保核保项目不提供明细录入。");
    return false;
  }
  
  detailEdorType();
}


//校验万能险
//万能险同时只能对一个保全项目进行操作
function checkULIRisk()
{
  	var sql = "select 1 from LPEdorItem where EdorAcceptNo = '" + fm.all('EdorAcceptNo').value + "' ";
	var result = easyExecSql(sql);
	if(result)
	{
		alert("不能再添加保全项目!\n万能险保单保全，同时只能对一个保全项目操作!");
	 	return false;
	}
	
	/**if( fm.all('EdorType').value == "CM" )
	{
		if( compareDate(fm.all("monAfterCurrent").value,fm.all("EdorValiDate").value) != 0 )
  		{
  			alert("保全生效日期输入错误\n个险万能客户资料变更生效日期必须是下月一号:\n"　+　fm.all("monAfterCurrent").value);
  			return false;
  		}
	}*/
	
	if( fm.all('EdorType').value == "BA")
  	{
  		if( compareDate(fm.all("monAfterCurrent").value,fm.all("EdorValiDate").value) != 0 )
  		{
  			alert("保全生效日期输入错误\n个险万能保障调整生效日期必须是下月一号:\n"　+　fm.all("monAfterCurrent").value);
  			return false;
  		}
  		if(!checkULIBA())
 		{    
 			return false;
 		}
  		/*
		if(!isLoanState(fm.ContNo.value))
 		{    
 			return false;
 		}
 		if(!checkInsuredAge())
 		{
 			return false;
 		}
 		*/
  	}
  	//20090706 zhanggm 万能保费调整校验
  	if( fm.all('EdorType').value == "BP")
  	{
 		if(!checkULIBP())
 		{    
 			return false;
 		}		
  	}
  	
  	//20100623 zhanggm 万能追加保费时不能欠缴保费
  	if( fm.all('EdorType').value == "ZB")
  	{
 		if(!checkULIZB())
 		{    
 			return false;
 		}	
 		
 		if(!checkU332301ZB())
 		{    
 			return false;
 		}
  	}
  	return true;	
}

function hasULIRisk(ContNo)
{
//	var sql = "select * from LMRiskApp where RiskCode in (select RiskCode from LCPol where ContNo='"+ContNo+"') and RiskType4='4'";
	var sql = "select 1 from LMRiskApp as R,LCPol as P where R.RiskCode=P.RiskCode and  ContNo='"+ContNo+"' and RiskType4='4'";
	var result = easyExecSql(sql);
	if(result)
	{
		return true;
	}
	return false;	
}

function hasSubULIRisk(ContNo)
{
	var sql = "select 1 from LMRiskApp a,LCPol b where a.RiskCode=b.RiskCode and  b.ContNo='"+ContNo+"' and a.RiskType4='4' and a.subriskflag='S'";
	var result = easyExecSql(sql);
	if(result)
	{
		return true;
	}
	return false;	
}
//是否是税优保单
function hasSYRisk(ContNo)
{
	var sql = "select 1 from LMRiskApp a,LCPol b where a.RiskCode=b.RiskCode and  b.ContNo='"+ContNo+"' and a.taxoptimal='Y'";
	var result = easyExecSql(sql);
	if(result)
	{
		return true;
	}
	return false;	
}

//校验被保险人是否大于60周岁
function checkInsuredAge()
{
 	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{	
		var InsuredBirthday = PolGrid.getRowColData(i, 14);
		var age = calAge(InsuredBirthday);
		if( age > 60 )
		{
			var InsuredName = PolGrid.getRowColData(i, 3);
			alert("被保人:" + InsuredName + "年龄大于60周岁，不能进行个险万能保障调整");
			return false;
		}
	}
	return true;
}

//获取保单犹豫期
function getWTPeriod(ContNo){
//	var salechnlSQL = " select salechnl from lccont where contno='"+ContNo+"' with ur  ";
//	var result = easyExecSql(salechnlSQL);
//	
//	if(result!=null&&(result[0][0]=="04"||result[0][0]=="13")){
//			return 15;
//	}else{
//    	var  wtSQL = " select max(hesitateend) from lmedorwt  " +
//		" where riskcode in (select riskcode from lcpol where contno='"+ContNo+"') with ur ";
//    	var result1 = easyExecSql(wtSQL);
//    	if(result1!=null&&result1[0][0]!=""){
//    		return parseInt(result1[0][0]);
//    	}else{
//    		return 10;
//    	}
//	}
	return 15;
}
//获取分红保单状态
function hasFHRisk()
{
	if(fm.all('EdorType').value=="FX" || fm.all('EdorType').value=="MF" || fm.all('EdorType').value=="TF" || fm.all('EdorType').value=="HA"){
		var sql = " select 1 from lccont as a,LCPol as b where a.contno=b.contno and  a.ContNo='"+fm.ContNo.value+"'" 
		 +" and b.RiskCode in (select riskcode from lmriskapp where risktype4='2') and a.stateflag='2' and a.Cinvalidate < current date ";
		var result = easyExecSql(sql);
		if(result)
		{
			alert("该分红保单过了满期日不能申请此保全项目!!");
			return false;
		}
	}
	//分红险保单满期之后不能操作解约
	if(fm.all('EdorType').value=="CT"){
		var sql = " select a.contno from lccont as a ,lcpol as b  where  a.contno=b.contno and a.conttype='1' and a.appflag='1'  and a.ContNo='"+fm.ContNo.value+"'" 
		 +"  and b.RiskCode in (select riskcode from lmriskapp where risktype4='2')  and b.polstate in ('03060001','03060002')   with ur  ";
		var result = easyExecSql(sql);
		if(result !=null )
		{
			alert("该分红保单已满期,不能申请此保全项目！");
			return false;
		}
	}
	return true;	
}


function isLoanState(ContNo)
{
	var SQL="select Paytodate,CValiDate,current date from  lcpol where  ContNo='"+ContNo+"' and polno=mainpolno";     
	var result = easyExecSql(SQL);
	  		
	var tPayToDate = result[0][0];
	var tCvaliDate = result[0][1];
	var tCurrDate=result[0][2];
	var tDayDiff=dateDiff(tPayToDate,tCurrDate,"D");
	var tDayDi=dateDiff(tCvaliDate,tCurrDate,"D");
	if(tDayDiff>0)
 	{	
 		alert("该保单有保费未缴纳!");
 		return false;    		
	}	
	if(tDayDi<=365)
	{
		alert("该保单生效未满一年，不能申请个险万能保障调整!!");
 		return false;    	
	}	
    return true;	
}

function checkRB()
{
  if(fm.all('EdorType').value=="RB")
  {
  	var sql = "select 1 from LBPol a where Contno = '" + fm.ContNo.value + "' "
          + "and exists (select 1 from LMRiskApp where RiskType4 in ('4') " 
          + "  and RiskCode = a.RiskCode) with ur";
    var result = easyExecSql(sql);
    if(result)
    {
      alert("该保单存在万能险种，不能做保全项目--退保回退！");
      return false;
    }
    sql = "select 1 from LBPol a where Contno = '" + fm.ContNo.value + "' "
        + "and RiskCode in ('330501','530301') with ur";
    var result1 = easyExecSql(sql);
    if(result1)
    {
      alert("该保单险种是常无忧B，不能做保全项目--退保回退！");
      return false;
    }
  }
  return true;
}

function checkLN()
{
  if(fm.all('EdorType').value=="LN"||fm.all('EdorType').value=="RF")
  {
// 	var sql = "select * from LCInsured a " +
//		          "where exists (select * from LLCase " +
//		          "     where CustomerNo = a.InsuredNo " +
//		          "     and rgtstate not in ('11', '12', '14')) " +
//		          "and ContNo = '" + fm.ContNo.value + "' ";
//	var result = easyExecSql(sql);
//		if (result)
//		{
//				alert("该保单正在理赔，无法继续操作！");
//				return false;
//		}
    sql = "select 1 from ljspay a where otherno = '" + fm.ContNo.value + "' and othernotype='2' "
        + "and bankonthewayflag='1' with ur";
    var result1 = easyExecSql(sql);
    if(result1)
    {
      alert("该保单存在银行在途的续期应收,无法添加此保全项目！");
      return false;
    }
    sql = "select 1 from lpedoritem a where Contno = '" + fm.ContNo.value + "' "
        + "and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') with ur";
    var result2 = easyExecSql(sql);
    if(result2)
    {
      alert("该保单存在未结案的保全,无法添加此保全项目！");
      return false;
    }
  }
  return true;
}

function checkDD_CF()
{           
  if(fm.all('EdorType').value=="DD"||fm.all('EdorType').value=="CF")
  {
 	var sql = "select * from LCInsured a " +
		          "where exists (select * from LLCase " +
		          "     where CustomerNo = a.InsuredNo " +
		          "     and rgtstate not in ('11', '12', '14')) " +
		          "and ContNo = '" + fm.ContNo.value + "' ";
	var result = easyExecSql(sql);
		if (result)
		{
				alert("该保单正在理赔，无法继续操作！");
				return false;
		}    
  }
  return true;
}

function checkLjagetDrawinfo()
{
  if(fm.all('EdorType').value=="CF")
  {
 	var sql = "select a.getnoticeno from ljsgetdraw a " +
		          "where not exists (select * from ljaget " +
		          "     where actugetno = a.getnoticeno " +
		          "     and confdate is not null ) " +
		          "and ContNo = '" + fm.ContNo.value + "' ";
	var result = easyExecSql(sql);
		if (result)
		{
				alert("存在未核销的满期给付数据"+result+"，无法继续操作！");
				return false;
		}    
  }
  return true;
}

function checkRF()
{
  if(fm.all('EdorType').value=="RF")
  {
 	var sql = "select * from lccont a " +
		          "where (stateflag is  null or stateflag!='1') " +
		          "and ContNo = '" + fm.ContNo.value + "' ";
	var result = easyExecSql(sql);
		if (result)
		{
				alert("失效状态下的保单不能添加此保全项目！");
				return false;
		}
    sql = "select * from loloan a where polno in (select polno from lcpol where contno= '" + fm.ContNo.value + "') "
    	+ " and payoffflag='0' and loantype='0'  with ur";
    var result1 = easyExecSql(sql);
    if(!result1)
    {
      alert("该保单不存在贷款,无需申请此项目！");
      return false;
    }
  }
  return true;
}


//有贷款保全操作的保单允许申请解约
function HaveLNcheck()
{
  	var sql = "select 1 from loloan a where polno in (select polno from lcpol where contno='" + fm.ContNo.value + "')   "
          + " and payoffflag='0'   with ur";
    var result = easyExecSql(sql);
    if(result)
    {
    	if(fm.all('EdorType').value=="AD"||fm.all('EdorType').value=="FX"||fm.all('EdorType').value=="MF"||fm.all('EdorType').value=="RF"||fm.all('EdorType').value=="CC"||fm.all('EdorType').value=="CT" ||fm.all('EdorType').value=="HA")    //modify by lc 保单免息复效
        {
  			return true;
    	}
    	else
  		{
      		alert("该保单存在贷款，不能操作此保全项目！");
      		return false;
      	}	  
    }
    return true;
}

//20090320 zhanggm 校验保单是否冻结 LCCont--State=1
function checkState()
{
  var sql = "select 1 from LCCont where Contno = '" + fm.ContNo.value + "' "
          + "and State = '1' with ur" ;
  var result = easyExecSql(sql);
  if(result)
  {
    alert("保单"+ fm.ContNo.value + "已经被冻结，不能做保全操作。");
    return false;
  }
  return true;
}

/*********************************************************************
 *  万能帐户信息
 *  参数  ：  无
 *  返回值：  无
 *  添加日期：2009-6-10
 *********************************************************************
 */
function showOmAcc()
{
  var tSel = LCContGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择保单！");
  }
  else
  {
    var cContNo = fm.all('ContNo').value;
    var sql = "select polno,riskcode from lcpol a where contno = '"+cContNo+"' "
            + "and exists (select 1 from lmriskapp where RiskCode = a.riskcode and RiskType4 = '4') ";
    var rs = easyExecSql(sql);
    if(rs == null || rs[0][0] == null || rs[0][0] == "")
    {
      alert( "此保单不是万能险保单！");
    }
    else
    {
      var cPolNo = rs[0][0];
      var cRiskCode = rs[0][1];
  	  var sql = "select min(InsuAccNo) from LMRiskToAcc where RiskCode = '" + cRiskCode + "'";
  	  var arrReturn = easyExecSql(sql);
  	  var cInsuAccNo = arrReturn[0][0];
      window.open("../bq/OmnipotenceAcc.jsp?PolNo=" + cPolNo + "&InsuAccNo=" + cInsuAccNo);
  	}
  }
}

function showOmAccButton()
{
    var cContNo = fm.all('ContNo').value;
    var sql = "select 1 from lcpol a where contno = '"+cContNo+"' "
            + "and exists (select 1 from lmriskapp where RiskCode = a.riskcode and RiskType4 = '4') ";
    var rs = easyExecSql(sql);
    if(rs == null || rs[0][0] == null || rs[0][0] == "")
    {
        fm.all('omnipotenceAcc').style.display = "none";
    }
    else
    {
        fm.all('omnipotenceAcc').style.display = "";
  	}
}

function checkULIBA(ContNo)
{
  var ContNo = fm.ContNo.value;
  var sql="select PolNo,cvalidate from lcpol where  ContNo='"+ContNo+"' and polno=mainpolno and riskcode in (select riskcode from lmriskapp where risktype4='4')";     
  var result = easyExecSql(sql);
  var tPolNo = result[0][0]; 
  var tCvaliDate = result[0][1];
  var sql1 = "select lastpaytodate,curpaytodate,current date from ljapayperson where polno = '"+tPolNo+"' order by curpaytodate desc ";
  var result1 = easyExecSql(sql1);
  var tLastPayToDate = result1[0][0];                            
  var tCurPayToDate = result1[0][1];
  var tCurrDate=result1[0][2];
  var tDayDiff=dateDiff(tCvaliDate,tCurrDate,"D");          
  if(tDayDiff<=365)
  {
    alert("该保单生效未满一年，不能申请个险万能保障调整!");
    return false;    	
  }
  var sql2="select paytodate from lcpol p where contno='"+ContNo+"'  and polno=mainpolno and  exists (select 1 from lmriskapp where riskcode = p.riskcode and risktype4 = '4')";	
  var result2 = easyExecSql(sql2);	
  var tPayToDate= result2[0][0];
  var tDayDiff2=dateDiff(tPayToDate,tCurrDate,"D");
  //说明到目前保单已经欠费,且保单处于自动缓交 或者 缓交状态	
  //if(tDayDiff2>0)
  //{	
   	//alert("有保费未缴纳，不能做保费调整!");
    //return false;    		
  //}	
  
  
  if(!checkChangeTimes(ContNo,"BA",tLastPayToDate,tCurPayToDate))
  {
  	return false;
  }
  return true;
}

function checkU332301ZB(){
	
	  var ContNo = fm.ContNo.value;
	  var sql332301="select 1 from lcpol where  ContNo='"+ContNo+"' and riskcode='332301'";     
	  var result332301 = easyExecSql(sql332301);
	  var sql340101="select 1 from lcpol where  ContNo='"+ContNo+"' and riskcode='340101'";     
	  var result340101 = easyExecSql(sql340101);
	  
	  if(result332301!=null&&result332301!=""&&result332301!="null"&&result340101!=null&&result340101!=""
		  &&result340101!="null")
	  {
	    alert("金生无忧下的附加万能险种不能做追加保费的保全项目");
	    return false;
	  }
	return true;
}
function checkPC(){

	  var ContNo = fm.ContNo.value;
	  var sql="select PolNo,standbyflag1 from lcpol where  ContNo='"+ContNo+"' and polno=mainpolno";     
	  var result = easyExecSql(sql);
	  var tPolNo = result[0][0];                            
	  var tHuan = result[0][1]; 
	  if("1"==result[0][1])
	  {
	    alert("该保单现在是保费缓交状态，不能申请缴费频次变更！");
	    return false;
	  }
	  
	  var sql1 = "select lastpaytodate,curpaytodate,current date - 1 days from ljapayperson where polno = '"+tPolNo+"' order by curpaytodate desc ";
	  var result1 = easyExecSql(sql1);
	  var tLastPayToDate = result1[0][0];                            
	  var tCurPayToDate = result1[0][1];
	  var tCurrDate=result1[0][2]; 
	  var tEdorValiDate = fm.all('EdorValiDate').value;
	  var tEdorType=fm.all('EdorType').value;                                                          
	  var tDayDiff1=dateDiff(tCurPayToDate,tCurrDate,"D");
	  if(tDayDiff1>60)                                            
	  {	                                                        
	  	alert("该保单过了60天宽限期还没有收费，不能申请缴费频次变更!");                           
	  	return false;    		                                    
	  }	           
	   
	  var tDayDiff2=dateDiff(tLastPayToDate,tEdorValiDate,"D");          
	  var tDayDiff3=dateDiff(tEdorValiDate,tCurPayToDate,"D");
	  
	  if(tDayDiff2<0)                                            
	  {	                                                        
	  	alert("保全生效日期不能早于保单上次缴费日期！");                           
	  	return false;    		                                    
	  }	                                                        
	  if(tDayDiff3<0&&tEdorType!='PC')                                           
	  {                                                         
	  	alert("保全生效日期不能晚于保单本次交至日期！");
	  	return false;    	                                      
	  }
	//税优单检验
	  var sql2 = "select cvalidate,paytodate,payenddate,stateflag,current date ,payintv,enddate from lcpol where polno = '"+tPolNo+"' and  riskcode in (select riskcode from lmriskapp where taxoptimal='Y' ) ";
	  var result2 = easyExecSql(sql2);
	 //电商
	  var sql3 = "select cvalidate,paytodate,payenddate,stateflag,current date ,payintv,enddate from lcpol where  contno ='"+ContNo+"' and  riskcode in (select code from LDCode where codeType = 'sepriskcode' )";
	  var result3 = easyExecSql(sql3);
	  if(result2){

		  var tCvalidate = result2[0][0]; 
		  var tPayToDate = result2[0][1];                            
		  var tPayendDate = result2[0][2]; 
		  var tStateflag = result2[0][3]; 
		  var mCurrDate = result2[0][4];
		  var tpayintv = result2[0][5];
		  var tEndDate = result2[0][6]; 
		  var mEdorValiDate = fm.all('EdorValiDate').value;
		  var tDayDiff4=dateDiff(tPayToDate,tPayendDate,"D"); 
		  
		  if("1"!=tpayintv)
		  {
		    alert("该保单的缴费频次不是月缴，不能申请缴费频次变更！");
		    return false;
		  }
		  
		  if(tDayDiff4>0)                                            
		  {	                                                        
		  	alert("税优保单下有未缴纳的续期保费，不能申请缴费频次变更！");                           
		  	return false;    		                                    
		  }	 
		  if(tStateflag!="1"){
			alert("税优保单已失效，不能申请缴费频次变更！");                           
			return false; 
		  }
		  var tDayDiff5=dateDiff(mCurrDate,tPayendDate,"D"); 
		  if(tDayDiff5<=0)                                            
		  {	                                                        
		  	alert("税优保单已过了满期日，不能申请缴费频次变更！");                           
		  	return false;    		                                    
		  }	
		  var tDayDiff7=dateDiff(mEdorValiDate,tPayToDate,"D"); 
		  if(tDayDiff7<0)                                            
		  {	                                                        
		  	alert("保全生效日期不能晚于保单的交至日期！");                           
		  	return false;    		                                    
		  }	
		  var tDayDiff6=dateDiff(mEdorValiDate,tPayendDate,"D"); 
		  if(tDayDiff6<0)                                            
		  {	                                                        
		  	alert("保全生效日期不能晚于保单的满期日！");                           
		  	return false;    		                                    
		  }	 
		  if(!checkChangeTimes(ContNo,"PC",tCvalidate,tPayToDate))
		  {
		  	return false;
		  }
	  }
//	  else{
//		  if(!checkChangeTimes(ContNo,"PC",tLastPayToDate,tCurPayToDate))
//		  {
//		  	return false;
//		  }
//	  }
	  
	  else if(result3){

		  var tCvalidate = result3[0][0]; 
		  var tPayToDate = result3[0][1];                            
		  var tPayendDate = result3[0][2]; 
		  var tStateflag = result3[0][3]; 
		  var mCurrDate = result3[0][4];
		  var tpayintv = result3[0][5];
		  var tEndDate = result3[0][6]; 
		  var mEdorValiDate = fm.all('EdorValiDate').value;
		  var tDayDiff4=dateDiff(tPayToDate,tPayendDate,"D"); 
		  
		  if("1"!=tpayintv)
		  {
		    alert("该险种的缴费频次不是月缴，不能申请缴费频次变更！");
		    return false;
		  }
		  
		  if(tDayDiff4>0)                                            
		  {	                                                        
		  	alert("此保单下有未缴纳的续期保费，不能申请缴费频次变更！");                           
		  	return false;    		                                    
		  }	 
		  if(tStateflag!="1"){
			alert("此保单已失效，不能申请缴费频次变更！");                           
			return false; 
		  }
		  var tDayDiff5=dateDiff(mCurrDate,tPayendDate,"D"); 
		  if(tDayDiff5<=0)                                            
		  {	                                                        
		  	alert("此保单已过了满期日，不能申请缴费频次变更！");                           
		  	return false;    		                                    
		  }	
		  var tDayDiff7=dateDiff(mEdorValiDate,tPayToDate,"D"); 
		  if(tDayDiff7<0)                                            
		  {	                                                        
		  	alert("保全生效日期不能晚于保单的交至日期！");                           
		  	return false;    		                                    
		  }	
		  var tDayDiff6=dateDiff(mEdorValiDate,tPayendDate,"D"); 
		  if(tDayDiff6<0)                                            
		  {	                                                        
		  	alert("保全生效日期不能晚于保单的满期日！");                           
		  	return false;    		                                    
		  }	 
		  if(!checkChangeTimes(ContNo,"PC",tCvalidate,tPayToDate))
		  {
		  	return false;
		  }
	  }else{
		  if(!checkChangeTimes(ContNo,"PC",tLastPayToDate,tCurPayToDate))
		  {
		  	return false;
		  }
	  }

	  return true;

}

//保全生效日期不能晚于交至日期
//该保单有保费未缴纳，不能申请个险万能保费调整
//万能BP、BA每个保单年度只能做一次
//缓交状态不能申请保费调整
function checkULIBP()
{
  var ContNo = fm.ContNo.value;
  var sql="select PolNo,standbyflag1 from lcpol where  ContNo='"+ContNo+"' and polno=mainpolno";     
  var result = easyExecSql(sql);
  var tPolNo = result[0][0];                            
  var tHuan = result[0][1]; 
  if("1"==result[0][1])
  {
    alert("该保单现在是保费缓交状态，不能申请保费调整！");
    return false;
  }
  
  var sql1 = "select lastpaytodate,curpaytodate,current date - 1 days from ljapayperson where polno = '"+tPolNo+"' order by curpaytodate desc ";
  var result1 = easyExecSql(sql1);
  var tLastPayToDate = result1[0][0];                            
  var tCurPayToDate = result1[0][1];
  var tCurrDate=result1[0][2]; 
  var tEdorValiDate = fm.all('EdorValiDate').value;
  var tEdorType=fm.all('EdorType').value;                                                          
  var tDayDiff1=dateDiff(tCurPayToDate,tCurrDate,"D");
  if(tDayDiff1>60)                                            
  {	                                                        
  	alert("该保单过了60天宽限期还没有收费，不能申请个险万能保费调整!");                           
  	return false;    		                                    
  }	                                                        
  var tDayDiff2=dateDiff(tLastPayToDate,tEdorValiDate,"D");          
  var tDayDiff3=dateDiff(tEdorValiDate,tCurPayToDate,"D");
  if(tDayDiff2<0)                                            
  {	                                                        
  	alert("保全生效日期不能早于保单上次缴费日期！");                           
  	return false;    		                                    
  }	                                                        
  if(tDayDiff3<0&&tEdorType!='BP')                                           
  {                                                         
  	alert("保全生效日期不能晚于保单本次交至日期！");
  	return false;    	                                      
  }
  var sql2 = "select cvalidate,paytodate,payenddate,stateflag,current date,enddate from lcpol where polno = '"+tPolNo+"' and  riskcode in (select riskcode from lmriskapp where taxoptimal='Y' ) ";
  var result2 = easyExecSql(sql2);
  if(result2){
	  var tCvalidate = result2[0][0]; 
	  var tPayToDate = result2[0][1];                            
	  var tPayendDate = result2[0][2]; 
	  var tStateflag = result2[0][3]; 
	  var mCurrDate = result2[0][4];
	  var tEndDate = result2[0][5]; 
	  var mEdorValiDate = fm.all('EdorValiDate').value;
	  var tDayDiff4=dateDiff(tPayToDate,tPayendDate,"D"); 
	  if(tDayDiff4>0)                                            
	  {	                                                        
	  	alert("税优保单下有未缴纳的续期保费，不能申请保费变更！");                           
	  	return false;    		                                    
	  }	 
	  if(tStateflag!="1"){
		  	alert("税优保单已失效，不能申请保费变更！");                           
		  	return false; 
	  }
	  var tDayDiff5=dateDiff(mCurrDate,tPayendDate,"D"); 
	  if(tDayDiff5<=0)                                            
	  {	                                                        
	  	alert("税优保单已过了满期日，不能申请保费变更！");                           
	  	return false;    		                                    
	  }	
	  var tDayDiff6=dateDiff(mEdorValiDate,tPayendDate,"D"); 
	  if(tDayDiff6<0)                                            
	  {	                                                        
	  	alert("保全生效日期不能晚于保单的满期日！");                           
	  	return false;    		                                    
	  }	 
	  if(!checkChangeTimes(ContNo,"BP",tCvalidate,tPayToDate))
	  {
	  	return false;
	  }
  }else{
	  if(!checkChangeTimes(ContNo,"BP",tLastPayToDate,tCurPayToDate))
	  {
	  	return false;
	  }
  }

  return true;
}

//万能BP、BA每个保单年度只能做一次
function checkChangeTimes(aContNo,aEdorType,aLastPayToDate,aCurPayToDate)
{
  var sql1 = "select edorname from lmedoritem where edorcode = '" +aEdorType+"' ";
  var result1 = easyExecSql(sql1);
  var EdorName = "";
  if(result1)
  {
    EdorName=result1[0][0];
  }

  var sql2 = "select count(1) from lpedoritem a where contno = '"+aContNo+"' and edortype = '"+aEdorType+"' "
           + "and edorvalidate >= '"+aLastPayToDate+"' and edorvalidate<='" +aCurPayToDate+ "' "
           + "and exists (select 1 from lpedorapp where edoracceptno = a.edoracceptno and edorstate = '0') ";
  var result2 = easyExecSql(sql2);
  var sqlw = "select max(payenddate) from lcpol p where contno='" + aContNo + "' and polno=mainpolno ";
 // + " and exists (select 1 from lmriskapp where riskcode = p.riskcode and risktype4 = '4') ";
  var rsw = easyExecSql(sqlw);
//  alert("rsw==="+rsw);
	//校验环境-模拟，校验受理号：20130221000001，保单号：000074562000005，项目：BA
//  var sql_cvalidate = "select cvalidate from lccont where contno='"+aContNo+"' ";  
//  var cvalidate = easyExecSql(sql_cvalidate);
//  alert("cvalidate==="+cvalidate);
//  var sql20130220 = "select year(date('"+aCurPayToDate+"')-date('"+cvalidate+"')) from dual";
  var sql_years = "select (date('"+rsw[0][0]+"')+year(current date - date('"+rsw[0][0]+"'))year) from dual  ";
  var rsyear = easyExecSql(sql_years);
  var sql_niandu="";
  if(aEdorType=="BA"){
   sql_niandu = "select count(1) from lpedoritem a where contno='"+aContNo+"' and edortype = '"+aEdorType+"' "
               + "and edorvalidate <= '"+rsyear[0][0]+"' and edorvalidate>=(date('"+rsyear[0][0]+"')- 1 year) "
			   + "and exists (select 1 from lpedorapp where edoracceptno = a.edoracceptno and edorstate = '0') ";
  }else{
   sql_niandu = "select count(1) from lpedoritem a where contno='"+aContNo+"' and edortype = '"+aEdorType+"' "
               + "and edorvalidate >= '"+rsyear[0][0]+"' and edorvalidate<=(date('"+rsyear[0][0]+"')+ 1 year) "
			   + "and exists (select 1 from lpedorapp where edoracceptno = a.edoracceptno and edorstate = '0') ";
  }
  var rsnd = easyExecSql(sql_niandu);
  
     	  if("0"!=rsnd){
		  	if(fm.all('EdorAppDate').value>aCurPayToDate&&aEdorType!='BP'&&aCurPayToDate != rsw[0][0]){
				alert("您的保单交至日期为"+aCurPayToDate+"。目前，您还没有续交保费，因此不可以申请"+EdorName+"请您先去缴费");
				return false;
			}
			alert("该保单本保单年度已经做过"+EdorName+"，不能再次申请！");
			return false;
		  }
  
  
//  alert("rsnd==="+rsnd);
//   if(aCurPayToDate == rsw[0][0]){
////   	alert("进来了");
//   	  if("0"!=rsnd){
//		alert("该保单本保单年度已经做过"+EdorName+"，不能再次申请！");
//  	    return false;
//	  }
//   }else if("0"!=result2[0][0]){
//  	// 【OoO?】杨天政  添加  这只是一个提示
//  	  if(fm.all('EdorAppDate').value>aCurPayToDate&&aEdorType!='BP')
//  	  {
//  	     if(aCurPayToDate != rsw[0][0]){
//            alert("您的保单交至日期为"+aCurPayToDate+"。目前，您还没有续交保费，因此不可以申请"+EdorName+"请您先去缴费");
//  	        return false;
//  	     }  
//  	 }
//    alert("该保单本保单年度已经做过"+EdorName+"，不能再次申请！");
//  	return false;  
//  }
  return true;
}

//缓交状态不能申请追加保费
//保全生效日期不能晚于交至日期
//该保单有保费未缴纳，不能申请追加保费
function checkULIZB()
{
  var tContNo = fm.ContNo.value;
  var sql="select PolNo, standbyflag1, PayToDate, PayEndDate, Current Date from LCPol where  ContNo='"+tContNo+"' and PolNo = MainPolNo"; 
  var result = easyExecSql(sql);
  
  var tPolNo = result[0][0];                            
  var tHuan = result[0][1]; 
  var tPayToDate = result[0][2];
  var tPayEndDate = result[0][3];
  var tCurrDate = result[0][4];
  
  var tEdorValiDate = fm.all('EdorValiDate').value;
  
  var tDayDiff=dateDiff(tPayEndDate,tCurrDate,"D");
  if(tDayDiff>0)    
  {
    //已过缴费截止日
    var tDayDiff1=dateDiff(tPayToDate,tPayEndDate,"D");
    if(tDayDiff1>0)                                            
    {	                                                        
    	alert("该保单有保费未缴纳，不能申请追加保费!");                           
    	return false;    		                                    
    }
    	                                                        
    var tDayDiff2=dateDiff(tPayEndDate,tEdorValiDate,"D");          
    if(tDayDiff2<0)                                            
    {	                                                        
    	alert("保全生效日期不能早于保单上次缴费日期！");                           
    	return false;    		                                    
    }	                                                        
    return true;  		                                    
  }
  else
  {
    //缴费截止日之前
    if("1"==tHuan)
    {
      alert("该保单现在是保费缓交状态，不能申请追加保费！");
      return false;
    }
    
    var sql1 = "select lastpaytodate,curpaytodate from ljapayperson where polno = '"+tPolNo+"' order by curpaytodate desc ";
    var result1 = easyExecSql(sql1);
    var tLastPayToDate = result1[0][0];                            
    var tCurPayToDate = result1[0][1];
                                                              
    var tDayDiff1=dateDiff(tCurPayToDate,tCurrDate,"D");
    if(tDayDiff1>0)                                            
    {	                                                        
    	alert("该保单有保费未缴纳，不能申请追加保费!");                           
    	return false;    		                                    
    }
    	                                                        
    var tDayDiff2=dateDiff(tLastPayToDate,tEdorValiDate,"D");          
    var tDayDiff3=dateDiff(tEdorValiDate,tCurPayToDate,"D");
    if(tDayDiff2<0)                                            
    {	                                                        
    	alert("保全生效日期不能早于保单上次缴费日期！");                           
    	return false;    		                                    
    }	                                                        
    if(tDayDiff3<0)                                           
    {                                                         
    	alert("保全生效日期不能晚于保单本次交至日期！");
    	return false;    	                                      
    }
    return true;
  
  }	
}

function checkULIVali()
{
	var tContNo = fm.all('ContNo').value;
	var tEdorType = fm.all('EdorType').value;
	var tEdorValiDate = fm.all("EdorValiDate").value;
	if(tEdorType=="WT" || tEdorType=="CT" || tEdorType=="XT")
	{
		var sql = "select 1 from lcinsureacctrace where  ContNo='" + tContNo + "' and moneytype='B' and paydate>'" + tEdorValiDate + "'  ";
		var result = easyExecSql(sql);
		if(result)
		{
			alert("保单退保的生效日期不能早于最后一次持续奖金的赠送日期！");
			return false;
		}
	}
	return true;	
}


function checkULI231001()
{
	var tContNo = fm.all('ContNo').value;
	var tEdorType = fm.all('EdorType').value;
	var sql = "select 1 from LCPol where RiskCode = '231001' and ContType = '1' and AppFlag = '1' and  ContNo='" + tContNo + "' ";
	var result = easyExecSql(sql);
	
	if(result)
	{
		if(tEdorType=="FX" 
//		|| tEdorType=="BP" 
//		|| tEdorType=="XT"
		)
		{
			alert("保单含有险种附加个人重疾，不能添加该保全项目！");
			return false;
		}
	}
	return true;	
}

function alertDate()
{
  var sql1 = "select current date from dual ";
  var result1 = easyExecSql(sql1);
  var tCurrDate = result1[0][0];                            
  alert(tCurrDate);
}


/**
 * 新增税优保单复效，且只针对月缴的,交至日期小于满期日期的税优保单
 * @param edorType
 * @return
 */
function checkTaxFX(edorType){
	if(edorType != "FX"){
		return true;
	}
	var checkRiskSQL = "select 1 from lccont a where a.contno = '"+ fm.ContNo1.value +"' and (select count(1) from lcpol where contno = '"+ fm.ContNo1.value +"') = 1 and exists (select 1 from lmriskapp where taxoptimal = 'Y' and riskcode in (select riskcode from lcpol where contno = '"+ fm.ContNo1.value +"'))";
	var result = easyExecSql(checkRiskSQL);
	if(result){
		var payIntvSQL = "select payIntv from lcpol where contno = '"+ fm.ContNo1.value +"'";
		var payIntv = easyExecSql(payIntvSQL);
		if(payIntv!=1){
			alert("该税优保单不是月缴，不能做保单复效！");
			return false;
		}
		var checkOwingSQL = "select 1 from LCCont where ContNo = '"+fm.ContNo1.value+"' and PayToDate < CInValiDate";
		var isOWing = easyExecSql(checkOwingSQL);
		if(isOWing){
			return true;
		}else{
			alert("该税优保单下没有欠缴保费，不能做保单复效！");
			return false;
		}
	}
	return true;
}