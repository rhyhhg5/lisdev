<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LPTypeIAPersonNew.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="LPTypeIAPersonNew.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LPTypeIAPersonInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LPTypeIAPersonSave.jsp" method=post name=fm target="fraSubmit">
    <table>
         <td >
         <Input class= common type=Button value="保 存" onclick="insertClick()">
         </td>
         <td >
         <Input class= common type=Button value="返 回" onclick="returnParent()">      
         </td>
         <td >
         <Input class= common type=Button value="关 闭" onclick="closeCurrentWindow()">      
         </td>
    </table>
    	
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
	      </td>
	      <td class= titleImg>
	        个人客户信息（带红色 <font color=red>*</font> 号为必填项）
	      </td>
	  </tr>
    </table>
    
    <Div  id= "divLDPerson1" style= "display: ''">
      <table  class= common>
       <TR  class= common> 
         <TD  class= title>
            客户号码
          </TD>
          <TD  class= input>
            <Input class="readonly" name=CustomerNo readonly > 
          </TD>       
        </TR>
        <TR  class= common>     
          <TD  class= title>
            客户姓名 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class= common name=Name verify="客户姓名|NOTNULL">
          </TD>
          <TD  class= title>
            客户性别 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class="code" name=Sex verify="客户性别|code:Sex&NOTNULL" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">            
          </TD>
          <TD  class= title>
            客户出生日期 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <input class="coolDatePicker" verify="客户出生日期|DATE&NOTNULL" dateFormat="short" name="Birthday" >           
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= title>
            籍贯 
          </TD>
          <TD  class= input>
            <Input class="code" name=NativePlace ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">                       
          </TD>
          <TD  class= title>
            工资 
          </TD>
          <TD  class= input>
            <Input class= common name=Salary >
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= title>
            证件类型 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class="code" name=IDType verify="证件类型|code:IDType&NOTNULL" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">                                                                                             
          </TD>
          <TD  class= title>
            证件号码 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class= common name=IDNo verify="证件号码|NOTNULL">
          </TD> 
          <TD  class= title>
            邮政编码 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class= common name=ZipCode verify="邮政编码|NOTNULL">
          </TD>         
        </TR>

        <TR  class= common>
          <TD  class= title>
            通讯地址 <font color=red>*</font>
          </TD>
          <TD  class= input colspan=5>
            <Input class= common5 name=PostalAddress verify="通讯地址|NOTNULL">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            电话1 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class= common name=Phone verify="电话1|NOTNULL">
          </TD>
          <TD  class= title>
            电话2 
          </TD>
          <TD  class= input>
            <Input class= common name=Mobile >
          </TD>
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= title>
            单位名称 
          </TD>
          <TD  class= input>
            <Input class= common name=GrpName >
          </TD>
          <TD  class= title>
            单位电话 
          </TD>          
          <TD  class= input>
            <Input class= common name=GrpPhone >
          </TD>
          
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            单位地址 
          </TD>
          <TD  class= input colspan=5>
            <Input class= common5 name=GrpAddress >
          </TD>
        </TR>
 
        <TR  class= common>     
          <TD  class= title>
            职业（工种）
          </TD>
          <TD  class= input>
            <Input class=common name=WorkType >
          </TD>              
          <TD  class= title>
            职业代码 
          </TD>
          <TD  class= input>
						<Input class="code" name=OccupationCode ondblclick="return showCodeList('OccupationCode',[this]);" onkeyup="return showCodeListKey('OccupationCode',[this]);">
          </TD>
        </TR>   

          <TD>
          <input type=hidden name=Transact >
          </TD>        
      </table>
    </Div>
    
    <Div  id= "divHidden" style= "display: 'none'">
          <TD  class= title>
            密码
          </TD>
          <TD  class= input>
            <Input class= common name=Password >
          </TD>
          <TD  class= title>
            民族 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class="code" name=Nationality ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">                                  
          </TD>
          <TD  class= title>
            婚姻状况 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class="code" name=Marriage ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);">                                              
          </TD>
          <TD  class= title>
            结婚日期
          </TD>
          <TD  class= input>
            <input class=common  name=MarriageDate >                       
          </TD>
          <TD  class= title>
            职业类别 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class="code" name=OccupationType ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">                                                          
          </TD>
          <TD  class= title>
            开始工作日期 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <input class="coolDatePicker1" dateFormat="short" name="StartWorkDate" >                                   
          </TD>
          <TD  class= title>
            健康状况 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class="code" name=Health ondblclick="return showCodeList('Health',[this]);" onkeyup="return showCodeListKey('Health',[this]);">                                                                      
          </TD>
          <TD  class= title>
            身高 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class= common name=Stature >
          </TD>
          <TD  class= title>
            体重 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class= common name=Avoirdupois >
          </TD>
          <TD  class= title>
            信用等级
          </TD>
          <TD  class= input>
            <Input class="code" name=CreditGrade ondblclick="return showCodeList('CreditGrade',[this]);" onkeyup="return showCodeListKey('CreditGrade',[this]);">                                                                                  
          </TD>
          <TD  class= title>
            属性
          </TD>
          <TD  class= input>
            <Input class="code" name=Proterty ondblclick="return showCodeList('Proterty',[this]);" onkeyup="return showCodeListKey('Proterty',[this]);">                                                                                                        
          </TD>
          <TD  class= title>
            其它证件类型
          </TD>
          <TD  class= input>
            <Input class="code" name=OthIDType verify="证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">                                                                                                                    
          </TD>
          <TD  class= title>
            其它证件号码
          </TD>
          <TD  class= input>
            <Input class= common name=OthIDNo >
          </TD>
          <TD  class= title>
            ic卡号
          </TD>
          <TD  class= input>
            <Input class= common name=ICNo >
          </TD>
          <TD  class= title>
            家庭地址编码
          </TD>
          <TD  class= input>
            <Input class= common name=HomeAddressCode >
          </TD>
          <TD  class= title>
            家庭地址 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class= common name=HomeAddress >
          </TD>
          <TD  class= title>
            传呼
          </TD>
          <TD  class= input>
            <Input class= common name=BP >
          </TD>
          <TD  class= title>
            入司日期
          </TD>
          <TD  class= input>
            <input class="coolDatePicker1" dateFormat="short" name="JoinCompanyDate" >                                               
          </TD>
          <TD  class= title>
            职位
          </TD>
          <TD  class= input>
            <Input class= common name=Position >
          </TD>
          <TD  class= title>
            单位编码
          </TD>
          <TD  class= title>
            单位地址编码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpAddressCode >
          </TD>
          <TD  class= input>
            <Input class= common name=GrpNo >
          </TD>
          <TD  class= title>
            死亡日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker1" dateFormat="short" name=DeathDate >
          </TD>              
          <TD  class= title>
            备注
          </TD>
          <TD  class= input>
            <Input class= common name=Remark >
          </TD>
          <TD  class= title>
            状态
          </TD>
          <TD  class= input>
            <Input class=common name=State >
          </TD>              
          <TD  class= title>
            黑名单标记
          </TD>
          <TD  class= input>
            <Input class= common name=BlacklistFlag >
          </TD>
          <TD  class= title>
            兼职（工种）
          </TD>
          <TD  class= input>
            <Input class= common name=PluralityType >
          </TD>
          <TD  class= title>
            学历 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class="code" name=Degree ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">
          </TD>
          <TR  class= common>     
          <TD  class= title>
            单位邮编 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class=common name=GrpZipCode >
          </TD>              
          <TD  class= title>
            是否吸烟标志 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class="code" name=SmokeFlag ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
          </TD>
        </TR>  
        <TR  class= common>     
          <TD  class= title>
            户口所在地 <font color=red>*</font>
          </TD>
          <TD  class= input>
          	<Input class="code" name=RgtAddress ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">
          </TD>              
          <TD  class= title>
            家庭地址邮政编码 <font color=red>*</font>
          </TD>
          <TD  class= input>
            <Input class= common name=HomeZipCode >
          </TD>
        </TR>  
        <TR>
        	<TD  class= title>
            联系电话2
          </TD>
          <TD  class= input>
            <Input class= common name=Phone2 >
          </TD>
        </TR>  
        <TR  class= common>
          <TD  class= title>
            银行编码
          </TD>
          <TD  class= input>
            <Input class= common name=BankCode >            
          </TD>
          <TD  class= title>
            银行帐号
          </TD>
          <TD  class= input>
            <Input class= common name=BankAccNo >
          </TD>
      
        </TR> 
    </div>
    
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


