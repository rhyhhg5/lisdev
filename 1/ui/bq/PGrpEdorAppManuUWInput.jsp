<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：PGrpEdorAppManuUWInput.jsp.
//程序功能：保全人工核保
//创建日期：2005-2-24 
//创建人  ：Fanx
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	String type = "";
	type = request.getParameter("Type");//1个单，2团单
	String tEdorAcceptNo = request.getParameter("EdorAcceptNo");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
%>
<html>
<SCRIPT>
    var tType="<%=type%>";
    var tEdorAcceptNo="<%=tEdorAcceptNo%>";
    var tMissionID="<%=tMissionID%>";
    var tSubMissionID="<%=tSubMissionID%>";
    
</SCRIPT>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="PGrpEdorAppManuUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PGrpEdorAppManuUWInit.jsp"%>
</head>

<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="">
    <!-- 保单查询条件 -->
    <Div  id= "divEdorApp" style= "display: ''">
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 保全受理号 </TD>
          <TD  class= input> <Input class= common name=EdorAcceptNo > </TD>       
          <TD  class= title> 管理机构  </TD>
          <TD  class= input>  <Input class="code" name=QManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">  </TD>
         </TR>
         <TR  class= common>
          <TD  class= title> 核保级别  </TD>
          <TD  class= input>  <Input class="code" name=UWGradeStatus value= "1" CodeData= "0|^1|本级保单^2|下级保单" ondblClick="showCodeListEx('Grade',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('Grade',[this,''],[0,1]);">  </TD>
          <TD  class= title>  保单状态 </TD>
          <TD  class= input>  <Input class="code" readonly name=State value= "" CodeData= "0|^1|未人工核保^2|核保已回复^3|核保未回复" ondblClick="showCodeListEx('State',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('State',[this,''],[0,1]);"> </TD>
        </TR>
    </table>
          <INPUT VALUE="查  询" class=cssButton TYPE=button onclick="easyQueryClick();"> 
          <!--INPUT VALUE="撤单申请查询" class=cssButton TYPE=button onclick="withdrawQueryClick();"--> 
          <INPUT type= "hidden" name= "Operator" value= ""> 
    <!-- 保单查询结果部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保全申请查询结果
    		</td>
    	</tr>
    </table>
    
      	<table  class= common>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanEdorAppGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();">      

  </Div>
<Div  id= "divLCPol1" style= "display: 'none'">
    <!-- 保全批单 -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保全批单
    		</td>
    	</tr>
    </table>
    
      	<table  class= common>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanEdorMainGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();">  
      <br>
      <br>
          <INPUT VALUE="保单明细信息" class=cssButton TYPE=button onclick="showPolDetail();"> 
          <INPUT VALUE="扫描件查询" class=cssButton TYPE=button onclick="ScanQuery();"> 
          <INPUT VALUE="投保人既往投保信息" class=cssButton TYPE=button onclick="showApp();"> 
    </div>
   <Div  id= "divMain" style= "display: 'none'">
    <!--保全项目-->
    <Div  id= "divLCPol2" style= "display: 'none'">
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol2);">
    		</td>
    		<td class= titleImg>
    			 保全项目
    		</td>
    	    </tr>
        </table>	
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanEdorItemGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
        <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
        <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
        <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();"> 
		<table  class= common align=center>			
			<TR  class= common>
			  <TD  class= input>
			  <INPUT  type= "hidden" class= Common name= UWGrade value= "">
			  <INPUT  type= "hidden" class= Common name= AppGrade value= "">
			  <INPUT  type= "hidden" class= Common name= EdorNo  value= "">
			  <INPUT  type="hidden" name= "EdorType" value= "">
			  <INPUT  type="hidden"  name="InsuredNo" value= "">
			  <INPUT  type= "hidden" class= Common name= ContNo  value= "">
			  <INPUT  type= "hidden" class= Common name= PolNo  value= "">
			  <INPUT  type= "hidden" class= Common name= MissionID  value= "">
			  <INPUT  type= "hidden" class= Common name= SubMissionID  value= "">
			  <INPUT  type= "hidden" class= Common name= SubNoticeMissionID  value= "">
			 </TD>
		   </TR>
		</table>
		
    </Div>

	
    	  
   </Div>
      <div id = "divUWResult" style = "display: 'none'">
      <table class= common border=0 width=100% >
          <hr>
	  </hr>
          <br><br>
          <tr></tr>
          <input value="体检资料查询" class=cssButton TYPE=button name= "showHealthQ1"  onclick="showHealthQ();" >         
          <INPUT VALUE="生存调查查询" class=cssButton TYPE=button name= "RReportQuery1"  onclick="RReportQuery();">
          <INPUT VALUE="保全明细查询" class=cssButton TYPE=button onclick="PrtEdor();"> 
          <INPUT VALUE="保全项目查询" class=cssButton TYPE=button onclick="ItemQuery();"> 
          <INPUT VALUE="自核未过原因" class=cssButton TYPE=button onclick="showNewUWSub();">    

          <!--INPUT  VALUE="既往理赔查询" class=cssButton TYPE=button onclick="ClaimGetQuery();"--> 
          <br><br>
          <tr></tr>
          <input value="体检资料录入" class=cssButton TYPE=button name= "showHealth1"  onclick="showHealth();" width="200">
          <input value="生调请求说明" class=cssButton TYPE=button name= "showRReport1"  onclick="showRReport();">
          <input value="加费承保录入" class=cssButton TYPE=button name= "showAdd1"  onclick="showAdd();"> 
          <input value="特约承保录入" class=cssButton TYPE=button name= "showSpec1"  onclick="showSpec();"> 
          <input value="发核保通知书" class=cssButton TYPE=button name= "SendNotice1"  onclick="SendAppNotice();"> 
          <!--<input value="核保分析报告录入" class=cssButton TYPE=button onclick="showReport();" width="200">  -->                   
          <input value="记事本" class=cssButton TYPE=button onclick="showNotePad();" width="200">
    </table>
    	  <input type="hidden" name= "PrtNoHide" value= "">
    	  <input type="hidden" name= "ContNoHide" value= "">
    	  <input type="hidden" name= "EdorNoHide" value= "">
    	  <input type="hidden" name= "EdorTypeHide" value= "">    
    	  <!-- 核保结论 -->
    	  <table class= common border=0 width=100%>
    	  	<tr>
			<td class= titleImg align= center>保全核保结论：</td>
	  	</tr>
	  </table>
    
  	  <table  class= common align=center>
    
    	  	<TR  class= common>
          		<TD height="29"  class= title>
          		 	<span id= "UWResult">保全核保结论<Input class="code" name=EdorUWState ondblclick="return showCodeList('edoruwstate',[this]);" onkeyup="return showCodeListKey('edoruwstate',[this]);">  </span>          		
	   			    <span id= "UWDelay" style = "display: ''">延长时间<Input class="common" name=UWDelay value= ""> </span>          		
	   		        <span id= "UWPopedom" style = "display: ''">上报核保<Input class="code" name=UWPopedomCode ondblclick="showCodeList('UWPopedomCode', [this,''], [0, 1],null,codeSql,'1',1);" onkeyup="showCodeListKey('UWPopedomCode', [this,''], [0, 1],null,codeSql,'1',1);"> </span> 
	   		 	    <span id= "Delay" style = "display: 'none'">本单核保师<Input class="common" name=UWOperater value= ""> </span>  
	   		 	   </TD>
          	</TR>
		<tr></tr>
          	<TD height="24"  class= title>
            		保全核保意见
          	</TD>
		<tr></tr>          
      		<TD  class= input> <textarea name="UWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
	  </table>
          
  	<p>
    		<INPUT VALUE="确  定" class=cssButton TYPE=button name= "submitForm1"  onclick="submitForm();">
    		<INPUT VALUE="取  消" class=cssButton TYPE=button name= "cancelchk1"  onclick="cancelchk();">
    		<INPUT VALUE="返  回" class=cssButton TYPE=button name= "cancelchk1"  onclick="returnBack();">
  	</p>
    </div>
    <div id = "divChangeResult" style = "display: 'none'">
      	  <table  class= common align=center>
          	<TD height="24"  class= title>
            		承保计划变更结论录入:
          	</TD>
		<tr></tr>          
      		<TD  class= input><textarea name="ChangeIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
    	 </table>
    	 <INPUT VALUE="确  定" class=cssButton TYPE=button onclick="showChangeResult();">
    	 <INPUT VALUE="取  消" class=cssButton TYPE=button onclick="HideChangeResult();">
    </div>
  <table  class= common align=center>
      	
	</table>         
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
