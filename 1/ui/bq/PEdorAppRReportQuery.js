//程序名称：RReportQuery.js
//程序功能：生存调查报告查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var k = 0;
var turnPage = new turnPageClass();
var cflag = "";  //问题件操作位置 1.核保

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showInfo.close();
    
  }
  else
  { 
	var showStr="操作成功";
  alert(showStr);
  showInfo.close();
  parent.close();
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
         

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function easyQueryClick(tEdorAcceptNo, tCustomerNo)
{
	var strsql = "";
	
	//var tEdorAcceptNo = "";
	
        if(tEdorAcceptNo != "")
        {
		strsql = "select ContNo,name,operator,makedate,replyoperator,replydate,replyflag,prtseq from LPAppRReport where EdorAcceptNo = '"+tEdorAcceptNo+"'" + " and customerno = '" + tCustomerNo + "'";	
		//alert(strsql);
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未查到该客户的生存调查信息！");
	initQuestGrid();
    return true;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = QuestGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
  return true;
}

// 查询按钮
function easyQueryChoClick(parm1,parm2)
{	
	// 书写SQL语句
	k++;
	var tEdorAcceptNo = "";
	var strSQL = "";

	if(fm.all(parm1).all('InpQuestGridSel').value == '1' )
	{
	//当前行第1列的值设为：选中
   		tEdorAcceptNo = fm.all(parm1).all('QuestGrid1').value;
   		tSerialNo = fm.all(parm1).all('QuestGrid8').value;
  }	
	
	if (tEdorAcceptNo != " ")
	{
		strSQL = "select RReportItemCode,RReportItemName from LPAppRReportItem where EdorAcceptNo = '"+tEdorAcceptNo+"' and prtseq = '"+tSerialNo+"'";
	}
	var strsql= "select Content from LPAppRReport where 1=1"	
				+"and EdorAcceptNo = '"+tEdorAcceptNo+"'"; 	
	//alert(strSQL);	
		
	var arrReturn = new Array();
  arrReturn = easyExecSql(strsql);
       
  if(arrReturn!=null)
  {
        
		fm.all('Content').value = arrReturn[0][0];
	}

	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	//alert(strQueryResult[0][0]);
    //判断是否查询成功
  if (!turnPage.strQueryResult) 
    return "";
    
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);    
  //设置初始化过的MULTILINE对象，RReportGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = RReportGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  
  return true;
}

function QueryCustomerList(tEdorAcceptNo)
{
	var i = 0;
	var j = 0;
	var m = 0;
	var n = 0;
	var strsql = "";
	var tCodeData = "0|";
	var strsql = "select distinct CustomerNo,Name from LPAppPENotice where 1=1 "
	       + "and EdorAcceptNo = '" +tEdorAcceptNo+"'";
	turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
	if (turnPage.strQueryResult != "")
	{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		m = turnPage.arrDataCacheSet.length;
		for (i = 0; i < m; i++)
		{
			j = i + 1;
			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][1] + "|" + turnPage.arrDataCacheSet[i][0];
		}
	}

	return tCodeData;
}

function showSelectRecord(tEdorAcceptNo)
{
	easyQueryClick(tEdorAcceptNo, fm.all("CustomerNo").value);
}

function afterCodeSelect(cCodeName, Field)
{
	if (cCodeName == "CustomerName")
	{
		showSelectRecord(fm.all("EdorAcceptNo").value);
	}
}

/*********************************************************************
 *  生调信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function saveRReport()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	
  var tSelNo = QuestGrid.getSelNo();
  var tContNo = QuestGrid.getRowColData(tSelNo-1,1);
  fm.ContNo.value = tContNo;
  //alert(tContNo);
 	if(tSelNo==0)
 	{
 		alert("请选择生调履历！");
 		return;	
 	}
 	//alert(tSelNo);
  fm.all('PrtSeq').value = QuestGrid.getRowColData(tSelNo - 1,8);
  //alert(fm.all('PrtSeq').value);
  fm.action= "./PEdorAppRReportQuerySave.jsp";
  fm.submit(); //提交
}