<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<% 
//程序名称：GEdorTypeMultiDetailInput.jsp
//程序功能：团体保全明细总页面
//创建日期：2003-12-03 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>   
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeMultiDetail.js"></SCRIPT>
  <%@include file="GEdorTypeMultiDetailInit.jsp"%>
  
  <title>团体保全明细总页面</title> 
</head>

<body  onload="initForm();" >
  <form action="./GEdorTypeMultiDetailSubmit.jsp" method=post name=fm target="fraSubmit">    
  <input type=hidden readonly name=EdorAcceptNo >
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 批单号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 批改类型 </TD>
        <TD class = input >
        	<input class = "readonly" type="hidden" readonly name=EdorType>
      		<input class = "readonly" readonly name=EdorTypeName>
        </TD>
        <TD class = title > 集体保单号 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=GrpContNo>
        </TD>   
      </TR>
    </TABLE> 
      <%@include file="SpecialInfoCommon.jsp"%>
    <br><hr>
    
    <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
        </td>
        <td class= titleImg>
          被保人信息(最多显示1000条，若有必要，请查询)
        </td>
      </tr>
    </table>
    
    <table class = common>
      <tr class = common>
      	<td class = title>
      		证件号码
      	</td>
      	<td class = input>
      		<input class = common  name=IDNo>   
        </TD>
        <td class = title>
      		个人客户号
        </td>
      	<td class = input>
      		<input class = common  name=CustomerNo>
        </TD>
        <td class = title>
      		客户姓名
      	</td>
      	<td class = input>
      		<input class = common  name=Name>
        </TD>  
        <td class = title>
      		其他号码
      	</td>
      	<td class = title>
      		<input class = common  name=OthIDNo>
      	</td>
      </tr>
    </table>
    <INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="queryClick();"> 
    <br><br>
    <Div  id= "divLPInsured" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsuredGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage" align=center style= "display: 'none' ">
        <INPUT class=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage(); changeNullToEmpty();"> 					
        <INPUT class=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage(); changeNullToEmpty();"> 			
      </Div>		
  	</div>
	  <br><hr> 
	  <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
        </td>
        <td class= titleImg>
          修改过的被保人信息(最多显示1000条，若有必要，请查询)
        </td>
      </tr>
    </table>
    <table class = common>
      <tr class = common>
      	<td class = title>
      		证件号码
      		</td>
      	<td class = input>
      		<input class = common  name=IDNo3>   
          </TD>
        <td class = title>
      		个人客户号
      		</td>
      	<td class = input>
      		<input class = common  name=CustomerNo3>
          </TD>
        <td class = title>
      		客户姓名
      		</td>
      	<td class = input>
      		<input class = common  name=Name3>
        </TD>  
        <td class = title>
      		其他号码
      	</td>
      	<td class = title>
      		<input class = common  name=OthIDNo3>
      	</td>
      </tr>
    </table>
    <INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="querySelectedInsured();"> 
	  <Div  id= "divLPInsured2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsured2Grid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage2" align=center style= "display: 'none' ">
        <INPUT class=cssButton VALUE="首页" TYPE=button onclick="turnPage2.firstPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage(); changeNullToEmpty();"> 					
        <INPUT class=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage(); changeNullToEmpty();">					
      </Div>
  	</div>
  	<table class="common">
  	<tr>
  	<td>
  	<p><font color="#ff0000" size="2">
    非账户类型减人算费计算公式:
  	<br>按月计算公式为：当期保费*（1-扣费比例）*剩余月份/本期缴费所含月数
  	<br>按日计算公式为：当期保费*（1-扣费比例）*剩余天数/（当期缴费起期-当期缴费止期）
  	<br>增人进入且没有续期记录的当期缴费起期是：增人保全的生效日期；当期保费是：该单增人的保全费用。
  	<br>"计算方式"选择按月计算和按日计算的计算公式不同，因此算出来的费用也不同。
  	</font>
  	</td>
  	</tr>
  	</table>
  	
  	<table class="common">
  	  <tr class="common">
  	    <td class="input">
  	     计算方式按<input type="radio" name="CalTime" value="1" checked>月<input type="radio" name="CalTime" value="2">日计算
  	    </td>
  	    <td class="title">
  	      扣除手续费比例
  	    </td>
  	    <td class="input">
  	      <input class="common" name="FeeRate" value="25">%
  	    </td>
  	  </tr>
  	</table>
  	<div id="calTypeDiv" style="display: 'none'">&nbsp;
	    <input class="codeNo" name="CalType" value="1" 
	        CodeData="0|^1|条款规定算法^2|月比例算法"  
	        ondblclick="return showCodeListEx('CalType',[this,CalTypeName],[0,1]);" 
	        onkeyup="return showCodeListEx('CalType',[this,CalTypeName],[0,1]);"><Input class="codeName" name="CalTypeName" readonly>
	</div>
	  
	  <br><hr>
	  <Input type=Button name="goIntoItem" value="进入个人保全" class=cssButton onclick="pEdorMultiDetail()">
	  <Input type=Button name="goDiskImport" value="磁盘导入" id="goDiskImport" class=cssButton onclick="diskImport()">
	  <Input type=Button name="cancel" value="撤销个人保全" class=cssButton onclick="cancelPEdor()"> 
	 <!-- <Input type=Button value="保  存" class=cssButton onclick="saveEdor()"> --->
	 
	 <!--将原来的返回改成保存并返回，主要目的为更新edorState 为1-录入完成。------------>
	  <Input type=Button name="save" value="保存申请" class=cssButton onclick="returnParent()">
	  
	  <input type=hidden id="ContNo" name="ContNo">
	  <!-- add ContType for PEdor GT -->
	  <input type=hidden id="ContType" name="ContType"> 
	  <input type=hidden id="Transact" name="Transact">
	  <div id="test"></div>
	  <hr>
	  <div id="ErrorsInfo"></div> 
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  window.focus();
</script>
