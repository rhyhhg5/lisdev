//程序名称：Underwrite.js
//程序功能：个人人工核保
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保
var pflag = "1";  //保单类型 1.个人单
var UWConculsionType = "";

// 标记核保师是否已查看了相应的信息
var showPolDetailFlag ;
var showAppFlag ;
var showHealthFlag ;
var QuestQueryFlag ;

//提交，保存按钮对应操作
function submitForm()
{
	//alert(UWConculsionType);
	if (UWConculsionType == "EdorMain")
	{
		if (fm.EdorNo.value == "" || fm.ContNo.value == "")
		{
			alert("请先选择相应的申请批单！");
			return false;
		}
	}
	else if (UWConculsionType == "EdorItem")
	{
		if (fm.EdorNo.value == "" || fm.ContNo.value == "" || fm.EdorType.value == "" || fm.InsuredNo.value == "" || fm.PolNo.value == "")
		{
			alert("请先选择相应的保全项目！");
			return false;
		}
	}

	var i = 0;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	var tEdorUWState = fm.EdorUWState.value;
	var tUWDelay = fm.UWDelay.value;
	var tUWIdea = fm.UWIdea.value;
	var tUWPopedomCode = fm.UWPopedomCode.value;
	if(tEdorUWState == "")
   {
		showInfo.close();
		alert("请先录入保全核保结论!");  
		return ;
	}	
	if(tEdorUWState == "6" && tUWPopedomCode == "")
	{
		showInfo.close();
		alert("若要上报核保,请选择上报级别!");  
		return ;
	}	

	if(tEdorUWState == "2" && tUWDelay == "")
	{
		showInfo.close();
		alert("若要延期承保,请录入延长时间信息!");   
		return ;
	}
	fm.action = "./PEdorUWManuNormChk.jsp?UWConculsionType=" + UWConculsionType;
	fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
 	showInfo.close;
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
   	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2( FlagStr, content )
{
    showInfo.close();
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
   	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	if (FlagStr == "Fail" )
 	{                 
	}
 	else
	{
		if (UWConculsionType == "EdorMain")
		{
			easyQueryClick();
		}
		else if (UWConculsionType == "EdorItem")
		{
			ShowEdorItemList(fm.EdorNo.value);
		}
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterApply( FlagStr, content )
{
	if (FlagStr == "Fail" )
	{                     
		alert(content);    
		// 初始化表格
		HideChangeResult();
		initEdorMainGrid();
		fm.EdorNo.value = "";
		fm.ContNo.value = "";
		divLCPol1.style.display= "";
		divLCPol2.style.display= "none";
		divMain.style.display = "none";    
	}
}

function afterAddFeeApply( FlagStr, content )
{
	if (FlagStr == "Fail" )
	{                     
		alert(content);    
	}
	else
	{ 
		var cPolNo=fm.ContNo.value;
		var cMissionID =fm.MissionID.value; 
		var cSubMissionID =fm.SubMissionID.value; 
		var tSelNo = EdorItemGrid.getSelNo()-1;
		var cEdorNo = EdorItemGrid.getRowColData(tSelNo,1);	
		var cPrtNo = EdorItemGrid.getRowColData(tSelNo,3);	
		var cEdorType = EdorItemGrid.getRowColData(tSelNo,7);	

		if (cPrtNo != ""&& cEdorNo !="" && cMissionID != "" )
		{
			window.open("./PEdorUWManuAddMain.jsp?PrtNo="+cPrtNo+"&PolNo="+cPolNo+"&EdorNo="+cEdorNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&EdorType="+cEdorType,"window1"); 
			showInfo.close();
		}
		else
		{
			showInfo.close();
			alert("请先选择保单!");
		}
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

/*********************************************************************
 *  选择核保结后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) {
	
		if( cCodeName == "EdorUWState" ) {
			DoUWStateCodeSelect(Field.value);//loadFlag在页面出始化的时候声明
		}
}

/*********************************************************************
 *  根据不同的核保结论,处理不同的事务
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DoUWStateCodeSelect(cSelectCode) {
	
	if(trim(cSelectCode) == '6')//上保核保
	{
		 uwgrade = fm.all('UWGrade').value;
         appgrade= fm.all('AppGrade').value;
         if(uwgrade==null||uwgrade<appgrade)
         {
         	uwpopedomgrade = appgrade ;
         }
        else
         {
        	uwpopedomgrade = uwgrade ;
         }
        //alert(uwpopedomgrade);
        codeSql="#1# and Comcode like #"+ comcode+"%%#"+" and Edorpopedom > #"+uwpopedomgrade+"#"	;
         //alert(codeSql);	
	}
	else
	codeSql="";    	
}


//既往投保信息
function showApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cContNo=fm.ContNo.value;
  var cInsureNo = fm.InsuredNo.value;
  if (cContNo != "")
  {
    var tSelNo = EdorItemGrid.getSelNo()-1;
  	showAppFlag[tSelNo] = 1 ;
  	window.open("../uw/UWAppMain.jsp?ContNo="+cContNo+"&AppntNo="+cInsureNo);
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  	
  }
}         

//以往核保记录
function showOldUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cContNo=fm.ContNo.value;
  //showModalDialog("./UWSubMain.jsp?ContNo1="+cContNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  if (cContNo != "")
  {
  	window.open("./UWSubMain.jsp?ContNo1="+cContNo,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  	
  }
}

//当前核保记录
function showNewUWSub()
{
  var tSelNo = EdorItemGrid.getSelNo();
  var cEdorNo = fm.EdorNo.value;	
  var cEdorType = fm.EdorType.value;				
  var cContNo=fm.ContNo.value;

  if (cContNo != "" && cEdorNo !="" && cEdorType != "")
  {
  	window.open("./PEdorUWErrMain.jsp?ContNo="+cContNo+"&EdorNo="+cEdorNo+"&EdorType="+cEdorType,"window1");
  }
  else
  {
  	alert("请先选择保全项目!");  
  }
}                      


// 该保单的理赔给付查询
function ClaimGetQuery2()
{
	var arrReturn = new Array();
	var tSel = EdorItemGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = EdorItemGrid.getRowColData(tSel - 1,2);				
		if (cPolNo == "")
		    return;
		  window.open("../sys/ClaimGetQueryMain.jsp?PolNo=" + cPolNo);										
	}	
}

//基于被保人的理赔给付查询
function ClaimGetQuery()
{
	var arrReturn = new Array();
	var tSel = EdorItemGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cInsuredNo = fm.InsuredNo.value;				
		if (cInsuredNo == "")
		    return;
		  window.open("../sys/AllClaimGetQueryMain.jsp?InsuredNo=" + cInsuredNo);										
	}	
}


//保单明细信息
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cContNo=fm.ContNo.value;
  if (cContNo != "")
  {
  	var tSelNo = EdorItemGrid.getSelNo()-1;
  	showPolDetailFlag[tSelNo] = 1 ;
  	mSwitch.deleteVar( "PolNo" );
  	mSwitch.addVar( "PolNo", "", cContNo );
  	mSwitch.updateVar("PolNo", "", cContNo);		
  	var prtNo = EdorItemGrid.getRowColData(EdorItemGrid.getSelNo() - 1, 2);
  	window.open("../sys/AllProQueryMain.jsp?LoadFlag=3","window1");
  	
  }
  else
  {  	
  	alert("请先选择保单!");	
  }

}           


//扫描件查询
function ScanQuery()
{
	var tSel = EdorItemGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var prtNo = EdorItemGrid.getRowColData(tSel - 1,3);				
		if (prtNo == "")
		    return;	
		  window.open("../sys/ProposalEasyScan.jsp?prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");								
	}	     
}


//体检资料查询
function showHealthQ()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cContNo = fm.ContNo.value;
  var cInsuredNo = fm.InsuredNo.value;
  var cSelNo = EdorItemGrid.getSelNo()-1;
  var cEdorNo = EdorItemGrid.getRowColData(cSelNo,1);		
  alert(cEdorNo);
  if (cContNo != "" && cInsuredNo != "" && cEdorNo != "" )
  {  
   	window.open("./PEdorUWManuHealthQMain.jsp?PolNo="+cContNo+"&EdorNo="+cEdorNo+"&InsuredNo="+cInsuredNo,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}           

// 项目明细查询
function ItemQuery()
{
	var arrReturn = new Array();
	var tSel = EdorItemGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击明细查询按钮。" );
	else
	{
	    var cEdorNo = EdorItemGrid.getRowColData(tSel - 1,1);	
	    var cSumGetMoney = 	EdorItemGrid.getRowColData(tSel - 1,8);			
		
		if (cEdorNo == "")
		   { 
		   	alert( "请先选择一条申请了保全项目的记录，再点击保全项目查询按钮。" );
		   	return;
		   }
		window.open("../sys/AllPBqItemQueryMain.jsp?EdorNo=" + cEdorNo + "&SumGetMoney=" + cSumGetMoney);		
								
	}
}

//查看批单信息
function PrtEdor()
{	
	var tSel = EdorItemGrid.getSelNo();
    if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击批单查看按钮。" );
	else
		{
			var cEdorNo = EdorItemGrid.getRowColData(tSel - 1,1);	
			if (cEdorNo == "")
		   { 
			   	alert( "请先选择一条申请了保全项目的记录，再点击保全明细查询按钮。" );
			   	return;
		   }			   
			fm.all('EdorNo').value = EdorItemGrid.getRowColData(tSel - 1,1);	
			fm.all('PolNo').value = EdorItemGrid.getRowColData(tSel - 1,2);	
			
			var taction = parent.fraInterface.fm.action;
			var ttarget = parent.fraInterface.fm.target;				
			parent.fraInterface.fm.action = "../f1print/EndorsementF1P.jsp";
			parent.fraInterface.fm.target="f1print";							
			fm.submit();
			
			parent.fraInterface.fm.action = taction;
			parent.fraInterface.fm.target=ttarget;	
			fm.all('EdorNo').value = '';	
			fm.all('PolNo').value = '';					
		}
}


//体检资料
function showHealth()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cContNo = fm.ContNo.value;
  var cMissionID = fm.MissionID.value;
  var cSubMissionID = fm.SubMissionID.value;
  if (cContNo != "")
  {
  	var tSelNo = EdorMainGrid.getSelNo()-1;
  	var tEdorNo = EdorMainGrid.getRowColData(tSelNo,1);	
  	showHealthFlag[tSelNo] = 1 ;
  	window.open("./PEdorUWManuHealthMain.jsp?ContNo="+cContNo+"&EdorNo="+tEdorNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}           

//特约承保
function showSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cPolNo=fm.ContNo.value;
  var cMissionID =fm.MissionID.value; 
  var cSubMissionID =fm.SubMissionID.value; 
  var tSelNo = EdorItemGrid.getSelNo()-1;
  var cEdorNo = EdorItemGrid.getRowColData(tSelNo,1);	
  tUWIdea = fm.all('UWIdea').value;
  if (cPolNo != ""&& cEdorNo !="" && cMissionID != "" )
  { 	
  	window.open("./PEdorUWManuSpecMain.jsp?PolNo="+cPolNo+"&EdorNo="+cEdorNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID,"window1");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}

//加费承保
function showAdd()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  var tInfo = "确定要加费或是查看已加费历史信息吗?";
  if(!window.confirm( tInfo ))
        return;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    
  var cSelNo = EdorItemGrid.getSelNo()-1;
  fm.EdorNoHide.value = EdorItemGrid.getRowColData(cSelNo,1);	
  fm.ContNoHide.value = EdorItemGrid.getRowColData(cSelNo,2);	
  fm.PrtNoHide.value = EdorItemGrid.getRowColData(cSelNo,3);
  fm.EdorTypeHide.value = EdorItemGrid.getRowColData(cSelNo,7);  
    
  if (fm.EdorNoHide.value != ""&& fm.ContNoHide.value !="" && fm.EdorTypeHide.value != "" )
  {
  	  fm.action = "./PEdorManuUWAddFeeApply.jsp";
      fm.submit();
 
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}
//生存调查报告
function showRReport()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cContNo=fm.ContNo.value;
  var cMissionID =fm.MissionID.value; 
  var cSubMissionID =fm.SubMissionID.value; 
  var tSelNo = EdorItemGrid.getSelNo()-1;
  var cEdorNo = EdorItemGrid.getRowColData(tSelNo,1);	
  
  if (cContNo != "" && cEdorNo != "" && cMissionID != "")
  {
  	window.open("./PEdorUWManuRReportMain.jsp?PolNo="+cContNo+"&EdorNo="+cEdorNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&Flag="+pflag,"window1");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}                     

//核保报告书
function showReport()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cContNo=fm.ContNo.value;
  tUWIdea = fm.all('UWIdea').value;
  if (cContNo != "")
  {
  	window.open("../uw/UWManuReportMain.jsp?ContNo1="+cContNo,"window1");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}         

//发核保通知书
function SendNotice()
{
  cContNo = fm.ContNo.value;
  fm.EdorUWState.value = "8";
  
  if (cContNo != "")
  {  	
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	  cContNo=fm.ContNo.value;
	  var cMissionID =fm.MissionID.value; 
	  var cSelNo = EdorItemGrid.getSelNo()-1;
	  var cEdorNo = EdorItemGrid.getRowColData(cSelNo,1);	
	  fm.EdorNo.value = cEdorNo ;
	  strsql = "select LWMission.SubMissionID from LWMission where 1=1"
				 + " and LWMission.MissionProp1 = '" + cEdorNo +"'"
				 + " and LWMission.MissionProp2 = '"+ cContNo + "'"
				 + " and LWMission.ActivityID = '0000000005'"
				 + " and LWMission.ProcessID = '0000000000'"
				 + " and LWMission.MissionID = '" +cMissionID +"'";				 
	turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);    
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    	showInfo.close();
     	alert("不容许发放新的核保通知书,原因可能是:1.已发核保通知书,但未打印.2.未录入核保通知书内容.");
        fm.SubNoticeMissionID.value = "";
        return ;
    } 
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.SubNoticeMissionID.value = turnPage.arrDataCacheSet[0][0]; 
    var tSubNoticeMissionID =   fm.SubNoticeMissionID.value ;
    if (cContNo != "" && cEdorNo != "" && cMissionID != "" && tSubNoticeMissionID != "" )
	  {
	  	showInfo.close();
	  	fm.action = "./PEdorUWManuSendNoticeChk.jsp";
	    fm.submit();
	  }
	  else
	  {
  	   showInfo.close();
  	   alert("请先选择保单!");
      }
  }
  else
  {  	
  	alert("请先选择保单!");
  }
}

function SendHealth()
{
	//
	}
//发首交通知书
function SendFirstNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cContNo=fm.ContNo.value;
  cOtherNoType="00"; //其他号码类型
  cCode="07";        //单据类型
  
  if (cContNo != "")
  {
  	showModalDialog("./UWSendPrintMain.jsp?ContNo1="+cContNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}

//发催办通知书
function SendPressNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cContNo=fm.ContNo.value;
  cOtherNoType="00"; //其他号码类型
  cCode="06";        //单据类型
  
  if (cContNo != "")
  {
  	showModalDialog("./UWSendPrintMain.jsp?ContNo1="+cContNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  	showInfo.close();
  	 }
  else
 
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

var withdrawFlag = false;
//撤单申请查询,add by Minim
function withdrawQueryClick()
{
	withdrawFlag = true;
	easyQueryClick();
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	HideChangeResult();
	initEdorMainGrid();
	fm.EdorNo.value = "";
	fm.ContNo.value = "";
	divLCPol1.style.display= "";
    divLCPol2.style.display= "none";
    divMain.style.display = "none";
	
	// 书写SQL语句
	k++;
	var uwgradestatus = fm.UWGradeStatus.value;
	var mOperate = fm.Operator.value;
	var state = fm.State.value;       //保单所处状态 
	var strSQL = "select a.EdorNo, a.ContNo, a.EdorValiDate, a.EdorAppDate, a.ChgPrem, a.ChgAmnt, a.GetMoney, a.GetInterest, a.ManageCom, b.MissionId, b.SubMissionID from LPEdorMain a, LWMission b where " + k + "=" + k
				 + " and trim(a.EdorNo) = b.MissionProp1 " 
				 + " and b.ProcessID = '0000000000' " //保全核保工作流
				 + " and b.ActivityID = '0000000000' " //保全核保工作流中的待人工核保任务节点
				 + getWherePart( 'b.MissionProp4','QManageCom' );
				+ " and b.MissionProp4 like '" + comcode + "%%'";  //集中权限管理体现	
	var strCase = "";
	if (uwgradestatus == "1")//本级保单
	{
		strCase = " and ((select UWPopedom from LDUser where UserCode = '"+mOperate+"') = (select max(AppGrade) from LPCUWMaster where trim(EdorNo) = b.MissionProp1 and trim(ContNo) = b.MissionProp2))"
	}
	else if (uwgradestatus == "2")//下级保单
	{
		strCase = " and ((select UWPopedom from LDUser where UserCode = '"+mOperate+"') > (select max(AppGrade) from LPCUWMaster where trim(EdorNo) = b.MissionProp1 and trim(ContNo) = b.MissionProp2))"
	}
	else //本级+下级保单
	{
		strCase = " and ((select UWPopedom from LDUser where usercode = '"+mOperate+"') >= (select max(AppGrade) from LPCUWMaster where trim(EdorNo) = b.MissionProp1 and trim(ContNo) = b.MissionProp2))"
	}
	strSQL += strCase;
	
	var tOperator = fm.Operator.value;
	if(state == "1")
	{	
		strSQL = strSQL + " and  b.ActivityStatus = '1'";
	}
	if(state == "2")
	{
		strSQL = strSQL + " and  b.ActivityStatus = '3'";
	}
	if(state == "3")
	{
		strSQL = strSQL + " and  b.ActivityStatus = '2'";
	}
	
	if(tOperator != "" && state != "1")
	   strSQL = strSQL + " and b.DefaultOperator = '" + tOperator + "'" ;
	
	if (withdrawFlag) {
	  strSQL = "select LCPol.ContNo,LCPol.PrtNo,LMRisk.RiskName,LCPol.AppntName,LCPol.InsuredName "
           + " from LCPol,LCUWMaster,LMRisk where 10=10 "
           + " and LCPol.AppFlag='0'  "
           + " and LCPol.UWFlag not in ('1','2','a','4','9')  "
           + " and LCPol.grppolno = '00000000000000000000' and LCPol.contno = '00000000000000000000' "
           + " and LCPol.ContNo = LCPol.MainPolNo  and LCPol.ContNo= LCUWMaster.ContNo  "
           + " and LCUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"') "
           + " and LCPol.ManageCom like '" + manageCom + "%%'"
           + " and LMRisk.RiskCode = LCPol.RiskCode "
           + getWherePart( 'LCUWMaster.Operator','QOperator')
           + " and LCPol.PrtNo in (select prtno from LCApplyRecallPol where ApplyType='0')";

	  withdrawFlag = false;
	}

	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("没有没通过保全核保的个人单！");
		return "";
	}

	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	//设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = EdorMainGrid;    
		  
	//保存SQL语句
	turnPage.strQuerySql     = strSQL; 

	//设置查询起始位置
	turnPage.pageIndex       = 0;  

	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

	return true;
}

function initQuery()
{
	// 初始化表格
	HideChangeResult();
	initEdorMainGrid();
	fm.EdorNo.value = "";
	fm.ContNo.value = "";
	divLCPol1.style.display= "";
        divLCPol2.style.display= "none";
        divMain.style.display = "none";
	
	// 书写SQL语句
	k++;
	var uwgradestatus = fm.UWGradeStatus.value;
	var mOperate = fm.Operator.value;
	var state = fm.State.value;       //保单所处状态 
	var strSQL = "select a.EdorNo, a.ContNo, a.EdorValiDate, a.EdorAppDate, a.ChgPrem, a.ChgAmnt, a.GetMoney, a.GetInterest, a.ManageCom, b.MissionId, b.SubMissionID from LPEdorMain a, LWMission b ,LPEdorApp c where " + k + "=" + k
	                         + " and c.EdorAcceptNo = '"+edorAcceptNo+"' "
	                         + " and a.EdorAcceptNo = c.EdorAcceptNo " 
				 + " and trim(a.EdorNo) = b.MissionProp1 " 
				 + " and b.ProcessID = '0000000000' " //保全核保工作流
				 + " and b.ActivityID = '0000000000' " //保全核保工作流中的待人工核保任务节点
				 + getWherePart( 'b.MissionProp4','QManageCom' );
				+ " and b.MissionProp4 like '" + comcode + "%%'";  //集中权限管理体现	
	var strCase = "";
	//if (uwgradestatus == "1")//本级保单
	//{
	//	strCase = " and ((select UWPopedom from LDUser where UserCode = '"+mOperate+"') = (select max(AppGrade) from LPCUWMaster where trim(EdorNo) = b.MissionProp1 and trim(ContNo) = b.MissionProp2))"
	//}
	//else if (uwgradestatus == "2")//下级保单
	//{
	//	strCase = " and ((select UWPopedom from LDUser where UserCode = '"+mOperate+"') > (select max(AppGrade) from LPCUWMaster where trim(EdorNo) = b.MissionProp1 and trim(ContNo) = b.MissionProp2))"
	//}
	//else //本级+下级保单
	//{
		strCase = " and ((select UWPopedom from LDUser where usercode = '"+mOperate+"') >= (select max(AppGrade) from LPCUWMaster where trim(EdorNo) = b.MissionProp1 and trim(ContNo) = b.MissionProp2))"
	//}
	strSQL += strCase;
	
	var tOperator = fm.Operator.value;
	/**if(state == "1")
	{	
		strSQL = strSQL + " and  b.ActivityStatus = '1'";
	}
	if(state == "2")
	{
		strSQL = strSQL + " and  b.ActivityStatus = '3'";
	}
	if(state == "3")
	{
		strSQL = strSQL + " and  b.ActivityStatus = '2'";
	}
	
	if(tOperator != "" && state != "1")
	   strSQL = strSQL + " and b.DefaultOperator = '" + tOperator + "'" ;
	
	if (withdrawFlag) {
	  strSQL = "select LCPol.ContNo,LCPol.PrtNo,LMRisk.RiskName,LCPol.AppntName,LCPol.InsuredName "
           + " from LCPol,LCUWMaster,LMRisk where 10=10 "
           + " and LCPol.AppFlag='0'  "
           + " and LCPol.UWFlag not in ('1','2','a','4','9')  "
           + " and LCPol.grppolno = '00000000000000000000' and LCPol.contno = '00000000000000000000' "
           + " and LCPol.ContNo = LCPol.MainPolNo  and LCPol.ContNo= LCUWMaster.ContNo  "
           + " and LCUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"') "
           + " and LCPol.ManageCom like '" + manageCom + "%%'"
           + " and LMRisk.RiskCode = LCPol.RiskCode "
           + getWherePart( 'LCUWMaster.Operator','QOperator')
           + " and LCPol.PrtNo in (select prtno from LCApplyRecallPol where ApplyType='0')";

	  withdrawFlag = false;
	}*/

	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("没有没通过保全核保的个人单！");
		window.location="./PGrpEdorAppManuUWInput.jsp?Type=1";
		return "";
	}

	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	//设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = EdorMainGrid;    
		  
	//保存SQL语句
	turnPage.strQuerySql     = strSQL; 

	//设置查询起始位置
	turnPage.pageIndex       = 0;  

	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

	return true;
}
// 查询按钮
function easyQueryAddClick(parm1,parm2)
{
	
	// 书写SQL语句
	var uwgrade = fm.UWGradeStatus.value;
	var mOperate = fm.Operator.value;
	var state = fm.State.value;       //投保单所处状态 
	var tContNo = "";
	var tPEdorNo = "";
	var strSQL = "";
	
	if(fm.all(parm1).all('InpEdorMainGridSel').value == '1')
	{
		//当前行第1列的值设为：选中
   		tContNo = fm.all(parm1).all('EdorMainGrid2').value;
   		tPEdorNo = fm.all(parm1).all('EdorMainGrid1').value;
		fm.all('EdorNo').value = fm.all(parm1).all('EdorMainGrid1').value;
		fm.all('ContNo').value = fm.all(parm1).all('EdorMainGrid2').value;
		fm.all('MissionID').value = fm.all(parm1).all('EdorMainGrid10').value;
		fm.all('SubMissionID').value = fm.all(parm1).all('EdorMainGrid11').value;
  	}

	if(state == "1")
	{
		checkDouble(tContNo);
	}
	ShowEdorItemList(tPEdorNo);
}

function ShowEdorItemList(aPEdorNo)
{
	k++;
	// 初始化表格
	initEdorItemGrid();
	initPolBox();

	//divLCPol1.style.display= "none";
	divLCPol1.style.display= "";
	divLCPol2.style.display= "";
	divMain.style.display = "";

	//strSQL = "select LPEdorItem.EdorNo ,LPEdorItem.ContNo,LPEdorItem.EdorType,LPEdorMain.EdorAppName,LCInsured.Name,LPEdorItem.PolNo from LPEdorMain,LPEdorItem,LCInsured where " + k + "=" + k				 	
	//		+ " and LPEdorItem.EdorNo = '" + aPEdorNo + "' and LPEdorMain.EdorNo = LPEdorItem.EdorNo and LPEdorItem.InsuredNo = LCInsured.InsuredNo " 
	//		+ " and LPEdorItem.ContNo = LCInsured.ContNo and LPEdorItem.GrpContNo = LCInsured.GrpContNo and LPEdorItem.UWFlag not in  ('0', '1', '4', '9')"
	//		+ "  order by LPEdorItem.ContNo,LPEdorItem.makedate ,LPEdorItem.maketime desc";
	
	strSQL="select EdorNo,ContNo,EdorType,InsuredNo,PolNo,UWFlag from LPEdorItem where LPEdorItem.EdorNo = '" + aPEdorNo + "' and LPEdorItem.UWFlag not in  ('0', '1', '4', '9')"
	  //查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		showInfo.close();
		divLCPol1.style.display= "";
		divLCPol2.style.display= "none";
		alert("没有未通过自动核保的保全项目，请对申请批单下核保结论！");
		UWConculsionType = "EdorMain"
		return true;
	}
  
	UWConculsionType = "EdorItem"
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	//设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = EdorItemGrid;    
		  
	//保存SQL语句
	turnPage.strQuerySql     = strSQL; 

	//设置查询起始位置
	turnPage.pageIndex       = 0;  

	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet   = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	initFlag(arrDataSet.length);
	//showInfo.close();
	return true;
}


function displayEasyResult(arrResult)
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initEdorMainGrid();
		fm.EdorNo.value = "";
		fm.ContNo.value = "";
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				EdorMainGrid.setRowColData( i, j+1, arrResult[i][j] );
			}// end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

//申请要人工核保保单
function checkDouble(tContNo)
{
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.ContNoHide.value = tContNo;
	fm.ContNo.value = tContNo;
	fm.action = "./PEdorManuUWApply.jsp";
	fm.submit();
}	

//选择要人工核保保单
function getEdorItemGridCho(parm1,parm2)
{
	var cSelNo = EdorItemGrid.getSelNo() - 1;
	fm.EdorNo.value=fm.all(parm1).all('EdorItemGrid1').value;
	fm.ContNo.value=fm.all(parm1).all('EdorItemGrid2').value;
	fm.EdorType.value = fm.all(parm1).all('EdorItemGrid3').value;
	fm.PolNo.value = fm.all(parm1).all('EdorItemGrid6').value;
	fm.InsuredNo.value = fm.all(parm1).all('EdorItemGrid5').value;

	/**var strSql = "select a.EdorNo, a.InsuredNo, b.Name, c.RiskCode, a.ChgPrem, a.ChgAmnt, a.GetMoney, a.GetInterest, a.ManageCom, d.RiskName, c.AppntNo, c.AppntName"
				+ " from LPEdorItem a left join LPPol c on a.PolNo = c.PolNo, LCInsured b, LMRisk d where a.InsuredNo = b.InsuredNo and a.ContNo = b.ContNo"
				+ " and a.EdorNo = '" + fm.EdorNo.value + "' and a.ContNo = '" + fm.ContNo.value + "' and a.EdorType = '" + tEdorType + "' and a.PolNo = '" + tPolNo + "'"
				+ " and c.RiskCode = d.RiskCode"; 
	var arrResult = easyExecSql(strSql);     //执行SQL语句进行查询，返回二维数组

	if (arrResult == null)
	{
		strSql = "select a.EdorNo, a.InsuredNo, b.Name, c.RiskCode, a.ChgPrem, a.ChgAmnt, a.GetMoney, a.GetInterest, a.ManageCom, d.RiskName, c.AppntNo, c.AppntName"
				+ " from LPEdorItem a left join LCPol c on a.PolNo = c.PolNo, LCInsured b, LMRisk d where a.InsuredNo = b.InsuredNo and a.ContNo = b.ContNo"
				+ " and a.EdorNo = '" + fm.EdorNo.value + "' and a.ContNo = '" + fm.ContNo.value + "' and a.EdorType = '" + tEdorType + "' and a.PolNo = '" + tPolNo + "'"
				+ " and c.RiskCode = d.RiskCode"; 
		arrResult = easyExecSql(strSql);     //执行SQL语句进行查询，返回二维数组
	}


	if (arrResult == null)
	{
		
		fm.all("showHealthQ1").disabled = true ;
		fm.all("RReportQuery1").disabled = true ;
		fm.all("showHealth1").disabled = true ;
		fm.all("showRReport1").disabled = true ;
		fm.all("showAdd1").disabled = true ;
		fm.all("showSpec1").disabled = true ;
		fm.all("SendNotice1").disabled = true ;	
		fm.all("submitForm1").disabled = true ;
		fm.all("cancelchk1").disabled = true ;
		
		alert("查询保全项目信息失败！");
		return;
	}*/
	
	//fm.EdorNo1.value = fm.EdorNo.value;
	//fm.ContNo1.value = fm.ContNo.value;
	//fm.EdorType.value = tEdorType;
	//fm.PolNo.value = tPolNo;
	//fm.InsuredNo.value = arrResult[0][1];
	//fm.InsuredName.value = arrResult[0][2];
	//fm.AppntNo.value = arrResult[0][9];
	//fm.AppntName.value = arrResult[0][10];
	//fm.RiskCode.value = arrResult[0][3];
	//fm.RiskName.value = arrResult[0][9];
	//fm.ChgPrem.value = arrResult[0][4];
	//fm.ChgAmnt.value = arrResult[0][5];
	//fm.GetMoney.value = arrResult[0][6];
	//fm.GetInterest.value = arrResult[0][7];
	//fm.ManageCom.value = arrResult[0][8];

	//fm.all("showHealthQ1").disabled = false ;
	//fm.all("RReportQuery1").disabled = false ;
	//fm.all("showHealth1").disabled = false ;
	//fm.all("showRReport1").disabled = false ;
	//fm.all("showAdd1").disabled = false ;
	//fm.all("showSpec1").disabled = false ;
	//fm.all("SendNotice1").disabled = false ;
	//fm.all("submitForm1").disabled = false ;
	//fm.all("cancelchk1").disabled = false ;
	//codeSql = "code";
	//fm.UWPopedomCode.value = "";
//	fm.action = "./PEdorManuUWCho.jsp";
//	fm.submit();
	
}

function checkBackPol(ContNo) {
  var strSql = "select * from LCApplyRecallPol where ContNo='" + ContNo + "' and InputState='0'";
  var arrResult = easyExecSql(strSql);
  
  if (arrResult != null) {
    return false;
  }
  return true;
}

//  初始化核保师是否已查看了相应的信息标记数组
function initFlag(  tlength )
{
	// 标记核保师是否已查看了相应的信息
	showPolDetailFlag =  new Array() ;
	showAppFlag = new Array() ;
	showHealthFlag = new Array() ;
	QuestQueryFlag = new Array() ;

	var i=0;
	for( i = 0; i < tlength ; i++ )
	{
		showPolDetailFlag[i] = 0;
		showAppFlag[i] = 0;
		showHealthFlag[i] = 0;
		QuestQueryFlag[i] = 0;
	} 
}

//下核保结论
function manuchk()
{
	
	flag = fm.all('UWState').value;
	var ContNo = fm.all('ContNo').value;
	var MainPolNo = fm.all('MainContNoHide').value;
	
	if (trim(fm.UWState.value) == "") {
    alert("必须先录入核保结论！");
    return;
  }
   
	if (!checkBackPol(ContNo)) {
	  if (!confirm("该投保单有撤单申请，继续下此结论吗？")) return;
	}
	
    if (trim(fm.UWState.value) == "6") {
      if(trim(fm.UWPopedomCode.value) !="")
         fm.UWOperater.value = fm.UWPopedomCode.value
      else 
         fm.UWOperater.value = operator;
}

    var tSelNo = EdorItemGrid.getSelNo()-1;
        
	if(fm.State.value == "1"&&(fm.UWState.value == "1"||fm.UWState.value == "2"||fm.UWState.value =="4"||fm.UWState.value =="6"||fm.UWState.value =="9"||fm.UWState.value =="a")) {
      if( showPolDetailFlag[tSelNo] == 0 || showAppFlag[tSelNo] == 0 || showHealthFlag[tSelNo] == 0 || QuestQueryFlag[tSelNo] == 0 ){
         var tInfo = "";
         if(showPolDetailFlag[tSelNo] == 0)
            tInfo = tInfo + " [投保单明细信息]";
         if(showAppFlag[tSelNo] == 0)   
            tInfo = tInfo + " [既往投保信息]";
         if( EdorItemGrid.getRowColData(tSelNo,1,EdorItemGrid) == EdorItemGrid.getRowColData(tSelNo ,2,EdorItemGrid)) {
         	if(showHealthFlag[tSelNo] == 0)
              tInfo = tInfo + " [体检资料录入]";
         }
         if(QuestQueryFlag[tSelNo] == 0)
            tInfo = tInfo + " [问题件查询]";
         if ( tInfo != "" ) {
         	tInfo = "有重要信息:" + tInfo + " 未查看,是否要下该核保结论?";
         	if(!window.confirm( tInfo ))
         	    return;
             }
             
        } 
     }
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.action = "./UWManuNormChk.jsp";
	fm.submit();

	if (flag != "b"&&ContNo == MainPolNo)
	{
		initInpBox();
   		initPolBox();
		initEdorMainGrid();
		initEdorItemGrid();
		fm.EdorNo.value = "";
		fm.ContNo.value = "";
	}
}

//问题件录入
function QuestInput()
{
	cContNo = fm.ContNo.value;  //保单号码
	
	var strSql = "select * from LCUWMaster where ContNo='" + cContNo + "' and ChangePolFlag ='1'";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null) {
      var tInfo = "承保计划变更未回复,确认要录入新的问题件?";
   if(!window.confirm( tInfo )){ 
          return;
        }
      }    
	window.open("./QuestInputMain.jsp?ContNo1="+cContNo+"&Flag="+cflag,"window1");

}

//问题件回复
function QuestReply()
{
	cContNo = fm.ContNo.value;  //保单号码
	
	//showModalDialog("./QuestInputMain.jsp?ContNo1="+cContNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./QuestReplyMain.jsp?ContNo1="+cContNo+"&Flag="+cflag,"window1");
	
	initInpBox();
    initPolBox();
	initEdorMainGrid();
	
}

//问题件查询
function QuestQuery()
{
  cContNo = fm.ContNo.value;  //保单号码

  
  if (cContNo != "")
  {	
  	var tSelNo = EdorItemGrid.getSelNo()-1;
  	QuestQueryFlag[tSelNo] = 1 ;	
	window.open("./QuestQueryMain.jsp?ContNo1="+cContNo+"&Flag="+cflag,"window2");
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }
	
}

//生存调查报告查询
function RReportQuery()
{
  cContNo = fm.ContNo.value;  //保单号码
  var cEdorNo = EdorItemGrid.getRowColData(EdorItemGrid.getSelNo()-1, 1);

  
  if (cContNo != "")
  {			
	window.open("./PEdorRReportQueryMain.jsp?ContNo1="+cContNo);
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

//保单撤销申请查询
function BackPolQuery()
{
  cContNo = fm.ContNo.value;  //保单号码

  
  if (cContNo != "")
  {			
	window.open("./BackPolQueryMain.jsp?ContNo1="+cContNo);
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

//催办超时查询
function OutTimeQuery()
{
  cContNo = fm.ContNo.value;  //保单号码

  
  if (cContNo != "")
  {			
	window.open("./OutTimeQueryMain.jsp?ContNo1="+cContNo);
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

//保险计划变更
function showChangePlan()
{
  cContNo = fm.ContNo.value;  //保单号码
  cPrtNo = fm.PrtNoHide.value; //印刷号
  var cType = "ChangePlan";
  mSwitch.deleteVar( "PolNo" );
  mSwitch.addVar( "PolNo", "", cContNo );
  
  if (cContNo != ""&&cPrtNo != "")
  {	
  	 var strSql = "select * from LCIssuepol where ContNo='" + cContNo + "' and (( backobjtype in('1','4') and replyresult is null) or ( backobjtype in('2','3') and needprint = 'Y' and replyresult is null))";
     var arrResult = easyExecSql(strSql);
     if (arrResult != null) {
       var tInfo = "有未回复的问题件,确认要进行承保计划变更?";
       if(!window.confirm( tInfo ))
         	    return;
      }
    window.open("../app/ProposalEasyScan.jsp?LoadFlag=3&Type="+cType+"&prtNo="+cPrtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");  	
      
   }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

//保险计划变更结论录入显示
function showChangeResultView()
{
	fm.ChangeIdea.value = "";
	fm.ContNoHide.value = fm.ContNo.value;  //保单号码
	divUWResult.style.display= "none";
	divChangeResult.style.display= "";	
}


//显示保险计划变更结论
function showChangeResult()
{
	fm.UWState.value = "b";
	fm.UWIdea.value = fm.ChangeIdea.value;
	
	cChangeResult = fm.UWIdea.value;
	
	if (cChangeResult == "")
	{
		alert("没有录入结论!");
	}
	else
	{
		var strSql = "select * from LCIssuepol where ContNo='" + cContNo + "' and (( backobjtype in('1','4') and replyresult is null) or ( backobjtype in('2','3') and needprint = 'Y' and replyresult is null))";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
       if (arrResult != null) {
       var tInfo = "有未回复的问题件,确认要进行承保计划变更?";
       if(!window.confirm( tInfo )){
       	      HideChangeResult()
         	    return;
          }
       }
	 
    }	
	divUWResult.style.display= "";
	divChangeResult.style.display= "none";
	fm.EdorUWState.value = "";
	fm.UWIdea.value = "";
	fm.UWPopedomCode.value = "";		
}

//隐藏保险计划变更结论
function HideChangeResult()
{
	divUWResult.style.display= "";
	divChangeResult.style.display= "none";
	fm.EdorUWState.value = "";
	fm.UWIdea.value = "";
	fm.UWPopedomCode.value = "";			
}


function cancelchk()
{
	fm.all('EdorUWState').value = "";
	fm.all('UWPopedomCode').value = "";
	fm.all('UWIdea').value = "";
}

function setContNo()
{
	var count = EdorMainGrid.getSelNo();
	fm.all('ContNo').value = EdorMainGrid.getRowColData(count - 1,1,EdorMainGrid);
}

//附件险按钮隐藏函数
function hideAddButton()
{
	parent.fraInterface.divAddButton1.style.display= "none";
	parent.fraInterface.divAddButton2.style.display= "none";
	parent.fraInterface.divAddButton3.style.display= "none";
	parent.fraInterface.divAddButton4.style.display= "none";
	parent.fraInterface.divAddButton5.style.display= "none";
	parent.fraInterface.divAddButton6.style.display= "none";
	parent.fraInterface.divAddButton7.style.display= "none";
	parent.fraInterface.divAddButton8.style.display= "none";
	parent.fraInterface.divAddButton9.style.display= "none";
	parent.fraInterface.divAddButton10.style.display= "none";
}

//显示隐藏按钮
function showAddButton()
{	
	parent.fraInterface.divAddButton1.style.display= "";
	parent.fraInterface.divAddButton2.style.display= "";
	parent.fraInterface.divAddButton3.style.display= "";
	parent.fraInterface.divAddButton4.style.display= "";
	parent.fraInterface.divAddButton5.style.display= "";
	parent.fraInterface.divAddButton6.style.display= "";
	parent.fraInterface.divAddButton7.style.display= "";
	parent.fraInterface.divAddButton8.style.display= "";
	parent.fraInterface.divAddButton9.style.display= "";
	parent.fraInterface.divAddButton10.style.display= "";
}

function showNotePad() {
  var cEdorNo = EdorItemGrid.getRowColData(EdorItemGrid.getSelNo()-1, 1);
  var PrtNo = EdorItemGrid.getRowColData(EdorItemGrid.getSelNo()-1, 3);
  
  if (cEdorNo!="" && PrtNo!="") {
  	window.open("../uw/UWNotePadMain.jsp?ContNo="+cEdorNo+"&PrtNo="+PrtNo+"&OperatePos=3", "window1");
  }
  else {
  	alert("请先选择保单!");
  }
}

function returnBack()
{
	easyQueryClick();
}