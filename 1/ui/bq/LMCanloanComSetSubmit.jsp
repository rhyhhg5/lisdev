<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-20
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>

<SCRIPT language="javascript">
var mLoadFlag = "<%=request.getParameter("LoadFlag")%>";
</SCRIPT>
<%
  //接收信息，并作校验处理。
  //输入参数
  //输出参数
  String FlagStr = "";
  String Content = "";
  GlobalInput tGI = new GlobalInput(); 
  tGI=(GlobalInput)session.getValue("GI");  
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  
 {
			String Operator  = tGI.Operator ;  //保存登陆管理员账号
			String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom内存中登陆区站代码

		  CErrors tError = null;
			String tBmCert = "";
			String delReason = "";
			String reasonCode = "";

			//后面要执行的动作：添加，修改，删除
		  String transact = request.getParameter("transact");
		  String aManageCom = request.getParameter("aManageCom");
	      String RiskCode = request.getParameter("RiskCode");
	      String MinPremLimit = request.getParameter("MinPremLimit");
	      String MaxPremLimit = request.getParameter("MaxPremLimit");
	      String CanLoanRate = request.getParameter("CanLoanRate");
	      
	      LMComLoanRateSchema tLMComLoanRateSchema = new LMComLoanRateSchema();
	      tLMComLoanRateSchema.setManageCom(aManageCom);
	      tLMComLoanRateSchema.setMaxPremLimit(MaxPremLimit);
	      tLMComLoanRateSchema.setMinPremLimit(MinPremLimit);
	      tLMComLoanRateSchema.setRiskCode(RiskCode);
	      tLMComLoanRateSchema.setCanLoanRate(CanLoanRate);
	      
	      LMComLoanRateSchema dLMComLoanRateSchema = new LMComLoanRateSchema();
	      dLMComLoanRateSchema.setManageCom(request.getParameter("DManageCom"));
	      dLMComLoanRateSchema.setRiskCode(request.getParameter("DRiskCode"));
	      dLMComLoanRateSchema.setMinPremLimit(request.getParameter("DMinPremLimit"));
	      dLMComLoanRateSchema.setMaxPremLimit(request.getParameter("DMaxPremLimit"));
	      dLMComLoanRateSchema.setCanLoanRate(request.getParameter("DCanLoanRate"));

			// 准备传输数据 VData	
			VData tVData = new VData();
			TransferData tTransferData = new TransferData();
			tVData.add(tGI);
			tTransferData.setNameAndValue("tLMComLoanRateSchema",tLMComLoanRateSchema);
			tTransferData.setNameAndValue("dLMComLoanRateSchema",dLMComLoanRateSchema);
			tVData.add(tTransferData);
	      
			LMCanloanComSetUI mLMCanloanComSetUI = new LMCanloanComSetUI();
     try
     {
    	
    	 mLMCanloanComSetUI.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
      Content = "设置失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
    tError = mLMCanloanComSetUI.getError();
    if (!tError.needDealError())
    {
      Content ="设置成功！";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = "设置失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
}
%>
<html>
	<script language="javascript">
	if (mLoadFlag == "TASKFRAME")
	{
		parent.fraInterface.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
	else
	{
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
</script>
</html>