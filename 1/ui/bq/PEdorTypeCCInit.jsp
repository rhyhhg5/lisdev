<%
//程序名称：PEdorInputInit.jsp
//程序功能：
//创建日期：2003-01-08 
//创建人  ：Dingzhong
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script language="JavaScript">  
	var cancelCount = 0;
function initInpBox()
{ 
  try
  {        
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    showOneCodeName("EdorCode", "EdorTypeName");  
  }
  catch(ex)
  {
    alert("在PEdorTypeCCInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    getContDetail();
    showAllCodeName();
    
    if(cancelCount == 0)
    {
      initElementtype();
      cancelCount = cancelCount + 1;
    }
  }
  catch(re)
  {
    alert("PEdorTypeCCInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>