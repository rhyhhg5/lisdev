<html> 
<%   
//程序名称：
//程序功能：交费频次变更
//创建日期：2005-12-30
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypePC.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypePCInit.jsp"%>
  <title>交费频次变更</title> 
</head>
<body onload="initForm();">
  <form action="./PEdorTypePCSubmit.jsp" method=post name=fm target="fraSubmit">    
   <table class=common>
   		<tr class= common> 
	      <td class="title"> 受理号 </td>
	      <td class=input>
	        <input class="readonly" type="text" readonly name=EdorNo >
	      </td>
	      <td class="title"> 批改类型 </td>
	      <td class="input">
	      	<input class="readonly" type="hidden" readonly name=EdorType>
	      	<input class="readonly" readonly name=EdorTypeName>
	      </td>
	      <td class="title"> 保单号 </td>
	      <td class="input">
	      	<input class = "readonly" readonly name=ContNo>
	      </td>   
    	</tr>
  	</table>
    <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
        </td>
        <td class= titleImg>险种信息</td>
      </tr>
    </table>
    <div id= "divLCPol" style= "display: ''">
      <table  class= common>
        <tr class= common>
          <td text-align: left colSpan=1>
            <span id="spanPolGrid" >
            </span> 
          </td>
        </tr>
      </table>	
      <div id= "divPage" align=center style= "display: 'none' ">
      <input value="首  页" class=cssButton type="button" onclick="getFirstPage();"> 
      <input value="上一页" class=cssButton type="button" onclick="getPreviousPage();"> 					
      <input value="下一页" class=cssButton type="button" onclick="getNextPage();"> 
      <input value="尾  页" class=cssButton type="button" onclick="getLastPage();"> 
      </div>				
    </div>  
    <br>
	 <Input class=cssButton type=Button value="保  存" onclick="save();">
	 <Input class=cssButton type=Button value="取  消" onclick="initForm();">
	 <Input class=cssButton type=Button value="返  回" onclick="returnParent();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
