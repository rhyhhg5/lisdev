<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UnderwriteApply.jsp
//程序功能：人工核保投保单申请校验
//创建日期：2003-04-09 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.workflow.bq.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
//输出参数
CErrors tError = null;
String FlagStr = "Fail";
String Content = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

if(tG == null)
{
	out.println("session has expired");
	return;
}

//校验处理
LCContSet tLCContSet = new LCContSet();
String tContNo = request.getParameter("ContNo");
String tEdorNo = request.getParameter("EdorNo");
String tMissionID = request.getParameter("MissionID");
String tSubMissionID = request.getParameter("SubMissionID");
boolean flag = false;

TransferData tTransferData = new TransferData();
if (!tContNo.equals(""))
{	
	LCContSchema tLCContSchema = new LCContSchema();
	tLCContSchema.setContNo( tContNo);	    
	tLCContSet.add( tLCContSchema );	  
	
	tTransferData.setNameAndValue("ContNo",tContNo);
	tTransferData.setNameAndValue("EdorNo",tEdorNo) ;
	tTransferData.setNameAndValue("MissionID",tMissionID);
	tTransferData.setNameAndValue("SubMissionID",tSubMissionID);

	flag = true;
}
else
{
	FlagStr = "Fail";
	Content = "申请失败，号码传输失败!";
}

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tLCContSet);
		tVData.add(tTransferData);
		tVData.add(tG);
		
		// 数据传输
//		PEdorUWManuApplyChkUI tPEdorUWManuApplyChkUI   = new PEdorUWManuApplyChkUI();
//		if (tPEdorUWManuApplyChkUI.submitData(tVData,"INSERT") == false)

		EdorWorkFlowUI tEdorWorkFlowUI   = new EdorWorkFlowUI();
		if (!tEdorWorkFlowUI.submitData(tVData,"0000000000"))//执行保全核保工作流节点0000000000
		{
			Content = " 保全人工核保申请失败，原因是: " + tEdorWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tEdorWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                     
		    	Content = " 保全人工核保申请申请成功!";
		    	FlagStr = "Succ";
		    }
		    else                                                              
		    {
		    	FlagStr = "Fail";
		    }
		}
	}
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>       
<html>
<script language="javascript">
	parent.fraInterface.afterApply("<%=FlagStr%>","<%=Content%>");	
</script>
</html>
