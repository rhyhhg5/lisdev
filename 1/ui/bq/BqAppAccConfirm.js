var showInfo;
var mDebug="1";

function afterSubmit(FlagStr, content	)
{
	try {showInfo.close();} catch(re) {}
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
		top.close();
	}
}

/*********************************************************************
 *  查询投保人帐户信息
 *  参数：contNo 合同号
 *********************************************************************
 */       
function getAppAccInfo(customerNo)
{
	var strSQL ="select customerno,accbala,accgetmoney " +
	            "from LCAppAcc "+	            
	            "where customerno = '" + customerNo + "' " +
	            "and state = '1'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("该客户号的超收保费余额帐户功能暂时不能用！");
		return;
	}
	
	fm.CustomerNo.value=arrResult[0][0];
	fm.AccBala.value=arrResult[0][1];
	fm.AccGetBala.value=arrResult[0][2];
	fm.CustomerName.value=getCustomerName();
}

function getCustomerName()
{
	var strSQL;
	if(fm.ContType.value=='0')//个人
	{
		var strSQL ="select name from ldperson where customerno='"+fm.CustomerNo.value+"'";
	}
	if(fm.ContType.value=='1')//团体
	{
		var strSQL ="select grpname from LDgrp where CustomerNo='"+fm.CustomerNo.value+"'";
	}
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询客户名称时出错！");
		return "";
	}
	return arrResult[0][0];
}

function clickOK()
{
	var selectValue='';
	for(i = 0; i <fm.chkYesNo.length; i++){
		if(fm.chkYesNo[i].checked){
			selectValue=fm.chkYesNo[i].value;			
			break;
		}
	}
	if(selectValue=='')
	{
		alert('请先选择是或者否！');
		return;
	}
	
	if(selectValue=='yes')
	{
		if(fm.all('accType').value=='0')
		{
			fm.all("fmtransact").value = "INSERT||TakeOut";	
		}
		if(fm.all('accType').value=='1')
		{
			fm.all("fmtransact").value = "INSERT||ShiftTo";	
		}
		fm.action = "./BqAppAccConfirmSubmit.jsp";
		fm.submit();
	}
	if(selectValue=='no')
	{
		top.close();
	}
}