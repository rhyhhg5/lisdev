<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    

<%
//程序名称：BqBalanceAdvanceInput.jsp
//程序功能：保单结算提前结算界面
//创建日期：2006-03-10
//创建人  ：Huxl
//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   

<script>
	var comCode = "<%=tG.ComCode%>";
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="BqBalanceAdvanceInput.js"></SCRIPT> 
<%@include file="BqBalanceAdvanceInit.jsp"%>

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>提前结算</title>
</head>
<body  onload= "iniForm();">  
   <form action="./BqBalanceAdvanceSave.jsp" method=post name=fm target="fraSubmit"> 
    <table class= common >
     <tr class= common >
      <td class= title >保单号</td>
      <td class= input>
        <Input class= common name=grpcontno >
      </td>
      <td class= title>
        结算截止日期
      </td>
      <td  class= input>
      	<Input class="coolDatePicker" name=EndDate verify="扫描时间|date">
      </td>
     </tr>
     <tr>
      <td>
        <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp; 
        <INPUT VALUE="日处理" class = cssButton TYPE=button onclick="balanceDailyBatch();">     
      </td>    
     </tr>
    </table>
		<table>
				<tr> 
					<td class= common> 
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divbalance);"> 
					</td>
					<td class= titleImg>提前结算保单信息</td>
				</tr>
		</table>
		<Div  id= "divbalance" style= "display: ''" >  
		  	<table class= common border=0 width=100% >		
          <TR  class= common>		
           <td> 
            <span id ="spanBalancePolGrid" >
            </span>	
           </td>					
					</TR>
		  </table>
	  </Div> 
	  <Div id= "divPage" align=center style= "display: 'none' ">
	  	<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();calBalmoney()"> 
	  	<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();calBalmoney()"> 					
	  	<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();calBalmoney()"> 
	  	<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();calBalmoney()"> 
	 	</Div>
	  	<INPUT VALUE="提前结算" class = cssButton TYPE=button onclick="balanceAdvance();">&nbsp;
	 </form> 
	 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 