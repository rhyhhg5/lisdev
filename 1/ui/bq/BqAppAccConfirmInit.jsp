<%
//程序名称：PEdorInputInit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	String contType=request.getParameter("ContType");
	String customerNo=request.getParameter("CustomerNo");
	String accType=request.getParameter("AccType");
	String edorAcceptNo=request.getParameter("EdorAcceptNo");
	String destSource=request.getParameter("DestSource");
%>                             

<script language="JavaScript">  

//把null的字符串转为空
function nullToEmpty(string)
{
	if ((string == "null") || (string == "undefined"))
	{
		string = "";
	}
	return string;
}

function initInpBox()
{ 	
	fm.all('ContType').value="<%= contType%>";
	fm.all('CustomerNo').value = "<%= customerNo%>";
	fm.all('accType').value = "<%= accType%>";
	fm.all('EdorAcceptNo').value ="<%= edorAcceptNo%>";
	fm.all('DestSource').value ="<%= destSource%>";
}


function initForm()
{ 
  initInpBox();
  getAppAccInfo(fm.all('CustomerNo').value);
  
}
</script>