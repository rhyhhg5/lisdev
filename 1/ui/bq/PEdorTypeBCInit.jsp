<%
/*
  受益人资料变更--初始化
  create : 0 孙  迪 2005-3-29 15:25
  modify : 1 
*/
%>
<script language="JavaScript">  

// 信息列表的初始化
function initForm()
{
  initInpBox();
  initLCBnfGrid();
  queryBnfInfo();
  try
  {  
    fm.all('PolNo').value = top.opener.fm.all('PolNo').value;
    fm.all('grpsignal').value = "Y";
    initQuery();
  }
  catch(ex)
  {
    fm.all('grpsignal').value = "N";
    initPolGrid();
    queryRisk();
  }
}

function initPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="险种序号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="80px";         		//列宽
    iArray[1][2]=100;          				//列最大值
    iArray[1][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="险种号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="80px";         		//列宽
    iArray[2][2]=100;          				//列最大值
    iArray[2][3]=3;            				//是否允许输入,1表示允许，0表示不允许
  
    iArray[3]=new Array();
    iArray[3][0]="险种编码";					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="80px";         		//列宽
    iArray[3][2]=100;          				//列最大值
    iArray[3][3]=0;            				//是否允许输入,1表示允许，0表示不允许
  
    iArray[4]=new Array();
    iArray[4][0]="险种名称";    			//列名
    iArray[4][1]="300px";            	//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    PolGrid = new MulLineEnter("fm", "PolGrid"); 
    PolGrid.mulLineCount = 0;
    PolGrid.displayTitle = 1;
    PolGrid.canSel = 1;
    PolGrid.hiddenSubtraction=1;
    PolGrid.hiddenPlus = 1;
    //PolGrid.selBoxEventFuncName = "reportDetailClick";
    PolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);        
  }
}
function initInpBox(){
    //判断是否是在工单查看总查看项目明细，若是则没有保存按钮
  var flag;
  try
  {
  	flag = top.opener.fm.all('loadFlagForItem').value;
  }
  catch(ex)
  {
  	flag = "";	
  }
  if(flag == "TASK")
  {
  	fm.saveButton.style.display = "none";
  	fm.returnButton.style.display = "none";
  }
  
  try 
  {//团单
    fm.all('PolNo').value = top.opener.fm.all('PolNo').value;
    fm.all('InsuredNo').value = top.opener.fm.all('CustomerNo').value;
	  divPol.style.display='none';
  }
  catch(ex){//个单
    fm.all('InsuredNo').value = top.opener.fm.all('CustomerNoBak').value;
  }
  try {
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
	  fm.all('ContNo').value = top.opener.fm.all('ContNo').value;	
	  fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
	  //制定汉化
    showOneCodeName("EdorCode", "EdorTypeName");  
	}
  catch(ex) {
    alert("在GEdorTypeBCInit.jsp-->initInpBox()函数中发生异常:初始化界面错误!");
  }
}

function initLCBnfGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;
    
    iArray[1]=new Array();
    iArray[1][0]="险种序号";			                                                           
    iArray[1][1]="80px";         		                                                      
    iArray[1][2]=100;          				
    iArray[1][3]=0;
    iArray[1][9]="险种序号|notnull";
    
    iArray[2]=new Array();
    iArray[2][0]="险种号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="80px";         		//列宽
    iArray[2][2]=100;          				//列最大值
    iArray[2][3]=3;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="受益人类别"; 	
    iArray[3][1]="50px";
    iArray[3][2]=40;		
    iArray[3][3]=2;		
    iArray[3][4]="BnfType";
    iArray[3][9]="受益人类别|notnull&code:BnfType";
    iArray[3][21]="bnfType";
    
    iArray[4]=new Array();
    iArray[4][0]="受益人姓名";
    iArray[4][1]="80px";	
    iArray[4][2]=40;		
    iArray[4][3]=1;			
    iArray[4][9]="受益人姓名|notnull&len<=20";
    iArray[4][21]="bnfName";
    
    iArray[5]=new Array();
    iArray[5][0]="证件类型"; 	 
    iArray[5][1]="80px";	    
    iArray[5][2]=80;		     
    iArray[5][3]=2;		        	
    iArray[5][4]="IDType";
    iArray[5][9]="证件类型|code:IDType";
    iArray[5][21]="IDType";
    
    iArray[6]=new Array();
    iArray[6][0]="证件号码";
    iArray[6][1]="100px";
    iArray[6][2]=80;		
    iArray[6][3]=1;			
    iArray[6][9]="证件号码|len<=20";
    iArray[6][21]="IDNo";
    
    iArray[7]=new Array();
    iArray[7][0]="与被保人关系"; 
    iArray[7][1]="50px";
    iArray[7][2]=80;		
    iArray[7][3]=2;			
    iArray[7][4]="Relation";
    iArray[7][9]="与被保人关系|notnull&code:Relation";
    iArray[7][21]="relation";
    
    iArray[8]=new Array();
    iArray[8][0]="受益顺序"; 		
    iArray[8][1]="80px";
    iArray[8][2]=80;		
    iArray[8][3]=2;
    iArray[8][4]="bnfgrade2";
    iArray[8][9]="受益顺序|code:bnfgrade2";
    iArray[8][21]="bnfGrade";
    
    iArray[9]=new Array();
    iArray[9][0]="受益份额"; 
    iArray[9][1]="80px";
    iArray[9][2]=80;		
    iArray[9][3]=1;			
    iArray[9][9]="受益份额|num&len<=10";
    iArray[9][21]="bnfLot";                                                            
    
    iArray[10]=new Array();
    iArray[10][0]="性别";
    iArray[10][1]="30px";
    iArray[10][2]=80;
    iArray[10][3]=2;
    iArray[10][4]="sex";
    iArray[10][9]="性别|notnull&code:sex";
    iArray[10][21]="sex";

    iArray[11]=new Array();
    iArray[11][0]="出生日期";
    iArray[11][1]="60px";
    iArray[11][2]=80;
    iArray[11][3]=1;
    iArray[11][9]="出生日期|notnull&date";
    iArray[11][21]="birthday";
    
    LCBnfGrid = new MulLineEnter("fm", "LCBnfGrid"); 
    LCBnfGrid.mulLineCount = 0;
    LCBnfGrid.displayTitle = 1;
    LCBnfGrid.hiddenSubtraction=0;
    LCBnfGrid.hiddenPlus = 0;
    LCBnfGrid.addEventFuncName = "initBnfGrid";
    LCBnfGrid.loadMulLine(iArray);        
  }
  catch(ex)
  {
    alert(ex);        
  }
}
</script>