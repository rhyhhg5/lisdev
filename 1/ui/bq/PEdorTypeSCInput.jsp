<html> 
<%
//程序名称：
//程序功能：个人保全
//创建日期：2004-7-19 11:03
//创建人  ：JiangLai
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeSC.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeSCInit.jsp"%>
  
  <title>特约约定内容变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeSCSubmit.jsp" method=post name=fm target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>
     
      <TD class = title > 保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=PolNo>
      </TD>   
    </TR>
  </TABLE> 

  <table>
    <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPAppntDetail);">
      </td>
      <td class= titleImg>
        特别约定内容
      </td>
    </tr>
  </table>
	 
      <Div  id= "divLPAppntDetail" style= "display: ''">
      <table>

    <TR  class= common>
      <TD  class= title>
      <textarea name="Speccontent" cols="120" rows="20" class="common" >
      </textarea></TD>
    </TR>
  </table>


      <table class = common>
			<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="撤消修改" onclick="edorTypeSCCancel()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="清空特约" onclick="edorTypeSCDelete()">
     	 </TD>
     	 </TR>
     	</table>
    </Div>
  
    <Div  id= "divSubmit" style= "display:''">
      <table class = common>
	<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeSCSave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="返回" onclick="returnParent()">
     	 </TD>
     	 </TR>
     </table> 
   </Div>
	
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>