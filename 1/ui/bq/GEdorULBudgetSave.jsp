<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<% 
//程序名称：GEdorBudgetSave.jsp
//程序功能：退保试算
//创建日期：2006-04-19
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String flag = "Succ";
  String content = "退保试算成功";
  String remark = "无";
  LCPolDB tLCPolDB = new LCPolDB();
  LCPolSet tLCPolSet = null;
  long startTime = System.currentTimeMillis();
  
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  
  String grpContNo = request.getParameter("grpContNo");
  String edorValiDate = request.getParameter("edorValiDate");
  String edorType = request.getParameter("edorType");
  String edorNo = request.getParameter("edorNo");
  
  boolean needResultFlag = (edorNo != null && !edorNo.equals("") 
                            && edorType != null && !edorType.equals(""));
  GEdorULBudgetBL tGEdorULBudgetBL = new GEdorULBudgetBL();
  tGEdorULBudgetBL.setOperator(tGI.Operator);
  
  TransferData tTransferData=new TransferData();	
  tTransferData.setNameAndValue("GrpContNo",grpContNo);
  tTransferData.setNameAndValue("EdorValiDate",edorValiDate);
  tTransferData.setNameAndValue("EdorType",edorType);
  tTransferData.setNameAndValue("EdorNo",edorNo);
  System.out.println("\n\n\n\n\n\n\n\n" + edorValiDate +"---" + grpContNo);
  
  VData mVData = new VData();
  mVData.add(tGI);
  mVData.add(tTransferData);
  tGEdorULBudgetBL.GULbudget(mVData);
  if(tGEdorULBudgetBL.mErrors.needDealError())
  {
  		flag="Fail";
  		content = "退保试算发生错误：\n"+tGEdorULBudgetBL.mErrors.getFirstError();
  }
  System.out.println(System.currentTimeMillis() - startTime);
  content = PubFun.changForHTML(content);
  
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>

