<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpFeeSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人 ：CrtHtml程序创建
//更新记录： 更新人  更新日期   更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//接收信息，并作校验处理。
//输入参数
LPAscriptionRuleFactorySchema tLPAscriptionRuleFactorySchema = new LPAscriptionRuleFactorySchema();
LPAscriptionRuleFactorySet tLPAscriptionRuleFactorySet = new LPAscriptionRuleFactorySet();

GrpEdorGBDetailUI tGrpEdorGBDetailUI = new GrpEdorGBDetailUI();

//输出参数
CErrors tError = null;
String tRearStr = "";
String tRela = "";
String FlagStr = "Fail";
String Content = "";

//全局变量
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

System.out.println("begin ...");
String edorNo = request.getParameter("EdorNo");
String edorType = request.getParameter("EdorType");
String tOperate=request.getParameter("mOperate");	//操作模式
String GrpContNo = request.getParameter("GrpContNo");	//集体合同号码
String AscriptionRuleCode = request.getParameter("AscriptionRuleCode");	//员工类别
String AscriptionRuleName = request.getParameter("AscriptionRuleName");	//分类说明
LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
tLPGrpEdorItemSchema.setEdorNo(edorNo);
tLPGrpEdorItemSchema.setGrpContNo(GrpContNo);
tLPGrpEdorItemSchema.setEdorType(edorType);
//取得险种管理费明细
int lineCount = 0;
String arrCount[] = request.getParameterValues("AscriptionRuleNewGridNo");
if (arrCount != null){
	String tRiskCode[] = request.getParameterValues("AscriptionRuleNewGrid1");	//险种编码
	String tFactoryType[] = request.getParameterValues("AscriptionRuleNewGrid2");	//要素类型
	String tOtherNo[] = request.getParameterValues("AscriptionRuleNewGrid3");	//目标类型
	String tFactory[] = request.getParameterValues("AscriptionRuleNewGrid4");	//要素计算编码
	String tCalRemark[] = request.getParameterValues("AscriptionRuleNewGrid5");	//要素内容
	String tParams[] = request.getParameterValues("AscriptionRuleNewGrid6");	//要素值
	String tFactoryName[] = request.getParameterValues("AscriptionRuleNewGrid7");	//计算编码名称
	String tGrpPolNo[] = request.getParameterValues("AscriptionRuleNewGrid9");	//集体保单险种号码
	String tFactoryCode = "";
	String tFactorySubCode = "";
	
	lineCount = arrCount.length; //行数
	
	for(int i=0;i<lineCount;i++){
		tLPAscriptionRuleFactorySchema = new LPAscriptionRuleFactorySchema();
	
        tLPAscriptionRuleFactorySchema.setEdorNo(edorNo);
        tLPAscriptionRuleFactorySchema.setEdorType(edorType);
		tLPAscriptionRuleFactorySchema.setGrpContNo(GrpContNo);
		tLPAscriptionRuleFactorySchema.setAscriptionRuleCode(AscriptionRuleCode);
		tLPAscriptionRuleFactorySchema.setAscriptionRuleName(AscriptionRuleName);
		tLPAscriptionRuleFactorySchema.setRiskCode(tRiskCode[i]);
		tLPAscriptionRuleFactorySchema.setFactoryType(tFactoryType[i]);
		tLPAscriptionRuleFactorySchema.setOtherNo(tOtherNo[i]);
		tFactoryCode = tFactory[i].substring(0,6);
		tFactorySubCode = tFactory[i].substring(6);
		System.out.println(tFactory[i]+"****"+tFactoryCode+"****"+tFactorySubCode);
		tLPAscriptionRuleFactorySchema.setFactoryCode(tFactoryCode);
		tLPAscriptionRuleFactorySchema.setFactorySubCode(tFactorySubCode);
		tLPAscriptionRuleFactorySchema.setCalRemark(tCalRemark[i]);
		tLPAscriptionRuleFactorySchema.setParams(tParams[i]);
		tLPAscriptionRuleFactorySchema.setFactoryName(tFactoryName[i]);
		tLPAscriptionRuleFactorySchema.setGrpPolNo(tGrpPolNo[i]);
	
		tLPAscriptionRuleFactorySet.add(tLPAscriptionRuleFactorySchema);
		System.out.println("记录"+i+"放入Set！");
	}
}
System.out.println("end ...");

// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tG);
tVData.addElement(tLPAscriptionRuleFactorySet);
tVData.addElement(GrpContNo);
tVData.add(tLPGrpEdorItemSchema);

try{
	System.out.println("this will save the data!!!");
	tGrpEdorGBDetailUI.submitData(tVData,tOperate);
}
catch(Exception ex){
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}

if (!FlagStr.equals("Fail")){
	tError = tGrpEdorGBDetailUI.mErrors;
	if (!tError.needDealError()){
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	else{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
parent.fraInterface.fm.all('AscriptionRuleCode').value="";
parent.fraInterface.fm.all('AscriptionRuleName').value="";
</script>
</html>