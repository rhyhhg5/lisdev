<%@page contentType="text/html;charset=GBK" %>
<html>
<%
 GlobalInput tGI1 = new GlobalInput();
     tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
     String ManageCom = tGI1.ManageCom;
   
	String strEdorNo = request.getParameter("EdorNo"); 
	if (strEdorNo == null)
	    strEdorNo = "";
	String viewFlag=request.getParameter("View");
	 
 %>    

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  
  <SCRIPT src="./PrtGrpEndorse.js"></SCRIPT>
  <SCRIPT src="PEdor.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PrtGrpEndorseInit.jsp"%>
    <%@include file = "ManageComLimit.jsp"%>

  <title>集体批改查询 </title>

</head>
<body  onload="initForm();" >

  <form action="./PEdorQueryOut.jsp" method=post name=fm target="fraSubmit" >
    <!-- 个人信息部分 -->
    <table>
    
    
    
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLPEdor1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            批单号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo value=<%=strEdorNo%>>
          </TD>

          <TD  class= title>
            集体合同号
          </TD>
          <TD  class= input>
            <Input class= common name=GrpContNo >
          </TD>
        </TR>
      </table>
    </Div>
    
    <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
    
    <br>
    <br>
          					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor2);">
    		</td>
    		<td class= titleImg>
    			 集体批改信息
    		</td>
    	</tr>
    	 
    </table>
   
  	<Div  id= "divLPEdor2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanPEdorMainGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
  	
  	<br>
  	
     <INPUT VALUE="打印批单" class= common TYPE=button onclick="submitForm();" name="btnPrintMain"> 
     <INPUT VALUE="打印变动清单" class= common TYPE=button onclick="prtChangeList();" name="btnPrintBill"> 

  		<input type=hidden id="EdorType" name="EdorType">
  	 <input type=hidden id="ContType" name="ContType">
  	 <input type=hidden id="Operate" name="Operate">
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <%
  if(viewFlag!=null&&!"".equals(viewFlag)){
  %>
  <script language="javascript">
	fm.btnPrintMain.value="查看批单";
	fm.btnPrintBill.value="查看变动清单";
</script>
  <%
  }
  %>
</body>
</html>
