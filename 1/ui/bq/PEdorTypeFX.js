//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var iArray ;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 

function edorTypeFXReturn()
{
	top.opener.getInsuredPolInfo(fm.all('ContNo').value);
	top.opener.getEdorItem();
	top.opener.focus();
	top.close();
}

//查询失效险种信息
function getPolInfo(tContNo)
{
    //alert(tContNo);
    //var tContNo;		  

//    var strSQL ="  select riskSeqNo,"
//                + "  (select riskName from LMRisk a where a.riskCode = LCPol.riskCode), "
//                + "  insuredName, amnt, mult, prem, PolState,cValiDate, "
//                + "  EndDate,PayToDate "
//                + "from LCPol "
//               + "where ContNo='" + tContNo + "' ";
    // 书写SQL语句 查询条件LCContState表StateType = 'Acaliable'，State = '1'
    var strSQL =" select b.riskSeqNo,"
               + "  (select riskName from LMRisk a where a.riskCode = b.riskCode), "
               + "  b.insuredName, b.amnt, b.mult, b.prem, codeName('stateflag', b.StateFlag),b.cValiDate, b.EndDate,b.PayToDate,b.polno,b.insuredno "
               + " from LCPol b "
               + " where b.contno ='" + tContNo + "' ";

    turnPage.pageLineNum = 15;
	turnPage.queryModal(strSQL, PolGrid);
    
    /*turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
    
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象

    turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	*/
    
//    for(var i = 0; i < PolGrid.mulLineCount; i++)
//    {
//    	if(PolGrid.getRowColData(i, 12) == "0")
//    	{
//    		PolGrid.setRowColData(i, 12, "");
//    	}
//    }
}

//选中已被选择的险种
function chkPol()
{
    var tContno=fm.all('ContNo').value;
    var tEdorNo=fm.all('EdorNo').value;
    var tEdorType=fm.all('EdorType').value;
    var strSQL="select polno from lppol where edorno='"+tEdorNo+"' and edortype='"+tEdorType+"' and contno='"+tContno+"'";        
    var arrResult2=easyExecSql(strSQL);
    var m=0;
  	var n=0;
  	
  	if(arrResult2!=null)
  	{
  		var q=arrResult2.length;
  		for(m=0;m<PolGrid.mulLineCount;m++)
	  	{
	  		for(n=0;n<q;n++)
	  	   {
	  			if(PolGrid.getRowColData(m,2)==arrResult2[n][0])
	  			{
	  				PolGrid.checkBoxSel(m+1);
	  			}
	  		}
	  	}				
  	}
}


function easyQueryClick(tContNo)
{
  var tContNo;		  
  // 书写SQL语句
  var strSQL = "";
  var strSQL ="select distinct a.CustomerNo,a.Name,a.Sex,a.Birthday,a.IdNo,b.contno,b.grpcontno from LDPerson a,LCCont b where b.ContNo='"+tContNo+"'"
                    +" and a.CustomerNo in (select insuredno from LCInsured where ContNo='"+tContNo+"')";
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = InsuredGrid;    
        //保存SQL语句
        turnPage.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
}


function edorTypeFXSave()
{
    //校验是否选择了险种
    if(!polChecked())
    {
      return false;
    }
    //校验是否存在附加万能险种
    if(!riskChecked())
    {
      return false;
    }
    
    if(!checkInput())
    {
      return false;
    }
    if(!checkULI231001()){
    	
    	return false;
    }
    
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}

function checkULI231001()
{
	
	var sql = "select polno from LCPol where RiskCode = '231001' and ContType = '1' and AppFlag = '1' and  ContNo='" + fm.ContNo.value + "' ";
	var result = easyExecSql(sql);
	
	if(result)
	{
		var rowCount = 0;
		  for (i = 0; i < PolGrid.mulLineCount; i++)
			{
				if (PolGrid.getChkNo(i)) 
				{
				  	rowCount = rowCount + 1;
				  	if(PolGrid.getRowColData(i,11)!=result[0][0])
				  	{
				  	  alert("保单下存在附加重疾险种，仅可对该险种做复效操作");
			          return false;
				  	}
				  	
				}
			}
			if(rowCount > 1)
			{
				alert("仅可对附加重疾险种做复效操作。");
				return false;
			}
		
			
		
	}
	return true;	
}

//校验是否选择了险种
function polChecked()
{
  var rowCount = 0;
  for (i = 0; i < PolGrid.mulLineCount; i++)
	{
		if (PolGrid.getChkNo(i)) 
		{
		  	rowCount = rowCount + 1;
		  	if(PolGrid.getRowColData(i,7)=="承保有效")
		  	{
		  	  alert("选择的险种包含承保有效险种，不能对有效的险种进行复效！");
	          return false;
		  	}
		}
	}
	if(rowCount == 0)
	{
    alert("请选择需要复效的险种");
	  return false;
	}
	
  return true;
}
//含有附加万能险种的保单，复效时失效的附加万能险种须与主线一同复效
function riskChecked(){
	  var rowCount = 0;
	  var rowCount2 = 0;
	  //附加万能险种失效
	  var sql = " select polno from LCPol where ContNo='" + fm.ContNo.value + "'  " 
	  		   +" and RiskCode in (select riskcode from lmriskapp where risktype4='4' and subriskflag='S') and stateflag='2' with ur  ";
	  var result = easyExecSql(sql);
	  if(result)
	  {
		 //主险失效
		 var sql2 = " select polno from LCPol where ContNo='" + fm.ContNo.value + "'  " 
	           	   +" and RiskCode not in (select riskcode from lmriskapp where risktype4='4' and subriskflag='S') and stateflag='2' and polno=mainpolno with ur  ";
		 var result2 = easyExecSql(sql2);
		 for (i = 0; i < PolGrid.mulLineCount; i++)
		 {
			 if (PolGrid.getChkNo(i) && PolGrid.getRowColData(i,11)==result[0][0]) 
			 {
				 rowCount = rowCount + 1;
			 }
			 if (result2 && PolGrid.getChkNo(i) && PolGrid.getRowColData(i,11)==result2[0][0]) 
			 {
				 rowCount2 = rowCount2 + 1;
			 }
		 }
		 if((rowCount == 0 && rowCount2 == 1) || (rowCount == 1 && rowCount2 == 0))
		 {
			 alert("保单下存在附加万能险种，须将附加万能险种与主险一同做复效操作");
			 return false;
		 }
		 
	  }
	  return true;
}

function checkInput()
{
  if(fm.rates.value == "")
  {
    alert("请您填入利率！");
    return false;
  }
  if(fm.EdorAppDate.value == "")
  {
    alert("请您填入时间！");
    return false;
  }
  
  return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  top.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   // alert(FlagStr);
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
    
    edorTypeFXReturn();
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function healthInput()
{
		if(!selInsured())
		{
			alert("请选择险种后再做操作！")
			return false;
		}
		if(!chkInsured())
		{
			alert("一次只能选择一个被保人！")
			return false;
		}
		var sql ="select 1 from LPCustomerImpart where EdorType ='FX' and EdorNo ='"+fm.all('EdorNo').value+"' and CustomerNoType = '1' and CustomerNo ='"+iArray[0]+"'";
		var result = easyExecSql(sql) ;
		if(result!=null)
		{
				alert("您好，该被保人在此次保全中已经录入过健康告知，不需再录入！")
				return false;
		}
		else
		{
				var	win = window.open("PEdorHealthImpartMain.jsp?EdorNo="+fm.all('EdorNo').value+"&EdorType=" + fm.all('EdorType').value + "&ContNo=" + fm.all('ContNo').value + "&InsuredNo=" +iArray[0]+"&Flag=1",
						  "",
						  "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0");
				win.focus();
		}


}
//取得选择的被保人号
function selInsured()
{
	count=0;
	iArray = new Array();
	//取出选中的保单中的被保人号
	for(i =0; i<PolGrid.mulLineCount; i++)
  {
  	
  	if(PolGrid.getChkNo(i))
  	{
  		iArray[count]= PolGrid.getRowColData(i,12);
  		count++;
  		
  	}
  }
  if(count == 0)
  	return false;
  	else
  		return true;
}
//一次只能有一个被保人录入
function chkInsured()
{
	var temp =iArray[0];
	for(i=1;i<iArray.length;i++)
	{
		if(temp != iArray[i])
		return false;
	}
	return true;
}
//钩选已录入的复效险种
function checkSelectPol()
{
	PolGrid.checkBoxAllNot(this, PolGrid.colCount);
	
	var sql = "  select polno "
	          + "from LPpol "
	          + "where edorNo = '" + fm.EdorNo.value + "' "
	          + "   and edorType = '" + fm.EdorType.value + "' "
	          + "   and ContNo = '" + fm.ContNo.value + "' ";
	
	
	var sqlURA = "select 1 from LCPol where RiskCode = '231001' and ContType = '1' and AppFlag = '1' and  ContNo='" + fm.ContNo.value + "' ";
	var resultURA = easyExecSql(sqlURA);
	if(resultURA)
	{
		sql = "  select polno "
	          + "from LPpol "
	          + "where edorNo = '" + fm.EdorNo.value + "' "
	          + "   and edorType = '" + fm.EdorType.value + "' "
	          + "   and ContNo = '" + fm.ContNo.value + "' AND RISKCODE='231001' WITH UR";			
		
	}
	var result = easyExecSql(sql);
	if(result)
	{
  	for(var i = 0; i < PolGrid.mulLineCount; i++)
  	{
  	  for(var t = 0; t < result.length; t++)
  	  {
    		if(PolGrid.getRowColDataByName(i, "polno") == result[t])
    		{
    			PolGrid.checkBoxSel(i + 1);
    		}
    	}
  	}
  }
}


function showTips(){
	var content = " 保单当前涉及多次续期未缴费，本次复效会收取当前所欠缴的最早一期的续期费用，保全确认后会自动对保单进行后续续期抽档，请及时对保单进行续期收费。";
	//比较选择的日期与保单交至日相差的年数
	var fxdate=fm.EdorAppDate.value;
	  var t1 = new Date(fxdate.replace(/-/g,"\/")).getTime();
	  var sqlURA = "select paytodate from LCPol where RiskCode in( '231001','231201') and ContType = '1' and AppFlag = '1' and  ContNo='" + fm.ContNo.value + "' ";
	  var result = easyExecSql(sqlURA);
	  if(result){
		  var t2 = new Date(result[0][0].replace(/-/g,"\/")).getTime();
		  var tfixdate_0624 = (t1-t2)/(24*60*60*1000);  
		  if(tfixdate_0624>=365){
			  fm.all("ErrorsInfo").innerHTML = content;
		  }else{
			  fm.all("ErrorsInfo").innerHTML = "";
		  }
	  }else{
		  fm.all("ErrorsInfo").innerHTML = "";
	  }
}

function initInpBox()
{
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
  	
  	if(flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  	}

  try
  {        
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAppDate').value = top.opener.fm.all('EdorValiDate').value;
    
  	
  	var sql = "select EdorValue "
  	          + "from LPEdorEspecialData "
  	          + "where EdorNo = '" + fm.EdorNo.value + "' "
  	          + "   and EdorType = '" + fm.EdorType.value + "' "
  	          + "   and DetailType = 'FX_R' ";
  	var rs = easyExecSql(sql);
  	if(rs)
  	{
  	  fm.rates.value = rs[0][0];
  	}
  	else
  	{
  	  sql = "select Rate "
  	        + "from LDBankRate "
  	        + "where StartDate <= Current Date "
  	        + "   and EndDate > Current Date "
  	        + "   and RateIntv = '2' "  //年利率
  	        + "   and RateType = 'S' "
  	        + "   and RateIntvFlag = 'Y' "
  	        ;
  	  var rs = easyExecSql(sql);
  	  //document.all("test").innerHTML = sql;
    	if(rs)
    	{
    	  fm.rates.value = rs[0][0];
    	}
  	}
    
    //制定汉化
    showOneCodeName("EdorCode", "EdorTypeName"); 
    
  }
  catch(ex)
  {
    alert("初始化界面错误：" + ex.message);
  }
}