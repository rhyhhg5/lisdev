<%
//程序名称：ReportManuInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	DivGetAcc.style.left=ex;
  	DivGetAcc.style.top =ey;
   	AccQueryClick();
}
//单击时查询
function reportDetailClick1(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	DivGetAcc.style.left=ex;
  	DivGetAcc.style.top =ey;
   
   	ManageAccQueryClicked();
    //DivGetAccManage.style.display ='';
}
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {         
	fm.all('ContType').value='1';
	fm.all('EdorNo').value= top.opener.fm.all('EdorNo').value;
	fm.all('ContNo').value= top.opener.fm.all('ContNoBAK').value;
	fm.all('InsuredNo').value = top.opener.fm.all('CustomerNoBAK').value;
	fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    //fm.all("RiskCode").CodeData=getRiskByInsuredNo();	
  }
  catch(ex)
  {
    alert("在PEdorTypeGAInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }    
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("PEdorTypeGAInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
 		
    initInpBox();
    initSelBox();
    initDiv();
   
    //initGetDutyKind();
   initLPAccMoveGrid();
    initLPGetGrid();
    initAccGrid();
    queryClick();
    queryByLingqu();
 //   initQuery(); 
    // initLCInsureAccClassGrid();
	//	ctrlGetEndorse();
  }
  catch(re)
  {
    alert("PEdorTypeGAInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initDiv()
{
	//DivGetAccManage.style.display ='';
	DivGetAcc.style.display ='none';
  DivLCInsureAcc.style.display='';
}

function initAccGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="产品代码";
    iArray[1][1]="100px";
    iArray[1][2]=60;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="产品名称";
    iArray[2][1]="160px";
    iArray[2][2]=60;
    iArray[2][3]=0;


    iArray[3]=new Array();
    iArray[3][0]="账户名称";
    iArray[3][1]="90px";
    iArray[3][2]=200;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="账户金额";
    iArray[4][1]="70px";
    iArray[4][2]=200;
    iArray[4][3]=0;	
    
    iArray[5]=new Array();
    iArray[5][0]="结息日期";
    iArray[5][1]="70px";
    iArray[5][2]=200;
    iArray[5][3]=0;	
    
    iArray[6]=new Array();
    iArray[6][0]="被保人姓名";
    iArray[6][1]="60px";
    iArray[6][2]=200;
    iArray[6][3]=0;	
    
    iArray[7]=new Array();
    iArray[7][0]="合同号";
    iArray[7][1]="60px";
    iArray[7][2]=200;
    iArray[7][3]=3;	
    iArray[8]=new Array();
    iArray[8][0]="交费计划名称";
    iArray[8][1]="60px";
    iArray[8][2]=200;
    iArray[8][3]=3;	

    AccGrid = new MulLineEnter( "fm" , "AccGrid" );
    //这些属性必须在loadMulLine前
    AccGrid.mulLineCount = 1;
    AccGrid.displayTitle = 1;
    AccGrid.hiddenSubtraction = 1;
    AccGrid.hiddenPlus = 1;
    AccGrid.canSel = 0;
    AccGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
function initGetDutyKind()
{
try
{
	var tRiskCode = fm.all('RiskCode').value;
	var tSql = "select GetDutyKind,GetDutyName,GetIntv,AddValue from LMDutyGetAlive where Getdutycode in ( select distinct getdutycode from  LMDutyGetRela where dutycode in (select distinct dutycode from LMRiskDuty where RiskCode ='"+tRiskCode+"'))";
	tReturn = easyQueryVer3(tSql,1,0,1);
}
catch(e)
{
	alert(e);
}
}
// 信息列表的初始化
function initLPGetGrid()
{
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="给付责任编码";    	//列名4
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="责任编码";         			//列名3
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

       iArray[3]=new Array();
      iArray[3][0]="给付责任类型";         			//列名6
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]= 2;              			//是否允许输入,1表示允许，0表示不允许
     

	  iArray[3][10]="GetDutyKind";
			
	  iArray[3][11]= tReturn;
	  iArray[3][12]="3|4|5";     //引用代码对应第几列，'|'为分割符
      iArray[3][13]="0|2|3";    //上面的列中放置引用代码中第几位值

	  iArray[3][15]="GetDutyCode";  //引用代码："CodeName"为传入数据的名称
      iArray[3][17]="2"; //传入要下拉显示的代码 

      iArray[4]=new Array();
      iArray[4][0]="领取间隔";         			//列名9
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[5]=new Array();
      iArray[5][0]="递增率";         		//列名10
      iArray[5][1]="100px";            	//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[6]=new Array();
      iArray[6][0]="起领日期";         		//列名14
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[7]=new Array();
      iArray[7][0]="止领日期";         		//列名15
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="领取金额";         		//列名12
      iArray[8][1]="120px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="险种保单号";         		//列名12
      iArray[9][1]="120px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      LPGetGrid = new MulLineEnter( "fm" , "LPGetGrid" ); 
      //这些属性必须在loadMulLine前
      //LPGetGrid.mulLineCount = 10;   
      LPGetGrid.displayTitle = 1;
      LPGetGrid.canSel=1;
      LPGetGrid.selBoxEventFuncName ="getInsureAccClass";
      //LPGetGrid.canChk=1;
      LPGetGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 信息列表的初始化
function initLPAccMoveGrid()
{
      var iArray = new Array();
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";         			//列宽
        iArray[0][2]=10;          			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
        iArray[1]=new Array();
        iArray[1][0]="批单号";
        iArray[1][1]="0px";
        iArray[1][2]=100;
        iArray[1][3]=3; 
        
        iArray[2]=new Array();
        iArray[2][0]="批改类型";
        iArray[2][1]="0px";
        iArray[2][2]=100;
        iArray[2][3]=3;
        
        iArray[3]=new Array();
        iArray[3][0]="保单险种号码";
        iArray[3][1]="0px";
        iArray[3][2]=100;
        iArray[3][3]=3;
        
        iArray[4]=new Array();
        iArray[4][0]="保险帐户号码";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=3;
        
        iArray[5]=new Array();
        iArray[5][0]="账户分类名称";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=2;
        
        iArray[6]=new Array();
        iArray[6][0]="金额";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="账户类型";
        iArray[7][1]="100px";
        iArray[7][2]=100;
        iArray[7][3]=3;
        
        iArray[8]=new Array();
        iArray[8][0]="对应其它号码"
        iArray[8][1]="0px";
        iArray[8][2]=100;
        iArray[8][3]=3
         
        iArray[9]=new Array();
        iArray[9][0]="对应其它号码类型";
        iArray[9][1]="0px";
        iArray[9][2]=100;
        iArray[9][3]=3;
        
        iArray[10]=new Array();
        iArray[10][0]="账户归属属性";
        iArray[10][1]="0px";
        iArray[10][2]=100;
        iArray[10][3]=3;
        
        iArray[11]=new Array();
        iArray[11][0]="帐户转移类型";
        iArray[11][1]="0px";
        iArray[11][2]=100;
        iArray[11][3]=3;
        
        iArray[12]=new Array();
        iArray[12][0]="给付项编码";
        iArray[12][1]="100px";
        iArray[12][2]=100;
        iArray[12][3]=3;
        
        iArray[13]=new Array();
        iArray[13][0]="转换金额";
        iArray[13][1]="50px";
        iArray[13][2]=100;
        iArray[13][3]=3;
        
        iArray[14]=new Array();
        iArray[14][0]="变动保险帐户单位数";
        iArray[14][1]="0px";
        iArray[14][2]=100;
        iArray[14][3]=3;
        
        iArray[15]=new Array();
        iArray[15][0]="转换比例";
        iArray[15][1]="50px";
        iArray[15][2]=100;
        iArray[15][3]=3;
        
        iArray[16]=new Array();
        iArray[16][0]="转换类型";
        iArray[16][1]="50px";
        iArray[16][2]=100;
        iArray[16][3]=3;
        

      LPAccMoveGrid = new MulLineEnter( "fm" , "LPAccMoveGrid" ); 
      //这些属性必须在loadMulLine前
      //LPGetGrid.mulLineCount = 10;   
      LPAccMoveGrid.displayTitle = 1;
      LPAccMoveGrid.canSel=1;
      //LPGetGrid.canChk=1;
      LPAccMoveGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 信息列表的初始化
function initLCInsureAccClassGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="帐户名称";    	//列名1
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="帐户号码";         			//列名4
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="帐户分类名称";         			//列名0
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
	
	  iArray[4]=new Array();
      iArray[4][0]="交费计划编码";         			//列名0
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=3;              	

      iArray[5]=new Array();
      iArray[5][0]="帐户类型";         			//列名11
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="保单险种号码";         			//列名11
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=60;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[7]=new Array();
      iArray[7][0]="结息日期";         		//列名16
      iArray[7][1]="100px";            	//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[8]=new Array();
      iArray[8][0]="帐户余额";         		//列名8
      iArray[8][1]="150px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[9]=new Array();
      iArray[9][0]="帐户可领余额";         		//列名8
      iArray[9][1]="150px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="其他号码";         		//列名8
      iArray[10][1]="150px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="其他号码类型";         		//列名8
      iArray[11][1]="150px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="账户归属属性";         		//列名8
      iArray[12][1]="150px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许
          
    
      LCInsureAccClassGrid = new MulLineEnter( "fm" , "LCInsureAccClassGrid" ); 
      //这些属性必须在loadMulLine前
      //LCInsureAccClassGrid.mulLineCount = 10;   
      LCInsureAccClassGrid.displayTitle = 1;
      //LCInsureAccClassGrid.canSel=1;
      LCInsureAccClassGrid.selBoxEventFuncName ="reportDetailClick1";
      LCInsureAccClassGrid.canChk=1;
      LCInsureAccClassGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      
      }
      catch(ex)
      {
        alert(ex);
      }
}
// 查询按钮
function AccQueryClick()
{
	 var i = 0;
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
	fm.all('fmtransact').value = "QUERY||ACCOUNT";
	//showSubmitFrame(mDebug);
	fm.submit();
}
// 查询按钮
function ManageAccQueryClicked()
{
	 var i = 0;
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
	fm.all('fmtransact').value = "QUERY||MANAGE";
	//showSubmitFrame(mDebug);
	fm.submit();
}
function initQuery()
{	
	// 书写SQL语句
	 var i = 0;
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.all('fmtransact').value = "QUERY||MAIN";
		//alert("----begin---");
		//showSubmitFrame(mDebug);
		fm.submit();	  	 	 
}
</script>