//程序名称：PEdorTypeMO.js
//程序功能：该文件中包含客户端需要处理的函数和事件
//创建日期：2003-01-08 
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容          

var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeIOSave() {
  if (!verifyInput()) {
    return false; 
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  fm.all('fmtransact').value = "INSERT";
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result, lpMoveResult) { 
  try { showInfo.close(); } catch (e) {}
  
  var tTransact = fm.all('fmtransact').value;
	if (FlagStr=="Success" && tTransact=="QUERY")	{
		var tArr = decodeEasyQueryResult(Result, 0);
		var lpMoveArr = decodeEasyQueryResult(lpMoveResult, 0);
		
		fm.all('RiskCode').value = tArr[0][11];
		fm.all('AppntNo').value = tArr[0][28];
		fm.all('AppntName').value = tArr[0][29];
		fm.all('InsuredNo').value = tArr[0][20];
		fm.all('InsuredName').value = tArr[0][21];
		fm.all('CValidate').value = tArr[0][31];
		fm.all('PayToDate').value = tArr[0][36];
		fm.all('LastGetDate').value = tArr[0][53];
		fm.all('GetStartDate').value = tArr[0][37];
		fm.all('Prem').value = tArr[0][42];
		fm.all('Amnt').value = tArr[0][45];
		fm.all('SumPrem').value = tArr[0][43];
		fm.all('LeavingMoney').value = tArr[0][44];
		
		//显示原管理机构
		fm.all('ManageCom').value = tArr[0][14];
		
		var strSql = "select name from ldcom where comcode='" + fm.all('ManageCom').value + "'";
		var arrResult = easyExecSql(strSql);
		fm.all('ManageName').value = arrResult;

    //显示迁入管理机构
    if (lpMoveArr != "") {
      fm.all("ManageCom2").value = lpMoveArr[0][11];
      fm.all("ManageName2").value = lpMoveArr[0][13];
    }
	}
	else {  
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
}

function returnParent() {
	top.close();
}


