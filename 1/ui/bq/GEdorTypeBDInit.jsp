<%
//GEdorTypeBCInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">  
//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y

   	detailQueryClick();
}
function initInpBox()
{ 

  try
  {      
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('GrpPolNo').value = top.opener.fm.all('GrpPolNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    
    showOneCodeName("EdorCode", "EdorTypeName");
  }
  catch(ex)
  {
    alert("在GEdorTypeBCInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在GEdorTypeBCInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initDiv();
    initLCPolGrid();
    initQuery();  
  }
  catch(re)
  {
    alert("GEdorTypeBDInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//保单信息列表
function initLCPolGrid()
{
    var iArray = new Array();
    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）             
      iArray[0][1]="30px";         			//列宽                                                     
      iArray[0][2]=10;          			//列最大值                                                 
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[1]=new Array();                                                                                       
      iArray[1][0]="保单号";    			//列名                                                     
      iArray[1][1]="150px";            			//列宽                                                     
      iArray[1][2]=100;            			//列最大值                                                 
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[2]=new Array();                                                                                       
      iArray[2][0]="险种编码";         		//列名                                                     
      iArray[2][1]="150px";            			//列宽                                                     
      iArray[2][2]=100;            			//列最大值                                                 
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                          
      iArray[3]=new Array();                                                                                       
      iArray[3][0]="起保日期";         		//列名                                                     
      iArray[3][1]="100px";            			//列宽                                                     
      iArray[3][2]=100;            			//列最大值                                                 
      iArray[3][3]=0;             			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[4]=new Array();                                                                                       
      iArray[4][0]="被保人客户号";         		//列名                                                     
      iArray[4][1]="150px";            			//列宽                                                     
      iArray[4][2]=100;            			//列最大值                                                 
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用        
                                                                                                                   
      iArray[5]=new Array();                                                                                       
      iArray[5][0]="被保人姓名";         		//列名                                                     
      iArray[5][1]="100px";            			//列宽                                                     
      iArray[5][2]=100;            			//列最大值                                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用        
                                                                                                                   
      iArray[6]=new Array();                                                                                       
      iArray[6][0]="投保人姓名";         			//列名                                                     
      iArray[6][1]="100px";            			//列宽                                                     
      iArray[6][2]=100;            			//列最大值                                                 
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用        

      LCPolGrid = new MulLineEnter( "fm" , "LCPolGrid" ); 
      //这些属性必须在loadMulLine前
      LCPolGrid.mulLineCount = 10;   
      LCPolGrid.displayTitle = 1;
      LCPolGrid.canSel=1;
 			LCPolGrid.selBoxEventFuncName ="reportDetailClick";      //这些操作必须在loadMulLine后面
      LCPolGrid.loadMulLine(iArray);  
      LCPolGrid.detailInfo="单击显示详细信息";
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initQuery()
{	
	 var i = 0;
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.all('fmtransact').value = "QUERY||MAIN";
		mFlag = '0';
		//alert("----begin---");
		//showSubmitFrame(mDebug);
	
		fm.submit();	  	 	 
}

function CondQueryClick()
{
		var i = 0;
	 	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.all('fmtransact').value = "QUERY||MAIN";
		mFlag = '1';
		var tPolNo = fm.all('PolNo').value;
		if (tPolNo==null&&tPolNo=='')
			mFlag ='0';
		//alert("----begin---");
		//showSubmitFrame(mDebug);
		fm.submit();
}

function detailQueryClick()
{
 // alert("detailQUeryClikc");
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
	fm.all('fmtransact').value = "QUERY||BonusGetMode";

	var tSel=LCPolGrid.getSelNo();
	
	if (tSel==0||tSel==null)
		alert("不能是空记录!");
	else
	{
	    divLCPol.style.display ='';

		var tPolNo =LCPolGrid.getRowColData(tSel-1,1);
		fm.all("PolNo").value = tPolNo;
	//	alert(tPolNo);
        var strSql = "";
        
        strSql = "select RiskCode,InsuredNo,InsuredName,CValidate,PayToDate,BonusGetMode,Prem,Amnt from lcpol where 1 =1 and polno = " + tPolNo;	 
	//	alert(strSql);
	    turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
	  
	  //判断是否查询成功
	  if (turnPage.strQueryResult) {
	 		//查询成功则拆分字符串，返回二维数组
	 	 turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
      }	      
      
   //   alert(turnPage.arrDataCacheSet[0][0]);
    //  alert(turnPage.arrDataCacheSet[0][1]);
   //   alert(turnPage.arrDataCacheSet[0][2]);
    //  alert(turnPage.arrDataCacheSet[0][3]);
   //   alert(turnPage.arrDataCacheSet[0][4]);
      fm.all("RiskCode").value = turnPage.arrDataCacheSet[0][0];
      fm.all("InsuredNo").value = turnPage.arrDataCacheSet[0][1];
      fm.all("InsuredName").value = turnPage.arrDataCacheSet[0][2];
      fm.all("CValidate").value = turnPage.arrDataCacheSet[0][3];
      fm.all("PayToDate").value = turnPage.arrDataCacheSet[0][4];
      fm.all("LCBonusGetMode").value = turnPage.arrDataCacheSet[0][5];
      fm.all("Prem").value = turnPage.arrDataCacheSet[0][6];
      fm.all("Amnt").value = turnPage.arrDataCacheSet[0][7];		
      
	  fm.submit();
	}
}
function initDiv()
{
 //	

}

</script>