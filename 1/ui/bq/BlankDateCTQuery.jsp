<%
//程序名称：PAchieveRate.jsp
//程序功能：个险续期达成率统计
//创建日期：2010-10-13 
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<html> 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="BlankDateCTQuery.js"></SCRIPT>
  <%@include file="BlankDateCTQueryInit.jsp"%>
<script>
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	 var tsql =" 1  and char(length(trim(comcode))) in (#4#,#8#) and comcode like #"+manageCom+"%#";
</script> 
</head>
<body  onload="initForm();initElementtype();" >
	<form method=post name=fm action="./BlankDateCTQuerySave.jsp" >
		<Table  class= common>
	    	<tr>
	    		<td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" 
					ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,1);" 
					onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,1);" >
					<Input class=codename  name=ManageComName elementtype=nacessary readonly>
				</td>
				
				<td class= title>保全结案起期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=DueStartDate elementtype=nacessary verify="应缴开始日期|NOTNULL">
				</td>
				
				
			</tr>

	    	<tr>
				
				<td class= title>保单类型</td>
				<td class=input>
					<Input class= "codeno" name=SaleChnlType verify="保单类型|NOTNULL" CodeData="0|^1|犹豫期撤保^2|部分领取^3|协议退保^4|解约^5|全部" ondblclick="return showCodeListEx('SaleChnlType',[this,SaleChnlTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnlType',[this,SaleChnlTypeName],[0,1]);" value = "1"><Input class=codename name=SaleChnlTypeName elementtype=nacessary readonly  >
				</td>
				
				<td class= title>保全结案止期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=DueEndDate elementtype=nacessary verify="应缴开始日期|NOTNULL">
				</td>
			</tr>
			
	
		</Table>
		<Table class= common>
		   <tr>
			<td>
				<input value="清单下载" class = cssButton TYPE=button onclick="easyPrint();">
			</td>
			</tr>
			</br>
		</Table>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
