<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeAD.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeADInit.jsp"%>
  
  <title>投保人重要资料变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeADSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
     
      <TD class = title > 集体保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpContNo>
      </TD>   
    </TR>
  </TABLE> 
  
 	<Div  id= "divLPAppntGrpDetail" style= "display: ''">
  <table>
    	<tr>
    		<td class= titleImg>
    			 投保单位资料
    		</td>
    	</tr>
    </table>
    
    <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
       
        
        
          <TR>
          <TD  class= title8>
            单位地址编码
          </TD>
          <TD  class= input8>
             <Input class="code" name="GrpAddressNo"  onchange="setAddr()" ondblclick="getaddresscodedata();return showCodeListEx('GetGrpAddressNo',[this],[0],'', '', '', true);" onkeyup="getaddresscodedata();return showCodeListKeyEx('GetGrpAddressNo',[this],[0],'', '', '', true);">
          </TD>								
          <TD  class= title8>
            单位地址
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpAddress readonly=false elementtype=nacessary  verify="单位地址|notnull&len<=60">
          </TD>
          <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpZipCode  readonly=false elementtype=nacessary  verify="邮政编码|notnull&zipcode">
          </TD>       
        </TR>
        <TR  class= common>
          <TD  class= title8 >
            保险联系人一
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan1 readonly=false elementtype=nacessary verify="保险联系人一姓名|notnull&len<=10">
          </TD>
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone1 readonly=false elementtype=nacessary verify="保险联系人一联系电话|notnull&NUM&len<=30">
          </TD>
          <TD  class= title8>
            E-MAIL
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail1 readonly=false verify="保险联系人一E-MAIL|len<=60&Email">
          </TD>           
        </TR>
     
        <TR  class= common>
          <TD  class= title8>
            保险联系人二
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan2 readonly=false verify="保险联系人二姓名|len<=10">
          </TD>
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone2 readonly=false verify="保险联系人二联系电话|NUM&len<=30">
          </TD>
          <TD  class= title8>
            E-MAIL
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail2 readonly=false verify="保险联系人二E-MAIL|len<=60&Email">
          </TD>
        </TR>     

      </table>
      <table class = common>
			<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input type =button class=cssButton value="保存申请" onclick="edorTypeADSave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input type =button class=cssButton value="取消" onclick="edorTypeADReturn()">
     	 </TD>
     	 </TR>
     	</table>
    </Div>
	</Div>
	
	  <Input type=Button value="返回" onclick="returnParent()">
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	 <input type=hidden id="addrFlag" name="addrFlag">
	 <input type=hidden id="GrpNo" name="GrpNo">    
	 <input type=hidden id="PrtNo" name="PrtNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
