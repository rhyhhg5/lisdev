<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：GEdorTypeMultiDetailSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
    //接收信息，并作校验处理
    System.out.println("-----Detail submit---");
                 
    String FlagStr = "";
    String Content = "";
    String transact = "";
    String Result="";
    TransferData tTransferData = new TransferData(); 
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
    
    LPEdorItemSet tLPEdorItemSet   = new LPEdorItemSet();
    LPInsuredSet tLPInsuredSet = null;
    LPGrpEdorItemSchema tLPGrpEdorItemSchema   = new LPGrpEdorItemSchema();
    
    tLPGrpEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
    tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
    tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
    transact=(String)request.getParameter("Transact");
    if (transact.equals("INSERT||EDOR"))
    {
        String tContNo[] = request.getParameterValues("LCInsuredGrid1");    //合同号
        String tInsuredNo[] = request.getParameterValues("LCInsuredGrid2"); //客户号
        String tChk[] = request.getParameterValues("InpLCInsuredGridChk"); 
        tLPInsuredSet = new LPInsuredSet();
        
        for(int index=0;index < tChk.length;index++)
        {
           if(tChk[index].equals("1"))
           {
             LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
             tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
             tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	           tLPEdorItemSchema.setContNo(tContNo[index]);
	           tLPEdorItemSchema.setInsuredNo(tInsuredNo[index]);
             tLPEdorItemSet.add(tLPEdorItemSchema);
             
             LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
             tLPInsuredSchema.setContNo(tContNo[index]);
             tLPInsuredSchema.setInsuredNo(tInsuredNo[index]);
             tLPInsuredSet.add(tLPInsuredSchema);
           }           
        }
    }
    else
    {
        String tContNo[] = request.getParameterValues("LCInsured2Grid1");    //合同号
        String tInsuredNo[] = request.getParameterValues("LCInsured2Grid2"); //客户号
        String tChk[] = request.getParameterValues("InpLCInsured2GridChk"); 
        tLPInsuredSet = new LPInsuredSet();
        
        System.out.println("tChk.length"+tChk.length);
        for(int index=0;index<tChk.length;index++)
        {
            if(tChk[index].equals("1"))
            {
              LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
              //个人保单批改主表信息
              tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
              tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	            tLPEdorItemSchema.setContNo(tContNo[index]);
	            tLPEdorItemSchema.setInsuredNo(tInsuredNo[index]);
              tLPEdorItemSet.add(tLPEdorItemSchema);
              
              LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
              tLPInsuredSchema.setContNo(tContNo[index]);
              tLPInsuredSchema.setInsuredNo(tInsuredNo[index]);
              tLPInsuredSet.add(tLPInsuredSchema);
            }           
        }
    }   	
  
  
  GEdorZTDetailUI tGEdorZTDetailUI   = new GEdorZTDetailUI();
  try {
    // 准备传输数据 VData
  	VData tVData = new VData();  
	 	tVData.addElement(tG);
	 	tVData.addElement(tLPGrpEdorItemSchema);
	 	tVData.addElement(tLPEdorItemSet);
	 	tVData.addElement(tLPInsuredSet);
	 	
	 	//保存个人保单信息(保全)	
    tGEdorZTDetailUI.submitData(tVData, transact);
	}
	catch(Exception ex) {
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="") {
    CErrors tError = new CErrors(); 
    tError = tGEdorZTDetailUI.mErrors;
    
    if (!tError.needDealError()) {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	
    	if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))	{
      	if (tGEdorZTDetailUI.getResult()!=null&&tGEdorZTDetailUI.getResult().size()>0) {
      		Result = (String)tGEdorZTDetailUI.getResult().get(0);
      		
      		if (Result==null||Result.trim().equals(""))	{
      			FlagStr = "Fail";
      			Content = "提交失败!!";
      		}
      	}
    	}
    }
    else {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>");
</script>
</html>

