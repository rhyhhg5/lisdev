var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
turnPage1.pageLineNum = 20;       //根据需求，显示20行
var turnPage2 = new turnPageClass();
var mSql="";

//简单查询
function easyQuery()
{
	var startDate = fm.all('StartDate').value;
	var endDate = fm.all('EndDate').value;
	var tSaleChnl = fm.all('SaleChnl').value; //保单类型
	var tManageCom = fm.all('ManageCom').value;//管理机构
	var tContNo = fm.all('ContNo').value;//保单号
	var tAgentCode = fm.all('AgentCode').value;//业务员代号
	var tAppntNo = fm.all('AppntNo').value;//投保人客户号
	var tPrintCount = fm.all('PrintCount').value;//打印标志
	
	//管理机构校验
	if(tManageCom == "" || tManageCom == null)
	{
		alert("请选择管理机构！");
		return false;
	}
	if(tManageCom == "86")
    {
      if(!confirm("管理机构选择“总公司”，查询速度会很慢，是否继续？"))
      {
        return false;
      }
    }
    
	//失效日期起期校验
	if (startDate == "")
	{
		alert("请输入失效日期起期！");
		return;
	}
	//失效日期止期校验
	if (endDate == "")
	{
		alert("请输入失效日期止期！");
		return;
	}
	var t1 = new Date(startDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(endDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>31 )
    {
	  alert("查询起止时间不能超过1个月！")
	  return false;
    }
    //var wherePar=" and exists (select 1 from lccontstate where contno=a.contno and State = '1' and PolNo = '000000' and statetype='Available' and statereason='02' and makedate between '"+startDate+"' and '"+endDate+" ') and  ";
    var wherePar=" and GETSTATEDATE(a.ContNo,'02','Available') >= '"+startDate+"' and GETSTATEDATE(a.ContNo,'02','Available')<='"+endDate+" ' and  ";
    //限制管理机构
    if(!(tManageCom == "" || tManageCom == null))
    {
      wherePar+="a.ManageCom like '"+tManageCom+"%' and " ; 
    }
    //限制保单类型
    if( !(tSaleChnl == "" || tSaleChnl == null))
    {
       if(tSaleChnl==1)
       {
          wherePar+="a.SaleChnl not in ('04','13') and ";
       }
       else if(tSaleChnl==2)
       {
          wherePar+="a.SaleChnl in ('04','13') and "; 
       }
       else
       {}
      
    }
    //限制保单号
    if( !(tContNo == "" ||  tContNo == null))
    {
       wherePar+="a.ContNo='"+tContNo+"' and " ;               
    }
    //限制业务员代号 
     if( !(tAgentCode == "" ||  tAgentCode == null))
    {
       wherePar+= "a.AgentCode=getAgentCode('"+tAgentCode+"') and ";             
    }
    //限制投保人客户号   
     if( !(tAppntNo == "" ||  tAppntNo == null))
    {
       wherePar+= "a.AppntNo='"+ tAppntNo+"' and " ;                       
    }
    //限制打印标志
     if( !(tPrintCount == "" ||  tPrintCount == null))
    {
       if(tPrintCount==2)
       {
          tPrintCount="1','0";
       }
       wherePar+= " exists( select 1 from LOPRTManager where code='42' and otherNo = a.ContNo and stateflag in ('"+ tPrintCount+"'))" ;                       
    }
    
        
   var strSQL =  "select "
	          +  "(select Name from ldcom where comcode=a.ManageCom) ," //管理机构名字
			  +  "(select (select name from labranchgroup where agentgroup=substr(b.branchseries,1,12) ) 	from labranchgroup  b where b.agentgroup=a.AgentGroup),"//营销部门
			  +  "a.ContNo,"//保单号
			  +  "a.AppntName,"//投保人 
			  +  "(select (case when c.Phone is null then c.homephone else c.phone end) from lcaddress c,lcappnt d where c.CustomerNo=d.AppntNo and c.AddressNo=d.AddressNo "
			  +  " and d.ContNo = a.ContNo fetch first 1 row only),"//投保人电话
			  +  "(select c.Mobile from lcaddress c,lcappnt d where c.CustomerNo=d.AppntNo and c.AddressNo=d.AddressNo "
			  +  " and d.ContNo = a.ContNo fetch first 1 row only)," //投保人移动电话
	          +  "(select c.PostalAddress from lcaddress c,lcappnt d where c.CustomerNo=d.AppntNo and c.AddressNo=d.AddressNo "
			  +  " and d.ContNo = a.ContNo fetch first 1 row only),"//投保人的联系地址
			  +  "(select sum(prem) from LCPol where ContNo = a.ContNo and ContType = '1' "
			  +  "and exists (select 1 from lmriskapp where riskcode = LCPol.riskcode and riskperiod = 'L')), " //（期缴保费）
			  +  "a.PaytoDate,"//（交至日）
			  +  "codename('paymode',a.PayMode),"//缴费方式
			  +  "(select max(prtSeq) from LOPRTManager where otherNo = a.ContNo and code = '42'),"//（失效/暂停通知书号）
			  +  "(case when a.SaleChnl='04' then '银保' when a.SaleChnl='13' then '银保' else '个险' end),"//（保单类型）
			  +  "getUniteCode(a.AgentCode),"//代理人编码
			  +  "(select Mobile from laagent where AgentCode=a.AgentCode),"//代理人电话
	          +  "(select Name from laagent where AgentCode=a.AgentCode),"//代理人姓名
	          +  "GETSTATEDATE(a.ContNo,'02','Available'), "//(失效日期)
	          //+" (select makedate from lccontstate where contno=a.contno and State = '1' and PolNo = '000000' and statetype='Available' and statereason='02')," //(失效日期)
	          +  "(case when (select agentstate from laagent where agentcode=a.agentcode)>='06' then '孤儿单' else '业务员在职' end) "//（保单归属状态）
			  +  "from lccont a "
			  +  "where a.conttype = '1' and a.stateflag = '2' "
			  +  "and exists(select 1 from LCPol where ContNo = a.ContNo and ContType = '1' "
			  +  "and exists (select 1 from lmriskapp where riskcode = LCPol.riskcode and riskperiod = 'L')) "//保证长期险
			  //+  "and GETSTATEDATE(a.ContNo,'02','Available') >= '" + startDate 
			  //+  "' and GETSTATEDATE(a.ContNo,'02','Available') <= '" + endDate + "' and " //限制起至时间	
			  +  wherePar
			  +"  with ur "
			  ;
	mSql=wherePar;
	turnPage1.queryModal(strSQL, ContPauseGrid);  
	if( ContPauseGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}
	fm.sql.value =wherePar;
	showCodeName(); 
}
//下载清单
function easyPrint()
{	
	
	fm.all('sql').value=mSql;
	if(fm.all('sql').value==null||fm.all('sql').value=="")
	{
	   alert("请先查询再进行打印！");
	   return false;
	}
	if( ContPauseGrid.mulLineCount == 0)
	{
	   if(!confirm("查询列表中没有长险记录，是否继续下载该清单？"))
	   {
		  return false;
	   }
		
	}
	fm.action="./PContPausePrint.jsp";
	fm.submit();
}
//打印PDF通知书
function newprintInsManage()
{
	if (ContPauseGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	
	
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < ContPauseGrid.mulLineCount; i++)
	{
		if(ContPauseGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	} 
	
	//保单号
	var ContNo=ContPauseGrid.getRowColData(tChked[0],3);
	fm.action = "../uw/PDFPrintSave.jsp?Code=42&RePrintFlag=&MissionID=&SubMissionID=&StandbyFlag2=&PrtFlag=&fmtransact=PRINT&OtherNo="+ContNo;
	fm.submit(); 
	
}
//批量打印
function printAllNotice()
{
	if (ContPauseGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < ContPauseGrid.mulLineCount; i++)
	{	
		if(ContPauseGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}

	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.action="../bq/PContPausePrintAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.action="../bq/PContPausePrintForBatPrt.jsp";
		fm.submit();
	}

}

function afterSubmit2(FlagStr,Content){
    alert(Content);
}















