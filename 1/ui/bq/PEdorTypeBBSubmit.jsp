<%
//程序名称：GEdorTypeBBSubmit.jsp
//程序功能：
//创建日期：2002-10-9 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 	<%@page import="com.sinosoft.lis.bq.*"%>
 	<%@page import="com.sinosoft.lis.pubfun.*"%>
 	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  
  LPInsuredSchema tLPInsuredSchema   = new LPInsuredSchema();
  LPPersonSchema  tLPPersonSchema = new LPPersonSchema();
  LPAddressSchema tLPAddressSchema = new LPAddressSchema();
  LPContSchema  tLPContSchema = new LPContSchema();
  LPEdorItemSchema tLPEdorItemSchema   = new LPEdorItemSchema();
  
  PEdorBBDetailUI  tPEdorBBDetailUI = new PEdorBBDetailUI();
  GlobalInput tG = new GlobalInput();  
  tG = (GlobalInput)session.getValue("GI"); 
  //输出参数
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String addrFlag="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  
  addrFlag=request.getParameter("addrFlag");
  
 
 //个人保全项目表信息
  	tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
  	tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
  	tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
  	tLPEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
  	tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
  	tLPEdorItemSchema.setInsuredNo(request.getParameter("CustomerNo"));  
	tLPEdorItemSchema.setPolNo("000000");
	  
  //保全被保人表信息
	tLPInsuredSchema.setContNo(request.getParameter("ContNo"));
        tLPInsuredSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPInsuredSchema.setEdorType(request.getParameter("EdorType"));
	tLPInsuredSchema.setInsuredNo(request.getParameter("CustomerNo"));
	tLPInsuredSchema.setName(request.getParameter("Name"));
        tLPInsuredSchema.setSex(request.getParameter("Sex"));
        tLPInsuredSchema.setBirthday(request.getParameter("Birthday"));
	tLPInsuredSchema.setIDType(request.getParameter("IDType"));
	tLPInsuredSchema.setIDNo(request.getParameter("IDNo"));
        tLPInsuredSchema.setAddressNo(request.getParameter("AddressNo"));
	tLPInsuredSchema.setGrpContNo(request.getParameter("GrpContNo"));
//个人保单信息
  tLPContSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPContSchema.setEdorType(request.getParameter("EdorType"));
  tLPContSchema.setContNo(request.getParameter("ContNo"));  
  tLPContSchema.setInsuredName(request.getParameter("Name"));
  tLPContSchema.setInsuredSex(request.getParameter("Sex"));
  tLPContSchema.setInsuredBirthday(request.getParameter("Birthday"));
  tLPContSchema.setInsuredIDType(request.getParameter("IDType"));
  tLPContSchema.setInsuredIDNo(request.getParameter("IDNo"));

//个人客户地址表信息
  tLPAddressSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPAddressSchema.setEdorType(request.getParameter("EdorType"));
  tLPAddressSchema.setCustomerNo(request.getParameter("CustomerNo"));
  tLPAddressSchema.setAddressNo(request.getParameter("AddressNo"));  
  tLPAddressSchema.setHomeAddress(request.getParameter("HomeAddress"));  
  tLPAddressSchema.setPostalAddress(request.getParameter("PostalAddress"));
  tLPAddressSchema.setZipCode(request.getParameter("ZipCode"));
  tLPAddressSchema.setPhone(request.getParameter("Phone"));
  tLPAddressSchema.setMobile(request.getParameter("Mobile"));
// 个人客户表信息
  tLPPersonSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPPersonSchema.setEdorType(request.getParameter("EdorType"));
  tLPPersonSchema.setCustomerNo(request.getParameter("CustomerNo"));
  tLPPersonSchema.setIDType(request.getParameter("IDType"));
  tLPPersonSchema.setIDNo(request.getParameter("IDNo"));

try
  {
  // 准备传输数据 VData
  
  	 VData tVData = new VData();  
  	
	 //保存集体保单信息(保全)		
  	tVData.addElement(addrFlag);
  	tVData.addElement(tLPEdorItemSchema);
    tVData.addElement(tLPInsuredSchema);
    tVData.addElement(tLPContSchema);
    tVData.addElement(tLPAddressSchema);
    tVData.addElement(tLPPersonSchema);
  	tVData.addElement(tG);
    tPEdorBBDetailUI.submitData(tVData,transact);
	
	
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorBBDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

