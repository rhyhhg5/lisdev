<%
//程序名称：PEdorTypePROut.jsp
//程序功能：
//创建日期：2007-10-12 10:10
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
 	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
	//接收信息，并作校验处理。
  //输入参数
  LPEdorItemSchema tLPEdorItemSchema   = new LPEdorItemSchema();
  PEdorTypePROutUI tPEdorTypePROutUI = new PEdorTypePROutUI();
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
	  String tRela  = "";                
	  String FlagStr = "";
	  String Content = "";
	  String transact = "";
	  String Result="1";
  
  //个人保单批改信息
  
    tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
		tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
		tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
		tLPEdorItemSchema.setEdorNo(request.getParameter("EdorAcceptNo"));
          
		try
  	{
			 // 准备传输数据 VData
    	 VData tVData = new VData();  
  	
	 		//保存个人保单信息(保全)	
			 tVData.addElement(tG);
			 tVData.addElement(tLPEdorItemSchema);
		   tPEdorTypePROutUI.submitData(tVData,"");
		}
		catch(Exception ex)
		{
			Content = transact+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
		}			
  	//如果在Catch中发现异常，则不从错误类中提取错误信息
 	 if (FlagStr=="")
 	 {
   	 tError = tPEdorTypePROutUI.mErrors;
 	   if (!tError.needDealError())
 	   {                          
  	    Content = " 保存成功";
  	  	FlagStr = "Success";
   	 }
   	 else                                                                           
   	 {
   		 	Content = " 保存失败，原因是:" + tError.getFirstError();
   		 	FlagStr = "Fail";
  	  }
  	}
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

