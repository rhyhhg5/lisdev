<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：LCPolQueyr.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>	
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  //保单信息部分
  LCPolSchema tLCPolSchema   = new LCPolSchema();
  tLCPolSchema.setPolNo(request.getParameter("PolNo"));
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLCPolSchema);
  // 数据传输
  CPolUI tCPolUI   = new CPolUI();
	if (!tCPolUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tCPolUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tCPolUI.getResult();
		
		// 显示
		LCPolSet mLCPolSet = new LCPolSet(); 
		mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolBLSet",0));
		int n = mLCPolSet.size();
		System.out.println("get LCPol "+n);
		for (int i = 1; i <= n; i++)
		{
		  	LCPolSchema mLCPolSchema = mLCPolSet.get(i);
		   	%>
		   	<script language="javascript">
		   	  parent.fraInterface.LCPolGrid.addOne("LCPolGrid")
		   		parent.fraInterface.fm.LCPolGrid1[<%=i-1%>].value="<%=mLCPolSchema.getPolNo()%>";
		   		parent.fraInterface.fm.LCPolGrid2[<%=i-1%>].value="<%=mLCPolSchema.getContNo()%>";
		   		parent.fraInterface.fm.LCPolGrid3[<%=i-1%>].value="<%=mLCPolSchema.getGrpPolNo()%>";
		   		parent.fraInterface.fm.LCPolGrid4[<%=i-1%>].value="<%=mLCPolSchema.getProposalNo()%>";
		   	</script>
		<%		
		}
	}
 // end of if  
 
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tCPolUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
//  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
//  out.println("top.close();");
//  out.println("</script>");
%>
