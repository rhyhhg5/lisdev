<%
//程序名称：LMInsuAccRateMain.jsp
//程序功能：万能险帐户结算利率录入主界面
//创建日期：2007-12-12
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>		<!--字符集编码-->
<%@include file="../common/jsp/UsrCheck.jsp"%>	<!--用户校验，校验用户是否登录-->
<html>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="1">
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	<!--表单提交帧，必须有，在提交时指向本帧-->
	<frame name="fraSubmit"  scrolling="yes" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="*,0" cols="*">
        <!--页面展现帧，src一般指向需要展现的页面文件-->
		<frame id="fraInterfaceView" name="fraInterface" scrolling="auto" src="LMInsuAccRate.jsp">
		<frame id="fraNext" name="fraNext" scrolling="auto" src="">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
