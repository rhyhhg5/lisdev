//               该文件中包含客户端需要处理的函数和事件
var showInfo1;
var mDebug = "1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function edorTypeLNReturn() {
	try {
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {
	}
}
function getPolInfo(tContNo) {
	var strSQL = "select a.riskcode,(select riskname from lmrisk where riskcode=a.riskcode),amnt,prem," + " (select edorvalue from lpedorespecialdata where edorno='" + fm.all("EdorNo").value + "' and detailtype='CASHVALUE')," + " (select edorvalue from lpedorespecialdata where edorno='" + fm.all("EdorNo").value + "' and detailtype='CANLOAN')," + " (select edorvalue from lpedorespecialdata where edorno='" + fm.all("EdorNo").value + "' and detailtype='LOAN')," + " (case when payintv!=0 and current date>paytodate and paytodate<payenddate then 1 else 0 end),polno from LCPol a " + "  where ContNo='" + fm.all("ContNo").value + "' and riskcode in (select riskcode from lmloan) order by riskcode fetch first 1 row only with ur";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
	if (!turnPage.strQueryResult) {
		return false;
	}
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
	turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
	turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	if (PolGrid.getRowColData(0, 8) !== null && PolGrid.getRowColData(0, 8) !== "null" && PolGrid.getRowColData(0, 8) !== "") {
    	//XQFlag:0表示无需续期,1表示需要交续期保费
		fm.all("XQFlag").value = PolGrid.getRowColData(0, 8);
		fm.all("Prem").value = PolGrid.getRowColData(0, 4);
		fm.all("PolNo").value = PolGrid.getRowColData(0, 9);
		fm.all("RiskCode").value = PolGrid.getRowColData(0, 1);
//    		PolGrid.setRowColData(i, 10, "");
	}
}
function edorTypeLNSave() {
	if (!beforeSubmit()) {
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo1 = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "PEdorTypeLNSubmit.jsp";
	fm.submit();
}
function CalLoanInfo() {
	fm.action = "PEdorLNCalLoanSave.jsp";
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo1 = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Flag) {
	try {
		showInfo1.close();
	}
	catch (ex) {
	}
	window.focus();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		if (Flag == "1") {
			initForm();
		} else {
			edorTypeLNReturn();
		}
	}
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
	try {
		initForm();
	}
	catch (re) {
		alert("\u5728Proposal.js-->resetForm\u51fd\u6570\u4e2d\u53d1\u751f\u5f02\u5e38:\u521d\u59cb\u5316\u754c\u9762\u9519\u8bef!");
	}
} 

//取消按钮对应操作
function cancelForm() {
	showDiv(operateButton, "true");
	showDiv(inputButton, "false");
}
 
//提交前的校验、计算  
function beforeSubmit() {
	if (PolGrid.getRowColData(0, 6) == null || PolGrid.getRowColData(0, 6) == "" || PolGrid.getRowColData(0, 6) == "null") {
		alert("\u8bf7\u5148\u8ba1\u7b97\u8d37\u6b3e\u76f8\u5173\u4fe1\u606f!");
		return false;
	}
	if (PolGrid.getRowColData(0, 7) == null || PolGrid.getRowColData(0, 7) == "" || PolGrid.getRowColData(0, 7) == "null") {
		alert("\u8bf7\u5f55\u5165\u5b9e\u9645\u7533\u8bf7\u989d\u5ea6!");
		return false;
	}
	//设置贷款金额
	fm.all("LoanMoney").value = PolGrid.getRowColData(0, 7);
	var loanmoney = (fm.all("LoanMoney").value)*1.00;
	var canloan = (PolGrid.getRowColData(0, 6))*1.00;
	if (loanmoney > canloan) {
		alert("\u5b9e\u9645\u7533\u8bf7\u989d\u5ea6\u5927\u4e8e\u53ef\u8d37\u6b3e\u989d\u5ea6");
		return false;
	}
	if (!verifyInput2()) {
		return false;
	}
	return true;
}           

//Click事件，当点击增加图片时触发该函数
function addClick() {
  //下面增加相应的代码
	showDiv(operateButton, "false");
	showDiv(inputButton, "true");
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick() {
  //下面增加相应的代码
	alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick() {
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick() {
  //下面增加相应的代码
	alert("delete click");
}           
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,72,*";
	}
}

