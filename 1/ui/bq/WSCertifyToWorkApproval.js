//程序名称：
//程序功能：激活卡补激活并生成客户资料变更的工单
//创建日期：2010-08-12
//创建人 :  XP
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryCertifyList()
{
    if(!verifyInput2())
    {
        return false;
    }
	if((fm.EdorNo.value==""||fm.EdorNo.value==null)&&
		(fm.CardNo.value=="" || fm.CardNo.value==null))
	{
		alert("请至少录入工单号、保险卡号其中的一项！");
		return false;
	}
    var tStrSql = ""
        + " select lpe.ContNo, lpe.EdorNo,  lpe.InsuredNo, "
        + " lic.Name, lic.IdNo "
        + " from lpedoritem lpe "
        + " left join LICertifyInsured lic on lpe.ContNo = lic.CardNo and lpe.InsuredNo=lic.customerno "
        + " where 1=1"
        + getWherePart("lpe.ContNo", "CardNo")
        + getWherePart("lpe.EdorNo", "EdorNo")
        + " and edorreasonno is null "
		+ " and exists (select 1 from lpedorapp where edoracceptno=lpe.edorno and edorstate='0') "
        + " with ur "
        ;

    turnPage1.pageDivName = "divCertifyListGridPage";
    turnPage1.queryModal(tStrSql, CertifyListGrid);

    return true;
}

/**
 * 卡折实名化
 */
function dealWSCertify()
{
    var tRow = CertifyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    fm.BCardNo.value = CertifyListGrid.getRowColData(tRow,1);
    fm.BEdorNo.value = CertifyListGrid.getRowColData(tRow,2);
    fm.BInsuredNo.value = CertifyListGrid.getRowColData(tRow,3);
    
    var strSQL="select operatetype from lmcertifydes where subcode=(select cardtype from lzcardnumber where cardno='"+fm.BCardNo.value+"')";
    var arrResult=easyExecSql(strSQL);
    if(arrResult)
    {
    	if(arrResult[0][0]!='2'&&arrResult[0][0]!='0'&&arrResult[0][0]!='1')
    	{
    		alert("卡号"+fm.BCardNo.value+"不是激活卡或者撕单类型,程序暂不支持此类变更");
        	return false;
    	}
    }
    else
    {
    	alert("查询卡号"+fm.BCardNo.value+"定义信息失败");
    	return false;
    }
    var strSQL2="select count(1) from llcase where customerno=(select insuredno from lccont where contno='"+fm.BCardNo.value+"') and rgtstate  in ('11','12','14') with ur";
    var arrResult2=easyExecSql(strSQL2);
    if(arrResult2)
    {
    	if(arrResult2[0][0]!=0){
    		if(!confirm("该卡对应被保人之前有过已经结案或者撤销的理赔,是否继续操作?")){
    		return false;
    		}
    	}
    }
    showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.action = "WSCertifyToWorkApprovalSave.jsp";
    fm.submit();
    fm.action = "";
}

/**
 * 实名化提交后动作。
 */
function afterWSSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("确认失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        queryCertifyList()
    }
}

