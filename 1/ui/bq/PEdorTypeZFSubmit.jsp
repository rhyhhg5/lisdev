<%
//程序名称：PEdorTypeWTSubmit.jsp
//程序功能：
//创建日期：2008-2-25 10:16
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  LPPolSet mLPPolSet=new LPPolSet();
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

	transact = request.getParameter("fmtransact");
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String contNo = request.getParameter("ContNo");
	
	
	
	String tContNo[] = request.getParameterValues("PolGrid8");             
    String tGrpContNo[]= request.getParameterValues("PolGrid9");
    String tInsuredNo[]= request.getParameterValues("PolGrid1");
    String tPolNo[]= request.getParameterValues("PolGrid3");
    String tChk[] = request.getParameterValues("InpPolGridChk");
    for (int i=0;i<tChk.length;i++)
    {
        System.out.println("$$$$$$$$$$$44"+tChk[i]);
        if (tChk[i].equals("1"))
        {
            LPPolSchema tLPPolSchema=new LPPolSchema();
            tLPPolSchema.setEdorNo(edorNo);
            tLPPolSchema.setEdorType(edorType);
            tLPPolSchema.setContNo(contNo);
            tLPPolSchema.setInsuredNo(tInsuredNo[i]);
            tLPPolSchema.setPolNo(tPolNo[i]);
            System.out.println("++++++++++++++++"+tPolNo[i]);
            mLPPolSet.add(tLPPolSchema);
        }
    }
                
	
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setContNo(contNo);
	tLPEdorItemSchema.setEdorType(edorType);
	tLPEdorItemSchema.setInsuredNo("000000");
	tLPEdorItemSchema.setPolNo("000000");
	//tLPEdorItemSchema.setGetMoney(getMoney);
	
        // 准备传输数据 VData		
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPEdorItemSchema);
	tVData.add(mLPPolSet);

    

    PEdorZFDetailUI tPEdorZFDetailUI   = new PEdorZFDetailUI();  
		if (!tPEdorZFDetailUI.submitData(tVData, ""))
		{
			VData rVData = tPEdorZFDetailUI.getResult();
			System.out.println("Submit Failed! " + tPEdorZFDetailUI.mErrors.getErrContent());
			Content = transact + "失败，原因是:" + tPEdorZFDetailUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
		else 
		{
			Content = "保存成功";
			FlagStr = "Success";
		} 

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

