//程序名称：LMInsuAccRate.js
//程序功能：万能险帐户结算利率录入
//创建日期：2007-12-12
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}

function beforeSubmit(){
	var reg=/^\d*\.\d*$/;
	var regs=/^(:?(:?\d+.\d+)|(:?\d+))$/;
	if(!reg.test(fm.CanLoanRate.value)||fm.CanLoanRate.value>=1){
		alert("请输入正确的小于1的贷款比例");
		return false;
	}
	if(!regs.test(fm.MinPremLimit.value)){
		alert("请输入正确的最小保费金额");
		return false;
	}
	if(fm.MaxPremLimit.value!=""){
		if(!regs.test(fm.MaxPremLimit.value)){
			alert("请输入正确的最大保费金额");
			return flase;
		}
	}
	return true;
}

function queryCanLoanRate(){
	var tSql = " select a.managecom,(select  name from ldcom where comcode=a.managecom), " +
			" a.riskcode,(select riskname from lmriskapp where riskcode=a.riskcode)," +
			" CanLoanRate,a.MinPremLimit,a.MaxPremLimit  from LMComLoanRate a " +
			" where 1=1 " 
	        + getWherePart('riskcode','RiskCode')
	        + getWherePart('managecom','aManageCom')
	        + " order by  a.managecom,a.riskcode,a.MinPremLimit ";
	turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    	LMCanloanComGrid.clearData();
        alert("未查询到满足条件的数据！");
        return false;
    }
    turnPage.pageDivName="divturnPage";
    turnPage.queryModal(tSql, LMCanloanComGrid);
}

function deleteConfig(){
	//提交数据
	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.transact.value="DELETE||MAIN";
	fm.submit();
}

function saveConfig()
{
	  if (!verifyInput2())
	  {
	    return false;
	  }
	  if(!beforeSubmit()){
		  return false;
	  }
	  if(fm.MinPremLimit.value!=""&&fm.MaxPremLimit.value!=""){
		  if(fm.MinPremLimit.value*1>=fm.MaxPremLimit.value*1){
			  alert("最小保费金额必须小于最大保费金额");
			  return false;
		  }
	  }
	//提交数据
	var showStr="正在设置数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.transact.value="INSERT||MAIN";
	fm.submit();	
}

//重置
function cancleConfig(){
	initRateBox();
}

//当选择一条记录时，在页面显示其具体信息
function ShowDetail(){
	var arrReturn = getSelectedResult();
  	afterSelected(arrReturn);
}
function getSelectedResult(){
	var arrSelected = null;
	tRow =LMCanloanComGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	{
	  return arrSelected;
	}
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = LMCanloanComGrid.getRowData(tRow-1);
	return arrSelected;
}	
function afterSelected(arrSelectedResult){
	
	var arrResult = new Array();	
	if(arrSelectedResult!= null )
	{
	  arrResult = arrSelectedResult;
	  fm.aManageCom.value=arrResult[0][0];
	  fm.ManageComName.value=arrResult[0][1];
	  fm.RiskCode.value = arrResult[0][2];
	  fm.MinPremLimit.value=arrResult[0][5];
	  fm.MaxPremLimit.value=arrResult[0][6];
	  fm.CanLoanRate.value = arrResult[0][4];
	  fm.RiskName.value = arrResult[0][3];
	  
	  fm.DManageCom.value=arrResult[0][0];
	  fm.DRiskCode.value = arrResult[0][2];
	  fm.DMinPremLimit.value = arrResult[0][5];
	  fm.DMaxPremLimit.value = arrResult[0][6];
	  fm.DCanLoanRate.value= arrResult[0][4];
	  
	  //选中后只能修改【利率终止日期】字段
	  fm.all("aManageCom").disabled = true;
	  fm.ManageComName.disabled = true;
	  fm.all("RiskCode").disabled = true;
	  fm.MinPremLimit.disabled = true;
	  fm.MaxPremLimit.disabled = true;
	  fm.all("CanLoanRate").disabled = true;
	  fm.RiskName.disabled = true;
	}
	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	initRateBox();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  
}



