<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LDPersonSave.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  
  LPEdorMainSet mLPEdorMainSet   = new LPEdorMainSet();
  LPEdorAppSchema tLPEdorAppSchema=new LPEdorAppSchema();  
  
  LCInsuredSet tLCInsuredSet=new LCInsuredSet();
  LCPolSet tLCPolSet=new LCPolSet();
   
  TransferData tTransferData = new TransferData(); 
  PEdorMainUI tPEdorMainUI   = new PEdorMainUI();
  //输出参数
  String FlagStr = "";
  String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  System.out.println("tGI"+tGI);
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    CErrors tError = null;
    String tBmCert = "";
    System.out.println("aaaa");
    //后面要执行的动作：添加，修改，删除
    String fmAction=request.getParameter("fmtransact");
    System.out.println("fmAction:"+fmAction); 

     try
    {
        if(fmAction.equals("INSERT||EDOR"))
        {
            
  
              tLPEdorAppSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo")); //保全受理号
                tLPEdorAppSchema.setOtherNo(request.getParameter("OtherNo")); //申请号码
                tLPEdorAppSchema.setOtherNoType(request.getParameter("OtherNoType")); //申请号码类型
                tLPEdorAppSchema.setEdorAppName(request.getParameter("EdorAppName")); //申请人名称
                tLPEdorAppSchema.setAppType(request.getParameter("AppType")); //申请方式
                tLPEdorAppSchema.setManageCom(request.getParameter("ManageCom")); //管理机构
            //    tLPEdorAppSchema.setChgPrem(request.getParameter("ChgPrem")); //变动的保费
            //    tLPEdorAppSchema.setChgAmnt(request.getParameter("ChgAmnt")); //变动的保额
            //    tLPEdorAppSchema.setChgGetAmnt(request.getParameter("ChgGetAmnt")); //变动的领取保额
            //    tLPEdorAppSchema.setGetMoney(request.getParameter("GetMoney")); //补/退费金额
            //    tLPEdorAppSchema.setGetInterest(request.getParameter("GetInterest")); //补/退费利息
                tLPEdorAppSchema.setEdorAppDate(request.getParameter("EdorAppDate")); //批改申请日期
            //    tLPEdorAppSchema.setEdorState(request.getParameter("EdorState")); //批改状态
            //    tLPEdorAppSchema.setBankCode(request.getParameter("BankCode")); //银行编码
            //    tLPEdorAppSchema.setBankAccNo(request.getParameter("BankAccNo")); //银行帐号
            //    tLPEdorAppSchema.setAccName(request.getParameter("AccName")); //银行帐户名
            
            
            String tContNo[] = request.getParameterValues("LCContGrid1");             
            String tGrpContNo[]= request.getParameterValues("LCContGrid7");
            String tChk[] = request.getParameterValues("InpLCContGridChk");
            for(int index=0;index<tChk.length;index++)
            {
                if(tChk[index].equals("1"))
                {
                    LPEdorMainSchema tLPEdorMainSchema=new LPEdorMainSchema();
                    tLPEdorMainSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo")); 
              
                    tLPEdorMainSchema.setContNo(tContNo[index]);

                  
                 
                    mLPEdorMainSet.add(tLPEdorMainSchema);
                }           
            }   
             
                
        }       
   
        // 准备传输数据 VData
         VData tVData = new VData();
         tVData.add(tLPEdorAppSchema);
         tVData.add(mLPEdorMainSet);
         tVData.add(tTransferData);
         tVData.add(tGI);
         
          
         //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        if ( tPEdorMainUI.submitData(tVData,fmAction))
        {
	        %>
	            <SCRIPT language="javascript">
	                parent.fraInterface.initContGrid();
	                parent.fraInterface.initLCContGrid();
	                parent.fraInterface.queryLCCont();
	                parent.fraInterface.queryCont();     
                </SCRIPT>
	        <%
	    }
    }
    
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      System.out.println("aaaa"+ex.toString());
      FlagStr = "Fail";
    }
  

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorMainUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
   }
 
}//页面有效区
%>                                       
<html>
<script language="javascript">
	var mLoadFlag = "<%=request.getParameter("LoadFlag")%>";
	if (mLoadFlag == "TASKFRAME")
	{
		parent.fraInterface.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
	else
	{
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
</script>
</html>

