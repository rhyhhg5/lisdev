//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function verify()
{
  if (parseFloat(fm.all("LPSumMoney").value) < parseFloat(fm.all("SumMoney").value))
  {
    alert("借款金额大于可借款总额，请重新输入！"); 
    return false;
  }
  if(parseFloat(fm.all("SumMoney").value) < 10000)
  {
    alert("借款金额小于规定借款最低金额10000元，请重新输入！");	
    return false;
  }  
  return true;
}

function edorTypeLFSave()
{
  if (!verifyInput()) 
  {
    return false; 
  }
  
  if (!verify()) 
  {
    return false; 
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result, canLoanMoney, thisLoanMoney)
{ 
  try { showInfo.close(); } catch (e) {}
  
  var tTransact=fm.all('fmtransact').value;
	if (FlagStr=="Success" && tTransact=="QUERY||MAIN")
	{
		var tArr = decodeEasyQueryResult(Result, 0);
		
		fm.all('RiskCode').value = tArr[0][7];
		fm.all('GrpNo').value = tArr[0][15];
		fm.all('GrpName').value = tArr[0][17];
                fm.all('Peoples2').value = tArr[0][23];
		fm.all('CValidate').value = tArr[0][53];
		fm.all('PayToDate').value = tArr[0][58];
		fm.all('Prem').value = tArr[0][62];
		fm.all('Amnt').value = tArr[0][63];

                fm.all("LPSumMoney").value = canLoanMoney;
                fm.all("SumMoney").value = thisLoanMoney;
	}
	else
	{  
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
}

function returnParent()
{
	top.close();
}

/*
function showChoose() 
{
  if (fm.RChoose[0].checked == true) 
  {
    divRate.style.display = ""; 
  } 
  else 
  {
    divRate.style.display = "none"; 
  }
}
*/
