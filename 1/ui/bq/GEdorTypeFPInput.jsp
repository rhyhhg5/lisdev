<html> 
<% 
//程序名称：GEdorTypeFPInput.jsp
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeFP.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeFPInit.jsp"%>
  <title>投保人重要资料变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeFPSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class= "readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 集体保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpContNo>
      </TD>   
    </TR>
  </TABLE> 
  <br>
		<table>
		  <tr>
		    <td>
		    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpInfo);">
		    </td>
				<td class= titleImg>
					 投保单位资料
				</td>
		  </tr>
		</table>
    <Div  id= "divGrpInfo" style= "display: ''">
      <table  class= common>
       <TR>
          <TD  class= title>
            单位客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" name=AppntNo readonly >
          </TD>  
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input class="readonly" name=GrpName readonly >
          </TD> 
        </TR>
        <TR>
          <TD  class= title>
            签单日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=SignDate readonly >
          </TD>
      
          <TD  class= title>
            生效日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=CValiDate readonly >
          </TD>
          <TD  class= title>
              合同终止日期
          </TD>
          <TD  class= input>
              <Input class= "readonly" readonly name=CInValiDate >
          </TD>
        </TR>
        <TR>
        	<TD  class= title>
            签单机构
          </TD>
          <TD  class= input>
            <Input class="readonly" name=SignCom readonly >
          </TD>
          <TD  class= title>
            投保人数
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=Peoples2 readonly >
          </TD>
        </TR>  
      </table>
      <br>
    </Div>
    <table>
		  <tr>
		    <td>
		    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divServiceInfo);">
		    </td>
				<td class= titleImg>
					 保单服务约定
				</td>
		  </tr>
		</table>
    <Div  id= "divServiceInfo" style= "display: ''">
      <table  class= common>
       <TR>
          <!--TD  class= title8>
            提交方式
          </TD>
          <TD  class= input8>
            <Input class=codeNo name=servchoose0 readonly CodeData="0|2|^1|保险公司上门收取^2|投保单位上门办理" ondblclick="return showCodeListEx('ServerInfoChooseCode10',[this,servchooseName0],[0,1]);" onkeyup="return showCodeListKeyEx('ServerInfoChooseCode',[this,servchooseName0],[0,1]);"><input class=codename name=servchooseName0 readonly=true>
          </TD-->  
          <TD class= title8>
            提交频次
          </TD>
          <TD  class= input8>
          	<Input class=codeNo name=servchoose1 MAXLENGTH=2 verify="提交频次|code:ServerInfoChooseCode" CodeData="0|^1|月度^2|季度^3|半年度^4|随时^5|年度" ondblclick="return showCodeListEx('ServerInfoChooseCode',[this,servchooseName1],[0,1]);" onkeyup="return showCodeListKeyEx('ServerInfoChooseCode',[this,servchooseName1],[0,1]);"><input class=codename name=servchooseName1>
          	<Input class=readonly type=hidden name=servchoose1Bak>
          </TD>
          <TD  class= title8>
            结算频次
          </TD>
          <TD  class= input8>
            <Input class=codeNo name=servchoose2 MAXLENGTH=2 verify="结算频次|code:ServerInfoChooseCode" CodeData="0|^1|月度^2|季度^3|半年度^4|随时^5|年度" ondblclick="return showCodeListEx('ServerInfoChooseCode',[this,servchooseName2],[0,1]);" onkeyup="return showCodeListKeyEx('ServerInfoChooseCode',[this,servchooseName2],[0,1]);"><input class=codename name=servchooseName2>
            <Input class=readonly type=hidden name=servchoose2Bak>
          </TD>
        </TR>
      </table>
      <br>
    </Div>
		<table class = common>
			<TR class =common>
				<TD width="26%"> 
				 <Input type=button class=cssButton name="save" value="保  存" onclick="edorTypeFPSave()">
				 <Input type=button class=cssButton name="cancel" value="取  消" onclick="edorTypeFPCancel()">
				 <Input type=Button class=cssButton name="goBack" value="返  回" onclick="returnParent()">
				</TD>
			</TR>
		</table>
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	 <input type=hidden id="ProposalGrpcontno" name="ProposalGrpcontno">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
