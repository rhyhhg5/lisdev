//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var arrResult
var oldSex = "";
var oldBirthday = "";
var oldOccupationType = "";


//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeEWReturn() {
  initForm();
}

function edorTypeEWSave() 
{
  if (!beforeSubmit())
  {
    return false;
  }

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}

//function customerQuery()
////{	
////	window.open("./LCAppntIndQuery.html");
//}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result)
{
	try
	{
	  showInfo.close();
	  window.focus();
	}
	catch (ex) {}
	
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	  
	  	var tTransact=fm.all('fmtransact').value;
			if (tTransact=="QUERY||MAIN")
			{
				var iArray;
				//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	  		    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
				//保存查询结果字符串
	 		 	turnPage.strQueryResult  = Result;
	  		    //使用模拟数据源，必须写在拆分之前
	  		    turnPage.useSimulation   = 1;  
	    
	  		    //查询成功则拆分字符串，返回二维数组
	  		    var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
				
				turnPage.arrDataCacheSet =chooseArray(tArr,[1,2,8,5]);
				//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
			 	turnPage.pageDisplayGrid = LCAppntIndGrid;    
			  
			  //设置查询起始位置
			 	turnPage.pageIndex       = 0;
			 	//在查询结果数组中取出符合页面显示大小设置的数组
		  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
				//调用MULTILINE对象显示查询结果
		   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
			}
			else if (tTransact=="QUERY||DETAIL")
			{
//				//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
			}
			else
			{    
		    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
		   	returnParent();   
			}
	  

  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  //校验联系电话和移动电话
//  if(!verifyPhone(fm.all("PPhone").value,fm.all("PMobile").value)){
//	  return false;
//  }
  
//  if(fm.PPostalAddress.value.trim().length<=6){
//	  alert("地址信息低于字符限制要求，须提详细地址信息!");
//	  fm.PPostalAddress.focus();
//	  return false;
//  }
  
  return true;
}           

//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		// 查询保单明细
		queryAppntIndDetail();
	}
}
function queryAppntIndDetail()
{
	var tEdorNO;
	var tEdorType;
	var tContNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	//alert(tEdorNo);
	tEdorType=fm.all('EdorType').value;
	//alert(tEdorType);
	tContNo=fm.all('ContNo').value;
	//alert(tContNo);
	tCustomerNo = fm.all('CustomerNo').value;
	//alert(tCustomerNo);
	//top.location.href = "./AppntIndQueryDetail.jsp?EdorNo=" + tEdorNo+"&EdorType="+tEdorType+"&ContNo="+tContNo+"&CustomerNo="+tCustomerNo;
	parent.fraInterface.fm.action = "./AppntIndQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PedorTypeEWSubmit.jsp";
}

function returnParent()
{
	try
	{
		top.opener.getEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {}
}


function initQuery()
{

	var tStrold = "select MedicalCode,IfAutoPay,(case IfAutoPay when '1' then '是' when '0' then '否' end)IfAutoPayName, RenemalPayMethod," +
			" (case RenemalPayMethod when '1' then '医保优先，不足银行卡扣款' when '2' then '仅医保账户扣款' when '3' then '仅银行卡扣款' end)RenemalPayMethodName,prtno " +
			" from LCcontsub where prtno= (select prtno from lccont where contno='"+fm.all('ContNo').value+"')"; 

	arrResult = easyExecSql(tStrold,1,0);
	if(arrResult == null)
	{
		return false;
	}
	else
	{
		try{fm.all('prtno').value= arrResult[0][5]; }catch(ex){}; 
		try{fm.all('Name').value= arrResult[0][0]; }catch(ex){};
		
		try{fm.all('IfAutoPay').value= arrResult[0][1]; }catch(ex){}; 
		try{fm.all('IfAutoPayName').value = arrResult[0][2];}catch(ex){};
		
		try{fm.all('PayMode').value= arrResult[0][3]; }catch(ex){};
		try{fm.all('PayModeName').value = arrResult[0][4]; }catch(ex){}; 
	}
	
	var tEdorNo = fm.all('EdorNO').value;
	var tEdorType = fm.all('EdorType').value;
	var tContNo = fm.all('ContNo').value;
	
	var tStrnew="select medicalcode,ifautopay,(case IfAutoPay when '1' then '是' when '0' then '否' end)IfAutoPayName," +
			" renemalpaymethod,(case RenemalPayMethod when '1' then '医保优先，不足银行卡扣款' when '2' then '仅医保账户扣款' when '3' then '仅银行卡扣款' end)RenemalPayMethodName" +
			" from LPcontsub where EdorNo='"+tEdorNo+"' and EdorType='"+tEdorType+"' " +
			" with ur";
	arrResult = easyExecSql(tStrnew,1,0);
	if (arrResult == null){
		
	}else{
		try{fm.all('Name1').value= arrResult[0][0];}catch(ex){};
		
	    try{fm.all('IfAutoPay1').value= arrResult[0][1];}catch(ex){};
	    try{fm.all('IfAutoPayName1').value = arrResult[0][2];}catch(ex){};  
		
	    try{fm.all('PayMode1').value= arrResult[0][3];}catch(ex){};
	    try{fm.all('PayModeName1').value = arrResult[0][4];}catch(ex){}; 
	}


}
