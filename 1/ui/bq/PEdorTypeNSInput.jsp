<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%

//得到界面的调用位置,默认为1,表示个人保单直接录入.
// 1 -- 个人投保单直接录入
// 2 -- 集体下个人投保单录入
// 3 -- 个人投保单明细查询
// 4 -- 集体下个人投保单明细查询
// 5 --
// 6 --

	String tLoadFlag = "";
	try
	{
		tLoadFlag = "2";
	}
	catch( Exception e1 )
	{
		tLoadFlag = "2";
	}
System.out.println("LoadFlag:" + tLoadFlag);
%>
<head >
<script>
	var loadFlag = "<%=tLoadFlag%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置.
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var uwFlag =  "<%=request.getParameter("uwFlag")%>";
	if(uwFlag ==null || uwFlag =="")
	{
	 uwFlag=0;
	}

  window.onfocus = f;

	var showInfo;
	function f() {
	  try { showInfo.focus(); } catch(e) {}
	  //initQuery();
	}
</script>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<SCRIPT src="PEdorTypeNSInput.js"></SCRIPT>
	<!--<SCRIPT src="../app/ProposalInput.js"></SCRIPT>-->
	<%@include file="PEdorTypeNSInit.jsp"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file = "ManageComLimit.jsp"%>

</head>

<body  onload="initForm(); " >
  <form action="./PEdorTypeNSSubmit.jsp" method=post name=fm target="fraSubmit">
  <!--  -->

	<table class=common>
	    <TR  class= common>
	      <TD  class= title > 批单号</TD>
	      <TD  class= input >
	        <input class="readonly" readonly name=EdorNo >
	      </TD>
	      <TD class = title > 批改类型 </TD>
	      <TD class = input >
	      	      	 <Input class=codeno readonly name=EdorType CodeData="0|^NS|新增附加险" ondblclick="return showCodeListEx('EdorType', [this,EdorTypeName],[0,1])"		onkeyup="return showCodeListKeyEx('EdorType', [this,EdorTypeName],[0,1])"><input class=codename name=EdorTypeName readonly=true>
	      </TD>

	      <TD class = title >保单号 </TD>
	      <TD class = input >
	      	<input class = "readonly" readonly name=ContNo>
	      </TD>
	    </TR>
	  </TABLE>
	  
	  <!--被保人列表-->
	  <table>
     <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsuredGrid);">
      </td>
      <td class= titleImg>
        被保人信息
      </td>
    </tr>
   </table>
    <Div  id= "divLCInsuredGrid" style= "display: ''">
        <table  class= common>
        	<tr  class= common>
          		<td text-align: left colSpan=1>
        			<span id="spanLCInsuredGrid" >
        			</span>
        	  	</td>
        	</tr>
        </table>
    </div>
	  <Div id= "divPage2" align="center" style= "display: 'none' ">
  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
  		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
  		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
  		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
	  </Div>
	
	<!--已投保险种信息-->
   <table>
     <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
      </td>
      <td class= titleImg>
        已投保险种信息
      </td>
    </tr>
   </table>
    <Div  id= "divPolGrid" style= "display: ''">
        <table  class= common>
        	<tr  class= common>
          		<td text-align: left colSpan=1>
        			<span id="spanPolGrid" >
        			</span>
        	  	</td>
        	</tr>
        </table>
    </div>
    <Div id= "divPage3" align="center" style= "display: 'none' ">
  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();"> 
  		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage();"> 					
  		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage();"> 
  		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage();"> 
	  </Div>

    <br>
    <!---------新增险列表-------------------------------->
    <table class = common>
			<TR class= common>
        <TD  class= input width="26%">
       		<Input class= cssButton type=Button value="添加险种" onclick="intoRiskInfo()" id='add'>
     	    <Input name="addImp" class= cssButton type=Button value="健康告知录入" onclick="addCustomerImpart()" id='addImp' >
     	  </TD>
     	</TR>
    </table>
    <table>
     <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPolAdd);">
      </td>
      <td class= titleImg>
        已新增的险种信息
      </td>
    </tr>
   </table>
    <Div  id= "divLCPolAdd" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
    					<span id="spanLCPolAddGrid" >
    					</span>
  			  	</td>
  			</tr>
    	</table>
  	</div>
  	<Div id= "divPage4" align="center" style= "display: 'none' ">
  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage4.firstPage();"> 
  		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage4.previousPage();"> 					
  		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage4.nextPage();"> 
  		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage4.lastPage();"> 
	  </Div>
    <br>

     <Div  id= "divSubmit" style= "display:''">
      <table class = common>
			<TR class= common>
         <TD  class= input width="26%">
       		<!--<Input class= cssButton type=Button value="添加险种" onclick="intoRiskInfo()" id='add'>
       		<Input class= cssButton type=Button value="修改附加险" onclick="modfiyRiskInfo()" id ='modify'>-->
       		<Input class= cssButton type=Button value="删除险种" onclick="delRecord();" id='del'>
       		 <Input class= cssbutton type=Button value="保存申请" onclick="edorSave();" id='save'>
       		 <Input class= cssbutton type=Button value="返  回" onclick="returnParent();">
     	 </TD>
     	 </TR>
     	</table>
     </Div>

   <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="InsuredNo" name="InsuredNo">
	 <input type=hidden id="PolNo" name="PolNo">
	  <input type=hidden id="Subtype" name="Subtype">
	  <input type=hidden id="RiskCode" name="RiskCode">
	  <input type=hidden id="MainPolNo" name="MainPolNo">
	  <input type=hidden id="CValiDate" name="CValiDate">
	  <input type=hidden id="fmAction" name="fmAction">
	   <input type=hidden id="SelPolNo" name="SelPolNo">



  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <span id="spanApprove"  style="display: none; position:relative; slategray"></span>
</body>
</html>


