//程序名称：LMInsuAccRate.js
//程序功能：万能险帐户结算利率录入
//创建日期：2007-12-12
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}

function beforeSubmit()
{
	return true;
}

//提交，保存按钮对应操作
function submitForm()
{
  if(!verifyInput2())
  {
    return false;
  }
  if(!checkInpBox())
  {
    return false;
  }
  if(!checkInsert())
  {
    return false;
  }
  if (beforeSubmit())
  {
    if (confirm("您确实想保存该记录吗?"))
    {	
      fm.fmtransact.value = "INSERT||MAIN" ;
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      fm.action = "LMInsuAccRateSave.jsp";
      fm.submit(); //提交
    }
    else
    {
      alert("您取消了保存操作！");
    }
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, transact)
{
  easyQueryClick();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if(!checkBalanced())
  {
    return false;
  }
  
  if(LMInsuAccRateGrid.getSelNo() == 0)
  {
    alert("请选择需要修改的记录，并重新做修改");
    return false;
  }
  
  if(!verifyInput2())
  {
    return false;
  }
  if(!checkInpBox())
  {
    return false;
  }
  if(!checkUpdate())
  {
    return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {	
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "UPDATE||MAIN";
    fm.action = "LMInsuAccRateSave.jsp";
    fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}   

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if(!checkBalanced())
  {
    return false;
  }
  
  if(LMInsuAccRateGrid.getSelNo() == 0)
  {
    alert("请选择需要删除的记录");
    return false;
  }
  
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "DELETE||MAIN";
    fm.action = "LMInsuAccRateSave.jsp";
    fm.submit(); //提交
    initInpBox();
  }
  else
  {
    alert("您取消了删除操作！");
  }
} 

//校验利率是否已结算过
function checkBalanced()
{
  var sql = "select 1 from LCInsureAccBalance a, LMInsuAccRate b "
          + "where a.DueBalaDate = b.BalaDate "
          + "    and b.InsuAccNo = '" + fm.InsuAccNo.value + "' "
          + "    and b.RateType = '" + fm.RateType.value + "' "
          + "    and b.RateIntv = " + fm.RateIntv.value 
          + "    and b.RateIntvUnit = '" + fm.RateIntvUnit.value + "' "
          + "    and (ltrim(rtrim(char(year(StartBalaDate)))) || '-' || ltrim(rtrim(char(month(StartBalaDate))))) = '" + fm.BalaMonth.value + "' ";
          + "fetch first 1 rows only ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    alert("该利率已结算过，不能执行本操作");
    return false;
  }
  return true;
}

function easyQueryClick() 
{
  var strSql = " select RiskCode, (select RiskName from LMRisk where RiskCode = a.RiskCode), "
               + "        InsuAccNo, (select InsuAccName from LMRiskToAcc where RiskCode = a.riskCode and InsuAccNo = a.InsuAccNo), "
               + "        ltrim(rtrim(char(year(StartBalaDate))))||'-'||ltrim(rtrim(char(month(StartBalaDate)))), "
               + "        RateType, (case RateType when 'S' then '单利' when 'C' then '复利' end), "
               + "        RateIntv, "
               + "        RateIntvUnit, (case RateIntvUnit when 'Y' then '年利率' when 'M' then '月利率' when 'D' then '日利率' end), "
               + "        Rate "
	           + " from LMInsuAccRate a "
	           + "     where 1 = 1 " ;
  if(fm.BalaMonth.value != null && fm.BalaMonth.value != "" && fm.BalaMonth.value != "null")
  {
    strSql = strSql + " and (ltrim(rtrim(char(year(a.StartBalaDate)))) || '-' || ltrim(rtrim(char(month(a.StartBalaDate))))) = '" + getYearMonth() + "' ";
  }
  if(fm.RateIntv.value != null && fm.RateIntv.value != "" && fm.RateIntv.value != "null")
  {
    strSql = strSql + " and RateIntv = " + fm.RateIntv.value;
  }
  if(fm.Rate.value != null && fm.Rate.value != "" && fm.Rate.value != "null")
  {
    strSql = strSql + " and Rate = " + fm.Rate.value;
  }
  strSql = strSql + getWherePart('RiskCode','RiskCode','=')
	              + getWherePart('InsuAccNo','InsuAccNo','=')
			      + getWherePart('RateType', 'RateType','=')
		          + getWherePart('RateIntvUnit', 'RateIntvUnit','=')
			      + "     order by RiskCode ,StartBalaDate ";
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(strSql, LMInsuAccRateGrid);
  return true;
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{ 
  if(!easyQueryClick())
  {
    return false;
  }
  if(LMInsuAccRateGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }
}

function selectOne() 
{
  var arrReturn = getQueryResult();
  afterQuery(arrReturn);	          		  
}

function getQueryResult()
{
  var arrSelected = null;
  tRow = LMInsuAccRateGrid.getSelNo();
  if( tRow == 0 || tRow == null )
  {
    return arrSelected;
  }
  arrSelected = new Array();
  //设置需要返回的数组
  arrSelected[0] = new Array();
  arrSelected[0] = LMInsuAccRateGrid.getRowData(tRow-1);
  return arrSelected;
}

function afterQuery( arrQueryResult )
{
  var arrResult = new Array();	
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.RiskCode.value = arrResult[0][0];
    fm.RiskName.value = arrResult[0][1];
    fm.InsuAccNo.value = arrResult[0][2];
    fm.InsuAccName.value = arrResult[0][3];
    fm.BalaMonth.value = arrResult[0][4];
    fm.RateType.value = arrResult[0][5];
    fm.RateTypeName.value = arrResult[0][6];
    fm.RateIntv.value = arrResult[0][7];
    fm.RateIntvUnit.value = arrResult[0][8];
    fm.RateIntvUnitName.value = arrResult[0][9];
    fm.Rate.value = arrResult[0][10]; 
    fm.RateConfirm.value = arrResult[0][10];
    
    fm.DRiskCode.value = arrResult[0][0];
    fm.DInsuAccNo.value = arrResult[0][2];
    fm.DBalaMonth.value = arrResult[0][4];
    fm.DRateType.value = arrResult[0][5];
    fm.DRateIntv.value = arrResult[0][7];
    fm.DRateIntvUnit.value = arrResult[0][8];
    fm.DRate.value = arrResult[0][10]; 
    showAllCodeName();
  }
}

//检查结算月份格式
function checkBalaMonth()
{
  var month = /^([12]\d{3})[-](([0][1-9])|([1-9])|(1[0-2]))$/
  if(!month.test(fm.BalaMonth.value))
  {
    alert("结算月份格式为 yyyy-mm 如:2007-12");
    return false;
  } 
    return true;
}

//根据险种编码显示第一个保险帐户号码
function shwoInsuAccNo()
{
  fm.all('InsuAccNo').value = "";
  fm.all('InsuAccName').value = "";
  var strSql = "select InsuAccNo, InsuAccName from LMRiskToAcc where RiskCode = '" + fm.RiskCode.value + "'";
  var arrReturn = easyExecSql(strSql);
  if(arrReturn != "null" && arrReturn != "" && arrReturn != null)
  {
    fm.all('InsuAccNo').value = arrReturn[0][0];
    fm.all('InsuAccName').value = arrReturn[0][1];
  }
  return true;
}

//检查利率
function checkRate()
{
  var rate1 = fm.Rate.value;
  var rate2 = fm.RateConfirm.value;
  if(rate1 != rate2)
  {
    alert("两次输入的利率不一致，请校对后重新输入！");
    return false;
  }
  else
  {
    if(rate1 < 1)
    {
      return true;
    }
    alert("输入的利率有误，请输入一个小于1的小数！");
    return false;
  }
  return true;
}

//检查各输入项是否符合要求
function checkInpBox()
{
  if(!checkBalaMonth())
  {
    return false;
  }
  if(!checkRate())
  {
    return false;
  }
  return true;
}

//保存时检查该条记录是否存在
function checkInsert()
{
  var sql = " select count(*) from LMInsuAccRate where RiskCode = '" + fm.RiskCode.value + "' " 
             + "     and InsuAccNo = '" + fm.InsuAccNo.value + "' "
             + "     and RateType = '" + fm.RateType.value + "' "
             + "     and RateIntv = " + fm.RateIntv.value 
             + "     and RateIntvUnit = '" + fm.RateIntvUnit.value + "' "
             + "     and (ltrim(rtrim(char(year(StartBalaDate)))) || '-' || ltrim(rtrim(char(month(StartBalaDate))))) = '" + fm.BalaMonth.value + "' ";
  var arrReturn = easyExecSql(sql);
  if (arrReturn != 0)
  {
    alert("该条记录已存在！");
    return false;
  }
  
  sql = " select count(*) from LMInsuAccRate where RiskCode = '" + fm.RiskCode.value + "' " 
      + "     and InsuAccNo = '" + fm.InsuAccNo.value + "' "
      + "     and RateType = '" + fm.RateType.value + "' "
      + "     and RateIntv = " + fm.RateIntv.value 
      + "     and RateIntvUnit = '" + fm.RateIntvUnit.value + "' ";
  arrReturn = easyExecSql(sql);
  if(arrReturn !=0)
  {
    var sql1 = " select count(*) from LMInsuAccRate where RiskCode = '" + fm.RiskCode.value + "' " 
             + "     and InsuAccNo = '" + fm.InsuAccNo.value + "' "
             + "     and RateType = '" + fm.RateType.value + "' "
             + "     and RateIntv = " + fm.RateIntv.value 
             + "     and RateIntvUnit = '" + fm.RateIntvUnit.value + "' "
             + "     and (ltrim(rtrim(char(year(StartBalaDate + 1 month)))) || '-' || ltrim(rtrim(char(month(StartBalaDate + 1 month))))) = '" + getYearMonth() + "' ";
    var arrReturn1 = easyExecSql(sql1);
    if (arrReturn1 == 0)
    {
      alert("请先录入上个月结算利率！");
      return false;
    }
  }
  return true;
}

//得到去掉无效0的年月,若原数据是2007-01,则返回2007-1
function getYearMonth()
{
  var tArray = (fm.BalaMonth.value).split("-");
  if(tArray.length != 2)
  {
    alert("请按格式录入结算月份");
    return false;
  }
  
  var tMonth = tArray[1];
  if(tMonth.length == 2 && tMonth.substring(0, 1) == "0")
  {
    tMonth = tMonth.substring(1, 2);
  }
  
  var tYearMonth = tArray[0] + "-" + tMonth;
  
  return tYearMonth;
}

//检查记录能否修改
function checkUpdate()
{
  if(fm.RiskCode.value != fm.DRiskCode.value)
  {
    alert("险种编码不能修改！");
    return false;
  }
  if(fm.InsuAccNo.value != fm.DInsuAccNo.value)
  {
    alert("保险帐户号码不能修改！");
    return false;
  }
  if(fm.BalaMonth.value != fm.DBalaMonth.value)
  {
    alert("结算月份不能修改！");
    return false;
  }
  if(fm.RateType.value != fm.DRateType.value)
  {
    alert("利率类型不能修改！");
    return false;
  }
  if(fm.RateIntv.value != fm.DRateIntv.value)
  {
    alert("利率间隔不能修改！");
    return false;
  }
  if(fm.RateIntvUnit.value != fm.DRateIntvUnit.value)
  {
    alert("利率间隔单位不能修改！");
    return false;
  }
  if(fm.Rate.value == fm.DRate.value)
  {
    alert("请修改利率后再保存！");
    return false;
  }
  return true;
}