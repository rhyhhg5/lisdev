var turnPage = new turnPageClass();      
var turnPage2 = new turnPageClass();      
var turnPage3 = new turnPageClass();  

//查询导入的数据
function queryImportData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, Money " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '1' " +
	             "order by Int(SerialNo) ";
	turnPage.queryModal(sql, InsuredListGrid); 
}

//查询导入无效的数据
function queryFailData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, Money, ErrorReason " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '0' " +
	             "order by Int(SerialNo) ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, FailGrid); 
}

//查询团体险种信息
function queryPolInfo()
{
  var groupMoney = "0";
  var groupFixMoney = "0";
  var sql = "select DetailType, EdorValue from LPEdorEspecialData " +
            "where EdorNo = '" + fm.all("EdorNo").value + "' " +
            "and EdorType ='" + fm.all("EdorType").value + "' ";
  var arrResult = easyExecSql(sql);
  if (arrResult)
  {
    for (i = 0; i < arrResult.length; i++)
    {
      var detailType = arrResult[i][0];
      var edorValue = arrResult[i][1];
      if (detailType == "GRPMONEY")
      {
        groupMoney = edorValue;
        break;
      }
      if (detailType == "GRPFIXMONEY")
      {
      	groupFixMoney = edorValue;
      	break;
      }
    }
  }
  sql = " select a.GrpPolNo, a.RiskSeqNo, a.RiskCode, b.RiskName, '" + groupMoney + "', '" + groupFixMoney + "' " +
        " from LCGrpPol a,LMRiskApp b" +
        " where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
        " and a.riskcode= b.riskcode "+
        " and a.riskcode in (select riskcode from lmriskapp where RiskType3 ='7' or RiskType8='6')"+
        " order by RiskSeqNo ";
  turnPage3.pageLineNum = 100;
  turnPage3.pageDivName = "divPage3";
  turnPage3.queryModal(sql, PolGrid);
}

//磁盘导入 
function importInsured()
{
	var url = "./BqDiskImportMain.jsp?EdorNo=" + fm.all("EdorNo").value + 
	          "&EdorType=" + fm.all("EdorType").value +
	          "&GrpContNo=" + fm.all("GrpContNo").value;
	var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
	window.open(url, "特需帐户资金导入", param);
}

//数据提交后的操作
function afterSubmit(flag, content)
{
  try 
  { 
    showInfo.close();
	  window.focus();
	}
	catch (ex) {}
	
	if (flag == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		returnParent();
	}
}

//保存
function save()
{
  //校验是否有录入金额
  if (PolGrid.mulLineCount == 1)
  {
    if ((fm.PolGrid5.value == 0) && (fm.PolGrid6.value == 0) && (InsuredListGrid.mulLineCount == 0))
    {
      alert("请录入团体账户或个人账户领取金额！");
      return false;
    }
  }
  else
  {
    var sum = 0;
    for (i = 0; i < PolGrid.mulLineCount; i++)
    {
      sum += fm.PolGrid5[i].value;
    }
    if ((sum == 0) && (InsuredListGrid.mulLineCount == 0))
    {
      alert("请录入团体账户或个人账户领取金额！");
      return false;
    }
  }
	if (FailGrid.mulLineCount > 0)
	{
		alert("有错误数据，请重新导入！");
		return false;
	}
  if (!PolGrid.checkValue())
  {
    return false;
  }
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
}

//返回父窗口
function returnParent()
{
	try
	{
		top.opener.getGrpEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {};
}