//程序功能：
//创建日期：2012/8/30
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus; 
var turnPage = new turnPageClass();

var RelaGridTurnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	 if(showInfo!=null)
	 {
		   try
		   {
		     showInfo.focus();  
		   }
		   catch(ex)
		   {
		     showInfo=null;
		   }
	 }
}
//删除
function lmRateDelete(){
	if(fm.bonusYear.value!=tYear){
		alert("无法修改、保存或删除不是今年公布的数据！");
		return false;
	}
	if(!checkAfterJuly()){
		alert("已经过了7月1日,无法再进行修改或删除");
		return false;
	}
	var arrReturn = new Array();
	var tSel = BonusInterestGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录，再点删除按钮。" );
		return false;
	}
	var result = new Array();
	result[0] = new Array();
	result[0] = BonusInterestGrid.getRowData(tSel-1);
		if(confirm("确定要删除所选记录吗？")){
			var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
			fm.fmtransact.value = "DELETE||MAIN";;
			fm.submit();
			//initRateBox();
		}else{
			alert("您取消了删除操作");
		}
}
//查询
function ldBonusInterestRateQuery(){
	// 书写SQL语句
	var strSQL = "select riskcode,bonusYear,appYear,HLInterestRate from LDBonusInterestRate where 1=1";
	
	if(fm.RiskCode.value != ""){
		strSQL = strSQL +" and RiskCode="+"'"+fm.RiskCode.value+"'";
	}
	if(fm.bonusYear.value != ""){
		strSQL = strSQL + " and bonusYear="+"'"+fm.bonusYear.value+"'";
	}
	if(fm.contYear.value != ""){
		strSQL = strSQL + " and appYear="+"'"+fm.contYear.value+"'";
	}
	if(fm.accRate.value != ""){
		var rateNum = parseFloat(fm.accRate.value);
		strSQL = strSQL + " and HLInterestRate="+rateNum;
		
	}
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    	BonusInterestGrid.clearData();
        alert("未查询到满足条件的数据！");
        return false;
    }
    turnPage.pageDivName="divturnPage";
    turnPage.queryModal(strSQL, BonusInterestGrid);
}
//年度校验
function  checkYear(obj){
window.alert(obj.getAttribute("value"));
var year = obj.getAttribute("value");
	if( /^\d{4}$/.test(year)){
		;
	}else{
		window.alert("年份应是四位数字");
	}
}
//累积生息利率校验
function checkRate(){
   // window.alert("hello-------");
	var num=document.getElementById("accRate").value;
	  //判断输入的是不是数字
	 if(num != "") {
	     if(/^(\+|-)?\d+($|\.\d+$)/.test(num)) {
	        var numInt=parseInt(num);
	        if(numInt < 0 || numInt > 1){
	           window.alert("累积生息利率必须是0到1之间的数字"); 
	           return false;  
	        }      
	     }else{
	          window.alert("您输入的不是数字！"); 
	          return false; 
	     } 
     }
	return true;
}
//修改
function ldBonusInterestRateUpdate(){
	if(fm.bonusYear.value!=tYear){
		alert("无法修改、保存或删除不是今年公布的数据！");
		return false;
	}
	if(!checkAfterJuly()){
		alert("已经过了7月1日,无法再进行修改或删除");
		return false;
	}

	var arrReturn = new Array();
	var tSel = BonusInterestGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录，再点修改按钮。" );
		return false;
	}
	var result = new Array();
	result[0] = new Array();
	result[0] = BonusInterestGrid.getRowData(tSel-1);
    if(!beforeSubmit()){
    	return false;
    }
	if (confirm("您确实想修改该记录吗?"))
	  {
	  var i = 0;
	  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
	  fm.fmtransact.value = "UPDATE||MAIN";
	  fm.submit(); //提交
	  }
	  else
	  {
	    alert("您取消了修改操作！");
	  }
}
//提交前的校验
function beforeSubmit(){
	var reg=/^\d*\.\d*$/;
	if(fm.RiskCode.value==""){
		alert("请输入险种");
		return false;
	}
	if(fm.bonusYear.value==""){
		alert("请输入红利年度");
		return false;
	}
	if(fm.contYear.value==""){
		alert("请输入保单承保年度");
		return false;
	}
	if(fm.contYearAfter.value==""){
		alert("请输入保单承保年度");
		return false;
	}
	if(fm.accRate.value==""){
		alert("请输入累积生息利率");
		return false;
	}
	if(fm.bonusYear.value<fm.contYearAfter.value){
		alert("保单承保年度不能大于利率公布年度!");
		return false;
	}
	if(fm.contYear.value>fm.contYearAfter.value){
		alert("利率公布年度起期不能大于利率公布年度止期!");
		return false;
	}
	return true;
}
//保存
function ldBonusInterestRateSave(){
	var fmyear=fm.bonusYear.value;
	if(fmyear!=tYear){
		alert("无法修改、保存或删除不是今年公布的数据！");
		return false;
	}
	if(!beforeSubmit()){
		return false;
	}
	if(confirm("确认保存吗？")){
	
		fm.fmtransact.value="INSERT||MAIN";
		var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.action = "LDBonusInterestRateSave.jsp";
		fm.submit();
	}else{
		alert("取消保存！");
	}
	
}

//查询
function queryAgain(){

	var strSQL = "select riskcode,bonusYear,appYear,HLInterestRate from LDBonusInterestRate where 1=1 and bonusyear='"+tYear+"'";

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    	BonusInterestGrid.clearData();
        alert("未查询到满足条件的数据！");
        return false;
    }
    turnPage.pageDivName="divturnPage";
    turnPage.queryModal(strSQL, BonusInterestGrid);
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr,content,transact){

	if (transact == "DELETE||MAIN")
	{
	    if (FlagStr != "Fail" )
	    {
	        content = "删除成功";
	    }
	}
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	queryAgain();
 
}
function checkAfterJuly(){
	 var sql = "select 1 from dual where month(current date)>=7 with ur";
	  if(easyExecSql(sql)==null){
		  return true;
	  }else{
		  return false;
	  }
}

//将字符串日期转换成日期格式
function strToDate(str)
{
  var arys= new Array();
  arys=str.split('-');
  var newDate=new Date(arys[0],arys[1],arys[2]);  
  return newDate;
}  
//当选择一条记录时，在页面显示其具体信息
function ShowDetail(){
	var arrReturn = getSelectedResult();
  	afterSelected(arrReturn);
}
function getSelectedResult(){
	var arrSelected = null;
	tRow =BonusInterestGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	{
	  return arrSelected;
	}
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = BonusInterestGrid.getRowData(tRow-1);
	return arrSelected;
}	
function afterSelected(arrSelectedResult){
	
	var arrResult = new Array();	
	if(arrSelectedResult!= null )
	{
	  arrResult = arrSelectedResult;
	  fm.RiskCode.value = arrResult[0][0];
	  fm.bonusYear.value = arrResult[0][1];
	  fm.contYear.value = arrResult[0][2];
	  fm.contYearAfter.value = arrResult[0][2];
	  fm.accRate.value = arrResult[0][3];
	  
	  var sql = "select riskcode,riskname from lmriskapp where riskcode = '"+fm.RiskCode.value+"' with ur";
	  var riskname = easyExecSql(sql);
	  if(fm.RiskCode.value!=""){
	  	fm.RiskName.value = riskname[0][1];
	  }
	  document.getElementById("RiskCode").readOnly=true;
	  document.getElementById("bonusYear").readOnly=true;
	  document.getElementById("contYear").readOnly=true;
	  document.getElementById("contYearAfter").readOnly=true;
	}
	
}