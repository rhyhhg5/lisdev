
<%
	//GEdorTypeXTInit.jsp
	//程序功能：
	//创建日期：2005.3.31 20:00:00
	//创建人  ：LHS
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
	//添加页面控件的初始化。
%>

<script language="JavaScript">
	//单击时查询
	function reportDetailClick(parm1, parm2) {
		var ex, ey;
		ex = window.event.clientX + document.body.scrollLeft; //得到事件的坐标x
		ey = window.event.clientY + document.body.scrollTop; //得到事件的坐标y
		divLPAppntGrpDetail.style.left = ex;
		divLPAppntGrpDetail.style.top = ey;
		detailQueryClick();
	}
	function initInpBox() {
		try {
			//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
			var flag;
			try {
				flag = top.opener.fm.all('loadFlagForItem').value;
			} catch (ex) {
				flag = "";
			}

			if (flag == "TASK") {
				fm.save.style.display = "none";
				fm.goBack.style.display = "none";
				fm.cancel.style.display = "none";
			}

			fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
			fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
			fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
			fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
			fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
			fm.all('ContType').value = '2';
			showOneCodeName("EdorCode", "EdorTypeName");
		} catch (ex) {
			alert("在GEdorTypeXTInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}

	function initSelBox() {
		try {
		} catch (ex) {
			alert("在GEdorTypeXTInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
		}
	}

	function initForm() {
		try {
			initInpBox();
			initSelBox();
			initQuery(); 

			initGrpAccGrid();
			initPAccGrid();
			queryLCGrpPolGrid();

			//initElementtype();
		} catch (re) {
			alert("GEdorTypeXTInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}

	function initQuery() {
		try {
			getGrpCont();
			getEdorItemInfo();
		} catch (re) {
			alert("GEdorTypeXTInit.jsp-->getGrpCont函数中发生异常:初始化界面错误!");
		}

	}

	function initGrpAccGrid() {
		var iArray = new Array();

		try {
			iArray[0] = new Array();
			iArray[0][0] = "险种序号"; //列名
			iArray[0][1] = "50px"; //列宽
			iArray[0][2] = 100; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[0][21] = "RiskSeqNo";

			iArray[1] = new Array();
			iArray[1][0] = "险种代码"; //列名
			iArray[1][1] = "50px"; //列宽
			iArray[1][2] = 100; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[1][21] = "RiskCode";

			iArray[2] = new Array();
			iArray[2][0] = "险种名称"; //列名
			iArray[2][1] = "100px"; //列宽
			iArray[2][2] = 100; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[2][21] = "RiskName";

			iArray[3] = new Array();
			iArray[3][0] = "公共账户余额"; //列名
			iArray[3][1] = "50px"; //列宽
			iArray[3][2] = 100; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[3][21] = "SumDuePayMoney";

			iArray[4] = new Array();
			iArray[4][0] = "正常解约退费"; //列名
			iArray[4][1] = "50px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[4][21] = "CTMoney";

			iArray[5] = new Array();
			iArray[5][0] = "协议解约金额"; //列名
			iArray[5][1] = "50px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 1; //是否允许输入,1表示允许，0表示不允许
			iArray[5][21] = "XTMoney";

			GrpAccGrid = new MulLineEnter("fm", "GrpAccGrid");
			//这些属性必须在loadMulLine前
			GrpAccGrid.mulLineCount = 0;
			GrpAccGrid.displayTitle = 1;
			GrpAccGrid.locked = 1;
			GrpAccGrid.canSel = 0;
			GrpAccGrid.canChk = 0;
			GrpAccGrid.hiddenSubtraction = 1;
			GrpAccGrid.hiddenPlus = 1;
			GrpAccGrid.loadMulLine(iArray);
		} catch (ex) {
			alert(ex);
		}
	}

	function initPAccGrid() {
		var iArray = new Array();

		try {
			iArray[0] = new Array();
			iArray[0][0] = "险种序号"; //列名
			iArray[0][1] = "50px"; //列宽
			iArray[0][2] = 100; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[0][21] = "RiskSeqNo";

			iArray[1] = new Array();
			iArray[1][0] = "险种代码"; //列名
			iArray[1][1] = "50px"; //列宽
			iArray[1][2] = 100; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[1][21] = "RiskCode";

			iArray[2] = new Array();
			iArray[2][0] = "险种名称"; //列名
			iArray[2][1] = "150px"; //列宽
			iArray[2][2] = 100; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[2][21] = "RiskName";

			iArray[3] = new Array();
			iArray[3][0] = "账户类别"; //列名
			iArray[3][1] = "80px"; //列宽
			iArray[3][2] = 100; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[3][21] = "AccType";

			iArray[4] = new Array();
			iArray[4][0] = "被保险人人数"; //列名
			iArray[4][1] = "50px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[4][21] = "Peoples";

			iArray[5] = new Array();
			iArray[5][0] = "账户价值合计"; //列名
			iArray[5][1] = "50px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 1; //是否允许输入,1表示允许，0表示不允许
			iArray[5][21] = "SumAccBala";

			iArray[6] = new Array();
			iArray[6][0] = "正常解约金额"; //列名
			iArray[6][1] = "50px"; //列宽
			iArray[6][2] = 200; //列最大值
			iArray[6][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[6][21] = "CTMoney";

			iArray[7] = new Array();
			iArray[7][0] = "协议退保金额"; //列名
			iArray[7][1] = "50px"; //列宽
			iArray[7][2] = 200; //列最大值
			iArray[7][3] = 1; //是否允许输入,1表示允许，0表示不允许
			iArray[7][21] = "XTMoney";

			iArray[8] = new Array();
			iArray[8][0] = "账户号码"; //列名
			iArray[8][1] = "0px"; //列宽
			iArray[8][2] = 100; //列最大值
			iArray[8][3] = 0; //是否允许输入,1表示允许，0表示不允许
			iArray[8][21] = "AccNo";

			PAccGrid = new MulLineEnter("fm", "PAccGrid");
			//这些属性必须在loadMulLine前
			PAccGrid.mulLineCount = 0;
			PAccGrid.displayTitle = 1;
			PAccGrid.locked = 1;
			PAccGrid.canSel = 0;
			PAccGrid.canChk = 0;
			PAccGrid.hiddenSubtraction = 1;
			PAccGrid.hiddenPlus = 1;
			PAccGrid.loadMulLine(iArray);
		} catch (ex) {
			alert(ex);
		}
	}
</script>