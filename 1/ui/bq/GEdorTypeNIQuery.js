//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

// 数据返回父窗口
function returnMain()
{
	top.close();
}

/*********************************************************************
 *  显示投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showPolDetail()
{
	var tSel = GrpPolGrid.getSelNo();
	var cPolNo = "";
	if( tSel != null && tSel != 0 )
		cPolNo = GrpPolGrid.getRowColData( tSel - 1, 1 );

	if( cPolNo == null || cPolNo == "" )
		alert("请选择一张增人保单后，再进行查询增人保单明细操作");
	else
	{
		mSwitch.deleteVar( "PolNo" );
		mSwitch.addVar( "PolNo", "", cPolNo );
		
		window.open("./GEdorTypeNI.jsp?LoadFlag=4");
	}
}           

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select PolNo,PrtNo,RiskCode,InsuredName,Prem,Amnt from LPPol where 1=1 "
				 + "and AppFlag='0' "
				 + "and GrpPolNo='" + GrpPolNo + "' and EdorNo='"+EdorNo+"' and EdorType='"+EdorType+"'";
//alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpPolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}
