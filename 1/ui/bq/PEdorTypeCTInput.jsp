<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="./PEdor.js"></SCRIPT>
	<SCRIPT src="./PEdorTypeCT.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="PEdorTypeCTInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="./PEdorTypeCTSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
    	<tr class= common> 
        <td class="title"> 受理号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="EdorNo" >
        </td>
        <td class="title"> 批改类型 </td>
        <td class="input">
        	<input class="readonly" type="hidden" readonly name="EdorType">
        	<input class="readonly" readonly name="EdorTypeName">
        </td>
        <td class="title"> 保单号 </td>
        <td class="input">
        	<input class = "readonly" readonly name="ContNo">
        </td>   
    	</tr>
    </table> 
		<%@include file="ULICommon.jsp"%> 
    <Div id= "divPolInfo" style= "display: ''">
			<table>
			  <tr>
		      <td>
		      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
		      </td>
		      <td class= titleImg>
		         保单险种信息
		      </td>
			  </tr>
			</table>
	    <Div  id= "divPolGrid" style= "display: ''">
				<table  class= common>
					<tr  class= common>
			  		<td text-align: left colSpan=1>
						<span id="spanPolGrid" >
						</span> 
				  	</td>
					</tr>
				</table>					
	    </div>
    </DIV>
    <br>
        <Div id= "divLoanInfo" style= "display: 'none'">
			<table>
			  <tr>
		      <td class= titleImg>
		         保单贷款信息
		      </td>
			  </tr>
			</table>
	   <table class=common>
    	<tr class= common> 
        <td class="title"> 险种号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="PolNo" >
        </td>
        <td class="title"> 贷款起始日期 </td>
        <td class="input">
        	<input class="readonly" readonly name="LoanDate">
        </td>
        <td class="title"> 贷款金额 </td>
        <td class="input">
        	<input class = "readonly" readonly name="LoanMoney">
        </td>   
    	</tr>
    </table> 
    </DIV>
    <br>
    <table  class= common>
      <TR class= common>
        <TD class= title> 退保原因 </TD>
        <TD class= input>
		      <input class="codeNo" name="reason_tb" readOnly verify="退保原因|notnull&code:reason_tb&len<=10" ondblclick="showCodeList('reason_tb',[this,reason_tbName],[0,1]);" onkeyup="showCodeListKey('reason_tb',[this,reason_tbName],[0,1]);"><Input class="codeName" name=reason_tbName readonly elementtype=nacessary>
        </TD>
        <TD  class= title> 退保日期 </TD>
        <TD class=input>
          <Input class= "readonly" name=EndDate readonly >
        </TD> 
      </TR>
    </table> 
    <br>
    <Div id= "divSYInfo" style= "display: 'none'">
		<table class=common>
        <td class="title"> 补交税收优惠额度： </td>
        <td class="input">
        	<input class = "text"  name="TaxMoney">
       </table> 
    </DIV>
    <br>
        <Div id= "divPayInfo" style= "display: 'none'">
			<table>
			  <tr>
		      <td>
		      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
		      </td>
		      <td class= titleImg>
		         缴费资料变更
		      </td>
			  </tr>
			</table>
			</Div>
    <Div  id="DivPayMode" style= "display: 'none'"> 
        <table  class= common>
		<TR class= common>
			<TD class= title> 转帐银行 </TD>
			<TD class= input>
			    <input NAME=BankCode VALUE="" CLASS="codeNo" MAXLENGTH=20 ondblclick=" return showCodeList('bankcode',[this,BankCodeName],[0,1],null,fm.PayMode.value,fm.ManageCom.value,1);" onkeyup="return showCodeListKey('bankcode',[this,BankCodeName],[0,1],null,fm.PayMode.value,fm.ManageCom.value,1);" ><Input class= codeName name="BankCodeName" elementtype=nacessary readonly >
			</TD>	
			<TD class= title> 转账帐号 </TD>  
			<TD class= input>
			  <Input class= "common" name=BankAccNo verify="转账帐号|int">
			</TD>		      
			<TD class= title> 账户名 </TD>   
			<TD class= input>
			  <Input class= "common" name=AccName >
			</TD>		       
       </TR>
       </table> 
     </div>
<br>

    <Div>
	   <Input type=button name="save" class = cssButton value="保  存" onclick="edorTypeCTSave()">
		 <Input  type=Button name="goBack" class = cssButton value="返  回" onclick="edorTypeCTReturn()">
		</Div>

		 <input type=hidden id="fmtransact" name="fmtransact">
		 <input type=hidden id="ContType" name="ContType">
		 <input type=hidden id="PayMode" name="PayMode">
		 <input type=hidden id="ManageCom" name="ManageCom">
		 <input type=hidden name="EdorAcceptNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
