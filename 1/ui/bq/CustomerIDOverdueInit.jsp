<%
//程序名称：CustomerIDOverdueInit.jsp
//程序功能：
//创建日期：2018-02-26 
//创建人  ：fengzhihang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
%> 

<script language="JavaScript">

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";

//表单初始化
function initForm()
{
	initInputBox();
	//初始化Grid
  	initIDOverdueGrid();
  	initElementtype();
}

//符合条件的列表展示
function initIDOverdueGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="保单号";       			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[1][1]="120px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="保单状态";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[2][1]="70px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="客户号";	   				//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[3][1]="80px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="客户姓名";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[4][1]="80px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="客户性质";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[5][1]="70px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[6]=new Array();
		iArray[6][0]="管理机构";	   				//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[6][1]="150px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[7]=new Array();
		iArray[7][0]="证件类型";			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[7][1]="60px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[8]=new Array();
		iArray[8][0]="证件号码";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[8][1]="150px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[9]=new Array();
		iArray[9][0]="证件有效期起期";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[9][1]="90px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[10]=new Array();
		iArray[10][0]="证件有效期止期";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[10][1]="90px";            		//列宽
		iArray[10][2]=100;            			//列最大值
		iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[11]=new Array();
		iArray[11][0]="业务员代号";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[11][1]="80px";            		//列宽
		iArray[11][2]=100;            			//列最大值
		iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[12]=new Array();
		iArray[12][0]="业务员姓名";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[12][1]="80px";            		//列宽
		iArray[12][2]=100;            			//列最大值
		iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[13]=new Array();
		iArray[13][0]="业务员联系电话";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[13][1]="100px";            		//列宽
		iArray[13][2]=100;            			//列最大值
		iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许


		
		IDOverdueGrid = new MulLineEnter( "fm" , "IDOverdueGrid" );
		//这些属性必须在loadMulLine前
		IDOverdueGrid.canChk =1 //显示复选框
		IDOverdueGrid.mulLineCount = 0;
		IDOverdueGrid.displayTitle = 1;
		IDOverdueGrid.locked = 1;
		IDOverdueGrid.hiddenPlus=1;
		IDOverdueGrid.hiddenSubtraction=1;
		IDOverdueGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert("CustomerIDOverdueInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}   
}
function initInputBox()
{
  try
	{
		fm.all('ManageCom').value = manageCom;
		var sql1 = "select Name from LDCom where ComCode = '" + manageCom + "'";
		fm.all('ManageComName').value = easyExecSql(sql1);
		showAllCodeName();
	}
	catch(ex)
	{
		alert("CustomerIDOverdueInit.jsp-->initInputBox函数中发生异常:初始化界面错误!");
	}      
}
</script>