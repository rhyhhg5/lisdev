<%
//程序名称：PEdorInputInit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
//              Yang Yaliln 20051019 支持按险种退保
%>
<!--用户校验类-->
                      

<script language="JavaScript">  
  var cancelCount = 0;
  
function initInpBox()
{
    try
    {        
    	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
    	var flag;
    	try
    	{  		
    		flag = top.opener.fm.all('loadFlagForItem').value;
    	}
    	catch(ex)
    	{
    		flag = "";	
    	}
    	
    	if(flag == "TASK")
  	 	{
    		fm.save1.style.display = "none";
    		fm.save2.style.display = "none";
    		fm.save3.style.display = "none";
    		fm.save.style.display = "none";
    		fm.goBack.style.display = "none";
    	
    	}

        fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
        fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
        fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
        fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
        fm.all('ContType').value = top.opener.fm.all('ContType').value;
        
	    showOneCodeName("EdorCode", "EdorTypeName");
    }
    catch(ex)
    {
        alert("在GEdorTypeWTInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }      
}

function initForm()
{
  try
  {
    initInpBox();
    initQuery();
    initLCGrpPolGrid();
    queryLCGrpPolGrid();
    checkSelectContPlanRisk();
    initTestGrid();
    getTestGrid();
    setLPGrpEdoItemInfo();
      
    if(cancelCount == 0)
    {
      initElementtype();
      cancelCount = cancelCount + 1;
    }
  }
  catch(re)
  {
    alert("PEdorTypeWTInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initQuery()
{	
	getGrpCont();
}

function initLCGrpPolGrid()
{
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保障计划";         		//列名
    iArray[1][1]="30px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21] = "contPlanCode";
    
    iArray[2]=new Array();
    iArray[2][0]="险种序号";         		//列名
    iArray[2][1]="30px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21] = "riskSeqNo";
    
    iArray[3]=new Array();
    iArray[3][0]="险种代码";         		//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21] = "riskCode";
    
    iArray[4]=new Array();
    iArray[4][0]="险种名称";         		//列名
    iArray[4][1]="150px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21] = "riskName";
    
    iArray[5]=new Array();
    iArray[5][0]="被保人数";         		//列名
    iArray[5][1]="30px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21] = "insurdCount";
    
    iArray[6]=new Array();
    iArray[6][0]="总保费";         		//列名
    iArray[6][1]="40px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21] = "prem";
    
    iArray[7]=new Array();
    iArray[7][0]="集体投保单号";         		//列名
    iArray[7][1]="20px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21] = "proposalGrpContNo";
    
    iArray[8]=new Array();
    iArray[8][0]="主险险种编码";         		//列名
    iArray[8][1]="20px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21] = "mainRiskCode";
    
    iArray[9]=new Array();
    iArray[9][0]="计划类别";         		//列名
    iArray[9][1]="20px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[9][21] = "planType";
    
    iArray[10]=new Array();
    iArray[10][0]="生效日期";         		//列名
    iArray[10][1]="60px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[10][21] = "cValiDate";

         
    LCGrpPolGrid = new MulLineEnter( "fm" , "LCGrpPolGrid" ); 
    //这些属性必须在loadMulLine前
    LCGrpPolGrid.mulLineCount = 0;
    LCGrpPolGrid.displayTitle = 1;
    LCGrpPolGrid.locked = 1;
    LCGrpPolGrid.canSel = 0;
    LCGrpPolGrid.canChk = 1;
    LCGrpPolGrid.hiddenSubtraction = 1;
    LCGrpPolGrid.hiddenPlus = 1;
    LCGrpPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}

//初始化体检信息
function initTestGrid()
{                               
    var iArray = new Array();
             
      try    
      {      
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="总单投保单号码";
        iArray[1][1]="0";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="打印流水号";
        iArray[2][1]="0";
        iArray[2][2]=100;
        iArray[2][3]=0;
             
        iArray[3]=new Array();
        iArray[3][0]="被保人号码";
        iArray[3][1]="60";
        iArray[3][2]=100;
        iArray[3][3]=0;
             
        iArray[4]=new Array();
        iArray[4][0]="被保人";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
             
        iArray[5]=new Array();
        iArray[5][0]="体检费";
        iArray[5][1]="60px";
        iArray[5][2]=100;
        iArray[5][3]=0;
             
        iArray[6]=new Array();
        iArray[6][0]="";
        iArray[6][1]="50px";
        iArray[6][2]=100;
        iArray[6][3]=3;        
             
        iArray[7]=new Array();
        iArray[7][0]="";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=3;
             
        TestGrid = new MulLineEnter( "fm" , "TestGrid" ); 
        //这些属性必须在loadMulLine前
        TestGrid.mulLineCount = 0;   
        TestGrid.displayTitle = 1;      
        TestGrid.canChk = 1;                                                                  
        TestGrid.canSel = 0;
        TestGrid.hiddenPlus = 1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
        TestGrid.hiddenSubtraction = 1;
        TestGrid.loadMulLine(iArray);
      }      
      catch(ex)
      {      
        alert(ex);
      }      
}  

</script>