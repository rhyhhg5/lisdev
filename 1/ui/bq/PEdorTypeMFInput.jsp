<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2016-11-23 16:20
//创建人  ：LC
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<head >
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeMF.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeMFInit.jsp"%> 
</head>

<body onload="initForm();initElementtype();">
	<form action="./PEdorTypeMFSubmit.jsp" method=post name=fm target="fraSubmit">    
		<table class=common>
			<TR  class= common> 
			  <TD  class= title > 批单号</TD>
			  <TD  class= input > 
			    <input class="readonly" readonly name=EdorNo >
			  </TD>
			  <TD class = title > 批改类型 </TD>
			  <TD class = input >
			  	<input class = "readonly" readonly name=EdorType type=hidden>
			  	<input class = "readonly" readonly name=EdorTypeName>
			  </TD>
			  <TD class = title > 个人保单号 </TD>
			  <TD class = input >
			  	<input class = "readonly" readonly name=ContNo>
			  </TD>   
			</TR>
		</TABLE> 


		
    	<Div  id= "divPolInfo" style= "display: ''">
    	    <table>
    	        <tr>
    	            <td>
    	                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
    	            </td>
    	            <td class= titleImg>
    	                险种信息
    	            </td>
    	        </tr>
    	    </table>
    		<Div  id= "divPolGrid" style= "display: ''">
        		<table  class= common>
        			<tr  class= common>
        		  		<td text-align: left colSpan=1>
        					<span id="spanPolGrid" >
        					</span> 
        			  	</td>
        			</tr>
 				<Div id= "divPage2" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();	"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
				</Div>
        		</table>					
    		</Div>
    	</DIV>

    	    <table>
    	        <tr>
    	            <td class = title>
    	                复效生效日
    	            </td>
          <TD  class= input colspan="4">
              <Input class= "coolDatePicker" dateFormat="short" name=EdorAppDate onfocus="showTips();" >
          </TD> 
          				<TD>
          					<Input class='cssButton' type=Button value="健康告知录入" onclick="healthInput()">
          				</TD>
    	        </tr>
    	    </table>
    	</DIV>

	 <div id="ErrorsInfo" style="color: red;"></div> 
    	<table class =common>
			<TR  class= common> 
			  <TD class = title >  </TD>
			  <TD class = input >
				  
			  </TD>
			  <TD class = title >  </TD>
			  <TD class = input >
			  </TD>   
			</TR>
			<TR class= common>
     	 		<TD > 
					<Input type=button name=save class = cssButton value="保  存" onclick="edorTypeMFSave()">
       		<Input  type=Button name=goBack class = cssButton value="返  回" onclick="edorTypeMFReturn()">
     	 		</TD>
     	 	</TR>
     	</table>
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden id="ContType" name="ContType">
			<input type=hidden name="EdorAcceptNo">
			<input type=hidden name="rates">
			<div id="test"></div>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
