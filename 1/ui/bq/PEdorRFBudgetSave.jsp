<%@page import="java.util.Arrays"%>
<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<% 
//程序名称：PEdorRFBudgetSave.jsp
//程序功能：还款试算
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  System.out.println("开始试算");
  String flag = "Succ";
  String content = "还款试算成功";
  String remark = "无";
  long startTime = System.currentTimeMillis();
  
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String contNo = request.getParameter("contNo");
  String edorType = request.getParameter("edorType");//空
  String edorNo = request.getParameter("edorNo");//空
  boolean needResultFlag = (edorNo != null && !edorNo.equals("") 
                            && edorType != null && !edorType.equals(""));//false
  System.out.println("\n\n\n\n\n\n" + needResultFlag);
  String edorValiDate = request.getParameter("RFDate");//选择预计还款时间
  System.out.println("预计还款日期："+edorValiDate);
  EdorRFBudgetBL tEdorRFBudgetBL = new EdorRFBudgetBL();//用来构建保全项目lpedoritem
  tEdorRFBudgetBL.setEdorValiDate(edorValiDate);
  tEdorRFBudgetBL.setNeedBugetResultFlag(needResultFlag);
  tEdorRFBudgetBL.setOperator(tGI.Operator);
  LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
  
  String[] tChecks = request.getParameterValues("InpLCPolGridChk");
  System.out.println("这个到底是什么："+Arrays.toString(tChecks));
  String[] tPolNos = request.getParameterValues("LCPolGrid1");
  System.out.println("这个到底是什么："+Arrays.toString(tPolNos));
  if(tChecks != null)
  {
  
    LPBudgetResultSet tLPBudgetResultSet = new LPBudgetResultSet();
    for(int i = 0; i < tChecks.length; i++)
    {
      if (!tChecks[i].equals("1"))
      {
        continue;
      }
      double rfBFMoney = 0;  
      double rfLXMoney = 0;
      double rfAllMoney = 0;
    //调用核心退费计算
      LJSGetEndorseSet tLJSGetEndorseSet
        = tEdorRFBudgetBL.BudgetOnePol(tPolNos[i]);
      System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
      System.out.println(tEdorRFBudgetBL.mErrors.needDealError());
      System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
      if(tEdorRFBudgetBL.mErrors.needDealError())
      {
        flag = "Fail";
        content = tEdorRFBudgetBL.mErrors.getErrContent();
        System.out.println("rfAllMoney:"+rfAllMoney);
        break;
      }

      if(tLJSGetEndorseSet != null)
      {
    	for(int k = 1;k <= tLJSGetEndorseSet.size();k++){
    		if((tLJSGetEndorseSet.get(k).getFeeFinaType()).equals("RFBF")){
    			rfBFMoney += tLJSGetEndorseSet.get(k).getGetMoney();
    		}else if((tLJSGetEndorseSet.get(k).getFeeFinaType()).equals("RFLX")){
    			rfLXMoney += tLJSGetEndorseSet.get(k).getGetMoney();
    		}
    	}
    	rfAllMoney += rfBFMoney+rfLXMoney;
    	rfAllMoney = PubFun.setPrecision(rfAllMoney, "0.00");
        LPBudgetResultSchema tLPBudgetResultSchema = tEdorRFBudgetBL.getOnePolLPBudgetResult();
        tLPBudgetResultSet.add(tLPBudgetResultSchema);
      }

%>      
<script language="javascript">
      parent.fraInterface.LCPolGrid.setRowColDataByName("<%=i%>", "rfMoney", "<%=rfAllMoney%>");
      parent.fraInterface.LCPolGrid.setRowColDataByName("<%=i%>", "remark", "<%=remark%>");      
</script>
<%      
      if(flag.equals("Fail"))
      {
        break;
      }
    }
  }
  System.out.println(System.currentTimeMillis() - startTime);
  content = PubFun.changForHTML(content);
  System.out.println("成功标志："+flag);
  System.out.println("content："+content);
  
%>                                       
<html>
<script language="javascript">
	
	parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>

