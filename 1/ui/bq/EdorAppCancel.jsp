<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="EdorAppCancel.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="EdorAppCancelInit.jsp"%>
  <title>保全查询 </title>
</head>
<body  onload="initForm();">
  <form action="./EdorAppCancelSubmit.jsp" method=post name=fm target="_blank" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入个人保全查询条件：
			</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            批单号码
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
          </TD>
          <TD  class= title>
            个人保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            批改类型
          </TD>
          <TD  class= input>
            <Input class=common name=EdorType >
          </TD>          
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            被保人客户号码
          </TD>
          <TD  class= input>
            <Input class=common name=InsuredNo >
          </TD>
          <TD  class= title>
            申请日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EdorAppDate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 保全信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		<TR>	
       <input type=hidden name=Transact >
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">	  			
  		</TR>	
    	</table>

      
   	<table class= common align=center>
    		<tr class= common>
    			<td  class= input>
    				<INPUT class= common VALUE="申请撤销" TYPE=button onclick="PrtEdor();">     			
    			</td>
    			<td>
    			</td>
    		</tr>
    	</table>
       			
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
