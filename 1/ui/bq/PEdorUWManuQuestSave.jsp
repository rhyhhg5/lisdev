<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QuestInputChk.jsp
//程序功能：问题件录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
<%
		String flag = "Fail";
		String content = "";
		
		GlobalInput gi =(GlobalInput)session.getValue("GI");
		String operate = request.getParameter("fmtransact");

		LPIssuePolSchema tLPIssuePolSchema = new LPIssuePolSchema();
		tLPIssuePolSchema.setEdorNo(request.getParameter("EdorNo"));
		tLPIssuePolSchema.setContNo(request.getParameter("ContNo"));
		tLPIssuePolSchema.setSerialNo(request.getParameter("SerialNo"));
		tLPIssuePolSchema.setIssueCont(request.getParameter("Content"));
		tLPIssuePolSchema.setOperatePos(request.getParameter("Flag"));
		tLPIssuePolSchema.setBackObjType(request.getParameter("BackObj"));
		tLPIssuePolSchema.setQuestionObj(request.getParameter("QuestionObj"));
		tLPIssuePolSchema.setIssueType(request.getParameter("Quest"));
		tLPIssuePolSchema.setErrField(request.getParameter("QuestionField"));
		tLPIssuePolSchema.setErrFieldName(request.getParameter("QuestionFieldValue"));
		tLPIssuePolSchema.setErrContent(request.getParameter("OldFieldValue"));
		
		VData data = new VData();
		data.add(gi);
		data.add(tLPIssuePolSchema);
		PEdorUWManuQuestUI tPEdorUWManuQuestUI = new PEdorUWManuQuestUI();
		if (!tPEdorUWManuQuestUI.submitData(data, operate))
		{
		  flag = "Fail";
		  content = "数据保存失败！原因是：" + tPEdorUWManuQuestUI.getError();
		}
		else
		{
		  flag = "Succ";
		  content = "数据保存成功！";
		  if (tPEdorUWManuQuestUI.getPrtSeq() != null)
		  {
		  	content += "问题件通知书号为" + tPEdorUWManuQuestUI.getPrtSeq();
		  }
		}
		content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>
