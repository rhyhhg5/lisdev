<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
  GlobalInput tGI = (GlobalInput)session.getValue("GI");	
  if(tGI == null) 
  {
    out.println("session has expired");
    return;
  }
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>

<script language="JavaScript">
function initInpBox()
{ 
  try
  {  
    var sql = "select current date - 1 month,current date from dual ";
    var rs = easyExecSql(sql);
    fm.all('StartDate').value = rs[0][0];
    fm.all('EndDate').value = rs[0][1];
    fm.all('ManageCom').value = <%=tGI.ManageCom%>;
    fm.all('RiskCode').value = '';
  }
  catch(ex)
  {
    alert("在OmPayBalaNewInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("OmPayBalaNewInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>