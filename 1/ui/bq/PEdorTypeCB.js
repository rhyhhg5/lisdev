//               该文件中包含客户端需要处理的函数和事件
var showInfo2;
var showInfo1;
var mDebug="1";

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage3 = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorReturn()
{
	try
	{
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {}
}

function getPolInfo(tContNo)
{
    var strSQL ="select InsuredNo,InsuredName,PolNo,RiskCode,Prem,Amnt,CValiDate,contno,grpcontno, to_char(LCPol.mult) from LCPol where ContNo='"+tContNo+"'";
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    for(var i = 0; i < PolGrid.mulLineCount; i++)
    {
    	if(PolGrid.getRowColData(i, 10) == "0")
    	{
    		PolGrid.setRowColData(i, 10, "");
    	}
    }
    
    strSQL = "select distinct a.SerialNo,a.otherno,d.GetNoticeNo, "
             + "(select appntname from lccont where contno = d.contno), "
             + "(select appntidno from lccont where contno = d.contno), "
             + "(select name from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + "(select idno from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + "a.sumgetmoney,d.curgettodate,d.agentcode,d.insuredno, "
             + "(case when exists (select 1 from ljaget where actugetno = a.getnoticeno) then codename('paymode',a.paymode) else '' end), "
             + "(select min(name) from lacom where agentcom = d.agentcom), "
             + "(case when exists (select 1 from ljaget where actugetno = a.getnoticeno and confdate is not null) then '给付完成' "
             + "when exists (select 1 from ljaget where actugetno = a.getnoticeno and confdate is null) then '待给付' else '待给付确认' end), " 
             + "d.appntno,a.paymode,d.handler,d.GetNoticeNo from ljsget a ,ljsgetdraw d "
             + "where a.getnoticeno = d.getnoticeno "
             + "and a.othernotype='20' and feefinatype != 'YEI' "
             + " and riskcode in ('330501','530301') "
             + getWherePart( 'd.ContNo','ContNo') ;
    turnPage1.queryModal(strSQL, LjsGetGrid); 
    
	strSQL = "SELECT CONTNO,insuredno,'',RISKCODE,GETMONEY,(select enddate from lcpol where polno = LJSGETDRAW.polno),POLNO, " 
	        + "case DutyCode when '000000' then '初始忠诚奖' when '000001' then '外加忠诚奖' end "
	        + "FROM LJSGETDRAW WHERE 1=1 "
	        + " and riskcode in ('330501','530301') "
	        + getWherePart( 'ContNo','ContNo') ;
	turnPage2.queryModal(strSQL, LjsGetDrawGrid);
	
	fm.PolNo.value = LjsGetDrawGrid.getRowColDataByName(0,"PolNo"); 
	
	strSQL = "SELECT sum(GETMONEY) " 
	       + "FROM LJSGETDRAW WHERE 1=1 and DutyCode in ('000000','000001') "
	       + " and riskcode in ('330501','530301') "
	       + getWherePart('ContNo','ContNo') ;
	var rs = easyExecSql(strSQL);
    if(rs)
    {
	  fm.Bonus.value = rs;
	}
	strSQL = "SELECT EdorValue " 
	       + "FROM LPEdorEspecialData WHERE DetailType = 'NEWBONUS' "
	       + getWherePart('EdorNo','EdorNo') ;
	var rs1 = easyExecSql(strSQL);
    if(rs1)
    {
	  fm.NewBonus.value = rs1;
	}
	strSQL = "SELECT EdorValue " 
	       + "FROM LPEdorEspecialData WHERE DetailType = 'GETMONEY' "
	       + getWherePart('EdorNo','EdorNo') ;
	var rs2 = easyExecSql(strSQL);
    if(rs2)
    {
	  fm.GetMoney.value = rs2;
	}
}

function saveData()
{
  if (!beforeSubmit())
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./PEdorTypeCBSubmit.jsp";
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  edorReturn();
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit1( FlagStr, content, bonus )
{
	try { showInfo2.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	fm.NewBonus.value = bonus;
	var oldBonus = parseFloat(fm.Bonus.value);
	var newBonus = parseFloat(bonus);
	var getmoney = mathRound(newBonus - oldBonus);
	fm.GetMoney.value = getmoney;
}


//提交前的校验、计算  
function beforeSubmit()
{
  tGetMoney = fm.GetMoney.value;
  if(trim(tGetMoney)==null || trim(tGetMoney) == "")
  {
    alert("补充退费金额不能为空");
    return false;
  }
  if(tGetMoney==0)
  {
    alert("补充退费金额为0，不能补充退费");
    return false;
  }
  if(tGetMoney<0)
  {
    alert("补充退费金额为负数，不能补充退费");
    return false;
  }
  return true;
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无1
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function calData()
{
  if (fm.RateFlag.checked == true)
  {
    fm.RateFlagValue.value = "Y";
  }
  else if (fm.RateFlag.checked == false)
  {
    fm.RateFlagValue.value = "N";
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo2=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.action = "./PEdorTypeCBCal.jsp";
  fm.submit();
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

