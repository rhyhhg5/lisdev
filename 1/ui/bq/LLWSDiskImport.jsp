<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLWSDiskImport.jsp
//程序功能：卡折实名化磁盘导入
//创建日期：2009-12-30
//创建人 ：Zhanggm
//更新记录： 更新人  更新日期   更新原因/内容
%>
<html>
<head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>磁盘导入</title>
<script>

  //卡折实名化磁盘导入
  function importFile()
  {
  	var sql = "select 1 from LPDiskImport "  
  	        + "where GrpContNo = '" + fm.all("GrpContNo").value + "' " 
  	        + "and EdorType = '" + fm.all("EdorType").value + "' " 
  	        + "and EdorNo = '" + fm.all("EdorNo").value + "' " ;
  	var result = easyExecSql(sql);
  	if (result != null)
  	{
  		if (!confirm("已导入数据将被全部删除！"))
  		{
  			return false;
  		}
  	}
  	document.all("Info").innerText = "上传文件导入中，请稍后...";
  	fm.submit();
  }
  
  function afterSubmit(flag, content)
  {
  	document.all("Info").innerHTML = content;
  	if (flag == "Succ")
  	{
  		top.opener.initForm();
  		top.close();
  	}
  }
	
  function initForm()
  {
    fm.all("EdorNo").value = "<%=request.getParameter("EdorNo")%>";
    fm.all("EdorType").value = "<%=request.getParameter("EdorType")%>";
    fm.all("GrpContNo").value = "<%=request.getParameter("GrpContNo")%>";
  }
</script>
</head>
<body onload="initForm();">
<span id='show'></span>
	<form name="fm" action="./LLWSDiskImportSave.jsp" method=post target="fraSubmit" ENCTYPE="multipart/form-data">
		<input type="hidden" name="EdorNo">
		<input type="hidden" name="EdorType">
		<input type="hidden" name="GrpContNo">
		<br><br>
		<table class=common>
			<TR>
				<TD width='15%' style="font:9pt">文件名</TD>
				<TD width='85%'>
					<Input type="file" width="100%" name="FileName">
					<INPUT VALUE="导  入" class="cssButton" TYPE=button onclick="importFile();">
				</TD>
			</TR>
		</table>
		<br><br>
		<table class=common>
			<TR align="center">
				<TD id="Info" width='100%' style="font:10pt"></TD>
			</TR>
		</table>
	</form>
</body>
</html>