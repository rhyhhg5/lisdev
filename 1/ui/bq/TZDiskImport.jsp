<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DiskImport.jsp
//程序功能：磁盘导入
//创建日期：2005-7-7
//创建人 ：QiuYang
//更新记录： 更新人  更新日期   更新原因/内容
%>
<html>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="./TZDiskImport.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>磁盘导入</title>
<script>
	var edorNo = "<%=request.getParameter("EdorNo")%>";
	var grpContNo = "<%=request.getParameter("GrpContNo")%>";
	
	function initForm()
	{
		fm.all("EdorNo").value = edorNo;
		fm.all("GrpContNo").value = grpContNo;
	}
</script>
</head>
<body onload="initForm();">
  <p>请上传模板</p>
  <form action="" method=post name=fm1 target="fraSubmit" enctype="multipart/form-data">
  			<TR class= common>
			<TD  width='8%' style="font:9pt">        文件名： </TD>
            <TD  width='80%'>
            <input type="file"　width="100%" name=FileName class= common>
            <br/><br/><br/>
			<input VALUE="上载被保险人清单" class="cssbutton" TYPE=button onclick = "InsuredListUpload();" >
			
			<table class=common>
			<TR align="center">
				<TD id="Info" width='100%' style="font:10pt"></TD>
			</TR>
		</table>
  </form>
</body>
</html>