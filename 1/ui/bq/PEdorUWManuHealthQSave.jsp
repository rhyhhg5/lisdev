<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ManuHealthQChk.jsp
//程序功能：人工核保体检资料查询
//创建日期：2005-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%
  String flag = "";
  String content = "";

	GlobalInput gi = (GlobalInput)session.getValue("GI");
	String edorNo = request.getParameter("EdorNo");
 	String tContNo = request.getParameter("ContNo");
 	String tProposalContNo = request.getParameter("ContNo");
 	String tCustomerNo = request.getParameter("InsuredNo");
 	String tPrtSeq = request.getParameter("PrtSeq");
 	String tNote = request.getParameter("Note");
  String tSerialNo[] = request.getParameterValues("DisDesbGridNo");
  String tDisDesb[]=request.getParameterValues("DisDesbGrid1");
	String tDisResult[]=request.getParameterValues("DisDesbGrid2");
	String tICDCode[]=request.getParameterValues("DisDesbGrid3");
  int lineCount = 0;
  if (tSerialNo != null)
  {		
    lineCount = tSerialNo.length;
  }

	LPPENoticeResultSet tLPPENoticeResultSet = new LPPENoticeResultSet();	
	for (int i = 0; i < lineCount; i++)
	{
		LPPENoticeResultSchema tLPPENoticeResultSchema = new LPPENoticeResultSchema();
		tLPPENoticeResultSchema.setEdorNo(edorNo);
		tLPPENoticeResultSchema.setPrtSeq(tPrtSeq);
		tLPPENoticeResultSchema.setSerialNo(tPrtSeq);
		tLPPENoticeResultSchema.setCustomerNo(tCustomerNo);
		tLPPENoticeResultSchema.setDisDesb(tDisDesb[i]);
		tLPPENoticeResultSchema.setDisResult(tDisResult[i]);
		tLPPENoticeResultSchema.setICDCode(tICDCode[i]);
		tLPPENoticeResultSchema.setRemark(tNote);
		tLPPENoticeResultSet.add(tLPPENoticeResultSchema);
	}

	VData data = new VData();
  data.add(gi);
  data.add(tLPPENoticeResultSet);
  PEdorUWManuHealthQUI tPEdorUWManuHealthQUI = new PEdorUWManuHealthQUI();
  if (!tPEdorUWManuHealthQUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tPEdorUWManuHealthQUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！";
  }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>
