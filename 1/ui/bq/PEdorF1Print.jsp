<%@ page contentType="text/html; charset=GBK" %>
<%@ page import="com.sinosoft.utility.XMLPathTool" %>
<%@ page import="com.sinosoft.utility.F1Print" %>
<%@ page import="javax.xml.transform.*" %>
<%@ page import="com.f1j.ss.*" %>
<%@ page import="com.f1j.swing.*" %>
<%@ page import="com.f1j.ss.BookModelImpl" %>
<%@ page import="com.f1j.util.F1Exception" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%!
  String strXMLFileName, strF1FileName, strRealFile;
  OutputStream outStream = null;
%>

<%
  String strText, strSubText, strNodeVal;
  String strListVal[];
  String strXPath, strNodeName;
  int intChildPos, i, j;
  int intNewRow, intNewCol;
  int intListLength;
  int intCurrRow;
  int intRowHeight;
  int intColWidth;
  int intNewMaxRow, intNewMaxCol;
  com.f1j.ss.CellFormat cellFmt, cellFmtNew;
  short shortTopBorder, shortBottomBorder, shortLeftBorder, shortRightBorder;
  boolean bMergeCells;

  intNewRow = 0;
  intNewCol = 0;
  intListLength = 0;
  intNewMaxRow = 1;
  intNewMaxCol = 1;
  strXMLFileName = request.getParameter("xmlFile");
  if (strXMLFileName == null){
    strXMLFileName = "2.xml";
  }else{
    if (strXMLFileName.equalsIgnoreCase(""))
      strXMLFileName = "2.xml";
  }
  strRealFile = application.getRealPath(strXMLFileName);
  if (!strRealFile.equalsIgnoreCase("") || strRealFile != null){
    F1Print aF1Print = new F1Print(strRealFile);
    strF1FileName = aF1Print.getTemplate();
    strRealFile = application.getRealPath(strF1FileName);

    try{
      com.f1j.ss.Book book = new com.f1j.ss.BookImpl();
      com.f1j.ss.BookModelImpl bookModel = new com.f1j.ss.BookModelImpl(book);
//从指定的格式文件中读入数据
      com.f1j.ss.ReadResults result;
      result = bookModel.read(strRealFile, new com.f1j.ss.ReadParams());
//为存放最后输出结果，创建一个新的F1
      com.f1j.ss.Book bookNew = new com.f1j.ss.BookImpl();
      com.f1j.ss.BookModelImpl bookModelNew = new com.f1j.ss.BookModelImpl(bookNew);
      result = bookModelNew.read(strRealFile, new com.f1j.ss.ReadParams());
      int intMaxRow = bookModel.getMaxRow();
      int intMaxCol = bookModel.getMaxCol();
      bookModelNew.clearRange(0, 0, intMaxRow, intMaxCol, com.f1j.ss.BookModelImpl.eClearValues);
      cellFmt = bookModel.getCellFormat(0,0,0,0);
      for (i = 0; i < intMaxRow - 1; i++){
        intNewRow = intNewRow + intListLength;
        intListLength = 0;
        intRowHeight = bookModel.getRowHeight(i);
        if (intNewMaxRow < (intNewRow + 1)) intNewMaxRow = intNewRow + 1;
        for (j = 0; j < intMaxCol - 1; j++){
          intNewCol = j;
          strText = bookModel.getText(i,j);
          bookModel.setSelection(i,j,i,j);
          bookModel.getCellFormat(cellFmt);
          bMergeCells = cellFmt.isMergeCells();
          cellFmt.useAllFormats();
          if (strText.equals("$/")){
            if ( bookModelNew.getText(i, j).equals("$/")){
              bookModelNew.setText(i, j, "");
            }
            if (intNewMaxCol < (j + 1)) intNewMaxCol = j + 1;
            break;
          }
          if (!strText.equals("")){
            if (strText.length() > 2)
              strSubText = strText.substring(0,2);
            else
              strSubText = strText;
          }else
            strSubText = "";
          if (strSubText.equals("$=")){
            strXPath = "/DATASET" + strText.substring(2);
            strNodeVal = aF1Print.getNodeValue(strXPath);
            bookModelNew.setText(intNewRow, intNewCol, strNodeVal);
            bookModelNew.setSelection(intNewRow, intNewCol, intNewRow, intNewCol);
            bookModelNew.setCellFormat(cellFmt);
            bookModelNew.setRowHeight(intNewRow, intRowHeight);
            if (intListLength < 1) intListLength = 1;
          }else if (strSubText.equals("$*")){
            strText = strText.substring(2);
            intChildPos = strText.lastIndexOf("/");
            strXPath = "/DATASET" + strText.substring(0, intChildPos);
            strNodeName = strText.substring(intChildPos + 1);
            strListVal = aF1Print.getNodeListValue(strXPath, strNodeName);
            if (intListLength < strListVal.length) intListLength = strListVal.length;
            for (int k = 0; k < strListVal.length; k++){
              intCurrRow = intNewRow + k;
              if (k > 0 && intNewCol == 0) bookModelNew.insertRange(intCurrRow, intNewCol, intCurrRow, intNewCol, bookModelNew.eShiftRows);
              if (strText.length() > 0)
                bookModelNew.setText(intCurrRow, intNewCol, strListVal[k]);
              bookModelNew.setRowHeight(intCurrRow, intRowHeight);
              bookModelNew.setSelection(intCurrRow, intNewCol, intCurrRow, intNewCol);
              bookModelNew.setCellFormat(cellFmt);
            }
          }else{
            bookModelNew.setText(intNewRow, intNewCol, strText);
            bookModelNew.setSelection(intNewRow, intNewCol, intNewRow, intNewCol);
            bookModelNew.setCellFormat(cellFmt);
            bookModelNew.setRowHeight(intNewRow, intRowHeight);
            if (intListLength < 1) intListLength = 1;
          }
        }
        if (j==0) break;
      }
      bookModelNew.setSelection(0, 0, intNewMaxRow - 1, intNewMaxCol - 1);
      bookModelNew.setPrintArea();
      bookModelNew.saveViewInfo();
      bookModelNew.write(response.getOutputStream(),new com.f1j.ss.WriteParams(bookModel.eFileFormulaOne6));
//        bookModelNew.write("E:\\jbuilder5\\f1print\\defaultroot\\r.vts",new com.f1j.ss.WriteParams(bookModel.eFileFormulaOne6));
    }catch(java.net.MalformedURLException urlEx){
      urlEx.printStackTrace();
    }catch(java.io.IOException ioEx){
      ioEx.printStackTrace();
    }catch(F1Exception f1Ex){
      f1Ex.printStackTrace();
    }
  }
%>