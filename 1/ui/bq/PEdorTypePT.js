//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//window.onfocus = initFocus;
focusTag = true;
var remainAmnt;
var remainMult;
function initForm2(){
    
	initFocus();
}
function edorTypePTReturn()
{
		initForm();
}

function edorTypePTSave()
{

  if (fm.all('CalMode').value == 'O') {
  	var nRemainMulti = parseFloat(fm.all('RemainMulti').value);
  	var nMult = parseFloat(fm.all('Mult').value);
  	if (nRemainMulti > nMulti) {
  		alert("份数必须小于原份数。");
  		return;
  	}
  	if (nRemainMulti <= 0 ) {
  		alert("余留份数必须大于0。");
  		return;
    }
  } else {
    var nRemainAmnt = parseInt(remainAmnt);
    var nAmnt = parseInt(fm.all('Amnt').value);
    var nRemainMult = parseInt(remainMult);
    var nMult = parseInt(fm.Mult.value);
    if(0 > nMult){
    	alert("份数必须大于0。");
    }
  	if (nRemainAmnt < nAmnt) {
  		alert("总基本保额余数必须小于原保额"+remainAmnt+"。");
  		return;
  	}
  	if (nAmnt <= 0) {
  		alert("总基本保额余数必须大于0。");
  		return;
  	}
  }

  //团险保全复用，第一次进入时提供录入功能
	if (typeof(top.opener.GrpBQ)=="boolean" && top.opener.GrpBQ==false) {
	  //接收录入数据
	  top.opener.GTArr.push(fm.RemainAmnt.value);


	  top.opener.GrpBQ = true;
	  //top.opener.pEdorMultiDetail();
	  //top.close();
	}
	//**************************************
	else {
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.all('fmtransact').value = "INSERT||MAIN";

    fm.submit();
  }
  focusTag = true;

}

function customerQuery()
{
	window.open("./LCAppntIndQuery.html");
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  initLCAppntIndGrid();
 //  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
  showInfo.close();
  top.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  	var tTransact=fm.all('fmtransact').value;
//  	alert(tTransact);
		if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;

 	 		//查询成功则拆分字符串，返回二维数组
	  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
//	  		alert(tArr[0]);
//	  		alert(tArr[0].length);
	  		fm.all('Amnt').value = tArr[0][45];
	  		fm.all('Prem').value = tArr[0][42];
	  		fm.all('AppntNo').value = tArr[0][28];
	  		fm.all('AppntName').value = tArr[0][29];
	  		fm.all('Multi').value = tArr[0][40];
	  		fm.all('CalMode').value = tArr[0][134];

	  		//calMode是付加在保单的字符串传过来的，并放在了最后
	  		var calMode = tArr[0][134];
	  		//alert(tArr[0][tArr[0].length-2]);

	  		if (calMode == 'P') {
	  			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content= 此保单无法进行部分退保计算" ;
			    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	   			initForm();
	   			return;
  			} else if (calMode == 'O') {   //利用分数变化进行计算

  			    //fm.all('RemainMulti').value = tArr[0][132];
  				RemainAmntDiv.style.display = "none";
  				RemainMultiDiv.style.display = "";
  			} else {                      //利用保额变化进行计算

  				//fm.all('RemainAmnt').value = tArr[0][132];
  				RemainMultiDiv.style.display = 'none';
  				RemainAmntDiv.style.display = '';
  			}

			//团险保全复用，自动填写数值，并提交
    	if (typeof(top.opener.GrpBQ)=="boolean" && top.opener.GrpBQ==true) {
    	  fm.RemainAmnt.value = top.opener.GTArr.pop();

    	  edorTypePTSave();
     	}
     	//***********************************

		} else {

      //团险保全复用，提交成功后，再次调用，以循环
   		if (typeof(top.opener.GrpBQ)=="boolean" && top.opener.GrpBQ==true) {
			  top.opener.PEdorDetail();
			  //top.close();
			}
			else {
			  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   		  initForm();
   		}
  	}
		//returnParent();
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{

}

function returnParent()
{
	top.opener.getEdorItem();
	top.window.close();
}

function personQuery()
{
    //window.open("./LCPolQuery.html");
    window.open("./LPTypeIAPersonQuery.html");
}

function afterPersonQuery(arrResult)
{
    if (arrResult == null ||arrResult[0] == null || arrResult[0][0] == "" )
        return;

    //选择了一个投保人,显示详细细节
    fm.all("QueryCustomerNo").value = arrResult[0][0];

    var strSql = "select * from ldperson where customerNo = " + arrResult[0][0];

	//查询SQL，返回结果字符串
    turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);

 //   alert(turnPage.strQueryResult);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    	//清空MULTILINE，使用方法见MULTILINE使用说明
      	VarGrid.clearData('VarGrid');
      	alert("查询失败！");
      	return false;
  	}


 	 //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

    //查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    fillPersonDetail();

  divLPAppntIndDetail.style.display = "";



}

function fillPersonDetail()
{
	try {
		fm.all("AppntCustomerNo").value = turnPage.arrDataCacheSet[0][0];
		fm.all("AppntName").value = turnPage.arrDataCacheSet[0][2];
		fm.all("AppntSex").value = turnPage.arrDataCacheSet[0][3];
		fm.all("AppntBirthday").value = turnPage.arrDataCacheSet[0][4];

		fm.all("AppntIDType").value = turnPage.arrDataCacheSet[0][16];
		fm.all("AppntIDNo").value = turnPage.arrDataCacheSet[0][18];
		fm.all("AppntNativePlace").value = turnPage.arrDataCacheSet[0][5];

		fm.all("AppntPostalAddress").value = turnPage.arrDataCacheSet[0][24];
		fm.all("AppntZipCode").value = turnPage.arrDataCacheSet[0][25];
		fm.all("AppntHomeAddress").value = turnPage.arrDataCacheSet[0][23];
		fm.all("AppntHomeZipCode").value = turnPage.arrDataCacheSet[0][22];

		fm.all("AppntPhone").value = turnPage.arrDataCacheSet[0][26];
		fm.all("AppntPhone2").value = turnPage.arrDataCacheSet[0][56];
		fm.all("AppntMobile").value = turnPage.arrDataCacheSet[0][28];
		fm.all("AppntEMail").value = turnPage.arrDataCacheSet[0][29];
		fm.all("AppntGrpName").value = turnPage.arrDataCacheSet[0][38];

		fm.all("AppntWorkType").value = turnPage.arrDataCacheSet[0][48];
		fm.all("AppntPluralityType").value = turnPage.arrDataCacheSet[0][49];
		fm.all("AppntOccupationCode").value = turnPage.arrDataCacheSet[0][50];
		fm.all("AppntOccupationType").value = turnPage.arrDataCacheSet[0][9];
    } catch(ex) {
    	alert("在PEdorTypePT.js-->fillPersonDetail444函数中发生异常:初始化界面错误!");
  	}
}
function initFocus()
{
	if(focusTag){
	    alert("hello");
	focusTag = false;
    	queryClick();
	}
}
function queryClick()
{
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        TypeFlag = fm.TypeFlag.value;
	if(TypeFlag == 'Grp'){
            var strSql = "select insuredno, InsuredName, riskcode, polno, CValiDate, GrpContNo, ContNo from LCPol where "
		   +" contno='"+fm.ContNo.value+"' and insuredNo='"+fm.InsuredNo.value+"'" ;
                   arrResult2 = easyExecSql(strSql, 1, 0);
                   
	}else{
	var strSql = "select a.RiskSeqNo,b.riskname,a.InsuredName, a.Amnt,a.prem, a.StandPrem,a.prem-a.standprem, a.ContNo,a.polno, Mult from LCPol a,lmriskapp b where "
		   +"a.riskcode=b.riskcode and a.riskcode in (select riskcode from lmriskedoritem where edorcode='PT') and a.contno='"+fm.ContNo.value+"'" ;
		   //alert(strSql);
	}
	
	turnPage.queryModal(strSql, PolGrid);
	
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getRowColData(i, 9) == "0")
		{
			PolGrid.setRowColData(i, 9, "");
		}
	}

	showInfo.close();
}


//查询已经修改的项目信息
function queryLPPol()
{
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  TypeFlag = fm.TypeFlag.value;
	var strSql = "select a.RiskSeqNo,b.riskname,a.InsuredName, (select b.amnt from lcpol b where a.polno=b.polno),a.Amnt, a.ContNo,a.polno from LPPol a,lmriskapp b where "
		   +"a.riskcode=b.riskcode and a.contno='"+fm.ContNo.value+"' and a.edortype='PT' and edorno='"+fm.EdorNo.value+"'" ;	
	//alert(strSql);
	turnPage.queryModal(strSql, LPPolGrid);

	showInfo.close();
}

function getPolDetail()
{
    	var strSQL;
    	if(TypeFlag == 'Grp'){
    	var tSel = PolGrid.getSelNo();
    	fm.all("PolNo").value = PolGrid.getRowColData(tSel-1,4);
		  strSQL = "Select a.CustomerNo,a.Name,b.mult,b.amnt,b.prem  From lcpol b,lcgrpappnt a  Where b.contno='"+fm.all("ContNo").value+"'"+
					" And b.grpcontno=a.grpcontno And b.polno='"+fm.all("PolNo").value+"'";
    	}
    	else
    	{
    		var tSel = PolGrid.getSelNo();
    		fm.all("PolNo").value = PolGrid.getRowColData(tSel-1,9);
    		strSQL = "select a.AppntName,a.appntNo,b.mult,b.amnt,b.prem,"
    	         +"b.InsuredNo,b.RiskCode from lcappnt a,lcpol b "
    		       +"where a.contno = b.contno  and "
    		       +" b.polno='"+fm.all("PolNo").value+"'";
      }
		arrResult = easyExecSql(strSQL);
		var selectdRow = PolGrid.getSelNo();
 	    if (arrResult == null)
 	    {
                    alert("未查到个人保单信息！");
	    }
	    else
	    {
 	        try {fm.all('AppntName').value= arrResult[0][0]; } catch(ex) { };
 	        try {fm.all('AppntNo').value= arrResult[0][1]; } catch(ex) { };
 	        try {fm.all('Mult').value= arrResult[0][2]; } catch(ex) { };
 	        try {fm.all('Amnt').value= arrResult[0][3]; } catch(ex) { };
 	        try {fm.all('Prem').value= arrResult[0][4]; } catch(ex) { };
 	        try {fm.all('RiskCode').value= arrResult[0][6]; } catch(ex) { };
 	        remainAmnt = arrResult[0][3];
 	        remainMult = arrResult[0][2];
 	        fm.InsuredNo.value = arrResult[0][5];
 	      //  fm.PolNo.value = top.opener.fm.all("PolGrid4").value;
 	        fm.EdorAcceptNo.value = top.opener.fm.all("EdorAcceptNo").value;
			
 	    }
}
