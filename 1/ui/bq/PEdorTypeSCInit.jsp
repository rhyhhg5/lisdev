<%
//程序名称：PEdorTypeSCInit.jsp
//程序功能：
//创建日期：2004-7-19 11:35
//创建人  ：JiangLai
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">  

function initInpBox()
{ 
  try
  {      
  	
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('PolNo').value = top.opener.fm.all('PolNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
  }
  catch(ex)
  {
    alert("在PEdorTypeBCInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initQuery();  
  }
  catch(re)
  {
    alert("在PEdorTypeBCInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initQuery()
{	
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
  fm.all('fmtransact').value = "QUERY||MAIN";
  fm.submit();	  	 	 
}

</script>