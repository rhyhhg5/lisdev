//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{	
	var strSQL = "";
	var agentCode = fm.all('agentCode').value;
	var agentName = fm.all('agentName').value;
	var agentGroup = fm.all('agentGroup').value;
	if(agentCode != null && agentCode != "")
	{
		agentCode = "and groupAgentCode like '%" + agentCode + "%' ";
	}
	if(agentName != null && agentName != "")
	{
		agentName = "and Name like '%" + agentName + "%' ";
	}
	if(agentGroup != null && agentGroup != "")
	{
		agentGroup = "and agentGroup like '%" + agentGroup + "%' ";
	}
		
	// 初始化表格
	initAgentGrid();
	var strSQL = "select groupAgentCode,Name from laagent "
						+ "where managecom like '"+ fm.all('comcode').value +"%'"
	           + agentCode 
             + agentName
             + agentGroup
             + " order by agentCode";
  
  turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSQL, AgentGrid); 
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterAgentQuery( arrReturn );
			top.close();
		}
		catch(ex)
		{
			alert( "请先选择一条非空记录，再点击返回按钮。");
			//alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		
	}
}

function getQueryResult()
{
	//获取正确的行号
	tRow = AgentGrid.getSelNo() - 1;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = AgentGrid.getRowColData(tRow, 1); 
	arrSelected[1] = AgentGrid.getRowColData(tRow, 2);
	return arrSelected;
}





