var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var mIdNo=null;
var mName=null;
var mSex=null;
var mBirthday=null;
var mOccupationType=null;

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  if(!checkDate())
  {
    return false;
  }
  return true;
}

function edorTypeJMSave()
{
  if (!beforeSubmit())
  {
    return false;
  }
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmtransact').value = "INSERT||MAIN";
	fm.submit();

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content,Result )
{
	try{ showInfo.close(); } catch(e) {}
	window.focus();
	if (fm.all("TypeFlag").value == "G")
	{
	  top.opener.queryClick2();
	}
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var tTransact=fm.all('fmtransact').value;
	  if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	  	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
	  	//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
	  	var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
			
			turnPage.arrDataCacheSet =chooseArray(tArr,[29,4,14,15,16,11]);
			
			fm.all('BankCode').value = turnPage.arrDataCacheSet[0][0];
		}
	  
	//	else if (tTransact=="QUERY||DETAIL")
	//	if (tTransact=="QUERY||MAIN")
		if (tTransact=="QUERY||DETAIL")
		{
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
			turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
			//	alert(Result);
			turnPage.strQueryResult  = Result;
			//使用模拟数据源，必须写在拆分之前
			turnPage.useSimulation   = 1;  

			//查询成功则拆分字符串，返回二维数组
			var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,tTransact);
			turnPage.arrDataCacheSet =chooseArray(tArr,[2,3,4,5,6,7,27,28,9,12]);
			//turnPage.arrDataCacheSet =chooseArray(tArr,[4,14,15,16,17,18,36,37,19,22]);
			
			fm.all('CustomerNo').value = turnPage.arrDataCacheSet[0][0];
			fm.all('Name').value = turnPage.arrDataCacheSet[0][1];
			fm.all('NameBak').value = turnPage.arrDataCacheSet[0][1];
			fm.all('Sex').value = turnPage.arrDataCacheSet[0][2];
			fm.all('SexBak').value = turnPage.arrDataCacheSet[0][2];
			fm.all('Birthday').value = turnPage.arrDataCacheSet[0][3];
			fm.all('BirthdayBak').value = turnPage.arrDataCacheSet[0][3];
			fm.all('IDType').value = turnPage.arrDataCacheSet[0][4];
			fm.all('IDTypeBak').value = turnPage.arrDataCacheSet[0][4];
			fm.all('IDNo').value = turnPage.arrDataCacheSet[0][5];
			fm.all('IDNoBak').value = turnPage.arrDataCacheSet[0][5];
			fm.all('OccupationType').value = turnPage.arrDataCacheSet[0][6];
			fm.all('OccupationTypeBak').value = turnPage.arrDataCacheSet[0][6];
			fm.all('OccupationCode').value = turnPage.arrDataCacheSet[0][7];
			fm.all('OccupationCodeBak').value = turnPage.arrDataCacheSet[0][7];
		//	fm.all('NativePlace').value = turnPage.arrDataCacheSet[0][8];
			fm.all('Marriage').value = turnPage.arrDataCacheSet[0][9];
			
			var customerNo = turnPage.arrDataCacheSet[0][0];
			var sql = "select RelationToMainInsured from LCInsured where InsuredNo = '" + customerNo + "' ";
			var arrResult = easyExecSql(sql);
			if (arrResult)
			{
				fm.all('Relation').value = arrResult[0][0];
			}
		  
			divLPInsuredDetail.style.display ='';
			divDetail.style.display='';
			queryBank();
			queryCont();
      showAllCodeName();
		} 
		else
		{ 
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
			 
			returnParent();
	  }
	}   
}     
                 
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		// 查询保单明细
		queryInsuredDetail();
	}
}

function queryInsuredDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	tEdorType=fm.all('EdorType').value;
	tPolNo=fm.all('PolNo').value;
	tCustomerNo = fm.all('CustomerNo').value;
	parent.fraInterface.fm.action = "./InsuredQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorTypeICSubmit.jsp";
}

function returnParent()
{
	try
	{
      top.opener.focus();
	  top.opener.getGrpEdorItem();
	}
	catch (ex) {}
	top.close();
}

function queryCont()
{
  var customerNo = fm.all('CustomerNo').value;
  var appntNo = fm.all('AppntNo').value;
  var contNo = fm.all('ContNo').value;
  var strSQL;
  	strSQL = "select a.ContNo, a.AppntName, a.InsuredName, a.CValiDate, a.Prem, a.Amnt, a.GrpContNo " +
  	             "from LCCont a " +
  	             "where a.ContNo = '" + contNo + "' ";

	turnPage.queryModal(strSQL, ContGrid);
}
function getContNo()
{
   var contNoSql="SELECT edorvalue from lpedorespecialdata where edoracceptno='"+fm.all('EdorAcceptNo').value+"' and detailtype='CARDNO'";
   var contNoResult=easyExecSql(contNoSql);
   if(contNoResult)
   {
     fm.all('ContNo').value = contNoResult[0][0];
   }
   else{
	   alert("获取卡号失败");
	   return false;
   }
   
}

function getCustomerNo()
{
//	针对多被保人的情况修改此部分取值
//   var insuredNoSql="select insuredno from lccont where contno='"+fm.all('ContNo').value+"'";
	var insuredNoSql="SELECT edorvalue from lpedorespecialdata where edoracceptno='"+fm.all('EdorAcceptNo').value+"' and detailtype='INSUREDNO'";
   var insuredNoResult=easyExecSql(insuredNoSql);
   if(insuredNoResult)
   {
     fm.all('CustomerNo').value = insuredNoResult[0][0];
   }
   else{
	   alert("获取被保人号失败");
	   return false;
   }
   
}

//查询客户基本信息
function getCustomerInfo(customerNo)
{
	var contNo = fm.all("ContNo").value
	//以后去掉下面这行  将查询第一个licardactiveinfolist改为lpcardactiveinfolist
/*	contNo="MN000008665";//AK00192
	fm.all('CustomerNo').value='000944022';
	fm.all('contNo').value='MN000008665';
	fm.all('EdorType').value='JM';
	fm.all('EdorNo').value='20100826';*/
	//
	
	var edorNo = fm.all("EdorNo").value; 
	var edorType = fm.all("EdorType").value;
	//先从P表中查询，再查C表
	var sql1 ="select name ,sex,Birthday,(select max(cvalidate) from licertify where cardno='"+contNo+"'),idno,"
	         +"(select edorvalue from lpedorespecialdata where edoracceptno=a.edorno and detailtype='Mobile'),"
	         +"(select edorvalue from lpedorespecialdata where edoracceptno=a.edorno and detailtype='Phone'),"
	         +"(select edorvalue from lpedorespecialdata where edoracceptno=a.edorno and detailtype='PostalAddress'),"
	         +"(select edorvalue from lpedorespecialdata where edoracceptno=a.edorno and detailtype='ZipCode'),"
	         +"(select edorvalue from lpedorespecialdata where edoracceptno=a.edorno and detailtype='EMail'), "
	         +"(select edorvalue from lpedorespecialdata where edoracceptno=a.edorno and detailtype='IDType'), "
	         +"(select edorvalue from lpedorespecialdata where edoracceptno=a.edorno and detailtype='OccupationCode'), "
	         +"(select edorvalue from lpedorespecialdata where edoracceptno=a.edorno and detailtype='OccupationType') "
	         +" from lpperson a where edorno='"+fm.all('EdorAcceptNo').value+"' " ;
                       
//   	var sql2 ="select name ,sex,Birthday,cvalidate,idno,mobile,phone,postALADDRESS,zipcode,email,idtype,occupationcode,occupationtype " 
//	         +" from licardactiveinfolist where cardno='"+contNo+"' " ;
//由于多被保人的问题修改取值方式
   	var sql2 ="select name ,sex,Birthday,(select max(cvalidate) from licertify where cardno='"+contNo+"'),idno,mobile,phone,postALADDRESS,zipcode,email,idtype,occupationcode,occupationtype " 
        +" from licertifyinsured where cardno='"+contNo+"' and customerno='"+customerNo+"'" ;
   	
    var arrResult1 = easyExecSql(sql1); 
    var arrResult2 = easyExecSql(sql2); 
	if (arrResult1)
	{
		fm.all('Name').value = arrResult1[0][0];
		fm.all('NameBak').value = arrResult1[0][0];
		fm.all('Sex').value = arrResult1[0][1];
		fm.all('SexBak').value = arrResult1[0][1];
		fm.all('Birthday').value = arrResult1[0][2];
		fm.all('BirthdayBak').value = arrResult1[0][2];
		fm.all('Cvalidate').value = arrResult1[0][3];
		fm.all('CvalidateBak').value = arrResult1[0][3];
		fm.all('IDNo').value = arrResult1[0][4];
		fm.all('IDNoBak').value = arrResult1[0][4];
		fm.all('Mobile').value = arrResult1[0][5];
		fm.all('MobileBak').value = arrResult1[0][5];
		fm.all('Phone').value = arrResult1[0][6];
		fm.all('PhoneBak').value = arrResult1[0][6];
		fm.all('PostAddress').value = arrResult1[0][7];
		fm.all('PostAddressBak').value = arrResult1[0][7];
		fm.all('ZipCode').value = arrResult1[0][8];
		fm.all('ZipCodeBak').value = arrResult1[0][8];
		fm.all('Email').value = arrResult1[0][9];
		fm.all('EmailBak').value = arrResult1[0][9];
		fm.all('IDType').value = arrResult1[0][10];
		fm.all('IDTypeBak').value = arrResult1[0][10];
		fm.all('OccupationCode').value = arrResult1[0][11];
		fm.all('OccupationCodeBak').value = arrResult1[0][11];
		fm.all('OccupationType').value = arrResult1[0][12];
		fm.all('OccupationTypeBak').value = arrResult1[0][12];
		
	}
  if((!arrResult1)&&arrResult2)
  {
  	    fm.all('Name').value = arrResult2[0][0];
		fm.all('NameBak').value = arrResult2[0][0];
		fm.all('Sex').value = arrResult2[0][1];
		fm.all('SexBak').value = arrResult2[0][1];
		fm.all('Birthday').value = arrResult2[0][2];
		fm.all('BirthdayBak').value = arrResult2[0][2];
		fm.all('Cvalidate').value = arrResult2[0][3];
		fm.all('CvalidateBak').value = arrResult2[0][3];
		fm.all('IDNo').value = arrResult2[0][4];
		fm.all('IDNoBak').value = arrResult2[0][4];
		fm.all('Mobile').value = arrResult2[0][5];
		fm.all('MobileBak').value = arrResult2[0][5];
		fm.all('Phone').value = arrResult2[0][6];
		fm.all('PhoneBak').value = arrResult2[0][6];
		fm.all('PostAddress').value = arrResult2[0][7];
		fm.all('PostAddressBak').value = arrResult2[0][7];
		fm.all('ZipCode').value = arrResult2[0][8];
		fm.all('ZipCodeBak').value = arrResult2[0][8];
		fm.all('Email').value = arrResult2[0][9];
		fm.all('EmailBak').value = arrResult2[0][9];
		fm.all('IDType').value = arrResult2[0][10];
		fm.all('IDTypeBak').value = arrResult2[0][10];
		fm.all('OccupationCode').value = arrResult2[0][11];
		fm.all('OccupationCodeBak').value = arrResult2[0][11];
		fm.all('OccupationType').value = arrResult2[0][12];
		fm.all('OccupationTypeBak').value = arrResult2[0][12];
  }
	
	// mIdNo=fm.all('IDNo').value;
    // mName=fm.all('Name').value;
    // mSex=fm.all('Sex').value;
    // mBirthday=fm.all('Birthday').value;
	
	
	
	divLPInsuredDetail.style.display ='';
	divDetail.style.display='';
	queryCont();
}

function getData()
{
	var insuredNoSql="SELECT edorvalue from lpedorespecialdata where edoracceptno='"+fm.all('EdorAcceptNo').value+"' and detailtype='INSUREDNO'";
	   var insuredNoResult=easyExecSql(insuredNoSql);
	   if(!insuredNoResult)
	   {
		   alert("获取被保人号失败");
		   return false;
	   }
	var sql3 ="select name ,sex,Birthday,idno,occupationtype  from licertifyinsured where cardno='"+fm.all("ContNo").value+"' and customerno='"+insuredNoResult[0][0]+"' " ; 
	var arrResult3 = easyExecSql(sql3); 
    if (arrResult3)
	{
		mName = arrResult3[0][0];
		mSex = arrResult3[0][1];
		mBirthday =arrResult3[0][2];
		mIdNo = arrResult3[0][3];
	    mOccupationType= arrResult3[0][4];

	}

}



function checkDate()
{  
   getData();
   if((mIdNo!=fm.all('IDNo').value)&&(mName!=fm.all('Name').value))
   {
      alert("不允许证件号与姓名同时修改,请选择其中一个进行修改。");
      return false;
   }
   var i=0;
   if(mName!=fm.all('Name').value)
   {
     i++;
   }
   if(mSex!=fm.all('Sex').value)
   {
     i++;
   }
   if(mBirthday!=fm.all('Birthday').value)
   {
     i++;
   }
   if(i>1)
   {
      alert("不允许姓名、性别、出生日期其中的两项同时修改。");
      return false;
   }
   if(mOccupationType!=fm.all('OccupationType').value)
   {
	   if(fm.all('OccupationType').value=="5"||fm.all('OccupationType').value=="6")
	   {
		   alert("职业类别不允许为5或6。");
		   return false; 
	   }
   }
   if(fm.all('IDType').value == "0" || fm.all('IDType').value  == "5"){
		var strCheckIdNo=checkIdNo(fm.all('IDType').value ,fm.all('IDNo').value ,fm.all('Birthday').value ,fm.all('Sex').value );
		if ("" != strCheckIdNo && strCheckIdNo!=null)
		{
			alert(strCheckIdNo);
			return false;
		}
	}
  return true;

}

