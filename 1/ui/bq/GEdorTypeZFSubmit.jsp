<%
//程序名称：PEdorTypeWTSubmit.jsp
//程序功能：
//创建日期：2008-2-25 10:16
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  LPGrpContSet mLPGrpContSet=new LPGrpContSet();
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

	transact = request.getParameter("fmtransact");
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String GrpcontNo = request.getParameter("GrpContNo");
	
	
    
    String tChk[] = request.getParameterValues("InpPolGridChk");
    //String tGrpContNo[]= request.getParameterValues("PolGrid9");
    String tGrpPolNo[]= request.getParameterValues("PolGrid1");
//    for (int i=0;i<tChk.length;i++)
//    {
 //       System.out.println("$$$$$$$$$$$44"+tChk[i]);
 //       if (tChk[i].equals("1"))
//        {
            LPGrpContSchema tLPGrpContSchema=new LPGrpContSchema();
            tLPGrpContSchema.setEdorNo(edorNo);
            tLPGrpContSchema.setEdorType(edorType);
            tLPGrpContSchema.setGrpContNo(GrpcontNo);
            
            mLPGrpContSet.add(tLPGrpContSchema);
//        }
//    }
                
	
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setGrpContNo(GrpcontNo);
	tLPEdorItemSchema.setContNo("000000");
	tLPEdorItemSchema.setEdorType(edorType);
	tLPEdorItemSchema.setInsuredNo("000000");
	tLPEdorItemSchema.setPolNo("000000");
	//tLPEdorItemSchema.setGetMoney(getMoney);
	
        // 准备传输数据 VData		
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPEdorItemSchema);
	tVData.add(mLPGrpContSet);

    

    GEdorZFDetailUI tGEdorZFDetailUI   = new GEdorZFDetailUI();  
		if (!tGEdorZFDetailUI.submitData(tVData, ""))
		{
			VData rVData = tGEdorZFDetailUI.getResult();
			System.out.println("Submit Failed! " + tGEdorZFDetailUI.mErrors.getErrContent());
			Content = transact + "失败，原因是:" + tGEdorZFDetailUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
		else 
		{
			Content = "保存成功";
			FlagStr = "Success";
		} 

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

