//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var oldSex = "";
var oldBirthday = "";
var oldOccupationType = "";
var oldName="";

//保存申请按钮
function edorTypeACSave()
{ 
	//if (!verify()) 
	//return false; 
	fm.all('fmtransact').value ="INSERT||MAIN";  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action="./PEdorTypeACSubmit.jsp"
	fm.submit();
}

//取消按钮
function edorTypeACReturn()
{
		initForm();
}

//返回按钮
function returnParent()
{
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0][0] = fm.all( 'EdorAcceptNo' ).value;
	arrSelected[0][1] = fm.all( 'ContNo' ).value;
	arrSelected[0][2] = '3';
	try{
		top.opener.afterQuery2( arrSelected );
	}catch(ex){}
		
  top.opener.getEdorItem();
	top.close();
}

//校验
function verify()
 {
  //校验投保人与被保人为同一人时的重要资料变更
  var strSql = "select count(*) from lCCont where ContNo='" + fm.ContNo.value + "' and AppntNo=InsuredNo";
  var arrResult = easyExecSql(strSql);  
  if (arrResult=="1") {
    if (fm.AppntSex.value!=oldSex || fm.AppntBirthday.value!=oldBirthday) {
      alert("因为该保单投保人与被保人为同一人，所以不能在此进行性别和出生日期的变更，请到被保人重要资料变更保全项目中进行！");
      return; 
    }
    if (fm.OccupationType.value!=oldOccupationType) 
    {
      alert("因为该保单投保人与被保人为同一人，所以不能在此进行职业类别的变更，请到被保人职业类别变更保全项目中进行！");
      return;
    }
  }  
  //以年龄和性别校验身份证号
  if (fm.all('IDType').value == "0") 
  var strChkIdNo = chkIdNo(fm.all('IDNo').value, fm.all('AppntBirthday').value, fm.all('AppntSex').value);
  if (strChkIdNo!="" && strChkIdNo!=null) {
    alert(strChkIdNo);
    return false;
  }  
  //提示账户信息变更
 if (fm.AppntName.value!=fm.oldName.value)
  {
    strSql = "select PayLocation from LCCont where ContNo='" + fm.ContNo.value + "'";
    arrResult = easyExecSql(strSql);
    //alert(arrResult);    
    if (arrResult=="0") {
      alert("该保单为银行转账方式，投保人姓名已更改，请做账户信息变更！");
    }
  }  
  return true;
}


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function afterSubmit( FlagStr, content ,Result,return2)
{
  showInfo.close();
  var tTransact=fm.all('fmtransact').value;
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  {
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
			turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
			turnPage.strQueryResult  = Result;
			//使用模拟数据源，必须写在拆分之前
			turnPage.useSimulation   = 1;  
			//查询成功则拆分字符串，返回二维数组
			var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,tTransact);
	if(tArr != null){
			turnPage.arrDataCacheSet =chooseArray(tArr,[5,7,8,9,15,17,12,13,31,32,11]);
		    fm.all('AppntNo').value = turnPage.arrDataCacheSet[0][0];
		    fm.all('AppntName').value =turnPage.arrDataCacheSet[0][1];
		    fm.all('AppntSex').value = turnPage.arrDataCacheSet[0][2];
		    fm.all('AppntBirthday').value = turnPage.arrDataCacheSet[0][3];
		    fm.all('Nationality').value = turnPage.arrDataCacheSet[0][4];
		    fm.all('Marriage').value =  turnPage.arrDataCacheSet[0][5];
		    fm.all('IDType').value = turnPage.arrDataCacheSet[0][6];
		    fm.all('IDNo').value =turnPage.arrDataCacheSet[0][7];
		    fm.all('OccupationCode').value = turnPage.arrDataCacheSet[0][8];
		    fm.all('OccupationType').value =turnPage.arrDataCacheSet[0][9]; 
		    fm.all('AddressNo').value =turnPage.arrDataCacheSet[0][10];  

			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
			turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
			//	alert(Result);
			turnPage.strQueryResult  = return2;
			//使用模拟数据源，必须写在拆分之前
			turnPage.useSimulation   = 1;  
			//查询成功则拆分字符串，返回二维数组
			var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,tTransact);
			turnPage.arrDataCacheSet =chooseArray(tArr,[8,9,10,11,12,13,14,15,4,5,6,7,16]);
			//alert(turnPage.arrDataCacheSet);
		    fm.all('HomeAddress').value =turnPage.arrDataCacheSet[0][0];
		    fm.all('HomeZipCode').value = turnPage.arrDataCacheSet[0][1];
		    
		    fm.all('HomePhone').value = turnPage.arrDataCacheSet[0][2];
		    fm.all('HomeFax').value = turnPage.arrDataCacheSet[0][3];
		    
		    fm.all('CompanyAddress').value =turnPage.arrDataCacheSet[0][4];
		    fm.all('CompanyZipCode').value =turnPage.arrDataCacheSet[0][5];
		    
		    fm.all('CompanyPhone').value =turnPage.arrDataCacheSet[0][6];
		    fm.all('CompanyFax').value =turnPage.arrDataCacheSet[0][7]; 
		    
		    fm.all('PostalAddress').value =turnPage.arrDataCacheSet[0][8];    
		    fm.all('ZipCode').value =turnPage.arrDataCacheSet[0][9];
		    
		    fm.all('Phone').value = turnPage.arrDataCacheSet[0][10];    
		    fm.all('Fax').value =turnPage.arrDataCacheSet[0][11];    
		    fm.all('Mobile').value =turnPage.arrDataCacheSet[0][12];
	}else{
//    fm.all('BankAccNo').value = arrResult[0][23+parseInt(i)];
//    fm.all('BankCode').value =arrResult[0][22+parseInt(i)];    
//    fm.all('AccName').value =arrResult[0][24+parseInt(i)];      
    
//    var oldSex =fm.all('AppntSex').value;
//    var oldBirthday = fm.all('AppntBirthday').value;
//    var oldOccupationType = fm.all('OccupationType').value;
//    var oldName=fm.all('AppntName').value;    
    		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
   			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
  }
}
function checkidtype()
{
	if(fm.IDType.value==""&&fm.IDNo.value!="")
	{
		alert("请先选择证件类型！");
		fm.IDNo.value=""; 
  }
}
function getBirthdaySexByIDNo(iIdNo)
{
	if(fm.all('IDType').value=="0")
	{
		fm.all('AppntBirthday').value=getBirthdatByIdNo(iIdNo);
		fm.all('AppntSex').value=getSexByIDNo(iIdNo);
	}	
}    