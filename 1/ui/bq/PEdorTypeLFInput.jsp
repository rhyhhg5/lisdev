<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2003-01-14 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <SCRIPT src="./PEdorTypeLF.js"></SCRIPT>
  <%@include file="PEdorTypeLFInit.jsp"%> 
</head>

<body  onload="initForm();" >
  <form action="./PEdorTypeLFSubmit.jsp" method=post name=fm target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= title > 
        批单号
      </TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 
        批改类型 
      </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>    
      <TD class = title >  
        保单号 
      </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=PolNo>
      </TD>   
    </TR>
  </TABLE> 
  
  <table>
   	<tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
      </td>
      <td class= titleImg>
        投保人详细信息
      </td>
   	</tr>
  </table> 
   
  <Div  id= "divLCPol" style= "display: ''"> 
  <table  class= common>
   	<TR  class= common>
  	  <TD class = title>
  	     险种编码
  	  </TD>
  	  <TD class= input>
  	    <Input class= "readOnly" readonly  name=RiskCode >
  	  </TD>      
    </TR>
    
    <TR>
      <TD class = title>
         投保人客户号
      </TD>
      <TD class= input>
        <Input class= "readOnly" readonly  name=AppntNo >
      </TD>     
      <TD class= title>
        投保人姓名
      </TD>
  	  <TD class= input>
  	    <Input class= "readOnly" readonly  name=AppntName >
  	  </TD>      
    </TR> 
       
    <TR>
      <TD class = title>
         被保人客户号
      </TD>
      <TD class= input>
        <Input class= "readOnly" readonly  name=InsuredNo >
      </TD>     
      <TD class= title>
        被保人姓名
      </TD>
  	  <TD class= input>
  	    <Input class= "readOnly" readonly  name=InsuredName >
  	  </TD>      
    </TR> 
    	
    <TR>
		  <TD class= title>
		    生效日期
		  </TD>
		  <TD class= input>
		    <Input class= "readOnly" readonly  name=CValidate >
		  </TD>      
      <TD class= title>
		    交至日期
		  </TD>
		  <TD class= input>
		    <Input class= "readOnly" readonly  name=PayToDate >
		  </TD>	 
		</TR>
      
    <TR>
       <TD class= title>
       总保费
       </TD>
       <TD class= input>
         <Input class= "readOnly" readonly  name=Prem >
       </TD>         
       <TD class= title>
       总保额
       </TD>
       <TD class= input>
         <Input class= "readOnly" readonly  name=Amnt >
       </TD>       
     </TR>
     
     <TR> 
  		 <TD class= title>
  		   可借款总额
  		 </TD>    
  		 <TD class= input>
  		    <Input class= "readOnly" readonly  name=LPSumMoney >
  		 </TD>		       
		       
     </TR>
    </Table>
    </Div>
    
    <table>
	   	<tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBonusGetMode);">
	      </td>
	      <td class= titleImg>
	        借款
	      </td>
	   	</tr>
    </table> 
  
    <DIV id= "divBonusGetMode" style= "display: ''">
	  <table class= common>
      <TR>	  
        <TD  class= title>
          借款金额
        </TD>
        <TD  class= input>
          <Input class=common name=SumMoney verify="借款金额|num">
        </TD>
        
      </TR>
      
      <TR>
        <TD  class= title>
          指定利率
        </TD>
        <TD  class= input>
          指定<input type="radio" value="YES" name="RChoose" onclick="showChoose()">
          不指定<input type="radio" value="NO" name="RChoose" checked onclick="showChoose()">
        </TD>
      </TR>
	  </table>
    </div>  
    
    <DIV id= "divRate" style= "display: 'none'">
	  <table class= common>
      <TR>	  
        <TD  class= title>
          借款利息方式
        </TD>
        <TD  class= input>
          <Input class=code name=InterestType ondblclick="return showCodeList('InterestType', [this]);" onkeyup="return showCodeListKey('InterestType', [this]);">
        </TD>
        <TD  class= title>
          利率类型
        </TD>
        <TD  class= input>
          <Input class=code name=InterestMode ondblclick="return showCodeList('InterestMode', [this]);" onkeyup="return showCodeListKey('InterestMode', [this]);">
        </TD>
      </TR>
      
      <TR>	  
        <TD  class= title>
          借款利率
        </TD>
        <TD  class= input>
          <Input class=common name=InterestRate >
        </TD>
        
      </TR>
	  </table>
    </div>
    
    <table class= common>
		<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeLFSave()">
	     	 </TD>
	     	 <TD  class= input width="26%"> 
	       	 <Input class= common type=Button value="返回" onclick="returnParent()">
	     	 </TD>
	    </TR>
    </table>
	
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
	 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
