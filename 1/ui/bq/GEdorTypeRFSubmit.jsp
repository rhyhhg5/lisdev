<%
//程序名称：GEdorTypeRFSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
  //个人批改信息
  System.out.println("---RF submit---");
  LPReturnLoanSchema tLPReturnLoanSchema = new LPReturnLoanSchema();
  LPReturnLoanSet tLPReturnLoanSet = new LPReturnLoanSet();
    
  //后面要执行的动作：添加，修改
  CErrors tError = null;                 
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result = "";
  String loanResult = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("---transact: " + transact);  
  
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  
  //批改信息
  tLPReturnLoanSchema.setPolNo(request.getParameter("GrpPolNo"));
  tLPReturnLoanSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPReturnLoanSchema.setEdorType(request.getParameter("EdorType"));
  System.out.println("tLPReturnLoanSchema:" + tLPReturnLoanSchema.encode());
  
  if (transact.equals("INSERT||MAIN")) 
  {
    String tGrid1[] = request.getParameterValues("LoanGrid1"); //得到第1列的所有值
    String tGrid5[] = request.getParameterValues("LoanGrid5"); //得到第5列的所有值
    
    int Count = tGrid5.length; //得到接受到的记录数
    for (int index=0; index<Count; index++)
    {
      LPReturnLoanSchema aLPReturnLoanSchema = new LPReturnLoanSchema();
      
      aLPReturnLoanSchema.setPolNo(request.getParameter("GrpPolNo"));
      aLPReturnLoanSchema.setEdorType(request.getParameter("EdorType"));
      aLPReturnLoanSchema.setEdorNo(request.getParameter("EdorNo"));
      aLPReturnLoanSchema.setLoanNo(tGrid1[index]);
      
      if (tGrid5[index] != null)
      {
        aLPReturnLoanSchema.setReturnMoney(tGrid5[index]);
        System.out.println("\nReturnMoney:" + tGrid5[index]);
      }
      else
      {
        aLPReturnLoanSchema.setReturnMoney("0");
      }
      
      tLPReturnLoanSet.add(aLPReturnLoanSchema);
    }
  }
  
  GEdorRFDetailUI tGEdorRFDetailUI = new GEdorRFDetailUI();

  try 
  {
    // 准备传输数据 VData
    VData tVData = new VData();  
  	
    //保存个人保单信息(保全)	
    tVData.addElement(tG);
    tVData.addElement(tLPReturnLoanSchema);
    tVData.add(tLPReturnLoanSet);
    
    boolean tag = tGEdorRFDetailUI.submitData(tVData,transact);
    
    if (tag == false) 
    {
      FlagStr = "Fail";
      Content = "操作失败!!";
    }
  } 
  catch(Exception ex) 
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "") 
  {
  	tError = tGEdorRFDetailUI.mErrors;
  	if (!tError.needDealError()) 
  	{                          
      Content = "保存成功";
  		FlagStr = "Success";
  		
  		if (transact.equals("QUERY||MAIN")) 
  		{
    		if (tGEdorRFDetailUI.getResult()!=null && tGEdorRFDetailUI.getResult().size()>0)
    	  {
    			Result = (String)tGEdorRFDetailUI.getResult().get(0);
    			loanResult = (String)tGEdorRFDetailUI.getResult().get(1);
    			
    		  if (Result==null || Result.trim().equals("")) 
    		  {
    				FlagStr = "Fail"; 
    				Content = "提交失败!!";
    		  }
    	  }
  	  }
    } 
    else  
    {
  		Content = " 保存失败，原因是:" + tError.getFirstError();
  		FlagStr = "Fail";
  	}
	}
	
  //添加各种预处理
  System.out.println("------------Result is------------\n" + Result);
  System.out.println("------------loanResult is------------\n" + loanResult);
%>   
                   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>", "<%=loanResult%>");
</script>
</html>

