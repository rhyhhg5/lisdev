var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mSql="";

//简单查询
function easyQuery()
{
	var startDate = fm.all('AppStartDate').value;
	var endDate = fm.all('AppEndDate').value;
	var tContType = fm.all('ContType').value; //业务类型
	var tContType1 = fm.all('ContType1').value; //保单类型
	var tManageCom = fm.all('ManageCom').value;
	var tEdorState = fm.all('EdorState').value;
	var tCcontno = fm.all('Contno').value;
	var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
	
	
	if (startDate == "")
	{
		alert("请输入受理时间起期！");
		return;
	}
	if (endDate == "")
	{
		alert("请输入受理时间止期！");
		return;
	}
	var t1 = new Date(startDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(endDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>92 )
    {
	  alert("查询起止时间不能超过3个月！")
	  return false;
    }
    
    var sqlpartEdorState = "";  
	if (tEdorState != "")
	{
		sqlpartEdorState = " and (select edorstate from lpedorapp where edoracceptno = a.edoracceptno) = '"+tEdorState+"' ";
	}
	
	var sqlpartCont = "";  
	var sqlpartGrpCont = "";  
	if (tCcontno != "")
	{
		sqlpartCont = " and contno = '"+tCcontno+"' ";
		sqlpartGrpCont = " and grpcontno = '"+tCcontno+"' ";
	}
	
	var sqlpartEdorNo = "";  
	if (tEdorAcceptNo != "")
	{
		sqlpartEdorNo = " and edoracceptno = '"+tEdorAcceptNo+"' ";
	}
	
	var sqlpartContType1 = "";  //业务类型
	var sqlpartContType2 = "";  //业务类型
	if (tContType != "")
	{
	  if (tContType=='1') //个
	  {
		sqlpartContType1 = " and (select salechnl from lccont where contno=a.contno union select salechnl from lbcont where contno=a.contno fetch first 1 row only) not in ('04','13') ";
		sqlpartContType2 = " and grpcontno = '00000000000000000000'";
	  }
	  else if (tContType=='2') //团
	  {
	    sqlpartContType1 = " and grpcontno <> '00000000000000000000'";
	    sqlpartContType2 = "";
	  }
	  else if (tContType=='3') //银
	  {
	    sqlpartContType1 = " and (select salechnl from lccont where contno=a.contno union select salechnl from lbcont where contno=a.contno fetch first 1 row only) in ('04','13') ";
	    sqlpartContType2 = " and grpcontno = '00000000000000000000'";
	  }
	  else
	  {
	    sqlpartContType1 = "";
	    sqlpartContType2 = "";
	  }
	}
  
  //个单保全
  var sqlPer = "select edoracceptno 批单号, (select edorname from lmedoritem where edorcode = a.edortype and appobj = 'I') 保全业务类型, "
             + "contno 保单号, (select appntname from lccont where contno = a.contno "
             + "union select appntname from lbcont where contno = a.contno fetch first 1 row only) 投保人, "
             + "(select appntno from lccont where contno = a.contno "
             + "union select appntno from lbcont where contno = a.contno fetch first 1 row only)  投保人客户号, "
             + "(select prtno from lccont where contno = a.contno "
             + "union select prtno from lbcont where contno = a.contno fetch first 1 row only) 保单印刷号, "
             + "(select case when salechnl in ('04','13') then '银保' else '个险' end from lccont where contno = a.contno "
             + "union select case when salechnl in ('04','13') then '银保' else '个险' end from lbcont where contno = a.contno fetch first 1 row only) 业务类型, "
             + "(select signdate from lccont where contno = a.contno "
             + "union select signdate from lbcont where contno = a.contno fetch first 1 row only) 承保时间, "
             + "edorappdate 保全受理日期, edorvalidate 保全生效日期, "
             + "(case when (select edorstate from lpedorapp where edoracceptno = a.edoracceptno) = '0' then a.modifydate else null end) 保全结案日期, getmoney 保全金额, "
             + "(case when a.getmoney >0 then '收费' when a.getmoney <0 then '付费' else null end) 收付费标识, "
             + "(select codename('appedorstate',edorstate) from lpedorapp where edoracceptno = a.edoracceptno) 批单状态, "
             + "managecom 分公司代码, (select name from ldcom where comcode = a.managecom) 分公司名称 "
             + "from lpedoritem a "
             + "where managecom like '" + tManageCom + "%%' "
             + "and (grpcontno = '00000000000000000000' or grpcontno is null or grpcontno = '') "
             + sqlpartEdorState
             + sqlpartCont 
             + sqlpartEdorNo 
             + sqlpartContType1
             + "and edorappdate between '"+startDate+"' and '"+endDate+"' ";
				
  //团单保全
  var sqlGrp = "select edoracceptno 批单号, (select edorname from lmedoritem where edorcode = a.edortype and appobj = 'G') 保全业务类型, "
             + "grpcontno 保单号, (select grpname from lcgrpcont where grpcontno = a.grpcontno "
             + "union select grpname from lbgrpcont where grpcontno = a.grpcontno fetch first 1 row only) 投保人, "
             + "(select appntno from lcgrpcont where grpcontno = a.grpcontno "
             + "union select appntno from lbgrpcont where grpcontno = a.grpcontno fetch first 1 row only)  投保人客户号, "
             + "(select prtno from lcgrpcont where grpcontno = a.grpcontno "
             + "union select prtno from lbgrpcont where grpcontno = a.grpcontno fetch first 1 row only) 保单印刷号, "
             + "'团险' 业务类型,"
             + "(select signdate from lcgrpcont where grpcontno = a.grpcontno "
             + "union select signdate from lbgrpcont where grpcontno = a.grpcontno fetch first 1 row only) 承保时间,  "
             + "edorappdate 保全受理日期, edorvalidate 保全生效日期, "
             + "(case when (select edorstate from lpedorapp where edoracceptno = a.edoracceptno) = '0' then a.modifydate else null end) 保全结案日期, getmoney 保全金额, "
             + "(case when a.getmoney >0 then '收费' when a.getmoney <0 then '付费' else null end) 收付费标识, "
             + "(select codename('appedorstate',edorstate) from lpedorapp where edoracceptno = a.edoracceptno) 批单状态, "
             + "managecom 分公司代码, (select name from ldcom where comcode = a.managecom) 分公司名称 "
             + "from lpgrpedoritem a "
             + "where managecom like '" + tManageCom + "%%' "
             + "and (grpcontno <> '00000000000000000000' and grpcontno is not null) "
             + sqlpartEdorState 
             + sqlpartGrpCont 
             + sqlpartEdorNo 
             + sqlpartContType2
             + "and edorappdate between '"+startDate+"' and '"+endDate+"' ";
             
	if (tContType1=='1')
	{
		var strSQL = sqlPer + " order by edoracceptno ";
	}
	else if (tContType1=='2')
	{
	    var strSQL = sqlGrp + " order by edoracceptno ";
	}
	else
	{
	    var strSQL = sqlGrp + " union all " + sqlPer + " order by 批单号 ";
	}
	mSql=strSQL;
	turnPage1.queryModal(strSQL, CodeGrid);  
	if( CodeGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}
	showCodeName(); 
}

function easyPrint()
{	
	if( CodeGrid.mulLineCount == 0)
	{
		alert("没有需要打印的信息");
		return false;
	}
	fm.all('strsql').value=mSql;
	fm.submit();
}
