<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2005.3.31 20:00:00
//创建人  ：LHS
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeTG.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeTGInit.jsp"%>
  
<title>投保人协议退保！</title>
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeTGSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 批单号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 批改类型 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=EdorType type=hidden>
        	<input class = "readonly" readonly name=EdorTypeName>
        </TD>
       
        <TD class = title > 集体保单号 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=GrpContNo>
        </TD>   
      </TR>
      <TR  class= common> 
  	  		  <TD class= title> 退保原因 </TD>
            <TD class= input>
          	   <input class="codeNo" name="reason_tb" readonly ondblclick="return showCodeList('reason_tb', [this,reason_tbName], [0,1]);" onkeyup="return showCodeListKey('reason_tb', [this,reason_tbName],[0,1]);" verify="退保原因|not null"><input class="codeName" name="reason_tbName" readonly elementtype="nacessary">
            </TD>
          </TR>
    </TABLE> 
  
        <!--公共账户信息-->   
        <Div  id= "divPolInfo" style= "display: ''">
          <table>
            <tr>
              <td>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpPolGrid);">
              </td>
              <td class= titleImg>
                公共账户协议金额
              </td>
            </tr>
          </table>
	        <Div  id= "divLPGrpPolGrid" style= "display: ''">
            <table  class= common>
              <tr  class= common>
                <td text-align: left colSpan=1>
  	      		    <span id="spanGrpAccGrid" ></span> 
  	      	    </td>
  	      	  </tr>
            </table>
            <Div id= "divPage" align="center" style= "display: 'none' ">
      	      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
      	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
      	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
      	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 
	          </Div>				
  	      </div>
        </div>
        
        <!--个人账户信息-->   
        <Div  id= "divAccInfo" style= "display: ''">
          <table>
            <tr>
              <td>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPAccGrid);">
              </td>
              <td class= titleImg>
                个人账户协议金额
              </td>
            </tr>
          </table>
	        <Div  id= "divPAccGrid" style= "display: ''">
            <table  class= common>
              <tr  class= common>
                <td text-align: left colSpan=1>
  	      		    <span id="spanPAccGrid" ></span> 
  	      	    </td>
  	      	  </tr>
            </table>
            <Div id= "divPage2" align="center" style= "display: 'none' ">
      	      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
      	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
      	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
      	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
	          </Div>				
  	      </div>
        </div>
        
        <Input type =button name=budgetButton class=cssButton value="退保试算" onclick="edorCalZTTest()">
 		    <Input name="save" type =button class=cssButton value="保  存" onclick="edorTypeXTSave()">
 		    <Input name="goBack" type =button class=cssButton value="返  回" onclick="returnParent()">
      </Div>
	  </Div>
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="ContType" name="ContType">
    <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
    <input type=hidden id="EdorValiDate" name="EdorValiDate">
    <input type=hidden id="addrFlag" name="addrFlag">
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
