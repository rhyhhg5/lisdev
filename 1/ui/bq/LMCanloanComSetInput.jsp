<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>   
<Script>
var comCode = <%=tG.ComCode%>
var msql=" 1 and comcode not in ( select #86# from dual union select #8600# from dual)";
</Script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LMCanloanComSetInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>贷款比例机构配置 </title>
  <%@include file="LMCanloanComSetInit.jsp"%>
</head>
<body  onload="initForm();">
  <form action="./LMCanloanComSetSubmit.jsp" method=post name=fm target="fraSubmit" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请选择需要设置的机构
			</td>
		</tr>
	</table>
	
    <table  class= common align=center>      
        <TR>
  		    <TD  class= title> 管理机构 </TD>
          <TD  class= input >
            <Input class= "codeno" name=aManageCom readonly="readonly" verify="管理机构|notnull" ondblclick="return showCodeList('comcode10',[this,ManageComName],[0,1],null,msql,1);" onkeyup="return showCodeListKey('comcode10',[this,ManageComName],[0,1],null,msql,1);" ><Input class=codename readonly name=ManageComName>
            <font style=color:red;valign:right> *</font>
          </TD>
		<td class="title">
			险种
		</td>
		<td class="input"><Input class=codeNo verify="险种编码|notnull" name=RiskCode readOnly ondblclick="return showCodeList('loanriskcode',[this,RiskName], [0,1],null,null,null,1);" onkeyup="return showCodeListKey('loanriskcode',[this,RiskName], [0,1],null,null,null,1);"><Input class=codename name=RiskName readonly >
		<font style=color:red;valign:right> *</font> </td>
		</TR>
		<tr>
		  <TD  class= title> 最小保费金额 </TD>
          <TD  class= input >
            <Input class=common  name=MinPremLimit verify="最小保费金额|notnull">
            <font style=color:red;valign:right> *</font>
          </TD>
  		    <TD  class= title> 最大保费金额 </TD>
          <TD  class= input >
            <Input class=common  name=MaxPremLimit >
          </TD>	
		</tr>
		<tr>
  		    <TD  class= title> 贷款额度比例 </TD>
          <TD  class= input colspan="3">
            <Input class=common  name=CanLoanRate verify="贷款额度比例|notnull">
            <font style=color:red;valign:right> *</font>
          </TD>		
		</tr>
		<tr>
			<td class=button>
				<input value="查   询" type=button onclick="queryCanLoanRate();"class=cssButton >
				<input value="重   置" type=button onclick="cancleConfig();"class=cssButton >
			</td>
		</tr>	
    </table>
			<Div id="divLMCanloanComInfo" style="display: ''">
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divLMCanloanComGrid);">
						</td>
						<td class=titleImg>
							查询结果
						</td>
					</tr>
				</table>
				 <!-- 信息（列表） -->
				  <div id="divLMCanloanComGrid" style="display:''">
					<table class=common>
				      <tr class=common>
					    <td text-align:left colSpan=1><span id="spanLMCanloanComGrid"></span></td>
					  </tr>
					</table>
				  </div>
				<Div  id= "divturnPage" style="display:none">
					<div align="center">
				      <INPUT VALUE="首页" class = cssButton  TYPE=button onclick="turnPage.firstPage();"> 
				      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage();"> 					
				      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage();"> 
				      <INPUT VALUE="尾页" class = cssButton TYPE=button onclick="turnPage.lastPage();"> 	
				    </div>
				</DIV>
			</Div>
          <INPUT VALUE="添加特殊配置" class=cssButton TYPE=button onclick="saveConfig();"> 
          <INPUT VALUE="删除特殊配置" class=cssButton TYPE=button onclick="deleteConfig();"> 
          
          <input type="hidden" name="transact"/>
          <input type=hidden name="DManageCom"/>
          <input type="hidden" name="DRiskCode" >
          <input TYPE="hidden" NAME="DMinPremLimit">
          <input type="hidden" name="DMaxPremLimit">
          <input type="hidden" name="DCanLoanRate">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
