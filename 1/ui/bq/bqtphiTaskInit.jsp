<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
    
}


function initForm()
{

}

// 催收记录的初始化
function initBonusGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="SerialNo";

      iArray[2]=new Array();
      iArray[2][0]="管理机构";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="TaskNo"; 
      
      iArray[3]=new Array();
      iArray[3][0]="红利应分配时间";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="ContNo";

      iArray[4]=new Array();
      iArray[4][0]="红利领取方式";         		//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="分配年度";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="分配金额";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="红利领取方式代码";         		//列名
      iArray[7][1]="0px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="险种号";         		//列名
      iArray[8][1]="0px";            		//列宽
      iArray[8][2]=200;            	        //列最大值
      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
	  BonusGrid = new MulLineEnter( "fm" , "BonusGrid" ); 
      //这些属性必须在loadMulLine前
      BonusGrid.mulLineCount =0;   
      BonusGrid.displayTitle = 1;
      BonusGrid.hiddenPlus = 1;
      BonusGrid.hiddenSubtraction = 1;
      //LjsGetGrid.locked = 1;
      BonusGrid.canChk = 0;
      BonusGrid.canSel = 1;
      BonusGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        //alert("BonusPrintInit.jsp-->initLjsGetGrid函数中发生异常:初始化界面错误!");
      }
}

</script>