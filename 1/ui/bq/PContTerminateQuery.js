var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var mSql="";

//简单查询
function easyQuery()
{
	var startDate = fm.all('StartDate').value;
	var endDate = fm.all('EndDate').value;
	var tSaleChnl = fm.all('SaleChnl').value; //保单类型
	var tManageCom = fm.all('ManageCom').value;//管理机构
	var tContNo = fm.all('ContNo').value;//保单号
	var tAgentCode = fm.all('AgentCode').value;//业务员代号
	var tAppntNo = fm.all('AppntNo').value;//投保人客户号
	var tPrintCount = fm.all('PrintCount').value;//打印标志
	
	//管理机构校验
	if(tManageCom == "" || tManageCom == null)
	{
		alert("请选择管理机构！");
		return false;
	}
	if(tManageCom == "86")
    {
      if(!confirm("管理机构选择“总公司”，查询速度会很慢，是否继续？"))
      {
        return false;
      }
    }
   	
	//失效日期起期校验
	if (startDate == "")
	{
		alert("请输入失效日期起期！");
		return;
	}
	//失效日期止期校验
	if (endDate == "")
	{
		alert("请输入失效日期止期！");
		return;
	}
	var t1 = new Date(startDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(endDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>31 )
    {
	  alert("查询起止时间不能超过1个月！")
	  return false;
    }
    var wherePar="";
    //限制管理机构
    if(!(tManageCom == "" || tManageCom == null))
    {
      wherePar+="a.ManageCom like '"+tManageCom+"%' and " ; 
    }
    //限制保单类型
    if( !(tSaleChnl == "" || tSaleChnl == null))
    {
       if(tSaleChnl==1)
       {
          wherePar+="a.SaleChnl not in ('04','13') and ";
       }
       else if(tSaleChnl==2)
       {
          wherePar+="a.SaleChnl in ('04','13') and "; 
       }
       else
       {}
      
    }
    //限制保单号
    if( !(tContNo == "" ||  tContNo == null))
    {
       wherePar+="a.ContNo='"+tContNo+"' and " ;               
    }
    //限制业务员代号 
     if( !(tAgentCode == "" ||  tAgentCode == null))
    {
       wherePar+= "a.AgentCode= getAgentCode('"+tAgentCode+"') and ";             
    }
    //限制投保人客户号   
     if( !(tAppntNo == "" ||  tAppntNo == null))
    {
       wherePar+= "a.AppntNo='"+ tAppntNo+"' and " ;                       
    }
    //限制打印标志
     if( !(tPrintCount == "" ||  tPrintCount == null))
    {
       if(tPrintCount==2)
       {
          tPrintCount="1','0";
       }
       wherePar+= "(select max(stateflag) from LOPRTManager where code='210' and otherNo = a.ContNo) in ('"+ tPrintCount+"')" ;                       
    }
    
        
   var strSQL = "select "
			  + "(select Name from ldcom where comcode=a.ManageCom) , " //管理机构名字
			  + "(select Name from labranchgroup where AgentGroup=a.AgentGroup), "//营销部门
			  + "a.ContNo, "//保单号
			  + "a.AppntName, "//投保人
			  + "(select (case when c.Phone is null then c.homephone else c.phone end) from lcaddress c,lcappnt d where c.CustomerNo=d.AppntNo and c.AddressNo=d.AddressNo "
			  + " and d.ContNo = a.ContNo fetch first 1 row only ), "//投保人电话
			  + "(select c.Mobile from lcaddress c,lcappnt d where c.CustomerNo=d.AppntNo and c.AddressNo=d.AddressNo "
			  + " and d.ContNo = a.ContNo fetch first 1 row only), " //投保人移动电话
			  + "(select c.PostalAddress from lcaddress c,lcappnt d where c.CustomerNo=d.AppntNo and c.AddressNo=d.AddressNo "
			  + " and d.ContNo = a.ContNo fetch first 1 row only),"//投保人的联系地址
			  + "(select sum(prem) from LCPol where ContNo = a.ContNo and ContType = '1' "
			  + "and exists (select 1 from lmriskapp where riskcode = LCPol.riskcode and riskperiod = 'L')), " //（期缴保费）
			  + "a.PaytoDate, "//（交至日）
			  + "codename('paymode',a.PayMode), "//缴费方式
			  + "(select max(prtSeq) from LOPRTManager where otherNo = a.ContNo and code = '210'),"//（永久失效通知书号）
			  + "(case when a.SaleChnl='04' then '银保' when a.SaleChnl='13' then '银保' else '个险' end), "//（保单类型）
			  + "getUniteCode(a.AgentCode), "//代理人编码
			  + "(select Mobile from laagent where AgentCode=a.AgentCode), "//代理人电话
			  + "(select Name from laagent where AgentCode=a.AgentCode), "//代理人姓名
			  + "GETSTATEDATE(a.ContNo,'09','Terminate'), "//(永久失效日期)
			  + "(case when (select agentstate from laagent where agentcode=a.agentcode)>='06' then '孤儿单' else '业务员在职' end) "//（保单归属状态）
			  + "from lccont a  "
			  + "where a.conttype = '1' and a.stateflag='3' "
			  + "and exists(select 1 from LCPol where ContNo = a.ContNo and ContType = '1' "
			  + "and exists (select 1 from lmriskapp where riskcode = LCPol.riskcode and riskperiod = 'L')) "//保证长期险
			  + "and GETSTATEDATE(a.ContNo,'09','Terminate') >= '" + startDate 
			  + "' and GETSTATEDATE(a.ContNo,'09','Terminate') <= '" + endDate + "' and " //限制起至时间	
			  + wherePar;
			  //GETSTATEDATE(保单号，LCContState-StateReason，LCContState-StateType) ，返回LCContState-Makedate
	mSql=strSQL;
	turnPage1.queryModal(strSQL, PContTerminateGrid);  
	if( PContTerminateGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}
	
	fm.sql.value =  strSQL;
	showCodeName(); 
}
//下载清单
function easyPrint()
{	
	if( PContTerminateGrid.mulLineCount == 0)
	{
		alert("没有需要打印的信息");
		return false;
	}
	fm.all('sql').value=mSql;
	fm.action="./PContTerminatePrint.jsp";
	fm.submit();
}
//打印PDF通知书
function newprintInsManage()
{
	if (PContTerminateGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
		
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PContTerminateGrid.mulLineCount; i++)
	{
		if(PContTerminateGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	} 
	
	//保单号
	var ContNo=PContTerminateGrid.getRowColData(tChked[0],3);
	fm.action = "../uw/PDFPrintSave.jsp?Code=210&RePrintFlag=&MissionID=&SubMissionID=&StandbyFlag2=&PrtFlag=&fmtransact=PRINT&OtherNo="+ContNo;
	fm.submit(); 
	
}
//批量打印
function printAllNotice()
{
	if (PContTerminateGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PContTerminateGrid.mulLineCount; i++)
	{	
		if(PContTerminateGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}

	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.action="../bq/PContTerminatePrintAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.action="../bq/PContTerminatePrintForBatPrt.jsp";
		fm.submit();
	}

}
function afterSubmit2(FlagStr,Content){
    alert(Content);
}
















