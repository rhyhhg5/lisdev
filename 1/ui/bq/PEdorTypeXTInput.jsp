<%@page contentType="text/html;charset=GBK" %>
<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2005-4-6
//创建人  ：LHS
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeXT.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeXTInit.jsp"%>
<title>投保人协议退保！</title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeXTSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
     
      <TD class = title > 个人保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE> 
  
  <Div  id= "divLPAppntDetail" style= "display: ''">
  <table>
    	<tr>
    		<td class= titleImg>
    			 个人保单资料
    		</td>
    	</tr>
   </table>
    
   <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
       <TR>
          <TD  class= title8>
            个人客户号
          </TD>
          <TD  class= input8>
            <Input class= "readonly" name=AppntNo readonly >
          </TD>  
          <TD  class= title8>
            客户姓名
          </TD>
          <TD  class= input8 colspan="3">
            <Input class= "readonly" name=AppntName readonly >
          </TD>     
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            签单日期
          </TD>
          <TD  class= input8>
            <Input class="readonly" name=SignDate readonly >
          </TD>
      
          <TD  class= title8>
            生效日期
          </TD>
          <TD  class= input8>
            <Input class="readonly" name=CValiDate readonly >
          </TD>
          <TD  class= title8>
            签单机构
          </TD>
          <TD  class= input8>
            <Input class= "readonly" name=SignCom readonly >
          </TD>       
        </TR>
        <TR  class= common>
          <TD  class= title8>
            期交保费
          </TD>
          <TD  class= input8>
            <Input class= "readonly" name=Prem readonly >
          </TD>
          <TD  class= title8>
            保额
          </TD>
          <TD  class= input8>
            <Input class= "readonly" name=Amnt readonly >
          </TD>  
          <TD  class= title8>
            保全生效日
          </TD>
          <TD  class= input8>
            <Input class= "readonly" name=EdorValiDate readonly >
          </TD>
        </TR>
        <TR  class= common> 
  			<TD class= title> 退保原因 </TD>
        <TD class= input>
        	<input class="codeNo" name="reason_tb" readOnly verify="退保原因|notnull&code:reason_tb&len<=10" ondblclick="showCodeList('reason_tb',[this,reason_tbName],[0,1]);" onkeyup="showCodeListKey('reason_tb',[this,reason_tbName],[0,1]);"><Input class="codeName" name=reason_tbName readonly elementtype=nacessary>
        </TD>
        </TR>
      </table>
      <br>
			<Div  id= "divPolInfo" style= "display: ''">
				<table>
				  <tr>
				      <td>
				          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
				      </td>
				      <td class= titleImg>
				          保单险种信息
				      </td>
				  </tr>
				</table>
				<Div  id= "divPolGrid" style= "display: ''">
				<table  class= common>
				<tr  class= common>
						<td text-align: left colSpan=1>
						<span id="spanPolGrid" >
						</span> 
				  	</td>
				</tr>
				</table>					
			</div>
    </DIV>
    <Div  id= "divPremInfo" style= "display: 'none'">
      <table  class= common>
       <TR class= common>
          <TD  class= common >
            <Input class= "readonly" name="TotalPremInfo" value = "" readonly style = "width:100%">
            <input type=hidden id="TotalPrem" name="TotalPrem">
          </TD>  
        </TR>
       </table>
    </DIV>
        <br>
    <Div id= "divSYInfo" style= "display: 'none'">
		<table class=common>
        <td class="title"> 补交税收优惠额度： </td>
        <td class="input">
        	<input class = "text"  name="TaxMoney">
       </table> 
    </DIV>
      <br>
      <Input type =button name=budgetButton class=cssButton value="退保试算" onclick="pEdorCalZTTest()">
     	<Input type =button name=save1 class=cssButton value="算  费" onclick="calFee()">
     	<Input type =button name=save class=cssButton value="保存申请" onclick="edorTypeXTSave()">
    	<!--  <Input type =button class=cssButton value="取    消" onclick="checkULIPrem()">  -->
     	<Input type =button name=goBack class=cssButton value="返    回" onclick="returnParent()">
    </Div>
	</Div>
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	 <input type=hidden id="addrFlag" name="addrFlag">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
