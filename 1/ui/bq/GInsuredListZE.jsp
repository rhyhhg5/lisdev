<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2006-02-07
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  XmlExport txmlExport = null;   
  
  String tZipFile="";
  String outname="";
  String outpathname="";
  
  String edorAcceptNo = request.getParameter("edorAcceptNo");
  System.out.println("-----------------"+edorAcceptNo);  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  PrtGrpInsuredListZEUI tPrtGrpInsuredListZEUI = new PrtGrpInsuredListZEUI(tG,edorAcceptNo); 
  if(!tPrtGrpInsuredListZEUI.submitData())
  {          
     	operFlag = false;
     	Content = "打印发送错误"; 
  }
  else
  {             
    VData mResult = tPrtGrpInsuredListZEUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    
    if(txmlExport==null)
    {
    	operFlag=false;
    	Content="没有得到要显示的数据文件";	  
    }
  }
	
	if (operFlag==true)
	{
		System.out.println("处理报表");
	    Readhtml rh=new Readhtml();
	    rh.XmlParse(txmlExport.getInputStream());
	    String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
		String temp=realpath.substring(realpath.length()-1,realpath.length());
		if(!temp.equals("\\"))
		{
			realpath=realpath+"/"; 
		}
		String templatename=rh.getTempLateName();//模板名字
		String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
		System.out.println("*********************templatepathname= " + templatepathname);
		System.out.println("************************realpath="+realpath);

		String date=PubFun.getCurrentDate().replaceAll("-","");  
		String time=PubFun.getCurrentTime3().replaceAll(":","");
		
		outname="GInsuredListZE"+edorAcceptNo+".xls";
		outpathname=realpath+"vtsfile\\"+outname;
		
		rh.setReadFileAddress(templatepathname);
		rh.setWriteFileAddress(outpathname);
		rh.start("vtsmuch");
		
		try{
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
		  	outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
		  	outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	    }catch(Exception ex)
	    {
	     	ex.printStackTrace();
	    }
	    String[] InputEntry = new String[1];
	    InputEntry[0] = outpathname;
	    //tZipFile = realpath+"vtsfile\\"+ StrTool.replace(outname,".xls",".zip");
		System.out.println("tZipFile == " + outpathname);
	    //rh.CreateZipFile(InputEntry, tZipFile);
	}
	else
	{
    	FlagStr = "Fail";
    	%>
  		<%=Content%>  
  		<%
	}
	
%>
<html>
<a href="../f1print/download.jsp?filename=<%=outname%>&filenamepath=<%=outpathname%>">点击下载</a>
 
</html>