<%
//GEdorTypeBCInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">  
//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y

   	detailQueryClick();
}

function initInpBox()
{ 
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
	var flag;
	try
	{
		flag = top.opener.fm.all('loadFlagForItem').value;
	}
	catch(ex)
	{
		flag = "";	
	}
	
	if(flag == "TASK")
	{
	  fm.saveButton.style.display = "none";
	  fm.cancelButton.style.display = "none";
		fm.returnButton.style.display = "none";
	}
	
  try
  {
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('GrpPolNo').value = top.opener.fm.all('GrpPolNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    
    showOneCodeName("EdorCode", "EdorTypeName");
  }
  catch(ex)
  {
    alert("在GEdorTypeBCInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在GEdorTypeBCInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox(); 
    initDiv();
    initLCPolGrid();
    initLCBnfGrid();
    initQuery();  
  }
  catch(re)
  {
    alert("GEdorTypeBCInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//保单信息列表
function initLCPolGrid()
{
    var iArray = new Array();
    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）             
      iArray[0][1]="30px";         			//列宽                                                     
      iArray[0][2]=10;          			//列最大值                                                 
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[1]=new Array();                                                                                       
      iArray[1][0]="保单号";    			//列名                                                     
      iArray[1][1]="150px";            			//列宽                                                     
      iArray[1][2]=100;            			//列最大值                                                 
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[2]=new Array();                                                                                       
      iArray[2][0]="险种编码";         		//列名                                                     
      iArray[2][1]="150px";            			//列宽                                                     
      iArray[2][2]=100;            			//列最大值                                                 
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                          
      iArray[3]=new Array();                                                                                       
      iArray[3][0]="起保日期";         		//列名                                                     
      iArray[3][1]="100px";            			//列宽                                                     
      iArray[3][2]=100;            			//列最大值                                                 
      iArray[3][3]=0;             			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[4]=new Array();                                                                                       
      iArray[4][0]="被保人客户号";         		//列名                                                     
      iArray[4][1]="150px";            			//列宽                                                     
      iArray[4][2]=100;            			//列最大值                                                 
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用        
                                                                                                                   
      iArray[5]=new Array();                                                                                       
      iArray[5][0]="被保人姓名";         		//列名                                                     
      iArray[5][1]="100px";            			//列宽                                                     
      iArray[5][2]=100;            			//列最大值                                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用        
                                                                                                                   
      iArray[6]=new Array();                                                                                       
      iArray[6][0]="投保人姓名";         			//列名                                                     
      iArray[6][1]="100px";            			//列宽                                                     
      iArray[6][2]=100;            			//列最大值                                                 
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用        

      LCPolGrid = new MulLineEnter( "fm" , "LCPolGrid" ); 
      //这些属性必须在loadMulLine前
      LCPolGrid.mulLineCount = 10;   
      LCPolGrid.displayTitle = 1;
      LCPolGrid.canSel=1;
 			LCPolGrid.selBoxEventFuncName ="reportDetailClick";      //这些操作必须在loadMulLine后面
      LCPolGrid.loadMulLine(iArray);  
      LCPolGrid.detailInfo="单击显示详细信息";
      }
      catch(ex)
      {
        alert(ex);
      }
}
// 信息列表的初始化
function initLCBnfGrid()
{
    var iArray = new Array();
      
      try
      {
	      var iArray = new Array();
		    iArray[0]=new Array();
		    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
		    iArray[0][1]="30px";		//列宽
		    iArray[0][2]=10;			//列最大值
		    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许
		  
		    iArray[1]=new Array();
		    iArray[1][0]="受益人类别"; 		//列名
		    iArray[1][1]="80px";		//列宽
		    iArray[1][2]=40;			//列最大值
		    iArray[1][3]=2;			//是否允许输入,1表示允许，0表示不允许
		    iArray[1][4]="BnfType";
		    iArray[1][9]="受益人类别|notnull&code:BnfType";
		    
		    iArray[2]=new Array();
		    iArray[2][0]="受益人客户号"; 		//列名
		    iArray[2][1]="150px";		//列宽
		    iArray[2][2]=40;			//列最大值
		    iArray[2][3]=1;			//是否允许输入,1表示允许，0表示不允许
		   
		    iArray[3]=new Array();
		    iArray[3][0]="受益人姓名"; 	//列名
		    iArray[3][1]="80px";		//列宽
		    iArray[3][2]=40;			//列最大值
		    iArray[3][3]=1;			//是否允许输入,1表示允许，0表示不允许
		    iArray[3][9]="受益人姓名|len<=20";//校验
		   
		    iArray[4]=new Array();
		    iArray[4][0]="性别"; 	//列名
		    iArray[4][1]="30px";		//列宽
		    iArray[4][2]=100;			//列最大值
		    iArray[4][3]=2;			//是否允许输入,1表示允许，0表示不允许
		    iArray[4][4]="sex";
		    iArray[4][9]="性别|code:sex";//校验
		  
		    iArray[5]=new Array();
		    iArray[5][0]="出生日期"; 		//列名
		    iArray[5][1]="80px";		//列宽
		    iArray[5][2]=80;			//列最大值
		    iArray[5][3]=1;			//是否允许输入,1表示允许，0表示不允许
		    iArray[5][9]="出生日期|date";
		  
		  	iArray[6]=new Array();
		    iArray[6][0]="证件类型"; 		//列名
		    iArray[6][1]="60px";		//列宽
		    iArray[6][2]=80;			//列最大值
		    iArray[6][3]=2;			//是否允许输入,1表示允许，0表示不允许
		    iArray[6][4]="IDType";
		    iArray[6][9]="证件类型|code:IDType";
		    
		    iArray[7]=new Array();
		    iArray[7][0]="证件号码"; 		//列名
		    iArray[7][1]="150px";		//列宽
		    iArray[7][2]=80;			//列最大值
		    iArray[7][3]=1;			//是否允许输入,1表示允许，0表示不允许
		    iArray[7][9]="证件号码|len<=20";
	  
		    iArray[8]=new Array();
		    iArray[8][0]="与被保人关系"; 	//列名
		    iArray[8][1]="90px";		//列宽
		    iArray[8][2]=100;			//列最大值
		    iArray[8][3]=2;			//是否允许输入,1表示允许，0表示不允许
		    iArray[8][4]="Relation";
		    iArray[8][9]="与被保人关系|code:Relation";
		    
		    iArray[9]=new Array();
		    iArray[9][0]="受益级别"; 		//列名
		    iArray[9][1]="60px";		//列宽
		    iArray[9][2]=80;			//列最大值
		    iArray[9][3]=2;			//是否允许输入,1表示允许，0表示不允许
		    iArray[9][4]="OccupationType";
		    iArray[9][9]="受益级别|code:OccupationType";
		  
		    iArray[10]=new Array();
		    iArray[10][0]="受益份额"; 		//列名
		    iArray[10][1]="60px";		//列宽
		    iArray[10][2]=80;			//列最大值
		    iArray[10][3]=1;			//是否允许输入,1表示允许，0表示不允许
		    iArray[10][9]="受益份额|num&len<=10";
		            
		  
		    iArray[11]=new Array();
		    iArray[11][0]="联系地址"; 		//列名
		    iArray[11][1]="400px";		//列宽
		    iArray[11][2]=240;			//列最大值
		    iArray[11][3]=1;			//是否允许输入,1表示允许，0表示不允许
		    iArray[11][9]="联系地址|len<=80";
		  
		    iArray[12]=new Array();
		    iArray[12][0]="邮编"; 		//列名
		    iArray[12][1]="60px";		//列宽
		    iArray[12][2]=80;			//列最大值
		    iArray[12][3]=1;			//是否允许输入,1表示允许，0表示不允许
		    iArray[12][9]="邮编|zipcode";
		  
		    iArray[13]=new Array();
		    iArray[13][0]="电话"; 		//列名
		    iArray[13][1]="100px";		//列宽
		    iArray[13][2]=80;			//列最大值
		    iArray[13][3]=1;			//是否允许输入,1表示允许，0表示不允许
		    iArray[13][9]="电话|len<=18";
		    
		    iArray[14]=new Array();
		    iArray[14][0]="户口所在地"; 		//列名
		    iArray[14][1]="400px";		//列宽
		    iArray[14][2]=240;			//列最大值
		    iArray[14][3]=1;			//是否允许输入,1表示允许，0表示不允许
		    iArray[14][9]="户口所在地|len<=80";

      LCBnfGrid = new MulLineEnter( "fm" , "LCBnfGrid" ); 
      //这些属性必须在loadMulLine前
      LCBnfGrid.mulLineCount = 10;   
      LCBnfGrid.displayTitle = 1;
      LCBnfGrid.loadMulLine(iArray);  
      LCBnfGrid.detailInfo="单击显示详细信息";
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initQuery()
{	
	 var i = 0;
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.all('fmtransact').value = "QUERY||MAIN";
		mFlag = '0';
		//alert("----begin---");
		//showSubmitFrame(mDebug);
		fm.submit();	  	 	 
}

function CondQueryClick()
{
		var i = 0;
	 	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.all('fmtransact').value = "QUERY||MAIN";
		mFlag = '1';
		var tPolNo = fm.all('PolNo').value;
		if (tPolNo==null&&tPolNo=='')
			mFlag ='0';
		//alert("----begin---");
		//showSubmitFrame(mDebug);
		fm.submit();
}

function detailQueryClick()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
	fm.all('fmtransact').value = "QUERY||DETAIL";
	
	var tSel=LCPolGrid.getSelNo();
	
	if (tSel==0||tSel==null)
		alert("不能是空记录!");
	else
	{
		var tPolNo =LCPolGrid.getRowColData(tSel-1,1);
		//alert(tPolNo);
		fm.all('PolNo').value =tPolNo;
		//showSubmitFrame(mDebug);
		fm.submit();
	}
}
function initDiv()
{
	//divLPBnfDetail.style.display ='none';
  //divDetail.style.display='none';
 	divBnfInfo.style.display ='none';

}

</script>