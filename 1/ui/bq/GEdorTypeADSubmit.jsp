<%
//程序名称：GEdorTypeADSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----ADsubmit---");
  LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
  LPGrpAppntSchema tLPGrpAppntSchema = new LPGrpAppntSchema();
  LPGrpAddressSchema tLPGrpAddressSchema = new LPGrpAddressSchema();
  LPGrpContSchema tLPGrpContSchema= new LPGrpContSchema();  
    PGrpEdorADDetailUI tPGrpEdorADDetailUI = new  PGrpEdorADDetailUI();
//  PGrpEdorADDetailUI tPGrpEdorADDetailUI=new PGrpEdorADDetailUI();
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String fmAction = "";
  String Result="";
  String addrFlag="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  fmAction = request.getParameter("fmAction");
  addrFlag=request.getParameter("addrFlag");
  System.out.println("------fmAction:"+addrFlag);
	GlobalInput tG = new GlobalInput();
	    System.out.println("------------------begin ui");
	tG = (GlobalInput)session.getValue("GI");
	
//	tLPGrpEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
	tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
	tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
  tLPGrpEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
//	tLPGrpEdorItemSchema.setEdorAppNo(request.getParameter("EdorAppNo"));	
	
  
        //团单投保人信息  LCGrpAppnt
      tLPGrpAppntSchema.setEdorNo(request.getParameter("EdorNo"));
      tLPGrpAppntSchema.setEdorType(request.getParameter("EdorType"));
      tLPGrpAppntSchema.setGrpContNo(request.getParameter("GrpContNo"));     //集体投保单号码
	    tLPGrpAppntSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码	
	    tLPGrpAppntSchema.setName(request.getParameter("GrpName"));
	    tLPGrpAppntSchema.setAddressNo(request.getParameter("GrpAddressNo"));
	    tLPGrpAppntSchema.setZipCode(request.getParameter("GrpZipCode"));
  //团单地址信息
  tLPGrpAddressSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPGrpAddressSchema.setEdorType(request.getParameter("EdorType"));
  tLPGrpAddressSchema.setCustomerNo(request.getParameter("GrpNo"));
	tLPGrpAddressSchema.setAddressNo(request.getParameter("GrpAddressNo"));
	tLPGrpAddressSchema.setGrpAddress(request.getParameter("GrpAddress"));
  tLPGrpAddressSchema.setGrpZipCode(request.getParameter("GrpZipCode"));
	tLPGrpAddressSchema.setLinkMan1(request.getParameter("LinkMan1"));
	tLPGrpAddressSchema.setPhone1(request.getParameter("Phone1"));
  tLPGrpAddressSchema.setE_Mail1(request.getParameter("E_Mail1"));
  tLPGrpAddressSchema.setLinkMan2(request.getParameter("LinkMan2"));
	tLPGrpAddressSchema.setPhone2(request.getParameter("Phone2"));
  tLPGrpAddressSchema.setE_Mail2(request.getParameter("E_Mail2"));
  //团单合同信息
  tLPGrpContSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPGrpContSchema.setEdorType(request.getParameter("EdorType"));
	tLPGrpContSchema.setGrpContNo(request.getParameter("GrpContNo"));
  tLPGrpContSchema.setProposalGrpContNo(request.getParameter("ProposalGrpContNo"));  

try
  {
  // 准备传输数据 VData
  
  	 VData tVData = new VData();  
  	
	 //保存个人保单信息(保全)
	 tVData.addElement(addrFlag);	
	 tVData.addElement(tG);
	 	
	 	tVData.addElement(tLPGrpEdorItemSchema);
  	tVData.addElement(tLPGrpAppntSchema);
  	tVData.addElement(tLPGrpAddressSchema);
    
    tPGrpEdorADDetailUI.submitData(tVData,fmAction);
	 
	  
	  
	}
	catch(Exception ex)
	{
		Content = fmAction+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPGrpEdorADDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	if (fmAction.equals("QUERY||MAIN")||fmAction.equals("QUERY||DETAIL"))
    	{
    	if (tPGrpEdorADDetailUI.getResult()!=null&&tPGrpEdorADDetailUI.getResult().size()>0)
    	{
    		Result = (String)tPGrpEdorADDetailUI.getResult().get(0);
    		if (Result==null||Result.trim().equals(""))
    		{
    			FlagStr = "Fail";
    			Content = "提交失败!!";
    		}
    	}
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
  
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

