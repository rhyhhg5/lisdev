var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass();      
var turnPage2 = new turnPageClass();      

//查询按钮
function queryClick()
{
	var sql = "select a.ContNo, a.InsuredNo, a.Name, a.Sex, a.Birthday, " +
	     " a.IDType, a.IDNo, a.OccupationCode, a.OccupationType " +
	     "from LCInsured a, LCCont b " +
	     "where a.ContNo = b.ContNo " +
		   "and not exists (select insuredno from LPDiskImport " +
		   "    where InsuredNo = a.InsuredNo " +
		   "    and EdorType = '" + fm.EdorType.value + "' " +
		   "    and EdorNo = '" + fm.EdorNo.value + "')" +
		   "and b.AppFlag = '1' " +
		   "and a.GrpContNo = '" + fm.GrpContNo.value + "' " +
		   getWherePart('a.InsuredNo', 'InsuredNo1') +                  
		   getWherePart('a.Name', 'Name', 'like', '0') +
		   getWherePart('a.IdNo', 'IdNo') + 
		   "order by InsuredNo";
  turnPage.pageDivName = "divPage";
	turnPage.queryModal(sql, LCInsuredGrid);
}

function queryGrpAccInfo()
{
	var sql ="select ClaimBankCode,ClaimAccName,ClaimBankAccNo from LPGrpAppnt where EdorNo='"+fm.EdorNo.value+"' and EdorType='LP'";
	var arrResult1 = easyExecSql(sql); 
	if(!arrResult1)
	{
		var sql1 ="select ClaimBankCode,ClaimAccName,ClaimBankAccNo from LCGrpAppnt where grpcontno='"+fm.GrpContNo.value+"'";
		var arrResult1 = easyExecSql(sql1);
		if (!arrResult1)
	  {
			alert("未查到投保人帐户信息！");
			return;
		}		
	}
	
	fm.all('BankCode').value = arrResult1[0][0];
	fm.all('BankCodeBak').value = arrResult1[0][0];
	fm.all('AccName').value = arrResult1[0][1];
	fm.all('AccNameBak').value = arrResult1[0][1];
	fm.all('BankAccNo').value = arrResult1[0][2];
	fm.all('BankAccNoBak').value = arrResult1[0][2];
	showAllCodeName();
}

//添加保全
function addGEdor()
{
	tRow = LCInsuredGrid.getSelNo();
	if (tRow == 0)
  {
    alert("请选择一个客户！");
    return false;
  }
	fm.ContNo.value = LCInsuredGrid.getRowColData(tRow-1, 1);
  fm.InsuredNo.value = LCInsuredGrid.getRowColData(tRow-1, 2);
	window.open("./PEdorTypeLP.jsp?TypeFlag=G");
}

//撤销集体下个人保全
function cancelGEdor()
{
	tRow = InsuredListGrid.getSelNo();
	if (tRow == 0)
	{
		alert("请先选择一个被保人！");
		return;
	}
	var serialNo=InsuredListGrid.getRowColData(tRow-1, 1);         
	fm.all('fmtransact').value = "DELETE||MAIN";
	fm.action = "./PEdorTypeLPCancelSubmit.jsp?SerialNo="+serialNo;
	fm.submit();

}

//打开个人保全的明细界面
function openGEdorDetail()
{
	tRow = InsuredListGrid.getSelNo();
	if (tRow == 0)
	{
		alert("请先选择一个被保人！");
		return;
	}

	fm.ContNo.value = "";
  fm.InsuredNo.value = InsuredListGrid.getRowColData(tRow-1, 3);
	window.open("./PEdorTypeLP.jsp?TypeFlag=G");
}

//查询导入的数据
function queryImportData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, BankCode,AccName,BankAccNo " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '1' " +
	             "order by Int(SerialNo) ";
	turnPage1.pageDivName = "divPage1";
	turnPage1.queryModal(sql, InsuredListGrid); 
}

//查询导入无效的数据
function queryFailData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, BankCode,AccName,BankAccNo, ErrorReason " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '0' " +
	             "order by Int(SerialNo) ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, FailGrid); 
}

//磁盘导入 
function importInsured()
{
	var url = "./BqDiskImportMain.jsp?EdorNo=" + fm.all("EdorNo").value + 
	          "&EdorType=" + fm.all("EdorType").value +
	          "&GrpContNo=" + fm.all("GrpContNo").value;
	var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
	window.open(url, "理赔金帐户导入", param);
}

//数据提交后的操作
function afterSubmit(flag, content)
{
  try 
  { 
    showInfo.close();
	  window.focus();
	}
	catch (ex) {}
	
	if (flag == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		returnParent();
	}
}

//数据提交后的操作
function afterSubmit1(flag, content)
{
  try 
  { 
    showInfo.close();
	  window.focus();
	}
	catch (ex) {}
	
	if (flag == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

//数据提交后的操作
function afterCancelSubmit(flag, content)
{
  if (flag == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		queryImportData();
		queryClick();
	}
}


//保存
function save()
{
  if (InsuredListGrid.mulLineCount == 0)
	{
		alert("没有有效的导入数据！");
		return false;
	}
	if (FailGrid.mulLineCount > 0)
	{
		alert("有错误数据，请重新导入！");
		return false;
	}
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "GEdorTypeLPSubmit.jsp"
	fm.submit();
}

//返回父窗口
function returnParent()
{
	try
	{
		top.opener.getGrpEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {};
}

function update()
{
	if (!verifyInput2())
  {
    return false;
  }
  if (!verifySameInfo()){
  alert('两次录入的理赔金账户信息不同,请重新输入');
  return false;
  }
	if (fm.BankCodeBak.value == fm.BankCode.value&&
   	fm.AccNameBak.value == fm.AccName.value&&
    fm.BankAccNoBak.value == fm.BankAccNo.value
		)
	{
		alert('信息未改变，不能执行修改操作！');
		return;
	}
	if (fm.BankCode.value == ""||
   	fm.AccName.value == ""||
    fm.BankAccNo.value == ""
		)
	{
		alert('信息不完整！');
		return;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "GEdorTypeGrpLPSubmit.jsp"
	fm.submit();
}
function verifySameInfo(){
	if(fm.BankCode1.value == fm.BankCode.value&&
   	fm.AccName1.value == fm.AccName.value&&
    fm.BankAccNo1.value == fm.BankAccNo.value){
    	return true;
   }
  else{
  	return false;
  }
}
function afterQuery( arrReturn )
{
	fm.all('BankCode').value = arrReturn[0];
//	fm.all('BankCodeBak').value = arrReturn[0];
	fm.all('BankCodeName').value = arrReturn[1];
	fm.all('BankCode1').value = arrReturn[0];
	fm.all('BankCodeName1').value = arrReturn[1];
	
}
function querybank()
{
	showInfo = window.open("LDBankQueryMain.jsp");
}