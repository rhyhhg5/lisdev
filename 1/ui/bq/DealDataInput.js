var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  if(!beforeSubmit())
  	return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}
 
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}              
          
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//工单查询
function workQuery()
{
	showInfo = window.open("WorkQueryMain.jsp?edorAcceptNo="+fm.edorAcceptNo.value+"&actuGetNo="+fm.actuGetNo.value,
						"","toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0");

}
//批单查询
function sementQuery()
{
		var width = screen.availWidth - 10;
	  var height = screen.availHeight - 28;
	  var parma = "top=0,left=0,width="+width+",height="+height;
		if(fm.gpflag.value=="1")
		{

			showInfo =window.open("ShowEdorPrint.jsp?EdorAcceptNo="+fm.edorNo.value, "", parma);
		}
		else
			showInfo=window.open("ShowGrpEdorPrint.jsp?EdorAcceptNo="+fm.edorNo.value, "", parma);

	
}
//返回上一页
function exit()
{
	parent.close();

}
//重打批单
function rePrint()
{
		var sql = "select edorvalue from lpedorespecialdata where edoracceptno = '"
						+fm.edorNo.value
						+"' and edorno ='"
						+fm.edorNo.value
						+"' and edortype ='QQ' and detailtype = 'DEALSTATE'";
		var result = easyExecSql(sql);
		if(result!=null)
		{
			if(result!="已处理")
			{
				alert("保存修改后才可以打印");
				return;
			}
		}
		else
		{
			alert("error");
			return;
		}

		var width = screen.availWidth - 10;
	  var height = screen.availHeight - 28;
	  var parma = "top=0,left=0,width="+width+",height="+height;
		if(fm.gpflag.value=="1")
		{

			showInfo =window.open("ShowEdorPrint.jsp?EdorAcceptNo="+fm.edorNo.value, "", parma);
		}
		else
			showInfo=window.open("ShowGrpEdorPrint.jsp?EdorAcceptNo="+fm.edorNo.value, "", parma);
}
//打印通知书
function printNotice()
{
	showInfo = window.open("../f1print/PrtFailNotice.jsp?actuGetNo="+fm.actuGetNo.value);
}
//处理完毕
function save()
{
	if(!checkSelect())
	return false;
	submitForm();
}
function checkSelect()
{
	
	if(fm.dealRadio[0].checked == false&&fm.dealRadio[1].checked == false)
	{ 
		alert("您好，请选择处理方式后再进行处理！");
		return false;
	}
	if(fm.dealRadio[0].checked==true)
	{
		fm.deal.value="reset";
	if(fm.tranDate.value =="")
	{
		alert("您好，日期必须填写！");
		return false;
	}
	}
	else
	if(fm.dealRadio[1].checked==true)
	{
		fm.deal.value="modify";
	if(fm.dealRadiotype[0].chexked ==false&&fm.dealRadiotype[1].chexked ==false)
	{
		alert("您好，请选择处理方法！");
		return false;
	}
	if(fm.dealRadiotype[0].checked==true)
	{
		fm.dealtype.value="money"
	if(fm.getPerson.value==""||fm.appKind.value==""||fm.no.value=="")
	{
		alert("您好，领款人,申请人性质,领款人证件号是必填内容！");
		return false;
	}
	}
	else
	{
		fm.dealtype.value="accont"
	}
	}
	return true;
}
function init()
{
		var sql = "select (select codename from ldcode b where codetype = 'bank' and b.code = a.bankcode ),AccName,BankAccNo from ljaget a where ActuGetNo='"+fm.actuGetNo.value+"'";
		var result = easyExecSql(sql);
		if(result!=null)
		{
		fm.bank.value = result[0][0];
		fm.accontName.value = result[0][1];
		fm.accontNo.value = result[0][2];
		}
		else
		{
		alert("查询银行信息错误！");
		}

}
function unInit()
{
		fm.bank.value = "";
		fm.accontName.value = "";
		fm.accontNo.value = "";
	
}