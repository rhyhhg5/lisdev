//该文件中包含客户端需要处理的函数和事件
var showInfo;
var pEdorFlag = true;                        //用于实时刷新处理过的数据的列表

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();         //使用翻页功能，必须建立为全局变量
window.onfocus = initFocus;                  //重定义获取焦点处理事件
var arrList1 = new Array();                  //选择的记录列表
 
/**
 * 查询按钮
 */
function queryClick() {
  var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var compareAccContNo = "";
  var sql = "  select contNo "
            + "from LCCont "
            + "where grpContNo = '" + fm.GrpContNo.value + "' "
            + "   and polType = '2' "
	          + "with ur ";
  var result = easyExecSql(sql);
  if(result)
  {
    compareAccContNo = " and not exists (select 1 from LCCont where  contno = b.contno   and polType = '2') ";
  }
	var strSql = "  select b.contNo, b.insuredNo, b.name, CodeName('sex', b.sex), b.birthday, "
	             + "  CodeName('idtype', b.IDType), b.IDNo,  "
	             + "( select sum(insuaccbala) sum1  from lcinsureacc where grpcontno='"+fm.GrpContNo.value+"' and contno=b.contNo and insuredno=b.insuredNo "
                 + " and insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4'))),  "
                 + "  ( select sum(insuaccbala) sum2  from lcinsureacc where grpcontno='"+fm.GrpContNo.value+"' and contno=b.contNo and insuredno=b.insuredNo "
                 + " and insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5'))), "
	             + "( select sum(insuaccbala) sum1  from lcinsureacc where grpcontno='"+fm.GrpContNo.value+"' and contno=b.contNo and insuredno=b.insuredNo "
                 + " and insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4')))"
                 + " + ( select sum(insuaccbala) sum2  from lcinsureacc where grpcontno='"+fm.GrpContNo.value+"' and contno=b.contNo and insuredno=b.insuredNo "
                 + " and insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')))"
	             + "from LCInsured b "
	             + "where 1 = 1 "
	             + getWherePart('b.IDNo','IDNo','like', '0')
	             + getWherePart('b.InsuredNo','CustomerNo','like', '0')
	             + getWherePart('b.Name','Name','like', '0')
	             + getWherePart('b.OthIDNo','OthIDNo','like', '0')
	             + "    and grpContNo = '" + fm.GrpContNo.value + "' "
	             + compareAccContNo
	             + "    and b.insuredNo not in "
	             + "      (select insuredNo "
	             + "      from LPInsured "
	             + "      where edorno = '" + fm.EdorNo.value + "' "
	             + "      and edortype = '" + fm.EdorType.value + "' "
	             + "      and grpContNo = '" + fm.GrpContNo.value + "') "
	             + "      and exists ( select 'x' from lccont where contno=b.contno  and  appflag='1')"
	             + "order by b.insuredNo "
	             + "with ur ";
	turnPage.pageDivName = "divPage";    
	turnPage.queryModal(strSql, LCInsuredGrid); 

	//document.all("test").innerHTML = strSql;
	
	changeNullToEmpty();
	showInfo.close();	 
}

//将账户余额一列的"null"转为""，并计算被保人的帐户余额
function changeNullToEmpty()
{
  //原被保人
  for(var i = 0; i < LCInsuredGrid.mulLineCount; i++)
	{
	  if(LCInsuredGrid.getRowColData(i, 8) == "null")
	  {
	    LCInsuredGrid.setRowColData(i, 8, "");
	  }
	  
	  //帐户余额
	  //var insuAccBala = queryInsuAccBala(LCInsuredGrid.getRowColData(i, 1),
	  //      LCInsuredGrid.getRowColData(i, 2));
	  //LCInsuredGrid.setRowColData(i, 8, insuAccBala);
	}
	
	//欲减掉的被保人
	for(var i = 0; i < LCInsured2Grid.mulLineCount; i++)
	{
	  if(LCInsured2Grid.getRowColData(i, 8) == "null")
	  {
	    LCInsured2Grid.setRowColData(i, 8, "");
	  }
	  //var insuAccBala = queryInsuAccBala(LCInsured2Grid.getRowColData(i, 1),
	  //      LCInsured2Grid.getRowColData(i, 2));
	  //LCInsured2Grid.setRowColData(i, 8, insuAccBala);
	}
}

//查询帐户余额
function queryInsuAccBala(contNo, insuredNo)
{
  var sql = "  select sum(InsuAccBala) "
            + "from LCInsureAcc "
            + "where contNo = '" + contNo + "' "
            + "   and insuredNo = '" + insuredNo + "' "
            + "with ur ";
  var turnPage3 = new turnPageClass();
  var result = easyExecSql(sql, null, null, null, null, null, turnPage3);
  if(result && result[0][0] != "null")
  {
    return result[0][0];
  }
  else
  {
    return "";
  }
}

/**
 * 单击MultiLine的复选按钮时操作
 */
function reportDetailClick(parm1, parm2) {	
  if (fm.all(parm1).all('LCInsuredGridChk').checked) {
    arrList1[fm.all(parm1).all('LCInsuredGridNo').value] = fm.all(parm1).all('LCInsuredGrid1').value;
  }
  else {
    arrList1[fm.all(parm1).all('LCInsuredGridNo').value] = null;
  }
  
  GrpBQ = false;
}

/**
 * 进入多个人保全
 */
var arrP = 0;
function pEdorMultiDetail() {
  //校验是否选择
    var i = 0;
    var chkFlag=false;
    var count = LCInsured2Grid.mulLineCount;
    var count2 =0;
    for (i=0;i<LCInsuredGrid.mulLineCount;i++ )
    {
    	
        if (LCInsuredGrid.getChkNo(i))
        {
          chkFlag=true;
          count2++;
        }
    }
    
    if(count2>1){
    	alert("该保全每次只能对一个人进行该操作");
    	return false;
    }
    if(checkPerson()>=1){
    	alert("正在对一个人进行身故保全业务，不能同时再对其他人进行该业务的操作");
    	return false;
    }
    if (chkFlag)
    {
    	fm.all("ErrorsInfo").innerHTML =""; 
        var showStr = "正在申请集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        PEdorDetail();
    }
    else
    {
        alert("请选择需要操作的记录!");
    }     
  
}

function checkPerson(){
	var sql = "select count(*) from lpdiskimport where edortype='SG' and grpcontno='"+fm.all("GrpContNo").value+"' and edorno='"+fm.all("EdorNo").value+"'";
	var result = easyExecSql(sql);
	if(result==null){
		return 0;
	}
	return result[0][0];
}
//减人的磁盘导入
function diskImport()
{
	fm.all("ErrorsInfo").innerHTML =""; 
	var strUrl = "DiskImportSGMain.jsp?EdorNo=" + fm.all("EdorNo").value 
	    + "&GrpContNo=" + fm.all("GrpContNo").value + "&EdorType=" + fm.all("EdorType").value;
	window.open(strUrl, "被保人清单导入", "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=yes");
}

/**
 * 进入个人保全
 */
function PEdorDetail() {  
      
 
    fm.all("Transact").value = "INSERT||EDOR";
    fm.action = "GEdorTypeMultiDetailSubmit.jsp";
    fm.submit();
 
}

/**
 * 提交后操作,服务器数据返回后执行的操作
 */
function afterSubmit(FlagStr, content, Result) 
{
  if(fm.Transact.value == "QUERY||ASRATE"){
  	  if(LCInsured2Grid.mulLineCount > 0){
      	LCInsured2Grid.setRowColData(0,7,Result);
      }
  }else{
  	showInfo.close();
  	window.focus();



	  if (FlagStr == "Fail") 
	  {             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  }
	  else 
	  { 
	    pEdorFlag = true;
	    
	    if (fm.Transact.value=="DELETE||EDOR") {
	      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	    }
	    else {
	      //openPEdorDetail();
	      //PEdorDetail();
	    }
	  }
  }
}

/**
 * 处理获取焦点事件
 */
function initFocus() {
  if (pEdorFlag) {   
    pEdorFlag = false;
        
    queryPEdorList();
  }
}

var GrpBQ = false;
var GTArr = new Array();
/**
 * 打开个人保全的明细界面
 */
function openPEdorDetail() {
	
	  if (trim(fm.all('EdorType').value)=="PT") {
		  window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp", "", 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		}
		if (trim(fm.all('EdorType').value)=="GT") {
		  window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp", "", 'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		}
		if (trim(fm.all('EdorType').value)=="ZT") {
		  PEdorDetail();
		}

		try { showInfo.close();	} catch(e) {}

}

/**
 * 查询出申请后的个人保全列表
 */
function queryPEdorList() {
  
	querySelectedInsured();
	queryClick();
}

//查询选择了的被保人信息
function querySelectedInsured(){

  var strSql = "  select ContNo, InsuredNo, Name, CodeName('sex', a.sex), IDNo, (select edorvalidate from lpdiskimport l where l.edorno=a.edorno and l.edortype=a.edortype and l.grpcontno=a.grpcontno and l.insuredno=a.insuredno ),(select money2 from lpdiskimport l where l.edorno=a.edorno and l.edortype=a.edortype and l.grpcontno=a.grpcontno and l.insuredno=a.insuredno ),"
               + " ( select sum(insuaccbala) from lcinsureacc where grpcontno='"+fm.GrpContNo.value+"' and contno=a.contNo and insuredno=a.insuredno "
               + " and insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4'))),  "
               + " ( select sum(insuaccbala) from lcinsureacc where grpcontno='"+fm.GrpContNo.value+"' and contno=a.contNo and insuredno=a.insuredno "
               + " and insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5'))),"
               + " ( select sum(insuaccbala) from lcinsureacc where grpcontno='"+fm.GrpContNo.value+"' and contno=a.contNo and insuredno=a.insuredno "
               + " and insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4')))"
               + " + ( select sum(insuaccbala) from lcinsureacc where grpcontno='"+fm.GrpContNo.value+"' and contno=a.contNo and insuredno=a.insuredno "
               + " and insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5')))" 
               + "from LPInsured a "
               + "where grpContNo = '" + fm.GrpContNo.value + "' "
               + "    and edorNo = '" + fm.EdorNo.value + "' "
               + "    and edorType = '" + fm.EdorType.value + "' "
               + getWherePart('IDNo','IDNo3','like', '0')
	             + getWherePart('InsuredNo','CustomerNo3','like', '0')
	             + getWherePart('Name','Name3','like', '0')
	             + getWherePart('OthIDNo','OthIDNo3','like', '0')
	             + "order by InsuredNo "
	             + "with ur ";
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, LCInsured2Grid); 	
	changeNullToEmpty();
	if(LCInsured2Grid.mulLineCount > 0) {
		fm.Transact.value = "QUERY||ASRATE"
		fm.action = "GEdorTypeMultiSGSave.jsp";
		fm.submit();
	}
}


/**
 * 单击MultiLine的单选按钮时操作
 */
function reportDetail2Click(parm1, parm2) {	
  if (fm.all(parm1).all('LCInsured2GridChk').value=='on' || fm.all(parm1).all('LCInsured2GridChk').value=='1') {
    fm.ContNo.value = fm.all(parm1).all('LCInsured2Grid1').value;
  }
}

/**
 * 撤销集体下个人保全
 */
function cancelPEdor() 
{
    
    var i = 0;
    var chkFlag=false;
    for (i=0;i<LCInsured2Grid.mulLineCount;i++ )
    {
        if (LCInsured2Grid.getChkNo(i))
        {
          chkFlag=true;
          break;
        }
        
    }
    if (chkFlag)
    {
        fm.all("ErrorsInfo").innerHTML =""; 
        fm.all("Transact").value = "DELETE||EDOR"
        var showStr = "正在撤销集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.action = "GEdorTypeMultiDetailSubmit.jsp";
        fm.submit();
    }
    else
    {
        alert("请选择需要操作的记录!");
    }     
 
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function checkHeFa()
{
    var rowNum=LCInsured2Grid.mulLineCount; 
    for(i=0;i<rowNum;i++)
    {
	    if(!isDate(LCInsured2Grid.getRowColData(i,6)))
	    {
	      alert("第"+i+"行的生效日期不是日期格式【某年-某月-某日】");
	      return false;
	    }
    }
    return true;
    
    
}
function returnParent() 
{
  /*
    if(!checkHeFa())
    {
      return false;
    }  
    */
    //若是减人，则需要存储算法类别
    if(fm.EdorType.value == "SG")
    {
        doSG();
    }
    else
    {
        goBack();
    }
}

//校验保单操作后的人数是否小于8，若小于8则给出提示
function checkPeoples2()
{
    if(getPeoples2() < 8)
    {
        alert("保单总人数已小于8人。");
    }
}

//查询保单操作后的人数
function getPeoples2()
{

    var sql = "  select (select count(1) "
              + "       from LCInsured  m"
              + "       where grpContNo = '" + fm.GrpContNo.value + "'  "
              + "    and exists (select 'x' from lccont where contno=m.contno and appflag='1')) "
	            + "   -   (select count(1) "
	            + "       from LPInsured "
	            + "       where grpContNo = '" + fm.GrpContNo.value + "' "
	            + "           and edorType = '" + fm.EdorType.value + "' "
	            + "           and edorNo = '" + fm.EdorNo.value + "') "
              + "from dual ";
    var result = easyExecSql(sql);

    if(result)
    {
        return parseInt(result[0][0]);
    }
    else
    {
        return -1;
    }
}

//减人操作
function doSG()
{
	//加入js校验，验证当选择银行转账时候，所选银行的有效性。
	if(!checkBankDate()){
		alert("请选择有效的银行信息");
		return false;
	}

	 fm.all("ErrorsInfo").innerHTML =""; 
    if(!checkInput())
    {
        return false;
    }
    fm.action = "GEdorTypeSGCaltypeSave.jsp";
    var showStr = "正在保存集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); 
}

//校验录入的合法性
function checkInput()
{
    var sql = "  select edorAcceptNo "
              + "from LPEdorItem "
              + "where edorAcceptNo = '" + fm.EdorNo.value + "' "
              + "    and edorType = '" + fm.EdorType.value + "' ";
    var result = easyExecSql(sql);
    if(result == null)
    {
        alert("您还没有录入需退保的被保人");
        return false;
    }
    
    return true;
}

//返回父目录
function goBack()
{
    top.opener.focus();
    top.opener.getGrpEdorItem();
    top.close();
}

//存储算类别择后的操作
function afterSubmitCalType(flag, content)
{
    showInfo.close();
    window.focus();
    //此处content可能较长，使用showModalDialog可能有问题，现将错误显示到页面
    if (flag == "Fail" )
    {
        fm.all("ErrorsInfo").innerHTML = content;  
        content = "保存数据失败！发生的错误信息见页面底部";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
	else
	{
		content = "保存数据成功！";
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	    goBack();
	}
}

function saveEdor() {
  if (LCInsured2Grid.mulLineCount == 0)
  {
    alert("必须修改后才能保存申请！");
    return;
  } 
  
   top.opener.getGrpEdorItem();

   var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=保存成功"   ;  
   showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
 }  
//点击“保存归属比例”按钮执行的操作
function submitForm(){
	if (!beforeSubmit()){
		return false;
	}
	if (!confirm('确认您的操作')){
		return false;
	}
	
	fm.action = "GEdorTypeMultiSGSave.jsp";
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
	querySelectedInsured();
	
}
//提交前的校验
function beforeSubmit(){
	var lineCount = 0;
	var selLineCount=0;
	
	LCInsured2Grid.delBlankLine("LCInsured2Grid");
	lineCount = LCInsured2Grid.mulLineCount;
	for (i=0;i<lineCount;i++){
		if (LCInsured2Grid.getChkNo(i)){
			selLineCount=selLineCount+1;
			if((LCInsured2Grid.getRowColData(i,7)=='')||(LCInsured2Grid.getRowColData(i,7)<0)||(LCInsured2Grid.getRowColData(i,7)>1)){
				alert("归属比例应该在0-1之间，请正确输入！");
				return false;
			}
			if(!isNumeric(LCInsured2Grid.getRowColData(i,7))){
			alert("非法数字");
			return false;
		}
		}
	}
	if(selLineCount==0){
		alert("请选中修改行的序号");
		return false;
	}
	return true;
}
//
function afterCodeSelect( cCodeName, Field ) {
  if(checkPerson()==0){
  	alert("请先选择一个人，对其进行该保全！");
  	fm.PayMode.value='1';
	fm.PayModeName.value='现金';
  	return false;
  }
  if( cCodeName == "PayMode"){
    if(Field.value=="1"||Field.value=="2" ||Field.value=="9"){
      divBankInfo.style.display='none';
      fm.BankCode.value='';
      fm.BankAccNo.value='';
      fm.AccName.value='';
      fm.BankCodeName.value='';
    }
    else{
      divBankInfo.style.display='';
      var tcontno=LCInsured2Grid.getRowColData(0, 1);
      var sql ="select bankaccno,accname,bankcode,(select BankName from LDBank where (BankUniteFlag is null or BankUniteFlag<>'1') and bankcode=a.bankcode and Comcode like '86%' and 1 = 1 order by BankCode) from lccont a where grpcontno='"+fm.GrpContNo.value+"' and contno='"+tcontno+"'";
      var result = easyExecSql(sql);
      if(result!=null)
      	fm.BankCode.value=result[0][2];
      	fm.BankAccNo.value=result[0][0];
     	fm.AccName.value=result[0][1];
     	fm.BankCodeName.value=result[0][3];
    }
  }
}

//每次页面初始化页面后，检验grid2中是否有数据，如果有，则顺便把该人的银行信息查出来 ，如果没有，则清空银行信息的数据
function checkClaimInfo(){
//从LPGrpEdorItem 查看 EdorState 看看录入的状态，如果未录入，则初始化之后，如果grid2有人，则银行信息从lccont中获取
//如果grid无人，则默认隐藏，只显示支付为1 现金形式，如果未录入状态，则初始化之后，grid必然有人，那么该银行信息从specialdate这个表中获取
	var itemSql = "select EdorState from LPGrpEdorItem where edortype='SG' and edorno='"+fm.all("EdorNo").value +"'";
	var itemResult = easyExecSql(itemSql);
	//3 为初始状态   1为录入完毕
	if(itemResult[0][0]==3){
		//if(checkPerson()==1){
			//divBankInfo.style.display='';
      		//var tcontno=LCInsured2Grid.getRowColData(0, 1);
      		//var sql ="select bankaccno,accname,bankcode,(select BankName from LDBank where (BankUniteFlag is null or BankUniteFlag<>'1') and bankcode=a.bankcode and Comcode like '86%' and 1 = 1 order by BankCode) from lccont a where grpcontno='"+fm.GrpContNo.value+"' and contno='"+tcontno+"'";
      		//var result = easyExecSql(sql);
      		//if(result!=null){
      		//	fm.BankCode.value=result[0][2];
      		//	fm.BankAccNo.value=result[0][0];
     		//	fm.AccName.value=result[0][1];
     		//	fm.BankCodeName.value=result[0][3];
     		//divBankInfo.style.display='none';
     	//	}
		//}else{
		cleanDate();
		//}
	}else if(itemResult[0][0]==1||itemResult[0][0]==2){
		var specialSql = "select detailtype,edorvalue from LPEdorEspecialData where edorno='"+fm.all("EdorNo").value+"' and edortype='SG'";
		var specialResult = easyExecSql(specialSql);
		if(specialResult==null){
			alert("数据异常，请联系技术人员!");
			return ;
			
		}else{
		
			for(var i=0;i<specialResult.length;i++){
				if(specialResult[i][0]=='PAYMODE'){
					fm.PayMode.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='PAYMODENAME'){
					fm.PayModeName.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='BANKCODE'){
					fm.BankCode.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='BANKCODENAME'){
					fm.BankCodeName.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='BANKACCNO'){
					fm.BankAccNo.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='ACCNAME'){
					fm.AccName.value=specialResult[i][1];
				}
			}
			if(fm.PayMode.value==3 || fm.PayMode.value==4){
				divBankInfo.style.display='';
			}
		}
	}
}

function cleanDate(){
		divBankInfo.style.display='none';
		fm.PayMode.value='1';
		fm.PayModeName.value='现金';
      	fm.BankCode.value='';
      	fm.BankAccNo.value='';
      	fm.AccName.value='';
      	fm.BankCodeName.value='';
}

function checkBankDate(){
	var PayMode = fm.PayMode.value;
	var BankCode = fm.BankCode.value;
	var BankAccNo = fm.BankAccNo.value;
	if(PayMode=='3'||PayMode=='4'){
		if(BankCode=='' ||BankCode==null){
			return false;
		}
		if(BankAccNo=='' ||BankAccNo==null){
			return false;
		}
	
		if(BankCode=='' ||BankAccNo==''){
			return false;
		}
	
		if(BankCode==null ||BankAccNo==null){
			return false;
		}
	
		var sql = "select 1 from ldbank where bankcode='"+BankCode+"'";
		var result = easyExecSql(sql);
		if(result==null){
			return false;
		}
	}
	return true;
}

function selectClick(){
	if(checkPerson()==0){
  		alert("请先选择一个人，对其进行该保全！");
  		return false;
  	}
  	
	fm.selectedInsuredNo.value = LCInsured2Grid.getRowColData(0,2);
	var insuredno = LCInsured2Grid.getRowColData(0,2);
	var contno = LCInsured2Grid.getRowColData(0,1);
	showInfo = window.open("./GEdorTypeSGDetailMain.jsp?edorno="+fm.EdorNo.value+"&edortype="+fm.EdorType.value+"&insuredno="+insuredno+"&contno="+contno);
}


