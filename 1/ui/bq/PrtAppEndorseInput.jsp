<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：PrtAppEndorseInput.jsp
//程序功能：
//创建日期：2005-03-03 
//创建人  ：FanX
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
 GlobalInput tGI1 = new GlobalInput();
     tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
     String ManageCom = tGI1.ManageCom;
   
	String strEdorAcceptNo = request.getParameter("EdorAcceptNo");
	if (strEdorAcceptNo == null)
	    strEdorAcceptNo = "";
 %>    
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  
  <SCRIPT src="./PrtAppEndorse.js"></SCRIPT>
  <SCRIPT src="PEdor.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PrtAppEndorseInit.jsp"%>
  <%@include file = "ManageComLimit.jsp"%>

  <title>个人批改查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./PEdorQueryOut.jsp" method=post name=fm target="fraSubmit" >
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLPEdor1" style= "display: ''">
      <table  class= common>

        <TR  class= common>
          <TD  class= title>   批改申请号  </TD>
          <TD  class= input>  <Input class= common name=EdorAcceptNo value='<%=strEdorAcceptNo%>' > </TD>
          <TD  class= title>   申请号 </TD>
          <TD  class= input> <Input class= common name=OtherNo > </TD>
          <TD  class= title> 申请号码类型 </TD>
          <TD  class= input>  <Input type="input" class="codeno" name=OtherNoType CodeData="0|^1|个人客户号^3|个人保单号" ondblClick="showCodeListEx('OtherNoType',[this,OtherNoTypeName],[0,1]);" onkeyup="showCodeListKeyEx('OtherNoType',[this,OtherNoTypeName],[0,1]);"><input class=codename name=OtherNoTypeName readonly=true >
        </TR>
      </table>
    </Div>
    
          <INPUT VALUE="查询" class= cssButton TYPE=button onclick="easyQueryClick();"> 
          					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor2);">
    		</td>
    		<td class= titleImg>
    			 个人批改信息
    		</td>
    	</tr>
    	 
    </table>
   
  	<Div  id= "divLPEdor2" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanPEdorAppGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<br>
     <!--INPUT VALUE="使用文件打印批单" class= common TYPE=button onclick="submitForm();"--> 
     <!--INPUT VALUE="使用会话打印批单" class= common TYPE=button onclick="submitFormold();"-->
     <INPUT VALUE="打印批单" class= cssButton TYPE=button onclick="submitForm();">
     <INPUT VALUE="打印现金价值表" class= cssButton TYPE=button onclick="prtCashValue();">     
     <!--INPUT VALUE="显示批单XML" class= common TYPE=button onclick="changeEdorPrint();"--> 

  <input type=hidden id="EdorType" name="EdorType">
  	 <input type=hidden id="ContType" name="ContType">
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
