//该文件中包含客户端需要处理的函数和事件
var showInfo;
var pEdorFlag = true;                        //用于实时刷新处理过的数据的列表

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var arrList1 = new Array();                  //选择的记录列表
 

function returnParent() 
{
  /*
    if(!checkHeFa())
    {
      return false;
    }  
    */
    //若是减人，则需要存储算法类别
    if(fm.EdorType.value == "SG")
    {
        doSG();
    }
    else
    {
        goBack();
    }
}

//减人操作
function doSG()
{
    
  var chk = 1;
  var i = 0;
  var arr = new Array();
  var arrlot = new Array(0,0,0,0,0,0,0);
  var row = BnfGrid.mulLineCount-1;
  	//效验同一级别的份额总额是否等于1
  for(var m=0;m<=row;m++ )
  {
   		arr[m]=i;
   		for(var n=1;n<=6;n++)
   		{	
   		if(eval(n)==eval(BnfGrid.getRowColData(m,4)))   		
   		arrlot[n] = eval(arrlot[n])+eval(BnfGrid.getRowColData(m,5));        
   	    }
   		
   	i=i+1;
  }
  if(eval(arrlot[1])!=1&&eval(arrlot[2])!=1&&eval(arrlot[3])!=1&&eval(arrlot[4])!=1&&eval(arrlot[5])!=1&&eval(arrlot[6])!=1)
  {
  	alert("同一级别的受益份额加起来应该等于1");
    return false;
  }
  if(arr.length==0)
  {
  	alert("请录入受益人信息！");
  	return false;
  }
	 
    //校验死亡日期
    if(!verifyElementWrap2("死亡日期|date&notnull",fm.all("deathDate").value,"fm.deathDate"))
		{
				return false;
		}
		if(!verifyElementWrap2("缴费方式|code:PayMode&notnull",fm.all("PayMode").value,"fm.PayMode"))
		{
				return false;
		}
		
		
		if(fm.all("PayMode").value=='3'||fm.all("PayMode").value=='4'){
			if(!verifyElementWrap2("银行编码|code:llbank&notnull",fm.all("BankCode").value,"fm.BankCode"))
			{
				return false;
			}
			if(!verifyElementWrap2("账号|notnull",fm.all("BankAccNo").value,"fm.BankAccNo"))
			{
				return false;
			}
			if(!verifyElementWrap2("账户名|notnull",fm.all("AccName").value,"fm.AccName"))
			{
				return false;
			}
			if(!checkISTheSamePerson()){
				return false;
			}
		}
		
		if(!checkdatestring()){
			
			return false;
		}
		
		if(!isPermitDateFormat()){
			return false;
		}
		if(!checkBnfID()){
			return false;
		}
    
    fm.action = "GEdorTypeSGCaltypeSave.jsp";
    var showStr = "正在保存集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); 
}


//返回父目录
function goBack()
{
    top.opener.focus();
    top.opener.getGrpEdorItem();
    top.close();
}

function checkBankDate(){
	var PayMode = fm.PayMode.value;
	var BankCode = fm.BankCode.value;
	var BankAccNo = fm.BankAccNo.value;
	if(PayMode=='3'||PayMode=='4'){
		if(BankCode=='' ||BankCode==null){
			return false;
		}
		if(BankAccNo=='' ||BankAccNo==null){
			return false;
		}
	
		if(BankCode=='' ||BankAccNo==''){
			return false;
		}
	
		if(BankCode==null ||BankAccNo==null){
			return false;
		}
	
		var sql = "select 1 from ldbank where bankcode='"+BankCode+"'";
		var result = easyExecSql(sql);
		if(result==null){
			return false;
		}
	}
	return true;
}

function afterCodeSelect( cCodeName, Field ) {
  if( cCodeName == "PayMode"){
    if(Field.value=="1"||Field.value=="2" ||Field.value=="9"){
      divBankAcc.style.display='none';
      fm.BankCode.value='';
      fm.BankAccNo.value='';
      fm.AccName.value='';
      fm.BankCodeName.value='';
    }
    else{
        divBankAcc.style.display='';
      	fm.BankCode.value='';
        fm.BankAccNo.value='';
        fm.AccName.value='';
        fm.BankCodeName.value='';
    }
  }
}

function checkClaimInfo(){
//从LPGrpEdorItem 查看 EdorState 看看录入的状态，如果未录入，则初始化之后，如果grid2有人，则银行信息从lccont中获取
//如果grid无人，则默认隐藏，只显示支付为1 现金形式，如果未录入状态，则初始化之后，grid必然有人，那么该银行信息从specialdate这个表中获取
	var itemSql = "select EdorState from LPGrpEdorItem where edortype='SG' and edorno='"+fm.all("EdorNo").value +"'";
	var itemResult = easyExecSql(itemSql);
	//3 为初始状态   1为录入完毕
	if(itemResult[0][0]==3){
		cleanDate();
	}else if(itemResult[0][0]==1||itemResult[0][0]==2){
		var specialSql = "select detailtype,edorvalue from LPEdorEspecialData where edorno='"+fm.all("EdorNo").value+"' and edortype='SG'";
		var specialResult = easyExecSql(specialSql);
		if(specialResult==null){
			alert("数据异常，请联系技术人员!");
			return ;
			
		}else{
		
			for(var i=0;i<specialResult.length;i++){
				if(specialResult[i][0]=='PAYMODE'){
					fm.PayMode.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='PAYMODENAME'){
					fm.PayModeName.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='BANKCODE'){
					fm.BankCode.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='BANKCODENAME'){
					fm.BankCodeName.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='BANKACCNO'){
					fm.BankAccNo.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='ACCNAME'){
					fm.AccName.value=specialResult[i][1];
				}
				if(specialResult[i][0]=='DEATHDATE'){
					fm.deathDate.value=specialResult[i][1];
				}
			}
			if(fm.PayMode.value==3 || fm.PayMode.value==4){
				divBankAcc.style.display='';
			}
		}
		//加载bnfGrid中的数据
		var bnfGridString = "select name,sex,birthday,bnfgrade,bnflot,idtype,idno,RelationToInsured from lpbnf where edorno='"+fm.all("EdorNo").value+"' and edortype='SG' and insuredno='"+fm.all("InsuredNo").value+"'";
		var bnfGridResult = easyExecSql(itemSql);
		if(bnfGridResult==null){
			alert("数据异常");
		}else{
			turnPage.pageDivName = "divPage";    
			turnPage.queryModal(bnfGridString, BnfGrid);
		}
		
	}
}

function cleanDate(){
		divBankAcc.style.display='none';
		fm.PayMode.value='1';
		fm.PayModeName.value='现金';
      	fm.BankCode.value='';
      	fm.BankAccNo.value='';
      	fm.AccName.value='';
      	fm.BankCodeName.value='';
      	fm.deathDate.value='';
}

//存储算类别择后的操作
function afterSubmitCalType(flag, content)
{
    showInfo.close();
    window.focus();
    //此处content可能较长，使用showModalDialog可能有问题，现将错误显示到页面
    if (flag == "Fail" )
    {
        fm.all("ErrorsInfo").innerHTML = content;  
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
	else
	{
		content = "保存数据成功！";
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	    top.close();
	}
}

function checkdatestring(){
	var contno=fm.all("ContNo").value;
	var deathDate = fm.all("deathDate").value;
	//var deathDate = "2011-04-20";
	var sql ="select max(paydate) from lcinsureacctrace where contno='"+contno+"'";
	var Result = easyExecSql(sql);
	var paydate
	if(Result!=null){
		paydate = Result[0][0];
		if(compareDate(trim(paydate),trim(deathDate))==1){
			alert("身故日期后，账户有变动，不能进行保全处理");
			return false;
		}
	}
	return true;
	
}
//录入转帐信息时，受益人和转帐帐户名增加一致性效验
function checkISTheSamePerson(){
	var flag=false;
	var lineCount = BnfGrid.mulLineCount;
	var AccName = fm.AccName.value;
	if(lineCount==0){
		alert("请输入相关受益人信息");
		return false;
	}
	
	for(var i=0;i<lineCount;i++){
		if(BnfGrid.getRowColData(i,1)==AccName){
			flag=true;
			break;
		}
	}
	if(flag==false){
		alert("转账的账户名与受益人的姓名不一致！请重新录入！");
		return false;
	}
	return true;
}

// 对bnfgrid中 出生日期格式的校验

function isPermitDateFormat(){
	var lineCount = BnfGrid.mulLineCount;
	if(lineCount==0){
		alert("请输入相关受益人信息");
		return false;
	}
	for(var i=0;i<lineCount;i++){
		//alert(BnfGrid.getRowColData(i,3));
		if(!isDate(BnfGrid.getRowColData(i,3))){
			alert("日期格式不正确，请正确输入！");
			return false;
		}
	}
	return true;
}

function checkBnfID(){
	var lineCount = BnfGrid.mulLineCount;
	if(lineCount==0){
		alert("请输入相关受益人信息");
		return false;
	}
	alert(lineCount);
	for(var i=0;i<lineCount;i++){
		var idType=BnfGrid.getRowColData(i,6);
		var idNO=BnfGrid.getRowColData(i,7);
		var sex=BnfGrid.getRowColData(i,2);
		var birthday=BnfGrid.getRowColData(i,3);
		alert(lineCount);
		if(idType == "0" || idType == "5"){
			var strCheckIdNo = checkIdNo(idType,idNO,birthday,sex);
			if ("" != strCheckIdNo && strCheckIdNo!=null)
			{
				alert(strCheckIdNo);
				return false;
			}
		 }
	}
	return true;
}


