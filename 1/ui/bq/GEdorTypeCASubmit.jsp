<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>

<%
//程序名称：GEdorTypeCASubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理
  System.out.println("-----GEdorTypeCA submit---");
               
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  transact = request.getParameter("Transact");
  String strOutCount = request.getParameter("nOutCount");
  
  String tLimit = PubFun.getNoLimit(tG.ComCode );
  String mSerialNo = PubFun1.CreateMaxNo( "SerialNo", tLimit );  
  
  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
  
  tLPGrpEdorMainSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
  tLPGrpEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPGrpEdorMainSchema.setEdorType(request.getParameter("EdorType"));
 
  LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
  
  tLPInsureAccTraceSchema.setPolNo(request.getParameter("PolNo"));
  tLPInsureAccTraceSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPInsureAccTraceSchema.setEdorType(request.getParameter("EdorType"));  
  tLPInsureAccTraceSchema.setSerialNo(mSerialNo);
  tLPInsureAccTraceSchema.setMoney(request.getParameter("GetMoney"));
  tLPInsureAccTraceSchema.setInsuAccNo(request.getParameter("InsuAccNo"));
  tLPInsureAccTraceSchema.setInsuredNo(request.getParameter("InsuredNo"));
  tLPInsureAccTraceSchema.setRiskCode(request.getParameter("RiskCode"));
  //add at 2004-6-28 
  tLPInsureAccTraceSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
  
  PGrpEdorCADetailUI tPGrpEdorCADetailUI = new PGrpEdorCADetailUI();
  try {
        // 准备传输数据 VData
  	VData tVData = new VData();  
	tVData.addElement(tG);
	tVData.addElement(tLPGrpEdorMainSchema);
	tVData.addElement(tLPInsureAccTraceSchema);
	tVData.addElement(strOutCount);
	 	
	//保存个人保单信息(保全)
        tPGrpEdorCADetailUI.submitData(tVData, transact);
        
        //Result = (String)tPGrpEdorCADetailUI.getResult().get(0);
      } catch(Exception ex) {
	   Content = transact+"失败，原因是:" + ex.toString();
           FlagStr = "Fail";
      }			
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="") {
    CErrors tError = new CErrors(); 
    tError = tPGrpEdorCADetailUI.mErrors;
    
    if (!tError.needDealError()) {                          
        Content = " 保存成功";
    	FlagStr = "Success";
    	
    	if (transact.equals("INSERT||EDOR")) {
      	     if (tPGrpEdorCADetailUI.getResult()!=null && tPGrpEdorCADetailUI.getResult().size()>0) {
      		   Result = (String)tPGrpEdorCADetailUI.getResult().get(0);
      	           System.out.println("**********Result:"+Result);
      	           if (Result==null||Result.trim().equals("")) {
      		        FlagStr = "Fail";
      		        Content = "提交失败!!";
      	           }
      	     }
    	}
    }
    else {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>");
</script>
</html>

