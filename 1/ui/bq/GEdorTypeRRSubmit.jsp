<%
//程序名称：PEdorTypeRRSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
  <%
  String flag = "";
  String content = "";
  String typeFlag = request.getParameter("TypeFlag");
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  
  LCGrpContStateSchema tLCGrpContStateSchema = new LCGrpContStateSchema();
  tLCGrpContStateSchema.setGrpContNo(request.getParameter("GrpContNo"));
  tLCGrpContStateSchema.setOtherNo(request.getParameter("EdorNo"));
  tLCGrpContStateSchema.setOtherNoType("RR");
  tLCGrpContStateSchema.setEndDate(request.getParameter("ResumeDate"));

  
  VData data = new VData();
  data.add(typeFlag);
  data.add(tLCGrpContStateSchema);
  data.add(gi);
  
  GEdorRRDetailUI tGEdorRRDetailUI = new GEdorRRDetailUI();
  if (!tGEdorRRDetailUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tGEdorRRDetailUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！";
  }
  content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>