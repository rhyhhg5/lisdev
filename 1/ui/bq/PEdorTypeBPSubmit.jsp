<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  GlobalInput tGlobalInput = new GlobalInput();
  tGlobalInput = (GlobalInput)session.getValue("GI");

  transact = request.getParameter("fmtransact");
  String edorAcceptNo = request.getParameter("EdorAcceptNo");
  String edorNo = request.getParameter("EdorNo");
  String edorType = request.getParameter("EdorType");
  String contNo = request.getParameter("ContNo");
  String newPrem = request.getParameter("NewPrem");
  String polNo = request.getParameter("PolNo");
  LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
  tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
  tLPEdorItemSchema.setEdorNo(edorNo);
  tLPEdorItemSchema.setContNo(contNo);
  tLPEdorItemSchema.setEdorType(edorType);
  tLPEdorItemSchema.setPolNo("000000");
  tLPEdorItemSchema.setInsuredNo("000000");
  // 准备传输数据 VData
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("NewPrem",newPrem);	
  tTransferData.setNameAndValue("PolNo",polNo);
  	
  VData tVData = new VData();
  tVData.add(tGlobalInput);
  tVData.add(tLPEdorItemSchema);
  tVData.add(tTransferData);
  PEdorBPDetailBL tPEdorBPDetailBL   = new PEdorBPDetailBL();  
  if (!tPEdorBPDetailBL.submitData(tVData, ""))
  {
  	VData rVData = tPEdorBPDetailBL.getResult();
  	System.out.println("Submit Failed! " + tPEdorBPDetailBL.mErrors.getErrContent());
  	Content = transact + "失败，原因是:" + tPEdorBPDetailBL.mErrors.getFirstError();
  	FlagStr = "Fail";
  }
  else 
  {
  	Content = "保存成功";
  	FlagStr = "Success";
  }
%>                      
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>