<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorUWManuHealth.jsp
//程序功能：保全人工核保体检资料录入
//创建日期：2006-06-19 
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./PEdorUWManuHealthQ.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./PEdorUWManuHealthQInit.jsp"%>
  <title> 保全体检回销 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./PEdorUWManuHealthChk.jsp">
    <input type="hidden" name="EdorNo">
    <input type="hidden" name="AppntNo">
    <input type="hidden" name="ContNo">
    <input type="hidden" name="InsuredNo">
    <input type="hidden" name="PrtSeq">
    <table>
    	<tr><td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured);"></td>
    		<td class= titleImg>被保人信息</td>                            
    	</tr>	
    </table>
    <Div  id= "divLCInsured" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1 >
      				<span id="spanLCInsuredGrid">
      				</span> 
  		  	  </td>
  		    </tr>
    	  </table>
    	<div id= "divPage" align=center style= "display: 'none' ">
        <input class=cssButton value="首  页" type=button onclick="turnPage.firstPage();"> 
        <input class=cssButton value="上一页" type=button onclick="turnPage.previousPage();"> 					
        <input class=cssButton value="下一页" type=button onclick="turnPage.nextPage();"> 
        <input class=cssButton value="尾  页" type=button onclick="turnPage.lastPage();"> 			
      </div>
    </div>
    <table>
    	<tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHealth);"></td>
    		<td class= titleImg>体检项目</td>                            
    	</tr>	
    </table>
    <Div  id= "divHealth" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanHealthGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
      <div id= "divPage2" align=center style= "display: 'none' ">
        <input class=cssButton value="首  页" type=button onclick="turnPage2.firstPage();"> 
        <input class=cssButton value="上一页" type=button onclick="turnPage2.previousPage();"> 					
        <input class=cssButton value="下一页" type=button onclick="turnPage2.nextPage();"> 
        <input class=cssButton value="尾  页" type=button onclick="turnPage2.lastPage();"> 			
      </div>	
    <table>
    	<tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWDis);"></td>
        <td class= titleImg>疾病结果</td>                            
    	</tr>	
    </table>
    <Div  id= "divUWDis" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1 >
              <span id="spanDisDesbGrid">
              </span> 
  		  	  </td>
  		    </tr>
    	  </table>
      </div>
    	<table class=common>
         <TR  class= common> 
           <TD  class= common> 其他体检信息 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Note" cols="120" rows="3" class="common" ></textarea>
           </TD>
         </TR>
      </table>
	<div id="divinsertbutton" style="display: ''">
	  <input value="体检结果保存" class=cssButton type=button onclick="saveDisDesb();" > 
	  <input value="体检加项" class=cssButton type=button onclick="" > 
  </div>
    <!--读取信息-->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>