<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
       <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
       <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
       <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
       <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
       <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
       <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
       <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
       <script src="jsflInput.js"></script>
	</head>
	<body>
		<form name='fm' action="jsflSave.jsp" method="post" name="fm" target="fraSubmit">
			<!-- 页面头部 -->
			<div>
				<h3>价税分离当天保全增减人金额调整：</h3>
				<span style="color: red">1、签单当天保全进行增减人导致lcpol数据改变，引起的契约首期价税分离金额错误</span><br>
				<span style="color: red">2、从暂收表中获取契约首次缴费金额进行重新价税</span><br>
				<span>------------------------------------------</span><br>
			</div>
			<br>
			<!-- 页面头部结束 -->
			<div>
				<table>
					<tr>
						<td><img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPart1);"></td>
						<td class="titleImg">调整价税分离数据</td>
					</tr>
				</table>
			</div>
			<div id="divPart1">
				<table class=common>
					<tr class = common>
						<td class = title>暂收号&nbsp;<input name="tempfeeno" class = common></td>
					</tr>
					<tr>
						<td><input value="调  整" class=cssButton type=button onclick="return run();"></td>
					</tr>
				</table>
			</div><br>
			<!-- 第一种情况结束 -->
			<!-- 设置flag标签，区分是哪种业务形式 -->
			<input name="flag" type="hidden">
			<!-- flag标签结束 -->
		</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>