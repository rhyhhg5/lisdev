var turnPage = new turnPageClass();      
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();      

//查询polGrid的初始化数据
function queryPolInfo()
{
  var groupMoney = "0";
  var sql = "select insuaccbala FROM LCINSUREACC where grpcontno = '"+fm.all("GrpContNo").value+"' and acctype = '001'";
  var arrResult = easyExecSql(sql);
  if (arrResult)
  {
    for (i = 0; i < arrResult.length; i++)
    {
      groupMoney = arrResult[i][0];
    }
  }
  sql = "select a.GrpPolNo, a.RiskSeqNo, a.RiskCode, b.RiskName, '" + groupMoney + "' " +
        "from LCGrpPol a ,LMRiskApp b " +
        "where a.GrpContNo = '" + fm.all("GrpContNo").value + "' and a.RiskCode = b.RiskCode "+
        "order by RiskSeqNo ";
  turnPage3.pageLineNum = 100;
  turnPage3.pageDivName = "divPage333";
  turnPage3.queryModal(sql, PolGrid);
}

//万能险账户查询按钮功能
function GPublicAccQuery()
{
  var strSql = "SELECT prtno from lcgrppol l where grpcontno='"+fm.all( 'GrpContNo' ).value+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode) ";
  var arrResult = easyExecSql(strSql);
  if(arrResult != null){
      window.open("../sys/GPublicAccQueryInput.jsp?GrpContNo="+fm.all( 'GrpContNo' ).value+"&PrtNo="+arrResult[0][0]);  
  }else{
  	alert("查询异常");
  }
}

//被保人模板
function grpInsuList(){
  window.open("../sys/GrpUliInsuredInput.jsp?ContNo=" + fm.all( 'GrpContNo' ).value + "&ModeFlag=0&ContLoadFlag=2" +"&ContType=1");  
}

//查询导入的数据
function queryImportData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, Money " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '1' " +
	             "order by Int(SerialNo) ";
	turnPage.queryModal(sql, InsuredListGrid); 
}

//查询导入无效的数据
function queryFailData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, Money, ErrorReason " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '0' " +
	             "order by Int(SerialNo) ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, FailGrid); 
}

//磁盘导入 
function importInsured()
{
	var url = "./BqDiskImportMain.jsp?EdorNo=" + fm.all("EdorNo").value + 
	          "&EdorType=" + fm.all("EdorType").value +
	          "&GrpContNo=" + fm.all("GrpContNo").value;
	var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
	window.open(url, "特需帐户资金导入", param);
}

//数据提交后的操作
function afterSubmit(flag, content)
{
  try 
  { 
    showInfo.close();
	  window.focus();
	}
	catch (ex) {}
	
	if (flag == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		returnParent();
	}
}

//保存
function save()
{
  if (InsuredListGrid.mulLineCount == 0)
	{
		alert("没有有效的导入数据！");
		return false;
	}
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "GEdorTypeTASubmit.jsp"
	fm.submit();
}

//返回父窗口
function returnParent()
{
	try
	{
		top.opener.getGrpEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {};
}