<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：LCPolQueryDir.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
  //输出参数
   
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  
  //全局变量
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

  //保单信息部分
  LCPolSchema tLCPolSchema = new LCPolSchema();
  tLCPolSchema.setPolNo(request.getParameter("PolNo"));
  System.out.println("-------222");

  System.out.println("LCPolQuery.PolNo:"+tLCPolSchema.getPolNo());
  
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.addElement(tGlobalInput);
  tVData.addElement(tLCPolSchema);
  System.out.println("-------333");

  // 数据传输
  CPolUI tCPolUI   = new CPolUI();
	if (!tCPolUI.submitData(tVData,"QUERY||MAIN"))
	{
     		 Content = tCPolUI.mErrors.getError(0).errorMessage;
     		 FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tCPolUI.getResult();
		System.out.println("------tjj----");
		// 显示
		LCPolSet mLCPolSet = new LCPolSet(); 
		mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolBLSet",0));
		if (mLCPolSet==null)
		{	Content = "没有这张保单！";
			FlagStr = "Fail";
		}
		else
		{
			LCPolSchema mLCPolSchema = mLCPolSet.get(1);
			System.out.println("-----sumPrem"+mLCPolSchema.getSumPrem());
			System.out.println("-----n"+mLCPolSet.size()); 
			   	%>
			   	<script language="javascript">
			   	 	parent.fraInterface.fm1.PolNo.value="<%=mLCPolSchema.getPolNo()%>";
			   		parent.fraInterface.fm.PolNo.value="<%=mLCPolSchema.getPolNo()%>";
			   		parent.fraInterface.fm.all("GetPolDate").value = "<%=mLCPolSchema.getCustomGetPolDate()%>";
	    	 		parent.fraInterface.fm.all("InsuredName").value = "<%=mLCPolSchema.getInsuredName()%>";
	    	 		parent.fraInterface.fm.all("AppntName").value = "<%=mLCPolSchema.getAppntName()%>";
	    	 		parent.fraInterface.fm.all("ValiDate").value = "<%=mLCPolSchema.getCValiDate()%>";
						parent.fraInterface.fm.all("PaytoDate").value = "<%=mLCPolSchema.getPaytoDate()%>";
	    	 		parent.fraInterface.fm.all("Prem").value = "<%=mLCPolSchema.getPrem()%>";
	    	 		parent.fraInterface.fm.all("AgentCode").value = "<%=mLCPolSchema.getAgentCode()%>";
	    	 		parent.fraInterface.fm.all("RiskCode").value = "<%=mLCPolSchema.getRiskCode()%>";
	    	 		
	    	 		//parent.fraInterface.fm.all("PayMode").value = "<%=mLCPolSchema.getPayMode()%>";
			   	</script>
		<%
		}		
	}
 // end of if  
 
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tCPolUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.emptyUndefined();
	parent.fraInterface.afterSubmit1("<%=FlagStr%>","<%=Content%>");
</script>
</html>