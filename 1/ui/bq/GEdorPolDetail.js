//该文件中包含客户端需要处理的函数和事件
var showInfo;
var GEdorFlag = true;
var selGridFlag = 0;            //标识当前选中记录的GRID

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();           //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//重定义获取焦点处理事件
window.onfocus = initFocus;

//查询按钮
function queryClick()
{
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	var strSql = "select ContNo, InsuredNo, Name, Sex, Birthday, IDType, IDNo from LCInsured where GrpContNo='" 
		   + fm.GrpContNo.value + "'" 
		   + "and ContNo  not in  (select ContNo from LPEdorItem where edortype='" + fm.EdorType.value + "' and edorno='" + fm.EdorNo.value + "')";
		   + getWherePart('ContNo', 'ContNo2')
		   + getWherePart('CustomerNo', 'CustomerNo2')
		   + getWherePart('Name', 'Name2', 'like', '0');

	turnPage.queryModal(strSql, LCInsuredGrid);
	if (selGridFlag == 1)
	{
		fm.ContNo.value = "";
		fm.CustomerNo.value = "";
		selGridFlag = 0;
	}

	showInfo.close();	 
}

//查询按钮
function queryClick2()
{
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	var strSql = "select ContNo, InsuredNo, Name, Sex, Birthday, IDType, IDNo from lcinsured where ContNo in "
			   + " (select ContNo from LPEdorItem where edortype='" + fm.EdorType.value 
			   + "' and edorno='" + fm.EdorNo.value + "')"
			   + getWherePart('ContNo', 'ContNo3')
			   + getWherePart('CustomerNo', 'CustomerNo3')
			   + getWherePart('Name', 'Name3', 'like', '0');

	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, LCInsured2Grid);
	if (selGridFlag == 2)
	{
		fm.ContNo.value = "";
		fm.CustomerNo.value = "";
		selGridFlag = 0;
	}

	showInfo.close();	 
}

//单击MultiLine的单选按钮时操作
function getLCPol(parm1, parm2)
{	
	
	var ContNo=fm.all(parm1).all('LCInsuredGrid1').value;
	var InsuredNo=fm.all(parm1).all('LCInsuredGrid2').value;
	initLCPolGrid();	

    //险种信息初始化
    if(InsuredNo!=null&&InsuredNo!="")
    {
        var strSQL ="select PolNo,RiskCode,Prem,Amnt,CValiDate,PaytoDate from LCPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'";
        alert(strSQL);
        //alert(strSQL);
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = LCPolGrid;    
        //保存SQL语句
        turnPage.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}

//单击MultiLine的单选按钮时操作
function getLPPol(parm1, parm2)
{	
	var ContNo=fm.all(parm1).all('LCInsured2Grid1').value;
	var InsuredNo=fm.all(parm1).all('LCInsured2Grid2').value;
	var EdorNo=fm.all(EdorNo).value;
	var EdorType=fm.all(EdorType).value;
	initLCPol2Grid();	

    //险种信息初始化
    if(InsuredNo!=null&&InsuredNo!="")
    {
        var strSQL ="select a.PolNo,a.RiskCode,a.Prem,a.Amnt,a.CValiDate,a.PaytoDate from LCPol a,LPEdorItem b where a.InsuredNo='"+InsuredNo+"' and a.ContNo='"+ContNo+"'"
                    + " and b.EdorNo='"+EdorNo+"' and b.EdorType='"+EdorType+"' and a.polno=b.polno";
        //alert(strSQL);
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = LCPol2Grid;    
        //保存SQL语句
        turnPage.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}

//进入个人保全
function GEdorDetail()
{
	if (selGridFlag == 0)
	{
		alert("请先选择一个被保人！");
		return;
	}
	var showStr = "正在申请集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	if (selGridFlag == 1)
	{
		fm.all("Transact").value = "INSERT||EDOR";
	}
	fm.submit();
	fm.all("Transact").value = "";
}

//打开个人保全的明细界面
function openGEdorDetail() 
{
	if (fm.ContNo.value=="")
	{
		alert("不能是空记录!");
	}
	else
	{
		fm.CustomerNoBak.value = fm.CustomerNo.value;
		fm.ContNoBak.value = fm.ContNo.value;
		window.open("./GEdorType" + trim(fm.all('EdorType').value) + ".jsp");
		showInfo.close();	 
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result)
{
	showInfo.close();

	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		GEdorFlag = true;

		if (fm.Transact.value=="DELETE||EDOR")
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		}
		else
		{
			openGEdorDetail();
		}
	}
}

//处理获取焦点事件
function initFocus() 
{
  if (GEdorFlag)
  {   
    GEdorFlag = false;
    queryClick();
    queryGEdorList();
 	selGridFlag = 0;
  }
}

//查询出申请后的个人保全列表 已经作过保全项目的 
function queryGEdorList() 
{
	var strSql = "select ContNo, InsuredNo, Name, Sex, Birthday, IDType, IDNo from LcInsured where GrpContNo= '" + fm.all('GrpContNo').value + "' and ContNo in "
			   + " (select ContNo from LPEdorItem where edortype='" + fm.EdorType.value + "' and edorno='" + fm.EdorNo.value + "')";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, LCInsured2Grid); 	 
	if (selGridFlag == 2)
	{
		fm.ContNo.value = "";
		fm.CustomerNo.value = "";
		selGridFlag = 0;
	}
}

//单击MultiLine的单选按钮时操作
function reportDetail2Click(parm1, parm2)
{	
	fm.ContNo.value = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 1);
	fm.CustomerNo.value = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 2);
	selGridFlag = 2;
	queryClick();
}

//撤销集体下个人保全
function cancelGEdor()
{
	fm.all("Transact").value = "DELETE||EDOR"

	var showStr = "正在撤销集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.action = "./GEdorAppCancelSubmit.jsp";
	fm.submit();
	fm.action = "./GEdorTypeDetailSubmit.jsp";
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent() {
	top.close();
}
