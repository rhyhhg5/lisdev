<%
//程序名称：PEdorTypeACSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  LPAppntSchema tLPAppntSchema   = new LPAppntSchema();
  LPAddressSchema tLPAddressSchema   = new LPAddressSchema();
  LPEdorItemSchema tLPEdorItemSchema   = new LPEdorItemSchema();
  PEdorACDetailUI tPEdorACDetailUI   = new PEdorACDetailUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  String return2="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
  
  //个人保单批改信息
  tLPEdorItemSchema.setPolNo("000000");
  tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
	tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
  tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
	tLPEdorItemSchema.setInsuredNo(request.getParameter("AppntNo"));
	String theCurrentDate = PubFun.getCurrentDate();
  String theCurrentTime = PubFun.getCurrentTime();
  tLPEdorItemSchema.setMakeDate(theCurrentDate);
  tLPEdorItemSchema.setMakeTime(theCurrentTime);
  tLPEdorItemSchema.setModifyDate(theCurrentDate);
  tLPEdorItemSchema.setModifyTime(theCurrentTime);
  tLPEdorItemSchema.setOperator("001");
	
	
	tLPAppntSchema.setContNo(request.getParameter("ContNo"));
  tLPAppntSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPAppntSchema.setEdorType(request.getParameter("EdorType"));	
	
	tLPAppntSchema.setAppntNo(request.getParameter("AppntNo"));
	tLPAppntSchema.setAppntName(request.getParameter("AppntName"));
	tLPAppntSchema.setAppntSex(request.getParameter("AppntSex"));
	tLPAppntSchema.setAppntBirthday(request.getParameter("AppntBirthday"));
	tLPAppntSchema.setIDType(request.getParameter("IDType"));
	tLPAppntSchema.setIDNo(request.getParameter("IDNo"));
	tLPAppntSchema.setOccupationType(request.getParameter("OccupationType"));
	tLPAppntSchema.setOccupationCode(request.getParameter("OccupationCode"));
	tLPAppntSchema.setMarriage(request.getParameter("Marriage"));
	tLPAppntSchema.setNationality(request.getParameter("Nationality"));
	tLPAppntSchema.setBankCode(request.getParameter("BankCode"));
	tLPAppntSchema.setBankAccNo(request.getParameter("BankAccNo"));
	tLPAppntSchema.setBankAccNo(request.getParameter("BankAccNo"));
	tLPAppntSchema.setAccName(request.getParameter("AccName"));
	
	tLPAddressSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPAddressSchema.setEdorType(request.getParameter("EdorType"));	
	tLPAddressSchema.setCustomerNo(request.getParameter("AppntNo"));
	tLPAddressSchema.setAddressNo(request.getParameter("AddressNo"));
	
	tLPAddressSchema.setPostalAddress(request.getParameter("PostalAddress"));
	tLPAddressSchema.setZipCode(request.getParameter("ZipCode"));
	tLPAddressSchema.setPhone(request.getParameter("Phone"));
	tLPAddressSchema.setMobile(request.getParameter("Mobile"));
	tLPAddressSchema.setFax(request.getParameter("Fax"));
	tLPAddressSchema.setHomeAddress(request.getParameter("HomeAddress"));
	tLPAddressSchema.setHomeZipCode(request.getParameter("HomeZipCode"));
  tLPAddressSchema.setHomePhone(request.getParameter("HomePhone"));
  tLPAddressSchema.setHomeFax(request.getParameter("HomeFax"));
  tLPAddressSchema.setCompanyAddress(request.getParameter("CompanyAddress"));
	tLPAddressSchema.setCompanyZipCode(request.getParameter("CompanyZipCode"));
  tLPAddressSchema.setCompanyPhone(request.getParameter("CompanyPhone"));
  tLPAddressSchema.setCompanyFax(request.getParameter("CompanyFax"));
  //System.out.println("tLPAddressSchema.getHomePhone-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= = "+tLPAddressSchema.getHomePhone());
  
try
  {
  // 准备传输数据 VData
  
  	 VData tVData = new VData();  
  	
	 //保存个人保单信息(保全)	
	 tVData.addElement(tG);
	 tVData.addElement(tLPEdorItemSchema);
   tVData.addElement(tLPAppntSchema);
	 tVData.addElement(tLPAddressSchema);
   tPEdorACDetailUI.submitData(tVData,transact);	
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorACDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";   
			if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))
			{
				if (tPEdorACDetailUI.getResult()!=null&&tPEdorACDetailUI.getResult().size()>0)
				{
					Result = (String)tPEdorACDetailUI.getResult().get(0);
					return2 = (String)tPEdorACDetailUI.getResult().get(1);
					if (Result==null||Result.trim().equals(""))
					{
						FlagStr = "Fail";
						Content = "查询失败或未查询到相应记录!!";
					}
					System.out.println("return2 :"+return2);
					System.out.println("Result :"+Result);
				}
			} 
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>","<%=return2%>");
</script>
</html>

