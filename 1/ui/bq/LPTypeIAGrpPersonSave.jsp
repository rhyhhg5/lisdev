<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
 <%
//程序名称：LDGrpSave.jsp
//程序功能：
//创建日期：2002-1-7
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%> 
  <%@page import="java.lang.*"%>    
<%

  //接收信息，并作校验处理。
  //输入参数
  System.out.println("start Save");
  LDGrpSchema tLDGrpSchema   = new LDGrpSchema();
  LDGrpUI tLDGrpUI   = new LDGrpUI();

  //输出参数
  CErrors tError = null;
  String tBmCert = "";
  //后面要执行的动作：添加，修改，删除
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
   String Operator  = tGI.Operator ;  //保存登陆管理员账号
   //String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码

  String transact=request.getParameter("Transact");
System.out.println("transact:"+transact);
   
    if(transact.equals("INSERT"))
    {
    //生成客户号
    /*
    String tLimit=PubFun.getNoLimit(ManageCom);
    String GrpNo=PubFun1.CreateMaxNo("GrpNo",tLimit);           
    tLDGrpSchema.setGrpNo(GrpNo);
    */
    }
    else
    {
    tLDGrpSchema.setGrpNo(request.getParameter("GrpNo"));    
    }
    tLDGrpSchema.setPassword(request.getParameter("Password"));
 System.out.println("password"+tLDGrpSchema.getPassword());
    tLDGrpSchema.setGrpName(request.getParameter("GrpName"));
    tLDGrpSchema.setGrpAddressCode(request.getParameter("GrpAddressCode"));
    tLDGrpSchema.setGrpAddress(request.getParameter("GrpAddress"));
    tLDGrpSchema.setGrpZipCode(request.getParameter("GrpZipCode"));
    tLDGrpSchema.setBusinessType(request.getParameter("BusinessType"));
    tLDGrpSchema.setGrpNature(request.getParameter("GrpNature"));
    tLDGrpSchema.setPeoples(request.getParameter("Peoples"));
    tLDGrpSchema.setRgtMoney(request.getParameter("RgtMoney"));
    tLDGrpSchema.setAsset(request.getParameter("Asset"));
    tLDGrpSchema.setNetProfitRate(request.getParameter("NetProfitRate"));       
    tLDGrpSchema.setMainBussiness(request.getParameter("MainBussiness"));
    tLDGrpSchema.setCorporation(request.getParameter("Corporation"));
    tLDGrpSchema.setComAera(request.getParameter("ComAera"));
    tLDGrpSchema.setLinkMan1(request.getParameter("LinkMan1"));
    tLDGrpSchema.setDepartment1(request.getParameter("Department1"));
    tLDGrpSchema.setHeadShip1(request.getParameter("HeadShip1"));
    tLDGrpSchema.setPhone1(request.getParameter("Phone1"));
    tLDGrpSchema.setE_Mail1(request.getParameter("E_Mail1"));
    tLDGrpSchema.setFax1(request.getParameter("Fax1"));
    tLDGrpSchema.setLinkMan2(request.getParameter("LinkMan2")); 
    tLDGrpSchema.setDepartment2(request.getParameter("Department2"));
    tLDGrpSchema.setHeadShip2(request.getParameter("HeadShip2"));
    tLDGrpSchema.setPhone2(request.getParameter("Phone2"));
    tLDGrpSchema.setE_Mail2(request.getParameter("E_Mail2"));
    tLDGrpSchema.setFax2(request.getParameter("Fax2"));    
    tLDGrpSchema.setFax(request.getParameter("Fax"));
    tLDGrpSchema.setPhone(request.getParameter("Phone"));
    tLDGrpSchema.setGetFlag(request.getParameter("GetFlag"));
    tLDGrpSchema.setSatrap(request.getParameter("Satrap"));
    tLDGrpSchema.setEMail(request.getParameter("EMail"));    
    tLDGrpSchema.setFoundDate(request.getParameter("FoundDate"));  
    tLDGrpSchema.setBankCode(request.getParameter("BankCode"));
    tLDGrpSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLDGrpSchema.setGrpGroupNo(request.getParameter("GrpGroupNo"));
    tLDGrpSchema.setState(request.getParameter("State"));
    tLDGrpSchema.setRemark(request.getParameter("Remark"));
    tLDGrpSchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));
    tLDGrpSchema.setOperator(Operator);
    
try
	{
  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tLDGrpSchema);
   tVData.addElement(tGI);
    
   //执行动作：INSERT 添加纪录，UPDATE 修改纪录，DELETE 删除纪录
   if(tLDGrpUI.submitData(tVData,transact))
  {
    		if (transact.equals("INSERT"))
		{
		    	System.out.println("------return");
			    	
		    	tVData.clear();
		    	tVData = tLDGrpUI.getResult();
		    	System.out.println("-----size:"+tVData.size());
		    	
		    	LDGrpSchema mLDGrpSchema = new LDGrpSchema(); 
			mLDGrpSchema=(LDGrpSchema)tVData.getObjectByObjectName("LDGrpSchema",0);
			
			System.out.println("test");
			
			if( mLDGrpSchema.getGrpNo() == null ) {
				System.out.println("null");
			}
			
			String strGrpNo = mLDGrpSchema.getGrpNo();
			System.out.println("jsp"+strGrpNo);
			if( strGrpNo == null ) {
				strGrpNo = "123";
				System.out.println("null");
			}
			
			// System.out.println("-----:"+mLAAgentSchema.getAgentCode());
		%>
		<SCRIPT language="javascript">
			parent.fraInterface.fm.all("GrpNo").value ="<%=strGrpNo%>";
		</SCRIPT>
		<%
		}
	}
}
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDGrpUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
    }
}//页面有效区
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

