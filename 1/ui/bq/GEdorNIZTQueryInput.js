var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
turnPage1.pageLineNum = 20;       //根据需求，显示20行
var turnPage2 = new turnPageClass();
var mSql="";
var showInfo;

//简单查询
function easyQuery()
{
	var startDate = fm.all('StartDate').value;
	var endDate = fm.all('EndDate').value;
	var tManageCom = fm.all('ManageCom').value;//管理机构
	var tGrpContNo = fm.all('GrpContNo').value;//保单号
	var DealState = fm.all('DealState').value;//处理状态
	var EdorType = fm.all('EdorType').value;//保全类型
	
	//管理机构校验
	if(tManageCom == "" || tManageCom == null)
	{
		alert("请选择管理机构！");
		return false;
	}
	
	if(tManageCom == "86")
    {
      if(!confirm("管理机构选择“总公司”，查询速度会很慢，是否继续？"))
      {
        return false;
      }
    }
	
	  //处理状态校验
    if(DealState == "" || DealState == null || DealState == "0")
	{
		alert("请选择处理状态！");
		return false;
	}
	
	//申请日期起期校验
	if (startDate == ""||startDate==null)
	{
		alert("请输入申请日期起期！");
		return;
	}
	
	//申请日期止期校验
	if (startDate==null||endDate == "")
	{
		alert("请输入申请日期止期！");
		return;
	}
	
	//保全类型校验
	if (EdorType == null|| EdorType == "")
	{
		alert("请选择保全类型");
		return;
	}
	
//	var t1 = new Date(startDate.replace(/-/g,"\/")).getTime();
//    var t2 = new Date(endDate.replace(/-/g,"\/")).getTime();
//    var tMinus = (t2-t1)/(24*60*60*1000);  
//    if(tMinus>31 )
//    {
//	  alert("查询起止时间不能超过1个月！")
//	  return false;
//    }
	
   
	//限制申请日期
    var wherePar=" and a.acceptdate >= '"+startDate+"' and a.acceptdate<='"+endDate+" ' and  ";
    
    //限制管理机构
    if(!(tManageCom == "" || tManageCom == null))
    {
      wherePar+="a.ManageCom like '"+tManageCom+"%' and " ; 
    }

    //限制保单号
    if( !(tGrpContNo == "" ||  tGrpContNo == null))
    {
       wherePar+="a.GrpContNo='"+tGrpContNo+"' and " ;               
    }
    
    //限制处理状态 
    if( !(DealState == "" ||  DealState == null))
    {
       wherePar+="a.DealState='"+DealState+"' and " ;
    }
    
    //限制保全类型
    if(EdorType == "0")
    {
    	wherePar+="a.edortype in ('NI','ZT') ";
    } 
    else if (EdorType == "1")
    {
    	wherePar+="a.edortype ='NI' ";
    }
    else 
    {
    	wherePar+="a.edortype ='ZT' ";
    }

   var strSQL =  "select a.batchno,a.grpcontno, b.grpname, a.edortype, " 
	   		  +  " (case when a.edortype='NI' then (select count(1) from lbinsuredlist where serialno=a.batchno) " 
	   		  +  " else (select count(1) from lpdiskimport where edorno=a.batchno) end ), " 
	   		  +  " (case when a.dealstate='1' then '审核中' else '处理完成' end ), " 
	   		  +  " a.acceptdate, a.confdate, " 
	   		  +  " (case when a.dealresult='1' then '成功' when a.dealresult='2' then '失败' else '待处理' end ), " 
	   		  +  " a.edorno, a.remark "
			  +  " from LPNIZTInsured a, lcgrpcont b " 
			  +  "where 1=1  "//保单号
			  +  " and a.grpcontno=b.grpcontno "
			  +  wherePar
			  +"  with ur "
			  ;
	mSql=strSQL;
	turnPage1.queryModal(strSQL, ContPauseGrid);  
	if( ContPauseGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}
	fm.sql.value =strSQL;
	showCodeName(); 
}

//处理完毕
function dealComplete() 
{
	var selNo = ContPauseGrid.getSelNo();
	if (selNo == null || selNo == 0) {
		alert("请选择一条处理记录");
		return false;
	} else {

		var tRemark = fm.all('Remark').value;// 保全类型
		var tEdorNo = fm.all('EdorNo').value;// 工单号
		var tDealResult = fm.all('DealResult').value;// 状态

		var tBatchNo = ContPauseGrid.getRowColData(selNo - 1, 1);
		var tGrpContNo = ContPauseGrid.getRowColData(selNo - 1, 2);
		
		
		if (fm.all('DealResult').value == '') {
			alert("请选择处理结果");
			return false;

		}

		if (tDealResult == 1) {
			var tStateSQL = "select edorstate from lpedorapp " + "where 1=1  "// 保单号
					+ "and edoracceptno = '" + EdorNo + "'" + "  with ur ";

			var rResult = easyExecSql(tStateSQL);
			if (rResult != 0) {
				alert("该工单：" + EdorNo + "未结案，暂无法进行处理成功操作！");
				return false;
			}
		}

		var showStr = "正在处理，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fmSaveAll.all('SubmitBatchNo').value = tBatchNo;
		fmSaveAll.all('SubmitGrpContNo').value = tGrpContNo;
		fmSaveAll.all('SubmitRemark').value = tRemark;
		fmSaveAll.all('SubmitEdorNo').value = tEdorNo;
		fmSaveAll.all('SubmitDealResult').value = tDealResult;
		fmSaveAll.submit();
	}
}


//打印
function exportList(){
	if( ContPauseGrid.mulLineCount == 0)
	{
		alert("没有需要打印的信息");
		return false;
	}
	
	var selNo = ContPauseGrid.getSelNo();
	if (selNo == null || selNo == 0) {
		alert("请选择一条处理记录");
		return false;
	} else {
		var tBatchNo = ContPauseGrid.getRowColData(selNo - 1, 1);
		fm.all('PrintBatchNo').value = tBatchNo;
		var EdorType = fm.all('EdorType').value;//保全类型  1--增人，2--减人
	
		fm.submit();
	}
}

function afterSubmit2(FlagStr,Content){
    alert(Content);
}


//处理结果为成功时，需要录入工单号
function afterCodeSelect(cCodeName, Field) {
	var StatusResult = Field.value;
	if(cCodeName == "DealResult") {
		if(StatusResult == 1){
			divEdorNo.style.display='';
		} else {
			divEdorNo.style.display='none';
		}
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
   var urlStr="";
  if (FlagStr == "Fail" )
  {             
  urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  //	parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	
  }
}









