<%
//程序名称：GEdorTypeNISubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String flag;
	String content;
	
	String calType = request.getParameter("CalType");
	String revertType = request.getParameter("RevertType");
	String calTime = request.getParameter("CalTime");
	if ((calType != null) && (calType.equals("1"))) //一年期险选月份表必须还原
	{
		revertType = "1";
	}
	if ((calType != null) && (calType.equals("2"))) //选月份比例不用还原
	{
		revertType = "2";
	}

	GlobalInput gi = (GlobalInput)session.getValue("GI");
	LPGrpEdorItemSchema itemSchema = new LPGrpEdorItemSchema();
	itemSchema.setEdorNo(request.getParameter("EdorNo"));
	itemSchema.setEdorType(request.getParameter("EdorType"));
	itemSchema.setGrpContNo(request.getParameter("GrpContNo"));
	
	EdorItemSpecialData tSpecialData = new EdorItemSpecialData(itemSchema);
	tSpecialData.add("CalType", calType);
	tSpecialData.add("RevertType", revertType);
	tSpecialData.add("CalTime", calTime);
  tSpecialData.add("CreateAccFlag", request.getParameter("CreateAccFlag"));
  tSpecialData.add("InputType", request.getParameter("InputType"));
  tSpecialData.add("Source", request.getParameter("Source"));
  tSpecialData.add("Prem", request.getParameter("Prem"));
  
  System.out.println(request.getParameter("CreateAccFlag"));
  System.out.println(request.getParameter("InputType"));
    System.out.println(request.getParameter("Source"));
	
	VData data = new VData();
	data.add(gi);
	data.add(itemSchema);
	data.add(tSpecialData);

	GrpEdorNIDetailUI tGrpEdorNIDetailUI = new GrpEdorNIDetailUI();
	if (!tGrpEdorNIDetailUI.submitData(data))
	{
		flag = "Fail";
		content = "数据保存失败！原因是:" + tGrpEdorNIDetailUI.getError();
	}
	else 
	{
		flag = "Succ";
		content = "数据保存成功。";
	}
	String message = tGrpEdorNIDetailUI.getMessage();
	if ((message != null) && (!message.equals("")))
	{
		content += message;
	}
	content = PubFun.changForHTML(content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>