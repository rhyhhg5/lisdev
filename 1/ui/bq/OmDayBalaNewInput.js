//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	 parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function clearRisk()
{
  fm.RiskCode.value = "";
  fm.RiskName.value = "";
}

function Download()
{
  if(!verifyInput2())
  {
    return false;
  }
  var startdate = fm.StartDate.value;
  var enddate = fm.EndDate.value;
  var managecom = trim(fm.ManageCom.value);
  var riskcode = trim(fm.RiskCode.value);

  if(managecom=="86")
  {
    if(riskcode=="") 
    {
      if(!confirm("管理机构选择“总公司”并且没有选择险种，查询速度会很慢，是否继续？"))
      {
        return false;
      }
    }
    else
    {
      if(!confirm("管理机构选择“总公司”，查询速度会很慢，是否继续？"))
      {
        return false;
      }
    }
  }
  
  //起始时间不能为空
	if(startdate == "" || startdate == null)
	{
		alert("起始时间不能为空！");
		return false;
	}
	//终止时间不能为空
	if(enddate == "" || enddate == null)
	{
		alert("终止时间不能为空！");
		return false;
	}
	
	//查询时间1个月校验
	var t1 = new Date(startdate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(enddate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>31)
    {
		alert("查询时间区间不能超过1个月!");
		return false;
    }
    
  fm.action = "../bq/OmDayBalaNewDownload.jsp";
  fm.submit();
}

function afterDownload(outname,outpathname)
{
  var url = "../f1print/download.jsp?filename="+outname+"&filenamepath="+outpathname;
  fm.action=url;
  fm.submit();
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

  
