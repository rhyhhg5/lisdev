<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GEdorTypeMJInit.jsp
//程序功能：工单管理个人信箱页面初始化
//创建日期：2006-01-20 9:40:36
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>              

<script language="JavaScript">
var edorNo = "<%=request.getParameter("DetailWorkNo")%>";
var edorType = "MJ";

var queryGrpContSql = "  select contNo "
                      + "from LGWork "
                      + "where detailWorkNo = '" + edorNo + "' "
var result = easyExecSql(queryGrpContSql);
var grpContNo = result[0][0];
var loadFlag = "<%=request.getParameter("loadFlag")%>";

//表单初始化
function initForm()
{
	try
	{
	  initLCGrpPolGrid();
	  queryLCGrpPol();
	
	  controlButton();
	}
	catch(re)
	{
		alert("GEdorTypeMJInit.jsp-->InitForm函数中发生异常:初始化界面错误!" + re.message);
	}
}

function initLCGrpPolGrid()
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=200;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="险种号";         	  //列名
    iArray[1][1]="80px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="grpPolNo";
    
    iArray[2]=new Array();
    iArray[2][0]="险种编码";         	  //列名
    iArray[2][1]="20px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="riskCode";
    
    iArray[3]=new Array();
    iArray[3][0]="险种名称";          //列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许;
    
    LCGrpPolGrid = new MulLineEnter("fm", "LCGrpPolGrid"); 
    //设置Grid属性
    LCGrpPolGrid.mulLineCount = 0;
    LCGrpPolGrid.displayTitle = 1;
    LCGrpPolGrid.locked = 1;
    LCGrpPolGrid.canSel = 1;
    LCGrpPolGrid.canChk = 0;
    LCGrpPolGrid.hiddenSubtraction = 1;
    LCGrpPolGrid.hiddenPlus = 1;
    LCGrpPolGrid.selBoxEventFuncName = "showLCGrpPolInfo";
    LCGrpPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
}

</script>