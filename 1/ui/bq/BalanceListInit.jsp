<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>	

<%
//程序名称：BalanceListInit.jsp
//程序功能：万能保单月度结算清单查询界面初始化
//创建日期：2007-12-14
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<% 
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>

<script language="JavaScript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var operator = "<%=tGI.Operator%>"; //记录当前登录人
  var tCurrentDate = "<%=tCurrentDate%>";
  var tCurrentTime = "<%=tCurrentTime%>";
 
//输入框的初始化 
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = manageCom;
    var sql1 = "select Name from LDCom where ComCode = '" + manageCom + "'";
    fm.all('ManageComName').value = easyExecSql(sql1);
    //fm.all('ManageComName').value = easyExecSql("select Name from LDCom where ComCode = '" + manageCom + "'");
    fm.all('PolYear').value = "";
    fm.all('PolMonth').value = "";
    var sql1 = "select Current Date - 3 month from dual ";
    fm.all('RunDateStart').value = easyExecSql(sql1);
    fm.all('RunDateEnd').value = tCurrentDate;
    fm.all('SaleChnl').value = "";
    fm.all('SaleChnlName').value = "";
  }
  catch(ex)
  {
    alert("在BalanceListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initInpBox();
    initLCInsureAccBalanceGrid(); 
    initElementtype();
  }
  catch(re) 
  {
    alert("在BalanceListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//Mulline的初始化
function initLCInsureAccBalanceGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=3;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="管理机构";      //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
   
    iArray[2]=new Array();
    iArray[2][0]="保单号";      //列名
    iArray[2][1]="130px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;   
    iArray[2][21]="ContNo";   
    
    iArray[3]=new Array();
    iArray[3][0]="投保人";      //列名
    iArray[3][1]="50px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[4]=new Array();
    iArray[4][0]="结算年度";      //列名
    iArray[4][1]="50px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[5]=new Array();
    iArray[5][0]="结息月度";      //列名
    iArray[5][1]="50px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="本次结息金";      //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="管理费扣除";      //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="风险保费扣除";      //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="持续奖金";      //列名
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[10]=new Array();
    iArray[10][0]="结算后金额";      //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=200;           //列最大值
    iArray[10][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[11]=new Array();
    iArray[11][0]="结息日期";      //列名
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=200;           //列最大值
    iArray[11][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="业务员";      //列名
    iArray[12][1]="50px";        //列宽
    iArray[12][2]=200;           //列最大值
    iArray[12][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[13]=new Array();
    iArray[13][0]="业务员代码";      //列名
    iArray[13][1]="80px";        //列宽
    iArray[13][2]=200;           //列最大值
    iArray[13][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[14]=new Array();
    iArray[14][0]="业务员部门";      //列名
    iArray[14][1]="130px";        //列宽
    iArray[14][2]=200;           //列最大值
    iArray[14][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[15]=new Array();
    iArray[15][0]="投保人手机号";      //列名
    iArray[15][1]="100px";        //列宽
    iArray[15][2]=200;           //列最大值
    iArray[15][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    LCInsureAccBalanceGrid = new MulLineEnter("fm", "LCInsureAccBalanceGrid"); 
	  //设置Grid属性
    LCInsureAccBalanceGrid.mulLineCount = 0;
    LCInsureAccBalanceGrid.displayTitle = 1;
    LCInsureAccBalanceGrid.locked = 1;
    LCInsureAccBalanceGrid.canSel = 1;	
    LCInsureAccBalanceGrid.canChk = 0;
    LCInsureAccBalanceGrid.hiddenSubtraction = 1;
    LCInsureAccBalanceGrid.hiddenPlus = 1;
    LCInsureAccBalanceGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在BalanceListInit.jsp-->initLCInsureAccBalanceGrid函数中发生异常:初始化界面错误!");
  }
}
</script>
