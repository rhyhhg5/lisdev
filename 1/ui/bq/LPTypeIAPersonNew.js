//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mAction = "";

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//  showSubmitFrame(mDebug);
  mAction = "INSERT";
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
//  alert(FlagStr);
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //初始化
    //initForm();
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    //parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
    if( mAction == "INSERT" ) mAction = "INSERT||OK";
    
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LDPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";

    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  if(!verifyInput()) 
    return false;
    
  if(fm.all('CustomerNo').value!='')
    {
      alert("不能保存");
      return false;
    }
    
  var strChkIdNo = "";
  //以年龄和性别校验身份证号
  if (fm.IDType.value == "0") {
    strChkIdNo = chkIdNo(fm.IDNo.value, fm.Birthday.value, fm.Sex.value);
  }
  
  if (strChkIdNo != "") {
    alert(strChkIdNo);
    return false;
  }  
    
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function insertClick()
{
  //下面增加相应的代码
  //表单中的隐藏字段"活动名称"赋为insert 
  fm.all('Transact').value ="INSERT";
  submitForm();
  
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  //请考虑修改客户号码的情况
//  alert("update");
  //表单中的隐藏字段"活动名称"赋为update
  fm.all('Transact').value ="UPDATE";
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
 
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	var loadFlag = "0";

	try
	{
		if( top.opener.mShowCustomerDetail == "PROPOSAL" ) loadFlag = "1";
	}
	catch(ex){}
	
	if( loadFlag == "1" )
		parent.fraInterface.window.location = "./LPTypeIAPersonQuery.jsp";
	else
	    window.open("./LPTypeIAPersonQuery.jsp");
  
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
//  alert("delete");
  //尚未考虑全部为空的情况
  if(fm.all('CustomerNo').value == '')
  {
   alert("客户号码不能为空!");
   return false;
  }
  else
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  	
  //表单中的隐藏字段"活动名称"赋为insert	
  fm.all('Transact').value ="DELETE";
  fm.submit();
  parent.fraInterface.initForm();
  }
  
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//关闭本窗口
function closeCurrentWindow()
{
	top.close();
}

// 数据返回父窗口
function returnParent()
{
	if (fm.all('CustomerNo').value == '') {
		alert("请保存后再返回！");
		return;
	}

        
	var arrReturn = new Array();
	arrReturn[0] = new Array();
	arrReturn[0][0] = fm.all('CustomerNo').value;
	top.opener.afterPersonQuery( arrReturn );
	top.close();
/*	
	var loadFlag = "0";
	
	try
	{
		if( top.opener.mShowCustomerDetail == "PROPOSAL" ) loadFlag = "1";
	}
	catch(ex){}
	
	if( loadFlag == "1" )
	{
		if( mAction != "INSERT||OK" )
			alert( "请先保存，再点击返回按钮。" );
		else
		{
			try
			{
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		}
	}
*/	
}

function getQueryResult()
{
	var arrSelected = new Array();

	arrSelected[0] = new Array();
	arrSelected[0][0] = fm.all( 'CustomerNo' ).value;
	arrSelected[0][1] = fm.all( 'Name' ).value;
	arrSelected[0][2] = fm.all( 'Sex' ).value;
	arrSelected[0][3] = fm.all( 'Birthday' ).value;
	arrSelected[0][4] = fm.all( 'IDType' ).value;
	arrSelected[0][5] = fm.all( 'IDNo' ).value;

	return arrSelected;
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	if( arrQueryResult != null )
	{
		fm.all( 'CustomerNo' ).value = arrQueryResult[0][0];
		
		easyQueryClick();
	}
}

// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL = "";
	var tCustomerNo = fm.all( 'CustomerNo' ).value;
	strSQL = "select * from LDPerson where CustomerNo='" + tCustomerNo + "'";
//alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initInpBox();
		 
		// 显示查询结果
		fm.all('CustomerNo').value = arrResult[0][0];
		fm.all('Password').value = arrResult[0][1];
		fm.all('Name').value = arrResult[0][2];
		fm.all('Sex').value = arrResult[0][3];
		fm.all('Birthday').value = arrResult[0][4];
		fm.all('NativePlace').value = arrResult[0][5];
		fm.all('Nationality').value = arrResult[0][6];
		fm.all('Marriage').value = arrResult[0][7];
		fm.all('MarriageDate').value = arrResult[0][8];
		fm.all('OccupationType').value = arrResult[0][9];
		fm.all('StartWorkDate').value = arrResult[0][10];
		fm.all('Salary').value = arrResult[0][11];       //float 
		fm.all('Health').value = arrResult[0][12];
		fm.all('Stature').value = arrResult[0][13];      //float 
		fm.all('Avoirdupois').value = arrResult[0][14];   //float
		fm.all('CreditGrade').value = arrResult[0][15];
		fm.all('IDType').value = arrResult[0][16];
		fm.all('Proterty').value = arrResult[0][17];
		fm.all('IDNo').value = arrResult[0][18];
		fm.all('OthIDType').value = arrResult[0][19];
		fm.all('OthIDNo').value = arrResult[0][20];
		fm.all('ICNo').value = arrResult[0][21];
		fm.all('HomeAddressCode').value = arrResult[0][22];
		fm.all('HomeAddress').value = arrResult[0][23];
		fm.all('PostalAddress').value = arrResult[0][24];
		fm.all('ZipCode').value = arrResult[0][25];
		fm.all('Phone').value = arrResult[0][26];
		fm.all('BP').value = arrResult[0][27];
		fm.all('Mobile').value = arrResult[0][28];
		fm.all('EMail').value = arrResult[0][29];
		fm.all('BankCode').value = arrResult[0][30];
		fm.all('BankAccNo').value = arrResult[0][31];
		fm.all('JoinCompanyDate').value = arrResult[0][32];
		fm.all('Position').value = arrResult[0][33];
		fm.all('GrpNo').value = arrResult[0][34];
		fm.all('GrpName').value = arrResult[0][35];
		fm.all('GrpPhone').value = arrResult[0][36];
		fm.all('GrpAddressCode').value = arrResult[0][37];
		fm.all('GrpAddress').value = arrResult[0][38];
		fm.all('DeathDate').value = arrResult[0][39];
		fm.all('Remark').value = arrResult[0][40];
		fm.all('State').value = arrResult[0][41];
		fm.all('BlacklistFlag').value = arrResult[0][42];
//		fm.all('Operator').value = arrResult[0][43];
		fm.all('WorkType').value = arrResult[0][48];
		fm.all('PluralityType').value = arrResult[0][49];
		fm.all('OccupationCode').value = arrResult[0][50];
		fm.all('Degree').value = arrResult[0][51];
		fm.all('GrpZipCode').value = arrResult[0][52];
		fm.all('SmokeFlag').value = arrResult[0][53];
		fm.all('RgtAddress').value = arrResult[0][54];
		fm.all('HomeZipCode').value = arrResult[0][55];
		fm.all('Phone2').value = arrResult[0][56];
	} // end of if
}


function afterCodeSelect(cCodeName, Field)
{
	try {
		if( cCodeName == 'Marriage'&&Field.value=='0'||Field.value=='5' ) {
			fm.MarriageDate.value="";
			fm.MarriageDate.disabled = true;
		}
		else if( cCodeName == 'Marriage'&&Field.value=='1'||Field.value=='2'||Field.value=='3'||Field.value=='4' ) {
			//fm.MarriageDate.value="";
			fm.MarriageDate.disabled = false;
		}
	} catch(ex) {
		alert("在afterCodeSelect中发生异常");
	}
	
}