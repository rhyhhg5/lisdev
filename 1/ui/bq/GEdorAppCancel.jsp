<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GEdorAppCancel.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GEdorAppCancelInit.jsp"%>
  <title>保全查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./GEdorAppCancelSubmit.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入集体保全查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      	  <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="readonly" name=ManageCom readonly >
          </TD>
          <TD  class= title>
            保全受理号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
          </TD>
          <TD  class= title>
            集体保单号
          </TD>
          <TD  class= input>
            <Input class=common name=GrpContNo >
          </TD>
        </TR>
        <TR  class= common style="display:'none'">
         <TD  class= title>
           批改状态
          </TD>
          <TD  class= input>
            <Input class= common name=EdorState >
          </TD>
        </TR>     
    
    </table>
          
          <!--INPUT VALUE="保单明细" TYPE=button onclick="getQueryDetail();"--> 					
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpEdorMain);">
    		</td>
    		<td class= titleImg>
    			 保全批单信息
    		</td>
    	
    	 <td>
    	 <INPUT VALUE="查  询" class= cssbutton TYPE=button onclick="easyQueryClickMain();"> 
    	 </td>
    	</tr>
    </table>
   
   
  <Div  id= "divLPGrpEdorMain" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanMainGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
     <!-- <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 	
     --> 

      <INPUT VALUE="首  页" class= cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class= cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class= cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class= cssbutton TYPE=button onclick="turnPage.lastPage();">
      
      <input type = "hidden" name='EdorNoHidden' value = '' >	
      
   	<table class= common align=center>
   	
   	    		<tr class= common>
    			<td width="2%" height="24" valign="middle" class="input" ><div align="left">撤销原因
                </div></td>
   			    <td width="98%" valign="middle">	<input type="input" class="code" name='CancelMainReasonCode' readOnly elementtype="nacessary" ondblclick="showCodeList('CancelEdorReason', [this, delMainReason], [0, 1]);" onkeyup=" showCodeListKey('CancelEdorReason', [this, delMainReason] , [0, 1]);"></td>
    		</tr>
    		<tr class= input>
    		  <td valign="middle" class="input" ><div align="left">详细情况</div></td>
  		      <td width="98%" valign="middle"><textarea name="delMainReason" cols="60" rows="3" class="common" id="delMainReason"></textarea>
	          <input name="button2" type=button class= cssbutton onClick="CancelEdorMain();" value="申请撤销"></td>
   		  </tr>
    	</table>      
      			
  	</div> 
    <br>

    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpEdorItem);">
    		</td>
    		<td class= titleImg>
    			 保全项目信息
    		</td>
    	
    	 <!--<td>
    	 <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
    	 </td>-->
    	</tr>
    </table>
  	
  	<input type=hidden name=Transact >
  	<input type=hidden name=DelFlag >
  	<input type=hidden name=EdorType >  
  	<input type=hidden name=MakeDate>
  	<input type=hidden name=MakeTime>
  	
  	 <input type=hidden name=hEdorNo >
  	<input type=hidden name=hGrpContNo >
  	<input type=hidden name=hEdorType >  
  	<input type=hidden name=hEdorState>
  	
  	
  	
  	
  	<Div  id= "divLPGrpEdorItem" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>


   	<table class= common align=center bgcolor='#f9f9f9'>
   	    		<tr class= common>
    			<td width="2%" height="24" valign="middle" class="input" ><div align="left">撤销原因
                </div></td>
   			    <td width="98%" valign="middle">	<input type="input" class="code" name='CancelItemReasonCode' readOnly elementtype="nacessary" ondblclick="showCodeList('CancelEdorReason', [this, delItemReason], [0, 1]);" onkeyup=" showCodeListKey('CancelEdorReason', [this, delItemReason] , [0, 1]);"></td>
    		</tr>
    		<tr class= input>
    		  <td valign="middle" class="input" ><div align="left">详细情况</div></td>
  		      <td width="98%" valign="middle"><textarea name="delItemReason" cols="60" rows="3" class="common" id="delItemReason"></textarea>
	          <input name="button2" type=button class= cssbutton onClick="CancelEdor();" value="申请撤销"></td>
   		  </tr>


    	</table>      
      			
  	</div>
  
  
   
  
  
  
  
  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
