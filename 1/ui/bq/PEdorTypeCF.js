var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage3 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage4 = new turnPageClass();          //使用翻页功能，必须建立为全局变量


function queryOldPol()
{
  var sql = " select insuredno, name, idtype, idno " +
            " from LCinsured a " +
            " where ContNo = '" + fm.ContNo.value + "'" +
            " and insuredno not in (select insuredno from lpinsured " +
            "where edorno = '"+ fm.EdorNo.value +"' )" +
            "order by InsuredNo ";
  turnPage.pageLineNum = 100;
  turnPage.queryModal(sql, OldPolGrid);
}

function queryOldPol234()
{
  var sql = " select insuredno, name, relationtoappnt " +
            " from Lpinsured a " +
            " where " +
            " insuredno not in (select appntno from lppol " +
            "where edorno = '"+ fm.EdorNo.value +"' )" +
            " and edorno = '"+ fm.EdorNo.value +"' " +
            "order by InsuredNo ";
  turnPage4.pageLineNum = 100;
  turnPage4.queryModal(sql, OldPolGrid234);
 
}

function queryLastPol()
{
  var sql = "select ContNo, InsuredNo, InsuredName, " +
  		    "(select riskname from lmriskapp where riskcode=a.riskcode),RiskCode, " +
            " Amnt, Prem,cvalidate,enddate " +
            "from LcPol a " +
            " where ContNo = '" + fm.ContNo.value + "'" +
            " and insuredno not in (select insuredno from lpinsured " +
            "where edorno = '"+ fm.EdorNo.value +"' )" +
            "order by InsuredNo ";
  turnPage3.pageLineNum = 100;
  turnPage3.queryModal(sql, LastPolGrid);
}

function queryNewPol()
{
   var sql = "select ContNo, appntno,InsuredNo, InsuredName, " +
  		    "(select riskname from lmriskapp where riskcode=a.riskcode),RiskCode, " +
            " Amnt, Prem,cvalidate,enddate " +
            "from LPPol a " +
            "where EdorNo = '" + fm.EdorNo.value + "' " +
            "and EdorType = '" + fm.EdorType.value + "' ";
  turnPage2.pageLineNum = 100;
  turnPage2.queryModal(sql, NewPolGrid);
}

function afterQuery(arrQueryResult)
{
   fm.all("NewAppntNo").value = arrQueryResult[0][0];
   fm.all("NewAppntName").value = arrQueryResult[0][1];
}

function splitCont() //拆分保单的操作
{
  if (fm.NewAppntNo.value == "")
  {
    alert("没有输入新投保人客户号");
    return false;
  }
  if (trim(fm.NewAppntNo.value).length != 9)
  {
    alert("新投保人客户号输入的位数不正确");
    return false;
  }
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  fm.action = "./PEdorTypeCFSplit.jsp?Operator=SPLIT";
	fm.submit();
}

function backCont()  //删除保单的操作
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  fm.action = "./PEdorTypeCFSplit.jsp?Operator=DEL";
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSplit(flag, content)
{
  try
  {
    showInfo.close();
    window.focus();
  }
  catch(ex) {}
  
  if (flag == "Fail")
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  initForm();
}

function edorTypeCFSave()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PEdorTypeCFSubmit.jsp";

	fm.submit();
}

function afterSubmit(flag, content)
{
 try
  {
    showInfo.close();
    window.focus();
  }
  catch(ex) {}
  
  if (flag == "Fail")
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    returnParent();
  }
  initForm();
}

function returnParent()
{	
  try
	{
    top.opener.focus();
	  top.opener.getEdorItem();
	  top.close();
	}
	catch (ex) {}
}