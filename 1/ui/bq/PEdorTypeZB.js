//               该文件中包含客户端需要处理的函数和事件
  
var showInfo1;
var mDebug="1";

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPagePrem = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPageLonePrem = new turnPageClass();
var turnPageAcc = new turnPageClass();
var turnPageSumLoanPrem = new turnPageClass();
var turnPagePolState = new turnPageClass();
var feeName1="初始扣费比例1";
var feeName2="初始扣费比例2";

function returnParent()
{
	try
	{
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {}
}



function save()
{

	//税优验收临时修改
	var mySql = "select 1 from lcpol a,lmriskapp b where a.conttype='1' and a.contno='" + fm.ContNo.value + "'" 
				+" and a.riskcode=b.riskcode and b.taxoptimal='Y'";
    var result = easyExecSql(mySql);
    if(result != "null" && result != "" && result != null)
    {
    }else{
	  if(!checkAppMoney())
	  {
	    return false;
	  }
    }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value='deal';
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, transact )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		if(transact=="init"){
			queryLoanPremInfo(); 
			alert("有欠交保费，请先进行补交保费！");
			
		}
		else{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  		returnParent();
		}
	  
	}
}
        

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function checkAppMoney()
{
	var appMoney = fm.AppMoney.value;
	var tLoanPrem=fm.all('SumLoanPrem').value;
	if(tLoanPrem>0&&(tLoanPrem>appMoney))
	{
	  alert("补交保费不能不足以补交所欠保费");
	  return false;
	}else{
		var strSQL = "select d.codealias,d.othersign from lcpol p,ldcode d  where d.codetype='bq_zb' and  p.riskcode=d.code and p.contno='"+fm.all('ContNo').value+"' fetch first 1 rows only with ur";
		var arrResult = easyExecSql(strSQL);
		if(arrResult != null && arrResult !=''){
			var MinZBMoney = Number(arrResult[0][0]);
			var numtime = Number(arrResult[0][1]);
			if (appMoney < MinZBMoney)
			  {
			  	  alert("追加保费不能低于"+MinZBMoney+"元");
			  	  return false;
			  }
			 if ((appMoney % numtime)!= 0)
			  {
			  	alert("追加保费必须为"+numtime+"元的整数倍");
			    return false;
			  }
			 return true;	
		}else{
			if (appMoney < 1000)
			{
				alert("追加保费不能低于1000元");
				return false;
			} 
			else if ((appMoney % 100)!= 0)
			{
				alert("追加保费必须为100元的整数倍");
				return false;
				
			} 
		}
  }
  return true;
}

function initPremInfo()
{
	  try
  {   
	   var strSQL = "select payintv from lccont where  contno='"+fm.all('ContNo').value+"'  with ur";
	   var tPayIntv = easyExecSql(strSQL);
	   if(tPayIntv=="0"){
		   
	   }else{
		    var tSQL="select (select max(paydate) from ljapay where incomeno=p.contno),prem,(select a.codename from ldcode a where a.codetype='payintv' and a.code=char(p.payintv)),FIRSTPAYDATE,6000*12/payintv from lccont p "
		    		+"where p.contno='"+fm.all('ContNo').value+"'";   
		    turnPagePrem.strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
	    	turnPagePrem.arrDataCacheSet = decodeEasyQueryResult(turnPagePrem.strQueryResult);
	    	fm.all('LastPayDate').value =turnPagePrem.arrDataCacheSet[0][0];
	    	fm.all('Prem').value = turnPagePrem.arrDataCacheSet[0][1];      
      		fm.all('PayFreq').value = turnPagePrem.arrDataCacheSet[0][2];
      		fm.all('PayStartDate').value = turnPagePrem.arrDataCacheSet[0][3];
      		var feeMoney=turnPagePrem.arrDataCacheSet[0][4];
      		feeName1=feeMoney+"元保费以下部分扣费比例";
            feeName2=feeMoney+"元保费以上部分扣费比例";
	   }
  }
  catch(re)
  {
    alert("初始化保费信息错误!");
  }

	    
}

function initAccInfo()
{
	  try
  {
	   var tSQL="select baladate,InsuAccBala from lcinsureacc "
	           +"where contno='"+fm.all('ContNo').value+"'";   
	    turnPageAcc.strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
	    turnPageAcc.arrDataCacheSet = decodeEasyQueryResult(turnPageAcc.strQueryResult);
	    fm.all('LastAccBalDate').value =turnPageAcc.arrDataCacheSet[0][0];
      fm.all('AccBala').value = turnPageAcc.arrDataCacheSet[0][1]; 
  }
  catch(re)
  {
    alert("初始化帐户信息错误!");
  }	  
   var strSQL = "select edorvalue from LPEdorEspecialData where  edorAcceptno='"+fm.all('EdorAcceptNo').value+"' and edorType='ZB' and detailType='ZF' with ur";
		var tmoney = easyExecSql(strSQL);
		
		if(tmoney!=null){
		
		fm.AppMoney.value=tmoney;
		}  
}

function initPayState()
{
	
	 try
  {
  	  var tPolStateSQL="select (select a.codename from ldcode a where a.codetype='paystate' and a.code=value(p.StandbyFlag1,'0') ),p.paytodate,p.payintv,p.polno,(select insuaccno from lmrisktoacc where riskcode=p.riskcode),value(p.StandbyFlag1,'0') from  lcpol p where  p.ContNo='"+fm.all('ContNo').value+"' and polno=mainpolno";     
      turnPagePolState.strQueryResult  = easyQueryVer3(tPolStateSQL, 1, 0, 1);
	    turnPagePolState.arrDataCacheSet = decodeEasyQueryResult(turnPagePolState.strQueryResult);
	    fm.all('PayState').value =turnPagePolState.arrDataCacheSet[0][0];
      fm.PayToDate.value=turnPagePolState.arrDataCacheSet[0][1];
      fm.PayIntv.value=turnPagePolState.arrDataCacheSet[0][2];
      //fm.all('AppMoneyFee').value =0; 
      fm.PolNo.value=turnPagePolState.arrDataCacheSet[0][3];
      fm.InsuAccNo.value=turnPagePolState.arrDataCacheSet[0][4];
      fm.PayFlag.value=turnPagePolState.arrDataCacheSet[0][5];
      //var feeMoney=turnPagePolState.arrDataCacheSet[0][5];
      //feeName1=feeMoney+"元保费以下部分扣费比例";
        //    feeName2=feeMoney+"元保费以上部分扣费比例";
  }
  catch(re)
  {
    alert("初始化缴费状态信息错误!");
  }	 
  try{
  	isLoanState();
  	}
  	catch(re){
  		alert("查询欠费信息错误!");
  	}
}

function isLoanState()
{
		var tPayFlag=fm.all('PayFlag').value;
	  var tPayToDate=fm.PayToDate.value;
	  var tCurrDate=getCurrentDate();
    var tDayDiff=dateDiff(tPayToDate,tCurrDate,"D")
    //说明到目前保单已经欠费,且保单处于自动缓交 或者 缓交状态	
    //alert(tDayDiff+""+tPayFlag);
    if(tDayDiff>0 && (tPayFlag=='2' || tPayFlag=='1'))
    { 
    	
    	  fm.ZBFlag.value='BF';
    	  //var showStr="正在初始化数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        //showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        fm.fmtransact.value='init';
        //alert("before init ljspayperson");
        fm.submit();
        
        //趸交不需要考虑
        //if(fm.PayIntv.value !='0' || fm.PayIntv.value!=0)
        //{
        	  //queryLoanPremInfo(); 
        //	}
        
	     	     	    	   	    	
    }else
    	{
    		
    		fm.ZBFlag.value='ZF';   		    		
    	}
		
	
	}

/**查询欠费信息*/
function queryLoanPremInfo()
{
	 
	  var tEdorNo=fm.all('EdorNo').value;
	  var tEdorValiDate=fm.all('EdorValiDate').value;	
	  var tContNo=fm.all('ContNo').value;	
    var strSQL ="select '',sum(SumDuePayMoney),'','',paycount from ljspayperson where contno='"+tContNo+"' group by paycount";
    turnPageLonePrem.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //alert(" 11111 +"+turnPageLonePrem.strQueryResult);
    
    if (!turnPageLonePrem.strQueryResult) 
    {
        return false;
    }
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
    turnPageLonePrem.arrDataCacheSet = decodeEasyQueryResult(turnPageLonePrem.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPageLonePrem.pageDisplayGrid = LoanPremGrid;    
    //保存SQL语句
    turnPageLonePrem.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPageLonePrem.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPageLonePrem.getData(turnPageLonePrem.arrDataCacheSet, turnPageLonePrem.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPageLonePrem.pageDisplayGrid);  
    
    //分别对相应字段赋值
    var tStartDate=fm.all('PayStartDate').value;
    var tPayIntv=0;
    var tSumLoanPrem=Number(0);
    tPayIntv=fm.PayIntv.value;
    
    if(LoanPremGrid.mulLineCount>0){
    	    	divAppMoneyFee.style.display  = "none";
    	    	divPayMoneyFee.style.display  = "";
    }
    for(var t=0;t<LoanPremGrid.mulLineCount;t++)
    {	//alert(LoanPremGrid.getRowColData(t,2));
    	tSumLoanPrem+=Number(LoanPremGrid.getRowColData(t,2));
    	//var tempDate=LoanPremGrid.getRowColData(t,5);
    	//var tInv=0;
    	//alert(tempDate+" 1"+tStartDate);
    	//tInv=dateDiff(tStartDate,tempDate,"M");
    	//alert(tInv+" 1 "+tPayIntv);
    	//var tPayTimes=Math.ceil(tInv/tPayIntv);
    	var tPayTimes=LoanPremGrid.getRowColData(t,5);
    	//alert(tPayTimes);
    	LoanPremGrid.setRowColData(t,1,tPayTimes+"");   	
    	strSQL="select rate from rate330801 where payintv="+tPayIntv+" and "+tPayTimes+" between mincount and maxcount order by type";
    	var rate=easyExecSql(strSQL, 1, 0, 1);
    	//alert(rate[0][0]+""+rate[1][0]);
    	LoanPremGrid.setRowColData(t,3,rate[0][0]);
    	LoanPremGrid.setRowColData(t,4,rate[1][0]);
    	//通过查询描叙表去查询每一期的交费比例,暂时没开发
    	//LoanPremGrid.setRowColData(t,2,100);
    }
    //alert(tSumLoanPrem);
  fm.all('AppMoney').value =tSumLoanPrem; 	 
   fm.all('PayMoneyFee').value =tSumLoanPrem; 	 
   	  				
}	
function InsuAccTraceQuery(){
	//alert("待后续开发");
	//return ;
	var tPolNo=fm.PolNo.value;
	var tInsuAccNo=fm.InsuAccNo.value;
	
	window.open("../bq/OmnipotenceAcc.jsp?PolNo="+tPolNo+"&InsuAccNo="+tInsuAccNo);
}