<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ScanListPrintBQMain.jsp
//程序功能：
//创建日期：2006-11-20 15:26
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("sql", request.getParameter("sql"));
  System.out.println("\n\n\n\n" + request.getParameter("sql"));
  
  VData data = new VData();
  data.add(tG);
  data.add(tTransferData);
         
  ScanListPrintBQUI tScanListPrintBQUI = new ScanListPrintBQUI();
  XmlExport txmlExport = tScanListPrintBQUI.getXmlExport(data, "");    
  if(txmlExport == null)
  {
    operFlag = false;
    Content = tScanListPrintBQUI.mErrors.getFirstError().toString();                 
  }
	
	if (operFlag==true)
	{
	  ExeSQL tExeSQL = new ExeSQL();
    //获取临时文件名
    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
    String strFilePath = tExeSQL.getOneValue(strSql);
    String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
    //获取存放临时文件的路径
    //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
    //String strRealPath = tExeSQL.getOneValue(strSql);
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strVFPathName = strRealPath + "//" +strVFFileName;
    
    CombineVts tcombineVts = null;	
    
    String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
  	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
  
  	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
  	tcombineVts.output(dataStream);
      	
  	//把dataStream存储到磁盘文件
  	//System.out.println("存储文件到"+strVFPathName);
  	AccessVtsFile.saveToFile(dataStream,strVFPathName);
    System.out.println("==> Write VTS file to disk ");
          
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
	}
	else
	{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>