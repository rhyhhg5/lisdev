<%
//程序名称：PolContractQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.pubfun.*"%>
<%

String CurrentDate = PubFun.getCurrentDate();
CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
System.out.println("CurrentDate="+CurrentDate);
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）


// 下拉框的初始化


function initForm()
{
  try
  {
  	initGrpContGrid();
		initGetModeGrid();
  }
  catch(re)
  {
    alert("LLAccModifyInput.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 产品查询信息列表的初始化
function initGetModeGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="团单号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();                                                 
      iArray[2][0]="投保单位名称";         		//列名                     
      iArray[2][1]="70px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[3]=new Array();                                                 
      iArray[3][0]="生效日期";         		//列名                     
      iArray[3][1]="60px";            		//列宽                             
      iArray[3][2]=100;            			//列最大值                           
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      iArray[4]=new Array();
      iArray[4][0]="终止日期";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              

      GetModeGrid = new MulLineEnter( "fm" , "GetModeGrid" ); 
      //这些属性必须在loadMulLine前
      //GetModeGrid.mulLineCount = 5;   
      GetModeGrid.displayTitle = 1;
      GetModeGrid.locked = 1;
      GetModeGrid.canSel = 1;
      GetModeGrid.hiddenPlus = 1;
      GetModeGrid.hiddenSubtraction = 1;
      GetModeGrid.loadMulLine(iArray);
      GetModeGrid.selBoxEventFuncName ="queryAll";
  
      
      //这些操作必须在loadMulLine后面
      //ContGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


// 产品查询信息列表的初始化
function initGrpContGrid()
  {                               
    var iArray = new Array();
    
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="团单号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();                                                 
      iArray[2][0]="个单号";         		//列名                     
      iArray[2][1]="70px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[3]=new Array();                                                 
      iArray[3][0]="被保人姓名";         		//列名                     
      iArray[3][1]="60px";            		//列宽                             
      iArray[3][2]=100;            			//列最大值                           
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      iArray[4]=new Array();
      iArray[4][0]="生效日期";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
			iArray[5]=new Array();
      iArray[5][0]="终止日期";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" ); 
      //这些属性必须在loadMulLine前
      //GrpContGrid.displayTitle = 1;
      GrpContGrid.locked = 1;
      GrpContGrid.canSel = 0;
      GrpContGrid.hiddenPlus = 1;
      GrpContGrid.hiddenSubtraction = 1;
      GrpContGrid.loadMulLine(iArray);

      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>