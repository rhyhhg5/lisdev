<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LDPersonSave.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  
  LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
  LPEdorItemSet mLPEdorItemSet =new LPEdorItemSet();
  
  LCInsuredSet tLCInsuredSet=new LCInsuredSet();
  LCPolSet tLCPolSet=new LCPolSet();
   
  TransferData tTransferData = new TransferData(); 
  PEdorItemUI tPEdorItemUI   = new PEdorItemUI();
  //输出参数
  String FlagStr = "";
  String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  System.out.println("tGI"+tGI);
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    CErrors tError = null;
    String tBmCert = "";    
    //后面要执行的动作：添加，修改，删除
    String fmAction=request.getParameter("fmtransact");   
    String displayType=request.getParameter("DisplayType");       
     try
    {
        if(fmAction.equals("INSERT||EDORITEM"))
        {
            tLPEdorMainSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo")); 
            tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
            tLPEdorMainSchema.setContNo(request.getParameter("ContNo"));
            
            
            if (displayType.equals("1"))
            {
                LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
                tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));  
                tLPEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
                tLPEdorItemSchema.setEdorAppNo(request.getParameter("EdorNo")); 
                tLPEdorItemSchema.setDisplayType(displayType);  
                tLPEdorItemSchema.setEdorType(request.getParameter("EdorType")); 
                tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
                tLPEdorItemSchema.setInsuredNo("000000");
                tLPEdorItemSchema.setPolNo("000000"); 
                
 //               tLPEdorItemSchema.setManageCom(request.getParameter("ManageCom"));
                tLPEdorItemSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
                tLPEdorItemSchema.setEdorAppDate(request.getParameter("EdorAppDate"));
                mLPEdorItemSet.add(tLPEdorItemSchema);
             }
             else if (displayType.equals("2"))
             {
                String tInsuredNo[] = request.getParameterValues("InsuredGrid1");            //被保险人号码 
               
                String tChk[] = request.getParameterValues("InpInsuredGridChk");
                for(int index=0;index<tChk.length;index++)
                {
                    if(tChk[index].equals("1"))
                    {
                        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                        tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
                        tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
                         tLPEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo")); 
                        tLPEdorItemSchema.setEdorAppNo(request.getParameter("EdorNo")); 
                         tLPEdorItemSchema.setDisplayType(displayType); 
                        tLPEdorItemSchema.setEdorType(request.getParameter("EdorType")); 
                        tLPEdorItemSchema.setContNo(request.getParameter("ContNo")); 
                        tLPEdorItemSchema.setInsuredNo(tInsuredNo[index]);
                        tLPEdorItemSchema.setPolNo("000000");
//                        tLPEdorItemSchema.setManageCom(request.getParameter("ManageCom"));
                        tLPEdorItemSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
                        tLPEdorItemSchema.setEdorAppDate(request.getParameter("EdorAppDate"));
                        mLPEdorItemSet.add(tLPEdorItemSchema);
                    }           
                }            
                
             }  
             else if (displayType.equals("3"))
             {
               
                 String tPolNo[] = request.getParameterValues("PolGrid1");                //险种保单号            
                String tInsuredNo[] = request.getParameterValues("PolGrid4");            //被保险人号码        
           
                String tChk[] = request.getParameterValues("InpPolGridChk"); 
                for(int index=0;index<tChk.length;index++)
                {
                    if(tChk[index].equals("1"))
                    {
                        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                        tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
                        tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
                         tLPEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
                        tLPEdorItemSchema.setEdorAppNo(request.getParameter("EdorNo")); 
                        tLPEdorItemSchema.setDisplayType(displayType);  
                        tLPEdorItemSchema.setEdorType(request.getParameter("EdorType")); 
                        tLPEdorItemSchema.setContNo(request.getParameter("ContNo")); 
                        tLPEdorItemSchema.setInsuredNo(tInsuredNo[index]);
                        tLPEdorItemSchema.setPolNo(tPolNo[index]);
//                        tLPEdorItemSchema.setManageCom(request.getParameter("ManageCom"));
                        tLPEdorItemSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
                        tLPEdorItemSchema.setEdorAppDate(request.getParameter("EdorAppDate"));
                        mLPEdorItemSet.add(tLPEdorItemSchema);
                    }           
                }
                  
             }
                
        }       
   
        // 准备传输数据 VData
         VData tVData = new VData();
         
         tVData.add(mLPEdorItemSet);
         System.out.println("mLPEdorItemSet"+mLPEdorItemSet.size());
         tVData.add(tLPEdorMainSchema);
         tVData.add(tTransferData);
         tVData.add(tGI);
         
          
         //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        if ( tPEdorItemUI.submitData(tVData,fmAction))
        {
            if (fmAction.equals("INSERT||EDORITEM"))
	        {
	    	    System.out.println("11111------return");
	            	
	    	    //tVData.clear();
	    	    //tVData = tPEdorItemUI.getResult();
	    	    //
	    	    //LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet(); 
	            //tLPEdorItemSet=(LPEdorItemSet)tVData.getObjectByObjectName("LPEdorItemSet",0);
	            for (int i=1;i<=mLPEdorItemSet.size();i++)
	            {
	                %>
	                <SCRIPT language="javascript">
	                    parent.fraInterface.EdorItemGrid.addOne("EdorItemGrid");     
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,1,"<%=mLPEdorItemSet.get(i).getEdorAcceptNo()%>");
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,2,"<%=mLPEdorItemSet.get(i).getEdorNo()%>");
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,3,"<%=mLPEdorItemSet.get(i).getEdorType()%>");                                                                                                                                
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,4,"<%=mLPEdorItemSet.get(i).getDisplayType()%>");
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,5,"<%=mLPEdorItemSet.get(i).getGrpContNo()%>");
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,6,"<%=mLPEdorItemSet.get(i).getContNo()%>");                                                                 
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,7,"<%=mLPEdorItemSet.get(i).getInsuredNo()%>");
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,8,"<%=mLPEdorItemSet.get(i).getPolNo()%>");
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,9,"<%=mLPEdorItemSet.get(i).getEdorValiDate()%>");
                        parent.fraInterface.EdorItemGrid.setRowColData(parent.fraInterface.EdorItemGrid.mulLineCount-1,10,"0");
                    </SCRIPT>
	            <%
	            }
	        }
	        else if (fmAction.equals("DELETE||PEdorItem"))
	        {
	             %>
	            <SCRIPT language="javascript">
	                parent.fraInterface.EdorItemGrid.delCheckTrueLine ();    
	            </SCRIPT>
	            <%
	        }
	    }
    }
    
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      System.out.println("aaaa"+ex.toString());
      FlagStr = "Fail";
    }
  

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorItemUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
   }
   Content = PubFun.changForHTML(Content);
}//页面有效区
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

