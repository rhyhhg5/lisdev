<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>";// 记录管理机构
    var comcode = "<%=tGI.ComCode%>";// 记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<!-- 通用录入校验要是用的 js 文件 -->
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!-- 使用双击下拉需要引入的 JS 文件 -->
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>电商保单续期维护</title>
<!-- 自己的输入验证 js -->
<script src="BqUpdateDScardflag.js"></script>
<!-- 初始化 mulline -->
<%@include file="BqUpdateDScardflagInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<!-- target 属性规定在何处打开 action URL,但是帮助文档没有这个属性啊啊啊? -->
	<form action="BqUpdateDScardflagSave.jsp" method="post" name="fm" target="fraSubmit">
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>电商保单续期维护</td>
			</tr>
		</table>

		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>保 单 号</td>
				<td class=input><input class=common name=ContNo /></td>
			</tr>
		</table>
		<INPUT VALUE="查询" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif" style="cursor: hand;" OnClick="showPage(this,divLCPol1);"></td>
				<td class=titleImg>查询结果</td>
			</tr>
		</table>
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1>
						<!--
							关于 MulLine:
								1.在显示 MulLine 的页面上(也就是该页面,一般是 XxxInput.jsp)必须有如下代码
									<span id="spanObjGrid " ></span>
									这个代码必须在一个表单中,一般情况下这个 form 的 name 为 fm(?不知道是不是强制的?)
									id 必须是:span+MulLine 对象名,这里对象名就是 ObjGrid
								2.还需要创建一个 XxxInit.jsp 来初始化 MulLine,需要引入到这个页面;
									首先这个页面最核心的方法是:function initPolGrid(){}(方法名也必须是:init+MulLine 对象名)
									然后在这个函数里面先定义一个数组 var iArray = new Array();(应该是一个二维数组,这个表示每一行,里面那个数组表示每一列)
									具体内容看文档或代码
									其实这个 jsp 页面一般还有一个 initInpBox() 和 initForm() 函数(?感觉应该像是显示不成功报错的?)
								3.关于查询出来的值如何显示在页面:
									有点懵逼,拆分好几次,最后应该用 displayMultiline() 方法显示的
								4.这里还有一个判断 MulLine 中哪个单选按钮被选中的功能,在 XxxInput.js 中,然后把结果显示到了下面的修改前市场类型的 Input 了
						-->
						<span id="spanPolGrid"></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<!--
					一个分页,感觉封装的挺牛逼的,一会看看
				-->
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
		<br />
		<!--
			1.点击修改是先响应 onclick,进入 XxxInput.js 中执行 modifyMarketType 函数
			2.校验完成后提交表单到 XxxSave.jsp
		-->
		<INPUT VALUE="修改" class="cssButton" TYPE=button onclick="updateDScardflag();">
		<input type=hidden id="myFlag" name="myFlag" value="0">
		<INPUT VALUE="恢复" class="cssButton" TYPE=button onclick="disUpdateDScardflag();">
		<input type=hidden id="fmtransact" name="fmtransact">
		<div>
			<p style="color: red">
				* 点击修改按钮将保单 carflag 维护为 0<br>
				* 点击恢复按钮将保单 carflag 维护为 b<br>
				* 请维护之前务必做好相关数据备份
			</p>
		</div>
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
