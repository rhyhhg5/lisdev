<%
//程序名称：GEdorTypeMJSubmit.jsp
//程序功能：
//创建日期：2006-01-09
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  String flag = "";
  String content = "";

  String edorNo = request.getParameter("EdorAcceptNo");
  String dealType = request.getParameter("DealType");
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("edorNo", edorNo);
  tTransferData.setNameAndValue("edorType", BQ.EDORTYPE_MJ);
  tTransferData.setNameAndValue("dealType", dealType);
  tTransferData.setNameAndValue("payMode", request.getParameter("PayMode"));
  tTransferData.setNameAndValue("payDate", request.getParameter("PayDate"));
  tTransferData.setNameAndValue("bank", request.getParameter("Bank"));
  tTransferData.setNameAndValue("bankAccno", request.getParameter("BankAccno"));
  
  
  VData tVData = new VData();
  tVData.add((GlobalInput)session.getValue("GI"));
  tVData.add(tTransferData);

  GEdorMJDealUI tGEdorMJDealUI = new GEdorMJDealUI();
  if(!tGEdorMJDealUI.submitData(tVData, ""))
  {
    flag = "Fail";
    
    if(tGEdorMJDealUI.needConfirm())
    {
      content = "需要进行审批：" + tGEdorMJDealUI.mErrors.getFirstError();
    }
    else
    {
      content = "满期处理失败";
      System.out.println(tGEdorMJDealUI.mErrors.getErrContent());
    }
  }
  else
  {
    flag = "Succ";
    content = "满期处理成功";
  }
  
  content = PubFun.changForHTML(content);
%>
<html>
<script language="javascript">
	parent.fraInterface.AfterEndTimeDeal("<%=flag%>", "<%=content%>");
</script>
</html>