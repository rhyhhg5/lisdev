<%
//程序名称：PEdorAutoUWSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
//接收信息，并作校验处理。
//输入参数
//个人批改信息
LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
EdorAutoUWUI tEdorAutoUWUI   = new EdorAutoUWUI();
GlobalInput tG = new GlobalInput();
tG = (GlobalInput)session.getValue("GI");

//输出参数
CErrors tError = null;
//后面要执行的动作：添加，修改，删除

boolean tResult  = true;                
String FlagStr = "";
String Content = "";
String transact = "";
String tEdorNo[] = request.getParameterValues("PEdorMainGrid1");
String tContNo[] = request.getParameterValues("PEdorMainGrid2");
String tChk[] = request.getParameterValues("InpPEdorMainGridChk");
TransferData tResultData = null;

int n = tEdorNo.length;
for (int i = 0; i < n; i++)
{
	if (tContNo[i].equals("") || !tChk[i].equals("1"))
	{
		continue;
	}
	//个人批改信息
	tLPEdorMainSchema.setContNo(tContNo[i]);
	tLPEdorMainSchema.setEdorNo(tEdorNo[i]);
	try
	{
		// 准备传输数据 VData
		VData tVData = new VData();   
		//保存个人保单信息(保全)	
		tVData.addElement(tLPEdorMainSchema);
		tVData.addElement(tG);
		tResult = tEdorAutoUWUI.submitData(tVData,transact);
		if (!tResult)
		{
			tError = tEdorAutoUWUI.mErrors;
			Content = "申请批单"+ tEdorNo[i] + "核保失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
		else
		{
			tResultData = (TransferData) tEdorAutoUWUI.getResult().getObjectByObjectName("TransferData", 0);
			if (!((String) tResultData.getValueByName("UWFlag")).equals("9"))
			{
				Content = "申请批单"+ tEdorNo[i] + "需要人工核保！";
				FlagStr = "Fail";
			}
			if (!FlagStr.equals("Fail"))
			{
				Content = "核保成功！";
				FlagStr = "Succ";
			}
		}
	}
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	catch(Exception ex)
	{
		Content = "申请批单"+ tEdorNo[i] + "核保失败!\n";
		FlagStr = "Fail";
	}
}

//添加各种预处理
%>                      
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

