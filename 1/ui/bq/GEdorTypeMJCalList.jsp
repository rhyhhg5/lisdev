<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GEdorTypeMJCalList.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  XmlExport txmlExport = null;   
  
  String edorAcceptNo = request.getParameter("edorAcceptNo");
    
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("edorAcceptNo", edorAcceptNo);
  tTransferData.setNameAndValue("edorType", request.getParameter("edorType"));
  tTransferData.setNameAndValue("prtType", request.getParameter("prtType"));

  VData tVData = new VData();
  tVData.addElement(tG);
  tVData.addElement(tTransferData);
         
  PrtGrpEdorMJListUI tPrtGrpEdorMJListUI = new PrtGrpEdorMJListUI(); 
  if(!tPrtGrpEdorMJListUI.submitData(tVData,"PRINT"))
  {
     	operFlag = false;
     	Content = tPrtGrpEdorMJListUI.mErrors.getFirstError(); 
  }
  else
  {
    txmlExport = tPrtGrpEdorMJListUI.getDealXmlExport();
    
    if(txmlExport==null)
    {
    	operFlag=false;
    	Content="没有得到要显示的数据文件";	  
    }
  }

%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
		alert("打印成功");
		parent.fraInterface.printPDF();
	}
	else
	{
		alert("打印失败 "+"<%=Content%>");
	}
</script>
</html>
