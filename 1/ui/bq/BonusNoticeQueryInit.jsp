<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//程序名称：BonusShareQueryInit.jsp
	//程序功能：分红信息界面初始化
	//创建日期：2011-11-10
	//创建人  ：gaoyx
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
	function initBox() {
		try {
			fm.all('organcode').value = "";
			fm.all('queryType').value = "";
			fm.all('StartDate').value = tCurrentDate;
			fm.all('EndDate').value = tCurrentDate;
			fm.all('organname').value = "";
			fm.all('queryTypeName').value = "";
			fm.all('getTypeName').value = "";
			fm.all('getType').value = "";
			fm.all("divInfo").style.display = "none";
		} catch (ex) {
			alert("在BonusShareQueryInit.jsp   InitBox函数中发生异常:初始化界面错误!");
		}
	}

	//初始化表单
	function initForm() {
		try {
			initBox();
		} catch (re) {
			alert("在OmnipotenceAccInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
</script>
