<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LMInsuAccRate.jsp
//程序功能：万能险帐户结算利率录入界面
//创建日期：2007-12-12
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!—页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <SCRIPT src="LMInsuAccRate.js"></SCRIPT> 
  <%@include file="LMInsuAccRateInit.jsp"%> 
 
  
  <title>结算利率录入</title>
</head>
<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form action="LMInsuAccRateSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>险种</td>
        <td  class= input><input class=codeNo  name=RiskCode verify="险种编码|notnull" ondblclick="showCodeList('riskcodebyacc',[this,RiskName],[0,1],null,null,null,1);" onkeyup="showCodeListKey('riskcodebyacc',[this,RiskName],[0,1],null,null,null,1);" onpropertychange = "shwoInsuAccNo();"><input class=codename name=RiskName readonly=true  elementtype="nacessary"></td>
        <td  class= title>保险帐户</td>
        <td  class= input><input class=codeNo  name=InsuAccNo verify="保险帐户号码|notnull" ondblclick="return showCodeList('insuaccno',[this,InsuAccName],[0,1],null,RiskCode.value,RiskCode.value,1);" onkeyup="return showCodeListKey('insuaccno',[this,InsuAccName],[0,1],null,RiskCode.value,RiskCode.value,1);"><input class=codename name=InsuAccName readonly=true  elementtype="nacessary"></td>
        <td  class= title>结算月份</td>
        <td  class= input><input class=common name=BalaMonth verify="结算月份|notnull"  style="width:60"  elementtype="nacessary">(格式:yyyy-mm)</td>
      </tr>
      <tr  class= common>
        <td  class= title>利率类型</td>
        <td  class= input><Input class=codeNo name="RateType"  verify="利率类型|notnull" readonly= true CodeData="0|^S|单利^C|复利" ondblclick="return showCodeListEx('ratetype',[this,RateTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('ratetype',[this,RateTypeName],[0,1]);" ><input class=codename name=RateTypeName readonly=true  elementtype="nacessary"></td>   
        <td  class= title>利率间隔</td>
        <td  class= input><input class=common name=RateIntv verify="利率间隔|int&notnull" elementtype="nacessary"  style="width:50">(整数)</td>
        <td  class= title>利率间隔单位</td>
        <td  class= input><Input class=codeNo name="RateIntvUnit"  verify="利率间隔单位|notnull" readonly= true CodeData="0|^Y|年利率^M|月利率^D|日利率" ondblclick="return showCodeListEx('rateintvunit',[this,RateIntvUnitName],[0,1]);" onkeyup="return showCodeListKeyEx('rateintvunit',[this,RateIntvUnitName],[0,1]);" ><input class=codename name=RateIntvUnitName readonly=true  elementtype="nacessary"></td>   
      </tr>
      <tr  class= common>
        <td  class= title>利率</td>
        <td  class= input><input class=common verify="利率|notnull" name=Rate  elementtype="nacessary"></td>
        <td  class= title>确认利率</td>
        <td  class= input><input class=common verify="确认利率|notnull" name=RateConfirm  elementtype="nacessary"></td>
      </tr>
      <tr class= common>
      <td class=button><input class=cssButton  VALUE="重  置"  TYPE=button onclick="initInpBox();"></td>
      <!-- <td class=button><input class=cssButton  VALUE="测试用"  TYPE=button onclick="checkBalaMonth();"></td> -->
      
      </tr>
    </table>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLMInsuAccRateInfo);"></td>
	  <td class=titleImg>查询结果</td>
	</tr>
  </table>
  <!-- 信息（列表） -->
  <div id="divLMInsuAccRateInfo" style="display:''">
	<table class=common>
      <tr class=common>
	    <td text-align:left colSpan=1><span id="spanLMInsuAccRateGrid"></span></td>
	  </tr>
	</table>
  </div>

  <div id="divPage2" align=center style="display: 'none' ">
	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </div>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden name=Sql>
  <input type=hidden id="DRiskCode" name="DRiskCode">
  <input type=hidden id="DInsuAccNo" name="DInsuAccNo">
  <input type=hidden id="DBalaMonth" name="DBalaMonth">
  <input type=hidden id="DRateType" name="DRateType">
  <input type=hidden id="DRateIntv" name="DRateIntv">
  <input type=hidden id="DRateIntvUnit" name="DRateIntvUnit">
  <input type=hidden id="DRate" name="DRate">
</form>
<!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
