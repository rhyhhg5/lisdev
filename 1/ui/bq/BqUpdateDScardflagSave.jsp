<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%
	//输入参数
	BqUpdateDScardflagUI updateDScardflagUI = new BqUpdateDScardflagUI();

	/*
	 * request.getParameterValues(String name)
	 * 获得如checkbox类(名字相同，但值有多个)的数据,接收数组变量,如:checkobx 类型    
	 * request.getParameter(String name)
	 * 获得相应名的数据,如果有重复,则返回第一个的值,接收一般变量,如:text 类型 
	 */
	String[] tGridNo = request.getParameterValues("PolGridNo"); // 得到 MulLine 中序号列的所有值
	String[] tGrpContNos = request.getParameterValues("PolGrid1"); // 得到第1列的所有值,也就是合同号
	String myFlag = request.getParameter("myFlag");
	String tRadio[] = request.getParameterValues("InpPolGridSel");


	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	String strManageCom = tG.ComCode;
	String strInput = "";

	TransferData transferData = new TransferData();

	// 从 mulline 中取得的是一列的值,所以遍历取出一行的值,然后把参数放入 TransferData 容器?
	for (int i = 0; i < tGridNo.length; i++) {
		if("1".equals(tRadio[i])){
			transferData.setNameAndValue("ContNo", tGrpContNos[i]);
		}
	}
	transferData.setNameAndValue("myFlag", myFlag);

	if (!"Fail".equals(FlagStr)) {// 绝对能进去,自从初始化 FlagStr 后就没有改过值
		// 准备向后台传输数据 VData
		VData tVData = new VData();// 继承了 Vector(List 实现类之一,支持线程的同步)
		FlagStr = "";
		tVData.add(tG);// 这里面没有重写 add 方法,还是 Vector 的,还是带线程的
		tVData.add(transferData);
		try {
			updateDScardflagUI.submitData(tVData, "");// 提交数据和要进行的操作
		} catch (Exception ex) {
			Content = "保修改失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {// 修改成功
			tError = updateDScardflagUI.mErrors;
			if (!tError.needDealError()) {
				Content = "修改成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>