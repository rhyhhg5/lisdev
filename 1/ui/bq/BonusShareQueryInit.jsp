<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>	

<%
//程序名称：BonusShareQueryInit.jsp
//程序功能：分红信息界面初始化
//创建日期：2011-11-10
//创建人  ：gaoyx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

function initBox()
{
  try
  {
    bonuseShare();
    
  }
  catch(ex)
  {
    alert("在BonusShareQueryInit.jsp-->InitBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initBox();
    initBonusGrid();
    initAccGrid();
    queryBonus();
  }
  catch(re) 
  {
    alert("在BonusShareQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 分红记录的初始化
function initBonusGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="SerialNo";

      iArray[2]=new Array();
      iArray[2][0]="红利公布年度";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="TaskNo"; 
      
      iArray[3]=new Array();
      iArray[3][0]="红利分配方式";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="ContNo";

      iArray[4]=new Array();
      iArray[4][0]="红利应分配时间";         		//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="分配金额";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  BonusGrid = new MulLineEnter( "fm" , "BonusGrid" ); 
      //这些属性必须在loadMulLine前
      BonusGrid.mulLineCount =0;   
      BonusGrid.displayTitle = 1;
      BonusGrid.hiddenPlus = 1;
      BonusGrid.hiddenSubtraction = 1;
      BonusGrid.canChk = 0;
      BonusGrid.canSel = 0;
      BonusGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        alert("BonusPrintInit.jsp-->initBonusGrid函数中发生异常:初始化界面错误!");
      }
}
// 分红记录的初始化
function initAccGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="SerialNo";

      iArray[2]=new Array();
      iArray[2][0]="发生时间";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="TaskNo"; 
      
      iArray[3]=new Array();
      iArray[3][0]="业务类型";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="ContNo";

      iArray[4]=new Array();
      iArray[4][0]="业务号";         		//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="变动金额";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  AccGrid = new MulLineEnter( "fm" , "AccGrid" ); 
      //这些属性必须在loadMulLine前
      AccGrid.mulLineCount =0;   
      AccGrid.displayTitle = 1;
      AccGrid.hiddenPlus = 1;
      AccGrid.hiddenSubtraction = 1;
      AccGrid.canChk = 0;
      AccGrid.canSel = 0;
      AccGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        alert("BonusPrintInit.jsp-->initAccGrid函数中发生异常:初始化界面错误!");
      }
}

</script>
