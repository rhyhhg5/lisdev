<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<title>保单服务理算结果 </title>
<%
	String mEdorNo = request.getParameter("EdorNo");
	
	//工单入口
	String mLoadFlag = request.getParameter("LoadFlag");
	String mDetailWorkNo = request.getParameter("DetailWorkNo");
	if (mLoadFlag != null)
	{
		String tFlag = mLoadFlag.substring(0, 4);
		if ((tFlag != null) && (tFlag.equals("TASK")))
		{
    		mEdorNo = mDetailWorkNo;
    	}
    }
%>
<script language="javascript">
	var intPageWidth = screen.availWidth;
	var intPageHeight = screen.availHeight;
	window.resizeTo(intPageWidth, intPageHeight);
	window.moveTo(-1, -1);
	window.focus();
</script>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" frameborder="no" border="1" framespacing="0" cols="*">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="yes" noresize src="about:blank" >
	<frame name="fraTitle"  scrolling="no" noresize src="about:blank" >
	<frameset name="fraSet" rows="0,380,*" frameborder="no" border="1" framespacing="0" rows="*">
		<!--菜单区域-->
		<frame name="fraMenu" scrolling="yes" noresize src="about:blank">
		<!--交互区域-->
		<frame id="fraDetail" name="fraDetail" scrolling="auto" src="./ShowGrpEdorInfo.jsp?EdorNo=<%=mEdorNo%>">
    	<!--下一步页面区域-->
    	<frame id="fraInterface" name="fraInterface" noresize scrolling="auto" src="./GEdorAppConfirm.jsp?loadFlag=<%=mLoadFlag%>&EdorNo=<%=mEdorNo%>">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>