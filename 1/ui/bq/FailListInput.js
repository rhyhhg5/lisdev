var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  if(!beforeSubmit())
  	return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(ex)
  {
  	alert("在FailListInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  //if()
 // return false;
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//结果中1代表个单，2代表团单
function easyQueryClick()
{
  queryCondition =queConCHK();
  
	var sql = " select a.managecom,(select name from ldcom where ComCode = a.managecom),a.otherno,c.EdorType,a.ShouldDate,"
					+" (select codename from ldcode1 where codetype='bankerror' and code1 =d.BankSuccFlag and code = a.bankcode),"
					+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='DEALSTATE'),"
					+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='OPERATOR'),"
					+" (select ConfDate from lpedorapp where EdorAcceptNo =a.ActuGetNo),(select Name from LAAgent where AgentCode = e.agentcode),a.ActuGetNo,1"
					+" from LJAGet a,lpedoritem c ,LYReturnFromBankB d ,lccont e "
	        +" where a.PayMode='4' and a.OtherNoType = '10' and a.EnterAccDate is null and SendBankCount > 0 and CanSendBank != '1' "
	        +" and a.ActuGetNo not in(select PayCode from LYSendToBank) "
	        +" and a.otherno = c.edorno and d.paycode = a.ActuGetNo and e.contno =c.contno "
	        +queryCondition
	        +" union"
	        +" select a.managecom,(select name from ldcom where ComCode = a.managecom),a.otherno,c.EdorType,a.ShouldDate,"
					+" (select codename from ldcode1 where codetype='bankerror' and code1 =d.BankSuccFlag and code = a.bankcode),"
					+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='DEALSTATE'),"
					+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='OPERATOR'),"
					+" (select ConfDate from lpedorapp where EdorAcceptNo =a.ActuGetNo),(select Name from LAAgent where AgentCode = e.agentcode),a.ActuGetNo,1"
					+" from LJAGet a,lpedoritem c ,LYReturnFromBankB d ,lbcont e "
	        +" where a.PayMode='4' and a.OtherNoType = '10' and a.EnterAccDate is null and SendBankCount > 0 and CanSendBank != '1' "
	        +" and a.ActuGetNo not in(select PayCode from LYSendToBank)  "
	        +" and a.otherno = c.edorno and d.paycode = a.ActuGetNo and e.contno =c.contno and c.EdorNo = e.EdorNo "
	        +queryCondition
	        //+" union"
	        //+" select a.ManageCom,(select name from ldcom where ComCode = a.managecom),a.otherno,c.EdorType,a.ShouldDate,"
	        //+" (select codealias from ldcode where codetype='bankerror' and code =d.BankSuccFlag and code = a.bankcode),"
					//+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='DEALSTATE'),"
					//+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='OPERATOR'),"
  				//+" (select ConfDate from lpedorapp where EdorAcceptNo =a.ActuGetNo),(select Name from LAAgent where AgentCode = e.agentcode),a.ActuGetNo,2"
	        //+" from Ljaget a,lpgrpedoritem c ,LYReturnFromBankB d ,lcgrpcont e"
	        //+" where a.PayMode='4' and a.OtherNoType = '3' and a.ConfDate is null and a.BankOnTheWayFlag is not null "
	        //+" and a.otherno = c.edorno and d.paycode = a.ActuGetNo and e.grpcontno =c.grpcontno"
	        //+queryCondition
	        //+" union"
	        //+" select a.ManageCom,(select name from ldcom where ComCode = a.managecom),a.otherno,c.EdorType,a.ShouldDate,"
	        //+" (select codealias from ldcode where codetype='bankerror' and code =d.BankSuccFlag and code = a.bankcode),"
	        //+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='DEALSTATE'),"
					//+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='OPERATOR'),"
	        //+" (select ConfDate from lpedorapp where EdorAcceptNo =a.ActuGetNo),(select Name from LAAgent where AgentCode = e.agentcode),a.ActuGetNo,2"
	        //+" from Ljaget a,lpgrpedoritem c ,LYReturnFromBankB d ,lbgrpcont e"
	        //+" where a.PayMode='4' and a.OtherNoType = '3' and a.ConfDate is null and a.BankOnTheWayFlag is not null "
	        //+" and a.otherno = c.edorno and d.paycode = a.ActuGetNo and e.grpcontno =c.grpcontno"
	        //+queryCondition
	        ;
	turnPage.strQueryResult  = easyQueryVer3(sql, 1, 1, 1);
	if (!turnPage.strQueryResult) 
	  {
	    alert("没有数据！");
	  }
    else
	  {
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = failListGrid;    
	          
	//保存SQL语句
	turnPage.strQuerySql     = sql; 
	  
	//设置查询起始位置
	turnPage.pageIndex       = 0;  
	  
    //在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { fm.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { fm.divPage.style.display = "none"; } catch(ex) { }
  }
	}
}

//处理选中的数据
function dealData()
{
	var sel = failListGrid.getSelNo();
	if(sel==0)
	{
	    alert("请先选择要处理的记录！");
	    return false;
	}
	var edorAcceptNo = failListGrid.getRowColData(sel-1,3);
	var gpflag= failListGrid.getRowColData(sel-1,12);
	var actuGetNo= failListGrid.getRowColData(sel-1,11);
	showInfo = window.open("DealDataMain.jsp?edorAcceptNo="+edorAcceptNo+"&gpflag="+gpflag+"&actuGetNo="+actuGetNo ,"","toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0");

}
//打印清单
function printAllList()
{
		if (failListGrid.mulLineCount == 0)
		{
		alert("您好，目前没有需要打印的清单");
		return false;
		}
  queryCondition = queConCHK();
  		
		fm.all("sql").value= " select a.otherno,c.EdorType,a.sumgetmoney,a.AccName, "
					+" ( select phone from LCAddress where CustomerNo = a.appntno and addressno =(select max(int(AddressNo)) from lcaddress where CustomerNo = a.appntno )),a.ShouldDate, "
					+" (select codename from ldcode1 where  codetype='bankerror' and code1 =d.BankSuccFlag and code = a.bankcode), "
					+" (select Name from LAAgent where AgentCode = e.agentcode), "
					+" (select managecom from LAAgent where AgentCode = e.agentcode), "
					+" (select phone from LAAgent where AgentCode = e.agentcode),  "
					+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='DEALSTATE')"
					+" from LJAGet a,lpedoritem c ,LYReturnFromBankB d ,lccont e "
	        +" where a.PayMode='4' and a.OtherNoType = '10' and a.EnterAccDate is null and SendBankCount > 0 and CanSendBank != '1' "
	        +" and a.ActuGetNo not in(select PayCode from LYSendToBank)  "
	        +"  and a.otherno = c.edorno and d.paycode = a.ActuGetNo and e.contno =c.contno "
	        +queryCondition
	        +" union select a.otherno,c.EdorType,a.sumgetmoney,a.AccName, "
					+" ( select phone from LCAddress where CustomerNo = a.appntno and addressno =(select max(int(AddressNo)) from lcaddress where CustomerNo = a.appntno)),a.ShouldDate, "
					+" (select codename from ldcode1 where  codetype='bankerror' and code1 =d.BankSuccFlag and code = a.bankcode), "
					+" (select Name from LAAgent where AgentCode = e.agentcode), "
					+" (select managecom from LAAgent where AgentCode = e.agentcode), "
					+" (select phone from LAAgent where AgentCode = e.agentcode),"
					+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='DEALSTATE')"
					+" from LJAGet a,lpedoritem c ,LYReturnFromBankB d ,lbcont e "
	        +" where a.PayMode='4' and a.OtherNoType = '10' and a.EnterAccDate is null  and SendBankCount > 0 and CanSendBank != '1' "
	        +" and a.ActuGetNo not in(select PayCode from LYSendToBank)  "
	        +"  and a.otherno = c.edorno and d.paycode = a.ActuGetNo and e.contno =c.contno and c.EdorNo = e.EdorNo "
	        +queryCondition
	        //+" union select a.otherno,c.EdorType,a.sumgetmoney,a.AccName, "
					//+" ( select phone from LCAddress where CustomerNo = a.appntno and addressno =(select max(addressno) from lcaddress where CustomerNo = a.appntno)),a.ShouldDate, "
					//+" (select codename from ldcode1 where  codetype='bankerror' and code1 =d.BankSuccFlag and code = a.bankcode), "
					//+" (select Name from LAAgent where AgentCode = e.agentcode), "
					//+" (select managecom from LAAgent where AgentCode = e.agentcode), "
					//+" (select phone from LAAgent where AgentCode = e.agentcode), "
					//+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='DEALSTATE')"
					//+" from LJAGet a,lpgrpedoritem c ,LYReturnFromBankB d ,lcgrpcont e "
	        //+" where a.PayMode='4' and a.OtherNoType = '10' and a.ConfDate is null  "
	        //+" and a.BankOnTheWayFlag is not null  and a.otherno = c.edorno and d.paycode = a.ActuGetNo and e.grpcontno =c.grpcontno "
	        //+queryCondition
	       	//+" union select a.otherno,c.EdorType,a.sumgetmoney,a.AccName, "
					//+" ( select phone from LCAddress where CustomerNo = a.appntno and addressno =(select max(addressno) from lcaddress where CustomerNo = a.appntno)),a.ShouldDate, "
					//+" (select codename from ldcode1 where  codetype='bankerror' and code1 =d.BankSuccFlag and code = a.bankcode), "
					//+" (select Name from LAAgent where AgentCode = e.agentcode), "
					//+" (select managecom from LAAgent where AgentCode = e.agentcode), "
					//+" (select phone from LAAgent where AgentCode = e.agentcode),"
					//+" (select edorvalue from lpedorespecialdata where edoracceptno = a.otherno and edorno = a.otherno and edortype ='QQ' and detailtype ='DEALSTATE')"
					//+" from LJAGet a,lpgrpedoritem c ,LYReturnFromBankB d ,lbgrpcont e "
	        //+" where a.PayMode='4' and a.OtherNoType = '10' and a.ConfDate is null  "
	        //+" and a.BankOnTheWayFlag is not null  and a.otherno = c.edorno and d.paycode = a.ActuGetNo and e.grpcontno =c.grpcontno "
	        //+queryCondition
	        ;
	  
		fm.action = "FailListPrint.jsp";
		fm.target = "_blank";
		fm.submit();
}
//页面查询条件
function queConCHK()
{
	queryCondition ="";
  if(fm.manageCom.value !="")
  		queryCondition += " and  a.ManageCom like '"+fm.manageCom.value +"%%'"; 
  if(fm.startDate.value !="")
  		queryCondition += " and  a.ShouldDate >= '"+fm.startDate.value +"'";
  if(fm.endDate.value !="")
  		queryCondition += " and  a.ShouldDate <= '"+fm.endDate.value +"'"; 
  if(fm.dealState.value !="")
  		queryCondition += " and  c.EdorState = '"+fm.dealState.value +"'"; 
  if(fm.edorAcceptNo.value !="")
  		queryCondition += " and  c.EdorNo = '"+fm.edorAcceptNo.value +"'";     
  		
  return queryCondition + "   and a.ManageCom like '" + mManageCom + "%' ";
}