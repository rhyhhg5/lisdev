<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GEdorConfirmOption.jsp
//程序功能：保全确认选项页面
//创建日期：2005-07-21
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./GEdorConfirmOption.js"></SCRIPT>
  <%@include file="./GEdorConfirmOptionInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<title>方式选择</title>
</head>
<body onload="initForm();">
<br>
<br>
  <form action="./GEdorConfirmOptionSave.jsp" method=post name=fm target="fraSubmit">
  <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
  <input type=hidden id="GetNoticeNo" name="GetNoticeNo">
  <input type=hidden id="fmtransact" name="fmtransact">
  <div id="FeeFlag" style="display:'none'"><p align="center">需要财务交费</p></div>
	<table class=common>
   <tr class=common id="trPayMode">
    <TD  class= title>交费/付费方式</TD>
		<TD  class= input>
		  <select name="PayMode" style="width: 128; height: 23" onchange="afterCodeSelect();" >
		    <option value = "0"></option>
  			<option value = "1">现金</option>
  			<option value = "2">现金支票</option>
  			<option value = "3">转帐支票</option>
  			<option value = "4">银行转帐</option>
  			<option value = "9">其它</option>
		  </select>
	  </tr>
	  <tr class=common id="trEndDate" style="display: 'none'">
			<TD  class= title>截止日期</TD>
			<TD  class= input>
				<Input class="coolDatePicker" name="EndDate" DateFormat="short" elementtype="nacessary">
			</TD>
	  </tr>
	  <tr class=common id="trPayDate" style="display: 'none'">
			<TD  class= title>转帐日期</TD>
			<TD  class= input>
				<Input class="coolDatePicker" name="PayDate" DateFormat="short"">
			</TD>
	  </tr>
	  <tr class=common id="trBank" style="display: 'none'">
			<TD  class= title>转帐银行</TD>
			<TD  class= input>
      	<input type="hidden" class = "readonly" readonly name="Bank">
      	<input class = "readonly" readonly name="BankName">
			</TD>
	  </tr>
	  <tr class=common id="trAccount" style="display: 'none'">
			<TD  class= title>转帐帐号</TD>
			<TD  class= input>
				<Input class="readonly" name="BankAccno" readonly>
				<!--帐户名 -->
				<Input type="hidden" name="AccName">
			</TD>
	  </tr>
	  <tr class=common>
	    <TD class= title> 批单打印方式 </TD>
        <TD class= input>
        	<input type="radio" name="printMode" value="1" checked >即时打印
        	<input type="radio" name="printMode" value="2">批量打印
        	<input type="radio" name="printMode" value="3">暂不打印
	    </TD>
      </tr>
    </table>
    <p align="center">
    <INPUT VALUE="确  定" class=cssButton TYPE=button onclick="submitForm();"> &nbsp;&nbsp;
	<INPUT VALUE="取  消" class=cssButton TYPE=button onclick="cancel();"> 
	</p>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>