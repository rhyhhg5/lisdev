<%
//程序名称：GEdorTypeLRSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
   <%@page import="com.sinosoft.lis.bq.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。 
 
  
  CErrors tError = null; 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";  

  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

	 transact = request.getParameter("fmtransact");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String grpContNo = request.getParameter("GrpContNo");
	String getMoney = request.getParameter("GetMoney");
	String needGetMoney=request.getParameter("NeedGetMoney");	


  // 准备传输数据 VData
  		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorNo(edorNo);
		tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
		tLPGrpEdorItemSchema.setEdorType(edorType);
		if("Yes".equals(needGetMoney)){//判断是否需要交费
		tLPGrpEdorItemSchema.setGetMoney(getMoney);
		}else{
		tLPGrpEdorItemSchema.setGetMoney(0);
		}
	//工本费 add by hyy
	LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = null;
	String costCheck = request.getParameter("costCheck");
	if(costCheck != null && costCheck.equals("1"))
	{
	    tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
	    tLPEdorEspecialDataSchema.setEdorAcceptNo(edorNo);
	    tLPEdorEspecialDataSchema.setEdorNo(edorNo);
	    tLPEdorEspecialDataSchema.setEdorType(edorType);
	    tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_GB);
	    tLPEdorEspecialDataSchema.setEdorValue(request.getParameter("cost"));
	    tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
	}	
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPGrpEdorItemSchema);
	//add by hyy
	tVData.add(tLPEdorEspecialDataSchema);

	GrpEdorLRDetailUI tGrpEdorLRDetailUI = new GrpEdorLRDetailUI();
	if (!tGrpEdorLRDetailUI.submitData(tVData, transact))
	{
		VData rVData = tGrpEdorLRDetailUI.getResult();
		System.out.println("Submit Failed! " + tGrpEdorLRDetailUI.mErrors.getErrContent());
		Content = transact + "失败，原因是:" + tGrpEdorLRDetailUI.mErrors.getFirstError();
		FlagStr = "Fail";
	}
	else 
	{
		Content = "保存成功";
		FlagStr = "Success";
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

