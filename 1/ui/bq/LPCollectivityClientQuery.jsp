<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<title>客户信息查询</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<script src="./LPCollectivityClientQuery.js"></script> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LPCollectivityClientQueryInit.jsp"%>

</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm target=fraSubmit method=post>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
          单位号码：
          </TD>
          <TD  class= input>
            <Input class= common name=GrpNo >
          </TD>
          <TD  class= title>
          单位名称：
          </TD>
          <TD  class= input>
            <Input class= common name=GrpName >
          </TD>
       </TR>             
      	<TR  class= common>
          <TD  class= title>
          单位性质：
          </TD>
          <TD  class= input>
            <Input class="code" name=GrpNature ondblclick="return showCodeList('GrpNature',[this]);" onkeyup="return showCodeListKey('GrpNature',[this]);">            
          </TD>
          <TD  class= title>
          行业分类：
          </TD>
          <TD  class= input>
            <Input class="code" name=BusinessType ondblclick="return showCodeList('BusinessType',[this]);" onkeyup="return showCodeListKey('BusinessType',[this]);">            
          </TD>
       </TR>     
   </Table>  
      <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="返回" TYPE=button onclick="returnParent();">   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCollectivityClient1);">
    		</TD>
    		<TD class= titleImg>
    			 客户信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divCollectivityClient1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanCollectivityGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 
 </Div>					

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
</html>
