
<%
	//程序名称：GEdorTypeTGSubmit.jsp
	//程序功能：
	//创建日期：2014-12-08
	//创建人  ：
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>

<%
	String FlagStr = "";
	String Content = "";
	String fmAction = request.getParameter("fmAction");
	GlobalInput tG = (GlobalInput) session.getValue("GI");

	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");

	LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	tLPGrpEdorItemSchema.setEdorNo(edorNo);
	tLPGrpEdorItemSchema.setEdorType(edorType);
	tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
	tLPGrpEdorItemSchema.setReasonCode(request.getParameter("reason_tb"));

	EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(edorNo, edorType);

	 String[] getMoney_G = request.getParameterValues("GrpAccGrid4");
	 String[] getMoney_P = request.getParameterValues("PAccGrid6");
	 String[] XTFEE_G = request.getParameterValues("GrpAccGrid5");
	 String[] XTFEE_P = request.getParameterValues("PAccGrid7");
	 String[] accNo = request.getParameterValues("PAccGrid8");
	 String[] riskCode = request.getParameterValues("GrpAccGrid1");
	
	 	System.out.println("---"+getMoney_G[0]);
	
	tEdorItemSpecialData.setGrpPolNo(BQ.FILLDATA);
	tEdorItemSpecialData.add("GETMONEY_G", getMoney_G[0]);
	tEdorItemSpecialData.add("XTFEE_G",XTFEE_G[0]);
	String accType_P2=new ExeSQL().getOneValue("select insuaccno from LMRiskAccPay where riskcode in " +
			"(select riskcode from lcgrppol where grpcontno ='"+request.getParameter("GrpContNo")+"') and  payplancode in " +
			"(select payplancode from LMDutyPay where AccPayClass ='5') with ur " );
	System.out.println("---ssss"+accType_P2);
	if(!"370301".equals(riskCode[0])){
		if(accType_P2.equals(accNo[0])){//第一行为个人账户个人交费部分
			tEdorItemSpecialData.add("GETMONEY_P1",getMoney_P[0]);
			tEdorItemSpecialData.add("XTFEE_P1",XTFEE_P[0]);
			tEdorItemSpecialData.add("GETMONEY_P2",getMoney_P[1]);
			tEdorItemSpecialData.add("XTFEE_P2",XTFEE_P[1]);
		}else{
			tEdorItemSpecialData.add("GETMONEY_P1",getMoney_P[1]);
			tEdorItemSpecialData.add("XTFEE_P1",XTFEE_P[1]);
			tEdorItemSpecialData.add("GETMONEY_P2",getMoney_P[0]);
			tEdorItemSpecialData.add("XTFEE_P2",XTFEE_P[0]);
		}
	}
		
	

	GrpEdorTGDetailUI tGrpEdorTGDetailUI = new GrpEdorTGDetailUI();
	try {
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.addElement(tG);
		tVData.addElement(tLPGrpEdorItemSchema);
		tVData.add(tEdorItemSpecialData);
		tGrpEdorTGDetailUI.submitData(tVData, fmAction);
	} catch (Exception ex) {
		Content = fmAction + "失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "") {
		if (!tGrpEdorTGDetailUI.mErrors.needDealError()) {
			Content = " 保存成功";
			FlagStr = "Success";
		} else {
			Content = " 保存失败，原因是:"
					+ tGrpEdorTGDetailUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

