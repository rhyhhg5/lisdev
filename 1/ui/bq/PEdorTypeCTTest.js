var showInfo;

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();


function DisplayContInfo()
{
	if (fm.ContNo.value == null || fm.ContNo.value == "")
	{
		alert("请输入保单号!");
		return;
	}
	
	var ContNo = getContInfo();
	
	if (ContNo == null || ContNo == "")
	{
		alert("该保单不存在或已经终止!");
		return;
	}
	
	divZTTest.style.display=''; 
	divZTDetail.style.display='none'; 
	
	getContZTInfo();
    initCustomerGrid();
    getCustomerGrid();
    
    initPolGrid(); 
    getPolGrid(fm.all('ContNo').value);
    
}

function getPolGrid(tContNo)
{
	var EdorAppDate = fm.EdorItemAppDate.value;
	var tContno=fm.all('ContNo').value;
    var strSQL = " select RiskCode," +
    			 " (select RiskName from LMRisk where LMRisk.RiskCode = c.RiskCode)," +
    			 " InsuredName, Amnt, " +
				 " nvl((select sum(p.prem) from lcprem p where paystartdate <= '" + EdorAppDate + "' and payenddate >= '" + EdorAppDate + "' and p.polno = c.polno and p.payplantype in ('0')), 0), " +
				 " nvl((select sum(p.prem) from lcprem p where paystartdate <= '" + EdorAppDate + "' and payenddate >= '" + EdorAppDate + "' and p.polno = c.polno and p.payplantype in ('01', '03')), 0), " +
				 " nvl((select sum(p.prem) from lcprem p where paystartdate <= '" + EdorAppDate + "' and payenddate >= '" + EdorAppDate + "' and p.polno = c.polno and p.payplantype in ('02', '04')), 0), " +
    			 " polno " +
    			 " from LCPol c where ContNo='" + tContNo + "' and ( " +
    			 " (appflag = '1' and  (select count(*) from lccontstate s where trim(statetype) in('Terminate') and trim(state) = '1' and trim(statereason) not in ('05', '06') and ((startdate <= '" + fm.EdorItemAppDate.value + "' and '" + fm.EdorItemAppDate.value + "' <= enddate )or(startdate <= '" + fm.EdorItemAppDate.value + "' and enddate is null ))and s.polno = c.polno) = 0 )" +
    			 " or " +
    			 " (appflag = '4' and  (select count(*) from lccontstate s where trim(statetype) in('Terminate') and trim(state) = '1' and trim(statereason) in ('05', '06') and ((startdate <= '" + fm.EdorItemAppDate.value + "' and '" + fm.EdorItemAppDate.value + "' <= enddate )or(startdate <= '" + fm.EdorItemAppDate.value + "' and enddate is null ))and s.polno = c.polno) > 0 )" +
    			 " )";
	brrResult = easyExecSql(strSQL);
	if (brrResult)
	{
		displayMultiline(brrResult,PolGrid);
	} 
	
	/*  
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    alert(turnPage.strQueryResult);
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;    
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	 
	**/
}

function getCustomerGrid()
{

    var tContNo=fm.all("ContNo").value;
    if(tContNo!=null&&tContNo!="")
    {
        var strSQL = " Select a.appntno, '投保人', a.appntname, " 
        			+" (select trim(n.code)||'-'||trim(n.CodeName) from LDCode n where trim(n.codetype) = 'sex' and trim(n.code) = trim(appntsex)),"
        			+" a.appntbirthday, " 
        			+" (select trim(m.code)||'-'||trim(m.CodeName) from LDCode m where trim(m.codetype) = 'idtype' and trim(m.code) = trim(idtype)), "
        			+" a.idno " 
        			+" From lcappnt a Where contno='" + tContNo+"'"
					+" Union"					
					+" Select i.insuredno, '被保人', i.name, " 
					+" (select trim(u.code)||'-'||trim(u.CodeName) from LDCode u where trim(u.codetype) = 'sex' and trim(u.code) = trim(sex)),"
					+" i.Birthday," 
					+" (select trim(y.code)||'-'||trim(y.CodeName) from LDCode y where trim(y.codetype) = 'idtype' and trim(y.code) = trim(idtype)), "
					+" i.IDNo " 
					+" From lcinsured i Where contno='" + tContNo+"'";
		arrResult = easyExecSql(strSQL);
		if (arrResult)
		{
			displayMultiline(arrResult,CustomerGrid);
		}
    }
} 

function edorTypeCTSave()
{
    var row = PolGrid.mulLineCount;
    var i = 0;
      for(var m = 0; m < row; m++ )
      {
        if(PolGrid.getChkNo(m))
        {
          i = i+1;
        }
      }
      if(i == 0)
      {
        alert("请选择需要计算的险种！");
        return false;
      }
	
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close(); 
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    if (FlagStr == "Succ")
    {
    	divZTDetail.style.display=''; 
    	initCTFeePolGrid();
    	getCTFeePolGrid();
    	
    	initCTFeeDetailGrid();
    	getCTFeeDetailGrid();
    }

}

function getCTFeeDetailGrid()
{

	if (fm.StrCTFeeDetail.value == null || fm.StrCTFeeDetail.value.trim == "null")
	{
		return;
	}
	turnPage2.strQueryResult = fm.StrCTFeeDetail.value;
    turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage2.pageDisplayGrid = CTFeeDetailGrid;    
    turnPage2.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage2.pageDisplayGrid);	
    
    
}

function getCTFeePolGrid()
{
	turnPage.strQueryResult = fm.StrCTFeePol.value;
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = CTFeePolGrid;    
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
}

/*********************************************************************
 *  保单查询
 *  描述: 保单查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function contQuery()
{ 
	mflag = "0";
	var showInfo = window.open("../sys/AllProposalQueryMain.jsp","AllProposalQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	showInfo.focus();  
}


function afterQuery( arrQueryResult ) 
{
	if( arrQueryResult != null ) 
	{
		try { fm.all('ContNo').value = arrQueryResult[0][0]; } catch(ex) { };	
		DisplayContInfo();	
	}

}

/*********************************************************************
 *  查询保单详细信息
 *  描述: 查询保单详细信息
 *  参数  ：  ContNo
 *  返回值：  无
 *********************************************************************
 */
function getContInfo()
{
    var strSQL;
    var ContNo;
    strSQL =  " select ContNo " 
            + " from lccont "
            + " where ContNo = '" + fm.ContNo.value + "'" 
            + " and appflag = '1'";

    var brr = easyExecSql(strSQL);
    if ( brr )
    {
        brr[0][0]==null||brr[0][0]=='null'?'0':ContNo = brr[0][0];
    }
	else         
	{
		ContNo = "";
	}
	return ContNo;
}

function getContZTInfo()
{
    var MainPolNo;
	var MainPayIntv;
	var MainPayToDate;
	var MainPayEndDate;
	var MainRiskCode;
    strSQL = " select polno, paytodate, payintv, payenddate, riskcode from lcPol " +
    		 " where contno = '" + fm.ContNo.value + 
    		 "' and polno = mainpolno";
    var hrr = easyExecSql(strSQL);
    if ( hrr ) 
    {
    	hrr[0][0]==null||hrr[0][0]=='null'?'0':MainPolNo = hrr[0][0];
    	hrr[0][1]==null||hrr[0][1]=='null'?'0':MainPayToDate = hrr[0][1];
    	hrr[0][2]==null||hrr[0][2]=='null'?'0':MainPayIntv = hrr[0][2];
    	hrr[0][3]==null||hrr[0][3]=='null'?'0':MainPayEndDate = hrr[0][3];
    	hrr[0][4]==null||hrr[0][4]=='null'?'0':MainRiskCode = hrr[0][4];
    }
	else
	{
		alert("查找主险失败");
		return;
	}	
	    
    strSQL = " select customgetpoldate, ReceiveDate, cvalidate from lccont " +
    		 " where contno = '" + fm.ContNo.value + "' ";
    var drr = easyExecSql(strSQL);
    
    if ( drr ) 
    {
    	drr[0][0]==null||drr[0][0]=='null'?'0':fm.CustomGetPolDate.value = drr[0][0];
        //drr[0][1]==null||drr[0][1]=='null'?'0':fm.ReceiveDate.value = drr[0][1];
        drr[0][2]==null||drr[0][2]=='null'?'0':fm.CvaliDate.value = drr[0][2];
        
	    strSQL = " select count(*) from lccontstate  " +
	    		 " where statetype in('Available') and state = '1' " +
	    		 " and  ( (startdate <= '" + fm.EdorItemAppDate.value + "' and enddate >= '" + fm.EdorItemAppDate.value + "' ) " +
	    		 " or (startdate <= '" + fm.EdorItemAppDate.value + "' and enddate is null )) " +
	    		 " and polno = '" + MainPolNo + "' ";
	    var frr = easyExecSql(strSQL);
	    var CalDate;
	    if ( frr ) 
	    {
	    	frr[0][0]==null||frr[0][0]=='null'?'0':disAvailable = frr[0][0];
	    	//alert(disAvailable);
	    	//判断主险是否失效
	    	if (disAvailable == 0)
	    	{
	    		//没有失效
	    		
	    		//判断交至日期
	    		
	    		var intv = dateDiffCT(fm.EdorItemAppDate.value, MainPayToDate, "D");
	    		//alert(intv);
	    		if (intv > 0)
	    		{
	    			CalDate = fm.EdorItemAppDate.value;
	    		}
	    	    else
	    		{
	    			if (MainPayIntv == 0 || MainPayToDate == MainPayEndDate)
	    			{
	    				CalDate = fm.EdorItemAppDate.value;
	    			}
	    		    else
	    		    {
	    		    	CalDate = MainPayToDate;
	    		    }
	    		}
	    		
	    	}
	    	else
	    	{
	    		CalDate = MainPayToDate;
	    	}
	    }

	    strSQL = " select riskcode from lmrisksort " +
	    		 " where riskcode = '" + MainRiskCode + 
	    		 "' and risksorttype = '30' ";
	    var srr = easyExecSql(strSQL);
	    if ( srr )  //如果是不定期交费，保单年度显示实际已过年度
	    {
	    	CalDate = fm.EdorItemAppDate.value;
	    }

        var intval = dateDiffCT(fm.CvaliDate.value, CalDate, "M");
        var year = (intval- intval%12)/12;
        var month = intval%12;
        fm.Inteval.value = year + " 年 零 " + month + " 月";
        
        if (MainPayIntv == 0)
        {
        	//alert("趸交");
        	//判断有无附加险
		    strSQL = " select paytodate from lcPol " +
		    		 " where contno = '" + fm.ContNo.value + 
		    		 "' and polno <> mainpolno and rownum = 1  and appflag = '1' ";
		    var mrr = easyExecSql(strSQL);
		    if ( mrr ) 
		    {
				mrr[0][0]==null||mrr[0][0]=='null'?'0':fm.PayToDate.value = mrr[0][0];
		    }
		    else
		    {
		    	fm.PayToDate.value = "";
		    }
        }
        else
    	{
    		//alert("期交");
    		//判断期满
    		//alert(MainPayEndDate);
    		//alert(MainPayToDate);
    		if (MainPayEndDate == MainPayToDate)
    		{
	        	//判断有无附加险
			    strSQL = " select paytodate from lcPol " +
			    		 " where contno = '" + fm.ContNo.value + 
			    		 "' and polno <> mainpolno and rownum = 1  and appflag = '1' ";
			    var mrr = easyExecSql(strSQL);
			    if ( mrr ) 
			    {
					mrr[0][0]==null||mrr[0][0]=='null'?'0':fm.PayToDate.value = mrr[0][0];
			    }
			    else
			    {
			    	fm.PayToDate.value = "";
			    }
    		}
    	    else
    		{
    			fm.PayToDate.value = MainPayToDate;
    		}
    	}
    }
}

/**
* 计算两个日期的差,返回差的月数(M)或天数(D) (其中天数除2.29这一天)
* <p><b>Example: </b><p>
* <p>dateDiff("2002-10-1", "2002-10-3", "D") returns "2"<p>
* <p>dateDiff("2002-1-1", "2002-10-3", "M") returns "9"<p>
* @param dateStart 减日期
* @param dateEnd 被减日期
* @param MD 标记，“M”为要求返回差的月数；“D”为要求返回差的天数
* @return 返回两个日期差的月数(M)或天数(D)
*/
function dateDiffCT(dateStart,dateEnd,MD)
{
  if(dateStart==""||dateEnd=="")
  {
  	return false;
  }
  if (typeof(dateStart) == "string") {
    dateStart = getDate(dateStart);
  }

  if (typeof(dateEnd) == "string") {
    dateEnd = getDate(dateEnd);
  }

  var i;
  if(MD=="D") //按天计算差
  {
    var endD = dateEnd.getDate();
    var endM = dateEnd.getMonth();
    var endY = dateEnd.getFullYear();
    var startD = dateStart.getDate();
    var startM = dateStart.getMonth();
    var startY = dateStart.getFullYear();
    var startT=new Date(startY,startM-1,startD);
    var endT=new Date(endY,endM-1,endD);
    var diffDay=(endT.valueOf()-startT.valueOf())/86400000;
    return diffDay;
  }
  else //按月计算差
  {
    var endD = dateEnd.getDate();
    var endM = dateEnd.getMonth();
    var endY = dateEnd.getFullYear();
    var startD = dateStart.getDate();
    var startM = dateStart.getMonth();
    var startY = dateStart.getFullYear();

    if(endD<startD)
    {
      return (endY-startY)*12 + (endM-startM) -1;  //只算整月数
    }
    else
    {
      return (endY-startY)*12 + (endM-startM);
    }
  }
}

/**
* 获取日期对象
* @param strDate 日期字符串
* @param splitOp 分割符
* @return 返回日期对象
*/
function getDate(strDate, splitOp) {
  if (splitOp == null) splitOp = "-";

  var arrDate = strDate.split(splitOp);
  if (arrDate[2] == "31")
  {
  	arrDate[2] = "30";
  }
  if (arrDate[1].length == 1) arrDate[1] = "0" + arrDate[1];
  if (arrDate[2].length == 1) arrDate[2] = "0" + arrDate[2];

  return new Date(arrDate[0], arrDate[1], arrDate[2]);

}