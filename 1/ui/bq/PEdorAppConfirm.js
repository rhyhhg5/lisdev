//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";

var targetWin = null;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initPEdorAppConfirmGrid();
  // showSubmitFrame(mDebug);
  fm.submit(); //提交
}

function  edorAppConfirm()
{
	fm.all("EdorNo").value = PEdorAppConfirmGrid.getRowColData(0,1);
	//OtherNo在打印保全交费表时使用
	fm.all("OtherNo").value = PEdorAppConfirmGrid.getRowColData(0,1);
	fm.all("fmtransact").value = "INSERT||EDORAPPCONFIRM";
	
	if (window.confirm("是否确认本次申请?"))
	{
		
		//alert("申请确认中...");
		var showStr="正在计算数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		//showSubmitFrame(mDebug);
		fm.submit();
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,prtParams )
{
  //alert("prtParams : " + prtParams);
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    easyQueryClick();

    	if (window.confirm("是否打印本次保全申请书?")){
    	    PrtEdor();
        }
        if (window.confirm("是否打印本次费用通知书?")){
    	    PrePrtPay(prtParams);
        }

//    top.close();
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

// 查询按钮
function easyQueryClick()
{
	var EdorState;
	
	tEdorState=top.opener.fm.all('EdorState').value;
		
	// 初始化表格
	initPEdorAppConfirmGrid();
	
	var tReturn = parseManageComLimit(); 
	//alert(tReturn);

	// 书写SQL语句,GetMoney
	var strSQL = "";
	strSQL = "select EdorNo,ContNo,EdorType,EdorValiDate,EdorAppDate from LPEdorItem where EdorState='"+ tEdorState//+" and"+tReturn
				 +"' "+ getWherePart( 'ContNo' )
				 + getWherePart( 'EdorNo' );
  
//    alert(strSQL);
  
	execEasyQuery( strSQL );
	
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPEdorAppConfirmGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PEdorAppConfirmGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

// 数据返回父窗口
function returnParent()
{

	top.close();
		
}

//打印批单
function PrtEdor(prtParams)
{

	if (fm.all("EdorNo").value == "0") {
		alert("由于数据出错，无法打印批单。");
		return;
	}
	
	parent.fraInterface.fm.action = "../f1print/EndorsementF1P.jsp";
	parent.fraInterface.fm.target="fprint1";	
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorAppConfirmSubmit.jsp";
	parent.fraInterface.fm.target="fraSubmit";


}

//打印保全交费
function PrePrtPay(prtParams)
{
    /**fm.all('prtParams').value = prtParams;
    parent.fraInterface.fm.action = "../f1print/EdorFeeF1PSave.jsp";
    fm.all('fmtransact').value = 'PRINT';
    parent.fraInterface.fm.target="fraSubmit";
    fm.submit();*/
    if (prtParams!="") {
    parent.fraInterface.fm.action = "../f1print/EndorsementF1PJ1.jsp?type=CashValue";
    fm.all('fmtransact').value = 'PRINT';
    parent.fraInterface.fm.target="fraSubmit";
    fm.submit();    
    parent.fraInterface.fm.action = "./PEdorAppConfirmSubmit.jsp";
    parent.fraInterface.fm.target="fraSubmit";
    }    

}

function PrtPay(GetNoticeNo,AppntNo,SumdeupayMoney)
{

    fm.all('GetNoticeNo').value = GetNoticeNo;
    fm.all('AppntNo').value = AppntNo;
    fm.all('SumDuePayMoney').value = SumdeupayMoney;
/*    
    var urlStr="../f1print/EdorFeeF1PSave.jsp?OtherNo=" + fm.all("OtherNo").value
       +"&GetNoticeNo=" + fm.all("GetNoticeNo").value
       + "&AppntNo=" + fm.all("AppntNo").value
       +"&SumDuePayMoney=" + fm.all("SumDuePayMoney").value
       +"&fmtransact=" + fm.all("fmtransact").value;  

    alert(urlStr);
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:950px;dialogHeight:600px");   	

*/
    parent.fraInterface.fm.action = "../f1print/EdorFeeF1PSave.jsp";
    parent.fraInterface.fm.target="f1print2";
    fm.submit();
    parent.fraInterface.fm.action = "./PEdorAppConfirmSubmit.jsp";
    parent.fraInterface.fm.target="fraSubmit";
   
}

function QueryUWDetailClick()
{
  //var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  var cEdorNo = fm.all("EdorNo").value;
  var cContNo = fm.all("ContNo").value;		
  if (cContNo != "" && cEdorNo !="" )
  {
  	window.open("./PEdorAppUWErrMain.jsp?ProposalNo1="+cContNo+"&EdorNo="+cEdorNo,"window1");
  	//showInfo.close();
  }
  else
  {
  	//showInfo.close();
  	alert("请先进行申请确认!");  
  }
	
}