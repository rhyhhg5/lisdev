<html> 
<% 
//程序名称：PEdorTypeWXInput.jsp
//程序功能：
//创建日期：2011-08-03
//创建人  xp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="./PEdor.js"></SCRIPT>
	<SCRIPT src="./PEdorTypeWXInput.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="PEdorTypeWXInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="./PEdorTypeWXSubmit.jsp" method=post name=fm target="fraSubmit">     
    <Div  id= "divIndiInfo" style= "display: ''">
    <table>
      <tr>
       <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJSGrid);"></td>
       <td class= titleImg>保单续期记录</td>
      </tr>
    </table>
  </Div>
    <div id= "divLJSGrid" style= "display: ''">
    <table class= common>
      <tr  class= common>
        <td text-align: left colSpan=1>
          <span id="spanLJSGrid"></span> 
        </td>
      </tr>
    </table>			
	<div id= "divPage" align=center style= "display: 'none' ">
		<input value="首  页" class=cssButton type=button onclick="turnPage.firstPage();"> 
		<input value="上一页" class=cssButton type=button onclick="turnPage.previousPage();"> 					
		<input value="下一页" class=cssButton type=button onclick="turnPage.nextPage();"> 
		<input value="尾  页" class=cssButton type=button onclick="turnPage.lastPage();"> 
	</div>	
  </div>
  <br/>
    <br/>
   <Div id="divFeeBase" style= "display: '' ">
   <table class = "common"> 
   	 <tr  class= "common"><td><b>请输入多扣的初始费用金额：</b></td></tr>
     <tr  class= "common">
      <td class="title">补费金额</td>
      <td class= input><input class="common" type="text" name="getFee" verify="补费金额|notnull&num"></td>
	 </tr>
    <tr> 
    <td><b>请选择本保单后续续期初始费用扣除方式：<b></td>
     </tr>
    <tr>
    <td><input type="radio" name="chkfeemode" id="chkfeemode" value="1" /> 采用续期核销时保额计算</td>
	<td><input type="radio" name="chkfeemode" id="chkfeemode" value="0" /> 采用投保时保额计算</td>
	</tr>
  </table>
  <br>
  <div  id= "divSubmit" style="display:''">
    <table class = common>
      <tr class= common>
        <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
        <input type=Button name="returnButton" class= cssButton value="返  回" onclick="returnParent();">
      </tr>
    </table>
  </div>
  <br>
  <input type=hidden id="Amnt" name="Amnt">
  <input type=hidden id="TBAmnt" name="TBAmnt">
  <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
  <input type=hidden id="EdorAppDate" name="EdorAppDate">
  <input type=hidden id="EdorNo" name="EdorNo">
  <input type=hidden id="ContNo" name="ContNo">
  <input type=hidden id="EdorType" name="EdorType">
  <input type=hidden id="EdorTypeName" name="EdorTypeName">
  <input type=hidden id="EdorValiDate" name="EdorValiDate">
  <input type=hidden id="getnoticeno" name="getnoticeno">
  <input type=hidden id="Fee" name="Fee">
  <input type=hidden id="feemode" name="feemode">
  </form>

</body>
</html>
