<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PEdorConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.task.*"%>
<%  
		int failType = 0;
    String flag = "";
    String content = "";
    String edorAcceptNo = request.getParameter("EdorAcceptNo");
    GlobalInput tGI = (GlobalInput)session.getValue("GI");

		VData data = new VData();
		data.add(tGI); 
		data.add(edorAcceptNo);
		PEdorAutoUWUI tPEdorAutoUWUI = new PEdorAutoUWUI();
		if (!tPEdorAutoUWUI.submitData(data))
		{
			flag = "Fail";
			content = "自动核保失败！原因是：" + tPEdorAutoUWUI.getError();
		}
	  else
	  {
			if (tPEdorAutoUWUI.getUWFlag().equals("5"))
			{
				flag = "Fail";
				content = "自动核保未通过，已送人工核保。原因如下：\n" + tPEdorAutoUWUI.getUWErrors();
			}
			else  //UWFlag = "9" 或 UWFlag = "1" ("1"是已人工核保)
			{
		  	//自动审批
		  	data = new VData();
		  	LGWorkSchema workSchema = new LGWorkSchema();
		  	workSchema.setWorkNo(edorAcceptNo);
		  	data.add(tGI);
		  	data.add(workSchema);
		  
		  	//自动审批失败
		  	TaskAutoExaminationBL exame = new TaskAutoExaminationBL();
		  	if (!exame.submitData(data, ""))
		  	{
		  		failType = 1;
		  		flag = "Fail";
		  		content = "自动送审批失败！" + exame.mErrors.getFirstError();
		  	}
		    else
		    {
		    	//把补退费金额放入财务接口
			    FinanceDataUI tFinanceDataUI = new FinanceDataUI(tGI, edorAcceptNo);
	        if (!tFinanceDataUI.submitData())
	        {
	        	flag = "Fail";
	          content = "设置财务数据失败！原因是：" + tFinanceDataUI.getError();
	        }
		    
		      //保全结案
		      EdorFinishUI tEdorFinishUI = new EdorFinishUI(tGI, edorAcceptNo);
		      if (!tEdorFinishUI.submitData("PERSONAL"))
		      {
		      	flag = "Fail";
		  			content = "保全确认未通过！原因是：" + tEdorFinishUI.getError();
		      }
		   		else
		    	{
		    		flag = "Succ";
		  			content = "保全确认成功！";
		    	}
				}
	    }
	  }
    content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	if(<%=failType%> == 1)
	{
		//自动审批未通过
		parent.fraInterface.fm.failType.value = 1;
	}
  parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>","<%=edorAcceptNo%>");
</script>
</html>

