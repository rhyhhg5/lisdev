//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var mFlag;

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 
//****************yangzhao********11.11
//自动填写受益人信息
function afterCodeSelect( cCodeName, Field ) 
{
    if (cCodeName == "Relation") 
    {
	  if (Field.value == "00") 
	  {
               var strSql = "select * from LCInsured where polno='" + fm.PolNo.value + "' and CustomerNo in (select insuredNo from lcpol where polno='" + fm.PolNo.value + "')";
               var arrResult = easyExecSql(strSql);
               //alert(arrResult);
               if (arrResult==null) 
                    alert("被保人信息查询失败！");
	       var index = LCBnfGrid.mulLineCount;
               LCBnfGrid.setRowColData(index-1, 3, arrResult[0][6]);
               LCBnfGrid.setRowColData(index-1, 4, arrResult[0][7]);
               LCBnfGrid.setRowColData(index-1, 5, arrResult[0][8]);
               LCBnfGrid.setRowColData(index-1, 6, arrResult[0][20]);
               LCBnfGrid.setRowColData(index-1, 7, arrResult[0][22]);
               LCBnfGrid.setRowColData(index-1, 11, arrResult[0][28]);
               LCBnfGrid.setRowColData(index-1, 12, arrResult[0][29]);
               LCBnfGrid.setRowColData(index-1, 13, arrResult[0][30]);
               LCBnfGrid.setRowColData(index-1, 14, arrResult[0][52]);	  
	  } 
    }	
}

function verify() 
{
    var strSql = "select * from LCInsured where polno='" + fm.PolNo.value + "' and CustomerNo in (select insuredNo from lcpol where polno='" + fm.PolNo.value + "')";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult==null) 
        alert("被保人信息查询失败！");
    for (i=0; i<LCBnfGrid.mulLineCount; i++) 
    {
        //alert("relation:" + LCBnfGrid.getRowColData(i, 8));
        if (LCBnfGrid.getRowColData(i, 8)=="00") 
        {
            if (LCBnfGrid.getRowColData(i, 1)=="1") 
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人类别 不能是身故（死亡）受益人！");
                return false;
            }
            if (LCBnfGrid.getRowColData(i, 3)!=arrResult[0][6]) 
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人姓名 不能进行修改，需要与被保人相同！");
                return false;
            }
            if (LCBnfGrid.getRowColData(i, 4)!=arrResult[0][7]) 
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人性别 不能进行修改，需要与被保人相同！");
                return false;
            }
            if (LCBnfGrid.getRowColData(i, 5)!=arrResult[0][8]) 
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人出生日期 不能进行修改，需要与被保人相同！");
                return false;
            }
            if (LCBnfGrid.getRowColData(i, 6)!=arrResult[0][20]) 
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人证件类型 不能进行修改，需要与被保人相同！");
                return false;
            }
            if (LCBnfGrid.getRowColData(i, 7)!=arrResult[0][22]) 
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人证件号码 不能进行修改，需要与被保人相同！");
                return false;
            }
            if (LCBnfGrid.getRowColData(i, 11)!=arrResult[0][28]) 
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人联系地址 不能进行修改，需要与被保人相同！");
                return false;
            }
            if (LCBnfGrid.getRowColData(i, 12)!=arrResult[0][29]) 
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人邮编 不能进行修改，需要与被保人相同！");
                return false;
            }
            if (LCBnfGrid.getRowColData(i, 13)!=arrResult[0][30])
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人电话 不能进行修改，需要与被保人相同！");
            }
            if (LCBnfGrid.getRowColData(i, 14)!=arrResult[0][52])
            {
                alert("因为受益人选择了同被保人，所以第" + (i+1) + "行的 受益人户口所在地 不能进行修改，需要与被保人相同！");
            }        
        }
    
        if (LCBnfGrid.getRowColData(i, 4) == "") 
        {
	    alert("第" + (i+1) + "行的性别必须填写！");
	    return false;
	}
	if (LCBnfGrid.getRowColData(i, 5) == "") 
	{
	    alert("第" + (i+1) + "行的出生日期必须填写！");
	    return false;
	}
    
        var strChkIdNo = "";
	  //以年龄和性别校验身份证号
	if (LCBnfGrid.getRowColData(i, 6) == "0") 
	{
	    strChkIdNo = chkIdNo(LCBnfGrid.getRowColData(i, 7), LCBnfGrid.getRowColData(i, 5), LCBnfGrid.getRowColData(i, 4));
	}
	if (strChkIdNo != "") 
	{
	    alert("第" + (i+1) + "行" + strChkIdNo);
	    return false;
	}  
	//2017-8-28
	if (LCBnfGrid.getRowColData(i, 6) == "0" || LCBnfGrid.getRowColData(i, 6) == "5") 
	{
	    strChkIdNo = checkIdNo(LCBnfGrid.getRowColData(i, 6),LCBnfGrid.getRowColData(i, 7), LCBnfGrid.getRowColData(i, 5), LCBnfGrid.getRowColData(i, 4));
	}
	if (strChkIdNo != "") 
	{
	    alert("第" + (i+1) + "行" + strChkIdNo);
	    return false;
	} 
    }
    return true;
}
//***************************************************

function edorTypeBCReturn()
{
			
	 	if (mFlag=='0')
	   		fm.all('PolNo').value='';
		initForm();
}

function edorTypeBCSave()
{
  if (!verify()) 
      return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||GRPMAIN";
 	//showSubmitFrame(mDebug);
  fm.submit();
  //showSubmitFrame(mDebug);

}

function customerQuery()
{	
	window.open("./LCBnfQuery.html");
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initLCBnfGrid();
 //  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
 // alert("FlagStr" + FlagStr);
//  alert("Result : " + Result);
  
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact=fm.all('fmtransact').value;
//  	alert(tTransact);
	if (tTransact=="QUERY||DETAIL")
	{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
  		//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  

  		//查询成功则拆分字符串，返回二维数组
  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
		if (tArr != null) {
		    turnPage.arrDataCacheSet =chooseArray(tArr,[2,9,10,11,12,13,14,6,5,7,8,18,19,15]);
		    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		    turnPage.pageDisplayGrid = LCBnfGrid;    
		  
		    //设置查询起始位置
		    turnPage.pageIndex = 0;
		    //在查询结果数组中取出符合页面显示大小设置的数组
	  	    var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
		    //调用MULTILINE对象显示查询结果
	   	    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	   	} else {
	   	    //LCBnfGrid.delBlankLine("LCBnfGrid");
	   	    LCBnfGrid.clearData("LCBnfGrid");
	   	}
	   	
	   	divBnfInfo.style.display ='';
	} else if (tTransact=="QUERY||MAIN") {
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		        turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
  		//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
		if (tArr != null) {	
			turnPage.arrDataCacheSet =chooseArray(tArr,[1,11,31,20,21,29]);
			//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		 	turnPage.pageDisplayGrid = LCPolGrid;    
		  
		  //设置查询起始位置
		 	turnPage.pageIndex       = 0;
		 	//在查询结果数组中取出符合页面显示大小设置的数组
	  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
			//调用MULTILINE对象显示查询结果
	   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	        } else {
	            LCPolGrid.delBlankLine("LCPolGrid");
	        }
	}  else if (tTransact=="QUERY||COND") {
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
  		//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
			
			turnPage.arrDataCacheSet =chooseArray(tArr,[1,11,31,20,21,29]);
			//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		 	turnPage.pageDisplayGrid = LCPolGrid;    
		  
		  //设置查询起始位置
		 	turnPage.pageIndex       = 0;
		 	//在查询结果数组中取出符合页面显示大小设置的数组
	  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
			//调用MULTILINE对象显示查询结果
	   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
		}
		else
		{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   		
	  if (mFlag=='0')
	   		fm.all('PolNo').value='';
   	initForm();

  	}
  	
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		/**
		alert("aa:"+arrResult[0][5]);
		fm.all('Nationality').value = arrResult[0][5];
		fm.all('Marriage').value=arrResult[0][6];
		fm.all('Stature').value=arrResult[0][7];
		fm.all('Avoirdupois').value=arrResult[0][8];
		fm.all('ICNo').value=arrResult[0][9];
		fm.all('HomeAddressCode').value=arrResult[0][10];
		fm.all('HomeAddress').value=arrResult[0][11];
		fm.all('PostalAddress').value=arrResult[0][12];
		fm.all('ZipCode').value=arrResult[0][13];
		fm.all('Phone').value=arrResult[0][14];
		fm.all('Mobile').value=arrResult[0][15];
		fm.all('EMail').value=arrResult[0][16];
		*/
		// 查询保单明细
		queryBnfDetail();
	}
}
function queryBnfDetail()
{
	var tEdorNO;
	var tEdorType;
	var tGrpPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	//alert(tEdorNo);
	tEdorType=fm.all('EdorType').value;
	//alert(tEdorType);
	tPolNo=fm.all('PolNo').value;
	//alert(tPolNo);
	tCustomerNo = fm.all('CustomerNo').value;
	//alert(tCustomerNo);
	//top.location.href = "./BnfQueryDetail.jsp?EdorNo=" + tEdorNo+"&EdorType="+tEdorType+"&GrpPolNo="+tGrpPolNo+"&CustomerNo="+tCustomerNo;
	parent.fraInterface.fm.action = "./BnfQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./GEdorTypeBCSubmit.jsp";
}
function returnParent()
{
	top.close();
}
