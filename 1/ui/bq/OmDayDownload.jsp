<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：OmDayDownload.jsp
//程序功能：万能可转投资净额日结报表
//创建日期：2009-6-9
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%		
    System.out.println("in bq OmnDayDownload.jsp");	
    boolean operFlag = true;
    String FlagStr = "";
    String Content = "";
    String StartDate = request.getParameter("StartDate1");
    String EndDate = request.getParameter("EndDate1");
    String ManageCom = request.getParameter("ManageCom1");
    String RiskCode = request.getParameter("RiskCode1");
    GlobalInput gi = (GlobalInput)session.getValue("GI");
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("StartDate",StartDate);
    tTransferData.setNameAndValue("EndDate",EndDate);
    tTransferData.setNameAndValue("ManageCom",ManageCom);
    tTransferData.setNameAndValue("RiskCode",RiskCode);
    System.out.println(StartDate+", "+EndDate+", "+ManageCom);
    
    ArrayList arrayList = new ArrayList();
    arrayList.add("startDate");
    arrayList.add(StartDate);
    arrayList.add("endDate");
    arrayList.add(EndDate);
    
    tTransferData.setNameAndValue("arrayList", arrayList);
    VData tVData = new VData();
    tVData.addElement(gi);
    tVData.addElement(tTransferData);
          
    OmDayDownloadBL tOmDayDownloadBL = new OmDayDownloadBL(); 
    if(!tOmDayDownloadBL.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tOmDayDownloadBL.mErrors.getErrContent();                
    }
    else
    { 
	    Readhtml rh=new Readhtml();
	    rh.XmlParse(tOmDayDownloadBL.getInputStream());
	    String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
		String temp=realpath.substring(realpath.length()-1,realpath.length());
	    if(!temp.equals("/"))
	    {
	  	  realpath=realpath+"/"; 
	    }
	    String templatename=rh.getTempLateName();//模板名字
	    System.out.println("templatename="+templatename);
	    String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
	    System.out.println("templatepathname="+templatepathname);
	    String date=PubFun.getCurrentDate().replaceAll("-","");
	    String time=PubFun.getCurrentTime().replaceAll(":","");
	    String outname="OmDay"+gi.Operator+date+time+".xls";
	    String outpathname=realpath+"vtsfile/"+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	 
	    rh.setReadFileAddress(templatepathname);
	    rh.setWriteFileAddress(outpathname);
	    rh.start("vtsmuch");
	    try 
	    {
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
			outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
			outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
			System.out.println("outpathname="+outpathname);
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}   
%>
<script language="JavaScript">
  parent.fraInterface.afterDownload("<%=outname%>","<%=outpathname%>");
</script>
<%
  }
%>
