 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//  showSubmitFrame(mDebug);
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
//    initForm();
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在CollectivityClientInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if(!verifyInput()) 
    return false;
  if(fm.all('GrpNo').value!="")
    {
    alert("不能保存");	
    return false;	
    }
  return true;

}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function insertClick()
{
  //下面增加相应的代码

  //表单中的隐藏字段"活动名称"赋为INSERT 
  fm.all('Transact').value ="INSERT";
  submitForm(); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  //表单中的隐藏字段"活动名称"赋为UPDATE 
    fm.all('Transact').value ="UPDATE";
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交 
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	var loadFlag = "0";

	try
	{
		if( top.opener.mShowCustomerDetail == "GROUPPOL" ) loadFlag = "1";
	}
	catch(ex){}
	
	if( loadFlag == "1" )
		parent.fraInterface.window.location = "./CollectivityClientQuery.jsp";
	else
    	window.open("./CollectivityClientQuery.html");
  
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
//  alert("delete");
  //尚未考虑全部为空的情况
  if(fm.all('GrpNo').value == '')
  {
   alert("单位号码不能为空!");
   return false;
  }
  else
  {
    fm.all('Transact').value ="DELETE";
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//  showSubmitFrame(mDebug);
    fm.submit(); //提交 
    initForm();
  }
  
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function closeCurrentWindow()
{
	top.close();
}

function returnParent()
{
	if (fm.all('GrpNo').value == '') {
		alert("请保存后再返回！");
		return;
	}

	var arrReturn = new Array();
	arrReturn[0] = new Array();
	arrReturn[0][0] = fm.all('GrpNo').value;
	top.opener.afterGrpDetailQuery( arrReturn );
	top.close();	
	
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	if( arrQueryResult != null )
	{
		fm.all( 'GrpNo' ).value = arrQueryResult[0][0];
		
		easyQueryClick();
	}
}

// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL = "";
	var tGrpNo = fm.all( 'GrpNo' ).value;
	strSQL = "select * from LDGrp where GrpNo='" + tGrpNo + "'";
//alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initInpBox();
		 
		// 显示查询结果
	    fm.all('GrpNo').value = arrResult[0][0];
	    fm.all('Password').value = arrResult[0][1];
	    fm.all('GrpName').value = arrResult[0][2];
	    fm.all('GrpAddressCode').value = arrResult[0][3];
	    fm.all('GrpAddress').value = arrResult[0][4];
	    fm.all('GrpZipCode').value = arrResult[0][5];
	    fm.all('BusinessType').value = arrResult[0][6];
	    fm.all('GrpNature').value = arrResult[0][7];
	    fm.all('Peoples').value = arrResult[0][8];
	    fm.all('RgtMoney').value = arrResult[0][9];
	    fm.all('Asset').value = arrResult[0][10];
	    fm.all('NetProfitRate').value = arrResult[0][11];
	    fm.all('MainBussiness').value = arrResult[0][12];       
	    fm.all('Corporation').value = arrResult[0][13];
	    fm.all('ComAera').value = arrResult[0][14];       
	    fm.all('LinkMan1').value = arrResult[0][15];   
	    fm.all('Department1').value = arrResult[0][16];
	    fm.all('HeadShip1').value = arrResult[0][17];
	    fm.all('Phone1').value = arrResult[0][18];
	    fm.all('E_Mail1').value = arrResult[0][19];
	    fm.all('Fax1').value = arrResult[0][20];
	    fm.all('LinkMan2').value = arrResult[0][21];
	    fm.all('Department2').value = arrResult[0][22];
	    fm.all('HeadShip2').value = arrResult[0][23];
	    fm.all('Phone2').value = arrResult[0][24];
	    fm.all('E_Mail2').value = arrResult[0][25];
	    fm.all('Fax2').value = arrResult[0][26];
	    fm.all('Fax').value = arrResult[0][27];
	    fm.all('Phone').value = arrResult[0][28];
	    fm.all('GetFlag').value = arrResult[0][29];       
	    fm.all('Satrap').value = arrResult[0][30];
	    fm.all('EMail').value = arrResult[0][31];       
	    fm.all('FoundDate').value = arrResult[0][32];   
	    fm.all('BankCode').value = arrResult[0][34];
	    fm.all('BankAccNo').value = arrResult[0][33];
	    fm.all('GrpGroupNo').value = arrResult[0][35];
	    fm.all('State').value = arrResult[0][36];
	    fm.all('Remark').value = arrResult[0][37];
	    fm.all('BlacklistFlag').value = arrResult[0][38];
	} // end of if
}


