<%@page import="com.sinosoft.lis.pubfun.*"%>
<% 
 String acceptNo=request.getParameter("AcceptNo");
%>
 
<script language="JavaScript">
 
function initInpBox()
{
  fm.all('AcceptNo').value="<%=acceptNo%>";
  fm.all('EndDate').value="<%=PubFun.getCurrentDate()%>";
}
 
function iniForm()
{
	try{			
		  initInpBox();
		  queryContInfo();			
			iniBalancePolGrid();
			easyQuery(); 
	}
	catch(re)
	{
	  alert("BqBalanceTimeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function iniBalancePolGrid()
 {
 	 var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="发生日期";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=30;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="批单号";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="保全项目";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][4]="EdorCode";
      iArray[3][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[3][9]="保全项目|code:EdorCode";
      iArray[3][18]=250;
      iArray[3][19]= 0 ;

      iArray[4]=new Array();
      iArray[4][0]="收退费金额";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      BalancePolGrid = new MulLineEnter( "fm" , "BalancePolGrid" ); 
      //这些属性必须在loadMulLine前
      BalancePolGrid.mulLineCount = 10;   
      BalancePolGrid.displayTitle = 1;
      BalancePolGrid.locked = 1;
      BalancePolGrid.canSel = 1;
      BalancePolGrid.hiddenPlus=1;   
    	BalancePolGrid.hiddenSubtraction=1;
      BalancePolGrid.loadMulLine(iArray);        
      
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }   
 }
</script>