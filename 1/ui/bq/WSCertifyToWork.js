//程序名称：
//程序功能：激活卡补激活并生成客户资料变更的工单
//创建日期：2010-08-12
//创建人 :  XP
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
var isCard = "0";
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryCertifyList()
{
    if(!verifyInput2())
    {
        return false;
    }
	if((fm.PrtNo.value==""||fm.PrtNo.value==null)&&
		(fm.tIDNo.value=="" || fm.tIDNo.value==null)&&
		(fm.CardNo.value=="" || fm.CardNo.value==null))
	{
		alert("请至少录入结算单号、保险卡号、证件号码其中的一项！");
		return false;
	}
    var tStrSql = ""
        + " select lict.CardNo, lict.PrtNo, licti.CValidate, licti.ActiveDate, "
        + " licti.Name, licti.IdNo, "
        + " CodeName('certifycontstate', lict.State) State, "
        + " (case lict.WSState when '00' then '未确认' when '01' then '已确认' end) WSState,(select edorno from lpedoritem lp where contno= licti.cardno and exists (select 1 from lpedorapp where edoracceptno=lp.edorno and edorstate<>'0'))"
        + " from LICertify lict "
        + " inner join LICardActiveInfoList licti on licti.CardNo = lict.CardNo "
        + " where 1=1"
        + getWherePart("lict.CValidate", "StartCValidate", ">=")
        + getWherePart("lict.CValidate", "EndCValidate", "<=")
        + getWherePart("lict.CardNo", "CardNo")
        + getWherePart("lict.PrtNo", "PrtNo")
        + getWherePart("licti.idno","tIDNo")
        + getWherePart("licti.name","CustomerName")
        + getWherePart("licti.idtype","tIDType")
        + " with ur "
        ;

    turnPage1.pageDivName = "divCertifyListGridPage";
    turnPage1.queryModal(tStrSql, CertifyListGrid);
    
    if (!turnPage1.strQueryResult)
    {
    	isCard = "1";
    	var tStrSqlnew = ""
            + " select lict.CardNo, lict.PrtNo, lict.CValidate, lict.ActiveDate, "
            + " licti.Name, licti.IdNo, "
            + " CodeName('certifycontstate', lict.State) State, "
            + " (case lict.WSState when '00' then '未确认' when '01' then '已确认' end) WSState,(select edorno from lpedoritem lp where contno= licti.cardno and exists (select 1 from lpedorapp where edoracceptno=lp.edorno and edorstate<>'0'))"
            + " from LICertify lict "
            + " inner join LICertifyInsured licti on licti.CardNo = lict.CardNo "
            + " where 1=1"
            + getWherePart("lict.CValidate", "StartCValidate", ">=")
            + getWherePart("lict.CValidate", "EndCValidate", "<=")
            + getWherePart("lict.CardNo", "CardNo")
            + getWherePart("lict.PrtNo", "PrtNo")
            + getWherePart("licti.idno","tIDNo")
            + getWherePart("licti.name","CustomerName")
            + getWherePart("licti.idtype","tIDType")
            + " with ur "
            ;
    	turnPage1.pageDivName = "divCertifyListGridPage";
        turnPage1.queryModal(tStrSqlnew, CertifyListGrid);
        if (!turnPage1.strQueryResult)
        {	        
        	alert("没有待处理的结算信息,请确认卡折信息是否已经录入系统！");
        	return false;
        }     
     }
    return true;
}

/**
 * 卡折实名化
 */
function dealWSCertify()
{
    var tRow = CertifyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    fm.BCardNo.value = CertifyListGrid.getRowColData(tRow,1);
    fm.BName.value = CertifyListGrid.getRowColData(tRow,5);
    fm.BIDNo.value = CertifyListGrid.getRowColData(tRow,6);
    var tEdorNo=CertifyListGrid.getRowColData(tRow,9);
    if(tEdorNo != "")
    {
        alert("该激活卡已经生成客户资料变更的工单，请查看。");
        return false;
    }
    
    var strSQL="select operatetype from lmcertifydes where subcode=(select cardtype from lzcardnumber where cardno='"+fm.BCardNo.value+"')";
    var arrResult=easyExecSql(strSQL);
    if(arrResult)
    {
    	if(arrResult[0][0]!='2'&&arrResult[0][0]!='0'&&arrResult[0][0]!='1')
    	{
    		alert("卡号"+fm.BCardNo.value+"不是激活卡或者撕单类型,程序暂不支持此类变更");
        	return false;
    	}
    }
    else
    {
    	alert("查询卡号"+fm.BCardNo.value+"定义信息失败");
    	return false;
    }

    if(isCard=="0")
    {
    	var strSQL1="select count(1) from licardactiveinfolist lict where 1=1 and not exists (select 1 from licertifyinsured "
    			+" where cardno = lict.cardno) and cardno =  '"+fm.BCardNo.value+"'";
    	var arrResult1=easyExecSql(strSQL1);
    	if(arrResult1)
    	{
    		if(arrResult1[0][0]!=0){
    			fm.all('Flag').value='1';
    		}
    		else
    		{
    			fm.all('Flag').value='0';
    		}
    	}
    }
    else if(isCard=="1")
    {
    	fm.all('Flag').value='0';
    }
    var strSQL2="select count(1) from llcase where customerno=(select insuredno from lccont where contno='"+fm.BCardNo.value+"') and rgtstate  in ('11','12','14') with ur";
    var arrResult2=easyExecSql(strSQL2);
    if(arrResult2)
    {
    	if(arrResult2[0][0]!=0){
    		if(!confirm("该卡对应被保人之前有过已经结案或者撤销的理赔,是否继续操作?")){
    		return false;
    		}
    	}
    }
    showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.btnWSCertify.disabled = true;
    fm.action = "WSCertifyToWorkSave.jsp";
    fm.submit();
    fm.action = "";
}

/**
 * 实名化提交后动作。
 */
function afterWSSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("确认失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
//        for(var rowNum=0;rowNum<CertifyListGrid.mulLineCount;rowNum++){
//        	if (fm.BCardNo.value==CertifyListGrid.getRowColData(rowNum,1)) {
//        		var SQL = "select edorno from lpedoritem where  contno='"+fm.BCardNo.value+"' and exists (select 1 from lpedorapp where edoracceptno=lp.edorno and edorstate<>'0')";
//        		arrResult = easyExecSql(SQL);
//        		
//        		CertifyListGrid.setRowColData(rowNum,8,"已确认");
//        		CertifyListGrid.setRowColData(rowNum,9,arrResult[0][0]);
//        		break;
//        	}
//        	
//        }
        queryCertifyList()
    }

    fm.btnWSCertify.disabled = false;
}

