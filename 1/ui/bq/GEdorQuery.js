//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量


//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initGEdorMainGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    //var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  //alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
 // alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

/**
function returnParent()
{
    tRow=GEdorMainGrid.getSelNo();
    tCol=1;
    tEdorNo = GEdorMainGrid.getRowColData(tRow-1,tCol);
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    top.location.href="./GEdorQueryDetail.jsp?EdorNo="+tEdorNo;
}
*/
// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GEdorMainGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = GEdorMainGrid.getSelNo();
	
	try
	{
		if (arrGrid==null)
		{
			alert("请先查询!");
			return null;
		}
	}
	catch(ex)
	{
			alert("请先查询!");
			return null;
	}
	
	try
	{
		if( tRow == 0 || tRow == null || arrGrid == null )
			return arrSelected;
		
			arrSelected = new Array();
	//		alert("here");
     //************dingzhong****************************
            arrSelected[0] = new Array();
            arrSelected[0][0] = GEdorMainGrid.getRowColData(tRow-1,1);			
            arrSelected[0][1] = GEdorMainGrid.getRowColData(tRow-1,2);
            arrSelected[0][2] = GEdorMainGrid.getRowColData(tRow-1,3);
            arrSelected[0][3] = GEdorMainGrid.getRowColData(tRow-1,4);
            arrSelected[0][4] = GEdorMainGrid.getRowColData(tRow-1,5);
            arrSelected[0][5] = GEdorMainGrid.getRowColData(tRow-1,6);
    //*******************************************       
    /* 
			//设置需要返回的数组
			arrSelected[0] = arrGrid[tRow-1];
	*/	
			return arrSelected;
	}
	catch (e)
	{}
}


// 查询按钮
function easyQueryClick()
{
	var tEdorState;
		
//	tEdorState=parent.fraInterface.fm.all('EdorState')
	tEdorState=top.opener.fm.all('EdorState').value;
	
	//alert("aaa"+tEdorState);
	// 初始化表格
	initGEdorMainGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var tReturn = parseManageComLimit();

	strSQL = "select EdorAcceptNo,EdorNo,GrpContNo,EdorState,EdorValiDate,EdorAppDate from LPGrpEdorMain where EdorState='"+ tEdorState+"'"+" and"+tReturn
				 + getWherePart( 'GrpContNo' )
				 + getWherePart( 'EdorNo' );
				 + "order by MakeDate desc,MakeTime desc"
			 
	//alert(strSQL);
	//查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	  
	  //判断是否查询成功
	  if (!turnPage.strQueryResult) 
	  {
	    alert("查询失败！");
	  }
	  else
	  {
	  	//查询成功则拆分字符串，返回二维数组
	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  
	  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	  turnPage.pageDisplayGrid = GEdorMainGrid;    
	          
	  //保存SQL语句
	  turnPage.strQuerySql     = strSQL; 
	  
	  //设置查询起始位置
	  turnPage.pageIndex       = 0;  
	  
	  //在查询结果数组中取出符合页面显示大小设置的数组
	  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	  arrGrid = arrDataSet;
	  //调用MULTILINE对象显示查询结果
	  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
		}
}
/*
function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGEdorMainGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		//alert(n);
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			//alert(i);
			for( j = 0; j < m; j++ )
			{
				//alert("j;"+j);
				GEdorMainGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}
*/
function detailEdor()
{
		var arrReturn = new Array();
		var tSel = GEdorMainGrid.getSelNo();
	
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击察看明细按钮。" );	
		else
		{
			if (top.opener.fm.all('EdorState').value =='2') 
			{
				arrReturn = getQueryResult();
				fm.all('EdorType').value =arrReturn[0][2];
				fm.all('EdorNo').value = arrReturn[0][0];
				fm.all('GrpPolNo').value=arrReturn[0][1];
				//top.close();
				detailEdorType();
			}
			else
			{
				alert("还没有申请确认！");
				top.close();
			}
		}
}
// 数据返回父窗口