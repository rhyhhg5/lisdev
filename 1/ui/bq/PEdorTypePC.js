var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var ifsy;
var sep;

function queryPolInfo()
{     
	  var sysql = "";
	  var psql = "";
	  var SQLTPHI =" select insuredno from lcpol where riskcode in (select riskcode from lmriskapp where taxoptimal='Y') and contno='"+fm.all('ContNo').value+"'";
	  var result1 = easyExecSql(SQLTPHI);
	  
	  var seprisk = "select 1 from lcpol where riskcode in (select code from LDCode where codeType = 'sepriskcode') and contno='"+fm.all('ContNo').value+"'";
	  var result2 = easyExecSql(seprisk);
	  //税优产品需要不同校验
	  if (result1 != null) 
	  { 
		  ifsy = true;
		  sep = false;
		  sysql =  " and exists (select * from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'M') ";		
	  }
	  else {
		  ifsy = false;
		  if(result2 != null){
			  sep = true;  
			  sysql = " and riskcode in (select code from LDCode where codeType = 'sepriskcode')  ";		
		  }
	//	  sysql =  " and exists (select * from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') "; 
	  }
  //先查P表再查C表
  var sql = " select PolNo, RiskSeqNo, RiskCode, (select RiskName from LMRisk where RiskCode = a.RiskCode), Prem, CValiDate, " +
            "       PayToDate, PayIntv " +
            " from LPPol a" +
            " where EdorNo = '" + fm.EdorNo.value + "' " +
            " and EdorType = '" + fm.EdorType.value + "' " +
            " and ContNo = '" + fm.ContNo.value + "' " +
            sysql +
            " union " +
            " select PolNo, RiskSeqNo, RiskCode, (select RiskName from LMRisk where RiskCode = a.RiskCode), Prem, CValiDate, " +
            "       PayToDate, PayIntv " +
            " from LCPol a " +
            " where ContNo = '" + fm.ContNo.value + "' " +
            " and PolNo not in " +
            "    (select PolNo from LPPol " +
            "     where EdorNo = '" + fm.EdorNo.value + "' " +
            "     and EdorType = '" + fm.EdorType.value + "' " +
            "     and ContNo = '" + fm.ContNo.value + "') " +
              sysql +
            " order by RiskSeqNo ";
  turnPage.pageLineNum = 100;
  turnPage.queryModal(sql, PolGrid);
}

function save()
{
  if(!checkData())
  {
    return false;
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}

function checkData()
{
  for(var i = 0; i < PolGrid.mulLineCount; i++)
  {
    var payIntv = PolGrid.getRowColData(i, 8);
    
    sql = "select 1 from LDCode where codeType = 'payintv' and Code= '" + payIntv + "' ";
    var rs = easyExecSql(sql);
    if(rs == null || rs[0][0] == "" || rs[0][0] == "null")
    {
      alert("缴费频次不能为" + payIntv);
      return false;
    }
    
    if(ifsy)
    {
      if(payIntv != 12){
      alert("税优产品的缴费频次只能变更为年缴");
      return false;  
      }
    }
    
    if(sep){
      if(payIntv != 12){
     alert("此产品的缴费频次只能变更为年缴");
     return false;  
     }   	
    }
  }
  
  return true;
}

function afterSubmit(flag, content)
{
	try 
	{ 
	  showInfo.close(); 
	  window.focus();
	} catch(ex) { }
	  
	if (flag == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
    returnParent();
	}
}


function returnParent()
{
	try
	{
		top.opener.getEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {}
}