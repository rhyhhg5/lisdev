//程序名称：BqBalanceTimeInput.js
//程序功能：实现保全结算定期结算
//创建日期：2006-03-13 11:10:36
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var statusno = '';
var edorstate = '';
function afterCodeSelect()
{
	//缴费方式变更
	var payMode = fm.all("PayMode").value;
	
	if ((payMode == "1") || (payMode == "2") || (payMode == "3")|| (payMode == "9")) //自行缴费
	{
		document.all("trBank").style.display="none";
		if(Number(fm.all('balprem').value)>0){
			fm.all("trPayEndDate").style.display="";
  		fm.all("trPayEndDate2").style.display="";
  	}
	}
	if (payMode == "4") //银行转帐
	{
		document.all("trBank").style.display = "";
		if(Number(fm.all('balprem').value)>0){  
			fm.all("trPayEndDate").style.display="none";
  		fm.all("trPayEndDate2").style.display="none";
  	}
	}
}

//根据工单号查询需要的信息
function queryContInfo()
{
  var sql = "select  contno,customerno,customername,statusno ,edorstate from lgwork ,lpedorapp where lgwork.workno = '"+fm.AcceptNo.value+"' and lgwork.workno = lpedorapp.edoracceptno ";
  arrayResult = easyExecSql(sql);
 	fm.all('GrpContNo').value= arrayResult[0][0];
  fm.all('AppntName').value= arrayResult[0][2];
  statusno = arrayResult[0][3];
	edorstate = arrayResult[0][4];  
  sql = "select  Balintv,BalToDate,BalTimes from lcgrpbalplan where grpcontno ='"+fm.all('GrpContNo').value+"'";
  arrayResult = easyExecSql(sql);
  fm.all('balintv').value = arrayResult[0][0];
  if(fm.all('balintv').value=='0'){
  fm.all('balintv').value = '随时';
  }else if(fm.all('balintv').value=='1'){
  fm.all('balintv').value = '月度';
  }else if(fm.all('balintv').value=='3'){
  fm.all('balintv').value = '季度';
  }else if(fm.all('balintv').value=='6'){
  fm.all('balintv').value = '半年度';
  }else if(fm.all('balintv').value=='12'){
  fm.all('balintv').value = '年度';
  }  
  fm.all('StartDate').value = arrayResult[0][1]; 
  fm.all('baltimes').value = arrayResult[0][2]; 
}

//查询本期结算期间的所有保全处理项目
function easyQuery()
{
	controlButton();
	var strSql = '';
	if(statusno != 5 && edorstate!= 9){
		fm.all('ballater').disabled = true;
		 strSql = "select b.EdorAppDate,b.edorno,b.EdorType,a.sumactupaymoney money from  ljapay a ,lpgrpedoritem b "+
			 "where a.incometype='B' and a.IncomeNo=b.edorno and b.grpcontno='"+fm.all('GrpContNo').value+"'"+
			 " union "+
			 "select b.EdorAppDate,b.edorno,b.EdorType,-a.sumgetmoney money from  ljaget a ,lpgrpedoritem b "+
			 "where a.OtherNo=b.edorno and a.PayMode='B' and b.grpcontno = '"+fm.all('GrpContNo').value+"'" ;
var strtemp = "select a.sumactupaymoney money from  ljapay a  "+
"where a.incometype='B' and  exists ( select 1 from lpgrpedoritem b " +
" where b.edorno=a.IncomeNo  and b.grpcontno='"+fm.all('GrpContNo').value+"')"+
" union all "+
"select -a.sumgetmoney from  ljaget a  "+
" where  a.PayMode='B'  and  exists ( select 1 from lpgrpedoritem b " +
" where b.edorno=a.OtherNo and b.grpcontno='"+fm.all('GrpContNo').value+"')" ;
turnPage.queryModal(strSql,BalancePolGrid);
if(BalancePolGrid.mulLineCount==0)
{
fm.all("trSelectTitleShow").style.display="none";
alert("本次结算期间没有发生任何保全收退费!");
fm.all('balprem').value = "0";
return;
}
strSql = "select sum(money) from ("+strtemp+") as y";
  var arrayResult = easyExecSql(strSql);
  if(arrayResult==null)
  {
  	alert("计算汇总金额出错!");
  	return;
  }
  fm.all('balprem').value = arrayResult[0][0];
  if(Number(fm.all('balprem').value)<= 0)
  {
  	fm.all("trGetTitleShow").style.display="";
  }
  else if(Number(fm.all('balprem').value)>0)
  {
  	fm.all("trPayTitleShow").style.display="";
  	fm.all("trPayEndDate").style.display="";
  	fm.all("trPayEndDate2").style.display="";
	}
//	queryAccount(fm.all('GrpContNo').value);
  }
	else{
		 fm.all('bal').disabled = true; 
		 if(edorstate==9){ 		
		 		fm.all("laterPayDate").style.display="";
		 		fm.all("trLater").style.display="";
		 		fm.all("trPayMode").style.display="none";
		 }
		 else{
		 		 fm.all('ballater').disabled = true;
		 }
		 var sql = "select ActuGetNo no,'1',BankCode,AccName,BankAccNo,StartGetDate,shoulddate from ljaget where OtherNo='" +
                fm.AcceptNo.value +
               "' and OtherNoType='13' "
               + " union "
               + "select GetNoticeNo no,'0',BankCode,AccName,BankAccNo,StartPayDate,paydate from ljspay where OtherNo='" +
             	fm.AcceptNo.value + "' and OtherNoType='13' order by no desc";
     	arrayResult = easyExecSql(sql);
     	if(arrayResult == null){
     		fm.all("trSelectTitleShow").style.display="none";
     		alert("本次结算期间没有发生任何保全收退费!");
     		fm.all('balprem').value = "0";
			}
  		else{
  			var tActuGetNo = arrayResult[0][0];
      	var tFlag = arrayResult[0][1]; //此字符可判断收退费通知书
      	fm.all('PayEndDate').value = arrayResult[0][6];
      	strSql = "select b.EdorAppDate,b.edorno,b.EdorType,b.getMoney Money from lpgrpedoritem b "
      	         + "where b.grpcontno='" + fm.all('GrpContNo').value +
                 "' and b.EdorAcceptNo in "
                 + "( select IncomeNo from ljapay where PayNo in( "
                 + "select BTActuNo from LJAEdorBalDetail where ActuNo='" +
                 tActuGetNo + "' and flag ='" + tFlag + "' and BTFlag='0'"
                 + ")  and IncomeType='J' union"
                 + " select OtherNo from LJAGet where ActuGetNo in("
                 + " select BTActuNo from LJAEdorBalDetail where ActuNo='" +
                 tActuGetNo + "' and flag ='" + tFlag + "' and BTFlag='1'"
                 + " )  and paymode='J')";
       turnPage.queryModal(strSql,BalancePolGrid);
       if(BalancePolGrid.mulLineCount==0){
  			fm.all("trSelectTitleShow").style.display="none";
  			alert("本次结算期间没有发生任何保全收退费!");
  			fm.all('balprem').value = "0";
  			return;
  		}
  		strSql = "select sum(money) from ("+strSql+") as y";
  		var arrayResult = easyExecSql(strSql);
 			if(arrayResult==null){
  		alert("计算汇总金额出错!");
  		return;
  		}
   		fm.all('balprem').value = arrayResult[0][0];
   		if(Number(fm.all('balprem').value)>0){
   				fm.all("trPayTitleShow").style.display="";
   				fm.all("trPayEndDate").style.display="";
  				fm.all("trPayEndDate2").style.display="";
   		}
   		else if(Number(fm.all('balprem').value)<=0){
   			fm.all("trGetTitleShow").style.display="";
   		}
  	}    
	} 
		queryAccount(fm.all('GrpContNo').value);  
}

//得到交费银行和帐号
function queryAccount(grpContNo)
{
	var strSql = "select a.BankCode, a.BankAccno, a.AccName " +
				 "from LCGrpCont a " +
				 "where a.GrpContNo = '" + grpContNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.all("Bank").value = arrResult[0][0];
		fm.all("BankName").value = arrResult[0][0];
		fm.all("BankAccno").value = arrResult[0][1];
		fm.all("AccName").value = arrResult[0][2];
		showOneCodeName("Bank", "BankName");  
	}
}

//结算
function doBalance()
{
	if(!beforeSubmit())
	{
		return false;
	}
	submitForm();
		
}

function beforeSubmit()
{
// 已经产生的工单在保单失效，满期时也可以操作

//  if(!grpContValidate())
//  {
//    return false;
//  }
  
	//缴费方式变更
	var payMode = fm.all("PayMode").value;
	if (payMode == "4") //银行转帐
	{
		if(fm.Bank.value=="")
		{
			alert("转帐银行不能为空");
			return false;
		}
		if(fm.BankAccno.value=="")
		{
			alert("转帐帐号不能为空");
			return false;
		}
		if(fm.PayDate.value=="")
		{
			alert("转帐日期不能为空");
		//	PayDate.focus();
			return false;
		}
	}
else{
	if(fm.PayEndDate.value==""&&Number(fm.all('balprem').value)>0)
	{
		alert("交费截止日期不能为空");
	//PayEndDate.focus();
	return false;
	}
}
	return true;
}

//校验保单是否有效
function grpContValidate()
{
  var sql = "select StateFlag "
            + "from LCGrpCont "
            + "where GrpContNo = '" + fm.GrpContNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    if(rs != null && rs[0][0] != "" && rs[0][0] != "null" 
      && rs[0][0] != "1")
    {
      sql = "select CodeName from LDCode "
            + "where CodeType = 'stateflag' and code = '" + rs[0][0] + "'";
      rs = easyExecSql(sql);
      if(rs)
      {
        alert("保单处于" + rs[0][0] + "状态，不能结算");
        return false;
      }
    }
    return true;
  }
  else
  {
    return false;
  }
}

function submitForm()
{
		
  	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm.submit();			
}

//延期处理
function balancelater()
{
  if(!grpContValidate())
  {
    return false;
  }
  
	if(!checklaterPayDate()){
		return false;
	}
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "../bq/BqBalanceLaterSave.jsp";
	fm.submit();
}

//打印定期结算单
function PrintTimeBal()
{
  var sql = "select state from lcgrpbalplan where grpcontno = '"+fm.all('GrpContNo').value+"'";
  arrResult = easyExecSql(sql);
	if(arrResult[0][0] == 1){
		alert("请先结算！");	
		return false ;
	}
	if(!beforeSubmit())
	{
		return false;
	}
	else{
		var newWindow = window.open("../f1print/bqBalTimeLis.jsp?StartDate="+fm.StartDate.value+"&EndDate="+fm.EndDate.value+"&GrpContNo="+fm.GrpContNo.value+"&WorkNo="+fm.AcceptNo.value+"&PayMode="+fm.PayMode.value+"&PayEndDate="+fm.PayEndDate.value+"&Bank="+fm.Bank.value+"&BankAccno="+fm.BankAccno.value+"&AccName="+fm.AccName.value+"&PayDate="+fm.PayDate.value);	
	}
}



/* 保存完成后的操作，由子窗口调用 */
function afterSubmit(FlagStr, content, workNo)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	//iniForm();
} 

function checklaterPayDate(){
	if(fm.laterPayDate.value ==null||fm.laterPayDate.value==""){
		alert("请选择延期结算的日期!");
		return false;
	}
	//限制延期结算日期的添加--20110803--[ytz][OoO]
	var ExeSql="select cinvalidate from lcgrpcont where grpcontno = '"+ fm.all('GrpContNo').value +"'";
    var result_201100803 = easyExecSql(ExeSql);  
     
     if(result_201100803 != null && result_201100803 != "" && result_201100803 != "null")
     {
       var laterPayDate_0803=fm.all('laterPayDate').value;
       
       var dt1_c = new Date(result_201100803[0][0].replace(/-/g,"\/")).getTime();
       
       var dt2_c = new Date(laterPayDate_0803.replace(/-/g,"\/")).getTime();
     
        if(dt2_c >= dt1_c)
        {	
          alert("延期交费日期大于保单的终止日期，请您重新选择延期缴费的时间");
			return false;
	    }
     }
     else 
     {
        alert("保单的终止日期为空！");
     	return false;
     }
     return true;
}

function controlButton()
{
  if(tLoadFlag == "PERSONALBOXVIEW")
  {
    hiddenButton();
  }
}

function hiddenButton()
{
  fm.all("bal").style.display = "none";
//  fm.all("printballis").style.display = "none";
  fm.all("ballater").style.display = "none";
}
