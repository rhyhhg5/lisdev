<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="PEdorUWAppCancel.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PEdorUWAppCancelInit.jsp"%>
  <title>保全查询 </title>
</head>
<body  onload="initForm();">
  <form action="./PEdorUWAppCancelSubmit.jsp" method=post name=fm target="fraSubmit" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入个人保全查询条件：
			</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>  批单号码 </TD>
          <TD  class= input>  <Input class= common name=EdorNo >   </TD>
          <TD  class= title> 个人保单号码 </TD>
          <TD  class= input>  <Input class= common name=PolNo >  </TD>
          <TD  class= title> 批改类型 </TD>
          <TD  class= input> <Input class= "code" name=EdorType  ondblclick="return showCodeList('EdorType',[this]);" onkeyup="return showCodeListKey('EdorType',[this]);"> </TD>  
         </TR>
        <TR  class= common>        
          <TD  class= title> 险种编码 </TD>
          <TD  class= input> <Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);"> </TD>
          <TD  class= title>  被保人客户号码 </TD>
          <TD  class= input>  <Input class=common name=InsuredNo >   </TD>
          <TD  class= title>  申请日期 </TD>
          <TD  class= input>  <Input class= "coolDatePicker" dateFormat="short" name=EdorAppDate >   </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
        </TR>
    </table>
          <table class= common align=center>
          <INPUT class = common VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 保全信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		</table>
  	<table  class= common>	
  	  <input type=hidden name=Transact >
      <INPUT class = common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class = common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class = common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class = common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">	  			
  	</table>       			
  	</div>
  	 	<table class= common align=center>
    		<tr class= common>
    			<td  class= input>
    				<INPUT class= common VALUE="撤销申请" TYPE=button onclick="CancelEdor();">     			
    			</td>
    			<td>
    			</td>
    		</tr>
    	</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
