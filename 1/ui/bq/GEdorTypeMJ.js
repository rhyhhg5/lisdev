//程序名称：GEdorTypeMJ.js
//程序功能：工单管理个人信箱页面
//创建日期：2005-01-17 11:03:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();
var prtCode="";

//查询团险信息
function queryLCGrpPol()
{
  var sql = "  select a.grpPolNo, a.riskCode, b.riskName "
            + "from LCGrpPol a, LMRisk b "
            + "where a.riskCode = b.riskCode "
            + "   and grpContNo = '" + grpContNo + "' ";
  turnPage.pageLineNum = 10;
	turnPage.queryModal(sql, LCGrpPolGrid);
}

//初始化录入框信息
function initInputBox()
{
  var grpPolNo = LCGrpPolGrid.getRowColDataByName(LCGrpPolGrid.getSelNo() - 1, "grpPolNo");
  setCommonInfo(grpPolNo);
  setAccInfo(grpPolNo);
  connectToClaim(grpPolNo);
  getGrpAccFoundDate(grpPolNo);
  
  //setAccBala();
}

/*
function setAccBala()
{
  
  alert("已放到满期理算中，可删除本汉书");
  return false;
  var temp = fm.action;
  
  fm.action = "GEdorTypeMJAccBalaSave.jsp";
  fm.submit();
  fm.action = temp;
}
*/

//计算账户余额
function setAccInfo(grpPolNo)
{
  fm.rateType.value = getEspecialData("ENDTIME_RATETYPE");
  
  var state = getEspecialData("MJSTATE");
  if(state == null || state == "1")
  {
    return true;
  }
  
  var interestGrp;
  var interestInsured;
  
  //得到人账户总金额及账户数目
//  var sql = "select sum(getMoney - money) from LPDiskImport a, LPPol b "
//      + "where COALESCE(a.grpContNo, a.grpContNo) = b.grpContNo "
//      + "   and a.edorNo = b.edorNo "
//      + "   and a.edorType = COALESCE(b.edorType, b.edorType) "
//      + "   and a.insuredNo = b.insuredNo "
//      + "   and b.grpContNo = '" + fm.GrpContNo.value + "' "
//      + "   and b.edorNo = '" + fm.EdorNo.value + "' "
//      + "   and b.polTypeFlag = '0' ";
  
  var sql1 = "select distinct insuredNo from LPPol  " +
  		" where grpContNo = '" +  fm.GrpContNo.value  + "'   and edorno = '" +fm.EdorNo.value+ "' " +
  		" and polTypeFlag = '2'" +
  		" fetch first 1 rows only" +
  		" with ur";
  var insuredno = easyExecSql(sql1);
  
  var sql = "select sum(a.getMoney - a.money) from LPDiskImport a  where a.edorno='" +fm.EdorNo.value+ "' "
	  + " and a.grpcontno = '" +  fm.GrpContNo.value  + "' ";
  if(insuredno && insuredno[0][0]!="null"){
	  sql = sql+" and a.insuredNo != '"+insuredno[0][0]+"' ";
  }
  
    var intInsured = easyExecSql(sql);
    if(intInsured && intInsured[0][0] != "null")
    {
      interestInsured = intInsured[0][0];
    }
    else
    {
      interestInsured = 0;
    }
    
    //公共账户
    var sql = "select (getMoney - money) from LPDiskImport a, LPPol b "
      + "where COALESCE(a.grpContNo, a.grpContNo) = b.grpContNo "
      + "   and a.insuredNo = b.insuredNo "
      + "   and b.grpContNo = '" + fm.GrpContNo.value + "' "
      + "   and b.edorNo = '" + fm.EdorNo.value + "' "
      + "   and b.polTypeFlag = '2' order by a.makedate desc ";
    var intGrp = easyExecSql(sql);
    if(intGrp)
    {
      interestGrp = intGrp[0][0];
    }
    else
    {
      interestGrp = 0;
    }
    fm.grpInterest.value = interestGrp;
    fm.insuredInterest.value = interestInsured;
    fm.rateType.value = getEspecialData("ENDTIME_RATETYPE");
}

function setCommonInfo(grpPolNo)
{
  fm.EdorNo.value = edorNo;
  fm.EdorType.value = edorType;
  fm.grpPolNo.value = grpPolNo;
  fm.GrpContNo.value = grpContNo;
  fm.GrpContNo2.value = grpContNo;
  
  //若未结案或满期终止,则从LCGrpCont表取,否则从本对应的B表取本次备份的LBGrpCont
  var tEndTimeDeal = getEspecialData("ENDTIME_DEAL");
  if(tEndTimeDeal == null || tEndTimeDeal == "" 
      || tEndTimeDeal == "null" || tEndTimeDeal == "2")
  {
    var sql = "  select * "
        + "from LCGrpCont "
        + "where grpContNo = '" + grpContNo + "' ";
    var result = easyExecSql(sql);
    if(result)
    {
      fm.grpName.value = result[0][15];
      fm.polApplyDate.value = result[0][108];
      fm.cInValiDate.value = result[0][119];
      fm.remark.value = result[0][64];
    }
  }
  else
  {
    var sql = "select * from LBGrpCont "
              + "where EdorNo = 'xb" + fm.EdorNo.value + "' ";
    var result = easyExecSql(sql);
    if(result)
    {
      fm.grpName.value = result[0][16];
      fm.polApplyDate.value = result[0][109];
      fm.cInValiDate.value = result[0][120];
      fm.remark.value = result[0][65];
    }
  }
  
  setCalculatedData();
}

//得到险种的帐户号码
function getInsuAccNo(accType)
{
  
  var sql = "  select a.InsuAccNo "
            + "from LMRiskToAcc a, LMRiskInsuAcc b "
            + "where a.InsuAccNo = b.InsuAccNo "
	          + "   and b.AccType = '" + accType + "' "
	          + "   and RiskCode = '" 
	          + LCGrpPolGrid.getRowColDataByName(LCGrpPolGrid.getSelNo() - 1, "riskCode") + "' ";
	var rs = easyExecSql(sql);
	if(rs)
	{
	  return rs[0][0];
	}
	
	return null;
}

//显示已计算的数据
function setCalculatedData()
{
  var accBalaGrp;
  var accBalaInsured;
  
  //为满期理算，从LCInsureAcc中得到余额
  var state = getEspecialData("MJSTATE");
  if(state == null || state == "1")
  {
    //个人账户余额--------
    
    //个人理赔账户编码
    var indiInsuAccNo = getInsuAccNo("002");
    
    sql = "select sum(InsuAccBala), count(1) from LCInsureAcc a, LCPol b "
        + "where a.polNo = b.polNo and b.grpPolNo ='" + fm.grpPolNo.value + "' "
        + "   and b.poltypeFlag = '0' "
        + "   and a.InsuAccNo = '" + indiInsuAccNo + "' ";
    result = easyExecSql(sql);
    if(result && result[0][0] && result[0][0] != "null")
    {
      fm.totalInsuredAccMoney.value = result[0][0];
      fm.insuredAccCount.value = result[0][1];
    }
    else
    {
      fm.totalInsuredAccMoney.value = 0;
      fm.insuredAccCount.value = 0;
    }
    
    //公共账户-------
    sql = "select sum(InsuAccBala) from LCInsureAcc a, LCPol b "
        + "where a.polNo = b.polNo and b.grpPolNo ='" + fm.grpPolNo.value + "' "
        + "   and b.poltypeFlag = '2' ";
    var result = easyExecSql(sql);
    if(result && result[0][0] != "null")
    {
      fm.grpAccMoney.value = result[0][0];
    }
    else
    {
      fm.grpAccMoney.value = 0;
    }
    fm.totalAccMoney.value = parseFloat(fm.grpAccMoney.value) 
        + parseFloat(fm.totalInsuredAccMoney.value);
  }
  
  else
  {
  
    //已理算过，LPDiskImport取得
    //公共账户-----------
    var sql = "select money from LPDiskImport a, LPPol b "
      + "where COALESCE(a.grpContNo, a.grpContNo) = b.grpContNo "
      + "   and a.insuredNo = b.insuredNo "
      + "   and b.grpContNo = '" + fm.GrpContNo.value + "' "
      + "   and b.edorNo = '" + fm.EdorNo.value + "' "
      + "   and b.polTypeFlag = '2' order by a.makedate desc";
    var result = easyExecSql(sql);
    if(result)
    {
      accBalaGrp = result[0][0];
    }
    else
    {
      accBalaGrp = 0;
    }
    
    //总账户总金额---------------
    var allMonry;
    var sql = "select sum(money), count(1) from LPDiskImport "
      + "where grpContNo = '" + fm.GrpContNo.value + "' "
      + "   and edorType = '" + fm.EdorType.value + "' "
      + "   and edorNo = '" + fm.EdorNo.value + "' ";
    var result = easyExecSql(sql);
    if(result && result[0][0] != "null")
    { 
      allMonry = result[0][0];
      fm.insuredAccCount.value = parseInt(result[0][1]) - 1;
    }
    else
    {
      allMonry = 0;
      fm.insuredAccCount.value = 0;
    }
    
    fm.totalAccMoney.value = allMonry;
    fm.grpAccMoney.value = accBalaGrp;
    fm.totalInsuredAccMoney.value = parseFloat(allMonry) - parseFloat(accBalaGrp);
    
  }
  
  fm.rateType.value = getAEspecialInfo("ENDTIME_RATETYPE");
  if(fm.rateType.value == 1)
  {
    fm.rateTypename.value = "录入利息";
  }
  else if(fm.rateType.value == 2)
  {
    fm.rateTypename.value = "录入利率";
  }
  else if(fm.rateType.value == 3)
  {
    fm.rateTypename.value = "默认利率";
  }
  else
  {
    fm.rateTypename.value = "";
  }
  fm.accRate.value = getAEspecialInfo("ACCRATE");
  fm.dealType.value = getAEspecialInfo("ENDTIME_DEAL");
  if(fm.dealType.value == "1")
  {
    fm.dealTypeName.value = "续保";
  }
  else if(fm.dealType.value == "2")
  {
    fm.dealTypeName.value = "满期终止";
  }
  
  //利息利率录入方式
  sql = "select CodeName "
        + "from LDCode "
        + "where CodeType = 'mjrateinterest' "
        + "   and Code = '" + fm.rateType.value + "' ";
  result = easyExecSql(sql);
  if(result && result[0][0] != "" && result[0][0] != "null")
  {
    fm.rateTypename.value = result[0][0];
  }
}


function afterCodeSelect(cCodeName, Field)
{
	if (cCodeName == "mjrateinterest")
	{
	  //系统录入的值
	  fm.accRate.value = getAEspecialInfo("ACCRATE");
	  var tSql = "select EdorValue from LPEdorEspecialData "
	             + "where EdorNo = '" + fm.EdorNo.value + "' "
	             + "    and EdorType = '" + fm.EdorType.value + "' "
	             + "    and PolNo = '" + fm.grpPolNo.value + "' "
	             + "    and DetailType = 'ENDTIME_RATETYPE' "
	             + "    and EdorValue = '" + fm.rateType.value + "' ";
    var result = easyExecSql(tSql);
    if(result)
    {
      fm.accRate.value = getEspecialData("ACCRATE");
    }
    else
    {
      tSql = "select EdorValue from LPEdorEspecialData "
	             + "where EdorNo = '" + fm.EdorNo.value + "' "
	             + "    and EdorType = '" + fm.EdorType.value + "' "
	             + "    and PolNo = '000000' "
	             + "    and DetailType = 'ENDTIME_RATETYPE' "
	             + "    and EdorValue = '" + fm.rateType.value + "' ";
      result = easyExecSql(tSql);
      if(result)
      {
        fm.accRate.value = getEspecialData("ACCRATE");
      }
      else
      {
        fm.accRate.value = "";
      }
    }

	  //若没有存储，则取系统默认的满期结算利率
	  if(fm.accRate.value == null || fm.accRate.value == "")
	  {
	    //默认利率
	    if(fm.rateType.value == "3")
	    {
	      var sql = "  select DefaultRate "
	                + "from LCGrpInterest "
	                + "where GrpPolNo = '" + fm.grpPolNo.value + "' ";
	      var rs = easyExecSql(sql);
	      if(rs)
	      {
	        fm.accRate.value = rs[0][0];
	      }
	    }
	  }
	}
}

//控制按钮的显示
function controlButton()
{
  if(loadFlag == "PERSONALBOXVIEW")
  {
    hiddenButton();
  //隐藏所有可能影响数据库的操作按钮
  try{
  	fm.all("ReturnButton").style.display = "none";
  }catch(e){};
  }
  
  var sql = "  select edorValue, "
            + "   case edorValue "
            + "     when '1' then '未处理' "
            + "     when '2' then '满期理算完毕' "
            + "     when '3' then '等待客户反馈' "
            + "     when '0' then '已结案' "
            + "   end "
            + "from LPEdorEspecialData "
            + "where edorNo = '" + edorNo + "' "
            + "   and edorType = 'MJ' "
            + "   and detailType = 'MJSTATE'";
  var result = easyExecSql(sql);
  if(result)
  {
    if(result[0][0] == "0")
    {
      hiddenButton();
    }
    fm.MJState.value = result[0][1];
  }
  
  var sqlState = "select state ,case state when '03030001' then '正在满期结算' " 
               + " when '03030002' then '满期终止' when '00060001' then '续保有效' end"
               + " from lcgrpcont where grpcontno = '"
               + grpContNo + "'";
  var result2 = easyExecSql(sqlState);
  if(result2)
  {
  	fm.grpContState.value = result2[0][1];
  }               
}

function hiddenButton()
{
  fm.all("calButton").style.display = "none";
  fm.all("dealButton").style.display = "none";
  var sql = "  select edorValue "
            + "from LPEdorEspecialData "
            + "where edorNo = '" + edorNo + "' "
            + "   and edorType = 'MJ' "
            + "   and detailType = 'ENDTIME_DEAL'";
  var result = easyExecSql(sql);
  if(result)
  {
  	if(result[0][0] == "1")
  	{
  		fm.all("MJDealButton").style.display = "none";
  	}
  	else if(result[0][0] == "2")
  	{
  		fm.all("XBDealButton").style.display = "none";
  	}
  	
  	fm.all("ReturnButton").style.display = "none";
  }
  
}

//得到
function getAEspecialInfo(detailType)
{
  var especialData = getEspecialData(detailType);
  if(especialData == null)
  {
    //此处查询新契约录入的利率，若未录入则返回null
    //..........
    
    return "";
  }
  
  return especialData[0][0];
}

//查询利率，利息，满期处理，状态等信息
function getEspecialData(detailType)
{
  var sql = "  select edorValue "
            + "from LPEdorEspecialData "
            + "where edorNo = '" + fm.EdorNo.value + "' "
            + "   and edorType = '" + fm.EdorType.value + "' "
            + "   and polNo = '" + fm.grpPolNo.value + "' "
            + "   and detailType = '" + detailType + "' ";
  var result = easyExecSql(sql);
  
  if(result == null || result == "" || result == "null")
  {
    sql = "  select edorValue "
          + "from LPEdorEspecialData "
          + "where edorNo = '" + fm.EdorNo.value + "' "
          + "   and edorType = '" + fm.EdorType.value + "' "
          + "   and polNo = '" + fm.grpPolNo.value + "' "
          + "   and detailType = '" + detailType + "' ";
    result = easyExecSql(sql);
    if(result)
    {
      return result;
    }
    else
    {
      sql = "  select edorValue "
          + "from LPEdorEspecialData "
          + "where edorNo = '" + fm.EdorNo.value + "' "
          + "   and edorType = '" + fm.EdorType.value + "' "
          + "   and polNo = '000000' "
          + "   and detailType = '" + detailType + "' ";
      result = easyExecSql(sql);
      
      return result;
    }
  }
  
  return result;
}

//得到保单的理赔状态
function connectToClaim(grpPolNo)
{
  var temp = fm.action;
  fm.action = "GEdorMJConnectToClaim.jsp?calFlag=grpPolState&grpPolNo=" + grpPolNo;
  fm.submit();
  
  fm.action = temp;
}

function afterConnectToClaim(claimState)
{
  fm.claimState.value = claimState;
  if(claimState == "1")
  {
    fm.claimStateName.value = "正理赔";
  }
  else
  {
    fm.claimStateName.value = "无理赔";
  }
}

//满期结算
function endDateCalculate()
{
  if(!isNumeric(fm.accRate.value))
  {
    alert("利息/利率录入有误");  
    return false;
  }
  if(fm.rateTypename.value==null||fm.rateTypename.value==""){
	  alert("请选择利息/利率的类型再进行操作");
      return false;
  }
  //默认利率
  if(fm.rateType.value == "3")
  {
    var sql = "  select DefaultRate "
              + "from LCGrpInterest "
              + "where GrpPolNo = '" + fm.grpPolNo.value + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      if(fm.accRate.value != rs[0][0])
      {
        alert("您选择了默认利率，不能修改默认利率值");
        return false;
      }
    }
  }
  
  showSavaInfoWindow();
  
  fm.action = "GEdorCalInterest.jsp";
  fm.submit();
}

//保存数据提示框
function showSavaInfoWindow()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
}

//后台计算完利息后负责在页面显示
function afterCalculate(flag, content, interestGrp, interestInsured)
{
  showInfo.close();
  window.focus();
  
  if (flag == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
    controlButton();
    setAccInfo(fm.grpPolNo.value);
  }
}

//满期处理
function endDateDeal()
{
  //20081120 zhanggm 团体账户利息金额为负数时，结算后生成错误的财务数据
  if(fm.grpInterest.value != null && fm.grpInterest.value !="" && fm.grpInterest.value !="null")
  {
    var grpInterest = parseFloat(fm.grpInterest.value);
    if(grpInterest<0)
    {
      alert("团体账户利息金额为负数，可能是数据错误，请联系总公司运维人员查找错误原因或进行数据维护。");
      return false;
    }
  }
  //---------------------------------------------
  var tMJState = getEspecialData("MJSTATE");
  if(tMJState == null || tMJState == "1" || tMJState == "2")
  {
    alert("您不能进行满期处理，请确认已打印满期结算单。");
    return false;
  }
  
  if(fm.dealType.value == "")
  {
    alert("请录入满期处理操作。");
    return false;
  }
  
  //续保
  if(fm.dealType.value == "1")
  {
    var temp = fm.action;
    fm.action = "GEdorTypeMJSubmit.jsp?" 
      + "Flag=MJOption&EdorAcceptNo=" + fm.EdorNo.value 
      + "&PayMode=1"
      + "&DealType=" + fm.dealType.value;
    showSavaInfoWindow();
    fm.submit();
    fm.action = temp;
  }
  else
  { 
    //总余额
    var sumGetMoney = parseFloat(fm.grpAccMoney.value) + parseFloat(fm.grpInterest.value)
      + parseFloat(fm.totalInsuredAccMoney.value) + parseFloat(fm.insuredInterest.value);
  
    win = window.open("GEdorTypeMJOptionMain.jsp?Flag=MJOption&EdorNo=" + fm.EdorNo.value 
      + "&edorType=" + fm.EdorType.value + "&sumGetMoney=" + sumGetMoney 
      + "&DealType=" + fm.dealType.value, 
        "MJOption",
        "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width=300,height=200");
    win.focus();
  }
}

//打印满期结算单
function printCalList()
{
  var tMJState = getEspecialData("MJSTATE");
  if(tMJState == null || tMJState == "1")
  {
    alert("您不能打印满期结算单，请确认是否已满期理算。");
    return false;
  }
  win = window.open("GEdorTypeMJListMain.jsp?prtType=cal&loadFlag=calculate&edorAcceptNo=" + fm.EdorNo.value 
      + "&edorType=" + fm.EdorType.value, "printList");
  win.focus();
}

function printCalListPdf()
{
	prtCode = "96"
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '"+prtCode+"' and standbyflag2='"+fm.EdorNo.value+"'");
//	alert(PrtSeq);
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		fm.action="GEdorTypeMJCalList.jsp?prtType=cal&edorAcceptNo=" + fm.EdorNo.value + "&edorType=" + fm.EdorType.value;
		fm.submit();
	}
	else
	{
		printPDF();
	}

}

function printPDF()
{
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '"+prtCode+"' and standbyflag2='"+fm.EdorNo.value+"'");
	//window.open("../uw/PrintPDFSave.jsp?Code=096&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq );	
	fm.action = "../uw/PrintPDFSave.jsp?Code=0"+prtCode+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq;
	fm.submit();
}

/*
//打印续期批单
function printKeepList()
{
  var tMJState = getEspecialData("MJSTATE");
  if(tMJState != "0")
  {
    alert("您不能打印满期终止结算单，请确认是否已满期处理。");
    return false;
  }
  
  return true;
}
*/

//打印续期终止结算单
function printEndDateList(prtType)
{
  var tMJState = getEspecialData("MJSTATE");
  if(tMJState != "0")
  {
    alert("您不能进行本操作，请确认是否已满期处理。");
    return false;
  }
  
  var tEndTimeDeal = getEspecialData("ENDTIME_DEAL");
  if(tEndTimeDeal == null || tEndTimeDeal == "" || tEndTimeDeal == "null")
  {
    alert("没有查询到满期处理方式");
    return false;
  }
  if(prtType == "XBDeal" && tEndTimeDeal != "1")
  {
    alert("满期处理方式不是续保，不能执行本操作");
    return false;
  }
  if(prtType == "MJDeal" && tEndTimeDeal != "2")
  {
    alert("满期处理方式不是满期终止，不能执行本操作");
    return false;
  }
  
  window.open("GEdorTypeMJCalList.jsp?edorType=MJ&prtType=" + prtType
      + "&edorAcceptNo=" + fm.EdorNo.value, "MJPrint");
}

function printEndDateListPdf(prtType)
{
  var tMJState = getEspecialData("MJSTATE");
  if(tMJState != "0")
  {
    alert("您不能进行本操作，请确认是否已满期处理。");
    return false;
  }
  
  var tEndTimeDeal = getEspecialData("ENDTIME_DEAL");
  if(tEndTimeDeal == null || tEndTimeDeal == "" || tEndTimeDeal == "null")
  {
    alert("没有查询到满期处理方式");
    return false;
  }
  if(prtType == "XBDeal" && tEndTimeDeal != "1")
  {
    alert("满期处理方式不是续保，不能执行本操作");
    return false;
  }
  if(prtType == "MJDeal" && tEndTimeDeal != "2")
  {
    alert("满期处理方式不是满期终止，不能执行本操作");
    return false;
  }
  if(prtType == "MJDeal")
  {
  	prtCode="95"
	}
    if(prtType == "XBDeal")
  {
  	prtCode="94"
	}
  
  	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '"+prtCode+"' and standbyflag2='"+fm.EdorNo.value+"'");
	//alert(PrtSeq);
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		fm.action="GEdorTypeMJCalList.jsp?edorType=MJ&prtType=" + prtType + "&edorAcceptNo=" + fm.EdorNo.value;
		fm.submit();
	}
	else
	{
		printPDF();
	}
  
	//window.open("GEdorTypeMJCalList.jsp?edorType=MJ&prtType=" + prtType + "&edorAcceptNo=" + fm.EdorNo.value, "MJPrint");
}



//满期处理后的提示
function AfterEndTimeDeal(flag, content)
{
  showInfo.close();
  window.focus();
  
  if (flag == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//控制特需险种的显示
function showLCGrpPolInfo()
{
  var row = LCGrpPolGrid.getSelNo() - 1;
  
  if(isEspecialPol(LCGrpPolGrid.getRowColDataByName(row, "riskCode")))
  {
    fm.all("divLCGrpContInfo").style.display = "";
    
	  initInputBox();
  }
  else
  {
    fm.all("divLCGrpContInfo").style.display = "none";
  }
}


//判断险种是不是特需险种
function isEspecialPol(riskCode)
{
	//170101 不是特需 qulq 2007-3-26 客户就是有理
  var sql = "  select riskType3 "
            + "from LMRiskApp "
            + "where riskCode = '" + riskCode + "' "
            + "   and riskType3 = '7'  and riskCode != '170101' ";
  var result = easyExecSql(sql);
  if(result)
  {
    return true;
  }
  return false;  
}

function printInsuredList()
{
  var tMJState = getEspecialData("MJSTATE");
  if(tMJState == null || tMJState == "1")
  {
    alert("请满期理算后再查看被保人清单。");
    return false;
  }
  
  win = window.open("GInsuredListMJMain.jsp?edorAcceptNo=" + fm.EdorNo.value 
      + "&edorType=" + fm.EdorType.value, "printList");
  win.focus();
}

function grpSpecQuery()
{
								
	window.open("../sys/GrpSpecialAccQuery.jsp?ContNo=" + fm.GrpContNo.value 
	  + "&ContType=1&RiskCode=1605&GrpPolNo=" + fm.grpPolNo.value, "accInfo");
}

//回退到满期理算前状态
function returnCal()
{
  if(!confirm("理算回退后原理算数据将会消失，你真的要回退么？"))
  {
    return false;
  }
  showSavaInfoWindow();
  
  fm.action = "GEdorEdorMJReturnCal.jsp";
  fm.submit();
}

//满期处理后的提示
function AfterReturnCal(flag, content)
{
  showInfo.close();
  window.focus();
  
  if (flag == "Fail")
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		location.reload();
  }
}

function printCalListPdfNew()
{
	prtCode = "96"
//	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '"+prtCode+"' and standbyflag2='"+fm.EdorNo.value+"'");
//	alert(PrtSeq);
//	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
//	{
//		fm.action="GEdorTypeMJCalList.jsp?prtType=cal&edorAcceptNo=" + fm.EdorNo.value + "&edorType=" + fm.EdorType.value;
//		fm.submit();
//	}
//	else
//	{
//		printPDF();
//	}
		var showStr="正在准备打印数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.target = "fraSubmit";
		fm.action = "../uw/PDFPrintSave.jsp?Code=96&StandbyFlag2=" + fm.EdorNo.value + "&StandbyFlag1=" + fm.EdorType.value +"&StandbyFlag4=cal";
		fm.submit();
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

function printEndDateListPdfNew(prtType)
{
  var tMJState = getEspecialData("MJSTATE");
  if(tMJState != "0")
  {
    alert("您不能进行本操作，请确认是否已满期处理。");
    return false;
  }
  
  var tEndTimeDeal = getEspecialData("ENDTIME_DEAL");
  if(tEndTimeDeal == null || tEndTimeDeal == "" || tEndTimeDeal == "null")
  {
    alert("没有查询到满期处理方式");
    return false;
  }
  if(prtType == "XBDeal" && tEndTimeDeal != "1")
  {
    alert("满期处理方式不是续保，不能执行本操作");
    return false;
  }
  if(prtType == "MJDeal" && tEndTimeDeal != "2")
  {
    alert("满期处理方式不是满期终止，不能执行本操作");
    return false;
  }
  if(prtType == "MJDeal")
  {
  	prtCode="95"
	}
    if(prtType == "XBDeal")
  {
  	prtCode="94"
	}
  
  //	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '"+prtCode+"' and standbyflag2='"+fm.EdorNo.value+"'");
	//alert(PrtSeq);
	//if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	//{
	//	fm.action="GEdorTypeMJCalList.jsp?edorType=MJ&prtType=" + prtType + "&edorAcceptNo=" + fm.EdorNo.value;
	//	fm.submit();
	//}
	//else
	//{
	//	printPDF();
	//}
  
	//window.open("GEdorTypeMJCalList.jsp?edorType=MJ&prtType=" + prtType + "&edorAcceptNo=" + fm.EdorNo.value, "MJPrint");
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
  fm.action = "../uw/PDFPrintSave.jsp?Code="+prtCode+"&StandbyFlag2=" + fm.EdorNo.value + "&StandbyFlag1=" + fm.EdorType.value +"&StandbyFlag4="+prtType;
	fm.submit();
}

//得到保费到帐日期
function getGrpAccFoundDate(grpPolNo)
{
  strSQL = "select min(a.AccFoundDate) from lcinsureacc a,lcpol b where a.polno = b.polno "
         + "and b.grppolno = '"+grpPolNo+"' and b.poltypeFlag = '2' ";
  var result =easyExecSql(strSQL);
  if (result != null) 
  {
	fm.all('GrpAccFoundDate').value = result[0][0];
  }
}
