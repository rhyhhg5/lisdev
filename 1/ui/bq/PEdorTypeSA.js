//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypePTReturn()
{
		initForm();
}

function edorTypeSASave()
{ 
  if (fm.all('CalMode').value == 'O') {
  	var nRemainMulti = parseFloat(fm.all('RemainMulti').value);
  	var nMulti = parseFloat(fm.all('Multi').value);
  	
  	if (nRemainMulti <= nMulti) {
  		alert("份数必须大于原份数。");
  		return;
  	}
  	if (nRemainMulti <= 0 ) {
  		alert("余留份数必须大于0。");
  		return;
    }  		
  } else {
    var nRemainAmnt = parseInt(fm.all('RemainAmnt').value);
    var nAmnt = parseInt(fm.all('Amnt').value);
  	if (nRemainAmnt <= nAmnt) {
  		alert("保额余数必须小于原保额。");
  		return;
  	}
  	if (nRemainAmnt <= 0) {
  		alert("保额余数必须大于0。");
  		return;
  	}  	
  }  
  
  //团险保全复用，第一次进入时提供录入功能
	if (typeof(top.opener.GrpBQ)=="boolean" && top.opener.GrpBQ==false) {
	  //接收录入数据
	  top.opener.GTArr.push(fm.RemainAmnt.value);

	  
	  top.opener.GrpBQ = true;
	  top.opener.pEdorMultiDetail();
	  top.close();
	} 
	//**************************************
	else {
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    
    fm.all('fmtransact').value = "INSERT||MAIN";  	  
    
    fm.submit();
  }

}

function customerQuery()
{	
	window.open("./LCAppntIndQuery.html");
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initLCAppntIndGrid();
 //  showSubmitFrame(mDebug);
  fm.submit(); //提交
}

function reportDetailClick(parm1, parm2) {
  //calMode是付加在保单的字符串传过来的，并放在了最后
  fm.DutyCode.value = LCDutyGrid.getRowColData(LCDutyGrid.getSelNo()-1, 1);
  fm.Multi.value = LCDutyGrid.getRowColData(LCDutyGrid.getSelNo()-1, 3);
  fm.Amnt.value = LCDutyGrid.getRowColData(LCDutyGrid.getSelNo()-1, 4);
  
	var calMode = LCDutyGrid.getRowColData(LCDutyGrid.getSelNo()-1, 6);  		
  
	if (calMode == 'P') {	  		
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content= 此保单无法进行部分退保计算" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
		return;
  //利用分数变化进行计算
	} else if (calMode == 'O') {   
	  //fm.all('RemainMulti').value = tArr[0][132];
		RemainAmntDiv.style.display = "none";
		RemainMultiDiv.style.display = "";  				
	//利用保额变化进行计算
	} else {                      
		//fm.all('RemainAmnt').value = tArr[0][132];
		RemainMultiDiv.style.display = 'none';	
		RemainAmntDiv.style.display = '';					
	}
}

//显示MultiLine中的责任名称
function displayName()
{	
    for (var i=0; i<LCDutyGrid.mulLineCount; i++ ) {
         var strSqla = "select DutyName from LMDuty where Dutycode='" + LCDutyGrid.getRowColData(i, 1) + "'" ;
         //查询SQL，返回结果字符串
         var tDutyName  = easyQueryVer3(strSqla, 1, 1, 1); 
      	
         if(tDutyName != '')
         {
            var tname = tDutyName.substring(4);
            LCDutyGrid.setRowColData(i,2,tname);
         }
    }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result)
{
  showInfo.close();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact = fm.all('fmtransact').value;
//  	alert(tTransact);
		if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
    
 	 		//查询成功则拆分字符串，返回二维数组
	  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);

	  		if (tArr != null) {	
            turnPage.arrDataCacheSet =chooseArray(tArr,[2, , 4, 9, 5, 37, 8]);		
            //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
            turnPage.pageDisplayGrid = LCDutyGrid;    
            //设置查询起始位置
            turnPage.pageIndex = 0;
            //在查询结果数组中取出符合页面显示大小设置的数组
            var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
            //调用MULTILINE对象显示查询结果
            displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
            //显示责任名称
            displayName();
            
            //控制是否显示翻页按钮
            if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
              try { window.divPage.style.display = ""; } catch(ex) { }
            } else {
              try { window.divPage.style.display = "none"; } catch(ex) { }
            }
	      } else {
  		      LCDutyGrid.delBlankLine("LCDutyGrid");
  	    }
			
			//团险保全复用，自动填写数值，并提交
    	if (typeof(top.opener.GrpBQ)=="boolean" && top.opener.GrpBQ==true) {
    	  fm.RemainAmnt.value = top.opener.GTArr.pop();
    	  
    	  edorTypeSASave();
     	}
     	//***********************************

		} else {
      
      //团险保全复用，提交成功后，再次调用，以循环
   		if (typeof(top.opener.GrpBQ)=="boolean" && top.opener.GrpBQ==true) {
			  top.opener.PEdorDetail();
			  top.close();
			}
			else {
			  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   		  initForm();
   		}
  	}
  	
  }

}      

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent()
{
	top.close();
}

function afterPersonQuery(arrResult)
{
    if (arrResult == null ||arrResult[0] == null || arrResult[0][0] == "" )
        return;

    //选择了一个投保人,显示详细细节
    fm.all("QueryCustomerNo").value = arrResult[0][0];
    
    var strSql = "select * from ldperson where customerNo = " + arrResult[0][0];
    
	//查询SQL，返回结果字符串
    turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

 //   alert(turnPage.strQueryResult);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {  
    	//清空MULTILINE，使用方法见MULTILINE使用说明 
      	VarGrid.clearData('VarGrid');  
      	alert("查询失败！");
      	return false;
  	}
  
 	 //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
    //查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
    fillPersonDetail();
    
  divLPAppntIndDetail.style.display = "";
}

function fillPersonDetail()
{
	try {
		fm.all("AppntCustomerNo").value = turnPage.arrDataCacheSet[0][0];
		fm.all("AppntName").value = turnPage.arrDataCacheSet[0][2];
		fm.all("AppntSex").value = turnPage.arrDataCacheSet[0][3];
		fm.all("AppntBirthday").value = turnPage.arrDataCacheSet[0][4];
		
		fm.all("AppntIDType").value = turnPage.arrDataCacheSet[0][16];
		fm.all("AppntIDNo").value = turnPage.arrDataCacheSet[0][18];
		fm.all("AppntNativePlace").value = turnPage.arrDataCacheSet[0][5];
		
		fm.all("AppntPostalAddress").value = turnPage.arrDataCacheSet[0][24];
		fm.all("AppntZipCode").value = turnPage.arrDataCacheSet[0][25];
		fm.all("AppntHomeAddress").value = turnPage.arrDataCacheSet[0][23];
		fm.all("AppntHomeZipCode").value = turnPage.arrDataCacheSet[0][22];
		
		fm.all("AppntPhone").value = turnPage.arrDataCacheSet[0][26];
		fm.all("AppntPhone2").value = turnPage.arrDataCacheSet[0][56];
		fm.all("AppntMobile").value = turnPage.arrDataCacheSet[0][28];
		fm.all("AppntEMail").value = turnPage.arrDataCacheSet[0][29];
		fm.all("AppntGrpName").value = turnPage.arrDataCacheSet[0][38];
		
		fm.all("AppntWorkType").value = turnPage.arrDataCacheSet[0][48];
		fm.all("AppntPluralityType").value = turnPage.arrDataCacheSet[0][49];
		fm.all("AppntOccupationCode").value = turnPage.arrDataCacheSet[0][50];
		fm.all("AppntOccupationType").value = turnPage.arrDataCacheSet[0][9];
    } catch(ex) {
    	alert("在PEdorTypeSA.js-->fillPersonDetail函数中发生异常:初始化界面错误!");
  	}      
}