<%
//PEdorTypeIAInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">  
//单击时查询

function initInpBox()
{ 
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
  	
  	if(flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  	}

  try
  {      
  	
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;  
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('ContNo').value = top.opener.fm.all("ContNo").value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all("EdorAcceptNo").value;
    //fm.all('InsuredNo').value = top.opener.fm.all("CustomerNo").value;
    showOneCodeName("EdorCode", "EdorTypeName"); 
  
  }
  catch(ex)
  {
    alert("在PEdorTypePTInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!sc");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在PEdorTypeIAInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {

    initInpBox();
    initSelBox(); 
    initDiv();
    initPolGrid();
    queryClick();
    initLPPolGrid();
    queryLPPol()
  }
  catch(re)
  {
    alert("PEdorTypeIAInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initQuery()
{	
	var i = 0;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
	fm.all('fmtransact').value = "QUERY||MAIN";
	fm.submit();	  	 	 

}


function initDiv()
{

}
function initPolGrid()
{                               
    var iArray = new Array();
      try
      {
      	iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="0px";            		//列宽
        iArray[0][2]=40;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="险种序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[1][1]="80px";            		//列宽
        iArray[1][2]=40;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[2]=new Array();
        iArray[2][0]="险种名称";
        iArray[2][1]="150px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="被保人";
        iArray[3][1]="60px";
        iArray[3][2]=100;
        iArray[3][3]=0;

        iArray[4]=new Array();
        iArray[4][0]="保额";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="总保费";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
           
        
        iArray[6]=new Array();
        iArray[6][0]="标准保费"
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        
        iArray[7]=new Array();
        iArray[7][0]="加费";
        iArray[7][1]="80px";
        iArray[7][2]=80;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="保单号码";
        iArray[8][1]="100px";
        iArray[8][2]=100;
        iArray[8][3]=3;
        
        iArray[9]=new Array();
        iArray[9][0]="险种号";
        iArray[9][1]="100px";
        iArray[9][2]=100;
        iArray[9][3]=3;
        
        iArray[10]=new Array();
        iArray[10][0]="档次";
        iArray[10][1]="30px";
        iArray[10][2]=100;
        iArray[10][3]=0;

        PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.canSel=1;
      PolGrid.selBoxEventFuncName ="getPolDetail" ;     //点击RadioBox时响应的JS函数
      PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      PolGrid.hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {      
        alert(ex);
      }
}


function initLPPolGrid()
{                               
    var iArray = new Array();
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="0px";            		//列宽
        iArray[0][2]=40;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="险种序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[1][1]="80px";            		//列宽
        iArray[1][2]=40;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[2]=new Array();
        iArray[2][0]="险种名称";
        iArray[2][1]="150px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="被保人";
        iArray[3][1]="60px";
        iArray[3][2]=100;
        iArray[3][3]=0;

        iArray[4]=new Array();
        iArray[4][0]="原保额";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="新保额";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
           
        

        
        iArray[6]=new Array();
        iArray[6][0]="保单号码";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=3;
        
        iArray[7]=new Array();
        iArray[7][0]="险种号";
        iArray[7][1]="100px";
        iArray[7][2]=100;
        iArray[7][3]=3;

        LPPolGrid = new MulLineEnter( "fm" , "LPPolGrid" ); 
      //这些属性必须在loadMulLine前
      LPPolGrid.mulLineCount = 0;   
      LPPolGrid.displayTitle = 1;
      LPPolGrid.canSel=0;
      //PolGrid.selBoxEventFuncName ="getPolDetail" ;     //点击RadioBox时响应的JS函数
      LPPolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      LPPolGrid.hiddenSubtraction=1;
      LPPolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {      
        alert(ex);
      }
}

</script>