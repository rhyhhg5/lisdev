<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="java.util.*"%> 
<html>
	<%
		//程序名称：
		//程序功能：个人保全
		//创建日期：2011-10-31
		//创建人  ：XP
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	
	<%	
		GlobalInput tG = new GlobalInput(); 
	  	tG=(GlobalInput)session.getValue("GI");
	  	String Operator=tG.Operator;
	%>
	<head>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/CommonTools.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="./PEdor.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="./LMSolvencyInput.js"></SCRIPT>
		<%@include file="../common/jsp/UsrCheck.jsp"%>
		<%@include file="LMSolvencyInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="./LMSolvencySave.jsp" method=post name=fm
			target="fraSubmit">
		<table>
				<tr>
					<td class=titleImg>
						偿付能力充足率录入
					</td>
				</tr>
		</table><br/>
		<table>
				<tr>
					<td class=titleImg>
						导入信息
					</td>
				</tr>
		</table>
			<div id="divImportGrid" style="display:''" >
					<table class=common>
				      <tr class=common>
					    <td text-align:left colSpan=1><span id="spanImportGrid"></span></td>
					  </tr>
					</table>
				  </div>
				<Div  id= "divPage" style="display:none" align="center">
					<div align="center">
				      <INPUT VALUE="首页" class = cssButton  TYPE=button onclick="turnPage1.firstPage();"> 
				      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
				      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage1.nextPage();"> 
				      <INPUT VALUE="尾页" class = cssButton TYPE=button onclick="turnPage1.lastPage();"> 	
				    </div>
				</DIV><br/>
			<div align="left">
				<br>
				<input type=Button name="importButton" class=cssButton value="导  入" 
				    onclick="importInsured();">&nbsp;&nbsp;&nbsp;
				 <Input type=Button name="save" class=cssButton value="保  存"
					onclick="submitForm();">&nbsp;&nbsp;&nbsp;

			</div><br/>
			
				<table>
				<tr>
					<td class=titleImg>
						偿付能力充足率信息
					</td>
				</tr>
				</table>
				  <div id="divSolvencyGrid" style="display:''" >
					<table class=common>
				      <tr class=common>
					    <td text-align:left colSpan=1><span id="spanSolvencyGrid"></span></td>
					  </tr>
					</table>
				  </div>
				<Div  id= "divPage" style="display:none" align="center">
					<div align="center">
				      <INPUT VALUE="首页" class = cssButton  TYPE=button onclick="turnPage.firstPage();"> 
				      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage();"> 					
				      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage();"> 
				      <INPUT VALUE="尾页" class = cssButton TYPE=button onclick="turnPage.lastPage();"> 	
				    </div>
				</DIV><br/>
				
				<table>
				<tr>
					<td class=titleImg>
						偿付能力充足率信息修改
					</td>
				</tr>
				</table>
				<Div>
				<tr class=common>
					<td class="title">
						年度
					</td>
					<td class="input"><Input class="readonly" readonly name=Year id=Year  >
					</td>
					<td class="title">
						季度
					</td>
					<td class="input">
						<input class="readonly" readonly name=Quarter id=Quarter  >
					</td>
					<td class="title">
						偿付能力充足率
					</td>
					<td class="input">
						<input class=common  name=CFNLRate  id=CFNLRate >
					</td>
				</tr> 
				<tr class=common>
					<td class="title">
						是否达到监管要求
					</td>
					<td class="input">
						<input class="codeNo" name="getCode" value='0' CodeData="0|未达到^1|达到^2|未达到" readOnly
						ondblclick="return showCodeListEx('getCode',[this,getCodeName],[0,1]);"  
						onkeyup="return showCodeListEx('getCode',[this,getCodeName],[0,1]);"><Input class="codeName" name="getCodeName" readonly>
					</td>
				</tr>
				<!-- 
				<select name="Flag" style="width: 100; height: 23" onchange="easyQueryClick();" >
				<option value = "0">未达到</option>
				<option value = "1">达到</option>
				</select>&nbsp; -->
				
				</DIV>
				<br/>
				<Input type=button name="update" class=cssButton value="修  改"
				onclick="LMSolvencyUpdate()">&nbsp;&nbsp;&nbsp;
			<input type=hidden id="fmtransact" name="fmtransact" > 
			<input type=hidden id="Operator" name="Operator" value='<%=Operator%>'>
			<INPUT TYPE=hidden name=tYear value="">
 			<INPUT TYPE=hidden name=tQuarter value="">
 			<INPUT TYPE=hidden name=tSolvency value="">
 			<INPUT TYPE=hidden name=tFlag value="">
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
