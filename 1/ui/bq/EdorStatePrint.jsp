<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "个险保全工作状况_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String tSQL = request.getParameter("strsql");
	System.out.println("打印查询:"+tSQL);
	
    //设置表头
    String[][] tTitle = {{"工单号", "客户号", "保单号", "受理日期", "受理时间", " 生效日期","送审日期", "送审时间", "保全确认完成日期", " 交费结束结案日期", "交费结束结案时间", "管理机构", "用户名"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(tSQL,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>