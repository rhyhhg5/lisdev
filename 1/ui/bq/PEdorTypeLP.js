var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function verifyPage()
{ 
	//以年龄和性别校验身份证号
	if (fm.all("IDType").value == "0") 
	var strChkIdNo = chkIdNo(fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);

	if (strChkIdNo!="" && strChkIdNo!=null)
	{
		alert(strChkIdNo);
		return false;
	}
	return true;
}

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  if (fm.IDType.value == "0")
  {
    if (fm.IDNo.value == "")
    {
      fm.IDNo.focus();
      return false;
    }
    if (!verifyPage())
    {
      fm.IDNo.focus();
      return false;
    }
  }
  return true;
}

function edorTypeLPSave()
{
  if (!beforeSubmit())
  {
    return false;
  }

	if (fm.BankCodeBak.value == fm.BankCode.value&&
   	fm.AccNameBak.value == fm.AccName.value&&
    fm.BankAccNoBak.value == fm.BankAccNo.value
		)
	{
		alert('被保人资料未发生变更！');
		return;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmtransact').value = "INSERT||MAIN";
	fm.submit();

}

function customerQuery()
{	
	window.open("./LCInsuredQuery.html");
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content,Result )
{
	try{ showInfo.close(); } catch(e) {}
	window.focus();
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		top.opener.queryImportData();
		top.opener.queryClick();		
		returnParent();
	}   
}     
                 
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		// 查询保单明细
		queryInsuredDetail();
	}
}

function queryInsuredDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	tEdorType=fm.all('EdorType').value;
	tPolNo=fm.all('PolNo').value;
	tCustomerNo = fm.all('CustomerNo').value;
	parent.fraInterface.fm.action = "./InsuredQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorTypeICSubmit.jsp";
}

function returnParent()
{
	try
	{
   // top.opener.updatClick(); 
    top.opener.focus();
	}
	catch (ex) {}
	top.close();
}

//查询客户基本信息
function getCustomerInfo(customerNo)
{
	var grpContNo = fm.all("GrpContNo").value;
	var contNo=fm.all("ContNo").value;
	var edorNo = fm.all("EdorNo").value; 
	var edorType = fm.all("EdorType").value;
	//先从P表中查询，再查C表
	var sql1 =
            "select InsuredNo, InsuredName, Sex, Birthday, IDType, IDNo," +
            "BankCode, AccName, BankAccNo,SerialNo " +
            "from LPDiskImport " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
            "and GrpContNo = '" + grpContNo + "' " +
            "and InsuredNo = '" + customerNo + "' ";


  var arrResult1 = easyExecSql(sql1); 


  if (!arrResult1)
	{
		sql1 = 
		      "select InsuredNo, Name, Sex, Birthday, IDType, IDNo, " +
					"BankCode, AccName, BankAccNo,'' " +
            "from LCInsured " +
            "where  ContNo = '" + contNo + "' " +
            "and InsuredNo = '" + customerNo + "' ";

		arrResult1 = easyExecSql(sql1);
		if (!arrResult1)
		{
			alert("未查到该客户资料！");
			return false;
		}
	}
	if (arrResult1)
	{
		fm.all('CustomerNo').value = arrResult1[0][0];
		fm.all('Name').value = arrResult1[0][1];
		fm.all('NameBak').value = arrResult1[0][1];
		fm.all('Sex').value = arrResult1[0][2];
		fm.all('SexBak').value = arrResult1[0][2];
		fm.all('Birthday').value = arrResult1[0][3];
		fm.all('BirthdayBak').value = arrResult1[0][3];
		fm.all('IDType').value = arrResult1[0][4];
		fm.all('IDTypeBak').value = arrResult1[0][4];
		fm.all('IDNo').value = arrResult1[0][5];
		fm.all('IDNoBak').value = arrResult1[0][5];
		fm.all('BankCode').value = arrResult1[0][6];
		fm.all('BankCodeBak').value = arrResult1[0][6];
		fm.all('AccName').value = arrResult1[0][7];
		fm.all('AccNameBak').value = arrResult1[0][7];
		fm.all('BankAccNo').value = arrResult1[0][8];
		fm.all('BankAccNoBak').value = arrResult1[0][8];
		fm.all('SerialNo').value=arrResult1[0][9];
	}


	divLPInsuredDetail.style.display ='';
	divDetail.style.display='';
}
function afterQuery( arrReturn )
{
	fm.all('BankCode').value = arrReturn[0];
//	fm.all('BankCodeBak').value = arrReturn[0];
	fm.all('BankCodeName').value = arrReturn[1];
	fm.all('BankCode1').value = arrReturn[0];
	fm.all('BankCodeName1').value = arrReturn[1];
	
}
function querybank()
{
	showInfo = window.open("LDBankQueryMain.jsp");
}