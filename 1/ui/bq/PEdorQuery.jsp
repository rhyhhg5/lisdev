<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="PEdorQuery.js"></SCRIPT>
  <SCRIPT src="PEdor.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="../common/jsp/AccessCheck.jsp"%>
  <%@include file="PEdorQueryInit.jsp"%>
    <%@include file = "ManageComLimit.jsp"%>
 <%
  GlobalInput tG = new GlobalInput();
     tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
 %>

  <title>个人批改查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./PEdorQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLPEdor1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            批单号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
          </TD>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class= common name=ContNo >
          </TD>
        </TR>
      </table>
    </Div>
    
    <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="easyQueryClick();"> 
    <INPUT VALUE="返回" class = cssbutton TYPE=button onclick="returnParent();"> 
          
    <hr>
          					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor2);">
    		</td>
    		<td class= titleImg>
    			 个人批改信息
    		</td>
    	</tr>
    	 
    </table>
    <div id = "divdetail" style = "display:none">
          <INPUT VALUE="查看明细" class = cssbutton TYPE=button onclick="detailEdor();"> 
 		</div>  
  	<Div  id= "divLPEdor2" style= "display: ''" align = "center">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanPEdorMainGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class = cssbutton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class = cssbutton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<input type=hidden id="EdorType" name="EdorType">
  	 <input type=hidden id="ContType" name="ContType">
  	 <input type=hidden id="ManageCom" name="ManageCom" value='<%=tG.ManageCom%>'>
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
