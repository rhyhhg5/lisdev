<%
//程序名称：GEdorTypeDetailSave.jsp
//程序功能：
//创建日期：2005-11-1
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String flag;
	String content;
	
	GlobalInput gi = (GlobalInput)session.getValue("GI");
	LPEdorItemSchema itemSchema = new LPEdorItemSchema();
	itemSchema.setEdorNo(request.getParameter("EdorNo"));
	itemSchema.setEdorType(request.getParameter("EdorType"));
	  System.out.println("OK1");
	 String[] splitChk = request.getParameterValues("InpOldPolGrid234Chk");
  String[] Relationship2x = request.getParameterValues("OldPolGrid2343");
  String[] InsuredNo = request.getParameterValues("OldPolGrid2341");
  System.out.println("OK2");
  LPInsuredSet tLPInsuredSet = new LPInsuredSet();
   VData data = new VData();
   
   if(splitChk!=null)
  {
  for (int i = 0; i < splitChk.length; i++)
    {
      LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
      tLPInsuredSchema.setEdorNo(request.getParameter("EdorNo"));
      tLPInsuredSchema.setInsuredNo(InsuredNo[i]);
      tLPInsuredSchema.setRelationToAppnt(Relationship2x[i]);
      tLPInsuredSchema.setRelationToMainInsured(Relationship2x[i]);
      tLPInsuredSet.add(tLPInsuredSchema);   
    }
       data.add(tLPInsuredSet);
  }

	PEdorCFSaveDetailUI tPEdorCFSaveDetailUI = new PEdorCFSaveDetailUI(gi, itemSchema);
	if (!tPEdorCFSaveDetailUI.submitData(data))
	{
		flag = "Fail";
		content = "数据保存失败！原因是:" + tPEdorCFSaveDetailUI.getError();
	}
	else 
	{
		flag = "Succ";
		content = "数据保存成功。";
	}
%>   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>