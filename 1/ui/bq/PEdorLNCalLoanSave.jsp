
<%
	//程序名称：PEdorLNCalLoanSave.jsp
	//程序功能：
	//创建日期：2002-07-19 16:49:22
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息
	CErrors tError = null;
	//后面要执行的动作：添加，修改

	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String transact = "";
	LPPolSet mLPPolSet = new LPPolSet();

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");

	transact = request.getParameter("fmtransact");
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String contNo = request.getParameter("ContNo");
	String XQFlag = request.getParameter("XQFlag");
	String Prem = request.getParameter("Prem");
	String PolNo = request.getParameter("PolNo");
	String riskcode = request.getParameter("RiskCode");

	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setContNo(contNo);
	tLPEdorItemSchema.setEdorType(edorType);
	tLPEdorItemSchema.setInsuredNo("000000");
	tLPEdorItemSchema.setPolNo(PolNo);
	// 准备传输数据 VData		
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPEdorItemSchema);

	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("XQFlag", XQFlag);
	tTransferData.setNameAndValue("Prem", Prem);
	tTransferData.setNameAndValue("riskcode", riskcode);
	tVData.add(tTransferData);

	PEdorLNCalLoanDetailBL tPEdorLNCalLoanDetailBL = new PEdorLNCalLoanDetailBL();
	if (!tPEdorLNCalLoanDetailBL.submitData(tVData, "")) {
		VData rVData = tPEdorLNCalLoanDetailBL.getResult();
		System.out.println("Submit Failed! " + tPEdorLNCalLoanDetailBL.mErrors.getErrContent());
		Content = transact + "失败，原因是:" + tPEdorLNCalLoanDetailBL.mErrors.getFirstError();
		FlagStr = "Fail";
	} else {
		Content = "保存成功";
		FlagStr = "Success";
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","1");
</script>
</html>

