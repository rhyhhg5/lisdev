<%
//程序名称：PEdorTypeNSInput.jsp
//程序功能：
//创建日期：2005-7-19 14:20  Lanjun 修改
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">

var Subtype;
//初始化
function initEdor() {
	
	//增加附加险时，附加险的生效日期为保全申请日次日
	try
	{
		var sql = "select edorvalidate from lpedoritem where edorno='"
		          +top.opener.fm.all('EdorNo').value+"' and edortype='"
		          +top.opener.fm.all('EdorType').value+"'";
		var rsArr = easyExecSql(sql);       
		if (rsArr != null)
		{
			fm.all('CValiDate').value = rsArr[0][0];
		}   
	}
	catch(ex)
	{
	}

	try
	{

		fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
		fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
		fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
		

    fm.all('Subtype').value = Subtype ;
		if(uwFlag==1)
		{
			fm.add.style.display = 'none';
			fm.modify.style.display = 'none';
			fm.del.style.display = 'none';
			fm.save.style.display = 'none';
			fm.addImp.value ="查看告知";
		}
		showCodeName();
		if (showMeTheManageCom() != "86")
		{
			fm.addImp.disabled = true;
		}
		else
		{
			fm.addImp.disabled = false;
		}
 }
catch(ex)
{
	//alert(ex.description);
}

}

//已经有的险种列表
function initPolGrid()
{
    var iArray = new Array();

      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="保单号码";
        iArray[1][1]="100px";
        iArray[1][2]=120;
        iArray[1][3]=0;

        iArray[2]=new Array();
        iArray[2][0]="被保险人号码";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;

        iArray[3]=new Array();
        iArray[3][0]="被保险人姓名";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;

        iArray[4]=new Array();
        iArray[4][0]="险种名称";
        iArray[4][1]="130px";
        iArray[4][2]=100;
        iArray[4][3]=0;

        iArray[5]=new Array();
        iArray[5][0]="险种代码";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;

        iArray[6]=new Array();
        iArray[6][0]="险种号码";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=3;

        iArray[7]=new Array();
        iArray[7][0]="保额/份数";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;

        iArray[8]=new Array();
        iArray[8][0]="期交保费";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;

        iArray[9]=new Array();
        iArray[9][0]="生效日期"
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=0;

        iArray[10]=new Array();
        iArray[10][0]="交至日期";
        iArray[10][1]="80px";
        iArray[10][2]=100;
        iArray[10][3]=0;



        iArray[11]=new Array();
        iArray[11][0]="集体保单号码";
        iArray[11][1]="100px";
        iArray[11][2]=100;
        iArray[11][3]=3;

        PolGrid = new MulLineEnter( "fm" , "PolGrid" );
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;
      PolGrid.displayTitle = 1;
      PolGrid.canSel=1;
      PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      PolGrid.hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面

      }
      catch(ex)
      {
        alert(ex);
      }
}

//新增附加险列表初始化
function initLCPolAddGrid()
{
    var iArray = new Array();

      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="被保险人号码";         			//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="insuredNo";

      iArray[2]=new Array();
      iArray[2][0]="被保险人姓名";         			//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="insuredName";

      iArray[3]=new Array();
      iArray[3][0]="险种代码";         			//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="riskCode";

      iArray[4]=new Array();
      iArray[4][0]="险种名称";         			//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][21]="riskName";

      iArray[5]=new Array();
      iArray[5][0]="险种号";         			//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][21]="polNo";

      iArray[6]=new Array();
      iArray[6][0]="保额";         			//列名
      iArray[6][1]="66px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][21]="amnt";

      iArray[7]=new Array();
      iArray[7][0]="档次";         			//列名
      iArray[7][1]="66px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][21]="mult";
      
      iArray[8]=new Array();
      iArray[8][0]="期交保费";         			//列名
      iArray[8][1]="66px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][21]="prem";
      
      iArray[9]=new Array();
      iArray[9][0]="生效时间编码";         			//列名
      iArray[9][1]="70px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[9][4]="cvalidatetype";
      iArray[9][5]="9|10"; 
		  iArray[9][6]="0|1|";
      iArray[9][21]="cValiDateType";
      
      iArray[10]=new Array();
      iArray[10][0]="生效时间";         			//列名
      iArray[10][1]="66px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[10][21]="cValiDateNameType";

      iArray[11]=new Array();
      iArray[11][0]="保单号";         			//列名
      iArray[11][1]="66px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[11][21]="contNo";

      iArray[12]=new Array();
      iArray[12][0]="主险种号";         			//列名
      iArray[12][1]="100px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[12][21]="mainPolNo";

      iArray[13]=new Array();
      iArray[13][0]="险种标志";         			//列名
      iArray[13][1]="0px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[13][21]="appFlag";

      LCPolAddGrid = new MulLineEnter( "fm" , "LCPolAddGrid");
      //这些属性必须在loadMulLine前
      LCPolAddGrid.mulLineCount = 0;
      LCPolAddGrid.displayTitle = 1;
      LCPolAddGrid.canSel = 1;
      //SubInsuredGrid.tableWidth = 200;
      LCPolAddGrid.selBoxEventFuncName ="queryPolInfo" ;
      LCPolAddGrid.hiddenPlus = 1;
      LCPolAddGrid.hiddenSubtraction = 1;

      LCPolAddGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面

      }
      catch(ex)
      {
        alert(ex);
      }
}



function initForm()
{
  try
  {
  	initEdor();  //初始化项目信息
  	
  	initLCInsuredGrid();  //初始化被保人信息列表
  	queryLCInsured();  //查询被保人信息
  	
  	initPolGrid();
  	
    //新增的附加险
    initLCPolAddGrid();
    queryAddedPols();
  	
  	//initQuery();
    mSwitch = parent.VD.gVSwitch;
  
  
    
    
    initPEdorTry() ;
  }
  catch(ex)
  {
    alert(ex.message);
  }
}

//新增附加险列表初始化
function initLCInsuredGrid()
{
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="客户号";         			//列名
    iArray[1][1]="50px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="insuredNo";
    
    iArray[2]=new Array();
    iArray[2][0]="被保人姓名";         			//列名
    iArray[2][1]="60px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="name";
    
    iArray[3]=new Array();
    iArray[3][0]="性别";         			//列名
    iArray[3][1]="20px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="sex";
    
    iArray[4]=new Array();
    iArray[4][0]="出生日期";         			//列名
    iArray[4][1]="50px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="birthday";
    
    iArray[5]=new Array();
    iArray[5][0]="证件类型";         			//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="idType";
    
    iArray[6]=new Array();
    iArray[6][0]="证件号码";         			//列名
    iArray[6][1]="66px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="idNo";
    
    
    LCInsuredGrid = new MulLineEnter( "fm" , "LCInsuredGrid");
    //这些属性必须在loadMulLine前
    LCInsuredGrid.mulLineCount = 1;
    LCInsuredGrid.displayTitle = 1;
    LCInsuredGrid.canSel = 1;
    LCInsuredGrid.selBoxEventFuncName ="queryInsuredInfo" ;
    LCInsuredGrid.hiddenPlus = 1;
    LCInsuredGrid.hiddenSubtraction = 1;
    
    LCInsuredGrid.loadMulLine(iArray);
    
    //这些操作必须在loadMulLine后面
  
  }
  catch(ex)
  {
    alert(ex.message);
  }
}


</script>
