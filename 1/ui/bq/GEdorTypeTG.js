// 该文件中包含客户端(协议退保)需要处理的函数和事件

var showInfo;
var mDebug="1";
var addrFlag="NEW"
var turnPage = new turnPageClass();// 使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();

// 初始化页面

function edorTypeXTReturn()
{
	//initForm();
}

// 实现明细保存的功能

function edorTypeXTSave()
{
 	if(!checkData())
 	{
 	  return false;
 	}
 	
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="GEdorTypeTGSubmit.jsp";
  fm.all('fmAction').value = "INSERT||EDORXT";
  fm.submit();
}

// 校验可否保存
function checkData()
{
	//是否退保试算
	var testSQL = "select 1 from lpinsureacc "
			+ " where edorno='"+ fm.EdorNo.value +"' and edortype='"+fm.EdorType.value +"' "
			+ " and GrpContNo = '" + fm.GrpContNo.value + "' "
			+ " with ur";
	var test = easyExecSql(testSQL);
	if(null==test||""==test||"null"==test)
	{
		alert("请先退保试算");
		return false;  
	}
	
	if(null==fm.reason_tb.value || ""==fm.reason_tb.value || "null"==fm.reason_tb.value){
		alert("请选择退保原因");
		return false;
	}
	
	for(var j = 0; j < GrpAccGrid.mulLineCount; j++)
	{
		var getMoney = GrpAccGrid.getRowColDataByName(j, "CTMoney");
		var xtMoney = GrpAccGrid.getRowColDataByName(j, "XTMoney");
		if(null==xtMoney || ""==xtMoney){
			alert("请录入协议解约金额");
			return false;
		}
		xtMoney = parseFloat(xtMoney);
		getMoney = parseFloat(getMoney);
	    if(xtMoney < 0)
	    {
	       alert("协议退费金额必须为正数");
	       return false;
	    }
	    var sql = "select sum(a.SumActuPayMoney) "
            + " from LJAPayPerson a, LCPol b "
            + " where a.PolNo = b.PolNo "
            + "  and a.GrpContNo = '" + fm.GrpContNo.value + "' "
            + "  and b.poltypeflag='2' with ur";
		var rs = easyExecSql(sql);
		if(rs && rs[0][0] != "" && rs[0][0] != "null")
		{
		   bfMoney = rs[0][0];
		}
		var sumActuPayMoney = parseFloat(bfMoney);
	    var proportion = parseFloat(xtMoney) / parseFloat(sumActuPayMoney);
	    if(proportion > 1.1)
	    {
	        alert("公共账户的协议退保比例不能超过总保费的110%");
	        return false;
	    }
	}
	
	for(var i = 0; i < PAccGrid.mulLineCount; i++)
	{
		var getMoney = PAccGrid.getRowColDataByName(i, "CTMoney");
		var xtMoney = PAccGrid.getRowColDataByName(i, "XTMoney");
		var accType = PAccGrid.getRowColDataByName(i, "AccType");
		var accNo = PAccGrid.getRowColDataByName(i, "AccNo");
		if(null==xtMoney || ""==xtMoney){
			alert("请录入"+accType+"的协议解约金额");
			return false;
		}
		xtMoney = parseFloat(xtMoney);
		getMoney = parseFloat(getMoney);
	    if(xtMoney < 0)
	    {
	       alert("协议退费金额必须为正数");
	       return false;
	    }
	    
	    var sql = "select sum(a.SumActuPayMoney) "
            + " from ljapayperson a,lmriskaccpay b "
            + "where a.payplancode = b.payplancode "
            + "  and a.GrpContNo = '" + fm.GrpContNo.value + "' "
            + "  and b.insuaccno='" + accNo + "' with ur";
		var rs = easyExecSql(sql);
		if(rs && rs[0][0] != "" && rs[0][0] != "null")
		{
		   bfMoney = rs[0][0];
		}
		var sumActuPayMoney = parseFloat(bfMoney);
	    var proportion = parseFloat(xtMoney) / parseFloat(sumActuPayMoney);
	    if(proportion > 1.1)
	    {
	        alert(accType+"的协议退保比例不能超过总保费的110%");
	        return false;
	    }
	}
	
  return true;
}

function submitForm()
{
  fm.all('addrFlag').value=addrFlag;

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 // initLCAppntGrpGrid();
 // showSubmitFrame(mDebug); 
  
  fm.submit(); // 提交
}

// 提交后操作,服务器数据返回后执行的操作

function afterSubmit( FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		queryLCGrpPolGrid();
	    
	}
}

// 提交前的校验、计算

function afterCodeSelect( cCodeName, Field )
{
}

// 提交前的校验、计算

function beforeSubmit()
{
  // 添加操作
}           
       
// 显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*******************************************************************************
 * 显示frmSubmit框架，用来调试 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

// 返回主页面
function returnParent()
{
	top.opener.getGrpEdorItem();
	top.close();
}

function getEdorItemInfo()
{
  var sql = "select EdorValiDate, ReasonCode "
            + "from LPGrpEdorItem "
            + "where EdorNo = '" + fm.EdorNo.value + "' "
            + "   and EdorType = '" + fm.EdorType.value + "' "
            + "   and GrpContNo = '" + fm.GrpContNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.EdorValiDate.value = rs[0][0];
    fm.reason_tb.value = rs[0][1];
  }
}

/*******************************************************************************
 * 查询投保单位信息 参数 ： 查询返回的二维数组 返回值： 无
 * ********************************************************************
 */
 
function getGrpCont()
{
  
  var strSQL1 = "select * from  lpgrpedoritem where EdorNo='"+fm.EdorNo.value+"' and edortype='TG'";
  var arrResult1 = easyExecSql(strSQL1,1,0);      
      fm.reason_tb.value = arrResult1[0][23];
      
      var sql = "select CodeName "
              + "from LDCode "
              + "where CodeType = 'reason_tb' "
              + "   and Code = '" + fm.reason_tb.value + "' ";
      var rs = easyExecSql(sql);
      if(rs && rs[0][0] != "" && rs[0][0] != "null")
      {
        fm.reason_tbName.value = rs[0][0];
      }
      showCodeName();
}

// 查询公共账户以及个人账户信息
function queryLCGrpPolGrid()
{
  var grpAccSQL = " select a.riskcode, "
            + " (select riskname from lmriskapp where riskcode =a.riskcode),"
            + " b.insuaccbala,"
            + " nvl((select insuaccbala from lpinsureacc where grpcontno=b.grpcontno and insuaccno=b.insuaccno"
            + " and edorno='"+ fm.EdorNo.value +"' and edortype='"+fm.EdorType.value +"'),0),"
            + " '0' "
            + " from lcpol a, lcinsureacc b "
            + " where a.grpcontno=b.grpcontno and a.polno=b.polno"
            + " and a.grpContNo = '" + fm.GrpContNo.value + "' "
            + " and a.poltypeflag='2' "
            + " with ur";
	
  	turnPage.pageLineNum = 100;
	turnPage.queryModal(grpAccSQL, GrpAccGrid);
	
	var accSQL =" select riskcode,riskname,accname,sum(peoples),sum(accbala),"
			+ " sum(double(getmoney))," 
			+ " (case when aa.insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass = '4')) then  "
			+ " (select EdorValue from lpedorespecialdata where edorno='"+ fm.EdorNo.value +"' and edortype='"+fm.EdorType.value +"' and detailtype='XTFEE_P2' "
			+ "  fetch first 1 rows only ) "
			+ " when aa.insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass = '5')) then "
			+ " (select EdorValue from lpedorespecialdata where edorno='"+ fm.EdorNo.value +"' and edortype='"+fm.EdorType.value +"' and detailtype='XTFEE_P1' "
			+ " fetch first 1 rows only )"
			+ " else '' end) as xtMoney,"
			+ " insuaccno from ("
			+ " select b.riskcode as riskcode, "
			+ " (select riskname from lmriskapp where riskcode=b.riskcode) as riskname, "
			+ " b.insuaccno as insuaccno,"
			+ " (select insuaccname from lmrisktoacc where riskcode=b.riskcode and insuaccno=b.insuaccno) as accname, "
			+ " sum(a.peoples) as peoples,sum(b.insuaccbala) as accbala," 
			+ " (select sum(insuaccbala) from lpinsureacc where grpcontno=a.grpcontno and contno=a.contno "
			+ " and insuaccno=b.insuaccno and edorno='"+ fm.EdorNo.value +"' and edortype='"+fm.EdorType.value +"')  as getmoney"
			+ " from lccont a,lcinsureacc b "
			+ " where a.grpcontno=b.grpcontno and a.contno=b.contno"
			+ " and a.grpcontno='" + fm.GrpContNo.value + "'"
			+ " and b.insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass in ('4','5')))"
			+ " group by b.riskcode,insuaccno,a.grpcontno,a.contno"
			+ " )aa group by accname,riskname,riskcode,insuaccno"
			+ " with ur";
	turnPage2.queryModal(accSQL, PAccGrid);
	setField();
}

// 计算实收保费
function setField()
{
	  var sql = "select EdorValue from lpedorespecialdata "
	  		+ " where edorno='"+ fm.EdorNo.value +"' and edortype='"+fm.EdorType.value +"' "
	  		+ " and detailtype='XTFEE_G' with ur";
	var rs = easyExecSql(sql);
	if(rs && rs[0][0] != "" && rs[0][0] != "null")
	{
		GrpAccGrid.setRowColDataByName(0, "XTMoney", rs[0][0]);
	}
	
	for(var i = 0; i < PAccGrid.mulLineCount; i++)
	{
		var getMoney = PAccGrid.getRowColDataByName(i, "CTMoney");
		var xtMoney = PAccGrid.getRowColDataByName(i, "XTMoney");
		if(null==getMoney || "null"==getMoney){
			PAccGrid.setRowColDataByName(i,"CTMoney","0");
		}
		if(null==xtMoney || "null"==xtMoney){
			PAccGrid.setRowColDataByName(i,"XTMoney","0");
		}
	}
}

// 退保试算
function edorCalZTTest()
{
	var showStr="正在进行退保试算，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="GEdorULBudgetSave.jsp?grpContNo="+ fm.GrpContNo.value+ "&edorNo=" + fm.EdorNo.value + "&edorType=" + fm.EdorType.value+ "&edorValiDate=" + fm.EdorValiDate.value;
	fm.submit();
}

