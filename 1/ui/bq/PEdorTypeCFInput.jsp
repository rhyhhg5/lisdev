<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK"> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeCF.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeCFInit.jsp"%>
  <title>保单拆分 </title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeCFSubmit.jsp" method=post name=fm target="fraSubmit"> 
  <input type="hidden" name="AppntNo">
  <table class=common>
    <TR class= common> 
      <TD class= title > 受理号 </TD>
      <TD class= input>
        <input class="readonly" type="text" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" type="hidden" readonly name=EdorType>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 合同保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </table>
   <table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divOldPol);">
	      </td>
	      <td class= titleImg>
	        原保单被保人信息
	      </td>
		  </tr>
		</table>
    <div id= "divOldPol" style= "display: ''">
  		<table class= common>
  		 		<tr  class= common>
  			  		<td text-align: left colSpan=1>
  					<span id="spanOldPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		</table>			
    	<div id= "divPage" align=center style= "display: 'none' ">
  			<input value="首  页" class=cssButton type=button onclick="turnPage.firstPage();"> 
  			<input value="上一页" class=cssButton type=button onclick="turnPage.previousPage();"> 					
  			<input value="下一页" class=cssButton type=button onclick="turnPage.nextPage();"> 
  			<input value="尾  页" class=cssButton type=button onclick="turnPage.lastPage();"> 
  		</div>		
  	</div>  
  	<br>
  	  新投保人客户号
  		<Input class="common" name="NewAppntNo">
  		<Input type="hidden" name="NewAppntName">
	    <input type="button" Class="cssButton"  value="拆  分" onclick="splitCont();" >
	    <input type="button" Class="cssButton"  value="取  消  拆  分" onclick="backCont();" >
	    <br><br>
	    <hr>
	 <table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLastPol);">
	      </td>
	      <td class= titleImg>
	        原保单险种信息
	      </td>
		  </tr>
		</table>
		 <div id= "divLastPol" style= "display: ''">
  		<table class= common>
  		 		<tr  class= common>
  			  		<td text-align: left colSpan=1>
  					<span id="spanLastPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		</table>
  		<div id= "divPage3" align=center style= "display: 'none' ">
  			<input value="首  页" class=cssButton type=button onclick="turnPage3.firstPage();"> 
  			<input value="上一页" class=cssButton type=button onclick="turnPage3.previousPage();"> 					
  			<input value="下一页" class=cssButton type=button onclick="turnPage3.nextPage();"> 
  			<input value="尾  页" class=cssButton type=button onclick="turnPage3.lastPage();"> 
  		</div>					
  	</div>  
  	
  	<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divOldPol234);">
	      </td>
	      <td class= titleImg>
	        拆分保单的其他被保人与投保人关系设置
	      </td>
		  </tr>
		</table>
    <div id= "divOldPol234" style= "display: ''">
  		<table class= common>
  		 		<tr  class= common>
  			  		<td text-align: left colSpan=1>
  					<span id="spanOldPolGrid234" >
  					</span> 
  			  	</td>
  			</tr>
  		</table>			
    	<div id= "divPage" align=center style= "display: 'none' ">
  			<input value="首  页" class=cssButton type=button onclick="turnPage4.firstPage();"> 
  			<input value="上一页" class=cssButton type=button onclick="turnPage4.previousPage();"> 					
  			<input value="下一页" class=cssButton type=button onclick="turnPage4.nextPage();"> 
  			<input value="尾  页" class=cssButton type=button onclick="turnPage4.lastPage();"> 
  		</div>		
  	</div> 
  	
  	<br>
  	<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divNewPol);">
	      </td>
	      <td class= titleImg>
	        拆分保单险种信息
	      </td>
		  </tr>
		</table>
    <div id= "divNewPol" style= "display: ''">
  		<table class= common>
  		 		<tr  class= common>
  			  		<td text-align: left colSpan=1>
  					<span id="spanNewPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		</table>
  		<div id= "divPage2" align=center style= "display: 'none' ">
  			<input value="首  页" class=cssButton type=button onclick="turnPage2.firstPage();"> 
  			<input value="上一页" class=cssButton type=button onclick="turnPage2.previousPage();"> 					
  			<input value="下一页" class=cssButton type=button onclick="turnPage2.nextPage();"> 
  			<input value="尾  页" class=cssButton type=button onclick="turnPage2.lastPage();"> 
  		</div>					
  	</div>  
  	<br><hr>
  	<Input class= cssButton type=Button value="保  存" onclick="edorTypeCFSave();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>