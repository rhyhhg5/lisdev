<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
	//程序名称：
	//程序功能：激活卡补激活并生成客户资料变更的工单审批
	//创建日期：2010-08-12
	//创建人 :  XP
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%>


<%
	System.out.println("WSCertifyToWorkSave.jsp start ...");

	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tGI = (GlobalInput) session.getValue("GI");

	try {
		String tCardNo = request.getParameter("BCardNo");
		String tInsuredNo = request.getParameter("BInsuredNo");
		String tEdorNo = request.getParameter("BEdorNo");
		System.out.println("tCardNo:" + tCardNo);

		VData tVData = new VData();
		TransferData tTransferData = new TransferData();

		tTransferData.setNameAndValue("InsuredNo", tInsuredNo);
		tTransferData.setNameAndValue("EdorNo", tEdorNo);
		tTransferData.setNameAndValue("CardNo", tCardNo);

		tVData.add(tTransferData);
		tVData.add(tGI);

			WSCertifyToWorkApprovalBL tWSCertifyToWorkBL = new WSCertifyToWorkApprovalBL();
			if (!tWSCertifyToWorkBL.submitData(tVData, null)) {
				Content = "审批失败，原因是: "
						+ tWSCertifyToWorkBL.mErrors.getFirstError();
				FlagStr = "Fail";
			} else {
				Content = "审批成功！";
				FlagStr = "Succ";
			}
		
	} catch (Exception e) {
		System.out.println("fsfsfsdfsdd");
		Content = " 处理失败。";
		FlagStr = "Fail";
		e.printStackTrace();
	}
	

	System.out.println("WSCertifyToWorkApprovalSave.jsp end ...");
%>

<html>
	<script language="javascript">
    parent.fraInterface.afterWSSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
