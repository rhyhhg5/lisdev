// 该文件中包含客户端(协议退保)需要处理的函数和事件

var showInfo;
var mDebug="1";
var addrFlag="NEW"
var turnPage = new turnPageClass();//使用翻页功能，必须建立为全局变量

//初始化页面

function edorTypeXTReturn()
{
	initForm();
}

//实现明细保存的功能

function edorTypeXTSave()
{
 	if(!checkData())
 	{
 	  return false;
 	}
 	
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmAction').value = "INSERT||EDORXT";
  fm.submit();
}

//校验可否保存
function checkData()
{
  var polSelectedCount = 0;
  for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
  {
    if(LCGrpPolGrid.getChkNo(i))
    {
      polSelectedCount = polSelectedCount + 1;
      
      var contPlanCode = LCGrpPolGrid.getRowColDataByName(i, "ContPlanCode");
      var riskCode = LCGrpPolGrid.getRowColDataByName(i, "RiskCode");
      var getMoney = LCGrpPolGrid.getRowColDataByName(i, "GetMoney");
      var xtMoney = LCGrpPolGrid.getRowColDataByName(i, "XTFee");
      var xtProportion = LCGrpPolGrid.getRowColDataByName(i, "XTFeeRate");
      
      if(getMoney == "")
      {
        alert("请先退保试算");
        return false;
      }
      if(xtMoney == "" || xtProportion == "")
      {
        alert("请先算费");
        return false;
      }
      
      xtMoney = parseFloat(xtMoney);
      if(xtMoney < 0)
      {
        alert("协议退费金额必须为正数");
        return false;
      }
      
      if(xtProportion == "-")
      {
        continue;
      }
      
      var xtProportionCheck = pointFour(parseFloat(xtMoney) / parseFloat(getMoney));
      var xtMoneyCheck = pointTwo(getMoney * parseFloat(xtProportion));
      if(xtProportion != xtProportionCheck && xtMoney != xtMoneyCheck)
      {
        alert(contPlanCode + " " + riskCode + "算费结果被修改，请再次算费");
        return false;
      }
    }
  }
  
  if(polSelectedCount == 0)
  {
    alert("请选择险种");
    return false;
  }
  
  if(fm.reason_tb.value == "")
  {
    alert("请选择退保原因");
    return false;
  }
  
  if (!verifyInput2())
  {
    return false;
  }
  
  return true;
}

function submitForm()
{
  fm.all('addrFlag').value=addrFlag;

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 // initLCAppntGrpGrid();
 //  showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作

function afterSubmit( FlagStr, content)
{
  //showInfo.close();
  //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		top.opener.focus();
		top.opener.getGrpEdorItem();
	    top.close();
	}
}

//提交前的校验、计算

function afterCodeSelect( cCodeName, Field )
{
}

//提交前的校验、计算  

function beforeSubmit()
{
  //添加操作
}           
       
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

//返回主页面
function returnParent()
{
	top.opener.getGrpEdorItem();
	top.close();
}

function getEdorItemInfo()
{
  var sql = "select EdorValiDate, ReasonCode "
            + "from LPGrpEdorItem "
            + "where EdorNo = '" + fm.EdorNo.value + "' "
            + "   and EdorType = '" + fm.EdorType.value + "' "
            + "   and GrpContNo = '" + fm.GrpContNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.EdorValiDate.value = rs[0][0];
    fm.reason_tb.value = rs[0][1];
  }
}

/*********************************************************************
 *  查询投保单位信息
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
 
function getGrpCont()
{
  
  try {fm.all('GrpContNo2').value= fm.all('GrpContNo').value; } catch(ex) { };
  
  var strSQL = "select * from LCGrpCont  where GrpContNo = '"+ fm.GrpContNo.value + "'";
  var arrResult = easyExecSql(strSQL, 1, 0);
  if (arrResult != null) 
  {
    try {fm.all('AppntNo').value= arrResult[0][12]; } catch(ex) { }; //客户号码
    try {fm.all('Peoples2').value= arrResult[0][14]; } catch(ex) { }; //投保总人数
    try {fm.all('GrpName').value= arrResult[0][15]; } catch(ex) { }; //单位名称
    try {fm.all('SignCom').value= arrResult[0][48]; } catch(ex) { }; //签单机构
    try {fm.all('SignDate').value= arrResult[0][49]; } catch(ex) { }; //签单日期
    try {fm.all('CValiDate').value= arrResult[0][51]; } catch(ex) { }; //保单生效日期
    try {fm.all('Prem').value= arrResult[0][59]; } catch(ex) { }; //总保费
    try {fm.all('Amnt').value= arrResult[0][60]; } catch(ex) { }; //总保额
    try {fm.all('PolApplyDate').value= arrResult[0][80]; } catch(ex) { }; //投保日期
    try {fm.all('PayToDate').value= getPayToDate(); } catch(ex) { }; //交至日期
  }
  else
  {
      alert('未查到该集体保单！');
  } 
  
  var strSQL1 = "select * from  lpgrpedoritem where EdorNo='"+fm.EdorNo.value+"' and edortype='XT'";
  var arrResult1 = easyExecSql(strSQL1,1,0);
  if(arrResult1 != null)
  {
  	var getMoney;
  	getMoney =   mathRound(arrResult1[0][16]); 
  	//将负数转换为正数
  	if(getMoney<0)	
  	{
       getMoney = -getMoney;
  	}
  	if(getMoney == 0)
  	{
  	    getMoney = fm.all('Prem').value;
  	}
  	try
  	{
  	    fm.all('GetMoneyNum').value=getMoney;
  	    if(fm.all('GetMoneyNum').value == "NaN")
  	    {
  	        fm.all('GetMoneyNum').value = fm.all('Prem').value;
  	    }
  	} catch(ex){ };//退费
      try 
      {
      	var a=parseFloat(fm.all('GetMoneyNum').value);
      	var b=parseFloat(fm.all('Prem').value);
      	fm.all('GetMoneyPro').value= mathRound(a/b*100);
      } 
      catch(ex){};//退费(按比例)
      
      fm.reason_tb.value = arrResult1[0][23];
      
      var sql = "select CodeName "
              + "from LDCode "
              + "where CodeType = 'reason_tb' "
              + "   and Code = '" + fm.reason_tb.value + "' ";
      var rs = easyExecSql(sql);
      if(rs && rs[0][0] != "" && rs[0][0] != "null")
      {
        fm.reason_tbName.value = rs[0][0];
      }
      showCodeName();
  }
}

//得到保单交至日期
function getPayToDate()
{
  var sql = "select min(PayToDate) "
            + "from LCGrpPol "
            + "where GrpContNo = '" + fm.GrpContNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
   return rs[0][0];
  }
  return "";
}

//处理险种相关信息
function queryLCGrpPolGrid()
{
  var sql = "  select a.contPlanCode, "
            + "   (select riskSeqNo from LCGrPPol where grpContNo = c.grpContNo and riskCode = a.riskCode), "
            + "   a.riskCode, (select riskName from LMRisk where riskCode = a.riskCode), "
            + "   sum(c.prem), sum(c.prem), '', "
            + "   min(c.CValiDate), max(c.EndDate), min(c.PayToDate), '', '', '' "
            + "from LCContPlanRisk a, LCPol c "
            + "where a.grpContNo = c.grpContNo "
            + "   and a.contPlanCode = c.contPlanCode "
            + "   and a.riskCode = c.riskCode "
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
            + "group by c.grpContNo, a.contPlanCode, a.riskCode, "
            + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType "
            + "order by a.contPlanCode, a.RiskCode ";
	
  turnPage.pageLineNum = 100;
	turnPage.queryModal(sql, LCGrpPolGrid);
	
	setField();
	setRisk590206();
}

//计算实收保费
function setField()
{
  for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
	{
	  var contPlanCode = LCGrpPolGrid.getRowColDataByName(i, "ContPlanCode");
	  var riskCode = LCGrpPolGrid.getRowColDataByName(i, "RiskCode");
	  
	  var bfMoney = '0';
	  //实收保费
	  var sql = "select sum(a.SumActuPayMoney) "
	            + "from LJAPayPerson a, LCPol b "
	            + "where a.PolNo = b.PolNo "
	            //+ "   and a.CurPayToDate = b.PayToDate "
	            + "   and a.GrpContNo = '" + fm.GrpContNo.value + "' "
	            + "   and b.ContPlanCode = '" + contPlanCode + "' "
	            + "   and b.RiskCode = '" + riskCode + "' ";
	  var rs = easyExecSql(sql);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    bfMoney = rs[0][0];
	  }
	  
	  //20110413，zhanggm 如果是特需险，实收保费要+上保全追加保费
	  var zfMoney = 0;
	  var sql = "select sum(a.GetMoney) "
	            + "from LJAGetEndorse a, LCPol b "
	            + "where a.PolNo = b.PolNo "
	            + "   and a.GrpContNo = '" + fm.GrpContNo.value + "' "
	            + "   and b.ContPlanCode = '" + contPlanCode + "' "
	            + "   and b.RiskCode = '" + riskCode + "' " 
	            + "   and a.FeeOperaTionType = 'ZB' ";
	  rs = easyExecSql(sql);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    zfMoney = rs[0][0];
	  }
	  //20170523,by hhw 管理式医疗产品，实收保费+追加保费+减少保额
	  var isTDBC="select 1 from ldcode where codetype='tdbc' and code='" + riskCode+ "' ";
	  rs = easyExecSql(isTDBC);
	  if(rs){
		  var tSql = "select sum(a.GetMoney) "
		            + "from LJAGetEndorse a, LCPol b "
		            + "where a.PolNo = b.PolNo "
		            + "   and a.GrpContNo = '" + fm.GrpContNo.value + "' "
		            + "   and b.ContPlanCode = '" + contPlanCode + "' "
		            + "   and b.RiskCode = '" + riskCode + "' " 
		            + "   and a.FeeOperaTionType in ('ZA','ZE') ";
		  rs = easyExecSql(tSql);
		  if(rs && rs[0][0] != "" && rs[0][0] != "null")
		  {
		    zfMoney = rs[0][0];
		  }
	  }
	  var sumActuPayMoney = parseFloat(bfMoney)+parseFloat(zfMoney);
	  LCGrpPolGrid.setRowColDataByName(i, "SumActuPayMoney", String(sumActuPayMoney));
	  
	  //正常退保金
	  sql = "select abs(sum(GetMoney)) "
	        + "from LPBudgetResult "
	        + "where EdorNo = '" + fm.EdorNo.value + "' "
	        + "   and EdorType = '" + fm.EdorType.value + "' "
	        + "   and GrpContNo = '" + fm.GrpContNo.value + "' "
	        + "   and ContPlanCode = '" + contPlanCode + "' "
	        + "   and RiskCode = '" + riskCode + "' ";
	  
	  rs = easyExecSql(sql);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    LCGrpPolGrid.setRowColDataByName(i, "GetMoney", rs[0][0]);
	  }
	  
	  //协议退保金
	  sql = "select abs(decimal(edorValue, 20, 2)) "
	        + "from LPEdorEspecialData "
	        + "where EdorNo = '" + fm.EdorNo.value + "' "
	        + "   and EdorType = '" + fm.EdorType.value + "' "
	        + "   and DetailType = 'XTFEEG' "
	        + "   and PolNo = '" + contPlanCode + "," + riskCode + "' ";
	  rs = easyExecSql(sql);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    LCGrpPolGrid.setRowColDataByName(i, "XTFee", rs[0][0]);
	    
	    //若计算了协议退费，则选中险种
	    LCGrpPolGrid.checkBoxSel(i + 1);
	  }
	  
	  //协议退保比例
	  sql = "select edorValue "
	        + "from LPEdorEspecialData "
	        + "where EdorNo = '" + fm.EdorNo.value + "' "
	        + "   and EdorType = '" + fm.EdorType.value + "' "
	        + "   and DetailType = 'XTFEERATEG' "
	        + "   and PolNo = '" + contPlanCode + "," + riskCode + "' ";
	  rs = easyExecSql(sql);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    LCGrpPolGrid.setRowColDataByName(i, "XTFeeRate", rs[0][0]);
	  }
	}
}

//退保试算
function edorCalZTTest()
{
  var sql = "select 1 from LCGrpPol where GrpContNo = '" + fm.GrpContNo.value + "' "
          + "and riskcode = '590206' with ur ";
  if(easyQueryVer3(sql))
  {
    alert("该保单含有建工险种，不需要退保试算！");
    return false;
  }
  win = window.open("../bq/GEdorBudgetMain.jsp?grpContNo=" + fm.GrpContNo.value
                      + "&edorNo=" + fm.EdorNo.value
                      + "&edorType=" + fm.EdorType.value
                      + "&edorValiDate=" + fm.EdorValiDate.value,
                    "GEdorCalZTTest");
  win.focus();
}

//把退保试算数据显示在协议退保明细页面
function setResultOnXTDetail()
{
  for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
  {
    var contPlanCode = LCGrpPolGrid.getRowColDataByName(i, "ContPlanCode");
    var riskCode = LCGrpPolGrid.getRowColDataByName(i, "RiskCode");
    var sql = "  select sum(getMoney) "
              + "from LPBudgetResult "
              + "where GrpContNo = '" + fm.GrpContNo.value + "' "
              + "   and edorNo = '" + fm.EdorNo.value + "' "
              + "   and edorType = '" + fm.EdorType.value + "' "
              + "   and ContPlanCode = '" + contPlanCode + "' "
              + "   and RiskCode = '" + riskCode + "' ";
    var result = easyExecSql(sql);
    if(result != null && result[0][0] != "" && result[0][0] != "null")
    {
      var money = Math.abs(result);
    	//退费为负，所以要转换
      LCGrpPolGrid.setRowColDataByName(i, "GetMoney", String(money));
    }
    else
    {
      LCGrpPolGrid.setRowColDataByName(i, "GetMoney", "0");
    }
  }
}

//响应算费事件
function calFee()
{
  var selectCount = 0;
  
  for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
  {
    if(LCGrpPolGrid.getChkNo(i))
    {			
      selectCount = selectCount + 1;
      
      var getMoney = LCGrpPolGrid.getRowColDataByName(i, "GetMoney");
      var xtMoney = LCGrpPolGrid.getRowColDataByName(i, "XTFee");
      var xtProportion = LCGrpPolGrid.getRowColDataByName(i, "XTFeeRate");
      
      if(getMoney == "")
      {
        alert("请先退保试算");
        return false;
      }
      
      if(getMoney == "0")
      {
        LCGrpPolGrid.setRowColDataByName(i, "XTFeeRate", "-");
        continue;
      }
      
      if(xtMoney == "" && xtProportion == "")
      {
        alert("必须填协议退费金额和协议退费比例其中一项");
        return false;
      }
      
      //协议退保金不能超过总保费的110%
      //得到险种总实交保费
      var sumActuPayMoney = LCGrpPolGrid.getRowColDataByName(i, "SumActuPayMoney");
      
      //if(sumActuPayMoney == null || sumActuPayMoney == "" || sumActuPayMoney == "0")
      //{
      //  LCGrpPolGrid.setRowColDataByName(i, "XTFee", "0");
      //}
      //else if(xtMoney != "")
      if(xtMoney != "")
      {
        proportion = parseFloat(xtMoney) / parseFloat(sumActuPayMoney);
	      if(proportion > 1.1)
	      {
	        alert("协议退保比例不能超过总保费的110%");
	        return false;
	      }
	      
	      xtProportion = pointFour(parseFloat(xtMoney) / getMoney);
        LCGrpPolGrid.setRowColDataByName(i, "XTFeeRate", String(xtProportion));
      }
      else if(xtProportion != "")
      {
        var money = xtProportion * getMoney ;
        
      	if(money > sumActuPayMoney * 1.1)
	      {
	        alert("协议退保比例不能超过总保费的110%");
	        return false;
	      }
        xtMoney = pointTwo(getMoney * parseFloat(xtProportion));
        //pointTwo 不是按照4舍5入来规格化 ，所以重写一个。qulq 061205 
       // LCGrpPolGrid.setRowColDataByName(i, "xtMoney", pointTwo(xtMoney));
       LCGrpPolGrid.setRowColDataByName(i, "XTFee", String(xtMoney));
      }
    }
  }
  
  if(selectCount == 0)
  {
    alert("请选择险种");
    return false;
  }
  
  return true;
}

//20081223 zhanggm 590206、190106的正常解约金额直接显示实交保费金额
function setRisk590206()
{
  var sql = "select 1 from LCGrpPol where GrpContNo = '" + fm.GrpContNo.value + "' "
          + "and riskcode = '590206' with ur ";
  if(easyQueryVer3(sql))
  {
    for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
	{
      var contPlanCode = LCGrpPolGrid.getRowColDataByName(i, "ContPlanCode");
      var riskCode = LCGrpPolGrid.getRowColDataByName(i, "RiskCode");
	  //实收保费
	  sql = "select sum(a.SumActuPayMoney) "
	      + "from LJAPayPerson a, LCPol b "
	      + "where a.PolNo = b.PolNo "
	      + "   and a.GrpContNo = '" + fm.GrpContNo.value + "' "
	      + "   and b.ContPlanCode = '" + contPlanCode + "' "
	      + "   and b.RiskCode = '" + riskCode + "' ";
	  var rs = easyExecSql(sql);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    LCGrpPolGrid.setRowColDataByName(i, "GetMoney", rs[0][0]);
	  }
	}
  }
}


