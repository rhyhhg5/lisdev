<%
//程序名称：PEdorAcptAppConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%    
    String flag = "";
    String content = "";
    
    GlobalInput tG = (GlobalInput)session.getValue("GI");
		LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
    tLPEdorAppSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));   
              
    VData tVData = new VData();
			flag = "Succ";
     	content = "保全理算成功。";
       
	    //生成打印数据
	    tVData = new VData();
			tVData.add(tG);
			PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
			        tLPEdorAppSchema.getEdorAcceptNo());
			if (!tPrtAppEndorsementBL.submitData(tVData, ""))
			{
				flag = "Fail";
				content = "保全理算成功。但生成理算结果预览失败！原因是：" + tPrtAppEndorsementBL.mErrors.getFirstError();
			}
		
    content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	var mLoadFlag = "<%=request.getParameter("LoadFlag")%>";
	if (mLoadFlag == "TASKFRAME")
	{
		parent.fraInterface.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
	}
	else
	{
		parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
	}
</script>
</html>

