<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：limanagecomInput.jsp
 //程序功能：广东卡单流转查询
 //创建日期：2011/11/10
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
	<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
  
 	%>
<script>
  var comcode = "<%=tGI1.ComCode%>";
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="limanagecom.js"></SCRIPT>
  <%@include file="limanagecomInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./limanagecomSave.jsp" method=post name=fm target="fraSubmit">
<Div id= "divLLReport1" style= "display: ''">
  	<table>
    	<tr>
    		 <td class= titleImg>查询条件</td>   		 
    	</tr>
    </table>
	<table  class= common>
        <TR  class= common>
			<TD  class= title>SAP管理机构</TD>
			<TD  class= input>
			  <Input class= "common"  name='sapcom'>
			</TD>
	     	<td class="title">核心管理机构</td>
		    <td class="input">
			<Input class="codeNo" name="ManageCom" size="8"
			ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,' char(length(trim(comcode))) ',1);" verify="管理机构|notnull"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,' char(length(trim(comcode))) ',1);" 
			/><input class="codename" name="ManageComName"  />
		</td>
  </tr>
	</table> 
	<br>
	<input class="cssButton" type=button value="查  询" onclick="CardQuery();">
	<br><br>
	<table>
		<tr>
			<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,manageGridhidden);">
			</td>
			<td class= titleImg>SAP与核心系统管理机构映射查询</td>
		</tr>
	</table>
	<Div  id= "manageGridhidden" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanmanagecomGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 					
	</Div>
	
	
	<table  class= common>
    	<TR  class= common>
			<TD class= title>
				<INPUT class = cssButton name=uploadbutton VALUE="导出Excel"  TYPE=button onclick="upload();" >  
			</TD>			
			<TD class= title></TD>
			<TD class= input></TD>
			<TD class= title></TD>
			<TD class= input></TD>
		</TR>
    </table>
	<hr></hr>
	<input type=hidden name=querysql>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
