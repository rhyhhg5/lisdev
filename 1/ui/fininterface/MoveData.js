//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();         //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";
var Action;
var tRowNo=0;

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  //alert("test_test");
  if (FlagStr == "Fail" )
  {
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

function initQuery() {

//  var strSql = "select batchno, makedate||' '||maketime from lidistilllog where batchno not in(select batchno from litranlog where flag='3') and batchno <> 'FinInterFaceService' and managecom like '" + fm.cManageCom.value +"%%'";            
/*  var strSql = "select batchno, makedate || ' ' || maketime, startdate || '至' || enddate, operator,"+
							" to_char(nvl((select sum(sumactumoney) from liaboriginaldata where batchno = lidistilllog.batchno),0),'FM999999999990.00') as moneyinfo" +
  						" from lidistilllog" +
 							" where batchno not in (select batchno from litranlog where flag = '3') and batchno <> 'FinInterFaceService'" +
   						" and managecom like '" + fm.cManageCom.value +"%%'";
*/
		var strSql = "select a.batchno,  char(a.makedate) || ' ' || char(a.maketime), char(a.startdate) || '至' || char(a.enddate),a.operator,"
								+" nvl(sum(b.sumactumoney),0) as moneyinfo"
								+" from lidistilllog a,liaboriginaldata b"
								+" where a.batchno = b.batchno "
								+" and a.batchno not in (select batchno from litranlog where flag = '3') and a.batchno <> 'FinInterFaceService'"
								+" and a.managecom like '" + fm.cManageCom.value +"%%'"
								+" group by a.batchno,a.makedate,a.maketime,a.startdate,a.enddate,a.operator,a.managecom"
								+" order by batchno";

  turnPage.queryModal(strSql, MoveDataGrid);
} 
 
function SubmitForm()
{
  if(MoveDataGrid.getSelNo()) { 
    fm.all("sBatchNo").value = MoveDataGrid.getRowColData(MoveDataGrid.getSelNo()-1, 1);
    //fm.all("fmtransact").value = "create"    
     var i = 0;
     var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
//     fm.target = "_blank";
     fm.submit(); //提交
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}
