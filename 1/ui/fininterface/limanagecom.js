//该文件中包含客户端需要处理的函数和事件

//程序名称：ligdcardview.js
//程序功能：元数据定义
//创建日期：2011/11/10
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

function ManagecomName()
{
	var checksql = "select name from ldcom where comcode='"+comcode+"' " ;	
	var result=easyExecSql(checksql);
	fm.ManageComName.value = result ;
}

//查询按钮
function CardQuery(){		  	  		 
	var strSQL = "";
	var sapcomname=fm.sapcom.value.replace(/(^\s*)|(\s*$)/g,"");
	var ManageComname=fm.ManageCom.value.replace(/(^\s*)|(\s*$)/g,"");
	if(sapcomname.length>4)
	{
		alert('SAP机构代码不能大于4位，请重新输入');
		fm.sapcom.value='';
		return false;
	}
	strSQL = "select codealias,code,codename from licodetrans"
	         +" where codetype='ManageCom' and length(code)<>4";
	if(sapcomname.length>0 && sapcomname.length != "")
	{
	    strSQL=strSQL+" and codealias like '"+sapcomname+"%'";
	}
	if(ManageComname.length>0 && ManageComname.length != "")
	{
	    strSQL=strSQL+" and code='"+ManageComname+"'";
	}
	strSQL=strSQL+' with ur'
	turnPage.queryModal(strSQL, managecomGrid);
	if(managecomGrid.mulLineCount=="0"){
		alert("没有查询到数据");
	}	
	fm.querysql.value=strSQL;
}

//导出Excel按钮
function upload(){

if(managecomGrid.mulLineCount=="0"){
		alert("没有查询到数据");
		return false;
	} 	 	
	fm.action="./limanagecomtoexcel.jsp";
	fm.target="fraSubmit";
	fm.submit(); //提交
}


//提交前的校验、计算  
function beforeSubmit(){
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}
