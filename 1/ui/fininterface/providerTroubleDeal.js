//程序名称：ShieldBorUnevenInput.js 
//程序功能：接口表数据维护
//创建日期：2015-06-03 
//创建人  ：m
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();//日结试算相关数据
var turnPage2 = new turnPageClass();//日结确认相关数据
var mFlag = "0";
var showInfo;
var mOperate="";


function DataQuery()
{ 	
	 if(fm.Accountdate.value == null||fm.Accountdate.value=='')
     {alert("请选择记账日期！！！");
     return false;
  }
	 var i = 0;
	 var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	 
    initDataGrid();
    var sql="";
	
	 if(fm.agentcom.value == null||fm.agentcom.value=='')
	    {
		sql = "select indexno,Accountdate ,sumactumoney,managecom,agentcom from FiabStandarddata a where bustypeid in('YF-SX-000002','YF-SX-000001','YF-SX-000003','YF-SX-000004','F-CG-000001','DF-CG-000001','DF-CG-000002','F-CG-000002','F-CG-000003','F-CG-000004')and db2inst1.LF_getBankCodeForAgentCom(agentcom,'A',managecom) is not null and Accountdate ='"+fm.all('Accountdate').value+"' and exists(select 1 from fivoucherdatadetail b where a.serialno=b.serialno and checkflag='01' and Accountdate = '"+fm.all('Accountdate').value+"')"
		}else{
		sql = "select indexno,Accountdate ,sumactumoney,managecom,agentcom from FiabStandarddata a where (bustypeid in('YF-SX-000002','YF-SX-000001','YF-SX-000003','YF-SX-000004','F-CG-000001','F-CG-000003') or (bustypeid in ('DF-CG-000001','DF-CG-000002') and costid='F000000000P')) and db2inst1.LF_getBankCodeForAgentCom(agentcom,'A',managecom) is not null  and agentcom='"+fm.all('agentcom').value+"' and Accountdate ='"+fm.all('Accountdate').value+"' and exists(select 1 from fivoucherdatadetail b where a.serialno=b.serialno and checkflag='01' and Accountdate = '"+fm.all('Accountdate').value+"')"
		}
	 turnPage.pageDivName = "divPage";
     turnPage.queryModal(sql,DataGrid); 
    	 
    	 afterSubmitl();
 
    return false;
  }


function DataEmpty()
{
	if(!beforesubmit()){
		return false;
	};
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
    fm.action="./DataSave.jsp" 
    fm.submit(); //提交
      
	}


function InterEmpty()
{
	if(!beforesubmit2()){
		return false;
	};
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
    fm.action="./InterfactableDataSave.jsp" 
    fm.submit(); //提交
      
	}


function beforesubmit(){
	 if(fm.agentcom.value == null||fm.agentcom.value=='')
     {alert("请输入中介机构！！！");
     return false;
  } 
	 if(fm.Accountdate.value == null||fm.Accountdate.value=='')
     {alert("请选择记账日期！！！");
     return false;
  }
	 return true;
}

function BankSelect() {
	var tSel = DataGrid.getSelNo();
	try
	{
		fm.all('agentcom').value = DataGrid.getRowColData(tSel-1, 5);
	    fm.all('Accountdate').value= DataGrid.getRowColData(tSel-1,2);

	}
	catch(ex)
	{
		alert( "读取数据错误,请刷新页面再使用。" + ex );
	}
	}


function beforesubmit2(){
	if(fm.batchno.value == null||fm.batchno.value=='')
    {alert("请输入批次！！！");
    return false;
 } 
	if(fm.chargedate.value == null||fm.chargedate.value=='')
    {alert("请输入记账日期！！！");
    return false;
 } 
	if(fm.vouchertype.value == null||fm.vouchertype.value=='')
    {alert("请输入凭证类型！！！");
    return false;
 } 
	if(fm.agentno.value == null||fm.agentno.value=='')
    {alert("请输入错误供应商号码！！！");
    return false;
 } 
	if(fm.serialno.value == null||fm.serialno.value=='')
    {alert("请输入流水号码！！！");
    return false;
 } 
	if(fm.depcode.value == null||fm.depcode.value=='')
    {alert("请输入四位机构！！！");
    return false;
 } 
	if(fm.Style.value == null||fm.Style.value=='')
    {alert("请选择数据维护类型！！！");
    return false;
 } 
	return true;
}







function InterQuery()
{
	 if(fm.batchno.value == null||fm.batchno.value=='')
     {alert("请输入批次号码！！！");
     return false;
  }
	 if(fm.chargedate.value == null||fm.chargedate.value=='')
     {alert("请选择记账日期！！！");
     return false;
  }
	 if(fm.vouchertype.value == null||fm.vouchertype.value=='')
     {alert("请输入凭证类型！！！");
     return false;
  }
	 var i = 0;
	 var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	 
    initInterfaceDataGrid();
    var sql="";

	    sql = "select serialno,chargedate,depcode,vouchertype,batchno,summoney,agentno from interfacetable where batchno='"+fm.all('batchno').value+"' and voucherType='"+fm.all('vouchertype').value+"' and chargedate='"+fm.all('chargedate').value+"' and ReadState='3'";
	    if(fm.serialno.value == null||fm.serialno.value==''){
	    	sql=sql;
	    }else{sql=sql+" and serialno='"+fm.all('serialno').value+"'";}
	    if(fm.agentno.value == null||fm.agentno.value==''){
	    	sql=sql;
	    }else{sql=sql+" and agentno='"+fm.all('agentno').value+"'";}
//	 alert(sql);
	 turnPage2.pageDivName = "divPage2";
     turnPage2.queryModal(sql,InterfaceDataGrid); 
    	 
    	 afterSubmit2();
 
    return false;
  }



function AgentnoSelect() {
	var mSel = InterfaceDataGrid.getSelNo();
	try
	{
		fm.all('batchno').value = InterfaceDataGrid.getRowColData(mSel-1, 5);
	    fm.all('chargedate').value= InterfaceDataGrid.getRowColData(mSel-1,2);
	    fm.all('serialno').value= InterfaceDataGrid.getRowColData(mSel-1,1);
	    fm.all('vouchertype').value= InterfaceDataGrid.getRowColData(mSel-1,4);
	    fm.all('agentno').value= InterfaceDataGrid.getRowColData(mSel-1,7);
	    fm.all('depcode').value= InterfaceDataGrid.getRowColData(mSel-1,3);

	}
	catch(ex)
	{
		alert( "读取数据错误,请刷新页面再使用。" + ex );
	}
	}



/*********************************************************************
 *  后台执行完毕反馈信息
 *  描述: 后台执行完毕反馈信息
 *********************************************************************/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,cOperate )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
}
function afterSubmitl( FlagStr, content )
{
  showInfo.close();
  if (!turnPage.strQueryResult )
  {        
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "没有查询到数据" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
  }
  else
  { 
  	if(turnPage.strQueryResult){
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "查询成功" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  } 
  }
}


function afterSubmit2( FlagStr, content )
{
  showInfo.close();
  if (!turnPage2.strQueryResult )
  {        
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "没有查询到数据" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
  }
  else
  { 
  	if(turnPage2.strQueryResult){
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "查询成功" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  } 
  }
}




