<html>
<%
//程序名称 :IntFaceTabDataInput.jsp 
//程序功能 :维护接口表数据
//创建人 :
//创建日期 :2015-06-03 
//
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。  
 %>
<script>
  var comcode = "<%=tGI1.ComCode%>";
 

</script>
<head >
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
<SCRIPT src = "IntFaceTabDataInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="IntFaceTabDataInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./IntFaceTabDataSave.jsp" method=post name=fm target="fraSubmit">
<Div id= "divLLReport1" style= "display: ''">
<strong><IMG id="a1" src="../common/images/butExpand.gif"
style="cursor:hand;" OnClick="showPage(this,divOperator);" />接口表数据状态维护</strong>
  	<table>
    	<tr>
    		 <td class= titleImg>请输入数据维护条件</td>   		 
    	</tr>
    </table>

 <table class= common border=0 width=100%>
        <TR  class= common>
		<TD class=title>批次号</TD>
		<TD class=input><Input class="common" name=BatchNo verify="批次号|notnull" elementtype=nacessary type="text"></TD>

		<TD class=title>4位机构</TD>
		<TD class=input><Input class="common" name=DepCode></TD>
		
		<TD class=title>凭证类型</TD>
		<TD class=input><Input class="common" name=VoucherType></TD>
        </TR> 
      	<TR  class= common>	
		<TD class=title>记账日期</TD>
		<TD class=input><Input class="coolDatePicker"
			verify="记账日期|notnull&date" dateFormat="short" name=ChargeDate elementtype=nacessary></TD>	
			
										
		<TD class=title>读取状态</TD>
		<TD class=input><Input class=codeno name=ReadState
			CodeData="0|^1|读取数据|M^2|读取成功|M^3|读取失败|M^4|尚未读取" verify="校检标记"
			ondblclick="showCodeListEx('ReadState',[this,ReadStateName],[0,1]);"
			onkeyup="showCodeListKeyEx('ReadState',[this,ReadStateName],[0,1]);"><input
			class=codename name=ReadStateName readonly=true elementtype=nacessary></TD>
			
		<TD class=title>流水号</TD>
		<TD class=input><Input class="common" name=SerialNo></TD>	
			
        </TR>  
            
 </table> 
 <br>
 <INPUT VALUE="查询数据" class=cssButton TYPE=button onclick="Query();">
 <INPUT VALUE="置空数据" class=cssButton TYPE=button onclick="EmptyData();">
 <INPUT VALUE="屏蔽数据" class=cssButton TYPE=button onclick="ShieldData();">
 <br><br/><font color="#ff0000">说明：(1)置空数据是将读取状态为1-读取数据，2-读取成功，3-读取失败数据的“读取状态”，“读取日期”，“读取时间”，“凭证年度”，“回写凭证日期”，“回写凭证时间”，“凭证号”置空。 </font>
<br><br/><font color="#ff0000">说明：(2)屏蔽数据是将读取状态为4-尚未读取数据的“读取状态”置为3-读取失败。 </font>
 <INPUT class=common name=mOperate TYPE=hidden >
    </TR>
    

    
<hr width=98%>

<br>

	<Div  id= "divIntFaceTabDataGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanIntFaceTabDataGrid" >
  					</span> 
  			  	</td>
  			</tr>					
</Div>

</table>	
      <input class=cssButton value="首页"    type=button  onclick="turnPage.firstPage();"></input>
      <input class=cssButton value="上一页"  type=button  onclick="turnPage.previousPage();"></input>
      <input class=cssButton value="下一页"  type=button  onclick="turnPage.nextPage();"></input>
      <input class=cssButton value="尾页"    type=button  onclick="turnPage.lastPage();"></input>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>