//程序名称：ShieldBorUnevenInput.js
//程序功能：屏蔽数据
//创建日期：2015-06-03 
//创建人  ：m
//更新记录：  更新人    更新日期     更新原因/内容

var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var mode_flag;
var tAppealFlag="0";
var turnPage = new turnPageClass();        
var turnPage1 = new turnPageClass();

/****************************************************
*导出相关数据财务接口报表分明细和期间的统计
*********************************************************/ 
function ShieldBor()
{ 	 
 if(!beforeSubmit())
  {
    return false;
  }
 if(fm.CheckF.value == null||fm.CheckF.value==''){
		alert("请选择您想要将数据维护成的状态，谢谢！");
		return false;
	}
	submitForm();
//	fm.action="./ShieldBorUnevenSave.jsp ";
//	fm.submit(); //提交

}

function Query()
{ 	 
	 if(!beforeSubmit())
  {
    return false;
  }
	 var i = 0;
	 var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	 
    initShieldBorUnevenGrid();
    var sql="";
	sql = "select batchno,VoucherType ,AccountCode,FinItemType,SumMoney,AccountDate,ManageCom ,RiskCode,CostCenter,ContNo, checkflag from FIVoucherDataDetail where batchno = '"+fm.all('BatchNo').value+"'"
	       +" and accountdate = '"+fm.all('AccountDate').value+"' and contno = '"+fm.all('ContNo').value+"'"
	      +getWherePart('CheckFlag','CheckF')
	      +getWherePart('ManageCom','ManageCom')
	      +getWherePart('VoucherType','VoucherType');
	
     turnPage.queryModal(sql,ShieldBorUnevenGrid); 
     afterSubmitl();
    return false;
  }
function beforeSubmit()
{
	if(fm.BatchNo.value == null||fm.BatchNo.value==''){
		alert("请输入您想要维护数据的批次号，谢谢！");
		return false;
	}
	
	if(fm.ContNo.value == null||fm.ContNo.value==''){
		alert("请输入您想要维护数据的保单号，谢谢！");
		return false;
	}	
	
	if(fm.AccountDate.value == null||fm.AccountDate.value==''){
		alert("请选择您想要维护数据的记账日期，谢谢！");
		return false;
	}
	
	return true;
}
/************************
*PDF打印功能实现 
***************************/
function submitForm()
{
 
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交

      
	}

/*********************************************************************
 *  后台执行完毕反馈信息
 *  描述: 后台执行完毕反馈信息
 *********************************************************************/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
}

function afterSubmitl( FlagStr, content )
{
  showInfo.close();
  if (!turnPage.strQueryResult )
  {        
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "没有查询到数据" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
  }
  else
  { 
  	if(turnPage.strQueryResult){
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "查询成功" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  } 
  }
}
// add 2015-06-03 
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function BankSelect() {
//       alert('oooo');
		var tSel = ShieldBorUnevenGrid.getSelNo();
		try
		{
			fm.all('ManageCom').value = ShieldBorUnevenGrid.getRowColData(tSel-1, 7);
//			alert (fm.all('Codea').value);
			fm.VoucherType.value = ShieldBorUnevenGrid.getRowColData(tSel-1,2);// 业务名称
		    fm.all('CheckF').value= ShieldBorUnevenGrid.getRowColData(tSel-1,11);// 编码		   
		}
		catch(ex)
		{
			alert( "读取数据错误,请刷新页面再使用。" + ex );
		}
		}