<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：ligdcardviewInput.jsp
 //程序功能：广东卡单流转查询
 //创建日期：2011/11/10
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
	<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
  
 	%>
<script>
  var comcode = "<%=tGI1.ComCode%>";
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="ligdcardview.js"></SCRIPT>
  <%@include file="ligdcardviewInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./ligdcardviewSave.jsp" method=post name=fm target="fraSubmit">
<Div id= "divLLReport1" style= "display: ''">
  	<table>
    	<tr>
    		 <td class= titleImg>查询条件</td>   		 
    	</tr>
    </table>
	<table  class= common>
        <TR  class= common>
			<TD  class= title>起始时间</TD>
			<TD  class= input>
			  <Input class= "coolDatePicker" verify="起始时间|notnull&date" dateFormat="short" name=StartDay >
			</TD>
			<TD  class= title>结束时间</TD>
			<TD  class= input>
			  <Input class= "coolDatePicker" verify="结束时间|notnull&date"  dateFormat="short" name=EndDay >
			</TD>
			<TD  class= title></TD>
			<TD  class= input>
			</TD>		
        </TR> 
        <tr class="common">
	    <td class="title">机 构</td>
	    <td class="input">
	    <input class="codeno" name="ManageCom"  ondblclick="return showCodeList(null,[,],[0,1]);" onkeyup="return showCodeListKey(null,[,],[0,1]);" readOnly><input class="codename" name="ManageComName" readOnly></td>  
  </tr>
	</table> 
	<br>
	<input class="cssButton" type=button value="查  询" onclick="CardQuery();">
	<br><br>
	<table>
		<tr>
			<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,cardGridhidden);">
			</td>
			<td class= titleImg>广东卡单查询</td>
		</tr>
	</table>
	<Div  id= "cardGridhidden" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spancardGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 					
	</Div>
	
	
	<table  class= common>
    	<TR  class= common>
			<TD class= title>
				<INPUT class = cssButton name=uploadbutton VALUE="导出Excel"  TYPE=button onclick="upload();" >  
			</TD>			
			<TD class= title></TD>
			<TD class= input></TD>
			<TD class= title></TD>
			<TD class= input></TD>
		</TR>
    </table>
	<hr></hr>
	<input type=hidden name=querysql>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
