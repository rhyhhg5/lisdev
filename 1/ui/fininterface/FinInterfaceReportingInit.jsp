<% 
//程序名称：FinInterfaceReportingInit.jsp
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">                           

function initInpBox()
{
  try
  {
	//查询条件置空
	fm.all('StartDay1').value = '';
	fm.all('EndDay1').value = '';
  }
  catch(ex)
  {
    alert("FinInterfaceReportingInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
   

function initForm()
{
  try
  {
    initInpBox();
    initFinInterfaceGrid();
  }
  catch(re)
  {
    alert("FinInterfaceReportingInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//
function initFinInterfaceGrid()
{                                  
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="操作员";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="运行时间";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="处理笔数";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();
      iArray[5][0]="处理金额";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      FinInterfaceGrid = new MulLineEnter( "fm" , "FinInterfaceGrid" ); 
      //这些属性必须在loadMulLine前
      FinInterfaceGrid.mulLineCount = 5;   
      FinInterfaceGrid.displayTitle = 1;
      FinInterfaceGrid.locked = 1;
      FinInterfaceGrid.canSel = 1;
      FinInterfaceGrid.canChk = 0;
      //FinDayConfGrid.hiddenPlus = 1;
      //FinDayConfGrid.hiddenSubtraction = 1;        
      FinInterfaceGrid.loadMulLine(iArray);  

      //这些操作必须在loadMulLine后面
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>
