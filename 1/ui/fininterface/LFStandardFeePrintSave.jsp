<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：标准保费报表打印
	//程序功能：LFStandardFeePrintSave.jsp
	//创建日期：2008-06-02 
	//创建人  ：yh
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%
	String mDay[] = new String[2];
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	//输出参数
	CError cError = new CError();
	//后面要执行的动作：添加，修改，删除
	boolean operFlag = true;

	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";

	strOperation = request.getParameter("fmtransact");
	mDay[0] = request.getParameter("StartDay");
	mDay[1] = request.getParameter("EndDay");

	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	tVData.addElement(mDay);
	tVData.addElement(tG);
	LFStandardFeePrintBL tLFStandardFeePrintBL = new LFStandardFeePrintBL();
	if (!tLFStandardFeePrintBL.submitData(tVData, "CONFIRM")) {
		operFlag = false;
		mErrors.copyAllErrors(tLFStandardFeePrintBL.mErrors);
		cError.moduleName = "FinPayDayRiskF1PSave";
		cError.functionName = "submitData";
		cError.errorMessage = "LFStandardFeePrintBL发生错误，但是没有提供详细的出错信息";
		mErrors.addOneError(cError);
	}
	mResult = tLFStandardFeePrintBL.getResult();
	XmlExport txmlExport = new XmlExport();
	txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport",0);
	if (txmlExport == null) {
		System.out.println("The XMLExport is null");
	} else {
		out.print(txmlExport.toString());
	}
 
	ExeSQL tExeSQL = new ExeSQL();

	//获取临时文件名
	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	String strFilePath = tExeSQL.getOneValue(strSql);
	String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName() + ".vts";
	//获取存放临时文件的路径
	String strRealPath = application.getRealPath("/").replace('\\', '/');
	String strVFPathName = strRealPath + "/" + strVFFileName;

	CombineVts tcombineVts = null;

	if (operFlag == true) {
		//合并VTS文件
		String strTemplatePath = application.getRealPath("f1print/picctemplate/")+ "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);

		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);

		//把dataStream存储到磁盘文件
		System.out.println("存储文件到" + strVFPathName);
		AccessVtsFile.saveToFile(dataStream, strVFPathName);
		System.out.println("==> Write VTS file to disk ");
		System.out.println("===strVFFileName : " + strVFFileName);
		//本来打算采用get方式来传递文件路径
		//缺点是文件路径被暴露
		System.out.println("---passing params by get method");
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="+ strVFPathName);
	}

%>
