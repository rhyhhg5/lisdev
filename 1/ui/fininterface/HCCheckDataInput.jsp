<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：HCCheckDataInput.jsp
	//程序功能：数据核对
	//创建日期：2017-11-30 
	//创建人  ：弓彩霞
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";   
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var comcode = "<%=tGI.ComCode%>";
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src = "HCCheckData.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file = "HCCheckDataInit.jsp" %>
</head>
<body  onload = "initHCCheckDataForm();">
<form method=post name=fm target="fraSubmit">
<!-- 流水号码 --> 
<input type=hidden name=serialNo>
<input type=hidden name=ExportExcelSQL>
<input type=hidden name=ClassType>

<table><tr> <td class= titleImg>请输入查询条件</td></tr></table> 
<!-- 选择查询的是哪种数据 -->
<Div id="divSearch" style="display: ''">
	<table class=common>
	<TR class=common>
		<TD class=title>核对类型</TD>
		<TD class=input><Input class=codeno name=checkType
			CodeData="0|^1|暂收|M^2|实收|M^3|应付|M^4|实付|M" verify="核对类型|notnull"
			ondblclick="showCodeListEx('checkType',[this,checkTypeName],[0,1]);"
			onkeyup="showCodeListKeyEx('checkType',[this,checkTypeName],[0,1]);">
			<input class=codename name=checkTypeName readonly=true elementtype=nacessary></TD>	
		<TD class=title></TD>	
		<TD class=input></TD>		
		<TD class=title></TD>	
		<TD class=input></TD>			
	</TR>
	</table>	
</Div>

<!-- 暂收 -->
<Div id="divHCCheckData1" style="display:none"> 	
	<table class=common>
	<TR class=common>
		<TD class=title>暂收数据类型</TD>
		<TD class=input><Input class=codeno name=ZSType
			CodeData="0|^1|非异地收费|M^2|异地收费" verify="暂收数据类型|notnull"
			ondblclick="showCodeListEx('ZSType',[this,ZSTypeName],[0,1]);"
			onkeyup="showCodeListKeyEx('ZSType',[this,ZSTypeName],[0,1]);"><input
			class=codename name=ZSTypeName readonly=true elementtype=nacessary></TD>
			</TR>
			<TR calss=common>		
		<TD class=title>起始时间</TD>
		<TD class=input><Input class="coolDatePicker" verify="起始时间|notnull&date" dateFormat="short" name=StartDay elementtype=nacessary></TD>
		<TD class=title>结束时间</TD>
		<TD class=input><Input class="coolDatePicker" verify="结束时间|notnull&date" dateFormat="short" name=EndDay elementtype=nacessary></TD>
		<TD  class= title>管理机构</TD>
      	<TD  class= input><Input class=codeno name=ManageCom verify="管理机构|NOTNULL"  readonly=true
      		ondblclick="return showCodeList('comcode',[this,PolicyComName],[0,1],null,null,null,'1',null);" 
      		onkeyup="return showCodeListKey('comcode',[this,PolicyComName],[0,1],null,null,null,'1',null);"><input 
      		class=codename name=PolicyComName readonly=true elementtype=nacessary></TD>	
	</TR>		
	</table>	
</Div>
	
<!--

<Div id="divHCCheckData2" style="display:none"> 	
	<table class=common>
	<TR class=common>
		<TD class=title>实收类型</TD>
		<TD class=input><Input class=codeno name=SSType
			CodeData="0|^1|非异地收费|M^2|异地收费" verify="实收类型|notnull"
			ondblclick="showCodeListEx('SSType',[this,SSTypeName],[0,1]);"
			onkeyup="showCodeListKeyEx('SSType',[this,SSTypeName],[0,1]);"><input
			class=codename name=SSTypeName readonly=true elementtype=nacessary></TD>		
		<TD class=title>起始时间</TD>
		<TD class=input><Input class="coolDatePicker" verify="起始时间|notnull&date" dateFormat="short" name=SDate2 elementtype=nacessary></TD>
		<TD class=title>结束时间</TD>
		<TD class=input><Input class="coolDatePicker" verify="结束时间|notnull&date" dateFormat="short" name=SDate2 elementtype=nacessary></TD>
	</TR>		
	</table>	
</Div>
<Div id="divHCCheckData3" style="display:none"> 	
	<table class=common>
	<TR class=common>
		<TD class=title>应付类型</TD>
		<TD class=input><Input class=codeno name=YFType
			CodeData="0|^1|非异地收费|M^2|异地收费" verify="应付类型|notnull"
			ondblclick="showCodeListEx('YFType',[this,YFTypeName],[0,1]);"
			onkeyup="showCodeListKeyEx('YFType',[this,YFTypeName],[0,1]);"><input
			class=codename name=YFTypeName readonly=true elementtype=nacessary></TD>		
		<TD class=title>起始时间</TD>
		<TD class=input><Input class="coolDatePicker" verify="起始时间|notnull&date" dateFormat="short" name=SDate3 elementtype=nacessary></TD>
		<TD class=title>结束时间</TD>
		<TD class=input><Input class="coolDatePicker" verify="结束时间|notnull&date" dateFormat="short" name=SDate3 elementtype=nacessary></TD>
	</TR>		
	</table>	
</Div>
<Div id="divHCCheckData4" style="display:none"> 	
	<table class=common>
	<TR class=common>
		<TD class=title>实付类型</TD>
		<TD class=input><Input class=codeno name=SFType
			CodeData="0|^1|非异地收费|M^2|异地收费" verify="实付类型|notnull"
			ondblclick="showCodeListEx('SFType',[this,SFTypeName],[0,1]);"
			onkeyup="showCodeListKeyEx('SFType',[this,SFTypeName],[0,1]);"><input
			class=codename name=SFTypeName readonly=true elementtype=nacessary></TD>		
		<TD class=title>起始时间</TD>
		<TD class=input><Input class="coolDatePicker" verify="起始时间|notnull&date" dateFormat="short" name=SDate4 elementtype=nacessary></TD>
		<TD class=title>结束时间</TD>
		<TD class=input><Input class="coolDatePicker" verify="结束时间|notnull&date" dateFormat="short" name=SDate4 elementtype=nacessary></TD>
	</TR>		
	</table>	
</Div>

-->
<br>
<INPUT VALUE="对账" class=cssButton TYPE=button onclick="HCCheckData();">
<br>
<hr width=98%>
<br>
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divAllGrid);"></td>
		<td class=titleImg>查询结果</td>
	</tr>
</table>

<Div id="divAllGrid" style="display: ''" align=center>
<table class=common> <tr class=common><td text-align: left colSpan=1> <span id="spanHCCheckDataGrid"> </span></td></tr></table>
<INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
<INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
</DIV>
</form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
