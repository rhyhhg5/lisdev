<%@page contentType="text/html;charset=GBK" %>
<%
 String CurrentDate = PubFun.getCurrentDate();
%>
<!-- 
文件名：CheckCountMoney.jsp
功能：实现对数据的一种汇总核对
-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<html>
<head>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="CheckCountMoney.js"></SCRIPT>
  <title>财务数据校验</title>
<%@include file="CheckCountMoneyInit.jsp"%>
</head>
<body onload = "initForm();">
<form method=post name=fm action= "./CheckCountMoneySave.jsp" target="fraSubmit">
  <!-- 机构默认 值 -->
  <input type=hidden name=sComCode>
  <table class= common border=0 width=100%>
     <tr>
		<td class= titleImg align= center>请输入校验信息</td>
     </tr>
  </table>
   <table class= common align=center>
      	<!-- 
      	<tr>
      	<TD class=title>校验类型</TD>
		  <TD class=input><Input class=codeno name=CheckType verify="校验类型|NOTNULL"
      		ondblclick="return showCodeList('checktype',[this,checktypename],[0,1],null,null,null,'1',null);" 
      		onkeyup="return showCodeListKey('checktype',[this,checktypename],[0,1],null,null,null,'1',null);"><input 
      		class=codename name=checktypename readonly=true elementtype=nacessary></TD>
      	<TD class=title>机构</TD>
		  <TD class=input><Input class=codeno name=ManageCom verify="机构|NOTNULL"
      		ondblclick="return showCodeList('comcode',[this,PolicyComName],[0,1],null,null,null,'1',null);" 
      		onkeyup="return showCodeListKey('comcode',[this,PolicyComName],[0,1],null,null,null,'1',null);"><input 
      		class=codename name=PolicyComName readonly=true elementtype=nacessary></TD>
      	</tr>
      	 -->
      	<TR  class= common>
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate value = '<%=CurrentDate%>'>
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate value = '<%=CurrentDate%>'>
          </TD>		
        </TR>
		</table>
		<br>		
	<table class= common align=center>
	    <TR  class= common>
        <TD>
          <INPUT  class=cssButton VALUE="校  验" TYPE=Button onclick="SubmitForm()">
        </TD>
      </TR>
  </table>
  <br>
<Div id="selClassType" style="display:''" align=center>
<table class=common align=center>
	<tr class=common>
		<td text-align: left colSpan=1><span id="spanClassTypeGrid"> </span></td>
	</tr>
</table>
</Div>

    <Div id= "divPage1" align=center style= "display: '' ">
    	<center>
    		<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    		<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
		</center>
    </Div> 

</form>

<span id="spanCode" style="display: none; position:absolute; slategray"></span>

</body>
</html>