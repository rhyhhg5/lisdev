<%
	//程序名称：HCCheckDataInput.jsp
	//程序功能：数据核对
	//创建日期：2017-11-30 
	//创建人  ：弓彩霞
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
	function initInpBox() {
		try {
			//查询条件置空
			fm.all('checkType').value = '';
			fm.all('checkTypeName').value = '';
			fm.all('StartDay').value = '';
			fm.all('EndDay').value = '';
		} catch (ex) {
			alert("TestInterfaceNotRunInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	function initHCCheckDataForm() {
		try {
			initInpBox();
			//initCodeQuery();
			initHCCheckDataGrid(); //初始化共享工作池
		} catch (re) {
			alert("TestInterfaceNotRunInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
//由于下拉列表的东西太多所以将查询的结果以数组拼接的方式显示在下拉列表里面
function initCodeQuery() {
	var strQueryResultL = easyQueryVer3(
			"select classtype,classtypename from liclasstypekeydef order by classtype",
			1, 1, 1);
	var tArrayL = decodeEasyQueryResult(strQueryResultL);
	var tDS = "0|^";
	for (i = 0; i < tArrayL.length; i++) {
		tDS = tDS + tArrayL[i][0] + "|" + tArrayL[i][1] + "|M";
		if ((i + 1) != tArrayL.length) {
			tDS = tDS + "^";
	}
	}
	fm.all("").CodeData = tDS;
}

	function initHCCheckDataGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; 
			iArray[0][1] = "38px"; 
			iArray[0][2] = 10; 
			iArray[0][3] = 0; 

			iArray[1] = new Array();
			iArray[1][0] = "业务号"; 
			iArray[1][1] = "140px"; 
			iArray[1][2] = 170; 
			iArray[1][3] = 0; 

			iArray[2] = new Array();
			iArray[2][0] = "保单号"; 
			iArray[2][1] = "65px"; 
			iArray[2][2] = 100; 
			iArray[2][3] = 0; 

			iArray[3] = new Array();
			iArray[3][0] = "管理机构"; 
			iArray[3][1] = "60px"; 
			iArray[3][2] = 100; 
			iArray[3][3] = 0;  

			iArray[4] = new Array();
			iArray[4][0] = "对方机构"; 
			iArray[4][1] = "60px"; 
			iArray[4][2] = 100; 
			iArray[4][3] = 0; 

			iArray[5] = new Array();
			iArray[5][0] = "金额"; 
			iArray[5][1] = "120px"; 
			iArray[5][2] = 100; 
			iArray[5][3] = 0; 

			iArray[6] = new Array();
			iArray[6][0] = "记账日期"; 
			iArray[6][1] = "120px"; 
			iArray[6][2] = 100; 
			iArray[6][3] = 0;
			
			iArray[7] = new Array();
			iArray[7][0] = "数据错误原因"; 
			iArray[7][1] = "150px"; 
			iArray[7][2] = 100; 
			iArray[7][3] = 0;

			HCCheckDataGrid = new MulLineEnter("fm", "HCCheckDataGrid");
			//这些属性必须在loadMulLine前
			HCCheckDataGrid.mulLineCount = 10;
			HCCheckDataGrid.displayTitle = 1;
			HCCheckDataGrid.locked = 1;
			HCCheckDataGrid.canSel = 1;
			HCCheckDataGrid.canChk = 0;
			HCCheckDataGrid.loadMulLine(iArray);
		} catch (ex) {
			alert(ex);
		}
	}
</script>


