<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<!--用户校验类-->
<jsp:directive.page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"/>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.fininterface.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.LDFinBankDB"%>
<%@page import="com.sinosoft.lis.fininterface.InspeDimenConfigUI"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  FICodeTransSchema tFICodeTransSchema = new FICodeTransSchema();
  CErrors tError = null;
  InspeDimenConfigUI tInspeDimenConfigUI = new InspeDimenConfigUI();
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  System.out.println("save页面的tG值为："+tG);
  VData tVData = new VData();
  
  //输出参数
  String FlagStr = "";
  String Content = "";  
  String transact = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println(transact);

  
//动作为添加。
  if (transact.equals("INSERT")) 
  {
	  tFICodeTransSchema.setCodeName(request.getParameter("CodeName"));
	  tFICodeTransSchema.setCode(request.getParameter("Code"));
	  System.out.println("save页面CodeName为："+tFICodeTransSchema.getCodeName());
	  System.out.println("save页面Code为："+tFICodeTransSchema.getCode());
	  System.out.println("save页面OtherSign为："+tFICodeTransSchema.getOtherSign());
	  System.out.println("save页面VersionNo为："+tFICodeTransSchema.getVersionNo());
	   try
	   {
		   tVData.add(tFICodeTransSchema);
		   tVData.add(tG);
		   tInspeDimenConfigUI.submitData(tVData,transact);
	   }
	 catch(Exception ex)
	    {
	      Content = "保存失败，原因是:" + ex.toString();
	      System.out.println("错误信息"+ex.toString());
	      FlagStr = "Fail";
	    }
	    if (FlagStr=="")
	    {
		    tError = tInspeDimenConfigUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="保存成功！";
		    	FlagStr = "Succ";
		    	tVData.clear();
				tVData = tInspeDimenConfigUI.getResult();
		    }
		    else                                                                           
		    {
		    	Content = "保存失败，原因是:" + tError.getLastError();
		    	FlagStr = "Fail";
		    }
	     }	
	    
  }

  if(transact.equals("DELETE"))
  {
	  try{
	  System.out.println("transact的值等于DELETE");
	  tFICodeTransSchema.setCodeName(request.getParameter("CodeName"));
	  tFICodeTransSchema.setCode(request.getParameter("Code"));
	  tVData.add(tFICodeTransSchema);		   
	  tVData.add(tG);
	  tInspeDimenConfigUI.submitData(tVData,transact);
	  }
	  catch(Exception ex)
	    {
	      Content = "删除失败，原因是:" + ex.toString();
	      System.out.println("错误信息"+ex.toString());
	      FlagStr = "Fail";
	    }
	    if (FlagStr=="")
	    {
		    tError = tInspeDimenConfigUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="删除成功！";
		    	FlagStr = "Succ";
		    	tVData.clear();
				tVData = tInspeDimenConfigUI.getResult();
		    }
		    else                                                                           
		    {
		    	Content = "删除失败，原因是:" + tError.getLastError();
		    	FlagStr = "Fail";
		    }
	     }	
  }
%>	

<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>


