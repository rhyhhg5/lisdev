<%@page import="com.sinosoft.lis.pubfun.*"%>

<%GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI"); %>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initForm() {
  try {	
  	fm.curManageCom.value = <%=tGI.ManageCom%>;
    fm.cManageCom.value = "";
		fm.cBankCode.value = "";
		fm.cAccountType.value = "";
		fm.cBankDetail.value = "";
		initCodeQuery();
		initBankDetailGrid();
  }
  catch(re) {
    alert("InitForm 函数中发生异常:初始化界面错误!");
  }
}

function initCodeQuery(){
		var strQueryResultL = easyQueryVer3("select distinct bankcode,max(bankname) from ldbank where comcode like '" + fm.curManageCom.value + "%%' group by bankcode",1,1,1);
		var	tArrayL = decodeEasyQueryResult(strQueryResultL);
		var tDS = "0|^";
		for(i=0;i<tArrayL.length;i++){
			tDS =tDS + tArrayL[i][0] + "|" + tArrayL[i][1] + "|M";
			if((i+1)!=tArrayL.length){
				tDS = tDS + "^";
			}
		}
		fm.all("cBankCode").CodeData = tDS;
		//下面试修改的 yingxl 2008 - 05- 13
		strQueryResultL = easyQueryVer3("select (select name from ldcom where comcode = ldbank.comcode) as comname from ldbank where comcode like '" + fm.curManageCom.value + "%%'",1,1,1);
		//strQueryResultL = easyQueryVer3("select distinct rpad(comcode,6,'01'),(select name from ldcom where comcode = ldbank.comcode) as comname from ldbank where comcode like '" + fm.curManageCom.value + "%%'",1,1,1);
		tArrayL = decodeEasyQueryResult(strQueryResultL);
		tDS = "0|^";
		for(i=0;i<tArrayL.length;i++){
			tDS =tDS + tArrayL[i][0] + "|" + tArrayL[i][1] + "|M";
			if((i+1)!=tArrayL.length){
				tDS = tDS + "^";
			}
		}
		fm.all("cManageCom").CodeData = tDS;
}

function initBankDetailGrid()
{                                  
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="45px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="机构代码";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="银行代码";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="帐户类型";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="明细信息";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      BankDetailGrid = new MulLineEnter( "fm" , "BankDetailGrid" ); 
      //这些属性必须在loadMulLine前
      BankDetailGrid.mulLineCount = 10;   
      BankDetailGrid.displayTitle = 1;
      BankDetailGrid.locked = 1;
      BankDetailGrid.canSel = 1;
      BankDetailGrid.canChk = 0;
      //FinDayTestGrid.hiddenPlus = 1;
      //FinDayTestGrid.hiddenSubtraction = 1;        
      BankDetailGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>

	
