<html>
<%
//程序名称 :ShieldBorUnevenInput.jsp
//程序功能 :屏蔽借贷不平数据
//创建人 :
//创建日期 :2015-06-02 
//
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。  
 %>
<script>
  var comcode = "<%=tGI1.ComCode%>";
 

</script>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
<SCRIPT src = "ShieldBorUnevenInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ShieldBorUnevenInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="" method=post name=fm target="fraSubmit">
<Div id= "divLLReport1" style= "display: ''">
<strong><IMG id="a1" src="../common/images/butExpand.gif"
style="cursor:hand;" OnClick="showPage(this,divOperator);" />屏蔽借贷不平数据</strong>
  	<table>
    	<tr>
    		 <td class= titleImg>请输入屏蔽条件</td>   		 
    	</tr>
    </table>

 <table class= common border=0 width=100%>
	<table  class= common>
        <TR  class= common>
		<TD class=title>批次号</TD>
		<TD class=input><Input class="common" name=BatchNo elementtype=nacessary></TD>
		
		<TD class=title>保单号</TD>
		<TD class=input><Input class="common" name=ContNo elementtype=nacessary></TD>
		
		<TD class=title>4位机构</TD>
		<TD class=input><Input class="common" name=ManageCom></TD>
        </TR> 
      	<TR  class= common>
         		<TD class=title>凭证类型</TD>
		<TD class=input><Input class="common" name=VoucherType></TD>	

		<TD class=title>记账日期</TD>
		<TD class=input><Input class="coolDatePicker"
			verify="记账日期|notnull&date" dateFormat="short" name=AccountDate elementtype=nacessary></TD>	
			
										
		<TD class=title>校检标记</TD>
		<TD class=input><Input class=codeno name=CheckF
			CodeData="0|^00|恢复数据|M^01|屏蔽数据|M" verify="校检标记"
			ondblclick="showCodeListEx('CheckF',[this,CheckFName],[0,1]);"
			onkeyup="showCodeListKeyEx('CheckF',[this,CheckFName],[0,1]);"><input
			class=codename name=CheckFName readonly=true elementtype=nacessary></TD>
        </TR>  
            
 </table> 
 
    <INPUT VALUE="数据查询" class=cssButton TYPE=button onclick="Query();">
    <INPUT VALUE="数据维护" class=cssButton TYPE=button onclick="ShieldBor();">
    
<hr width=98%>
	<Div  id= "divShieldBorUnevenGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanShieldBorUnevenGrid" >
  					</span> 
  			  	</td>
  			</tr>					
</Div>
</table>	
      <input class=cssButton value="首页"    type=button  onclick="turnPage.firstPage();"></input>
      <input class=cssButton value="上一页"  type=button  onclick="turnPage.previousPage();"></input>
      <input class=cssButton value="下一页"  type=button  onclick="turnPage.nextPage();"></input>
      <input class=cssButton value="尾页"    type=button  onclick="turnPage.lastPage();"></input>

</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>