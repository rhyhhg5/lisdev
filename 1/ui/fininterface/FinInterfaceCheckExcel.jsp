<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
	String FlagStr = "";
  String Content = "";
  
	//在此设置导出Excel的列名，应与sql语句取出的域相对应
	GlobalInput tGlobalInput = new GlobalInput(); 
	tGlobalInput = (GlobalInput)session.getValue("GI"); 
	
	ExportExcel.Format format = new ExportExcel.Format();
	ArrayList listCell = new ArrayList();
	ArrayList listLB = new ArrayList();
	ArrayList listColWidth = new ArrayList();
	format.mListCell=listCell;
	format.mListBL=listLB;
	format.mListColWidth=listColWidth;
	
	ExportExcel.Cell tCell=null;
	ExportExcel.ListBlock tLB=null;
	
	String tManageCom = tGlobalInput.ComCode;
	
	listColWidth.add(new String[]{"0","5000"});  
	String sql = request.getParameter("ExportExcelSQL");
	System.out.println("Excel语句="+sql);
	
	tLB = new ExportExcel.ListBlock("001");
	tLB.colName = new String[]{"批次号", "机构","借贷标志", "科目信息", "业务信息", "险种", "渠道", "业务日期", "保单号码","明细","现金流量码","成本中心","金额","凭证类型","业务号码类型","业务号码","凭证号"};
	tLB.sql = sql;
	tLB.row1 = 0;
	tLB.col1 = 0;
	tLB.InitData();
	listLB.add(tLB);

	try
	{
		response.reset();
		response.setContentType("application/octet-stream");
		
		//设置导出的xls文件名默认值
		String HeaderParam = "\""+"attachment;filename="+"000001"+".xls"+"\"";
		response.setHeader("Content-Disposition",HeaderParam);
		
		OutputStream outOS = response.getOutputStream();
		BufferedOutputStream bos = new BufferedOutputStream(outOS);
		ExportExcel excel = new ExportExcel();
		excel.write(format, bos);
		bos.flush();
		bos.close();
	}
	catch(Exception e)
	{
	  Content = "导出Excel失败!";
    FlagStr = "Fail"; 
	}
	if (!FlagStr.equals("Fail"))
	{
		Content = " 导出Excel成功! ";
		FlagStr = "Succ";
	}
%>