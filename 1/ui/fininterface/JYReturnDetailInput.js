//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	   if(fm.EndDate.value<fm.StartDate.value){
					alert("起始时间大于终止时间,请重新输入!");
					fm.EndDate.focus();
					return false;
	　    }	
	  var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  //fm.hideOperate.value=mOperate;
	  //if (fm.hideOperate.value=="")
	  //{
	  //  alert("操作控制数据丢失！");
	  //}
	  //showSubmitFrame(mDebug);
	  fm.submit(); //提交
	}
	
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

//	预估结余返还统计明细表-Excel
function FMPrintX()
{
  fm.action="../fininterface/YGJYReturnDetailSave.jsp";
  if(!verifyInput())
      return false;
   if(!beforeSubmit())
  {
    return false;
  }
  var i = 0;
  fm.target='Print';
  submitForm();
  showInfo.close();
}

function beforeSubmit()
{
//校验开始日期必须为某月的第一天
	var Start = fm.all('StartDate').value;
	var End = fm.all('EndDate').value;
	if(Start == null||Start==''){
		alert("请输入起始日期！");
		return false;
	} 
	if(End == null||End==''){
		alert("请输入结束日期！");
		return false;
	} 
	//统计日期不能大于3个月
	if(trim(End.substring(5,7))-trim(Start.substring(5,7))>=3)
	{
	  alert("考虑到数据量原因，请把查询时间限定在3个月内，请理解！");  
	  return false;
	}
	return true;
}

//	实际结余返还统计明细表-Excel
function printPayX()
{
	fm.action="../fininterface/BJYReturnDetailSave.jsp";
   if(!verifyInput())
      return false;
   if(!beforeSubmit())
   {
    return false;
   }
   var i = 0;
    fm.target='Print';
	submitForm();
	showInfo.close();
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
// ----日期格式的验证------------------
function CheckDate(StartDate)
{
    var reg=/^(\d{4})([-])(\d{2})([-])(\d{2})/;
    if(!reg.test(strDate))
    {
        alert("日期格式不正确!\n 正确格式为:yyyy-mm-dd");
        return false;
    }
    return true;
}
 