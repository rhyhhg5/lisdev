 <%@ page contentType="text/html; charset=GBK" %>
 <%@page import="com.sinosoft.lis.fininterface.*"%>
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import = "com.sinosoft.lis.pubfun.*"%>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import="java.net.*"%>
 <%@page import="java.util.*"%>
 <%@page import="java.io.*"%>

<%
  CErrors tError = new CErrors();
  String FlagStr = "";
  String Content = "";
  VData tVData = new VData();

  String sBankcode = request.getParameter("cBankCode");
  String sManageCom = request.getParameter("cManageCom");
  String sAccountType = request.getParameter("cAccountType");
  String sBankDetail = request.getParameter("cBankDetail");
  String sDealType = request.getParameter("dealType");

  tVData.add(sBankcode);
  tVData.add(sManageCom);
  tVData.add(sAccountType);
  tVData.add(sBankDetail);
  tVData.add(sDealType);
  
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");

       System.out.println("开始添加银行帐户");

       try 
       {  
           BankDetailSet tBankDetailSet = new BankDetailSet();
           if(!tBankDetailSet.dealData(tVData))
           {
             FlagStr = "Fail";
             Content = "添加银行帐户失败，原因是：" + tBankDetailSet.mErrors.getFirstError();
             System.out.println("操作失败");
           }
          else
          {
             FlagStr = "Succ";
             Content = "添加成功"; 
             System.out.println("操作成功");
          }
          	                     
       }
       catch(Exception ex) 
       {
       
             FlagStr = "Fail";
             Content="添加失败，原因是: " + ex.getMessage();   
             System.out.println(Content);             
       }  	   
  	   System.out.println("添加结束");

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.initForm();
</script>
</html>

