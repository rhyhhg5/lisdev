/**
 * @author panbin
 * @date 2014-04-03
 * 对应InspeDimenConfigInput.jsp页面的一些操作。
 */
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mFlag = "0";
var showInfo;

function insertData(){
		if (verifyInput() == false) {
		return false;
	    }
		fm.fmtransact.value = "INSERT" ;
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  		fm.target = "fraSubmit";
  		fm.action="./InspeDimenConfigSave.jsp"
  		fm.submit();
}
	


function queryData() {
	initConfigFicodetransGrid();
		var CodeName=fm.CodeName.value;
	    var Code=fm.Code.value;
	    var spart1 = "";
	    var sql ="select CodeName,Code from ficodetrans where codetype='KHWD' "; 
	    if(CodeName!=''){
		spart1 = spart1+" and CodeName='"+CodeName+"' "
		}
		if(Code!=''){
		spart1 = spart1+" and Code='"+Code+"' "
		}
	    sql = sql+spart1;
		turnPage1.queryModal(sql,ConfigFicodetransGrid);
		var arr = easyExecSql(sql);
		
		if(!arr)alert("没有数据");
}

// 响应删除按钮
function delData() {
		if (verifyInput() == false) {
			return false;
		}
		fm.fmtransact.value = "DELETE" ;
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  		fm.target = "fraSubmit";
  		fm.action="./InspeDimenConfigSave.jsp"
  		fm.submit();
}
 

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	queryData();
}




