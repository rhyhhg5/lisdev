<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%
  String FlagStr = "";
  String Content = "";
    System.out.println("---print--getadvance---");
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
  SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
  String tDate=dateFormat.format(new Date());
  String tTime=timeFormat.format(new Date());
  String fileName=tDate.replace("-","")+tTime.replace(":","")+"-"+tG.Operator+".xls";
  System.out.println("文件名称："+fileName);
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
  System.out.println("文件路径:" + tOutXmlPath);
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
  tTransferData.setNameAndValue("StartDate",request.getParameter("StartDate"));
  tTransferData.setNameAndValue("EndDate",request.getParameter("EndDate"));

  
  try
  {
      VData vData = new VData();
      vData.add(tG);
      vData.add(tTransferData);
      BJYReturnDetailUI tBJYReturnDetailUI = new BJYReturnDetailUI();
      if (!tBJYReturnDetailUI.submitData(vData, ""))
      {
          System.out.println("cuow");
          Content = "报表下载失败，原因是:" + tBJYReturnDetailUI.mErrors.getFirstError();
          FlagStr = "Fail";
      }
      else
      {
          String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
          File file = new File(tOutXmlPath);
          
          response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[10000];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
              file.delete();
          }
       }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  
  if (!FlagStr.equals("Fail"))
  {
    Content = "打印报表成功！";
    FlagStr = "Succ";
  }
%>

<html>
   <script language="javascript">  
    alert('<%=Content %>');
    top.close();
   </script>
</html>