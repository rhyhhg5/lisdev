/*程序名称：HCCheckData.js
程序功能：数据核对
创建日期：2017-12-6 
创建人  ：弓彩霞
更新记录：  更新人    更新日期     更新原因/内容
*/

var turnPage = new turnPageClass();// 日结试算相关数据
// var turnPage2 = new turnPageClass();//日结确认相关数据

var mFlag = "0";
var showInfo;


function HCBeforeSubmit()
{
	if(fm.checkType.value == ''||fm.checkType.value == null){
		alert('请选择数据核对类型');
		fm.checkType.focus();
		return false;
	}
	var myDate = new Date();
	var hours = myDate.getHours(); 
	if(fm.checkType.value =="1"){
		if(fm.ZSType.value == ''||fm.ZSType.value == null){
			alert('请选择暂收数据核对类型');
			fm.ZSType.focus();
			return false;
		}
		if(fm.StartDay.value==''||fm.StartDay.value==null){
		  	alert("请选择起始日期");
		  	fm.StartDay.focus();
		  	return false;
		  	}
		if(fm.EndDay.value==''||fm.EndDay.value==null){
		  	alert("请选择终止日期");
		  	fm.EndDay.focus();
		  	return false;
		  	}
		if(fm.EndDay.value<fm.StartDay.value){
			alert("起始时间大于终止时间,请重新输入!");
			fm.EndDay.focus();
			return false;
		　    }
		  if(fm.ManageCom.value==''||fm.ManageCom.value==null){
	  			alert("请选择管理机构");
	  			fm.ManageCom.focus();
	  			return false;
	  	}
			//alert(hours);
			if(hours>="19"||hours<"5")
			{
			  if(fm.EndDay.value>fm.StartDay.value){
			 		var days = parseInt((new Date(fm.EndDay.value.replace(/-/g, "/")).getTime() - new Date(fm.StartDay.value.replace(/-/g, "/")).getTime())/1000/60/60/24);
			 				//alert(days);
			 		if(days>368) {
						alert("起始时间与结束时间相差不能大于一年,请重新输入按年统计，非常感谢!");
						fm.EndDay.focus();
						return false;
					}
			　    }	  		
			} else {
				if(fm.EndDay.value>fm.StartDay.value){
			 		var days = parseInt((new Date(fm.EndDay.value.replace(/-/g, "/")).getTime() - new Date(fm.StartDay.value.replace(/-/g, "/")).getTime())/1000/60/60/24);
			 				//alert(days);
			 		if(days>31) {
						alert("起始时间与结束时间相差不能大于一个月,请重新输入按月统计，如需跨月查询，请在晚7点至早5点，非常感谢!");
						fm.EndDay.focus();
						return false;
					}
			　    }
			}
		}
		
	
else if(fm.checkType.value =='2'){}
else 	if(fm.checkType.value =='3'){}
else	if(fm.checkType.value =='4'){}
	return true;
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function HCCheckData(){
	try{
		if(HCBeforeSubmit()){
			var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
			HCCheckDataGrid.clearData("HCCheckDataGrid");
			var HCSql = "";
			//var strQueryResultC = easyQueryVer3("select classtype,keyid from liclasstypekeydef order by classtype",1,1,1);
			//var	tArrayC = decodeEasyQueryResult(strQueryResultC);

			if(fm.checkType.value == "1"){//核对类型^1|暂收 ^2|实收^3|应付^4|实付
				if(fm.ZSType.value == "1"){//暂收类型^1|非异地 ^2|异地
					HCSql = "select a.tempfeeno, a.otherno, a.managecom, a.policycom, sum(a.paymoney), a.confmakedate, '核心未价税导致数据无法流转' from ljtempfee a " +
							"where a.confmakedate >= '" +fm.StartDay.value+ "' and a.confmakedate <= '" +fm.EndDay.value+ "' " +
							"and a.managecom like '" +fm.ManageCom.value+ "%' and (a.policycom is null or a.policycom = '' or a.managecom = a.policycom) " +
							"and not exists (select 1 from fiabstandarddata b where b.indexno = a.tempfeeno and b.indexcode = '01') " +
							"and not exists (select 1 from LYPremSeparateDetail d where a.tempfeeno = d.tempfeeno and d.otherstate = '01') " +
							"and exists (select 1 from fiaboriginalmain c where c.indexno = a.tempfeeno  and c.state = '01') " +
							"and a.tempfeetype not in('20','21','30') group by a.tempfeeno, a.otherno, a.managecom, a.policycom, a.confmakedate " +
							"union " +
							"select a.tempfeeno, a.otherno, a.managecom, a.policycom, sum(a.paymoney), a.confmakedate, '生成凭证借贷不平，财务接口会尽快处理' from ljtempfee a " +
							"where a.confmakedate >= '" +fm.StartDay.value+ "' and a.confmakedate <= '" +fm.EndDay.value+ "'  " +
							"and a.managecom like '" +fm.ManageCom.value+ "%' and (a.policycom is null or a.policycom = '' or a.managecom = a.policycom) " +
							"and exists (select 1 from fivoucherdatadetail b where b.serialno in " +
							"(select serialno from fiabstandarddata c where a.tempfeeno = c.indexno and c.indexcode = '01') and b.checkflag = '01') " +
							"group by a.tempfeeno, a.otherno, a.managecom, a.policycom, a.confmakedate"
						;
				}
				else if(fm.ZSType.value == "2"){
					HCSql ="select a.tempfeeno, a.otherno, a.managecom, a.policycom, sum(a.paymoney), a.confmakedate, '核心未价税导致数据无法流转' from ljtempfee a " +
					"where a.confmakedate >= '" +fm.StartDay.value+ "' and a.confmakedate <= '" +fm.EndDay.value+ "' " +
					"and (a.policycom like  '" +fm.ManageCom.value+ "%' or a.managecom like '"+fm.ManageCom.value+"%')  and  a.policycom <> a.managecom  " +
					"and not exists (select 1 from fiabstandarddata b where b.indexno = a.tempfeeno and b.indexcode = '01') " +
					"and not exists (select 1 from LYPremSeparateDetail d where a.tempfeeno = d.tempfeeno and d.otherstate = '01') " +
					"and exists (select 1 from fiaboriginalmain c where c.indexno = a.tempfeeno  and c.state = '01') " +
					"and a.tempfeetype not in('20','21','30') group by a.tempfeeno, a.otherno, a.managecom, a.policycom, a.confmakedate " +
					"union " +
					"select a.tempfeeno, a.otherno, a.managecom, a.policycom, sum(a.paymoney), a.confmakedate, '生成凭证借贷不平，财务接口会尽快处理' from ljtempfee a " +
					"where a.confmakedate >= '" +fm.StartDay.value+ "' and a.confmakedate <= '" +fm.EndDay.value+ "'  " +
					"and (a.policycom like  '" +fm.ManageCom.value+ "%' or a.managecom like '"+fm.ManageCom.value+"%')  and  a.policycom <> a.managecom   " +
					"and exists (select 1 from fivoucherdatadetail b where b.serialno in " +
					"(select serialno from fiabstandarddata c where a.tempfeeno = c.indexno and c.indexcode = '01') and b.checkflag = '01') " +
					"group by a.tempfeeno, a.otherno, a.managecom, a.policycom, a.confmakedate"
				;
				}
			}
			else if(fm.checkType.value == "2"){}
			else if(fm.checkType.value == "3"){}
			else if(fm.checkType.value == "4"){}
			//fm.ExportExcelSQL.value=HCSql;// 保存sql，导出excel时用到
			turnPage.queryModal(HCSql,HCCheckDataGrid);
			showInfo.close();
			if(HCCheckDataGrid.mulLineCount=="0"){
				alert("没有查询到数据");
			}
			return true;
			}
			
	}
	catch(ex){
		alert(ex);
	}
	
}

function afterCodeSelect(cCodeName, Field) {
	if(cCodeName == "checkType") 
	{  
		if(fm.checkType.value=="1"){
			fm.all("divHCCheckData1").style.display = '';
			fm.all("divHCCheckData2").style.display = 'none';
			fm.all("divHCCheckData3").style.display = 'none';
			fm.all("divHCCheckData4").style.display = 'none';
		}else if(fm.checkType.value=="2"){
			fm.all("divHCCheckData2").style.display = '';
			fm.all("divHCCheckData1").style.display = 'none';
			fm.all("divHCCheckData3").style.display = 'none';
			fm.all("divHCCheckData4").style.display = 'none';		
		}else if(fm.checkType.value=="3"){
			fm.all("divHCCheckData3").style.display = '';
			fm.all("divHCCheckData1").style.display = 'none';
			fm.all("divHCCheckData2").style.display = 'none';
			fm.all("divHCCheckData4").style.display = 'none';			
		}else if(fm.checkType.value=="4"){
			fm.all("divHCCheckData4").style.display = '';
			fm.all("divHCCheckData1").style.display = 'none';
			fm.all("divHCCheckData2").style.display = 'none';
			fm.all("divHCCheckData3").style.display = 'none';
			
		}
	}
	
}

function showRecord(strRecord){
  turnPage.strQueryResult  = strRecord;
  turnPage.useSimulation   = 1;  
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  var filterArray = new Array(0,9,4,1,7);
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  turnPage.pageDisplayGrid = HCCheckDataGrid;       
  turnPage.pageIndex = 0;  
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
	
}




