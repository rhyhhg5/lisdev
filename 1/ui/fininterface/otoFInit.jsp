<% 
//程序名称：otoFInit.jsp
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI"); %>
	
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

<script language="JavaScript">

function initForm()
{
  try
  {
    initClassTypeGrid();
    queryClassType();
  }
  catch(re)
  {
    alert("otoFInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initClassTypeGrid()
{
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="凭证类型编码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="凭证类型";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      ClassTypeGrid = new MulLineEnter( "fm" , "ClassTypeGrid" ); 
      //这些属性必须在loadMulLine前
      ClassTypeGrid.mulLineCount = 100;   
      ClassTypeGrid.displayTitle = 1;
      ClassTypeGrid.locked = 1;
      ClassTypeGrid.canSel = 0;
      ClassTypeGrid.canChk = 1;
      ClassTypeGrid.hiddenPlus = 1;
      ClassTypeGrid.hiddenSubtraction = 1;        
      ClassTypeGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>
