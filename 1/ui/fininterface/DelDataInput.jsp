<%@ page contentType="text/html; charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
 String CurrentDate = PubFun.getCurrentDate();
%>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="DelData.js"></SCRIPT>
  <%@include file="DelDataInit.jsp"%>
  <title>财务接口</title>
</head>
<body  onload="initForm();" >
  <form action="./DelDataSave.jsp" method=post name=fm target="fraTitle">    
  
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入日期区间：</td>
  		</tr>
  	</table>   
  
   <table  class= common align=center>

      	<TR  class= common>
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Bdate value = '<%=CurrentDate%>'>
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Edate value = '<%=CurrentDate%>'>
          </TD>
        </TR>

	      </TR>
	        <TR  class= common>
            <TD>
             <INPUT  class=cssButton VALUE=" 查 询 " TYPE=Button onclick="BatchNoQuery();">
            </TD>
        </TR>
        
    </table>
    
  	<br>
  	
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请选择批次号：</td>
  		</tr>
  	</table>   
        
    <!-- 批次号信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 批次号信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanMoveDataGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: 'none' ">
    	<center>
    		<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    		<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
		</center>
    </Div>  
    <br>    
       <!--INPUT VALUE="test" class= cssButton TYPE=button onclick="testtt()"-->
    <INPUT VALUE= "回滚数据" class= cssButton TYPE=button onclick="SubmitForm()">  
    <INPUT VALUE="" TYPE=hidden name=sBatchNo>     
    <INPUT value=<%= tGI.ManageCom %> type=hidden name=cManageCom>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
