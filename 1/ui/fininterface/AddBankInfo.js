/**
 * @author yanghao
 * @date 2008-07-05
 * 对应AddBankInfoInput页面的一些操作。
 */
 
var turnPage = new turnPageClass();
var mFlag = "0";
var showInfo;

// 提交之前的验证,验证一些必须填写的数据是否都填写。
function beforeSubmit() {

	if (verifyInput() ) {
//		if( fm.BankAccNo.value != "" ){
//		  if ( !isInteger(fm.BankAccNo.value) ){
//			   alert("银行帐号必须是整数");
//			   return false;
//			}
//		}
//	  alert(fm.BankAccNo.value.length);
	  if(/[^\x00-\xff]/g.test(fm.BankAccNo.value)){
	  	alert("银行账号中不能含有汉字。");
	  	return false;
	  }
	  
	    if( fm.BankAccNo.value.length > 40){
			alert("银行帐号位数过长。");
			return false;
		}
	  

	 	if( fm.Code.value != ""){
			if ( !isInteger(fm.Code.value) ){
			    alert("科目名称必须以1002开头,且必须是10位整数。");
			    return false;
			}
		}
		
		// 科目名称必须以1002开头
		if(fm.Code.value.indexOf("1002") == 0 && fm.Code.value.length == 10){
			return true;
		}
		else{
			alert("科目名称必须以1002开头,且必须是10位整数。");
		}
		

 
	}
	return false;
}

 

// 提交操作
function submitForm(dealType) {
		var showStr = "\u6b63\u5728\u67e5\u8be2\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.action = "AddBankInfoSave.jsp?dealType="+dealType;
		fm.submit();
}

// 响应查询按钮 - 查询数据
function queryData() {
	//	printStr();
	//	var sql = "select ManageCom,bankcode,bankname,bankaccno,finflag,CASE operater  WHEN  'YBT' THEN  '银保通' ELSE '非银保通' end, code, codename from LDFinBank a, LIDetailFinItemCode b ,licodetrans c where a.bankaccno = b.LevelCondition  and b.LevelCode = substr(c.code,5,6) "  
	//	var sql = "select ManageCom,bankcode,bankname,bankaccno,finflag,CASE ucase(operater)  WHEN 'YBT' THEN  '银保通' ELSE '非银保通' end, code, codename from LDFinBank a, LIDetailFinItemCode b ,licodetrans c where a.bankaccno = b.LevelCondition  and b.LevelCode = substr(c.code,5,6) "  

	 	if( fm.Code.value != ""){
			if ( !isInteger(fm.Code.value) ){
			    alert("请输入科目代码整数进行查询");
			    return false;
			}
		}
		
	//	if( fm.BankAccNo.value != "" ){
	//	  if ( !isInteger(fm.BankAccNo.value) ){
	//		   alert("请输入银行帐号整数进行查询");
	//		   return false;
	//		}
	//	}
		 if(fm.RuFlag.value == "1" && fm.RuZhName.value == ""){		 		
		 		alert("请输入账户户名");
				return false;				
		}
		 
		 var sql = "select a.managecom,a.bankcode,a.bankname,a.bankaccno,a.finflag,(CASE a.operater WHEN 'YBT' THEN  '银保通' ELSE '非银保通' end),'1002' || trim(b.levelcode) as Item,(select codename from licodetrans where codetype = 'accountcode' and code = '1002' || trim(b.levelcode)) as ItemName,a.RuFlag,a.RuZhName from ldfinbank a,LIDetailFinItemCode b where a.bankaccno = b.LevelCondition and b.finitemid = '0000003' "
		//	+ getWherePart("ManageCom", "ManageCom", "like") 
		//	+ getWherePart("BankCode", "BankCode") 
			+ getWherePart("BankName", "BankName") 
			+ getWherePart("FinFlag", "FinFlag_sp") 
		 	+ getWherePart("BankAccNo", "BankAccNo")
		 	+ getWherePart("RuFlag", "RuFlag")
		 	+ getWherePart("RuZhName", "RuZhName")
		 	;
		 //	+ getWherePart("Code", "Code")
		 //	+ getWherePart("CodeName", "CodeName");	
		 

		 	
		 if(fm.ManageCom.value != ""){
		 	sql += getWherePart("ManageCom", "ManageCom", "like");
		 }else{
		 	sql += getWherePart("ManageCom", "curManageCom", "like");
		 }	
		 
		if(fm.BankCode.value != ""){
		 	var bkc = fm.BankCode.value.substring(0,2);
	    	sql += " and BankCode like '"+ bkc + "%' ";
		 }	
		 
	 	 if(fm.Code.value != "" ){
		 	sql += " and b.levelcode  = substr('"+ fm.Code.value +"',5,6)";
	 	 }
	 	 
	 	 if (fm.CodeName.value != "" ){
	         sql += " and b.levelcode in ( select substr(code, 5,6) from licodetrans where codename='"+fm.CodeName.value+"' )";
	 	 }
	    	 
	 	 if (fm.Operater_sp.value == "Y"){
	  	 	sql += getWherePart("operater", "YBT", "=");
	 	 }
	 	 else if(fm.Operater_sp.value == ""){
	 	 }
	 	 else {
	 	 	sql += getWherePart("operater", "YBT", "<>");
	 	 } 
	 	 
	 	 turnPage.queryModal(sql, BankDetailGrid);
 
}

// 响应添加按钮
function insertData(){
//	if(canInsert()){
	if(beforeSubmit()){
		
		 if(fm.RuFlag.value == "1" && fm.RuZhName.value == ""){		 		
		 		alert("请输入账户户名");
				return false;				
		}
		 
		if( !queryLIDetailFinItemCode() && !queryLDFinBank() ){
			 alert(" 不能添加，表中已经存在该银行信息。");
		}else{
			submitForm("insert");
		}
	}
}

// 响应修改按钮
function updateData() {
	if(beforeSubmit()){
		submitForm("update");
	}
}

// 响应删除按钮
function delData() {

	var tSel = BankDetailGrid.getSelNo();
	if(tSel != 0){
		var yes = confirm("你确定删除该条数据吗？");
		if(yes){
			BankSelect();
			submitForm("delete");
		}
	}
	else{
		alert("没有选择数据。");
	}
}
 
// 看是否能添加 
function canInsert() {
	if (beforeSubmit()) {
		if(queryLDFinBank() && queryLDCode() && queryLICodeTrans() && queryLIDetailFinItemCode()){
			return true;
		}else{
			//alert("不能添加，该信息或者科目信息已经存在。");
			return false;
		}
		
	}
}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
// 	fm.reset();
//	alert("Over");
	queryData();
}

// 响应单选按钮
function BankSelect() {
 
		var tSel = BankDetailGrid.getSelNo();
		try
		{
			fm.ManageCom.value = BankDetailGrid.getRowColData(tSel-1,1);// 管理机构
			fm.ManageComName.value = BankDetailGrid.getRowColData(tSel-1,1);
			showOneCodeName("comcode","ManageComName");
			
			fm.BankCode.value = BankDetailGrid.getRowColData(tSel-1,2);// 银行编码
			fm.BankCodeName.value = BankDetailGrid.getRowColData(tSel-1,2);
			showOneCodeName("banknum","BankCodeName");
			
			fm.BankName.value = BankDetailGrid.getRowColData(tSel-1,3);// 银行名称
			
			fm.BankAccNo.value = BankDetailGrid.getRowColData(tSel-1,4);// 银行帐号
			
			var sFinFlag = BankDetailGrid.getRowColData(tSel-1,5);// 收付标记
			fm.FinFlag_sp.value = sFinFlag;
			if(sFinFlag=="S"){
		 		fm.FinFlag.value = "收帐号";
			}else if(sFinFlag=="F") {
				fm.FinFlag.value = "支帐号";
			}		
			
			var ybtFlag = BankDetailGrid.getRowColData(tSel-1,6);// 银保标记
			fm.Operater.value = ybtFlag; 
			if(ybtFlag=="银保通"){
				fm.Operater_sp.value = "Y";
			}else{
				fm.Operater_sp.value = "N";
			}
			
			fm.Code.value = BankDetailGrid.getRowColData(tSel-1,7);// 科目代码
			
			fm.CodeName.value = BankDetailGrid.getRowColData(tSel-1,8);// 科目代码名称
			
			
			fm.RuFlag.value = BankDetailGrid.getRowColData(tSel-1,9);// 税优转移收费账户标识
			fm.RuZhName.value = BankDetailGrid.getRowColData(tSel-1,10);// 账户户名
		}
		catch(ex)
		{
			alert( "读取数据错误,请刷新页面再使用。" + ex );
		}
 
}


// 查询LDFinBank
function queryLDFinBank() {
	var strsql = "select * from LDFinBank where managecom='" + fm.ManageCom.value + "' and BankCode='" + fm.BankCode.value + "'" + " and BankAccNo='" + fm.BankAccNo.value + "'" + " and FinFlag='" + fm.FinFlag_sp.value + "'";
	var arr = easyExecSql(strsql);
	if (arr) {
	   // alert("LDFinBank表中已经存在该银行信息。");
		return false;
	} else {
		return true;
	}
}
// 查询LDCode
function queryLDCode() {
	var strsql = "select * from LDCode where Codetype = 'accountcode' and othersign ='1' and " + " Code ='" + fm.Code.value + "' with ur";
   // var strsql = "select * from LDCode where Codetype = 'accountcode' and othersign ='1' and CodeName='" + fm.CodeName.value + "'  with ur";
	var arr = easyExecSql(strsql);
	if (arr) {
		alert("LICode表中已经存在科目编号为： "+fm.Code.value +" 的科目。");
		return false;
	} else {
		return true;
	}
}
// 查询LLICodeTrans
function queryLICodeTrans() {
	//var strsql = "select * from LICodeTrans where Codetype = 'accountcode' and othersign ='1' and " + " Code ='" + fm.Code.value + "' and CodeName='" + fm.CodeName.value + "' with ur";
 	var strsql = "select * from LICodeTrans where Codetype = 'accountcode' and othersign ='1' and " + " Code ='" + fm.Code.value + "' with ur";
	var arr = easyExecSql(strsql);
	if (arr) {
		alert("LICodeTrans表中已经存在科目编号为： "+fm.Code.value +" 的科目。");
		return false;
	} else {
		return true;
	}
}
// 查询科目名字是否已存在
function queryCodeName(){
	var strsql ="select * from LDCODE where codetype='accountcode' and othersign ='1' and codename ='"+fm.CodeName.value+"' with ur";
	var arr = easyExecSql(strsql);
	if (arr) {
		return false;
	} else {
		return true;
	}
}
// 查询LIDetailFinItemCode
function queryLIDetailFinItemCode() {
	//var strsql = "select * from LIDetailFinItemCode where FinItemlevel = '2' and JudgementNo = '02' and " + "LevelCondition = '" + fm.BankAccNo.value + "'  and LevelCode='" + fm.Code.value.substring(4,4+6) + "' with ur";
	var strsql = "select * from LIDetailFinItemCode where FinItemlevel = '2' and JudgementNo = '02' and " + "LevelCondition = '" + fm.BankAccNo.value + "' with ur";
	var arr = easyExecSql(strsql);
	if (arr) {
	    //alert("LIDetailFinItemCode表中已经存在银行账号为"+fm.BankAccNo.value+"的数据。");
		return false;
	} else {
		return true;
	}
} 


// 测试用 看是否获得值 基本上不用
function printStr() {
	var str = "";
	var sqlstr = "";
	var singleq = "'";
	if (fm.ManageCom.value != "") {
		str += "  \u7ba1\u7406\u673a\u6784\uff1a" + fm.ManageCom.value;
		sqlstr += " and ManageCom like '" + fm.ManageCom.value + "%'";
	}
	if (fm.BankCode.value != "") {
		str += "  \u94f6\u884c\u7f16\u7801\uff1a" + fm.BankCode.value;
		sqlstr += " and BankCode = '" + fm.BankCode.value + singleq;
	}
	if (fm.BankName.value != "") {
		str += "  \u94f6\u884c\u540d\u79f0\uff1a" + fm.BankName.value;
		sqlstr += " and BankName='" + fm.BankName.value + singleq;
	}
	if (fm.FinFlag_sp.value != "") {
		str += "  \u6536\u4ed8\u6807\u8bb0\uff1a" + fm.FinFlag_sp.value;
		sqlstr += " and FinFlag='" + fm.FinFlag_sp.value + singleq;
	}
	if (fm.BankAccNo.value != "") {
		str += "  \u94f6\u884c\u8d26\u53f7\uff1a" + fm.BankAccNo.value;
		sqlstr += " and BankAccNo='" + fm.BankAccNo.value + singleq;
	}
	if (fm.Operater_sp.value != "") {
		str += "  \u94f6\u4fdd\u901a\uff1a" + fm.Operater_sp.value;
		if (fm.Operater_sp.value == "Y") {
			sqlstr += " and Operater='YBT'";
		}
	}
	if (fm.Code.value != "") {
		str += "  \u79d1\u76ee\u4ee3\u7801\uff1a" + fm.Code.value;
		sqlstr += " and Code=" + fm.Code.value;
	}
	if (fm.CodeName.value != "") {
		str += "  \u79d1\u76ee\u540d\u79f0\uff1a" + fm.CodeName.value;
		sqlstr += " and CodeName=" + fm.CodeName.value;
	}
	/**/
	alert(str);
	alert(sqlstr);
}

