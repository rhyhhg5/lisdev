<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="OtoF.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="InterfaceStatue.js"></SCRIPT>
<title>财务接口状态查询</title>
<%@include file="InterfaceStatueInit.jsp"%>
</head>
<body onload="initForm();">
<form method=post name=fm action="./InterfaceStatueSave.jsp"
	target="fraSubmit">
<br>
<b>财务接口状态查询</b>
<br><br>
<Div id="NotRunning" style="display:'none'">
	<table class=common>
		<TR class=common>
			<TD class=title>目前没有机构在运行财务接口。</TD>
		</TR>
	</table>
</Div>
<Div id="Running" style="display:'none'">
	<table class=common id="TotalStatue">
	<TR class=common>
		<TD class=title>提取机构</TD>
		<TD class=title id=tManageCom></TD>	
		<TD class=title>提取区间</TD>	
		<TD class=title id=tTimeArea></TD>		
		<TD class=title>开始时间</TD>	
		<TD class=title id=tStartTime></TD>			
	</TR>
	<TR class=common>
		<TD class=title>已提总数据量</TD>
		<TD class=title id=tDataNum></TD>	
		<TD class=title>已提总金额</TD>	
		<TD class=title id='tSumMoney'></TD>		
		<TD class=title>操作员</TD>	
		<TD class=title id=tOperator></TD>			
	</TR>
	</table>
</Div>
<hr width=98%>
<Div id="divInterfaceStatue" style="display:'none'" align=center>
<table class=common>
	<tr>
		<td style="text-align: left" colSpan=1><span id="spanInterfaceStatueGrid"></span>
		</td>
	</tr>
</Table>
<INPUT CLASS=cssbutton VALUE="首  页" TYPE=button
	onclick="turnPage.firstPage();"> <INPUT CLASS=cssbutton
	VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> <INPUT
	CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
<INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button
	onclick="turnPage.lastPage();">
</Div>
<br>
&nbsp;<input type=button class=cssButton value='刷  新' onclick='getRefresh();'>
</form>
</body>
</html>
