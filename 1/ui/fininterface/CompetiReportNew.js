//Creator :hdl	
//Date :2012-08-09

var showInfo;
var mDebug="0";
var arrDataSet;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null) //shwoInfo是什么？
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//下载报表
function download()
{  
    fm.action="../fininterface/fin-"+operator+"-2012New.xls";
    fm.submit();
}
//生成报表
function build()
{
	if(verify()){
	var showStr="正在生成报表，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
	}else{
	return false;
	}
}
function verify()
{
	 if((fm.Bdate.value=="")||(fm.Bdate.value=="null"))
  {
    alert("请您录入保费起始日期！");
    return false;
  }
   if((fm.Edate.value=="")||(fm.Edate.value=="null"))
  {
    alert("请您录入保费终止日期！");
    return false;
  }
   if((fm.YBdate.value=="")||(fm.YBdate.value=="null"))
  {
    alert("请您录入犹豫期退保起始日期！");
    return false;
  }
   if((fm.YEdate.value=="")||(fm.YEdate.value=="null"))
  {
    alert("请您录入犹豫期退保终止日期！");
    return false;
  }
  return true;
}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}
