<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：
	//程序功能：
	//创建日期：2005-02-22 17:32:48
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	String tProjectNo = request.getParameter("ProjectNo");
	String transact = request.getParameter("transact");
	String tLookFlag = request.getParameter("LookFlag");
%>
<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<script src="AgentComECTypeInput.js"></script>
<%@include file="AgentComECTypeInputInit.jsp"%>

<script>
  		var ManageCom = "<%=tGI.ManageCom%>"; 
  		var tProjectNo = "<%=tProjectNo%>";
		var transact = "<%=transact%>";
		var tLookFlag = "<%=tLookFlag%>
	";
</script>
</head>
<body onload="initForm();initElementtype();">
	<form action="AgentComECTypeInputSave.jsp" method=post name=fm
		target="fraSubmit">
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divLAComECType1);"></td>
				<td class=titleImg>查询条件</td>
			</tr>
		</table>
		<div id="divLAComECType1" style="display: ''">
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>管理机构</TD>
					<TD class=input><input class="codeNo" name="ComCode"
						ondblClick="return showCodeList('ComCode',[this,ComCodeName], [0,1],null,null,null,1);"
						onkeyup=" showCodeListKey('ComCode',[this,ComCodeName], [0,1]),null,null,null,1;"><Input
						class="codeName" name="ComCodeName" readonly elementtype="nacessary"></TD>
						
					<TD class=title>中介所属销售渠道</TD>
					<TD class=input><input class="codeNo" name="SellFlag"
						 CodeData="0|^03|团险中介^10|个险中介^15|互动中介"
						ondblClick=" showCodeListEx('SellFlag',[this,SellFlagName], [0,1],null,null,null,1);"
						onkeyup=" showCodeListKeyEx('SellFlag',[this,SellFlagName], [0,1],null,null,null,1);"><Input
						class="codeName" name="SellFlagName"></TD>

					<TD class=title>中介机构类型</TD>
					<TD class=input><Input class=codeno name="ACType" 
						CodeData="0|^02|兼业代理^03|专业保险代理^04|保险经纪^05|共保机构^06|财险^07|寿险^99|其他"
						ondblclick="return showCodeListEx('ACType',[this,ACTypeName],[0,1],null,null,null,1);"
						onkeyup="return showCodeListKeyEx('ACType',[this,ACTypeName],[0,1],null,null,null,1);"><input
						class=codename name=ACTypeName></TD>
				</TR>
			</table>
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>中介机构编码</TD>
					<TD class=input><input class="codeNo" name="AgentCom"
						ondblClick="return showCodeList('AgentCom',[this,AgentComName], [0,1],null,null,null,1);"
						onkeyup=" showCodeListEx('AgentCom',[this,AgentComName], [0,1],null,null,null,1);"><Input
						class="codeName" name="AgentComName" readonly></TD>
					<TD class=title>中介机构名称</TD>
					<TD class=input><input class=common name="Name"></TD>
					<TD class=title>是否已归属</TD>
					<TD class=input><Input class=codeno name="StateFlag" CodeData="0|^Y|是^N|否"
						ondblclick="return showCodeListEx('StateFlag',[this,StateFlagName],[0,1],null,null,null,1);"
						onkeyup="return showCodeListKeyEx('StateFlag',[this,StateFlagName],[0,1],null,null,null,1);"><input
						class=codename name=StateFlagName></TD>

				</TR>
			</table>
		</div>
		<table>
			<td class=button><input type="button" class=cssButton
				value="查 询 " id="AddID" name="AddID" onclick="queryAgent()">
			</td>
		</table>
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divAgentGrid);"></td>
				<td class=titleImg>中介机构信息</td>
			</tr>
		</table>
		<!-- mulLine -->
		<Div id="divAgentGrid" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1><span id="spanAgentGrid">
					</span></td>
				</tr>
			</table>
		</div>
		<Div id="divPage" align=center style="display: 'none'">
			<table>
				<tr align=center>
					<td class=button width="10%"><INPUT CLASS=cssButton
						VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
						<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
						onclick="turnPage1.previousPage();"> <INPUT
						CLASS=cssButton VALUE="下一页" TYPE=button
						onclick="turnPage1.nextPage();"> <INPUT CLASS=cssButton
						VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
					</td>
				</tr>
			</table>
		</Div>

		<table>
			<input type=hidden id="fmtransact" name="fmtransact">
			<td class=button><input type="button" class=cssButton value="归属"
				id="AddID" name="AddID" onclick="stopAgent()"></td>
			<td class=button><input type="button" class=cssButton
				value="取消归属" id="UpdateID" name="UpdateID" onclick="qstopAgent()">
			</td>
		</table>
		<table>
			<br />
			<tr class=common>
				<td><font color=red size=2> 使用说明：<br />
						1、【查询】：根据管理机构、中介所属销售渠道、中介机构类型、中介机构编码、是否已归属，对已配置的中介机构信息进行查询。<br /> 
						2、【归属】：选择中介机构编码，归属新的中介机构。<br>
						3、【取消归属】：选中查询出的已配置中介机构，若已归属，可对该中介机构进行取消归属处理。<br />
						4、对已配置的中介机构处理时，可对同一分页中相同状态的险种进行批量处理。<br /> 
						5、不同分页中的中介机构不能同时处理。<br /></td>
			</tr>
		</table>
		<span id="spanCode" style="display: none; position: absolute;"></span>
		<!-- 大部分页面都会用到双击下拉控件，spanCode就是双击下拉的框，所以页面必须包含此控件 -->
	</form>
</body>
</html>
