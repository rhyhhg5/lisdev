<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
	function initForm() {
		try {
			initInpBox();
			initAgentGrid();
			showAllCodeName();
		} catch (re) {
			alert("初始化界面错误!");
		}
	}
	function initInpBox() {
	}

	var AgentGrid;
	function initAgentGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名
			iArray[0][1] = "30px"; //列名    
			iArray[0][2] = 0; //列名

			iArray[1] = new Array();
			iArray[1][0] = "管理机构代码"; //列名
			iArray[1][1] = "60px"; //列宽
			iArray[1][2] = 200; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许 

			iArray[2] = new Array();
			iArray[2][0] = "管理机构名称"; //2
			iArray[2][1] = "80px";
			iArray[2][2] = 200;
			iArray[2][3] = 0;

			iArray[3] = new Array();
			iArray[3][0] = "中介所属区销售渠道"; //列名3
			iArray[3][1] = "80px"; //列宽
			iArray[3][2] = 200; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array(); //列名4
			iArray[4][0] = "中介机构类型";
			iArray[4][1] = "80px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[5] = new Array();
			iArray[5][0] = "中介机构编码"; //列名5
			iArray[5][1] = "80px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 0;

			iArray[6] = new Array();
			iArray[6][0] = "中介机构名称"; //列名6
			iArray[6][1] = "100px"; //列宽
			iArray[6][2] = 200; //列最大值
			iArray[6][3] = 0;

			iArray[7] = new Array();
			iArray[7][0] = "销售资格"; //列名7
			iArray[7][1] = "40px"; //列宽
			iArray[7][2] = 200; //列最大值
			iArray[7][3] = 0;

			iArray[8] = new Array();
			iArray[8][0] = "合作状态"; //列名8
			iArray[8][1] = "40px"; //列宽
			iArray[8][2] = 200; //列最大值
			iArray[8][3] = 0;

			iArray[9] = new Array();
			iArray[9][0] = "是否已归属"; //列名9
			iArray[9][1] = "60px"; //列宽
			iArray[9][2] = 200; //列最大值
			iArray[9][3] = 0;

			iArray[10] = new Array();
			iArray[10][0] = "中介销售渠道编码"; //列名3
			iArray[10][1] = "0px"; //列宽
			iArray[10][2] = 200; //列最大值
			iArray[10][3] = 3; //是否允许输入,1表示允许，0表示不允许 

			iArray[11] = new Array();
			iArray[11][0] = "中介机构类型编码"; //列名3
			iArray[11][1] = "0px"; //列宽
			iArray[11][2] = 200; //列最大值
			iArray[11][3] = 3; //是否允许输入,1表示允许，0表示不允许

			AgentGrid = new MulLineEnter("fm", "AgentGrid");
			//设置Grid属性
			AgentGrid.mulLineCount = 0;
			AgentGrid.displayTitle = 1;
			AgentGrid.locked = 1;
			AgentGrid.canSel = 0;
			AgentGrid.canChk = 1;
			AgentGrid.hiddenSubtraction = 1;
			AgentGrid.hiddenPlus = 1;
			AgentGrid.loadMulLine(iArray);
		} catch (ex) {
			alert(ex);
		}
	}
</script>
