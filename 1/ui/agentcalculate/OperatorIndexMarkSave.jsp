<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：OperatorIndexMarkSave.jsp
//程序功能：
//创建日期：2008-03-03
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>
<%
  // 输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
    
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  {
  	transact = request.getParameter("fmtransact");
    String tManageCom = request.getParameter("ManageCom");
    String tBranchType= request.getParameter("BranchType");
    String tBranchType2= request.getParameter("BranchType2");
    String tChk[] = request.getParameterValues("InpOperatorIndexMarkGridChk");
    System.out.println("savejsp:tNum"+tChk.length);
    
    String tIDX[] = request.getParameterValues("OperatorIndexMarkGrid1");
    String tCode1[] = request.getParameterValues("OperatorIndexMarkGrid2");
    String tCode2[] = request.getParameterValues("OperatorIndexMarkGrid3");
    String tCode3[] = request.getParameterValues("OperatorIndexMarkGrid4");
    LADiscountSet tLADiscountSet=new LADiscountSet();
    for(int i=0;i<tChk.length;i++){
    	System.out.println("savejsp:tChk"+i+tChk[i]);
    	if(tChk[i].equals("1")){
    		LADiscountSchema tLADiscountSchema=new LADiscountSchema();
    	if(StrTool.cTrim(tIDX[i]).equals("")){
    		tLADiscountSchema.setIdx(-1);
    	}else{
    		tLADiscountSchema.setIdx(Integer.parseInt(tIDX[i]));
    	}
    	System.out.println("savejsp:tIDX"+StrTool.cTrim(tIDX[i]));
    	System.out.println("savejsp:code1"+tCode1[i]);
    	System.out.println("savejsp:code2"+tCode2[i]);
    	System.out.println("savejsp:code3"+tCode3[i]);
    	tLADiscountSchema.setCode1(tCode1[i]);
    	tLADiscountSchema.setCode2(tCode2[i]);
    	tLADiscountSchema.setCode3(tCode3[i]);
    	tLADiscountSchema.setManageCom(tManageCom);
    	tLADiscountSchema.setBranchType(tBranchType);
    	tLADiscountSchema.setBranchType2(tBranchType2);
    	tLADiscountSet.add(tLADiscountSchema);
    	}
    	
    }
    System.out.println("savejsp:tLADiscountSet.size"+tLADiscountSet.size());
	if(tLADiscountSet.size()==0){
		FlagStr="Fail";
		Content="未选中要操作的数据。";
	}else{
		try
  		{
	  	// 准备传输数据 VData
	    VData tVData = new VData();
	    tVData.addElement(tLADiscountSet);
	    tVData.addElement(tGI);
	    // 数据传输
	    OperatorIndexMarkUI tOperatorIndexMarkUI=new OperatorIndexMarkUI();
	    tOperatorIndexMarkUI.submitData(tVData, transact);
	    tError = tOperatorIndexMarkUI.mErrors;
  		}
	  catch(Exception ex)
	  {
	    Content = "操作失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	  }
	}	  
    
	  if (FlagStr=="")
  	{   
	    if (!tError.needDealError())
	    {                          
	    	Content = " 操作成功! ";
	    	FlagStr = "Success";
	    }
	    else                                                                           
	    {
	    	Content = " 操作失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
  	}
  }

%>
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>