<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：ActiveChargLACommisionInput.jsp
		//程序功能：
		//创建日期：2015-04-15
		//创建人  ：YY程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%
		String CurDate = PubFun.getCurrentDate();
		String BranchType = request.getParameter("BranchType");
		String BranchType2 = request.getParameter("BranchType2");
	%>
	<script>
	     var  MngSQL=" 1 and length(trim(comcode)) in (#8#) and comcode not in (select code from ldcode where codetype =#jiaochaMng#)";
	</script>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="ActiveChargLACommisionInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<%@include file="ActiveChargLACommisionInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./ActiveChargLACommisionSave.jsp" method=post name=fm
			target="fraSubmit">
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divAgent1);">
					</td>
					<td class=titleImg>
						区间提数
					</td>
				</tr>
			</table>
			<Div id="divAgent1" style="display: ''">
				<table class=common>
					<TR class=common>
						<TD class=title>
							管理机构
						</TD>
						<TD class=input>
							<Input class="codeno" name=ManageCom
								verify="管理机构|notnull&code:comcode"
								ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,MngSQL,1);"
								onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,MngSQL,1);"><Input class=codename name=ManageComName readOnly=true 
								elementtype=nacessary>
						</TD>
						<TD class=title>
							上次计算止期
						</TD>
						<TD class=input>
							<input class='readOnly' readOnly name=lastEndDate>
						</TD>
					</TR>
					<TR class=common>
						<TD class=title>
							计算起期
						</TD>
						<TD class=input>
							<!-- Input class="coolDatePicker" dateFormat="short" name=StartDate 	verify="计算起期|DATE&NOTNULL"-->
								<input class='readOnly' readOnly name=StartDate>
						</TD >
						<TD class=title>
							计算止期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=EndDate
								verify="计算止期|DATE&NOTNULL">
						</TD>
					</TR>
					<TR class=input>
					</TR>
				</table>
				<input type=button class=common value="手续费计算"
					onclick="AgentWageCalSave();">
				<input type="hidden" class=input name=CurrentDate
					value="<%=CurDate%>">
				<input type="hidden" class=input name=BranchType
					value="<%=BranchType%>">
				<input type="hidden" class=input name=BranchType2
					value="<%=BranchType2%>">
			</Div>
 <p> <font size="4" color="#ff0000">注:点击手续费计算后，会先计算业务手续费，再根据业务手续费计算出管理手续费,计算完后可在手续费重算页面,单独进行重算.</font></p>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
