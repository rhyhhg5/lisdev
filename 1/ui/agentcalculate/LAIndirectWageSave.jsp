<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LAIndirectWageSave.jsp
//程序功能：
//创建日期：2017-08-25
//创建人  ：zhangyingying创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="java.text.SimpleDateFormat"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>
<%
  // 输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String currentDate = PubFun.getCurrentDate();
  String currentTime = PubFun.getCurrentTime();
  currentDate = AgentPubFun.formatDate(currentDate,"yyyy-MM-dd");
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  String operator = request.getParameter("mOperate");
  String toperator = request.getParameter("tOperate");
  System.out.println("$$$$:"+request.getParameter("mOperate"));
  System.out.println("@@@@:"+request.getParameter("tOperate"));
  LAIndirectWageSet tLAIndirectWageSet=new LAIndirectWageSet();
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  {
   if(operator.equals("INSERT||MAIN")){
    String ManageCom = request.getParameter("manageCom");
    String StartDate = request.getParameter("StartDate");
    String EndDate   = request.getParameter("EndDate");
    System.out.println("EndDate:"+EndDate);
    String Year  = AgentPubFun.formatDate(StartDate,"yyyyMM");
    String Month = AgentPubFun.formatDate(StartDate,"MM");    
    String YearMonth = Year+Month;
    String strInfo = "";
    System.out.println("YearMonth:"+YearMonth);
    SSRS tSSRS = new SSRS();
    if(ManageCom!=null&&!ManageCom.equals("")){
    String sql = "select  comcode from ldcom where 1=1  and  sign='1'  and  length(trim(comcode))=8 and comcode like '"+ManageCom+"%' order by comcode" ;
    ExeSQL tExeSQL = new ExeSQL();
    tSSRS = tExeSQL.execSQL(sql);
 
      FlagStr="Fail";
   		Content=" 管理机构录入不正确！";
    
    }
    StartDate = AgentPubFun.formatDate(StartDate,"yyyy-MM-dd");
    EndDate = AgentPubFun.formatDate(EndDate,"yyyy-MM-dd");
    System.out.println("StartDate2:"+StartDate);
    System.out.println("EndDate2:"+EndDate);
		String currDay=StartDate;
    if(StartDate.compareTo(EndDate)>0)
   {		
    
   		FlagStr="Fail";
   		Content="计算止期应大于计算起期";
   }		
   else
   {				
	         LAIndirectWage tLAIndirectWage=new LAIndirectWage();
              VData tVData=new VData();
              tVData.add(0,ManageCom);
              tVData.add(1,StartDate);
              tVData.add(2,EndDate);              
              if (!tLAIndirectWage.dealData(tVData,operator))
              {
              
              	 	FlagStr="Fail";
              		Content=tLAIndirectWage.mErrors.getFirstError();
              }
              else
             {
            	  Content="保存成功";
              }
   }
  }else if(toperator.equals("GrpRateUpdate||MAIN")||toperator.equals("F1Update||MAIN")){
      String tChk[] = request.getParameterValues("InpLAIndirectWageGridChk");
	  String tCommisionSN[] = request.getParameterValues("LAIndirectWageGrid1");
	  String ttmakedate[]= request.getParameterValues("LAIndirectWageGrid7");
	  String tBranchtype[]= request.getParameterValues("LAIndirectWageGrid18");
	  String tBranchtype2[]= request.getParameterValues("LAIndirectWageGrid19");
	  System.out.println("JJJ:"+tCommisionSN.length);
	  System.out.println("PPP:"+tChk.length);
	  for(int index=0;index<tChk.length;index++)
      {   System.out.println("QQQQQQ:"+tChk[index]);
          if(tChk[index].equals("1")) 
          {  System.out.println("WWWWW");
        	  LAIndirectWageSchema tLAIndirectWageSchema = new LAIndirectWageSchema();
        	  System.out.println("TTTT");
        	  tLAIndirectWageSchema.setCommisionSN(tCommisionSN[index]);
        	  System.out.println("小猫："+tLAIndirectWageSchema.getCommisionSN());
        	  tLAIndirectWageSchema.setTMakeDate(ttmakedate[index]);
        	  System.out.println("熊猫："+tLAIndirectWageSchema.getCommisionSN());
        	  System.out.println("狗狗："+tLAIndirectWageSchema.getTMakeDate());
        	  tLAIndirectWageSet.add(tLAIndirectWageSchema);
        	  System.out.println("伯乐："+tLAIndirectWageSet.size());
        	  
          }
	  }
        VData tVData = new VData();
        tVData.addElement(tGI);
        tVData.addElement(tLAIndirectWageSet);
        LAIndirectWage tLAIndirectWage=new LAIndirectWage();
        if (!tLAIndirectWage.dealData(tVData,toperator))
              {
              	 	FlagStr="Fail";
              		Content=tLAIndirectWage.mErrors.getFirstError();
              }
              else
             {
            	  Content="保存成功";
              }
	 }
  }
         System.out.println(Content);
 //页面有效区
%>
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>