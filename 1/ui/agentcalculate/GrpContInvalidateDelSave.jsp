<%
//程序名称：BlackListInput.jsp
//程序功能：
//创建日期：2003-01-10
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*" %>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  

  LLAppClaimReasonSet tLLAppClaimReasonSet   = new LLAppClaimReasonSet();
	
  GrpContInvalidateUI tGrpContInvalidateUI = new GrpContInvalidateUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("operate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String operator=tG.Operator;
  String grpContNo[]=request.getParameterValues("LLAppClaimReasonGrid1");
  String mngcom[]=request.getParameterValues("LLAppClaimReasonGrid2");
  String operators[]=request.getParameterValues("LLAppClaimReasonGrid3");
  String makedate[]=request.getParameterValues("LLAppClaimReasonGrid4");
  String maketime[]=request.getParameterValues("LLAppClaimReasonGrid5");
  String modifydate[]=request.getParameterValues("LLAppClaimReasonGrid6");
  String modifytime[]=request.getParameterValues("LLAppClaimReasonGrid7");
  String tChk[] = request.getParameterValues("InpLLAppClaimReasonGridChk"); 
  for(int i=0;i<tChk.length;i++){
  if(tChk[i].equals("1")){
    LLAppClaimReasonSchema tLLAppClaimReasonSchema   = new LLAppClaimReasonSchema();
    tLLAppClaimReasonSchema.setRgtNo(grpContNo[i]);
    tLLAppClaimReasonSchema.setMngCom(mngcom[i]);
    tLLAppClaimReasonSchema.setOperator(operators[i]);
    tLLAppClaimReasonSchema.setMakeDate(makedate[i]);
    tLLAppClaimReasonSchema.setMakeTime(maketime[i]);
    tLLAppClaimReasonSchema.setModifyDate(modifydate[i]);
    tLLAppClaimReasonSchema.setModifyTime(modifytime[i]);
    System.out.println("循环进行第"+i+"次  tLLAppClaimReasonSchema.getRgtNo:"+tLLAppClaimReasonSchema.getRgtNo());
    
 
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLLAppClaimReasonSchema);
	tVData.addElement(tG);
  try
  {
    tGrpContInvalidateUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  }
 }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tGrpContInvalidateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	//Content = " 操作失败，原因是:" + tError.getFirstError();
    	Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

