//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAWrapChargeInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  var strSQL="";
  var strSQL = "select substr(a.managecom,1,4),"	 
             +"(select name from ldcom where comcode=substr(a.managecom,1,4)),"
             +" b.F3,(select wrapname from ldwrap where riskwrapcode=b.F3),sum(a.transmoney),sum(a.charge),"
             +" (case a.chargestate when '0' then '未结算'  when '1' then '已结算' when 'X' then '不结算' end )"
             +"from lacharge a,lacommision b"
             +" where 1=1 and a.commisionsn=b.commisionsn"
             +" and (b.F3 is not null or b.F3<>'')"
	         +" and a.tmakedate>='"+fm.all('StartDate').value+"' and a.tmakedate<='"+fm.all('EndDate').value+"'"
	         +" and a.managecom like '"+fm.all('ManageCom').value+"%'"
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
        
        strSQL=strSQL+"group by substr(a.managecom,1,4),b.F3,a.chargestate";
	 
	    turnPage.queryModal(strSQL, WrapChargeGrid);  
	         
	  if (WrapChargeGrid.mulLineCount == 0)
	  {
	    alert("没有查询到相应的代理机构信息！");
	    return false;
	  }
	
}

function beforeSubmit()
{
   return true;
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#2# and BranchType2=#02# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}

function ListExecl()
{
if (WrapChargeGrid.mulLineCount == 0)
{
  alert("列表中没有数据可下载");
  return false;
}
//定义查询的数据
var strSQL = "";
var strSQL = "select substr(a.managecom,1,4),"	 
             +"(select name from ldcom where comcode=substr(a.managecom,1,4)),"
             +" b.F3,(select wrapname from ldwrap where riskwrapcode=b.F3),sum(a.transmoney),sum(a.charge),"
             +" (case a.chargestate when '0' then '未结算'  when '1' then '已结算' when 'X' then '不结算' end )"
             +"from lacharge a,lacommision b"
             +" where 1=1 and a.commisionsn=b.commisionsn"
             +" and (b.F3 is not null or b.F3<>'')"
	         +" and a.tmakedate>='"+fm.all('StartDate').value+"' and a.tmakedate<='"+fm.all('EndDate').value+"'"
	         +" and a.managecom like '"+fm.all('ManageCom').value+"%'"
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
        
        strSQL=strSQL+"group by substr(a.managecom,1,4),b.F3,a.chargestate";
     
fm.querySql.value = strSQL;

//定义列名
var strSQLTitle = "select '管理机构代码','管理机构名称','套餐编码','套餐名称','保费','手续费','手续费状态' from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
//定义表名
fm.all("Title").value="select '中介套餐手续费查询' from dual where 1=1  ";  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}

