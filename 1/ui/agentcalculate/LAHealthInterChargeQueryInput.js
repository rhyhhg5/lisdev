//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//查询下载之前基本校验
function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("财务结算起期必须小于等于财务结算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}


//双击得到agentcom
function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#7# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}

//页面查询
function easyQueryClick()
{	 
	fm.all('divSpanLACommisionGrid').style.display='';
	fm.all('divSpanLACommisionGrid1').style.display='none';
    //首先检验录入框
    if(!verifyInput()) return false;   
	var sql="";
	var tConfState=fm.all('ConfState').value;
	var tContFlag = fm.all('ContFlag').value;
	
	  sql="select a.managecom,(select name from ldcom where comcode =a.managecom),a.agentcom,(select name from lacom where agentcom = a.agentcom)"+
	      ",a.contno,a.riskcode,a.transmoney,a.chargerate"+
	      ",a.charge,a.tmakedate,(select codename from ldcode where codetype='transtype' and code=a.transtype)"+
	      ",case a.chargestate when '0' then '未结算' when '1' then '已结算' else '不结算' end"+
	      ",case when a.chargestate = '0'  then '' when  a.chargestate = '1' and (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  then (select actugetno from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) when a.chargestate = '1' and (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  then (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC')  else '' end"+
	      ",b.p11,b.p14,b.F3,(select cardno from licertify where grpcontno=b.grpcontno fetch first 1 rows only )"+
	      ",(case when a.chargestate = '0'  then '未付' when a.chargestate='X' then '未付' when (a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  and (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) is null ) then '未付' when a.chargestate = '1' and ((select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  or (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) is not null) then '已付' end)"+
	      ",(case when a.chargestate = '0'  then null when  a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  then (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) when a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  then (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') end)"+
	      ",(case (select duefeetype from ljapay where payno = b.receiptno and incomeno = b.contno) when '0' then '新单' else '保全' end),c.groupagentcode"+
	      " from lacharge a,lacommision b,laagent c where a.commisionsn = b.commisionsn and c.agentcode = b.agentcode   and a.chargetype = '65' ";
	      if(fm.all('CardNo').value!=null && fm.all('CardNo').value!='')
	      {
  			sql+=" and a.grpcontno in (select grpcontno from licertify where cardno ='"+fm.all('CardNo').value+"')";
  		  }
	      if(fm.all('GroupAgentCode').value!=null && fm.all('GroupAgentCode').value!='')
	      {
  			sql+=" and c.groupagentcode ='"+fm.all('GroupAgentCode').value+"'";
  		  }
	      if(fm.all('AppntName').value!=null && fm.all('AppntName').value!='')
	      {
  			sql+=" and b.p11  like'%%"+fm.all('AppntName').value+"%%'";
  		  }
	      if(fm.all('ContNo').value!=null && fm.all('ContNo').value!='')
	      {
  			sql+=" and (a.contno ='"+fm.all('ContNo').value+"' or a.grpcontno ='"+fm.all('ContNo').value+"' )";
  		  }
  		  if(tConfState!=null && tConfState!='' && tConfState=="1")
	      {
	        sql+=" and ((select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.commisionsn and othernotype='BC') is not null or (select confdate from ljaget where othernotype='BC' and otherno=a.receiptno fetch first 1 row only )is not null) ";
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="0")
	      {
	        sql+=" and (select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.commisionsn and othernotype='BC') is null and (select confdate from ljaget where othernotype='BC' and otherno=a.receiptno fetch first 1 row only) is null"
	      }
	      if(tContFlag=='0')
	      {
	      	sql+=" and exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else if(tContFlag=='1')
	      {
	      	sql+=" and not exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else{
	       
	      }
            sql+= " and a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' "
         + getWherePart('a.ManageCom', 'ManageCom','like')
         + getWherePart('a.BranchType', 'BranchType')
         + getWherePart('a.BranchType2', 'BranchType2')
         + getWherePart('a.AgentCom', 'AgentCom')
	     + getWherePart('a.ChargeState', 'ChargeState')
	     +" order by a.tmakedate ";
	     
	turnPage.queryModal(sql, LACommisionGrid);   	
}

//页面下载
function doDownLoad(){
    
	if(!verifyInput()) return false;   
	if(!beforeSubmit()) return false;
	var sql="";
	var tConfState=fm.all('ConfState').value;
    var tContFlag = fm.all('ContFlag').value;
    
    sql="select a.managecom,(select name from ldcom where comcode =a.managecom),a.agentcom,(select name from lacom where agentcom = a.agentcom),a.contno,a.riskcode,a.transmoney,a.chargerate"+
	      ",a.charge,a.tmakedate,(select codename from ldcode where codetype='transtype' and code=a.transtype)"+
	      ",case a.chargestate when '0' then '未结算' when '1' then '已结算' else '不结算' end"+
	      ",case when a.chargestate = '0'  then '' when  a.chargestate = '1' and (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  then (select actugetno from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) when a.chargestate = '1' and (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  then (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') else '' end"+
	      ",b.p11,b.p14,b.F3,(select cardno from licertify where grpcontno=b.grpcontno fetch first 1 rows only )"+
	      ",(case when a.chargestate = '0'  then '未付' when a.chargestate='X' then '未付' when (a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  and (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) is null ) then '未付' when a.chargestate = '1' and ((select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  or (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) is not null) then '已付' end)"+
	      ",(case when a.chargestate = '0'  then null when  a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  then (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) when a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  then (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') end)"+
	      ",(case (select duefeetype from ljapay where payno = b.receiptno and incomeno = b.contno) when '0' then '新单' else '保全' end),c.groupagentcode"+
	      " from lacharge a,lacommision b,laagent c where a.commisionsn = b.commisionsn and c.agentcode = b.agentcode and a.chargetype = '65' ";
	      if(fm.all('CardNo').value!=null && fm.all('CardNo').value!='')
	      {
  		    sql+="and a.grpcontno in (select grpcontno from licertify where cardno ='"+fm.all('CardNo').value+"')";
  		  }
	      if(fm.all('GroupAgentCode').value!=null && fm.all('GroupAgentCode').value!='')
	      {
  			sql+=" and c.groupagentcode ='"+fm.all('GroupAgentCode').value+"'";
  		  }
	      if(fm.all('AppntName').value!=null && fm.all('AppntName').value!='')
	      {
  			sql+=" and b.p11 like'%%"+fm.all('AppntName').value+"%%'";
  		  }
	      if(fm.all('ContNo').value!=null && fm.all('ContNo').value!='')
	      {
  			sql+=" and (a.contno ='"+fm.all('ContNo').value+"' or a.grpcontno ='"+fm.all('ContNo').value+"' )";
  		  }
  		  if(tConfState!=null && tConfState!='' && tConfState=="1")
	      {
	        sql+="and ((select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.commisionsn and othernotype='BC') is not null or (select confdate from ljaget where othernotype='BC' and otherno=a.receiptno fetch first 1 row only )is not null) ";
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="0")
	      {
	        sql+="and (select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.commisionsn and othernotype='BC') is null and (select confdate from ljaget where othernotype='BC' and otherno=a.receiptno fetch first 1 row only) is null ";
	      }
	       if(tContFlag=='0')
	      {
	      	sql+="and exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else if(tContFlag=='1')
	      {
	      	sql+="and not exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else{
	       
	      }
            sql+= " and a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' and a.managecom like '"+fm.all('ManageCom').value+"%'"
         + getWherePart('a.BranchType', 'BranchType')
         + getWherePart('a.BranchType2', 'BranchType2')
         + getWherePart('a.AgentCom', 'AgentCom')
	     + getWherePart('a.ChargeState', 'ChargeState')
	     +" order by a.tmakedate "
	
    fm.action = "./LAHealthInterChargeQueryReport.jsp";
    fm.all('querySQL').value=sql;
    fm.submit();
}

function submitForm()
{
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var End = fm.EndDate.value;
  var cur = fm.CurrentDate.value;	
  
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

/*
** 注释：现在这个查询条件被去除了
*/
function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}
