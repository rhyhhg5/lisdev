//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var End = fm.EndDate.value;
	var cur = fm.CurrentDate.value;	
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
	fm.all('divSpanLACommisionGrid').style.display='';
	fm.all('divSpanLACommisionGrid1').style.display='none';
    //首先检验录入框
  if(!verifyInput()) return false;   
 
  var  sql="select	(select name from lacom where agentcom=a.agentcom),	"
			+ "a.contno,"	
			+ "a.AppntName,"  	
			+ "(select  name from labranchgroup where agentgroup=a.agentgroup),"	
			+ "(select  branchattr from labranchgroup where agentgroup=a.agentgroup),"	
			+ "a.agentcode ,"	
			+ "(select name from laagent where agentcode=a.agentcode),"	
			+ "b.riskcode,"
			+ "b.prem,"
			+ "c.confmakedate,"
			+ "a.cvalidate,"
			+ "a.cinvalidate  "
			+ "from lccont a,lcpol b ,ljtempfee c "
			+ "where a.contno=b.contno   "
			+ "and c.TempFeeType in ('1','5','16') "
			+ "and c.othernotype = '4' "
			+ "and a.prtno=c.otherno "
			+ "and c.confdate is null  "
			+ "and a.salechnl in ('03','04') "
			+ "and a.managecom like '"+ManageCom+"%'"
			+ getWherePart('c.confmakedate','WageNo1','<=')+getWherePart('c.confmakedate','WageNo','>=')
			+ "union select (select name from lacom where agentcom=a.agentcom),"
			+ "a.contno,"
			+ "a.AppntName,"  
			+ "(select  name from labranchgroup where agentgroup=a.agentgroup),"
			+ "(select  branchattr from labranchgroup where agentgroup=a.agentgroup),"
			+ "a.agentcode ,"
			+ "(select name from laagent where agentcode=a.agentcode),"
			+ "b.riskcode,"
			+ "b.prem,"
			+ "c.confmakedate,"
			+ "a.cvalidate,"
			+ "a.cinvalidate  "
			+ "from lccont a,lcpol b ,ljtempfee c "
			+ "where a.contno=b.contno  "
			+ "and  a.contno=c.otherno "
			+ "and c.TempFeeType in ('1','5','16') "
			+ "and c.othernotype <> '4' "
			+ "and a.salechnl in ('03','04') "
		    + "and a.managecom like '"+ManageCom+"%'"
			+getWherePart('c.confmakedate','WageNo1','<=')+getWherePart('c.confmakedate','WageNo','>=')
			+ "with ur" ;    

	turnPage.queryModal(sql, LACommisionGrid);   	
	
}
function easyQueryClick1()
{
	fm.all('divSpanLACommisionGrid').style.display='none';
	fm.all('divSpanLACommisionGrid1').style.display='';
    //首先检验录入框
  if(!verifyInput()) return false;   
 
  var  sql= "select (select name from lacom where agentcom=LJAGetEndorse.agentcom)"								
            + ",contno,(select appntname from lbcont where contno=LJAGetEndorse.contno)"								
			+ ",(select   branchattr from labranchgroup where agentgroup=LJAGetEndorse.agentgroup)"								
			+ ",(select   name from labranchgroup where agentgroup=LJAGetEndorse.agentgroup)"								
			+ ",agentcode,(select   name from laagent where agentcode=LJAGetEndorse.agentcode)"								
			+ ",riskcode"								
			+ ",getmoney"								
			+ ",getconfirmdate"								
			+ ",(select cvalidate  from lbcont where contno=LJAGetEndorse.contno)"								
			+ ",(select cinvalidate  from lbcont where contno=LJAGetEndorse.contno)"								
			+ "from LJAGetEndorse "
            + "where agentcom is not null "	
			+ "and GrpPolNo = '00000000000000000000' " 	
			+ "and FeeOperationType = 'WT' "
			+ "and managecom like '"+ManageCom+"%'"
			+getWherePart('getconfirmdate','WageNo1','<=')+getWherePart('getconfirmdate','WageNo','>=')
			+ "with ur ";
	turnPage.queryModal(sql, LACommisionGrid1); 
}
function afterCodeSelect(codeName,Field)
{
	if(codeName == "BranchType2")
	{
          var  sql=" select enddate+1 day  from  lawagelog  where 1=1 and  wageno=(select max(wageno) from lawagelog where branchtype='"+fm.BranchType.value+"' "
                      +"  and  branchtype2='"+fm.BranchType2.value+"' and managecom='"+fm.ManageCom.value+"') "
                     + getWherePart('ManageCom', 'ManageCom')
                     + getWherePart('BranchType', 'BranchType')
          	     + getWherePart('BranchType2', 'BranchType2');
          var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
       
   //查询成功则拆分字符串，返回二维数组
   
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);
	
          if( tArr != null )
	  {
           fm.all('StartDate').value= tArr[0][0];                                   
             
          }
	}
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("计算起期必须小于等于计算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}

function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}

function find1()
{
	if(!verifyInput()) return false;
	fm.action="../f1print/YBPremSave.jsp";
	fm.fmtransact.value="PRINTGET";
	var i = 0;
	var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.button1.disabled=true; 
	fm.submit();
}
function find2()
{
	if(!verifyInput()) return false;
	fm.action="../f1print/YBPremSave.jsp";
	fm.fmtransact.value="";
	var i = 0;
	var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.button1.disabled=true; 
	fm.submit();
	
}


function downAfterSubmit(cfilePath) {
	showInfo.close();
  fm.button1.disabled=false; 
  fm.button2.disabled=false; 
	fileUrl.href = cfilePath; 
  fileUrl.click();
}