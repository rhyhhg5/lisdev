//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{  
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false;
  
 	   
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}



function beforeSubmit()
{
  var grpcontno=fm.all('GrpContNo').value;
	var wageno=fm.all('IndexNo').value;
	var tsql=" select max(distinct indexcalno) from lawage"
	        +" where branchtype='2' and branchtype2='01'  "
	        +" and managecom=(select distinct managecom from lacommision where branchtype='2' and branchtype2='01' and grpcontno='"+grpcontno+"' and wageno='"+wageno+"') ";
       
  var strQueryResult = easyQueryVer3(tsql, 1, 0, 1);
//  alert(strQueryResult);
	if(strQueryResult) {
	var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
	var tsellflag=tArr[0][0] ;
	var wageno1=parseInt(wageno);
	var wageno2=parseInt(tsellflag);

  if(wageno2>=wageno1)
	{
		alert("分配执行年月已经计算过薪资无法进行拆分，请重新选择拆分年月！");	
    return false;
	}
}

  return true;
}

//执行分配处理         
function AgentWageCalSave()
{
  submitForm();	
} 


function easyQueryClick()
{
	  if(!verifyInput()) return false;
	// 初始化表格
	//initSetGrid();
	var grpcontno=fm.all('GrpContNo').value;
	var wageno=fm.all('IndexNo').value;

  // 书写SQL语句
	var strSQL = "";
	strSQL = "select grpcontno,managecom,branchattr,appntno,p11,transmoney,riskcode,"
	+ " signdate,agentcode,(select name from laagent where agentcode=lacommision.agentcode)"
	+ " from lacommision "
	+ " where branchtype='2' and branchtype2='01'  "
	+ " and grpcontno='"+grpcontno+"'"
	+ " and wageno='"+wageno+"'"
	+"  order by grpcontno,signdate";

					//查询SQL，返回结果字符串
					turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

					//判断是否查询成功
					if (!turnPage.strQueryResult) {
						alert("没有符合条件的数据，请重新录入查询条件！");
						return false;
					}

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;

					//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
					//设置初始化过的MULTILINE对象
					turnPage.pageDisplayGrid = SetGrid;
					//保存SQL语句
					turnPage.strQuerySql = strSQL;

					//设置查询起始位置
					turnPage.pageIndex = 0;

					//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
					tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

					//调用MULTILINE对象显示查询结果
					displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
				}