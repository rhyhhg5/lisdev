//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAActiveChargeQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//查询下载之前基本校验
function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("财务结算起期必须小于等于财务结算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}


//双击得到agentcom
function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#5# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}

//页面查询
function easyQueryClick()
{	 
    //首先检验录入框
    if(!verifyInput()) return false;   
	var sql="";
	var tConfState=fm.all('ConfState').value;
	var tContFlag = fm.all('ContFlag').value;
	var tChargeType = fm.all('FeeType').value; //手续费类型
	var tInnerType = fm.all('InnerType').value;
	var tCrsType = fm.all('CrsType').value;
	var cgpdSql=" and 1=1 "
	 if(fm.all("hesitant").value!=null&&fm.all("hesitant").value!=""){
		 if(fm.all("hesitant").value=='0'){
		 cgpdSql=" and ((DAYS(CURRENT DATE)-DAYS(b.CustomGetPolDate))>=15) ";
		 }else{
		 cgpdSql=" and ((DAYS(CURRENT DATE)-DAYS(b.CustomGetPolDate))<15 or (b.CustomGetPolDate) is null) ";
		 }
		 }  
	if(tInnerType=='1'||tInnerType=='2'){
		tCrsType='Y';
	}
	var strGrp=""
	if(fm.all('GrpContNo').value !=""&&fm.all('GrpContNo').value != null)
	{
		strGrp =" and (a.grpcontno ='"+fm.all('GrpContNo').value+"' or a.contno ='"+fm.all('GrpContNo').value+"') ";
	}

	if(tCrsType =='N')
	{
	  sql="select a.managecom,(select name from ldcom where comcode =a.managecom),a.agentcom,(select name from lacom where agentcom = a.agentcom)"+
	      " ,(case when a.grpcontno='00000000000000000000' then a.contno else a.grpcontno end ),a.riskcode,a.transmoney,a.chargerate"+
	      ",a.charge,(case when a.chargetype = '55' then '业务手续费' else '管理手续费' end),a.tmakedate,(select codename from ldcode where codetype='transtype' and code=a.transtype)"+
	      ",case a.chargestate when '0' then '未结算' when '1' then '已结算' else '不结算' end"+
	      ",case when a.chargestate = '0'  then '' when  a.chargestate = '1' then a.commisionsn else '' end"+
	      ",b.p11,b.p14,b.F3,(select cardno from licertify where grpcontno=b.grpcontno fetch first 1 rows only )"+
	      ",(case when a.chargestate = '0'  then '未付' when a.chargestate='X' then '未付' when (a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is null  and (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) is null ) then '未付' when a.chargestate = '1' and ((select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null  or (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) is not null) then '已付' end)"+
	      ",(case when a.chargestate = '0'  then null when  a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is null  then (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) when a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null  then (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) end)"+ 
	      ",(case (select duefeetype from ljapay where payno = b.receiptno and (incomeno = b.contno or incomeno = b.grpcontno)) when '0' then '新单' else '保全' end),b.branchattr,(select name from labranchgroup where agentgroup = b.agentgroup),(select groupagentcode from laagent where agentcode = b.agentcode),(select riskname from lmrisk where riskcode = b.riskcode),b.signdate,b.cvalidate,b.payintv,b.payyears,b.paycount,b.CustomGetPolDate,(case  when (DAYS(CURRENT DATE)-DAYS(b.CustomGetPolDate))>=15 then '是' else '否' end) "+
	      " from lacharge a,lacommision b where a.tcommisionsn = b.commisionsn  and a.branchtype = '5' and a.branchtype2 = '01'" +
	      " and   b.branchtype3 ='N'";
	  	  if(tChargeType=='0') {
	  		  sql+= " and a.chargetype = '55'";
	  	  }
	  	  if(tChargeType=='1'){
	  		  sql+= " and a.chargetype = '56'";
	  	  }
	      if(fm.all('CardNo').value!=null && fm.all('CardNo').value!='')
	      {
  			sql+=" and a.grpcontno in (select grpcontno from licertify where cardno ='"+fm.all('CardNo').value+"')";
  		  }
	      if(fm.all('BranchName').value!=null && fm.all('BranchName').value!='')
	      {
	    	sql+=" and b.branchattr in (select branchattr from labranchgroup where name like '%"+fm.all('BranchName').value+"%')";  
	      }
	      if(fm.all('GroupAgentCode').value!=null && fm.all('GroupAgentCode').value!='')
	      {
	    	sql+=" and b.agentcode in (select agentcode from laagent where groupagentcode = '"+fm.all('GroupAgentCode').value+"')";  
	      }
	      if(fm.all('AgentName').value!=null && fm.all('AgentName').value!='')
	      {
	    	sql+=" and b.agentcode in (select agentcode from laagent where name like '"+fm.all('AgentName').value+"%')";  
	      }

  		  if(tConfState!=null && tConfState!='' && tConfState=="1")
	      {
	        sql+=" and ((select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.tcommisionsn and othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null or (select confdate from ljaget where othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end) and otherno=a.receiptno fetch first 1 row only )is not null) ";
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="0")
	      {
	        sql+=" and (select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.tcommisionsn and othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end)) is null and (select confdate from ljaget where othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end) and otherno=a.receiptno fetch first 1 row only) is null"
	      }
	      if(tContFlag=='0')
	      {
	      	sql+=" and exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else if(tContFlag=='1')
	      {
	      	sql+=" and not exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else{
	       
	      }

            sql+= " and a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' "
            +strGrp
            +cgpdSql
         + getWherePart('a.ManageCom', 'ManageCom','like')
         + getWherePart('a.BranchType', 'BranchType')
         + getWherePart('b.BranchAttr','BranchAttr')
         + getWherePart('a.BranchType2', 'BranchType2')
         + getWherePart('a.AgentCom', 'AgentCom')
         + getWherePart('a.RiskCode','RiskCode')
         + getWherePart('b.SignDate','SignDate')
         + getWherePart('b.CValiDate','CValiDate')
         + getWherePart('b.PayIntv','PayIntv')
         + getWherePart('b.PayYears','PayYears')
         + getWherePart('b.PayCount','PayCount')
//         + getWherePart('a.GrpContNo', 'GrpContNo')
	     + getWherePart('a.ChargeState', 'ChargeState')
	     +" order by a.tmakedate ";
	    turnPage.queryModal(sql, LACommisionGrid);   	
	} 
	else
	{
		 if(fm.all('CrsType').value !=""&&fm.all('CrsType').value!=null&&fm.all('CrsType').value!="0")
 	    {
 	    	strGrp+=" and b.branchtype3='2' ";
 	    }
		 sql="select a.managecom,(select name from ldcom where comcode =a.managecom),a.agentcom,(select name from lacom where agentcom = a.agentcom)"+
		 ",(select GrpAgentCom from lccont where contno = a.contno union select GrpAgentCom from lbcont where contno = a.contno union select GrpAgentCom from lcgrpcont where grpcontno = a.grpcontno union select GrpAgentCom from lbgrpcont where grpcontno = a.grpcontno )" +
		  "	,''" +	      
		  ",(select GrpAgentCode from lccont where contno = a.contno union select GrpAgentCode from lbcont where contno = a.contno union select GrpAgentCode from lcgrpcont where grpcontno = a.grpcontno union select GrpAgentCode from lbgrpcont where grpcontno = a.grpcontno)" +
		  ",(select GrpAgentName from lccont where contno = a.contno union select GrpAgentName from lbcont where contno = a.contno union select GrpAgentName from lcgrpcont where grpcontno = a.grpcontno union select GrpAgentName from lbgrpcont where grpcontno = a.grpcontno)" +
	      " ,(case when a.grpcontno='00000000000000000000' then a.contno else a.grpcontno end ),a.riskcode,a.transmoney,a.chargerate"+
	      ",a.charge,(case when a.chargetype = '55' then '业务手续费' else '管理手续费' end),a.tmakedate,(select codename from ldcode where codetype='transtype' and code=a.transtype)"+
	      ",case a.chargestate when '0' then '未结算' when '1' then '已结算' else '不结算' end"+
	      ",case when a.chargestate = '0'  then '' when  a.chargestate = '1' then a.commisionsn else '' end"+
	      ",b.p11,b.p14,b.F3,(select cardno from licertify where grpcontno=b.grpcontno fetch first 1 rows only )"+
	      ",(case when a.chargestate = '0'  then '未付' when a.chargestate='X' then '未付' when (a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is null  and (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) is null ) then '未付' when a.chargestate = '1' and ((select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null  or (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) is not null) then '已付' end)"+
	      ",(case when a.chargestate = '0'  then null when  a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is null  then (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) when a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null  then (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) end)"+
	      ",(case (select duefeetype from ljapay where payno = b.receiptno and (incomeno = b.contno or incomeno = b.grpcontno)) when '0' then '新单' else '保全' end),b.branchattr,(select name from labranchgroup where agentgroup =b.agentgroup),getunitecode(AgentCode),(select name from laagent where agentcode = b.agentcode),(select riskname from lmrisk where riskcode = b.riskcode),b.signdate,b.cvalidate,b.payintv,b.payyears,b.paycount,b.CustomGetPolDate,(case  when (DAYS(CURRENT DATE)-DAYS(b.CustomGetPolDate))>=15 then '是' else '否' end) "+  	       
	      " from lacharge a,lacommision b where a.tcommisionsn = b.commisionsn  and a.branchtype = '5' and a.branchtype2 = '01'";
	  	  if(tChargeType=='0'){
	  		  sql+= " and a.chargetype = '55'";
	  	  }
	  	  if(tChargeType=='1'){
	  		  sql+= " and a.chargetype = '56'";
	  	  } 
		 if(fm.all('CardNo').value!=null && fm.all('CardNo').value!='')
	      {
 			sql+=" and a.grpcontno in (select grpcontno from licertify where cardno ='"+fm.all('CardNo').value+"')";
 		  }
 		  if(tConfState!=null && tConfState!='' && tConfState=="1")
	      {
	        sql+=" and ((select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.tcommisionsn and othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null or (select confdate from ljaget where othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end) and otherno=a.receiptno fetch first 1 row only )is not null) ";
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="0")
	      {
	        sql+=" and (select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.tcommisionsn and othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end)) is null and (select confdate from ljaget where othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end) and otherno=a.receiptno fetch first 1 row only) is null"
	      }
	      if(fm.all('BranchName').value!=null && fm.all('BranchName').value!='') 
	      {
		    sql+=" and b.branchattr in (select branchattr from labranchgroup where name like  '%"+fm.all('BranchName').value+"%')";  
	      }
	      if(fm.all('GroupAgentCode').value!=null && fm.all('GroupAgentCode').value!='')
	      {
	    	sql+=" and b.agentcode in (select agentcode from laagent where groupagentcode = '"+fm.all('GroupAgentCode').value+"')";  
	      }
	      if(fm.all('AgentName').value!=null && fm.all('AgentName').value!='')
	      {
	    	sql+=" and b.agentcode in (select agentcode from laagent where name like '"+fm.all('AgentName').value+"%')";  
	      }
	      if(tContFlag=='0')
	      {
	      	sql+=" and exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else if(tContFlag=='1')
	      {
	      	sql+=" and not exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else{
	       
	      }
	      //06产代健 07寿代健 
	      if(tInnerType=='1'){
	    	  sql+=" and exists (select 1 from lacom where agentcom = a.agentcom and actype = '06')";
	      }
	      if(tInnerType=='2'){
	    	  sql+=" and exists (select 1 from lacom where agentcom = a.agentcom and actype = '07')";
	      }
           sql+= " and a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' "
           +strGrp
           +cgpdSql           
        + getWherePart('a.ManageCom', 'ManageCom','like')
        + getWherePart('a.BranchType', 'BranchType')
        + getWherePart('a.BranchType2', 'BranchType2')
        + getWherePart('a.AgentCom', 'AgentCom')
        + getWherePart('a.RiskCode','RiskCode')
        + getWherePart('b.SignDate','SignDate')
        + getWherePart('b.CValiDate','CValiDate')
        + getWherePart('b.PayIntv','PayIntv')
        + getWherePart('b.BranchAttr','BranchAttr')
        + getWherePart('b.PayYears','PayYears')
        + getWherePart('b.PayCount','PayCount')
//        + getWherePart('a.GrpContNo', 'GrpContNo')
        + getWherePart('a.ChargeState', 'ChargeState')
	    +" order by a.tmakedate ";
	    turnPage.queryModal(sql, LACommisionGrid2);   
	}
	
}

//页面下载
function doDownLoad(){
    
	if(!verifyInput()) return false;   
	if(!beforeSubmit()) return false;
	var sql="";
	var tConfState=fm.all('ConfState').value;
    var tContFlag = fm.all('ContFlag').value;
	var tChargeType = fm.all('FeeType').value; //手续费类型
	var tInnerType = fm.all('InnerType').value;
	var tCrsType = fm.all('CrsType').value;
	var cgpdSql=" and 1=1 "
	 if(fm.all("hesitant").value!=null&&fm.all("hesitant").value!=""){
		 if(fm.all("hesitant").value=='0'){
		 cgpdSql=" and ((DAYS(CURRENT DATE)-DAYS(b.CustomGetPolDate))>=15) ";
		 }else{
		 cgpdSql=" and ((DAYS(CURRENT DATE)-DAYS(b.CustomGetPolDate))<15 or b.CustomGetPolDate is null) ";
		 }
		 }
	if(tInnerType=='1'||tInnerType=='2'){
		tCrsType='Y';
	}
	var strGrp=""
	if(fm.all('GrpContNo').value !=""&&fm.all('GrpContNo').value != null)
	{
		strGrp =" and (a.grpcontno ='"+fm.all('GrpContNo').value+"' or a.contno ='"+fm.all('GrpContNo').value+"') ";
	}
	if(tCrsType =='N')
	{
	  sql="select a.managecom,(select name from ldcom where comcode =a.managecom),a.agentcom,(select name from lacom where agentcom = a.agentcom)"+
	      " ,(case when a.grpcontno='00000000000000000000' then a.contno else a.grpcontno end ),a.riskcode,a.transmoney,a.chargerate"+
	      ",a.charge,(case when a.chargetype = '55' then '业务手续费' else '管理手续费' end),a.tmakedate,(select codename from ldcode where codetype='transtype' and code=a.transtype)"+
	      ",case a.chargestate when '0' then '未结算' when '1' then '已结算' else '不结算' end"+
	      ",case when a.chargestate = '0'  then '' when  a.chargestate = '1' then a.commisionsn else '' end"+
	      ",b.p11,b.p14,b.F3,(select cardno from licertify where grpcontno=b.grpcontno fetch first 1 rows only )"+
	      ",(case when a.chargestate = '0'  then '未付' when a.chargestate='X' then '未付' when (a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is null  and (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) is null ) then '未付' when a.chargestate = '1' and ((select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null  or (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) is not null) then '已付' end)"+
	      ",(case when a.chargestate = '0'  then null when  a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is null  then (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) when a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null  then (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) end)"+ 
	      ",(case (select duefeetype from ljapay where payno = b.receiptno and (incomeno = b.contno or incomeno = b.grpcontno)) when '0' then '新单' else '保全' end),b.branchattr,(select name from labranchgroup where agentgroup = b.agentgroup),(select groupagentcode from laagent where agentcode = b.agentcode),(select riskname from lmrisk where riskcode = b.riskcode),b.signdate,b.cvalidate,b.payintv,b.payyears,b.paycount,b.CustomGetPolDate,(case  when (DAYS(CURRENT DATE)-DAYS(b.CustomGetPolDate))>=15 then '是' else '否' end) "+
	      " from lacharge a,lacommision b where a.tcommisionsn = b.commisionsn  and a.branchtype = '5' and a.branchtype2 = '01'" +
	      " and   b.branchtype3 ='N'";
	  	  if(tChargeType=='0') {
	  		  sql+= " and a.chargetype = '55'";
	  	  }
	  	  if(tChargeType=='1'){
	  		  sql+= " and a.chargetype = '56'";
	  	  }
	      if(fm.all('CardNo').value!=null && fm.all('CardNo').value!='')
	      {
  			sql+=" and a.grpcontno in (select grpcontno from licertify where cardno ='"+fm.all('CardNo').value+"')";
  		  }
	      if(fm.all('BranchName').value!=null && fm.all('BranchName').value!='')
	      {
	    	sql+=" and b.branchattr in (select branchattr from labranchgroup where name like '%"+fm.all('BranchName').value+"%')";  
	      }
	      if(fm.all('GroupAgentCode').value!=null && fm.all('GroupAgentCode').value!='')
	      {
	    	sql+=" and b.agentcode in (select agentcode from laagent where groupagentcode = '"+fm.all('GroupAgentCode').value+"')";  
	      }
	      if(fm.all('AgentName').value!=null && fm.all('AgentName').value!='')
	      {
	    	sql+=" and b.agentcode in (select agentcode from laagent where name like '"+fm.all('AgentName').value+"%')";  
	      }

  		  if(tConfState!=null && tConfState!='' && tConfState=="1")
	      {
	        sql+=" and ((select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.tcommisionsn and othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null or (select confdate from ljaget where othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end) and otherno=a.receiptno fetch first 1 row only )is not null) ";
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="0")
	      {
	        sql+=" and (select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.tcommisionsn and othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end)) is null and (select confdate from ljaget where othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end) and otherno=a.receiptno fetch first 1 row only) is null"
	      }
	      if(tContFlag=='0')
	      {
	      	sql+=" and exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else if(tContFlag=='1')
	      {
	      	sql+=" and not exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else{
	       
	      }

            sql+= " and a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' "
            +strGrp
            +cgpdSql
         + getWherePart('a.ManageCom', 'ManageCom','like')
         + getWherePart('a.BranchType', 'BranchType')
         + getWherePart('b.BranchAttr','BranchAttr')
         + getWherePart('a.BranchType2', 'BranchType2')
         + getWherePart('a.AgentCom', 'AgentCom')
         + getWherePart('a.RiskCode','RiskCode')
         + getWherePart('b.SignDate','SignDate')
         + getWherePart('b.CValiDate','CValiDate')
         + getWherePart('b.PayIntv','PayIntv')
         + getWherePart('b.PayYears','PayYears')
         + getWherePart('b.PayCount','PayCount')
//         + getWherePart('a.GrpContNo', 'GrpContNo')
	     + getWherePart('a.ChargeState', 'ChargeState')
	     +" order by a.tmakedate ";  	
	} 
	else
	{
		 if(fm.all('CrsType').value !=""&&fm.all('CrsType').value!=null&&fm.all('CrsType').value!="0")
 	    {
 	    	strGrp+=" and b.branchtype3='2' ";
 	    }
		 sql="select a.managecom,(select name from ldcom where comcode =a.managecom),a.agentcom,(select name from lacom where agentcom = a.agentcom)"+
		 ",(select GrpAgentCom from lccont where contno = a.contno union select GrpAgentCom from lbcont where contno = a.contno union select GrpAgentCom from lcgrpcont where grpcontno = a.grpcontno union select GrpAgentCom from lbgrpcont where grpcontno = a.grpcontno )" +
//		  "	,''" +	      
		  ",(select GrpAgentCode from lccont where contno = a.contno union select GrpAgentCode from lbcont where contno = a.contno union select GrpAgentCode from lcgrpcont where grpcontno = a.grpcontno union select GrpAgentCode from lbgrpcont where grpcontno = a.grpcontno)" +
		  ",(select GrpAgentName from lccont where contno = a.contno union select GrpAgentName from lbcont where contno = a.contno union select GrpAgentName from lcgrpcont where grpcontno = a.grpcontno union select GrpAgentName from lbgrpcont where grpcontno = a.grpcontno)" +
	      " ,(case when a.grpcontno='00000000000000000000' then a.contno else a.grpcontno end ),a.riskcode,a.transmoney,a.chargerate"+
	      ",a.charge,(case when a.chargetype = '55' then '业务手续费' else '管理手续费' end),a.tmakedate,(select codename from ldcode where codetype='transtype' and code=a.transtype)"+
	      ",case a.chargestate when '0' then '未结算' when '1' then '已结算' else '不结算' end"+
	      ",case when a.chargestate = '0'  then '' when  a.chargestate = '1' then a.commisionsn else '' end"+
	      ",b.p11,b.p14,b.F3,(select cardno from licertify where grpcontno=b.grpcontno fetch first 1 rows only )"+
	      ",(case when a.chargestate = '0'  then '未付' when a.chargestate='X' then '未付' when (a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is null  and (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) is null ) then '未付' when a.chargestate = '1' and ((select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null  or (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) is not null) then '已付' end)"+
	      ",(case when a.chargestate = '0'  then null when  a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is null  then (select confdate from ljaget where otherno = a.receiptno and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end) fetch first 1 rows only ) when a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null  then (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.tcommisionsn and othernotype = (case when a.chargetype = '55' then 'BC' else 'MC' end)) end)"+
	      ",(case (select duefeetype from ljapay where payno = b.receiptno and (incomeno = b.contno or incomeno = b.grpcontno)) when '0' then '新单' else '保全' end),b.branchattr,(select name from labranchgroup where agentgroup =b.agentgroup),getunitecode(AgentCode),(select name from laagent where agentcode = b.agentcode),(select riskname from lmrisk where riskcode = b.riskcode),b.signdate,b.cvalidate,b.payintv,b.payyears,b.paycount,b.CustomGetPolDate,(case  when (DAYS(CURRENT DATE)-DAYS(b.CustomGetPolDate))>=15 then '是' else '否' end) "+  	       
	      " from lacharge a,lacommision b where a.tcommisionsn = b.commisionsn  and a.branchtype = '5' and a.branchtype2 = '01'";
	  	  if(tChargeType=='0'){
	  		  sql+= " and a.chargetype = '55'";
	  	  }
	  	  if(tChargeType=='1'){
	  		  sql+= " and a.chargetype = '56'";
	  	  } 
		 if(fm.all('CardNo').value!=null && fm.all('CardNo').value!='')
	      {
 			sql+=" and a.grpcontno in (select grpcontno from licertify where cardno ='"+fm.all('CardNo').value+"')";
 		  }
 		  if(tConfState!=null && tConfState!='' && tConfState=="1")
	      {
	        sql+=" and ((select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.tcommisionsn and othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end)) is not null or (select confdate from ljaget where othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end) and otherno=a.receiptno fetch first 1 row only )is not null) ";
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="0")
	      {
	        sql+=" and (select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.tcommisionsn and othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end)) is null and (select confdate from ljaget where othernotype=(case when a.chargetype = '55' then 'BC' else 'MC' end) and otherno=a.receiptno fetch first 1 row only) is null"
	      }
	      if(fm.all('BranchName').value!=null && fm.all('BranchName').value!='') 
	      {
		    sql+=" and b.branchattr in (select branchattr from labranchgroup where name like  '%"+fm.all('BranchName').value+"%')";  
	      }
	      if(fm.all('GroupAgentCode').value!=null && fm.all('GroupAgentCode').value!='')
	      {
	    	sql+=" and b.agentcode in (select agentcode from laagent where groupagentcode = '"+fm.all('GroupAgentCode').value+"')";  
	      }
	      if(fm.all('AgentName').value!=null && fm.all('AgentName').value!='')
	      {
	    	sql+=" and b.agentcode in (select agentcode from laagent where name like '"+fm.all('AgentName').value+"%')";  
	      }
	      if(tContFlag=='0')
	      {
	      	sql+=" and exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else if(tContFlag=='1')
	      {
	      	sql+=" and not exists(select 1 from ljapay where payno = b.receiptno and incomeno = b.contno and duefeetype = '0' )";
	      }else{
	       
	      }
	      //06产代健 07寿代健 
	      if(tInnerType=='1'){
	    	  sql+=" and exists (select 1 from lacom where agentcom = a.agentcom and actype = '06')";
	      }
	      if(tInnerType=='2'){
	    	  sql+=" and exists (select 1 from lacom where agentcom = a.agentcom and actype = '07')";
	      }
           sql+= " and a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' "
           +strGrp
           +cgpdSql           
        + getWherePart('a.ManageCom', 'ManageCom','like')
        + getWherePart('a.BranchType', 'BranchType')
        + getWherePart('a.BranchType2', 'BranchType2')
        + getWherePart('a.AgentCom', 'AgentCom')
        + getWherePart('a.RiskCode','RiskCode')
        + getWherePart('b.SignDate','SignDate')
        + getWherePart('b.CValiDate','CValiDate')
        + getWherePart('b.PayIntv','PayIntv')
        + getWherePart('b.BranchAttr','BranchAttr')
        + getWherePart('b.PayYears','PayYears')
        + getWherePart('b.PayCount','PayCount')
//        + getWherePart('a.GrpContNo', 'GrpContNo')
        + getWherePart('a.ChargeState', 'ChargeState')
	    +" order by a.tmakedate ";
	}
    fm.action = "./LAActiveChargeQueryReport.jsp?crstype="+fm.all('CrsType').value;
    fm.all('querySQL').value=sql;
    fm.submit();
}

function submitForm()
{
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var End = fm.EndDate.value;
  var cur = fm.CurrentDate.value;	
  
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

/*
** 注释：现在这个查询条件被去除了
*/
function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#5# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}
function afterCodeSelect( cCodeName, Field )
{
  if( cCodeName == "crstype")
  {
    if(fm.all('CrsType').value=='N')
    {
    	document.getElementById("divSpanLACommisionGrid").style.display='';
    	document.getElementById("divSpanLACommisionGrid2").style.display='none';
    	initLACommisionGrid(); 
        initLACommisionGrid2();
    }
    else
    {
    	document.getElementById("divSpanLACommisionGrid").style.display='none';
    	document.getElementById("divSpanLACommisionGrid2").style.display='';
    	initLACommisionGrid(); 
        initLACommisionGrid2();
    }
  }
}
