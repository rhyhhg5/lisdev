//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();   
var turnPage2 = new turnPageClass(); //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var End = fm.EndDate.value;
	var cur = fm.CurrentDate.value;	
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
	fm.all('divSpanLACommisionGrid').style.display='';
	fm.all('divSpanLACommisionGrid1').style.display='none';
	initLACommisionGrid();
	initLACommisionGrid1();
	    //首先检验录入框
  if(!verifyInput()) return false;   
	
	var tAgentCom=fm.all('AgentCom').value;
	var tConfState=fm.all('ConfState').value;
	var sql="";
	sql = "select (select name from ldcom where comcode = a.managecom),"
	    +"a.agentcom,(select name from lacom where agentcom= a.agentcom),"
	    +"(case when a.grpcontno='00000000000000000000' then a.contno else a.grpcontno end),a.riskcode,a.transmoney,a.chargerate,a.charge,"
	    +"case b.paymode when '4' then '银行代收' else (select codename from ldcode where codetype = 'paymode' and code = b.paymode) end,"
	    +"b.TPayDate,case when a.chargestate = '0'  then '' when  a.chargestate = '1' and (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  then (select actugetno from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) when a.chargestate = '1' and (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  then (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') end"
	    //+",(select completetime from returnvisittable where policyno=a.contno  fetch first 1 rows only ) "
	    +",(case when a.chargestate = '0'  then '未付' when (a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  and (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) is null ) then '未付' when a.chargestate = '1' and ((select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  or (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) is not null) then '已付' end)"
	    +",(case when a.chargestate = '0'  then null when  a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  then (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) when a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  then (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') end)"
	    +",(case when a.grpcontno='00000000000000000000' then (select max(completetime) from returnvisittable where policyno = a.contno and returnvisitflag  in ('1','4') ) else (select max(completetime) from returnvisittable where policyno = a.grpcontno and returnvisitflag  in ('1','4') ) end) "
	    +" from lacharge a,lacommision b where 1= 1  and a.commisionsn = b.commisionsn  and a.branchtype = '3' and a.branchtype2 = '01' "
	    +" and a.tmakedate >='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' and a.managecom like '"+ fm.all('ManageCom').value+"%' "
	      + getWherePart('a.BranchType', 'BranchType')
	      + getWherePart('a.chargestate', 'ChargeState');
	      if(tAgentCom!=null && tAgentCom!='')
	      {
	       sql+=" and a.agentcom like '"+tAgentCom+"%' " ;
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="1")
	      {
	       sql+="and ((select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.commisionsn and othernotype='BC') is not null or (select confdate from ljaget where othernotype='BC' and otherno=a.receiptno fetch first 1 row only )is not null) "
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="0")
	      {
	       sql+="and (select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.commisionsn and othernotype='BC') is null and (select confdate from ljaget where othernotype='BC' and otherno=a.receiptno fetch first 1 row only) is null"
	      }
	      
	      sql+=" order by a.tmakedate";
	      
 
	turnPage.queryModal(sql, LACommisionGrid);   	
	
}
function easyQueryClick1()
{
	fm.all('divSpanLACommisionGrid').style.display='none';
	fm.all('divSpanLACommisionGrid1').style.display='';
	initLACommisionGrid();
	initLACommisionGrid1();
    //首先检验录入框
  if(!verifyInput()) return false;   
 
  var  sql= 
           "select (select name from lacom where agentcom=LACharge.AgentCom) comName,(select name from ldcom where comcode=LACharge.ManageCom) ManageName,"
 					+"(select name from labranchgroup where agentgroup=LACOMMISION.agentgroup)  groupname, "
 					+"LACOMMISION.agentcode, "
 					+"(select  name from laagent where agentcode=LACOMMISION.agentcode) agentName  , "
 					+"LACharge.ContNo,"
 					+"(select  appntname  from lccont where contno=LACharge.contno union select  appntname  from lbcont where contno=LACharge.contno) appntname, "
 					+"LACharge.RiskCode, sum(LACharge.TransMoney), LACOMMISION.tenteraccdate,LACOMMISION.signdate,(select CInValiDate from lccont where contno =LACharge.contno union select CInValiDate from lbcont where contno =LACharge.contno ) CInValiDate  " 			
 					+"from 	LACharge ,LACOMMISION where 1=1 "
 					+ " AND LACharge.CommisionSN = LACOMMISION.CommisionSN"
          + " and LACharge.GrpContNo = '00000000000000000000' and LACOMMISION.tmakedate>='"+fm.all('StartDate').value+"'and LACOMMISION.tmakedate<='"+fm.all('EndDate').value+"' " 
           + "and lacharge.managecom like '"+ fm.all('ManageCom').value+"%'"
          + getWherePart('LACharge.BranchType', 'BranchType')
          + getWherePart('LACharge.AgentCom', 'AgentCom','like') 
          + " group by LACharge.AgentCom,LACharge.ManageCom,LACOMMISION.agentgroup,LACOMMISION.agentcode,"  
          + "  LACharge.RiskCode,  LACOMMISION.tenteraccdate,LACOMMISION.signdate,lacharge.contno"     	 
          +	" union select (select name from lacom where agentcom=LACharge.AgentCom) comName,(select name from ldcom where comcode=LACharge.ManageCom) ManageName,"
 					+"(select name from labranchgroup where agentgroup=LACOMMISION.agentgroup)  groupname, "
 					+"LACOMMISION.agentcode, "
 					+"(select  name from laagent where agentcode=LACOMMISION.agentcode) agentName  , "
 					+"LACharge.grpContNo,"
 					+"(select  appntname  from lccont where grpContNo=LACharge.grpContNo union select  appntname  from lbcont where grpContNo=LACharge.grpContNo) appntname, "
 					+"LACharge.RiskCode, sum(LACharge.TransMoney), LACOMMISION.tenteraccdate,LACOMMISION.signdate,(select CInValiDate from lccont where grpContNo =LACharge.grpContNo union select CInValiDate from lbcont where grpContNo =LACharge.grpContNo ) CInValiDate  " 			
 					+"from 	LACharge ,LACOMMISION where 1=1 "
 					+ " AND LACharge.CommisionSN = LACOMMISION.CommisionSN"
          + " and LACharge.GrpContNo <> '00000000000000000000' and LACOMMISION.tmakedate>='"+fm.all('StartDate').value+"'and LACOMMISION.tmakedate<='"+fm.all('EndDate').value+"' " 
           + "and lacharge.managecom like '"+ fm.all('ManageCom').value+"%'"
          + getWherePart('LACharge.BranchType', 'BranchType')
          + getWherePart('LACharge.AgentCom', 'AgentCom','like') 
          + " group by LACharge.AgentCom,LACharge.ManageCom,LACOMMISION.agentgroup,LACOMMISION.agentcode,"  
          + "  LACharge.RiskCode,  LACOMMISION.tenteraccdate,LACOMMISION.signdate,lacharge.grpContNo" ;       	 
	turnPage2.queryModal(sql, LACommisionGrid1); 

}


function DoDownload()
{
    if(!verifyInput()) return false;   
	
	var tAgentCom=fm.all('AgentCom').value;
	var tConfState=fm.all('ConfState').value;
	var sql="";
	sql = "select (select name from ldcom where comcode = a.managecom),a.agentcom,"
	   +"(select name from lacom where agentcom= a.agentcom),(case when a.grpcontno='00000000000000000000' then a.contno else a.grpcontno end),"
	   +"(select name from laagent where agentcode= b.agentcode),b.p11,a.riskcode,a.transmoney,"
	   +"a.chargerate,a.charge"
	   +",case b.paymode when '4' then '银行代收' else (select codename from ldcode where codetype = 'paymode' and code = b.paymode) end,b.TPayDate"
	   +",case when a.chargestate = '0'  then '' when  a.chargestate = '1' and (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  then (select actugetno from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) when a.chargestate = '1' and (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  then (select actugetno from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC')  end"
       +", case when a.chargestate = '1'  then a.modifydate else null end " 
       //+",(select completetime from returnvisittable where policyno=a.contno  fetch first 1 rows only ) "
       +",(case when a.chargestate = '0'  then '未付' when (a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  and (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) is null ) then '未付' when a.chargestate = '1' and ((select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  or (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) is not null) then '已付' end)"
	   +",(case when a.chargestate = '0'  then null when  a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is null  then (select confdate from ljaget where otherno = a.receiptno and othernotype = 'BC' fetch first 1 rows only ) when a.chargestate = '1' and (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') is not null  then (select confdate from ljaget where otherno = a.receiptno and getnoticeno = a.commisionsn and othernotype = 'BC') end)"
	   +",(case when a.grpcontno='00000000000000000000' then (select max(completetime) from returnvisittable where policyno = a.contno and returnvisitflag  in ('1','4') ) else (select max(completetime) from returnvisittable where policyno = a.grpcontno and returnvisitflag  in ('1','4') ) end)"
       +"  from lacharge a,lacommision b "
       +"where 1= 1 and a.commisionsn = b.commisionsn "
       +" and a.branchtype = '3' and a.branchtype2 = '01' "+
	      " and a.tmakedate >='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' and a.managecom like '"+ fm.all('ManageCom').value+"%' "
	      + getWherePart('a.chargestate', 'ChargeState');
	     if(tAgentCom!=null && tAgentCom!='')
	      {
	       sql+=" and a.agentcom like '"+tAgentCom+"%' ";
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="1")
	      {
	       sql+="and ((select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.commisionsn and othernotype='BC') is not null or (select confdate from ljaget where othernotype='BC' and otherno=a.receiptno fetch first 1 row only )is not null) "
	      }
	      if(tConfState!=null && tConfState!='' && tConfState=="0")
	      {
	       sql+="and (select confdate from ljaget where otherno=a.receiptno and getnoticeno=a.commisionsn and othernotype='BC') is null and (select confdate from ljaget where othernotype='BC' and otherno=a.receiptno fetch first 1 row only) is null"
	      }
	      
	      sql+=" order by a.tmakedate";
	      
    fm.querySql.value = sql;
	fm.all('Type').value="Charge";
	var i = 0;
	var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
	showInfo.close();	
}

function DoDownload1()
{
	
	var  strSQL= 
           "select (select name from lacom where agentcom=LACharge.AgentCom) comName,(select name from ldcom where comcode=LACharge.ManageCom) ManageName,"
 					+"(select name from labranchgroup where agentgroup=LACOMMISION.agentgroup)  groupname, "
 					+"LACOMMISION.agentcode, "
 					+"(select  name from laagent where agentcode=LACOMMISION.agentcode) agentName  , "
 					+"LACharge.ContNo, "
 					+"(select  appntname  from lccont where contno=LACharge.contno union select  appntname  from lbcont where contno=LACharge.contno) appntname, "
 					+"LACharge.RiskCode, sum(LACharge.TransMoney), LACOMMISION.tenteraccdate,LACOMMISION.signdate,(select CInValiDate from lccont where contno =LACharge.contno union select CInValiDate from lbcont where contno =LACharge.contno ) CInValiDate  " 			
 					+"from 	LACharge ,LACOMMISION where 1=1 "
 					+ " AND LACharge.CommisionSN = LACOMMISION.CommisionSN"
          + " and LACharge.GrpContNo = '00000000000000000000' and LACOMMISION.tmakedate>='"+fm.all('StartDate').value+"'and LACOMMISION.tmakedate<='"+fm.all('EndDate').value+"' " 
           + "and lacharge.managecom like '"+ fm.all('ManageCom').value+"%'"
          + getWherePart('LACharge.BranchType', 'BranchType');
                if(fm.all('AgentCom').value!=null&&fm.all('AgentCom').value!='')
          	 {strSQL=strSQL+" and LACharge.AgentCom like '"+ fm.all('AgentCom').value+"%'";}
         strSQL=strSQL+" group by LACharge.AgentCom,LACharge.ManageCom,LACOMMISION.agentgroup,LACOMMISION.agentcode,"  
          + "  LACharge.RiskCode,  LACOMMISION.tenteraccdate,LACOMMISION.signdate,lacharge.contno"     	 
          + " union select (select name from lacom where agentcom=LACharge.AgentCom) comName,(select name from ldcom where comcode=LACharge.ManageCom) ManageName,"
 					+"(select name from labranchgroup where agentgroup=LACOMMISION.agentgroup)  groupname, "
 					+"LACOMMISION.agentcode, "
 					+"(select  name from laagent where agentcode=LACOMMISION.agentcode) agentName  , "
 					+"LACharge.grpContNo, "
 					+"(select  appntname  from lccont where grpContNo=LACharge.grpContNo union select  appntname  from lbcont where grpContNo=LACharge.grpContNo) appntname, "
 					+"LACharge.RiskCode, sum(LACharge.TransMoney), LACOMMISION.tenteraccdate,LACOMMISION.signdate,(select CInValiDate from lccont where grpContNo =LACharge.grpContNo union select CInValiDate from lbcont where grpContNo =LACharge.grpContNo ) CInValiDate  " 			
 					+"from 	LACharge ,LACOMMISION where 1=1 "
 					+ " AND LACharge.CommisionSN = LACOMMISION.CommisionSN"
          + " and LACharge.GrpContNo <> '00000000000000000000' and LACOMMISION.tmakedate>='"+fm.all('StartDate').value+"'and LACOMMISION.tmakedate<='"+fm.all('EndDate').value+"' " 
           + "and lacharge.managecom like '"+ fm.all('ManageCom').value+"%'"
          + getWherePart('LACharge.BranchType', 'BranchType');
                if(fm.all('AgentCom').value!=null&&fm.all('AgentCom').value!='')
          	 {strSQL=strSQL+" and LACharge.AgentCom like '"+ fm.all('AgentCom').value+"%'";}
         strSQL=strSQL+" group by LACharge.AgentCom,LACharge.ManageCom,LACOMMISION.agentgroup,LACOMMISION.agentcode,"  
          + "  LACharge.RiskCode,  LACOMMISION.tenteraccdate,LACOMMISION.signdate,lacharge.grpContNo" 	 ;  
	fm.querySql.value = strSQL;
  
	if(!verifyInput()) return false;
	//fm.action="../BankChargeQuerySave.jsp";
	fm.all('Type').value="Commision";
	var i = 0;
	var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
	showInfo.close();
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == "BranchType2")
	{
          var  sql=" select enddate+1 day  from  lawagelog  where 1=1 and  wageno=(select max(wageno) from lawagelog where branchtype='"+fm.BranchType.value+"' "
                     +"  and  branchtype2='"+fm.BranchType2.value+"' and managecom='"+fm.ManageCom.value+"') "
                     + getWherePart('ManageCom', 'ManageCom','like')
                     + getWherePart('BranchType', 'BranchType')
          	     + getWherePart('BranchType2', 'BranchType2');
          var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
       
   //查询成功则拆分字符串，返回二维数组
   
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);
	
          if( tArr != null )
	  {
           fm.all('StartDate').value= tArr[0][0];                                   
             
          }
	}
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("计算起期必须小于等于计算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}

function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}
