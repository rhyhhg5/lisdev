<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：DataIntoLACommisionSave.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="java.text.SimpleDateFormat"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>
<%
  // 输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String currentDate = PubFun.getCurrentDate();
  String currentTime = PubFun.getCurrentTime();
  currentDate = AgentPubFun.formatDate(currentDate,"yyyy-MM-dd");
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
 
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  {
  	String tWhereSql = request.getParameter("configwhereSql");
  	String tOperator = request.getParameter("operator");
  	
    String tManageCom = request.getParameter("ManageCom");
    String tWageNo = request.getParameter("WageNo");
    String tGrpContNo = request.getParameter("GrpContNo");
    String tContNo = request.getParameter("ContNo");
    String tTMakeDate = request.getParameter("TMakeDate");
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ManageCom", tManageCom);
    tTransferData.setNameAndValue("WageNo", tWageNo);
    tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
    tTransferData.setNameAndValue("ContNo", tContNo);
    tTransferData.setNameAndValue("TMakeDate", tTMakeDate);
    tTransferData.setNameAndValue("configwhereSql", tWhereSql);
    VData tVData = new VData();
    tVData.add(tTransferData);
    tVData.add(tGI);
    AgentWageOfFillData tAgentWageOfFillData = new AgentWageOfFillData();
    if(!tAgentWageOfFillData.submitData(tVData,tOperator))
   	{
    	 FlagStr = "Fail";
   	     Content = tAgentWageOfFillData.mErrors.getFirstError();
   	}
    else
    {
    	FlagStr = "success";
  	    Content = "执行成功";
    }
   }
 
%>
<html>
<script language="javascript">
      parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>