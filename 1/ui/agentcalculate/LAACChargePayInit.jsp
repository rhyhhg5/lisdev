<%
//程序名称：DataIntoLACommisionInit.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
    fm.all('BranchType').value = mBranchType;
  }
  catch(ex)
  {
    alert("LAACChargePayInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
    initLACommisionGrid();    
  }
  catch(re)
  {
    alert("LAACChargePayInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var LACommisionGrid;
function initLACommisionGrid() {                           
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构编码";         		//列名
    iArray[1][1]="80px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="中介机构编码";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    iArray[4]=new Array();
    iArray[4][0]="中介机构名称";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="合同号";         		//列名
    iArray[5][1]="80px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许    
    
    iArray[6]=new Array();
    iArray[6][0]="险种";         		//列名
    iArray[6][1]="60px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="保费";         		//列名
    iArray[7][1]="60px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
           
    iArray[8]=new Array();
    iArray[8][0]="手续费比例";         		//列名
    iArray[8][1]="80px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="手续费";         		//列名
    iArray[9][1]="60px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="签单日期";         		//列名
    iArray[10][1]="70px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="财务结算日期";         		//列名
    iArray[11][1]="80px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="财务结算号";         		//列名
    iArray[12][1]="0px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();
    iArray[13][0]="财务结算类型代码";         		//列名
    iArray[13][1]="0px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();
    iArray[14][0]="财务结算类型";         		//列名
    iArray[14][1]="80px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=0;         		//是否允许录入，0--不能，1--允许
    
    
    iArray[15]=new Array();
    iArray[15][0]="主键";         		//列名
    iArray[15][1]="0px";         		//宽度
    iArray[15][3]=100;         		//最大长度
    iArray[15][4]=0;         		//是否允许录入，0--不能，1--允许
    
    
    iArray[16]=new Array();
    iArray[16][0]="套餐编码";         		//列名
    iArray[16][1]="80px";         		//宽度
    iArray[16][3]=100;         		//最大长度
    iArray[16][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[17]=new Array();
    iArray[17][0]="保单类型";         		//列名
    iArray[17][1]="80px";         		//宽度
    iArray[17][3]=100;         		//最大长度
    iArray[17][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[18]=new Array();
    iArray[18][0]="中介专员";         		//列名
    iArray[18][1]="80px";         		//宽度
    iArray[18][3]=100;         		//最大长度
    iArray[18][4]=0;         		//是否允许录入，0--不能，1--允许
    
   LACommisionGrid = new MulLineEnter( "fm" , "LACommisionGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACommisionGrid.mulLineCount = 0;   
    LACommisionGrid.displayTitle = 1;
    LACommisionGrid.hiddenPlus = 1;
    LACommisionGrid.hiddenSubtraction = 1;
    LACommisionGrid.canSel = 0;
    LACommisionGrid.canChk = 1;
  
    LACommisionGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACommisionGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
