//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
//  showSubmitFrame(mDebug);
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在ReDataInto.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行提数         
function AgentWageCalSave()
{
  submitForm();	
}   

function beforeSubmit()
{
   if (fm.all('StartDate').value > fm.all('EndDate').value)
   {
   	alert("计算起期必须小于等于计算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   makeUpCalTypeStr();
   return true;
}

function makeUpCalTypeStr()
{
    var strCalType1 = "CalType in (";
    var strCalType2 = "";
    if(fm.DirectWage.checked)
    {
    	if (fm.BranchType.value == null || fm.BranchType.value == '')
    	   strCalType1 += "'00','09','10'"; 
    	else if (fm.BranchType.value == '1')
    	   strCalType1 += "'00'";
    	else if (fm.BranchType.value == '2')
    	   strCalType1 += "'10'";
    	else if (fm.BranchType.value == '3')
    	   strCalType1 += "'09'";
    }
    if (fm.StandPrem.checked)
    {
    	if (strCalType1 != "CalType in (")
    	   strCalType1 += ",";
    	strCalType1 += "'03'";
    }
    if (fm.CalCount.checked)
    {
    	if (strCalType1 != "CalType in (")
    	   strCalType1 += ",";
    	strCalType1 += "'04'";    	
    }
    if (fm.GrpFyc.checked)
    {
    	if (strCalType1 != "CalType in (")
    	   strCalType1 += ",";
    	strCalType1 += "'01'";    	
    }
    if (fm.DepFyc.checked)
    {    	
    	if (strCalType1 != "CalType in (")
    	   strCalType1 += ",";
    	strCalType1 += "'02'";
    }        
    if (strCalType1 != "CalType in (")
    	strCalType1 += ")";
    	
    if (fm.Charge.checked)
    {
    	strCalType2 = "substr(CalType,0,1) in ('2','3','4','5')";
    }
    
    if (strCalType1 != "CalType in (" && strCalType2 != "")
    	fm.CalTypeStr.value = "("+strCalType1+" Or "+strCalType2+")";
    else if (strCalType1 != "CalType in (")
        fm.CalTypeStr.value = strCalType1;
    else if (strCalType2 != "")
        fm.CalTypeStr.value = strCalType2;
    //alert(fm.CalTypeStr.value);
}
