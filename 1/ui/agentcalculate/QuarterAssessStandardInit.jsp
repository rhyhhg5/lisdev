<%
//程序名称：QuarterAssessStandardInit.jsp
//程序功能：
//创建日期：2008-03-03
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script language="JavaScript">
  var StrSql=" 1 and branchtype=#3# and branchtype2=#01# and GRADECODE <>#F00# ";

</SCRIPT>
<script language="JavaScript">

function initForm()
{
  try
  {
    initQuarterAssessStandardGrid();
  }
  catch(re)
  {
    alert("QuarterAssessStandardInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var QuarterAssessStandardGrid;
function initQuarterAssessStandardGrid() {                            
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=0;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="主键";         		//列名
    iArray[1][1]="0px";         		//宽度
    
    
    iArray[2]=new Array();
    iArray[2][0]="职级";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=2;         		//最大长度
    iArray[2][4]="assessgrade2";         		//是否允许录入，0--不能，1--允许
    iArray[2][5]="2|3"; ;
    iArray[2][6]="0|1";
    iArray[2][9]="职级代码|code:assessgrade2"; 
    iArray[2][15]= "1";                                                    
    iArray[2][16]= StrSql;  
    
    iArray[3]=new Array();
    iArray[3][0]="职级名称";         		//列名
    iArray[3][1]="100px";         		//宽度
    iArray[3][3]=0;         		//最大长度
     
    
    
   
   
   
   
   
    
    iArray[4]=new Array();
	  iArray[4][0]="险种编码";          		//列名
	  iArray[4][1]="80px";      	      		//列宽
	  iArray[4][2]=20;            			//列最大值
	  iArray[4][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  iArray[4][4]="bankriskcode";              	        //是否引用代码:null||""为不引用
	  iArray[4][5]="2|3";              	                //引用代码对应第几列，'|'为分割符
	  iArray[4][6]="0|1";
//	  iArray[1][9]="险种编码|code:RiskCode&notnull";  
//	  iArray[1][15]= "1";              	        //校验输入是否正确
//	  iArray[1][16]= StrSql;  
    
    
    iArray[5]=new Array();
    iArray[5][0]="险种名称";         		//列名
    iArray[5][1]="100px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=1;         		//是否允许录入，0--不能，1--允许  
    
    
    
    iArray[6]=new Array();
    iArray[6][0]="间接绩效提奖比例（小数 如23%，录入0.23）";         		//列名
    iArray[6][1]="100px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=1;         		//是否允许录入，0--不能，1--允许  
    
    
    
   QuarterAssessStandardGrid = new MulLineEnter( "fm" , "QuarterAssessStandardGrid" ); 
    //这些属性必须在loadMulLine前
 
    //QuarterAssessStandardGrid.mulLineCount = 0;   
    QuarterAssessStandardGrid.displayTitle = 1;
    //QuarterAssessStandardGrid.hiddenPlus = 0;
    //QuarterAssessStandardGrid.hiddenSubtraction = 0;
    //QuarterAssessStandardGrid.canSel = 0;
    QuarterAssessStandardGrid.canChk = 1;
    QuarterAssessStandardGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
