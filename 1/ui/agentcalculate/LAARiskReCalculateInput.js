//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 

	fm.all('divSpanLACommisionGrid').style.display='';
	fm.all('divSpanLACommisionGrid1').style.display='none';
    //首先检验录入框
  if(!verifyInput()) return false;
  var managecom = fm.all('ManageCom').value;
  if('8632'==managecom.substr(0,4))
  {
	  if(""==fm.all('AgentCom').value||null==fm.all('AgentCom').value)
	  {
		  alert("江苏中介机构手续费重算时需要录入中介机构");
		  return false;
	  }
//	  if(""==fm.all('ContType').value||null==fm.all('ContType').value)
//	  {
//		  alert("江苏中介机构手续费重算时需要录入团个标记");
//		  return false;
//	  }
  }
  var tContFlag = fm.all('ContFlag').value;
  var tRiskCode = fm.all('RiskCode').value;
  //var tContType = fm.all('ContType').value;
  var tContNo = fm.all('GrpContNo').value;
  var  sql= "select  a.managecom,(select name from ldcom where comcode=a.ManageCom) aa,a.AgentCom,"
  			+"(select name from lacom where agentcom=a.agentcom) bb,"
  			+"(case when a.grpcontno='00000000000000000000' then a.ContNo else a.grpcontno end) as contno,a.RiskCode,a.TransMoney," 
  			//首期手续费比例，首续期手续费
  			+"coalesce((select FirConChargeRate from lachargefee where commisionsn = a.commisionsn and exists(select 'x' from lacommision where commisionsn = a.commisionsn and payyear = 0 and Renewcount >= 0)),0),coalesce((select FirConCharge from lachargefee where commisionsn = a.commisionsn and exists(select 'x' from lacommision where commisionsn = a.commisionsn and payyear = 0 and Renewcount >= 0) ),0),"
  			//续期手续费手续费比率，续期手续费,销售奖金
  			+"coalesce((select FirConChargeRate from lachargefee where commisionsn = a.commisionsn and  exists(select 'x' from lacommision where commisionsn = a.commisionsn and payyear > 0 and Renewcount = 0 )),0),coalesce((select FirConCharge from lachargefee where commisionsn = a.commisionsn and exists (select 'x' from lacommision where commisionsn  = a.commisionsn and payyear > 0 and renewcount = 0 ) ),0),coalesce((select SaleBonus from lachargefee where commisionsn = a.commisionsn ),0),"
  			//月度奖，年度奖，季度奖
  			+"coalesce((select MonCharge from lachargefee where commisionsn = a.commisionsn ),0),coalesce((select YearCharge from lachargefee where commisionsn = a.commisionsn ),0),coalesce((select ExtraCharge1 from lachargefee where commisionsn = a.commisionsn ),0),"
  			//继续率奖，
  			+"coalesce((select ContCharge from lachargefee where commisionsn = a.commisionsn ),0),"
  		
  		    +"case when a.grpcontno='00000000000000000000' then (select signdate from lccont where contno=a.contno) else (select signdate from lcgrpcont where grpcontno=a.grpcontno) end cc,"
  			+"a.tmakedate,a.receiptno,a.transtype,"
  			+"(select f3 from lacommision where commisionsn=a.commisionsn ) ff  "
  			+",(case (select duefeetype from ljapay where payno = a.receiptno and incomeno = (select contno from lacommision where commisionsn = a.commisionsn)) when '0' then '新单' else '保全' end) gg"
  			+" ,a.commisionsn ee from LACharge a where a.chargetype='15' and a.chargestate='0' "
             + " and a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' " 
             + getWherePart('a.ManageCom', 'ManageCom','like')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('a.AgentCom', 'AgentCom')
           //+ getWherePart('a.GrpContNo', 'GrpContNo')
          	 ;
          	  
          	 if(fm.all('WrapCode').value!=null && fm.all('WrapCode').value!=''){
  				sql+=" and exists  (select '1' from lacommision where f3='"+fm.all('WrapCode').value+"' and commisionsn=a.commisionsn)";
  			}
  			if(fm.all('CardNo').value!=null && fm.all('CardNo').value!=''){
  				sql+=" and a.grpcontno in (select grpcontno from licertify where cardno ='"+fm.all('CardNo').value+"')";
  			}
  			if(tContFlag=='0')
	        {
	      	  sql+="and exists(select 1 from ljapay where payno = a.receiptno and incomeno in (select contno from lacommision where commisionsn = a.commisionsn union select grpcontno from lacommision where commisionsn = a.commisionsn) and duefeetype = '0' )";
	        }else if(tContFlag=='1')
	        {
	          sql+="and not exists(select 1 from ljapay where payno = a.receiptno and incomeno in (select contno from lacommision where commisionsn = a.commisionsn union select grpcontno from lacommision where commisionsn = a.commisionsn) and duefeetype = '0' )";
	        }else{
	       
	        }	
  			if("" != tRiskCode){
  				sql +=" and  a.riskcode ='"+tRiskCode+"' ";
  			}
  			if(""!=tContNo)
  			{
  				sql+=" and ( a.grpcontno ='"+tContNo+"' or a.contno = '"+tContNo+"')";
  			}
             sql+= " order  by ManageCom,AgentCom,tmakedate  "        	 
           ;   
             	
	turnPage.queryModal(sql, LACommisionGrid);   	
	
}
function afterCodeSelect(codeName,Field)
{
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("财务结算起期必须小于等于财务结算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}
function chkMulLine(){
	//alert("enter chkmulline");
	var i;
	var iCount = 0;
	var rowNum = LACommisionGrid.mulLineCount;
	for(i=0;i<rowNum;i++){
		if(LACommisionGrid.getChkNo(i)){
			iCount++;
		} 
	}
	if(iCount == 0){
		alert("请选择要结算的记录!");
		return false
	}
	return true;
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#1# and BranchType2=#02# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}
function selectRePay(){
	if(!chkMulLine()){
		return false;
	}
	fm.all('fmAction').value='SELECTREPAY';
	submitForm();
}
function allRePay(){
	fm.all('fmAction').value='ALLREPAY';
	submitForm();
}




