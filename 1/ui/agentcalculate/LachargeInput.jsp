<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LachargeInput.jsp
//程序功能：
//创建日期：2017-06-15
//创建人  ：zyy程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LachargeInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LachargeInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="LachargeSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		手续费结算
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);" verify="管理机构|notnull&code:comcode"><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>          
           <TD class = title>
             中介机构
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentCom ondblclick="return getAgentComName(this,AgentComName);" onkeyup="return getAgentComName(this,AgentComName);"  ><Input class=codename name=AgentComName readOnly >
          </TD>
        </TR>
        <TR  class = common>
           <TD  class = title>
           财务结算起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="起始年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            财务结算止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="终止年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          
        </TR> 
        <tr class=common>
        <TD  class = title>
           保单号
          </TD>
          <TD  class= input>
     		<Input class= 'common' name=ContNo >
    	  </TD>  	  
    	   <TD  class= title>  
    	团个标记
          </TD>
          <TD  class= input> <Input class= "codeno" name=ContType CodeData="0|^0|团单|^1|个单|" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);" onchange="" ><input class=codename name=ContTypeName  readonly=true ></TD>
  		</tr>  
  	    <TR  class= common>			
		<TD class=title>展业类型</TD>
		<TD class=input><Input class='codeno' name=BranchType
		    verify="展业类型|notnull"
		    ondblclick="return showCodeList('branchtype',[this,BranchTypeName],[0,1]);"
			onkeyup="return showCodeListKey('branchtype',[this,BranchTypeName],[0,1]);"
			readonly><Input class=codename name=BranchTypeName elementtype=nacessary>
	   </TD>
	    <TD class=title>销售渠道</TD>
		<TD class=input><Input class='codeno' name=BranchType2
		    verify="销售渠道|notnull"
		    ondblclick="return showCodeList('branchtype2',[this,BranchType2Name],[0,1]);"
			onkeyup="return showCodeListKey('branchtype2',[this,BranchType2Name],[0,1]);"
			readonly><Input class=codename name=BranchType2Name elementtype=nacessary>
	   </TD>
	   </TR>  
	   <tr class=common> 
  		<TD  class= title>  
    	回访标记
          </TD>
          <TD  class= input> <Input class= "codeno" name=BackSign CodeData="0|^0|全部|^1|成功|^2|不成功|" ondblclick="return showCodeListEx('BackSign',[this,BackSignName],[0,1]);" onkeyup="return showCodeListKeyEx('BackSign',[this,BackSignName],[0,1]);" onchange="" ><input class=codename name=BackSignName  readonly=true ></TD>
  	    </tr>          
      </table>       
         <Table class="common" align=center>
  			<Tr>
    			<Td class=button>
    			
      				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="easyQueryClick();">
      				<INPUT class=cssButton VALUE="选择结算"  TYPE=button onclick="selectPay()"> 
      				<INPUT class=cssButton VALUE="全部结算"  TYPE=button onclick="allPay();">
    			</Td>
  			</Tr>
		</Table>
          <input type="hidden" class=input name=fmAction >
    </Div>
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <Div id= "divSpanLACommisionGrid" align=center style= "display: '' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
	<Div id= "divSpanLACommisionGrid1" align=center style= "display: 'none' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid1">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
    	<Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      </div>
      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>