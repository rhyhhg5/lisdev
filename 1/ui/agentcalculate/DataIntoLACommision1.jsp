<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：DataIntoLACommision.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();
    System.out.println(CurDate);
%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="DataIntoLACommision.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="DataIntoLACommisionInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./DataIntoLACommisionSave1.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		区间提数
       		 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);" verify="管理机构|notnull&code:comcode" 
             ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>
          <TD class = title>
             展业类型
          </TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType 
             ondblclick="return showCodeList('BranchType',[this,BranchTypeName],[0,1]);" verify="展业类型|code:BranchType" 
             ><Input class=codename name=BranchTypeName readOnly elementtype=nacessary>
          </TD>
           <TD class = title>
             渠道类型
          </TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1]);" verify="渠道类型|code" 
             ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
          </TD>
        </TR>
        <TR  class = common>
           <TD  class = title>
            计算起期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="计算起期|DATE&NOTNULL" >
          </TD>
           <TD  class= title>
            计算止期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="计算止期|DATE&NOTNULL" >
          </TD>
        </TR>
        <TR class=input>
        
         
        
       </TR>
      </table>
       <input type =button class=common  name=calculate value="提数计算" onclick="AgentWageCalSave();">
          <input type =button class=common value="查询" onclick="easyQueryClick();">
          <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
    </Div>
    
    <Div  id= "divLACross1" style= "display: ''"> 
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	
    	<Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      </div>
      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>