<%
//程序名称：DataIntoLACommisionInit.jsp
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
	fm.all('WageNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('TMakeDate').value = '';
    fm.all('FetchBussTypeCode').value='';
    fm.all('GrpContNo').value='';
    fm.all('ContNo').value='';
  }
  catch(ex)
  {
    alert("DataIntoLACommisionNewInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
    initLAWageNewLogGrid(); 
  }
  catch(re)
  {
    alert("DataIntoLACommisionNewInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initLAWageNewLogGrid()
{
	var iArray = new Array();
	  try
	  {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";
	    iArray[0][1]="30px";
	    iArray[0][2]=10;
	    iArray[0][3]=0;

	    iArray[1]=new Array();
	    iArray[1][0]="薪资月";
	    iArray[1][1]="30px";
	    iArray[1][2]=60;
	    iArray[1][3]=0;
	    
	    iArray[2]=new Array();
	    iArray[2][0]="提取表名";
	    iArray[2][1]="50px";
	    iArray[2][2]=60;
	    iArray[2][3]=0;


	    iArray[3]=new Array();
	    iArray[3][0]="数据类型";
	    iArray[3][1]="50px";
	    iArray[3][2]=200;
	    iArray[3][3]=0;

	    iArray[4]=new Array();
	    iArray[4][0]="提数说明";
	    iArray[4][1]="150px";
	    iArray[4][2]=200;
	    iArray[4][3]=0;	
	    
	    iArray[5]=new Array();
	    iArray[5][0]="当前实际提到的日期";
	    iArray[5][1]="70px";
	    iArray[5][2]=200;
	    iArray[5][3]=0;	
	    
	    iArray[6]=new Array();
	    iArray[6][0]="当前实际提数状态";
	    iArray[6][1]="60px";
	    iArray[6][2]=200;
	    iArray[6][3]=0;	

	    iArray[7]=new Array();
	    iArray[7][0]="当前应提到的日期";
	    iArray[7][1]="60px";
	    iArray[7][2]=200;
	    iArray[7][3]=0;	
	    
		iArray[8]=new Array();
	    iArray[8][0]="主键（隐藏）";
	    iArray[8][1]="60px";
	    iArray[8][2]=200;
	    iArray[8][3]=3;	
	    
	    LAWageNewLogGrid = new MulLineEnter( "fm" , "LAWageNewLogGrid" );
	    //这些属性必须在loadMulLine前
	    LAWageNewLogGrid.mulLineCount = 1;
	    LAWageNewLogGrid.displayTitle = 1;
	    LAWageNewLogGrid.hiddenSubtraction = 1;
	    LAWageNewLogGrid.hiddenPlus = 1;
	    LAWageNewLogGrid.locked = 1;
	    LAWageNewLogGrid.canChk = 1;
	    LAWageNewLogGrid.loadMulLine(iArray);
	  }
	  catch(ex)
	  {
	    alert(ex);
	  }
}
</script>
