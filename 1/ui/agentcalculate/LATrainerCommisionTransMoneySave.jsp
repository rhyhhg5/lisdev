<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
	//程序名称：LATrainerWageCalculateSave.jsp
	//程序功能：
    //创建日期：2018-06-7
    //创建人  ： WangQingMin
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT src="LATrainerWageCalculateInput.js"></SCRIPT>
<!--用户校验类-->
<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.agentcalculate.*"%>
<%@page import="com.sinosoft.lis.agentwages.*"%>
<%
	// 输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";

	GlobalInput tGI = new GlobalInput(); //repair:
	tGI = (GlobalInput) session.getValue("GI"); //参见loginSubmit.jsp
	if (tGI == null) {
		System.out.println("页面失效,请重新登陆");
		FlagStr = "Fail";
		Content = "页面失效,请重新登陆";
	} else //页面有效
	{
	String ManageCom = request.getParameter("ManageCom");
    String StartDate = request.getParameter("StartDate");
    String EndDate   = request.getParameter("EndDate");
    System.out.println("EndDate:"+EndDate);
    String YearMonth  = AgentPubFun.formatDate(StartDate,"yyyyMM");
    String EndYearMonth = AgentPubFun.formatDate(EndDate,"yyyyMM");
    if(!YearMonth.equals(EndYearMonth)){
  		FlagStr="Fail";
   		Content="计算结束日期和计算开始日期应在同一个年月，不能跨年月计算！";
    }else{
    String strInfo = "";
    System.out.println("YearMonth:"+YearMonth);
    StartDate = AgentPubFun.formatDate(StartDate,"yyyy-MM-dd");
    EndDate = AgentPubFun.formatDate(EndDate,"yyyy-MM-dd");
    System.out.println("StartDate2:"+StartDate);
    System.out.println("EndDate2:"+EndDate);
    if(StartDate.compareTo(EndDate)>0)
   {		
    
   		FlagStr="Fail";
   		Content="计算止期应大于计算起期";
   }		
   else
   {				
	         LATrainerCommisionTransMoneyBL tLATrainerCommisionTransMoneyBL=new LATrainerCommisionTransMoneyBL();
              VData tVData=new VData();
              tVData.add(0,ManageCom);
              tVData.add(1,StartDate);
              tVData.add(2,EndDate);        
              tVData.add(3,YearMonth);
              if (!tLATrainerCommisionTransMoneyBL.dealData(tVData))
              {
              
              	 	FlagStr="Fail";
              		Content=tLATrainerCommisionTransMoneyBL.mErrors.getFirstError();
              }
              else
             {
            	  Content="保存成功";
              }
   }
    }
		System.out.println(Content);
	}//页面有效区
%>
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

