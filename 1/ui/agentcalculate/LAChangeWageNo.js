//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;

/*******************************************************************************
 * 注: 使得从该窗口弹出的窗口能够聚焦
 * @  add 2012-8-14
 *******************************************************************************/ 
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
/*******************************************************************************
 * 注: 提交，保存按钮对应操作
 * @  add 2012-8-14
 *******************************************************************************/ 
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

/*******************************************************************************
 * 注: 提交后操作,服务器数据返回后执行的操作
 * @  add 2012-8-14
 *******************************************************************************/   
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}
/*******************************************************************************
 * 注: 提前校验
 * @  add 2012-8-14
 *******************************************************************************/   
function beforeSubmit()
{
  var twageno1 = trim(fm.all('WageNo1').value);
  var twageno = trim(fm.all('WageNo').value);
  if(twageno1==''||twageno1==null)
   {
      alert("调整至薪资月录入为空,烦请重新进行录入操作！");
      return false;
   }
   if(twageno1==twageno)
   {
      alert("调整至薪资月与原保单所属薪资月相同,无需进行修改操作！");
      return false;
   }
   return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
         
/*******************************************************************************
 * 注: Click事件，当点击“修改”图片时触发该函数
 * @  add 2012-8-14
 *******************************************************************************/   
function updateClick()
{
  if (verifyInput() == false) return false;	
  if(!beforeSubmit())
  {
    return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}                    
/*******************************************************************************
 * 注: 显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 * @  add 2012-8-14
 *******************************************************************************/       

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*******************************************************************************
 * 注: 查询操作 
 * @  add 2012-8-14
 *******************************************************************************/
function easyQueryClick() {
  if (verifyInput() == false) return false;	// 校验页面必录项
  initArchieveGrid();
	//此处书写SQL语句			     			    
  var strSql = "select a.commisionsn,a.managecom,a.wageno,"
  +"a.agentcode, (select name from laagent where agentcode=a.agentcode),"
  +"a.riskcode,a.transmoney,a.fyc,a.signdate,a.customgetpoldate,a.getpoldate  "
  +" from Lacommision  a  "
  +" where 1=1 and a.branchtype='1'  and a.grpcontno = '00000000000000000000' and renewcount >= 0 and payyear = 0"
  + getWherePart('a.ContNo', 'ContNo')
  +" order by wageno " ;
 
  var arrResult = new Array();
  arrResult = easyExecSql(strSql);
  
  if (!arrResult) 
  {
    alert("保单不存在或者查询错误！");
    return false;
  }
  displayMultiline(arrResult,ArchieveGrid);
}


         
