<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAPlanQueryInput.jsp
//程序功能：
//创建日期：2003-07-08 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String BranchType = "";
	String BranchType2 = "";
	try
	{
		BranchType = request.getParameter("BranchType");
		BranchType2=request.getParameter("BranchType2");
	}
	catch( Exception e )
	{
		BranchType = "";
		BranchType2="";
	}
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAPlanQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAPlanQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
	<%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>团代计划任务信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./LAPlanQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPlan1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAPlan1" style= "display: ''">
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  计划代码 
		</td>
        <td  class= input> 
		  <input class= common name=PlanCode > 
		</td>
        <td  class= title> 
		  计划类型 
		</td>
        <td  class= input> 
		 <input class='code' name=PlanType verify="计划类型|notnull&code:PlanType"
		         <input class='code' name=PlanType verify="计划类型|notnull&code:PlanType"
		         CodeData="0|^1|个人计划任务 ^2|营业部计划任务 ^3|支公司计划任务 ^4|分公司计划任务" 
		         ondblclick="showCodeListEx('PlanType',[this],[0]);" 
		         onkeyup="showCodeListKeyEx('PlanType',[this],[0]);"> 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  计划对象 
		</td>
        <td  class= input> 
		  <input class=common name=PlanObject > 
		</td>
		<td  class= title> 
		  对象名称 
		</td>
        <td  class= input> 
		  <input class=common name=ObjectName onchange="return checkValid();"> 
		</td>
		</tr>
		<tr  class= common> 
        <td  class= title> 
		  计划区间单位 
		</td>
        <td  class= input> 
		  <input class="code" name=PlanPeriodUnit verify = "计划时间单位|code:PlanPeriodUnit" 
		         ondblclick="return showCodeList('PlanPeriodUnit',[this]);" 
                         onkeyup="return showCodeListKey('PlanPeriodUnit',[this]);">
		</td>
     <td  class= title> 
		  计划标准 
		</td>
        <td  class= input> 
		  <input class=common name=PlanValue > 
		</td>
		</tr>
		<tr  class= common> 
        <td  class= title> 
		  计划起期
		</td>
        <td  class= input> 
		   <Input class= "coolDatePicker" dateFormat="short" name=PlanStartDate verify="计划起期|NOTNULL "> 
		</td>
       
        <td  class= title> 
		  计划止期
		</td>
        <td  class= input> 
		  <Input class= "coolDatePicker" dateFormat="short" name=PlanEndDate verify="计划止期|NOTNULL "> 
		</td>
		</tr>
		<tr  class= common> 
		<td  class= title> 
		  计划类别
		</td>
        <td  class= input> 
			  <input name=PlanCond3 class='code'  
		         ondblclick="return showCodeList('plankindpicc',[this]);" 
                         onkeyup="return showCodeListKey('plankindpicc',[this]);" >  
		</td>	
        <td  class= title> 
		  计划录入日期
		</td>
        <td  class= input> 
		  <input name=MakeDate class='coolDatePicker' dateFormat='short' > 
		</td>
      </tr>
      <tr>
      <td  class= title> 
		  计划险种类别
		</td>
        <td  class= input> 
			  <input name=PlanCond1 class='codeno'  
		         ondblclick="return showCodeList('larisktype',[this,PlanCond1Name],[0,1]);" 
                         onkeyup="return showCodeListKey('larisktype',[this,PlanCond1Name],[0,1]);" 
               ><input name=PlanCond1Name  class=codename >  
      </Tr>
      </table>
          <input type=hidden name=BranchType value=''>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();"  class="cssButton"> 
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();"  class="cssButton"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPlanGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPlanGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPlanGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"  class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"  class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"  class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"  class="cssButton"> 				
  	</div>
  	  <input name=BranchType type=hidden value = ''>
  	  <input name=BranchType2 type=hidden value = ''>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
