<%
//程序名称：LAPlanQueryInit.js
//程序功能：
//创建日期：2003-07-08 14:14:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('PlanCode').value = '';
    fm.all('PlanType').value = '';
    fm.all('PlanObject').value = '';
    fm.all('ObjectName').value='';
    fm.all('PlanPeriodUnit').value = '';
    fm.all('PlanStartDate').value = '';
    fm.all('PlanEndDate').value = '';
    fm.all('PlanValue').value = '';
    fm.all('MakeDate').value = '';
    fm.all('PlanCond3').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';
    //alert(fm.all('BranchType').value);
  }
  catch(ex)
  {
    alert("在LAPlanQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAPlanQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initPlanGrid();
  }
  catch(re)
  {
    alert("LAPlanQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化PlanGrid
 ************************************************************
 */
function initPlanGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="计划代码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="计划类型";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="计划对象";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

      //  iArray[4]=new Array();
      //  iArray[4][0]="对象名称";         //列名
      //  iArray[4][1]="100px";         //宽度
      //  iArray[4][2]=100;         //最大长度
      //  iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="计划区间单位";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="计划起期";         //列名
        iArray[5][1]="80px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="计划止期";         //列名
        iArray[6][1]="80px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[7]=new Array();
        iArray[7][0]="计划标准";         //列名
        iArray[7][1]="100px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
         iArray[8]=new Array();
        iArray[8][0]="计划类别";         //列名
        iArray[8][1]="100px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

        
        iArray[9]=new Array();
        iArray[9][0]="计划任务录入日期";         //列名
        iArray[9][1]="110px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[10]=new Array();
        iArray[10][0]="计划险种类别";         //列名
        iArray[10][1]="110px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;        
        
        iArray[11]=new Array();
        iArray[11][0]="计划标准保费";         //列名
        iArray[11][1]="110px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;        
  
        PlanGrid = new MulLineEnter( "fm" , "PlanGrid" ); 

        //这些属性必须在loadMulLine前
        PlanGrid.mulLineCount = 0;   
        PlanGrid.displayTitle = 1;
        PlanGrid.locked=1;
        PlanGrid.canSel=1;
        PlanGrid.canChk=0;
        PlanGrid.hiddenPlus = 1;
      	PlanGrid.hiddenSubtraction = 1;
        PlanGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化PlanGrid时出错："+ ex);
      }
    }


</script>