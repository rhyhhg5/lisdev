//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}
        
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
     
     if(!verifyInput()) return false;
     
	 var  sql="select managecom ,(select name from ldcom where comcode = lacharge.managecom),agentcom,(select name from lacom where agentcom = lacharge.agentcom),contno,riskcode,transmoney,chargerate,charge,tmakedate,"
	          +"(case when chargestate = '1' then '已结算' else '未结算' end),commisionsn,chargestate "
	         + " from lacharge "
	         + " where branchtype = '2' and branchtype2 = '02' and chargestate = '1' and charge > 0 "
	         + " AND NOT EXISTS (SELECT '1' FROM LAWAGEACTIVITYLOG where WageLogType='CR'  AND COMMISIONSN=LACHARGE.COMMISIONSN )"
	         + getWherePart('ManageCom', 'ManageCom')
	         //+ getWherePart('AgentCom', 'AgentCom')
	         + getWherePart('GrpContNo', 'GrpContNo')
	         + getWherePart('TMakeDate', 'TMakeDate')
	         //+ getWherePart('RiskCode', 'RiskCode')
	         ;
	         //if(fm.all('WrapCode').value!=null && fm.all('WrapCode').value!=''){
  			//	sql+=" and exists  (select '1' from lacommision where f3='"+fm.all('WrapCode').value+"' and commisionsn=a.commisionsn)";
  			//}
	         
          	 turnPage.queryModal(sql, LAChargeGrid);   	
	
}



function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#2# and BranchType2=#02# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}



function chkMulLine(){
	var i;
	var iCount = 0;
	var rowNum = LAChargeGrid.mulLineCount;
	for(i=0;i<rowNum;i++){
		if(LAChargeGrid.getChkNo(i)){
			iCount++;
		} 
	}
	if(iCount == 0){
		alert("请选择要反冲的手续费记录!");
		return false
	}
	return true;
}
function selectReturn(){
	if(!chkMulLine()){
		return false;
	}
	fm.all('fmAction').value='SelectReturn';
	submitForm();
}
function allReturn(){
	fm.all('fmAction').value='AllReturn';
	submitForm();

}
function beforeSubmit()
{
    var tSQL = "select 'Y' from ljaget where confdate is not null and otherno in (select receiptno from lacharge a where a.receiptno = ljaget.otherno"
           	 + getWherePart('a.ManageCom', 'ManageCom')
	         + getWherePart('a.TMakeDate', 'TMakeDate')
	         + getWherePart('a.GrpContNo', 'GrpContNo');
  			 tSQL+=" )" ;
  	var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
  	if(strQueryResult)
  	{
  	    alert("该笔保单已做了付费确认操作,不能进行手续费反冲操作！");
  	    return false;
  	}
         return true;
}
