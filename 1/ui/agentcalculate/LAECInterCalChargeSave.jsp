<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ActiveChargLACommisionSave.jsp.jsp
//程序功能：
//创建日期：2015-04-17
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="java.text.SimpleDateFormat"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>
<%
  // 输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String currentDate = PubFun.getCurrentDate();
  String currentTime = PubFun.getCurrentTime();
  currentDate = AgentPubFun.formatDate(currentDate,"yyyy-MM-dd");
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  //tGI.ManageCom="8611";
  //tGI.Operator="Admin";
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  {
  
    String ManageCom = request.getParameter("ManageCom");
    String StartDate = request.getParameter("StartDate");
    String EndDate   = request.getParameter("EndDate");
    String tBranchType = request.getParameter("BranchType");
    String tBranchType2 = request.getParameter("BranchType2");
    System.out.println("BranchType:"+tBranchType);
    System.out.println("BranchType2:"+tBranchType2);
    System.out.println("EndDate:"+EndDate);
    System.out.println("EndDate:"+currentDate);
    String strInfo = "";

    StartDate = AgentPubFun.formatDate(StartDate,"yyyy-MM-dd");
    EndDate = AgentPubFun.formatDate(EndDate,"yyyy-MM-dd");
    if(currentDate.compareTo(EndDate)<=0)
    {
        
   		FlagStr="Fail";
   		Content="计算止期应小于今天";
   		
    }
    else 
    {
   	    String currDay=StartDate;
	    if(StartDate.compareTo(EndDate)>0)
	    {		
	    
	   		FlagStr="Fail";
	   		Content="计算止期应大于计算起期";
	    }		
	    else
	    {	
	    	////////////////////////
		        while (currDay.compareTo(EndDate)<=0)
               {
			   //佣金计算日志表
			   LAChargeLogSchema tLAChargeLogSchema = new LAChargeLogSchema();
			   tLAChargeLogSchema.setManageCom(ManageCom);
			   tLAChargeLogSchema.setStartDate(currDay);
			   tLAChargeLogSchema.setEndDate(currDay);	 
			   tLAChargeLogSchema.setBranchType(tBranchType);       
			   tLAChargeLogSchema.setBranchType2(tBranchType2); 
			   String Year  = AgentPubFun.formatDate(currDay,"yyyy");
			   String Month = AgentPubFun.formatDate(currDay,"MM");    
			   String YearMonth = Year+Month;
			   tLAChargeLogSchema.setChargeMonth(Month);
			   tLAChargeLogSchema.setChargeCalNo(YearMonth);
			   tLAChargeLogSchema.setChargeYear(Year);
			   VData tVData=new VData();
			   tVData.addElement(tGI);	   
			   tVData.addElement(tLAChargeLogSchema);
					  	
	  	  AECInterCalChargeCalUI tAECInterCalChargeCalUI=new AECInterCalChargeCalUI();
	  	  if (!tAECInterCalChargeCalUI.submitData(tVData,""))
	  	  {
	  	    FlagStr="Fail";
	  	    Content=tAECInterCalChargeCalUI.mErrors.getFirstError();	  	     
	  	  }
	  	  else
	  	 	{
	  	 	   FlagStr="Succ";
	  	 	   Content="保存成功";
	  	 	}
	  	 currDay=PubFun.calDate(currDay,1,"D",null);
         }    
		/////////////////
	    }
    }
         System.out.println(Content);
 }//页面有效区

%>
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>