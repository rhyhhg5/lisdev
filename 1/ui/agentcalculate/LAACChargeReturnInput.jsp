<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();    
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    String strSql= " 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'"; 
%>
<script>
	 var  mBranchType =  <%=BranchType%>;
	 //var  mBranchType2 =  <%=BranchType2%>;
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LAACChargeReturnInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LAACChargeReturnInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="LAACChargeReturnSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		手续费回退
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);" verify="管理机构|notnull&code:comcode"><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>  
           <TD  class = title>
           团单号
          </TD>
          <TD  class= input>
     		<Input class= 'common' name=GrpContNo verify="团单号|notnull"  elementtype=nacessary>
    	  </TD>  	          
         
        </TR>
         <TR  class = common>
           <TD  class = title>
           财务结算日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=TMakeDate verify="财务结算日期|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
        </TR>    
        <!-- tr class=common>
         <TD class = title>
             中介机构
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentCom ondblclick="return getAgentComName(this,AgentComName);" onkeyup="return getAgentComName(this,AgentComName);"  ><Input class=codename name=AgentComName readOnly >
          </TD>
    	 
          <TD  class= title>
              险种编码
          </TD>
          <TD  class= input>
            <Input class= 'codeno' name= RiskCode 
             ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);"
             onkeyup="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);"
            ><Input class=codename name=RiskCodeName readOnly  >
          </TD>			
       </TR> 
       <tr class=common>
          <TD  class= title>
           套餐编码
          </TD>
          <TD  class= input>
            <Input class= 'codeno' name= WrapCode 
              ondblclick="return showCodeList('wrapcode',[this,WrapCodeName],[0,1]);"
              onkeyup="return showCodeList('wrapcode',[this,WrapCodeName],[0,1]);"
            ><Input class=codename name=WrapCodeName readOnly  >
          </TD>
          </tr -->
          
      </table> 
      
         <Table class="common" align=center>
  			<Tr>
    			<Td class=button>
      				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="easyQueryClick()">
      				<INPUT class=cssButton VALUE="选择回退"  TYPE=button onclick="selectReturn()">
      				<INPUT class=cssButton VALUE="全部回退"  TYPE=button onclick="allReturn()">
      				
    			</Td>
  			</Tr>
		</Table>
          <input type="hidden" class=input name=fmAction >
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">
          <input type="hidden" class=input name=BranchType2 value="<%=BranchType2%>">           
    </Div>
     <p> <font color="#ff0000">注：回退已经结算的手续处理流程较复杂，涉及到财务等相关模块，请慎重选择，尽量减少回退结算手续费操作！！保单只能进行一次回退！</font></p> 
     <p> <font color="#ff0000">注：能够进行回退的手续费数据必须是已经结算且手续费不为0的数据</font></p> 
     <p> <font color="#ff0000">注：回退后的数据，系统将自动将反冲后手续费置为0，请通过“重算”功能进行手续费的重新计算，再次结算时请确认无误后再进行结算！！！</font></p>      
     <p> <font color="#ff0000">注：回退“保存成功”后，务必不要再次进行回退操作，避免两次或者多次冲负！！！</font></p> 
     <p> <font color="#ff0000">注：页面查询无数据，可能是已经进行了回退操作的保单，请核查！！！</font></p> 
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 需回退的手续费信息
  		</td>
  	</tr>
  </table>
  <div id= "divspanLAChargeGrid" align=center style= "display: ''">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLAChargeGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
    	<Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      </div> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>