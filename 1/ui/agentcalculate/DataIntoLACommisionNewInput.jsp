<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：DataIntoLACommisionNewInput.jsp
//程序功能：
//创建日期：2017-10-11
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();
    System.out.println(CurDate);
%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="DataIntoLACommisionNewInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="DataIntoLACommisionNewInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./DataIntoLACommisionNewSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		批处理中断日志表状态修改
       		 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
           <TD  class = title>
         	薪资月
          </TD>
          <TD >
           <Input class= common name=WageNo elementtype=nacessary >
           </TD>
          </TR>
      </table>
         <input type =button class=cssButton value="状态查询" onclick="easyQueryClick();"> 
         <input type =button class=cssButton name="calculate" value="状态修改" onclick="updateState();">
    	<br><br>
    	<font color="red">注：根据薪资月，把00修改状态为11，且enddate修改为enddate-1天
    	</font>
    </Div>
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			查询结果
  		</td>
  	</tr>
  </table>
   	<Div id= "divPage111" align=center style= "display: '' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLAWageNewLogGrid">
  				</span> 
		    </td>
			</tr>
		</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      </div>
      <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		漏提数据补提数据（不用维护日志表，不删除数据，只补提）
       		 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class=title>
                          提取业务类型
          </TD>
          <TD class=input>
            <Input class="codeno" name=FetchBussTypeCode 
             ondblclick="return showCodeList('FetchBussType',[this,FetchBussTypeName],[0,1]);" verify="提取业务类型|notnull&code:FetchBussType" 
             ><Input class=codename name=FetchBussTypeName readOnly elementtype=nacessary>
          </TD>
           <TD  class = title>
         	补提日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=TMakeDate verify="补提日期|DATE&NOTNULL" >
          </TD>
          </TR>
        <TR class=input>
           <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);" verify="管理机构|code:comcode" 
             ><Input class=codename name=ManageComName readOnly >
          </TD>
         <TD class=title>
                 团单号
         </TD>
         <TD>
           <Input class= common name=GrpContNo >
         </TD>
        
       </TR>
       <TR class=input>
           <TD class = title>
                 个单号
         </TD>
         <TD>
           <Input class= common name=ContNo >
         </TD>
         </TR>
      </table>
         <input type =button class=cssButton name="calculate" value="单独补提数据" onclick="AgentWageCalOneData();">
         <input type =button class=cssButton name="calculate" value="全部补提数据" onclick="AgentWageCalAllData();">
   <br><br>
    	<font color="red">注：此功能只能用于日志表状态为11时的数据补提，如果为00，请先进行修改状态 <br>
    	一、补提后，会进行日志表更新。
    	二、【单独补提数据】时，提取业务类型和补提日期必须录入<br>
    	三、【全部补提数据】时，补提日期必须录入，且只根据补提日期来进行补提数据。
    	</font>
    </Div>
    <Input type=hidden name=operator>
    <Input type=hidden name=configwhereSql>
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>