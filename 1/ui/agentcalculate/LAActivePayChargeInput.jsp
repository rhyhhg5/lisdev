<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAActivePayChargeInput.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();    
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    String strSql= " 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'"; 
%>
<script>
	 var  mBranchType =  <%=BranchType%>;
	 var  mBranchType2 =  '<%=BranchType2%>';
	 var  MngSQL=" 1 and length(trim(comcode)) in (#8#) and comcode not in (select code from ldcode where codetype =#jiaochaMng#)";
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LAActivePayChargeInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LAActivePayChargeInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="LAActivePayChargeSave.jsp" method=post name=fm target="fraSubmit">

  

    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		互动渠道手续费结算
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom 
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,MngSQL,1);" verify="管理机构|notnull&code:comcode"
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,MngSQL,1);"><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>          
           <TD class = title>
             中介机构
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentCom ondblclick="return getAgentComName(this,AgentComName);" onkeyup="return getAgentComName(this,AgentComName);"  ><Input class=codename name=AgentComName readOnly >
          </TD>
        </TR>
        <TR  class = common>
           <TD  class = title>
           财务结算起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="起始年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            财务结算止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="终止年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          
        </TR> 
        <tr class=common>
        <TD  class = title>
           是否交叉销售
          </TD>
          <TD  class= input>
             <Input class="codeno" name=CrsType  CodeData="0|^0|全部|^Y|交叉销售|^N|非交叉销售" 
             ondblclick="return showCodeListEx('CrsType',[this,CrsTypeName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('CrsType',[this,CrsTypeName],[0,1]);" onchange=""
             ><input class=codename name=CrsTypeName  readonly=true ></TD>  
    	   <TD  class= title>  
    	团个标记
          </TD>
          <TD  class= input> <Input class= "codeno" name=ContType CodeData="0|^0|团单|^1|个单|" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);" onchange="" ><input class=codename name=ContTypeName  readonly=true ></TD>
  		</tr>
  		<tr class=common>
    	   <TD  class= title>  
    	保单号
          </TD>
          <TD  class= input> <Input class= "common" name=ContNo  ></TD>
  		<td class = title >手续费类型</td>
  		<td class= input><input class = "codeno" name = FeeType CodeData="0|^0|业务手续费|^1|管理手续费|" ondblclick="return showCodeListEx('FeeType',[this,FeeName],[0,1]);" verify="手续费类型|notnull"  onkeyup="return showCodeListKeyEx('FeeType',[this,FeeName],[0,1]);" onchange=""><input class=codename name=FeeName readonly = true  elementtype=nacessary></td>
  		</tr> 
  		<tr class=common>
					<TD class=title>是否过犹豫期</TD>
         <td class= input><input class = "codeno" name = hesitant CodeData="0|^0|是|^1|否|" ondblclick="return showCodeListEx('hesitant',[this,hesitantName],[0,1]);"   onkeyup="return showCodeListKeyEx('hesitant',[this,hesitantName],[0,1]);" readonly = true><input class=codename name=hesitantName readonly = true  ></td>
									</tr>        
      </table>       
          
         <Table class="common" align=center>
  			<Tr>
    			<Td class=button>
    			<!--INPUT class=cssButton VALUE="犹豫期撤保数据处理"  TYPE=button onclick="dealWT();"-->
      				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="easyQueryClick();">
      				<INPUT class=cssButton VALUE="选择结算"  TYPE=button onclick="selectPay()"> 
      				<INPUT class=cssButton VALUE="全部结算"  TYPE=button onclick="allPay();">
    			</Td>
  			</Tr>
		</Table>
         <!-- 
         <input type =button class=common value="银保保费明细查询" onclick="easyQueryClick1();">
         <input type =button class=cssButton value="查询" onclick="easyQueryClick();">
          --> 
          <input type="hidden" class=input name=fmAction >
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">
          <input type="hidden" class=input name=BranchType2 value="<%=BranchType2%>">           
    </Div>
    <!--p><font color="#ff0000">注：请在结算前进行“犹豫期撤保数据处理”操作。</p-->
    <p> <font size="5" color="#ff0000">注:手续费结算分为业务手续费结算和管理手续费结算,请在结算前慎重选择手续费类型,仔细核对手续费金额无误后,再进行手续费结算.</font></p>          
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <Div id= "divSpanLACommisionGrid" align=center style= "display: '' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
	<Div id= "divSpanLACommisionGrid1" align=center style= "display: 'none' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid1">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
    	<Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      </div>
      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>