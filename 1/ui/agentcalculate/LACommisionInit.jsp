<%
//程序名称：commisionQueryInit.jsp
//程序功能：
//创建日期：2003-02-16 15:12:44
//创建人  ：程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  { 
   fm.all('WageNo').value = '';
   fm.all('AgentCode').value = '';
   fm.all('AgentGroup').value = ''; 
   fm.all('Operator').value = '';                                
  //$initfield$
  }
  catch(ex)
  {
    alert("在LACommisionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LACommisionInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initLACommisionGrid();
  }
  catch(re)
  {
    alert("LACommisionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }

}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化commisionGrid
 ************************************************************
 */
function initLACommisionGrid()
  {                               
    var iArray = new Array();
   
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="代理人编码";         //列名
        iArray[1][1]="100px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=1;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="代理人组别";         //列名
        iArray[2][1]="130px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="佣金计算年月代码";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=1;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="佣金计算日期";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=1;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="生成扎账信息日期";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=1;         //是否允许录入，0--不能，1--允许
		
        iArray[6]=new Array();
        iArray[6][0]="直接佣金";         //列名
        iArray[6][1]="100px";         //列名
        iArray[6][2]=100;         //列名
        iArray[6][3]=1;         //列名

        iArray[7]=new Array();
        iArray[7][0]="系列号";         //列名
        iArray[7][1]="100px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[8]=new Array();
        iArray[8][0]="保单号码";         //列名
        iArray[8][1]="180px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=1;         //是否允许录入，0--不能，1--允许
	
	iArray[9]=new Array();
        iArray[9][0]="主险保单号码";         //列名
        iArray[9][1]="180px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=1;         //是否允许录入，0--不能，1--允许
				
	iArray[10]=new Array();
        iArray[10][0]="保单生效日期";         //列名
        iArray[10][1]="100px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[11]=new Array();
        iArray[11][0]="保单送达日期";         //列名
        iArray[11][1]="100px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[12]=new Array();
        iArray[12][0]="保单签单日期";         //列名
        iArray[12][1]="100px";         //宽度
        iArray[12][2]=100;         //最大长度
        iArray[12][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[13]=new Array();
        iArray[13][0]="交易号";         //列名
        iArray[13][1]="180px";         //宽度
        iArray[13][2]=100;         //最大长度
        iArray[13][3]=1;         //是否允许录入，0--不能，1--允许

        iArray[14]=new Array();
        iArray[14][0]="交易金额";         //列名
        iArray[14][1]="100px";         //宽度
        iArray[14][2]=100;         //最大长度
        iArray[14][3]=1;         //是否允许录入，0--不能，1--允许
			
        iArray[15]=new Array();
        iArray[15][0]="原交至日期";         //列名
        iArray[15][1]="100px";         //宽度
        iArray[15][2]=100;         //最大长度
        iArray[15][3]=1;         //是否允许录入，0--不能，1--允许

        iArray[16]=new Array();
        iArray[16][0]="现交至日期";         //列名
        iArray[16][1]="100px";         //宽度
        iArray[16][2]=100;         //最大长度
        iArray[16][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[17]=new Array();
        iArray[17][0]="交费年度";         //列名
        iArray[17][1]="80px";         //宽度
        iArray[17][2]=100;         //最大长度
        iArray[17][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[18]=new Array();
        iArray[18][0]="佣金计算特征";         //列名
        iArray[18][1]="100px";         //宽度
        iArray[18][2]=100;         //最大长度
        iArray[18][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[19]=new Array();
        iArray[19][0]="险种编码";         //列名
        iArray[19][1]="100px";         //宽度
        iArray[19][2]=100;         //最大长度
        iArray[19][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[20]=new Array();
        iArray[20][0]="交费间隔";         //列名
        iArray[20][1]="100px";         //宽度
        iArray[20][2]=100;         //最大长度
        iArray[20][3]= 0;         //是否允许录入，0--不能，1--允许
        
        
        iArray[21]=new Array();
        iArray[21][0]="代理人实际组别";         //列名
        iArray[21][1]="0px";         //宽度
        iArray[21][2]=100;         //最大长度
        iArray[21][3]=3;         //是否允许录入，0--不能，1--允许
        
        iArray[22]=new Array();
        iArray[22][0]="代理机构编码";         //列名
        iArray[22][1]="0px";         //宽度
        iArray[22][2]=100;         //最大长度
        iArray[22][3]=3;         //是否允许录入，0--不能，1--允许
        
        iArray[23]=new Array();
        iArray[23][0]="代理机构内部编码";         //列名
        iArray[23][1]="0px";         //宽度
        iArray[23][2]=100;         //最大长度
        iArray[23][3]=3;         //是否允许录入，0--不能，1--允许
       
	LACommisionGrid = new MulLineEnter( "fm" , "LACommisionGrid" ); 
        
        //这些属性必须在loadMulLine前
        LACommisionGrid.mulLineCount = 10;  
         
        LACommisionGrid.displayTitle = 1;
        //LACommisionGrid.locked=1;
        LACommisionGrid.canSel=0;
        LACommisionGrid.canChk=0;
              LACommisionGrid.hiddenPlus=1;
      LACommisionGrid.hiddenSubtraction=1;
        LACommisionGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化LACommisionGrid时出错："+ ex);
      }
    }


</script>