//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
	if(!checkValue()) return false;
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  easyQueryClick();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在OperatorIndexMarkInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
    //首先检验录入框
  if(!verifyInput()) return false;   
  var  sql="select idx,code1,code2,code3 from LADiscount where discounttype='03' "
             + getWherePart('ManageCom', 'ManageCom');    
	turnPage.queryModal(sql, OperatorIndexMarkGrid);   	
	
}

function afterCodeSelect(codeName,Field)
{
	
}

function beforeSubmit()
{
   return true;
}

function checkImportNumber(control){
	var reg= /^[0-9]+.?[0-9]*$/;
	if(!reg.test(control)){
			return false;
	}
	return true;
}

function checkValue(){
	if(OperatorIndexMarkGrid.mulLineCount==0){
		alert('标准保费信息不能为空');
		return false;
	}
	//var rowNum = OperatorIndexMarkGrid.mulLineCount;
	//alert(rowNum);
	var countNum=0;
	var selFlag=true;
	//for (int i=0; i<rowNum; i++){
		//if(OperatorIndexMarkGrid.getChkNo(i)){
			//icount++;
		//}
	//}
	//alert(icount);
	//if(icount<=0){
		//alert("请选择进行操作的记录。");
		//return false;
	//} 
	for(var i=0;i<OperatorIndexMarkGrid.mulLineCount;i++){
		if(OperatorIndexMarkGrid.getChkNo(i)){
			countNum++;
			if(OperatorIndexMarkGrid.getRowColData(i,2).trim()==''){
				alert('达成率最小值不能为空');
				return false;
			}
			if(OperatorIndexMarkGrid.getRowColData(i,3).trim()==''){
				alert('达成率最大值不能为空');
				return false;
			}
			if(OperatorIndexMarkGrid.getRowColData(i,4).trim()==''){
				alert('对应分数不能为空');
				return false;
			}
			if(!checkImportNumber(OperatorIndexMarkGrid.getRowColData(i,2))){
				alert('达成率最小值请输入数字');
				return false;
			}
			if(!checkImportNumber(OperatorIndexMarkGrid.getRowColData(i,3))){
				alert('达成率最大值请输入数字');
				return false;
			}
			if(!checkImportNumber(OperatorIndexMarkGrid.getRowColData(i,4))){
				alert('对应分数请输入数字');
				return false;
			}
			
			if((OperatorIndexMarkGrid.getRowColData(i,1) == null)||(OperatorIndexMarkGrid.getRowColData(i,1)==""))
	    							{
	    								if((fm.all("fmtransact").value =="UPDATE") )
	    								{
	    	   							 alert("必须先查询出记录，再修改！");
	    	   							 OperatorIndexMarkGrid.checkBoxAllNot();
	    	    						 OperatorIndexMarkGrid.setFocus(i,2,OperatorIndexMarkGrid);
	    	    						 selFlag = false;
	    	   							 break;
	    								}
	    							}
	    							else if ((OperatorIndexMarkGrid.getRowColData(i,1) != null)||(OperatorIndexMarkGrid.getRowColData(i,1)!=""))
   	                {	//如果Serialno不为空不能插入
   	    	            if(fm.all("fmtransact").value =="INSERT")
   	    	            {
   	    	               alert("此纪录已存在，不可插入！");
   	    	               OperatorIndexMarkGrid.checkBoxAllNot();
   	    	               OperatorIndexMarkGrid.setFocus(i,2,OperatorIndexMarkGrid);
   	    	               selFlag = false;
   	    	                break;
   	    	             }
   	                }
   	                
		}
		
		
	}
	if(!selFlag){
   	  return selFlag;
   	}
	if(countNum==0){
		alert("请选择进行操作的记录。");
		return false;
	}
	if(fm.all("fmtransact").value=="UPDATE"){
		for(var i=0;i<OperatorIndexMarkGrid.mulLineCount;i++){
			if(OperatorIndexMarkGrid.getRowColData(0,1).trim()==''){
				alert('列表中存在系统没有的记录，不能修改');
				return false;
			}
		}
	} 
	return true 
}
function save(){
	fm.all("fmtransact").value="INSERT";
	submitForm();
}
function update(){
	fm.all("fmtransact").value="UPDATE";
	submitForm();
}