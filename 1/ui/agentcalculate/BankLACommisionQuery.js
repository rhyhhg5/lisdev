//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  //if(!beforeSubmit()) return false; 
  
  var i = 0;
  var showStr="正在打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.target = "f1print";
	fm.all('op').value = 'PRINT';
	fm.submit();
	showInfo.close();

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
	//查询统一工号： 2014-11-27  解青青
	if(fm.SiteManagerGroupCode.value!=null&&fm.SiteManagerGroupCode.value!=""){
	var sql=" select agentcode  from laagent   where  1=1"       
         + getWherePart("GroupAgentCode","SiteManagerGroupCode")	;
         + getWherePart("ManageCom","ManageCom",'like')	;
   var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
   if (!strQueryResult) 
    {
     alert("此管理机构没有此销售员！");  
     fm.SiteManagerGroupCode.value="";
     return;
    }
     var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('SiteManagerCode').value  =tArr[0][0];
//    alert(fm.all('SiteManagerCode').value);
    
	}
    //首先检验录入框
  if(!verifyInput()) return false;   
  var strCon = " and tmakedate >= (select startdate from  lastatsegment  WHERE STATTYPE='1' AND YEARMONTH =int("+ fm.WageNo.value+") )"
               + " and tmakedate <= (select enddate from  lastatsegment  WHERE STATTYPE='1' AND YEARMONTH =int("+ fm.WageNo.value+") )" ;
  var  sql="select ManageCom,AgentCom,getUniteCode(AgentCode),(select Name from laagent where laagent.agentcode =lacommision.agentcode ),GrpContNo,AppntNo,RiskCode,TransMoney,FYCRate,FYC,(select codename from ldcode where ldcode.codetype='paymode' and ldcode.code = lacommision.PayMode),TPayDate  from lacommision  where 1=1  "
             + " and GrpContNo <> '00000000000000000000'"

             + getWherePart('ManageCom', 'ManageCom','like')
          	 + getWherePart('BranchType', 'BranchType')
          	 + getWherePart('BranchType2', 'BranchType2')
          	 + getWherePart('AgentCom', 'AgentCom')
          	 + strCon
          	 + getWherePart('AgentCode','SiteManagerCode')
          	 + " union all "
          	 + "select ManageCom,AgentCom,getUniteCode(AgentCode),(select Name from laagent where laagent.agentcode =lacommision.agentcode ),ContNo,AppntNo,RiskCode,TransMoney,FYCRate,FYC,(select codename from ldcode where ldcode.codetype='paymode' and ldcode.code = lacommision.PayMode),TPayDate  from lacommision  where 1=1  "
             + " and GrpContNo = '00000000000000000000'"
             + getWherePart('ManageCom', 'ManageCom','like')
          	 + getWherePart('BranchType', 'BranchType')
          	 + getWherePart('BranchType2', 'BranchType2')
          	 + getWherePart('AgentCom', 'AgentCom')
          	 + strCon
          	 + getWherePart('AgentCode','SiteManagerCode')
          	 ;
  
 //alert(strSql);
	turnPage.queryModal(sql, LACommisionGrid);   	
	
}
function afterCodeSelect(codeName,Field)
{
	if(codeName == "BranchType2")
	{
          var  sql=" select enddate+1 day  from  lawagelog  where 1=1 and  wageno=(select max(wageno) from lawagelog where branchtype='"+fm.BranchType.value+"' "
                     +"  and  branchtype2='"+fm.BranchType2.value+"' and managecom='"+fm.ManageCom.value+"') "
                     + getWherePart('ManageCom', 'ManageCom')
                     + getWherePart('BranchType', 'BranchType')
          	     + getWherePart('BranchType2', 'BranchType2');
          var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
       
   //查询成功则拆分字符串，返回二维数组
   
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);
	
          if( tArr != null )
	  {
           fm.all('StartDate').value= tArr[0][0];                                   
             
          }
	}
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("计算起期必须小于等于计算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}

function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
  var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
	//查询所有人员
	//var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}


