<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：DataIntoLACommisionSave.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="java.text.SimpleDateFormat"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>
<%
  // 输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String currentDate = PubFun.getCurrentDate();
  String currentTime = PubFun.getCurrentTime();
  currentDate = AgentPubFun.formatDate(currentDate,"yyyy-MM-dd");
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  //tGI.ManageCom="8611";
  //tGI.Operator="Admin";
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  {
  
    String ManageCom = request.getParameter("ManageCom");
    String StartDate = request.getParameter("StartDate");
    String EndDate   = request.getParameter("EndDate");
    String tBranchType = request.getParameter("BranchType");
    String tBranchType2 = request.getParameter("BranchType2");
    System.out.println("BranchType:"+tBranchType);
    System.out.println("BranchType2:"+tBranchType2);
    System.out.println("EndDate:"+EndDate);
    String Year  = AgentPubFun.formatDate(StartDate,"yyyyMM");
    String Month = AgentPubFun.formatDate(StartDate,"MM");    
    String YearMonth = Year+Month;
    String strInfo = "";
    System.out.println("YearMonth:"+YearMonth);

    StartDate = AgentPubFun.formatDate(StartDate,"yyyy-MM-dd");
    EndDate = AgentPubFun.formatDate(EndDate,"yyyy-MM-dd");
    if(currentDate.compareTo(EndDate)<=0)
    {
        
   		FlagStr="Fail";
   		Content="计算止期应小于今天";
   		
    }
    else 
    {
    System.out.println("StartDate2:"+StartDate);
    System.out.println("EndDate2:"+EndDate);
		String currDay=StartDate;
    if(StartDate.compareTo(EndDate)>0)
   {		
    
   		FlagStr="Fail";
   		Content="计算止期应大于计算起期";
   }		
   else
   {				
   while (currDay.compareTo(EndDate)<=0)
   {
   //佣金计算日志表
   LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
   tLAWageLogSchema.setManageCom(ManageCom);
   tLAWageLogSchema.setStartDate(currDay);
   tLAWageLogSchema.setEndDate(currDay);
   if (tBranchType == null || tBranchType.equals(""))   
     tLAWageLogSchema.setBranchType("");  //0:表示三种展业类型的数据都提     
   else
     tLAWageLogSchema.setBranchType(tBranchType);
    if (tBranchType2 == null || tBranchType2.equals(""))
     tLAWageLogSchema.setBranchType2("");  //0:表示三种展业类型的数据都提
   else
     tLAWageLogSchema.setBranchType2(tBranchType2);
   AgentWageCalSaveUI tAgentWageCalSaveUI=new AgentWageCalSaveUI();
   VData tVData=new VData();
   tVData.addElement(tGI);
   
   tVData.addElement(tLAWageLogSchema);
   
   if (!tAgentWageCalSaveUI.submitData(tVData,""))
   {
   
   		FlagStr="Fail";
   		Content=tAgentWageCalSaveUI.mErrors.getFirstError();
   		break;
   }
  else
  	{
  	
  	   AgentWageCalDoUI tAgentWageCalDoUI=new AgentWageCalDoUI();
  	   if (!tAgentWageCalDoUI.submitData(tVData,""))
  	   {
  	     FlagStr="Fail";
  	     Content=tAgentWageCalDoUI.mErrors.getFirstError();
  	     break;
  	   }
  	 else
  	 	{
  	 	  FlagStr="Succ";
  	 	  Content="保存成功";
  	 	}
  	}
  	
    currDay=PubFun.calDate(currDay,1,"D",null);
   }    
   }
   }
         System.out.println(Content);
 }//页面有效区

%>
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>