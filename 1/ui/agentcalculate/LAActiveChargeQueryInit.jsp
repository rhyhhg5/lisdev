<%
//程序名称：LAActiveChargeQueryInit.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
    fm.all('BranchType').value = mBranchType;
    fm.all('BranchType2').value = mBranchType2;
  }
  catch(ex)
  {
    alert("LAActiveChargeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
   // alert(121);
    initLACommisionGrid(); 
    initLACommisionGrid1();
    initLACommisionGrid2();
   
  }
  catch(re)
  {
    alert("LAActiveChargeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var LACommisionGrid;
var LACommisionGrid1;
var LACommisionGrid2;
function initLACommisionGrid() {  
//alert(12);                            
  var iArray = new Array();
//  alert(12);
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构编码";         		//列名
    iArray[1][1]="80px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="中介机构编码";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    iArray[4]=new Array();
    iArray[4][0]="中介机构名称";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="保单号";         		//列名
    iArray[5][1]="100px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许    
    
    iArray[6]=new Array();
    iArray[6][0]="险种";         		//列名
    iArray[6][1]="60px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="保费";         		//列名
    iArray[7][1]="60px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
           
    iArray[8]=new Array();
    iArray[8][0]="手续费比例";         		//列名
    iArray[8][1]="80px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="手续费";         		//列名
    iArray[9][1]="60px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许

    iArray[10]=new Array();
    iArray[10][0]="手续费类型";         		//列名
    iArray[10][1]="80px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="财务结算日期";         		//列名
    iArray[11][1]="100px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="财务结算类型";         		//列名
    iArray[12][1]="100px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();
    iArray[13][0]="手续费结算状态";         		//列名
    iArray[13][1]="100px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();
    iArray[14][0]="手续费给付号";         		//列名
    iArray[14][1]="100px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=0;         		//是否允许录入，0--不能，1--允许
    
    
    iArray[15]=new Array();
    iArray[15][0]="投保人";         		//列名
    iArray[15][1]="100px";         		//宽度
    iArray[15][3]=100;         		//最大长度
    iArray[15][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[16]=new Array();
    iArray[16][0]="印刷号";         		//列名
    iArray[16][1]="100px";         		//宽度
    iArray[16][3]=100;         		//最大长度
    iArray[16][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[17]=new Array();
    iArray[17][0]="套餐编码";         		//列名
    iArray[17][1]="100px";         		//宽度
    iArray[17][3]=100;         		//最大长度
    iArray[17][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[18]=new Array();
    iArray[18][0]="卡单批单号";         		//列名
    iArray[18][1]="100px";         		//宽度
    iArray[18][3]=100;         		//最大长度
    iArray[18][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[19]=new Array();
    iArray[19][0]="财务实付状态";         		//列名
    iArray[19][1]="100px";         		//宽度
    iArray[19][3]=100;         		//最大长度
    iArray[19][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[20]=new Array();
    iArray[20][0]="财务实付时间";         		//列名
    iArray[20][1]="100px";         		//宽度
    iArray[20][3]=100;         		//最大长度
    iArray[20][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[21]=new Array();
    iArray[21][0]="保单类型";         		//列名
    iArray[21][1]="100px";         		//宽度
    iArray[21][3]=100;         		//最大长度
    iArray[21][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[22]=new Array();
    iArray[22][0]="销售机构代码";         		//列名
    iArray[22][1]="100px";         		//宽度
    iArray[22][3]=100;         		//最大长度
    iArray[22][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[23]=new Array();
    iArray[23][0]="销售机构名称";         		//列名
    iArray[23][1]="100px";         		//宽度
    iArray[23][3]=100;         		//最大长度
    iArray[23][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[24]=new Array();
    iArray[24][0]="业务员代码";         		//列名
    iArray[24][1]="100px";         		//宽度
    iArray[24][3]=100;         		//最大长度
    iArray[24][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[25]=new Array();
    iArray[25][0]="业务员名称";         		//列名
    iArray[25][1]="100px";         		//宽度
    iArray[25][3]=100;         		//最大长度
    iArray[25][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[26]=new Array();
    iArray[26][0]="险种名称";         		//列名
    iArray[26][1]="100px";         		//宽度
    iArray[26][3]=100;         		//最大长度
    iArray[26][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[27]=new Array();
    iArray[27][0]="投保日期";         		//列名
    iArray[27][1]="100px";         		//宽度
    iArray[27][3]=100;         		//最大长度
    iArray[27][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[28]=new Array();
    iArray[28][0]="承保日期";         		//列名
    iArray[28][1]="100px";         		//宽度
    iArray[28][3]=100;         		//最大长度
    iArray[28][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[29]=new Array();
    iArray[29][0]="交费频次";         		//列名
    iArray[29][1]="100px";         		//宽度
    iArray[29][3]=100;         		//最大长度
    iArray[29][4]=0;         		//是否允许录入，0--不能，1--允许
   
    iArray[30]=new Array();
    iArray[30][0]="缴费年期";         		//列名
    iArray[30][1]="100px";         		//宽度
    iArray[30][3]=100;         		//最大长度
    iArray[30][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[31]=new Array();
    iArray[31][0]="缴费次数";         		//列名
    iArray[31][1]="100px";         		//宽度
    iArray[31][3]=100;         		//最大长度
    iArray[31][4]=0;         		//是否允许录入，0--不能，1--允许
    
    
    
    
   LACommisionGrid = new MulLineEnter( "fm" , "LACommisionGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACommisionGrid.mulLineCount = 0;   
    LACommisionGrid.displayTitle = 1;
    LACommisionGrid.hiddenPlus = 1;
    LACommisionGrid.hiddenSubtraction = 1;
    LACommisionGrid.canSel = 0;
    LACommisionGrid.canChk = 0;
  //  LACommisionGrid.selBoxEventFuncName = "showOne";
  
    LACommisionGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACommisionGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
function initLACommisionGrid2() {  
//alert(12);                            
  var iArray = new Array();
//  alert(12);
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构编码";         		//列名
    iArray[1][1]="80px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="中介机构编码";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    iArray[4]=new Array();
    iArray[4][0]="中介机构名称";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;
    
   	iArray[5]=new Array();
    iArray[5][0]="对方中介机构代码";         		//列名
    iArray[5][1]="100px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许
            
    iArray[6]=new Array();
    iArray[6][0]="对方中介机构名称";         		//列名
    iArray[6][1]="0px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=3;
            
    iArray[7]=new Array();
    iArray[7][0]="对方业务员代码";         		//列名
    iArray[7][1]="100px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许  
            
    iArray[8]=new Array();
    iArray[8][0]="对方业务员姓名";         		//列名
    iArray[8][1]="100px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;
            
 		iArray[9]=new Array();
    iArray[9][0]="保单号";         		//列名
    iArray[9][1]="100px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许    
    				
    iArray[10]=new Array();
    iArray[10][0]="险种";         		//列名
    iArray[10][1]="60px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;         		//是否允许录入，0--不能，1--允许
             
    iArray[11]=new Array();
    iArray[11][0]="保费";         		//列名
    iArray[11][1]="60px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许
             
    iArray[12]=new Array();
    iArray[12][0]="手续费比例";         		//列名
    iArray[12][1]="80px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
             
    iArray[13]=new Array();
    iArray[13][0]="手续费";         		//列名
    iArray[13][1]="60px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;         		//是否允许录入，0--不能，1--允许

    iArray[14]=new Array();
    iArray[14][0]="手续费类型";         		//列名
    iArray[14][1]="80px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=0;         		//是否允许录入，0--不能，1--允许
             
    iArray[15]=new Array();
    iArray[15][0]="财务结算日期";         		//列名
    iArray[15][1]="100px";         		//宽度
    iArray[15][3]=100;         		//最大长度
    iArray[15][4]=0;         		//是否允许录入，0--不能，1--允许
          
    iArray[16]=new Array();
    iArray[16][0]="财务结算类型";         		//列名
    iArray[16][1]="100px";         		//宽度
    iArray[16][3]=100;         		//最大长度
    iArray[16][4]=0;         		//是否允许录入，0--不能，1--允许
             
    iArray[17]=new Array();
    iArray[17][0]="手续费结算状态";         		//列名
    iArray[17][1]="100px";         		//宽度
    iArray[17][3]=100;         		//最大长度
    iArray[17][4]=0;         		//是否允许录入，0--不能，1--允许
             
    iArray[18]=new Array();
    iArray[18][0]="手续费给付号";         		//列名
    iArray[18][1]="100px";         		//宽度
    iArray[18][3]=100;         		//最大长度
    iArray[18][4]=0;         		//是否允许录入，0--不能，1--允许
           
             
    iArray[19]=new Array();
    iArray[19][0]="投保人";         		//列名
    iArray[19][1]="100px";         		//宽度
    iArray[19][3]=100;         		//最大长度
    iArray[19][4]=0;         		//是否允许录入，0--不能，1--允许
             
    iArray[20]=new Array();
    iArray[20][0]="印刷号";         		//列名
    iArray[20][1]="100px";         		//宽度
    iArray[20][3]=100;         		//最大长度
    iArray[20][4]=0;         		//是否允许录入，0--不能，1--允许
             
    iArray[21]=new Array();
    iArray[21][0]="套餐编码";         		//列名
    iArray[21][1]="100px";         		//宽度
    iArray[21][3]=100;         		//最大长度
    iArray[21][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[22]=new Array();
    iArray[22][0]="卡单批单号";         		//列名
    iArray[22][1]="100px";         		//宽度
    iArray[22][3]=100;         		//最大长度
    iArray[22][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[23]=new Array();
    iArray[23][0]="财务实付状态";         		//列名
    iArray[23][1]="100px";         		//宽度
    iArray[23][3]=100;         		//最大长度
    iArray[23][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[24]=new Array();
    iArray[24][0]="财务实付时间";         		//列名
    iArray[24][1]="100px";         		//宽度
    iArray[24][3]=100;         		//最大长度
    iArray[24][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[25]=new Array();
    iArray[25][0]="保单类型";         		//列名
    iArray[25][1]="100px";         		//宽度
    iArray[25][3]=100;         		//最大长度
    iArray[25][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[26]=new Array();
    iArray[26][0]="销售机构代码";         		//列名
    iArray[26][1]="100px";         		//宽度
    iArray[26][3]=100;         		//最大长度
    iArray[26][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[27]=new Array();
    iArray[27][0]="销售机构名称";         		//列名
    iArray[27][1]="100px";         		//宽度
    iArray[27][3]=100;         		//最大长度
    iArray[27][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[28]=new Array();
    iArray[28][0]="业务员代码";         		//列名
    iArray[28][1]="100px";         		//宽度
    iArray[28][3]=100;         		//最大长度
    iArray[28][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[29]=new Array();
    iArray[29][0]="业务员名称";         		//列名
    iArray[29][1]="100px";         		//宽度
    iArray[29][3]=100;         		//最大长度
    iArray[29][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[30]=new Array();
    iArray[30][0]="险种名称";         		//列名
    iArray[30][1]="100px";         		//宽度
    iArray[30][3]=100;         		//最大长度
    iArray[30][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[31]=new Array();
    iArray[31][0]="投保日期";         		//列名
    iArray[31][1]="100px";         		//宽度
    iArray[31][3]=100;         		//最大长度
    iArray[31][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[32]=new Array();
    iArray[32][0]="承保日期";         		//列名
    iArray[32][1]="100px";         		//宽度
    iArray[32][3]=100;         		//最大长度
    iArray[32][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[33]=new Array();
    iArray[33][0]="交费频次";         		//列名
    iArray[33][1]="100px";         		//宽度
    iArray[33][3]=100;         		//最大长度
    iArray[33][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[34]=new Array();
    iArray[34][0]="缴费年期";         		//列名
    iArray[34][1]="100px";         		//宽度
    iArray[34][3]=100;         		//最大长度
    iArray[34][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[35]=new Array();
    iArray[35][0]="缴费次数";         		//列名
    iArray[35][1]="100px";         		//宽度
    iArray[35][3]=100;         		//最大长度
    iArray[35][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[36]=new Array();
    iArray[36][0]="回执签收日期";  
    iArray[36][1]="80px";         
    iArray[36][3]=100;         	
    iArray[36][4]=0;
    
    iArray[37]=new Array();
    iArray[37][0]="是否过犹豫期";  
    iArray[37][1]="80px";         
    iArray[37][3]=100;         	
    iArray[37][4]=0;
    
   LACommisionGrid2 = new MulLineEnter( "fm" , "LACommisionGrid2" ); 
    //这些属性必须在loadMulLine前
 
    LACommisionGrid2.mulLineCount = 0;   
    LACommisionGrid2.displayTitle = 1;
    LACommisionGrid2.hiddenPlus = 1;
    LACommisionGrid2.hiddenSubtraction = 1;
    LACommisionGrid2.canSel = 0;
    LACommisionGrid2.canChk = 0;
  //  LACommisionGrid2.selBoxEventFuncName = "showOne";
  
    LACommisionGrid2.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACommisionGrid2.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
function initLACommisionGrid1() {                              
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="银邮机构";       		//列名
    iArray[1][1]="90px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构";         		//列名
    iArray[2][1]="70px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    
    iArray[3]=new Array();
    iArray[3][0]="营业部";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许    
    
    iArray[4]=new Array();
    iArray[4][0]="销售人员代码";         		//列名
    iArray[4][1]="70px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array();
    iArray[5][0]="销售人员姓名";         		//列名
    iArray[5][1]="70px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许
           
    iArray[6]=new Array();
    iArray[6][0]="合同号";         		//列名
    iArray[6][1]="80px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="客户姓名";         		//列名
    iArray[7][1]="70px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="险种代码";         		//列名
    iArray[8][1]="60px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="保费";         		//列名
    iArray[9][1]="60px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="到帐日";         		//列名
    iArray[10][1]="70px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="生效日";         		//列名
    iArray[11][1]="70px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="满期日";         		//列名
    iArray[12][1]="70px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
    
   LACommisionGrid1 = new MulLineEnter( "fm" , "LACommisionGrid1" ); 
    //这些属性必须在loadMulLine前
 
    LACommisionGrid1.mulLineCount = 0;   
    LACommisionGrid1.displayTitle = 1;
    LACommisionGrid1.hiddenPlus = 1;
    LACommisionGrid1.hiddenSubtraction = 1;
    LACommisionGrid1.canSel = 0;
    LACommisionGrid1.canChk = 0;
  //  LACommisionGrid.selBoxEventFuncName = "showOne";
  
    LACommisionGrid1.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACommisionGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
