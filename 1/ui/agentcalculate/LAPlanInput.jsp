 <%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAPlanInput.jsp
//程序功能：团代的计划任务录入
//创建日期：2003-07-04 09:54:00
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAPlanInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAPlanInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<body  onload="initForm();" >
  <form action="./LAPlanSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
   <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPlan1);">
     <td class=titleImg>
      计划任务信息
     </td>
    </td>
    </table>
  <Div  id= "divLAPlan1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  计划类型 
		</td>
        <td  class= input> 
		  <input class='codeno' name=PlanType verify="计划类型|notnull&code:PlanType"
		         CodeData="0|^1|个人计划任务 ^2|营业部计划任务 ^3|支公司计划任务 ^4|分公司计划任务" 
		         ondblclick="showCodeListEx('PlanType',[this,PlanTypeName],[0,1]);" 
		         onkeyup="showCodeListKeyEx('PlanType',[this,PlanTypeName],[0,1]);"
		         ><input class='codename' name=PlanTypeName >
  
		</td>  
   <td  class= title> 
         计划区间单位
		</td>
        <td  class= input> 
		  <input name=PlanPeriodUnit class='codeno'  
		  ondblclick="return showCodeList('PlanPeriodUnit',[this,PlanPeriodUnitName],[0,1]);" 
      onkeyup="return showCodeListKey('PlanPeriodUnit',[this,PlanPeriodUnitName],[0,1]);"
       ><input name=PlanPeriodUnitName class='codename' >
		</td>
      </tr>
      <tr  class= common>    
       
        <td  class= title> 
		  计划对象 
		</td>
        <td  class= input> 
		  <input class=common name=PlanObject verify="计划对象|notnull" onchange="return checkValid();"> 
		</td>
      
        <td  class= title> 
		  对象名称
		</td>
        <td  class= input> 
		    <input class= 'readonly' readonly name=ObjectName  > 
		</td>
		 </tr>
      <tr  class= common>
        <td  class= title> 
		  计划标准
		</td>
        <td  class= input> 
		    <input name=PlanValue class=common verify = "计划标准|notnull&NUM" > 
		</td>
     
        <td  class= title> 
		  计划起期
		   </td>
        <td  class= input> 
		   <Input class= "coolDatePicker" dateFormat="short" name=PlanStartDate verify="计划起期|NOTNULL "> 
		</td>	
		 </tr>
      <tr  class= common> 
		<td  class= title> 
		  计划止期
		   </td>
        <td  class= input> 
		   <Input class= "coolDatePicker" dateFormat="short" name=PlanEndDate verify="计划止期|NOTNULL "> 
		</td>	
	
		<td  class= title> 
		  计划类别
		</td>
        <td  class= input> 
			  <input name=PlanCond3 class='codeno'  
		         ondblclick="return showCodeList('plankindpicc',[this,PlanCond3Name],[0,1]);" 
                         onkeyup="return showCodeListKey('plankindpicc',[this,PlanCond3Name],[0,1]);"
                   ><input name=PlanCond3Name class='codename' >  
		</td>	
		 	</tr>
		<tr  class= common> 
        <td  class= title> 
		  操作员代码
		</td>
        <td  class= input> 
		  <input name=Operator class= 'readonly'readonly > 
		</td>
	<!--td  class= title> 
		  计划险种类别
		</td>
        <td  class= input> 
			  <input name=PlanCond1 class='codeno'  
		         ondblclick="return showCodeList('larisktype',[this,PlanCond1Name],[0,1]);" 
                         onkeyup="return showCodeListKey('larisktype',[this,PlanCond1Name],[0,1]);" 
               ><input name=PlanCond1Name  class=codename >  
		</td-->		
      </tr>
     
         </table>
  </Div>
    <input name=PlanCode type=hidden value = ''>
    <input name=BranchType type=hidden value = ''>
    <input name=BranchType2 type=hidden value = ''>
    <input name=hideOperate type=hidden value = ''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>


<script>
  var sql = "";
  var sqlField = "";
</script>