<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAActiveChargeQuery.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();    
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
%>
<script>
	 var  mBranchType =  <%=BranchType%>;
	 var  mBranchType2 =  '<%=BranchType2%>';
	 var  MngSQL=" 1 and length(trim(comcode)) in (#8#) and comcode  in (select code from ldcode where codetype =#jiaochaMng#)"; 
	 </script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LAActiveMixedCheckInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LAActiveMixedCheckInit.jsp"%>
</head> 
<body  onload="initForm();initElementtype();" >
  <form action="LAActiveChargeMixedQueryReport.jsp" method=post name=fm target="fraSubmit">
  <input type=hidden name=querySQL value=""> 
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		手续费查询
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,MngSQL,1);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,MngSQL,1);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>        
           <TD class = title>
             中介机构
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentCom ondblclick="return getAgentComName(this,AgentComName);" onkeyup="return getAgentComName(this,AgentComName);"  ><Input class=codename name=AgentComName readOnly >
          </TD>
        </TR>
        <TR  class = common>
           <TD  class = title>
           财务结算起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="财务结算起期|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            财务结算止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="财务结算止期|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          
        </TR> 
        <tr class=common>	
  		  <td  class= title>结算状态</td>
          <TD  class= input> <Input class= "codeno" name=ChargeState CodeData="0|^1|已结算|^0|未结算|^X|不结算" ondblclick="return showCodeListEx('F01list',[this,StateName],[0,1]);" onkeyup="return showCodeListKeyEx('F01list',[this,StateName],[0,1]);" onchange="" readonly=true><input class=codename name=StateName  readonly=true ></TD> 	
  		<TD  class = title>
           保险合同号
          </TD>
          <TD  class= input>
     		<Input class= 'common' name=GrpContNo >
    	  </TD>
    	  
    	 
  		</tr>
  		<tr class=common>  
         <TD  class = title>
           卡单批单号
          </TD>
          <TD  class= input>
     		<Input class= 'common' name=CardNo >
    	  </TD>
    	  <td  class= title>财务实付状态</td>
          <TD  class= input> <Input class= "codeno" name=ConfState CodeData="0|^1|已付|^0|未付|" ondblclick="return showCodeListEx('ConfState',[this,ConfStateName],[0,1]);" onkeyup="return showCodeListKeyEx('ConfState',[this,ConfStateName],[0,1]);" onchange="" ><input class=codename name=ConfStateName  readonly=true ></TD>
    	 </tr>
    	 <tr>
         <TD  class = title>
           销售机构代码
          </TD>
          <TD  class= input> <Input class= "common" name=BranchAttr></TD>
            <TD  class = title>
         销售机构名称      
          </TD>
          <TD  class= input>
            <input class ="common" name =BranchName>
          </TD>  
    	 </tr>
    	  <tr class=common>
         <TD  class = title>
           业务员代码
          </TD>
          <TD  class= input> <Input class= "common" name=GroupAgentCode></TD>
            <TD  class = title>
          业务员名称      
          </TD>
          <TD  class= input>
            <input class ="common" name =AgentName>
          </TD>  
    	 </tr>
    	  <tr class=common>
         <TD  class = title>
           险种名称
          </TD>
            <TD  class= input>
            <Input class= 'codeno' name= RiskCode 
             ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);"
             onkeyup="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);"
            ><Input class=codename name=RiskCodeName readOnly  >
          </TD>
            <TD  class = title>
          投保日期      
          </TD>
          <TD  class= input>
            <input class ="coolDatePicker" dateFormat="short" name =SignDate elementtype=nacessary />
          </TD>  
    	 </tr>
    	  <tr class=common>
         <TD  class = title>
           承保日期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=CValiDate elementtype=nacessary />
           </TD>
            <TD  class = title>
          交费频次      
          </TD>
          <TD  class= input>
            <input class ="common" name =PayIntv>
          </TD>  
    	 </tr>
    	  <tr class=common>
         <TD  class = title>
           缴费年期
          </TD>
          <TD  class= input> <Input class= "common" name=PayYears></TD>
            <TD  class = title>
          缴费次数     
          </TD>
          <TD  class= input>
            <input class ="common" name =PayCount>
          </TD>  
    	 </tr>  
    <tr class=commmon>
    <td class = title>
        手续费类型
    </td>
    <td class= input><input class = "codeno" name = FeeType CodeData="0|^0|业务手续费|^1|管理手续费|" ondblclick="return showCodeListEx('FeeType',[this,FeeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeType',[this,FeeName],[0,1]);" onchange=""><input class=codename name=FeeName readonly = true ></td>
    <td class = title>
    交叉销售类型</td>
    <td class = input><input class = "codeno" name = InnerType CodeData="0|^1|产代健|^2|寿代健|" ondblclick="return showCodeListEx('InnerType',[this,InnerName],[0,1]);" onkeyup="return showCodeListKeyEx('InnerType',[this,InnerName],[0,1]);" onchange=""><input class=codename name=InnerName readonly = true ></td>
    </tr>	
    <tr class=common>
					<TD class=title>是否过犹豫期</TD>
         <td class= input><input class = "codeno" name = hesitant readonly CodeData="0|^0|是|^1|否|" ondblclick="return showCodeListEx('hesitant',[this,hesitantName],[0,1]);"   onkeyup="return showCodeListKeyEx('hesitant',[this,hesitantName],[0,1]);" onchange=""><input class=codename name=hesitantName readonly = true  ></td>
         <TD  class = title>
           保单类型
          </TD>
          <TD  class= input> <Input class= "codeno" name=ContFlag CodeData="0|^0|新单|^1|保全|" ondblclick="return showCodeListEx('ContFlag',[this,ContFlagName],[0,1]);" onkeyup="return showCodeListKeyEx('ConfState',[this,ContFlagName],[0,1]);" onchange="" ><input class=codename name=ContFlagName  readonly=true ></TD>
	</tr>                    
      </table>       
         <Table class="common" align=center>
  			<Tr>
    			<Td class=button>
      				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="easyQueryClick()">
      				<INPUT class=cssButton VALUE="打  印"  TYPE=button onclick="doDownLoad()">
    			</Td>
  			</Tr>
		</Table>
         <!-- 
         <input type =button class=common value="银保保费明细查询" onclick="easyQueryClick1();">
         <input type =button class=cssButton value="查询" onclick="easyQueryClick();">
          --> 
          <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">
          <input type="hidden" class=input name=BranchType2 value="<%=BranchType2%>">           
    	  <input type="hidden" class=input name=CrsType value='Y'>           
    	  
    </Div>
    <p> <font color="#ff0000">注：为防止系统压力过大，用户点击打印后请耐心等待系统后台处理！不要重复点击“打印”按钮以防用户被锁定</font></p> 
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
	<Div id= "divSpanLACommisionGrid2" align=center style= "display: '' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid2">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
    	<Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      </div>
      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>