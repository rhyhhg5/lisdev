<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：佣金基数表设置录入界面
//创建时间：2014-12-1
//创建人  ：yangyang
// above is miaoxiangzheng's new_add
String BranchType2=request.getParameter("BranchType2");
if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<%@page contentType="text/html;charset=GBK" %>
<script language="JavaScript">
 	var msql=" 1 and   char(length(trim(comcode)))<=#8# ";
</script>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="InterImportChargeInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="InterImportChargeInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   
</head>
   
<body  onload="initForm();initElementtype();" >
 <form action="" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
   <tr>	
    <TD  class= title>
            销售人员工号
          </TD>
          <TD  class= input>
            <Input class= "common"  name=GroupAgentCode >
          </TD>
            <TD  class= title>
            销售人员姓名
          </TD>
          <TD  class= input>
            <Input class= "common"  name=AgentName >
          </TD>
   </tr>
   <tr class=common> 
          
          <td  class= title>
   		销售机构
   		</td>
          <TD  class= input> 
          <Input class= "common" name=MangeCom verify="销售机构|code:null" elementtype=nacessary>
          </TD>
           <TD  class= title>
           保单号
          </TD>
          <TD  class= input>
             <Input class="common" name=ContNo ></TD>
   </tr>
   <TR  class= common> 
		<TD  class= title>
            投保人
    </TD>
        <TD  class= input>
            <Input class="common" name=AppntName  > 
     </TD> 
          <TD  class = title>
           起保日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="起始年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TR>
               <TD  class = title>
           终保日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="起始年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <Input class="readonly" type=hidden name=diskimporttype>
   </TR>
    
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 
    	</td>
    </tr>      
  </table>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>佣金基数例表</td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>
  <br><hr>
  <Input type=hidden name=branchtype2> 
  <Input type=hidden id="BranchType" name=BranchType value=''>
  <input type=hidden id="sql_where" name="sql_where" >
  <input type=hidden id="fmAction" name="fmAction">  
  <input type=hidden class=Common name=querySql > 
  </form>
<form action="./InterLADiskImportActiveSave.jsp" method=post name=fm2 enctype="multipart/form-data" target="fraSubmit">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportFile);">
    		</td>
    		<td class= titleImg>佣金磁盘导入</td>
    		
    	</tr>
  </table>
  <div id="divImportFile" style="display:''">   

  <table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">文件名</TD>
				<TD class=input>
					<Input type="file" class= "common" style="width:300px" name="FileName" id="FileName"/>
				</TD><td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()"></td>
			</TR>
  </table>
				  <Input type=button class=cssButton name="goDiskImport" value="导  入" id="goDiskImport"  onclick="diskImport()">
				  <Input type=button class=cssButton value="重  置" onclick="clearImport()">

<!--
 <Input type=button class=cssButton name="goDiskImport" value="磁盘导入" id="goDiskImport"  onclick="diskImport()">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()">
-->

<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportQuery);">
    		</td>
    		<td class= titleImg>佣金磁盘导入日志查询</td>
    		
    	</tr>
  </table>
  <div id="divImportQuery" style="display:''">
  	<table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">导入文件批次</TD>
				<TD class=input>
					<Input type="text" class= "common"  name="FileImportNo"/>
				</TD>
				
				<TD  class=title style="width:100px">
				查询类型
				</td>
				<td class=input>
				<Input class="codeno" name=queryType  CodeData="0|^0|全部|^1|导入错误行|^2|导入成功行" ondblclick="return showCodeListEx('querytypelist',[this,queryName],[0,1]);" onkeyup="return showCodeListKeyEx('querytypelist',[this,queryName],[0,1]);" onchange=""><input class=codename name=queryName  readonly=true >
				</TD>
				<td></td>
			</TR>
   </table>
  <Input type=button class=cssButton value="查  询" onclick="queryImportResult()"/>
  <div id="divImportResultGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanImportResultGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage2.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage2.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage2.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage2.lastPage();">
  </div>
  </div>  
 </div>
  <Input type=hidden name=branchtype2> 
  <Input type=hidden  name=BranchType >
  <Input type=hidden name=diskimporttype>
 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




