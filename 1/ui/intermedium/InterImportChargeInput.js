//该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2009-01-13
//创建人  ：   miaoxz
//更新记录：  更新人    更新日期     	更新原因/内容
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var queryFlag = false;
var strSQL ="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
if(showInfo!=null)
{
  try
  {
    showInfo.focus();  
  }
  catch(ex)
  {
    showInfo=null;
  }
}
}


function clearImport(){
	document.getElementById('FileName').outerHTML=document.getElementById('FileName').outerHTML;
}
// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initSetGrid();
	var tManageCom  = fm.all('MangeCom').value;
	if ( tManageCom == null || tManageCom == "" )
	{
		alert("请输入销售机构！");
		return;
	}
	strSQL = "select calno,mangecom,agentname,groupagentcode," +
			 "contno,ascripcom,ascripname,riskcode,riskname,transmoney,chargerate,charge,startdate,enddate,appntname,carno,getmoneyname, "+
	         "bankcode,orgcrspayvou,signdate,tmakedate,paynum,actno"
	+ " from lagetothercharge "
	+ " where 1=1  and mangeCom is not null    "
	+ getWherePart( 'GroupAgentCode')
	+ getWherePart( 'AgentName','AgentName','like')
	+ getWherePart( 'MangeCom','MangeCom','like')
	// + getWherePart( 'a.f05','payyears')
	+ getWherePart( 'ContNo')
	+ getWherePart( 'AppntName','AppntName','like')
	+ getWherePart('StartDate')
	+ getWherePart('EndDate')
	+ " Order by groupagentcode,startdate,enddate";
 
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
	queryFlag = true;
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
	//查询成功则拆分字符串，返回二维数
	//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = SetGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数
	var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	//调用MULTILINE对象显示查询结果
	displayMultiline(tArr, turnPage.pageDisplayGrid);
	//return true;
}
				
				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
	}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("初始化页面错误，重置出错");
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

							//判断是否选择了要增加、修改或删除的行

//提交前的校验、计算
function beforeSubmit()
{
	return true;
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";
	}
}
function diskImport()
{ 
    //alert("111111111111111");
		//alert("====="+fm.all("diskimporttype").value);
   if(fm2.FileName.value==null||fm2.FileName.value==""){
   	 alert("请选择要导入的文件！");
   	 return false;
   }
   var showStr="正在导入数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   fm2.submit();
}
function moduleDownload(){
	var tSQL="";
	fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "./InterLAActiveChargeCommisionDownload.jsp";
    fm.submit();
    fm.action = oldAction;
}

function queryImportResult(){
	var tbatchno=fm2.FileImportNo.value;
	var tqueryType=fm2.queryType.value;
	strSQL = "select batchno,contid,errorinfo"
	+ " from lcgrpimportlog "
	+ " where 1=1 and contno='Active'  and grpcontno='00000000000000000000' " ;
	if(tbatchno!=""&&tbatchno!=null)
	{
		strSQL+=" and batchno='"+tbatchno+"' ";
	}
	if(tqueryType==1){
		strSQL+=" and errorstate='1'";
	}else if(tqueryType==2){
		strSQL+=" and errorstate='0'";
	}
	strSQL+=" order by contid";
  //执行查询并返回结果
	turnPage2.queryModal(strSQL, ImportResultGrid);
}



