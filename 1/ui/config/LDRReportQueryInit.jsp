<%
//程序名称：LDRReportQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-25 11:06:58
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('SerialNo').value = "";
    fm.all('RReportCode').value = "";
    fm.all('RReportName').value = "";
    fm.all('RReportClass').value = "";
    fm.all('StartMoney').value = "";
    fm.all('EndMoney').value = "";
    fm.all('StartAge').value = "";
    fm.all('EndAge').value = "";
    fm.all('Sex').value = "";
    fm.all('Note1').value = "";
    fm.all('Note2').value = "";
  }
  catch(ex) {
    alert("在LDRReportQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDRReportGrid();  
  }
  catch(re) {
    alert("LDRReportQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDRReportGrid;
function initLDRReportGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="流水号";   
	iArray[1][1]="0px";   
	iArray[1][2]=20;        
	iArray[1][3]=3;
    
    iArray[2]=new Array(); 
	iArray[2][0]="生调项目代码";   
	iArray[2][1]="90px";   
	iArray[2][2]=20;        
	iArray[2][3]=0;
	
	iArray[3]=new Array(); 
	iArray[3][0]="生调项目名称";   
	iArray[3][1]="90px";   
	iArray[3][2]=20;        
	iArray[3][3]=0;
		
    iArray[4]=new Array(); 
	iArray[4][0]="生调类型";   
	iArray[4][1]="90px";   
	iArray[4][2]=20;        
	iArray[4][3]=0;
	
	iArray[5]=new Array(); 
	iArray[5][0]="生调人性别";   
	iArray[5][1]="90px";   
	iArray[5][2]=20;        
	iArray[5][3]=0;
	
	iArray[6]=new Array(); 
	iArray[6][0]="备注";   
	iArray[6][1]="120px";   
	iArray[6][2]=20;        
	iArray[6][3]=0;
	
    LDRReportGrid = new MulLineEnter( "fm" , "LDRReportGrid" ); 
    //这些属性必须在loadMulLine前

    LDRReportGrid.mulLineCount = 3;   
    LDRReportGrid.displayTitle = 1;
    LDRReportGrid.hiddenPlus = 1;
    LDRReportGrid.hiddenSubtraction = 1;
    LDRReportGrid.canSel = 1;
    LDRReportGrid.canChk = 0;
    //LDRReportGrid.selBoxEventFuncName = "showOne";

    LDRReportGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDRReportGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
