<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDUWAddFeeSave.jsp
//程序功能：
//创建日期：2005-01-20 09:41:35
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.config.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDUWAddFeeSchema tLDUWAddFeeSchema   = new LDUWAddFeeSchema();
  OLDUWAddFeeUI tOLDUWAddFeeUI   = new OLDUWAddFeeUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("transact:"+transact); 
  
  
    
    tLDUWAddFeeSchema.setICDCode(request.getParameter("ICDCode"));
    tLDUWAddFeeSchema.setICDName(request.getParameter("ICDName"));
    tLDUWAddFeeSchema.setDiseasDegree(request.getParameter("DiseasDegree"));
    tLDUWAddFeeSchema.setSex(request.getParameter("Sex"));
    tLDUWAddFeeSchema.setAge(request.getParameter("Age"));
    tLDUWAddFeeSchema.setRiskKind(request.getParameter("RiskKind"));
    tLDUWAddFeeSchema.setOPSFlag(request.getParameter("OPSFlag"));
    tLDUWAddFeeSchema.setOPSNo(request.getParameter("OPSNo"));
    tLDUWAddFeeSchema.setDiseasBTypeCode(request.getParameter("DiseasBTypeCode"));
    tLDUWAddFeeSchema.setDiseasType(request.getParameter("DiseasType"));
    tLDUWAddFeeSchema.setDiseasSTypeCode(request.getParameter("DiseasSTypeCode"));
    tLDUWAddFeeSchema.setDiseasSType(request.getParameter("DiseasSType"));
    tLDUWAddFeeSchema.setRiskGrade(request.getParameter("RiskGrade"));
    tLDUWAddFeeSchema.setOPSName(request.getParameter("OPSName"));
    tLDUWAddFeeSchema.setRelapse(request.getParameter("Relapse"));

  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLDUWAddFeeSchema);
  	tVData.add(tG);
    tOLDUWAddFeeUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDUWAddFeeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
