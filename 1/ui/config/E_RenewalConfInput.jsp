<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：电商续期配置
//创建日期：2017-12-08 
//创建人  ：LZJ
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>   
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="E_RenewalConfInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="E_RenewalConfInit.jsp"%>
  
  <title>电商续期配置</title>
<Script>
var comCode = <%=tG.ComCode%>
</Script>
</head>
<body  onload="initForm()">
  <form action="E_RenewalConfSave.jsp" method=post name=fm target="fraSubmit" >
  	<div>
	  	<table class= common border=0 width=100%>
	    	<tr>
				<td class= titleImg align= center>
					请录入查询条件
				</td>
			</tr>
		</table>
  	</div>
	<div>
	    <table  class= common align=center>      
	        <TR>
	  		  <TD  class= title>销售渠道</TD>
	          <TD  class= input colspan="3">
      			<Input class=codeNo name=saleChnl verify="销售渠道|notnull" onblur = "checkInput()" ondblclick="return showCodeList('unitesalechnl',[this,SaleChnlName],[0,1],null,'1',null,1);" onkeyup="return showCodeListKey('unitesalechnl',[this,SaleChnlName],[0,1],null,'1',null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>
	          </TD>
	  		  <TD  class= title>业务员代码</TD>
	          <TD  class= input colspan="3">
				<Input class=codename style="" name=agentCode onchange = "monitor(1)"/>
	          </TD>
	        </TR>
	        <TR>
	          <TD  class= title>中介机构代码</TD>
	          <TD  class= input colspan="3">
	            <Input class=codename style="width=130px;" name=agentCom onchange = "monitor(2)"/>
	          </TD>
	          <TD>
				<INPUT VALUE="查询" class=cssButton style="height=20px;width=80px;" TYPE=button onclick="queryLAAgent();"> 
	          </TD>
			</TR>
	    </table>
	</div>
  	<div>
	  	<table class= common border=0 width=100%>
	    	<tr>
				<td class= titleImg align= center>
					业务员/中介机构信息
				</td>
			</tr>
		</table>
  	</div>
	<div id= "divLAAgentGrid" style= "display: ''">
		<table class= common>
       		<tr class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLAAgentGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
	</div>
	<Div id= "divPage" align=center style= "display: none ">
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">      
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">       
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">       
	</Div>
	<div align=right>
	    <INPUT VALUE="添加配置" class=cssButton TYPE=button onclick="setConfig(1);"> 
		<span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span>
	</div>
	<div>
	  	<table class= common border=0 width=100%>
	    	<tr>
				<td class= titleImg align= center>
					已配置业务员/中介机构清单
				</td>
			</tr>
		</table>
  	</div>
	<div id= "divLDCodeGrid" style= "display: ''">
		<table class= common>
       		<tr class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLDCodeGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
	</div>
	<Div id= "divPage2" align=center style= "display: none ">
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();">      
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();">       
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();">       
	</Div>
	<div align=right>
		<INPUT VALUE="查询" class=cssButton lign=left TYPE=button onclick="queryLDCode();"> 
		<span>&nbsp&nbsp&nbsp&nbsp</span>
	   	<INPUT VALUE="删除配置" class=cssButton align=right TYPE=button onclick="setConfig(2);"> 
		<span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span>
	</div>
	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="Code" name="code">
	<input type=hidden id="CodeName" name="codeName">
	<input type=hidden id="ComCode" name="comCode">
	<input type=hidden id="OtherSign" name="otherSign">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

