
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function stopWrap(){
	if(!beforeStop('S')){
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "Stop";
	fm.submit();
}
function qstopWrap(){
	if(!beforeStop('Q')){
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "QStop";
	fm.submit();
}
function afterSubmit(FlagStr, content, cOperate) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}
function beforeStop(tFlag){
	//判定是否有选择打印数据
    var count = 0;
    var tStopCont = 0;
    var tQStopCont = 0;
    var tErrors = "";
	for(var i = 0; i < WrapGrid.mulLineCount; i++ )
	{
		if( WrapGrid.getChkNo(i) == true )
		{
			count ++;
			var tRowDatas = WrapGrid.getRowData(i);
			var tWrapCode = tRowDatas[0];
		    var tStopFlag = tRowDatas[5];
		    var tEndDate = tRowDatas[3];
		    if(tFlag == 'S'){
		    	if(tStopFlag == '0'){
			    	tStopCont++;
			    	if(tEndDate == null || trim(tEndDate) == '' ){
			    		tErrors = tErrors + "套餐："+tWrapCode+"的停售时间不能为空！\n";
			    	}else if(!isDate(tEndDate)){
			    		tErrors = tErrors + "套餐："+tWrapCode+"的停售时间格式不正确！\n";
			    	}
			    }else{
			    	tQStopCont++;
			    	tErrors = tErrors + "套餐："+tWrapCode+"的状态为【已停售】，不可进行停售处理！\n";
			    }
		    }else if(tFlag == 'Q'){
		    	if(tStopFlag == '0'){
			    	tStopCont++;
			    	tErrors = tErrors + "套餐："+tWrapCode+"的状态为【未停售】，不可进行取消停售处理！\n";
			    }else{
			    	tQStopCont++;
			    }
		    }
		}
	}
	if(count == 0){
		alert("请选择需要处理的套餐！");
		return false;
	}
	if(tStopCont>0 && tQStopCont>0){
		alert("处理的套餐中，存在已停售和未停售两种状态，不能同时处理！");
		return false;
	}
	if(tErrors != ""){
		alert("进行处理时：\n"+tErrors);
		return false;
	}
	return true;
}
function queryWrap(){
	initWrapGrid();
	var tTJ = "";
	if(fm.StateFlag.value == '1'){
		tTJ = " and lrw.enddate is not null ";
	}
	if(fm.StateFlag.value == '2'){
		tTJ = " and lrw.enddate is null ";
	}
	var strSql = "select distinct lrw.RiskWrapCode,lrw.RiskWrapName,lrw.startdate,lrw.enddate,"
	           +" case when lrw.enddate is not null then '已停售' else '未停售' end ,"
	           +" case when lrw.enddate is null then '0' else '1' end "
	           + "from LDRiskWrap lrw where 1=1 "
	           + tTJ
		       + getWherePart('RiskWrapCode', 'WrapCode')
		       + " order by lrw.RiskWrapCode";
	var strResult = easyExecSql(strSql);
	if(!strResult){
		alert("未查询到已配置的该套餐信息！");
		return ;
	}	       				   
	turnPage1.queryModal(strSql, WrapGrid);
}