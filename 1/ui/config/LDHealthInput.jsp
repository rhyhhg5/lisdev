<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-25 15:21:34
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDHealthInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDHealthInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDHealthSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDHealth1);">
    		</td>
    		 <td class= titleImg>
        		 体检资料描述表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDHealth1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    
    <TD  class= title>
      体检项目代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HealthCode elementtype=nacessary verify="体检项目代码|notnull&len<=20">
    </TD>
    <TD  class= title>
      体检项目名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HealthName elementtype=nacessary verify="体检项目名称|notnull&len<=10">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      体检额度起始值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StartMoney elementtype=nacessary verify="体检额度起始值|notnull&len<=60">
    </TD>
    <TD  class= title>
      体检额度终止值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndMoney elementtype=nacessary verify="体检额度终止值|num&notnull&len<=10">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      体检年龄起始值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StartAge elementtype=nacessary verify="体检年龄起始值|num&notnull&len<=10" >
    </TD>
    <TD  class= title>
      体检年龄终止值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndAge elementtype=nacessary verify="体检年龄终止值|num&notnull&len<=10">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      体检人性别
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=Sex  verify="体检人性别|notnull&len<=1" ondblclick=" showCodeList('Sex',[this,SexName],[0,1]);"  onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input name = "SexName" class = "codename" elementtype=nacessary >
    </TD>
    <TD  class= title>
      备注1
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Note1 verify="备注1|len<=60">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      备注2
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Note2 verify="备注2|len<=60">
    </TD>
  </TR>
</table>
    </Div>
    <div id="div1" style="display: 'none'">
    	<table>
    	<TD  class= title>
	      流水号
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=SerialNo >
	    </TD>
    	</table>
    </div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
