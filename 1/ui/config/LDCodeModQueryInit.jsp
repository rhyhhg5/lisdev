<%
//程序名称：LDCodeModQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-25 18:32:48
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CodeType').value = "";
    fm.all('Code').value = "";
    fm.all('CodeName').value = "";
    fm.all('Cont').value = "";
  }
  catch(ex) {
    alert("在LDCodeModQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDCodeModGrid();  
  }
  catch(re) {
    alert("LDCodeModQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDCodeModGrid;
function initLDCodeModGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
      
    iArray[1]=new Array();
    iArray[1][0]="代码类型";         		//列名
    iArray[1][1]="50px";         		//列名
    iArray[1][3]=0;         		//列名
    
    
    iArray[2]=new Array();
    iArray[2][0]="编码";         		//列名
    iArray[2][1]="30px";         		//列名
    iArray[2][3]=0;         		//列名
  
    iArray[3]=new Array();
    iArray[3][0]="编码名称";         		//列名
    iArray[3][1]="60px";         		//列名
    iArray[3][3]=0;         		//列名
  
    
    iArray[4]=new Array();
    iArray[4][0]="内容";         		//列名
    iArray[4][1]="200px";         		//列名
    iArray[4][3]=0;         		//列名
  
    
    
    
    LDCodeModGrid = new MulLineEnter( "fm" , "LDCodeModGrid" ); 
    //这些属性必须在loadMulLine前

    LDCodeModGrid.mulLineCount = 10;   
    LDCodeModGrid.displayTitle = 1;
    LDCodeModGrid.hiddenPlus = 1;
    LDCodeModGrid.hiddenSubtraction = 1;
    LDCodeModGrid.canSel = 1;
    LDCodeModGrid.canChk = 0;
    LDCodeModGrid.selBoxEventFuncName = "showOne";

    LDCodeModGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDCodeModGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
