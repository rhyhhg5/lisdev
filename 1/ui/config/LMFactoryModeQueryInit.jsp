<%
//程序名称：LMFactoryModeQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-12-22 14:24:48
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('RiskCode').value = "";
    fm.all('FactoryType').value = "";
    fm.all('FactoryCode').value = "";
    fm.all('FactoryName').value = "";
    fm.all('FactorySubCode').value = "";
    fm.all('FactorySubName').value = "";
    fm.all('CalSql').value = "";
    fm.all('ParamsNum').value = "";
    fm.all('Params').value = "";
    fm.all('CalRemark').value = "";
  }
  catch(ex) {
    alert("在LMFactoryModeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLMFactoryModeGrid();  
  }
  catch(re) {
    alert("LMFactoryModeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LMFactoryModeGrid;
function initLMFactoryModeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         	
    iArray[0][1]="30px";         	
    iArray[0][3]=0;         		
    
    iArray[1]=new Array();
    iArray[1][0]="险种编码";         	
    iArray[1][1]="30px";         	
    iArray[1][3]=0;         		
    
    iArray[2]=new Array();
    iArray[2][0]="要素类型";         	
    iArray[2][1]="30px";         	
    iArray[2][3]=0;         		
    
    iArray[3]=new Array();
    iArray[3][0]="计算编码";         	
    iArray[3][1]="30px";         	
    iArray[3][3]=0;         		
    
    
    iArray[4]=new Array();
    iArray[4][0]="计算名称";         	
    iArray[4][1]="30px";         	
    iArray[4][3]=3;         		
 
    iArray[5]=new Array();
    iArray[5][0]="计算子编码";         	
    iArray[5][1]="30px";         	
    iArray[5][3]=3;         		
        
    iArray[6]=new Array();
    iArray[6][0]="计算子名称";         	
    iArray[6][1]="30px";         	
    iArray[6][3]=0;         		
           		
    
    iArray[7]=new Array();
    iArray[7][0]="参数个数";         	
    iArray[7][1]="30px";         	
    iArray[7][3]=0;         		
          		
    
    
    
    
    LMFactoryModeGrid = new MulLineEnter( "fm" , "LMFactoryModeGrid" ); 
    //这些属性必须在loadMulLine前

    LMFactoryModeGrid.mulLineCount = 0;   
    LMFactoryModeGrid.displayTitle = 1;
    LMFactoryModeGrid.hiddenPlus = 1;
    LMFactoryModeGrid.hiddenSubtraction = 1;
    LMFactoryModeGrid.canSel = 1;
    LMFactoryModeGrid.canChk = 0;

    LMFactoryModeGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
