<%
//程序名称：LMFactoryCodeToTypeQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-12-22 14:46:51
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('FactoryType').value = "";
    fm.all('FactoryCode').value = "";
    fm.all('FactoryName').value = "";
    fm.all('Remark').value = "";
  }
  catch(ex) {
    alert("在LMFactoryCodeToTypeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLMFactoryCodeToTypeGrid();  
  }
  catch(re) {
    alert("LMFactoryCodeToTypeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LMFactoryCodeToTypeGrid;
function initLMFactoryCodeToTypeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		
    iArray[0][1]="30px";         		
    iArray[0][3]=0; 
    
    iArray[1]=new Array();
    iArray[1][0]="要素类型 ";         		
    iArray[1][1]="30px";         		
    iArray[1][3]=0;   
    
    iArray[2]=new Array();
    iArray[2][0]="计算编码";         		
    iArray[2][1]="30px";         		
    iArray[2][3]=0;   
    
    iArray[3]=new Array();
    iArray[3][0]="计算名称";         		
    iArray[3][1]="30px";         		
    iArray[3][3]=0;   
    
    iArray[4]=new Array();
    iArray[4][0]="备注";         		
    iArray[4][1]="30px";         		
    iArray[4][3]=0;                      		

    LMFactoryCodeToTypeGrid = new MulLineEnter( "fm" , "LMFactoryCodeToTypeGrid" ); 
    //这些属性必须在loadMulLine前
    LMFactoryCodeToTypeGrid.mulLineCount = 0;   
    LMFactoryCodeToTypeGrid.displayTitle = 1;
    LMFactoryCodeToTypeGrid.hiddenPlus = 1;
    LMFactoryCodeToTypeGrid.hiddenSubtraction = 1;
    LMFactoryCodeToTypeGrid.canSel = 1;
    LMFactoryCodeToTypeGrid.canChk = 0;

    LMFactoryCodeToTypeGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
