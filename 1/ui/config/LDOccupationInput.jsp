<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-25 18:26:51
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDOccupationInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDOccupationInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDOccupationSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 职业分类表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDOccupation1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      职业代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OccupationCode elementtype=nacessary verify="职业代码|notnull&len<=10&NUM">
    </TD>
    <TD  class= title>
      职业名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OccupationName verify="职业名称|len<=120">
    </TD>
    <TD  class= title>
      职业类别
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OccupationType verify="职业类别|len<=10">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      行业代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WorkType verify="行业代码|len<=10&NUM">
    </TD>
    <TD  class= title>
      行业名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WorkName verify="行业名称|len<=60">
    </TD>
    <TD  class= title>
      具体说明
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Remark verify="具体说明|len<=500">
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <div style="display: 'none'">
    <TR  class= common>
	  	<TD  class= title>
	      医疗险加费比例
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MedRate verify="医疗险价费比例|num&len<=10">
	    </TD>
	    <TD  class= title>
	      意外风险
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=SuddRisk verify="以外风险|len<=30">
	    </TD>
	    <TD  class= title>
	      重疾风险
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=DiseaRisk verify="工种代码|len<=30">
	    </TD>
	    <TD  class= title>
	      住院风险
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=HosipRisk verify="工种代码|len<=30">
	    </TD>
	  </TR>
    </div>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
