<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：保全项目添加无扫描件不能结案配置
//创建日期：2017-11-20 
//创建人  ：LZJ
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>   
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="AddScanConfInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AddScanConfInit.jsp"%>
  <title>保全项目保全结案扫描件配置</title>
<Script>
var comCode = <%=tG.ComCode%>
</Script>
</head>
<body  onload="initForm()">
  <form action="AddScanConfSave.jsp" method=post name=fm target="fraSubmit" >
  	<div>
	  	<table class= common border=0 width=100%>
	    	<tr>
				<td class= titleImg align= center>
					请录入查询条件
				</td>
			</tr>
		</table>
  	</div>
	<div>
	    <table  class= common align=center>      
	        <TR>
	  		  <TD  class= title> 保单类型 </TD>
	          <TD  class= input colspan="3">
	            <Input class= "codeno" name=contType  CodeData="0|^0|全部^1|个单^2|团单"  ondblclick="return showCodeListEx('contType',[this,ContType],[0,1]);" onkeyup="return showCodeListKey('contType',[this,ContType],[0,1]);" ><Input class=codename style="width:80" readonly name=ContType id=ContType />
	          </TD>
	  		  <TD  class= title> 保全项目类型 </TD>
	          <TD  class= input colspan="3">
	            <Input class= "codeno" name=edorType   ondblclick="return showCodeList('conttypeedorcode',[this,EdorName],[0,1],null,fm.contType.value,1);" onkeyup="return showCodeListKey('conttypeedorcode',[this,EdorName],[0,1],null,fm.contType.value,1);" ><Input class=codename style="width:160" readonly name=EdorName id=EdorName />
	          </TD>
	          <TD  class= title>扫描件配置状态</TD>
	          <TD  class= input colspan="3">
	            <Input class= "codeno" name=state CodeData="0|^0|全部^1|已配置^2|未配置"  ondblclick="return showCodeListEx('state',[this,stateName],[0,1]);" onkeyup="return showCodeListKey('state',[this,stateName],[0,1]);" ><Input class=codename style="width:80" readonly name=stateName id=stateName />
	          </TD>
			</TR>
	    </table>
	</div>
  	<div>
	  	<table class= common border=0 width=100%>
	    	<tr>
				<td class= titleImg align= center>
					保全项目信息
				</td>
			</tr>
		</table>
  	</div>
	<div id= "divLMEdorItemGrid" style= "display: ''">
		<table class= common>
       		<tr class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLMEdorItemGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
	</div>
	<Div id= "divPage" align=center style= "display: none ">
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">      
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">       
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">       
	</Div>
	</Div>
	<div align=right>
		<INPUT VALUE="重置" class=cssButton TYPE=button onclick="resetQuery();"> 
		<INPUT VALUE="查询" class=cssButton TYPE=button onclick="queryLMEdorItem();"> 
	    <INPUT VALUE="添加配置" class=cssButton TYPE=button onclick="setConfig(1);"> 
	   	<INPUT VALUE="删除配置" class=cssButton TYPE=button onclick="setConfig(0);"> 
	</div>
	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="IsNeedScanning" name="IsNeedScanning">
	<input type=hidden id="appObj" name="appObj">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

