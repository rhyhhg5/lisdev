<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();
    initWrapGrid();
    queryWrapGrid();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
}

var WrapGrid;
function initWrapGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         			//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="套餐编码";         	  		//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="套餐名称";         	
    iArray[2][1]="120px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;   
     
    iArray[3]=new Array();
    iArray[3][0]="配置日期";         	
    iArray[3][1]="120px";            	
    iArray[3][2]=200;            		 
    iArray[3][3]=0;    
    
    WrapGrid = new MulLineEnter("fm","WrapGrid"); 
    //设置Grid属性
    WrapGrid.mulLineCount = 10;
    WrapGrid.displayTitle = 1;
    WrapGrid.locked = 1;
    WrapGrid.canSel = 1;
    WrapGrid.canChk = 0;
    WrapGrid.hiddenSubtraction = 1;
    WrapGrid.hiddenPlus = 1;
    WrapGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
