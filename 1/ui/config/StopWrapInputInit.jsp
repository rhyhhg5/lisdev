<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();
    initWrapGrid();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
}

var WrapGrid;
function initWrapGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         			//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="套餐编码";         	  		//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="套餐名称";         	
    iArray[2][1]="120px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;    
    
    iArray[3]=new Array();
    iArray[3][0]="起售日期";         	  		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="AveragePrem";
    
    iArray[4]=new Array();
    iArray[4][0]="停售日期";         	  		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="状态";      				//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0; 
	
	iArray[6]=new Array();
    iArray[6][0]="状态编码";      				//列名
    iArray[6][1]="60px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=3; 
    
    WrapGrid = new MulLineEnter("fm","WrapGrid"); 
    //设置Grid属性
    WrapGrid.mulLineCount = 0;
    WrapGrid.displayTitle = 1;
    WrapGrid.locked = 1;
    WrapGrid.canSel = 0;
    WrapGrid.canChk = 1;
    WrapGrid.hiddenSubtraction = 1;
    WrapGrid.hiddenPlus = 1;
    WrapGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
