
<%@page import="com.sinosoft.lis.config.E_RenewalConfUI"%><%
//程序名称：E_RenewalConfSave.jsp
//程序功能：
//创建日期：2017-12-11
//创建人  ：lzj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
	//接收信息，并作校验处理。
	//输入参数

	CErrors tError = null;
	//后面要执行的动作：添加，修改
	
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	String Result= "";
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");
	System.out.println(transact);
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	E_RenewalConfUI renewalConfUI = new E_RenewalConfUI();	
	LDCodeSchema codeSchema = new LDCodeSchema();
	codeSchema.setCodeType("DSXQ");
	codeSchema.setCode(request.getParameter("code"));
	codeSchema.setCodeName(request.getParameter("codeName"));
	codeSchema.setCodeAlias("电商保单续期配置");
	codeSchema.setComCode(request.getParameter("comCode"));
	codeSchema.setOtherSign(request.getParameter("otherSign"));
	try
  	{
		// 准备传输数据 VData	  
		VData tVData = new VData();  
		tVData.addElement(tG);
		tVData.addElement(codeSchema);
		if(renewalConfUI.submitData(tVData,transact)) 
		 transact="QUERY||MAIN";	
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}			
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr==""){
		tError = renewalConfUI.mErrors;
	  	if (!tError.needDealError()){                          
			Content = " 保存成功";
	  		FlagStr = "Success";
	  		if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL")){
			  	if (renewalConfUI.getResult()!=null&&renewalConfUI.getResult().size()>0){
			  		LDCodeSchema ldCodeSchema = (LDCodeSchema)renewalConfUI.getResult().get(0);
			  		Result = ldCodeSchema.getCode();
			  		if (Result==null){
			  			FlagStr = "Fail";
			  			Content = "提交失败!!";
			  		}
			  	}
	  		}
	  }else{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	  }
	}
  	//添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
