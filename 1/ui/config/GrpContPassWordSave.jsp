<%
//程序名称：LCCustomerRCConfirmSave.jsp
//程序功能：
//创建日期：2009-08-04
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  GrpContPassWordInputUI tGrpContPassWordInputUI = new GrpContPassWordInputUI();
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("Operate");
  tOperate=tOperate.trim();
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  String tRadio[] = request.getParameterValues("InpGrpContInfoGridSel");   
  String tGrpContNo[] = request.getParameterValues("GrpContInfoGrid1");   
  String tPassWord[] = request.getParameterValues("GrpContInfoGrid4");   

  System.out.println(tRadio.length);
  for (int index=0; index< tRadio.length;index++) {
    if(tRadio[index].equals("1")) {
       tLCGrpContSchema.setGrpContNo(tGrpContNo[index]);
       tLCGrpContSchema.setPassword(tPassWord[index]);
    }
  }

 
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLCGrpContSchema);
	tVData.addElement(tG);
  try
  {
    tGrpContPassWordInputUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tGrpContPassWordInputUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	//Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
%>     
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

