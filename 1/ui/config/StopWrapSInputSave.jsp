<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.config.*"%>

<%
	//输入参数
	String tWrapCode = request.getParameter("WrapCode");
	String tWrapName = request.getParameter("WrapCodeName");
	String tSaleChnl = request.getParameter("SaleChnl");
	String szNo[] = request.getParameterValues("WrapGridNo");
	String tChk[] = request.getParameterValues("InpWrapGridChk");
	String tWrapCodes[] = request.getParameterValues("WrapGrid1");
	String tSaleChnls[] = request.getParameterValues("WrapGrid3");
	String tStateFlag[] = request.getParameterValues("WrapGrid6");
	LDCode1Set tLDCode1Set = new LDCode1Set();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mProjectNo = request.getParameter("ProjectNo");

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	String strManageCom = tG.ComCode;

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	System.out.println(transact);

	if ("Add".equals(transact)) {
		LDCode1Schema tLDCode1Schema = new LDCode1Schema();
		tLDCode1Schema.setCodeType("stopwrap");
		tLDCode1Schema.setCode(tWrapCode);
		tLDCode1Schema.setCode1(tSaleChnl);
		tLDCode1Schema.setCodeName(tWrapName);

		tLDCode1Set.add(tLDCode1Schema);
	} else if ("Stop".equals(transact)){
		for (int nIndex = 0; nIndex < szNo.length; nIndex++) {
			if ("1".equals(tChk[nIndex])) {
				LDCode1Schema tLDCode1Schema = new LDCode1Schema();
				tLDCode1Schema.setCodeType("stopwrapb");
				tLDCode1Schema.setCode(tWrapCodes[nIndex]);
				tLDCode1Schema.setCode1(tSaleChnls[nIndex]);
		
				tLDCode1Set.add(tLDCode1Schema);
			}
		}
	}else if ("QStop".equals(transact)){
		for (int nIndex = 0; nIndex < szNo.length; nIndex++) {
			if ("1".equals(tChk[nIndex])) {
				LDCode1Schema tLDCode1Schema = new LDCode1Schema();
				tLDCode1Schema.setCodeType("stopwrap");
				tLDCode1Schema.setCode(tWrapCodes[nIndex]);
				tLDCode1Schema.setCode1(tSaleChnls[nIndex]);
		
				tLDCode1Set.add(tLDCode1Schema);
			}
		}
	}

	// 准备向后台传输数据 VData
	VData tVData = new VData();
	FlagStr = "";
	tVData.add(tG);
	tVData.addElement(tLDCode1Set);
	StopWrapSUI tStopWrapSUI = new StopWrapSUI();
	try {
		tStopWrapSUI.submitData(tVData, transact);
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	if (!FlagStr.equals("Fail")) {
		tError = tStopWrapSUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Succ";
		} else {
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
