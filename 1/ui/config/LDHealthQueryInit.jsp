<%
//程序名称：LDHealthQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-25 15:21:34
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('SerialNo').value = "";
    fm.all('HealthCode').value = "";
    fm.all('HealthName').value = "";
    fm.all('StartMoney').value = "";
    fm.all('EndMoney').value = "";
    fm.all('StartAge').value = "";
    fm.all('EndAge').value = "";
    fm.all('Sex').value = "";
    fm.all('Note1').value = "";
    fm.all('Note2').value = "";
  }
  catch(ex) {
    alert("在LDHealthQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDHealthGrid();  
  }
  catch(re) {
    alert("LDHealthQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDHealthGrid;
function initLDHealthGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="流水号";   
	iArray[1][1]="0px";   
	iArray[1][2]=20;        
	iArray[1][3]=3;
	
	iArray[2]=new Array(); 
	iArray[2][0]="体检项目代码";   
	iArray[2][1]="60px";   
	iArray[2][2]=20;        
	iArray[2][3]=1;
	
	iArray[3]=new Array(); 
	iArray[3][0]="体检项目名称";   
	iArray[3][1]="150px";   
	iArray[3][2]=20;        
	iArray[3][3]=1;
	
	iArray[4]=new Array(); 
	iArray[4][0]="体检额度起始值";   
	iArray[4][1]="60px";   
	iArray[4][2]=20;        
	iArray[4][3]=1;
	
	iArray[5]=new Array(); 
	iArray[5][0]="体检额度终止值";   
	iArray[5][1]="60px";   
	iArray[5][2]=20;        
	iArray[5][3]=1;
	
	iArray[6]=new Array(); 
	iArray[6][0]="体检年龄起始值";   
	iArray[6][1]="60px";   
	iArray[6][2]=20;        
	iArray[6][3]=1;
	
	iArray[7]=new Array(); 
	iArray[7][0]="体检年龄终止值";   
	iArray[7][1]="60px";   
	iArray[7][2]=20;        
	iArray[7][3]=1;
    
    iArray[8]=new Array(); 
	iArray[8][0]="体检人性别";   
	iArray[8][1]="60px";   
	iArray[8][2]=20;        
	iArray[8][3]=1;
	
	iArray[9]=new Array(); 
	iArray[9][0]="备注";   
	iArray[9][1]="140px";   
	iArray[9][2]=20;        
	iArray[9][3]=1;
    
    LDHealthGrid = new MulLineEnter( "fm" , "LDHealthGrid" ); 
    //这些属性必须在loadMulLine前

    LDHealthGrid.mulLineCount = 3;   
    LDHealthGrid.displayTitle = 1;
    LDHealthGrid.hiddenPlus = 1;
    LDHealthGrid.hiddenSubtraction = 1;
    LDHealthGrid.canSel = 1;
    LDHealthGrid.canChk = 0;
    //LDHealthGrid.selBoxEventFuncName = "showOne";

    LDHealthGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDHealthGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
