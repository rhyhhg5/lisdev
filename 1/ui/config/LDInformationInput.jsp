<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-25 11:56:54
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDInformationInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDInformationInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDInformationSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDInformation1);">
    		</td>
    		 <td class= titleImg>
        		 补充资料描述表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDInformation1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    
    <TD  class= title>
      资料分类
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InformationKind elementtype=nacessary verify="资料分类|notnull&len<=1">
    </TD>
    <TD  class= title>
      资料代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InformationCode elementtype=nacessary verify="资料代码|notnull&len<=10">
    </TD>
  </TR>
  <TR  class= common>

    <TD  class= title>
      资料名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InformationName verify="资料名称|len<=300">
    </TD>
    <TD  class= title>
      资料额度起始值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StartMoney verify="资料额度起始值|num&len<=20">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      资料额度终止值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndMoney verify="资料额度终止值|num&len<=20">
    </TD>
    <TD  class= title>
      资料年龄起始值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StartAge verify="资料年龄起始值|num&len<=20">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      资料年龄终止值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndAge verify="资料年龄终止值|num&len<=20">
    </TD>
    <TD  class= title>
      资料人性别
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Sex verify="资料人性别|len<=1">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      资料说明
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Remark verify="资料说明|len<=300">
    </TD>
    <TD  class= title>
      备注2
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Note2 verify="备注2|len<=60">
    </TD>
  </TR>

</table>
    </Div>
    <div id="div1" style="display: 'none'">
    	<table>
    		<TD  class= title>
	      流水号
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=SerialNo >
	    </TD>
	    </table>
	  </div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
