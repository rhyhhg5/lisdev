<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-25 12:57:38
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LDImpartInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDImpartInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDImpartSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDImpart1);">
    		</td>
    		 <td class= titleImg>
        		 告知信息表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDImpart1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      告知版别
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=ImpartVer  verify="告别版别|notnull&len<6"  verify="核保理赔参考标志|code:ImpartVer&len<2" ondblclick="return showCodeList('ImpartVer',[this,ImpartVerName],[0,1]);" onkeyup="return showCodeListKey('ImpartVer',[this,ImpartVerName],[0,1]);"><input name = "ImpartVerName" class = "codename" elementtype=nacessary >
    </TD>
    <TD  class= title>
      告知编码
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=ImpartCode   verify="告别版号|notnull&len<6"  verify="核保理赔参考标志|code:ImpartCode&len<2" ondblclick="return showCodeList('ImpartCode',[this,ImpartCodeName],[0,1]);" onkeyup="return showCodeListKey('ImpartCode',[this,ImpartCodeName],[0,1]);" ><input name = "ImpartCodeName" class = "codename" elementtype=nacessary >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      告知内容
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ImpartContent  elementtype=nacessary  verify="告别内容|notnull&len<1001">
    </TD>
    <TD  class= title>
      告知参数展现模版
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ImpartParamModle elementtype=nacessary  verify="告知参数展现模版|notnull&len<31">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      核保理赔参考标志
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=UWClaimFlg  verify="核保理赔参考标志|code:yesno&NOTNULL" ondblclick="return showCodeList('yesno',[this,UWClaimFlgName],[0,1]);" onkeyup="return showCodeListKey('yesno',[this,UWClaimFlgName],[0,1]);"><input name = "UWClaimFlgName" class = "codename" >
    </TD>
    <TD  class= title>
      打印标记
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=PrtFlag verify="打印标记|code:yesno&NOTNULL" ondblclick="return showCodeList('yesno',[this,PrtFlagName],[0,1]);" onkeyup="return showCodeListKey('yesno',[this,PrtFlagName],[0,1]);"><input name ="PrtFlagName" class = "codename" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      备注
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Remark  verify="备注|len<200">
    </TD>
      
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
