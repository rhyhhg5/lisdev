<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：AddScanConfInput.jsp
//程序功能：
//创建日期：2017-11-20 
//创建人  ：lzj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initLMEdorItemGrid(){
	var iArray = new Array();
	try {
		
	  	iArray[0]=new Array();
	  	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  	iArray[0][1]="30px";            		//列宽
	  	iArray[0][2]=10;            			//列最大值
	  	iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

	  	iArray[1]=new Array();
	  	iArray[1][0]="保全项目代码";
	  	iArray[1][1]="60px";
	  	iArray[1][2]=100;
	  	iArray[1][3]=0;
	  	iArray[1][21]="edorCode";

	  	iArray[2]=new Array();
	  	iArray[2][0]="保全项目名称";
	  	iArray[2][1]="100px";
	  	iArray[2][2]=100;
	  	iArray[2][3]=0;
	  	iArray[2][21]="edorName";

	  	iArray[3]=new Array();
	  	iArray[3][0]="保单类型";
	  	iArray[3][1]="60px";
	  	iArray[3][2]=100;
	  	iArray[3][3]=0;
	  	iArray[3][21]="appObjType";

	  	iArray[4]=new Array();
	  	iArray[4][0]="是否需要扫描件";
	  	iArray[4][1]="60px";
	  	iArray[4][2]=100;
	  	iArray[4][3]=0;
	  	iArray[4][21]="isNeedScanning";

	  	LMEdorItemGrid = new MulLineEnter( "fm" , "LMEdorItemGrid" ); 
		//这些属性必须在loadMulLine前
		LMEdorItemGrid.mulLineCount = 0;   
		LMEdorItemGrid.displayTitle = 1;
		LMEdorItemGrid.canSel=1;
		//LMEdorItemGrid.selBoxEventFuncName ="getLCContDetail" ;     //点击RadioBox时响应的JS函数
		LMEdorItemGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		LMEdorItemGrid.hiddenSubtraction=1;
		//LMEdorItemGrid.selBoxEventFuncName = "invert";
		LMEdorItemGrid.loadMulLine(iArray); 
			
	} catch (e) {
	}
}
function initInpBox()
{ 
  try
  {                                   
    fm.contType.value = '0';
  }
  catch(ex)
  {
    alert("在AddScanConfInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                       
function initForm()
{
  try{
	  initElementtype();
	  initInpBox();
	  initLMEdorItemGrid();
	  showAllCodeName();
  }
  catch(ex)
  {
    alert("AddScanConfInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
