<%
//程序名称：LDUWUserInput.jsp
//程序功能：
//创建日期：2005-01-24 18:15:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('UserCode').value = "";
    fm.all('UWType').value = "";
    fm.all('UWPopedom').value = "";
    fm.all('UWRate').value = "";
    fm.all('UpUserCode').value = "";
    fm.all('PopUWFlag').value = "";
    fm.all('AgentUserCode').value = "";
    //fm.all('UWRate').value = "";

  }
  catch(ex)
  {
    alert("在LDUWUserInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LDUWUserInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initJuniorGrid();
  }
  catch(re)
  {
    alert("LDUWUserInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//初始化项目列表
function initJuniorGrid()
{                               
	var iArray = new Array();
	try
	{
  	iArray[0]=new Array();
  	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  	iArray[0][1]="30px";            		//列宽
  	iArray[0][2]=10;            			//列最大值
  	iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
  	iArray[1]=new Array();
  	iArray[1][0]="下级核保师编码";
  	iArray[1][1]="50px";
  	iArray[1][2]=100;
  	iArray[1][3]=0;
  	iArray[1][21]="UserCode";
  	
  	iArray[2]=new Array();
  	iArray[2][0]="下级核保师姓名";
  	iArray[2][1]="50px";
  	iArray[2][2]=100;
  	iArray[2][3]=0;
  	iArray[2][21]= "UserName";
  	
  	iArray[3]=new Array();
  	iArray[3][0]="下级核保师类别";
  	iArray[3][1]="50px";
  	iArray[3][2]=100;
  	iArray[3][3]=0;
  	iArray[3][21]="UWTypeName";
  	
  	iArray[4]=new Array();
  	iArray[4][0]="下级核保师级别";
  	iArray[4][1]="50px";
  	iArray[4][2]=100;
  	iArray[4][3]=2;
  	iArray[4][3]=0;
  	iArray[4][21]="UWPopedomName";
  	
  	iArray[5]=new Array();
  	iArray[5][0]="首席核保标志";
  	iArray[5][1]="50px";
  	iArray[5][2]=100;
  	iArray[5][3]=2;
  	iArray[5][3]=0;
  	iArray[5][21]="PopUWFlagName";
	  	
		JuniorGrid = new MulLineEnter( "fm" , "JuniorGrid" ); 
		//这些属性必须在loadMulLine前
		JuniorGrid.mulLineCount = 0;   
		JuniorGrid.displayTitle = 1;
		JuniorGrid.canSel =0;
		JuniorGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		JuniorGrid.hiddenSubtraction=1;
		JuniorGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
}

</script>