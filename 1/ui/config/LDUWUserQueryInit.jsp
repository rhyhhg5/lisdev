<%
//程序名称：LDUWUserQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-24 18:15:01
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('UserCode').value = "";
    fm.all('UWType').value = "";
    fm.all('UWPopedom').value = "";
    fm.all('UpUserCode').value = "";
    fm.all('PopUWFlag').value = "";
    fm.all('AgentUserCode').value = "";
  }
  catch(ex) {
    alert("在LDUWUserQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDUWUserGrid();  
  }
  catch(re) {
    alert("LDUWUserQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDUWUserGrid;
function initLDUWUserGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
	iArray[1][0]="核保师编码";  
	iArray[1][1]="80px";  
	iArray[1][3]=0;
	
	iArray[2]=new Array();
	iArray[2][0]="核保师类型";  
	iArray[2][1]="80px";  
	iArray[2][3]=0;       

	iArray[3]=new Array();
	iArray[3][0]="核保师级别";  
	iArray[3][1]="80px";  
	iArray[3][3]=0;       

	iArray[4]=new Array();
	iArray[4][0]="上级核保师";  
	iArray[4][1]="80px";  
	iArray[4][3]=0;       

	iArray[5]=new Array();
	iArray[5][0]="首席核保标志";  
	iArray[5][1]="120px";  
	iArray[5][3]=0;       

	iArray[6]=new Array();
	iArray[6][0]="核保委托人";  
	iArray[6][1]="80px";  
	iArray[6][3]=0;     
	
	iArray[7]=new Array();
	iArray[7][0]="抽检比率";  
	iArray[7][1]="50px";  
	iArray[7][3]=0;       
       

    
    LDUWUserGrid = new MulLineEnter( "fm" , "LDUWUserGrid" ); 
    //这些属性必须在loadMulLine前

    LDUWUserGrid.mulLineCount = 0;   
    LDUWUserGrid.displayTitle = 1;
    LDUWUserGrid.hiddenPlus = 1;
    LDUWUserGrid.hiddenSubtraction = 1;
    LDUWUserGrid.canSel = 1;
    LDUWUserGrid.canChk = 0;
    //LDUWUserGrid.selBoxEventFuncName = "showOne";

	LDUWUserGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDUWUserGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>