<%
//程序名称：LDUWAddFeeQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-20 09:41:35
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ICDCode').value = "";
    fm.all('ICDName').value = "";
    fm.all('DiseasDegree').value = "";
    fm.all('Sex').value = "";
    fm.all('Age').value = "";
    fm.all('RiskKind').value = "";
    fm.all('OPSFlag').value = "";
    fm.all('OPSNo').value = "";
   /* fm.all('DiseasBTypeCode').value = "";
    fm.all('DiseasType').value = "";
    fm.all('DiseasSTypeCode').value = "";
    fm.all('DiseasSType').value = "";
    fm.all('RiskGrade').value = "";
    fm.all('OPSName').value = "";
    fm.all('Relapse').value = "";*/
  }
  catch(ex) {
    alert("在LDUWAddFeeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDUWAddFeeGrid();  
  }
  catch(re) {
    alert("LDUWAddFeeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDUWAddFeeGrid;
function initLDUWAddFeeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
      
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="主要疾病类目编码 ";         		//列名
      iArray[1][1]="90px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="主要疾病类目";         		//列名
      iArray[2][1]="90px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="疾病严重程度";         		//列名
      iArray[3][1]="90px";            		//列宽
      iArray[3][2]=200;            			//列最大值]
      /*
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][10] = "Sex";
      iArray[3][11] = "0|^0|男^1|女^2|不详";
      iArray[3][12] = "3";
      iArray[3][19] = "0";		
      */

      iArray[4]=new Array();
      iArray[4][0]="性别";         		//列名
      iArray[4][1]="90px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="年龄";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;
       
      iArray[6]=new Array();
      iArray[6][0]="险种分类";              	        //是否引用代码:null||""为不引用
      iArray[6][1]="90px";              	                //引用代码对应第几列，'|'为分割符
      iArray[6][2]="90";
      iArray[6][3]=0;
     
      

      iArray[7]=new Array();
      iArray[7][0]="是否做手术";         		//列名
      iArray[7][1]="90px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="手术编码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[8][1]="90px";            		//列宽
      iArray[8][2]=10;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      LDUWAddFeeGrid = new MulLineEnter( "fm" , "LDUWAddFeeGrid" ); 
      //这些属性必须在loadMulLine前
      LDUWAddFeeGrid.mulLineCount = 10;   
      LDUWAddFeeGrid.displayTitle = 1;
      LDUWAddFeeGrid.locked = 1;
      LDUWAddFeeGrid.canSel = 1;
      LDUWAddFeeGrid.hiddenPlus=1;
      LDUWAddFeeGrid.hiddenSubtraction=1;
      LDUWAddFeeGrid.loadMulLine(iArray);  
      
      

      }
      catch(ex)
      {
        alert(ex);
      }
/*
    LDUWAddFeeGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDUWAddFeeGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
  */
}
</script>
