<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2016-04-22 16:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
		String tProjectNo = request.getParameter("ProjectNo");
		String transact = request.getParameter("transact");
		String tLookFlag = request.getParameter("LookFlag"); 
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="IndiDueFeeWrapInput.js"></script>
		<%@include file="IndiDueFeeWrapInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>"; 
  		var tProjectNo = "<%=tProjectNo%>";
		var transact = "<%=transact%>";
		var tLookFlag = "<%=tLookFlag%>";
  		</script>
	</head>
<body onload="initForm();">
		<form action="IndiDueFeeWrapSave.jsp" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td class=titleImg>
						套餐查询条件
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						套餐编码
					</TD>
					<TD class=input>
						<input class="codeNo" name="RiskWrapCode" ondblClick=" showCodeList('wrapcode1',[this,WrapCodeName], [0,1],null,null,null,1);" onkeyup=" showCodeList('wrapcode1',[this,WrapCodeName], [0,1]),null,null,null,1;"><Input class="codeName" name="WrapCodeName" readonly>
					</TD>
				</TR>
			</table>
			<table>
				<td class=button>
					<input type="button" class=cssButton value="查 询 " id = "AddID" name="AddID" onclick="queryWrap()">
				</td>
			</table>
			<table class="common" align='center'>
		     <tr class="common">
             <td class="title">套餐编码</td>
             <td class="input">
              <input class="readonly" readonly name="WrapCode2" value="">
             </td>
             <td class="title">套餐名称</td>
             <td class="input">
              <input class="readonly" readonly name="WrapName2" value="" >
            </td>
			</table>
			 <table>
				<input type=hidden id="fmtransact" name="fmtransact">
				<td class=button>
					<input type="button" class=cssButton value="添加" id = "AddID" name="AddID" onclick="submitForm()">&nbsp;&nbsp;&nbsp;
				</td>
				<td class=button>
					<input type="button" class=cssButton value="删除" id = "UpdateID" name="UpdateID" onclick="deleteClick()">
				</td>
				<INPUT TYPE=hidden name=tWrapCode value="">
 				<INPUT TYPE=hidden name=tWrapCodeName value="">
			</table>

			<table>
				<tr>
					<td class=titleImg>
						配置套餐信息
					</td>
				</tr>
			</table>

			<Div id="divWrapGrid" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanWrapGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">
				<table>
					<tr align=center>
						<td class=button width="10%">
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</Div>


			<table>
				<br/>
				<tr class=common>
					<td>
					<font color = red  size = 2>
						使用说明：<br/>
						1、【查询】：根据套餐编码，对套餐进行查询。<br/>
						2、【配置套餐】：选中查询出的套餐，对该套餐进行配置处理。<br/>
					</td>
				</tr>
			</table>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
