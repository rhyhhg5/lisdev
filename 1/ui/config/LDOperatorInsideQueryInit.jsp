<%
//程序名称：LDOperatorInsideQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-08-23 14:52:31
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('UserCode').value = "";
    fm.all('UserType').value = "";
    fm.all('UserName').value = "";
    fm.all('Sex').value = "";
    fm.all('Birthday').value = "";
    fm.all('Phone').value = "";
    fm.all('Mobile').value = "";
    fm.all('ManageCom').value = "";
    fm.all('AgentCom').value = "";
  }
  catch(ex) {
    alert("在LDOperatorInsideQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDOperatorInsideGrid();  
  }
  catch(re) {
    alert("LDOperatorInsideQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDOperatorInsideGrid;
function initLDOperatorInsideGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="用户编码";         		//列名
    iArray[1][1]="30px";         		//列名
    iArray[1][3]=0;         		//列名
    iArray[1][4]="station";         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="用户类型";         		//列名
    iArray[2][1]="30px";         		//列名
    iArray[2][3]=0;         		//列名
    iArray[2][4]="station";         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="姓名";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=0;         		//列名
    iArray[3][4]="station";         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="性别";         		//列名
    iArray[4][1]="30px";         		//列名
    iArray[4][3]=0;         		//列名
    iArray[4][4]="station";         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="联系电话";         		//列名
    iArray[5][1]="30px";         		//列名
    iArray[5][3]=0;         		//列名
    iArray[5][4]="station";         		//列名
    
    iArray[6]=new Array();
    iArray[6][0]="手机";         		//列名
    iArray[6][1]="30px";         		//列名
    iArray[6][3]=0;         		//列名
    iArray[6][4]="station";         		//列名
    
    iArray[7]=new Array();
    iArray[7][0]="代理机构";         		//列名
    iArray[7][1]="30px";         		//列名
    iArray[7][3]=0;         		//列名
    iArray[7][4]="station";         		//列名
    
    iArray[8]=new Array();
    iArray[8][0]="管理机构";         		//列名
    iArray[8][1]="30px";         		//列名
    iArray[8][3]=0;         		//列名
    iArray[8][4]="station";         		//列名
    
    iArray[9]=new Array();
    iArray[9][0]="生日";         		//列名
    iArray[9][1]="30px";         		//列名
    iArray[9][3]=0;         		    //列名
    iArray[9][4]="station";         //列名

    LDOperatorInsideGrid = new MulLineEnter( "fm" , "LDOperatorInsideGrid" ); 
    //这些属性必须在loadMulLine前

    LDOperatorInsideGrid.mulLineCount = 0;   
    LDOperatorInsideGrid.displayTitle = 1;
    LDOperatorInsideGrid.hiddenPlus = 1;
    LDOperatorInsideGrid.hiddenSubtraction = 1;
    LDOperatorInsideGrid.canSel = 1;
    LDOperatorInsideGrid.canChk = 0;
    LDOperatorInsideGrid.selBoxEventFuncName = "showOne";

    LDOperatorInsideGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDOperatorInsideGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
