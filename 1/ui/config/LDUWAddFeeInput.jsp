<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-20 09:41:35
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDUWAddFeeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDUWAddFeeInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDUWAddFeeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDUWAddFee1);">
    		</td>
    		 <td class= titleImg>
        		 核保加费基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDUWAddFee1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      主要疾病类目编码
    </TD>   
    <TD  class= input>
    <Input class= 'codeno' name=ICDCode  verify="主要疾病类目编码|notnull&len<20" ondblclick="getHospitCode();return showCodeListEx('GetHospitCode',[this,ICDCodeName],[0,1],'', '', '', true);" onkeyup="getHospitCode();return showCodeListKeyEx('GetHospitCode',[this,ICDCodeName],[0,1],'', '', '', true);"><input  class="codename" name="ICDCodeName" elementtype=nacessary > 	
  </TD>
    <TD  class= title>
      主要疾病类目
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICDName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      疾病严重程度
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasDegree elementtype=nacessary verify="疾病严重程度|notnull">
    </TD>
    <TD  class= title>
      性别
    </TD>
    <TD  class= input>
      <Input class="codeno" name=Sex  verify="性别|code:Sex&NOTNULL" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" ><input  class="codename" name="SexName" elementtype=nacessary > 	
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      年龄
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Age elementtype=nacessary verify="年龄|num&notnull" >
    </TD>
    
    <TD  class= title>
      险种分类
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskKind elementtype=nacessary verify="险种分类|notnull">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否做手术
    </TD>
    <TD  class= input>
      <Input class="codeno" name=OPSFlag  verify="是否做手术|code:yesno&NOTNULL" ondblclick="return showCodeList('yesno',[this,OPSFlagName],[0,1]);" onkeyup="return showCodeListKey('yesno',[this,OPSFlagName],[0,1]);" ><input  class="codename" name="OPSFlagName" elementtype=nacessary > 	
    </TD>
    <TD  class= title>
      手术编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OPSNo elementtype=nacessary verify="手术编码|notnull">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      疾病大分类编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasBTypeCode >
    </TD>
    <TD  class= title>
      疾病大分类名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      疾病小分类编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasSTypeCode >
    </TD>
    <TD  class= title>
      疾病小分类名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasSType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      风险等级
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskGrade >
    </TD>
    <TD  class= title>
      手术名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OPSName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      复发期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Relapse >
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
