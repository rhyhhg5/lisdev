<%
//程序名称：LDUWAddFeeInput.jsp
//程序功能：功能描述
//创建日期：2005-01-20 09:41:35
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LDUWAddFeeQueryInput.js"></SCRIPT> 
  <%@include file="LDUWAddFeeQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();initElementtype();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLDUWAddFeeGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      主要疾病类目编码
    </TD>
    
    <TD  class= input>
    <Input class= 'code' name=ICDCode  verify="主要疾病类目编码|notnull&len<20" ondblclick="getHospitCode();return showCodeListEx('GetHospitCode',[this],[0],'', '', '', true);" onkeyup="getHospitCode();return showCodeListKeyEx('GetHospitCode',[this],[0],'', '', '', true);">
  </TD>
    <TD  class= title>
      主要疾病类目
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICDName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      疾病严重程度
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasDegree verify="疾病严重程度|notnull">
    </TD>
    <TD  class= title>
      性别
    </TD>
    <TD  class= input>
      <Input class="code" name=Sex  verify="性别|code:Sex&NOTNULL" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      年龄
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Age  verify="年龄|num&notnull" >
    </TD>
    
    <TD  class= title>
      险种分类
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskKind  verify="险种分类|notnull">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      是否做手术
    </TD>
    <TD  class= input>
      <Input class="code" name=OPSFlag verify="是否做手术|code:OPSFlag&NOTNULL" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
    </TD>
    <TD  class= title>
      手术编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OPSNo  verify="手术编码|notnull">
    </TD>
  </TR>

 
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title  >
      <table>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
          </table>
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDUWAddFee1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLDUWAddFee1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLDUWAddFeeGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
 <table  align = center>
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </table>
 
  
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
