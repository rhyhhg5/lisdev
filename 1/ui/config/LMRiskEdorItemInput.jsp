<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-26 10:16:19
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMRiskEdorItemInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LMRiskEdorItemInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LMRiskEdorItemSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMRiskEdorItem1);">
    		</td>
    		 <td class= titleImg>
        		 保全险种项目配置表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLMRiskEdorItem1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      险种编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskCode elementtype=nacessary verify="险种编码|notnull&len<=6">
    </TD>
    <TD  class= title>
      险种名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskName elementtype=nacessary verify="险种名称|notnull&len<=90">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      保全项目编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EdorCode elementtype=nacessary verify="保全项目|notnull&len<=6">
    </TD>
    <TD  class= title>
      财务处理类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FinType verify="财务处理类型|len<=10">
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
