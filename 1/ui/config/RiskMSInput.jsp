<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2015-01-04 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
		String tProjectNo = request.getParameter("ProjectNo");
		String transact = request.getParameter("transact");
		String tLookFlag = request.getParameter("LookFlag"); 
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="RiskMSInput.js"></script>
		<%@include file="RiskMSInputInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>"; 
  		var tProjectNo = "<%=tProjectNo%>";
		var transact = "<%=transact%>";
		var tLookFlag = "<%=tLookFlag%>";
  		</script>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="RiskMSInputSave.jsp" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td class=titleImg>
						险种查询条件
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						险种编码
					</TD>
					<TD class=input>
						<input class="codeNo" name="RiskCode" ondblClick=" showCodeList('riskcodems',[this,RiskName], [0,1],null,null,null,1);" onkeyup=" showCodeList('riskcodems',[this,RiskName], [0,1]),null,null,null,1;"><Input class="codeName" name="RiskName" readonly>
					</TD>
					<TD class=title>
						状态
					</TD>
					<TD class=input>
						<Input class=codeno name="StateFlag" VALUE="0" CodeData="0|^0|全部^M|主险^S|附加险" ondblclick="return showCodeListEx('StateFlag',[this,StateFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StateFlag',[this,StateFlagName],[0,1],null,null,null,1);"><input class=codename name=StateFlagName readonly=true > 		     		
					</TD>
				</TR>
			</table>
			<table>
				<td class=button>
					<input type="button" class=cssButton value="查 询 " id = "AddID" name="AddID" onclick="queryRisk()">
				</td>
			</table>
			<table>
				<tr>
					<td class=titleImg>
						已配置险种信息
					</td>
				</tr>
			</table>

			<Div id="divRiskGrid" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanRiskGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">
				<table>
					<tr align=center>
						<td class=button width="10%">
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</Div>

			<table>
				<input type=hidden id="subriskflag" name="subriskflag">
				<td class=button>
					<input type="button" class=cssButton value="变更为主险" id = "AddID" name="AddID" onclick="updateRisk('M')">
				</td>
				<td class=button>
					<input type="button" class=cssButton value="变更为附加险" id = "UpdateID" name="UpdateID" onclick="updateRisk('S')">
				</td>
			</table>
			<table>
				<br/>
				<tr class=common>
					<td>
					<font color = red  size = 2>
						使用说明：<br/>
						1、【查询】：根据险种编码、险种当前状态，对已配置的险种进行查询。<br/>
						2、【变更为主险】：选择险种编码，变更该险种为主险。<br>
						3、【变更为附加险】：选择险种编码，变更该险种为附加险。<br/>
						4、对已配置的险种处理时，可对同一分页中相同状态的险种进行批量处理。<br/>
						5、不同分页中的险种不能同时处理。<br/>
					</td>
				</tr>
			</table>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
