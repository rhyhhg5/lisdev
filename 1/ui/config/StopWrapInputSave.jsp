<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.config.*"%>

<%
	//输入参数
	String szNo[] = request.getParameterValues("WrapGridNo");
	String tChk[] = request.getParameterValues("InpWrapGridChk");
	String tWrapCodes[] = request.getParameterValues("WrapGrid1");
	String tEndDates[] = request.getParameterValues("WrapGrid4");
	LDRiskWrapSet tLDRiskWrapSet = new LDRiskWrapSet();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mProjectNo = request.getParameter("ProjectNo");

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	String strManageCom = tG.ComCode;

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	System.out.println(transact);

	for (int nIndex = 0; nIndex < szNo.length; nIndex++) {
		if ("1".equals(tChk[nIndex])) {
			LDRiskWrapSchema tLDRiskWrapSchema = new LDRiskWrapSchema();
			tLDRiskWrapSchema.setRiskWrapCode(tWrapCodes[nIndex]);
			tLDRiskWrapSchema.setEndDate(tEndDates[nIndex]);
	
			tLDRiskWrapSet.add(tLDRiskWrapSchema);
		}
	}
	// 准备向后台传输数据 VData
	VData tVData = new VData();
	FlagStr = "";
	tVData.add(tG);
	tVData.addElement(tLDRiskWrapSet);
	StopWrapUI tStopWrapUI = new StopWrapUI();
	try {
		tStopWrapUI.submitData(tVData, transact);
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	if (!FlagStr.equals("Fail")) {
		tError = tStopWrapUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Succ";
		} else {
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
