<%
//程序名称：LMEdorItemQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-26 09:25:24
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('EdorCode').value = "";
    fm.all('EdorName').value = "";
    fm.all('AppObj').value = "";
    fm.all('DisplayFlag').value = "";
    fm.all('CalFlag').value = "";
    fm.all('NeedDetail').value = "";
    fm.all('GrpNeedList').value = "";
    fm.all('EdorPopedom').value = "";
  }
  catch(ex) {
    alert("在LMEdorItemQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLMEdorItemGrid();  
  }
  catch(re) {
    alert("LMEdorItemQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LMEdorItemGrid;
function initLMEdorItemGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    LMEdorItemGrid = new MulLineEnter( "fm" , "LMEdorItemGrid" ); 
    //这些属性必须在loadMulLine前

    iArray[1]=new Array(); 
	iArray[1][0]="保全项目编码";   
	iArray[1][1]="90px";   
	iArray[1][2]=20;        
	iArray[1][3]=0;
	
	iArray[2]=new Array(); 
	iArray[2][0]="保全项目名称";   
	iArray[2][1]="90px";   
	iArray[2][2]=20;        
	iArray[2][3]=0;
	
	iArray[3]=new Array(); 
	iArray[3][0]="申请对象类型";   
	iArray[3][1]="90px";   
	iArray[3][2]=20;        
	iArray[3][3]=0;
	
	iArray[4]=new Array(); 
	iArray[4][0]="是否需要重算";   
	iArray[4][1]="90px";   
	iArray[4][2]=20;        
	iArray[4][3]=0;
	
	iArray[5]=new Array(); 
	iArray[5][0]="保全权限";   
	iArray[5][1]="90px";   
	iArray[5][2]=20;        
	iArray[5][3]=0;
	
	    
    LMEdorItemGrid.mulLineCount = 3;   
    LMEdorItemGrid.displayTitle = 1;
    LMEdorItemGrid.hiddenPlus = 1;
    LMEdorItemGrid.hiddenSubtraction = 1;
    LMEdorItemGrid.canSel = 1;
    LMEdorItemGrid.canChk = 0;
    //LMEdorItemGrid.selBoxEventFuncName = "showOne";

    LMEdorItemGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LMEdorItemGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
