var turnPage = new turnPageClass();         //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 
var showInfo;

//提交，保存按钮对应操作
function submitForm()
{
	var Count=GrpContInfoGrid.mulLineCount;
  if(Count==0){
  	alert("没有保单信息!");
  	return false;
  }
  
  var SelNo=GrpContInfoGrid.getSelNo();
  
  if (SelNo<1){
    alert("请选择要修改密码的保单!");
    return false;
  }
  var tPassword = GrpContInfoGrid.getRowColDataByName(SelNo-1,"PassWord");
  if (tPassword==null||tPassword=="") {
      	alert("请输入密码！");
      	return false;
  }
  
  chkExp=/^\w{1,16}$/;
  if (!chkExp.test(tPassword)) {
  	alert("密码只能为数字和字母");
  	return false;
  }
  
  
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="./GrpContPassWordSave.jsp"
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在GrpContPassWordInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

// 事件响应函数，当用户改变CodeSelect的值时触发
function afterCodeSelect(cCodeName, Field)
{
	try {
	} catch(ex) {
		alert("在afterCodeSelect中发生异常");
	}
}


function searchGrpCont()
{
	if (fm.GrpContNo.value==""||fm.GrpContNo.value==null) {
	    	alert("请输入保单号码");
	    	return false;
	}
	
	strSQL = " select grpcontno,prtno,grpname,password from lcgrpcont where managecom like '"+fm.ManageCom.value+"%' "
	       + getWherePart('grpcontno','GrpContNo')
	       + "union all select grpcontno,prtno,grpname,password from lbgrpcont where managecom like '"+fm.ManageCom.value+"%' "
	       + getWherePart('grpcontno','GrpContNo');
	turnPage.queryModal(strSQL,GrpContInfoGrid);

	fm.SearchSQL.value= strSQL;
}

function printList()
{
	if (fm.SearchSQL.value=="") {
		alert("请先进行查询");
		return false;
	}

	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "LCCustomerRCPrint";
  fm.action = "./LCCustomerRCPrintSave.jsp";
	fm.submit();
  showInfo.close();
}

function closePage()
{
  top.close();
}