<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-26 08:49:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LDSysVarInput.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDSysVarInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype()" >
  <form action="./LDSysVarSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDSysVar1);">
    		</td>
    		 <td class= titleImg>
        		 系统变量表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDSysVar1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      系统变量名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SysVar elementtype=nacessary verify="系统变量名|notnull&len<21">
    </TD>
    <TD  class= title>
      系统变量类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SysVarType  verify="系统变量类型|len<4">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      系统变量值
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SysVarValue verify="系统变量值|len<101">
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
