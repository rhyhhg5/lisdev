<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-26 11:24:07
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDCode1Input.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDCode1InputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype()" >
  <form action="./LDCode1Save.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDCode11);">
    		</td>
    		 <td class= titleImg>
        		 公用代码表1基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDCode11" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      编码类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CodeType  verify="编码类型|notnull&len<21" >
    </TD>
    <TD  class= title>
      编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Code  verify="编码|notnull&len<21" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      子编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Code1  verify="子编码|notnull&len<21">
    </TD>
    <TD  class= title>
      编码名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CodeName verify="编码名称|len<121">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      编码别名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CodeAlias verify="编码别名|len<121">
    </TD>
    <TD  class= title>
      机构代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ComCode verify="机构代码|len<11">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      其它标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OtherSign verify="其它标志|len<11">
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
