<%
//程序名称：LMFactoryTypeQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-12-22 14:46:10
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('FactoryType').value = "";
    fm.all('FactoryTypeName').value = "";
    fm.all('Remark').value = "";
  }
  catch(ex) {
    alert("在LMFactoryTypeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLMFactoryTypeGrid();  
  }
  catch(re) {
    alert("LMFactoryTypeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LMFactoryTypeGrid;
function initLMFactoryTypeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		
    iArray[0][1]="30px";         		
    iArray[0][3]=0;
    
    iArray[1]=new Array();
    iArray[1][0]="要素类型";         		
    iArray[1][1]="30px";         		
    iArray[1][3]=0;   
    
    iArray[2]=new Array();
    iArray[2][0]="要素类型名称";         		
    iArray[2][1]="30px";         		
    iArray[2][3]=0; 
    
    iArray[3]=new Array();
    iArray[3][0]="备注";         		
    iArray[3][1]="30px";         		
    iArray[3][3]=0;                      		

    LMFactoryTypeGrid = new MulLineEnter( "fm" , "LMFactoryTypeGrid" ); 
    //这些属性必须在loadMulLine前

    LMFactoryTypeGrid.mulLineCount = 0;   
    LMFactoryTypeGrid.displayTitle = 1;
    LMFactoryTypeGrid.hiddenPlus = 1;
    LMFactoryTypeGrid.hiddenSubtraction = 1;
    LMFactoryTypeGrid.canSel = 1;
    LMFactoryTypeGrid.canChk = 0;

    LMFactoryTypeGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
