<%
//程序名称：LDContReceiveQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-08-25 10:42:49
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ManageCom').value = "";
    fm.all('ReceiveOperator').value = "";
    fm.all('Phone').value = "";
    fm.all('Mobile').value = "";
    fm.all('Address').value = "";
    fm.all('ZipCode').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LDContReceiveQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDContReceiveGrid();  
  }
  catch(re) {
    alert("LDContReceiveQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDContReceiveGrid;
function initLDContReceiveGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    LDContReceiveGrid = new MulLineEnter( "fm" , "LDContReceiveGrid" ); 
    //这些属性必须在loadMulLine前
/*
    LDContReceiveGrid.mulLineCount = 0;   
    LDContReceiveGrid.displayTitle = 1;
    LDContReceiveGrid.hiddenPlus = 1;
    LDContReceiveGrid.hiddenSubtraction = 1;
    LDContReceiveGrid.canSel = 1;
    LDContReceiveGrid.canChk = 0;
    LDContReceiveGrid.selBoxEventFuncName = "showOne";
*/
    LDContReceiveGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDContReceiveGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
