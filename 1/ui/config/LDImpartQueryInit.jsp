<%
//程序名称：LDImpartQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-25 17:05:21
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ImpartVer').value = "";
    fm.all('ImpartCode').value = "";
    fm.all('ImpartContent').value = "";
    fm.all('ImpartParamModle').value = "";
    fm.all('UWClaimFlg').value = "";
    fm.all('PrtFlag').value = "";
    fm.all('Remark').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LDImpartQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDImpartGrid();  
  }
  catch(re) {
    alert("LDImpartQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDImpartGrid;
function initLDImpartGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="告知版别";   
	iArray[1][1]="60px";   
	iArray[1][2]=20;        
	iArray[1][3]=0;
	
	iArray[2]=new Array(); 
	iArray[2][0]="告知编码";   
	iArray[2][1]="60px";   
	iArray[2][2]=20;        
	iArray[2][3]=0;
	
	iArray[3]=new Array(); 
	iArray[3][0]="告知内容";   
	iArray[3][1]="60px";   
	iArray[3][2]=20;        
	iArray[3][3]=0;
	
	iArray[4]=new Array(); 
	iArray[4][0]="告知参数展现模版";   
	iArray[4][1]="90px";   
	iArray[4][2]=20;        
	iArray[4][3]=0;
	
	iArray[5]=new Array(); 
	iArray[5][0]="核保理赔参考标志";   
	iArray[5][1]="90px";   
	iArray[5][2]=20;        
	iArray[5][3]=0;
	
	iArray[6]=new Array(); 
	iArray[6][0]="打印标记";   
	iArray[6][1]="60px";   
	iArray[6][2]=20;        
	iArray[6][3]=0;
	
	iArray[7]=new Array(); 
	iArray[7][0]="备注";   
	iArray[7][1]="100px";   
	iArray[7][2]=20;        
	iArray[7][3]=0;
	

    LDImpartGrid = new MulLineEnter( "fm" , "LDImpartGrid" ); 
    //这些属性必须在loadMulLine前

    LDImpartGrid.mulLineCount = 3;   
    LDImpartGrid.displayTitle = 1;
    LDImpartGrid.hiddenPlus = 1;
    LDImpartGrid.hiddenSubtraction = 1;
    LDImpartGrid.canSel = 1;
    LDImpartGrid.canChk = 0;
    //LDImpartGrid.selBoxEventFuncName = "showOne";

    LDImpartGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDImpartGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
