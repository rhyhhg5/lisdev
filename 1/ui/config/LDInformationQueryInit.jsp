<%
//程序名称：LDInformationQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-25 11:56:54
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('SerialNo').value = "";
    fm.all('InformationKind').value = "";
    fm.all('InformationCode').value = "";
    fm.all('InformationName').value = "";
    fm.all('StartMoney').value = "";
    fm.all('EndMoney').value = "";
    fm.all('StartAge').value = "";
    fm.all('EndAge').value = "";
    fm.all('Sex').value = "";
    fm.all('Remark').value = "";
    fm.all('Note2').value = "";
  }
  catch(ex) {
    alert("在LDInformationQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDInformationGrid();  
  }
  catch(re) {
    alert("LDInformationQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDInformationGrid;
function initLDInformationGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="流水号";   
	iArray[1][1]="0px";   
	iArray[1][2]=20;        
	iArray[1][3]=3;
    
    iArray[2]=new Array(); 
	iArray[2][0]="资料分类";   
	iArray[2][1]="60px";   
	iArray[2][2]=20;        
	iArray[2][3]=0;
	
	iArray[3]=new Array(); 
	iArray[3][0]="资料代码";   
	iArray[3][1]="60px";   
	iArray[3][2]=20;        
	iArray[3][3]=0;
	
	iArray[4]=new Array(); 
	iArray[4][0]="资料名称";   
	iArray[4][1]="60px";   
	iArray[4][2]=20;        
	iArray[4][3]=0;
	
	iArray[5]=new Array(); 
	iArray[5][0]="资料人性别";   
	iArray[5][1]="60px";   
	iArray[5][2]=20;        
	iArray[5][3]=0;
	
	iArray[6]=new Array(); 
	iArray[6][0]="资料说明";   
	iArray[6][1]="240px";   
	iArray[6][2]=20;        
	iArray[6][3]=0;
    
    LDInformationGrid = new MulLineEnter( "fm" , "LDInformationGrid" ); 
    //这些属性必须在loadMulLine前

    LDInformationGrid.mulLineCount = 3;   
    LDInformationGrid.displayTitle = 1;
    LDInformationGrid.hiddenPlus = 1;
    LDInformationGrid.hiddenSubtraction = 1;
    LDInformationGrid.canSel = 1;
    LDInformationGrid.canChk = 0;
    //LDInformationGrid.selBoxEventFuncName = "showOne";

   LDInformationGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDInformationGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
