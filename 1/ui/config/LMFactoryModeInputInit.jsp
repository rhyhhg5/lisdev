<%
//程序名称：LMFactoryModeInput.jsp
//程序功能：
//创建日期：2004-12-22 14:24:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('RiskCode').value = "";
    fm.all('FactoryType').value = "";
    fm.all('FactoryCode').value = "";
    fm.all('FactoryName').value = "";
    fm.all('FactorySubCode').value = "";
    fm.all('FactorySubName').value = "";
    fm.all('CalSql').value = "";
    fm.all('ParamsNum').value = "";
    fm.all('Params').value = "";
    fm.all('CalRemark').value = "";
  }
  catch(ex)
  {
    alert("在LMFactoryModeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LMFactoryModeInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();  
    initParamGrid();  
  }
  catch(re)
  {
    alert("LMFactoryModeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initParamGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="参数名";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=1;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      iArray[1][9]="参数名|NOTNULL";

      iArray[2]=new Array();
      iArray[2][0]="顺序号";         			//列名
      iArray[2][1]="140px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="参数说明";         			//列名
      iArray[3][1]="140px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许


      ParamGrid = new MulLineEnter( "fm" , "ParamGrid" ); 
      //这些属性必须在loadMulLine前
      ParamGrid.mulLineCount = 0;   
      ParamGrid.displayTitle = 1;
      ParamGrid.canChk =1;
      ParamGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      ParamGrid.hiddenSubtraction=0;

      ParamGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
