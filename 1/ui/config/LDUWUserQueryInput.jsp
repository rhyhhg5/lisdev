<%
//程序名称：LDUWUserInput.jsp
//程序功能：功能描述
//创建日期：2005-01-24 18:15:01
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LDUWUserQueryInput.js"></SCRIPT> 
  <%@include file="LDUWUserQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLDUWUserGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      核保师编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=UserCode >
    </TD>
    <TD  class= title>
      核保师类型
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=UWType verify="核保师类型|len<=1" CodeData= "0|^01|个险^02|团险" ondblClick="showCodeListEx('UWType',[this,UWTypeName],[0,1]);" onkeyup="showCodeListKeyEx('UWType',[this,UWTypeName],[0,1]);"><input  class="codename" name="UWTypeName" > 
    </TD>
    <TD  class= title>
      核保师级别
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=UWPopedom  verify="核保级别|notnull&len<=2" ondblclick=" showCodeList('UWPopedom',[this,UWPopedomName],[0,1]);"  onkeyup="return showCodeListKey('UWPopedom',[this,UWPopedomName],[0,1]);"><input  class="codename" name="UWPopedomName" > 	
    </TD>
  <TR  class= common>
    <TD  class= title>
      上级核保师
    </TD>
    <TD  class= input>
      <Input class= 'common' name=UpUserCode >
    </TD>
    <TD  class= title>
      首席核保标志
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=PopUWFlag verify="核保师组别|len<=1" CodeData= "0|^1|是首席核保师^0|不是首席核保师" ondblClick="showCodeListEx('PopUWFlag',[this,PopUWFlagName],[0,1]);" onkeyup="showCodeListKeyEx('PopUWFlag',[this,PopUWFlagName],[0,1]);"><input  class="codename" name="PopUWFlagName" > 	
    </TD>
    <TD  class= title>
      核保委托人
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AgentUserCode >
    </TD>
  </TR>
  <TR  class= common>
		<TD  class= title>
		  管理机构
		</TD>
		<TD  class= input>
		  <Input class= 'codeno' name=comCode verify="管理机构|len<=10" value=<%=tG.ManageCom%> ondblclick="return showCodeList('comcode',[this,comCodeName],[0,1]);" onkeyup="return showCodeListKey('comcode', [this,comCodeName],[0,1]);"><input  class="codename" name="comCodeName"> 	
		</TD>
		<TD  class= title>
		  核保师状态
		</TD>
		<TD  class= input>
		  <Input class= 'codeno' name=uwState verify="核保师状态|notnull"	CodeData="0|^0|有效^1|无效" ondblclick="return showCodeListEx('userstate',[this,uwStateName],[0,1]);" onkeyup="return showCodeListKeyEx('userstate', [this,uwStateName],[0,1]);"><input  class="codename" name="uwStateName"> 	
		</TD>
  </TR>
</table>
  </Div>
   
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDUWUser1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLDUWUser1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLDUWUserGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
