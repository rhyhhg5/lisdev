<%
//程序名称：LMRiskEdorItemQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-26 10:16:19
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('RiskCode').value = "";
    fm.all('RiskName').value = "";
    fm.all('EdorCode').value = "";
    fm.all('FinType').value = "";
  }
  catch(ex) {
    alert("在LMRiskEdorItemQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLMRiskEdorItemGrid();  
  }
  catch(re) {
    alert("LMRiskEdorItemQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LMRiskEdorItemGrid;
function initLMRiskEdorItemGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
	iArray[1][0]="险种编码";  
	iArray[1][1]="80px";  
	iArray[1][3]=0;
	
	iArray[2]=new Array();
	iArray[2][0]="险种名称";  
	iArray[2][1]="80px";  
	iArray[2][3]=0;
	
	iArray[3]=new Array();
	iArray[3][0]="保全项目编码";  
	iArray[3][1]="80px";  
	iArray[3][3]=0;
	
	iArray[4]=new Array();
	iArray[4][0]="财务处理类型";  
	iArray[4][1]="80px";  
	iArray[4][3]=0;
    
    LMRiskEdorItemGrid = new MulLineEnter( "fm" , "LMRiskEdorItemGrid" ); 
    //这些属性必须在loadMulLine前

    LMRiskEdorItemGrid.mulLineCount = 3;   
    LMRiskEdorItemGrid.displayTitle = 1;
    LMRiskEdorItemGrid.hiddenPlus = 1;
    LMRiskEdorItemGrid.hiddenSubtraction = 1;
    LMRiskEdorItemGrid.canSel = 1;
    LMRiskEdorItemGrid.canChk = 0;
    //LMRiskEdorItemGrid.selBoxEventFuncName = "showOne";

    LMRiskEdorItemGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LMRiskEdorItemGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
