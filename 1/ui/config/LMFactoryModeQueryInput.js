/** 
 * 程序名称：LMFactoryModeInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2004-12-22 14:24:48
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
var strSql = "select RiskCode,FactoryType,FactoryCode,FactoryName,FactorySubCode,FactorySubName,ParamsNum from LMFactoryMode where 1=1 "
    + getWherePart("RiskCode", "RiskCode")
    + getWherePart("FactoryType", "FactoryType")
    + getWherePart("FactoryCode", "FactoryCode")
    + getWherePart("FactoryName", "FactoryName")
    + getWherePart("FactorySubCode", "FactorySubCode")
    + getWherePart("FactorySubName", "FactorySubName")
    + getWherePart("CalSql", "CalSql")
    + getWherePart("ParamsNum", "ParamsNum")
    + getWherePart("Params", "Params")
    + getWherePart("CalRemark", "CalRemark")
  ;
  //alert(strSql);
	turnPage.queryModal(strSql, LMFactoryModeGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LMFactoryModeGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
	{
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
		return false;
	}
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LMFactoryModeGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = LMFactoryModeGrid.getRowData(tRow-1);	
	return arrSelected;
}
