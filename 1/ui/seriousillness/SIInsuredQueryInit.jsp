<%
//程序名称：LCInuredListQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-07-27 17:39:01
//创建人  ：Yangming
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                   
<script language="JavaScript">                                 
function initForm() {
  try {
    initLCInuredListGrid();  
  }
  catch(re) {
    alert("LCInuredListQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LCInuredListGrid;
function initLCInuredListGrid() {                               
  var iArray = new Array();
    
  try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）             
      iArray[0][1]="60px";         			//列宽                                                     
      iArray[0][2]=10;          			//列最大值                                                 
      iArray[0][3]=1;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[1]=new Array();                                                                                       
      iArray[1][0]="保险合同号";    			//列名                                                     
      iArray[1][1]="150px";            			//列宽                                                     
      iArray[1][2]=100;            			//列最大值                                                 
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许5                    
                                                                                                                   
      iArray[2]=new Array();                                                                                       
      iArray[2][0]="姓名";         		//列名                                                     
      iArray[2][1]="100px";            			//列宽                                                     
      iArray[2][2]=100;            			//列最大值                                                 
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 0                     

      iArray[3]=new Array();                                                                                       
      iArray[3][0]="性别";         		//列名                                                     
      iArray[3][1]="30px";            			//列宽                                                     
      iArray[3][2]=100;            			//列最大值                                                 
      iArray[3][3]=0;             			//是否允许输入,1表示允许，0表示不允许1                    
         
      iArray[4]=new Array();                                                                                       
      iArray[4][0]="出生日期";         		//列名                                                     
      iArray[4][1]="100px";            			//列宽                                                     
      iArray[4][2]=100;            			//列最大值                                                 
      iArray[4][3]=0;             			//是否允许输入,1表示允许，0表示不允许1                    
                                                                                                                          
      iArray[5]=new Array();                                                                                       
      iArray[5][0]="证件类别";         		//列名                                                     
      iArray[5][1]="80px";            			//列宽                                                     
      iArray[5][2]=100;            			//列最大值                                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用6     
                                                                                                                   
      iArray[6]=new Array();                                                                                       
      iArray[6][0]="身份证号";         		//列名                                                     
      iArray[6][1]="150px";            			//列宽                                                     
      iArray[6][2]=100;            			//列最大值                                                 
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用3
      
      iArray[7]=new Array();                                                                                       
      iArray[7][0]="人员类别";         		//列名                                                     
      iArray[7][1]="100px";            			//列宽                                                     
      iArray[7][2]=100;            			//列最大值                                                 
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用3
      
      iArray[8]=new Array();                                                                                       
      iArray[8][0]="社保卡号";         		//列名                                                     
      iArray[8][1]="100px";            			//列宽                                                     
      iArray[8][2]=100;            			//列最大值                                                 
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用3
      
      iArray[9]=new Array();                                                                                       
      iArray[9][0]="联系方式";         		//列名                                                     
      iArray[9][1]="100px";            			//列宽                                                     
      iArray[9][2]=100;            			//列最大值                                                 
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用3
      
      iArray[10]=new Array();                                                                                       
      iArray[10][0]="联系电话";         		//列名                                                     
      iArray[10][1]="100px";            			//列宽                                                     
      iArray[10][2]=100;            			//列最大值                                                 
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用3
      
      iArray[11]=new Array();                                                                                       
      iArray[11][0]="手机号码";         		//列名                                                     
      iArray[11][1]="80px";            			//列宽                                                     
      iArray[11][2]=100;            			//列最大值                                                 
      iArray[11][3]=0; 
      
      iArray[12]=new Array();                                                                                       
      iArray[12][0]="流水号";         		//列名                                                     
      iArray[12][1]="0px";            			//列宽                                                     
      iArray[12][2]=100;            			//列最大值                                                 
      iArray[12][3]=0; 
    
    
    LCInuredListGrid = new MulLineEnter( "fm" , "LCInuredListGrid" ); 
    //这些属性必须在loadMulLine前

    LCInuredListGrid.mulLineCount = 0;   
    LCInuredListGrid.displayTitle = 1;
    LCInuredListGrid.hiddenPlus = 1;
    LCInuredListGrid.hiddenSubtraction = 1;
    LCInuredListGrid.canSel = 0;
    LCInuredListGrid.canChk = 1;
    //LCInuredListGrid.selBoxEventFuncName = "showOne";

    LCInuredListGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LCInuredListGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
