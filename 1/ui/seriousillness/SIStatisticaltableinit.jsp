<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectGrid();
	divPage.style.display="";                 
    fm.all('ManageCom').value = ManageCom;    
}
var ProjectGrid;
function initProjectGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         	//列名
    iArray[0][1]="30px";         	//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="机构编码";         	 //列名
    iArray[1][1]="40px";            //列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    
    iArray[2]=new Array();
    iArray[2][0]="机构名称";         	
    iArray[2][1]="60px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;   
    
    iArray[3]=new Array();
    iArray[3][0]="投保人";         	  //列名
    iArray[3][1]="60px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="保单号码";      //列名
    iArray[4][1]="60px";            	//列宽
    iArray[4][2]=150;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
     
    iArray[5]=new Array();
    iArray[5][0]="承保时间";      //列名
    iArray[5][1]="50px";            	//列宽
    iArray[5][2]=80;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[6]=new Array();
    iArray[6][0]="生效时间";      //列名
    iArray[6][1]="50px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
     
    iArray[7]=new Array();
    iArray[7][0]="失效时间";      //列名
    iArray[7][1]="50px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
     iArray[8]=new Array();
    iArray[8][0]="承保保费";      //列名
    iArray[8][1]="40px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;  
     
    iArray[9]=new Array();
    iArray[9][0]="实收保费";      //列名
    iArray[9][1]="40px";            	//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;
     
    iArray[10]=new Array();
    iArray[10][0]="直接赔款";      //列名
    iArray[10][1]="40px";            	//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;
    
    iArray[11]=new Array();
    iArray[11][0]="回销预付赔款";      //列名
    iArray[11][1]="40px";            	//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=0;
    
    iArray[12]=new Array();
    iArray[12][0]="预付赔款余额";      //列名
    iArray[12][1]="50px";            	//列宽
    iArray[12][2]=200;            			//列最大值
    iArray[12][3]=0;
    
    iArray[13]=new Array();
    iArray[13][0]="已理算未结案赔款";      //列名
    iArray[13][1]="50px";            	//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=0;           			
     					 
     ProjectGrid = new MulLineEnter("fm", "ProjectGrid"); 
    //设置Grid属性
    
     //设置Grid属性
    ProjectGrid.mulLineCount = 0;
    ProjectGrid.displayTitle = 1;
    ProjectGrid.locked = 1;
    ProjectGrid.canSel = 0;
    ProjectGrid.canChk = 0;
    ProjectGrid.hiddenSubtraction = 1;
    ProjectGrid.hiddenPlus = 1;
 	ProjectGrid.selBoxEventFuncName = "getQueryResult";
    ProjectGrid.loadMulLine(iArray);
   
  }
  catch(ex) 
  {
    alert(ex);
  }
}
                                      

</script>
