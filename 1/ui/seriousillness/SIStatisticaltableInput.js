var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}

//查询
function queryClick(){
	initProjectGrid();
	var ManageCom =fm.ManageCom.value;
		if(ManageCom.length !='4' && ManageCom.length !='8' ){ 	
  			alert("管理机构不为四位机构或者八位机构，请重新选择！！");
  			return false;
 		}
	
	
	var strSQL ="select lcg.managecom ,(select name from ldcom  where  comcode=lcg.managecom) ,"
				+"lcg.GrpName ,lcg.GrpContNo , lcg.signdate ,lcg.CValiDate ,lcg.CInValiDate,"
				+"nvl((select sum(SumActuPayMoney)  from  ljapay  where DueFeeType='0' and IncomeNo =lcg.GrpContNo and IncomeType ='1') ,0) ,"
				+"nvl((select sum(SumActuPayMoney)  from  ljapay  where  IncomeNo =lcg.GrpContNo and IncomeType ='1'),0) ,"
				+"nvl((select sum(pay) from ljagetclaim  where grpcontno  = lcg.grpcontno),0) ,"
				+"nvl((select coalesce(sum(-money), 0) from llprepaidtrace where grpcontno = lcg.grpcontno and othernotype = 'C'and moneytype = 'PK' and state = '1'),0),"
				+"nvl((select PrepaidBala from LLPrepaidGrpCont where grpcontno = lcg.grpcontno),0),"
				+"nvl((select sum(realpay) from llcase a,llclaimdetail b where a.caseno = b.caseno and a.rgtstate not in ('09','11','12') and b.grpcontno = lcg.grpcontno),0) "
				+" from  LCGrpCont  lcg where  lcg.MarketType in ('12', '13','14', '15') and lcg.appflag= '1'"
				+getWherePart('lcg.ManageCom', 'ManageCom','like')
				+getWherePart('lcg.CValiDate','CValiDate','>=')
				+getWherePart('lcg.CValiDate','CInValiDate','<=')
				+ " union all"
				+"  select lcg.managecom ,(select name from ldcom  where  comcode=lcg.managecom) ,"
				+"lcg.GrpName ,lcg.GrpContNo , lcg.signdate ,lcg.CValiDate ,lcg.CInValiDate,"
				+"nvl((select sum(SumActuPayMoney)  from  ljapay  where DueFeeType='0' and IncomeNo =lcg.GrpContNo and IncomeType ='1') ,0) ,"
				+"nvl((select sum(SumActuPayMoney)  from  ljapay  where  IncomeNo =lcg.GrpContNo and IncomeType ='1'),0) ,"
				+"nvl((select sum(pay) from ljagetclaim  where grpcontno  = lcg.grpcontno),0) ,"
				+"nvl((select coalesce(sum(-money), 0) from llprepaidtrace where grpcontno = lcg.grpcontno and othernotype = 'C'and moneytype = 'PK' and state = '1'),0),"
				+"nvl((select PrepaidBala from LLPrepaidGrpCont where grpcontno = lcg.grpcontno),0),"
				+"nvl((select sum(realpay) from llcase a,llclaimdetail b where a.caseno = b.caseno and a.rgtstate not in ('09','11','12') and b.grpcontno = lcg.grpcontno),0) "
				+" from  LBGrpCont  lcg where  lcg.MarketType in ('12', '13','14', '15') and lcg.appflag= '1'"
				+getWherePart('lcg.ManageCom', 'ManageCom','like')
				+getWherePart('lcg.CValiDate','CValiDate','>=')
				+getWherePart('lcg.CValiDate','CInValiDate','<=')
				
				
				
	turnPage1.queryModal(strSQL, ProjectGrid);
	fm.querySql.value = strSQL;
	


}


//下载
function  downLoad(){
	if(fm.querySql.value != null && fm.querySql.value != "" && ProjectGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "SIStatisticaltableDownload.jsp";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }


}
