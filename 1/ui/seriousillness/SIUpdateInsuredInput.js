
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function initContInfo() {
	var contQuerySql = "select grpcontno ,managecom,grpname from lcgrpcont  where grpcontno='" + prtNo + "'";
	var arrResult = easyExecSql(contQuerySql);
	if (arrResult) {
		fm.PrtNo.value = prtNo;
		fm.ManageCom.value = arrResult[0][1];
		fm.GrpName.value = arrResult[0][2];
		fm.GrpContNo.value = arrResult[0][0];
	} else {
		alert("\u521d\u59cb\u5316\u4fdd\u5355\u4fe1\u606f\u5f02\u5e38\uff01");
		return false;
	}
}
function easyQueryClick() {
	showInfo = window.open("./SIInsuredQuery.jsp?PrtNo="+prtNo);
}
function initQuery() {
	try {
		var arr = new Array();
		var strSql = "select SeqNo,Name,Sex,Birthday,IDType,IDNo,IDStartDate,IDEndDate,OthIDType,OthIDNo,ContPlanCode,OccupationCode," 
				   + "OccupationType,Retire,EmployeeName,Relation,BankCode,BankAccNo,AccName,contno,InsuredID,MainInsuID,phone,mobile, "
				   +" PersonType, SocialSecurityNo,ContactInformation  "
				   + "from LCIllnessInsuredList where Seqno = '" + seqno + "'";
		var arr = easyExecSql(strSql);
		if (arr != null) {
			fm.all("SeqNo").value = arr[0][0];
			fm.all("Name").value = arr[0][1];
			fm.all("Sex").value = arr[0][2];
			fm.all("Birthday").value = arr[0][3];
			fm.all("IDType").value = arr[0][4];
			fm.all("IDNo").value = arr[0][5];
			fm.all("IDStartDate").value = arr[0][6];
			fm.all("IDEndDate").value = arr[0][7];
			fm.all("OthIDType").value = arr[0][8];
			fm.all("OthIDNo").value = arr[0][9];
			fm.all("ContPlanCode").value = arr[0][10];
			fm.all("OccupationCode").value = arr[0][11];
			fm.all("OccupationType").value = arr[0][12];
			fm.all("Retire").value = arr[0][13];
			fm.all("EmployeeName").value = arr[0][14];
			fm.all("Relation").value = arr[0][15];
			fm.all("BankCode").value = arr[0][16];
			fm.all("BankAccNo").value = arr[0][17];
			fm.all("AccName").value = arr[0][18];
			fm.all("PaperID").value = arr[0][19];
			fm.all("ProtectID").value = arr[0][20];
			fm.all("Relation1").value = arr[0][21];
			fm.all("Phone").value = arr[0][22];
			fm.all("Mobile").value = arr[0][23];
			fm.all('PersonType').value = arr[0][24];
			fm.all('SocialSecurityNo').value = arr[0][25];
			fm.all("ContactInformation").value  =arr[0][26];
		}else{
			alert("获取被保人信息失败!");
		}
	}
	catch (ex) {
		alert(ex.message);
	}
}

function addClick()
{
	var strChkIDNo=checkIdNo(fm.IDType.value,fm.IDNo.value,fm.Birthday.value,fm.Sex.value);
	if(strChkIDNo !=""){
		alert(strChkIDNo);
		return false;
	}
	if (verifyInput2()== false) 
    {        	
        return false;
    }
    
    if(fm.IDType.value == "0"){
    	if(!insertVerify3()){
    		return false;
    	}
    }
    
     if(fm.IDType.value != "0"){
    	if(!insertVerify5){
    	
    		return false;
    	}
    }
		fm.fmtransact.value="INSERT||MAIN";
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm.submit(); //提交
}

function updateClick()
{	
	var strChkIDNo=checkIdNo(fm.IDType.value,fm.IDNo.value,fm.Birthday.value,fm.Sex.value);
	if(strChkIDNo !=""){
		alert(strChkIDNo);
		return false;
	}
	if (verifyInput2()== false) 
    {        	
        return false;
    }
	fm.fmtransact.value="UPDATE||MAIN";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit(); //提交
	
}

function deleteClick()
{
	fm.fmtransact.value="DELETE||MAIN";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit(); //提交
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		window.focus();
		window.location.reload();
	}
}

function insertVerify3(){
	var query = "select 1 from LCIllnessInsuredList where name='" + fm.Name.value + "' and IDType ='"+ fm.IDType.value +"'"
	+ " and IDNo = '"+ fm.IDNo.value +"' and grpcontno = '"+ fm.PrtNo.value +"'"
	+ "with ur";
	var arrResult = easyExecSql(query);
	if(arrResult){
		alert("该保单已录入过，请录入新的保单！");
		return false;
	}
	return true;
}

function insertVerify5(){
	var query = "select 1 from LCIllnessInsuredList where name='" + fm.Name.value + "' and IDType ='"+ fm.IDType.value +"'"
	+ " and IDNo = '"+ fm.IDNo.value +"' and sex = '"+ fm.Sex.value +"' and birthday = '"+ fm.Birthday.value +"' and grpcontno = '"+ fm.PrtNo.value +"' "
	+ "with ur";
	var arrResult = easyExecSql(query);
	if(arrResult){
		alert("该保单已录入过，请录入新的保单！");
		return false;
	}
	return true;
}


