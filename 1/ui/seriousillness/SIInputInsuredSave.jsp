
<jsp:directive.page import="com.sinosoft.lis.tb.ParseGuideInUI"/>
<%
	//程序名称：LGrpInputInsuredSave.jsp
	//程序功能：大团单被保人清单上载
	//创建日期：2013-5-15
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.seriousillness.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%
	GlobalInput tGI = new GlobalInput(); //repair:
	tGI = (GlobalInput) session.getValue("GI");
  
	String FileName = "";
	String filePath = "";
	String tRela  = "";                
	String FlagStr = "Fail";
	String Content = "导入成功！";
	String Result="";
	//得到excel文件的保存路径
	String importPath = request.getParameter("ImportPath");
	String path = application.getRealPath("").replace('\\', '/') + '/';
	String tGrpContNo = request.getParameter("GrpContNo");
	String tPrtNo = request.getParameter("prtNo");
	System.out.println(importPath);
	System.out.println("--团单号：" + tGrpContNo);
	//System.out.println("--印刷号：" + tPrtNo);

	DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
	fu.setSizeMax(10000000);
// maximum size that will be stored in memory?
// 设置最多只允许在内存中存储的数据,单位:字节
	fu.setSizeThreshold(4096);
// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
	fu.setRepositoryPath(path+"temp");
	//开始读取上传信息
	System.out.println(path);
	List fileItems = null;
	try{
		fileItems = fu.parseRequest(request);
	// 依次处理每个上传的文件
		Iterator iter = fileItems.iterator();
		while (iter.hasNext()) {
			FileItem item = (FileItem) iter.next();
			//忽略其他不是文件域的所有表单信息
			if (!item.isFormField()) {
		String name = item.getName();
		System.out.println(name);
		long size = item.getSize();
		if ((name == null || name.equals("")) && size == 0) {
			continue;
		}
		importPath = path + importPath;
		FileName = name.substring(name.lastIndexOf("\\") + 1);
		
		System.out.println(FileName);
		System.out.println("-----------上传文件路径." + importPath + FileName);
		//保存上传的文件到指定的目录
		try {
			item.write(new File(importPath + FileName));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("上传文件失败");
		}
			}
		}
	
		System.out.println("sdfsfsf");
		//输出参数
		CErrors tError = null;
		
		
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
		
		TransferData tTransferData = new TransferData();
	  	boolean res = true;
		IllnessInsuredList tAddInsuredList = new IllnessInsuredList(tGrpContNo,tG);
	
			
		
		  // 准备传输数据 VData
		  VData tVData = new VData();
		  FlagStr="";
		  if(request.getParameter("EdorType")!=null) {
		    tTransferData.setNameAndValue("EdorType",request.getParameter("EdorType"));
		    tTransferData.setNameAndValue("EdorValiDate",request.getParameter("EdorValiDate"));
		    tTransferData.setNameAndValue("Flag", request.getParameter("Flag"));
	  	}
		  tTransferData.setNameAndValue("FileName", FileName);	
		  System.out.println("23234234234qweqwe  "+ importPath);
		  tTransferData.setNameAndValue("FilePath", importPath);
			
		  tVData.add(tTransferData);
			tVData.add(tG);
		  try
		  {
		  	if(!tAddInsuredList.doAdd(importPath, FileName))
		  	{
		  		Content = "保存失败，原因是:" + tAddInsuredList.mErrors.getLastError();
		    	FlagStr = "Fail";
		  	}
		  }
		  catch(Exception ex)
		  {
		    Content = "保存失败，原因是:" + ex.toString();
		    FlagStr = "Fail";
		  }
	    System.out.println("---bbb"+FlagStr);
	  //添加各种预处理
	}
	catch(Exception ex)
	{
		ex.printStackTrace();
		Content = "上传失败，请检查文件是否在4MB以内";
    	FlagStr = "Fail";
	}
	
%>                      
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>