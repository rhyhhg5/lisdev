var turnPage = new turnPageClass();
function initContInfo() {
	var contQuerySql = "select grpcontno ,managecom,grpname from lcgrpcont  where grpcontno='" + prtNo + "'";
	var arrResult = easyExecSql(contQuerySql);
	if(arrResult) {
		fm.PrtNo.value = prtNo;
		fm.ManageCom.value = arrResult[0][1];
		fm.GrpName.value = arrResult[0][2];
	} else {
		alert("初始化保单信息异常！");
		return false;
	}
}

function getSumInsured(){
	var  illInCountSql = "select  count(1)  from LCIllnessInsuredList  where grpcontno='" + prtNo + "'";
	var illInResult = easyExecSql(illInCountSql);
	if(illInResult != null ){
		fm.InsuredCount.value=illInResult[0][0];
	}
}

function goBack() {
	top.close();
}

function InsuredListUpload() {
	
	var pathQuerySql = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
	var arrResult = easyExecSql(pathQuerySql);
	
	var importPath = "";
	if(arrResult){
		importPath = arrResult[0][0];
	} else {
		alert("获取上传路径异常，文件上传失败！");
		return false;
	}
	
	var tprtno = fm.all('FileName').value;

	if (tprtno.indexOf("\\") > 0 ) {
		tprtno =tprtno.substring(tprtno.lastIndexOf("\\") + 1);
	}
    
	if (tprtno.indexOf("/") > 0 ) {
		tprtno =tprtno.substring(tprtno.lastIndexOf("/") + 1);
	}
	
	if ( tprtno.indexOf(".") > 0) {
  		tprtno = tprtno.substring(0, tprtno.indexOf("."));
	}
	
	var batchno = tprtno;
    
	if (tprtno.indexOf("_") > 0) {
		tprtno = tprtno.substring(0, tprtno.indexOf("_"));
	}
	
	if (prtNo!=tprtno ) {
    	alert("文件名与保单号不一致,请检查文件名!");
    	return false;
  	} else {
  		var checkSql = "select 1 from LCIllnessInsuredList where GrpContNo='" + batchno + "' fetch first 1 rows only with ur";
  		var arrResult = easyExecSql(checkSql);
		if(arrResult){
			alert("导入被保人失败：该文件名已使用，请确实该文件是否已经上传！");
			return false;
		}
		var cSql = "select 1 from LCIllnessInsuredList where batchno ='" + batchno + "' and grpcontno='"+tprtno+"' fetch first 1 rows only with ur";
		var arrsql = easyExecSql(cSql);
		if(arrsql){
			alert("导入被保人失败：该批次号已使用，请确实该文件是否已经上传！");
			return false;
		} else {
			fm.BatchNo.value = batchno;
			fm.InputInsured.value = "0";
			fm.InputErrorInsured.value = "0";
		}
  	}

    var showStr="正在上载数据……";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./SIInputInsuredSave.jsp?ImportPath=" + importPath + "&GrpContNo="  + prtNo;
    fm.submit(); //提交
}

function afterSubmit(flagStr, content) {

	showInfo.close();
	
	var urlStr = "";
	if (flagStr == "Fail") {             
		urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	} else { 
    	urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
   		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   		getSumInsured();   
	}
}

function refreshInsured(){
	if(fm.BatchNo.value != null && fm.BatchNo.value != ""){
		var querySQL = "Select count(1) From LCIllnessInsuredList Where grpcontno = '" +  prtNo + "' and BatchNo='" + fm.BatchNo.value + "' with ur";
		var arrResult = easyExecSql(querySQL);
		if(arrResult){
			fm.InputInsured.value = arrResult[0][0];
		}
		
		var strSql = "select grpcontno, name, sex, birthday, IDtype, IDNo, persontype, SocialSecurityNo, ContactInformation, phone, mobile,seqno "
				+ "from LCIllnessInsuredList where 1=1  " 
				+  getWherePart('BatchNo','BatchNo','=') 
				+ getWherePart('grpcontno','PrtNo','=')
				+ "with ur";
		turnPage.queryModal(strSql, DiskErrQueryGrid);
		getSumInsured();
		
	}
}


function GoToInput(){

  



    var strReturn="1";
    //打开扫描件录入界面
    sFeatures = "";
    
    var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 

    if (strReturn == "1"){
    
        window.open("./SIInsuredQueryMain.jsp?prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID , "", sFeatures);
				
	}
}