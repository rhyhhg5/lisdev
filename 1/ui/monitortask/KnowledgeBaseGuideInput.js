//程序名称：TaskService.js
//程序功能：
//创建日期：2004-12-15 
//创建人  ：ZhangRong
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var arrResult;
var mDebug = "0";
var mOperate = "";
var mAction = "";
var mSwitch = parent.VD.gVSwitch;
var k = 0;
 var turnPage = new turnPageClass();   
 var turnPage1 = new turnPageClass();
/*********************************************************************
 *  提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
	if( mAction == "")
		return;

	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit(); //提交
}

function query()
{	
	initKnowledgeBaseGuideGrid();
	var strSql = "select code,codename,code1 from ldcode1 where codetype = 'monitortask' "
		       + getWherePart('code', 'TaskCode')
		       + getWherePart('codename', 'TaskName')
		       + " order by code ";
		       				   
	turnPage.queryModal(strSql, KnowledgeBaseGuideGrid);
}
function addOne() {
	showInfo = window.open("./MonitorTaskInput.jsp");
}
function updateOne() {
  var checkFlag = 0;
  for (i=0; i<MonitorGrid.mulLineCount; i++)
  {
    if (MonitorGrid.getSelNo(i))
    {
      checkFlag = MonitorGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	var	tTaskCode = MonitorGrid.getRowColData(checkFlag - 1, 1);
  	if(tTaskCode == null || tTaskCode == "" || tTaskCode =="null"){
  		alert("任务编码为空，请核查！");
  		return false;
  	}
  	showInfo = window.open("./MonitorTaskInput.jsp?TaskCode="+tTaskCode);
  }else{
  	alert("请选择需要修改的配置！");
  	return false;
  }
}