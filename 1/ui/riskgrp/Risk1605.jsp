<!--
 * <p>FileName: E:\lis\ui\riskgrp\Risk212401.jsp </p>
 * <p>Description: 险种界面文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Author：Minim's ProposalInterfaceMaker
 * @CreateDate：2004-02-10
-->

<DIV id=DivPageHead STYLE="display:''">
<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
</head>

<body>
<form action="./ProposalSave.jsp" method=post name=fm target="fraTitle">

</DIV>

<DIV id=DivRiskCode STYLE="display:''">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      险种编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RiskCode VALUE=""  MAXLENGTH=6 CLASS=code ondblclick="showCodeList('RiskGrp',[this]);" onkeyup="return showCodeListKey('RiskGrp',[this]);" verify="险种编码|code:RiskGrp" >
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivRiskHidden STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      主险投保单号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=MainPolNo>
    </TD>
    <TD CLASS=title>
      代理机构 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentCom>
    </TD>
    <TD CLASS=title>
      经办人 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Handler>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      联合代理人编码
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentCode1>
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCPolHidden STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      险种版本 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RiskVersion>
    </TD>
    <TD CLASS=title>
      合同号
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=ContNo>
    </TD>
    <TD CLASS=title>
      首期交费日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=FirstPayDate>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      语种 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Lang>
    </TD>
    <TD CLASS=title>
      合同争议处理方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=DisputedFlag>
    </TD>
    <TD CLASS=title>
      银行代收标记
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentPayFlag>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      银行代付标记
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentGetFlag>
    </TD>
    <TD CLASS=title>
      管理费比例
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=ManageFeeRate>
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCPolButton STYLE="display:'none'">
<!-- 保单信息部分 -->
<table>
<tr>
<td>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCPol);">
</td>
<td class= titleImg>
保单信息
<!--<INPUT VALUE="查询责任信息" TYPE=button onclick="showDuty();">
<!--<INPUT VALUE="关联暂交费信息" TYPE=button onclick="showFee();">
<INPUT id="butChooseDuty" VALUE="选择责任" TYPE=button onclick="chooseDuty();" disabled >
<INPUT id="butBack" VALUE="返回" TYPE=button disabled >-->
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCPol STYLE="display:'none'">
<TABLE class=common>
  <TR CLASS=common>
    <TD CLASS=title>
      集体合同号码
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpContNo VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=20 verify="集体合同号码|notnull" >
    </TD>
  </TR>
  <TR CLASS=common>
    <TD CLASS=title>
      集体投保单号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpPolNo VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=20 verify="集体投保单号码|notnull" >
    </TD>
    <TD CLASS=title>
      投保单号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=ProposalNo VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=20 >
    </TD>
    <TD CLASS=title>
      印刷号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PrtNo VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=14 verify="印刷号码|notnull&len=14" >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      管理机构 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=ManageCom VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=10 verify="管理机构|code:station&notnull" >
    </TD>
    <TD CLASS=title>
      销售渠道 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=SaleChnl VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=2 verify="销售渠道|code:SaleChnl&notnull" >
    </TD>
    <TD CLASS=title>
      代理人编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentCode VALUE="" MAXLENGTH=10 CLASS=code ondblclick="return queryAgent();"onkeyup="return queryAgent2();" >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      代理人组别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentGroup VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=12 verify="代理人组别|notnull" >
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCAppntIndButton STYLE="display:'none'">
<!-- 投保人信息部分 -->
<table>
<tr>
<td>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
</td>
<td class= titleImg>
投保人信息（客户号：<Input class= common  name=AppntCustomerNo >
<INPUT id="butBack" VALUE="查询" TYPE=button onclick="queryAppntNo();">
首次投保客户无需填写客户号）
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCAppntInd STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      姓名 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntName>
    </TD>
    <TD CLASS=title>
      性别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntSex>
    </TD>
    <TD CLASS=title>
      出生日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntBirthday>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      与被保人关系 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntRelationToInsured>
    </TD>
    <TD CLASS=title>
      证件类型 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntIDType>
    </TD>
    <TD CLASS=title>
      证件号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntIDNo>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      国籍
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntNativePlace>
    </TD>
    <TD CLASS=title>
      户籍所在地 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntRgtAddress>
    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      通讯地址
    </TD>
    <TD CLASS=input COLSPAN=3>
      <Input NAME=AppntPostalAddress>
    </TD>
    <TD CLASS=title>
      通讯地址邮政编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntZipCode>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      住址
    </TD>
    <TD CLASS=input COLSPAN=3>
      <Input NAME=AppntHomeAddress>
    </TD>
    <TD CLASS=title>
      住址邮政编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntHomeZipCode>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      联系电话（1）
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntPhone>
    </TD>
    <TD CLASS=title>
      联系电话（2）
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntPhone2>
    </TD>
    <TD CLASS=title>
      移动电话
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntMobile>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      电子邮箱
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntEMail>
    </TD>
    <TD CLASS=title>
      工作单位
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntGrpName>
    </TD>
    <TD CLASS=title>
      职业（工种） 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntWorkType>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      兼职（工种） 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntPluralityType>
    </TD>
    <TD CLASS=title>
      职业代码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntOccupationCode>
    </TD>
    <TD CLASS=title>
      职业类别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntOccupationType>
    </TD>
  </TR>

  <TR CLASS=common>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCAppntGrpButton STYLE="display:'none'">
<!-- 集体投保人信息部分 -->
<table>
<tr>
<td>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntGrp);">
</td>
<td class= titleImg>
投保单位资料
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCAppntGrp STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      单位客户号
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppGrpNo VALUE="" CLASS=readonly readonly TABINDEX=-1 verify="单位客户号|notnull" >
    </TD>
    <TD CLASS=title>
      单位名称 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppGrpName VALUE="" CLASS=readonly readonly TABINDEX=-1 verify="单位名称 |notnull" >
    </TD>
    <TD CLASS=title>
      单位地址 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppGrpAddress VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      邮政编码
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppGrpZipCode VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      单位性质 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpNature VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      行业类别
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=BusinessType VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      单位总人数
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Peoples VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      主营业务 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=MainBussiness VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      单位法人代表
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Corporation VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD>
      <B>保险联系人一</B>
    </TD>
    <TD COLSPAN=1>

    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      姓名
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=LinkMan1 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      部门
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Department1 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      联系电话
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpPhone1 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      E_mail
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=E_Mail1 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      传真
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Fax1 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
  </TR>

  <TR CLASS=common>
    <TD>
      <B>保险联系人二</B>
    </TD>
    <TD COLSPAN=1>

    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      姓名
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=LinkMan2 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      部门
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Department2 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      联系电话
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpPhone2 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      E_mail
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=E_Mail2 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      传真
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Fax2 VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      付款方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetFlag VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      开户银行
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpBankCode VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
    <TD CLASS=title>
      银行帐号 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpBankAccNo VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      货币种类
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Currency VALUE="" CLASS=readonly readonly TABINDEX=-1 >
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCInsuredButton STYLE="display:'none'">
<!-- 被保人信息部分 -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCInsured);">
</td>
<td class= titleImg>
被保人信息（客户号：<Input class= common name=CustomerNo >
<INPUT id="butBack" VALUE="查询" TYPE=button onclick="queryInsuredNo();"> 首次投保客户无需填写客户号）
<!--<Div  id= "divSamePerson" style= "display: ''">
<font color=red>
如投保人为被保险人本人，可免填本栏，请选择
<INPUT TYPE="checkbox" NAME="SamePersonFlag" onclick="isSamePerson();">
</font>
</div>-->
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCInsured STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      姓名 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Name VALUE="" CLASS=common MAXLENGTH=20 verify="姓名|notnull" >
    </TD>
    <TD CLASS=title>
      性别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Sex VALUE="" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('Sex', [this]);" onkeyup="return showCodeListKey('Sex', [this]);" verify="被保人性别|notnull&code:Sex" >
    </TD>
    <TD CLASS=title>
      出生日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Birthday VALUE="" CLASS=common verify="被保人出生日期|date&notnull" >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      证件类型 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=IDType VALUE="" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('IDType', [this]);" onkeyup="return showCodeListKey('IDType', [this]);" verify="被保人证件类型|code:IDType" >
    </TD>
    <TD CLASS=title>
      证件号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=IDNo VALUE="" CLASS=common MAXLENGTH=20 >
    </TD>
    <TD CLASS=title>
      国籍
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=NativePlace VALUE="" CLASS=code ondblclick="return showCodeList('NativePlace', [this]);" onkeyup="return showCodeListKey('NativePlace', [this]);" verify="被保人国籍|code:NativePlace" >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      保单类型标记
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PolTypeFlag VALUE="0" CLASS=code ondblclick="showCodeList('PolTypeFlag', [this]);" onkeyup="showCodeListKey('PolTypeFlag', [this]);" verify="保单类型标记|notnull" >
    </TD>
    <TD CLASS=title>
      被保人人数
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InsuredPeoples VALUE="" CLASS=common verify="被保人人数|num" >
    </TD>
    <TD CLASS=title>
      被保人年龄
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InsuredAppAge VALUE="" CLASS=common verify="被保人年龄|num" >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      户口所在地 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RgtAddress VALUE="" CLASS=common MAXLENGTH=80 >
    </TD>
    <TD CLASS=title>
      住址 
    </TD>
    <TD CLASS=input COLSPAN=3>
      <Input NAME=HomeAddress VALUE="" CLASS=common3 MAXLENGTH=80 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      邮政编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=HomeZipCode VALUE="" CLASS=common MAXLENGTH=6 verify="被保人邮政编码|zipcode" >
    </TD>
    <TD CLASS=title>
      联系电话（1）
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Phone VALUE="" CLASS=common MAXLENGTH=18 >
    </TD>
    <TD CLASS=title>
      联系电话（2）
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Phone2 VALUE="" CLASS=common >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      工作单位
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpName VALUE="" CLASS=common MAXLENGTH=60 >
    </TD>
    <TD CLASS=title>
      职业（工种） 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=WorkType VALUE="" CLASS=common MAXLENGTH=10 >
    </TD>
    <TD CLASS=title>
      兼职（工种） 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PluralityType VALUE="" CLASS=common MAXLENGTH=10 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      职业代码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=OccupationCode VALUE="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('OccupationCode', [this,OccupationType],[0,2]);" onkeyup="return showCodeListKey('OccupationCode', [this,OccupationType],[0,2]);" verify="被保人职业代码|code:OccupationCode" >
    </TD>
    <TD CLASS=title>
      职业类别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=OccupationType VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=10 verify="被保人职业类别|notnull&code:OccupationType" >
    </TD>
    <TD CLASS=title>
      客户内部号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=SequenceNo VALUE="" CLASS=common MAXLENGTH=20 >
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCInsuredHidden STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      健康状况 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Health>
    </TD>
  </TR>

</TABLE>
</DIV>


<DIV id=DivLCKindButton STYLE="display:''">
<!-- 险种信息部分 -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCKind);">
</td>
<td class= titleImg>
险种信息
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCKind STYLE="display:''">
<TABLE width="349" class=common>
 <TR CLASS=common>

	 <TD width="80" class=title>
      等待期
    </TD>
    <TD width="257" class=input>
      <Input name=WaitPeriod class=common>
    </TD>
  

  </TR>
</TABLE>
</DIV>

<DIV id=DivLCKindHidden STYLE="display:'none'">
<TABLE class=common> 

  <TR CLASS=common>
      <TD class=title>
      缴费规则
    </TD>
    <TD class=input>
      <Input Name=PayRuleCode class=code ondblclick="showCodeListEx('PayRuleCode',[this])" onkeyup="showCodeListKeyEx('PayRuleCode',[this])">
    </TD>
    
        <TD CLASS=title>
      保单生效日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=CValiDate VALUE="" CLASS=common verify="保单生效日期|notnull&date" >
    </TD>
    <TD CLASS=title>
      是否指定生效日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=SpecifyValiDate VALUE="Y" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('YesNo', [this]);" onkeyup="return showCodeListKey('YesNo', [this]);" verify="是否指定生效日|code:YesNo" >
    </TD>
	 <TD CLASS=title>
      交费方式
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PayIntv VALUE="" CLASS=code CodeData="0|^0|趸交|^-1|不定期交" ondblClick="showCodeListEx('PayIntv212401',[this],[0]);" onkeyup="showCodeListKeyEx('PayIntv212401',[this],[0]);" verify="交费方式|notnull" >
    </TD>  
    
    </tr>
    <tr CLASS=common>
      <TD CLASS=title>
      收费方式
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PayLocation VALUE="" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('PayLocation', [this]);" onkeyup="return showCodeListKey('PayLocation', [this]);" verify="收费方式|code:PayLocation" >
    </TD>
    <TD CLASS=title>
      开户行 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=BankCode VALUE="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('bank', [this]);" onkeyup="return showCodeListKey('bank', [this]);" verify="开户行|code:bank" >
    </TD>
    <TD CLASS=title>
      户名 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20 >
    </TD>
    <TD CLASS=title>
      银行帐号 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=BankAccNo VALUE="" CLASS=common MAXLENGTH=40 >
    </TD>
  </TR>
  <TR CLASS=common>
    <TD CLASS=title>
      起领期间
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetYear VALUE="" CLASS=common verify="起领期间|notnull" >
    </TD>
    <TD CLASS=title>
      起领期间单位 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetYearFlag VALUE="" CLASS=code CodeData="0|^A|岁^Y|年" ondblClick="showCodeListEx('GetYear212401',[this],[0]);" onkeyup="showCodeListKeyEx('GetYear212401',[this,GetYearFlag],[0]);" verify="起领期间单位|notnull" >
    </TD>
    <TD CLASS=title>
      起领日期计算类型 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetStartType VALUE="" CLASS=code CodeData="0|^B|出生日期对应日|^S|起保日期对应日" ondblClick="showCodeListEx('GetStartTypef212401',[this],[0]);" onkeyup="showCodeListKeyEx('GetStartType212401',[this],[0]);" verify="起领日期计算类型|notnull" >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      年金领取方式
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetDutyKind VALUE="" CLASS=code CodeData="0|^9|一次性领取年金|0^10|月领固定期平准型年金|1^11|年领固定期平准型年金|12^12|月领固定期增额型年金|1^13|年领固定期增额型年金|12^14|月领定额平准型年金|1^15|年领定额平准型年金|12^16|月领定额递增型年金|1^17|年领定额递增型年金|12^18|月领十年固定定额平准型年金|1^19|年领十年固定定额平准型年金|12^20|月领十年固定定额递增型年金|1^21|年领十年固定定额递增型年金|12" ondblClick="showCodeListEx('GetDutyKind212401',[this,GetIntv],[0,2]);" onkeyup="showCodeListKeyEx('GetDutyKind212401',[this,GetIntv],[0,2]);" verify="年金领取方式|notnull" >
    </TD>  
      <TD CLASS=title>
      领取间隔
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetIntv value="" class=readonly readonly tabindex=-1 maxlength=12>
    </TD>   
     <TD CLASS=title>
    领取年期
    </TD>
    <TD CLASS=input COLSPAN=1>
    <Input NAME=StandbyFlag1 VALUE="" CLASS=code CodeData="0|^5|五年^10|十年^15|十五年^20|二十年^30|三十年" ondblClick="showCodeListEx('StandbyFlag1',[this],[0]);" onkeyup="showCodeListKeyEx('StandbyFlag1',[this],[0]);" verify="领取年期|notnull" >
     </TD>   

  </TR>

  <TR CLASS=common>
       <TD CLASS=title>
      保费 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Prem VALUE="" CLASS=common TABINDEX=-1 MAXLENGTH=12 >
    </TD>
 
   <TD CLASS=title>
      浮动费率
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=FloatRate VALUE="" CLASS=common verify="浮动费率|Num" >
    </TD>
    <TD CLASS=title>
      是否体检件
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=HealthCheckFlag VALUE="N" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('YesNo', [this]);" onkeyup="return showCodeListKey('YesNo', [this]);" verify="是否体检件|code:YesNo" >
    </TD>
    <TD CLASS=title>
      溢交保费方式
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=OutPayFlag VALUE="1" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('OutPayFlag', [this]);" onkeyup="return showCodeListKey('OutPayFlag', [this]);" verify="溢交保费处理方式|code:OutPayFlag&notnull" >
    </TD>
  </TR>



  <TR CLASS=common> 
     <TD CLASS=title>
      红利领取方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=BonusGetMode VALUE="" CLASS=code ondblclick="return showCodeList('LiveGetMode', [this]);" onkeyup="return showCodeListKey('LiveGetMode', [this]);" verify="红利领取方式|code:LiveGetMode" >
    </TD>
    <TD CLASS=title>
      生存金领取方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=LiveGetMode  CLASS=readonly readonly TABINDEX=-1 >
    </TD>
  </TR>
  <TR CLASS=common> 
   <TD CLASS=title>
      保额 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Amnt VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=12 >
    </TD>
    <TD CLASS=title>
      份数 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Mult>
    </TD>
    <TD CLASS=title>
      保险期间
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InsuYear>
    </TD>
    <TD CLASS=title>
      保险期间单位 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InsuYearFlag>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      交费期间
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PayEndYear>
    </TD>
    <TD CLASS=title>
      终交期间单位
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PayEndYearFlag>
    </TD>

  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      自动垫交标志 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AutoPayFlag>
    </TD>
    <TD CLASS=title>
      利差返还方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InterestDifFlag>
    </TD>
    <TD CLASS=title>
      减额交清标志 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=SubFlag>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      是否自动续保
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RnewFlag>
    </TD>
  </TR>

</TABLE>
</DIV>
<DIV id=DivChoosePrem STYLE="display:''">
<!--可以选择的保费项部分，该部分始终隐藏-->
<Div  id= "divChoosePrem0" style= "display: ''">
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPremGrid1);">
</td>
<td class= titleImg>
可选保费项信息
</td>
</tr>
</table>

<Div  id= "divPremGrid1" style= "display: ''">
		<table  class= common>
			<tr  class= common>
			<td text-align: left colSpan=1>
				<span id="spanPremGrid" >
				</span>
			</td>
			</tr>
		</table>
</div>
</div>

</DIV>
<DIV id=DivLCSubInsured STYLE="display:'none'">
<!-- 连带被保人信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured2);">
</td>
<td class= titleImg>
连带被保人信息
</td>
</tr>
</table>

<Div  id= "divLCInsured2" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanSubInsuredGrid" >
</span>
</td>
</tr>
</table>
</div>

</DIV>
<DIV id=DivLCBnf STYLE="display:''">
<!-- 受益人信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCBnf1);">
</td>
<td class= titleImg>
受益人信息
</td>
</tr>
</table>

<Div  id= "divLCBnf1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanBnfGrid" >
</span>
</td>
</tr>
</table>
</div>

</DIV>

<DIV id=DivLCImpart STYLE="display:'none'">
<!-- 告知信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
</td>
<td class= titleImg>
告知信息
</td>
</tr>
</table>

<Div  id= "divLCImpart1" style= "display: 'none'">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartGrid" >
</span>
</td>
</tr>
</table>
</div>

</DIV>

<DIV id=DivLCSpec STYLE="display:''">
<!-- 特约信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCSpec1);">
</td>
<td class= titleImg>
特约信息
</td>
</tr>
</table>

<Div  id= "divLCSpec1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanSpecGrid">
</span>
</td>
</tr>
</table>
</div>

</DIV>

<DIV id=DivChooseDuty STYLE="display:'none'">
<!--可以选择的责任部分，该部分始终隐藏-->
<Div  id= "divChooseDuty0" style= "display: ''">
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDutyGrid1);">
</td>
<td class= titleImg>
可选责任信息
</td>
</tr>
</table>

<Div  id= "divDutyGrid1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanDutyGrid" >
</span>
</td>
</tr>
</table>
</div>
</div>

</DIV>



<DIV id=DivPageEnd STYLE="display:''">
<input type=hidden id="inpNeedDutyGrid" name="inpNeedDutyGrid" value="0">
<input type=hidden id="fmAction" name="fmAction">
<input type=hidden id="ContType" name="ContType" value="">
<input  type= "hidden" class= Common name= SelPolNo value= "">
<input type=hidden id="inpNeedPremGrid" name="inpNeedPremGrid" value="1">

<Div  id= "divButton" style= "display: ''">
<br>
<%@include file="../common/jsp/ProposalOperateButton.jsp"%>
</DIV>

</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
<span id="spanApprove"  style="display: none; position:relative; slategray"></span>

</body>
</html>

<script>
function returnParent() {
var isDia=0;
var haveMenu=0;
var callerWindowObj;

try	{
callerWindowObj = dialogArguments;
isDia=1;
}
catch(ex1) {
isDia=0;
}

try	{
if(isDia==0) { //如果是打开一个新的窗口，则执行下面的代码
top.opener.parent.document.body.innerHTML=window.document.body.innerHTML;
}
else { //如果打开一个模态对话框，则调用下面的代码
callerWindowObj.document.body.innerHTML=window.document.body.innerHTML;
haveMenu = 1;
callerWindowObj.parent.frames("fraMenu").Ldd = 0;
callerWindowObj.parent.frames("fraMenu").Go();
}
}
catch(ex)	{
if( haveMenu != 1 ) {
alert("Riskxxx.jsp:发生错误："+ex.name);
}
}

top.close();
}

returnParent();
</script>

</DIV>



