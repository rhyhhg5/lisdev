<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：SendMsgSave.jsp
//程序功能：
//创建日期：2006-01-16
//创建人  ：张  君
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.io.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
String tflag = "True";
String Content = "发送成功";
String mobile =request.getParameter("mobile");
String tEmail = request.getParameter("to");
String tMsgContent =request.getParameter("content");
//String tTopic = request.getParameter("topic");
System.out.println(mobile);
System.out.println(tEmail);
System.out.println(tMsgContent);
GlobalInput tGlobalInput = new GlobalInput(); 
tGlobalInput = (GlobalInput)session.getValue("GI");
try {
    TransferData PrintElement = new TransferData();
    PrintElement.setNameAndValue("mobile", mobile);
    PrintElement.setNameAndValue("content", tMsgContent);
    PrintElement.setNameAndValue("strFrom","service@picchealth.com");
    PrintElement.setNameAndValue("strTo", tEmail);
    //PrintElement.setNameAndValue("strTitle", tTopic);
    PrintElement.setNameAndValue("strPassword","");

    VData aVData = new VData();
    aVData.add(tGlobalInput);
    aVData.add(PrintElement);

    SendMsgMail tSendMsgMail = new SendMsgMail();
    if (!(mobile.equals("")||mobile.equals(null)))
    {
      if (!tSendMsgMail.submitData(aVData, "Message")) {
          Content = "短信发送失败";
          System.out.println("短信发送失败");
          tflag = "Fail";
      }
    }
    if(!(tEmail.equals("")||tEmail.equals(null)))
    {
      if (!tSendMsgMail.submitData(aVData, "Email")) {
          Content = "邮件发送失败";
          System.out.println("邮件发送失败");
          tflag = "Fail";
      }
    }
} catch (IOException ex) {
    ex.printStackTrace();
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=tflag%>","<%=Content%>");
window.focus;
</script>
</html>