<%
  //程序名称：FameworkExample.jsp
  //程序功能：核心框架js相关方法例子
  //创建日期：2016-11-08
  //创建人  ：yangyang
  //更新人  ：
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
      GlobalInput tGI = new GlobalInput();
      tGI = (GlobalInput)session.getValue("GI");
%>
<script type="">
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		
		<SCRIPT src="./FameworkExample.js"></SCRIPT>
</head>
<body onload="initElementtype()">
 <form name ='fm'>
<table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCss);"></td>
    <td class=titleImg>样式用法 </td>    
   </tr>
  </table>
  <div id="divCss" style="display:''">
<table class= common >
	<TR>
	  	<TD>录入框样式</TD>
	  	<TD><input class="common" ></TD>
	    <TD colspan='3'>录入框调用class样式为：class= 'common'</TD>
    </TR>
	<TR>
	  	<TD>日期控件样式</TD>
	  	<TD>
	      <Input class= 'coolDatePicker'  dateFormat="Short" name=MakeDate1 value="" >
	    </TD>
     	<TD colspan='3' > 对应的控件调用class样式为：class= 'coolDatePicker'</TD>
   </TR>
   <TR>
	  	<TD>双击下拉样式</TD>
	  	<TD>
	      <Input class= 'codeno' >
	    </TD>
     	<TD colspan='3'>对应的控件调用class样式为：class= 'codeno',实际上是由两部分组成，后面再添加一个录入框(class='codename')即可</TD>
   </TR>
   <TR>
	  	<TD>双击下拉后面录入框样式</TD>
	  	<TD><input class='codename' ></TD>
	    <TD colspan='3'>双击下拉后面录入框调用class样式为：class='codename'</TD>
    </TR>
</table>
</div>
<table>

   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCheck);"></td>
    <td class=titleImg>校验用法 </td>    
   </tr>
  </table>
  <div id="divCheck" style="display:''">
<table class= common >
	<TR>
	  	<TD>必录项标志</TD>
	  	<TD><input elementtype=nacessary></TD>
	    <TD colspan='3'>必录项标志需要添加属性：elementtype=nacessary,其中：在body里面要调用:onload="initElementtype()";initElementtype()方法在common.js里面</TD>
    </TR>
    <TR rowspan='1' align="left">  
	  	<TD>非空或格式校验</TD>
     	<TD colspan='4' >非空或格式校验需要添加属性：verify="describe|校验类型&校验类型",其中:describe为提示描述，校验类型是固定格式，目前支持以下几种：</br>
     	</TD>
   	</TR>
	<TR >  
	  	<TD></TD>
     	<TD colspan='4' >
     	<textarea rows="14" cols="100" style="border: thin;overflow-x:hidden;overflow-y:hidden" readonly>
NULL--必须为空
NOTNULL--必须为非空校验
NUM--必须为数字，包括：整数和小数
YYYYMM--必须为年月格式
DATE--必须为日期格式(YYYY-MM-DD)或者(YYYYMMDD)
TIME--必须为时间格式(hh：mm：ss)
EMAIL--必须为邮件格式
DECIMAL--必须为0到1之间的小数
INT--必须为整数
IDNO--必须为身份证，只校验身份证的格式
LEN--长度，只能与数字比较大小。用法为：LEN=6或LEN<6或LEN>6等，录入的值的长度和6比大小
VALUE--值，用法为：VALUE=6或VALUE<6或VALUE>6等，录入的值和6比大小
CODE:--与双击下拉配套使用，用法：code:type,type即双击下拉中调用的那个代码，用来判断输入的是不是双击下拉中的数据
ZIPCODE--邮政编码校验
</textarea><br/>
<font color='red'>
 对于上面的校验，可以进行组合使用，两个校验类型之间只需要用&来连接即可。调用时，只需要在提交按钮对应的方法最前面调用：if(verifyInput()== false)
	{
		return false;
	}
	<br/>
	说明 ：verifyInput()此方法是VerifyInput.js里面的方法，是遍历所有的form里面所有有verify属性的要素，然后根据不同的校验类型进行校验。所以，一般调用此方法后，是会按form里面要素的先后顺序进行循环，如果前面的校验住了，后面的就不会再校验。	
</font>
</table>
  </div>
 
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divExample);"></td>
    <td class=titleImg>简单校验例子如下 </td>    
   </tr>
  </table>
  <div id ="divExample">
  <table class= common>
  	<tr>
  		<td>姓名（非空校验）</td>
  		<td><input class="common" name ="name"  elementtype=nacessary verify="姓名（提示）|notnull"></td>
  		<td><input TYPE=button class="cssbutton" value = "必录项校验" onClick="CheckValue();"></td>
  		<td>生日（日期校验）</td>
  		<td><input class="common"   verify="生日|Date"></td>
  		<td><input TYPE=button class="cssbutton" value = "日期校验" onClick="CheckValue();"></td>
  		<td>年龄（非空整数且在0-100之内）</td>
  		<td ><input class="common" name ="sex"  verify="年龄（组合）|notnull&value>a&value<c"></td>
  		<td ><input TYPE=button class="cssbutton" value = "组合校验" onClick="CheckValue();"></td>
  	</tr>
  </table>
  </div>
  <table>
   <tr>
    <td class=titleImg>其它框架相关 </td> 
   </tr>
   <tr>
    <td ><input TYPE=button class="cssbutton" value = "双击下拉" onClick="onDoubleClick('OnDoubleClick');"></td>
    <td ><input TYPE=button class="cssbutton" value = "mulline列表" onClick="onDoubleClick('Mulline');"></td>   
   </tr>
  </table>
  </form>
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>