<%
  //程序名称：FameworkMulline.jsp
  //程序功能：核心框架mulline.js相关方法例子
  //创建日期：2016-11-16
  //创建人  ：yangyang
  //更新人  ：
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
      GlobalInput tGI = new GlobalInput();
      tGI = (GlobalInput)session.getValue("GI");
%>
<script type="">
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构

</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file ='./FameworkMullineInit.jsp'%>
</head>
<body onload="initForm();initElementtype()">
<form name ='fm'>
<table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divMullineGrid);"></td>
    <td class=titleImg>mulline列表例子 </td>    
   </tr>
  </table>
	<div id="divMullineGrid" style="display:''">
		<span id="spanMullineGrid"></span>
	</div>
	<input type ='button' class='cssbutton' value ="mulline校验" onclick ="checkMullineValue();">
</form>
<font color='red' style ="size:20px"> 说明：</font>
<br>
1、一个列表，由两个页面共同完成 ，一个是*Init.jsp，一个是主页面，其中：主页面包括*init.jsp,语句 ：&lt%@include file ='./FameworkMullineInit.jsp'%&gt
<br>
2、列表在页面上的显示及相应的位置，是在主页面中控制的，由以下语句：&ltdiv id="divMullineGrid" style="display:''"&gt
		&ltspan id="spanMullineGrid"&gt：&lt/span&gt
	&lt/div&gt来显示列表，<br>此语句放置的位置，就是列表显示时的位置<br>
3、列表的格式，是在*init.jsp里面进行控制的，用一个二维数组中的行来表示这个列表显示多少列，二维数组中的列来对于列表中当列进行格式及校验设置 。可以参考：mullien.js中1170-1197行<br>
&nbsp;&nbsp;&nbsp;特别说明：对于一列数据校验时在第9列进行添加对应的校验 ，格式：<br>
&nbsp;&nbsp;&nbsp;iArray[1][9]="年龄|VALUE<100&VALUE>0&NOTNULL";表示此列的值不能为空且大于0小于100，如果不满足；则会提示年龄+不满足条件的说明<br>
&nbsp;&nbsp;&nbsp;此时 需要在提交时，调用以下方法：ObjGrid. CheckValue(“ObjGrid”);  (ObjGrid是MulLine对象名)
                或者ObjGrid.CheckValue( ); （即无参数）<br>
4、里面有对于列表显示时，单选、多选、+、-号 等相关属性的说明 ，可参考：mulline.js中20-28行。<br>
5、对于mulline列表的其它操作，可以查看：Mulline.js使用手册。<br>
6、对于mulline列表下拉时，显示框的宽度是通过第18列来设置的，如果此列设置了值 ，则就是这个值的固定宽度；
<br>&nbsp;&nbsp;&nbsp;如果没有定义，则默认为250（但是如果同一个下拉选多次双击，则宽度会变)。怀疑：CCodeOperate.js 文件711行，判断少加null（非字符串）判断(如果找到原因，可以把这条去掉)
<br>&nbsp;&nbsp;&nbsp;如果想随着查询的数据变化，则需要定义成："undefined";
<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
<script type="text/javascript">
	function checkMullineValue()
	{
		if(!MullineGrid.checkValue("MullineGrid")) return false;
	}
</script>
</body>
</html>