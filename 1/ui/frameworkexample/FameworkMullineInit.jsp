<%@page contentType="text/html;charset=GBK" %>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
  }
  catch(ex)
  {
    alert("在FameworkMullineInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initMullineGrid();
  }
  catch(ex)
  {
    alert("在FameworkMullineInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }  
}


// 保单信息列表的初始化
function initMullineGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=30;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="年龄";         			//列名
		iArray[1][1]="20px";            		//列宽
		iArray[1][2]=3;            			//列最大值
		iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		iArray[1][9]="年龄|VALUE<100&VALUE>0&NOTNULL";
		
		iArray[2]=new Array();
		iArray[2][0]="管理机构(固定宽度250)";         			//列名
		iArray[2][1]="80px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[2][4]="comcode";	 
		iArray[2][5]="2|3";       
		iArray[2][6]="0|1";       
		iArray[2][9]="管理机构|code:comcode";
		
		iArray[3]=new Array();
		iArray[3][0]="管理机构名称";         			//列名
		iArray[3][1]="100px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="管理机构(随查询的数据变化)";         			//列名
		iArray[4][1]="80px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[4][4]="comcode";	 
		iArray[4][5]="4|5";       
		iArray[4][6]="0|1";       
		iArray[4][9]="管理机构|code:comcode";
		iArray[4][18]="undefined";
		
		iArray[5]=new Array();
		iArray[5][0]="管理机构名称";         			//列名
		iArray[5][1]="100px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		MullineGrid = new MulLineEnter( "fm" , "MullineGrid" ); 
		//这些属性必须在loadMulLine前
		MullineGrid.mulLineCount = 3;   
		MullineGrid.displayTitle = 1;
		MullineGrid.locked = 0;
		MullineGrid.canSel = 1;
		MullineGrid.canChk = 1;
		MullineGrid.loadMulLine(iArray);
	}
  catch(ex)
  {
    alert(ex);
  }
}
</script>