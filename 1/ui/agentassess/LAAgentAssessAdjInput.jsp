 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //String tManagecom=tG.ManageCom;
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String AgentSeries=request.getParameter("AgentSeries");
%>
<script>
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
   var tsql="1 and char(length(db2inst1.trim(comcode)))<=#8# and comcode<>#8600#  "
</script>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAAgentAssessAdjInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAAgentAssessAdjInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <title>考核调整 </title>   
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAgentAssessAdjSave.jsp" method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
	  <td class= titleImg align= center>请输入考核信息的查询条件：</td>
	</tr>
	</table>
    <table  class= common align=center>
      <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>
          
          <TD  class= title>
            考核年月
          </TD>
          <TD  class= input>
            <Input class=common name=IndexCalNo verify="考核年月|notnull&len<=6&int" elementtype=nacessary >
            <font color="red">  'YYYYMM'</font>
          </TD>
      </TR>
      <TR class=common>
        <TD class=title>
          销售单位代码
        </TD>
        <TD class=title>
          <Input class=common name=BranchAttr>
        </TD>
        <TD class=title>
          晋降类型
        </TD>
        <TD class=title>
           <Input name=AssessType class="codeno"  
          CodeData="0|3^00|全部^01|降级^02|维持^03|晋升 " verify="晋降类型|notnull" 
          ondblclick="showCodeListEx('AssessType',[this,AssessTypeName],[0,1]);" 
          onkeyup="showCodeListKeyEx('AssessType',[this,AssessTypeName],[0,1]);" 
          ><Input name=AssessTypeName class="codename" elementtype=nacessary readonly> 
        </TD>
      </TR>
      <TR class=common>
        <TD class=title>
          <%=tTitleAgent%>编码
        </TD>
        <TD class=title>
          <Input class=common name=AgentCode>
        </TD>
        <TD class=title>
          <%=tTitleAgent%>姓名
        </TD>
        <TD class=title>
          <Input class='common' name=AgentName >
        </TD>
      </TR>
    </table>
    <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class=cssbutton >
    <INPUT VALUE="保  存" TYPE=button onclick="return submitForm();" class=cssbutton >
    <INPUT VALUE="打  印" TYPE=button onclick="return printClick();" class=cssbutton >
		<font color='red'> (“保存”每次只能处理页面上显示的内容) </font>
		<p> <font color="#ff0000" size='5'>注：进行考核调整时，请确认已经做过考核计算和套转考核。 </font></p>
		<!--p><font color='red'>注：人员职级调整不能跨序列调整，业务职级不能与区经理职级相互调整！ </p-->
    <input type=hidden name=BranchType value='<%=BranchType%>'>
    <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
    <input type=hidden name=AgentSeries value='<%=AgentSeries%>'>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssess1);">
    		</td>
    		<td class= titleImg>
    			 考核信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLAAssess1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanEvaluateGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton > 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
