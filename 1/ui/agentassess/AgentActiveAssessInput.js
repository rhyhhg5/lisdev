   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
//代码限制长度
function initEdorType(cObj)
{
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeList('comcode',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeListKey('comcode',[cObj], null, null, mEdorType, "1");
} 
 
//提数操作
function submitForm()
{

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在AgentActiveAssessInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else 
 	{
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行计算         
function AgentActiveAssessSave()
{
  //首先检验录入框
  if(!verifyInput()) return false;
  
  var assessmonth=trim(fm.all('AssessMonth').value);
  if(assessmonth!='03'&&assessmonth!='06'&&assessmonth!='09'&&assessmonth!='12')
  {
  alert("只能进行季度考核，请重新确认考核所属月");
  return false;	
  }
 
  var tReturn = parseManageComLimitlike();
  var tIndexCalNo = trim(fm.all('AssessYear').value)+trim(fm.all('AssessMonth').value);
  var agentseries = trim(fm.all('AgentSeries').value);
  var maxMonth=""; 
   //////////////////////2008-01-15  XX  加上校验 
 //校验是否算过薪资
 var strSQL = "select max(IndexCalNo) from LAWage where 1=1  and state='1' "
             +getWherePart('BranchType')
             +getWherePart('BranchType2')
             +getWherePart('ManageCom');
  var tResult = easyQueryVer3(strSQL,1,1,1);  
  if (!tResult) {
  alert("没有进行薪资计算，无法进行考核计算！");
  return false;
  }
  var tArr = new Array();
  tArr = decodeEasyQueryResult(tResult);
  maxMonth= tArr[0][0];
  if (maxMonth!=tIndexCalNo)
  {
      alert("无法进行该年月的考核，薪资现已计算到"+maxMonth);
      return false;;
  } 
  var strSql = "select indexcalno from laassess where IndexCalNo < '"+tIndexCalNo+"' "
  +"and state='0'"
  +getWherePart('AgentSeries')
  +getWherePart('ManageCom')
  +getWherePart('BranchType')
  +getWherePart('BranchType2');               
//查询SQL，返回结果字符串
	var strResult  = easyQueryVer3(strSql, 1, 1, 1);  
	//判断是否查询成功
	if (strResult) {  	
		tArr = decodeEasyQueryResult(strResult);
		alert("该管理机构在"+tArr[0][0]+"未审核确认或组织归属，不能进行"+tIndexCalNo+"考核计算！");
		return false;
	}  
//校验考核信息
   var strSql = "select agentcode from laassess where IndexCalNo = '"+tIndexCalNo+"' "
              +"and state>='1'"
              +getWherePart('AgentSeries')
              +getWherePart('ManageCom')
              +getWherePart('BranchType')
              +getWherePart('BranchType2');               
  //查询SQL，返回结果字符串
  var strResult  = easyQueryVer3(strSql, 1, 1, 1);  
  //判断是否查询成功
  if (strResult) {  	 
    alert("该职级已审核确认或组织归属，不能进行考核计算！");
    return false;
  }  
  //alert("111111111111");

//  var endDate = trim(fm.all('FirstEndDate').value);
//  if (endDate!='' && endDate!=null)
//  {
//     if (endDate.length != 6)
//     {
//     	alert('第一次考核截止日期录入格式有误!');
//     	return false;
//     }
//     if (endDate.substr(4) != trim(fm.all('AssessMonth').value))
//     {
//       alert('考核指标所属月与第一次考核截止日期不一致！');	
//       return false;
//     }
//  }  
  //修改：2004-05-08 LL
  //修改原因：注释掉校验问题，所有校验放到后台处理
  //var tIndexCalNo = trim(fm.all('AssessYear').value)+trim(fm.all('AssessMonth').value);
  //校验：该年月的佣金计算过而且下月的佣金每算过，才可录入考核信息
//  var strSQL = "select max(distinct IndexCalNo) from LAWage where IndexCalNo = '"+tIndexCalNo+"' "
//              +"and not exists(select * From LAWage Where IndexCalNo > '"+tIndexCalNo+"' "           
//              +getWherePart('BranchType')
//              +getWherePart('BranchType2','BranchType2')
//              +getWherePart('ManageCom')+")"
//              +getWherePart('ManageCom')
//              +getWherePart('BranchType2','BranchType2')
//              +getWherePart('BranchType'); 
  
  
 

////////////////////////////////////////////
/*
  //判断该职级是否已考核过
  var tReturn = parseManageComLimitlike();
  var strSql = "select IndexCalNo from laassess where IndexCalNo = '"+tIndexCalNo+"' "
              +getWherePart('AgentGrade')
              +getWherePart('ManageCom')+tReturn
              +getWherePart('BranchType')
              +" Union "
              +"Select IndexCalNo From LAAssessMain Where IndexCalNo = '"+tIndexCalNo+"' "
              +getWherePart('AgentGrade')
              +getWherePart('ManageCom')
              +getWherePart('BranchType');

  //alert(strSql);	    
  //查询SQL，返回结果字符串
  var strResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (strResult) {  
    alert("该职级已考核过！");
    return false;
  }
  */
  
  
  
  //divAgentQuery.style.display='none'; 
  fm.mOperate.value = 'INSERT||MAIN';
  submitForm();	
}  


function clearHistoryData()
{
  //首先检验录入框
  if(!verifyInput()) return false;
  
  //判断该职级是否已确认归属
  var tReturn = parseManageComLimitlike();
  var tIndexCalNo = trim(fm.all('AssessYear').value)+trim(fm.all('AssessMonth').value);
  var strSql = "select AgentCode from laassess where IndexCalNo = '"+tIndexCalNo+"' "
              +"And State <> '0' "
              +getWherePart('AgentGrade','AgentGrade')
              +getWherePart('ManageCom','ManageCom','like')+tReturn
              +getWherePart('BranchType','BranchType');

  //alert(strSql);	    
  //查询SQL，返回结果字符串
  var strResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (strResult) {  
    alert("该职级已确认归属，不能清除数据重新考核！");
    return false;
  }
  
  fm.mOperate.value = 'DELETE||MAIN';
  submitForm();
}                  


//执行查询
function AgentActiveAssessQuery()
{
 if(trim(fm.all('AssessYear').value)==''||trim(fm.all('AssessMonth').value)==''||trim(fm.all('AgentGrade').value)==''||trim(fm.all('ManageCom').value)=='')
  {
    alert("请填写查询条件!");
    return false;	
  }
 divAgentQuery.style.display='';
 //AgentQueryGrid.clearData();
 initAssessQueryGrid();
 showRecord();
}

//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord()
{
  // 拼SQL语句，从页面采集信息

  var Sql ="select WageNo from LAWageLog where 1=1 "
	           + getWherePart('AssessYear')
	           + getWherePart('AssessMonth');
  var strSql = "select AgentCode,AgentGroup,ManageCom,FirstPension,RearedSdy from LAIndexInfo " 
               +" where IndexCalNo in ("+Sql+")";

  //alert(strSql);	    
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AssessQueryGrid.clearData('AssessQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AssessQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}
//function afterCodeSelect(cCodeName, Field)
//{
//   
//  var value = Field.value;
//  try	
//  {
//     //alert(cCodeName);
//     if( cCodeName == "AgentGrade" )	
//     {
//        //alert("aa:"+value);
//        if (value <= 'A03')
//        {
//           fm.all('FirstBeginDate').disabled = true;
//           fm.all('FirstEndDate').disabled = true;
//        }else if (value > 'A03')
//        {
//           fm.all('FirstBeginDate').disabled = false;
//           fm.all('FirstEndDate').disabled = false;        	
//	}
//     }
//  }
//  catch( ex ) 
//  {
//     alert('代理人职级选择出错!');
//  }
//}