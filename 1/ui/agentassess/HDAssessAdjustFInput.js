//               该文件中包含客户端需要处理的函数和事件
var strRT=""; //存储代理人代码记录
//var strAgentGrade=""; //存储代理人职级
//var arrGrade = new Array();
var arrGrade = "";
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//***************************************************
//* 查询职级信息
//***************************************************
function getAgentGradeStr()
{
  var tReturn = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;

  var sSql = "select gradecode,gradename from laagentgrade where 1=1 ";
     sSql += getWherePart("BranchType");
     sSql += getWherePart("BranchType2");
/*
     sSql += " and gradeproperty6 = '0' ";
	sSql = sSql + " and gradeproperty2 in (select gradeproperty2 from laagentgrade where 1=1 "
	        +getWherePart("BranchType")
	        +getWherePart("BranchType2")
	        +" and gradeproperty6='0') ";
*/
	   sSql += "order by gradecode";

  var tqueryRe = easyQueryVer3(sSql,1,0,1);
  if (tqueryRe)
      tReturn = tqueryRe;

  return tReturn;
}

//***************************************************
//* 查询考核信息
//***************************************************
function easyQueryClick()
{
	//验证必填项
	if(!checkValid())
	{
		return false;
	}
	
	var tSQL = "";
//	
//	tSQL  = "SELECT";
//	tSQL += "    a.AgentCode,";
//	tSQL += "    b.Name,";
//	tSQL += "    a.BranchAttr,";
//	tSQL += "    c.Name,";
//	tSQL += "    a.AgentGrade,";
//	tSQL += "    a.CalAgentGrade,";
//	tSQL += "    a.AgentGrade1,";
//	tSQL += "    d.GradeName";
//	tSQL += " FROM";
//	tSQL += "    LAAssess a,";
//	tSQL += "    LAAgent b,";
//	tSQL += "    LABranchGroup c,";
//	tSQL += "    LAAgentGrade d";
//	tSQL += " WHERE";
//	tSQL += "    a.AgentCode=b.AgentCode AND";
//	tSQL += "    a.AgentGroup=c.AgentGroup AND";
//	tSQL += "    a.AgentGrade1=d.GradeCode ";
//	//tSQL += "    a.indexcalno='200506' AND";
//	tSQL += getWherePart("a.indexcalno","IndexCalNo");
//	//tSQL += "    a.agentcode='1101000056' AND";
//	tSQL += getWherePart("a.agentcode","AgentCode");
//	//tSQL += "    a.ManageCom='86110000'    AND";
//	tSQL += getWherePart("a.ManageCom","ManageCom","like");
//	//tSQL += "    a.BranchAttr='8611000001'";
//	tSQL += getWherePart("a.BranchAttr","BranchAttr");
//	tSQL += getWherePart("a.BranchType","BranchType");
//	tSQL += getWherePart("a.BranchType2","BranchType2");
//	tSQL += getWherePart("b.Name","AgentName");
//	tSQL += " ORDER BY a.BranchAttr,a.AgentGrade";
	
	tSQL = "select a.agentcode,b.Groupagentcode , b.name, a.BranchAttr, "
	+ " (select name from labranchgroup where agentgroup = b.agentgroup),"
	+ " a.AgentGrade, a.CalAgentGrade, a.TurnAgentGrade ,a.TurnFlag, a.AgentGrade1, "
  + " (select gradename from laagentgrade where gradecode=a.agentgrade1) "
  + " FROM LAAssess a ,LAAgent b" 
  + " where "
  + " a.agentcode = b.agentcode "
  + " and a.state = '0'  "
  tSQL += getWherePart("a.indexcalno","IndexCalNo")
  + getWherePart("a.ManageCom","ManageCom","like")
  + getWherePart("a.BranchAttr","BranchAttr")
	+ getWherePart("a.BranchType","BranchType")
	+ getWherePart("a.BranchType2","BranchType2")
	+ getWherePart("b.Groupagentcode","GroupAgentCode");
	if(fm.AssessType.value!='00'){
		tSQL +=getWherePart("a.ModifyFlag","AssessType");
	}
	//业务系列
	  if(fm.all('AgentSeries').value == '0')
	  {
	  		tSQL += " and a.agentgrade like 'U%' ";
	  }
	  else
	  {
	  	
	  	  tSQL += " and a.agentgrade like 'V%' ";
	  }
	
 tSQL=tSQL+getWherePart("b.Name","AgentName")
  + "ORDER BY a.modifyflag,a.BranchAttr,a.AgentGrade" ;
	
	//alert(tSQL);
	//执行查询并返回结果
	turnPage.queryModal(tSQL, EvaluateGrid);
  if (!turnPage.strQueryResult) {
   alert("未查询到需考核调整的信息！");
   return false;
  }

}

//***************************************************
//* 验证必填项
//***************************************************
function checkValid()
{
	if (trim(document.fm.ManageCom.value)=="")
	{
		alert("请填写管理机构!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		return false;
	}
	
	if (document.fm.ManageCom.value.length > 8)
	{
		alert("管理机构输入错误!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		fm.all('ManageCom').focus();
		return false;
	}
	
	if(trim(document.fm.IndexCalNo.value)=="")
	{
		alert("请填写考核年月!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	if (document.fm.IndexCalNo.value.length != 6)
	{
		alert("考核年月输入错误!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	if(trim(document.fm.AssessType.value)=="")
	{
		alert("请填写晋降类型!");
		document.fm.AssessType.value="";
		fm.all('AssessType').focus();
		return false;
	}
	return true;
}

//***************************************************
//* 点击‘保存’进行的操作
//***************************************************
function submitForm()
{
	var tRowCount = EvaluateGrid.mulLineCount;  //取得MulLine的行数
	
  //判断是否有数据需要处理
  if(tRowCount==0)
  {
  	alert("没有数据需要处理！请先查询！");
  	return false;
  }
  
	// 提交画面前的验证
	if (checkValid() == false)
    return false;
    
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.submit(); //提交
  
  return true;
}

//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    //EvaluateGrid.clearData("EvaluateGrid");
  }
}
function printClick(){
	//验证必填项
	if(!checkValid())
	{
		return false;
	}
	var tSQL="";
	 tSQL = "select b.Groupagentcode , b.name, a.BranchAttr, "
		+ " (select name from labranchgroup where agentgroup = b.agentgroup),"
		+ " a.AgentGrade, a.CalAgentGrade,  a.AgentGrade1, "
	  + " (select gradename from laagentgrade where gradecode=a.agentgrade1) "
	  + " FROM LAAssess a ,LAAgent b" 
	  + " where "
	  + " a.agentcode = b.agentcode"
	  + " and a.state = '0'  "
	  tSQL += getWherePart("a.indexcalno","IndexCalNo")
	  + getWherePart("a.ManageCom","ManageCom","like")
	  + getWherePart("a.BranchAttr","BranchAttr")
		+ getWherePart("a.BranchType","BranchType")
		+ getWherePart("a.BranchType2","BranchType2")
		+ getWherePart("b.Groupagentcode","GroupAgentCode");
		if(fm.AssessType.value!='00'){
			tSQL +=getWherePart("a.ModifyFlag","AssessType");
		}
		
		//业务系列
		  if(fm.all('AgentSeries').value == '0')
		  {
		  		tSQL += " and a.agentgrade like 'U%' ";
		  }
		  else
		  {
		  	
		  	  tSQL += " and a.agentgrade like 'V%' ";
		  }
		
	 tSQL=tSQL+getWherePart("b.Name","AgentName")
	  + "ORDER BY a.modifyflag,a.BranchAttr,a.AgentGrade" ;
	 var tResult = easyQueryVer3(tSQL,1,0,1);
		if(tResult==null||tResult==""){
			alert("下载失败，没有相关信息");
			return false;
		}
	fm.all("downSQL").value = tSQL; 
	fm.action='HDActiveAgentAssessList.jsp';
	fm.target='f1print';
	//alert('here');
	fm.submit();
}
