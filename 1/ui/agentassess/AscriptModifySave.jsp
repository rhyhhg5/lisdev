<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：AssessConfirmSave.jsp
//程序功能：
//创建日期：2003-3-26
//创建人  ：zsj
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>

<%
  //输出参数
  String FlagStr = "";
  String Content = "";

  AscriptModLAAssess tModify = new AscriptModLAAssess();
  VData tVData = new VData();

  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  System.out.println("ManagCom : " + tGI.ManageCom);
  System.out.println("Operator : " + tGI.Operator);
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tIndexCalNo = request.getParameter("IndexCalNo");
      System.out.println("--tIndexCalNo--"+tIndexCalNo);
      String tAgentCode[] = request.getParameterValues("AssessGrid1");      
      String tAgentGrade[] = request.getParameterValues("AssessGrid5");
      String tAgentGrade1[] = request.getParameterValues("AssessGrid6");
      String tAgentGroup[] = request.getParameterValues("AssessGrid3");
      int tCount = tAgentCode.length;
      int index = 0;

      System.out.println("----from grid--tCount = " + tCount);
      LAAssessSchema tLAAssessSchema = new LAAssessSchema();
      LAAssessSet    tLAAssessSet = new LAAssessSet();
      tLAAssessSet.clear();

      for ( index=0;index<tCount;index++ )  {
       LAAssessDB tDB = new LAAssessDB();
       tDB.setAgentCode(tAgentCode[index]);
       tDB.setIndexCalNo(tIndexCalNo);
       System.out.println("---fromGrid--" + index + "---AgentCode : " + tAgentCode[index]);
       if( !tDB.getInfo() )  {
         FlagStr = "Fail";
         Content = FlagStr + "查询失败，原因是：" + tDB.mErrors.getFirstError();
       }
       tLAAssessSchema = tDB.getSchema();
       System.out.println("tLAAssessSchema.AgentGrade1 : " + tLAAssessSchema.getAgentGrade1());
       System.out.println("---company:agentgrade1 = " + tAgentGrade1[index]);
       System.out.println("---company:agentgrade = " + tAgentGrade[index]);       
       System.out.println("---company:agentgrade = " + tAgentGroup[index]);       
       tLAAssessSchema.setAgentGrade1(tAgentGrade1[index]);
       tLAAssessSchema.setAgentGrade(tAgentGrade[index]);
       tLAAssessSchema.setAgentGroup(tAgentGroup[index]);
       

       String tSeries  = "";
       String tSeries1 = "";
       if ( tAgentGrade1[index].trim().equals("00") )
         tSeries1 = "0";
       
       if ( tAgentGrade[index].trim().equals("A01") ||
            tAgentGrade[index].trim().equals("A02") ||
            tAgentGrade[index].trim().equals("A03") )
         tSeries = "A";
       if ( tAgentGrade1[index].trim().equals("A01") ||
            tAgentGrade1[index].trim().equals("A02") ||
            tAgentGrade1[index].trim().equals("A03") )
         tSeries1 = "A";

       if ( tAgentGrade[index].trim().equals("A04") ||
            tAgentGrade[index].trim().equals("A05") )
         tSeries = "B";
       if ( tAgentGrade1[index].trim().equals("A04") ||
            tAgentGrade1[index].trim().equals("A05") )
         tSeries1 = "B";

       if ( tAgentGrade[index].trim().equals("A06") ||
            tAgentGrade[index].trim().equals("A07") )
         tSeries = "C";
       if ( tAgentGrade1[index].trim().equals("A06") ||
            tAgentGrade1[index].trim().equals("A07") )
         tSeries1 = "C";

       if ( tAgentGrade[index].trim().equals("A08") ||
            tAgentGrade[index].trim().equals("A09") )
         tSeries = "D";
       if ( tAgentGrade1[index].trim().equals("A08") ||
            tAgentGrade1[index].trim().equals("A09") )
         tSeries1 = "D";
       tLAAssessSchema.setAgentSeries1(tSeries1);
       tLAAssessSchema.setAgentSeries(tSeries);
       System.out.println("---company:Series1 = " + tSeries1);
       System.out.println("---company:Series = " + tSeries);
       tLAAssessSet.add(tLAAssessSchema);
      }

      tVData.clear();
      tVData.add(tGI);
      tVData.add(tLAAssessSet);
      System.out.println("VData.size---" + tVData.size());

      if ( !tModify.dealData(tVData) )  {
        FlagStr = "Fail";
        Content = FlagStr + "保存失败，原因是：" + tModify.mErrors.getFirstError();
        System.out.println("Content:  " + Content);
      }
      else  {
        FlagStr = "Succ";
        Content = FlagStr + "保存成功！";
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "保存失败，原因是：" + ex.toString();
    }
  }// tGI!=null
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
