<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>  
<%
//程序名称：AgentAssessInput.jsp
//程序功能：
//创建日期：2003-03-01 
//创建人  ：LL
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="LAEmployeeAssessInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAEmployeeAssessInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form action="./LAEmployeeAssessSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		代理人员工制考核计算
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='code' name=ManageCom verify="管理机构|notnull&code:comcode"
                                 ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);" >
          </TD>
  	  <TD  class= title>
            考核代码
          </TD>
          <TD  class= input>
            <Input class=common  name=IndexCalNo verify="考核代码|NOTNULL&INT" >(例:200401)
          </TD>
        </TR>
        <TR class=input>     
         <TD class=common>
          <input type =button class=common value="计算" onclick="EmployeeAssessSave();">     
         </TD>   
         <!--TD class=common>
          <input type =button class=common value="清除数据" onclick="clearHistoryData();">   
         </TD-->
          <Input type=hidden name=mOperate value="">
        </TR>          
      </table>
    </Div>  
    <input type=hidden name=BranchType value=''> 	     
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>