//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	if(!verifyInput()) 
  {
  	 return false;	  
  }
  
	if (mOperate=="")
	{
		addClick();
	}	 
	
  if (!beforeSubmit())
    return false; 
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
   
   fm.submit(); //提交

 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  mOperate="";
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  initForm();
    
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    initForm();
  }
  catch(re)
  {
    alert("在LARewardPunish.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if(!verifyInput()) 
  return false;
  if (fm.all('AgentCode').value == '')
  {
    alert("请输入育成人编码！");
    fm.all('AgentCode').focus();
    return false;	  
  }
  if (fm.all('RearAgentCode').value == '')
  {
    alert("请输入被育成人编码！");
    fm.all('RearAgentCode').focus();
    return false;	  
  }
   if (fm.all('StartDate').value == '')
  {
    alert("请输入育成起期！");
    fm.all('StartDate').focus();
    return false;	  
  }
   if (fm.all('RearFlag').value == '')
  {
    alert("请输入有效状态");
    fm.all('RearFlag').focus();
    return false;	  
  }
  if (fm.all('ManageCom').value != fm.all('RearManageCom').value)
  {
    alert("不能跨机构建立育成关系");
    fm.all('ManageCom').focus();
    fm.all('RearManageCom').focus();
    return false;	  
  }
  if (fm.all('AgentCode').value == fm.all('RearAgentCode').value)
  {
    alert("不能建立自己与自己的育成关系");
    fm.all('AgentCode').focus();
    fm.all('RearAgentCode').focus();
    return false;	  
  }
  //alert(mOperate);
   if(mOperate =='INSERT||MAIN'){
  	 var strSql  = "select count(*) from LARearRelation where AgentCode='"
		  + fm.all("AgentCode").value + "' and rearflag='1' and enddate>='"+fm.all("StartDate").value+"'" ;
	  var sResult = easyExecSql(strSql);	 
	  if(sResult && sResult[0][0] >0 ){
	  	 alert("业务员"+fm.all("AgentCode").value+"已经存在了有效的被育成关系，不能再次建立有效的被育成关系！");
	  	 return false;
	  }  
	  
	  var strSql1  = "select count(*) from LARearRelation where RearAgentCode='"
		  + fm.all("AgentCode").value + "' and rearflag='1' and enddate>='"+fm.all("StartDate").value+"'" ;
	  var sResult1 = easyExecSql(strSql1);	 
	  if(sResult1 && sResult1[0][0] >0 ){
	  	 alert("业务员"+fm.all("AgentCode").value+"已经存在了有效的育成关系，不能再次建立有效的育成关系！");
	  	 return false;
	  }  
	
	 var strSql2  = "select count(*) from LARearRelation where AgentCode='"
		  + fm.all("RearAgentCode").value + "' and rearflag='1' and enddate>='"+fm.all("StartDate").value+"'" ;
	  var sResult2 = easyExecSql(strSql2);	 
	  if(sResult2 && sResult2[0][0] >0 ){
	  	 alert("业务员"+fm.all("RearAgentCode").value+"已经存在了有效的被育成关系，不能再次建立有效的被育成关系！");
	  	 return false;
	  }  
	  
//	  var strSql3  = "select count(*) from LARearRelation where RearAgentCode='"
//		  + fm.all("RearAgentCode").value + "' and rearflag='1' and enddate>='"+fm.all("StartDate").value+"'" ;
//	  var sResult3 = easyExecSql(strSql3);	 
//	  if(sResult3 && sResult3[0][0] >0 ){
//	  	 alert("业务员"+fm.all("RearAgentCode").value+"已经存在了有效的育成关系，不能再次建立有效的育成关系！");
//	  	 return false;
//	  }    
  }
  return true	;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN"; 
 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert('请先查询出要修改的纪录！');
  else if ((fm.all("RearLevel").value==null)||(fm.all("RearLevel").value==''))
    alert('请先查询出要修改的纪录！');
  else if ((fm.all("RearedGens").value==null)||(fm.all("RearedGens").value==''))
    alert('请先查询出要修改的纪录！');
  else if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";    
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LAGrpRearQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
  
}   
function queryClicksure()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  fm.submit();
  
}              

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert('请先查询出要删除的纪录！');
  else if ((fm.all("RearLevel").value==null)||(fm.all("RearLevel").value==''))
    alert('请先查询出要删除的纪录！');
  else if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function checkvalid()
{
  var strSQL = "";
  if(fm.GroupAgentCode.value !=null && fm.GroupAgentCode.value != ""){
  	var tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+fm.GroupAgentCode.value+"' ";
  	var strQueryResult = easyExecSql(tYAgentCodeSQL);
  	if(strQueryResult == null){
  		alert("获取业务员信息失败！");
  		return false;
  	}
  	fm.AgentCode.value = strQueryResult[0][0];
  }
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select a.agentcode,a.agentgroup,a.managecom,a.name,a.agentstate "
       +" from LAAgent a,LATree b "
       +" where a.agentcode=b.agentcode  and a.agentcode ='"+fm.all('AgentCode').value+"' and b.agentgrade>='E01'"
	     + getWherePart('a.BranchType','BranchType')
	     + getWherePart('a.BranchType2','BranchType2');
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('AgentCode').value =  "";
    fm.all('BranchAttr').value = "";
    fm.all('BranchName').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('AgentState').value  = "";
    fm.all('AgentName').value  = "";
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("主管职级无此代理人！");
     fm.all('AgentCode').value = "";
    fm.all('BranchAttr').value = "";
    fm.all('BranchName').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('AgentState').value  = "";
    fm.all('AgentName').value  = "";
    return;
  }
  //查询成功则拆分字符串，返回二维数组
  
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult); 
  
  
  fm.all('AgentGroup').value  =tArr[0][1];
  fm.all('ManageCom').value  = tArr[0][2];
  fm.all('AgentName').value = tArr[0][3];
  fm.all('AgentState').value  = tArr[0][4];

  strSQL_AgentGroup = "select BranchAttr,name,branchlevel from labranchgroup where 1=1 "
                     +"and AgentGroup='"+fm.all('AgentGroup').value+"'"
  var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  if (!strQueryResult_AgentGroup)
  {
  	alert("没有查询到被育成人"+fm.all('AgentCode').value+"对应的销售单位信息!");
  	return false;
  }
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  
  fm.all('BranchAttr').value = tArr_AgentGroup[0][0];
  fm.all('BranchName').value = tArr_AgentGroup[0][1];
  fm.all('RearLevel').value = tArr_AgentGroup[0][2];
  
 }


function checkRear()
{
  var strSQL = "";
  if(fm.RearGroupAgentCode.value !=null && fm.RearGroupAgentCode.value != ""){
  	var tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+fm.RearGroupAgentCode.value+"' ";
  	var strQueryResult = easyExecSql(tYAgentCodeSQL);
  	if(strQueryResult == null){
  		alert("获取业务员信息失败！");
  		return false;
  	}
  	fm.RearAgentCode.value = strQueryResult[0][0];
  }
  if (getWherePart('RearAgentCode')!='')
  {
     strSQL = "select a.agentcode,a.agentgroup,a.managecom,a.name,a.agentstate "
       +" from LAAgent a,LATree b "
       +" where a.agentcode=b.agentcode  and a.agentcode ='"+fm.all('RearAgentCode').value+"' and b.agentgrade>='E01'"
	     + getWherePart('a.BranchType','BranchType')
	     + getWherePart('a.BranchType2','BranchType2');
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('RearAgentCode').value =  "";
    fm.all('RearBranchAttr').value = "";
    fm.all('RearBranchName').value = "";
    fm.all('RearManageCom').value  = "";
    fm.all('RearAgentState').value  = "";
    fm.all('RearAgentName').value  = "";
    return    false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("主管职级无此代理人！");
    fm.all('RearAgentCode').value =  "";
    fm.all('RearBranchAttr').value = "";
    fm.all('RearBranchName').value = "";
    fm.all('RearManageCom').value  = "";
    fm.all('RearAgentState').value  = "";
    fm.all('RearAgentName').value  = "";
    return  false;
  }
  //查询成功则拆分字符串，返回二维数组
  
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult); 
  
  
  fm.all('RearAgentGroup').value  =tArr[0][1];
  fm.all('RearManageCom').value  = tArr[0][2];
  fm.all('RearAgentName').value = tArr[0][3];
  fm.all('RearAgentState').value  = tArr[0][4];

  strSQL_AgentGroup = "select BranchAttr,name  from labranchgroup where 1=1 "
                     +"and AgentGroup='"+fm.all('RearAgentGroup').value+"'"
  var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  if (!strQueryResult_AgentGroup)
  {
  	alert("没有查询到育成人"+fm.all('RearAgentCode').value+"对应的销售单位信息!");
  	return false;
  }
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  
  fm.all('RearBranchAttr').value = tArr_AgentGroup[0][0];
  fm.all('RearBranchName').value = tArr_AgentGroup[0][1];
  
 }



function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
    fm.all('AgentCode').value = arrResult[0][0]; 
    fm.all('AgentName').value = arrResult[0][1]; 
    fm.all('BranchAttr').value = arrResult[0][2]; 
    fm.all('BranchName').value = arrResult[0][3]; 
    fm.all('AgentState').value = arrResult[0][4];        
    fm.all('ManageCom').value = arrResult[0][5]; 
    fm.all('AgentGroup').value = arrResult[0][6];      
    fm.all('RearAgentCode').value = arrResult[0][7]; 
    fm.all('RearAgentName').value = arrResult[0][8]; 
    fm.all('RearBranchAttr').value = arrResult[0][9]; 
    fm.all('RearBranchName').value = arrResult[0][10]; 
    fm.all('RearAgentState').value = arrResult[0][11];   
    fm.all('RearManageCom').value = arrResult[0][12]; 
    fm.all('RearAgentGroup').value = arrResult[0][13];  
    fm.all('StartDate').value = arrResult[0][14]; 
    fm.all('RearFlag').value = arrResult[0][15]; 
    fm.all('RearLevel').value = arrResult[0][16]; 
    fm.all('RearedGens').value = arrResult[0][17];  
    fm.all('RearFlagName').value = arrResult[0][18];  
    fm.all('GroupAgentCode').value = arrResult[0][19]; 
    fm.all('RearGroupAgentCode').value = arrResult[0][20]; 
    fm.all('AgentCode').readOnly=true;  
    fm.all('RearAgentCode').readOnly=true;
    fm.all('StartDate').readOnly=true;                                                                                                                                                                                                                                          	
  }
     
}

/***************************************
 * 验证字符的字节长度是否超标
 * 参数：pmStr      准备验证的字符串
 *       pmStrLen   长度标准
 *       pmMissage  报错信息
 * 返回：返回字符串字节长度
 ***************************************/
function strLen(pmStr,pmStrLen,pmMissage)
{
	var i;
  var len;
  len = 0;
  for (i=0;i<pmStr.length;i++)
  {
    if (pmStr.charCodeAt(i)>255) len+=3; else len++;
  }
  //alert("[" + pmStr + "] 长度为： " + len + " 规定长度为："+pmStrLen);
  if(len > pmStrLen)
  {
  	alert(pmMissage);
  	return false;
  }
  
  return true;
}