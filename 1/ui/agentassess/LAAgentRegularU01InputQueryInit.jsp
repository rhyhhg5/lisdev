<%
//程序名称：AdjustAssessInit.jsp
//程序功能：
//创建日期：2015-01-13 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = <%=tG.ManageCom%>;
	var sql="select name from ldcom where comcode ='"+<%=tG.ManageCom%>+"'";
	var array=easyExecSql(sql);
	fm.all('ManageComName').value =array[0][0];
  	fm.all('employdate').value = '';
    fm.all('groupAgentcode').value = ''; 
    fm.all('agentName').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('AgentGradeName').value = ''; 
    fm.all('indueformflag').value = '';
    fm.all('indueformflagName').value = '';     
    
  }
  catch(ex)
  {
    alert("在LAAgentRegularU01InputQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initWageGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="营销员编码"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="营销员姓名"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="管理机构"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许
    

    iArray[4]=new Array();
    iArray[4][0]="销售机构"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    
    iArray[5]=new Array();
    iArray[5][0]="销售机构名称"; //列名
    iArray[5][1]="100px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
  
    
    iArray[6]=new Array();
    iArray[6][0]="考核指标代码"; //列名
    iArray[6][1]="30px";        //列宽
    iArray[6][2]=100;            //列最大值  
    iArray[6][3]=3;              //是否允许输入,1表示允许,0表示不允许
    
    
    iArray[7]=new Array();
    iArray[7][0]="考核指标"; //列名
    iArray[7][1]="100px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
  	iArray[8]=new Array();
    iArray[8][0]="营销员职级代码"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
   iArray[9]=new Array();
   iArray[9][0]="营销员职级"; //列名
   iArray[9][1]="80px";        //列宽
   iArray[9][2]=100;            //列最大值
   iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许
   //
   iArray[10]=new Array();
   iArray[10][0]="入司时间"; //列名
   iArray[10][1]="80px";        //列宽
   iArray[10][2]=100;            //列最大值
   iArray[10][3]=0;              //是否允许输入,1表示允许,0表示不允许  
    
   iArray[11]=new Array();
   iArray[11][0]="代理人核心编码"; //列名
   iArray[11][1]="0px";        //列宽
   iArray[11][2]=100;            //列最大值
   iArray[11][3]=0;              //是否允许输入,1表示允许,0表示不允许  

    iArray[12]=new Array();
   iArray[12][0]="转正状态代码"; //列名
   iArray[12][1]="30px";        //列宽
   iArray[12][2]=100;            //列最大值
   iArray[12][3]=3;              //是否允许输入,1表示允许,0表示不允许  
   
   
   iArray[13]=new Array();
   iArray[13][0]="转正状态"; //列名
   iArray[13][1]="80px";        //列宽
   iArray[13][2]=100;            //列最大值
   iArray[13][3]=0;              //是否允许输入,1表示允许,0表示不允许  
   
    WageGrid = new MulLineEnter( "fm" , "WageGrid" );
    WageGrid.mulLineCount = 10;   
    WageGrid.displayTitle = 1;
    WageGrid.hiddenPlus = 1;
    WageGrid.hiddenSubtraction = 1;
    WageGrid.loadMulLine(iArray);
    WageGrid.canChk =0; //0:是单选框  1：是复选框 
    WageGrid.canSel=1;
    WageGrid.locked=1;
    WageGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在LAAgentRegularU01InputQueryInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
    initWageGrid();    
  }
  catch(re)
  {
    alert("在LAAgentRegularU01InputQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>