<%
//程序名称：LAEmployeeAscriptionInit.jsp
//程序功能：
//创建日期：2003-03-03
//创建人  ：LL
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
     fm.all('ManageCom').value = '';
     fm.all('IndexCalNo').value = '';
     fm.all('AgentGrade1').value = '';
    
  }
  catch(ex)
  {
    alert("在LAEmployeeAscriptionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initBeforeGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="代理人编码"; //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许


    iArray[2]=new Array();
    iArray[2][0]="代理人组别"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许    

    iArray[3]=new Array();
    iArray[3][0]="原待遇级别"; //列名
    iArray[3][1]="120px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="建议待遇级别"; //列名
    iArray[4][1]="120px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="确认待遇级别"; //列名
    iArray[5][1]="120px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许

    BeforeGrid = new MulLineEnter( "fm" , "BeforeGrid" );
    BeforeGrid.canChk = 0;
    BeforeGrid.mulLineCount = 0;
    BeforeGrid.displayTitle = 1;
    BeforeGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在LAEmployeeAscriptionInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
    initBeforeGrid();    
  }
  catch(re)
  {
    alert("LAEmployeeAscriptionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>