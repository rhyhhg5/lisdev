<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：EmployeeDocumentIn.jsp
//程序功能：实现档案管理
//创建日期：2004-03-29
//创建人  ：LL
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.agentassess.*"%>

<%
// 输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见logonSubmit.jsp
  //tGI.ManageCom="8611";
  //tGI.Operator="Admin";
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆"; 
  }
  else //页面有效
  {
   String tOperate = request.getParameter("mOperate");
   String tManageCom  = request.getParameter("ManageCom");	

   VData tVData = new VData();
   tVData.addElement(tGI);
   tVData.addElement(tManageCom);
   
   //准备往后台传数据
   LAEmployeeDocManUI tLAEmployeeDocManUI = new LAEmployeeDocManUI();
   //处理档案 全部调入 或 全部调出情况
   if( tOperate.trim().equals("INALL||MAIN") || tOperate.trim().equals("OUTALL||MAIN"))
   {
     tLAEmployeeDocManUI.submitData(tVData,tOperate);  
     //如果在Catch中发现异常，则不从错误类中提取错误信息
     try
     {
       tError = tLAEmployeeDocManUI.mErrors;
       if (!tError.needDealError())
       {                          
         Content = " 保存成功! ";
         FlagStr = "Succ";
       }
       else                                                                           
       {
         Content = " 失败，原因:" + tError.getFirstError();
         FlagStr = "Fail";
       }
     } 
     catch(Exception ex)
     {
       Content = " 失败，原因:" + ex.toString();
       FlagStr = "Fail";    
     }
   }
   //处理档案 部分调入 或 部分调出情况,只对选种的人员进行相关处理
   else if ( tOperate.trim().equals("INPART||MAIN") || tOperate.trim().equals("OUTPART||MAIN"))
   {
     String tChk[] = request.getParameterValues("InpAgentInfoGridChk");
     String tAgentCode[] = request.getParameterValues("AgentInfoGrid1");
     int tCount = tAgentCode.length;
     LAAgentSet tLAAgentSet = new LAAgentSet();
     LAAgentSchema tLAAgentSchema;
     
     int tR = 0;
     int tIndex = 0;
     //保存选种人员的代理人编码
     for ( tIndex=0;tIndex<tCount;tIndex++ )
     {
        if (tChk[tIndex].equals("1") )
        {
          tLAAgentSchema = new LAAgentSchema();
          tLAAgentSchema.setAgentCode(tAgentCode[tIndex]);
          tLAAgentSet.add(tLAAgentSchema);
        }
     }
     //如果没有选种对象则不用向后台提交数据
     if ( tLAAgentSet.size() == 0 )
     {
       FlagStr = "警告：";
       Content = FlagStr + "没有被选种的代理人！";
     }
     else //有选种对象则向后台提交数据
     {
       tVData.addElement(tLAAgentSet);
       tLAEmployeeDocManUI.submitData(tVData,tOperate);
       //处理出错情况
       try
       {
         tError = tLAEmployeeDocManUI.mErrors;
         if (!tError.needDealError())
         {                          
           Content = " 保存成功! ";
           FlagStr = "Succ";
         }
         else                                                                           
         {
           Content = " 失败，原因:" + tError.getFirstError();
           FlagStr = "Fail";
         }
       } 
       catch(Exception ex)
       {
         Content = " 失败，原因:" + ex.toString();
         FlagStr = "Fail";    
       }
     } // end of else
     
   }
                    
}//页面有效区

%> 
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>