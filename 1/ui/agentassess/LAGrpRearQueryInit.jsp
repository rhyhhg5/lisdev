<%
//程序名称：LAGrpRearQueryInit.jsp
//程序功能：
//创建日期：2009-01-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
    fm.all('ManageCom').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';
    fm.all('BranchAttr').value = '';
    fm.all('BranchName').value = '';
    fm.all('RearAgentCode').value = '';
    fm.all('RearAgentName').value = '';
    fm.all('RearBranchAttr').value = '';
    fm.all('RearBranchName').value = '';
    fm.all('StartDate').value = '';
    fm.all('RearFlag').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';
    
  }
  catch(ex)
  {
    alert("在LAGrpRearQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

                                

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox();   
    initRewardPunishGrid();
  }
  catch(re)
  {
    alert("在LAAddSubQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化RewardPunishGrid
 ************************************************************
 */
function initRewardPunishGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="被育成人代码";         //列名
        iArray[1][1]="100px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="被育成人姓名";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="育成人代码";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="育成人姓名";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="育成起期";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="有效状态";         //列名
        iArray[6][1]="100px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

      
        iArray[7]=new Array();
        iArray[7][0]="层级";         //列名
        iArray[7][1]="0px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="代数";         //列名
        iArray[8][1]="0px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        
        
        RewardPunishGrid = new MulLineEnter( "fm" , "RewardPunishGrid" ); 

        //这些属性必须在loadMulLine前
        RewardPunishGrid.mulLineCount = 0;   
        RewardPunishGrid.displayTitle = 1;
        RewardPunishGrid.hiddenPlus = 1;
        RewardPunishGrid.hiddenSubtraction = 1;
        RewardPunishGrid.locked=1;
        RewardPunishGrid.canSel=1;
        RewardPunishGrid.canChk=0;
        RewardPunishGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化RewardPunishGrid时出错："+ ex);
      }
    }


</script>