<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String UpDown=request.getParameter("UpDown");
%>
<script>
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LAAssessInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAAssessInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <title>人员职级异动 </title>   
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAssessSave.jsp" method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
	  <td class= titleImg align= center>请输入异动信息的查询条件：</td>
	</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD> 
          
          <TD  class= title>
            考核年月
          </TD>
          <TD  class= input>
            <Input class=common name=IndexCalNo verify="考核年月|notnull&len=6&int" onchange="return changeYearMonth();" elementtype=nacessary>
          </TD>          
        </TR>
      	<TR  class= common>
      	<TD  class= title>
            <%=tTitleAgent%>职级 
          </TD>
          <TD  class= input>
            <Input class="codeno" name=AgentGrade  
            ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
            onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
            ><Input class=codename name=AgentGradeName readOnly elementtype=nacessary>
          </TD>
        </TR>
    </table>
    <input type=hidden name=BranchType value='<%=BranchType%>'>
    <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
    <input type=hidden name=UpDown value='<%=UpDown%>'>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAssess1);">
    		</td>
    		<td class= titleImg>
    			 异动信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLAAssess1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLAAssessGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton > 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
      <p>
      <INPUT VALUE="保  存" TYPE=button onclick="return submitForm();" class=cssbutton > 
      </p>
      </div>
      <Div  id= "divQQQQ" style= "display: ''">			
      <p>
      <INPUT VALUE="组织归属" TYPE=button name=AscBtn onclick="ascriptionClick();" class=cssbutton > 
      </p>				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
