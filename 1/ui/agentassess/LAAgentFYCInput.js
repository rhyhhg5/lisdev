var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var len = manageCom.length;
var querySQL="";
var lenh = "";
var sumlenh = "";
function query(){
	if(!verifyInput2()){
		return false;
	}
	
	if(fm.ManageComHierarchy.value == null||fm.WageNo.value == null||fm.ManageComHierarchy.value == ""||fm.WageNo.value == ""){
		alert("当前薪资月和管理机构层级不能为空");
		return false;
	}
	
	if(len=="2"){
		if(fm.ManageComHierarchy.value=="总公司"){
			lenh = "2";
			sumlenh = "2";
		}
		if(fm.ManageComHierarchy.value=="省公司"){
			lenh = "4";
			sumlenh="2";
		}
		if(fm.ManageComHierarchy.value=="中支公司"){
			lenh = "8";
			sumlenh="2";
		}
	}
	if(len=="4"){
		if(fm.ManageComHierarchy.value=="总公司"){
			alert("省公司不能选择总公司进行查询下载！");
			return false; 
		}
		if(fm.ManageComHierarchy.value=="省公司"){
			lenh = "4";
			sumlenh="4";
		}
		if(fm.ManageComHierarchy.value=="中支公司"){
			lenh = "8";
			sumlenh="4";
		}
	}
	if(len=="8"||len=="6"){
		if(fm.ManageComHierarchy.value=="总公司"){
			alert("中支公司不能选择总公司进行查询下载！");
			return false; 
		}
		if(fm.ManageComHierarchy.value=="省公司"){
			alert("中支公司不能选择省公司进行查询下载！");
			return false; 
		}
		if(fm.ManageComHierarchy.value=="中支公司"){
			lenh = "8";
			sumlenh="8";
		}
	}
	
	var Month;
	if((fm.WageNo.value).substr(4,5)>9){
		Month=(fm.WageNo.value).substr(4,5);
	}else{
		Month=(fm.WageNo.value).substr(5,5);
	}
	var YM;
	if(parseInt(fm.MonthNum.value)>parseInt(Month)){
		YM=parseInt(fm.WageNo.value)+(12-parseInt(Month))-100-(parseInt(fm.MonthNum.value)-parseInt(Month))+1;	
	}else{
		YM=parseInt(fm.WageNo.value)-parseInt(fm.MonthNum.value)+1;			
	}
	//alert(YM);
	var AYM;
	var Mon;
	if(parseInt(String(YM).substr(4,5))>9){
		Mon=String(YM).substr(4,5);
	}else{
		Mon=String(YM).substr(5,5);
	}
	if(parseInt(Mon)<3){
		AYM=parseInt(YM)+(12-parseInt(Mon))-100;
	}else if(parseInt(Mon)>=3&&parseInt(Mon)<6){
		AYM=String(YM).substr(0,4)+'03';
	}else if(parseInt(Mon)>=6&&parseInt(Mon)<9){
		AYM=String(YM).substr(0,4)+'06';
	}else if(parseInt(Mon)>=9&&parseInt(Mon)<12){
		AYM=String(YM).substr(0,4)+'09';
	}else {
		AYM=String(YM).substr(0,4)+'12';
	}
//	alert(AYM);
	var tSQL="select 1 from lacommision where agentcode=a.agentcode and managecom like '"+manageCom+"%' and branchtype='1' and branchtype2='01' and " +
			"wageno between '"+YM+"' and '"+fm.WageNo.value+"'and payyear =0  group by agentcode having sum(fyc)>0 ";	
	if(fm.AgentCode.value!='' && fm.AgentCode.value!=null){
		tSQL  +=" and agentcode =getAgentCode('"+fm.AgentCode.value+"')";
	}
	var tEmployDate = String(YM).substr(0,4)+"-"+String(YM).substr(4,5)+"-01";
//	alert(tEmployDate);
	querySQL="select substr(i.b,1,4),(select name from ldcom where comcode=substr(i.b,1,4)), "+
		 	 "i.b,(select name from ldcom where comcode=i.b), "+
			 "(select BranchAttr from labranchgroup where EndFlag='N' and agentgroup= substr(i.c,1,12)),(select name from labranchgroup where EndFlag='N' and agentgroup=substr(i.c,1,12)), "+
		 	 "(select BranchAttr from labranchgroup where EndFlag='N' and agentgroup= substr(i.c,14,12)),(select name from labranchgroup where EndFlag='N' and agentgroup=substr(i.c,14,12)), "+
		 	 "(select BranchAttr from labranchgroup where EndFlag='N' and agentgroup= substr(i.c,27,12)),(select name from labranchgroup where EndFlag='N' and agentgroup=substr(i.c,27,12)), "+
			 "i.d,(case when (i.d<>'' and i.d is not null) then (select name||'组' from laagent where agentcode in (select agentcode from latree where agentgrade ='B01' and  agentgroup2=i.d)) else '' end), "+
		 	 "(select groupagentcode from laagent where agentcode=i.f),(select name from laagent where agentcode=i.f),'"+fm.MonthNum.value+"', "+
		 	 "i.g,(select gradename from laagentgrade where gradecode=i.g)," +
		 	 "i.h,(select gradename from laagentgrade where gradecode=i.h) "+
			 "from (select a.managecom b, "+
		     "		 		(select BranchSeries from labranchgroup where agentgroup=a.agentgroup) c, "+	
		     "		     	(select agentgroup2 from latree where agentcode=a.agentcode) d, "+
			 "				 a.agentcode f," +
			 "				(select agentgrade from laassess where agentcode=a.agentcode and indexcalno='"+AYM+"') g," +
			 "				(select agentgrade1 from laassess where agentcode=a.agentcode and state='2' and indexcalno='"+AYM+"') h "+
			 "		from laagent a where a.managecom like '"+manageCom+"%' and a.employdate <='"+tEmployDate+"' and a.branchtype='1' and a.branchtype2='01' and a.agentstate in ('01','02') and not exists ("+tSQL+") " +
			 "		 ) i ";			
	turnPage.queryModal(querySQL,MonthAudGaGrid);
}

function download(){
	fm.lenh.value = lenh;
	fm.querySql.value = querySQL;
	if(fm.querySql.value!=null && fm.querySql.value!=""){
		var formAction = fm.action;
		fm.action = "LAAgentFYCDownload.jsp";
		fm.submit();
		fm.target = "fraSubmit";
		fm.action = formAction;
	}else{
		alert("请先进行查询！");
		return;
	}
}