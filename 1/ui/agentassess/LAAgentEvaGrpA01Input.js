//               该文件中包含客户端需要处理的函数和事件
var strRT=""; //存储代理人代码记录
//var strAgentGrade=""; //存储代理人职级
//var arrGrade = new Array();
var arrGrade = "";
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//xqq  2014-11-27
function checkAgentCode(){
var sql=" select agentcode  from laagent   where  1=1 "
            + getWherePart("GroupAgentCode","GroupAgentCode")
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
}

//***************************************************
//* 查询职级信息
//***************************************************
function getAgentGradeStr()
{
  var tReturn = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;

  var sSql = "select gradecode,gradename from laagentgrade where 1=1 ";
     sSql += getWherePart("BranchType");
     sSql += getWherePart("BranchType2")
     sSql += "and gradecode like 'D%' ";
	   sSql += "order by gradecode";
	   
  var tqueryRe = easyQueryVer3(sSql,1,0,1);
  if (tqueryRe)
      tReturn = tqueryRe;

  return tReturn;
}

//***************************************************
//* 查询考核信息
//***************************************************
function easyQueryClick()
{
	//验证必填项
	if(!checkValid())
	{
		return false;
	}
	
	var tSQL = "";
	
	tSQL  = "SELECT"; 
	tSQL += " getUniteCode(a.AgentCode),";
	tSQL += " b.Name,";
	tSQL += " c.BranchAttr,";
	tSQL += " c.Name,";
	tSQL += " a.AgentGrade,";	
	tSQL += " a.CalAgentGrade,";
	tSQL += " (SELECT GradeName FROM LAAgentGrade WHERE GradeCode= a.CalAgentGrade)";
	tSQL += " ,CalAgentGrade ,b.agentcode ";//为了比较
	tSQL += " FROM";
	tSQL += " LAAssess a,";
	tSQL += " LAAgent b,";
	tSQL += " LABranchGroup c,";
	tSQL += " LAAgentGrade d";
	tSQL += " WHERE";	
	tSQL += " a.AgentCode=b.AgentCode AND";
	tSQL += " a.AgentGroup=c.AgentGroup AND";
	tSQL += " a.CalAgentGrade=d.GradeCode ";	
	tSQL += " and a.AgentGrade='A01'";
	tSQL += " and a.State='0'";
//	tSQL += " and a.calagentgrade <> 'A01' ";  去掉延迟转正期
//	tSQL += " and date_format(b.employdate,'yyyymm')<'"+fm.IndexCalNo.value+"'";
//	tSQL += " and not exists ";
//	tSQL += "(select 'X' from laassess where agentcode = a.agentcode and indexcalno='"+fm.IndexCalNo.value+"' and state in ('2','1'))";
	tSQL += getWherePart("a.indexcalno","IndexCalNo");
	tSQL += getWherePart("a.agentcode","AgentCode");
	tSQL += getWherePart("a.ManageCom","ManageCom","like");
	tSQL += getWherePart("c.BranchAttr","BranchAttr");
	tSQL += getWherePart("b.Name","AgentName");
	tSQL += " ORDER BY c.BranchAttr,a.AgentGrade";
	
	//执行查询并返回结果
	turnPage.queryModal(tSQL, EvaluateGrid);
}

//***************************************************
//* 验证必填项
//***************************************************
function checkValid()
{
	if (trim(document.fm.ManageCom.value)=="")
	{
		alert("请填写管理机构!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		return false;
	}
	
	if (document.fm.ManageCom.value.length != 8)
	{
		alert("管理机构输入错误!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		fm.all('ManageCom').focus();
		return false;
	}
	
	if(trim(document.fm.IndexCalNo.value)=="")
	{
		alert("请填写考核年月!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	if (document.fm.IndexCalNo.value.length != 6)
	{
		alert("考核年月输入错误!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	
	return true;
}

//***************************************************
//* 点击‘保存’进行的操作
//***************************************************
function submitForm()
{
	var tRowCount = EvaluateGrid.mulLineCount;  //取得MulLine的行数
  if(!beforeSubmit())
  {
    return false; 
  }
  //判断是否有数据需要处理
  if(tRowCount==0)
  {
  	alert("没有数据需要处理！请先查询！");
  	return false;
  }
  
	// 提交画面前的验证
	if (checkValid() == false)
    return false;
    
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.submit(); //提交
  
  return true;
}
//***************************************************
//*提交前的校验
//***************************************************
function beforeSubmit()
{
if (!chkMulLine()){
   return false;
}
//var temp="nihao";
//var temp2=temp.substring(0,2);
//alert(temp2);
//return false;
  //如果已经进行过薪资计算的考核结果不能再做确认处理
  var SQL = "select max(IndexCalNo) from lawage where branchtype='1' and branchtype2='01' and indexcalno>'"+fm.all('IndexCalNo').value+"' and managecom like '"+fm.all('ManageCom').value+"%'";
 
  var result = easyQueryVer3(SQL,1,1,1);

  if (!result)
 {     
	var arrDataSet = decodeEasyQueryResult(result); 
    alert(""+arrDataSet[0][0]+"的薪资已经计算完毕,不能再进行"+fm.all('IndexCalNo').value+"月的考核确认!");
    return false;
  }
  return true;
}
//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    //EvaluateGrid.clearData("EvaluateGrid");
  }
}

//判断是否选择了要增加、修改或删除的行
function chkMulLine(){
//alert("enter chkmulline");
var i;
var selFlag = true;
var iCount = 0;
var tIndexCalNo=fm.all('IndexCalNo').value;
if(tIndexCalNo==null||tIndexCalNo==''){
	alert("考核年月为空，请输入考核年月！");
	return false;
}
//var rowNum = EvaluateGrid.mulLineCount;
//for(i=0;i<rowNum;i++){
//	iCount++;
//	if(EvaluateGrid.getRowColData(i,6) == 'A03'||EvaluateGrid.getRowColData(i,6) == 'A05'||EvaluateGrid.getRowColData(i,6) == 'A07'){
//		var SQL = "select '1' from laagent where groupagentcode='"+EvaluateGrid.getRowColData(i,1)
//		+"' and months_between('"+tIndexCalNo.substring(0,4)+"-"+tIndexCalNo.substring(4,6)
//		+"-01',char(employdate))<24";
//  		var result = easyQueryVer3(SQL,1,0,1); 
//  		if (result!=null&&result!=''){     
//    		selFlag = confirm("代理人"+EvaluateGrid.getRowColData(i,1)+"入司不到2年，确定将其晋升为首席业务经理吗？");
//  		}
//		break;
//	}
//haidegaia ////////////////////////////
//	if(EvaluateGrid.getRowColData(i,6)> EvaluateGrid.getRowColData(i,5)
//	&&EvaluateGrid.getRowColData(i,6)> EvaluateGrid.getRowColData(i,8) )
//	{
//	    alert("第"+(i+1)+"行确认职级不能高于系统计算的职级标准以及业务员的当前职级两者中的最大者");
//		EvaluateGrid.setFocus(i,1,EvaluateGrid);
//		selFlag = false;
//		break;
//	}
//}
	if(!selFlag) return selFlag;
	return true;
}
