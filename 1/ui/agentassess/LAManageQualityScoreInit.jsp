
<%
	//程序名称：LAManageQualityScoreInit.jsp
	//程序功能：
	//创建日期：2008-02-03 
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
    fm.all('AgentCode').value = '';
    fm.all('GroupAgentcode').value = '';
    fm.all('Name').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('AgentGroupName').value = '';
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';
    fm.all('Score').value = '';
    fm.all('Month1').value = '';
    fm.all('MonthName').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value ='<%=BranchType2%>';
	fm.all('Year').value = '';
	fm.all('wageno1').value='';
	} catch (ex) {
			alert("在LAManageQualityScoreInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

	function initSelBox() {
		try {
			initLAAgentCompreScoreGrid();
			initLAManageQualityScoreGrid();
			initLAManageQualityScoreGridValue();
			initLAAgentCompreScoreGridValue();

		} catch (ex) {
			alert("在LAManageQualityScoreInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
		}
	}

	function initForm() {
		try {
			initInpBox();
			initSelBox();
		} catch (re) {
			alert("在LAManageQualityScoreInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	function initLAAgentCompreScoreGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1] = "0px"; //列宽
			iArray[0][2] = 1; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "项目代码"; //列名
			iArray[1][1] = "60px"; //列宽
			iArray[1][2] = 20; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

			iArray[2] = new Array();
			iArray[2][0] = "扣减分数"; //列名
			iArray[2][1] = "60px"; //列宽
			iArray[2][2] = 10; //列最大值
			iArray[2][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[3] = new Array();
			iArray[3][0] = "操作员"; //列名
			iArray[3][1] = "0px"; //列宽
			iArray[3][2] = 20; //列最大值
			iArray[3][3] = 0;

			iArray[4] = new Array();
			iArray[4][0] = "修改日期"; //列名
			iArray[4][1] = "0px"; //列宽
			iArray[4][2] = 20; //列最大值
			iArray[4][3] = 0;

			iArray[5] = new Array();
			iArray[5][0] = "执行年月"; //列名
			iArray[5][1] = "0px"; //列宽
			iArray[5][2] = 20; //列最大值
			iArray[5][3] = 0;

			LAAgentCompreScoreGrid = new MulLineEnter("fm",
					"LAAgentCompreScoreGrid");
			//这些属性必须在loadMulLine前
			LAAgentCompreScoreGrid.mulLineCount = 52;
			LAAgentCompreScoreGrid.displayTitle = 1;
			LAAgentCompreScoreGrid.hiddenPlus = 1;
			LAAgentCompreScoreGrid.hiddenSubtraction = 1;
			LAAgentCompreScoreGrid.loadMulLine(iArray);

		} catch (ex) {
			alert(ex);
		}
	}
	function initLAManageQualityScoreGrid() {
		var iArray2 = new Array();
       try{
		iArray2[0] = new Array();
		iArray2[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
		iArray2[0][1] = "0px"; //列宽
		iArray2[0][2] = 1; //列最大值
		iArray2[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

		iArray2[1] = new Array();
		iArray2[1][0] = "项目代码"; //列名
		iArray2[1][1] = "60px"; //列宽
		iArray2[1][2] = 20; //列最大值
		iArray2[1][3] = 0; //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

		iArray2[2] = new Array();
		iArray2[2][0] = "项目"; //列名
		iArray2[2][1] = "540px"; //列宽
		iArray2[2][2] = 10; //列最大值
		iArray2[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

		LAManageQualityScoreGrid = new MulLineEnter("fm",
				"LAManageQualityScoreGrid");
		//这些属性必须在loadMulLine前
		LAManageQualityScoreGrid.mulLineCount = 52;
		LAManageQualityScoreGrid.displayTitle = 1;
		LAManageQualityScoreGrid.hiddenPlus = 1;
		LAManageQualityScoreGrid.hiddenSubtraction = 1;
		LAManageQualityScoreGrid.loadMulLine(iArray2);
       } catch (ex) {
			alert(ex);
		}
	}
</script>