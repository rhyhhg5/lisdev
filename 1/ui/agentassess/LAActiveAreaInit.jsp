
<%
//程序名称：LAActiveAreaInit.jsp
//程序功能：互动渠道考核标准初始化
//创建日期：2014-12-5   9:42:50
//创建人  ：yangyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     String currdate = PubFun.getCurrentDate();
%>
<script language="JavaScript">
var tmanagecom =" 1 and  char(length(trim(comcode)))=#8# and comcode<>#86000000# ";
function initInpBox()
{ 
  try
  {                                
	fm.all('ManageCom').value = '';
	fm.all('ManageComName').value='';
	fm.all('AssessCode').value = '';
	fm.all('AssessName').value = '';
  }
  catch(ex)
  {
    alert("在LAActiveAreaInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  
  }
  catch(ex)
  {
    alert("在LAActiveAreaInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initActiveChargeGrid();  
  }
  catch(re)
  {
    alert("LAActiveAreaInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
} 
// 考核职级的标准的初始化
function initActiveChargeGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="管理机构";          		        //列名
      iArray[1][1]="80px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  iArray[1][4]="comcode";              	        //是否引用代码:null||""为不引用
	  iArray[1][5]="1|2";
	  iArray[1][6]="0|1";
	  iArray[1][9]="管理机构|code:comcode&NotNull&NUM&len=8";  
	  iArray[1][15]="1";
	  iArray[1][16]=tmanagecom;
	  iArray[1][18]="undefined";

	  iArray[2]=new Array();
	  iArray[2][0]="管理机构名称"; //列名
	  iArray[2][1]="80px";        //列宽
	  iArray[2][2]=100;            //列最大值
	  iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许        
	  	  
      iArray[3]=new Array();
      iArray[3][0]="考核标准类型";      	   		//列名                                                         
      iArray[3][1]="80px";            			//列宽                                                     
      iArray[3][2]=40;            			//列最大值                                                       
      iArray[3][3]=2;                                                                                    
      iArray[3][9]="考核标准类型|NotNull";              			//是否允许输入,1表示允许，0表示不允许                        
      iArray[3][10]="AssessCode";                                                                        
      iArray[3][11]="0|^A|A|^B|B|^C|C";                                                                  

      iArray[4]=new Array();
      iArray[4][0]="操作人";      	   		//列名
      iArray[4][1]="0px";            			//列宽
      iArray[4][2]=40;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
	  iArray[5][0]="管理机构";    	//列名
	  iArray[5][1]="0px";			//列宽
	  iArray[5][2]=20;			//列最大值
	  iArray[5][3]=3; //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[6]=new Array();
	  iArray[6][0]="idx";    	//列名
	  iArray[6][1]="0px";			//列宽
	  iArray[6][2]=20;			//列最大值
	  iArray[6][3]=3; //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
				
      ActiveChargeGrid = new MulLineEnter( "fm" , "ActiveChargeGrid" ); 
      //这些属性必须在loadMulLine前
      ActiveChargeGrid.mulLineCount = 0;   
      ActiveChargeGrid.displayTitle = 1;  
//      ActiveChargeGrid.hiddenPlus =1;  
      ActiveChargeGrid.canChk  =1;  
//      ActiveChargeGrid.hiddenSubtraction = 1;
      ActiveChargeGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
