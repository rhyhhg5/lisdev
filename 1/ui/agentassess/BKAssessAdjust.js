//               该文件中包含客户端需要处理的函数和事件
var strRT=""; //存储代理人代码记录
//var strAgentGrade=""; //存储代理人职级
//var arrGrade = new Array();
var arrGrade = "";
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//***************************************************
//* 查询职级信息
//***************************************************
function getAgentGradeStr()
{
  var tReturn = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;

  var sSql = "select gradecode,gradename from laagentgrade where 1=1 ";
     sSql += getWherePart("BranchType");
     sSql += getWherePart("BranchType2");  
     sSql += getWherePart("GradeProperty2");
	   sSql += "order by gradecode";
	   

  var tqueryRe = easyQueryVer3(sSql,1,0,1);
  if (tqueryRe)
      tReturn = tqueryRe;

  return tReturn;
}

//***************************************************
//* 查询考核信息
//***************************************************
function easyQueryClick()
{
	//验证必填项
	initEvaluateGrid();
	if(!verifyInput()) return false;
	if(!checkValid())
	{
		return false;
	}
	var subSql="";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		subSql=" and a.agentcode=getAgentCode('" + fm.AgentCode.value + "')";
	}
	
	var tSQL = "";
	
	tSQL  = "SELECT"; 
	tSQL += " getUniteCode(a.AgentCode),";
	tSQL += " b.Name,";
	tSQL += " c.BranchAttr,";
	tSQL += " c.Name,";
	tSQL += " a.AgentGrade,";
	tSQL += " a.AgentGrade1,";
	tSQL += " a.CalAgentGrade,";
	tSQL += " (SELECT GradeName FROM LAAgentGrade WHERE GradeCode= a.CalAgentGrade)";
	tSQL += " FROM";
	tSQL += " LAAssess a,";
	tSQL += " LAAgent b,";
	tSQL += " LABranchGroup c,";
	tSQL += " LAAgentGrade d";
	tSQL += " WHERE";
	tSQL += " a.AgentCode=b.AgentCode AND";
	tSQL += " a.AgentGroup=c.AgentGroup AND";
	tSQL += " a.AgentGrade=d.GradeCode and a.state in ('00') AND";
	tSQL += " d.GradeProperty2= '"+document.fm.GradeProperty2.value+ "' "
	tSQL += getWherePart("a.indexcalno","IndexCalNo");	
//	tSQL += getWherePart("a.agentcode","AgentCode");	
	tSQL += subSql;
	tSQL += getWherePart("a.ManageCom","ManageCom","like");
	tSQL += getWherePart("c.BranchAttr","BranchAttr");
	tSQL += getWherePart("b.Name","AgentName");
	tSQL += " and b.agentstate<='02'";
	tSQL += " and a.BranchType='3' and a.BranchType2='01'";
	tSQL += " and not exists ";
	tSQL += "(select 'X' from laassess where agentcode = a.agentcode and indexcalno='"+fm.IndexCalNo.value+"' and state in ('02','01'))";
	tSQL += " ORDER BY c.BranchAttr,a.AgentGrade";
	
	//执行查询并返回结果
//	var tResult = easyQueryVer3(tSQL,1,0,1);
//	if(tResult==null||tResult==""){
//		alert("查询失败，没有相关信息");
//		return false;
//	}
	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
    //查询成功则拆分字符串，返回二维数组
    arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	
    turnPage.arrDataCacheSet = arrDataSet;
	//查询成功则拆分字符串，返回二维数
	//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = EvaluateGrid;
	//保存SQL语句
	turnPage.strQuerySql = tSQL;
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数
	turnPage.pageLineNum = 20;
	var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 20);
	//调用MULTILINE对象显示查询结果
	displayMultiline(tArr, turnPage.pageDisplayGrid);

}

//***************************************************
//* 验证必填项
//***************************************************
function checkValid()
{
	if (trim(document.fm.ManageCom.value)=="")
	{
		alert("请填写管理机构!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		return false;
	}
	
	if (document.fm.ManageCom.value.length != 8)
	{
		alert("管理机构输入错误!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		fm.all('ManageCom').focus();
		return false;
	}
	
	if(trim(document.fm.IndexCalNo.value)=="")
	{
		alert("请填写考核年月!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	if (document.fm.IndexCalNo.value.length != 6)
	{
		alert("考核年月输入错误!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	
	return true;
}
function assessGrade(a){
	  var tAgentGrade = fm.all(a).all('EvaluateGrid5').value;
	  if(document.fm.GradeProperty2.value=="0"){
		  var StrSql=" 1 and branchtype=#3# and branchtype2=#01# and (gradeproperty2 = #"+tGradeProPerty2+"# or gradecode = #F00#)";
	  }
	  if(document.fm.GradeProperty2.value=="1"){
		  if (tAgentGrade=="G51"){
			  var StrSql=" 1 and branchtype=#3# and branchtype2=#01# and (gradeproperty2 = #"+tGradeProPerty2+"# or gradecode = #F23#)";
		  }else{
			  var StrSql=" 1 and branchtype=#3# and branchtype2=#01# and gradeproperty2 = #"+tGradeProPerty2+"#";
		  }
	  }
	  showCodeList('assessgrade2',[fm.all(a).all('EvaluateGrid7'),fm.all(a).all('EvaluateGrid8')],[0,1],null,StrSql,1,1);
}



//***************************************************
//* 点击‘保存’进行的操作
//***************************************************
function submitForm()
{
	var tRowCount = EvaluateGrid.mulLineCount;  //取得MulLine的行数
	
  //判断是否有数据需要处理
  if(tRowCount==0)
  {
  	alert("没有数据需要处理！请先查询！");
  	return false;
  }
	for(i=0;i<tRowCount;i++)
	{
		var tAgentGrade =EvaluateGrid.getRowColData(i,5);
		var tCalAgentGrade = EvaluateGrid.getRowColData(i,7)
		var checkSql = "";
		  if(document.fm.GradeProperty2.value=="0"){
			  checkSql=" select '1' from laagentgrade where branchtype='3' and branchtype2='01' and (gradeproperty2 = '"+tGradeProPerty2+"' or gradecode = 'F00') and GradeCode = '"
			  +tCalAgentGrade+"'";
		  
		  }
		  if(document.fm.GradeProperty2.value=="1"){
			  if (tAgentGrade=="G51"){
				  checkSql=" select '1' from laagentgrade where branchtype='3' and branchtype2='01' and (gradeproperty2 = '"+tGradeProPerty2+"' or gradecode = 'F23')  and GradeCode = '"
				  +tCalAgentGrade+"'";
			  }else{
				   checkSql=" select '1' from laagentgrade where branchtype='3' and branchtype2='01' and gradeproperty2 = '"+tGradeProPerty2+"' and GradeCode = '"
				  +tCalAgentGrade+"'";
			  }
		  }
		var tcheckResult = easyQueryVer3(checkSql,1,0,1);
		if(tcheckResult==null||tcheckResult==""){
			alert("第"+(++i)+"行确认职级错误，请检查。");
			return false;
		}
	}
//  
  
	// 提交画面前的验证
	if (checkValid() == false)
    return false;
    
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="./BKAssessAdjustSave.jsp";
  fm.submit(); //提交
  
  return true;
}

//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
//     initForm();
    //执行下一步操作
    initEvaluateGrid();
  }
}

function downLoad(){
	var tRowCount = EvaluateGrid.mulLineCount;  //取得MulLine的行数
	  //判断是否有数据需要处理
	  if(tRowCount==0)
	  {
	  	alert("没有数据！请先查询！");
	  	return false;
	  }	
		var subSql="";
		if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
			subSql=" and a.agentcode=getAgentCode('" + fm.AgentCode.value + "')";
		}
		var downSql = "";
		
		downSql  = "SELECT"; 
		downSql += " getUniteCode(a.AgentCode),";
		downSql += " b.Name,";
		downSql += " c.BranchAttr,";
		downSql += " c.Name,";
		downSql += " a.AgentGrade,";
		downSql += " a.AgentGrade1,";
		downSql += " a.CalAgentGrade,";
		downSql += " (SELECT GradeName FROM LAAgentGrade WHERE GradeCode= a.CalAgentGrade)";
		downSql += " FROM";
		downSql += " LAAssess a,";
		downSql += " LAAgent b,";
		downSql += " LABranchGroup c,";
		downSql += " LAAgentGrade d";
		downSql += " WHERE";
		downSql += " a.AgentCode=b.AgentCode AND";
		downSql += " a.AgentGroup=c.AgentGroup AND";
		downSql += " a.AgentGrade=d.GradeCode and a.state in ('00') AND";
		downSql += " d.GradeProperty2= '"+document.fm.GradeProperty2.value+ "' "
		downSql += getWherePart("a.indexcalno","IndexCalNo");	
//		downSql += getWherePart("a.agentcode","AgentCode");	
		downSql += subSql;
		downSql += getWherePart("a.ManageCom","ManageCom","like");
		downSql += getWherePart("c.BranchAttr","BranchAttr");
		downSql += getWherePart("b.Name","AgentName");
		downSql += " and b.agentstate<='02'";
		downSql += " and a.BranchType='3' and a.BranchType2='01'";
		downSql += " and not exists ";
		downSql += "(select 'X' from laassess where agentcode = a.agentcode and indexcalno='"+fm.IndexCalNo.value+"' and state in ('02','01'))";
		downSql += " ORDER BY c.BranchAttr,a.AgentGrade";  
		var tResult = easyQueryVer3(downSql,1,0,1);
		if(tResult==null||tResult==""){
			alert("下载失败，没有相关信息");
			return false;
		}
		
		var i = 0;
		fm.all("downSql").value = downSql;
		fm.action = "./BKAssessAdjustDownLoad.jsp"
		fm.submit();
	  
}
