
<%
//程序名称：AssessStandardInit.jsp
//程序功能：互动渠道考核标准初始化
//创建日期：2014-12-5   9:42:50
//创建人  ：yangyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     String currdate = PubFun.getCurrentDate();
%>

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                
	fm.all('ManageCom').value = <%=tG.ManageCom%>;
	var sql="select name from ldcom where comcode ='"+<%=tG.ManageCom%>+"'";
	var array=easyExecSql(sql);
	fm.all('ManageComName').value =array[0][0];
	fm.all('AssessType').value = '';
	fm.all('AssessTypeName').value = '';
  }
  catch(ex)
  {
    alert("在AssessStandardInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  
  }
  catch(ex)
  {
    alert("在AssessStandardInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initAssessGrid();  
    setAssessGridValue(); 
  }
  catch(re)
  {
    alert("AssessStandardInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
} 
// 考核职级的标准的初始化
function initAssessGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="职级编码";          		        //列名
      iArray[1][1]="30px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="职级名称";         		        //列名
      iArray[2][1]="100px";            			//列宽
      iArray[2][2]=10;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="晋升职级编码";      	   		//列名
      iArray[3][1]="0px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0; 
      
        
      iArray[4]=new Array();
      iArray[4][0]="晋升标准";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  iArray[4][9]="晋升标准|NotNull&NUM"; 
	  
      iArray[5]=new Array();
      iArray[5][0]="维持职级编码";      	   		//列名
      iArray[5][1]="0px";            			//列宽
      iArray[5][2]=30;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      iArray[6]=new Array();
      iArray[6][0]="维持标准";      	   		//列名
      iArray[6][1]="80px";            			//列宽
      iArray[6][2]=40;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][9]="维持标准|NotNull&NUM"; 
      
      AssessGrid = new MulLineEnter( "fm" , "AssessGrid" ); 
      //这些属性必须在loadMulLine前
      AssessGrid.mulLineCount = 10;   
      AssessGrid.displayTitle = 1;  
      AssessGrid.hiddenPlus =1;  
      AssessGrid.hiddenSubtraction = 1;
      AssessGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
