<%
//程序名称：LAAgentEvaluateInit.jsp
//程序功能：
//创建日期：2005-8-25 9:49
//创建人  ：LiuH
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

if(globalInput == null) {
	out.println("session has expired");
	return;
}

String strOperator = globalInput.Operator;
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">
var StrSql=" 1 and branchtype=#2# and branchtype2=#01# ";
</script>
<script language="JavaScript">
var tBranchType = "<%=BranchType%>";
var tAgentTitleGrid = "营销员";
if("2"==tBranchType)
{
  tAgentTitleGrid = "业务员";
}
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';
    fm.all('IndexCalNo').value = '';
    fm.all('BranchAttr').value = '';
    fm.all('BranchName').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';
    fm.all('GroupSeries').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';    
    fm.all('BranchType2').value = '<%=BranchType2%>';    
    
    if (fm.all('BranchType').value == '11')
    {
      fm.AscBtn.disabled = false;
    }
    else
    {
      fm.AscBtn.disabled = true;
      divQQQQ.style.display='none';
     }
  }
  catch(ex)
  {
    alert("在LAAssessInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    //initInpBox();
    initEvaluateGrid();
  }
  catch(re)
  {
    alert("LAAssessInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 var EvaluateGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initEvaluateGrid()
{
    var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
    var iArray = new Array();
    var i11Array 
    //= getAgentGradeStr();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]=tAgentTitleGrid+"代码";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]=tAgentTitleGrid+"姓名";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="销售单位代码";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="销售单位名称";         		//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="当前职级";      		//列名
      iArray[5][1]="40px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="参考职级";      		//列名
      iArray[6][1]="40px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="建议职级";         		//列名
      iArray[7][1]="40px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=2;     
      if (tBranchType=='1' && tBranchType2=='01' )
      {
         iArray[7][3]=0;
	 iArray[7][4]="assessgrade1";   
	 iArray[7][5]="7|8";              	               
         iArray[7][6]="0|1";
         iArray[7][9]="建议职级|code:assessgrade1";  
         iArray[7][15]= "1";  
         iArray[7][17]= "5"; 
      }          	        
      else if (tBranchType=='2' && tBranchType2=='01' )
      {
       iArray[7][4]="assessgrade2"; 
       iArray[7][5]="7|8";              	               
        iArray[7][6]="0|1";
        iArray[7][9]="建议职级|code:assessgrade2";  
        iArray[7][15]= "1"; 
       iArray[7][16]= StrSql; 
       }

      
     
       
           
      iArray[8]=new Array();
      iArray[8][0]="建议职级名称";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      EvaluateGrid = new MulLineEnter( "fm" , "EvaluateGrid" );
      //这些属性必须在loadMulLine前
      EvaluateGrid.displayTitle = 1;
      EvaluateGrid.mulLineCount = 0;
      EvaluateGrid.hiddenPlus=1;
      EvaluateGrid.hiddenSubtraction=1;
      EvaluateGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //EvaluateGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
