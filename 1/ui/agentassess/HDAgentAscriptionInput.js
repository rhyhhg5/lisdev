
//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var showInfo;
var mDebug="1";
var arrStrReturn = new Array();
var arrGrid;


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function easyQueryClick()
{
	if (verifyInput() == false)
    return false;
  var StrSQL = "";	
  var tManageCom = fm.all('ManageCom').value;
  var tBranchType = trim(fm.all('BranchType').value);

  StrSQL = "select getunitecode(a.agentcode) , b.name, a.BranchAttr, "
  + " (select name from labranchgroup where agentgroup = b.agentgroup),"
  + " a.AgentGrade, a.CalAgentGrade, a.AgentGrade1, "
  + " (select gradename from laagentgrade where gradecode=a.agentgrade1) " 
  + " FROM LAAssess a ,LAAgent b" 
  + " where "
  + " a.agentcode = b.agentcode"
  + " and a.State='1' "  
  + getWherePart("a.indexcalno","IndexCalNo")
  + getWherePart("a.ManageCom","ManageCom","like")
  + getWherePart("a.BranchType","BranchType")
  + getWherePart("a.BranchType2","BranchType2")
  + "ORDER BY a.BranchAttr,a.AgentGrade" ;
   
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(StrSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未查询到已确认需归属的考核结果信息！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组  
  turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = BeforeGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = StrSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);  	
	
}	
	
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  {           
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   }
   /*//链接到挂起人员信息查询界面
   //alert("请到挂起人员清单界面查询考核结束后产生的挂起人员！");
   var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
   showInfo = window.open("../agentquery/LASpecialAssessQueryhtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
*/ } 


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  var lineCount = BeforeGrid.mulLineCount;
  if(lineCount == 0)
  {
  	alert("无考核结果数据，不需要做组织归属！");
  	return false;
  }
  return true;
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




function useSimulationEasyQueryClick(strData) {
  //保存查询结果字符串
  turnPage.strQueryResult  = strData;
  
  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;  
    
  //拆分字符串，返回二维数组
  //var tArr                 = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
     //var filterArray          = new Array(1,2,6,8);

  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //过滤二维数组，使之与MULTILINE匹配
  //turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BeforeGrid; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  //必须将所有数据设置为一个数据块
  turnPage.pageLineNum = 5;
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
}

   	
function AgentAscript()
{
	var tIndexCalNo = trim(fm.all('IndexCalNo').value);
	//alert(tIndexCalNo);
	if (verifyInput() == false)
  return false;
  	//alert("111111");
  if(!beforeSubmit())
   return false;
	
 //校验是否算过薪资
 var strSQL1 = "select max(distinct IndexCalNo) from LAWage where 1=1  and state='1' "
             +getWherePart('BranchType')
             +getWherePart('BranchType2')
             +getWherePart('ManageCom');
  var tResult1 = easyQueryVer3(strSQL1,1,1,1);  
  if (!tResult1) {
  alert("没有进行薪资计算，无法进行组织归属！");
  return false;
  }
  
  var mArr = new Array();
  mArr = decodeEasyQueryResult(tResult1);
  maxWMonth= mArr[0][0];
  // alert(maxWMonth);
  if (maxWMonth!=tIndexCalNo)
  {
      alert("无法进行该年月的组织归属，当前薪资现已计算到"+maxWMonth);
      return false;;
  }
 
	
	//校验考核的最大月
  var strSQL = "select max(distinct IndexCalNo) from LAAssess where 1=1 "
             +getWherePart('BranchType')
             +getWherePart('BranchType2')
             +getWherePart('ManageCom');
  //alert(strSQL);          
  var tResult = easyQueryVer3(strSQL,1,1,1); 
  if (!tResult) {
  alert("没有组织归属信息，无法进行组织归属！");
  return false;
  } 
   var tArr = new Array();
  tArr = decodeEasyQueryResult(tResult);
  maxMonth= tArr[0][0];	
  //alert(maxMonth);
  if (maxMonth!=tIndexCalNo)
  {
      alert("只能进行"+maxMonth+"月的组织归属！");
      return false;
  }	
 
  var showStr="正在执行归属操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
  
  fm.fmAction.value = "Ascript"; 
  fm.submit();
}	
	

