//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var tOrphanCode="";
var queryFlag= false;
window.onfocus=myonfocus;

 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,500,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}
//查询
function queryForm()
{
	if( verifyInput() == false ) return false;
	var strSQL1="select agentgrade,(select gradename from laagentgrade where gradecode = a.agentgrade),(select gradecode from laagentgrade where gradecode > a.agentgrade  order by  gradecode fetch first 1 rows only )," +
			"upstandprem,agentgrade,keepstandprem from LAAgentPromRadix5 a " +
			"where managecom ='"+fm.all("ManageCom").value+"' and AssessType='"+fm.all("AssessType").value+"' with ur";
	turnPage1.strQueryResult  = easyQueryVer3(strSQL1, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage1.strQueryResult) {
		alert("没有符合条件的数据!");
		initForm();
		queryFlag = false;
		return false;
	}
	turnPage1.queryModal(strSQL1, AssessGrid);
	queryFlag = true;
}


//提交，保存按钮对应操作
function submitForm()
{
	mOperate = "INSERT";
	if( verifyInput() == false ) return false;
	if(!checkManageCom()){ return false;}
	if(!chkMulLine()) return false;
	if(!chkMulLineData()) return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.hideOperate.value=mOperate;
	fm.submit();
}
function updateClick()
{
	mOperate = "UPDATE";
	if( verifyInput() == false ) return false;
	if(!checkManageCom()){ return false;}
	if(!chkMulLine()) return false;
	if(!chkMulLineData()) return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.hideOperate.value=mOperate;
	fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
		queryFlag = false;
	}
	
}
//标准查询
function queryClick()
{
	//下面增加相应的代码
	var tAssessType = fm.all('AssessType').value;
//	if(tAssessType==""||tAssessType==null)
//	{
//		alert("请选择考核方式");
//		return;
//	}
	var hr = document.getElementById("hr");
	var divAssessStand11 = document.getElementById("divAssessStand11");
	var divAssessStand12 = document.getElementById("divAssessStand12");
	var divAssessStand21 = document.getElementById("divAssessStand21");
	var divAssessStand22 = document.getElementById("divAssessStand22");
	var divAssessStand31 = document.getElementById("divAssessStand31");
	var divAssessStand32 = document.getElementById("divAssessStand32");
	showDiv(hr,"true");
//	if(tAssessType==1)
//	{
//		showDiv(divAssessStand11,"true");
//		showDiv(divAssessStand21,"true");
//		showDiv(divAssessStand31,"true");
//		showDiv(divAssessStand12,"false");
//		showDiv(divAssessStand22,"false");
//		showDiv(divAssessStand32,"false");
//	}
//	else if(tAssessType==2)
//	{
//		showDiv(divAssessStand11,"false");
//		showDiv(divAssessStand21,"false");
//		showDiv(divAssessStand31,"false");
//		showDiv(divAssessStand12,"true");
//		showDiv(divAssessStand22,"true");
//		showDiv(divAssessStand32,"true");
//	}
	showDiv(divAssessStand11,"true");
	showDiv(divAssessStand21,"true");
	showDiv(divAssessStand31,"true");
	showDiv(divAssessStand12,"true");
	showDiv(divAssessStand22,"true");
	showDiv(divAssessStand32,"true");
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}
function checkManageCom()
{
//	var strSQL1="select * from LAAgentPromRadix5 where managecom ='"+fm.all("ManageCom").value+"' and AssessType='"+fm.all("AssessType").value+"' with ur";
//	var strQueryResult  = easyQueryVer3(strSQL1, 1, 1, 1);
	if(mOperate == "UPDATE")
	{
//		if(!strQueryResult)
//		{
//			alert("该管理机构对于考核方式为（"+fm.all("AssessTypeName")+"）的考核标准未录入,请先保存！");
//			return false;
//		}
		if(false==queryFlag)
		{
			alert("请先查询，再进行修改！");
			return false;
		}
	}
	if(mOperate == "INSERT")
	{
//		if(strQueryResult)
//		{
//			alert("该管理机构对于考核方式为（"+fm.all("AssessTypeName")+"）的考核标准已经录入！");
//			return false;
//		}
		if(true==queryFlag)
		{
			alert("该记录为查询出来的记录，请进行修改！");
			return false;
		}
	}
	return true;
}
function setAssessGridValue()
{
	var sql ="select  gradecode,gradename,(select gradecode from laagentgrade where gradecode > a.gradecode  order by  gradecode fetch first 1 rows only ),'' from laagentgrade a " +
			" where branchtype='5' and branchtype2='01' and GradeProperty2 is not null  and gradecode like 'U%' with ur ";
	turnPage.queryModal(sql, AssessGrid);
	var rowNum = AssessGrid.mulLineCount;
	for(var i=0;i<rowNum;i++)
	{
		var gradecode = AssessGrid.getRowColData(i,1);
		if(gradecode=="U10")
		{
			//资深互动业务经理三级为最高职级，不用录入最高职级，设置默认值为9999999999
			AssessGrid.setRowColData(i,4,"9999999999");
		}
	}
}
function chkMulLine()
{
	var rowNum = AssessGrid.mulLineCount;
	for(var i=0;i<rowNum;i++)
	{
		var gradecode = AssessGrid.getRowColData(i,1);
		var gradename =AssessGrid.getRowColData(i,2);
		var stand_up = AssessGrid.getRowColData(i,4);
		var stand_keep = AssessGrid.getRowColData(i,6);
		if(stand_keep==null||stand_keep==""||stand_keep==0)
		{
			alert(gradename+"的维持标准不能为空或0！");
			AssessGrid.setFocus(i,4,AssessGrid);
			return false;
		}
		else if(stand_up==null||stand_up==""||stand_up==0)
		{
			alert(gradename+"的晋升标准不能为空或0！");
			AssessGrid.setFocus(i,6,AssessGrid);
			return false;
		}
		else if((stand_up-stand_keep)<=0)
		{
			alert(gradename+"的维持标准应小于晋升标准");
			AssessGrid.setFocus(i,4,AssessGrid);
			return false;
		}
	}
	return true;
}
function chkMulLineData()
{
	var rowNum = AssessGrid.mulLineCount;
	for(var i=0;i<rowNum;i++)
	{
		var gradecode = AssessGrid.getRowColData(i,1);
		var gradename =AssessGrid.getRowColData(i,2);
		var stand_up = AssessGrid.getRowColData(i,4);
		var stand_keep = AssessGrid.getRowColData(i,6);
		for(var j=1;j<rowNum;j++)
		{
			var gradecode1 = AssessGrid.getRowColData(j,1);
			var gradename1 =AssessGrid.getRowColData(j,2);
			var stand_up1 = AssessGrid.getRowColData(j,4);
			var stand_keep1 = AssessGrid.getRowColData(j,6);
			if(gradecode<gradecode1)
			{
				if((stand_up-stand_up1)>=0)
				{
					alert(gradename+"的晋升标准应小于"+gradename1+"的晋升标准");
					AssessGrid.setFocus(i,4,AssessGrid);
					return false;
				}
				else if((stand_keep-stand_keep1)>=0)
				{
					alert(gradename+"的维持标准应小于"+gradename1+"的维持标准");
					AssessGrid.setFocus(i,6,AssessGrid);
					return false;
				}
			}
		}
	}
	return true;
}