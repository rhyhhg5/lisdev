<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABankWagePaySave.jsp
//程序功能： 
//创建日期：2016-3-24
//创建人  ：XX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
 boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    //业务员类型
//    String tAgentType = request.getParameter("AgentType");
    //薪资月，如果 为12月，则显示 年终奖，否则不显示 
//    String tIndexCalNo = request.getParameter("IndexCalNo");
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "银代考核报表 _"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    String querySql = request.getParameter("querySql");
    System.out.println(querySql);
    querySql = querySql.replaceAll("%25","%");
    		 //设置表头
		    String[][] tTitle = {{"","","","考核确认结果报表","","","",""},
    				 {"业务员代码","业务员姓名","销售单位代码","销售单位名称","当前职级","确认职级","确认职级名称","考核确认状态"}};
		    //表头的显示属性
		    int []displayTitle = {1,2,3,4,5,6,7,8};
		    
		    //数据的显示属性
		    int []displayData = {1,2,3,4,5,6,7,8};
		    //生成文件
		    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
		    createexcellist.createExcelFile();
		    String[] sheetName ={"list"};
		    createexcellist.addSheet(sheetName);
		    int row = createexcellist.setData(tTitle,displayTitle);
		    if(row ==-1) errorFlag = true;
		        createexcellist.setRowColOffset(row,0);//设置偏移
		        System.out.println(querySql);
		    if(createexcellist.setData(querySql,displayData)==-1)
		        errorFlag = true;
		    if(!errorFlag)
		        //写文件到磁盘
		        try{
		            createexcellist.write(tOutXmlPath);
		        }catch(Exception e)
		        {
		            errorFlag = true;
		            System.out.println(e);
		        }
//    	}
    
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>