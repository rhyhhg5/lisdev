<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：AssessConfirmBL.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>

<%
  //输出参数
  String FlagStr = "";
  String Content = "";

  AssessConfirmBL tConfirm = new AssessConfirmBL();
  VData tVData = new VData();

  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  System.out.println("ManagCom : " + tGI.ManageCom);
  System.out.println("Operator : " + tGI.Operator);
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {    
      String tManageCom  = request.getParameter("ManageCom");
      //String tAgentGrade = request.getParameter("AgentGrade");
      String tIndexCalNo = request.getParameter("IndexCalNo");
      String tBranchType = request.getParameter("BranchType");
      String tBranchType2 = request.getParameter("BranchType2");
      String tAgentSeries = request.getParameter("AgentSeries");
      
      tVData.clear();
      tVData.add(tGI);
      tVData.add(tManageCom);
      tVData.add(tIndexCalNo);    
      tVData.add(tBranchType);
			tVData.add(tBranchType2);	
			tVData.add(tAgentSeries);	
      if (!tConfirm.submitData(tVData))  {
        FlagStr = "Fail";
        Content = "保存失败，原因是：" +
                  tConfirm.mErrors.getFirstError();
        System.out.println("FlagStr:  " + FlagStr);
        System.out.println("Content:  " + Content);
      }
      else  {
        FlagStr = "Succ";
        Content = "保存成功！";
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "保存失败，原因是：" + ex.toString();
    }
  }// tGI!=null
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
