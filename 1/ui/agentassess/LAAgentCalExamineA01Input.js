var turnPage = new turnPageClass();

/***************************************************
 * 执行计算操作
 ***************************************************
 */
 function AgentCalExamineSave()
 {
 	 submitForm();
 }

/***************************************************
 * 执行提交操作
 ***************************************************
 */
 function submitForm()
 {
 	 //对非空进行验证
   if( verifyInput() == false ) return false;
    if(!beforeSubmit())
    {
      return false;
    }	 
   var i = 0;
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
   fm.submit(); //提交
 }
 
 function beforeSubmit()
 {
	//首先检验录入框
	  if(!verifyInput()) return false;
	  
	  var assessmonth=trim(fm.all('AssessMonth').value);
	  if(assessmonth=='03'||assessmonth=='06'||assessmonth=='09'||assessmonth=='12')
	  {
	  alert("只能进行自然月考核，请重新确认考核所属月");
	  return false;	
	  }

	  var tIndexCalNo = trim(fm.all('AssessYear').value)+trim(fm.all('AssessMonth').value);
	  var maxMonth="";
	  
	   //////////////////////2008-01-15  XX  加上校验 
	 //校验是否算过薪资
	 var strSQL = "select max(distinct IndexCalNo) from LAWage where 1=1  and state='1' "
	             +getWherePart('BranchType')
	             +getWherePart('BranchType2')
	             +getWherePart('ManageCom');
	  var tResult = easyQueryVer3(strSQL,1,1,1);  
	  if (!tResult) {
	  alert("没有进行薪资计算，无法进行考核计算！");
	  return false;
	  }
	  var tArr = new Array();
	  tArr = decodeEasyQueryResult(tResult);
	  maxMonth= tArr[0][0];
	  if (maxMonth!=tIndexCalNo)
	  {
	      alert("无法进行该年月的考核，薪资现已计算到"+maxMonth);
	      return false;;
	  }
	 
	//校验考核信息
	   var strSql = "select agentcode from laassess where IndexCalNo = '"+tIndexCalNo+"' "
	              +"and state>='1'"
	              +"and agentgrade ='A01' "
//	              +getWherePart('AgentSeries')
	              +getWherePart('ManageCom')
	              +getWherePart('BranchType')
	              +getWherePart('BranchType2');
	    
	  //查询SQL，返回结果字符串
	  var strResult  = easyQueryVer3(strSql, 1, 1, 1);  

	  //判断是否查询成功

	  if (strResult) { 
	  	 
	    alert("该职级已审核确认或组织归属，不能进行考核计算！");
	    return false;
	  }
	  return true;
 }
/***************************************************
 * 提交后操作,服务器数据返回后执行的操作
 ***************************************************
 */
 function afterSubmit( FlagStr, content )
 {
 	 showInfo.close();
 	
   if (FlagStr == "Fail" )
   {
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
     showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else
   {
     var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
     showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
     //执行下一步操作
   }
 }
 function download(){
	  var yearmonth = fm.all('AssessYear').value+fm.all('AssessMonth').value;
	  if( verifyInput() == false ) return false;
	  	var tSql = "select getUniteCode(a.AgentCode),b.Name,c.BranchAttr,c.Name,a.AgentGrade,a.CalAgentGrade," 
	  			+"(SELECT GradeName FROM LAAgentGrade WHERE GradeCode= a.CalAgentGrade)" 
	  			+" from LAAssess a,LAAgent b,LABranchGroup c,LAAgentGrade d "
	  			+" where a.AgentCode=b.AgentCode AND a.AgentGroup=c.AgentGroup AND  a.CalAgentGrade=d.GradeCode"
	  			+" and a.AgentGrade='A01' and a.State='0' and a.indexcalno = '"+yearmonth+"'"
	  			+ getWherePart("a.ManageCom","ManageCom")
	  			+ "ORDER BY c.BranchAttr,a.AgentGrade";
		var mArr = easyExecSql(tSql);
		fm.querySql.value = tSql;
		if(fm.querySql.value!=null && fm.querySql.value!=""&& mArr!=null){
			var formAction = fm.action;
			fm.action = "LAAgentExamineA01Download.jsp";
			fm.submit();
			fm.target = "fraSubmit";
			fm.action = formAction;
		}else{
			alert("没有数据");
			return;
		}
}
 
 
 