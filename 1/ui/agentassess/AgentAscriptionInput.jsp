<html>
<%
//程序名称：AgentAscriptionInput.jsp
//程序功能：代理人归属录入界面
//创建时间：
//更新记录：更新人   更新时间   更新原因
%>
<%@page contentType="text/html;charset=GBK" %>

<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String AgentSeries=request.getParameter("AgentSeries");
%>

<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=GBK">-->
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="AgentAscriptionInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="AgentAscriptionInit.jsp"%>
   <%@include file="../common/jsp/UsrCheck.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./AgentAscriptionSave.jsp" method=post name=fm target="fraSubmit">
 	<table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		考核处理
       		 </td>   		 
    	</tr>
    </table>
  <table class=common>
   <tr class=common>
    <td class=title>管理机构</td>
   
    <TD  class= input>
      <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
    </TD>
    <td class=title>考核年月</td>
    <td class=input colspan=3> 
    	<Input class=common type=text name=IndexCalNo verify="考核年月|INT&NOTNULL&len=6" elementtype=nacessary>
      	<font color="red">  'YYYYMM'</font>
    </td>
   </tr>
     <tr>
     	<!--
      <td class=title>代理人职级</td>
      <td class=input > <Input class='codeno' name=AgentGrade verify="代理人职级|AgentGrade&NOTNULL"
      	ondblclick="return showCodeList('AgentGrade', [this,AgentGradeName],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
      	onkeyup="return showCodeListKey('AgentGrade', [this,AgentGradeName],[0,1],null,'<%=tSqlAgentGrade%>','1');"><input name=AgentGradeName class='codename' readonly=true elementtype=nacessary>
      </td>  
      -->
     </tr>
  </table>
  
  <table>
   <tr class=input>
   	<td class=common><input type=button value="查    询" class=cssbutton onclick="easyQueryClick();"></td>
    <td class=common><input type=button value="组织归属" class=cssbutton onclick="AgentAscript();"></td>   
    <!--
    <td class=common><input type=button value="归属结果查询" class=cssbutton onclick="qryAfterAscript();"></td>
    -->
   </tr>
  </table>

  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBeforeGrid);">
    		</td>
    		<td class= titleImg>
    			 归属前营销员考核结果信息
    		</td>
    	</tr>
  </table>

  <div id="divBeforeGrid" style="display:''">
   <table class=common>
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanBeforeGrid"></span>
     </td>
    </tr>
   </table>
      <INPUT VALUE="首  页"  class="cssbutton" TYPE="button" onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  class="cssbutton" TYPE="button" onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  class="cssbutton" TYPE="button" onclick="turnPage.nextPage();">
      <INPUT VALUE="尾  页"  class="cssbutton" TYPE="button" onclick="turnPage.lastPage();">
  </div>

   <input type=hidden id="sql_where" name="sql_where">
   <input type=hidden id="fmAction" name="fmAction">   
   <input type=hidden name=BranchType value='<%=BranchType%>'>
   <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
   <input type=hidden name=AgentSeries value='<%=AgentSeries%>'>

 </form>
 <span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>