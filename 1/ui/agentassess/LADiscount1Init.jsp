<%
//程序名称：LADiscount1Init.jsp
//程序功能：
//创建时间：2008-02-26
//创建人  ：zhaojing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 ////var  tmanagecom=" 1 and  char(length(trim(comcode))) in (#4#,#2#)";
  //var  tmanagecom=" 1 and  char(length(trim(comcode))) in (#8#)";
  var StrSql=" 1 and branchtype=#3# and branchtype2=#01# and GRADECODE <>#F00# ";

</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {   
    fm.all('BranchType').value=getBranchType();                    
  }
  catch(ex)
  {
    alert("在LADiscount1Init.jsp.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initLADiscount1Grid()
{
  try
  {

    var iArray = new Array();
		//var i11Array = getAgentGradeStr();
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    iArray[1]=new Array();
    iArray[1][0]="idx";            //列名 idx
    iArray[1][1]="0px";          //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[2]=new Array();
	  iArray[2][0]="职级";          		//列名
	  iArray[2][1]="50px";      	      		//列宽
	  iArray[2][2]=20;            			//列最大值
	  iArray[2][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  iArray[2][4]="assessgrade2";              	        //是否引用代码:null||""为不引用
	  iArray[2][5]="2|3";              	                //引用代码对应第几列，'|'为分割符
	  iArray[2][6]="0|1";
		iArray[2][9]="职级|code:assessgrade2|NotNull&NUM";                             
    iArray[2][15]= "1";                                                    
    iArray[2][16]= StrSql;      

		iArray[3]=new Array();
    iArray[3][0]="职级名称";      		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    /*iArray[3][10]='AgentGrade1';
    iArray[3][11]=i11Array;
    iArray[3][12]="3";
    iArray[3][13]="0";   
    iArray[3][19]= 1; //1是需要强制刷新 */
       
    iArray[4]=new Array();
    iArray[4][0]="标准保费"; //列名
    iArray[4][1]="50px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[4][9]="标准保费|NotNull&NUM";
       
    
   
        
    LADiscount1Grid = new MulLineEnter( "fm" , "LADiscount1Grid" );
    LADiscount1Grid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    LADiscount1Grid.displayTitle = 1;
    //SetGrid.hiddenSubtraction =0;
    //SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    LADiscount1Grid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LADiscount1Init.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initLADiscount1Grid();
    
  }
  catch(re)
  {
    alert("在LADiscount1Init.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>