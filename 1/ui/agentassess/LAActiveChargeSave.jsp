<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String branchtype = request.getParameter("BranchType");
  String branchtype2 = request.getParameter("BranchType2");
  String FlagStr = "Fail";
  String Content = "";
  ExeSQL tExeSQL = new ExeSQL();
  String arrCount[] = request.getParameterValues("InpActiveChargeGridChk");
  String tmanagecom[] = request.getParameterValues("ActiveChargeGrid1");
  String tGroupAgentCode[] = request.getParameterValues("ActiveChargeGrid2");
  String tWageNo[] = request.getParameterValues("ActiveChargeGrid3");
  String tPreoperyPrem[] = request.getParameterValues("ActiveChargeGrid4");
  String tLifePrem[] = request.getParameterValues("ActiveChargeGrid5");
  String tPreoperyCharge[] = request.getParameterValues("ActiveChargeGrid6");
  String tLifeCharge[] = request.getParameterValues("ActiveChargeGrid7");
  String tSumCharge[] = request.getParameterValues("ActiveChargeGrid8");
  //lineCount = arrCount.length; //行数
  
  LAActiveChargeBL tLAActiveChargeBL = new LAActiveChargeBL();
  LAActiveChargeSchema tLAActiveChargeSchema;
  LAActiveChargeSet tLAActiveChargeSet = new LAActiveChargeSet();
  int lineCount = arrCount.length;
  for(int i=0;i<lineCount;i++)
  {
  	if(arrCount[i].equals("1"))
    {
	  	tLAActiveChargeSchema = new LAActiveChargeSchema();
	    tLAActiveChargeSchema.setManageCom(tmanagecom[i]);
	    tLAActiveChargeSchema.setBranchType(branchtype);
	    tLAActiveChargeSchema.setBranchType2(branchtype2);
	    String sql ="select agentcode from laagent where  groupagentcode ='"+tGroupAgentCode[i]+"'";
	    tLAActiveChargeSchema.setAgentCode(tExeSQL.getOneValue(sql));
	    tLAActiveChargeSchema.setWageNo(tWageNo[i]);
	    tLAActiveChargeSchema.setPropertyPrem(tPreoperyPrem[i]);
	    tLAActiveChargeSchema.setLifePrem(tLifePrem[i]);
	    tLAActiveChargeSchema.setPropertyCharge(tPreoperyCharge[i]);
	    tLAActiveChargeSchema.setLifeCharge(tLifeCharge[i]);
	    tLAActiveChargeSchema.setSumCharge(tSumCharge[i]);//1  是考核标准 
	    System.out.println(tSumCharge[i]+"---"+tmanagecom[i]);
	    tLAActiveChargeSchema.setState("00");//00   有效   01  无效
	    tLAActiveChargeSchema.setVersionType("11");
	    tLAActiveChargeSet.add(tLAActiveChargeSchema);
    }
  }

System.out.println("tOperate11:"+tOperate);
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.add(tLAActiveChargeSet);

System.out.println("tOperate:"+tOperate);
  try
  {
    System.out.println("this will save the data!!!");	
    tLAActiveChargeBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAActiveChargeBL.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
