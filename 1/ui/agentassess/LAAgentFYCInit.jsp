<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：MonAuditingGatherInit.jsp
//程序功能：
//创建日期：2016-7-12
//创建人：ZXJ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script src="../common/javascript/Common.js"></script>
<%
	PubFun tpubFun = new PubFun();
	String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
	function initInpBox(){
		try{
			var cLen = manageCom.length;
		if(cLen==2)
			fm.ManageComHierarchy.value="总公司";
	    if(cLen==4)
			fm.ManageComHierarchy.value="省公司";
	    if(cLen==8)
			fm.ManageComHierarchy.value="中支公司";
		
		}catch(ex){
			alert("在-->IninInpBox函数中发生异常 ：初始化界面错误！");
		}
	}
	function initForm(){
		try{
			initElementtype();
			initInpBox();
			initMonthAudGaGrid();
		}catch(re){
			alert("LAAgentFYCInit.jsp-->InitForm函数中发生异常:初始化界面错误");
		}
	}
	function initMonthAudGaGrid(){
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "省公司代码";
			iArray[1][1] = "80px";
			iArray[1][2] = 40;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "省公司";
			iArray[2][1] = "80px";
			iArray[2][2] = 40;
			iArray[2][3] = 0;
			        
			iArray[3] = new Array();
			iArray[3][0] = "中支公司代码";
			iArray[3][1] = "80px";
			iArray[3][2] = 40;
			iArray[3][3] = 0;
			        
			iArray[4] = new Array();
			iArray[4][0] = "中支公司";
			iArray[4][1] = "80px";
			iArray[4][2] = 40;
			iArray[4][3] = 0;
			        
			iArray[5] = new Array();
			iArray[5][0] = "营业部代码";
			iArray[5][1] = "80px";
			iArray[5][2] = 40;
			iArray[5][3] = 0;
			        
			iArray[6] = new Array();
			iArray[6][0] = "营业部";
			iArray[6][1] = "100px";
			iArray[6][2] = 40;
			iArray[6][3] = 0;
			        
			iArray[7] = new Array();
			iArray[7][0] = "营业区代码";
			iArray[7][1] = "80px";
			iArray[7][2] = 40;
			iArray[7][3] = 0;
			        
			iArray[8] = new Array();
			iArray[8][0] = "营业区";
			iArray[8][1] = "80px";
			iArray[8][2] = 40;
			iArray[8][3] = 0;
			        
			iArray[9] = new Array();
			iArray[9][0] = "营业处代码";
			iArray[9][1] = "80px";
			iArray[9][2] = 40;
			iArray[9][3] = 0;
			
			iArray[10] = new Array();
			iArray[10][0] = "营业处";
			iArray[10][1] = "80px";
			iArray[10][2] = 40;
			iArray[10][3] = 0;
			         
			iArray[11] = new Array();
			iArray[11][0] = "营业组代码";
			iArray[11][1] = "80px";
			iArray[11][2] = 40;
			iArray[11][3] = 0;
			         
			iArray[12] = new Array();
			iArray[12][0] = "营业组";
			iArray[12][1] = "80px";
			iArray[12][2] = 40;
			iArray[12][3] = 0;
               
			iArray[13] = new Array();
			iArray[13][0] = "营销员代码";
			iArray[13][1] = "80px";
			iArray[13][2] = 40;
			iArray[13][3] = 0;
			         
			iArray[14] = new Array();
			iArray[14][0] = "营销员";
			iArray[14][1] = "80px";
			iArray[14][2] = 60;
			iArray[14][3] = 0;
			         
			iArray[15] = new Array();
			iArray[15][0] = "N月FYC为0";
			iArray[15][1] = "80px";
			iArray[15][2] = 40;
			iArray[15][3] = 0;
		           
			iArray[16] = new Array();
			iArray[16][0] = "最近考核时点前职级代码";
			iArray[16][1] = "135px";
			iArray[16][2] = 40;
			iArray[16][3] = 0;
			         
			iArray[17] = new Array();
			iArray[17][0] = "最近考核时点前职级";
			iArray[17][1] = "135px";
			iArray[17][2] = 40;
			iArray[17][3] = 0;
               
			iArray[18] = new Array();
			iArray[18][0] = "最近考核时点后职级代码";
			iArray[18][1] = "135px";
			iArray[18][2] = 40;
			iArray[18][3] = 0;
               
			iArray[19] = new Array();
			iArray[19][0] = "最近考核时点后职级";
			iArray[19][1] = "135px";
			iArray[19][2] = 40;
			iArray[19][3] = 0;			
			
			MonthAudGaGrid = new MulLineEnter("fm","MonthAudGaGrid");
		     
			MonthAudGaGrid.mulLineCount = 10;   
			MonthAudGaGrid.displayTitle = 1;
			MonthAudGaGrid.canSel = 1;
			MonthAudGaGrid.canChk = 0;
			MonthAudGaGrid.locked = 1;
			MonthAudGaGrid.hiddenSubtraction = 1;
			MonthAudGaGrid.hiddenPlus = 1;
			MonthAudGaGrid.selBoxEventFuncName ="";
		     
			MonthAudGaGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>