  <%
//程序名称：AgentAssessInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                
    fm.all('ManageCom').value = '';
    fm.all('AssessYear').value = '';
    fm.all('AssessMonth').value = '';
    fm.all('BranchType').value = getBranchType();
    fm.all('BranchType2').value = '<%=BranchType2%>';
    fm.all('AgentSeries').value ='<%=AgentSeries%>';
    fm.all('AgentGrade').value ='A01';
    fm.all('AgentGradeName').value ='客户代表';
    fm.all('CalFlag').value ='<%=CalFlag%>';
   
    if(fm.all('AgentSeries').value=='0')
    {
    	  fm.all('AgentSeriesName').value = '业务系列';
    }
    else if(fm.all('AgentSeries').value!='0')
    {
        fm.all('AgentSeriesName').value = '主管系列';
    }
  }
  catch(ex)
  {
    alert("AgentAssessInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                  
 var AssessQueryGrid ;
 
// 保单信息列表的初始化
function initAssessQueryGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
 
      iArray[1]=new Array();
      iArray[1][0]="代理人编码";         	//列名
      iArray[1][1]="80px";              	//列宽
      iArray[1][2]=200;            	        //列最大值
      iArray[1][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="代理人组别";         	//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=200;            	        //列最大值
      iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="管理机构";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            	        //列最大值
      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="初年度佣金";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            	        //列最大值
      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="育成奖金";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=200;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      
      AssessQueryGrid = new MulLineEnter( "fm" , "AssessQueryGrid" ); 
      //这些属性必须在loadMulLine前
      AssessQueryGrid.mulLineCount = 10;   
      AssessQueryGrid.displayTitle = 1;
      AssessQueryGrid.hiddenPlus = 1;
      AssessQueryGrid.hiddenSubtraction = 1;
      AssessQueryGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
 }
function initForm()
{
  try
  {
    initInpBox();
    initAssessQueryGrid();
  }
  catch(re)
  {
    alert("AgentAssessInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>
