<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>  
<%
//程序名称：LAEmployeeAscriptionInput.jsp
//程序功能：
//创建日期：2003-03-01 
//创建人  ：LL
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
 
  <SCRIPT src="LAEmployeeAscriptionInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAEmployeeAscriptionInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  
  <!--script>
  	function test() {
  	  var a = 1;
  	}
  	
  	alert(turnPage);
  	var p = new turnPageClass();
  </script-->
</head>
<body  onload="initForm();" >    
  <form action="./LAEmployeeAscriptionSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		代理人员工制归属
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='code' name=ManageCom verify="管理机构|notnull&code:comcode"
                                 onDblClick="return showCodeList('comcode',[this]);" >
          </TD>
  	  <TD  class= title>
            考核代码
          </TD>
          <TD  class= input>
            <Input class=common  name=IndexCalNo verify="考核代码|NOTNULL&INT" >(例:200401)
          </TD>
        </TR>
        <TR class=input>     
          <TD  class= title>
            待遇职级
          </TD>
          <TD  class= input>
            <Input class='code' name=AgentGrade1 onDblClick="return showCodeList('employeegrade',[this]);" >
          </TD>
           
          <TD class=common>
            <input type =button class=common value="查询" onclick="easyQueryClick();">   
          </TD>
          
          <Input type=hidden name=mOperate value="">
        </TR>          
      </table>
    </Div>  
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBeforeGrid);">
    		</td>
    		<td class= titleImg>
    			 归属前代理人信息
    		</td>
    	</tr>
   </table>
   
   <Div id="divBeforeGrid" style="display:''">
    <table class=common>
     <tr class=common>
      <td text-align:left colSpan=1>
       <span id="spanBeforeGrid"></span>
      </td>
     </tr>
    </table>
      <INPUT VALUE=" 首页 "  TYPE="button" onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" onclick="turnPage.lastPage();">
  </Div>
  
  <table class=common>
   <tr class=common>
    <td class=input width="26%" colspan=4 align=center><input type=button value="员工制归属" class=common onclick="AgentAscript();"></td>
   </tr>
   <tr class=common>
    <td class=input width="26%" colspan=4 align=center><input type=button value="归属结果查询" class=common onclick="qryAfterAscript();"></td>
   </tr>
  </table>
    
    <input type=hidden name=BranchType value=''> 	     
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>