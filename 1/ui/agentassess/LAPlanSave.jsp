<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAPlanInput.jsp
//程序功能：
//创建日期：2003-07-08 
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAPlanSchema tLAPlanSchema   = new LAPlanSchema();

  ALAPlanUI tALAPlanUI = new ALAPlanUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  System.out.println("mOperate:"+request.getParameter("PlanPeriodUnit"));
  tOperate = tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();

  //tG.Operator = "Admin";
  //tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");
  
  tLAPlanSchema.setPlanCode(request.getParameter("PlanCode"));
  tLAPlanSchema.setPlanType(request.getParameter("PlanType"));
  tLAPlanSchema.setBranchType(request.getParameter("BranchType"));
  tLAPlanSchema.setBranchType2(request.getParameter("BranchType2"));
  tLAPlanSchema.setPlanPeriodUnit(request.getParameter("PlanPeriodUnit"));
  tLAPlanSchema.setPlanPeriod(request.getParameter("PlanPeriod"));
  System.out.println("PlanPeriodUnit:"+request.getParameter("PlanPeriodUnit"));
  tLAPlanSchema.setPlanObject(request.getParameter("PlanObject"));
  if(request.getParameter("PlanCond3")==null||request.getParameter("PlanCond3").equals(""))
    tLAPlanSchema.setPlanCond3("5");
   else
    tLAPlanSchema.setPlanCond3(request.getParameter("PlanCond3"));

  tLAPlanSchema.setPlanStartDate(request.getParameter("PlanStartDate"));
  tLAPlanSchema.setPlanEndDate(request.getParameter("PlanEndDate"));
  tLAPlanSchema.setPlanValue(request.getParameter("PlanValue"));
  tLAPlanSchema.setPlanCond1(request.getParameter("PlanCond1"));
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tLAPlanSchema);
  tVData.add(tG);
  try
  {
    tALAPlanUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tALAPlanUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.Operator.value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

