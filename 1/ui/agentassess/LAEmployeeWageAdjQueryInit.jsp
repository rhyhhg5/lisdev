<%
//程序名称：LAEmployeeWageAdjInit.jsp
//程序功能：
//创建日期：2003-07-09 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
     
%>                            

<script language="JavaScript">

 var str = "";
 
function initInpBox()
{ 
  try
  {                                   
    fm.all('AgentCode').value ='';
    fm.all('Name').value ='';
    fm.all('BranchAttr').value ='';
  }
  catch(ex)
  {
    alert("在LAEmployeeWageAdjInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                   

function initForm()
{
  try
  {
    initInpBox();  
    initWageAdjGrid();  
  }
  catch(re)
  {
    alert("在LAEmployeeWageAdjInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}




// 待遇级别信息的初始化
function initWageAdjGrid()
{                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         //列名
      iArray[0][1]="30px";         //列名
      iArray[0][2]=100;         //列名
      iArray[0][3]=0;         //列名
		
      iArray[1]=new Array();
      iArray[1][0]="福利类别";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    
		iArray[2]=new Array();
		iArray[2][0]="福利金额";         //列名
		iArray[2][1]="100px";         //宽度
		iArray[2][2]=100;         //最大长度
		iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[3]=new Array();
		iArray[3][0]="个人交纳";         //列名
		iArray[3][1]="100px";         //宽度
		iArray[3][2]=100;         //最大长度
		iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
		
		
		iArray[4]=new Array();
		iArray[4][0]="公司交纳";         //列名
		iArray[4][1]="100px";         //宽度
		iArray[4][2]=100;         //最大长度
		iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[5]=new Array();
		iArray[5][0]="生效年月";         //列名
		iArray[5][1]="100px";         //宽度
		iArray[5][2]=100;         //最大长度
		iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[6]=new Array();
		iArray[6][0]="服务期限";         //列名
		iArray[6][1]="100px";         //宽度
		iArray[6][2]=100;         //最大长度
		iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[7]=new Array();
		iArray[7][0]="生效日期";         //列名
		iArray[7][1]="100px";         //宽度
		iArray[7][2]=100;         //最大长度
		iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
      
	        iArray[8]=new Array();
		iArray[8][0]="终止日期";         //列名
		iArray[8][1]="100px";         //宽度
		iArray[8][2]=100;         //最大长度
		iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
		
	        
	      WageAdjGrid = new MulLineEnter( "fm" , "WageAdjGrid" ); 
	      
	      //这些属性必须在loadMulLine前
	      WageAdjGrid.mulLineCount = 1;   
	      WageAdjGrid.displayTitle = 1; 
	      //WageAdjGrid.locked=1;//解锁"+/-"
              WageAdjGrid.canSel=1;
              WageAdjGrid.canChk=0;
              WageAdjGrid. hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)    
	      WageAdjGrid.loadMulLine(iArray); 
	      
	    
	      
      }
      catch(ex)
      {
        alert(ex);
      }
}




</script>