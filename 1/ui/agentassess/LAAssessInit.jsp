<%
//程序名称：LAAssessInit.jsp
//程序功能：
//创建日期：2003-07-14
//创建人  ：liujw
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

if(globalInput == null) {
	out.println("session has expired");
	return;
}

String strOperator = globalInput.Operator;
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">
var tBranchType = "<%=BranchType%>";
var tAgentTitleGrid = "营销员";
if("2"==tBranchType)
{
  tAgentTitleGrid = "业务员";
}
function initInpBox()
{
  try
  {
    fm.all('AgentGrade').value = '';
    fm.all('IndexCalNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';    
    fm.all('BranchType2').value = '<%=BranchType2%>';    
    fm.all('UpDown').value = '<%=request.getParameter("UpDown")%>';    
    if (fm.all('BranchType').value == '11')
    {
      fm.AscBtn.disabled = false;
    }
    else
    {
      fm.AscBtn.disabled = true;
      divQQQQ.style.display='none';
     }
  }
  catch(ex)
  {
    alert("在LAAssessInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initLAAssessGrid();
  }
  catch(re)
  {
    alert("LAAssessInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 var LAAssessGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initLAAssessGrid()
{
    var iArray = new Array();
    var i11Array = getAgentGradeStr();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]=tAgentTitleGrid+"代码";         		//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="AgentCode2";
      iArray[1][9]="业务员代码";
      iArray[1][5]="1|2|4|5";     //引用代码对应第几列，'|'为分割符 --Muline中的列位置
      iArray[1][6]="0|1|3|5";    //上面的列中放置引用代码中第几位值 --sql中位置
      iArray[1][15]="1";
      iArray[1][16]=strRT;
      iArray[1][18]=200;          //下拉框的宽度
      iArray[1][19]=1; //1是需要强制刷新
      //alert(strRT);

      iArray[2]=new Array();
      iArray[2][0]=tAgentTitleGrid+"业务员姓名";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="确认职级";      		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][10]='AgentGrade1';
      iArray[3][11]=i11Array;
      iArray[3][12]="3";
      iArray[3][13]="0";
      iArray[3][19]= 1; //1是需要强制刷新

      iArray[4]=new Array();
      iArray[4][0]="销售单位代码";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[5]=new Array();
      iArray[5][0]="业务员当前职级";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      LAAssessGrid = new MulLineEnter( "fm" , "LAAssessGrid" );
      //这些属性必须在loadMulLine前
      LAAssessGrid.displayTitle = 1;
        LAAssessGrid.mulLineCount = 1;
        LAAssessGrid.hiddenPlus=1;
        LAAssessGrid.hiddenSubtraction=1;
      LAAssessGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //LAAssessGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
