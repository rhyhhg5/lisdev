//               该文件中包含客户端需要处理的函数和事件
//程序名称：LADiscountInput.js
//程序功能：
//创建日期：2008-02-26
//创建人  ：   zhaojing
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var mOperate="";




//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//xiugai按钮对应操作
function updateClick()
{
	mOperate="UPDATE||MAIN";
	if(!beforeSubmit()) {
		return false;
	}
	if (confirm("您确实想修改该记录吗?"))
     {
        mOperate="UPDATE||MAIN";
        submitForm();
     }
     else
    {
        mOperate="";
        alert("您取消了修改操作！");
    }

}


function addClick() {
	mOperate="INSERT||MAIN";
	if(!beforeSubmit()) {
		return false;
	}
	submitForm();
  
}

//提交，保存按钮对应操作

function submitForm()
{
	
	
		
	  
 // AgentInfoGrid.delBlankLine("AgentInfoGrid");
   
  /*if(!AgentInfoGrid.checkValue())
  {
    return false 
  }*/ 
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  
  
   
  fm.submit(); //提交
  
  
  
}

function deleteClick()
{
	mOperate="DELETE||MAIN";
	if(!beforeSubmit()) {
		return false;
	}
	if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}

// 查询按钮
function easyQueryClick()
{
	//fm.all('saveButton').disabled = true;
	//alert ("start easyQueryClick");
	// 初始化表格
	initLADiscount1Grid();
	var tManageCom  = fm.ManageCom.value;

	if ( tManageCom == null || tManageCom == "" )
	{
		alert("请选择管理机构代码！");
		return;
	}


	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.idx, a.agentgrade,b.gradename,a.standprem "
				 + "from ladiscount a,laagentgrade b "
				 + "where a.agentgrade = b.gradecode and discounttype='02' "
				 + getWherePart( 'a.managecom','ManageCom');
				 
	
	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
			alert("没有符合条件的数据，请重新录入查询条件！");
			return false;
	}

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;

					//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
					//设置初始化过的MULTILINE对象
					turnPage.pageDisplayGrid = LADiscount1Grid;
					//保存SQL语句
					turnPage.strQuerySql = strSQL;

					//设置查询起始位置
					turnPage.pageIndex = 0;

					//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
					tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

					//调用MULTILINE对象显示查询结果
					displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
}


				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}



	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
	function resetForm()
		{
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
		}




	//显示frmSubmit框架，用来调试
	function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}


		//Click事件，当点击“修改”图片时触发该函数
		function DoUpdate()
		{
			if (confirm("您确实想修改该记录吗?"))
			{
				mOperate = "UPDATE||MAIN";
				if(!beforeSubmit()) return false;
				var i = 0;
				var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.submit(); //提交
			}
			else
				{
					fm.OperateType.value = "";
					alert("您取消了修改操作！");
				}
			}

							//判断是否选择了要增加、修改或删除的行
							function chkMulLine()
							{
								//alert("enter chkmulline");
								var i;
								var selFlag = true;
								var iCount = 0;
								var rowNum = LADiscount1Grid.mulLineCount;
								for(i=0;i<rowNum;i++)
								{
									
									if(LADiscount1Grid.getChkNo(i))
									{
										iCount++;
										if((LADiscount1Grid.getRowColData(i,2) == null)||(LADiscount1Grid.getRowColData(i,2) == ""))
   	   							 {
   	    								alert("职级不能为空");
   	    								LADiscount1Grid.setFocus(i,2,LADiscount1Grid);
   	    								selFlag = false;
   	    								break;
   	   							}
										if((LADiscount1Grid.getRowColData(i,4) == null)||(LADiscount1Grid.getRowColData(i,4) == ""))
										{
											alert("标准保费不能为空");
											LADiscount1Grid.setFocus(i,4,LADiscount1Grid);
											selFlag = false;
											break;
									  	}
										
										
										if(!isNumeric(LADiscount1Grid.getRowColData(i,4)))
   	   							 {
   	       							alert("标准保费不是有效的数字！");
   	       							LADiscount1Grid.checkBoxAllNot();
   	       							LADiscount1Grid.setFocus(i,4,LADiscount1Grid);
   	       							selFlag = false;
   	        						break;
   	     						}
										 


										//如果ｒａｔｅ为空不能删除和更新
										if((LADiscount1Grid.getRowColData(i,1) == null)||(LADiscount1Grid.getRowColData(i,1)==""))
	    							{
	    								if((mOperate =="DELETE||MAIN") || (mOperate =="UPDATE||MAIN"))
	    								{
	    	   							 alert("必须先查询出记录，再修改和删除！");
	    	   							 LADiscount1Grid.checkBoxAllNot();
	    	    						 LADiscount1Grid.setFocus(i,2,LADiscount1Grid);
	    	    						 selFlag = false;
	    	   							 break;
	    								}
	    							}
	    							else if ((LADiscount1Grid.getRowColData(i,1) != null)||(LADiscount1Grid.getRowColData(i,1)!=""))
   	                {	//如果Serialno不为空不能插入
   	    	            if(mOperate =="INSERT||MAIN")
   	    	            {
   	    	               alert("此纪录已存在，不可插入！");
   	    	               LADiscount1Grid.checkBoxAllNot();
   	    	               LADiscount1Grid.setFocus(i,2,LADiscount1Grid);
   	    	               selFlag = false;
   	    	                break;
   	    	             }
   	                }
								}		
									else
									{}
								}
										if(!selFlag) return selFlag;
										if(iCount == 0)
										{
											alert("请选择要保存或更改的记录!");
											return false
										}
										return true;
									
                  }
//--									//提交前的校验、计算
									function beforeSubmit()
									{
										if(fm.all('ManageCom').value==null||fm.all('ManageCom').value=="") {
											alert("管理机构不能为空！");
											return false;
										}
										
										if(!chkMulLine()) return false;
										
										return true;
									}

									function showDiv(cDiv,cShow)
									{
										if (cShow=="true")
										{
											cDiv.style.display="";
										}
										else
											{
												cDiv.style.display="none";
											}
									}

									

						





