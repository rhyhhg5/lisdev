<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：EmployeeAssessConfirmSave.jsp
//程序功能：
//创建日期：2003-3-26
//创建人  ：zsj
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>

<%
  //输出参数
  String FlagStr = "";
  String Content = "";

  EmployeeAssessConfirmSave tConfirm = new EmployeeAssessConfirmSave();
  VData tVData = new VData();

  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  System.out.println("ManagCom : " + tGI.ManageCom);
  System.out.println("Operator : " + tGI.Operator);
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {    
      String tManageCom  = request.getParameter("ManageCom");
      String tAgentGrade = request.getParameter("AgentGrade");
      String tIndexCalNo = request.getParameter("IndexCalNo");
      String Type = request.getParameter("SubmitType");
      System.out.println("----ManageCom: " + tManageCom);
      System.out.println("----AgentGrade: " + tAgentGrade);
      System.out.println("----IndexCalNo: " + tIndexCalNo);
      System.out.println("----Type: " + Type);
      
      tVData.clear();
      tVData.add(tGI);
      tVData.add(0,tManageCom);
      tVData.add(1,tIndexCalNo);
      tVData.add(2,tAgentGrade);      

      if ( !tConfirm.getInputData(tVData) )  {
        FlagStr = "Fail";
        Content = FlagStr + "保存失败，原因是：" +
                  tConfirm.mErrors.getFirstError();
        System.out.println("FlagStr:  " + FlagStr);
        System.out.println("Content:  " + Content);      
      }
      
      if ( Type.trim().equals("All") )  {
        if ( !tConfirm.dealAll() )  {
          FlagStr = "Fail";
          Content = FlagStr + "保存失败，原因是：" +
                    tConfirm.mErrors.getFirstError();
          System.out.println("FlagStr:  " + FlagStr);
          System.out.println("Content:  " + Content);
        }
        else
        {
          FlagStr = "Succ";
          Content = FlagStr + "保存成功！";        
        }
      }
      
      if ( Type.trim().equals("Part") )  {
        LAAssessAccessorySet tSet = new LAAssessAccessorySet();
        
        String tChk[] = request.getParameterValues("InpAgentInfoGridChk");
        String tAgentCode[] = request.getParameterValues("AgentInfoGrid1");
        int tCount = tAgentCode.length;
        
        int tR = 0;
        int tIndex = 0;

        for ( tIndex=0;tIndex<tCount;tIndex++ )
        {
          if (tChk[tIndex].equals("1") )
          {
            tAgentCode[tR] = tAgentCode[tIndex];
            tR +=1;
          }
        }
        if ( tR == 0 )
        {
          FlagStr = "Fail";
          Content = FlagStr + "没有选中被确认的代理人！";
        }
        else  {	      
	      for (tIndex=0;tIndex<tR;tIndex++)
	      {
	        LAAssessAccessoryDB tDB = new LAAssessAccessoryDB();
	        tDB.setAgentCode(tAgentCode[tIndex]);
	        tDB.setAssessType("01");
	        tDB.setIndexCalNo(tIndexCalNo);
	        if ( !tDB.getInfo() )
	        {
	          FlagStr = "Fail";
	          Content = FlagStr + "LAAssessAccessory表中无此代理人" + tAgentCode[tIndex].trim()
	                  + "记录！";
	        }
	        else
	        {
	          tSet.add(tDB.getSchema());
	        }
	      }  
        }
       
        if ( !tConfirm.dealData(tSet) )  {
          FlagStr = "Fail";
          Content = FlagStr + "保存失败，原因是：" +
                    tConfirm.mErrors.getFirstError();
          System.out.println("FlagStr:  " + FlagStr);
          System.out.println("Content:  " + Content);
        }
        else
        {
          FlagStr = "Succ";
          Content = FlagStr + "保存成功！";        
        }
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "保存失败，原因是：" + ex.toString();
    }
  }// tGI!=null
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
