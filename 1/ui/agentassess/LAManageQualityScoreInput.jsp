<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<% 
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String Operator = tG.Operator;
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAManageQualityScoreInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAManageQualityScoreInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>

<body  onload="initForm();initElementtype();" >
  <form action="./LAManageQualityScoreSave.jsp" method=post name=fm target="fraSubmit">
    <table class="common" align=center>
			<tr align=right>
				<td class=button >
					&nbsp;&nbsp;
				</td>
				<td class=button width="10%" style="display:none">
					<INPUT class=cssButton VALUE="标准查询"  name=Query TYPE=button onclick="return queryStandClick();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="保  存" name=Save TYPE=button onclick="return submitForm();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="修  改" name=Modify TYPE=button onclick="return updateClick();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="查  询 " name=reset TYPE=button onclick="return queryClick();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="删  除" name=reset TYPE=button onclick="return deleteClick();">
				</td>
			</tr>
		</table>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLARewardPunish1);">
    
    <td class=titleImg>
      营销员综合评分信息
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLAActiveManage1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		 营销员代码 
		</td>
        <td  class= input> 
		  <input class='common'  name=GroupAgentcode  verify="营销员代码 |notnull"  OnChange="return checkCode();" elementtype=nacessary  > 
		</td>
		<td  class= title>
		  营销员姓名
		</td>
        <td  class= input>
		  <input name=Name class='readonly' readonly >
		</td>
      </tr>
      <tr  class= common> 
      <td  class= title> 
		  销售单位
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=AgentGroupName > 
		</td>
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=ManageComName > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  分数
		</td>
        <td  class= input> 
		  <input name=Score class= readonly  readonly > 
		</td>
      <td  class= title> 
		  执行年份
		</td>
        <td  class= input> 
		  <input name=Year class=common verify="执行年份|NOTNULL&INT&len=4"  elementtype=nacessary  > 
		</td>
  
	</tr>
	  <tr class=common>
	    <td  class= title> 
		  执行月份
		</td>
		<TD  class= input>
			<Input class="codeno" id=Month1 name=Month1 CodeData="0|^03|3月份|^06|6月份|^09|9月份|^12|12月份|" verify="执行月份|notnull" readOnly=''
			ondblclick="return showCodeListEx('activemonth',[this,MonthName],[0,1]);" 
			onkeyup="return showCodeListKeyEx('activemonth',[this,MonthName],[0,1]);"
			><Input class=codename name=MonthName readOnly elementtype=nacessary > 
		</TD> 
    <td  class= title> 
		  操作员代码 
		</td>
    <td  class= input> 
		  <input class="readonly"  readonly name=Operator value = "<%=tG.Operator%>" > 
		</td>
  </tr>
  <tr>
   <td  class= title>
		   最近操作日
		</td>
    <td  class= input>
		  <input name=MakeDate class="readonly" value = "<%=PubFun.getCurrentDate()%>" readonly >
		</td>
  </tr>
  </table>
  </Div>
     <table>
	 	<tr>
	 		<td class=common>
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
 			<td class= titleImg>
	 				综合评分分数录入   <font color="red">（注：如果不录入项目代码所对应的扣减分数，则此项扣减分数默认为零）</font>
 			</td>
	 	</tr>
	 </table>
 <Div  id= "divLAAgent2" style= "display: ''">
 	<table  class= common>
 		<tr  class= common>
 			<td text-align: left colSpan=1>
 				<span id="spanLAAgentCompreScoreGrid" >
 				</span>
 			</td>
 		</tr>
 	</table> 
 </div>
 	<table>
	 	<tr>
	 		<td >
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
	 			<td class= titleImg>
		 				综合服务评分标准
	 			</td>
	 	</tr>
	 </table>
<Div  id= "divLAAgent3" style= "display: ''">	 
 <table  class= common>
 		<tr  class= common>
 			<td text-align: left colSpan=1>
 				<span id="spanLAManageQualityScoreGrid" >
 				</span>
 			</td>
 		</tr>
 	</table>
 	</Div>
 <Div  id= "divLAActiveManageStand" style= "display: 'none'">
<hr/>
 	 <table>
	 	<tr>
	 		<td >
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessone);">
	 			<td class= titleImg>
		 				综合服务评分标准
	 			</td>
	 	</tr>
	 </table>
	 
	 <Div  id= "divAssessone" style= "display: ''">
		 <table border="2" cellpadding="2" cellspacing="0" align="center">
			<tr class=common>
			  <th >综合服务评分(r)</th>
			  <th >综合服务评分系数</th>
			</tr>
	 		<tr class= common align="center">
				<td>r≥90</td>
				<td>1.2</td>
			</tr>
			<tr class= common align="center">
				<td>80≤r< 90</td>
				<td>1</td>
			</tr>
			<tr class= common align="center">
				<td>60≤r< 80</td>
				<td>0.7</td>
			</tr>
			<tr class= common align="center">
				<td>r< 60</td>
				<td>0.4</td>
			</tr>
	 	</table>
 	</div>
 </Div>
    <input type=hidden name=AgentGroup value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=AgentCode value=''>
    <input type=hidden name=ManageCom value=''>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=hiddenOperator value=<%=Operator%>>
    <input type=hidden name=wageno1 value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

