<%
//程序名称：LAAssessInit.jsp
//程序功能：
//创建日期：2005-7-14 9:57
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//*********************************************************************************************
String pmSaveData = request.getParameter("saveData");
String tType = "0";
String tSaveData = "";
if (pmSaveData=="null"||pmSaveData==null||pmSaveData=="")
{
  tSaveData  = "0;1101000001;郭铃;86110000010001;86110000;B02;87000;32;0.732;无挂零;良;维持";
  tSaveData += ",1;1101000002;王晓玲;86110000010001;86110000;A01;5600;3;0;无挂零;良;晋升";
}else
{
  tType = "1";
  tSaveData = pmSaveData;
}

//*********************************************************************************************

//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

if(globalInput == null) {
	out.println("session has expired");
	return;
}

String strOperator = globalInput.Operator;
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">
//alert("<%=tSaveData%>");
//alert("<%=tType%>");
function initInpBox()
{
  try
  {
    fm.all('AgentGrade').value = '';
    fm.all('IndexCalNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';    
    fm.all('BranchType2').value = '<%=BranchType2%>';
    document.fm.saveData.value = "<%=tSaveData%>";
    fm.all('actType').value = '0';
  }
  catch(ex)
  {
    alert("在LAAssessDealInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  var tType = "<%=tType%>";
  try
  {
    initInpBox();
    initLAAssessGrid();
    if(tType == "1")
    {
      initLAAssessGrid();
      //alert("<%=tSaveData%>");
      initLAAssessGrid1("<%=tSaveData%>");
    }
    
  }
  catch(re)
  {
    alert("LAAssessDealInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 var LAAssessGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initLAAssessGrid()
{
    var iArray = new Array();
    //var i11Array = getAgentGradeStr();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="业务员代码";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      /*
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="AgentCode2";
      iArray[1][9]="业务员代码";
      iArray[1][5]="1|2|4|5";     //引用代码对应第几列，'|'为分割符 --Muline中的列位置
      iArray[1][6]="0|1|3|5";    //上面的列中放置引用代码中第几位值 --sql中位置
      iArray[1][15]="1";
      iArray[1][16]=strRT;
      iArray[1][18]=200;          //下拉框的宽度
      iArray[1][19]=1; //1是需要强制刷新
      */
      //alert(strRT);

      iArray[2]=new Array();
      iArray[2][0]="姓名";         		//列名
      iArray[2][1]="50px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="管理机构";      		   //列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="销售机构";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[5]=new Array();
      iArray[5][0]="当前职级";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="首年度FYC";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="客户数";         		//列名
      iArray[7][1]="40px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="第13个月继续率";         		//列名
      iArray[8][1]="90px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="挂零情况";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="培训考评";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="初评结果";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="考评结论";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许'assessverdict'
      iArray[12][4]='assessverdict';
      iArray[12][18]=60;          //下拉框的宽度

      LAAssessGrid = new MulLineEnter( "fm" , "LAAssessGrid" );
      //这些属性必须在loadMulLine前
      LAAssessGrid.mulLineCount = 3;   
      LAAssessGrid.displayTitle = 1;
      LAAssessGrid.hiddenPlus = 1;
      LAAssessGrid.hiddenSubtraction = 1;
      LAAssessGrid.locked=1;
      LAAssessGrid.canSel=0;
      LAAssessGrid.canChk=0;
      LAAssessGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //LAAssessGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
