var turnPage = new turnPageClass();

/***************************************************
 * 执行计算操作
 ***************************************************
 */
 function AgentCalExamineSave()
 {
 	 submitForm();
 }

/***************************************************
 * 执行提交操作
 ***************************************************
 */
 function submitForm()
 {
   //对非空进行验证
   if( verifyInput() == false ) return false;
   if(!Beforsubmit()) return false;
   var i = 0;
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
   fm.submit(); //提交
 }
 /***************************************************
 * 校验
 ***************************************************
 */
 function Beforsubmit()
{
 
	 return true;	
 	
}
/***************************************************
 * 提交后操作,服务器数据返回后执行的操作
 ***************************************************
 */
 function afterSubmit( FlagStr, content )
 {
 	 showInfo.close();
 	
   if (FlagStr == "Fail" )
   {
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
     showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else
   {
     var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
     showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
     //执行下一步操作
   }
 }