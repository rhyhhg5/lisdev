
<%
//程序名称：LACrsSaleRateInit.jsp
//程序功能：互动渠道考核交叉销售比例初始化
//创建日期：2015-03-10   16:03:45
//创建人  ：yangyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     String currdate = PubFun.getCurrentDate();
%>
<script language="JavaScript">
var tmanagecom =" 1 ";
var tagentcode =" 1 and branchtype=#"+'<%=BranchType %>'+"# and branchtype2=#"+'<%=BranchType2 %>'+"#";
function initInpBox()
{ 
  try
  {                                
	fm.all('ManageCom').value = <%=tG.ManageCom%>;
	var sql="select name from ldcom where comcode ='"+<%=tG.ManageCom%>+"'";
	var array=easyExecSql(sql);
	fm.all('ManageComName').value =array[0][0];
	fm.all('AssessType').value = '';
	fm.all('AssessTypeName').value = '';
  }
  catch(ex)
  {
    alert("在LACrsSaleRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  
  }
  catch(ex)
  {
    alert("在LACrsSaleRateInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCrsSaleRateGrid();  
  }
  catch(re)
  {
    alert("LACrsSaleRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
} 
// 考核职级的标准的初始化
function initCrsSaleRateGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="管理机构";          		        //列名
      iArray[1][1]="80px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  iArray[1][4]="comcode";              	        //是否引用代码:null||""为不引用
	  iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	  iArray[1][6]="0|1";
	  iArray[1][9]="管理机构|NotNull";  
	  iArray[1][15]="1";
	  iArray[1][16]=tmanagecom;
    
      iArray[2]=new Array();
	  iArray[2][0]="管理机构名称";      	   		//列名
	  iArray[2][1]="80px";            			//列宽
	  iArray[2][2]=20;            			//列最大值
	  iArray[2][3]=1; 
	
	  iArray[3]=new Array();
	  iArray[3][0]="考核方式";         		        //列名
	  iArray[3][1]="80px";            			//列宽
	  iArray[3][2]=10;            			//列最大值
	  iArray[3][3]=2;  
	  iArray[3][9]="考核方式|NotNull";
	  iArray[3][10]="AssessFlag";
	  iArray[3][11]="0|^1|月均互动开拓提奖|^2|季度规模保费";
	  iArray[3][12]="3|4";              	                //引用代码对应第几列，'|'为分割符
	  iArray[3][13]="0|1";            			
	
	  iArray[4]=new Array();
	  iArray[4][0]="考核方式名称";      	   		//列名
	  iArray[4][1]="80px";            			//列宽
	  iArray[4][2]=20;            			//列最大值
	  iArray[4][3]=1; 
	
	  iArray[5]=new Array();
	  iArray[5][0]="比率";      	   		//列名
	  iArray[5][1]="80px";            			//列宽
	  iArray[5][2]=20;            			//列最大值
	  iArray[5][3]=1; 
	  
	  iArray[6]=new Array();
	  iArray[6][0]="操作人";      	   		//列名
	  iArray[6][1]="0px";            			//列宽
	  iArray[6][2]=20;            			//列最大值
	  iArray[6][3]=3; 
		
	  iArray[7]=new Array();
	  iArray[7][0]="管理机构";      	   		//列名
	  iArray[7][1]="0px";            			//列宽
	  iArray[7][2]=20;            			//列最大值
	  iArray[7][3]=3; 
		
		
	  iArray[8]=new Array();
	  iArray[8][0]="考核类型";      	   		//列名
	  iArray[8][1]="0px";            			//列宽
	  iArray[8][2]=20;            			//列最大值
	  iArray[8][3]=3; 
		
      CrsSaleRateGrid = new MulLineEnter( "fm" , "CrsSaleRateGrid" ); 
      //这些属性必须在loadMulLine前
      CrsSaleRateGrid.mulLineCount = 0;   
      CrsSaleRateGrid.displayTitle = 1;  
      CrsSaleRateGrid.hiddenPlus =0;  
      CrsSaleRateGrid.canChk  =1;  
      CrsSaleRateGrid.hiddenSubtraction = 1;
      CrsSaleRateGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
