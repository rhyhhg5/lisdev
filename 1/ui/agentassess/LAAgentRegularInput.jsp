<html>
<%
//程序名称：LAAgentRegularInput
//程序功能：互动渠道的试用期转正
//创建时间：2015-01-13
//更新记录：更新人   更新时间   更新原因
%>
<%
String BranchType=request.getParameter("BranchType");
String BranchType2=request.getParameter("BranchType2");
GlobalInput tG = (GlobalInput)session.getValue("GI");
%>
<script>
   	var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
	</script>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=GBK">-->
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

   <SCRIPT src="LAAgentRegularInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="../common/jsp/UsrCheck.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="LAAgentRegularInit.jsp"%>
	
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./LAAgentRegularInputSave.jsp" method=post name=fm target="fraSubmit">
 <table class="common" align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return agentConfirm();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="return queryClick();">
			</td>
		</tr>
	
    </Table>
  <table class=common>
   <tr class=common>
    <td class=title>营销员管理机构</td>
    <td class=input > 
    <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL"
            readOnly><Input class="codename" name=ManageComName  readOnly > 
      </td>
    <td class=title nowrap>营销员入司时间</td>
    <td class=input > <input class=common type=text name=employdate  readOnly></td>
   </tr> 
   <tr>
    <td class=title>营销员编码</td>
    <td class=input > 
    	<input class=common  type=hidden name=Agentcode  readOnly>
    	<input class=common  type=text name=groupAgentcode  readOnly>
    </td>    
    <td class=title>营销员名称</td>
    <td class=input > <input class=common type=text name=agentName  readOnly></td>  
    </tr>
    <tr>
    <TD  class= title>
            转正日期
   </TD>
    <td  class= input>
            <Input class= 'coolDatePicker' verify="转正日期|NOTNULL&Date" name=InDueFormDate format='short'  elementtype=nacessary><span style="color:red">  'YYYY-MM-DD'</span>
    </td>  
    <td class=title>转正状态</td>
           	<TD class=input><Input name=indueformflag class='codeno' readOnly>
           	<Input class=codename name=indueformflagName  readOnly></TD>      
   </tr>
   <TD  class= title>
            营销员职级 
          </TD>
          <TD  class= input>
            <Input class="codeno" name=AgentGrade  
            ><Input class=codename name=AgentGradeName readOnly >
          </TD>
          <td class=title>考核指标</td>
           	<TD class=input><Input name=assessType class='codeno' verify="考核指标|notnull" 
			><Input class=codename name=assessTypeName  readOnly></TD>     
        </tr>
         <tr>
    <td class=title>操作员代码</td>
    <td class=input > 
    	<input class=common  type=text name= operator  value = "<%=tG.Operator%>" readOnly>
    </td>    
    <td class=title>操作时间</td>
    <td class=input > <input class=common type=text name= makedate value = "<%=PubFun.getCurrentDate()%>" readOnly></td>  
    </tr>
    <tr></tr>
    <TR  class= common>
    					
						<TD  class=input colSpan= 3>
							<font color='red'>提示：只能录入转正日期且转正日期必须为每月1号，代理人在入职一个月以后可以转正</font>
						</TD>
					</TR>
  </table>
    <Input name= BranchType type= hidden value= ''>
    <Input name= BranchType2 type= hidden value= ''>
 </form>
 <span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>




