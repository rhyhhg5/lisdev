   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}

//代码限制长度
function initEdorType(cObj)
{
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeList('comcode',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeListKey('comcode',[cObj], null, null, mEdorType, "1");
}  
 
//提数操作
function submitForm()
{

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  //showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行计算         
function EmployeeAssessSave()
{
  //首先检验录入框
  if(!verifyInput()) return false;   
  
  /*
  //校验：该年月的佣金计算过而且下月的佣金每算过，才可进行考核计算
  var tIndexCalNo = trim(fm.all('IndexCalNo').value);
  var strSQL = "select distinct IndexCalNo from LAWage where IndexCalNo = '"+tIndexCalNo+"' "
              +"and not exists(select * From LAWage Where IndexCalNo > '"+tIndexCalNo+"' "           
              +getWherePart('BranchType')
              +getWherePart('ManageCom')+")"
              +getWherePart('ManageCom')
              +getWherePart('BranchType');
  var tResult = easyQueryVer3(strSQL,1,1,1);  
  if (!tResult)
  {
      alert("现在无法进行该年月的考核！");
      return false;
  } 
  */

  //divAgentQuery.style.display='none'; 
  fm.mOperate.value = 'INSERT||MAIN';
  submitForm();	
}  

function clearHistoryData()
{
  //首先检验录入框
  if(!verifyInput()) return false;
  
  //判断该职级是否已确认归属
  var tReturn = parseManageComLimitlike();
  var tIndexCalNo = trim(fm.all('WageYear').value)+trim(fm.all('WageMonth').value);
  var strSql = "select AgentCode from laassess where IndexCalNo = '"+tIndexCalNo+"' "
              +"And State <> '0' "
              +getWherePart('AgentGrade')
              +getWherePart('ManageCom')+tReturn
              +getWherePart('BranchType');

  //alert(strSql);	    
  //查询SQL，返回结果字符串
  var strResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (strResult) {  
    alert("该职级已确认归属，不能清除数据重新考核！");
    return false;
  }
  
  fm.mOperate.value = 'DELETE||MAIN';
  submitForm();
}                  

