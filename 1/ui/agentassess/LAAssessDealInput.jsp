<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String UpDown=request.getParameter("UpDown");
%>
<script>
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK"%>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LAAssessDealInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAAssessDealInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <title> 业务员考核信息管理 </title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
	  <td class= titleImg align= center>请输入考核信息的查询条件：</td>
	</tr>
	</table>
    <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>
            业务员编码
          </TD>
          <TD  class= title>
            <Input class=common name=AgentCode>
          </TD>
          <TD  class= title>
            业务员姓名
          </TD>
          <TD  class= title>
            <Input class=common name=Name>
          </TD>
        </TR>
      	<TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom  verify = "管理机构|notnull&code:comcode"
                   ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);">
          </TD>
          <TD  class= title>
            销售机构
          </TD>
          <TD  class= input>
            <Input class=common name=AgentGroup>
          </TD>
        </TR>
      	<TR  class= common>
          <TD  class= title>
            考核年月
          </TD>
          <TD  class= input>
            <Input class=common name=IndexCalNo verify="考核年月|notnull&len<=6&int" onchange="return changeYearMonth();">
          </TD>
      	<TD  class= title>
            业务员职级 
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentGrade  ondblclick="return showCodeList('AgentGrade',[this],null,null,msql,1);" 
                   onkeyup="return showCodeListKey('AgentGrade',[this],null,null,msql,1);">
          </TD>
        </TR>
    </table>
    <input type=hidden name=BranchType value='<%=BranchType%>'>
    <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAssess1);">
    		</td>
    		<td class= titleImg>
    			 考核信息
    		</td>
    	</tr>
    </table>
    <Input type=hidden name=saveData>
    <Input type=hidden name=actType>
  	<Div  id= "divLAAssess1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLAAssessGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton > 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
      <p>
      <INPUT VALUE="保  存" TYPE=button class=cssbutton onclick="getSaveData();"> 
      <INPUT VALUE="查  询" TYPE=button class=cssbutton onclick="initLAAssessGrid1();">
      </p>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
