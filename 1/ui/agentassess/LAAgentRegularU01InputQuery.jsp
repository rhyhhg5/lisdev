<html>
<%
//程序名称：LAAgentRegularInput
//程序功能：互动渠道的试用期转正
//创建时间：2015-01-13
//更新记录：更新人   更新时间   更新原因
%>
<%
String BranchType=request.getParameter("BranchType");
String BranchType2=request.getParameter("BranchType2");
GlobalInput tG = (GlobalInput)session.getValue("GI");

%>
<script>
   	var msql=" 1 and GradeCode<>#U01# and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
	</script>
<%@page contentType="text/html;charset=GBK" %>
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=GBK">-->
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="LAAgentRegularU01InputQuery.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="../common/jsp/UsrCheck.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="LAAgentRegularU01InputQueryInit.jsp"%>
	
</head>

<body  onload="initForm();" >
 <form action="" method=post name=fm target="fraSubmit">
 <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		 <td class= titleImg>
        		查询条件
       		 </td>   		 
    	</tr>
    </table>  
  <table class=common>
   <tr class=common>
    <td class=title>营销员管理机构</td>
    <td class=input > 
    <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL"
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"
            ><Input class="codename" name=ManageComName  elementtype=nacessary > 
      </td>
    <td class=title nowrap>营销员入司时间</td>
    <td  class= input>
            <Input class= 'coolDatePicker' verify="转正日期|NOTNULL&Date" name=employdate format='short' elementtype=nacessary>
    </td>  
   </tr>
   <tr>
    <td class=title>营销员编码</td>
    <td class=input > 
    	<input class=common  type=text name=groupAgentcode >
    	<input class=common  type=hidden name=Agentcode >
    </td>    
    <td class=title>营销员名称</td>
    <td class=input > <input class=common type=text name=agentName></td>  
    </tr>
    <tr>
    <TD  class= title style="display:none">
            营销员职级 
          </TD>
          <TD  class= input style="display:none">
            <Input class="codeno" name=AgentGrade  
            ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
            onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
            ><Input class=codename name=AgentGradeName readOnly elementtype=nacessary>
          </TD>
    <td class=title>转正状态</td>
           	<TD class=input><Input name=indueformflag class='codeno'
					verify="转正状态|notnull"  CodeData="0|^0|未转正|^1|已转正"
					ondblclick="return showCodeListEx('indueformflag',[this,indueformflagName],[0,1]);"
					onkeyup="return showCodeListKeyEx('indueformflag',[this,indueformflagName],[0,1]);"
					><Input class=codename name=indueformflagName elementtype=nacessary readOnly></TD>      
   </tr>
  </table>     
  <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();">
  <INPUT VALUE="返  回" class="cssbutton" TYPE=button onclick="returnParent();"> 	
  
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessGrid);">
    		</td>
    		<td class= titleImg>
    			 佣金信息
    		</td>
    	</tr>
  </table>
  <div id="divWageGrid" style="display:''">
   <table class=common>
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanWageGrid"></span>
     </td>
    </tr>
   </table>      
      <INPUT class="cssButton"  VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class="cssButton"  VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class="cssButton"  VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class="cssButton"  VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">     
  </div>
  
    <Input name= BranchType type= hidden value= ''>
 </form>
 <span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>




