`<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：LAAgentFYCInput.jsp
//程序功能：
//创建日期：2016-07-12
//创建人：ZXJ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<html>
<%
	String tFlag = "";
  	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	//tFlag = request.getParameter("type"); 
%>
<script>	         
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	<%--var type = "<%=tFlag%>";--%>
</script>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>代理人N月FYC为0报表</title>
  <SCRIPT src="LAAgentFYCInput.js"></SCRIPT>
  <%@include file="LAAgentFYCInit.jsp"%>
</head>
<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divMonAudGa);">
				</td>
				<td class= titleImg>查询条件<span style="color:red;"> *为必录项</span></td>
			</tr>
		</table>
		<div  id="divMonAudGa" style="display:''">
			<table class=common>
				<tr class=common>					
					<td class=title>管理机构</td>
					<td class=input>
						<input class=readonly name=ManageCom value=<%=tGI.ManageCom%> >
					</td>
					<td class=title>管理机构层级</td>
					<td class=input>
						<input class=code name=ManageComHierarchy CodeData="0|^1|总公司^2|省公司^3|中支公司" verify="管理机构层级|notnull" elementtype=nacessary 
							ondblclick="return showCodeListEx('ManageComHierarchy',[this],[1],null,null,null,1);">
					</td>
				</tr>
				<tr>
					<td class=title>当前薪资月</td>
					<td class=input>
						<input maxLength=6 name=WageNo verify="当前薪资月|YYYYMM&notnull" elementtype=nacessary>
						<label style="color:red;">格式如:201606</label>
					</td>
					<td class=title>月时段</td>
					<td class=input>
						<input class=code name=MonthNum CodeData="0|^1|1^2|2^3|3^4|4^5|5^6|6^7|7^8|8^9|9^10|10^11|11^12|12" verify="月时段|notnull" elementtype=nacessary
							ondblclick="return showCodeListEx('MonthNum',[this],[1],null,null,null,1);">&nbsp;&nbsp;月
					</td>
				</tr>
				<tr>
				<td class=title>营销员代码</td>
					<td class=input>
						<input name=AgentCode >
					</td>
				</tr>
			</table>
		</div>
		
		<input class=common type=hidden name=querySql>
		<input class=common type=hidden name=lenh>
		<input type=button value=" 查  询 " class="cssButton" onclick="query();">
		<input type=button value=" 下  载 " class="cssButton" onclick="download();">
		<hr/>
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divMonAudGa);">
				</td>
				<td class= titleImg><span >查询结果</span></td>
			</tr>
		</table>
		<div id="MonthId" style="display:">
			<table class= common>
       			<tr class= common>
      	  			<td text-align: left colSpan=1>
  						<span id="spanMonthAudGaGrid" ></span> 
  			  		</td>
  				</tr>
    		</table>
    		<table align=left>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>		
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>