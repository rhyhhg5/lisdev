 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var queryCount=false;
var arrDataSet;
var turnPage = new turnPageClass();
//var turnPage2 = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{ 
  if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  }
  
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           


//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./LAEmployeeWageAdjQuery.html");
}           

/*
function getManageCom2()//把隐藏值取出来
 {
 var tManageCom;
 var tResult2;
 var tAgentCode=fm.all('AgentCode').value;
 var sSql = "select managecom from laagent where agentcode='"+tAgentCode+"'";
 var tResult2 = easyQueryVer3(sSql,1,1,1);
     if (tResult2)
     {      
      var arr = decodeEasyQueryResult(tResult2);
      tManageCom= arr[0][0];
      fm.all('ManageCom').value = tManageCom;}
      
 }
*
*/


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if (queryCount == false)
  {
    alert("请查询出要审核的数据！");
    return false;	  
  }
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
   parent.fraMain.rows = "0,0,50,82,*";
  }
 else {
  	parent.fraMain.rows = "0,0,0,82,*";
 }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//员工待遇级别信息字段
function queryWageAdj()
{
  var tReturn = getManageComLimitlike("a.ManageCom");
	
   var tAgentCode = fm.all('AgentCode').value;
   var strSql = "";
   var tResult;
   if (tAgentCode!='')
   {   	
   strSql = " select a.Name,b.branchattr from LAAgent a,LABranchGroup b where a.branchcode=b.agentgroup "
           +tReturn
           +" and a.agentcode='"+tAgentCode+"'";
   tResult = easyQueryVer3(strSql,1,1,1);
   }
   else  
  {  	
    alert("请输入员工编码！");
    fm.all('AgentCode').value = "";
    fm.all('Name').value = "";
    fm.all('BranchAttr').value = "";
    initInpBox();
    return false;    
  }
   //判断是否查询成功
  if (!tResult) 
  {
    alert("无此代理人！");
    fm.all('AgentCode').value  = "";
    fm.all('Name').value = "";
    fm.all('BranchAttr').value = "";
    initInpBox();
    return false;    
  }
   else
   {
   var arr = decodeEasyQueryResult(tResult);
   fm.all('Name').value= trim(arr[0][0]);
   fm.all('BranchAttr').value= trim(arr[0][1]);    
   }

  // 书写SQL语句
   
   var strSQL = ""; 
       strSQL = "select a.codename,b.summoney,b.PerMoney,b.GrpMoney,b.indexcalno,b.serveyear,b.wvaliddate,b.wvalidenddate from ldcode a,lawelfareinfo b where"
               +" a.codetype='employeeaclass' and a.code=b.aclass and b.agentcode='"+tAgentCode+"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) 
  {  
    alert("查询失败！");
    return false;
  }    
 

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = WageAdjGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);

 }  
 

function getQueryResult()
{
	var arrSelected = null;
	var tRow = WageAdjGrid.getSelNo();
	if( tRow == 0 || tRow == null ||arrDataSet == null )
	  return arrSelected;
	arrSelected = new Array();
	
	var tAgentCode = fm.all('AgentCode').value;
	if (tAgentCode==""||tAgentCode==null)
	{
	   fm.all('AgentCode').value = "";
           initInpBox();
           return false;    	
        }
        else
        {
          var strsql = "select code from ldcode where codename='"+WageAdjGrid.getRowColData(tRow-1,1)+"'";
          tResult = easyQueryVer3(strsql,1,1,1);
          var arr = decodeEasyQueryResult(tResult);
          tCode=trim(arr[0][0]);
          //alert(tCode);
          
          var  strSQL = "select b.agentcode,c.name,b.BranchAttr,b.AClass,f.gradename,b.SumMoney,b.indexcalno,b.ServeYear,b.WValidDate,b.WValidEndDate "
                      +" from LAAgent c,ldcode a,lawelfareinfo b,latree e,laagentgrade f where a.codetype='employeeaclass' "
                      +" and e.agentcode=c.agentcode and c.agentcode=b.agentcode and e.branchcode=f.gradecode and a.code=b.aclass and a.code='"+tCode+"' and c.agentcode='"+tAgentCode+"'";
	}
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	arrSelected = decodeEasyQueryResult(strQueryResult);
	

	//设置需要返回的数组
	
	return arrSelected;
}





function returnParent()
{
  var arrReturn = new Array();
	var tSel = WageAdjGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}



