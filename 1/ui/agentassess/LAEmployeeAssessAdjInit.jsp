<%
//程序名称：LAEmployeeAssessAdjInit.jsp
//程序功能：
//创建日期：2004-02-16 09:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = '';
   // alert("1111");
    fm.all('AgentCode').value = '';
   // alert("2222");
    fm.all('AgentGrade').value = '';
   // alert("3333");
    fm.all('QueryType').value = '';
   // alert("4444");
    fm.all('BranchAttr').value = '';
   // alert("5555");
  }
  catch(ex)
  {
    alert("在LAEmployeeAssessAdjInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}


function initForm()
{
  try
  {
    initInpBox();
    initAgentAdjGrid();
  }
  catch(re)
  {
    alert("在LAEmployeeAssessAdjInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentAdjGrid
 ************************************************************
 */
function initAgentAdjGrid()
  {                               
    var iArray = new Array();
      
      try
      {
    //  alert("6666");
		iArray[0]=new Array();
		iArray[0][0]="序号";         //列名
		iArray[0][1]="30px";         //列名
		iArray[0][2]=100;         //列名
		iArray[0][3]=0;         //列名	
		
	//alert("7777");	
		
		iArray[1]=new Array();
		iArray[1][0]="员工编码";         //列名
		iArray[1][1]="100px";         //宽度
		iArray[1][2]=100;         //最大长度
		iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
	//alert("8888");
		
		iArray[2]=new Array();
		iArray[2][0]="员工组别";         //列名
		iArray[2][1]="100px";         //宽度
		iArray[2][2]=100;         //最大长度
		iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
	//alert("9999");
		
		iArray[3]=new Array();
		iArray[3][0]="姓名";         //列名
		iArray[3][1]="100px";         //宽度
		iArray[3][2]=100;         //最大长度
		iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
	//alert("0000");
		
		iArray[4]=new Array();
		iArray[4][0]="入司日期";         //列名
		iArray[4][1]="100px";         //宽度
		iArray[4][2]=150;         //最大长度
		iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[5]=new Array();
		iArray[5][0]="原员工制待遇级别";         //列名
		iArray[5][1]="100px";         //宽度
		iArray[5][2]=150;         //最大长度
		iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
	//alert("1010");
		
		iArray[6]=new Array();
		iArray[6][0]="建议员工制待遇级别";         //列名
		iArray[6][1]="100px";         //宽度
		iArray[6][2]=150;         //最大长度
		iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
	//alert("1212");
	
		iArray[7]=new Array();
		iArray[7][0]="新员工制待遇级别";         //列名
		iArray[7][1]="100px";         //宽度
		iArray[7][2]=150;         //最大长度
		iArray[7][3]=2;         //是否允许录入，0--不能，1--允许
		iArray[7][4]="employeegrade";
	//alert("1313");

		
        AgentAdjGrid = new MulLineEnter( "fm" , "AgentAdjGrid" ); 

        //这些属性必须在loadMulLine前
        AgentAdjGrid.canChk = 1;
        AgentAdjGrid.mulLineCount = 6;   
        AgentAdjGrid.displayTitle = 1;
        
        AgentAdjGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化AgentAdjGrid时出错："+ ex);
      }
    }

</script>