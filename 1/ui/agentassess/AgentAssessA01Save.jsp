<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：AgentAssessSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT src="AgentAssessInput.js"></SCRIPT>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>  

<%
// 输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  //tGI.ManageCom="8611";
  //tGI.Operator="Admin";
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆"; 
  }
  else //页面有效
  {
   String mOperate = request.getParameter("mOperate");	
   String ManageCom = request.getParameter("ManageCom");
   String AgentSeries = request.getParameter("AgentSeries");
   String WageYear  = request.getParameter("AssessYear");
   String WageMonth = request.getParameter("AssessMonth");
   String tBranchType = request.getParameter("BranchType");
	 String tBranchType2 = request.getParameter("BranchType2");
	 String tCalFlag = request.getParameter("CalFlag");	 
	 String tAgentGrade = request.getParameter("AgentGrade");	

   VData tVData = new VData();
   tVData.addElement(tGI);
   tVData.addElement(ManageCom);
   tVData.addElement(AgentSeries);
   tVData.addElement(WageYear);
   tVData.addElement(WageMonth);
   tVData.addElement(tBranchType);
   tVData.addElement(tBranchType2);
   tVData.addElement(tCalFlag);
   tVData.addElement(tAgentGrade);
   
   System.out.println("WageMonth" +WageMonth);
   
   System.out.println("begin to submit to CalAssessBL...");
   CalBeforeAssessBL tCalAssessBL = new CalBeforeAssessBL();
   tCalAssessBL.submitData(tVData,mOperate);
   //如果在Catch中发现异常，则不从错误类中提取错误信息
   try
   {
     tError = tCalAssessBL.mErrors;
     if (!tError.needDealError())
     {                          
       Content = " 保存成功! ";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content = " 失败，原因:" + tError.getFirstError();
       FlagStr = "Fail";
     }
   }
   catch(Exception ex)
   {
     Content = " 失败，原因:" + ex.toString();
     FlagStr = "Fail";    
   }    
   System.out.println(Content);                  
}//页面有效区

%>                      
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

