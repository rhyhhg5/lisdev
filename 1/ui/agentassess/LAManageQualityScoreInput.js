//               该文件中包含客户端需要处理的函数和事件
var inputGroupAgentcode = "";
var inputMonth = "";
var inputYear = "";
var mDebug="0";
var mOperate="";
var showInfo;
//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
//</addcode>############################################################//
window.onfocus=myonfocus;
var nowDate = new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDay();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
    inputGroupAgentcode.disabled=false;
    inputMonth.disabled=false;
    inputYear.disabled=false;
	mOperate = "INSERT";
	if(!verifyInput()) return false;
	if(!checkWage()) return false;
	if(!checkHave()) return false;
	if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  fm.submit(); //提交
  initForm();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	 mOperate="";
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
  else
  { 
		//fm.all('ModifyDate').value=fm.all('DoneDate').value;
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    //showDiv(operateButton,"true"); 
    initForm();
  }
  catch(re)
  {
    alert("在LARewardPunish.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if(!verifyInput()) return false;

  //下面增加相应的代码  
	if((LAAgentCompreScoreGrid.getRowColData(1,3)==null)||(LAAgentCompreScoreGrid.getRowColData(1,3)==''))
	{
		alert('请先查询出要修改的纪录！');
		return false;
	}
	if((fm.all("Year").value+fm.all("Month1").value)!=fm.all('wageno1').value){
		alert('修改时不能修改执行年月，只能修改考评项目的分数!');
		return false;
	}
	if (!beforeSubmit())
	    return false;
  else if (confirm("您确实想修改该记录吗?"))
  {
	    inputGroupAgentcode.disabled=false;
	    inputMonth.disabled=false;
	    inputYear.disabled=false;
	  var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
    mOperate="UPDATE";
    fm.hideOperate.value=mOperate;
    fm.submit(); //提交
    initForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LAManageQualityScoreQueryInput.html?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
  
}   

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	inputGroupAgentcode.disabled=false;
    inputMonth.disabled=false;
    inputYear.disabled=false;
  //下面增加相应的删除代码  
  if((LAAgentCompreScoreGrid.getRowColData(1,3)==null)||(LAAgentCompreScoreGrid.getRowColData(1,3)==''))
    alert('请先查询出要删除的业务员记录！');
  else if (confirm("您确实想删除该记录吗?"))
  {
	  var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
    mOperate="DELETE";  
    fm.hideOperate.value=mOperate;
    fm.submit(); //提交
    initForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
  if( arrQueryResult != null )
  {
	  initForm();
  //	alert(1);
    arrResult = arrQueryResult;
    fm.all('GroupAgentcode').value = arrResult[0][0];
    fm.all('Name').value = arrResult[0][1];  
    fm.all('ManageCom').value = arrResult[0][2];
    fm.all('ManageComName').value = arrResult[0][3]; 
    fm.all('AgentGroup').value = arrResult[0][4];  
    fm.all('AgentGroupName').value = arrResult[0][5];                                              
    fm.all('AgentCode').value = arrResult[0][6];
    fm.all('wageno1').value=arrResult[0][7];
    var ReturnWageno=arrResult[0][7];
    var addSql="";
    if(ReturnWageno!=null&&ReturnWageno!=""){
    	addSql=" and wageno ='"+ReturnWageno+"' "
    }else{
    	addSql=" and wageno is null "
    }
    var sql ="select projectCode,propertyscore,operator,makedate,wageno from LAAgentCompreScore where branchtype='5' and branchtype2='01' and projectCode like 'AB%' " +
    		" and agentcode ='"+fm.all('AgentCode').value+"'"+addSql+" order by projectcode  with ur" ;
    var returnResult = easyQueryVer3(sql, 1, 1, 1);
    if(!returnResult)
    {
    	fm.all('Operator').value=fm.all('hiddenOperator').value;
		fm.all('MakeDate').value=nowDate;
		initLAAgentCompreScoreGrid();
		initLAAgentCompreScoreGridValue();
		initLAManageQualityScoreGridValue();

    }
    else
    {
    	turnPage.strQueryResult  = easyQueryVer3(sql);
    	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    	    //查询成功则拆分字符串，返回二维数组
	    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	    turnPage.pageDisplayGrid = LAAgentCompreScoreGrid;
	    //保存SQL语句
	    turnPage.strQuerySql = sql;
	    //设置查询起始位置
	    turnPage.pageIndex = 0;
	    //在查询结果数组中取出符合页面显示大小设置的数组
	    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 52);
	    //调用MULTILINE对象显示查询结果
	    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

	    fm.all('Score').value=100-LAAgentCompreScoreGrid.getRowColData(51,2);
	    fm.all('Operator').value=LAAgentCompreScoreGrid.getRowColData(51,3);
	    fm.all('MakeDate').value=LAAgentCompreScoreGrid.getRowColData(51,4);
	    var wageno = LAAgentCompreScoreGrid.getRowColData(51,5);
	    var month = wageno.substr(4);
	    var year  = wageno.substr(0,4);
	    fm.all('Month1').value=month;
    	fm.all('MonthName').value=month+"月份";
    	fm.all('Year').value=year;
    }
    inputGroupAgentcode=document.getElementById("GroupAgentcode");
    inputMonth=document.getElementById("Month1");
    inputYear=document.getElementById("Year");
    inputGroupAgentcode.disabled=true;
    inputMonth.disabled=true;
    inputYear.disabled=true;
  }
  
}
function initLAAgentCompreScoreGridValue(){
	LAAgentCompreScoreGrid.setRowColData(0,1,"AB0001");
	LAAgentCompreScoreGrid.setRowColData(1,1,"AB0002");
	LAAgentCompreScoreGrid.setRowColData(2,1,"AB0003");
	LAAgentCompreScoreGrid.setRowColData(3,1,"AB0004");
	LAAgentCompreScoreGrid.setRowColData(4,1,"AB0005");
	LAAgentCompreScoreGrid.setRowColData(5,1,"AB0006");
	LAAgentCompreScoreGrid.setRowColData(6,1,"AB0007");
	LAAgentCompreScoreGrid.setRowColData(7,1,"AB0008");
	LAAgentCompreScoreGrid.setRowColData(8,1,"AB0009");
	LAAgentCompreScoreGrid.setRowColData(9,1,"AB0010");
	LAAgentCompreScoreGrid.setRowColData(10,1,"AB0011");
	LAAgentCompreScoreGrid.setRowColData(11,1,"AB0012");
	LAAgentCompreScoreGrid.setRowColData(12,1,"AB0013");
	LAAgentCompreScoreGrid.setRowColData(13,1,"AB0014");
	LAAgentCompreScoreGrid.setRowColData(14,1,"AB0015");
	LAAgentCompreScoreGrid.setRowColData(15,1,"AB0016");
	LAAgentCompreScoreGrid.setRowColData(16,1,"AB0017");
	LAAgentCompreScoreGrid.setRowColData(17,1,"AB0018");
	LAAgentCompreScoreGrid.setRowColData(18,1,"AB0019");
	LAAgentCompreScoreGrid.setRowColData(19,1,"AB0020");
	LAAgentCompreScoreGrid.setRowColData(20,1,"AB0021");
	LAAgentCompreScoreGrid.setRowColData(21,1,"AB0022");
	LAAgentCompreScoreGrid.setRowColData(22,1,"AB0023");
	LAAgentCompreScoreGrid.setRowColData(23,1,"AB0024");
	LAAgentCompreScoreGrid.setRowColData(24,1,"AB0025");
	LAAgentCompreScoreGrid.setRowColData(25,1,"AB0026");
	LAAgentCompreScoreGrid.setRowColData(26,1,"AB0027");
	LAAgentCompreScoreGrid.setRowColData(27,1,"AB0028");
	LAAgentCompreScoreGrid.setRowColData(28,1,"AB0029");
	LAAgentCompreScoreGrid.setRowColData(29,1,"AB0030");
	LAAgentCompreScoreGrid.setRowColData(30,1,"AB0031");
	LAAgentCompreScoreGrid.setRowColData(31,1,"AB0032");
	LAAgentCompreScoreGrid.setRowColData(32,1,"AB0033");
	LAAgentCompreScoreGrid.setRowColData(33,1,"AB0034");
	LAAgentCompreScoreGrid.setRowColData(34,1,"AB0035");
	LAAgentCompreScoreGrid.setRowColData(35,1,"AB0036");
	LAAgentCompreScoreGrid.setRowColData(36,1,"AB0037");
	LAAgentCompreScoreGrid.setRowColData(37,1,"AB0038");
	LAAgentCompreScoreGrid.setRowColData(38,1,"AB0039");
	LAAgentCompreScoreGrid.setRowColData(39,1,"AB0040");
	LAAgentCompreScoreGrid.setRowColData(40,1,"AB0041");
	LAAgentCompreScoreGrid.setRowColData(41,1,"AB0042");
	LAAgentCompreScoreGrid.setRowColData(42,1,"AB0043");
	LAAgentCompreScoreGrid.setRowColData(43,1,"AB0044");
	LAAgentCompreScoreGrid.setRowColData(44,1,"AB0045");
	LAAgentCompreScoreGrid.setRowColData(45,1,"AB0046");
	LAAgentCompreScoreGrid.setRowColData(46,1,"AB0047");
	LAAgentCompreScoreGrid.setRowColData(47,1,"AB0048");
	LAAgentCompreScoreGrid.setRowColData(48,1,"AB0049");
	LAAgentCompreScoreGrid.setRowColData(49,1,"AB0050");
	LAAgentCompreScoreGrid.setRowColData(50,1,"AB0051");
	LAAgentCompreScoreGrid.setRowColData(51,1,"AB0052");

}
function initLAManageQualityScoreGridValue()
{
	LAManageQualityScoreGrid.setRowColData(0,1,"AB0001");
	LAManageQualityScoreGrid.setRowColData(0,2,"在展业、理赔过程中进行夸大、虚假宣传，对客户或公司利益造成不良影响的");

	LAManageQualityScoreGrid.setRowColData(1,1,"AB0002");
	LAManageQualityScoreGrid.setRowColData(1,2,"以不正当手段诱使或强迫客户投保、退保，对客户或公司利益造成不良损失的");

	LAManageQualityScoreGrid.setRowColData(2,1,"AB0003");
	LAManageQualityScoreGrid.setRowColData(2,2,"对客户隐瞒与保险合同有关的重要情况，致使客户对公司条款、核保过程、理赔过程、保全必备文件等产生歧义，经解释仍然无法消除客户不满情绪或对客户或公司利益造成损失的");

	LAManageQualityScoreGrid.setRowColData(3,1,"AB0004");
	LAManageQualityScoreGrid.setRowColData(3,2,"在必须由客户亲笔抄写风险语录时，出现代客户为抄录的行为（每件）");

	LAManageQualityScoreGrid.setRowColData(4,1,"AB0005");
	LAManageQualityScoreGrid.setRowColData(4,2,"在必须由客户（包括投保人、被保险人、监护人、受益人等）亲笔签名时出现他人代签名行为");

	LAManageQualityScoreGrid.setRowColData(5,1,"AB0006");
	LAManageQualityScoreGrid.setRowColData(5,2,"在必须由客户（包括投保人、被保险人、监护人、受益人等）亲笔签名时代为签署，或已知存在代签名情况不阻止、不告知公司，由此引起退保或理赔纠纷、投诉等严重后果的");

	LAManageQualityScoreGrid.setRowColData(6,1,"AB0007");
	LAManageQualityScoreGrid.setRowColData(6,2,"虚假客户投保，或虚挂业务员投保的");

	LAManageQualityScoreGrid.setRowColData(7,1,"AB0008");
	LAManageQualityScoreGrid.setRowColData(7,2,"没有按投保要求提醒客户做如实健康告知、财务告知等，致使公司或客户利益受损的");

	LAManageQualityScoreGrid.setRowColData(8,1,"AB0009");
	LAManageQualityScoreGrid.setRowColData(8,2,"在知情的情况下，故意提供客户虚假投保信息，如假证件，假电话、假地址等，导致无法回访或错访的");

	LAManageQualityScoreGrid.setRowColData(9,1,"AB0010");
	LAManageQualityScoreGrid.setRowColData(9,2,"替代客户体检或协同客户让他人替代体检、伪造或修改体检报告，或明知上述情况存在而不阻止、不告知公司，或隐瞒客户擅自找他人替代体检的");

	LAManageQualityScoreGrid.setRowColData(10,1,"AB0011");
	LAManageQualityScoreGrid.setRowColData(10,2,"在团体保险销售过程中，拼凑团单的");
	
	LAManageQualityScoreGrid.setRowColData(11,1,"AB0012");
	LAManageQualityScoreGrid.setRowColData(11,2,"协助客户隐瞒真相或明知客户告知不实却不如实告知公司，或不如实填写投保、保全、理赔等公司规定须如实填写文件的;诱导客户提供不真实信息，伪造、篡改客户信息的");
	LAManageQualityScoreGrid.setRowColData(12,1,"AB0013");
	LAManageQualityScoreGrid.setRowColData(12,2,"私自印制保险宣传资料、散发非公司印制的宣传资料或使用未经授权使用的产品说明工具或软件");
	LAManageQualityScoreGrid.setRowColData(13,1,"AB0014");
	LAManageQualityScoreGrid.setRowColData(13,2,"未经公司批准擅自通过开设网站、网页、博客等形式在互联网上销售、宣传公司产品、发布增员招聘信息；或者经公司检查部分内容需要修改、删除，经两次提醒不予修改或删除");
	LAManageQualityScoreGrid.setRowColData(14,1,"AB0015");
	LAManageQualityScoreGrid.setRowColData(14,2,"在销售过程中，未向客户展示及提供产品条款、产品说明书、人身保险投保提示书等需要提供的保险资料的，每次扣10分"+
"（仅未向客户讲解保险条款、免除责任、犹豫期、分红不确定性等关系客户利益的重要信息的，视情况严重程度酌情扣分）");
	LAManageQualityScoreGrid.setRowColData(15,1,"AB0016");
	LAManageQualityScoreGrid.setRowColData(15,2,"随机拨打电话约访陌生客户（个人客户），或假借公司名义电话约访客户");
	LAManageQualityScoreGrid.setRowColData(16,1,"AB0017");
	LAManageQualityScoreGrid.setRowColData(16,2,"不以公司利益为前提，恶意抢单的");
	LAManageQualityScoreGrid.setRowColData(17,1,"AB0018");
	LAManageQualityScoreGrid.setRowColData(17,2,"为追逐更高利益，违背职业道德，买卖保单的，当事双方均予以扣分");
	LAManageQualityScoreGrid.setRowColData(18,1,"AB0019");
	LAManageQualityScoreGrid.setRowColData(18,2,"擅自修改承保条件（如特别约定、加费等），骗取客户投保而引起纠纷的");
	LAManageQualityScoreGrid.setRowColData(19,1,"AB0020");
	LAManageQualityScoreGrid.setRowColData(19,2,"业务人员给予或者承诺给予投保人、被保险人或者受益人保险合同规定以外的其他利益的，造成客户投诉或造成公司损失的");
	LAManageQualityScoreGrid.setRowColData(20,1,"AB0021");
	LAManageQualityScoreGrid.setRowColData(20,2,"私下以现金或其他方式支付代理机构及其经办人保险费回扣或违法、违规的其他利益，造成公司损失的");
	LAManageQualityScoreGrid.setRowColData(21,1,"AB0022");
	LAManageQualityScoreGrid.setRowColData(21,2,"与非法从事保险业务、保险中介业务的机构或者个人发生保险业务往来的");
	LAManageQualityScoreGrid.setRowColData(22,1,"AB0023");
	LAManageQualityScoreGrid.setRowColData(22,2,"直销业务假借中介名义套取中介手续费");
	LAManageQualityScoreGrid.setRowColData(23,1,"AB0024");
	LAManageQualityScoreGrid.setRowColData(23,2,"以欺骗、不实说明等手段挪用、骗取、截留保费或保险金，或无正当理由超过三个工作日未将收取的保费交至公司者");
	LAManageQualityScoreGrid.setRowColData(24,1,"AB0025");
	LAManageQualityScoreGrid.setRowColData(24,2,"与公司以外的其他保险机构签订委托协议的,或为其他保险机构经办和销售保单者");
	LAManageQualityScoreGrid.setRowColData(25,1,"AB0026");
	LAManageQualityScoreGrid.setRowColData(25,2,"投保单信息填写有明显错误的，例如客户联系方式错误、尤其是电话号码错误（每件）；银行账号填写错误（每件）");
	LAManageQualityScoreGrid.setRowColData(26,1,"AB0027");
	LAManageQualityScoreGrid.setRowColData(26,2,"明知客户信息资料发生变更而不及时为客户办理变更手续，导致公司无法联系客户、不能进行正常的保单回访和按时续期收费");
	LAManageQualityScoreGrid.setRowColData(27,1,"AB0028");
	LAManageQualityScoreGrid.setRowColData(27,2,"对客户服务态度差或服务质量不好，未为客户提供续期服务，或违反公司新契约、保全、续期、理赔、回访、附加值服务等业务管理规定，对公司运作造成影响的");
	LAManageQualityScoreGrid.setRowColData(28,1,"AB0029");
	LAManageQualityScoreGrid.setRowColData(28,2,"未经公司批准在营销服务部以外擅自设立营业场所");
	LAManageQualityScoreGrid.setRowColData(29,1,"AB0030");
	LAManageQualityScoreGrid.setRowColData(29,2,"业务员在投诉处理过程中有下列行为之一的，每次："
+"1. 不配合公司做客户协调工作；"
+"2. 未在限定时间内送达书面材料；"
+"3. 未在限定时间内来公司核实面谈；"
+"4. 未在限定时间内执行公司投诉处理决定；"
+"5. 在递交公司的书面材料中存在虚假内容或在核实过程中作伪证；"
+"6. 在投诉处理过程中谩骂、威胁经办人员或客户。");
	LAManageQualityScoreGrid.setRowColData(30,1,"AB0031");
	LAManageQualityScoreGrid.setRowColData(30,2,"由于业务员个人原因，导致客户交办的业务超过犹豫期或宽限期的，给客户或公司造成损失的");
	LAManageQualityScoreGrid.setRowColData(31,1,"AB0032");
	LAManageQualityScoreGrid.setRowColData(31,2,"违背客户真实意愿或超越客户授权范围办理业务，或更改、伪造客户申请资料，造成纠纷的");
	LAManageQualityScoreGrid.setRowColData(32,1,"AB0033");
	LAManageQualityScoreGrid.setRowColData(32,2,"违反公司新契约、保全、续期、理赔、电话回访问题件处理规定等业务管理规定的期限要求的");
	LAManageQualityScoreGrid.setRowColData(33,1,"AB0034");
	LAManageQualityScoreGrid.setRowColData(33,2,"无故不接受公司业务检查，经解释拒不执行的");
	LAManageQualityScoreGrid.setRowColData(34,1,"AB0035");
	LAManageQualityScoreGrid.setRowColData(34,2,"由于业务人员责任，导致公司在投诉、诉讼、仲裁或媒体报道中蒙受经济或信誉损失的");
	LAManageQualityScoreGrid.setRowColData(35,1,"AB0036");
	LAManageQualityScoreGrid.setRowColData(35,2,"泄露分公司商业秘密，将客户信息及业务数据和资料泄露、私自转让给他人、挪作他用者，或故意行为导致批量客户信息外流，造成客户投诉或公司损失的；违反限制范围接触、使用客户信息，以及泄露和倒卖客户信息的");
	LAManageQualityScoreGrid.setRowColData(36,1,"AB0037");
	LAManageQualityScoreGrid.setRowColData(36,2,"伪造、转让《保险销售从业人员资格证》或《保险销售从业人员执业证》的");
	LAManageQualityScoreGrid.setRowColData(37,1,"AB0038");
	LAManageQualityScoreGrid.setRowColData(37,2,"私自刻制、伪造、改造公司各种印章、证照的");
	LAManageQualityScoreGrid.setRowColData(38,1,"AB0039");
	LAManageQualityScoreGrid.setRowColData(38,2,"参与保险欺诈：伪造、擅自变更保险合同，或者为保险合同当事人提供虚假证明材料，或擅自以公司的名义签发保险单，或擅自变更保险条款，或擅自与客户签订附加合约，或保险事故发生后再与客户签订投保申请（倒签单），或串通投保人、被保险人或受益人参与制造假赔案，欺骗公司的");
	LAManageQualityScoreGrid.setRowColData(39,1,"AB0040");
	LAManageQualityScoreGrid.setRowColData(39,2,"业务员超期未回销有价单证的");
	LAManageQualityScoreGrid.setRowColData(40,1,"AB0041");
	LAManageQualityScoreGrid.setRowColData(40,2,"私自印制、伪造、变造、倒买倒卖、隐匿、销毁有价单证、保险单证的");
	LAManageQualityScoreGrid.setRowColData(41,1,"AB0042");
	LAManageQualityScoreGrid.setRowColData(41,2,"业务员遗失有价单证，未在规定时限内办理登报遗失声明和上缴罚款的");
	LAManageQualityScoreGrid.setRowColData(42,1,"AB0043");
	LAManageQualityScoreGrid.setRowColData(42,2,"丢失客户原始单证、投保单据及保单正本的。");
	LAManageQualityScoreGrid.setRowColData(43,1,"AB0044");
	LAManageQualityScoreGrid.setRowColData(43,2,"未按照反洗钱法律法规的要求进行客户身份识别工作的");
	LAManageQualityScoreGrid.setRowColData(44,1,"AB0045");
	LAManageQualityScoreGrid.setRowColData(44,2,"以夸大或虚构从业前景、从业待遇、行业优势，不当许诺给予高职位、高底薪、高津贴等条件，进行增员活动的");
	LAManageQualityScoreGrid.setRowColData(45,1,"AB0046");
	LAManageQualityScoreGrid.setRowColData(45,2,"业务人员个人擅自发布招聘信息");
	LAManageQualityScoreGrid.setRowColData(46,1,"AB0047");
	LAManageQualityScoreGrid.setRowColData(46,2,"在求职表、面谈表、解除代理合同申请表等表格中代替他人签字");
	LAManageQualityScoreGrid.setRowColData(47,1,"AB0048");
	LAManageQualityScoreGrid.setRowColData(47,2,"提供或协助他人假学历、假证明、隐瞒或伪造个人简历者");
	LAManageQualityScoreGrid.setRowColData(48,1,"AB0049");
	LAManageQualityScoreGrid.setRowColData(48,2,"不服从管理，在职场内争吵斗殴、侮辱他人或其它类似不当行为，影响正常工作秩序的");
	LAManageQualityScoreGrid.setRowColData(49,1,"AB0050");
	LAManageQualityScoreGrid.setRowColData(49,2,"亲自或怂恿客户到公司无理取闹，或怂恿客户投诉，态度恶劣，干扰正常工作秩序的");
	LAManageQualityScoreGrid.setRowColData(50,1,"AB0051");
	LAManageQualityScoreGrid.setRowColData(50,2,"接到客户报案通知未及时告知公司理赔部门，导致保险事故性质、原因和损失程度难以确定，并给公司造成损失的,视情况严重程度");
	LAManageQualityScoreGrid.setRowColData(51,1,"AB0052");
	LAManageQualityScoreGrid.setRowColData(51,2,"合计");
		
}
function beforeSubmit()
{
	var rowNum = LAAgentCompreScoreGrid.mulLineCount;
	var propertyscoreSum=0;
	var lifescoreSum=0;
	var r = /^\+?[0-9][0-9]*$/;　　//整数 
	for(var i=0;i<rowNum-1;i++)
	{
		
		if(LAAgentCompreScoreGrid.getRowColData(i,2) == null||LAAgentCompreScoreGrid.getRowColData(i,2) == "")
		{
			LAAgentCompreScoreGrid.setRowColData(i,2,"0");
		}
		
		var num=LAAgentCompreScoreGrid.getRowColData(i,2);
		if(num<0)
		{
			alert(LAAgentCompreScoreGrid.getRowColData(i,1)+"分数不能为负数！");
			return false;
		}
		if(!r.test(num))
		{
			alert(LAAgentCompreScoreGrid.getRowColData(i,1)+"分数只能录入整数！");
			return false;
		}
		propertyscoreSum+=parseInt(num);

	}
	LAAgentCompreScoreGrid.setRowColData(51,2,parseFloat(propertyscoreSum)+"");
	fm.all("Score").value=100-propertyscoreSum;
	return true;
	
}
function queryStandClick()
{
	document.getElementById("divLAActiveManageStand").style.display="";
}
function checkWage()
{
	var wageno = fm.all("Year").value+fm.all("Month1").value;
	var sql ="select 1 from laassess where branchtype ='5' and branchtype2='01' and state='1' " +
	"  and agentcode ='"+fm.all("AgentCode").value+"'" +
	"  and indexcalno ='"+wageno+"' with ur";
	turnPage1.strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
	if (turnPage1.strQueryResult) {
		alert("业务员"+fm.all("GroupAgentCode").value+"在"+fm.all("Year").value+"的"+fm.all("Month1").value+"月份考核已经确认");
		return false;
	}
	return true;
}
//校验要修改的记录是否已存在
function checkHave(){
	  var NowWageno=fm.all("Year").value+fm.all("Month1").value;
	  var sql ="select 1 from LAAgentCompreScore where branchtype='5' and branchtype2='01' and projectCode like 'AB%' " +
			" and agentcode ='"+fm.all('AgentCode').value+"' and wageno='"+NowWageno+"' order by projectcode  with ur" ;
	    var returnResult = easyQueryVer3(sql, 1, 1, 1);
	    if(returnResult)
	{
		alert("此条记录已经存在，请修改！");
		return false;
	}
	    return true;
}
//光标移开,根据框里所输入的代码查询业务员信息
function checkCode(){
	  var strSQL1 = "";
	  if (getWherePart('GroupAgentcode')!='')
	  {
	     strSQL1 = "select a.GroupAgentcode,a.name,a.managecom,(select name from ldcom where comcode =a.ManageCom),a.agentgroup," 
	     	 +	"(select name  from LABranchGroup  where a.agentgroup = agentgroup and (state<>'1' or state is null)),a.agentcode from LAAgent a where 1=1 "
	         +" and a.agentstate in('01','02') and a.BranchType='5' and a.BranchType2='01' "
	     	 +" and a.managecom like '86%' "
	     	 + getWherePart('GroupAgentcode','GroupAgentcode');
	     
	     var strQueryResult = easyQueryVer3(strSQL1, 1, 1, 1);
	  }
	  else
	  {
		fm.all('GroupAgentcode').value = "";
	    fm.all('Name').value = "";
	    fm.all('AgentGroupName').value  = "";
	    fm.all('ManageComName').value  = "";
	    return false;
	  }
	  //判断是否查询成功
	  if (!strQueryResult) {
	    alert("此渠道下无此代理人或此代理人已经离职！");
	    fm.all('GroupAgentcode').value = '';
	    fm.all('Name').value = "";
	    fm.all('AgentGroupName').value  = "";
	    fm.all('ManageComName').value  = "";
	    return false;
	  }
	  //查询成功则拆分字符串，返回二维数组
	  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
	  var tArr = new Array();
	  tArr = decodeEasyQueryResult(strQueryResult);
	  fm.all('AgentCode').value = tArr[0][6];
	  //<rem>######//
	  fm.all('Name').value = tArr[0][1];
	  //</rem>######//
	  
	  fm.all('ManageCom').value  = tArr[0][2];
	//添加销售人员类型 用于区分代理制员工制 加扣款名称不同（2016银代基本法） 2016-3-1
	  fm.all('ManageComName').value = tArr[0][3];
	  fm.all('AgentGroup').value = tArr[0][4];
	  fm.all('AgentGroupName').value=tArr[0][5];
}