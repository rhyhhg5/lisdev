<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentEvaluateSave.jsp
//程序功能：
//创建日期：2005-8-25 14:38
//创建人  ：LiuH
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
	String tManageCom = request.getParameter("ManageCom");
	String tIndexCalNo = request.getParameter("IndexCalNo");
	String tBranchType = request.getParameter("BranchType");
	String tBranchType2 = request.getParameter("BranchType2");
	 
	String tChk[] = request.getParameterValues("InpEvaluateGridChk");
	String tAgentCode[] = request.getParameterValues("EvaluateGrid1");
	String tBranchAttr[] = request.getParameterValues("EvaluateGrid3");
	
////////String tAgentGrade[] = request.getParameterValues("EvaluateGrid5");
////////String tAgentGrade1[] = request.getParameterValues("EvaluateGrid6");
////////String CalAgentGrade[] = request.getParameterValues("EvaluateGrid7");
	int tDateCount = 0;

  //输入参数
  LAAssessSet mLAAssessSet = new LAAssessSet();
  LAAssessSchema mLAAssessSchema = new LAAssessSchema();
  LAAssessHistorySchema mLAAssessHistorySchema = new LAAssessHistorySchema();
  LAAgentEvaluateUI mLAAgentEvaluateUI = new LAAgentEvaluateUI();

  //输出参数
  CErrors tError = null;
  String tOperate="";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	// 准备数据
	mLAAssessHistorySchema.setManageCom(tManageCom);
  mLAAssessHistorySchema.setIndexCalNo(tIndexCalNo);
  mLAAssessHistorySchema.setBranchType(tBranchType);
  mLAAssessHistorySchema.setBranchType2(tBranchType2);
  // 准备参考序列 
  String tGroupSeries = "D" + "99";
  mLAAssessHistorySchema.setAgentGrade(tGroupSeries);
System.out.println("开始准备后台处理");
System.out.println(tGroupSeries);
  // 准备个人考核信息
tDateCount = tBranchAttr.length;
System.out.println("本次处理人数:["+tDateCount+"]");
for(int i=0;i<tDateCount;i++)
{
System.out.println("本次处理人数ww:"+i);
System.out.println("本次处理人数ww:"+tChk[i]);
     if(tChk[i].equals("1"))
     {
	mLAAssessSchema = new LAAssessSchema();
	mLAAssessSchema.setAgentCode(tAgentCode[i]);
	mLAAssessSchema.setBranchAttr(tBranchAttr[i]);
	mLAAssessSchema.setAgentGrade("D01");
	mLAAssessSchema.setAgentGrade1("D00");
	mLAAssessSchema.setCalAgentGrade("D01");
	
	mLAAssessSet.add(mLAAssessSchema);
      }
}
 

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(mLAAssessHistorySchema);
  tVData.addElement(mLAAssessSet);
  System.out.println("add over");
  try
  {
    mLAAgentEvaluateUI.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLAAgentEvaluateUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

