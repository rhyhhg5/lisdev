<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAssessSave.jsp
//程序功能：
//创建日期：2003-07-15 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAssessAccessorySchema mLAAssessAccessorySchema = new LAAssessAccessorySchema();
  LAAssessAccessorySet mLAAssessAccessorySet = new LAAssessAccessorySet();
  LAAssessInputUI mLAAssessInputUI  = new LAAssessInputUI();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT||MAIN";
  String tRela  = "";
  String FlagStr = "";
  String Content = "";

  GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin prepare LAAssessSet...");
  String tManageCom = request.getParameter("ManageCom");
  String tBranchType = request.getParameter("BranchType");
  String tBranchType2 = request.getParameter("BranchType2");
  String tIndexCalNo = request.getParameter("IndexCalNo");
  String tAgentGrade = request.getParameter("AgentGrade");  
  
  //取得Muline信息
  int lineCount = 0;
  String tAgentCode[] = request.getParameterValues("LAAssessGrid1");
  String tBranchAttr[] = request.getParameterValues("LAAssessGrid2");
  String tAgentGrade1[] = request.getParameterValues("LAAssessGrid3");
  String tAgentGroup[] = request.getParameterValues("LAAssessGrid4");
  String tAgentSeries[] = request.getParameterValues("LAAssessGrid5");
  lineCount = tAgentCode.length; //行数
  System.out.println("length= "+String.valueOf(lineCount));
  System.out.println("length= "+tAgentCode[0]);
  System.out.println("length= "+tBranchAttr[0]);
  System.out.println("length= "+tAgentGrade1[0]);
  System.out.println("length= "+tAgentSeries[0]);
  
  for(int i=0;i<lineCount;i++)
  {
     mLAAssessAccessorySchema = new LAAssessAccessorySchema();
     String agentcode ="";
     if(!"".equals(tAgentCode[i]))
     {
	     String sql ="select getAgentcode('"+tAgentCode[i]+"') from dual";
     	 agentcode = new ExeSQL().getOneValue(sql);
     }
     mLAAssessAccessorySchema.setAgentCode(agentcode);  
     mLAAssessAccessorySchema.setBranchAttr(tBranchAttr[i]); 
     mLAAssessAccessorySchema.setAgentGrade1(tAgentGrade1[i]);
     System.out.println("aaaa"+tAgentGrade1[i]);
     mLAAssessAccessorySchema.setAgentGroup(tAgentGroup[i]);
     mLAAssessAccessorySchema.setAgentSeries(tAgentSeries[i]);
     mLAAssessAccessorySchema.setAssessType("00"); //默认为职级考核维度
    //将外部编码转换为内部编码
    String AgentGroup = "";
    String branchSQL = "select agentgroup from labranchgroup where branchattr='"+tBranchAttr[i]+"' and branchtype='1' and branchtype2='01'";
     ExeSQL tExeSQL = new ExeSQL();
     AgentGroup = tExeSQL.getOneValue(branchSQL);
     System.out.println("AgentGroup"+AgentGroup);
     mLAAssessAccessorySchema.setAgentGroupNew(AgentGroup);

     mLAAssessAccessorySchema.setManageCom(tManageCom);
     mLAAssessAccessorySchema.setBranchType(tBranchType);
     mLAAssessAccessorySchema.setBranchType2(tBranchType2);
     mLAAssessAccessorySchema.setIndexCalNo(tIndexCalNo); 
     mLAAssessAccessorySchema.setAgentGrade(tAgentGrade);
	 //区分值：0 手动考核
	 mLAAssessAccessorySchema.setStandAssessFlag("0");

     System.out.println("i= "+String.valueOf(i)+" | "+ tAgentSeries[i]); 
     mLAAssessAccessorySet.add(mLAAssessAccessorySchema);    
  }
  System.out.println("end 考评信息...");
System.out.println("end size:"+mLAAssessAccessorySet.size());
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.addElement(mLAAssessAccessorySchema);
  tVData.addElement(tBranchType);
  tVData.addElement(tBranchType2);
  try
  {
    mLAAssessInputUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLAAssessInputUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>