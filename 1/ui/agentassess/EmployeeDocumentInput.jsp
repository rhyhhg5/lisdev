<html> 
<%
//程序名称：
//程序功能：
//创建日期：2004-2-17 11:48:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>  
  <SCRIPT src="EmployeeDocumentInput.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>  
  <%@include file="EmployeeDocumentInit.jsp"%>
  

</head>


<BODY onload = "initForm();">
<FORM action=EmployeeDocumentSave1.jsp method=post name=fm target="fraSubmit">     
    <TABLE class=common>
     <TR class=common>
      <TD  class= title> 管理机构 </TD>
      <TD  class= input>
        <Input class="code" name=ManageCom verify="管理机构|notnull&code:comcode"  
                       ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" 
                       onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
      </TD> 
      <TD class=titles> 代理人编码 </TD>
      <TD class=input> 
        <Input class=common type=text name=AgentCode onclick="">
      </TD>
     </TR>
     
     <TR>
      <TD class=titles> 代理人级别 </TD>      
      <TD class=input> 
        <Input name=AgentGrade class="code"          
         ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'1 and codealias = #1#','1');" 
         onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'1 and codealias = #1#','1');" >       
      </TD>               
      <TD class=titles> 档案调入标记 </TD>
      <TD class=input> 
        <Input class=Code type=text name=QualiPassFlag ondblclick="return showCodeList('yesno', [this]);" onkeyup="return showCodeList('yesno', [this]);">
      </TD>               
     </TR>
     
     <TR class=common> 
      <TD class=input width="26%" colspan=4 align=center><input type=button value="查    询" class=common onclick="easyQueryClick();">
      </TD>
     </TR>     
    </TABLE>
    

    <table> 
     <tr>
       	<td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentInfoGrid);">
    	</td>
    	<td class= titleImg>
    		 代理人信息
    	</td>
     </tr>
    </table>
  	<Div  id= "divAgentInfoGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanAgentInfoGrid">
  					</span> 
  			</td>
  		</tr>
    	</table>    	
    	 <INPUT VALUE=" 首页 "  TYPE="button" onclick="turnPage.firstPage();">
         <INPUT VALUE="上一页"  TYPE="button" onclick="turnPage.previousPage();">
         <INPUT VALUE="下一页"  TYPE="button" onclick="turnPage.nextPage();">
         <INPUT VALUE=" 尾页 "  TYPE="button" onclick="turnPage.lastPage();">			
  	</div>
  	<table class=common>     
     <tr class=common>
      <!--td class=input width="26%" colspan=2 align=center><input type=button value="全部调入" class=common onclick="submitForm();"></td-->
      <td class=input width="26%" colspan=2 align=center><input type=button value="部分调入" class=common onclick="submitPart();"></td>
     </tr>
     <tr class=common>
      <!--td class=input width="26%" colspan=2 align=center><input type=button value="全部调出" class=common onclick="submitOutForm();"></td-->
      <td class=input width="26%" colspan=2 align=center><input type=button value="部分调出" class=common onclick="submitOutPart();"></td>
     </tr>
     <tr class=common>
       <td class=input width="26%" colspan=2 align=center><input type=button value="福利待遇调整" class=common onclick="adjustForm();"></td>
     </tr>
     
    </table>
  	<input type="hidden" name="SubmitType">
  	<Input type=hidden name=mOperate value="">
</form>
 <!-- 作为代码选择的span -->
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>   