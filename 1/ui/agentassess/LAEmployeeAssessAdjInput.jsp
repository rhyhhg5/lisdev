<html> 
<%
//程序名称：
//程序功能：
//创建日期：2004-2-16 11:48:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>  
  <SCRIPT src="LAEmployeeAssessAdjInput.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>  
  <%@include file="./LAEmployeeAssessAdjInit.jsp"%>

</head>

<BODY onload = "initForm();">
<FORM action="./LAEmployeeAssessAdjSave.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
     <tr class=common>
      <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom verify="管理机构|notnull&code:comcode" 
            ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);">
          </TD>         
      <td class=titles>员工编码</td>
      <td class=input> <Input class=common type=text name=AgentCode ></td>
     </tr>
     <tr>
      <tr class=common>     
        <TD class= title>
          代理人职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="code" verify="代理人职级|notnull&code:AgentGrade"
                                 ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'1 and codealias = #1#','1');" 
                                 onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'1 and codealias = #1#','1');" >
        </TD>
        <TD  class= title>
            查询人类别
          </TD>
          <TD  class= input>
            <Input class='code' name=QueryType verify="|code:querytype&NOTNULL" ondblclick="return showCodeList('employeequerytype',[this]);" 
                                                         onkeyup="return showCodeListKey('employeequerytype',[this]);">
          </TD> 
     </tr>
     <tr>
      <tr class=common>     
        <TD class= title>
          展业机构
        </TD>
      <td class=input> <Input class=common type=text name=BranchAttr onclick="">
      </td>
      <TD class= title>
          考核年月
        </TD>
      <td class=input> <Input class=common type=text name=IndexCalNo onclick="">
      </td>
     </tr> 
      <tr>
      <tr class=common>  
      <td class=input width="26%" colspan=2 align=center><input type=button value="查    询" class=common onclick="easyQueryClick();">
      </td>
      </tr> 
    </table>
    

    <table> 
     <tr>
       	<td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentAdjGrid);">
    	</td>
    	<td class= titleImg>
    		 考核信息
    	</td>
     </tr>
    </table>
  	<Div  id= "divAgentAdjGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentAdjGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>    	
    	 <INPUT VALUE=" 首页 "  TYPE="button" onclick="turnPage.firstPage();">
         <INPUT VALUE="上一页"  TYPE="button" onclick="turnPage.previousPage();">
         <INPUT VALUE="下一页"  TYPE="button" onclick="turnPage.nextPage();">
         <INPUT VALUE=" 尾页 "  TYPE="button" onclick="turnPage.lastPage();">			
  	</div>
  	<table class=common>     
     <tr class=common>
      <td class=input width="26%" colspan=2 align=left><input type=button value="确认" class=common onclick="saveClick();"></td>
     </tr>
    </table>
    <input type=hidden name=Operate value=''>
    
 <!-- 作为代码选择的span -->
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>   