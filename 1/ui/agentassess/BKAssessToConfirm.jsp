 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String tDivShow = "none";
%>
<script>
   var mSql=" 1 and char(length(trim(comcode))) in (#4#,#8#) and comcode<>#86000000#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="BKAssessToConfirm.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BKAssessToConfirmInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <title>考核手工确认 </title>   
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else 
  {
    tTitleAgent = "业务员";
    tDivShow = "";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./BKAssessToConfirmSave.jsp" method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
	  <td class= titleImg align= center>请输入考核信息的查询条件：</td>
	</tr>
	</table>
    <table  class= common align=center>
      <TR  class= common>
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|notnull&code:comcode" 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,mSql,1,1);"
             onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,mSql,1,1);"
            ><Input name=ManageComName class="codename"   elementtype=nacessary>
          </TD> 
          
          <TD  class= title>
            考核年月
          </TD>
          <TD  class= input>
            <Input class=common name=IndexCalNo verify="考核年月|notnull&len=6&int" elementtype=nacessary >
            <font color="red">'YYYYMM'</font>
          </TD>
      </TR>
	<TR>
     <TD class=title>考核状态</TD>
		<TD class=input><Input name=State class=codeno CodeData="0|^01|考核未确认|^02|考核已确认"		
			ondblclick="return showCodeListEx('State',[this,StateName],[0,1]);"
			onkeyup="return showCodeListKeyEx('State',[this,StateName],[0,1]);"><Input
			class=codename name=StateName readOnly >
			</TD>
	</TR>
    </table>
 
    <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class=cssbutton >
    <INPUT VALUE="考核确认" TYPE=button onclick="return submitForm();" class=cssbutton >
    <INPUT VALUE="下  载" TYPE=button onclick="downLoad();" class=cssbutton >
    <font color = "red">（客户经理及营业部经理全部调整确认后再进行考核确认操作）</font>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden class=Common name=querySql > 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAssess1);">
    		</td>
    		<td class= titleImg>
    			 考核信息
    		</td>
    	</tr>
    </table>
  	<Div id= "divLAAssess1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanEvaluateGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton > 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
