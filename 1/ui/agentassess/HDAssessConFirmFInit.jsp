<%
//程序名称：HDAssessConFirmFInit.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = '';
    fm.all('IndexCalNo').value = '';
    fm.all('BranchType').value = getBranchType();
    fm.all('BranchType2').value = '<%=BranchType2%>';    
    fm.all('AgentSeries').value = '<%=AgentSeries%>';
    
  }
  catch(ex)
  {
    alert("在HDAssessConFirmFInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
	  initAgentInfoGrid();
  }
  catch(re)
  {
    alert("HDAssessConFirmFInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentInfoGrid
 ************************************************************
 */
function initAgentInfoGrid()
  {  
  	var tBranchType=getBranchType();                             
    var iArray = new Array();
      
      try
      {
		iArray[0]=new Array();
		iArray[0][0]="序号";         //列名
		iArray[0][1]="30px";         //列名
		iArray[0][2]=100;         //列名
		iArray[0][3]=0;         //列名		
		
		if(tBranchType=='5' )
		{//在这里分渠道
			//alert(tBranchType);
		
			iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="营销员代码";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="营销员姓名";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="销售单位代码";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="销售单位名称";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="当前职级";      		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="参考职级";      		//列名
      iArray[6][1]="50px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="确认职级";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;     
 		       
      iArray[8]=new Array();
      iArray[8][0]="确认职级名称";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=150;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
				         
		}
		
        AgentInfoGrid = new MulLineEnter( "fm" , "AgentInfoGrid" ); 

        //这些属性必须在loadMulLine前
        //AgentInfoGrid.mulLineCount = 5;   
        AgentInfoGrid.displayTitle = 1;
        AgentInfoGrid.hiddenPlus = 1;
    		AgentInfoGrid.hiddenSubtraction = 1;
        AgentInfoGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化AgentInfoGrid时出错："+ ex);
      }
    }

</script>