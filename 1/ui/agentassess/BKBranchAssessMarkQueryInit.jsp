<script language="JavaScript">
function initInpBox()
{
  try
  {
   	fm.all('AgentCode').value="";
  	fm.all('AgentCodeName').value="";
  	fm.all('BranchType').value='<%=BranchType%>';
  	fm.all('BranchType2').value='<%=BranchType2%>';
  	fm.all('AssessType').value="02";
  	fm.all('DoneDate').value='';
  	fm.all('AssessDate').value="";
  //	fm.all('ToolMark').value="";
        fm.all('ToolMark').value="";
        fm.all('ChargeMark').value="";
        fm.all('MeetMark').value="";
        fm.all('RuleMark').value="";  
        fm.all('ManageCom').value = '<%=tG.ManageCom%>';
  }catch(ex){alert("在BKAgentAssessInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");}
}

function initAssessMarkGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="部经理编码"; //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许


    iArray[2]=new Array();
    iArray[2][0]="考评序号"; //列名
    iArray[2][1]="0px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=3;              //是否允许输入,1表示允许,0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="考评类型"; //列名
    iArray[3][1]="0px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=3;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[4]=new Array();
    iArray[4][0]="执行日期";         //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="考评年月"; //列名
    iArray[5][1]="100px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许，0表示不允许


    iArray[6]=new Array();
    iArray[6][0]="活动工具管理分数"; //列名
    iArray[6][1]="120px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="费用使用分数"; //列名
    iArray[7][1]="120px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许 

    iArray[8]=new Array();
    iArray[8][0]="会议经营能力分数"; //列名
    iArray[8][1]="120px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="遵守规章制度分数"; //列名
    iArray[9][1]="120px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许           

    iArray[10]=new Array();
    iArray[10][0]="总分"; //列名
    iArray[10][1]="120px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=0;              //是否允许输入,1表示允许,0表示不允许  
	
    iArray[11]=new Array();
    iArray[11][0]="销售人员打分总和"; //列名
    iArray[11][1]="120px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=0;              //是否允许输入,1表示允许,0表示不允许  

	iArray[12]=new Array();
    iArray[12][0]="销售人员人数"; //列名
    iArray[12][1]="120px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=0;              //是否允许输入,1表示允许,0表示不允许  



    AssessMarkGrid = new MulLineEnter( "fm" , "AssessMarkGrid");
    
    AssessMarkGrid.mulLineCount = 0;   
    AssessMarkGrid.displayTitle = 1;
    AssessMarkGrid.locked=1;
    AssessMarkGrid.canSel=1;
    AssessMarkGrid.canChk=0;
          AssessMarkGrid.hiddenPlus=1;
      AssessMarkGrid.hiddenSubtraction=1;
    AssessMarkGrid.loadMulLine(iArray); 
  }
  catch(ex)
  {
    alert("在AfterAscriptQryInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox(); 
    initAssessMarkGrid();  
  }
  catch(re)
  {
    alert("InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>