<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LACrsSaleRateSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String branchtype = request.getParameter("BranchType");
  String branchtype2 = request.getParameter("BranchType2");
  String FlagStr = "Fail";
  String Content = "";
  String arrCount[] = request.getParameterValues("InpCrsSaleRateGridChk");
  String tmanagecom[] = request.getParameterValues("CrsSaleRateGrid1");
  String tAssessFlag[] = request.getParameterValues("CrsSaleRateGrid3");
  String tRate[] = request.getParameterValues("CrsSaleRateGrid5");
  //lineCount = arrCount.length; //行数
  
  LACrsSaleRateBL tLACrsSaleRateBL = new LACrsSaleRateBL();
  LACrsSaleRateSchema tLACrsSaleRateSchema;
  LACrsSaleRateSet tLACrsSaleRateSet = new LACrsSaleRateSet();
  int lineCount = arrCount.length;
  for(int i=0;i<lineCount;i++)
  {
  	if(arrCount[i].equals("1"))
    {
	  	tLACrsSaleRateSchema = new LACrsSaleRateSchema();
	    tLACrsSaleRateSchema.setManageCom(tmanagecom[i]);
	    tLACrsSaleRateSchema.setBranchType(branchtype);
	    tLACrsSaleRateSchema.setBranchType2(branchtype2);
	    tLACrsSaleRateSchema.setAssessFlag(tAssessFlag[i]);
	    tLACrsSaleRateSchema.setRate(tRate[i]);
	    tLACrsSaleRateSet.add(tLACrsSaleRateSchema);
    }
  }

System.out.println("tOperate11:"+tOperate);
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.add(tLACrsSaleRateSet);

System.out.println("tOperate:"+tOperate);
  try
  {
    System.out.println("this will save the data!!!");	
    tLACrsSaleRateBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLACrsSaleRateBL.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

