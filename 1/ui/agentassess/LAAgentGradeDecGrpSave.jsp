<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentGradeDecGrpSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
	String tManageCom = request.getParameter("ManageCom");
	String tIndexCalNo = request.getParameter("IndexCalNo");
	String tBranchType = request.getParameter("BranchType");
	String tBranchType2 = request.getParameter("BranchType2");
	String tBranchAttr = request.getParameter("BranchAttr");
	String tBranchName = request.getParameter("BranchName");
	String tAgentCode = request.getParameter("AgentCode");
	String tAgentName = request.getParameter("AgentName");
	int tDateCount = 0;

  //输入参数
  LAAssessHistorySchema tLAAssessHistorySchema = new LAAssessHistorySchema();
  LAAgentGradeDecGrpUI mLAAgentGradeDecGrpUI = new LAAgentGradeDecGrpUI();

  //输出参数
  CErrors tError = null;
  String tOperate="";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	// 准备数据
  tLAAssessHistorySchema.setManageCom(tManageCom);
  tLAAssessHistorySchema.setIndexCalNo(tIndexCalNo);
  tLAAssessHistorySchema.setBranchType(tBranchType);
  tLAAssessHistorySchema.setBranchType2(tBranchType2);
  
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.add(tLAAssessHistorySchema);
  tVData.add("");
  tVData.add(tBranchAttr);
  tVData.add(tBranchName);
  tVData.add(tAgentCode);
  tVData.add(tAgentName);
  
  System.out.println("add over");
  try
  {
    mLAAgentGradeDecGrpUI.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLAAgentGradeDecGrpUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>