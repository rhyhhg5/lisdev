//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function easyQueryClick()
{
	if (verifyInput() == false)
    return false;
	
  // 初始化表格
  initAgentInfoGrid();
	
  // 书写SQL语句
  var strSQL = "";
  	 
	strSQL = "select getunitecode(a.agentcode) , b.name, a.BranchAttr, "
  + " (select name from labranchgroup where agentgroup = b.agentgroup),"
	+ " a.AgentGrade, a.CalAgentGrade, a.AgentGrade1, "
  + " (select gradename from laagentgrade where gradecode=a.agentgrade1) " 
  + " FROM LAAssess a ,LAAgent b" 
  + " where "
  + " a.agentcode = b.agentcode"
  + " and a.State='1' "
   //业务系列
  if(fm.all('AgentSeries').value == '0')
  {
  		strSQL += " and a.agentgrade like 'A%' ";
  }
  else
  {
  	  strSQL += " and a.agentgrade like 'B%' ";
  }
  
  strSQL+= getWherePart("a.indexcalno","IndexCalNo")
  + getWherePart("a.ManageCom","ManageCom","like")
	+ getWherePart("a.BranchType","BranchType")
  + "ORDER BY a.BranchAttr,a.AgentGrade" ;
	
		 
	//alert(strSQL);
	//fm.IndexCalNo.value = strSQL;	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未查询到已确认需回退的考核结果信息！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组  
  turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = AgentInfoGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
  	alert("在CardPrintInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
	fm.submit(); //提交
}


