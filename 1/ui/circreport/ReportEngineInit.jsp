<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：CertifyQueryInit.jsp
//程序功能：
//创建日期：2002-08-15
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
var ItemClass='<%=request.getParameter("DisType")%>';
// 输入框的初始化（单记录部分）
function initfm1()
{ 

  try
  {
  	fm.reset();
  	fm.all('calType').value =ItemClass;
  	if(ItemClass=='0') {
        fm.all('TrRepType').style.display ='none';
    }
    //if(ItemClass=='X') {
        //fm.all('RepType').style.display ='';
    //}
  }
  catch(ex)
  {
    alert("ReportEngineInit.jsp-->form1---InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 初始化fm2
function initfm2()
{ 
  try
  {                            
    fm2.all('Year').value = '';
    fm2.all('Month').value = '';
    fm2.all('RepType').value = '';
    
    if(ItemClass=='0') {
      fm2.all('divfm2').style.display ='none';
    }
  
   
  }
  catch(ex)
  {
    alert("ReportEngineInit.jsp-->form2-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("ReportEngineInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    
    initSelBox();
    initfm1();
    initfm2();
    initLFDesbModeGrid();
	initContent(ItemClass);
	showCodeName();
  }
  catch(re)
  {
    alert("ReportEngineInit.jsp>InitForm函数中发生异常:初始化界面错误!");
  }
}

var LFDesbModeGrid;          //定义为全局变量，提供给displayMultiline使用 
function initLFDesbModeGrid()
{                               
	
	var iArray = new Array();
      
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		  //列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();  
		iArray[1][0]="模块类型";         		  //列名
		iArray[1][1]="80px";            		  //列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=2;              			  //是否允许输入,1表示允许，0表示不允许
	    iArray[1][10]="ItemType"; // 名字最好有唯一性
		iArray[1][11]="0|^01|业务数据^02|销售数据^03|财务数据^04|精算数据^05|再保数据^06|资金运用数据^07|人力资源数据^X1|业务数据^X2|销售数据^X3|财务数据^X4|精算数据^X5|再保数据^X6|资金运用数据^X7|人力资源数据";
		
	    
		iArray[2]=new Array();
		iArray[2][0]="操作时间";          	      //列名
		iArray[2][1]="80px";            		  //列宽
		iArray[2][2]=100;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		//iArray[3]=new Array();
		//iArray[3][0]="被提数表的编号";        	  //列名
		//iArray[3][1]="50px";            		  //列宽
		//iArray[3][2]=100;            			  //列最大值
		//iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		
		//iArray[4]=new Array();
		//iArray[4][0]="描述";        	  //列名
		//iArray[4][1]="80px";            		  //列宽
		//iArray[4][2]=100;            			  //列最大值
		//iArray[4][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
	  
		LFDesbModeGrid = new MulLineEnter("fm" , "LFDesbModeGrid"); 

        LFDesbModeGrid.mulLineCount = 0;   
        LFDesbModeGrid.displayTitle = 1;
        LFDesbModeGrid.hiddenPlus = 1;
        LFDesbModeGrid.hiddenSubtraction=1;
        LFDesbModeGrid.canChk =1; // 1为显示CheckBox列，0为不显示 (缺省值)
        //LFDesbModeGrid.chkBoxEventFuncName ="mlChange";
        //LFDesbModeGrid.chkBoxAllEventFuncName="mlChangeAll"; 
		LFDesbModeGrid.loadMulLine(iArray); 
		      
		      
	

 
	}
	catch(ex)
	{
		alert("初始化Grid时出错："+ ex);
	}
}


</script>