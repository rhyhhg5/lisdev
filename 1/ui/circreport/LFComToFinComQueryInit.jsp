<%
//程序名称：LFComToFinComQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-09-14 15:02:32
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ComCodeFin').value = "";
    fm.all('ComCode').value = "";
    fm.all('comcodefinname').value = "";
  }
  catch(ex) {
    alert("在LFComToFinComQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLFComToFinComGrid();  
  }
  catch(re) {
    alert("LFComToFinComQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LFComToFinComGrid;
function initLFComToFinComGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="机构编码";         		//列名
    iArray[0+1][1]="236px";         		//列名
    iArray[0+1][4]="code";         		//列名
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="财务系统机构编码";         		//列名
    iArray[1+1][1]="305px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="财务系统机构名称";         		//列名
    iArray[2+1][1]="324px";         		//列名
 
    LFComToFinComGrid = new MulLineEnter( "fm" , "LFComToFinComGrid" ); 
 
 
 
    LFComToFinComGrid.canChk = 0; 
    LFComToFinComGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LFComToFinComGrid.mulLineCount = 0;   
    LFComToFinComGrid.displayTitle = 1;
    LFComToFinComGrid.hiddenPlus = 1;
    LFComToFinComGrid.hiddenSubtraction = 1;
    LFComToFinComGrid.canSel = 1;
    LFComToFinComGrid.canChk = 0;
    LFComToFinComGrid.selBoxEventFuncName = "showOne";
*/
    LFComToFinComGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LFComToFinComGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
