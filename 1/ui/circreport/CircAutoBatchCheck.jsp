<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：CircAutoCheck.jsp
//程序功能：保监会报表自动校验
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="CircAutoBatchCheck.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CircAutoBatchCheckInit.jsp"%>
  <title>保监会报表自动校验 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./CircAutoBatchCheckChk.jsp">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入批量校验条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      	　 <TD  class= title>
           保监会管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=comcodeisc ondblclick="return showCodeList('comcodeisc',[this]);" onkeyup="return showCodeListKey('comcodeisc',[this]);">          
          </TD>        
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
           <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
           <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            统计类型
          </TD>  
          <TD class= input>
               <Input class="code" name=RepType verify="统计类型|NOTNULL" CodeData="0|^1|快报^2|月报^3|季报^4|半年报^5|年报" 
             ondblClick="showCodeListEx('RepType',[this],[0,1,2]);" onkeyup="showCodeListKeyEx('RepType',[this],[0,1,2]);">
          </TD>
        </TR>
    </table>
    <hr/>
          <INPUT VALUE="批量校验" class= common TYPE=button onclick="easyQueryClick();"> 
  	 <input type="hidden" name= "SQLHide" value= "">
  	 <input type="hidden" name= "SQLCountHide" value= "">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
