<%
//程序名称：LFDateQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-08-12 19:10:51
//创建人  ：yangtao
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('RepType').value = "";
    fm.all('StatMon').value = "";
    fm.all('StartDate').value = "";
    fm.all('EndDate').value = "";
  }
  catch(ex) {
    alert("在LFDateQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLFDateGrid();  
  }
  catch(re) {
    alert("LFDateQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LFDateGrid;
function initLFDateGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="类型";         		//列名
    iArray[0+1][1]="100px";         		//列名
 
    iArray[0+1][3]  =2;                 
    iArray[0+1][10] = "datetype";        
    iArray[0+1][11] = "0|^1|快报^2|月报^3|季报^4|半年报^5|年报"; 
           
    
    iArray[1+1]=new Array();
    iArray[1+1][0]="统计年月";         		//列名
    iArray[1+1][1]="200px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="报表开始日期";         		//列名
    iArray[2+1][1]="200px";         		//列名
 
    iArray[3+1]=new Array();
    iArray[3+1][0]="报表结束日期";         		//列名
    iArray[3+1][1]="200px";         		//列名
 
    LFDateGrid = new MulLineEnter( "fm" , "LFDateGrid" ); 
    LFDateGrid.mulLineCount = 1; 
    LFDateGrid.displayTitle = 1; 
    LFDateGrid.locked = 1; 
 
    LFDateGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LFDateGrid.mulLineCount = 0;   
    LFDateGrid.displayTitle = 1;
    LFDateGrid.hiddenPlus = 1;
    LFDateGrid.hiddenSubtraction = 1;
    LFDateGrid.canSel = 1;
    LFDateGrid.canChk = 0;
    LFDateGrid.selBoxEventFuncName = "showOne";
*/
    LFDateGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LFDateGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
