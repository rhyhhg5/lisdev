<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ReportYWImportChk.jsp
//程序功能：业务数据导入
//创建日期：2002-06-19 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
   <%@page import="com.sinosoft.msreport.*"%>
   <%@page import="com.sinosoft.workflow.circ.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean flag = true;
  GlobalInput tG = new GlobalInput();  
  tG=(GlobalInput)session.getValue("GI");  
  if(tG == null) {
		out.println("session has expired");
		return;
   } 
  
  	//接收信息
    //接收信息
  	TransferData tTransferData = new TransferData();
    String tStatYear = request.getParameter("StatYearHide");
	String tStatMonth = request.getParameter("StatMonHide");
	String tSubMissionID = request.getParameter("SubMissionID");
	String tMissionID = request.getParameter("MissionID");
	String tStartTime = "";
	String tEndTime   = "";
	
	


	System.out.println("tStatYear:"+tStatYear);
	System.out.println("tStatMonth:"+tStatMonth);
	String tReportDate= "";
	String tRepType   = "";
	tRepType   = "1";//月报
	LFDateDB tLFDateDB=new LFDateDB();
    LFDateSchema tLFDateSchema=new LFDateSchema();
    tLFDateSchema.setRepType(tRepType);
    tLFDateSchema.setStatMon(tStatYear+tStatMonth);
    tLFDateDB.setSchema(tLFDateSchema);

    if(!tLFDateDB.getInfo()){
     flag = false;
	
    }
    LFDateSchema mLFDateSchema=new LFDateSchema();
    mLFDateSchema = tLFDateDB.getSchema();
    tStartTime =mLFDateSchema.getStartDate();
    tEndTime   =mLFDateSchema.getEndDate();
	System.out.println("报告期开始日期:"+tStartTime);
	System.out.println("报告期结束日期:"+tEndTime);
	if(tStartTime==null)tStartTime="";
	if(tEndTime==null)  tEndTime="";
	//一月二月会有问题 
	//if(!tEndTime.equals(""))
	//{
      
       //String mDay[]=PubFun.calFLDate(tEndTime);
        
       //tReportDate=mDay[0];
	//} 
	tReportDate=tStatYear+"-"+tStatMonth+"-01";	
    String tItemCode="";
    String tItemNum="";
    String tItemType="";
	
	tItemType="  AND ItemType In ('01')  order by ItemNum";   
	String tWhereSQL=tItemCode+" ||"+tItemNum+"  ||"+tItemType;  

	if (tStatYear== "" ||  tStatMonth== ""  )
	{
		Content = "请录入统计年统计月!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{     
	      if(tStatYear != null && tStatMonth != null  )
	      {
             //准备公共工作流传输信息
            tTransferData.setNameAndValue("StatYear",tStatYear);
	        tTransferData.setNameAndValue("StatMon",tStatMonth) ;
	        tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
	        tTransferData.setNameAndValue("MissionID",tMissionID) ;	 
	        //准备公共传输信息
	        tTransferData.setNameAndValue("WhereSQL", tWhereSQL); 
	        tTransferData.setNameAndValue("NeedItemKey", "0"); 
	        tTransferData.setNameAndValue("ReportDate", tReportDate);
            tTransferData.setNameAndValue("makedate",PubFun.getCurrentDate());
            tTransferData.setNameAndValue("maketime",PubFun.getCurrentTime());
            tTransferData.setNameAndValue("sDate", tStartTime);
            tTransferData.setNameAndValue("eDate", tEndTime);	
	      
	      }// End of if
		  else
		  {
			Content = "传输数据失败!";
			flag = false;
		  }
	}

	System.out.println("MissionID:"+tMissionID);
    System.out.println("SubMissionID:"+tSubMissionID);
	System.out.println("tG.Operator:"+tG.Operator);
    System.out.println("tG.ComCode:"+tG.ComCode);
	System.out.println("tG.ManageCom:"+tG.ManageCom);
    System.out.println("tWhereSQL:"+tWhereSQL);
    System.out.println("tReportDate:"+tReportDate);
	System.out.println("makedate:"+PubFun.getCurrentDate());
    System.out.println("getCurrentTime:"+PubFun.getCurrentTime());
	System.out.println("sDate:"+tStartTime);
	System.out.println("eDate:"+tEndTime);

try
{
	System.out.println(flag);
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
  		CircReportWorkFlowUI tCircReportWorkFlowUI   = new CircReportWorkFlowUI();
		 if (!tCircReportWorkFlowUI.submitData(tVData,"0000000201"))//执行业务数据导入工作流节点0000000201
		{
			int n = tCircReportWorkFlowUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tCircReportWorkFlowUI.mErrors.getError(i).errorMessage);
			Content = "  业务数据导入失败，原因是: " + tCircReportWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tCircReportWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 业务数据导入成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
