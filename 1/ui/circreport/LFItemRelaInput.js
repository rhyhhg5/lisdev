//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if(!IsElementNull()){
	  return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "INSERT||MAIN";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LFItemRela.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
	  if(!IsElementNull()){
	  return false;
     }
	  var i = 0;
	  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
	  //showSubmitFrame(mDebug);
	  fm.fmtransact.value = "UPDATE||MAIN";
	  fm.submit(); //提交
	  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LFItemRelaQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
	  var i = 0;
	  if (!IsDelElementNull())
	  {
		  return false;
	  }
	  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
	  //showSubmitFrame(mDebug);
	  fm.fmtransact.value = "DELETE||MAIN";
	  fm.submit(); //提交
	  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
        fm.all('ItemCode').value   = arrResult[0][0] ;
        fm.all('ItemName').value   = arrResult[0][1] ;
        fm.all('OutItemCode').value= arrResult[0][2] ;
        fm.all('UpItemCode').value = arrResult[0][3] ;
        fm.all('ItemLevel').value  = arrResult[0][4] ;
        fm.all('CorpType').value   = arrResult[0][5] ;
        fm.all('ItemType').value   = arrResult[0][6] ;
        fm.all('IsQuick').value    = arrResult[0][7] ;
        fm.all('IsMon').value      = arrResult[0][8] ;
        fm.all('IsQut').value      = arrResult[0][9] ;
        fm.all('IsHalYer').value   = arrResult[0][10];
        fm.all('IsYear').value     = arrResult[0][11];
        fm.all('IsLeaf').value     = arrResult[0][12];
        fm.all('General').value    = arrResult[0][13];
        fm.all('Layer').value      = arrResult[0][14];
                                                 
                                                 
        fm.all('OutputFlag').value = arrResult[0][17];
        fm.all('ComFlag').value    = arrResult[0][18];
        fm.all('IsCalFlag').value  = arrResult[0][19];
        fm.all('IsYearAll').value  = arrResult[0][20];
	}
}               
        
/*********************************************************************
 *  判断是否为空
 *  参数  ：  无
 *  返回值： true;false
 *********************************************************************
 */
function IsElementNull(  )
{
	  if ( fm.ItemCode.value==null || fm.ItemCode.value=='')
	  {
		  alert("请输入内部科目编码  ！");
		  return false;
	  }
	  if ( fm.ItemName.value==null || fm.ItemName.value=='')
	  {
		  alert("请输入科目名称 ！");
		  return false;
	  }
	  if ( fm.OutItemCode.value==null || fm.OutItemCode.value=='')
	  {
		  alert("请输入外部科目编码  ！");
		  return false;
	  }
	  if ( fm.UpItemCode.value==null || fm.UpItemCode.value=='')
	  {
		  alert("请输入上级内部科目编码  ！");
		  return false;
	  }
	  if ( fm.ItemLevel.value==null || fm.ItemLevel.value=='')
	  {
		  alert("请输入科目级别  ！");
		  return false;
	  } 
	  if ( fm.CorpType.value==null || fm.CorpType.value=='')
	  {
		  alert("请输入公司类型  ！");
		  return false;
	  }
	  if ( fm.ItemType.value==null || fm.ItemType.value=='')
	  {
		  alert("请输入类型  ！");
		  return false;
	  }
	  if ( fm.IsQuick.value==null || fm.IsQuick.value=='')
	  {
		  alert("请输入是否是快报  ！");
		  return false;
	  }
	  if ( fm.IsMon.value==null || fm.IsMon.value=='')
	  {
		  alert("请输入是否是月报  ！");
		  return false;
	  }
	  if ( fm.IsQut.value==null || fm.IsQut.value=='')
	  {
		  alert("请输入是否是季报  ！");
		  return false;
	  } 
	  if ( fm.IsHalYer.value==null || fm.IsHalYer.value=='')
	  {
		  alert("请输入是否是半年报  ！");
		  return false;
	  }
	  if ( fm.IsYear.value==null || fm.IsYear.value=='')
	  {
		  alert("请输入是否是年报  ！");
		  return false;
	  }
	  if ( fm.IsLeaf.value==null || fm.IsLeaf.value=='')
	  {
		  alert("请输入是否是叶子结点  ！");
		  return false;
	  }
	  if ( fm.General.value==null || fm.General.value=='')
	  {
		  alert("请输入总分  ！");
		  return false;
	  }
	  if ( fm.OutputFlag.value==null || fm.OutputFlag.value=='')
	  {
		  alert("请输入是否上报标志  ！");
		  return false;
	  } 
	  if ( fm.IsCalFlag.value==null || fm.IsCalFlag.value=='')
	  {
		  alert("请输入是否一级计算标志  ！");
		  return false;
	  }
	  if ( fm.IsYearAll.value==null || fm.IsYearAll.value=='')
	  {
		  alert("请输入是否是年度报  ！");
		  return false;
	  }
	 
	  return true;
}

/*********************************************************************
 *  判断删除时是否为空
 *  参数  ：  无
 *  返回值： true;false
 *********************************************************************
 */
function IsDelElementNull(  )
{
	 if ( fm.ItemCode.value==null || fm.ItemCode.value=='')
	  {
		  alert("请输入内部科目编码  ！");
		  return false;
	  }
	 
	  return true;
}
