<%
//程序名称：LFChargeModeQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-09-14 14:29:17
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('FinItemCode').value = "";
    fm.all('RiskCode').value = "";
    fm.all('RiskName').value = "";
  }
  catch(ex) {
    alert("在LFChargeModeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLFChargeModeGrid();  
  }
  catch(re) {
    alert("LFChargeModeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LFChargeModeGrid;
function initLFChargeModeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="财务科目编码";         		//列名
    iArray[0+1][1]="261px";         		//列名
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="险种编码";         		//列名
    iArray[1+1][1]="254px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="险种名称";         		//列名
    iArray[2+1][1]="219px";         		//列名
 
    LFChargeModeGrid = new MulLineEnter( "fm" , "LFChargeModeGrid" ); 
    LFChargeModeGrid.mulLineCount = 1; 
    LFChargeModeGrid.displayTitle = 1; 
    LFChargeModeGrid.locked = 1; 
 
    LFChargeModeGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LFChargeModeGrid.mulLineCount = 0;   
    LFChargeModeGrid.displayTitle = 1;
    LFChargeModeGrid.hiddenPlus = 1;
    LFChargeModeGrid.hiddenSubtraction = 1;
    LFChargeModeGrid.canSel = 1;
    LFChargeModeGrid.canChk = 0;
    LFChargeModeGrid.selBoxEventFuncName = "showOne";
*/
    LFChargeModeGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LFChargeModeGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
