<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CircMidYWQueryInit.jsp
//程序功能：保监会导入数据查询 
//创建日期：2002-06-19 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
<%
  String mDisType="";
  mDisType=request.getParameter("DisType");
  String mStatYear="";
  mStatYear=request.getParameter("StatYear");
  String mStatMon="";
  mStatMon=request.getParameter("StatMon");
 %>        
// 输入框的初始化（单记录部分）
function initInpBox(tDisType,tStatYear,tStatMon)
{ 

  try
  {                                   
	// 保单查询条件
	fm.all('DisType').value=tDisType;
	fm.all('StatYear').value=tStatYear;
	fm.all('StatMon').value=tStatMon;
   }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm(tDisType,tStatYear,tStatMon)
{
  try
  {
    initInpBox(tDisType,tStatYear,tStatMon);   
    initSelBox();    
    initPolGrid();
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][9]="险种编码|code:RiskCode&NOTNULL";
      iArray[1][18]=250;
      iArray[1][19]= 0 ;
      
      iArray[2]=new Array();
      iArray[2][0]="管理机构";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="comcode";              	        //是否引用代码:null||""为不引用
      iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[2][9]="机构编码|code:comcode&NOTNULL";
      iArray[2][18]=250;
      iArray[2][19]= 0 ;

      iArray[3]=new Array();
      iArray[3][0]="再保项目";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][10] = "ReinsurItem";
      iArray[3][11] = "0|^L|法定分保^C|商业分保";
      iArray[3][12] = "5";
      iArray[3][19] = "0";

      iArray[4]=new Array();
      iArray[4][0]="再保地点";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][10] = "ReinsurLocal";
      iArray[4][11] = "0|^0|境内^1|境外 ";
      iArray[4][12] = "5";
      iArray[4][19] = "0";
     

      iArray[5]=new Array();
      iArray[5][0]="协议类型";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][10] = "ProtItem";
      iArray[5][11] = "0|^T|合同分保^F|临时分保";
      iArray[5][12] = "5";
      iArray[5][19] = "0";

      iArray[6]=new Array();
      iArray[6][0]="分出分入标志";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][10] = "InOutFlag";
      iArray[6][11] = "0|^0|分出^1|分入";
      iArray[6][12] = "5";
      iArray[6][19] = "0";


      iArray[7]=new Array();
      iArray[7][0]="销售渠道";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][10] = "SaleChnl";
      iArray[7][11] = "0|^01|个人代理^02|公司直销^03|保险专业代理^04|银行邮政代理^05|其他兼业代理^06|保险经纪业务";
      iArray[7][12] = "5";
      iArray[7][19] = "0";

      
      iArray[8]=new Array();
      iArray[8][0]="团单个单标志";         		//列名
      iArray[8][1]="0px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="生存金标志";         		//列名
      iArray[9][1]="0px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[9][10] = "AnnulFlag";
      iArray[9][11] = "0|^0|满期金^1|年金";
      iArray[9][12] = "5";
      iArray[9][19] = "0";
     
      iArray[10]=new Array();
      iArray[10][0]="经纪佣金标志";         		//列名
      iArray[10][1]="0px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[10][10] = "AgentComFlag";
      iArray[10][11] = "0|^0|非经纪佣金^1|经纪佣金";
      iArray[10][12] = "5";
      iArray[10][19] = "0";

      iArray[11]=new Array();
      iArray[11][0]="分保费用类型";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[11][10] = "CessFeeType";
      iArray[11][11] = "0|^1|分保保费^2|分保费用^3|分保赔款^4|分保退保金";
      iArray[11][12] = "5";
      iArray[11][19] = "0";
      
      iArray[12]=new Array();
      iArray[12][0]="分保金额";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      
      iArray[13]=new Array();
      iArray[13][0]="统计年";         		//列名
      iArray[13][1]="60px";            		//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="统计月";         		//列名
      iArray[14][1]="60px";            		//列宽
      iArray[14][2]=200;            			//列最大值
      iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许      

     
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 3;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 0;
      PolGrid.canChk = 0;
      PolGrid.loadMulLine(iArray);  
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 保单状态列表的初始化
function initPolStatuGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="校验错误明细";         		//列名
      iArray[1][1]="400px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      PolStatuGrid = new MulLineEnter( "fm" , "PolStatuGrid" ); 
      //这些属性必须在loadMulLine前
      PolStatuGrid.mulLineCount = 3;   
      PolStatuGrid.displayTitle = 1;
      PolStatuGrid.locked = 1;
      PolStatuGrid.canSel = 0;
      PolStatuGrid.canChk = 0;
      PolStatuGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolStatuGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>