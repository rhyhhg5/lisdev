<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LFComFinSave.jsp
//程序功能：
//创建日期：2004-09-14 15:49:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LFComFinSchema tLFComFinSchema   = new LFComFinSchema();
  OLFComFinUI tOLFComFinUI   = new OLFComFinUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLFComFinSchema.setComFin(request.getParameter("ComFin"));
    tLFComFinSchema.setName(request.getParameter("Name"));
    tLFComFinSchema.setFoundDate(request.getParameter("FoundDate"));
    tLFComFinSchema.setEndFlag(request.getParameter("EndFlag"));
    tLFComFinSchema.setEndDate(request.getParameter("EndDate"));
    tLFComFinSchema.setAttribute(request.getParameter("Attribute"));
    tLFComFinSchema.setSign(request.getParameter("Sign"));
    tLFComFinSchema.setcalFlag(request.getParameter("calFlag"));
    tLFComFinSchema.setAddress(request.getParameter("Address"));
    tLFComFinSchema.setZipCode(request.getParameter("ZipCode"));
    tLFComFinSchema.setPhone(request.getParameter("Phone"));
    tLFComFinSchema.setFax(request.getParameter("Fax"));
    tLFComFinSchema.setEMail(request.getParameter("EMail"));
    tLFComFinSchema.setWebAddress(request.getParameter("WebAddress"));
    tLFComFinSchema.setSatrapName(request.getParameter("SatrapName"));
    tLFComFinSchema.setApproveDate(request.getParameter("ApproveDate"));
    tLFComFinSchema.setPassDate(request.getParameter("PassDate"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLFComFinSchema);
  	tVData.add(tG);
    tOLFComFinUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLFComFinUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
