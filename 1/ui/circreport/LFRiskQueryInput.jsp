<%
//程序名称：LFRiskInput.jsp
//程序功能：功能描述
//创建日期：2004-07-20 09:40:17
//创建人  ：yangtao
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LFRiskQueryInput.js"></SCRIPT> 
  <%@include file="LFRiskQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLFRiskGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      险种编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskCode >
    </TD>
    <TD  class= title>
      险种版本
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskVer >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      险种名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskName >
    </TD>
    <TD  class= title>
      险种分类
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=RiskType  CodeData="0|^L|寿险^A|意外险^H|健康险"  ondblClick="showCodeListEx('RiskType',[this,RiskTypeName],[0,1]);" onkeyup="showCodeListKeyEx('RiskType',[this,RiskTypeName],[0,1]);"><input class=codename name= RiskTypeName>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      险种类别
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=RiskPeriod CodeData="0|^L|长险^M|一年期险^S|极短期险"  ondblClick="showCodeListEx('RiskPeriod',[this,RiskPeriodName],[0,1]);" onkeyup="showCodeListKeyEx('RiskPeriod',[this,RiskPeriodName],[0,1]);"><input class=codename name= RiskPeriodName>
    </TD>
    <TD  class= title>
      主附险标记
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=SubRiskFlag CodeData="0|^M|主险^S|附险^A|两者都可以"  ondblClick="showCodeListEx('SubRiskFlag',[this,SubRiskFlagName],[0,1]);" onkeyup="showCodeListKeyEx('SubRiskFlag',[this,SubRiskFlagName],[0,1]);"><input class=codename name= SubRiskFlagName>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      寿险分类
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=LifeType CodeData="0|^1|传统险（普通寿险）^2|分红^3|投连^4|万能^5|其他"  ondblClick="showCodeListEx('LifeType',[this,LifeTypeName],[0,1]);" onkeyup="showCodeListKeyEx('LifeType',[this,LifeTypeName],[0,1]);"><input class=codename name= LifeTypeName>
    </TD>
    <TD  class= title>
      险种事故责任分类（寿险）
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=RiskDutyType CodeData="0|^1|终身^2|两全及生存^3|定期^4|年金^5|其他"  ondblClick="showCodeListEx('RiskDutyType',[this,RiskDutyTypeName],[0,1]);" onkeyup="showCodeListKeyEx('RiskDutyType',[this,RiskDutyTypeName],[0,1]);"><input class=codename name= RiskDutyTypeName>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      健康险和意外险期限分类
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=RiskYearType CodeData="0|^0|一年期以内^1|一年期^2|一年期以上"  ondblClick="showCodeListEx('RiskYearType',[this,RiskYearTypeName],[0,1]);" onkeyup="showCodeListKeyEx('RiskYearType',[this,RiskYearTypeName],[0,1]);"><input class=codename name= RiskYearTypeName>
    </TD>
    <TD  class= title>
      健康险细分
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=HealthType CodeData="0|^1|医疗保险 ^2|普通疾病保险^3|重大疾病保^4|失能保险^5|护理保险"  ondblClick="showCodeListEx('HealthType',[this,HealthTypeName],[0,1]);" onkeyup="showCodeListKeyEx('HealthType',[this,HealthTypeName],[0,1]);"><input class=codename name= HealthTypeName>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      意外险细分
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=AccidentType CodeData="0|^1|航空意外险 ^2|学生平安险^3|建筑工人意外伤害险"  ondblClick="showCodeListEx('AccidentType',[this,AccidentTypeName],[0,1]);" onkeyup="showCodeListKeyEx('AccidentType',[this,AccidentTypeName],[0,1]);"><input class=codename name= AccidentTypeName>
    </TD>
    <TD  class= title>
      养老险标记
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=EndowmentFlag CodeData="0|^0|不是养老险 ^1|是养老险"  ondblClick="showCodeListEx('EndowmentFlag',[this,EndowmentFlagName],[0,1]);" onkeyup="showCodeListKeyEx('EndowmentFlag',[this,EndowmentFlagName],[0,1]);"><input class=codename name= EndowmentFlagName>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      养老险细分1
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=EndowmentType1 CodeData="0|^0|无税优计划 ^1|有税优计划"  ondblClick="showCodeListEx('EndowmentType1',[this,EndowmentType1Name],[0,1]);" onkeyup="showCodeListKeyEx('EndowmentType1',[this,EndowmentType1Name],[0,1]);"><input class=codename name= EndowmentType1Name>
    </TD>
    <TD  class= title>
      养老险细分2
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=EndowmentType2 CodeData="0|^0|无税优计划 ^1|有税优计划"  ondblClick="showCodeListEx('EndowmentType2',[this,EndowmentType2Name],[0,1]);" onkeyup="showCodeListKeyEx('EndowmentType2',[this,EndowmentType2Name],[0,1]);"><input class=codename name= EndowmentType2Name>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      险种的销售渠道
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=RiskSaleChnl CodeData="0|^01|个人代理^02|公司直销^03|保险专业代理^04|银行邮政代理^05|其他兼业代理^06|保险经纪业务"  ondblClick="showCodeListEx('RiskSaleChnl',[this,RiskSaleChnlName],[0,1]);" onkeyup="showCodeListKeyEx('RiskSaleChnl',[this,RiskSaleChnlName],[0,1]);"><input class=codename name= RiskSaleChnlName>
    </TD>
    <TD  class= title>
      险种的团个单标志
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=RiskGrpFlag CodeData="0|^1|个人 ^2|团体"  ondblClick="showCodeListEx('RiskGrpFlag',[this,RiskGrpFlagName],[0,1]);" onkeyup="showCodeListKeyEx('RiskGrpFlag',[this,RiskGrpFlagName],[0,1]);"><input class=codename name= RiskGrpFlagName>
    </TD>
  </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询" TYPE=button class=common onclick="easyQueryClick();">
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLFRisk1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLFRiskGrid" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLFRiskGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
