<%
//程序名称：LFItemRelaInput.jsp
//程序功能：功能描述
//创建日期：2004-08-12 19:10:47
//创建人  ：yangtao
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LFItemRelaQueryInput.js"></SCRIPT> 
  <%@include file="LFItemRelaQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLFItemRelaGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      内部科目编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ItemCode >
    </TD>
    <TD  class= title>
      科目名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ItemName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      外部科目编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OutItemCode >
    </TD>
    <TD  class= title>
      上级内部科目编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=UpItemCode >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      科目级别
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ItemLevel CodeData="0|^1|一级^2|二级^3|三级^4|四级^5|五级^6|六级^7|七级"  ondblClick="showCodeListEx('ItemLevel',[this],[0]);" onkeyup="showCodeListKeyEx('ItemLevel',[this],[0]);" >
    </TD>                                               
    <TD  class= title>
      公司类型
    </TD>
    <TD  class= input>
      <Input class= 'code' name=CorpType CodeData="0|^1|产^2|寿^3|再^4|集"  ondblClick="showCodeListEx('CorpType',[this],[0]);" onkeyup="showCodeListKeyEx('CorpType',[this],[0]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      类型
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ItemType CodeData="0|^1|资产^2|负债^3|权益^4|损益^5|现金流^6|统计^7|资金"  ondblClick="showCodeListEx('ItemType',[this],[0]);" onkeyup="showCodeListKeyEx('ItemType',[this],[0]);">
    </TD>
    <TD  class= title>
      是否是快报
    </TD>
    <TD  class= input>
      <Input class= 'code' name=IsQuick CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsQuick',[this],[0]);" onkeyup="showCodeListKeyEx('IsQuick',[this],[0]);">
    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否是月报
    </TD>
    <TD  class= input>
      <Input class= 'code' name=IsMon CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsMon',[this],[0]);" onkeyup="showCodeListKeyEx('IsMon',[this],[0]);">
    
    </TD>
    <TD  class= title>
      是否是季报
    </TD>
    <TD  class= input>
      <Input class= 'code' name=IsQut CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsQut',[this],[0]);" onkeyup="showCodeListKeyEx('IsQut',[this],[0]);">
    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否是半年报
    </TD>
    <TD  class= input>
      <Input class= 'code' name=IsHalYer CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsHalYer',[this],[0]);" onkeyup="showCodeListKeyEx('IsHalYer',[this],[0]);">
    
    </TD>
    <TD  class= title>
      是否是年报
    </TD>
    <TD  class= input>
      <Input class= 'code' name=IsYear CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsYear',[this],[0]);" onkeyup="showCodeListKeyEx('IsYear',[this],[0]);">
    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否是叶子结点
    </TD>
    <TD  class= input>
      <Input class= 'code' name=IsLeaf CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsLeaf',[this],[0]);" onkeyup="showCodeListKeyEx('IsLeaf',[this],[0]);">
    
    </TD>
    <TD  class= title>
      总分
    </TD>
    <TD  class= input>
      <Input class= 'common' name=General >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      层级
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Layer >
    </TD>
    <TD  class= title>
      定义口径描述
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Description >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      备注
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Remark >
    </TD>
    <TD  class= title>
      是否上报标志
    </TD>
    <TD  class= input>
      <Input class= 'code' name=OutputFlag CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('OutputFlag',[this],[0]);" onkeyup="showCodeListKeyEx('OutputFlag',[this],[0]);">
    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      上报数据的机构粒度
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ComFlag CodeData="0|^1|据明细到总公司^2|数据明细到分公司^3|明细到中心支公司^4|明细到支公司"  ondblClick="showCodeListEx('ComFlag',[this],[0]);" onkeyup="showCodeListKeyEx('ComFlag',[this],[0]);">
    
    </TD>
    <TD  class= title>
      是否一级计算标志
    </TD>
    <TD  class= input>
      <Input class= 'code' name=IsCalFlag CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsCalFlag ',[this],[0]);" onkeyup="showCodeListKeyEx('IsCalFlag ',[this],[0]);">
    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否是年度报
    </TD>
    <TD  class= input>
      <Input class= 'code' name=IsYearAll CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsYearAll',[this],[0]);" onkeyup="showCodeListKeyEx('IsYearAll',[this],[0]);">
    
    </TD>
    <TD> </TD>
    <TD>
    </TD>
  </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询" TYPE=button class=common onclick="easyQueryClick();">      
        <INPUT VALUE="返回" TYPE=button class=common name=Return   onclick="returnParent();"> 	 
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLFItemRela1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLFItemRela1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLFItemRelaGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
     
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
