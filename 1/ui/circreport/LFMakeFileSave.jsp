<%
//程序名称：LFMakeFileSave.jsp
//程序功能：
//创建日期：2004-06-14
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.msreport.*"%>   
<%  //接收信息，并作校验处理。
  //输入参数
  
//  GlobalInput tG = new GlobalInput();
//  tG = (GlobalInput)session.getValue("GI");              //获得用户信息

    //输出参数
  CErrors tError = null;                                 //错误类的获得
                                                         
  VData tVData = new VData();                            // 准备传输数据 VData
  String FlagStr ="";
  String Content="";
              							
    String StatYear=request.getParameter("Year");
    String StatMon=request.getParameter("Month");
    String RepType=request.getParameter("RepType");
    tVData.add(StatYear);
    tVData.add(StatMon);
    tVData.add(RepType);

     MakeXMLUI tMakeXMLUI = new MakeXMLUI();
    
    try
    {
    	tMakeXMLUI.submitData(tVData);
    }
    catch(Exception ex)
  	{
    		Content = "保存失败，原因是:" + ex.toString();
    		System.out.println(Content);
    		FlagStr = "Fail";
  	}
  	
  	
	  if (!FlagStr.equals("Fail"))
	  {
	    tError = tMakeXMLUI.mErrors;
	    if (!tError.needDealError())
	    {                          
	    	Content = " 保存成功! ";
	    	FlagStr = "Succ";
	    }
	    else                                                                           
	    {
	    	Content = " 保存失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	  }
  	
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>  	