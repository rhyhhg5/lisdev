<%
//程序名称：LFRiskQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-07-20 09:40:17
//创建人  ：yangtao
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('RiskCode').value = "";
    fm.all('RiskVer').value = "";
    fm.all('RiskName').value = "";
    fm.all('RiskType').value = "";
    fm.all('RiskPeriod').value = "";
    fm.all('SubRiskFlag').value = "";
    fm.all('LifeType').value = "";
    fm.all('RiskDutyType').value = "";
    fm.all('RiskYearType').value = "";
    fm.all('HealthType').value = "";
    fm.all('AccidentType').value = "";
    fm.all('EndowmentFlag').value = "";
    fm.all('EndowmentType1').value = "";
    fm.all('EndowmentType2').value = "";
    fm.all('RiskSaleChnl').value = "";
    fm.all('RiskGrpFlag').value = "";
  }
  catch(ex) {
    alert("在LFRiskQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLFRiskGrid();  

  }
  catch(re) {
    alert("LFRiskQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LFRiskGrid;
function initLFRiskGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";            //列名
    iArray[0][1]="30px";         	//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="险种编码";         		//列名
    iArray[1][1]="60px";         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="险种版本";         		//列名
    iArray[2][1]="60px";         		//列名
    
    iArray[3]=new Array();                    
    iArray[3][0]="险种名称";         		//列名
    iArray[3][1]="250px";         		//列名  
      
      
    iArray[4]=new Array();
    iArray[4][0]="险种分类";         	//列名
    iArray[4][1]="100px";         		//列名
    iArray[4][3]=2;  
    iArray[4][10] = "RiskType";  
    iArray[4][11] = "0|^L|寿险^A|意外险^H|健康险";        
    
   
    iArray[5]=new Array();
    iArray[5][0]="险种类别";         		//列名
    iArray[5][1]="100px";         		//列名       
    iArray[5][3]=2;
    iArray[5][10] = "RiskPeriod";     
    iArray[5][11] = "0|^L|长险^M|一年期险^S|极短期险";  
             
    iArray[6]=new Array();
    iArray[6][0]="主附险标记";         	//列名     
    iArray[6][1]="100px";         		//列名
    iArray[6][3]=2;  
    iArray[6][10] = "SubRiskFlag";  
    iArray[6][11] = "0|^M|主险^S|附险^A|两者都可以"; 
    
    iArray[7]=new Array();                           
    iArray[7][0]="寿险分类";         	//列名       
    iArray[7][1]="100px";         		//列名    
    iArray[7][3]=2;    
    iArray[7][10] = "LifeType";   
    iArray[7][11] = "0|^1|传统险（普通寿险）^2|分红^3|投连^4|万能^5|其他"; 
    
    iArray[8]=new Array();                           
    iArray[8][0]="寿险分类2";         	//列名       
    iArray[8][1]="100px";         		//列名   
    iArray[8][3]=2;      
    iArray[8][10] = "RiskDutyType";   
    iArray[8][11] = "0|^1|终身^2|两全及生存^3|定期^4|年金^5|其他"; 
    
    iArray[9]=new Array();                           
    iArray[9][0]="健康险和意外险期限分类";         	//列名       
    iArray[9][1]="100px";         		//列名   
    iArray[9][3]=2;      
    iArray[9][10] = "RiskYearType";   
    iArray[9][11] = "0|^0|一年期以内^1|一年期^2|一年期以上";
    
    iArray[10]=new Array();                           
    iArray[10][0]="健康险细分";         	//列名       
    iArray[10][1]="100px";         		//列名  
    iArray[10][3]=2;   
    iArray[10][10] = "HealthType";      
    iArray[10][11] = "0|^1|医疗保险 ^2|普通疾病保险^3|重大疾病保^4|失能保险^5|护理保险";
    
    iArray[11]=new Array();                           
    iArray[11][0]="意外险细分";         	//列名       
    iArray[11][1]="100px";         		//列名       
    iArray[11][3]=2;  
    iArray[11][10]="AccidentType";         		//列名
    iArray[11][11] = "0|^1|航空意外险 ^2|学生平安险^3|建筑工人意外伤害险";
    
    iArray[12]=new Array();                           
    iArray[12][0]="养老险标记";         	//列名       
    iArray[12][1]="100px";         		//列名   
    iArray[12][3]=2;  
    iArray[12][10]="EndowmentFlag";         		//列名     
    iArray[12][11] = "0|^0|不是养老险 ^1|是养老险";  
    
    iArray[13]=new Array();                           
    iArray[13][0]="养老险细分1";         	//列名       
    iArray[13][1]="100px";         		//列名    
    iArray[13][3]=2;  
    iArray[13][10]="EndowmentType1";         		//列名     
    iArray[13][11] = "0|^0|无税优计划 ^1|有税优计划";  
    
    iArray[14]=new Array();                           
    iArray[14][0]="养老险细分2";         	//列名       
    iArray[14][1]="100px";         		//列名    
    iArray[14][3]=2;  
    iArray[14][10]="EndowmentType2";         		//列名      
    iArray[14][11] = "0|^0|DB固定给付计划 ^1|DC固定缴费计划";  
    
    iArray[15]=new Array();                           
    iArray[15][0]="险种的销售渠道";      //列名       
    iArray[15][1]="100px";         		//列名    
    iArray[15][3]=2;     
    iArray[15][10] = "RiskSaleChnl";  
    iArray[15][11] = "0|^01|个人代理^02|公司直销^03|保险专业代理^04|银行邮政代理^05|其他兼业代理^06|保险经纪业务";

    iArray[16]=new Array();                           
    iArray[16][0]="险种的团个单标志";         	//列名       
    iArray[16][1]="100px";         		//列名      
    iArray[16][3]=2;  
    iArray[16][10]="RiskGrpFlag";         		//列名  
    iArray[16][11] = "0|^1|个人 ^2|团体";  
    
    LFRiskGrid = new MulLineEnter( "fm" , "LFRiskGrid" );                 
    LFRiskGrid.mulLineCount = 1; 
    LFRiskGrid.displayTitle = 1; 
    LFRiskGrid.locked = 1; 

    LFRiskGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前

    LFRiskGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LFRiskGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
