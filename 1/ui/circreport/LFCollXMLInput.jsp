<html>  
<%
//程序名称：
//程序功能：
//创建日期：2004-06-14
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="LFCollXMLInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LFCollXMLInit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form action="./LFCollXMLSave.jsp" method=post name=fm target="fraSubmit">
  <table>
	<tr>
    	<td>
    		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divReport);">
    	</td>
    	<td class= titleImg>
        	统计报表条件
       	</td>   		 
    </tr>
  </table>
  <Div  id= "divReport" style= "display: ''">
	<table  class= common>
    	<TR  class= common>
          	<TD  class= title>
            	统计报表年
          	</TD>
          	<TD  class= input>
            	<Input class= common name=Year verify="统计报表年|notnull">
           	</TD>           
          	<TD  class= title>
            	统计报表月
          	</TD>
          	<TD  class= input>
            	<Input class= code name=Month verify="统计报表月|notnull" CodeData="0|^1|一月|M^2|两月|M^3|三月|M^4|四月|M^5|五月|M^6|六月|M^7|七月|M^8|八月|M^9|九月|M^10|十月|M^11|十一月|M^12|十二月|M" ondblClick="showCodeListEx('ReportMonth',[this],[0]);" onkeyup="showCodeListKeyEx('ReportMonth',[this],[0]);">
          	</TD>
     	</TR>
     	<TR  class= common>
          	<TD  class= title>
            	报表类型
          	</TD>
          	<TD  class= input>
            	<Input class= code name=RepType verify="报表类型|notnull" CodeData="0|^1|快报|M^2|月报|M^3|季报|M^4|半年报|M^5|年报|M" ondblClick="showCodeListEx('ReportType',[this],[0]);" onkeyup="showCodeListKeyEx('ReportType',[this],[0]);">
          	</TD>
          	<TD  class= title>

          	</TD>
          	<TD  class= input>

          	</TD>
    	</TR>
    	<TR class=input>              
	    	<TD class=common>
	        	<input type =button class=common value="数据汇总" onclick="CollData();">    
	        </TD>
        	<TD class=common>
          		<input type =button class=common value="生成文件" onclick="MakeFile();">    
        	</TD>
       	</TR>   
    </table>
   </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>