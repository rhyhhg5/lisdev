<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>    
<%
//程序名称：LFIscPrint.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LFIscExcelCreat.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();" >    
  <form action="./LFIscExcelCreatSave.jsp" method=post name=fm target="fraSubmit">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
      	  <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD> 
          <TD  class= title >
            	报表类型
          </TD>
          <TD  class= input>
            	<Input class= codeno name=RepType verify="报表类型|notnull" CodeData="0|^1|快报|M^2|月报|M^3|季报|M^4|半年报|M^5|年报|M^6|年度报|M" ondblClick="showCodeListEx('ReportType',[this,RepTypeName],[0,1]);" onkeyup="showCodeListKeyEx('ReportType',[this,RepTypeName],[0,1]);"><input class = codename name =RepTypeName>
          </TD>            
        </TR>
        <TR  class= common>
          <TD  class= title>
            	统计报表年
          </TD>
          <TD  class= input>
            	
            	<!--  <Input class= codeno name=StatYear verify="统计报表年|notnull"CodeData="0|^2004|2004年|M^2005|2005年|M^2006|2006年|M^2007|2007年|M^2008|2008年|M^2009|2009年|M^2010|2010年|M" ondblClick="showCodeListEx('StatYear',[this,StatYearName],[0,1]);" onkeyup="showCodeListKeyEx('StatYear',[this,StatYearName],[0,1]);"><input class= codename name=StatYearName>-->
            	<Input class=codeNo name=StatYear  verify="统计报表年|notnull" ondblclick="return showCodeList('startyear',[this,StatYearName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('startyear',[this,StatYearName],[0,1],null,null,null,1);"><input class=codename name=StatYearName readonly=true>
           </TD>           
          <TD  class= title>
            	统计报表月
          </TD>
          <TD  class= input>
            	<Input class= codeno name=StatMon verify="统计报表月|notnull" CodeData="0|^01|一月|M^02|二月|M^03|三月|M^04|四月|M^05|五月|M^06|六月|M^07|七月|M^08|八月|M^09|九月|M^10|十月|M^11|十一月|M^12|十二月|M" ondblClick="showCodeListEx('ReportMonth',[this,StatMonthName],[0,1]);" onkeyup="showCodeListKeyEx('ReportMonth',[this,StatMonthName],[0,1]);"><input class= codename name=StatMonthName>          
          </TD>
        </TR>
    </table>
    <INPUT VALUE="生成excel文件" class="cssButton" TYPE="button" onclick="submitForm()">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 