<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
	//程序名称：
	//程序功能：
	//创建日期：2002-06-19 11:10:36
	//创建人  ：WHN
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="CirsTreeQuery.js"></SCRIPT>
<script src="../menumang/treeMenu.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="CircTreeQueryInit.jsp"%>
</head>
<body onload="initForm();">
<form method=post name=fm action=""
	target="fraSubmit">

<table class=common border=0 width=100%>
	<tr>
		<td class=titleImg align=center>请输入查询条件：</td>
	</tr>
</table>
<table class=common align=center>
	<TR class=common>
		<TD class=title>错误流水号</TD>
		<TD class=input><Input class="common" name=SerialNo ></TD>
		<TD class=title>生成日期</TD>
		<TD class=input><Input class="common" name=MakeDate ></TD>
	</TR>
</table>
<hr />
<INPUT VALUE="查询" class=common TYPE=button onclick="easyQueryClick();">
<!--INPUT VALUE="日志信息" TYPE=button class= common onclick="detailInfo();"-->
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this, divLCPol1);"></td>
		<td class=titleImg>公式查询</td>
	</tr>
</table>
<Div id="divLCPol1" style="display: ''">
<table class=common>
	<tr class=common>
		<td text-align: left colSpan=1><span id="spanPolGrid"> </span></td>
	</tr>
</table>
<INPUT VALUE="首页" class=common TYPE=button onclick=
	getFirstPage();;
>
<INPUT VALUE="上一页" class=common TYPE=button onclick=
	getPreviousPage();;
>
<INPUT VALUE="下一页" class=common TYPE=button onclick=
	getNextPage();;
>
<INPUT VALUE="尾页" class=common TYPE=button onclick=
	getLastPage();;
>
</div>
<table>
	<TR class=common>
		<TD class=title>日志信息</TD>
	<tr></tr>
	<TD class=input><textarea name="Remark" cols="100%" rows="5"
		witdh=100% class="common"></textarea></TD>
	</TR>
	<TR class=common>
		<TD class=title>算法公式</TD>
	<tr></tr>
	<TD class=input><textarea name="Remark1" cols="100%" rows="5"
		witdh=100% class="common"></textarea></TD>
	</TR>
	<TR class=common>
		<TD class=title>算法描述</TD>
	<tr></tr>
	<TD class=input><textarea name="Remark2" cols="100%" rows="5"
		witdh=100% class="common"></textarea></TD>
	</TR>
</table>
<table class=common>
	<TR class=common>

		<TD class=title>统计报表年</TD>
		<TD class=input><Input class=codeNo name=StatYear  verify="统计报表年|notnull" ondblclick="return showCodeList('startyear',[this,StatYearName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('startyear',[this,StatYearName],[0,1],null,null,null,1);"><input class=codename name=StatYearName readonly=true>
		</TD>
		<TD class=title>统计报表月(快报)</TD>
		<TD class=input><Input class=codeno name=StatMonth
			verify="统计报表月|notnull"
			CodeData="0|^01|一月|M^02|二月|M^03|三月|M^04|四月|M^05|五月|M^06|六月|M^07|七月|M^08|八月|M^09|九月|M^10|十月|M^11|十一月|M^12|十二月|M"
			ondblClick="showCodeListEx('ReportMonth',[this,StatMonthName],[0,1]);"
			onkeyup="showCodeListKeyEx('ReportMonth', [ this, StatMonthName ], [ 0, 1 ]);">
			<input class=codename name=StatMonthName></TD>
        <TD  class= title >
         	报表类型
       	</TD>
       	<TD  class= input>
         	<Input class= codeno name=RepType verify="报表类型|notnull" CodeData="0|^1|快报|M^2|月报|M^3|季报|M^4|半年报|M^5|年报|M^6|年度报|M" ondblClick="showCodeListEx('ReportType',[this,RepTypeName],[0,1]);" onkeyup="showCodeListKeyEx('ReportType',[this,RepTypeName],[0,1]);"><input class = codename name =RepTypeName>
       	</TD>
	</TR>
</table>
<INPUT VALUE="生成树" class=common TYPE=button onclick="buildTree();">
<table class = common>
<tr>
<td class = title>左边正</td>
<td class = title>左边负</td>
<td class = title>右边正</td>
<td class = title>右边负</td>
</tr>
<tr>
<td><span id="spanItemTreeLeftAdd" style="display: ''; position: absolute;"></span></td>
<td><span id="spanItemTreeLeftSub" style="display: ''; position: absolute;"></span></td>
<td><span id="spanItemTreeRightAdd" style="display: ''; position: absolute;"></span></td>
<td><span id="spanItemTreeRightSub" style="display: ''; position: absolute;"></span></td>
</tr>
</table>
</form>
<span id="spanCode" style="display: none; position: absolute;"></span>

</body>
</html>
