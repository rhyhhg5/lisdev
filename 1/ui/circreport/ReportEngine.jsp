<%
//程序名称: 一级汇总
//程序功能: 一级汇总提数前台界面  
//创建日期：2004-06-09
//创建人  ：  郭翔
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    String Branch =tG.ComCode;

      
%>


<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="ReportEngine.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ReportEngineInit.jsp"%>
 
  <title> </title>
</head>       
<body onload="initForm();">
<form method=post name=fm target="fraSubmit" action= "./ReportEngineChk.jsp">
  <table>
	<tr>
    	<td>
    		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLFDesbModeGrid);">
    	</td>
    	<td class= titleImg>
        	选择汇总的模块,输入提数条件
       	</td>   		 
    </tr>
  </table>
  
    <Div  id= "divLFDesbModeGrid" style= "display: ''">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanLFDesbModeGrid" >
					</span> 
			  	</td>
			</tr>
        </table>
    </Div>
    
      <Table class= common>
        <TR  class= common id=TrRepType>
           <TD  class= title >
            	报表类型
          	</TD>
          	<TD  class= input>
            	<Input class= codeno name=RepType1 verify="报表类型|notnull" CodeData="0|^1|快报|M^2|月报|M^3|季报|M^4|半年报|M^5|年报|M" ondblClick="showCodeListEx('ReportType',[this,RepType1Name],[0,1]);" onkeyup="showCodeListKeyEx('ReportType',[this,RepType1Name],[0,1]);"><input class=codename name=RepType1Name>
          	</TD>
          	<TD  class= title>
            	
          	</TD>
          	<TD  class= input>
            </TD>
     	</TR>

     	<TR  class= common>
        
          	<TD  class= title>
            	统计报表年
          	</TD>
          	<TD  class= input>
            	<Input class= common name=Year1 verify="统计报表年|notnull">
           	</TD>           
          	<TD  class= title>
            	统计报表月(快报)
          	</TD>
          	<TD  class= input>
            	<Input class= codeno name=Month1 verify="统计报表月|notnull" CodeData="0|^01|一月|M^02|二月|M^03|三月|M^04|四月|M^05|五月|M^06|六月|M^07|七月|M^08|八月|M^09|九月|M^10|十月|M^11|十一月|M^12|十二月|M" ondblClick="showCodeListEx('ReportMonth',[this,Month1Name],[0,1]);" onkeyup="showCodeListKeyEx('ReportMonth',[this,Month1Name],[0,1]);"><input class=codename name= Month1Name>
          	</TD>
          

    	</TR> 
     </Table>  
      <Table class= common>
        <TR class =common>    
          <TD class= common>
                
           <INPUT class=common  VALUE="汇总提数" TYPE=button onclick="calSumbit()">  
           <INPUT type="hidden" name="calType" value="">   
          </TD>
       </TR> 
      </Table> 
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"> </span> 

<form action="./LFCollXMLSave.jsp" method=post name=fm2 target="fraSubmit">
 <Div  id= "divfm2" style= "display: ''"> 
  <table>
	<tr>
    	<td>
    		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divReport);">
    	</td>
    	<td class= titleImg>
        	输入汇总和生成文件的条件
       	</td>   		 
    </tr>
  </table>
  <Div  id= "divReport" style= "display: ''">
	<table  class= common>
    	<TR  class= common>
          	<TD  class= title>
            	统计报表年
          	</TD>
          	<TD  class= input>
            	<Input class= common name=Year verify="统计报表年|notnull">
           	</TD>           
          	<TD  class= title>
            	统计报表月
          	</TD>
          	<TD  class= input>
            	<Input class= codeno name=Month verify="统计报表月|notnull" CodeData="0|^01|一月|M^02|二月|M^03|三月|M^04|四月|M^05|五月|M^06|六月|M^07|七月|M^08|八月|M^09|九月|M^10|十月|M^11|十一月|M^12|十二月|M" ondblClick="showCodeListEx('ReportMonth',[this,MonthName],[0,1]);" onkeyup="showCodeListKeyEx('ReportMonth',[this,MonthName],[0,1]);"><input class=codename name=MonthName>
          	</TD>
     	</TR>
     	<TR  class= common>
          	<TD  class= title>
            	报表类型
          	</TD>
          	<TD  class= input>
            	<Input class= codeno name=RepType verify="报表类型|notnull" CodeData="0|^1|快报|M^2|月报|M^3|季报|M^4|半年报|M^5|年报|M" ondblClick="showCodeListEx('ReportType',[this,RepTypeName],[0,1]);" onkeyup="showCodeListKeyEx('ReportType',[this,RepTypeName],[0,1]);"><input class=codename name=RepTypeName>
          	</TD>
          	<TD  class= title>

          	</TD>
          	<TD  class= input>

          	</TD>
    	</TR>
    	<TR class=input>              
	    	<TD class=common>
	        	<input type =button class=common value="数据汇总" onclick="CollData();">    
	        </TD>
        	<TD class=common>
          		<input type =button class=common value="生成文件" onclick="MakeFile();">    
        	</TD>
       	</TR>   
    </table>
   </Div>
  </Div>
  </form>

</body>
</html>
