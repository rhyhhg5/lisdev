<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CircMidQueryInit.jsp
//程序功能：保监会导入数据查询 
//创建日期：2002-06-19 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
<%
  String mDisType="";
  mDisType=request.getParameter("DisType");
  String mStatYear="";
  mStatYear=request.getParameter("StatYear");
  String mStatMon="";
  mStatMon=request.getParameter("StatMon");
 %>        
// 输入框的初始化（单记录部分）
function initInpBox(tDisType,tStatYear,tStatMon)
{ 

  try
  {                                   
	// 保单查询条件
	fm.all('DisType').value=tDisType;
	fm.all('StatYear').value=tStatYear;
	fm.all('StatMon').value=tStatMon;
   }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm(tDisType,tStatYear,tStatMon)
{
  try
  {
    initInpBox(tDisType,tStatYear,tStatMon);   
    initSelBox();    
    initPolGrid();
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="科目编码";         		//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="itemcode";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][9]="科目编码|code:itemcode&NOTNULL";
      iArray[1][18]=250;
      iArray[1][19]= 0 ;
      
      iArray[2]=new Array();
      iArray[2][0]="管理机构";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="comcode";              	        //是否引用代码:null||""为不引用
      iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[2][9]="机构编码|code:comcode&NOTNULL";
      iArray[2][18]=250;
      iArray[2][19]= 0 ;

      iArray[3]=new Array();
      iArray[3][0]="报表日期";         		//列名
      iArray[3][1]="0px";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="统计值";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="统计间隔";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][10] = "RepType";
      iArray[5][11] = "0|^1|快报^2|月报^3|季报^4|半年报^5|年报";
      iArray[5][12] = "5";
      iArray[5][19] = "0";

      iArray[6]=new Array();
      iArray[6][0]="统计年";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="统计月";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 3;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 0;
      PolGrid.canChk = 0;
      PolGrid.loadMulLine(iArray);  
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 保单状态列表的初始化
function initPolStatuGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="校验错误明细";         		//列名
      iArray[1][1]="400px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      PolStatuGrid = new MulLineEnter( "fm" , "PolStatuGrid" ); 
      //这些属性必须在loadMulLine前
      PolStatuGrid.mulLineCount = 3;   
      PolStatuGrid.displayTitle = 1;
      PolStatuGrid.locked = 1;
      PolStatuGrid.canSel = 0;
      PolStatuGrid.canChk = 0;
      PolStatuGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolStatuGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>