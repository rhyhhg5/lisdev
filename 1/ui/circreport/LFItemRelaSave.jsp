<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LFItemRelaSave.jsp
//程序功能：
//创建日期：2004-08-12 19:10:47
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LFItemRelaSchema tLFItemRelaSchema   = new LFItemRelaSchema();
  OLFItemRelaUI tOLFItemRelaUI   = new OLFItemRelaUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLFItemRelaSchema.setItemCode(request.getParameter("ItemCode"));
    tLFItemRelaSchema.setItemName(request.getParameter("ItemName"));
    tLFItemRelaSchema.setOutItemCode(request.getParameter("OutItemCode"));
    tLFItemRelaSchema.setUpItemCode(request.getParameter("UpItemCode"));
    tLFItemRelaSchema.setItemLevel(request.getParameter("ItemLevel"));
    tLFItemRelaSchema.setCorpType(request.getParameter("CorpType"));
    tLFItemRelaSchema.setItemType(request.getParameter("ItemType"));
    tLFItemRelaSchema.setIsQuick(request.getParameter("IsQuick"));
    tLFItemRelaSchema.setIsMon(request.getParameter("IsMon"));
    tLFItemRelaSchema.setIsQut(request.getParameter("IsQut"));
    tLFItemRelaSchema.setIsHalYer(request.getParameter("IsHalYer"));
    tLFItemRelaSchema.setIsYear(request.getParameter("IsYear"));
    tLFItemRelaSchema.setIsLeaf(request.getParameter("IsLeaf"));
    tLFItemRelaSchema.setGeneral(request.getParameter("General"));
    tLFItemRelaSchema.setLayer(request.getParameter("Layer"));
    tLFItemRelaSchema.setDescription(request.getParameter("Description"));
    tLFItemRelaSchema.setRemark(request.getParameter("Remark"));
    tLFItemRelaSchema.setOutputFlag(request.getParameter("OutputFlag"));
    tLFItemRelaSchema.setComFlag(request.getParameter("ComFlag"));
    tLFItemRelaSchema.setIsCalFlag(request.getParameter("IsCalFlag"));
    tLFItemRelaSchema.setIsYearAll(request.getParameter("IsYearAll"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLFItemRelaSchema);
  	tVData.add(tG);
    tOLFItemRelaUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLFItemRelaUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
