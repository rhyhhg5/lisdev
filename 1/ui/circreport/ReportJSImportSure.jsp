<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ReportJSImportSure.jsp
//程序功能：保监会报表精算数据确认
//创建日期：2004-07-08 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="ReportJSImportSure.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ReportJSImportSureInit.jsp"%>
  <title>精算数据确认</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./ReportJSImportSureChk.jsp">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
     	<TR  class= common>
        
          	<TD  class= title>
            	统计报表年
          	</TD>
          	<TD  class= input>
            	<Input class=codeNo name=StatYear  verify="统计报表年|notnull" ondblclick="return showCodeList('startyear',[this,StatYearName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('startyear',[this,StatYearName],[0,1],null,null,null,1);"><input class=codename name=StatYearName readonly=true>
            	</TD>           
          	<TD  class= title>
            	统计报表月(快报)
          	</TD>
          	<TD  class= input>
            	<Input class= codeno name=StatMonth verify="统计报表月|notnull" CodeData="0|^01|一月|M^02|二月|M^03|三月|M^04|四月|M^05|五月|M^06|六月|M^07|七月|M^08|八月|M^09|九月|M^10|十月|M^11|十一月|M^12|十二月|M" ondblClick="showCodeListEx('ReportMonth',[this,StatMonthName],[0,1]);" onkeyup="showCodeListKeyEx('ReportMonth',[this,StatMonthName],[0,1]);"><input class= codename name=StatMonthName>          
          	</TD>
    	</TR> 
    </table>
          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保监会报表信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
  	 <input type="hidden" name= "SubMissionID" value= "">
  	 <input type="hidden" name= "MissionID" value= "">
  	 <input type="hidden" name= "StatYearHide" value= "">
  	 <input type="hidden" name= "StatMonHide" value= "">
  	<p>
      <INPUT VALUE="精算数据确认" class= common TYPE=button onclick = "autochk();"> 
      <INPUT VALUE="精算数据查询" class= common TYPE=button onclick = "queryInfo();"> 
     <!--INPUT VALUE="发所有续保催收通知书" class= common TYPE=button onclick = "allautochk();"--> 
  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
