<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CircMidYWQueryInit.jsp
//程序功能：保监会导入数据查询 
//创建日期：2002-06-19 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
<%
  String mDisType="";
  mDisType=request.getParameter("DisType");
  String mStatYear="";
  mStatYear=request.getParameter("StatYear");
  String mStatMon="";
  mStatMon=request.getParameter("StatMon");
 %>        
// 输入框的初始化（单记录部分）
function initInpBox(tDisType,tStatYear,tStatMon)
{ 

  try
  {                                   
	// 保单查询条件
	fm.all('DisType').value=tDisType;
	fm.all('StatYear').value=tStatYear;
	fm.all('StatMon').value=tStatMon;
   }
  catch(ex)
  {
    alert("在CircMidYWQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  

}                                        

function initForm(tDisType,tStatYear,tStatMon)
{
  try
  {
    initInpBox(tDisType,tStatYear,tStatMon);   
    initSelBox();    
    if(tDisType=='YW1'){
       initPolGrid();
    }
    if(tDisType=='YW2'){
       initPolStatuGrid();
    }   
  }
  catch(re)
  {
    alert("在CircMidYWQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
  {                               
    var iArray = new Array();
      
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][9]="险种编码|code:RiskCode&NOTNULL";
      iArray[1][18]=250;
      iArray[1][19]= 0 ;
      
      iArray[2]=new Array();
      iArray[2][0]="管理机构";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="comcode";              	        //是否引用代码:null||""为不引用
      iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[2][9]="机构编码|code:comcode&NOTNULL";
      iArray[2][18]=250;
      iArray[2][19]= 0 ;

      iArray[3]=new Array();
      iArray[3][0]="缴费间隔";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][10] = "ReinsurItem";
      iArray[3][11] = "0|^-1 |不定期交^0|趸交^1 |月交^3|季交^6 |半年交^12|年交^3333|空值";
      iArray[3][12] = "5";
      iArray[3][19] = "0";

      iArray[4]=new Array();
      iArray[4][0]="销售渠道";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][10] = "SaleChnl";
      iArray[4][11] = "0|^01|个人代理^02|公司直销^03|保险专业代理^04|银行邮政代理^05|其他兼业代理^06|保险经纪业务";
      iArray[4][12] = "5";
      iArray[4][19] = "0";

      iArray[5]=new Array();
      iArray[5][0]="首期续期标志";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][10] = "ReinsurLocal";
      iArray[5][11] = "0|^1|首期^2|续期^Z|空值 ";
      iArray[5][12] = "5";
      iArray[5][19] = "0";
     
      iArray[6]=new Array();                                                            
      iArray[6][0]="团单个单标志";          //列名                               
      iArray[6][1]="0px";            		//列宽                                       
      iArray[6][2]=200;            			//列最大值                               
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许    
                                                                                           
      iArray[7]=new Array();
      iArray[7][0]="度量类型";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][10] = "ProtItem";
      iArray[7][11] = "0|^11|本年累计保费新增^12|期末有效保费新增|^21|本年累计保费新增^22|期末有效保额新增|^31|本年累计人次新增^32|期末有效人次新增 |^41|本年累计件次新增^42|期末有效件次新增";   
      iArray[7][12] = "5";
      iArray[7][19] = "0";
                       
      iArray[8]=new Array();
      iArray[8][0]="值";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      
      iArray[9]=new Array();
      iArray[9][0]="统计年";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="统计月";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   


      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 3;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 0;
      PolGrid.canChk = 0;
      PolGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initPolStatuGrid()
{                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      iArray[1][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[1][9]="险种编码|code:RiskCode&NOTNULL";
      iArray[1][18]=250;
      iArray[1][19]= 0 ;
      
      iArray[2]=new Array();
      iArray[2][0]="管理机构";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ManageCom";              	        //是否引用代码:null||""为不引用
      iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[2][9]="机构编码|code:ManageCom&NOTNULL";
      iArray[2][18]=250;
      iArray[2][19]= 0 ;

      iArray[3]=new Array();
      iArray[3][0]="缴费间隔";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][10] = "ReinsurItem";
      iArray[3][11] = "0|^-1 |不定期交^0|趸交^1 |月交^3|季交^6 |半年交^12|年交^3333|空值";
      iArray[3][12] = "5";
      iArray[3][19] = "0";

      iArray[4]=new Array();
      iArray[4][0]="销售渠道";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][10] = "SaleChnl";
      iArray[4][11] = "0|^01|个人代理^02|公司直销^03|保险专业代理^04|银行邮政代理^05|其他兼业代理^06|保险经纪业务";
      iArray[4][12] = "5";
      iArray[4][19] = "0";
      
      iArray[5]=new Array();
      iArray[5][0]="赔付类型标志";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][10] = "ProtItem";
      iArray[5][11] = "0|^1|伤残给付^2|医疗给付|^3|死亡给付^4|满期给付|^5|年金给付^6|退保";   
      iArray[5][12] = "5";
      iArray[5][19] = "0";
                       
      iArray[6]=new Array();
      iArray[6][0]="赔付金额";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      
      iArray[7]=new Array();
      iArray[7][0]="统计年";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="统计月";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许   

      PolStatuGrid = new MulLineEnter( "fm" , "PolStatuGrid" ); 
      //这些属性必须在loadMulLine前
      PolStatuGrid.mulLineCount = 3;   
      PolStatuGrid.displayTitle = 1;
      PolStatuGrid.locked = 1;
      PolStatuGrid.canSel = 0;
      PolStatuGrid.canChk = 0;
      PolStatuGrid.loadMulLine(iArray);  
 
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>