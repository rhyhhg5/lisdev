<%
//程序名称：LFComFinQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-09-14 15:49:07
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ComFin').value = "";
    fm.all('Name').value = "";
    fm.all('FoundDate').value = "";
    fm.all('EndFlag').value = "";
    fm.all('EndDate').value = "";
    fm.all('Attribute').value = "";
    fm.all('Sign').value = "";
    fm.all('calFlag').value = "";
    fm.all('Address').value = "";
    fm.all('ZipCode').value = "";
    fm.all('Phone').value = "";
    fm.all('Fax').value = "";
    fm.all('EMail').value = "";
    fm.all('WebAddress').value = "";
    fm.all('SatrapName').value = "";
    fm.all('ApproveDate').value = "";
    fm.all('PassDate').value = "";
  }
  catch(ex) {
    alert("在LFComFinQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLFComFinGrid();  
  }
  catch(re) {
    alert("LFComFinQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LFComFinGrid;
function initLFComFinGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="财务管理机构编码";         		//列名
    iArray[0+1][1]="100px";         		//列名
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="机构名称";         		//列名
    iArray[1+1][1]="100px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="成立日期";         		//列名
    iArray[2+1][1]="100px";         		//列名
 
    iArray[3+1]=new Array();
    iArray[3+1][0]="停业标志";         		//列名
    iArray[3+1][1]="100px";         		//列名
 
    iArray[4+1]=new Array();
    iArray[4+1][0]="停业日期";         		//列名
    iArray[4+1][1]="50px";         		//列名
 
    iArray[5+1]=new Array();
    iArray[5+1][0]="机构属性";         		//列名
    iArray[5+1][1]="50px";         		//列名
 
    iArray[6+1]=new Array();
    iArray[6+1][0]="机构标志";         		//列名
    iArray[6+1][1]="50px";         		//列名
 
    iArray[7+1]=new Array();
    iArray[7+1][0]="统计标志";         		//列名
    iArray[7+1][1]="50px";         		//列名
 
    iArray[8+1]=new Array();
    iArray[8+1][0]="机构地址";         		//列名
    iArray[8+1][1]="50px";         		//列名
 
    iArray[9+1]=new Array();
    iArray[9+1][0]="机构邮编";         		//列名
    iArray[9+1][1]="50px";         		//列名
 
    iArray[10+1]=new Array();
    iArray[10+1][0]="机构电话";         		//列名
    iArray[10+1][1]="50px";         		//列名
 
    iArray[11+1]=new Array();
    iArray[11+1][0]="机构传真";         		//列名
    iArray[11+1][1]="50px";         		//列名
 
    iArray[12+1]=new Array();
    iArray[12+1][0]="E-mail";         		//列名
    iArray[12+1][1]="50px";         		//列名
 
    iArray[13+1]=new Array();
    iArray[13+1][0]="网址";         		//列名
    iArray[13+1][1]="50px";         		//列名
 
    iArray[14+1]=new Array();
    iArray[14+1][0]="主管人姓名";         		//列名
    iArray[14+1][1]="50px";         		//列名
 
    iArray[15+1]=new Array();
    iArray[15+1][0]="总公司正式批示时间";         		//列名
    iArray[15+1][1]="50px";         		//列名
 
    iArray[16+1]=new Array();
    iArray[16+1][0]="工商批准营业时间";         		//列名
    iArray[16+1][1]="50px";         		//列名
 
    LFComFinGrid = new MulLineEnter( "fm" , "LFComFinGrid" ); 
 
 
 
    LFComFinGrid.canChk = 0; 
    LFComFinGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LFComFinGrid.mulLineCount = 0;   
    LFComFinGrid.displayTitle = 1;
    LFComFinGrid.hiddenPlus = 1;
    LFComFinGrid.hiddenSubtraction = 1;
    LFComFinGrid.canSel = 1;
    LFComFinGrid.canChk = 0;
    LFComFinGrid.selBoxEventFuncName = "showOne";
*/
    LFComFinGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LFComFinGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
