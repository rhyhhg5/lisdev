<%
//程序名称：LFComISCQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-08-12 19:10:44
//创建人  ：yangtao
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ComCodeISC').value = "";
    fm.all('Name').value = "";
    fm.all('ParentComCodeISC').value = "";
    fm.all('ComLevel').value = "";
    fm.all('IsLeaf').value = "";
    fm.all('OutputFlag').value = "";
  }
  catch(ex) {
    alert("在LFComISCQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLFComISCGrid();  
  }
  catch(re) {
    alert("LFComISCQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LFComISCGrid;
function initLFComISCGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="保监会机构编码";         		//列名
    iArray[0+1][1]="80px";         		//列名
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="机构名称";         		//列名
    iArray[1+1][1]="100px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="上级机构编码";         //列名
    iArray[2+1][1]="80px";         		//列名
 
    iArray[3+1]=new Array();
    iArray[3+1][0]="机构层次";         		//列名
    iArray[3+1][1]="100px";         		//列名
    iArray[3+1][3]  =2;  
    iArray[3+1][10] = "ItemLevel";  
    iArray[3+1][11] = "0|^0|总公司^1|分公司^2|地市公司";   
    
    
    iArray[4+1]=new Array();
    iArray[4+1][0]="是否是叶子结点";        //列名
    iArray[4+1][1]="80px";         		//列名  
    iArray[4+1][3]  =2;       
    iArray[4+1][10] = "IsLeaf";         
    iArray[4+1][11] = "0|^0|否^1|是";   
    iArray[5+1]=new Array();
    iArray[5+1][0]="是否上报标志";         	//列名
    iArray[5+1][1]="80px";         		//列名  
    iArray[5+1][3]  =2;                 
    iArray[5+1][10] = "OutputFlag";     
    iArray[5+1][11] = "0|^0|否^1|是";   

     
    LFComISCGrid = new MulLineEnter( "fm" , "LFComISCGrid" ); 
    LFComISCGrid.mulLineCount = 1; 
    LFComISCGrid.displayTitle = 1; 
    LFComISCGrid.locked = 1; 
 
    LFComISCGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LFComISCGrid.mulLineCount = 0;   
    LFComISCGrid.displayTitle = 1;
    LFComISCGrid.hiddenPlus = 1;
    LFComISCGrid.hiddenSubtraction = 1;
    LFComISCGrid.canSel = 1;
    LFComISCGrid.canChk = 0;
    LFComISCGrid.selBoxEventFuncName = "showOne";
*/
    LFComISCGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LFComISCGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
