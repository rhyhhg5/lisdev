<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2004-08-12 19:10:47
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>   
  <SCRIPT src="LFItemRelaInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LFItemRelaInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LFItemRelaSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 内外科目编码对应表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLFItemRela1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      内部科目编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ItemCode elementtype=nacessary>
    </TD>
    <TD  class= title>
      科目名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ItemName elementtype=nacessary>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      外部科目编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OutItemCode elementtype=nacessary>
    </TD>
    <TD  class= title>
      上级内部科目编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=UpItemCode elementtype=nacessary>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      科目级别
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=ItemLevel CodeData="0|^1|一级^2|二级^3|三级^4|四级^5|五级^6|六级^7|七级"  ondblClick="showCodeListEx('ItemLevel',[this,ItemLevelName],[0,1]);" onkeyup="showCodeListKeyEx('ItemLevel',[this,ItemLevelName],[0,1]);" ><input class=codename name= ItemLevelName elementtype=nacessary>
    </TD>                                               
    <TD  class= title>
      公司类型
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=CorpType CodeData="0|^1|产^2|寿^3|再^4|集"  ondblClick="showCodeListEx('CorpType',[this,CorpTypeName],[0,1]);" onkeyup="showCodeListKeyEx('CorpType',[this,CorpTypeName],[0,1]);" ><input class=codename name= CorpTypeName elementtype=nacessary>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      类型
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=ItemType CodeData="0|^01|资产^02|负债^03|权益^04|损益^05|现金流^06|统计^07|资金"  ondblClick="showCodeListEx('ItemType',[this,ItemTypeName],[0,1]);" onkeyup="showCodeListKeyEx('ItemType',[this,ItemTypeName],[0,1]);" ><input class=codename name= ItemTypeName elementtype=nacessary>
    </TD>
    <TD  class= title>
      是否是快报
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=IsQuick CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsQuick',[this,IsQuickName],[0,1]);" onkeyup="showCodeListKeyEx('IsQuick',[this,IsQuickName],[0,1]);" ><input class=codename name= IsQuickName elementtype=nacessary>
    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否是月报
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=IsMon CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsMon',[this,IsMonName],[0,1]);" onkeyup="showCodeListKeyEx('IsMon',[this,IsMonName],[0,1]);" ><input class=codename name= IsMonName elementtype=nacessary>
    
    </TD>
    <TD  class= title>
      是否是季报
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=IsQut CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsQut',[this,IsQutName],[0,1]);" onkeyup="showCodeListKeyEx('IsQut',[this,IsQutName],[0,1]);" ><input class=codename name= IsQutName elementtype=nacessary>
    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否是半年报
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=IsHalYer CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsHalYer',[this,IsHalYerName],[0,1]);" onkeyup="showCodeListKeyEx('IsHalYer',[this,IsHalYerName],[0,1]);" ><input class=codename name= IsHalYerName elementtype=nacessary>
    
    </TD>
    <TD  class= title>
      是否是年报
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=IsYear CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsYear',[this,IsYearName],[0,1]);" onkeyup="showCodeListKeyEx('IsYear',[this,IsYearName],[0,1]);" ><input class=codename name= IsYearName elementtype=nacessary>
    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      是否是叶子结点
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=IsLeaf CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsLeaf',[this,IsLeafName],[0,1]);" onkeyup="showCodeListKeyEx('IsLeaf',[this,IsLeafName],[0,1]);" ><input class=codename name= IsLeafName elementtype=nacessary>
    
    </TD>
    <TD  class= title>
      总分
    </TD>
    <TD  class= input>
      <Input class= 'common' name=General elementtype=nacessary>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      层级
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Layer >
    </TD>
    <TD  class= title>
      定义口径描述
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Description >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      备注
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Remark >
    </TD>
    <TD  class= title>
      是否上报标志
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=OutputFlag CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('OutputFlag',[this,OutputFlagName],[0,1]);" onkeyup="showCodeListKeyEx('OutputFlag',[this,OutputFlagName],[0,1]);" ><input class=codename name= OutputFlagName elementtype=nacessary>
    
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      上报数据的机构粒度
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=ComFlag CodeData="0|^1|据明细到总公司^2|数据明细到分公司^3|明细到中心支公司^4|明细到支公司"  ondblClick="showCodeListEx('ComFlag',[this,ComFlagName],[0,1]);" onkeyup="showCodeListKeyEx('ComFlag',[this,ComFlagName],[0,1]);"><input class=codename name= ComFlagName >
    
    </TD>
    <TD  class= title>
      是否一级计算标志
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=IsCalFlag CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsCalFlag ',[this,IsCalFlagName],[0,1]);" onkeyup="showCodeListKeyEx('IsCalFlag ',[this,,IsCalFlagName],[0,1]);" ><input class=codename name= IsCalFlagName elementtype=nacessary>
    
    </TD>
  </TR>
   <TR>
  <TD  class= title>
      是否是年度报
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=IsYearAll CodeData="0|^0|否^1|是"  ondblClick="showCodeListEx('IsYearAll',[this,IsYearAllName],[0,1]);" onkeyup="showCodeListKeyEx('IsYearAll',[this,IsYearAllName],[0,1]);" ><input class=codename name= IsYearAllName elementtype=nacessary>
    
    </TD>
   <td></td><td></td>
     </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
