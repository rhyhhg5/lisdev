<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-07-24 08:38:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="PayPlanInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PayPlanInit.jsp"%>
    
</head>
<body  onload="initForm();" >
  <form action="./PayPlanSave.jsp" method=post name=fm target="fraSubmit">
          <!-- 显示或隐藏PayPlan1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPayPlan1);">
      </td>
      <td class= titleImg>
        催付计划生成输入条件
      </td>
    	</tr>
    </table>

    <Div  id= "divPayPlan1" style= "display: ''">
      <table  class= common>
        <TR class=common>
        <TD class=title> 业务区站</TD>
        <TD  class= input>
            <Input class= code name=ManageCom value="" ondblclick="return showCodeList('station',[this]);" 
            onkeyup="return showCodeListKey('station',[this]);">

        </TD>
        <!--<TD class=title> 时间范围</TD>
        <td class="input" width="25%"><input class="coolDatePicker" dateFormat="short" id="timeStart" name="timeStart" verify="起始日期|NOTNULL&DATE" ></td>
        -->
        <TD class=title> 催付至日期</TD>
        <td class="input" width="25%"><input class="coolDatePicker" dateFormat="short" id="timeEnd" name="timeEnd" verify="终止日期|NOTNULL&DATE" ></td>
        </TR>
        <TR  class= common>
          <TD  class= title>
            集体保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpContNo >
          </TD>
         </TR>
        <TR  class= common>
          <TD  class= title>
            保单险种号码
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo >
          </TD>
          <TD  class= title>
            被保人客户号码
          </TD>
          <TD  class= input>
            <Input class= common name=InsuredNo >
          </TD>
        </TR>
        
          <TR>
          <TD>
             <INPUT VALUE="生成" class= cssbutton TYPE=button width=10% OnClick= "submitForm();">
          </TD>
          <TD>
          	<Div id = "divBTquery" style = "display :none">
             <INPUT VALUE="查询" TYPE=button width=10% OnClick="easyQueryClick();">
             </Div>
          </TD>
       </TR>
      </table>
    </Div>
      <!--生存领取表（列表） -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showCondPage();">
            </td>
            <td class= titleImg>
                     应付情况一览
            </td>
    	</tr>
     </table>
    <Div  id= "divLCInsured2" style= "display:none">
    <table  class= common>
            <tr  class= common>
                    <td text-align: left colSpan=1>
                                    <span id="spanGetGrid" >
                                    </span>
                            </td>
                    </tr>
            </table>
            <INPUT VALUE="首页" class=cssbutton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=cssbutton TYPE=button onclick="getLastPage();"> 
    </div>
    <table class = common>
    <tr class = common>
    
    	<td class = input width = 26%>
    		共生成&nbsp<Input class= readonly readonly name=getCount >&nbsp条记录。
    	</td>
     	<input type=hidden id="SerialNo" name="SerialNo">
     	 <Input type=hidden name=AppntNo >
     </table>
         
    <!-- 显示或隐藏LJSGet1的信息 -->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
