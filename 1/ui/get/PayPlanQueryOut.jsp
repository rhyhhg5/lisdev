<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：PayPlanQueryOut.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.get.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  // 保单信息部分
    LJSGetSchema tLJSGetSchema   = new LJSGetSchema();
    LCPolSchema tLCPolSchema=new LCPolSchema();
    LJSGetSet mLJSGetSet;
    tLCPolSchema.setPolNo(request.getParameter("PolNo"));
    System.out.println(tLCPolSchema.getPolNo());
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.addElement(tLCPolSchema);

  // 数据传输
   PayPlanQueryUI tPayPlanQueryUI  = new PayPlanQueryUI();
	if (!tPayPlanQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
     		 Content = " 查询失败，原因是: " + tPayPlanQueryUI.mErrors.getError(0).errorMessage;
     		 FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tPayPlanQueryUI.getResult();

		// 显示

		mLJSGetSet = new LJSGetSet();
		mLJSGetSet.set((LJSGetSet)tVData.getObjectByObjectName("LJSGetSet",0));
		int n = mLJSGetSet.size();
		for (int i = 1; i <= n; i++)
		{
		  	LJSGetSchema mLJSGetSchema = mLJSGetSet.get(i);
		  	%>
		   	<script language="javascript">
		   	  
		   	        parent.fraInterface.GetGrid.addOne("GetGrid")
		   	        parent.fraInterface.fm.GetGrid1[<%=i-1%>].value="<%=mLJSGetSchema.getGetNoticeNo()%>";
		   		parent.fraInterface.fm.GetGrid2[<%=i-1%>].value="<%=mLJSGetSchema.getAppntNo()%>";
		   		parent.fraInterface.fm.GetGrid3[<%=i-1%>].value="<%=mLJSGetSchema.getOtherNo()%>";
		   		parent.fraInterface.fm.GetGrid4[<%=i-1%>].value="<%=mLJSGetSchema.getApproveCode()%>";
		   		parent.fraInterface.fm.GetGrid5[<%=i-1%>].value="<%=mLJSGetSchema.getGetDate()%>";
		   		parent.fraInterface.fm.GetGrid6[<%=i-1%>].value="<%=mLJSGetSchema.getSumGetMoney()%>";
			</script>
			<%
		} // end of for
	} // end of if

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tPayPlanQueryUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

