//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var QueryResult="";
var QueryCount = 0;
var mulLineCount = 0;
var QueryWhere="";
window.onfocus=myonfocus;

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
	if (cShow=="true"){
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
	parent.fraMain.rows = "0,0,0,0,*";
}

//数据提交（保存）
function submitForm(){
	//alert("submitForm start");
	AscriptionRuleNewGrid.delBlankLine("AscriptionRuleGrid");
	//alert(AscriptionRuleNewGrid.mulLineCount);
	if(AscriptionRuleNewGrid.mulLineCount==0){
		alert("没有数据信息！");
		return false;
	}

	if(!beforeSubmit()){
		return false;
	}
    
    var tSql = "select count(*) from lcAscriptionrulefactory where grpcontno='"+GrpContNo+"'";
    var arr = easyExecSql(tSql);
    if(arr[0][0]>0){
    	alert("一个保单下只允许录入一个归属规则，如要修改归属规则，请点击归属规则修改按钮.");
    	return false;
    }
    
    var cSql="select count(*) from lcAscriptionrulefactory where grpcontno='"+GrpContNo+"' and ascriptionrulecode='"
    		+fm.AscriptionRuleCode.value+"'";
    		
    var result=easyExecSql(cSql);
    //alert(result);
    if(result>0){
    	alert("已经存在归属规则代码为"+fm.AscriptionRuleCode.value+"的归属规则.");
    	return false;
    }
	fm.all('mOperate').value = "INSERT||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./AscriptionRuleSave.jsp";
	fm.submit(); //提交
	initAscriptionRuleGrid();
}

//返回上一步
function returnparent(){
	//window.location.href = "GrpFeeInput.jsp?ProposalGrpContNo="+GrpContNo+"&LoadFlag="+LoadFlag;
	mSwitch = parent.VD.gVSwitch; 
    top.fraInterface.window.location = "../uliapp/ContPolInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+LoadFlag+"&scantype="+scantype+"&polNo="+mSwitch.getVar( "GrpContNo" )+"&oldContNo="+oldContNo;
}

//数据提交（删除）
function DelContClick(){
	AscriptionRuleNewGrid.delBlankLine("AscriptionRuleGrid");
//	alert(AscriptionRuleNewGrid.mulLineCount);
	if(AscriptionRuleNewGrid.mulLineCount==0){
		alert("没有数据信息！");
		return false;
	}

	fm.all('mOperate').value = "DELETE||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./AscriptionRuleSave.jsp";
	fm.submit(); //提交
	fm.all('AscriptionRuleCode').readOnly=false;	
}

//数据校验
function beforeSubmit(){
	if(fm.AscriptionRuleCode.value=="")
	{
	    alert("归属规则代码不能为空!");
	    return false;
	}
//chenwm20070924 检查是不是含有汉字,汉字录入的时候是一次性放到文本框里的.	
//	alert(chkzh(fm.AscriptionRuleCode.value));
	var AscriptionRuleCodeLength=2;
	if(chkzh(fm.AscriptionRuleCode.value)){
		AscriptionRuleCodeLength=1;
	}
	if(fm.AscriptionRuleCode.value.length>AscriptionRuleCodeLength){
	    alert("归属规则代码长度字母数字不能大于2,汉字不能大于1!");
	    return false;
	}
	var RiskCode="";
	var checkString=new Array(AscriptionRuleNewGrid.mulLineCount);
	for(var li=0;li<AscriptionRuleNewGrid.mulLineCount;li++){	
    	if(RiskCode==""){
    		RiskCode=AscriptionRuleNewGrid.getRowColData(li,1);
    	}
    	if(AscriptionRuleNewGrid.getRowColData(li,1)==""||AscriptionRuleNewGrid.getRowColData(li,2)==""||AscriptionRuleNewGrid.getRowColData(li,3)==""||AscriptionRuleNewGrid.getRowColData(li,4)==""||AscriptionRuleNewGrid.getRowColData(li,5)==""||AscriptionRuleNewGrid.getRowColData(li,6)=="")
        {
           alert("第'"+(li+1)+"'行，归属规则要素不可为空！");
           return false;
        }
		if(RiskCode!=AscriptionRuleNewGrid.getRowColData(li,1)){
    		alert("一个归属规则只能包含一个险种.");
    		return false;
    	}
        var Strings=AscriptionRuleNewGrid.getRowColData(li,6).split(",");
        if(Strings.length==3){
        	for(var m=0;m<Strings.length;m++){
			//alert(Strings[m]);
			if(!isNumeric(Strings[m])){
				alert("第'"+(li+1)+"'行，'要素值'有非法数字！");
				return false;
			}
		}
        }else{
             if(Strings.length==2){ 
               
			if(!isNumeric(Strings[1])){
			
				alert("第'"+(li+1)+"'行，'要素值'有非法数字！");
				return false;
      		}else{
           var tSql = "select 1 from lcgrpposition where grpcontno='"+GrpContNo+"' and Gradecode='"+Strings[0]+"'";
           var arr = easyExecSql(tSql);
            if(arr<1){
    		 alert("填入的级别不存在，请定义人员级别！");
    		 return false;
  			  }
      		
      		}
             }
                if(Strings.length==4){  
		 	for(var m=0;m<Strings.length;m++){
		 	if(m!=2){	
			//alert(Strings[m]);
			if(!isNumeric(Strings[m])){
				alert("第'"+(li+1)+"'行，'要素值'有非法数字！");
				return false;
			}
			  }else{
			var tSql = "select 1 from lcgrpposition where grpcontno='"+GrpContNo+"' and Gradecode='"+Strings[2]+"'";
            var arr = easyExecSql(tSql);
            if(arr<1){
    		 alert("填入的级别不存在，请定义人员级别！");
    		 return false;
  			  }
			  }
				
				}
             }      
        }
	
//		alert(Strings.length);
		if(Strings.length==3){
			if(parseInt(Strings[1])<=parseInt(Strings[0])){
				alert("第'"+(li+1)+"'行，'要素值'中截止年期必须大于起始年期！");
				return false;
			}
			for(var m=0;m<li;m++){
				if(checkString[m]!=null&&AscriptionRuleNewGrid.getRowColData(li,4)==AscriptionRuleNewGrid.getRowColData(m,4)
					&&AscriptionRuleNewGrid.getRowColData(li,3)==AscriptionRuleNewGrid.getRowColData(m,3)){
					//alert(m+' '+checkString[m][0]+' '+checkString[m][1]+' '+Strings[0]+' '+Strings[1]);
					if((parseInt(Strings[0])>=parseInt(checkString[m][0])&&parseInt(Strings[0])<parseInt(checkString[m][1]))   //起始值在某一个区间内
						||(parseInt(Strings[1])>parseInt(checkString[m][0])&&parseInt(Strings[1])<=parseInt(checkString[m][1]))   //截止值在某一个区间内
						||(parseInt(Strings[0])<=parseInt(checkString[m][0])&&parseInt(Strings[1])>=parseInt(checkString[m][1]))){  //某一个区间包括在这个起始截止值内
						alert("第'"+(li+1)+"'行与第'"+(m+1)+"'行，'要素值'中年期区间有重复！");
						return false;
					}
				}
			}
			checkString[m]=Strings;
//			alert("checkString="+checkString);
		}
        if(AscriptionRuleNewGrid.getRowColData(li,6).substring(AscriptionRuleNewGrid.getRowColData(li,6).lastIndexOf(",")+1)>1)
        {
           alert("第'"+(li+1)+"'行所录入的归属比例大于1,不能保存!");
           return false;
        }
        
    }
	return true;
}

function GrpPerPolDefine(){
	// 初始化表格
	//alert("GrpPerPolDefine 1");
	var str = "";
	var tGrpContNo = GrpContNo;

	var tImpAscriptionRuleCode = initAscriptionRuleNew(tGrpContNo);
	//alert(tImpAscriptionRuleCode);
	//初始化数组
	initAscriptionRuleNewGrid(tImpAscriptionRuleCode);
       // alert("GrpPerPolDefine 3");
	divAscriptionRule.style.display= "";
	var strSQL = "";

	strSQL = "select GrpContNo,ProposalGrpContNo,ManageCom,AppntNo,GrpName from LCGrpCont "
	       +" where GrpGroupNo IS NULL and GrpContNo = '" +GrpContNo+ "'"
	       +" union"
	       +" select GrpContNo,ProposalGrpContNo,ManageCom,AppntNo,GrpName from LBGrpCont "
	       +" where GrpGroupNo IS NULL and GrpContNo = '" +GrpContNo+ "'"
	       ;
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.all('GrpContNo').value = turnPage.arrDataCacheSet[0][0];
	fm.all('ProposalGrpContNo').value = turnPage.arrDataCacheSet[0][1];
	fm.all('ManageCom').value = turnPage.arrDataCacheSet[0][2];
	fm.all('AppntNo').value = turnPage.arrDataCacheSet[0][3];
	fm.all('GrpName').value = turnPage.arrDataCacheSet[0][4];
	return ;
}

function GrpPerPolDefineOld(){
	// 初始化表格
	var str = "";
	var tGrpContNo = GrpContNo;
	var tImpAscriptionRuleOldCode = initAscriptionRuleOld(tGrpContNo);
	//alert("is me");
	initAscriptionRuleOldGrid(tImpAscriptionRuleOldCode);
	//alert("not here");

	divAscriptionRuleOld.style.display= "";
	var strSQL = "";


	strSQL = "select distinct Ascriptionrulecode,Ascriptionrulename from lcAscriptionrulefactory a "
	       +" where a.GrpContNo='"+tGrpContNo+"'" 
	       +" union"
	       +" select distinct Ascriptionrulecode,Ascriptionrulename from lbAscriptionrulefactory a "
	       +" where a.GrpContNo='"+tGrpContNo+"'" 
	       ;
	       //alert(strSQL);
	turnPage = new turnPageClass();
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if (turnPage.strQueryResult) {
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		turnPage.pageDisplayGrid = AscriptionRuleOldGrid;
		turnPage.strQuerySql     = strSQL;
		turnPage.pageIndex = 0;
		arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
		displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	}
        return ;
}


function initAscriptionRuleNew(tGrpContNo){
	// 书写SQL语句
	var k=0;
	var strSQL = "";

	//strSQL = "select distinct a.FactoryType,b.FactoryTypeName,a.FactoryType||"+tGrpContNo+" from LMFactoryMode a ,LMFactoryType b  where 1=1 "
	//+ " and a.FactoryType= b.FactoryType "
	//+ " and (RiskCode =('"+tGrpContNo+"' ) or RiskCode ='000000' )";
	
	//alert(GrpContNo);
	strSQL = "select a.RiskCode,b.RiskName,a.GrpPolNo,'' from LCGrpPol a,LMRisk b where "
	       +" a.GrpContNo='"+GrpContNo+"' and a.RiskCode = b.RiskCode"
	       +" union"
	       +" select a.RiskCode,b.RiskName,a.GrpPolNo,'' from LBGrpPol a,LMRisk b where "
	       +" a.GrpContNo='"+GrpContNo+"' and a.RiskCode = b.RiskCode"
	       ;
	//fm.all('ff').value=strSQL;
	var str  = easyQueryVer3(strSQL, 1, 0, 1);
	//alert(str);
    return str;
}
function initAscriptionRuleOld(tGrpContNo){
	// 书写SQL语句
	var k=0;
	var strSQL = "";

	strSQL = "select Ascriptionrulecode,Ascriptionrulename from lcAscriptionrulefactory "
	       +" where lcAscriptionrulefactory.GrpContNo='"+tGrpContNo+"'"
	       +" union"
	       +" select Ascriptionrulecode,Ascriptionrulename from lBAscriptionrulefactory "
	       +" where lBAscriptionrulefactory.GrpContNo='tGrpContNo'"
	       ;
	var str  = easyQueryVer3(strSQL, 1, 0, 1);
	//alert(str);
    return str;
}
//单选框点击触发事件
function ShowAscriptionRule(parm1,parm2){
		//当前行第1列的值设为：选中
		//alert("not me");
		var tGrpContNo= GrpContNo;
		//alert(GrpContNo);
		var cAscriptionRuleCode = fm.all(parm1).all('AscriptionRuleOldGrid1').value;	//计划编码
		fm.all('AscriptionRuleCode').value = cAscriptionRuleCode;
		fm.all('AscriptionRuleName').value = fm.all(parm1).all('AscriptionRuleOldGrid2').value;
		fm.all('sumbitButton').disabled=true;
		fm.all('AscriptionRuleCode').readOnly=true;
		//alert("cAscriptionRuleCode");
               var strSQL="";
        /*       
               strSQL="select AscriptionRulecode,AscriptionRulename,Calremark,params from lcAscriptionrulefactory where Ascriptionrulecode='"+cAscriptionRuleCode+"' and lcAscriptionrulefactory.GrpContNo='"+tGrpContNo+"'";
      
            //   document.write(strSQL);
               	turnPage = new turnPageClass();
	        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//alert("not me");

	//判断是否查询成功
	if (turnPage.strQueryResult) {
	         // alert("result is right");
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象
		turnPage.pageDisplayGrid = AscriptionRuleGrid;
		//保存SQL语句
		turnPage.strQuerySql     = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
		//调用MULTILINE对象显示查询结果
		displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
		QueryCount = 1;
		tSearch = 1;
	*/
	strSQL="select riskcode,factorytype,otherno,factorycode||to_char(factorysubcode),calremark,params,factoryname,"
	      +" trim(FactoryType)||trim(RiskCode),grppolno from lcascriptionrulefactory "
		    +" where grpcontno='"+fm.all('GrpContNo').value+"' "
		    +" and ascriptionrulecode='"+cAscriptionRuleCode+"'"
		    +" union"
		    +" select riskcode,factorytype,otherno,factorycode||to_char(factorysubcode),calremark,params,factoryname,"
	        +" trim(FactoryType)||trim(RiskCode),grppolno from lBascriptionrulefactory "
		    +" where grpcontno='"+fm.all('GrpContNo').value+"' "
		    +" and ascriptionrulecode='"+cAscriptionRuleCode+"'"
		    ;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if (turnPage.strQueryResult) {
	         // alert("result is right");
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象
		turnPage.pageDisplayGrid = AscriptionRuleNewGrid;
		//保存SQL语句
		turnPage.strQuerySql     = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
		//调用MULTILINE对象显示查询结果
		displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
		QueryCount = 1;
		tSearch = 1;
	}
}
function afterSubmit(FlagStr,content){
	showInfo.close();
	if( FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{
		content = "操作成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
	fm.all('sumbitButton').disabled=false;
}

function updateClick(){
//	AscriptionRuleNewGrid.delBlankLine("AscriptionRuleGrid");
	if(AscriptionRuleNewGrid.mulLineCount==0){
		alert("没有数据信息！");
		return false;
	}
	var cSql="select count(*) from lcAscriptionrulefactory where grpcontno='"+GrpContNo+"' and ascriptionrulecode='"
    		+fm.AscriptionRuleCode.value+"'";
    var result=easyExecSql(cSql);
    if(result == "0" || result == null){
    	alert("不存在归属规则代码为"+fm.AscriptionRuleCode.value+"的归属规则，不可以进行修改操作。");
    	return false;
    }
	
	
	if(!beforeSubmit()){
		return false;
	}

	fm.all('mOperate').value = "UPDATE||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./AscriptionRuleSave.jsp";
	fm.submit(); //提交
	initAscriptionRuleGrid();
	fm.all('AscriptionRuleCode').readOnly=false;
}
function reload(){
	initForm();
	fm.all('sumbitButton').disabled=false;
	fm.all('AscriptionRuleCode').readOnly=false;
	fm.all('AscriptionRuleCode').value="";
	fm.all('AscriptionRuleName').value="";
}
//add by cjg
function addClick(){

var row=0;	
  row = AscriptionRuleGradeGrid.mulLineCount-1;	
  AscriptionRuleGradeGrid.setRowColData(row,1,''+(parseInt(row)+1));

}
//add by cjg
function Submit()
{  

  var row=0;	
  row = AscriptionRuleGradeGrid.mulLineCount-1;	


   	var tSql = "select distinct position from lcinsured where grpcontno='"+GrpContNo+"' and position is not null";
            var Strings = easyExecSql(tSql); 
       if(Strings!=null){
        	for(var m=0;m<Strings.length;m++)
        	{
        	   var flat='';
        	   for(var li=0;li<AscriptionRuleGradeGrid.mulLineCount;li++) 
        	   {  
	        	    if(AscriptionRuleGradeGrid.getRowColData(li,1)==Strings[m])
	        	    { 
	        	     flat='true'; 
	  	             }else{
	  	              flat='false'; 
	  	             }   
	  	            if(flat=='true')
	  	              {
	  	              break;
	  	              }   
		     }
	         if(flat=='false')
	          {
	          alert("该团单级别代码为"+Strings[m]+"下已存在被保险人,不能删除");
	          return false;
	          }
		}
     }
	for(var li=0;li<AscriptionRuleGradeGrid.mulLineCount;li++)
	{
	    if((AscriptionRuleGradeGrid.getRowColData(li,1))=='')
	    {
	       alert("第"+(li+1)+"行级别代码不能为空!");
	       return false;
	    }
	    
	    	if(!isNumeric(AscriptionRuleGradeGrid.getRowColData(li,1))){
				alert("第'"+(li+1)+"'行，'级别代码'有非法数字！");
				return false;
			}
			
			if(isNumeric(AscriptionRuleGradeGrid.getRowColData(li,1))){
				   if(AscriptionRuleGradeGrid.getRowColData(li,1)>20||AscriptionRuleGradeGrid.getRowColData(li,1)<1){
				    alert("第'"+(li+1)+"'行，'级别代码'输入有误，请选择1-20范围内的级别代码！");
				    return false;
				   }
				
			}
	  
		if((AscriptionRuleGradeGrid.getRowColData(li,2))=='')
	    {
	       alert("第"+(li+1)+"行级别名称不能为空!");
	       return false;
	    }else{
	   for(var m=0;m<li;m++){   
	   if(AscriptionRuleGradeGrid.getRowColData(li,1)==AscriptionRuleGradeGrid.getRowColData(m,1)){
	               alert('级别代码不能相同');   
	              return false;
	  		 }
			}
	   } 
	}
		var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	    fm.action="../uliapp/AscriptionPositionSave.jsp"  
        fm.submit(); //提交
		
}

function GrpAscriptionRuleGradeGrid(){
    var str = "";
	var tGrpContNo = GrpContNo;
	//alert("is me");
     initAscriptionRuleGradeGrid();
	var strSQL = "";
	strSQL = "select distinct Gradecode,Gradename from lcgrpposition a "
	       +" where a.GrpContNo='"+tGrpContNo+"' order by Gradecode ";
	       //alert(strSQL);
	turnPage = new turnPageClass();
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if (turnPage.strQueryResult) {
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		turnPage.pageDisplayGrid = AscriptionRuleGradeGrid;
		turnPage.strQuerySql     = strSQL;
		turnPage.pageIndex = 0;
		arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
		displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	}
        return ;

}

function returnparent(){
	//window.location.href = "GrpFeeInput.jsp?ProposalGrpContNo="+GrpContNo+"&LoadFlag="+LoadFlag;
	mSwitch = parent.VD.gVSwitch; 
    top.fraInterface.window.location = "../uliapp/ContPolInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+LoadFlag+"&scantype="+scantype+"&polNo="+mSwitch.getVar( "GrpContNo" )+"&oldContNo="+oldContNo;
}
