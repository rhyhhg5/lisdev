<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：GroupWrapSave.jsp
//程序功能：
//创建日期：2006-9-19 10:52
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.ulitb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//接收信息，并作校验处理。
//输入参数

LCRiskWrapSet tLCRiskWrapSet = new LCRiskWrapSet();
TransferData tTransferData = new TransferData();
TBGrpWrapUI tTBGrpWrapUI   = new TBGrpWrapUI();
//输出参数
String FlagStr = "";
String Content = "";
String mLoadFlag = "";

String RiskCode = "";
String GrpPolNo = "";
GlobalInput tGI = new GlobalInput(); //repair:
tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
System.out.println("tGI"+tGI);
if(tGI==null)
{
  System.out.println("页面失效,请重新登陆");
  FlagStr = "Fail";
  Content = "页面失效,请重新登陆";
}
else //页面有效
{
  String Operator  = tGI.Operator ;  //保存登陆管理员账号
  String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码

  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  String fmAction=request.getParameter("fmAction");
  System.out.println("fmAction:"+fmAction);

  /*
    String tLimit="";
    String CustomerNo="";
  */
  if(fmAction.equals("INSERT||GROUPRISK")){
  	tTransferData.setNameAndValue("GrpContNo",request.getParameter("GrpContNo"));
		tTransferData.setNameAndValue("WrapCode",request.getParameter("RiskCode"));
  }
  if(fmAction.equals("DELETE||GROUPRISK")){
    String tRiskNum[] = request.getParameterValues("RiskGridNo");
    String tRiskCode[] = request.getParameterValues("RiskGrid1");            //险种编码
    String tChk[] = request.getParameterValues("InpRiskGridChk");
    System.out.println("tChk.length"+tChk.length);
    tTransferData.setNameAndValue("GrpContNo",request.getParameter("GrpContNo"));
    for(int index=0;index<tChk.length;index++){
      if(tChk[index].equals("1")){
        LCRiskWrapSchema tLCRiskWrapSchema = new LCRiskWrapSchema();
        tLCRiskWrapSchema.setRiskWrapCode(tRiskCode[index]);
				tLCRiskWrapSet.add(tLCRiskWrapSchema);
      }
    }
  }
  try
  {
    // 准备传输数据 VData
    VData tVData = new VData();

    tVData.add(tLCRiskWrapSet);
    tVData.add(tTransferData);
    tVData.add(tGI);



    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		tTBGrpWrapUI.submitData(tVData,fmAction);

  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    System.out.println("aaaa"+ex.toString());
    FlagStr = "Fail";
  }


  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tTBGrpWrapUI.mErrors;
    if (!tError.needDealError())
    {
      Content ="保存成功！";
      FlagStr = "Succ";
    }
    else
    {
      Content = "保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

}//页面有效区
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=RiskCode%>","<%=GrpPolNo%>");
</script>
</html>

