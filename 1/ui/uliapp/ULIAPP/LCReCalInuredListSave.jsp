<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LCReCalInuredListSave.jsp
//程序功能：
//创建日期：2006-02-21 17:39:01
//创建人  ：zhangxing程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.ulitb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
 
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = ""; 
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
    ReCalInsuredPremBL tReCalInsuredPremBL = new ReCalInsuredPremBL();
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("GrpContNo",request.getParameter("GrpContNo"));
    tTransferData.setNameAndValue("RelatePremDelete","1");
    VData tVData = new VData();
		
    tVData.add(tTransferData);
		tVData.add(tG);			
			
			try
		  {
				tReCalInsuredPremBL.submitData(tVData,"");
			}
		  catch(Exception ex)
		  {
		  	ex.printStackTrace();
		    Content = "保存失败，原因是:" + tReCalInsuredPremBL.mErrors.getFirstError();
		    FlagStr = "Fail";
		  }
		
		    tError = tReCalInsuredPremBL.mErrors;
		    if (!tError.needDealError())
		    {
		    	Content = " 保存成功! ";
		    	FlagStr = "Success";
		    }
		    else                                                                           
		    {
		    	Content = " 保存失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
 
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
