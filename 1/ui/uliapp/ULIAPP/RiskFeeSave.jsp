<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpFeeSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人 ：CrtHtml程序创建
//更新记录： 更新人  更新日期   更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.ulitb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.ExeSQL"%>
<%@page import="com.sinosoft.utility.SSRS"%>
<%
//接收信息，并作校验处理。
//输入参数
LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
LCGrpFeeParamSchema tLCGrpFeeParamSchema = new LCGrpFeeParamSchema();
//LCGrpPolSpecSet tLCGrpPolSpecSet = new LCGrpPolSpecSet();
LCGrpFeeParamSet tLCGrpFeeParamSet = new LCGrpFeeParamSet();
GrpFeeUI tGrpFeeUI = new GrpFeeUI();
TransferData tTransferData = new TransferData();
LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
LMRiskFeeSet tLMRiskFeeSet = new LMRiskFeeSet();
ExeSQL mExeSQL=new ExeSQL();
//输出参数
CErrors tError = null;
String tOperate=request.getParameter("mOperate");
String tRearStr = "";
String tRela = "";
String FlagStr = "Fail";
String Content = "";

//全局变量
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

System.out.println("begin ...");

String GrpPolNo = request.getParameter("GrpPolNo");	//集体保单险种号码
String tRiskCode = request.getParameter("RiskCode");//险种代码
tTransferData.setNameAndValue("AddData",request.getParameter("AddData"));
String tC4Flag = request.getParameter("C4Flag");	//C4投资标志
String tMinGetIntv = request.getParameter("MinGetIntv");	//最低起领年限
String tLimitNum = request.getParameter("LimitNum");	//产品/投资转换次数
String tRiskFeeMode = request.getParameter("RiskFeeMode");	//管理费收取方式
//未归属帐户投资标志
//if(tC4Flag!=null&&!"".equals(StrTool.cTrim(tC4Flag))){
//LCGrpPolSpecSchema tLCGrpPolSpecSchema = new LCGrpPolSpecSchema();
//tLCGrpPolSpecSchema.setGrpPolNo(GrpPolNo);
//tLCGrpPolSpecSchema.setGrpPolSpecCode("C4investflag");
//tLCGrpPolSpecSchema.setRiskProp1(tC4Flag);
//tLCGrpPolSpecSet.add(tLCGrpPolSpecSchema);
//}
//领取信息
/**
if(tMinGetIntv!=null&&!"".equals(StrTool.cTrim(tMinGetIntv))){
LCGrpPolSpecSchema tLCGrpPolSpecSchema = new LCGrpPolSpecSchema();
tLCGrpPolSpecSchema.setGrpPolNo(GrpPolNo);
tLCGrpPolSpecSchema.setGrpPolSpecCode("TQRules");
tLCGrpPolSpecSchema.setRiskProp1(tMinGetIntv);
tLCGrpPolSpecSchema.setRiskProp2("Y");
tLCGrpPolSpecSchema.setRiskProp3(request.getParameter("GetIntvF"));
tLCGrpPolSpecSchema.setRiskProp4("Y");
tLCGrpPolSpecSchema.setRiskProp5(request.getParameter("GetIntvWT"));
tLCGrpPolSpecSchema.setRiskProp6("D");
tLCGrpPolSpecSchema.setRiskProp7(request.getParameter("TotalGetTime"));
tLCGrpPolSpecSet.add(tLCGrpPolSpecSchema);
}
//转换信息
if(tLimitNum!=null&&!"".equals(StrTool.cTrim(tLimitNum))){
	LCGrpPolSpecSchema tLCGrpPolSpecSchema = new LCGrpPolSpecSchema();
	tLCGrpPolSpecSchema.setGrpPolNo(GrpPolNo);
	tLCGrpPolSpecSchema.setGrpPolSpecCode("ZHLimit");
	tLCGrpPolSpecSchema.setRiskProp1("1");  //1次数
	tLCGrpPolSpecSchema.setRiskProp2(request.getParameter("LimitNum")); //产品/投资转换次数
	tLCGrpPolSpecSchema.setRiskProp3("1"); //1扣除费用
	tLCGrpPolSpecSchema.setRiskProp4(request.getParameter("LimitFee")); //超转换次数扣除费用(元)
	tLCGrpPolSpecSet.add(tLCGrpPolSpecSchema);
}
**/

//管理费主表信息
int lineCount = 0;
String arrCount[] = request.getParameterValues("RiskFeeGridNo");
if (arrCount != null){
//	String tRadio[] = request.getParameterValues("InpRiskFeeGridSel");	//Radio选项

	String tInsuAccNo[] = request.getParameterValues("RiskFeeGrid1");	//保险帐户号码
	String tPayPlanCode[] = request.getParameterValues("RiskFeeGrid3");	//交费项编码
	String tFeeCode[] = request.getParameterValues("RiskFeeGrid5");	//管理费编码
	String tPayInsuAccName[] = request.getParameterValues("RiskFeeGrid7");	//关系说明
	String tFeeCalMode[] = request.getParameterValues("RiskFeeGrid8");	//管理费计算方式
	String tFeeCalCode[] = request.getParameterValues("RiskFeeGrid9");	//管理费计算公式
	String tFeeValue[] = request.getParameterValues("RiskFeeGrid10");	//固定值
	String tCompareValue[] = request.getParameterValues("RiskFeeGrid11");	//比较值
	String tFeeCalModeType[] = request.getParameterValues("RiskFeeGrid12");	//计算类型
	String tFeePeriod[] = request.getParameterValues("RiskFeeGrid13");	//扣除管理费周期
	String tMaxTime[] = request.getParameterValues("RiskFeeGrid14");	//扣除管理费最大次数
	String tDefaultFlag[] = request.getParameterValues("RiskFeeGrid15");	//缺省标记
	
	String tChk[] = request.getParameterValues("InpRiskFeeGridChk");	//Chkbox选项
	if (tChk!=null&&tChk.length>0)
	{
		  for(int i=0;i<tChk.length;i++)
		  {
			  if(tChk[i].equals("1")) 
		      {  
 		//add  by  liyane
		if(tPayPlanCode[i].equals("111111"))  //tInsuAccNo,tFeeCode 为空
		{
			String strSQL="";

			String tFcode = " and b.FeeCode not in (select FeeCode from LCGrpFee where GrpPolNo = '"+GrpPolNo+"')";
  			String tFcode2 = " and b.FeeCode not in (select FeeCode from LBGrpFee where GrpPolNo = '"+GrpPolNo+"')";
 
          strSQL = " select  b.PayPlanCode from LCGrpPol a, LMRiskFee b, LMRiskToAcc d "
         +" where a.RiskCode = d.RiskCode  and b.InsuAccNo = d.InsuAccNo and a.GrpPolNo = '"+GrpPolNo+"' "
         +" and   b.payplancode in (select payplancode from lmdutypayrela where dutycode in  "
         +" (select dutycode from lmriskduty where riskcode = '"+tRiskCode+"')) "+tFcode
         +" union"
         +" select b.PayPlanCode from LBGrpPol a, LMRiskFee b, LMRiskToAcc d "
         +" where a.RiskCode = d.RiskCode  and b.InsuAccNo = d.InsuAccNo and a.GrpPolNo = '"+GrpPolNo+"' "
         +" and b.FeeCode not in (select FeeCode from LCGrpFee where GrpPolNo = '"+GrpPolNo+"') "
         +" and b.payplancode in (select payplancode from lmdutypayrela where dutycode in "
         +" (select dutycode from lmriskduty where riskcode = '"+tRiskCode+"')) "+tFcode2
         +" union "
         +" select b.PayPlanCode"
         +" from LCGrpFee a,LMRiskFee b,LMRiskToAcc d where a.RiskCode = d.RiskCode  "
         +"  and b.InsuAccNo = d.InsuAccNo and a.FeeCode = b.FeeCode "
         +"   and a.InsuAccNo =b.InsuAccNo and a.PayPlanCode = b.PayPlanCode "
         +" and a.GrpPolNO = '"+GrpPolNo+"' and b.payplancode in (select payplancode from lmdutypayrela "
         +" where dutycode in (select dutycode from lmriskduty where riskcode = '"+tRiskCode+"'))"
         + " union "
         +" select b.PayPlanCode "
         +" from LBGrpFee a,LMRiskFee b,LMRiskToAcc d where a.RiskCode = d.RiskCode  "
         +" and b.InsuAccNo = d.InsuAccNo and a.FeeCode = b.FeeCode "
         +" and a.InsuAccNo = b.InsuAccNo and a.PayPlanCode = b.PayPlanCode "
         +" and a.GrpPolNO = '"+GrpPolNo+"' and b.payplancode in (select payplancode from lmdutypayrela "
         +" where dutycode in (select dutycode from lmriskduty where riskcode = '"+tRiskCode+"'))";
	
	   	 SSRS sSRS = new SSRS();
	   	sSRS=mExeSQL.execSQL(strSQL);
  		if(sSRS!=null&&sSRS.getMaxRow()>0){
	   		for(int m=1;m<=sSRS.getMaxRow();m++){
	  		 tLMRiskFeeDB=new LMRiskFeeDB();
			tLMRiskFeeSet=new LMRiskFeeSet();
			tLMRiskFeeDB.setRiskCode(tRiskCode);
			tLMRiskFeeDB.setPayPlanCode(sSRS.GetText(m, 1));
			//System.out.println("PayPlanCode"+sSRS.GetText(m, 1));
			tLMRiskFeeSet=tLMRiskFeeDB.query();
			for(int n=0;n<tLMRiskFeeSet.size();n++){
				tLCGrpFeeSchema = new LCGrpFeeSchema();
				tLCGrpFeeSchema.setGrpPolNo(GrpPolNo);
				tLCGrpFeeSchema.setInsuAccNo(tLMRiskFeeSet.get(n+1).getInsuAccNo());
				tLCGrpFeeSchema.setPayPlanCode(sSRS.GetText(m, 1));
				tLCGrpFeeSchema.setFeeCode(tLMRiskFeeSet.get(n+1).getFeeCode());
				tLCGrpFeeSchema.setPayInsuAccName(tPayInsuAccName[i]);
				tLCGrpFeeSchema.setFeeCalMode(tFeeCalMode[i]);
				tLCGrpFeeSchema.setFeeCalCode(tFeeCalCode[i]);
				tLCGrpFeeSchema.setFeeValue(tFeeValue[i]);
				tLCGrpFeeSchema.setCompareValue(tCompareValue[i]);
				tLCGrpFeeSchema.setFeeCalModeType(tFeeCalModeType[i]);
				tLCGrpFeeSchema.setFeePeriod(tFeePeriod[i]);
				tLCGrpFeeSchema.setMaxTime(tMaxTime[i]);
				tLCGrpFeeSchema.setDefaultFlag(tDefaultFlag[i]);
				tLCGrpFeeSet.add(tLCGrpFeeSchema);
			}

	   }
	 }  

	}
	else{   //tInsuAccNo,tFeeCode 不为空
	//end

			tLMRiskFeeDB=new LMRiskFeeDB();
			tLMRiskFeeSet=new LMRiskFeeSet();
			tLMRiskFeeDB.setRiskCode(tRiskCode);
			tLMRiskFeeDB.setPayPlanCode(tPayPlanCode[i]);
			tLMRiskFeeDB.setInsuAccNo(tInsuAccNo[i]);
			tLMRiskFeeDB.setFeeCode(tFeeCode[i]);
			tLMRiskFeeSet=tLMRiskFeeDB.query();
				for(int n=0;n<tLMRiskFeeSet.size();n++){
				tLCGrpFeeSchema = new LCGrpFeeSchema();
				tLCGrpFeeSchema.setGrpPolNo(GrpPolNo);
				tLCGrpFeeSchema.setInsuAccNo(tLMRiskFeeSet.get(n+1).getInsuAccNo());
				tLCGrpFeeSchema.setPayPlanCode(tPayPlanCode[i]);
				tLCGrpFeeSchema.setFeeCode(tLMRiskFeeSet.get(n+1).getFeeCode());
				tLCGrpFeeSchema.setPayInsuAccName(tPayInsuAccName[i]);
				tLCGrpFeeSchema.setFeeCalMode(tFeeCalMode[i]);
				tLCGrpFeeSchema.setFeeCalCode(tFeeCalCode[i]);
				tLCGrpFeeSchema.setFeeValue(tFeeValue[i]);
				tLCGrpFeeSchema.setCompareValue(tCompareValue[i]);
				tLCGrpFeeSchema.setFeeCalModeType(tFeeCalModeType[i]);
				tLCGrpFeeSchema.setFeePeriod(tFeePeriod[i]);
				tLCGrpFeeSchema.setMaxTime(tMaxTime[i]);
				tLCGrpFeeSchema.setDefaultFlag(tDefaultFlag[i]);
				tLCGrpFeeSchema.setRiskFeeMode(tRiskFeeMode);
				tLCGrpFeeSet.add(tLCGrpFeeSchema);
				}
			System.out.println("当前行被选中"+tPayPlanCode[i]);
			System.out.println("$$$$$$$$"+tLCGrpFeeSchema.getFeeValue());
			
			}
			
		}
	}		
	}
}

//管理费子表信息
lineCount = 0;
String arrCount2[] = request.getParameterValues("RiskFeeParamGridNo");
if (arrCount2 != null){
	String tFeeMin[] = request.getParameterValues("RiskFeeParamGrid1");	//费用下限
	String tFeeMax[] = request.getParameterValues("RiskFeeParamGrid2");	//费用上限
	String tFeeRate[] = request.getParameterValues("RiskFeeParamGrid3");	//管理费比例
	String tFeeID[] = request.getParameterValues("RiskFeeParamGrid4");	//FeeID

	lineCount = arrCount2.length; //行数
	System.out.println(lineCount+"*******************************");

	for(int i=0;i<lineCount;i++){
		tLCGrpFeeParamSchema = new LCGrpFeeParamSchema();
		tLCGrpFeeParamSchema.setGrpPolNo(GrpPolNo);
		tLCGrpFeeParamSchema.setFeeMin(tFeeMin[i]);
		tLCGrpFeeParamSchema.setFeeMax(tFeeMax[i]);
		tLCGrpFeeParamSchema.setFeeRate(tFeeRate[i]);
		tLCGrpFeeParamSchema.setFeeID(tFeeID[i]);
		tLCGrpFeeParamSet.add(tLCGrpFeeParamSchema);
	}
}
System.out.println("end ...");

// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tG);
tVData.addElement(tLCGrpFeeSet);
tVData.addElement(tLCGrpFeeParamSet);
tVData.addElement(tTransferData);
//tVData.addElement(tLCGrpPolSpecSet);
try{
	System.out.println("this will save the data!!!");
	tGrpFeeUI.submitData(tVData,tOperate);
}
catch(Exception ex){
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}

if (!FlagStr.equals("Fail")){
	tError = tGrpFeeUI.mErrors;
	if (!tError.needDealError()){
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	else{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>