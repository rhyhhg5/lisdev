<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.ulitb.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>

<%	
	LCGrpPositionSet  tLCGrpPositionSet =  new LCGrpPositionSet();
    TransferData tTransferData = new TransferData(); 
	//输出参数
	String FlagStr = "Fail";
	String Content = "";
	//全局变量
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");

	System.out.println("*********begin****************");
    String GrpContNo = request.getParameter("GrpContNo");	//集体合同号码 
	System.out.println("GrpPolNo=" + GrpContNo);
	System.out.println("**********End****************");
	
	
	//取得险种退保费率明细
	int lineCount = 0;
	String arrCount[] = request.getParameterValues("AscriptionRuleGradeGridNo");
	if(arrCount != null)
	{
		String[] strGradecode = request.getParameterValues("AscriptionRuleGradeGrid1");//提取规则级别
		String[] strGradename = request.getParameterValues("AscriptionRuleGradeGrid2");//提取规则名字

		lineCount = arrCount.length;	

		for(int i=0;i<lineCount;i++)
		{
			LCGrpPositionSchema  tLCGrpPositionSchema = new LCGrpPositionSchema();	
			tLCGrpPositionSchema.setGrpContNo(GrpContNo);
		    tLCGrpPositionSchema.setGradeCode(strGradecode[i]);
			tLCGrpPositionSchema.setGradeName(strGradename[i]);		
			tLCGrpPositionSet.add(tLCGrpPositionSchema);
			System.out.println("记录"+i+"放入Set！");
		} 
		System.out.println("****************end ...***************");
   
	}
	    tTransferData.setNameAndValue("GrpContNo",GrpContNo);
//	 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";

	tVData.add(tG);
	tVData.addElement(tLCGrpPositionSet);
	tVData.add(tTransferData);
	
	
	LCPositionUI tLCPositionUI = new LCPositionUI();
	if( tLCPositionUI.submitData( tVData, "" ) == false )                       
	{                                                                               
		Content = " 保存失败，原因是: " + tLCPositionUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	System.out.println("Content=" + Content);
	System.out.println("FlagStr=" + FlagStr);
%>

<html>
<script language="javascript">
	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	
</script>
</html>
