//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var spanObj;
var mDebug = "100";
var mOperate = 0;
var mAction = "";
var arrResult = new Array();
var mShowCustomerDetail = "PROPOSAL";
var mCurOperateFlag = ""	// "1--录入，"2"--查询
var mGrpFlag = ""; 	//个人集体标志,"0"表示个人,"1"表示集体.
var cflag = "0";        //文件件录入位置
var sign=0;
var risksql;
var arrGrpRisk = null;
var mainRiskPolNo="";
var cMainRiskCode="";
var mSwitch = parent.VD.gVSwitch;
window.onfocus = myonfocus;
var hiddenBankInfo = "";
var CloseBase = "NO";
var approve="0";
var inputflag="0";
var showcontent="0";
/*********************************************************************
 *  选择险种后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) {
	try	{

	    //险种输入权限的暂时屏蔽
	    /*
	   try{
	       if( type =="noScan" && cCodeName == 'RiskInd')//个单无扫描件录入
		    {
		    	var strSql = "select distinct 1 from ldsysvar where VERIFYOPERATEPOPEDOM('"+Field.value+"','"+ManageCom+"','"+ManageCom+"',"+"'Pa')=1 ";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ;
	            //alert(strSql);
	           var arrResult = easyExecSql(strSql);
	           //alert(arrResult);
	          if (arrResult == null) {
	              alert("机构 ["+ManageCom+"] 无权录入险种 ["+Field.value+"] 的投保单!");
	              gotoInputPage();
	              return ;
	          }
	        }

        }
        catch(ex) {}

	   try{
	        //alert(cCodeName);
	        if (typeof(prtNo)!="undefined" && typeof(type)=="undefined" && cCodeName == 'RiskInd')//个单有扫描件录入
		    {
		    	//alert(cCodeName);
		    	var strSql2 = "select distinct 1 from ldsysvar where VERIFYOPERATEPOPEDOM('"+Field.value+"','"+ManageCom+"','"+ManageCom+"',"+"'Pz')=1 ";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ;
	            //alert(strSql2);
	           var arrResult2 = easyExecSql(strSql2);
	           //alert(arrResult2);
	          if (arrResult2 == null) {
	              alert("机构 ["+ManageCom+"] 无权录入险种 ["+Field.value+"] 的投保单!");
	              gotoInputPage();
	              return ;
	          }
	        }
        }
        catch(ex) {}
        */
        	
		if( cCodeName == "RiskInd" || cCodeName == "RiskGrp" || cCodeName == "RiskCode")
		{
			var tProposalGrpContNo = parent.VD.gVSwitch.getVar( "GrpContNo" );
		  var tContPlanCode = parent.VD.gVSwitch.getVar( "ContPlanCode" );
		  //alert("mainRiskPolNo:"+mainRiskPolNo);
		  //alert(parent.VD.gVSwitch.getVar("mainRiskPolNo"));

		  if(mainRiskPolNo==""&&parent.VD.gVSwitch.getVar("mainRiskPolNo")==true)
		  {
		    mainRiskPolNo=parent.VD.gVSwitch.getVar("mainRiskPolNo");
		    //alert(mainRiskPolNo);
		  }
		  //alert("mainRiskPolNo:"+parent.VD.gVSwitch.getVar("mainRiskPolNo"));
		  if(cCodeName=="RiskCode")
		  {
		  	if(!isSubRisk(Field.value)) //如果是主险
		  	{
		  	    cMainRiskCode=Field.value;
		  	}
		    else if(isSubRisk(Field.value)&&mainRiskPolNo!="") //如果是附险，并且主险不为空
		    {
		        var mainRiskSql="select riskcode from lcpol where polno='"+mainRiskPolNo+"'";
		        var arr = easyExecSql(mainRiskSql);
		        cMainRiskCode=arr[0];//得到主险险种号
			 }
			
		  //alert("mainriskcode:"+cMainRiskCode);
		  //alert("mainriskpolno:"+mainRiskPolNo);
		  }
		  //alert("mainRiskPolNo2:"+mainRiskPolNo);

		  //如果不是录入新的被保人，则从前一个页面取得相关的险种信息
			if(LoadFlag!=2&&LoadFlag!=7&&LoadFlag!=4)
			{
				getRiskInput(Field.value, LoadFlag);//LoadFlag在页面出始化的时候声明
				if(LoadFlag == 8 || LoadFlag == 18)
				{
					BnfGrid.clearData("BnfGrid");
					showDiv(divLCBnf1, "");
				}
			}
			else
			{
				if (tContPlanCode=='false'||tContPlanCode==null||tContPlanCode=='')
				{
					//如果没有保险计划编码,查询有没有默认值，如果有默认值也需要调用后台查询

					var strsql = "select ContPlanCode from LCContPlan where ContPlanCode='00' and ProposalGrpContNo='"+tProposalGrpContNo+"'";
					//alert(strsql);
					var defultContPlanCode=easyExecSql(strsql);
					//alert("defultContPlanCode"+defultContPlanCode);
					if (defultContPlanCode=='00')
					{
						queryRiskPlan( tProposalGrpContNo,Field.value,defultContPlanCode,cMainRiskCode );
					}
					else
					{
						getRiskInput(Field.value, LoadFlag);//LoadFlag在页面出始化的时候声明
						
					}

				}
				else
				{
					//alert("aa");
					queryRiskPlan( tProposalGrpContNo,Field.value,tContPlanCode,cMainRiskCode );
				}
				
			}


			//将扫描件图片翻到第一页
		  try { goToPic(0);	}	catch(ex2) {}
		}

		//自动填写受益人信息
		if (cCodeName == "customertype") {
		  if (Field.value == "0") {
		  	if(ContType!="2")
		  	{
                        var index = BnfGrid.mulLineCount;
                        BnfGrid.setRowColData(index-1, 2, fm.all("AppntName").value);
                        BnfGrid.setRowColData(index-1, 3, fm.all("AppntBirthday").value);
                        BnfGrid.setRowColData(index-1, 4, fm.all("AppntIDType").value);
                        BnfGrid.setRowColData(index-1, 5, fm.all("AppntIDNo").value);
                        BnfGrid.setRowColData(index-1, 6, fm.all("AppntRelationToInsured").value);
                       // BnfGrid.setRowColData(index-1, 8, fm.all("AppntHomeAddress").value);
                       BnfGrid.setRowColData(index-1, 9, "A");
		        }
		        else
		        {
		        alert("投保人为团体，不能作为受益人！")
                        var index = BnfGrid.mulLineCount;
                        BnfGrid.setRowColData(index-1, 2, "");
                        BnfGrid.setRowColData(index-1, 3, "");
                        BnfGrid.setRowColData(index-1, 4, "");
                        BnfGrid.setRowColData(index-1, 5, "");
                        BnfGrid.setRowColData(index-1, 6, "");
                        BnfGrid.setRowColData(index-1, 9, "");
		        }
		  }
		  else if (Field.value == "1") {
		  	             var index = BnfGrid.mulLineCount;
		  	             if(BnfGrid.getRowColData(index-1,1)=="1")
		  	             {
                        alert("身故受益人不能选择被保险人本人");
                        BnfGrid.setRowColData(index-1, 2, "");
                        BnfGrid.setRowColData(index-1, 3, "");
                        BnfGrid.setRowColData(index-1, 4, "");
                        BnfGrid.setRowColData(index-1, 5, "");
                        BnfGrid.setRowColData(index-1, 6, "");
                        BnfGrid.setRowColData(index-1, 9, "");
                    }
                    else
                    {
                        BnfGrid.setRowColData(index-1, 2, fm.all("Name").value);
                        BnfGrid.setRowColData(index-1, 3, fm.all("Birthday").value);
                        BnfGrid.setRowColData(index-1, 4, fm.all("IDType").value);
                        BnfGrid.setRowColData(index-1, 5, fm.all("IDNo").value);
                        BnfGrid.setRowColData(index-1, 6, "00");
                       // BnfGrid.setRowColData(index-1, 8, fm.all("AppntHomeAddress").value);
                        BnfGrid.setRowColData(index-1, 9, "I");
                    }
		  }
		}

		//收费方式选择
		if (cCodeName == "PayLocation") {
		  if (Field.value != "0") {
		    if (hiddenBankInfo=="") hiddenBankInfo = DivLCKind.innerHTML;

		    fm.all("BankCode").className = "readonly";
		    fm.all("BankCode").readOnly = true;
		    fm.all("BankCode").tabIndex = -1;
		    fm.all("BankCode").ondblclick = "";

		    fm.all("AccName").className = "readonly";
		    fm.all("AccName").readOnly = true;
		    fm.all("AccName").tabIndex = -1;
		    fm.all("AccName").ondblclick = "";

		    fm.all("BankAccNo").className = "readonly";
		    fm.all("BankAccNo").readOnly = true;
		    fm.all("BankAccNo").tabIndex = -1;
		    fm.all("BankAccNo").ondblclick = "";
		  }
		  else {
		    if (hiddenBankInfo!="") DivLCKind.innerHTML = hiddenBankInfo;

		    strSql = "select BankCode,BankAccNo,AccName from LCAppNT where AppNtNo = '" + fm.all('AppntCustomerNo').value + "' and contno='"+fm.all('ContNo').value+"'";
			var arrAppNtInfo = easyExecSql(strSql);
			//alert(strSql);
		    fm.all("BankCode").value = arrAppNtInfo[0][0];
		    fm.all("AccName").value = arrAppNtInfo[0][2];
		    fm.all("BankAccNo").value = arrAppNtInfo[0][1];
		    fm.all("PayLocation").value = "0";
		    fm.all("PayLocation").focus();
		  }
		}
		//alert(cCodeName);
		if(cCodeName == "PolCalRule1"){  //暂时未用
		  if(Field.value=="1"){       //统一费率
		    divFloatRate.style.display="none";
		    divFloatRate2.style.display="";
		  }
		  else if(Field.value=="2"){  //约定费率折扣
		    fm.all('FloatRate').value="";
		    divFloatRate.style.display="";
		    divFloatRate2.style.display="none";
		  }
		  else{
		    divFloatRate.style.display="none";
		    divFloatRate2.style.display="none";
		  }
		}
		if(cCodeName=="PayEndYear"){
		  getOtherInfo(Field.value);
		}
		
		if(cCodeName=="GetYear"){
		  getGetYearFlag(Field.value);
		}

		if(cCodeName=="PayRuleCode"){
		  fm.action="./ProposalQueryPayRule.jsp?AppFlag=0";
		  fm.submit();
		  //parent.fraSubmit.window.location ="./ProposalQueryPayRule.jsp?GrpContNo="
		  //		+ fm.all('GrpContNo').value+"&RiskCode="+fm.all('RiskCode').value
		  //		+"&InsuredNo="+fm.all('InsuredNo').value
		  //		+"&PayRuleCode="+fm.all('PayRuleCode').value
		  //		+"&JoinCompanyDate="+fm.all(JoinCompanyDate).value
		  //		+"&AppFlag=0";
		}
		if(cCodeName=="InsuYearFlag"){
			//针对NOK01的校验 zhanjq 2007-9-29
  	  if(!checkflag()){
  	  	return;
  	  }
    }
	}
	catch( ex ) {
	}

}
/*********************************************************************
 *  提交前的校验、计算
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function beforeSubmit()
{

  if( verifyInput() == false ){
   return false;
  }
}

/*********************************************************************
 *  根据LoadFlag设置一些Flag参数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function convertFlag( cFlag )
{
  //alert("cFlag:" + cFlag);
	if( cFlag == "1"||cFlag == "11")		// 个人投保单直接录入
	{
		mCurOperateFlag = "1";
		mGrpFlag = "0";
	}

  if(cFlag == "8") //保全增加附加险-团险
  {
  	mCurOperateFlag = "1";
		mGrpFlag = "1";
  }
  if(cFlag == "18") //保全增加附加险－个险
  {
  	mCurOperateFlag = "0";
		mGrpFlag = "0";
  }
	if( cFlag == "2" || cFlag == "7" || cFlag == "9" || cFlag == "13"||cFlag == "14"||cFlag == "16"||cFlag == "99"||cFlag=="23")		// 集体下个人投保单录入
	{
		mCurOperateFlag = "1";
		mGrpFlag = "1";
	}
	if( cFlag == "3"||cFlag == "6" )		// 个人投保单明细查询
	{
		mCurOperateFlag = "2";
		mGrpFlag = "0";
	}
	if( cFlag == "4" )		// 集体下个人投保单明细查询
	{
		mCurOperateFlag = "2";
		mGrpFlag = "1";
	}
	if( cFlag == "5"||cFlag=="25" )		// 个人投保单复核查询
	{
		mCurOperateFlag = "2";
		mGrpFlag = "3";
	}
	if(cFlag=="99"&&(checktype=="11"||checktype=="31"||checktype=="41"))
	{
		mGrpFlag = "0";
	}
	if(cFlag=="99"&&checktype=="2")
	{
		mGrpFlag = "1";
	}
}

/**
 * 获取该集体下所有险种信息
 */
function queryGrpPol(prtNo, riskCode) {
  var findFlag = false;

  //根据团体投保单号查找出所有的相关险种
  if (arrGrpRisk == null) {
    var strSql = "select GrpProposalNo, RiskCode from LCGrpPol where PrtNo = '" + prtNo + "'";
    arrGrpRisk  = easyExecSql(strSql);

    //通过承保描述定义表找到主险
    for (i=0; i<arrGrpRisk.length; i++) {
      strSql = "select SubRiskFlag from LMRiskApp where RiskCode = '"
             + arrGrpRisk[i][1] + "' and RiskVer = '2002'";
      
      var riskDescribe = easyExecSql(strSql);

      if (riskDescribe == "M") {
        top.mainRisk = arrGrpRisk[i][1];
        break;
      }
    }
  }
  //alert(arrGrpRisk);

  //获取选择的险种和集体投保单号码
  for (i=0; i<arrGrpRisk.length; i++) {
    if (arrGrpRisk[i][1] == riskCode) {
      fm.all("RiskCode").value = arrGrpRisk[i][1];
      fm.all("GrpPolNo").value = arrGrpRisk[i][0];

      if (arrGrpRisk[i][1] == top.mainRisk) {
        //top.mainPolNo = "";
        mainRiskPolNo ="";
      }

      findFlag = true;
      break;
    }
  }

  if (arrGrpRisk.length > 1) {
    fm.all("RiskCode").className = "code";
    fm.all("RiskCode").readOnly = false;
  }
  else {
    fm.all("RiskCode").onclick = "";
  }

  return findFlag;
}

/**
 * 根据身份证号码获取出生日期
 */
function grpGetBirthdayByIdno() {
  var id = fm.all("IDNo").value;

  if (fm.all("IDType").value == "0") {
    if (id.length == 15) {
      id = id.substring(6, 12);
      id = "19" + id;
      id = id.substring(0, 4) + "-" + id.substring(4, 6) + "-" + id.substring(6);
      fm.all("Birthday").value = id;
    }
    else if (id.length == 18) {
      id = id.substring(6, 14);
      id = id.substring(0, 4) + "-" + id.substring(4, 6) + "-" + id.substring(6);
      fm.all("Birthday").value = id;
    }
  }
}

/**
 * 校验主附险包含关系
 */
function checkRiskRelation(tPolNo, cRiskCode) {
  // 集体下个人投保单
  if (mGrpFlag == "1") {
    var strSql = "select RiskCode from LCGrpPol where GrpProposalNo = '" + tPolNo
               + "' and trim(RiskCode) in (select trim(Code1) from LDCode1 where Code = '" + cRiskCode + "')";
    return true;
  }
  else {
    var strSql = "select RiskCode from LCPol where PolNo = '" + tPolNo
               + "' and trim(RiskCode) in (select trim(Code1) from LDCode1 where Code = '" + cRiskCode + "')";
  }

  return easyQueryVer3(strSql);
}

/**
 * 出错返回到险种选择界面
 */
function gotoInputPage() {
  // 集体下个人投保单
  if (mGrpFlag == "1") {
    //top.fraInterface.window.location = "../app/ProposalGrpInput.jsp?LoadFlag=" + LoadFlag;
    top.fraInterface.window.location = "../app/ProposalInput.jsp?LoadFlag=" + LoadFlag+"&ScanFlag="+ScanFlag+"&ContType="+ContType+"&scantype="+scantype+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&SysType="+SysType+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&oldContNo="+oldContNo;
  }
  // 个人有扫描件投保单
  else if (typeof(prtNo)!="undefined" && typeof(type)=="undefined") {
    top.fraInterface.window.location = "../app/ProposalInput.jsp?LoadFlag=" + LoadFlag+"&ScanFlag="+ScanFlag+"&ContType="+ContType+"&scantype="+scantype+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&SysType="+SysType+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&oldContNo="+oldContNo;
  }
  // 个人无扫描件投保单
  else {
    top.fraInterface.window.location = "../app/ProposalInput.jsp?LoadFlag=" + LoadFlag+"&ScanFlag="+ScanFlag+"&ContType="+ContType+"&scantype="+scantype+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&SysType="+SysType+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&oldContNo="+oldContNo;
  }
}

function showSaleChnl() {
  showCodeList('SaleChnl',[this]);
}

/*********************************************************************
 *  根据不同的险种,读取不同的代码
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getRiskInput(cRiskCode, cFlag) {
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "";
	var tPolNo = "";
//	alert("getRiskInput.cRiskCode:"+cRiskCode);
	convertFlag(cFlag);
	// 首次进入集体下个人录入
	//mGrpFlag标志个单和团单  0－ 个单 1－团单下的个单 3－个单复核
	if( mGrpFlag == "0" )		// 个人投保单
		urlStr = "../riskinput/Risk" + cRiskCode + ".jsp";
	if( mGrpFlag == "1" )		// 集体下个人投保单
		urlStr = "../riskgrp/Risk" + cRiskCode + ".jsp";
	if( mGrpFlag == "3" )		// 个人投保单复核
		urlStr = "../riskinput/Risk" + cRiskCode + ".jsp";
	//读取险种的界面描述

	showInfo = window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;dialogTop:-800;dialogLeft:-800;resizable=1");
	initElementtype();
//  if(LoadFlag==4)
//  {
			//	showDiv(divApproveContButton, "true");
     //   showDiv(inputQuest, "true");

//  }
//  else 
	if(LoadFlag==5||LoadFlag ==25){
          showDiv(inputButton, "true"); 
          showDiv(inputQuest,"true"); 
          showDiv(inputApproveConFirm,"false");
          showDiv(inputQuestIn,"false");  		
	}   
	else
	{
      showDiv(inputButton, "true");
      if(LoadFlag == 4){
	      if(parent.VD.gVSwitch.getVar("PolNo") != false && parent.VD.gVSwitch.getVar("PolNo") !=""){
	      	showDiv(modifyButton, "true");
	      }
	    }
      //showDiv(modifyButton, "true");
      showDiv(inputQuest, "true");
      if(LoadFlag==16){
        showDiv(inputConFirm,"false");
       }else{
        showDiv(inputConFirm,"true");
       }
      if(LoadFlag==1)
      {
           showDiv(inputQuestIn,"true");
      }
      if(LoadFlag==11)
      {
        showDiv(inputQuestIn,"true");      	
      	showDiv(SpecPol,"false");
      	showDiv(inputQuestButton,"true");
      }
      

  }
  if(LoadFlag==9)
 {
      showDiv(inputConFirm,"false");
 }
 if(LoadFlag==5)
 {
      showDiv(inputApproveConFirm,"true");
      showDiv(inputQuestIn,"true");
 }
 //保全新增附加险－个单
 if(LoadFlag==18)
 {
      fm.CValiDate.value = parent.VD.gVSwitch.getVar( "CValiDate" );
      fm.all("MainPolNo").value = parent.VD.gVSwitch.getVar("mainRiskPolNo");

      showDiv(inputButton, "true");  //重置，保存按钮
      showDiv(inputQuest, "true"); //上一步按钮
      showDiv(inputConFirm,"");//录入完毕按钮
 }
 //保全新增附加险－团险
 if(LoadFlag==8)
 {
      fm.CValiDate.value = parent.VD.gVSwitch.getVar( "CValiDate" );
      showDiv(inputButton, "true");  //重置，保存按钮
      showDiv(inputQuest, "true"); //上一步按钮
      showDiv(inputConFirm,"");//录入完毕按钮
 }

 if(LoadFlag==7)
 {
      
      fm.CValiDate.value = parent.VD.gVSwitch.getVar( "CValiDate" );      
      showDiv(inputButton, "true");  //重置，保存按钮
      showDiv(inputQuest, "true"); //上一步按钮
      showDiv(inputConFirm,"");//录入完毕按钮
 }
 
   
if(LoadFlag==2||LoadFlag==4||LoadFlag==9||LoadFlag==5)
 {
      showDiv(inputConFirm,"false");//录入完毕按钮
 }
 if(LoadFlag==2){
    //modify by lilei
    //PIR20081504
    //这段代码在本地，测试环境都没有问题，但是在91生产上会报异常，问题没有查明
    //但是加上try就没事了，故加上，以保证生产正确
    try
    {
    fm.all("copyEnd").style.display="none";
    }
    catch(ex)
    {
    }
 }
 if (cFlag == "99")
 {
      showDiv(inputApproveConFirm,"false");
      showDiv(inputConFirm,"false");   	
 	    showDiv(inputQuestIn,"false");      	
      showDiv(SpecPol,"false");
      showDiv(inputQuestButton,"false");
      showDiv(inputButton, "false");
      showDiv(inputQuest, "false");
		  showDiv(autoMoveButton, "true");
	}

	if (cFlag == "3") {
	  fm.all("SaleChnl").readOnly = false;
          fm.all("SaleChnl").className = "code";
          fm.all("SaleChnl").ondblclick= showSaleChnl;
	}

	if (cFlag == "2"||cFlag == "4"){
    showDiv(SpecPol, "false");		
		var aGrpContNo=parent.VD.gVSwitch.getVar( "GrpContNo" );
		if(isSubRisk(cRiskCode))
		  fm.all('MainPolNo').value=mainRiskPolNo;
		//alert("GrpContNo:"+ aGrpContNo);
		strSql = "select PayIntv from LCGrpPol where RiskCode = '" + cRiskCode + "' and GrpContNo ='"+aGrpContNo+"'";
		
      var PayIntv = easyExecSql(strSql);
      //alert("PayIntv:"+PayIntv);
      try{fm.PayIntv.value=PayIntv;} catch(ex){}
      
	}
  try {
    //出始化公共信息
	  emptyForm();
	  isSinglePremium(cRiskCode);
    //根据需求单放开销售渠道只读的限制，by Minim at 2003-11-24
    fm.all("SaleChnl").readOnly = false;
    fm.all("SaleChnl").className = "code";
    fm.all("SaleChnl").ondblclick= showSaleChnl;
    //无扫描件录入
    if (typeof(type)!="undefined" && type=="noScan") {
//      fm.all("PrtNo").readOnly = false;
//      fm.all("PrtNo").className = "common";
      //通过承保描述定义表找到主险
      strSql = "select SubRiskFlag from LMRiskApp where RiskCode = '"
             + cRiskCode + "' and RiskVer = '2002'";
      var riskDescribe = easyExecSql(strSql);

//      if (riskDescribe == "M") {
//        top.mainPolNo = "";
//      }

    }
    if(scantype=="scan") {
      //fm.all("PrtNo").value = prtNo;
      fm.all("RiskCode").value=cRiskCode;
      setFocus();
    }
    getContInput();
    if(LoadFlag==1||LoadFlag==5||LoadFlag==11||LoadFlag==25)
    {
    getPolApplyDate();
    getRiskInfo(cRiskCode);
    getInsuredPolInfo()  ;
    }
    getContInputnew();
  } catch(e) {}

	//传入险种和团体投保单号信息
	fm.all("RiskCode").value = cRiskCode;
	//将焦点移动到团体投保单号，以方便随动录入
	//fm.all("PrtNo").focus();

  //初始化界面的销售渠道
  try {
    prtNo = fm.all("PrtNo").value;
  	var riskType = prtNo.substring(2, 4);
  	if (riskType == "11") { fm.all("SaleChnl").value = "02"; }
  	else if (riskType == "12") { fm.all("SaleChnl").value = "01"; }
  	else if (riskType == "15") { fm.all("SaleChnl").value = "03"; }

  	else if (riskType == "16") {
  	  fm.all("SaleChnl").value = "02";

  	  fm.all("SaleChnl").readOnly = false;
      fm.all("SaleChnl").className = "code";
      fm.all("SaleChnl").ondblclick= showSaleChnl;
    }
  } catch(e) {}
  if (!(typeof(top.type)!="undefined" && (top.type=="ChangePlan" || top.type=="SubChangePlan"))) {
    //将是否指定生效日期在录入时失效
    fm.all("SpecifyValiDate").readOnly = true;
    fm.all("SpecifyValiDate").className = "readOnly";
    fm.all("SpecifyValiDate").ondblclick = "";
    fm.all("SpecifyValiDate").onkeyup = "";
    //fm.all("SpecifyValiDate").value = "N";
    fm.all("SpecifyValiDate").tabIndex = -1;
  }
  //alert("mCurOperateFlag:"+mCurOperateFlag);
  
  if (mGrpFlag == "1" && fm.all("PolTypeFlag").value=='2')//对公共帐户界面需要一些特殊化处理
  {
  	initPubInsuAcc(cRiskCode);
  }	

  if(mainRiskPolNo==""){
  mainRiskPolNo=parent.VD.gVSwitch.getVar("mainRiskPolNo");
  }
 // alert("mainRiskPolNo="+mainRiskPolNo);
  //alert("mCurOperateFlag:"+mCurOperateFlag);
  	if( mCurOperateFlag == "1" ||mCurOperateFlag == "2" ) {             // 录入
		// 集体下个人投保单
		//alert("mGrpFlag:"+mGrpFlag);
		if( mGrpFlag == "1" ||mGrpFlag == "3")	{
			getGrpPolInfo();                       // 带入集体部分信息
			getPayRuleInfo();
			//支持集体下个人，录入身份证号带出出生日期
			fm.all("IDNo").onblur = grpGetBirthdayByIdno;
			//暂时不去掉去掉问题件按钮,随动定制时例外
			if(LoadFlag!="99")
			inputQuest.style.display = "";
			// 获取该集体下所有险种信息
			//alert("judging if the RiskCode input has been input in group info.");
			//if (!queryGrpPol(fm.all("PrtNo").value, cRiskCode))	{
			//  alert("集体团单没有录入这个险种，集体下个人不允许录入！");
			//  fm.all("RiskCode").value="";
			//  //alert("now go to the new location- ProposalGrpInput.jsp");
			//  top.fraInterface.window.location = "./ProposalGrpInput.jsp?LoadFlag=" + LoadFlag;
			//  //alert("top.location has been altered");
			//  return false; //hezy
			//}
		}
		//if(initDealForSpecRisk(cRiskCode)==false)//特殊险种在初始化时的特殊处理-houzm添加
		//{
			//alert("险种："+cRiskCode+"初始化时特殊处理失败！");
			//return false;
		//}

//		alert(cRiskCode);
		//特殊险种在初始化时的特殊处理扩展-guoxiang add at 2004-9-6 16:33
        if(initDealForSpecRiskEx(cRiskCode)==false)
		{
			alert("险种："+cRiskCode+"初始化时特殊处理失败！");
			return false;
		}
		//alert("getRiskInput.isSubRisk begin...");
		//alert("cRiskCode:"+cRiskCode);
		try
		{
		  fm.all('SelPolNo').value=parent.VD.gVSwitch.getVar("PolNo");
		  //alert("kjlkdsjal");
		  if (fm.all('SelPolNo').value=='false')
		  {
		  		fm.all('SelPolNo').value='';
		  }
		  if(fm.all('SelPolNo').value!='')
		  { //更换险种
		    var tSql="select riskcode from lcpol where polno='"+fm.all('SelPolNo').value+"'";
		    var arr=easyExecSql(tSql);
		    if(arr[0]!=cRiskCode)
		    {
		      fm.all('SelPolNo').value='';
		    }
		  }
	  }
	  catch(ex)
	  {
	  }
	   //alert("mainRiskPolNo="+mainRiskPolNo);
		//alert("selPolNo:"+fm.all('SelPolNo').value);		
		if( isMainRisk( cRiskCode ) == true&&fm.all('SelPolNo').value=="" )
		{
			
			if(PolGrid.mulLineCount>0)
			{			
			      alert("已有主险，不能添加新的主险！");
			      gotoInputPage();
			} 
		}

		if( isSubRisk( cRiskCode ) == true&&fm.all('SelPolNo').value=="" ) {   // 附险
		  //如果是新增附加险，则不清空缓存的主险保单号
		  if (cFlag != "8" && cFlag != "2"&&cFlag != "18") {
			  //top.mainPolNo = ""; //hezy add
			  mainRiskPolNo = "";
			}
		
			//alert("tPolNo:"+tPolNo);
	    //20050711更新,目的是不再弹出窗口直接取mutiline中的值
	    if(LoadFlag==1||LoadFlag==5||LoadFlag==11||LoadFlag==25)
	    {
	    	var num=0;
	    	if(PolGrid.mulLineCount>0)
	    	{
	    		for(var maincount=0;maincount<PolGrid.mulLineCount;maincount++)
	    		{
            var arrQueryResult = easyExecSql("select SubRiskFlag from LMRiskApp where RiskCode='" + PolGrid.getRowColData(maincount, 2) + "'", 1, 0);
          
            if(arrQueryResult[0][0]=="M")
            {
            	num=maincount;
            	break;
            }
          }
          	    	tPolNo=PolGrid.getRowColData(num, 1);
	    	}

	    }
	    else
	    {
			    tPolNo = getMainRiskNo(cRiskCode);   //弹出录入附险的窗口,得到主险保单号码

			    //alert("tPolNo:"+tPolNo);
	    }
	    //alert("mainRiskPolNo="+mainRiskPolNo);

			if (!checkRiskRelation(tPolNo, cRiskCode))
			{
			      alert("主附险包含关系错误，已录主险不能带这个附加险！");
			      gotoInputPage();
			      //top.mainPolNo = "";
			      mainRiskPolNo = "";
			      return false;
		 }    
//-----------------------------------------------------------------------
         if(cRiskCode=='121301'||cRiskCode=='321601')//出始化特殊的附险信息--houzm添加--可单独提出为一个函数
         {
         		if(cRiskCode=='121301')
         		{
								if (!initPrivateRiskInfo121301(tPolNo))
								{
								  gotoInputPage();
								  return false;
								}
						}
						if(cRiskCode=='321601')
      			{
								if (!initPrivateRiskInfo321601(tPolNo))
								{
								  gotoInputPage();
								  return false;
								}
						}
         }
         else
         {  		
         		//出始化附险信息
						if (!initPrivateRiskInfo(tPolNo))
						{		           
						  gotoInputPage();
						  return false;
						}
         }

			try	{}	catch(ex1) { alert( "初始化险种出错" + ex1 );	}
		} // end of 附险if
  
		return false;

	} // end of 录入if
	//alert("mainRiskPolNo="+mainRiskPolNo);
   
	mCurOperateFlag = "";
	mGrpFlag = "";
	
}
/*********************************************************************
 *  判断该险种是否是主险
 *  参数  ：  险种代码
 *  返回值：  无
 *********************************************************************
 */
function isMainRisk(cRiskCode) {
  //alert(cRiskCode);
  if (cRiskCode=="")
  {
   return false;
  }
  var arrQueryResult = easyExecSql("select SubRiskFlag from LMRiskApp where RiskCode='" + cRiskCode + "'", 1, 0);


	if(arrQueryResult[0] == "S")    //需要转成大写
		return false;
	if(arrQueryResult[0] == "M")
		return true;

	return false;
}
/*********************************************************************
 *  判断该险种是否是附险,在这里确定既可以做主险,又可以做附险的代码
 *  参数  ：  险种代码
 *  返回值：  无
 *********************************************************************
 */
function isSubRisk(cRiskCode) {
  //alert(cRiskCode);
  if (cRiskCode=="")
  {
   return false;
  }
  var arrQueryResult = easyExecSql("select SubRiskFlag from LMRiskApp where RiskCode='" + cRiskCode + "'", 1, 0);
  
  	if(PolTypeFlag==1||PolTypeFlag==3||PolTypeFlag==4)
	{
	fm.modify9.style.display="none";
	}

	if(arrQueryResult[0] == "S")    //需要转成大写
		return true;
	if(arrQueryResult[0] == "M")
		return false;

	if (arrQueryResult[0].toUpperCase() == "A")
		if (confirm("该险种既可以是主险,又可以是附险!选择确定进入主险录入,选择取消进入附险录入"))
			return false;
		else
			return true;

	return false;
}

/*********************************************************************
 *  弹出录入附险的窗口,得到主险保单号码
 *  参数  ：  险种代码
 *  返回值：  无
 *********************************************************************
 */
function getMainRiskNo(cRiskCode) {
	var urlStr = "../app/MainRiskNoInput.jsp";
	var tPolNo="";
  //alert("mainRiskPolNo:"+mainRiskPolNo);

  if (typeof(mainRiskPolNo)!="undefined" && mainRiskPolNo!="") //如果mainRiskPolNo存在，则返回主险号
  {
    tPolNo = mainRiskPolNo;
  }
  else  //否则根据附险号，查询对应的主险号
  {
     var s = new Object();
     s.risckcode=cRiskCode;
     s.contno=fm.ContNo.value;
     s.insuredno=fm.CustomerNo.value;
    tPolNo = window.showModalDialog(urlStr,s,"status:no;help:0;close:0;dialogWidth:600px;dialogHeight:400px;center:yes;");
    //top.mainPolNo = tPolNo;
    mainRiskPolNo = tPolNo;
		try
		{
		  arrResult = easyExecSql("select CValiDate from LCPOl where PolNo = '" + mainRiskPolNo + "'", 1, 0);
      fm.CValiDate.value=arrResult[0][0];
    }
    catch(ex)
    {}
  }

	return tPolNo;
}

/*********************************************************************
 *  初始化附险信息
 *  参数  ：  投保单号
 *  返回值：  无
 *********************************************************************
 */
function initPrivateRiskInfo(cPolNo) {

	if(cPolNo=="") {
		alert("没有主险保单号,不能进行附加险录入!");
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
		return false
	}
	var arrLCPol = new Array();
	var arrQueryResult = null;
	// 主保单信息部分
	var sql = "select * from lcpol where polno='" + cPolNo + "' "
			+ "and trim(riskcode) in "
			+ "( select trim(riskcode) from LMRiskApp where SubRiskFlag = 'M' )";
	arrQueryResult = easyExecSql( sql , 1, 0);

	if (arrQueryResult == null)	{
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.

		//top.mainPolNo = "";
		mainRiskPolNo = "";
		alert("读取主险信息失败,不能进行附加险录入!");
		return false
	}

	arrLCPol = arrQueryResult[0];
	//alert(arrLCPol);
	displayPol( arrLCPol );
						  		           
	fm.all("MainPolNo").value = cPolNo;
	var tAR;

  	//投保人信息
  	if (arrLCPol[6]=="2") {     //集体投保人信息
  	  arrQueryResult = null;
  	  //arrQueryResult = easyExecSql("select * from lcappntgrp where polno='"+cPolNo+"'"+" and grpno='"+arrLCPol[28]+"'", 1, 0);
  	  arrQueryResult = easyExecSql("select * from lcgrpappnt where grpcontno='"+arrLCPol[0]+"'"+" and customerno='"+arrLCPol[28]+"'", 1, 0);
  	  tAR = arrQueryResult[0];
  	  displayPolAppntGrp(tAR);
  	} else {                     //个人投保人信息
  		/*
  	  arrQueryResult = null;
  	  arrQueryResult = easyExecSql("select * from lcappnt where polno='"+cPolNo+"'"+" and appntno='"+arrLCPol[28]+"'", 1, 0);
  	  tAR = arrQueryResult[0];
  	  displayPolAppnt(tAR);
  	  */
  	}

	// 被保人信息部分
	if (arrLCPol[21] == arrLCPol[28]) {
	  fm.all("SamePersonFlag").checked = true;
		parent.fraInterface.isSamePersonQuery();
    parent.fraInterface.fm.all("CustomerNo").value = arrLCPol[21];
	}
	//else {
		arrQueryResult = null;
		arrQueryResult = easyExecSql("select * from lcinsured where contno='"+arrLCPol[2]+"'"+" and insuredno='"+arrLCPol[21]+"'", 1, 0);
		tAR = arrQueryResult[0];
		displayPolInsured(tAR);
	//}
	return true;
}

/*********************************************************************
 *  校验投保单的输入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function verifyProposal() {
  var passVerify = true;

  //保全新增附加险

  if(fm.AppntRelationToInsured.value=="00"){
    if(fm.AppntCustomerNo.value!= fm.CustomerNo.value){
      alert("投保人与被保人关系为本人，但客户号不一致");
      return false;
    }
  }

   if(needVerifyRiskcode()==true)
   {
		//if(verifyInput2() == false) passVerify = false;
		if(checkbnf() == false) passVerify = false;
		if(checkpayintv()==false)passVerify = false;
		BnfGrid.delBlankLine();

		//if(BnfGrid.checkValue2(BnfGrid.name,BnfGrid)== false) passVerify = false;

		 //校验单证是否发放给业务员
		 if (!verifyPrtNo(fm.all("PrtNo").value)) passVerify = false;
	}
	try {
	  var strChkIdNo = "";

		  //以年龄和性别校验身份证号
		  if (fm.all("AppntIDType").value == "0")
		    strChkIdNo = chkIdNo(fm.all("AppntIDNo").value, fm.all("AppntBirthday").value, fm.all("AppntSex").value);
		  if (fm.all("IDType").value == "0")
		    strChkIdNo = chkIdNo(fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);

		  if (strChkIdNo != "") {
		    alert(strChkIdNo);
		    passVerify = false;


	  }

	  //校验职业和职业代码
//	  var arrCode = new Array();", fm.all("AppntWorkType").value, "code:OccupationCode", 1);
//	  if (arrCode!=true && fm.all("AppntOccupationCode").value!=arrCode[0]) {
//	    alert("投保人职业和职业代码不匹配！");
//	    passVerify = false;
//	  }
//	  arrCode = verifyCode("职业（工种）
//	  arrCode = verifyCode("职业（工种）", fm.all("WorkType").value, "code:OccupationCode", 1);
//	  if (arrCode!=true && fm.all("OccupationCode").value!=arrCode[0]) {
//	    alert("被保人职业和职业代码不匹配！");
//	    passVerify = false;
//	  }



	  var li1=0;
	  var li2=0;
	  var li3=0;

	  
	  for (i=0; i<BnfGrid.mulLineCount; i++)
	  {
	      if (BnfGrid.getRowColData(i, 7)==null||BnfGrid.getRowColData(i, 7)=='')
	      {
	         li1++;
	      }
	      else
	      {
	         li2=li2+parseFloat(BnfGrid.getRowColData(i, 7));
	      }
	  }
	  
      if(li2>1)
      {
          alert("收益人比例大于1!");
          return false;
      }
      else
      {
          li3=(1-li2)/li1;
      }

	  //校验受益比例
	  var i;
	  var sumLiveBnf = new Array();
	  var sumDeadBnf = new Array();
	  for (i=0; i<BnfGrid.mulLineCount; i++)
	  {
	    if (BnfGrid.getRowColData(i, 8)==null||BnfGrid.getRowColData(i, 8)=='')
	    {
	        BnfGrid.setRowColData(i, 8,"1");
	    }
	    if (BnfGrid.getRowColData(i, 7)==null||BnfGrid.getRowColData(i, 7)=='')
	    {
	        BnfGrid.setRowColData(i, 7,li3.toString());
	    }
	    if (BnfGrid.getRowColData(i, 1) == "0")
	    {
	      if (typeof(sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 8))]) == "undefined")
	        sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 8))] = 0;
	      sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 8))] = sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 8))] + parseFloat(BnfGrid.getRowColData(i, 7));
	    }
	    else if (BnfGrid.getRowColData(i, 1) == "1")
	    {
	      if (typeof(sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 8))]) == "undefined")
	        sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 8))] = 0;
	      sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 8))] = sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 8))] + parseFloat(BnfGrid.getRowColData(i, 7));
	    }

	  }

	  for (i=0; i<sumLiveBnf.length; i++)
	  {
  	    if (typeof(sumLiveBnf[i])!="undefined" && sumLiveBnf[i]>1)
  	    {
  	        alert("生存受益人受益顺序 " + i + " 的受益比例和为：" + sumLiveBnf[i] + " 。大于1，不能提交！");
  	        return false;
  	        passVerify = false;
  	    }
  	    else if (typeof(sumLiveBnf[i])!="undefined" && sumLiveBnf[i]<1)
  	    {
  	        alert("生存受益人受益顺序 " + i + " 的受益比例和为：" + sumLiveBnf[i] + " 。小于1，不能提交！");
  	        return false;  	        
  	        passVerify = false;
  	    }
	  }

	  for (i=0; i<sumDeadBnf.length; i++)
	  {
  	    if (typeof(sumDeadBnf[i])!="undefined" && sumDeadBnf[i]>1)
  	    {
  	      alert("死亡受益人受益顺序 " + i + " 的受益比例和为：" + sumDeadBnf[i] + " 。大于1，不能提交！");
  	        return false;  	      
  	      passVerify = false;
  	    }
  	    else if (typeof(sumDeadBnf[i])!="undefined" && sumDeadBnf[i]<1)
  	    {
  	        alert("死亡受益人受益顺序 " + i + " 的受益比例和为：" + sumDeadBnf[i] + " 。小于1，不能提交！");
  	        return false;  	        
  	        passVerify = false;
  	    }
	  }


	  if (trim(fm.BankCode.value)=="0101")
	  {
        if (trim(fm.BankAccNo.value).length!=19 || !isInteger(trim(fm.BankAccNo.value)))
        {
            alert("工商银行的账号必须是19位的数字，最后一个星号（*）不要！\n如果客户填写无误，请一定发问题件！");
            passVerify = false;
        }
      }

    //校验客户是否死亡
    if (fm.AppntCustomerNo.value!="" && isDeath(fm.AppntCustomerNo.value)) {
      alert("投保人已经死亡！");
      passVerify = false;
    }
    if (fm.CustomerNo.value!="" && isDeath(fm.CustomerNo.value)) {
      alert("被保人已经死亡！");
      passVerify = false;
    }
	}
	catch(e) {}

	if (!passVerify) {
	  if (!confirm("投保单录入可能有错误，是否继续保存？")) return false;
	  else return true;
	}
}

//校验客户是否死亡
function isDeath(CustomerNo) {
  var strSql = "select DeathDate from LDPerson where CustomerNo='" + CustomerNo + "'";
  var arrResult = easyExecSql(strSql);

  if (arrResult == ""||arrResult == null) return false;
  else return true;
}

/*********************************************************************
 *  保存个人投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm() {
  if(!checkflag()){
	 return;
  }
    //保存修改时对险种的特殊校验
   if(!checkRiskInfo())
   {
      return false;
   }
  
 //孟德寅 添加此方法 2008-9-4 ASR20082288
 if(!CheckDate2())
 {
    return false;
 }
  //添加结束
//acctype表示保单类型
if(EdorType=="PB"||EdorType=="PE")
{
   fm.all('EdorType').value = EdorType;
}   
	if(acctype!="4"&&acctype!="5"){ 
		if(!checkDuty()){
				return ; 
		}
	}

	if(!checkInsurYear()){
		return ;
	}

	if(sign != 0)
	{
	   alert("请不要重复点击!");
	   return;
	}
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

    var verifyGrade = "1";


  //根据特殊的险种做特殊处的理函数
  try {
  	 if(specDealByRisk()==false)
  	  return ;
  	 if(chkMult()==false)
  	   return;
  	   
	   if(mGrpFlag == "1" && fm.all('PolTypeFlag').value == '2')
	  	{
	  	 	if(checkPayPlancode()==false)
	  	 	return;
			}
  	}
  	 catch(e){}

        //校验__受益人信息填写正确性.  _yz_ ;)
        if(!check_BnfGrid())
        {alert ("通过速填录入受益人信息有误,请认真检查");
        return false; }

  //检验出生日期，如果空，从身份证号取
  //try {checkBirthday(); } catch(e){}

  	// 校验被保人是否同投保人，相同则进行复制
  try { verifySamePerson(); } catch(e) {}

      	// 校验录入数据
    if( verifyProposal() == false )
    {
   	return;
   }


	if (trim(fm.all('ProposalNo').value) != "") {
	  alert("该投保单号已经存在，不允许再次新增，请重新进入录入界面！");
	  return false;
	}

	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");


	if (LoadFlag=="1") {
		mAction = "INSERTPERSON";
	}
	else {
		mAction = "INSERTGROUP";
	}

	fm.all( 'fmAction' ).value = mAction;
	fm.action="../uliapp/ProposalSave.jsp"
	//为保全增加，add by Minim
	if (LoadFlag=="7" || LoadFlag=="8"||LoadFlag=="18") {

	  fm.action="../uliapp/ProposalSave.jsp?BQFlag=2&EdorType="+EdorType;
	  //fm.all("BQFlag").value=BQFlag;
	}
	//无名单补名单
	if (LoadFlag=="9") {
	  var strmasterpol="select proposalno from lcpol where contno='"+oldContNo+"' and riskcode='"+fm.RiskCode.value+"'";
	  var arrmasterpol= easyExecSql(strmasterpol,1,0);
	  fm.action="../uliapp/ProposalSave.jsp?BQFlag=4&MasterPolNo=" + arrmasterpol[0][0];
	  //alert(fm.action);
	}
	sign = 1;
	//alert("payIntv:"+fm.all('PayIntv').value);
 // beforeSubmit();
	fm.submit(); //提交
	sign = 0;
}

/**
 * 强制解除锁定
 */
function unLockTable() {
  if (fm.PrtNo.value == "") {
    alert("需要填写团体投保单号！");
    return;
  }

  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + fm.PrtNo.value + "&CreatePos=承保录单&PolState=1002&Action=DELETE";
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1");
}
/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content ){



	if(PolTypeFlag==1||PolTypeFlag==3||PolTypeFlag==4)
	{
	fm.modify9.style.display="none";
	}
	try {
		if(showInfo!=null)
			showInfo.close();
	}
	catch(e){}
	if (FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}else{
		//alert("afterSubmit()@fm.ContType.value="+fm.ContType.value);
	    if (fm.ContType.value == "1" && cflag=="1") {
	  		if (confirm("保存成功！\n要解除该团体投保单号的锁定，让复核人员能操作吗？")) {
				unLockTable();
	  		}
		}else{
			try{
				mainRiskPolNo=parent.VD.gVSwitch.getVar("mainRiskPolNo");
			} catch(ex){}
			//alert("loadflag:"+LoadFlag);
			if(LoadFlag == '3'){
				inputQuestButton.disabled = false;
			}
		  	if((LoadFlag==5&&approve=="1")){
				if(checkuw()){
					content = "复核成功，自核未通过，转入人工核保！";
				}else{
					content = "复核成功，自核通过，转入签单！";
				}
			}else if(LoadFlag==1&&	inputflag=="1"){
	  	         if(approve!="1"){//有问题件，质检抽检，客户校验
					content = "录入完毕，转入新单复核";
	  	         }else{
					if(checkuw()){
						content = "录入完毕，无需复核，自核未通过，转入人工核保！";
					}else{
						content = "录入完毕，无需复核，自核通过，转入签单！";
					}
				}
			}else if(LoadFlag==1&&	inputflag=="2"){
				content = "标记特殊件成功！";
			}else if(LoadFlag==8&&	inputflag==18){
		  	  //alert("fdfd");
				fm.riskbutton2.style.display='none';
			}else{
				if(LoadFlag==1||LoadFlag==5){
					getInsuredPolInfo();
				}
				content = "保存成功！";
			}
			var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			if(CloseBase=='YES'){
				top.close();
			}
		  	if(mAction=="CONFIRM"){
				window.location.href("./ContPolInput.jsp");
			}
			if(inputflag=="1"||inputflag=="2"||approve=="1"){
		    	top.close();
		    	top.opener.easyQueryClick();
			}
		}

		//暂时保存主险投保单号码，方便附险的录入，重新选择扫描件后失效
		//try { if (top.mainPolNo == "") top.mainPolNo = fm.all("ProposalNo").value } catch(e) {}
		//try { if (mainRiskPolNo == "") mainRiskPolNo = fm.all("ProposalNo").value } catch(e) {alert("err");}
	}

	//承保计划变更，附险终止的后续处理
	if (mAction=="DELETE") {
		if (typeof(top.type)!="undefined" && top.type=="SubChangePlan") {
		    var tProposalNo = fm.all('ProposalNo').value;
	    	var tPrtNo = fm.all('PrtNo').value;
	    	var tRiskCode = fm.all('RiskCode').value;
	  		parent.fraTitle.window.location = "./ChangePlanSubWithdraw.jsp?polNo=" + tProposalNo + "&prtNo=" + tPrtNo + "&riskCode=" + tRiskCode;
		}
		returnparent();
	}

	mAction = "";
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
	try	{
		//initForm();
		var tRiskCode = fm.RiskCode.value;
		var prtNo = fm.PrtNo.value;

		emptyForm();

		fm.RiskCode.value = tRiskCode;
		fm.PrtNo.value = prtNo;

		if (LoadFlag == "2" || LoadFlag=="4") {
		  getGrpPolInfo();
		}
	}
	catch(re)	{
		alert("在ProposalInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	//if( cDebug == "1" )
		//parent.fraMain.rows = "0,0,50,82,*";
	//else
		//parent.fraMain.rows = "0,0,80,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	//showDiv( operateButton, "false" );
	//showDiv( inputButton, "true" );
}

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick() {
	if( mOperate == 0 )	{
		mOperate = 1;
		cGrpPolNo = fm.all( 'GrpPolNo' ).value;
		cContNo = fm.all( 'ContNo' ).value;

		window.open("./ProposalQueryMain.jsp?GrpPolNo=" + cGrpPolNo + "&ContNo=" + cContNo);
	}
}

/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
//	alert('update');
	if(!checkflag()){
  		return;
  	}
    //保存修改时对险种的特殊校验
   if(!checkRiskInfo())
   {
      return false;
   }
  	
 //孟德寅 添加此方法 2008-9-4  ASR20082288
  if(!CheckDate2())
  {
     return false;
  }
 //添加结束 	
    if(EdorType=="PB"||EdorType=="PE")
    {
     	fm.all('EdorType').value = EdorType;
    }   
	var tProposalNo = "";
	tProposalNo = fm.all( 'ProposalNo' ).value;

	if( tProposalNo == null || tProposalNo == "" )
		alert( "请先做投保单查询操作，再进行修改!" );
	else 
	{
        //校验__受益人信息填写正确性.  _yz_ ;)
        if(!check_BnfGrid())
        {
        	alert ("录入受益人信息有误,请认真检查");
        	return false; 
        }


		// 校验录入数据
		if (fm.all('DivLCInsured').style.display == "none") 
		{
     		 for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) 
     		 {
    	  		if (fm.elements[elementsNum].verify != null && fm.elements[elementsNum].name.indexOf("Appnt") != -1) 
    	  		{
    	   			 fm.elements[elementsNum].verify = "";
    	  		}
    		 }
   		 }
		if(!CheckReviewCon()&&LoadFlag==5)
		{
			alert("请填写差错类型和质检意见,再作修改！");
			return false;
		}
		//对于主险与附加险的保险期间进行校验
		if(!checkInsurYear()){
			return ;
		}
	
		if( verifyProposal() == false ) return;
		if(chkMult()==false)  return;
		// 校验被保人是否同投保人，相同则进行复制
    	try { verifySamePerson(); } catch(e) {}
    	
    	var arrResult = easyExecSql("select count(1) from lcgiltrace where serialno='Q' and contno='"+oldContNo+"' and riskcode='"+fm.all('RiskCode').value+"' ", 1, 0);
		if (arrResult != null && arrResult[0][0]>=1) {
		  	if(!confirm("此被保人在人工核保阶段保存的GIL信息将被清除!"))
			{
    			return false;
 			}
		}
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

		if( mAction == "" )	{
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			if (LoadFlag=="1") {
				mAction = "UPDATEPERSON";
			}
			else {
				mAction = "UPDATEGROUP";
			}

			fm.all( 'fmAction' ).value = mAction;

			//承保计划变更(保持投保单状态不变：复核状态，核保状态)
			if (typeof(window.ChangePlanSub) == "object") fm.all('fmAction').value = "ChangePlan" + fm.all('fmAction').value;
			//修改浮动费率(保持投保单状态不变：复核状态，核保状态,作用比承保计划变更大一项，能修改浮动费率，为权限考虑)
			if(LoadFlag=="10") fm.all('fmAction').value = "ChangePlan" + fm.all('fmAction').value;
			if(LoadFlag=="3") fm.all('fmAction').value = "Modify" + fm.all('fmAction').value;
			//inputQuestButton.disabled = false;
			fm.submit(); //提交
		}

		try {
		  if (typeof(top.opener.modifyClick) == "object") top.opener.initQuery();
		}
		catch(e) {
		}

	}
}

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick() {
	var tProposalNo = fm.all('ProposalNo').value;

	if(tProposalNo==null || tProposalNo=="") {
		alert( "请先做投保单查询操作，再进行删除!" );
	}
	else {
		//chenwm20070907 检查险种是否关联有投资规则.
		var arrResult = easyExecSql("select * from lcperinvestplan where polno='"+parent.VD.gVSwitch.getVar("PolNo")+"'", 1, 0);
		if (arrResult != null) {
		  alert("请先删除险种下投资规则!");
		  return false;
		}
		
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

		if( mAction == "" )	{
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}

/*********************************************************************
 *  Click事件，当点击“选择责任”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function chooseDuty()
{
	cRiskCode = fm.RiskCode.value;
	cRiskVersion = fm.RiskVersion.value

	if( cRiskCode == "" || cRiskVersion == "" )
	{
		alert( "您必须先录入险种和险种版本才能修改该投保单的责任项。" );
		return false
	}

	showInfo = window.open("./ChooseDutyInput.jsp?RiskCode="+cRiskCode+"&RiskVersion="+cRiskVersion);
	return true
}

/*********************************************************************
 *  Click事件，当点击“查询责任信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showDuty()
{
	//下面增加相应的代码
	cPolNo = fm.ProposalNo.value;
	if( cPolNo == "" )
	{
		alert( "您必须先保存投保单才能查看该投保单的责任项。" );
		return false
	}

	var showStr = "正在查询数据，请您稍候......";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	showModalDialog( "./ProposalDuty.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
	showInfo.close();
}

/*********************************************************************
 *  Click事件，当点击“关联暂交费信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showFee()
{
	cPolNo = fm.ProposalNo.value;
	var prtNo = fm.PrtNo.value;

	if( cPolNo == "" )
	{
		alert( "您必须先保存投保单才能进入暂交费信息部分。" );
		return false
	}

	showInfo = window.open( "./ProposalFee.jsp?PolNo=" + cPolNo + "&polType=PROPOSAL&prtNo=" + prtNo );
}

/*********************************************************************
 *  Click事件，当双击“投保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt()
{
	if( mOperate == 0 )
	{
		mOperate = 2;
		showInfo = window.open( "../sys/LDPersonMain.html" );
	}
}

/*********************************************************************
 *  Click事件，当双击“被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsured()
{
	if( mOperate == 0 )
	{
		mOperate = 3;
		showInfo = window.open( "../sys/LDPersonMain.html" );
	}
}

/*********************************************************************
 *  Click事件，当双击“连带被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubInsured( span, arrPara )
{
	if( mOperate == 0 )
	{
		mOperate = 4;
		spanObj = span;
		showInfo = window.open( "../sys/LDPersonMain.html" );
	}
}

/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPol(cArr)
{
	try
	{
	/*
	    try { fm.all('PrtNo').value = cArr[6]; } catch(ex) { };
	    try { fm.all('ManageCom').value = cArr[12]; } catch(ex) { };
	    try { fm.all('SaleChnl').value = cArr[15]; } catch(ex) { };
	    try { fm.all('AgentCom').value = cArr[13]; } catch(ex) { };
	    try { fm.all('AgentType').value = cArr[14]; } catch(ex) { };
	    try { fm.all('AgentCode').value = cArr[87]; } catch(ex) { };
	    try { fm.all('AgentGroup').value = cArr[88]; } catch(ex) { };
	    //try { fm.all('Handler').value = cArr[82]; } catch(ex) { };
	    //try { fm.all('AgentCode1').value = cArr[89]; } catch(ex) { };
	    try { fm.all('Remark').value = cArr[90]; } catch(ex) { };

	    try { fm.all('ContNo').value = cArr[1]; } catch(ex) { };

	    //try { fm.all('Amnt').value = cArr[43]; } catch(ex) { };
	    try { fm.all('CValiDate').value = cArr[29]; } catch(ex) { };
	    try { fm.all('PolApplyDate').value = cArr[128]; } catch(ex) { };
	    try { fm.all('HealthCheckFlag').value = cArr[72]; } catch(ex) { };
	    try { fm.all('OutPayFlag').value = cArr[97]; } catch(ex) { };
	    try { fm.all('PayLocation').value = cArr[59]; } catch(ex) { };
	    try { fm.all('BankCode').value = cArr[102]; } catch(ex) { };
	    try { fm.all('BankAccNo').value = cArr[103]; } catch(ex) { };
	    try { fm.all('AccName').value = cArr[118]; } catch(ex) { };
	    try { fm.all('LiveGetMode').value = cArr[98]; } catch(ex) { };
	    try { fm.all('BonusGetMode').value = cArr[100]; } catch(ex) { };
	    try { fm.all('AutoPayFlag').value = cArr[65]; } catch(ex) { };
	    try { fm.all('InterestDifFlag').value = cArr[66]; } catch(ex) { };

	    try { fm.all('InsuYear').value = cArr[111]; } catch(ex) { };
	    try { fm.all('InsuYearFlag').value = cArr[110]; } catch(ex) { };
	    try { fm.all('PolTypeFlag').value = cArr[69]; } catch(ex) { };
	    try { fm.all('InsuredPeoples').value = cArr[24]; } catch(ex) { };
	    try { fm.all('InsuredAppAge').value = cArr[22]; } catch(ex) { };


	    try { fm.all('StandbyFlag1').value = cArr[78]; } catch(ex) { };
	    try { fm.all('StandbyFlag2').value = cArr[79]; } catch(ex) { };
	    try { fm.all('StandbyFlag3').value = cArr[80]; } catch(ex) { };
	*/
	    try { fm.all('PrtNo').value = cArr[5]; } catch(ex) { };
	    try { fm.all('ManageCom').value = cArr[13]; } catch(ex) { };
	    try { fm.all('SaleChnl').value = cArr[19]; } catch(ex) { };
	    try { fm.all('SaleChnlDetail').value = cArr[115]; } catch(ex) { };
	    try { fm.all('AgentCom').value = cArr[14]; } catch(ex) { };
	    try { fm.all('AgentType').value = cArr[15]; } catch(ex) { };
	    try { fm.all('AgentCode').value = cArr[16]; } catch(ex) { };
	    try { fm.all('AgentGroup').value = cArr[17]; } catch(ex) { };
	    try { fm.all('Handler').value = cArr[20]; } catch(ex) { };
	    try { fm.all('AgentCode1').value = cArr[18]; } catch(ex) { };
	    try { fm.all('Remark').value = cArr[92]; } catch(ex) { };

	    try { fm.all('ContNo').value = cArr[2]; } catch(ex) { };

	    //try { fm.all('CValiDate').value = cArr[30]; } catch(ex) { };//改从前台页面传入 ALex
	    try { fm.all('PolApplyDate').value = cArr[101]; } catch(ex) { };
	    try { fm.all('HealthCheckFlag').value = cArr[81]; } catch(ex) { };
	    //try { fm.all('OutPayFlag').value = cArr[97]; } catch(ex) { };
	    try { fm.all('PayLocation').value = cArr[51]; } catch(ex) { };
	    //try { fm.all('BankCode').value = cArr[102]; } catch(ex) { };
	    //try { fm.all('BankAccNo').value = cArr[103]; } catch(ex) { };
	    //try { fm.all('AccName').value = cArr[118]; } catch(ex) { };
	    try { fm.all('LiveGetMode').value = cArr[86]; } catch(ex) { };
	    try { fm.all('BonusGetMode').value = cArr[88]; } catch(ex) { };
	    try { fm.all('AutoPayFlag').value = cArr[77]; } catch(ex) { };
	    try { fm.all('InterestDifFlag').value = cArr[78]; } catch(ex) { };
    	if(fm.RiskCode.value=="60601"||fm.RiskCode.value=="60701"||fm.RiskCode.value=="60602"||fm.RiskCode.value=="10201"||fm.RiskCode.value=="60702")
    	{
    	}
    	else
    	{
	      try { fm.all('InsuYear').value = cArr[45]; } catch(ex) { };
	      try { fm.all('InsuYearFlag').value = cArr[44]; } catch(ex) { };
	      switch(cArr[44]){
	      	case 'D': try{fm.all('InsuYearFlagName').value = "天";} catch(ex){}
	      	case 'M': try{fm.all('InsuYearFlagName').value = "月";} catch(ex){}
	      	case 'Y': try{fm.all('InsuYearFlagName').value = "年";} catch(ex){}
	      }
	    }
	    try { fm.all('PolTypeFlag').value = cArr[7]; } catch(ex) { };
	    try { fm.all('InsuredPeoples').value = cArr[26]; } catch(ex) { };
	    try { fm.all('InsuredAppAge').value = cArr[25]; } catch(ex) { };

      if(fm.RiskCode.value=="NOK01"||fm.RiskCode.value=="NOK02"){
    	}else{
	      try { fm.all('StandbyFlag1').value = cArr[104]; } catch(ex) { };
	    }
	    try { fm.all('StandbyFlag2').value = cArr[105]; } catch(ex) { };
	    try { fm.all('StandbyFlag3').value = cArr[106]; } catch(ex) { };

	} catch(ex) {
	  alert("displayPol err:" + ex + "\ndata is:" + cArr);
	}
}

/*********************************************************************
 *  把保单中的投保人信息显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolAppnt(cArr)
{
	// 从LCAppntInd表取数据
	try { fm.all('AppntCustomerNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppntRelationToInsured').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppntPassword').value = cArr[5]; } catch(ex) { };
	try { fm.all('AppntName').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntSex').value = cArr[7]; } catch(ex) { };
	try { fm.all('AppntBirthday').value = cArr[8]; } catch(ex) { };
	try { fm.all('AppntNativePlace').value = cArr[9]; } catch(ex) { };
	try { fm.all('AppntNationality').value = cArr[10]; } catch(ex) { };
	try { fm.all('AppntMarriage').value = cArr[11]; } catch(ex) { };
	try { fm.all('AppntMarriageDate').value = cArr[12]; } catch(ex) { };
	try { fm.all('AppntOccupationType').value = cArr[13]; } catch(ex) { };
	try { fm.all('AppntStartWorkDate').value = cArr[14]; } catch(ex) { };
	try { fm.all('AppntSalary').value = cArr[15]; } catch(ex) { };
	try { fm.all('AppntHealth').value = cArr[16]; } catch(ex) { };
	try { fm.all('AppntStature').value = cArr[17]; } catch(ex) { };
	try { fm.all('AppntAvoirdupois').value = cArr[18]; } catch(ex) { };
	try { fm.all('AppntCreditGrade').value = cArr[19]; } catch(ex) { };
	try { fm.all('AppntIDType').value = cArr[20]; } catch(ex) { };
	try { fm.all('AppntProterty').value = cArr[21]; } catch(ex) { };
	try { fm.all('AppntIDNo').value = cArr[22]; } catch(ex) { };
	try { fm.all('AppntOthIDType').value = cArr[23]; } catch(ex) { };
	try { fm.all('AppntOthIDNo').value = cArr[24]; } catch(ex) { };
	try { fm.all('AppntICNo').value = cArr[25]; } catch(ex) { };
	try { fm.all('AppntHomeAddressCode').value = cArr[26]; } catch(ex) { };
	try { fm.all('AppntHomeAddress').value = cArr[27]; } catch(ex) { };
	try { fm.all('AppntPostalAddress').value = cArr[28]; } catch(ex) { };
	try { fm.all('AppntZipCode').value = cArr[29]; } catch(ex) { };
	try { fm.all('AppntPhone').value = cArr[30]; } catch(ex) { };
	try { fm.all('AppntBP').value = cArr[31]; } catch(ex) { };
	try { fm.all('AppntMobile').value = cArr[32]; } catch(ex) { };
	try { fm.all('AppntEMail').value = cArr[33]; } catch(ex) { };
	try { fm.all('AppntJoinCompanyDate').value = cArr[34]; } catch(ex) { };
	try { fm.all('AppntPosition').value = cArr[35]; } catch(ex) { };
	try { fm.all('AppntGrpNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('AppntGrpName').value = cArr[37]; } catch(ex) { };
	try { fm.all('AppntGrpPhone').value = cArr[38]; } catch(ex) { };
	try { fm.all('AppntGrpAddressCode').value = cArr[39]; } catch(ex) { };
	try { fm.all('AppntGrpAddress').value = cArr[40]; } catch(ex) { };
	try { fm.all('AppntDeathDate').value = cArr[41]; } catch(ex) { };
	try { fm.all('AppntRemark').value = cArr[42]; } catch(ex) { };
	try { fm.all('AppntState').value = cArr[43]; } catch(ex) { };
	try { fm.all('AppntWorkType').value = cArr[46]; } catch(ex) { };
	try { fm.all('AppntPluralityType').value = cArr[47]; } catch(ex) { };
	try { fm.all('AppntOccupationCode').value = cArr[48]; } catch(ex) { };
	try { fm.all('AppntDegree').value = cArr[49]; } catch(ex) { };
	try { fm.all('AppntGrpZipCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('AppntSmokeFlag').value = cArr[51]; } catch(ex) { };
	try { fm.all('AppntRgtAddress').value = cArr[52]; } catch(ex) { };
	try { fm.all('AppntHomeZipCode').value = cArr[53]; } catch(ex) { };
	try { fm.all('AppntPhone2').value = cArr[54]; } catch(ex) { };

}

/*********************************************************************
 *  把保单中的投保人数据显示到投保人部分
 *  参数  ：  集体客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolAppntGrp( cArr )
{

	try { fm.all('AppntPolNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('AppntGrpNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppntRelationToInsured').value = cArr[2]; } catch(ex) { };
	try { fm.all('AppntAppntGrade').value = cArr[3]; } catch(ex) { };
	try { fm.all('AppntPassword').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppntGrpName').value = cArr[5]; } catch(ex) { };
	try { fm.all('AppntGrpAddressCode').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntGrpAddress').value = cArr[7]; } catch(ex) { };
	try { fm.all('AppntGrpZipCode').value = cArr[8]; } catch(ex) { };
	try { fm.all('AppntBusinessType').value = cArr[9]; } catch(ex) { };
	try { fm.all('AppntGrpNature').value = cArr[10]; } catch(ex) { };
	try { fm.all('AppntPeoples').value = cArr[11]; } catch(ex) { };
	try { fm.all('AppntRgtMoney').value = cArr[12]; } catch(ex) { };
	try { fm.all('AppntAsset').value = cArr[13]; } catch(ex) { };
	try { fm.all('AppntNetProfitRate').value = cArr[14]; } catch(ex) { };
	try { fm.all('AppntMainBussiness').value = cArr[15]; } catch(ex) { };
	try { fm.all('AppntCorporation').value = cArr[16]; } catch(ex) { };
	try { fm.all('AppntComAera').value = cArr[17]; } catch(ex) { };
	try { fm.all('AppntLinkMan1').value = cArr[18]; } catch(ex) { };
	try { fm.all('AppntDepartment1').value = cArr[19]; } catch(ex) { };
	try { fm.all('AppntHeadShip1').value = cArr[20]; } catch(ex) { };
	try { fm.all('AppntPhone1').value = cArr[21]; } catch(ex) { };
	try { fm.all('AppntE_Mail1').value = cArr[22]; } catch(ex) { };
	try { fm.all('AppntFax1').value = cArr[23]; } catch(ex) { };
	try { fm.all('AppntLinkMan2').value = cArr[24]; } catch(ex) { };
	try { fm.all('AppntDepartment2').value = cArr[25]; } catch(ex) { };
	try { fm.all('AppntHeadShip2').value = cArr[26]; } catch(ex) { };
	try { fm.all('AppntPhone2').value = cArr[27]; } catch(ex) { };
	try { fm.all('AppntE_Mail2').value = cArr[28]; } catch(ex) { };
	try { fm.all('AppntFax2').value = cArr[29]; } catch(ex) { };
	try { fm.all('AppntFax').value = cArr[30]; } catch(ex) { };
	try { fm.all('AppntPhone').value = cArr[31]; } catch(ex) { };
	try { fm.all('AppntGetFlag').value = cArr[32]; } catch(ex) { };
	try { fm.all('AppntSatrap').value = cArr[33]; } catch(ex) { };
	try { fm.all('AppntEMail').value = cArr[34]; } catch(ex) { };
	try { fm.all('AppntFoundDate').value = cArr[35]; } catch(ex) { };
	try { fm.all('AppntBankAccNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('AppntBankCode').value = cArr[37]; } catch(ex) { };
	try { fm.all('AppntGrpGroupNo').value = cArr[38]; } catch(ex) { };
	try { fm.all('AppntState').value = cArr[39]; } catch(ex) { };
	try { fm.all('AppntRemark').value = cArr[40]; } catch(ex) { };
	try { fm.all('AppntBlacklistFlag').value = cArr[41]; } catch(ex) { };
	try { fm.all('AppntOperator').value = cArr[42]; } catch(ex) { };
	try { fm.all('AppntMakeDate').value = cArr[43]; } catch(ex) { };
	try { fm.all('AppntMakeTime').value = cArr[44]; } catch(ex) { };
	try { fm.all('AppntModifyDate').value = cArr[45]; } catch(ex) { };
	try { fm.all('AppntModifyTime').value = cArr[46]; } catch(ex) { };
	try { fm.all('AppntFIELDNUM').value = cArr[47]; } catch(ex) { };
	try { fm.all('AppntPK').value = cArr[48]; } catch(ex) { };
	try { fm.all('AppntfDate').value = cArr[49]; } catch(ex) { };
	try { fm.all('AppntmErrors').value = cArr[50]; } catch(ex) { };
}

/*********************************************************************
 *  把保单中的被保人数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolInsured(cArr)
{
	// 从LCInsured表取数据
	try { fm.all('ContNo').value=cArr[1];} catch(ex){};
	//alert("contno mm:"+fm.all('ContNo').value);
	try { fm.all('CustomerNo').value = cArr[2]; } catch(ex) { };
	try { fm.all('SequenceNo').value = cArr[11]; } catch(ex) { };
	//try { fm.all('InsuredGrade').value = cArr[3]; } catch(ex) { };
	try { fm.all('RelationToInsured').value = cArr[8]; } catch(ex) { };
	//try { fm.all('Password').value = cArr[5]; } catch(ex) { };
	try { fm.all('Name').value = cArr[12]; } catch(ex) { };
	try { fm.all('Sex').value = cArr[13]; } catch(ex) { };
	try { fm.all('Birthday').value = cArr[14]; } catch(ex) { };
	try { fm.all('NativePlace').value = cArr[17]; } catch(ex) { };
	try { fm.all('Nationality').value = cArr[18]; } catch(ex) { };
	try { fm.all('Marriage').value = cArr[20]; } catch(ex) { };
	try { fm.all('MarriageDate').value = cArr[21]; } catch(ex) { };
	try { fm.all('OccupationType').value = cArr[34]; } catch(ex) { };
	try { fm.all('StartWorkDate').value = cArr[31]; } catch(ex) { };
	try { fm.all('Salary').value = cArr[33]; } catch(ex) { };
	try { fm.all('Health').value = cArr[22]; } catch(ex) { };
	try { fm.all('Stature').value = cArr[23]; } catch(ex) { };
	try { fm.all('Avoirdupois').value = cArr[24]; } catch(ex) { };
	try { fm.all('CreditGrade').value = cArr[26]; } catch(ex) { };
	try { fm.all('IDType').value = cArr[15]; } catch(ex) { };
	//try { fm.all('Proterty').value = cArr[21]; } catch(ex) { };
	try { fm.all('IDNo').value = cArr[16]; } catch(ex) { };
	//try { fm.all('OthIDType').value = cArr[23]; } catch(ex) { };
	//try { fm.all('OthIDNo').value = cArr[24]; } catch(ex) { };
	//try { fm.all('ICNo').value = cArr[25]; } catch(ex) { };
	//try { fm.all('HomeAddressCode').value = cArr[26]; } catch(ex) { };
	//try { fm.all('HomeAddress').value = cArr[27]; } catch(ex) { };
	//try { fm.all('PostalAddress').value = cArr[28]; } catch(ex) { };
	//try { fm.all('ZipCode').value = cArr[29]; } catch(ex) { };
	//try { fm.all('Phone').value = cArr[30]; } catch(ex) { };
	//try { fm.all('BP').value = cArr[31]; } catch(ex) { };
	//try { fm.all('Mobile').value = cArr[32]; } catch(ex) { };
	//try { fm.all('EMail').value = cArr[33]; } catch(ex) { };
	//try { fm.all('JoinCompanyDate').value = cArr[34]; } catch(ex) { };
	//try { fm.all('Position').value = cArr[35]; } catch(ex) { };
	//try { fm.all('GrpNo').value = cArr[4]; } catch(ex) { };
	//try { fm.all('GrpName').value = cArr[37]; } catch(ex) { };
	//try { fm.all('GrpPhone').value = cArr[38]; } catch(ex) { };
	//try { fm.all('GrpAddressCode').value = cArr[39]; } catch(ex) { };
	//try { fm.all('GrpAddress').value = cArr[40]; } catch(ex) { };
	//try { fm.all('DeathDate').value = cArr[41]; } catch(ex) { };
	//try { fm.all('State').value = cArr[43]; } catch(ex) { };
	try { fm.all('WorkType').value = cArr[36]; } catch(ex) { };
	try { fm.all('PluralityType').value = cArr[37]; } catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[35]; } catch(ex) { };
	try { fm.all('Degree').value = cArr[25]; } catch(ex) { };
	//try { fm.all('GrpZipCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('SmokeFlag').value = cArr[38]; } catch(ex) { };
	try { fm.all('RgtAddress').value = cArr[19]; } catch(ex) { };
	//try { fm.all('HomeZipCode').value = cArr[53]; } catch(ex) { };
	//try { fm.all('Phone2').value = cArr[54]; } catch(ex) { };
	return;

}

/*********************************************************************
 *  把查询返回的客户数据显示到连带被保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displaySubInsured()
{
	fm.all( spanObj ).all( 'SubInsuredGrid1' ).value = arrResult[0][0];
	fm.all( spanObj ).all( 'SubInsuredGrid2' ).value = arrResult[0][2];
	fm.all( spanObj ).all( 'SubInsuredGrid3' ).value = arrResult[0][3];
	fm.all( spanObj ).all( 'SubInsuredGrid4' ).value = arrResult[0][4];
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {
	if( arrQueryResult != null ) {
		arrResult = arrQueryResult;

		if( mOperate == 1 )	{           // 查询保单明细
			var tPolNo = arrResult[0][0];
			// 查询保单明细

			queryPolDetail( tPolNo );
		}

		if( mOperate == 2 ) {		// 投保人信息
			arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
			if (arrResult == null) {
			  alert("未查到投保人信息");
			} else {
			   displayAppnt(arrResult[0]);
			}

	  }
		if( mOperate == 3 )	{		// 主被保人信息
			arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LDPerson a Left Join LCAddress b On b.CustomerNo =a.CustomerNo Where 1=1 and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
			if (arrResult == null) {
			  alert("未查到主被保人信息");
			} else {
			   displayInsured(arrResult[0]);
			}

	  }
		if( mOperate == 4 )	{		// 连带被保人信息
			displaySubInsured(arrResult[0]);
	  }
	}
	mOperate = 0;		// 恢复初态

	emptyUndefined();
}

/*********************************************************************
 *  根据查询返回的信息查询投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPolDetail( cPolNo )
{
	emptyForm();
	//var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	//var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

	//showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//parent.fraSubmit.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;

	parent.fraTitle.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;				
}


/*********************************************************************
 *  根据查询返回的信息查询险种的保险计划明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryRiskPlan( tProposalGrpContNo,tRiskCode,tContPlanCode,tMainRiskCode )
{
	emptyForm();
	//var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	//var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//alert("./ProposalQueryRiskPlan.jsp?ProposalGrpContNo="
	//																		+ tProposalGrpContNo+"&RiskCode="+tRiskCode+"&ContPlanCode="+tContPlanCode);
	//showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//parent.fraSubmit.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;
	parent.fraSubmit.window.location = "./ProposalQueryRiskPlan.jsp?ProposalGrpContNo="
																			+ tProposalGrpContNo+"&RiskCode="+tRiskCode+"&ContPlanCode="+tContPlanCode+"&MainRiskCode="+tMainRiskCode;
}
/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";
}

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//*************************************************************
//被保人客户号查询按扭事件
function queryInsuredNo() {
  if (fm.all("CustomerNo").value == "") {
    showInsured1();
  //} else if (LoadFlag != "1" && LoadFlag != "2") {
  //  alert("只能在投保单录入时进行操作！");
  }  else {
      arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LDPerson a Left Join LCAddress b On b.CustomerNo =a.CustomerNo Where 1=1 and a.CustomerNo = '" + fm.all("CustomerNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到主被保人信息");
      displayInsured(new Array());
      emptyUndefined();
    } else {

      displayInsured(arrResult[0]);
    }
  }
}

//*************************************************************
//投保人客户号查询按扭事件
function queryAppntNo() {
  if (fm.all("AppntCustomerNo").value == "" && LoadFlag == "1") {
    showAppnt1();
  //} else if (LoadFlag != "1" && LoadFlag != "2") {
  //  alert("只能在投保单录入时进行操作！");
  } else {
    arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LDPerson a Left Join LCAddress b On b.CustomerNo =a.CustomerNo Where 1=1 and a.CustomerNo = '" + fm.all("AppntCustomerNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到投保人信息");
      displayAppnt(new Array());
      emptyUndefined();
    } else {
      displayAppnt(arrResult[0]);
    }
  }
}

//*************************************************************
//投保人与被保人相同选择框事件
function isSamePerson() {
  //对应未选同一人，又打钩的情况
  if (fm.AppntRelationToInsured.value!="00" && fm.SamePersonFlag.checked==true) {
    fm.all('DivLCInsured').style.display = "";
    fm.SamePersonFlag.checked = false;
    alert("投保人与被保人关系不是本人，不能进行该操作！");
  }
  //对应是同一人，又打钩的情况
  else if (fm.SamePersonFlag.checked == true) {
    fm.all('DivLCInsured').style.display = "none";
  }
  //对应不选同一人的情况
  else if (fm.SamePersonFlag.checked == false) {
    fm.all('DivLCInsured').style.display = "";
  }

  for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {
	  if (fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
	    try {
	      insuredName = fm.elements[elementsNum].name.substring(fm.elements[elementsNum].name.indexOf("t") + 1);
	      if (fm.all('DivLCInsured').style.display == "none") {
	        fm.all(insuredName).value = fm.elements[elementsNum].value;
	      }
	      else {
	        fm.all(insuredName).value = "";
	      }
	    }
	    catch (ex) {}
	  }
	}

}

//*************************************************************
//保存时校验投保人与被保人相同选择框事件
function verifySamePerson() {
  if (fm.SamePersonFlag.checked == true) {
    for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {
  	  if (fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
  	    try {
  	      insuredName = fm.elements[elementsNum].name.substring(fm.elements[elementsNum].name.indexOf("t") + 1);
  	      if (fm.all('DivLCInsured').style.display == "none") {
  	        fm.all(insuredName).value = fm.elements[elementsNum].value;
  	      }
  	      else {
  	        fm.all(insuredName).value = "";
  	      }
  	    }
  	    catch (ex) {}
  	  }
	  }
  }
  else if (fm.SamePersonFlag.checked == false) {

  }

}


/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt()
{
	// 从LDPerson表取数据
try{fm.all('AppntCustomerNo').value= arrResult[0][0]; }catch(ex){};
try{fm.all('AppntName').value= arrResult[0][1]; }catch(ex){};
try{fm.all('AppntSex').value= arrResult[0][2]; }catch(ex){};
try{fm.all('AppntBirthday').value= arrResult[0][3]; }catch(ex){};
try{fm.all('AppntIDType').value= arrResult[0][4]; }catch(ex){};
try{fm.all('AppntIDNo').value= arrResult[0][5]; }catch(ex){};
try{fm.all('AppntPassword').value= arrResult[0][6]; }catch(ex){};
try{fm.all('AppntNativePlace').value= arrResult[0][7]; }catch(ex){};
try{fm.all('AppntNationality').value= arrResult[0][8]; }catch(ex){};
try{fm.all('AppntRgtAddress').value= arrResult[0][9]; }catch(ex){};
try{fm.all('AppntMarriage').value= arrResult[0][10];}catch(ex){};
try{fm.all('AppntMarriageDate').value= arrResult[0][11];}catch(ex){};
try{fm.all('AppntHealth').value= arrResult[0][12];}catch(ex){};
try{fm.all('AppntStature').value= arrResult[0][13];}catch(ex){};
try{fm.all('AppntAvoirdupois').value= arrResult[0][14];}catch(ex){};
try{fm.all('AppntDegree').value= arrResult[0][15];}catch(ex){};
try{fm.all('AppntCreditGrade').value= arrResult[0][16];}catch(ex){};
try{fm.all('AppntOthIDType').value= arrResult[0][17];}catch(ex){};
try{fm.all('AppntOthIDNo').value= arrResult[0][18];}catch(ex){};
try{fm.all('AppntICNo').value= arrResult[0][19];}catch(ex){};
try{fm.all('AppntGrpNo').value= arrResult[0][20];}catch(ex){};
try{fm.all('AppntJoinCompanyDate').value= arrResult[0][21];}catch(ex){};
try{fm.all('AppntStartWorkDate').value= arrResult[0][22];}catch(ex){};
try{fm.all('AppntPosition').value= arrResult[0][23];}catch(ex){};
try{fm.all('AppntSalary').value= arrResult[0][24];}catch(ex){};
try{fm.all('AppntOccupationType').value= arrResult[0][25];}catch(ex){};
try{fm.all('AppntOccupationCode').value= arrResult[0][26];}catch(ex){};
try{fm.all('AppntWorkType').value= arrResult[0][27];}catch(ex){};
try{fm.all('AppntPluralityType').value= arrResult[0][28];}catch(ex){};
try{fm.all('AppntDeathDate').value= arrResult[0][29];}catch(ex){};
try{fm.all('AppntSmokeFlag').value= arrResult[0][30];}catch(ex){};
try{fm.all('AppntBlacklistFlag').value= arrResult[0][31];}catch(ex){};
try{fm.all('AppntProterty').value= arrResult[0][32];}catch(ex){};
try{fm.all('AppntRemark').value= arrResult[0][33];}catch(ex){};
try{fm.all('AppntState').value= arrResult[0][34];}catch(ex){};
try{fm.all('AppntOperator').value= arrResult[0][35];}catch(ex){};
try{fm.all('AppntMakeDate').value= arrResult[0][36];}catch(ex){};
try{fm.all('AppntMakeTime').value= arrResult[0][37];}catch(ex){};
try{fm.all('AppntModifyDate').value= arrResult[0][38];}catch(ex){};
try{fm.all('AppntModifyTime').value= arrResult[0][39];}catch(ex){};
try{fm.all('AppntGrpName').value= arrResult[0][40];}catch(ex){};
try{fm.all('AppntHomeAddress').value= arrResult[0][41];}catch(ex){};
try{fm.all('AppntHomeZipCode').value= arrResult[0][42];}catch(ex){};
try{fm.all('AppntPhone').value= arrResult[0][43];}catch(ex){};
try{fm.all('AppntPhone2').value= arrResult[0][44];}catch(ex){};
}

/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  集体客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayAddress1()
{
try {fm.all('GrpNo').value= arrResult[0][0]; } catch(ex) { };
try {fm.all('GrpCustomerNo').value= arrResult[0][0]; } catch(ex) { };
try {fm.all('AddressNo').value= arrResult[0][1]; } catch(ex) { };
try {fm.all('AppGrpAddress').value= arrResult[0][2]; } catch(ex) { };
try {fm.all('AppGrpZipCode').value= arrResult[0][3]; } catch(ex) { };
try {fm.all('LinkMan1').value= arrResult[0][4]; } catch(ex) { };
try {fm.all('Department1').value= arrResult[0][5]; } catch(ex) { };
try {fm.all('HeadShip1').value= arrResult[0][6]; } catch(ex) { };
try {fm.all('GrpPhone1').value= arrResult[0][7]; } catch(ex) { };
try {fm.all('E_Mail1').value= arrResult[0][8]; } catch(ex) { };
try {fm.all('Fax1').value= arrResult[0][9]; } catch(ex) { };
try {fm.all('LinkMan2').value= arrResult[0][10]; } catch(ex) { };
try {fm.all('Department2').value= arrResult[0][11]; } catch(ex) { };
try {fm.all('HeadShip2').value= arrResult[0][12]; } catch(ex) { };
try {fm.all('GrpPhone2').value= arrResult[0][13]; } catch(ex) { };
try {fm.all('E_Mail2').value= arrResult[0][14]; } catch(ex) { };
try {fm.all('Fax2').value= arrResult[0][15]; } catch(ex) { };
try {fm.all('Operator').value= arrResult[0][16]; } catch(ex) { };
try {fm.all('MakeDate').value= arrResult[0][17]; } catch(ex) { };
try {fm.all('MakeTime').value= arrResult[0][18]; } catch(ex) { };
try {fm.all('ModifyDate').value= arrResult[0][19]; } catch(ex) { };
try {fm.all('ModifyTime').value= arrResult[0][20]; } catch(ex) { };
//以下是ldgrp表
try {fm.all('BusinessType').value= arrResult[0][22];} catch(ex) { };
try {fm.all('GrpNature').value= arrResult[0][23]; } catch(ex) { };
try {fm.all('Peoples').value= arrResult[0][24]; } catch(ex) { };
try {fm.all('RgtMoney').value= arrResult[0][25]; } catch(ex) { };
try {fm.all('Asset').value= arrResult[0][26]; } catch(ex) { };
try {fm.all('NetProfitRate').value= arrResult[0][27];} catch(ex) { };
try {fm.all('MainBussiness').value= arrResult[0][28];} catch(ex) { };
try {fm.all('Corporation').value= arrResult[0][29];  } catch(ex) { };
try {fm.all('ComAera').value= arrResult[0][30]; } catch(ex) { };
try {fm.all('Fax').value= arrResult[0][31]; } catch(ex) { };
try {fm.all('Phone').value= arrResult[0][32]; } catch(ex) { };
try {fm.all('FoundDate').value= arrResult[0][33]; } catch(ex) { };
try {fm.all('AppGrpNo').value= arrResult[0][34]; } catch(ex) { };
try {fm.all('AppGrpName').value= arrResult[0][35]; } catch(ex) { };
}
function displayAppntGrp( cArr )
{
	// 从LDGrp表取数据
	try { fm.all('AppGrpNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('Password').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppGrpName').value = cArr[2]; } catch(ex) { };
	try { fm.all('GrpAddressCode').value = cArr[3]; } catch(ex) { };
	try { fm.all('AppGrpAddress').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppGrpZipCode').value = cArr[5]; } catch(ex) { };
	try { fm.all('BusinessType').value = cArr[6]; } catch(ex) { };
	try { fm.all('GrpNature').value = cArr[7]; } catch(ex) { };
	try { fm.all('Peoples').value = cArr[8]; } catch(ex) { };
	try { fm.all('RgtMoney').value = cArr[9]; } catch(ex) { };
	try { fm.all('Asset').value = cArr[10]; } catch(ex) { };
	try { fm.all('NetProfitRate').value = cArr[11]; } catch(ex) { };
	try { fm.all('MainBussiness').value = cArr[12]; } catch(ex) { };
	try { fm.all('Corporation').value = cArr[13]; } catch(ex) { };
	try { fm.all('ComAera').value = cArr[14]; } catch(ex) { };
	try { fm.all('LinkMan1').value = cArr[15]; } catch(ex) { };
	try { fm.all('Department1').value = cArr[16]; } catch(ex) { };
	try { fm.all('HeadShip1').value = cArr[17]; } catch(ex) { };
	try { fm.all('Phone1').value = cArr[18]; } catch(ex) { };
	try { fm.all('E_Mail1').value = cArr[19]; } catch(ex) { };
	try { fm.all('Fax1').value = cArr[20]; } catch(ex) { };
	try { fm.all('LinkMan2').value = cArr[21]; } catch(ex) { };
	try { fm.all('Department2').value = cArr[22]; } catch(ex) { };
	try { fm.all('HeadShip2').value = cArr[23]; } catch(ex) { };
	try { fm.all('Phone2').value = cArr[24]; } catch(ex) { };
	try { fm.all('E_Mail2').value = cArr[25]; } catch(ex) { };
	try { fm.all('Fax2').value = cArr[26]; } catch(ex) { };
	try { fm.all('Fax').value = cArr[27]; } catch(ex) { };
	try { fm.all('Phone').value = cArr[28]; } catch(ex) { };
	try { fm.all('GetFlag').value = cArr[29]; } catch(ex) { };
	try { fm.all('Satrap').value = cArr[30]; } catch(ex) { };
	try { fm.all('EMail').value = cArr[31]; } catch(ex) { };
	try { fm.all('FoundDate').value = cArr[32]; } catch(ex) { };
	try { fm.all('BankAccNo').value = cArr[33]; } catch(ex) { };
	try { fm.all('BankCode').value = cArr[34]; } catch(ex) { };
	try { fm.all('GrpGroupNo').value = cArr[35]; } catch(ex) { };
	try { fm.all('State').value = cArr[36]; } catch(ex) { };
}

/*********************************************************************
 *  把查询返回的客户数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayInsured()
{
	// 从LDPerson表取数据
    try{fm.all('GrpContNo').value=arrResult[0][0];}catch(ex){};
    try{fm.all('ContNo').value=arrResult[0][1];}catch(ex){};
    try{fm.all('InsuredNo').value=arrResult[0][2];}catch(ex){};
    try{fm.all('PrtNo').value=arrResult[0][3];}catch(ex){};
    try{fm.all('AppntNo').value=arrResult[0][4];}catch(ex){};
    try{fm.all('ManageCom').value=arrResult[0][5];}catch(ex){};
    try{fm.all('ExecuteCom').value=arrResult[0][6];}catch(ex){};
    try{fm.all('FamilyID').value=arrResult[0][7];}catch(ex){};
    try{fm.all('RelationToMainInsured').value=arrResult[0][8];}catch(ex){};
    try{fm.all('RelationToAppnt').value=arrResult[0][9];}catch(ex){};
    try{fm.all('AddressNo').value=arrResult[0][10];}catch(ex){};
    try{fm.all('SequenceNo').value=arrResult[0][11];}catch(ex){};
    try{fm.all('Name').value=arrResult[0][12];}catch(ex){};
    try{fm.all('Sex').value=arrResult[0][13];}catch(ex){};
    try{fm.all('Birthday').value=arrResult[0][14];}catch(ex){};
    try{fm.all('IDType').value=arrResult[0][15];}catch(ex){};
    try{fm.all('IDNo').value=arrResult[0][16];}catch(ex){};
    try{fm.all('NativePlace').value=arrResult[0][17];}catch(ex){};
    try{fm.all('Nationality').value=arrResult[0][18];}catch(ex){};
    try{fm.all('RgtAddress').value=arrResult[0][19];}catch(ex){};
    try{fm.all('Marriage').value=arrResult[0][20];}catch(ex){};
    try{fm.all('MarriageDate').value=arrResult[0][21];}catch(ex){};
    try{fm.all('Health').value=arrResult[0][22];}catch(ex){};
    try{fm.all('Stature').value=arrResult[0][23];}catch(ex){};
    try{fm.all('Avoirdupois').value=arrResult[0][24];}catch(ex){};
    try{fm.all('Degree').value=arrResult[0][25];}catch(ex){};
    try{fm.all('CreditGrade').value=arrResult[0][26];}catch(ex){};
    try{fm.all('BankCode').value=arrResult[0][27];}catch(ex){};
    try{fm.all('BankAccNo').value=arrResult[0][28];}catch(ex){};
    try{fm.all('AccName').value=arrResult[0][29];}catch(ex){};
    try{fm.all('JoinCompanyDate').value=arrResult[0][30];}catch(ex){};
    try{fm.all('StartWorkDate').value=arrResult[0][31];}catch(ex){};
    try{fm.all('Position').value=arrResult[0][32];}catch(ex){};
    try{fm.all('Salary').value=arrResult[0][33];}catch(ex){};
    try{fm.all('OccupationType').value=arrResult[0][34];}catch(ex){};
    try{fm.all('OccupationCode').value=arrResult[0][35];}catch(ex){};
    try{fm.all('WorkType').value=arrResult[0][36];}catch(ex){};
    try{fm.all('PluralityType').value=arrResult[0][37];}catch(ex){};
    try{fm.all('SmokeFlag').value=arrResult[0][38];}catch(ex){};
    try{fm.all('ContPlanCode').value=arrResult[0][39];}catch(ex){};
    try{fm.all('Operator').value=arrResult[0][40];}catch(ex){};
    try{fm.all('InsuredStat').value=arrResult[0][41];}catch(ex){};
    try{fm.all('MakeDate').value=arrResult[0][42];}catch(ex){};
    try{fm.all('MakeTime').value=arrResult[0][43];}catch(ex){};
    try{fm.all('ModifyDate').value=arrResult[0][44];}catch(ex){};
    try{fm.all('ModifyTime').value=arrResult[0][45];}catch(ex){};
    try{fm.all('GrpName').value= arrResult[0][46];}catch(ex){};
    try{fm.all('HomeAddress').value= arrResult[0][47];}catch(ex){};
    try{fm.all('HomeZipCode').value= arrResult[0][48];}catch(ex){};
    try{fm.all('Phone').value= arrResult[0][49];}catch(ex){};
    try{fm.all('Phone2').value= arrResult[0][50];}catch(ex){};
    //alert("joindate:"+fm.all('JoinCompanyDate').value);
    //alert("grpcontno:"+fm.all('GrpContNo').value);
}

//*********************************************************************
function showAppnt1()
{
	if( mOperate == 0 )
	{
		mOperate = 2;
		showInfo = window.open( "../sys/LDPersonQuery.html" );
	}
}

//*********************************************************************
function showInsured1()
{
	if( mOperate == 0 )
	{
		mOperate = 3;
		showInfo = window.open( "../sys/LDPersonQuery.html" );
	}
}

function isSamePersonQuery() {
  fm.SamePersonFlag.checked = true;
  //divSamePerson.style.display = "none";
  DivLCInsured.style.display = "none";
}

//问题件录入
function QuestInput()
{
	//if(inputQuestButton.disabled == true)
	  // return;
	cContNo = fm.all("ContNo").value;  //保单号码
		if(LoadFlag=="2"||LoadFlag=="4"){
			if(mSwitch.getVar( "ProposalGrpContNo" )==""){
			  alert("尚无集体合同投保单号，请先保存!");
		        }
		        else{
			window.open("./GrpQuestInputMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+LoadFlag);
	                }
	        }
	        else{
	                if(cContNo == ""){
		                  alert("尚无合同投保单号，请先保存!");
	                 }
	                 else
	                {
	                		if(prtNo.substring(0,4)=="1001")
	                    {
		                    window.open("../uw/QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+ LoadFlag+"&Page=13","window1");
		                  }
	                		if(prtNo.substring(0,4)=="1003")
	                    {
		                    window.open("../uw/QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+ LoadFlag+"&Page=33","window1");
		                  }
	                		if(prtNo.substring(0,4)=="1004")
	                    {
		                    window.open("../uw/QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+ LoadFlag+"&Page=43","window1");
		                  }
	                }
	        }
}
function QuestQuery()
{
   cContNo = fm.all("ContNo").value;  //保单号码
   if(LoadFlag=="2"||LoadFlag=="4"||LoadFlag=="13"){
	if(mSwitch.getVar( "ProposalGrpContNo" )==""||mSwitch.getVar( "ProposalGrpContNo" )==null)
	{
  		alert("请先选择一个团体主险投保单!");
  		return ;
        }
        else{
	        window.open("./GrpQuestQueryMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+LoadFlag);
        }
   }
   else{
        if(cContNo == ""){
	       alert("尚无合同投保单号，请先保存!");
	 }
	else{
               window.open("../uw/QuestQueryMain.jsp?ContNo="+cContNo+"&Flag="+LoadFlag,"window1");
        }
   }
}
//显示投保人年龄
function showAppntAge() {
  var age = calAge(fm.all("AppntBirthday").value);
  var today = new Date();

  fm.all("AppntBirthday").title = "投保人到今天 " + today.toLocaleString()
                                + " \n的年龄为：" + age + " 岁!";
}

//显示被保人年龄
function showAge() {
  var age = calAge(fm.all("Birthday").value);
  var today = new Date();

  fm.all("Birthday").title = "被保人到今天 " + today.toLocaleString()
                           + " \n的年龄为：" + age + " 岁!";
}

//检验投保人出生日期，如果空，且身份证号有，则从身份证取。取不到返回空格;
function checkBirthday()
{
	try{
		  var strBrithday = "";
		  if(trim(fm.all("AppntBirthday").value)==""||fm.all("AppntBirthday").value==null)
		  {
		  	if (trim(fm.all("AppntIDType").value) == "0")
		  	 {
		  	   strBrithday=	getBirthdatByIdNo(fm.all("AppntIDNo").value);
		  	   if(strBrithday=="") passVerify=false;

	           fm.all("AppntBirthday").value= strBrithday;
		  	 }
	      }
	 }
	 catch(e)
	 {

	 }
}

//校验录入的险种是否不需要校验，如果需要返回true,否则返回false
function needVerifyRiskcode()
{

  try {
  	   var riskcode=fm.all("RiskCode").value;

       var tSql = "select Sysvarvalue from LDSysVar where Sysvar='NotVerifyRiskcode'";
       var tResult = easyExecSql(tSql, 1, 1, 1);
       var strRiskcode = tResult[0][0];
       var strValue=strRiskcode.split("/");
       var i=0;
	   while(i<strValue.length)
	   {
	   	if(riskcode==strValue[i])
	   	{
           return false;
	   	}
	   	i++;
	   }
  	 }
  	catch(e)
  	 {}

  	return true;


}





/*********************************************************************
 *  把保单数组中的数据显示到特殊的险种信息显示部分-121301,
 *  参数  ：  保单的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolSpec(cArr)
{
	try
	{

	    try { fm.all('PayEndYear').value = cArr[109]; } catch(ex) { };
	    try { fm.all('PayEndYearFlag').value = cArr[108]; } catch(ex) { };
	    try { fm.all('PayIntv').value = cArr[57]; } catch(ex) { };
	    try { fm.all('Amnt').value = cArr[39]; } catch(ex) { };	    //主险的保费即附险的保额

	} catch(ex) {
	  alert("displayPolSpec err:" + ex + "\ndata is:" + cArr);
	}
}





/*********************************************************************
 *  特殊险种处理：把主保单中的投保人数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolInsuredSpec(cArr)
{
		// 从LCAppntInd表取数据
	try { fm.all('CustomerNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('Password').value = cArr[5]; } catch(ex) { };
	try { fm.all('Name').value = cArr[6]; } catch(ex) { };
	try { fm.all('Sex').value = cArr[7]; } catch(ex) { };
	try { fm.all('Birthday').value = cArr[8]; } catch(ex) { };
	try { fm.all('NativePlace').value = cArr[9]; } catch(ex) { };
	try { fm.all('Nationality').value = cArr[10]; } catch(ex) { };
	try { fm.all('Marriage').value = cArr[11]; } catch(ex) { };
	try { fm.all('MarriageDate').value = cArr[12]; } catch(ex) { };
	try { fm.all('OccupationType').value = cArr[13]; } catch(ex) { };
	try { fm.all('StartWorkDate').value = cArr[14]; } catch(ex) { };
	try { fm.all('Salary').value = cArr[15]; } catch(ex) { };
	try { fm.all('Health').value = cArr[16]; } catch(ex) { };
	try { fm.all('Stature').value = cArr[17]; } catch(ex) { };
	try { fm.all('Avoirdupois').value = cArr[18]; } catch(ex) { };
	try { fm.all('CreditGrade').value = cArr[19]; } catch(ex) { };
	try { fm.all('IDType').value = cArr[20]; } catch(ex) { };
	try { fm.all('Proterty').value = cArr[21]; } catch(ex) { };
	try { fm.all('IDNo').value = cArr[22]; } catch(ex) { };
	try { fm.all('OthIDType').value = cArr[23]; } catch(ex) { };
	try { fm.all('OthIDNo').value = cArr[24]; } catch(ex) { };
	try { fm.all('ICNo').value = cArr[25]; } catch(ex) { };
	try { fm.all('HomeAddressCode').value = cArr[26]; } catch(ex) { };
	try { fm.all('HomeAddress').value = cArr[27]; } catch(ex) { };
	try { fm.all('PostalAddress').value = cArr[28]; } catch(ex) { };
	try { fm.all('ZipCode').value = cArr[29]; } catch(ex) { };
	try { fm.all('Phone').value = cArr[30]; } catch(ex) { };
	try { fm.all('BP').value = cArr[31]; } catch(ex) { };
	try { fm.all('Mobile').value = cArr[32]; } catch(ex) { };
	try { fm.all('EMail').value = cArr[33]; } catch(ex) { };
	//try { fm.all('BankCode').value = cArr[34]; } catch(ex) { };
	//try { fm.all('BankAccNo').value = cArr[35]; } catch(ex) { };
	try { fm.all('JoinCompanyDate').value = cArr[34]; } catch(ex) { };
	try { fm.all('Position').value = cArr[35]; } catch(ex) { };
	try { fm.all('GrpNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('GrpName').value = cArr[37]; } catch(ex) { };
	try { fm.all('GrpPhone').value = cArr[38]; } catch(ex) { };
	try { fm.all('GrpAddressCode').value = cArr[39]; } catch(ex) { };
	try { fm.all('GrpAddress').value = cArr[40]; } catch(ex) { };
	try { fm.all('DeathDate').value = cArr[41]; } catch(ex) { };
	try { fm.all('State').value = cArr[43]; } catch(ex) { };
	try { fm.all('WorkType').value = cArr[46]; } catch(ex) { };
	try { fm.all('PluralityType').value = cArr[47]; } catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[49]; } catch(ex) { };
	try { fm.all('Degree').value = cArr[48]; } catch(ex) { };
	try { fm.all('GrpZipCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('SmokeFlag').value = cArr[51]; } catch(ex) { };
	try { fm.all('RgtAddress').value = cArr[52]; } catch(ex) { };

}


///险种页面数据提交时对特殊险种的特殊处理
//function specDealByRisk()
//{
//	//如果是同心卡-阳光旅程险
//	if(fm.all('RiskCode').value=='311603')
//	{
//	   if(trim(fm.all("AppntBirthday").value)==""||fm.all("AppntBirthday").value==null)
//	   {
//	   	if (trim(fm.all("AppntIDType").value) != "0"||fm.all("AppntIDNo").value==null||trim(fm.all("AppntIDNo").value)=="")
//	   	{
//	   		fm.all("AppntBirthday").value='1970-1-1';
//	   	}
//	   }
//
//		try
//		{
//			  var strBrithday = "";
//			  if(trim(fm.all("Birthday").value)==""||fm.all("Birthday").value==null)
//			  {
//			  	if (trim(fm.all("IDType").value) == "0")
//			  	 {
//			  	   strBrithday=	getBirthdatByIdNo(fm.all("IDNo").value);
//			  	   if(strBrithday=="") passVerify=false;
//
//		           fm.all("Birthday").value= strBrithday;
//			  	 }
//		      }
//		 }
//		 catch(e)
//		 {
//		}
//		return true;
//	}
//	//如果是团体商业补充医疗险
//	if(fm.all('RiskCode').value=='211801')
//	{
//		//可以对险种条件校验
//		var strChooseDuty="";
//		for(i=0;i<=8;i++)
//		{
//			if(DutyGrid.getChkNo(i)==true)
//			{
//				strChooseDuty=strChooseDuty+"1";
//				DutyGrid.setRowColData(i, 5, fm.all('PayEndYear').value);//交费年期
//				DutyGrid.setRowColData(i, 6, fm.all('PayEndYearFlag').value);//交费年期单位
//				DutyGrid.setRowColData(i, 9, fm.all('InsuYear').value);//保险年期
//				DutyGrid.setRowColData(i, 10, fm.all('InsuYearFlag').value);//保险年期单位
//				DutyGrid.setRowColData(i, 11, fm.all('PayIntv').value);//缴费方式
//			}
//			else
//			{
//				strChooseDuty=strChooseDuty+"0";
//			}
//		}
//		//alert(strChooseDuty);
//		//fm.all('StandbyFlag1').value=strChooseDuty;
//		return true;
//	}
//	//如果是民生基业长青员工福利计划 add by guoxiang 2004-9-8 10:24
//	if(fm.all('RiskCode').value=='211701')
//	{
//		//可以对险种条件校验
//		var strChooseDuty="";
//		for(i=0;i<=2;i++)
//		{
//			if(DutyGrid.getChkNo(i)==true)
//			{
//				strChooseDuty=strChooseDuty*1+1.0;
//                DutyGrid.setRowColData(i, 3, fm.all('Prem').value);//保费
//				DutyGrid.setRowColData(i, 9, fm.all('InsuYear').value);//保险年期
//				DutyGrid.setRowColData(i, 10, fm.all('InsuYearFlag').value);//保险年期单位
//				DutyGrid.setRowColData(i, 11, fm.all('PayIntv').value);//缴费方式
//				DutyGrid.setRowColData(i, 12, fm.all('ManageFeeRate').value);//管理费比例
//
//			}
//			else
//			{
//				strChooseDuty=strChooseDuty*1+0.0;
//			}
//		}
//		if(strChooseDuty>1){
//		   alert("基业长青员工福利每张保单只允许选择一个责任，您选择的责任次数为"+strChooseDuty+"，请修改！！！");
//		   return false;
//	    }
//		return true;
//	}
//
//	//如果是个人长瑞险
//	if(fm.all('RiskCode').value=='112401')
//	{
//		if(fm.all('GetYear').value!=''&&fm.all('InsuYear').value!='')
//		{
//		  	if(fm.all('InsuYear').value=='A')
//		  	{
//		  		fm.all('InsuYear').value='88';
//		  		fm.all('InsuYearFlag').value='A';
//		  	}
//		  	else if(fm.all('InsuYear').value=='B')
//		  	{
//		  		fm.all('InsuYear').value=20+Number(fm.all('GetYear').value);
//		  		fm.all('InsuYearFlag').value='A';
//		  	}
//		  	else
//		  	{
//		  		alert("保险期间必须选择！");
//		  		return false;
//
//		  	}
//		  	if(fm.all('PayIntv').value=='0')
//		  	{
//		  		fm.all('PayEndYear').value=fm.all('InsuYear').value;
//		  		fm.all('PayEndYearFlag').value=fm.all('InsuYearFlag').value;
//		  	}
//
//		}
//		return true;
//	}
//	//如果是君安行险种
//	if(fm.all('RiskCode').value=='241801')
//	{
//		try
//		{
//			var InsurCount = fm.all('StandbyFlag2').value;
//			if(InsurCount>4||InsurCount<0)
//			{
//				alert("连带被保人人数不能超过4人");
//				return false;
//			}
//			SubInsuredGrid.delBlankLine("SubInsuredGrid");
//			var rowNum=SubInsuredGrid.mulLineCount;
//			if(InsurCount!=rowNum)
//			{
//			    alert("连带被保人人数和多行输入的连带被保人信息的行数不符合！ ");
//				return false;
//			}
//
//	    }
//	    catch(ex)
//	    {
//	      alert(ex);
//	      return false;
//	    }
//	    return true;
//	}
//
//
//}

//险种页面初始化时对特殊险种的特殊处理
function initDealForSpecRisk(cRiskCode)
{
  try{
	//如果是211801
	if(cRiskCode=='211801')
	{
		DutyGrid.addOne();
		DutyGrid.setRowColData(0, 1, '610001');
		DutyGrid.setRowColData(0, 2, '基本责任1档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(1, 1, '610002');
		DutyGrid.setRowColData(1, 2, '基本责任2档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(2, 1, '610003');
		DutyGrid.setRowColData(2, 2, '基本责任3档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(3, 1, '610004');
		DutyGrid.setRowColData(3, 2, '基本责任4档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(4, 1, '610005');
		DutyGrid.setRowColData(4, 2, '基本责任5档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(5, 1, '610006');
		DutyGrid.setRowColData(5, 2, '基本责任6档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(6, 1, '610007');
		DutyGrid.setRowColData(6, 2, '公共责任7档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(7, 1, '610008');
		DutyGrid.setRowColData(7, 2, '公共责任8档');
		DutyGrid.addOne();
		DutyGrid.setRowColData(8, 1, '610009');
		DutyGrid.setRowColData(8, 2, '女员工生育责任');
		DutyGrid.lock();


	}

	//众悦年金
	if(cRiskCode=='212401')
	{

		PremGrid.addOne();
		PremGrid.setRowColData(0, 1, '601001');
		PremGrid.setRowColData(0, 2, '601101');
		PremGrid.setRowColData(0, 3, '集体交费');
		PremGrid.addOne();
		PremGrid.setRowColData(1, 1, '601001');
		PremGrid.setRowColData(1, 2, '601102');
		PremGrid.setRowColData(1, 3, '个人交费');
		PremGrid.lock();

	}

    //基业长青
	if(cRiskCode=='211701')
	{
        var strSql = "select *  from lmdutypayrela where dutycode in  "
	               + " (select dutycode from lmriskduty where riskcode='"+cRiskCode+"')";
	    turnPage.queryModal(strSql, PremGrid);
	    PremGrid.lock;
    }

  }catch(ex) {}

}

/*********************************************************************
* 险种页面初始化时对特殊险种的特殊处理扩展
*  add by guoxiang  at 2004-9-6 16:21
*  for update up function initDealForSpecRisk
*  not write function for every risk
*********************************************************************
 */
function initDealForSpecRiskEx(cRiskCode){
	try{
		var strSql="";
	    if(fm.all('inpNeedDutyGrid').value==1){

		initDutyGrid();  //根据险种初始化多行录入框
         //险种特殊处理    20070531 
         var sType = "";
//         alert(acctype+" "+(acctype=="null"));
//         alert(typeof(acctype)+" "+acctype+" "+(acctype==""));
		//chenwm20070828 添加被保险人后,直接添加险种 acctype值为"null" 一个字符串?
         if(acctype==null||acctype=="null"){
//         	alert("acctype="+acctype);
         	acctype=parent.VD.gVSwitch.getVar("AccType");
//         	alert("acctype="+acctype);
         }
//         alert(cRiskCode+" "+(cRiskCode=="SEK1"|| cRiskCode=="UEK1")+" ("+acctype+")");
         //modified by zhangjq 以下4行，改变"指定账户型产品编码"为"查询数据库是否为账户型产品"
         strSql = "select '1'||a.InsuAccFlag from lmrisk a,lmriskapp b where a.riskcode='"+cRiskCode+"' and a.riskcode=b.riskcode and b.risktype4 in ('3','4')";
         var InsuAccFlagArr = easyExecSql(strSql);
       
         //if(cRiskCode=="SEK1" || cRiskCode=="UEK1")
         if(InsuAccFlagArr != "" && InsuAccFlagArr != null && (InsuAccFlagArr[0]=="1Y" || InsuAccFlagArr[0]=="1y" ))
         {
	         //个人账户
	    
         	if(acctype=="0")
         	{
          		sType = " and exists (select 1 from lmriskduty r ,lmdutypayrela l where l.dutycode=r.dutycode and h.dutycode =r.dutycode and exists(select 1 from lmdutypay h where h.payplancode =l.payplancode and h.AccPayClass in ('4','5')))";    
         	}
	         //公共账户
         	if(acctype=="2")
         	{
          		sType = " and exists (select 1 from lmriskduty r ,lmdutypayrela l where l.dutycode=r.dutycode and h.dutycode =r.dutycode and exists(select 1 from lmdutypay h where h.payplancode =l.payplancode and h.AccPayClass ='3'))";          		
         	}
         }
       	if(acctype=="5")//公共保额
       	{
        		sType = " and addfeeflag='05'";
        		DutyGrid.lock();        		
       	}
              
         strSql = "select dutycode,dutyname,'','','','','','','','','',''  from lmduty h where dutycode in "
	               + " (select dutycode from lmriskduty where riskcode='"+cRiskCode+"' and choflag!='B')" + sType;
//	     alert("sql:"+strSql);
	     turnPage.queryModal(strSql, DutyGrid,"2","",50);
	       if(scantype=="scan")
			  {
			    setFocus();
			  } 

	     var cDutyCode="";
	     var tSql="";
	     for(var i=0;i<=DutyGrid.mulLineCount-1;i++){
	       cDutyCode=DutyGrid.getRowColData(i,1);
	       tSql="select choflag from lmriskduty where riskcode='"+cRiskCode+"' and dutycode='"+cDutyCode+"'";
	       
	       var arrResult=easyExecSql(tSql,1,0);
	       //alert("ChoFlag:"+arrResult[0]);
	       if(arrResult[0]=="M"){
	       	 DutyGrid.checkBoxSel(i+1);
	       }
	     }
	     DutyGrid.lock;

      }
        if(fm.all('inpNeedPremGrid').value==1){
          strSql = "select a.dutycode,a.payplancode,a.payplanname,'','','','','','' from lmdutypayrela a where dutycode in  "
	               + " (select dutycode from lmriskduty where riskcode='"+cRiskCode+"')";

          turnPage.queryModal(strSql, PremGrid);
          PremGrid.lock;
        }
        	       if(scantype=="scan")
  {
    setFocus();
  } 
        
  }catch(ex) {}

}
function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！");
		 return;
	}
    if(fm.all('AgentCode').value == "")	{
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  }
	if(fm.all('AgentCode').value != "")	 {
	var cAgentCode = fm.AgentCode.value;  //保单号码
	var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"'";// and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null) {
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
     }
	}
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];
  	fm.AgentGroup.value = arrResult[0][1];
  }
}

function queryAgent2()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！");
		 return;
	}
	if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==10 )	 {
	var cAgentCode = fm.AgentCode.value;  //保单号码
	var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
     }
	}
}
  function returnparent()
  {

  	var backstr=fm.all("ContNo").value;
   var InsuredNo=fm.all("CustomerNo").value;
  	//alert(backstr+"backstr");
  	mSwitch.deleteVar("ContNo");
	mSwitch.addVar("ContNo", "", backstr);
	mSwitch.updateVar("ContNo", "", backstr);
	if(SysType=="02"||SysType=="03")//原始状态
	{
		if(EdorType!="CO")
  	location.href="ContInsuredInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+LoadFlag+"&ContType="+ContType+"&acctype="+acctype+"&scantype="+scantype+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&SysType="+SysType+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&oldContNo="+oldContNo+"&InsuredNo="+InsuredNo;
  	else
  	{
  		location.href="../bq/GedorPubAccInput.jsp?OrganComCode="+parent.VD.gVSwitch.getVar("OrganComCode")+"&LoadFlag="+parent.VD.gVSwitch.getVar("LoadFlag")+"&acctype="+parent.VD.gVSwitch.getVar("AccType")+"&ContType="+parent.VD.gVSwitch.getVar("ContType")+"&scantype="+parent.VD.gVSwitch.getVar("ScanType")+"&checktype="+parent.VD.gVSwitch.getVar("CheckType")+"&ScanFlag="+parent.VD.gVSwitch.getVar("ScanFlag")+"&GrpContNo="+parent.VD.gVSwitch.getVar("GrpContNo")+"&PrtNo="+parent.VD.gVSwitch.getVar("PrtNo")+"&EdorNo="+parent.VD.gVSwitch.getVar("EdorNo")+"&EdorType="+parent.VD.gVSwitch.getVar("EdorType")+"&EdorValiDate="+parent.VD.gVSwitch.getVar("EdorValiDate");
    }  
  }
        if(SysType=="null"||SysType=="01")//分出的个单个人单
        {
        	location.href="NormContInsuredInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+LoadFlag+"&ContType="+ContType+"&scantype="+scantype+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&SysType="+SysType+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
        }
        if(SysType=="04")//询价单
        {

        }
        if(SysType=="05")//银代
        {
        	location.href="BankContInsuredInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+LoadFlag+"&ContType="+ContType+"&scantype="+scantype+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&SysType="+SysType+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
        }
        if(SysType=="06")//
        {
        	location.href="FacilityContInsuredInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+LoadFlag+"&ContType="+ContType+"&scantype="+scantype+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&SysType="+SysType+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
        }
        if(LoadFlag == "8")
        {
        	  	location.href="../bq/GEdorTypeNS.jsp?BQFlag="+BQFlag;
	  					return;
        }
        if(LoadFlag == "18")
        {
        	  	location.href="../bq/PEdorTypeNS.jsp?BQFlag="+BQFlag;
	  					return;
        }


}
//(GrpContNo,LoadFlag);//根据集体合同号查出险种信息
function getRiskByGrpPolNo(GrpContNo,LoadFlag)
{
    	//alert("1");
    	var i = 0;
	var j = 0;
	var m = 0;
	var n = 0;
	var strsql = "";
	var tCodeData = "0|";

	strsql = "select riskcode,riskname from lmriskapp where riskcode in (select riskcode from LCGrpPol where GrpContNo='"+GrpContNo+"')" ;
	//alert("strsql :" + strsql);
	turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
	if (turnPage.strQueryResult != "")
	{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		m = turnPage.arrDataCacheSet.length;
		for (i = 0; i < m; i++)
		{
			j = i + 1;
//			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
			tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
		}
	}
	//alert ("tcodedata : " + tCodeData);

	return tCodeData;
}

//add by Lanjun 2005-5-9 20:33
//通过主险查询附加险 根据ldcode1中的数据查询
function getRiskByCode(RiskCode)
{
  var i = 0;
	var j = 0;
	var m = 0;
	var n = 0;
	var strsql = "";
	var tCodeData = "0|";

	//SubRiskFlag A-即可是附加险，也可作为主险 S－附加险

	strsql = "select riskcode,riskname from lmriskapp where SubRiskFlag='A' or SubRiskFlag= 'S' and trim(riskcode) in (select trim(code) from ldcode1 where code1='"+RiskCode+"')" ;
	//alert("strsql :" + strsql);
	turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
	if (turnPage.strQueryResult != "")
	{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		m = turnPage.arrDataCacheSet.length;
		for (i = 0; i < m; i++)
		{
			j = i + 1;
			tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
		}
	}
  else
	{
		alert("没有与主险对应的附险!");
		if(LoadFlag=="8")
		{
		  location.href="../bq/GEdorTypeNS.jsp?BQFlag=4";
	  }
	  else if(LoadFlag=="18")
	  {
	  	location.href="../bq/PEdorTypeNS.jsp?BQFlag=4";
	  }
		return;
    tCodeData = tCodeData+"^000000|没有相关的附加险";
	}

	return tCodeData;
}


//add by Lanjun 2005-5-9 20:33
//通过主险查询附加险 根据lcpol查询
//本函数为测试用，由主险号选出附加险号 2005-5-13 19:37
function getRiskByMainPol(ContNo,InsuredNo)
{
    	//alert("1");
    	var i = 0;
	var j = 0;
	var m = 0;
	var n = 0;
	var strsql = "";
	var tCodeData = "0|";

	strsql = "select riskcode,riskname from lmriskapp where  SubRiskFlag='A' or SubRiskFlag= 'S' and riskcode in (select riskcode from lcpol where ContNo='" + ContNo + "'  and mainpolno <> polno)" ;
	//alert("strsql :" + strsql);
	turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
	if (turnPage.strQueryResult != "")
	{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		m = turnPage.arrDataCacheSet.length;
		for (i = 0; i < m; i++)
		{
			j = i + 1;
//			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
			tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
		}
	}
	//alert ("tcodedata : " + tCodeData);
  else
	{
    tCodeData = tCodeData+"^000000|没有相关的附加险";
	}
	return tCodeData;
}



function getRiskByGrpAll()
{
    	//alert("1");
    	var i = 0;
	var j = 0;
	var m = 0;
	var n = 0;
	var strsql = "";
	var tCodeData = "0|";
	strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('G','A','B','D') order by RiskCode" ;
	//alert("strsql :" + strsql);
	turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
	if (turnPage.strQueryResult != "")
	{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		m = turnPage.arrDataCacheSet.length;
		for (i = 0; i < m; i++)
		{
			j = i + 1;
//			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
			tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
		}
	}
	//alert ("tcodedata : " + tCodeData);

	return tCodeData;
}
function getRisk(){
    	var i = 0;
	var j = 0;
	var m = 0;
	var n = 0;
	var strsql = "";
	var tCodeData = "0|";
  var getprtno="select missionprop1 from lwmission where MissionID='"+MissionID+"' and (activityid='0000001098' or activityid='0000001099'or activityid='0000001100')";
	var arrResult=easyExecSql(getprtno,1,0);
	  if(arrResult!=null)
	  {
	  	prtNo=arrResult[0][0];
	  }
  if(LoadFlag==5)
  {
      getprtno="select missionprop2 from lwmission where MissionID='"+MissionID+"' and SubMissionID='"+SubMissionID+"' and (activityid='0000001001')";
	    arrResult=easyExecSql(getprtno,1,0);
	      if(arrResult!=null)
	      {
	      	prtNo=arrResult[0][0];
	      }	
  }  
	if(prtNo.substring(0,4)=="1001")
  {
	strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('I','A','C','D') and (RiskType6='1' or RiskType6='4')"
           + " order by RiskCode";
  }
  else if(prtNo.substring(0,4)=="1004")
  {
	strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('I','A','C','D') and RiskType6='4'"
           + " order by RiskCode";
  }
  else if(prtNo.substring(0,4)=="1003")
  {
	strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('Y','B','C','D')and RiskType6='3'"
           + " order by RiskCode";
  }
  else
  {
	strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('I','Y','A','B','C','D')"
           + " order by RiskCode";
  }
	turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
	if (turnPage.strQueryResult != "")
	{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		m = turnPage.arrDataCacheSet.length;
		for (i = 0; i < m; i++)
		{
			j = i + 1;
//			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
			tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
		}
	}

	return tCodeData;



}

function getRiskByContPlan(GrpContNo,ContPlanCode){
	//alert(GrpContNo+":"+ContPlanCode);
    	var i = 0;
	var j = 0;
	var m = 0;
	var n = 0;
	var strsql = "";
	var tCodeData = "0|";
	strsql = "select b.RiskCode, b.RiskName from LCContPlanRisk a,LMRiskApp b where  a.GrpContNo='"+GrpContNo+"' and a.ContPlanCode='"+ContPlanCode+"' and a.riskcode=b.riskcode";
	//alert(strsql);
	turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
	if (turnPage.strQueryResult != "")
	{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		m = turnPage.arrDataCacheSet.length;
		for (i = 0; i < m; i++)
		{
			j = i + 1;
//			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
			tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
		}
	}
	//alert("tCodeData:"+tCodeData);
	return tCodeData;



}


 /*********************************************************************
 *  把合同所有信息录入结束确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 /*
function inputConfirm(wFlag)
{
	if (wFlag ==1 ) //录入完毕确认
	{
		if(fm.all('ContNo').value == "")
	   {
	   	  alert("合同信息未保存,不容许您进行 [录入完毕] 确认！");
	   	  return;
	   }
		WorkFlowFlag = "7999999999";
  }
  else if (wFlag ==2)//复核完毕确认
  {
  	if(fm.all('ContNo').value == "")
	   {
	   	  alert("未查询出合同信息,不容许您进行 [复核完毕] 确认！");
	   	  return;
	   }
		WorkFlowFlag = "0000001001";

	}
	  else if (wFlag ==3)
  {
  	if(fm.all('ContNo').value == "")
	   {
	   	  alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
	   	  return;
	   }
		WorkFlowFlag = "0000001002";
	}
	else
		return;
	CloseBase = "YES";
	queryString = "WorkFlowFlag="+WorkFlowFlag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ProposalContNo="+parent.VD.gVSwitch.getVar("ContNo");
	mAction = "CONFIRM";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./InputConfirm.jsp?FrameType=0&"+queryString;
  fm.submit(); //提交
}*/

function getContInputnew(){//此函数的目的是：查询团单或者个单下的投保，被投保信息
	//取得个人投保人的所有信息
	if(fm.AppntCustomerNo.value!=""&&fm.AppntCustomerNo.value!="false"){
	arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LDPerson a Left Join LCAddress b On b.CustomerNo =a.CustomerNo Where 1=1 and a.CustomerNo ='"+fm.AppntCustomerNo.value+"'",1,0);
        displayAppnt(arrResult[0]);
        }
	//取得投保单位的 所有信息
	 if(fm.GrpContNo.value!=""&&fm.GrpContNo.value!="false"){
	 arrResult = easyExecSql("select a.*,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.CustomerNo,b.GrpName from LCGrpAddress a,LDGrp b where a.CustomerNo = b.CustomerNo and b.CustomerNo=(select CustomerNo from LCGrpAppnt  where GrpContNo = '" + fm.GrpContNo.value + "')", 1, 0);
	 displayAddress1(arrResult[0]);
	 }
	 //取得被投保人的所有信息
	 if(fm.all('CustomerNo').value!=""&&fm.all('CustomerNo').value!="false"){
         var tcustomerNo=fm.all('CustomerNo').value;
         //alert("contno:"+fm.all('ContNo').value);
         var tContNo=fm.all('ContNo').value;
         arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.InsuredNo) ),b.PostalAddress,b.ZipCode,b.Phone,b.HomePhone from LCInsured a Left Join LCAddress b On b.CustomerNo =a.InsuredNo Where 1=1 and a.InsuredNo ='"+tcustomerNo+"' and a.ContNo='"+tContNo+"'",1,0);
         displayInsured(arrResult[0]);
         var arrResult1 = easyExecSql("select PolType from lccont where contno='"+tContNo+"'");
         //alert(arrResult1);
         fm.all('PolTypeFlag').value=arrResult1[0];
         }
}

function GrpConfirm(){
	if(LoadFlag == '1'||LoadFlag == '11')
	{

		var tStr= "	select * from lwmission where 1=1 and lwmission.missionprop1 = '"+parent.VD.gVSwitch.getVar( "ContNo")+"'";
		turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
	  if (turnPage.strQueryResult) {
		  	alert("该合同已经做过保存！");
		  	return;
		  	}
	  if(ScanFlag==1)
	  {
	     WorkFlowFlag = "0000001099";

	  }else if(ScanFlag==0)
	  {
		   WorkFlowFlag = "0000001098";
		}
		geturgencyflag(WorkFlowFlag) ;
	 if(!confirm("确认保单录入完毕!"))
	 {
	 	 return;
	 }
	 		inputflag="1";
	 CloseBase = "YES";
	queryString = "WorkFlowFlag="+WorkFlowFlag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ProposalContNo="+parent.VD.gVSwitch.getVar("ContNo");
	mAction = "CONFIRM";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./InputConfirm.jsp?FrameType=0&"+queryString;
	fm.submit(); //提交
	}else if(EdorType=='CO'&&(LoadFlag=='2'||LoadFlag=="2"))
	{
		var tFlagStr = "Suss";
		var tContent = "保存成功!";
		var tStr = "select 1 from lcduty where polno = '"+parent.VD.gVSwitch.getVar("PolNo")+"'";
		arrResult = easyExecSql(tStr,1,0);
		if(arrResult!=null&&arrResult.length>0)
		{}
		else
		{
			tFlagStr = "Fail";
			tContent = "未录入责任项!";
		}
		
		afterSubmit( tFlagStr, tContent );
		}
	else
	{
	//		alert("check");
	//chenwm20070827 增加险种界面录入完毕的效验 和前面的保持一致
	if(!CheckAcc()){return false;}
	
		//alert("ss");
	//return false;
	 var grpscanflag="";
	if(ScanFlag==0)
	{
		grpscanflag = "0000002098";
	}
	if(ScanFlag==1)
	{
		 grpscanflag = "0000002099";
	}
	 var tGrpContNo = parent.VD.gVSwitch.getVar( "GrpContNo" );

	strSql = "select peoples2 from LCGrpCont 	where GrpGroupNo IS NULL and GrpContNo = '" + tGrpContNo + "'";
	var tPeoplesCount = easyExecSql(strSql);

	if(tPeoplesCount==null||tPeoplesCount[0][0]<=0){
		alert("检验失败，投保总人数为0！");
		return;
	}

	strSql = "select peoples2,riskcode from LCGrpPol 	where GrpContNo = '" + tGrpContNo + "'";
	tPeoplesCount = easyExecSql(strSql);
	//alert(tPeoplesCount);
	if(tPeoplesCount!=null)
	{
		for(var i=0;i<tPeoplesCount.length;i++)
		{
			if(tPeoplesCount[i][0]<=0)
			{
				alert("检验失败，险种"+tPeoplesCount[i][1]+"下投保总人数为0！");
				return;
			}
		}
	}

	strSql = "select SaleChnl,AgentCode,AgentGroup,ManageCom,GrpName,CValiDate,PrtNo from LCGrpCont where GrpGroupNo IS NULL and GrpContNo = '"
	+ tGrpContNo + "'";
	var grpContInfo = easyExecSql(strSql);
	//alert(grpContInfo);
	var queryString = 'SaleChnl='+grpContInfo[0][0]+'&AgentCode='+grpContInfo[0][1]+'&AgentGroup='+grpContInfo[0][2]
		+'&ManageCom='+grpContInfo[0][3]+'&GrpName='+grpContInfo[0][4]+'&CValiDate='+grpContInfo[0][5];
	strSql = "	select missionID,SubMissionID from lwmission where 1=1 "
					+" and lwmission.processid = '0000000004'"
					+" and lwmission.activityid = '"+grpscanflag+"'"
					+" and lwmission.missionprop1 = '"+grpContInfo[0][6]+"'";
	var missionInfo = easyExecSql(strSql);
	queryString = queryString+"&MissionID="+missionInfo[0][0]+"&SubMissionID="+missionInfo[0][1];
	//alert(queryString);
	var tStr= "	select * from lwmission where 1=1 "
					+" and lwmission.processid = '0000000004'"
					+" and lwmission.activityid = '0000002001'"
					+" and lwmission.missionprop1 = '"+fm.all('ProposalGrpContNo').value+"'";
	turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
	if (turnPage.strQueryResult) {
	alert("该团单合同已经做过保存！");
	return;
	}
	var WorkFlowFlag = "";
	WorkFlowFlag = grpscanflag;
	queryString = queryString+"&WorkFlowFlag="+WorkFlowFlag;
	mAction = "CONFIRM";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GrpInputConfirm.jsp?FrameType=0&"+queryString;
	fm.submit(); //提交
 }
}
function proposalapprove()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./ProposalApproveSave.jsp";
  fm.submit(); //提交
}

//>保险期间为5Y，缴费方式只能趸交
//>保险期间为10Y，缴费方式只能趸交和期交5年交，10年交
//>保险期间为20Y，缴费方式只能趸交和期交5年交，20年交
//>保险期间为30Y，缴费方式只能趸交和期交5年交，30年交
//>保险期间为50A，缴费方式只能趸交和50A
//>保险期间为55A，缴费方式只能趸交和55A
//>保险期间为60A，缴费方式只能趸交和60A
//>保险期间为65A，缴费方式只能趸交和65A
//>保险期间为70A，缴费方式只能趸交和70A
//保险期间与交费期间关联  0^5|5年交|Y^10|10年交|Y^20|20年交|Y^30|30年交|Y^50-->

function PayIntvData111301(){
var tInsuYear="";
var tCodeData="";

tInsuYear=fm.all('InsuYear').value;

	if(tInsuYear=="5"){
		tCodeData="0^0|趸交";
	}
  else	{
		tCodeData="0|^0|趸交^12|按年交";
	}
	fm.PayIntv.CodeData=tCodeData;
}


function PayEndYearData111301(){
var tInsuYear="";
var	tInsuYearFlag="";
var tCodeData="";
var tPayIntv="";

tInsuYear=fm.all('InsuYear').value;
tInsuYearFlag=fm.all('InsuYearFlag').value;
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		//fm.PayEndYear.value=tInsuYear;
		//fm.PayEndYearFlag.value=tInsuYearFlag;
		//fm.PayEndYear.readonly=true;
		if(tInsuYearFlag=='Y'){
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"年|Y";
		}
		else{
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"岁|A";
		}
		//alert(tCodeData);
	}
	else{
		if(tInsuYear=="10"){
			tCodeData="0^5|5年交|Y^10|10年交|Y";
		}
		else if(tInsuYear=="20"){
			tCodeData="0^5|5年交|Y^20|20年交|Y";
		}
		else if(tInsuYear=="30"){
			tCodeData="0^5|5年交|Y^30|30年交|Y";
		}
		else if(tInsuYear=="50"){
			tCodeData="0^50|50岁|A";
		}
		else if(tInsuYear=="55"){
			tCodeData="0^55|55岁|A";
		}
		else if(tInsuYear=="60"){
			tCodeData="0^60|60岁|A"	;
		}
		else if(tInsuYear=="65"){
			tCodeData="0^65|65岁|A"	;
		}
		else if(tInsuYear=="70"){
			tCodeData="0^70|70岁|A"	;
		}
		else{
			tCodeData="";
		}
	}
	fm.PayEndYear.CodeData=tCodeData;
}
//银保保证给付年金保险B(分红型）
function PayIntvData01303(){
var tStandByFlag1="";
var tCodeData="";

tStandByFlag1=fm.all('StandByFlag1').value;

 if(tStandByFlag1=="A" || tStandByFlag1=="B" ||tStandByFlag1=="C" ){
			tCodeData="0^1|月交";
		 }
		 else {
			tCodeData="0^12|年交";
		 	}

	 fm.PayIntv.CodeData=tCodeData;
 	}
function GetYearData01303(){
 	var tInsuYear="";
 	var tCodeData="";
 tInsuYear=fm.all('InsuYear').value;
      if(tInsuYear=="20"){
      	                  tCodeData="0^10|10年后|Y";
      	                }
      	         else if(tInsuYear=="40"){
			tCodeData="0^20|20年后|Y";
		 	}
		 else if(tInsuYear=="60"){
		 	tCodeData="0^30|30年后|Y";
		 	}
		  fm.GetYear.CodeData=tCodeData;
		}



function PayIntvData211302(){
var tInsuYear="";
var tCodeData="";

tInsuYear=fm.all('InsuYear').value;

	if(tInsuYear=="3"){
		tCodeData="0^0|趸交";
	}
	else if(tInsuYear=="5"){
		tCodeData="0^0|趸交";
	}
  else	{
		tCodeData="0|^0|趸交^12|按年交";
	}
	fm.PayIntv.CodeData=tCodeData;
}


function PayEndYearData211302(){
var tInsuYear="";
var	tInsuYearFlag="";
var tCodeData="";
var tPayIntv="";

tInsuYear=fm.all('InsuYear').value;
tInsuYearFlag=fm.all('InsuYearFlag').value;
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		//fm.PayEndYear.value=tInsuYear;
		//fm.PayEndYearFlag.value=tInsuYearFlag;
		//fm.PayEndYear.readonly=true;
		if(tInsuYearFlag=='Y'){
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"年|Y";
		}
		else{
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"岁|A";
		}
		//alert(tCodeData);
	}
	else
	  tCodeData="0^3|3年交|Y^5|5年交|Y";

	fm.PayEndYear.CodeData=tCodeData;
}
//华夏同心定期两全（B款，分红型）112201
//保险期间为5Y，缴费方式只能趸交
//保险期间为10Y，缴费方式只能趸交和期交5年交
//保险期间为20Y，缴费方式只能趸交和期交5年交，10年交，20年交
//保险期间为50A，缴费方式只能趸交和期交5年交，10年交，20年交
//保险期间为55A，缴费方式只能趸交和期交5年交，10年交，20年交
//保险期间为60A，缴费方式只能趸交和期交5年交，10年交，20年交
//保险期间为65A，缴费方式只能趸交和期交5年交，10年交，20年交
//保险期间为70A，缴费方式只能趸交和期交5年交，10年交，20年交
//保险期间与交费期间关联  0^5|5年交|Y^10|10年交|Y^20|20年交|Y^30|30年交|Y^50-->

function PayIntvData112201(){
var tInsuYear="";
var tCodeData="";

tInsuYear=fm.all('InsuYear').value;

	if(tInsuYear=="5"){
		tCodeData="0^0|趸交";
	}
  else	{
		tCodeData="0|^0|趸交^12|年交";
	}
	fm.PayIntv.CodeData=tCodeData;
}


function PayEndYearData112201(){
var tInsuYear="";
var	tInsuYearFlag="";
var tCodeData="";
var tPayIntv="";

tInsuYear=fm.all('InsuYear').value;
tInsuYearFlag=fm.all('InsuYearFlag').value;
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		//fm.PayEndYear.value=tInsuYear;
		//fm.PayEndYearFlag.value=tInsuYearFlag;
		//fm.PayEndYear.readonly=true;
		if(tInsuYearFlag=='Y'){
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"年|Y";
		}
		else{
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"岁|A";
		}
		//alert(tCodeData);
	}
	else{
		if(tInsuYear=="10"){
			tCodeData="0^5|5年交|Y";
		}
		else
			tCodeData="0^5|5年交|Y^10|10年交|Y^20|20年交|Y";

	}
	fm.PayEndYear.CodeData=tCodeData;
}
function PayIntvData211302(){
var tInsuYear="";
var tCodeData="";

tInsuYear=fm.all('InsuYear').value;

	if(tInsuYear=="3"){
		tCodeData="0^0|趸交";
	}
	else if(tInsuYear=="5"){
		tCodeData="0^0|趸交";
	}
  else	{
		tCodeData="0|^0|趸交^12|按年交";
	}
	fm.PayIntv.CodeData=tCodeData;
}


function PayEndYearData211302(){
var tInsuYear="";
var	tInsuYearFlag="";
var tCodeData="";
var tPayIntv="";

tInsuYear=fm.all('InsuYear').value;
tInsuYearFlag=fm.all('InsuYearFlag').value;
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		//fm.PayEndYear.value=tInsuYear;
		//fm.PayEndYearFlag.value=tInsuYearFlag;
		//fm.PayEndYear.readonly=true;
		if(tInsuYearFlag=='Y'){
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"年|Y";
		}
		else{
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"岁|A";
		}
		//alert(tCodeData);
	}
	else
	  tCodeData="0^3|3年交|Y^5|5年交|Y";

	fm.PayEndYear.CodeData=tCodeData;
}
function PayIntvData112203(){
var tInsuYear="";
var tCodeData="";

tInsuYear=fm.all('InsuYear').value;

	if(tInsuYear=="3"){
		tCodeData="0^0|趸交";
	}
  else if(tInsuYear=="5"){
		tCodeData="0^0|趸交";
	}
  else	{
		tCodeData="0|^0|趸交^12|按年交";
	}
	fm.PayIntv.CodeData=tCodeData;
}


function PayEndYearData112203(){
var tInsuYear="";
var	tInsuYearFlag="";
var tCodeData="";
var tPayIntv="";

tInsuYear=fm.all('InsuYear').value;
tInsuYearFlag=fm.all('InsuYearFlag').value;
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		//fm.PayEndYear.value=tInsuYear;
		//fm.PayEndYearFlag.value=tInsuYearFlag;
		//fm.PayEndYear.readonly=true;
		if(tInsuYearFlag=='Y'){
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"年|Y";
		}
		else{
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"岁|A";
		}
		//alert(tCodeData);
	}
	else{

	 if(tInsuYear=="10"){
			tCodeData="0^5|5年交|Y";
		}

		else if(tInsuYear=="20"){
			tCodeData="0^5|5年交|Y^10|10年交|Y^20|20年交|Y";
		}

		else if(tInsuYear=="50"){
			tCodeData="0^5|5年交|Y^50|50岁|A";
		}
		else if(tInsuYear=="55"){
			tCodeData="0^5|5年交|Y^55|55岁|A";
		}
		else if(tInsuYear=="60"){
			tCodeData="0^5|5年交|Y^60|60岁|A"	;
		}
		else if(tInsuYear=="65"){
			tCodeData="0^5|5年交|Y^65|65岁|A"	;
		}
		else if(tInsuYear=="70"){
			tCodeData="0^5|5年交|Y^70|70岁|A"	;
		}
    else{
			tCodeData="";
		}
	}
	fm.PayEndYear.CodeData=tCodeData;
}

//华夏定期两全重大疾病险111201
function PayEndYearData111201(){

var tCodeData="";
var tPayIntv="";

tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		//fm.PayEndYear.value=tInsuYear;
		//fm.PayEndYearFlag.value=tInsuYearFlag;
		//fm.PayEndYear.readonly=true;

			tCodeData="0^80|至80岁|A";
		//alert(tCodeData);
	}
	else{
			tCodeData="0^5|5年交|Y^10|10年交|Y^20|20年交|Y";
	}
	fm.PayEndYear.CodeData=tCodeData;
}


//长城成人重大疾病保险
function PayEndYearData00502(){
var tInsuYear="";
var	tInsuYearFlag="";
var tCodeData="";
var tPayIntv="";

tInsuYear=fm.all('InsuYear').value;
tInsuYearFlag=fm.all('InsuYearFlag').value;
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		 tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"岁|A";
	}
  	else {
		tCodeData="0^5|五年|Y^10|十年|Y^15|十五年|Y^20|二十年|Y^70|至70岁|A";
	}
	fm.PayEndYear.CodeData=tCodeData;
}

//长城快意人生终身寿险
function PayEndYearData00001(){
var tCodeData="";
var tPayIntv="";

tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		 tCodeData=	"0^100|趸交|A";
	}
  	else {
		tCodeData="0^5|五年|Y^10|十年|Y^15|十五年|Y^20|二十年|Y";
	}
	fm.PayEndYear.CodeData=tCodeData;
}

//中意颐和
function PayEndYearDataNEK2(){
var tCodeData="";
var tPayIntv="";
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		 tCodeData=	"0|^1000|趸交|Y|年";
	}
  	else {
		tCodeData="0^5|五年缴|Y|年^10|十年缴|Y|年^54|缴费至54周岁(女)|A|岁^59|缴费至59周岁(男)|A|岁";
	}
	fm.PayEndYear.CodeData=tCodeData;
//	alert(tPayIntv+" "+fm.PayEndYear.CodeData);
}


//国华两全保险（分红型）
function PayEndYearData00002(){
var tCodeData="";
var tPayIntv="";
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		 tCodeData=	"0^100|趸交";
	}
  	else {
		tCodeData="0^5|五年^10|十年^15|十五年^20|二十年";
	}
	fm.PayEndYear.CodeData=tCodeData;
}

//国华两全保险（分红型）趸交
function isSinglePremium(cRiskCode){
	if(cRiskCode == "1202")
	{
		var tPayIntv=parent.VD.gVSwitch.getVar("PayIntv");
		if(tPayIntv=="0"){
			fm.PayIntvName.value="趸缴";
		}
	  else {
	  	PEY.style.display = "";
			PEY2.style.display = "";
			PEYF.style.display = "";
			PEYF2.style.display = "";
			if(tPayIntv=="1"){
				fm.PayIntvName.value="月缴";
			}else if(tPayIntv=="3"){
				fm.PayIntvName.value="季缴";
			}else if(tPayIntv=="6"){
				fm.PayIntvName.value="半年缴";
			}else{
				fm.PayIntvName.value="年缴";
			}
		}
	}
}


//长城少儿重大疾病保险
function PayEndYearData00501(){
var tInsuYear="";
var	tInsuYearFlag="";
var tCodeData="";
var tPayIntv="";

tInsuYear=fm.all('InsuYear').value;
tInsuYearFlag=fm.all('InsuYearFlag').value;
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){

		if(tInsuYearFlag=='Y'){
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"年|Y";
		}
		else{
			tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"岁|A";
		}
	}
  	else {
		tCodeData="0^5|五年|Y^10|十年|Y^15|十五年|Y^20|二十年|Y^30|三十年|Y";
	}
	fm.PayEndYear.CodeData=tCodeData;
}
function GetYearData00501(){
	var tInsuYear="";

	var tCodeData="";
tInsuYear=fm.all('InsuYear').value;

 if(tInsuYear=="30"){
 	tCodeData="0^30|三十年后|Y";
}
else if(tInsuYear=="60"){
	tCodeData="0^60|六十岁|A";
}
 fm.GetYear.CodeData=tCodeData;
}

function GetStartTypeData00501(){
	var tInsuYear="";

	var tCodeData="";
tInsuYear=fm.all('InsuYear').value;

 if(tInsuYear=="30"){
 	tCodeData="0^S|起保日期对应日";
}
else if(tInsuYear=="60"){
	tCodeData="0^B|出生日期对应日";
}
 fm.GetStartType.CodeData=tCodeData;
}


//长城二十年期保证给付年金保险A（分红型）01301
//长城十年期保证给付年金保险A（分红型）01302
/*根据交费方式确定交费期间*/
function PayEndYearData01301(){
var tInsuYear="";
var tInsuYearFlag="";
var tCodeData="";
var tPayIntv="";

tInsuYear=fm.all('InsuYear').value;
tInsuYearFlag=fm.all('InsuYearFlag').value;
tPayIntv=fm.all('PayIntv').value;
	if(tPayIntv=="0"){
		 tCodeData=	"0^"+tInsuYear+"|"+tInsuYear+"岁|A";
	}
  	else {
		tCodeData="0^10|十年|Y^20|二十年|Y";
	}
	fm.PayEndYear.CodeData=tCodeData;
}

/*//长城团体退休金保险（分红型）21301
//领取年龄在30——80岁范围内年金领取方式不受限制，不在此范围内则只可趸领
function getdutykind21301(){
var tGetYear="";
var tCodeData="";

tGetYear=fm.all('GetYear').value;
	if(tGetYear>="30" && tGetYear<="80"){
		tCodeData="0|^100|一次性领取|0|趸领^101|年领平准型年金|12|年领^102|年领平准保证十年年金|12|年领^103|年领算数递增年金|12|年领^104|月领平准型年金|1|月领^105|月领平准保证十年年金|1|月领^106|月领算数递增年金|1|月领^107|部分领取年领平准型年金|12|年领^108|部分领取年领平准保证十年年金|12|年领^109|部分领取年领算数递增年金|12|年领^110|部分领取月领平准型年金|1|月领^111|部分领取月领平准保证十年年金|1|月领^112|部分领取月领算数递增年金|1|月领";
	}
	else{
	tCodeData="0|^100|一次性领取|0|趸领";
	}
	fm.GetDutyKind.CodeData=tCodeData;
}
*/
function CheckAcc()
{
		var tReturn= true;
	//	   alert(fm.ProposalGrpContNo.value);
	   var strSQL="select DISTINCT c.riskcode from lmrisktoacc a,lmriskinsuacc b,lcgrppol c where b.acctype='001' and c.grpcontno='"+fm.ProposalGrpContNo.value+"' and a.insuaccno=b.insuaccno and a.riskcode=c.riskcode";
//	   alert(strSQL);
	   var arrResult=easyExecSql(strSQL);
//	   alert(arrResult);
	   if(arrResult!=null)
	   {
	   		strSQL="select count(1) from lcpol where poltypeflag='2' and grpcontno='"+fm.ProposalGrpContNo.value+"'";
	   		var arrResult1=easyExecSql(strSQL);
	   		if(arrResult1==0)
	   		{
	   			alert("该保单必须为险种"+arrResult+"设置公共账户！");
	   			tReturn = false;
	   		}
	  	}
	  	return tReturn;
}
/*********************************************************************
 *  把合同所有信息录入结束确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function inputConfirm(wFlag)
{	
	
	if (wFlag ==1 ) //录入完毕确认
	{
		var tStr= "	select * from lwmission where 1=1 and lwmission.missionprop1 = '"+fm.ContNo.value+"'";
		turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
		  if (turnPage.strQueryResult) {
		  	alert("该合同已经做过保存！");
		  	return;
		  	}
		if(fm.all('ContNo').value == "")
	   {
	   	  alert("合同信息未保存,不容许您进行 [录入完毕] 确认！");
	   	  return;
	   }
	  if(ScanFlag==1)
	  {
	     WorkFlowFlag = "0000001099";
	  }else if(ScanFlag==0)
	  {
		   WorkFlowFlag = "0000001098";
		}
  }
  else if (wFlag ==2)//复核完毕确认
  {
  	  	if(fm.all('ContNo').value == "")
	   {
	   	  alert("未查询出合同信息,不容许您进行 [复核完毕] 确认！");
	   	  return;
	   }
  	var approvesql="select * from lcpol where approveflag='0' and contno='"+fm.all('ContNo').value+"'";
        arrResult = easyExecSql(approvesql,1,0);
        if(arrResult!=null)
        {
        	if(LoadFlag!=1)
        	{
            if(!confirm("还有未复核通过的险种,确认要整单复核？") )
        	  return false;
          }
        }

		WorkFlowFlag = "0000001001";					//复核完毕
		geturgencyflag(WorkFlowFlag) ;
		approvefalg="2";
		approve="1";
		//inputflag="1";
	}
	  else if (wFlag ==3)
  {
  	if(fm.all('ContNo').value == "")
	   {
	   	  alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
	   	  return;
	   }
		WorkFlowFlag = "0000001002";					//复核修改完毕

	}
	else if(wFlag == 4)
	{
		 if(fm.all('ContNo').value == "")
	   {
	   	  alert("未查询出合同信息,不容许您进行 [修改完毕] 确认！");
	   	  return;
	   }
		WorkFlowFlag = "0000001021";					//问题修改

	}
	else
		return;
	CloseBase = "YES";
	//alert(fm.all('AppntCustomerNo').value+"*****************"+fm.all('AppntName').value);杨红于20050611将此注销。
	queryString = "WorkFlowFlag="+WorkFlowFlag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&AppntNo="+fm.all('AppntCustomerNo').value+"&AppntName="+fm.all('AppntName').value+"&ProposalContNo="+parent.VD.gVSwitch.getVar("ContNo");
	mAction = "CONFIRM";
	if(inputflag=="1")
	{
   var showStr="已经录入完毕,没有问题件和相似客户,质检抽检未抽中,直接转入自动核保,请等待该过程结束";
	}
	else
	{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  }
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./InputConfirm.jsp?FrameType=0&"+queryString;
  fm.submit(); //提交
}
function checkuw()
{
		  var querysql="select * from lwmission where missionid='"+MissionID+"' and submissionid='"+SubMissionID+"' and activityid='0000001100'";
		  turnPage.strQueryResult  = easyQueryVer3(querysql, 1, 0, 1);
		  if(turnPage.strQueryResult)
		  {
		  	return true;
		  }
		  else
		  {
		  	return false;
		  }
}
function checkreview()
{
	  //是否有抽检
    var strSpotSql="select *　from ldspottrack where othertype='spotbargain' and otherno='"+fm.ContNo.value+"'";
    var arrSpotResult = easyExecSql(strSpotSql,1,0);
    //是否有问题件
    var strIssueSql="select *　from LCIssuePol where OperatePos='1' and ProposalContNo='"+fm.ContNo.value+"' and IssueType<>'9999999999' and BackObjType='5'";
    var arrIssueResult = easyExecSql(strIssueSql,1,0);
    //是否有相似客户
    var strPersonSql="select *from ldperson where "
    +" (Name='"+fm.AppntName.value+"' and Sex='"+fm.AppntSex.value+"' and Birthday='"+fm.AppntBirthday.value+"' and CustomerNo<>'"+fm.AppntCustomerNo.value+"')"
    +" or (Name='"+fm.AppntName.value+"' and Sex='"+fm.Sex.value+"' and Birthday='"+fm.Birthday.value+"' and CustomerNo<>'"+fm.CustomerNo.value+"')"
    +" or (IDType='"+fm.AppntIDType.value+"' and IDNo='"+fm.AppntIDNo.value+"'  and CustomerNo<>'"+fm.AppntCustomerNo.value+"')"
    +" or (IDType='"+fm.IDType.value+"' and IDNo='"+fm.IDNo.value+"'  and CustomerNo<>'"+fm.CustomerNo.value+"')";
    var arrPersonResult = easyExecSql(strPersonSql,1,0);
    if(arrSpotResult!=null||arrIssueResult!=null||arrPersonResult!=null)
    {
    	return false;
    }
    else
    {
    	return true;
    }
}

function checkbnf()
{
  for (var i=0; i<BnfGrid.mulLineCount; i++)
  {
      if(BnfGrid.getRowColData(i,1)=="1")
      {
      	if(BnfGrid.getRowColData(i,6)=="00")
      	{
      		alert("身故受益人不能选择被保险人本人");
    	         return false;
      	}
      	else
      	{
      		var strPersonSql="";
      		var arrPersonResult="";
               strPersonSql="select * from LCInsured where "
                           +" Name='"+BnfGrid.getRowColData(i,2)+"'and Birthday='"+BnfGrid.getRowColData(i,3)+"'"
                           +" and IDType='"+BnfGrid.getRowColData(i,4)+"' and IDNo='"+BnfGrid.getRowColData(i,5)+"' and ContNo='"
                           +fm.ContNo.value+"'";
                           arrPersonResult = easyExecSql(strPersonSql,1,0);
              //aleret(arrPersonResult);
              if(arrPersonResult!=null)
              {
      		      alert("身故受益人不能选择被保险人本人");
    	          return false;
              }
              if(arrPersonResult==""||arrPersonResult=="null"||arrPersonResult==null)
              {
              	 strPersonSql="select * from LbInsured where "
                           +" Name='"+BnfGrid.getRowColData(i,2)+"'and Birthday='"+BnfGrid.getRowColData(i,3)+"'"
                           +" and IDType='"+BnfGrid.getRowColData(i,4)+"' and IDNo='"+BnfGrid.getRowColData(i,5)+"' and ContNo='"
                           +fm.ContNo.value+"'";
                 arrPersonResult = easyExecSql(strPersonSql,1,0);
              }
       }
     }
     if(BnfGrid.getRowColData(i,4)=="0")
     {
     	if(checkIdCard(BnfGrid.getRowColData(i,5))==false)
     	{
     		return false;     		
     	}
     	if(prtNo.substring(0,4)!="1003")
     	{
     	  var noline=getBirthdatByIdNo(BnfGrid.getRowColData(i,5)).substring(0,4)+getBirthdatByIdNo(BnfGrid.getRowColData(i,5)).substring(5,7)+getBirthdatByIdNo(BnfGrid.getRowColData(i,5)).substring(8,10);
     	  if(BnfGrid.getRowColData(i,3)!=getBirthdatByIdNo(BnfGrid.getRowColData(i,5))&&BnfGrid.getRowColData(i,3)!=noline)
     	  {
     	  	alert("受益人证件编码与出生日期不符,请确认!");
     	  	return false;
     	  }
     }
    }
  }
  return true;
}
function 	geturgencyflag(tactivtiyid)
{
 var strSql = "select missionprop18 from lwmission where activityid = '"+tactivtiyid+"' and MissionID='"+MissionID+"' and SubMissionID='"+SubMissionID+"'";
	  var arrResult=easyExecSql(strSql,1,0);
	  if(arrResult!=null)
	  {
	  	fm.UrgencyFlag.value=arrResult[0][0];
	  }
	  else
	  {
	  	fm.UrgencyFlag.value="0";
	  }
}
function getRiskInfo(cRiskCode)
{
	var polsql="select Amnt,Mult from LCFirstPol where ContNo='"+fm.ContNo.value+"' and RiskCode='"+cRiskCode+"'";
	var arrResult=easyExecSql(polsql,1,0);
	  if(arrResult!=null)
	  {
	  	try{fm.Amnt.value=arrResult[0][0];fm.ConfirmAmnt.value=arrResult[0][0];	}catch(ex){}
	  	try{fm.Mult.value=arrResult[0][1];fm.ConfirmMult.value=arrResult[0][1];	}catch(ex){}
	  }
	  else
	  {
	  	fm.Amnt.value="";
	  	fm.Mult.value="";
	  }

}
/*********************************************************************
 *  获得被保人险种信息，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getInsuredPolInfo()
{
    initPolGrid();
    var InsuredNo=fm.all("CustomerNo").value;
    var ContNo=fm.all("ContNo").value;
    //险种信息初始化
    if(InsuredNo!=null&&InsuredNo!="")
    {
        var strSQL ="select PolNo,RiskCode,Prem,Amnt,case  when approveflag='9'  then '已复核'   else '未复核' end "
                   +" from LCPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
                   +" union"
                   +" select PolNo,RiskCode,Prem,Amnt,case  when approveflag='9'  then '已复核'   else '未复核' end "
                   +" from LBPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
                   ;
        //alert(strSQL);
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult)
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = PolGrid;
        //保存SQL语句
        turnPage.strQuerySql = strSQL;
        //设置查询起始位置
        turnPage.pageIndex = 0;
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
    var sumprem=0;
    for(var polcount=0;polcount<PolGrid.mulLineCount;polcount++)
    {
       sumprem=sumprem+parseFloat(PolGrid.getRowColData(polcount,3));
    }    
    var innerHTML = document.all("spanPolGrid").innerHTML;
    innerHTML += "<div align=right>";
    innerHTML += "保费合计："; 
    innerHTML += "<input  class=readonly value='"+sumprem+"(元)'>";
    innerHTML += "</div>";                
    document.all("spanPolGrid").innerHTML = innerHTML;         
    
}
function getPolApplyDate()
{
		    	var strSql = "select PolApplyDate,Payintv from lccont where contno='"+fm.ContNo.value+"'"
		    	           +" union"
		    	           +" select PolApplyDate,Payintv from lbcont where contno='"+fm.ContNo.value+"'"
		    	           ;
	        var arrResult = easyExecSql(strSql);
	        fm.CValiDate.value=arrResult[0][0];
	        fm.PayIntv.value=arrResult[0][1];
	        try
	        {
	          if(fm.PayIntv.value=="0")
	          {
	          	fm.PayEndYear.value=fm.InsuYear.value;
	          	fm.PayEndYearFlag.value=fm.InsuYearFlag.value;
	          }
	        }
	        catch(ex){}
	        showCodeName();
}
//真正的问题件
function QuestInputC()
{
	cContNo = fm.ContNo.value;  //保单号码
	if(cContNo == "")
	{
		alert("尚无合同投保单号，请先保存!");
	}
	else
	{
		window.open("./QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+ LoadFlag+"&PrtNo="+fm.PrtNo.value+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&Page=1","windoww");
	}
}
function CheckReviewCon()
{
	cContNo = fm.ContNo.value;  //保单号码
	var polsql="select ApproveErrType,ApproveOpinion from LCCont where ContNo='"+fm.ContNo.value+"'"
	          +" union"
	          +" select ApproveErrType,ApproveOpinion from LBCont where ContNo='"+fm.ContNo.value+"'"
	          ;
	var arrResult=easyExecSql(polsql,1,0);
	  if(arrResult!=null)
	  {
	  	if(arrResult[0][0]==""||arrResult[0][1]=="")
	  	{
        return false;
	  	}
	  	else
	  	{
	  		return true;
	  	}
	  }
	  else
	  {
      return false;
	  }
return true;
}


/**
 * FUNCTION: translate_TO_chinese()
 * DESC :对于一些字段进行汉化.     _yz_ ;)
 */
 function translate_TO_chinese() {
try
{
	
if(fm.RnewFlag.value!=null) {
var RnewFlag=fm.RnewFlag.value;
if(RnewFlag=="0")
fm.RnewFlagName.value="人工续保" ;
if(RnewFlag==-1)
fm.RnewFlagName.value="自动续保" ;
if(RnewFlag==-2)
fm.RnewFlagName.value="非续保" ;
}	
 //汉化__交费期间.
if(fm.PayEndYear.value!=null) {
var PayEndYear=fm.PayEndYear.value ;
if(PayEndYear=="5")
fm.PayEndYearName.value="五年" ;
if(PayEndYear=="10")
fm.PayEndYearName.value="十年" ;
if(PayEndYear=="15")
fm.PayEndYearName.value="十五年" ;
if(PayEndYear=="20")
fm.PayEndYearName.value="二十年" ;
if(PayEndYear=="70")
fm.PayEndYearName.value="至70岁" ;
}
//汉化__保费逾期未付选择
if(fm.AutoPayFlag.value!=null) {
var AutoPayFlag=fm.AutoPayFlag.value;
if(AutoPayFlag=="0")
fm.PayFlagName.value="中止合同" ;
if(AutoPayFlag=="1")
fm.PayFlagName.value="自动垫交" ;
}
}
catch(ex)
{}

}

/**
 * FUNCTION:check_BnfGrid()
 * DESC:在提交和修改__受益人信息__的时候,进行校验.
 *      主要进行校验的内容是:如果选择了速填,那么要对__出生日期__证件类型__证件号码进行校验.
 *                        必须和数据库当中已经存入的数据相同否则报错.
 */
 function check_BnfGrid(){
// BnfGrid.DelBlankLine();
 //暂时只对__个单进行校验
 if(LoadFlag==1){
for(var rowNum=0;rowNum<BnfGrid.mulLineCount;rowNum++)
{
    //是否选择了速填.
     var customertype=BnfGrid.getRowColData(rowNum,10);
     if(customertype!=null&&customertype=="1")  //被保人
     {
     var  insuredSQL=" select name,Birthday,IDType,IDNo from LCInsured where ContNo='"+fm.ContNo.value+"'" 
                    +" union"
                    +" select name,Birthday,IDType,IDNo from LBInsured where ContNo='"+fm.ContNo.value+"'" 
                    ;
     var arrResult = easyExecSql(insuredSQL);
     if(arrResult!=null){
    if(!(arrResult[0][0]==BnfGrid.getRowColData(rowNum,2)&&arrResult[0][1]==BnfGrid.getRowColData(rowNum,3)&&arrResult[0][2]==BnfGrid.getRowColData(rowNum,4)&&arrResult[0][3]==BnfGrid.getRowColData(rowNum,5) ))
     {
      return false;}
        }
     }
     if(customertype!=null&&customertype=="0")  //投保人
     {
     var  insuredSQL=" select AppntName,AppntBirthday,IDType,IDNo from LCAppnt where ContNo='"+fm.ContNo.value+"'" 
                    +" union"
                    +" select AppntName,AppntBirthday,IDType,IDNo from LBAppnt where ContNo='"+fm.ContNo.value+"'" 
                    ;
     var arrResult = easyExecSql(insuredSQL);
     if(arrResult!=null){
    if(!(arrResult[0][0]==BnfGrid.getRowColData(rowNum,2)&&arrResult[0][1]==BnfGrid.getRowColData(rowNum,3)&&arrResult[0][2]==BnfGrid.getRowColData(rowNum,4)&&arrResult[0][3]==BnfGrid.getRowColData(rowNum,5) ))
     {

      return false;}
        }
     }

}
 }
return true ;
}
function checkpayintv()
{
     var  insuredSQL="select PayIntv from LCCont where ContNo='"+fm.ContNo.value+"'"
                    +" union"
                    +" select PayIntv from LBCont where ContNo='"+fm.ContNo.value+"'"
                    ;
     var arrResult = easyExecSql(insuredSQL);
     if(arrResult!=null)
     {
       if(fm.PayIntv.value!=""&&arrResult[0][0]!=fm.PayIntv.value&&fm.RiskCode.value=="01303")
       {
       	alert("险种缴费方式:"+fm.PayIntv.value+"与合同的缴费方式:"+arrResult[0][0]+"不一致");
        return false;
       }        
     }
    return true; 	
}
function IssuePol()
{
	if (!confirm("将该单标记为特殊件,转由复核人员处理,确认吗？"))	
	return false;
	inputflag="2";
	queryString = "MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PrtNo="+fm.all('PrtNo').value+"&ContNo="+parent.VD.gVSwitch.getVar("ContNo");
  var showStr="正在标记该单为特殊件，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./IssuePol.jsp?FrameType=0&"+queryString;
  fm.submit(); //提交	
}
function getPolDetail()
{

	    var tRow = PolGrid.getSelNo() - 1;      	
			var tPolNo = PolGrid.getRowColData(tRow, 1);			
			queryPolDetail( tPolNo );
}

//公共帐户特殊初始化
function initPubInsuAcc(cRiskCode)
{
	if(cRiskCode=='20701')
	{
		//showDiv(DivLCKindGetRate, "false");
		DivLCKindGetRate.style.display="none";
	}
	DivLCKind1.style.display="none";
	DivLCKindButton.style.display="none";
}
function mik01(){
/*var amnt=fm.Amnt.value;
var mod=amnt%1000;
if(mod!=0||amnt<3000){
	alert("保额必须大于3000而且为1000的整数倍");
	return false;
}
	return true;*/
}

function checkDuty(){ 
  //modified by zhangjq on 2007-11-13,以下4行，改变"指定账户型产品编码"为"查询数据库是否为账户型产品"
  var strSql = "select '1'||a.InsuAccFlag from lmrisk a where a.riskcode='"
          + fm.all('RiskCode').value + "'";
  var InsuAccFlagArr = easyExecSql(strSql);
  if(InsuAccFlagArr[0]=="1Y" || InsuAccFlagArr[0]=="1y" ){
  	//账户型产品不执行此项校验.
  	return true;
  }
	if(DutyGrid.mulLineCount >0){
		var getWhere = "('000000',";//默认添加一个 防止里面没有数据 后面substring会剪掉, chenwm20070831
		for (i=0;i<DutyGrid.mulLineCount;i++){
			if (DutyGrid.getChkNo(i)){
				 getWhere = getWhere + "'"+DutyGrid.getRowColData(i,1)+"',"
			}
		}
		
		getWhere = getWhere.substring(0,getWhere.length-1) + ")"
	  var SQL = "select count(distinct dutycode) from lmriskduty where riskcode='"+fm.all('RiskCode').value+"' AND choflag!='B' AND CHOFLAG='M' AND dutycode not in "+getWhere+"";
		var arrResult=easyExecSql(SQL);
		//modified by zhangjq on 2007-11-13,移至本函数开头判断
		//if(fm.all('RiskCode').value=="SEK1" || fm.all('RiskCode').value=="UEK1")
		//{}
		//else if(arrResult != 0)
		if(arrResult != 0)
		{
				alert("还有必选责任编码没有选！");
				return false;
		}
		
	}
		return true ;
}
//校验附加险和主险的保险期间
function checkInsurYear(){
	var SQL = " select * from lmriskapp where subriskflag = 'S' and riskcode='"+fm.all('RiskCode').value+"'";
	var arrResult = easyExecSql(SQL);
	if(arrResult != null ){
			var sql = "select insuyear,InsuYearFlag from LCDuty where polno='"+parent.VD.gVSwitch.getVar("mainRiskPolNo")+"'";
			var arrResult = easyExecSql(sql);
			if(arrResult != null){
				//如果附加险的保险期间单位和主险的保险期间单位一致,附加险大于主险.给出提示
				 if((parseInt(fm.all('InsuYear').value) > arrResult[0][0]) && (fm.all('InsuYearFlag').value == arrResult[0][1])){
				 		alert("附加险保险期间不能长于主险保险期间！");
				 		return false;
				 		//如果,主险的保险期间单位为D天,而附加险的保险期间单位为M月,转化单位进行比较
				 }else if(fm.all("InsuYearFlag").value=="M" && arrResult[0][1]=="D"){
				 		if((fm.all('InsuYear').value *31) > arrResult[0][1]){
				 				alert("附加险保险期间不能长于主险保险期间！");
				 				return false;
				 		}
				 		//如果,主险的保险期间单位为M月,而附加险的保险期间单位为D天,转化单位进行比较
				 }else if(fm.all("InsuYearFlag").value=="D" && arrResult[0][1]=="M"){
						 	if(fm.all('InsuYear').value > (arrResult[0][1]*31)){
						 				alert("附加险保险期间不能长于主险保险期间！");
						 				return false;
						 	}
						 	//如果,主险的保险期间单位为Y年,而附加险的保险期间单位为M天,转化单位进行比较
				 }else if(fm.all("InsuYearFlag").value=="D" && arrResult[0][1]=="Y"){
				 				if(fm.all('InsuYear').value > (arrResult[0][1]*366)){
						 				alert("附加险保险期间不能长于主险保险期间！");
						 				return false;
						 		}
						 		//如果,主险的保险期间单位为Y年,而附加险的保险期间单位为M月,转化单位进行比较
				 }else if(fm.all("InsuYearFlag").value=="M" && arrResult[0][1]=="Y"){
				 				if((fm.all('InsuYear').value*31) > (arrResult[0][1]*366)){
						 				alert("附加险保险期间不能长于主险保险期间！");
						 				return false;
						 		}
				 }
			}
	}
	return true;
}
//针对险种NLK01、NOK01、NOK02设置的校验函数 zhanjq 2007-9-29
function checkflag(){
	//alert(fm.all("PolTypeFlag").value);
	if(fm.all('RiskCode').value=='NOK01'){
		var InsuYearFlag = fm.all("InsuYearFlag").value;
	  	var ptn = /^\d+\d$/;
	  	if(!fm.all("InsuYear").value=="" && parseInt(fm.all("InsuYear").value)<0){
	  		alert("保险期间必须大于0！");
	  		return false;
	  	}
	  	if(!fm.all("InsuYear").value=="" && !ptn.test(fm.all("InsuYear").value+0)){
	  		alert("保险期间必须为整数！");
	  		fm.all("InsuYear").focus();
	  		return false;
	  	}
	  	if(InsuYearFlag=="D"){
	  		if(!fm.all("InsuYear").value =="" && (fm.all("InsuYear").value<1 || fm.all("InsuYear").value>365)){
	  			alert("对于险种NOK01,保险期间必须在1－365之间！");
	  			fm.all("InsuYear").focus();
	  			return false;
	  		}
	  	}else if(InsuYearFlag=="Y" && fm.all('InsuYear').value != "" && fm.all('InsuYear').value != "1"){
	  	  alert("对于险种NOK01,当保险期间单位为Y-年时，保险期间只能为1！");
				fm.all('InsuYear').value = 1;
				return false;
	  	}
	  	var StandbyFlag1=fm.all('StandbyFlag1').value;
	  	if(StandbyFlag1!='1' && StandbyFlag1!='2' && StandbyFlag1!='3' && StandbyFlag1!='4'){
	  		alert("对于险种NOK01,计划类别必须为“1-计划一,2-计划二,3-计划三,4-计划四”之一！");
	  		return false;  		
	  	}
	  	//if(fm.all('CalRule').value!='0'){
	  	//	alert("对于险种NOK01,计算规则必须为0-表定费率！");
	  	//	return false;  		
	  	//}
	}else if(fm.all('RiskCode').value=='NOK02'){
		var InsuYearFlag = fm.all("InsuYearFlag").value;
	  	var ptn = /^\d+\d$/;
	  	if(!fm.all("InsuYear").value=="" && parseInt(fm.all("InsuYear").value)<0){
	  		alert("保险期间必须大于0！");
	  		return false;
	  	}
	  	if(!fm.all("InsuYear").value=="" && !ptn.test(fm.all("InsuYear").value+0)){
	  		alert("保险期间必须为整数！");
	  		fm.all("InsuYear").focus();
	  		return false;
	  	}
	  	if(InsuYearFlag=="D"){
	  		if(fm.all('InsuYear').value > 90 || fm.all('InsuYear').value < 0 ){
	  			alert('对于险种NOK02,保险期间必须在1天到90天之间(包含1天和90天)');
	  			fm.all('InsuYear').focus();
	  			return false;
	  		}
	  	}else{
	  		alert("对于险种NOK02,保险期间单位只能为D-天！");
	  		fm.all("InsuYearFlag").focus();
	  		return false;
	  	}
	  	var StandbyFlag1=fm.all('StandbyFlag1').value;
	  	if(StandbyFlag1!='P' && StandbyFlag1!='F'){
	  		alert("对于险种NOK02,计划类别必须为“P-凯撒(个人),F-凯撒(家庭)”之一！");
	  		return false;  		
	  	}
	  	//if(fm.all('CalRule').value!='0'){
	  	//	alert("对于险种NOK02,计算规则必须为0-表定费率！");
	  	//	return false;  		
	  	//}
	}else if(fm.all('RiskCode').value=='NLK01'){
	  var InsuYearFlag = fm.all("InsuYearFlag").value;
	  	var ptn = /^\d+\d$/;
	  	if(!fm.all("InsuYear").value=="" && parseInt(fm.all("InsuYear").value)<0){
	  		alert("保险期间必须大于0！");
	  		return false;
	  	}
	  	if(!fm.all("InsuYear").value=="" && !ptn.test(fm.all("InsuYear").value+0)){
	  		alert("保险期间必须为整数！");
	  		fm.all("InsuYear").focus();
	  		return false;
	  	}
	  	if(InsuYearFlag=="D"){
	  		if(fm.all('InsuYear').value > 365 || fm.all('InsuYear').value < 0 ){
	  			alert('对于险种NLK01,保险期间必须在1－365之间！');
	  			fm.all('InsuYear').focus();
	  			return false;
	  		}
	  	}else{
	  		alert("对于险种NLK01,保险期间单位只能为D-天！");
	  		fm.all("InsuYearFlag").focus();
	  		return false;
	  	}
	  	//if(fm.all('CalRule').value!='0'){
	  	//	alert("对于险种NLK01,计算规则必须为0-表定费率！");
	  	//	return false;  		
	  	//}
  }else if(fm.all('RiskCode').value=='MOK01'){
	  var InsuYearFlag = fm.all("InsuYearFlag").value;
	  	var ptn = /^\d+\d$/;
	  	if(!fm.all("InsuYear").value=="" && parseInt(fm.all("InsuYear").value)<0){
	  		alert("保险期间必须大于0！");
	  		return false;
	  	}
	  	if(!fm.all("InsuYear").value=="" && !ptn.test(fm.all("InsuYear").value+0)){
	  		alert("保险期间必须为整数！");
	  		fm.all("InsuYear").focus();
	  		return false;
	  	}
	  	if(InsuYearFlag=="D"){
	  		if(fm.all('InsuYear').value > 365 || fm.all('InsuYear').value < 0 ){
	  			alert('对于险种MOK01,保险期间必须在1－365之间！');
	  			fm.all('InsuYear').focus();
	  			return false;
	  		}
	  	}else{
	  		alert("对于险种MOK01,保险期间单位只能为D-天！");
	  		fm.all("InsuYearFlag").focus();
	  		return false;
	  	}
	  	//if(fm.all('CalRule').value!='0'){
	  	//	alert("对于险种MOK01,计算规则必须为0-表定费率！");
	  	//	return false;  		
	  	//}
	  	if( fm.all('Amnt').value!=""){
	  		if(parseInt(fm.all('Amnt').value)<0){
	  			alert("保额必须大于0！");
	  			fm.all('Amnt').focus();
	  			return false;
	  		}else if(fm.all('Amnt').value*0!=0 && !ptn.test(fm.all('Amnt').value+0)){
	  			alert("保额必须为整数！");
	  			fm.all('Amnt').focus();
	  			return false;
	  		}else if(parseInt(fm.all('Amnt').value)%1000!=0){
	  			alert("对于险种MOK01,保额只能录入1000的整数倍！");
	  			fm.all('Amnt').focus();
	  		  return false;
	  		}
	  	}
//	  	fm.all('Prem').value='';
  }else if(fm.all('RiskCode').value=='NLK02'){
  	var ptn = /^\d+\d$/;
  	var insuyear,insuyearflag,message;
	  for(var i=0;i<5;i++){
	  	if(DutyGrid.getChkNo(i)){
	  	  insuyear = DutyGrid.getRowColData(i,3);
	  	  insuyearflag = DutyGrid.getRowColData(i,4);
	  	  if(insuyear==""){
	  	  	alert("保险期间不能为空！");
  		    return false;
	  	  }
	  		if(parseInt(insuyear)<0){
  		    alert("保险期间必须大于0！");
  		    return false;
  	    }
  	    if(!ptn.test(insuyear+0)){
  		    alert("保险期间必须为整数！");
  		    return false;
  	    }
  	    message = DutyGrid.getRowColData(i,1)+"--"+DutyGrid.getRowColData(i,2);
  	    if(insuyearflag=="D"){
  		    if(insuyear > 180 || insuyear < 0 ){
  		    	
  			    alert('对于险种NLK02,保险期间必须在1－180之间！请修改责任“'+message+'”对应的保险期间！');
  			    return false;
  		    }
  	    }else{
  		    alert("对于险种NLK02,保险期间单位只能为D-天！请修改责任“"+message+"”对应的保险期间单位！");
      		return false;
  	    }
  	    //if(DutyGrid.getRowColData(i,16)!='0'){
  		  //  alert("对于险种NLK02,计算规则必须为0-表定费率！请修改责任“"+message+"”对应的计算规则！");
  		  //  return false;  		
  	    //}
  	    if(DutyGrid.getRowColData(i,12)!='G'){
  		    alert("对于险种NLK02,计算方向必须为G-保额算保费！请修改责任“"+message+"”对应的计算方向！");
  		    return false;  		
  	    }
  	    DutyGrid.setRowColData(i,6,'');
	  	}
	  }
	}else if(fm.all('RiskCode').value=='SEK1'){
		if(fm.all("PolTypeFlag").value!='2'&&fm.all("PolTypeFlag").value!='5'){//公共保额 公共帐户除外
			for(var i=0;i<DutyGrid.mulLineCount;i++){
				if(DutyGrid.getChkNo(i)){
					var dutycode = DutyGrid.getRowColData(i,1);
					//alert(dutycode);
					if(dutycode=='620006'){
						return true;
					}
				}
			}
			alert("久利年金险必须录入个人自愿账户.");
			return false;
		}
  }else if(fm.all('RiskCode').value=='UEK1'){
		if(fm.all("PolTypeFlag").value!='2'&&fm.all("PolTypeFlag").value!='5'){//公共保额 公共帐户除外
			for(var i=0;i<DutyGrid.mulLineCount;i++){
				if(DutyGrid.getChkNo(i)){
					var dutycode = DutyGrid.getRowColData(i,1);
					//alert(dutycode);
					if(dutycode=='621006'){
						return true;
					}
				}
			}
			alert("丰利投连险必须录入个人自愿账户.");
			return false;
		}  	
  }else if(fm.all('RiskCode').value=='UEK2'){
		if(fm.all("PolTypeFlag").value!='2'&&fm.all("PolTypeFlag").value!='5'){//公共保额 公共帐户除外
			for(var i=0;i<DutyGrid.mulLineCount;i++){
				if(DutyGrid.getChkNo(i)){
					var dutycode = DutyGrid.getRowColData(i,1);
					if(dutycode=='630006'){
						return true;
					}
				}
			}
			alert("创智企业留才团体投资连结保险(UEK2)必须录入个人自愿缴费账户！");
			return false;
		}  	
  }else if(fm.all('RiskCode').value=='SEK2'){
		if(fm.all("PolTypeFlag").value!='2'&&fm.all("PolTypeFlag").value!='5'){//公共保额 公共帐户除外
			for(var i=0;i<DutyGrid.mulLineCount;i++){
				if(DutyGrid.getChkNo(i)){
					var dutycode = DutyGrid.getRowColData(i,1);
					if(dutycode=='631006'){
						return true;
					}
				}
			}
			alert("创贤企业留才团体两全保险（万能型）(SEK2)必须录入个人自愿缴费账户！");
			return false;
		}  	
	}
	//alert(fm.all("PolTypeFlag").value=='2');
	if(fm.all("PolTypeFlag").value=='2'){//公共帐户
		var tSql="SELECT c.dutycode FROM lmriskduty c ,lmdutypayrela a ,lmdutypay b WHERE a.payplancode "
			+"=b.payplancode AND b.payplancode not like '%%03' and b.accpayclass in ('1','2','3') AND c.dutycode=a.dutycode AND c.riskcode in(select riskcode From lmriskapp where risktype3 in('3','4')) AND c.riskcode='"
			+fm.all('RiskCode').value+"' "
		//alert(tSql);	
		var arrResult = easyExecSql(tSql);
		if(arrResult != null ){
			var include=false;
			for(var b=0;b<arrResult.length;b++){
				include=false;
				for(var i=0;i<DutyGrid.mulLineCount;i++){
					if(DutyGrid.getChkNo(i)){
						var dutycode = DutyGrid.getRowColData(i,1);
						//alert(dutycode);
						if(dutycode==arrResult[b]){
							if(DutyGrid.getRowColData(i,13)!=null&&DutyGrid.getRowColData(i,13)!=''
								&&DutyGrid.getRowColData(i,13)!=0&&DutyGrid.getRowColData(i,13)!='0.00'){
								alert("公共帐户录入的未分配保费账户和未归属资金账户都必须为零.");
								return false;									
							}
							include=true;
							continue;
						}
					}
				}
				if(!include){
					alert("公共帐户必须录入未分配保费账户和未归属资金账户,且都为零!");
					return false;
				}
			}
			if(!include){
				alert("公共帐户必须录入未分配保费账户和未归属资金账户,且都为零.");
				return false;
			}
			
		}			
	}		  	
	return true;
}
  //保存修改时对险种的特殊校验
function checkRiskInfo()
{
      //安康产品特有校验
      if(fm.RiskCode.value=="NHK01")
      {
	   		var CPCount=DutyGrid.mulLineCount;
	  		for(var i=0;i<CPCount;i++)
	   		{
	      		if(DutyGrid.getRowColData(i,1)=="635001" )
	      		{
	          		if(DutyGrid.getRowColData(i,20)==null || DutyGrid.getRowColData(i,20)=="" )
	          		{
	               		alert("安康险种专项疾病责任赔付比例为空！请检查！");
		           		return false;
	          		}
	          		else
	          		{
	              		var tGetRate = DutyGrid.getRowColData(i,20);
	              		if(tGetRate<0.1 || tGetRate>0.5)
	              		{
	                   		alert("安康险种专项疾病责任赔付比例不能小于0.1且不能大于0.5 ！");
		               		return false;
	              		}
	          		}
	      
	      		}
	   		}
	   }
	      //SCK01产品特有校验
	   else if(fm.RiskCode.value=="SCK01")
	   {
	   		var CPCount=DutyGrid.mulLineCount;
	  		for(var i=0;i<CPCount;i++)
	  		{
	      		if(DutyGrid.getRowColData(i,1)=="633012" )
	     		{
	          		if(DutyGrid.getRowColData(i,13)!=null && DutyGrid.getRowColData(i,13)!="" && DutyGrid.getRowColData(i,13)!=".00")
	          		{
	               		alert("第"+(i+1)+"行风险保额责任保费项不需要录入！");
	               		DutyGrid.setRowColData(i,13,".00");
		           		return false;
	         		}
	     	    }
	     	    else
	     	    {
	     	        if(DutyGrid.getRowColData(i,14)!=null && DutyGrid.getRowColData(i,14)!="" && DutyGrid.getRowColData(i,14)!=".00")
	          		{
	               		alert("第"+(i+1)+"行风险保额项不需要录入！");
	               		DutyGrid.setRowColData(i,14,".00");
		           		return false;
	         		}
	     	    }
	 	    }
	   }
	  //NIK08产品特有校验,1所有责任就诊区域必须一致，2计算方向必须为I
	   else if(fm.RiskCode.value=="NIK08")
	   {
	   		var CPCount=DutyGrid.mulLineCount;
	   		var tArea = "";
	   		//保存住院责任的就诊区域
	   		for(var i=0;i<CPCount;i++)
	  		{
	      		if(DutyGrid.getRowColData(i,1)=="641001" )
	     		{
	   		        if(DutyGrid.getRowColData(i,9)==null || DutyGrid.getRowColData(i,9)=="")
	   		        {
	   		             alert("第"+(i+1)+"行住院责任就诊区域不允许为空！");
	   		             return false;
	   		        }
	   		        tArea = DutyGrid.getRowColData(0,9);
	   		    }
	   		}
	  		for(var i=0;i<CPCount;i++)
	  		{
	      		if(DutyGrid.getRowColData(i,9)!=null && DutyGrid.getRowColData(i,9)!="" && DutyGrid.getRowColData(i,9)!=tArea)
	      		{
	      		      alert("第"+(i+1)+"行就诊区域和住院责任就诊区域不一致！");
	      		      DutyGrid.setRowColData(i,9,tArea);
		              return false;
	      		}
	      		if(DutyGrid.getRowColData(i,12)!=null && DutyGrid.getRowColData(i,12)!="" && DutyGrid.getRowColData(i,12)!="I")
	      		{
	      		      alert("第"+(i+1)+"行计算方向必须为<I-录入保费保额>！");
	      		      DutyGrid.setRowColData(i,12,"I");
		              return false;
	      		}
	 	    }
	   }
	   //NIK07产品特有校验,1所有责任就诊区域必须一致  ASR20094060-全球医疗产品需要录入就医区域
	   else if(fm.RiskCode.value=="NIK07")
	   {
	   		var CPCount=DutyGrid.mulLineCount;
	   		var tArea = "";
	   		//保存住院责任的就诊区域
	   		for(var i=0;i<CPCount;i++)
	  		{
	      		if(DutyGrid.getRowColData(i,1)=="637001" )
	     		{
	   		        if(DutyGrid.getRowColData(i,9)==null || DutyGrid.getRowColData(i,9)=="")
	   		        {
	   		             alert("第"+(i+1)+"行住院责任就诊区域不允许为空！");
	   		             return false;
	   		        }
	   		        tArea = DutyGrid.getRowColData(0,9);
	   		    }
	   		}
	  		for(var i=0;i<CPCount;i++)
	  		{
	      		if(DutyGrid.getRowColData(i,9)!=null && DutyGrid.getRowColData(i,9)!="" && DutyGrid.getRowColData(i,9)!=tArea)
	      		{
	      		      alert("第"+(i+1)+"行就诊区域和住院责任就诊区域不一致！");
	      		      DutyGrid.setRowColData(i,9,tArea);
		              return false;
	      		}
	 	    }
	   }
	    //NIK09产品特有校验,1计算方向必须为I
	   else if(fm.RiskCode.value=="NIK09")
	   {
	        var CPCount=DutyGrid.mulLineCount;
	  		for(var i=0;i<CPCount;i++)
	  		{
	      		if(DutyGrid.getChkNo(i))
	      		{
	      			if(DutyGrid.getRowColData(i,12)!=null && DutyGrid.getRowColData(i,12)!="" && DutyGrid.getRowColData(i,12)!="I")
	      			{
	      		      	alert("第"+(i+1)+"行计算方向必须为<I-录入保费保额>！");
	      		      	DutyGrid.setRowColData(i,12,"I");
		              	return false;
	      			}
	      			if(DutyGrid.getRowColData(i,13)!=null && DutyGrid.getRowColData(i,13)!="" && isNaN(DutyGrid.getRowColData(i,13)+0))
	      			{
	      		      	alert("第"+(i+1)+"行保费必须为整数！");
		              	return false;
	      			}
	      			if(DutyGrid.getRowColData(i,14)!=null && DutyGrid.getRowColData(i,14)!="" && isNaN(DutyGrid.getRowColData(i,14)+0))
	      			{
	      		      	alert("第"+(i+1)+"行保额必须为整数！");
		              	return false;
	      			}
	      			if(DutyGrid.getRowColData(i,19)!=null && DutyGrid.getRowColData(i,19)!="" && isNaN(DutyGrid.getRowColData(i,19)+0))
	      			{
	      		      	alert("第"+(i+1)+"行免赔额必须为整数！");
		              	return false;
	      			}      			
	      			if(DutyGrid.getRowColData(i,22)!=null && DutyGrid.getRowColData(i,22)!="" && isNaN(DutyGrid.getRowColData(i,22)+0))
	      			{
	      		      	alert("第"+(i+1)+"行最高赔付限额必须为整数！");
		              	return false;
	      			}
	      		}
	 	    }
	   }else if(fm.all("PolTypeFlag").value=="0")
	    {
	     if(fm.RiskCode.value=="370101"){
	    if(fm.AscriptionRuleCode.value==""){ 
          	alert("归属规则不能为空");
          	return false;
	  	 }  
	   if(fm.GetYear.value==""){
	     	 alert("开始领取年龄不能为空");
	   		 return false;
	   		}else{
     	var arrQueryResult = easyExecSql("select impartparammodle from LCCustomerImpart where prtno='"+fm.PrtNo.value+"' and ImpartCode = '010' and Impartver='008'", 1, 0);
     	if(arrQueryResult==null||arrQueryResult.length==0)
	    	 {
		   	 	 alert("老年护理保险金领取起始日查询异常！退出");
		   		 return false;
			 }else{
		 var strAscriptionRule = arrQueryResult[0][0];
      	 var strValue=strAscriptionRule.split(",");
   		 if(strValue[0]=='Y'){	
           var arrQueryResult1 = easyExecSql("select Sex from lcinsured where prtno='"+fm.PrtNo.value+"' and InsuredNo ='"+ fm.CustomerNo.value+"'", 1, 0);
              if(arrQueryResult1==null||arrQueryResult1.length==0)
                    { 
                       alert("被保险人性别查询异常！退出");
		                return false;
                     }
                     else{
                     if(arrQueryResult1[0][0]=="0"){    
                            if(fm.GetYear.value!=strValue[1]){
                            alert("保单领取起始类型为全员统一，所以开始领取年龄应为"+strValue[1]+"！");
                            return false;
                            }
                     	}else{
                       	if(fm.GetYear.value!=strValue[2])
                      		 {	
                      		 alert("保单领取起始类型为全员统一，所以开始领取年龄应为"+strValue[2]+"！");
                             return false;
                     		}
                
		 				 }
          			 }
       			 } 
		}
		}
		//领取类型的判断	
		if(fm.GetDutyKind.value=="")
		{
		    alert("领取类型不能为空");	
		     return false;
		 }else{
      var arrQueryResult2 = easyExecSql("select impartparammodle from LCCustomerImpart where prtno='"+fm.PrtNo.value+"' and ImpartCode = '010' and Impartver='009'", 1, 0);
      if(arrQueryResult2==null||arrQueryResult2.length==0)
	     {
		    alert("老年护理保险金领取方式查询异常！退出");
		    return false;
		 }else{
			var strAscriptionRule2 = arrQueryResult2[0][0];
     	    var strValue2=strAscriptionRule2.split(",");
    	 if(strValue2[0]=='Y'){
          if(fm.GetDutyKind.value!='A'){
            alert("老年护理保险金领取方式应为一次性领取");
            return false;
          }
          }
          else if(strValue2[1]=='Y'){
          if(fm.GetDutyKind.value!='B'){
            alert("老年护理保险金领取方式应为按年或按月分期领取");
            return false;
          }
          }
          else if(strValue2[2]=='Y'){
          if(fm.GetDutyKind.value!='C'){
            alert("老年护理保险金领取方式应为部分分期领取");
            return false;
          }
          }else if(strValue2[3]=='Y'){
           if(fm.GetDutyKind.value!='D'){
            alert("老年护理保险金领取方式应为按年或按月分次领取");
            return false;
          }
          }
          
          }
          }
          }
	  if(arrQueryResult[0] == "S")    //需要转成大写
		return false;
	   
	   }
	   return true;
}

function CheckDate2()
{
 try
 {
     try
     {
          var mInsuYear="";
          var mInsuYearFlag="";
     	  if(DutyGrid==null||DutyGrid.mulLineCount==0)
     	  {
     	     var InsuYear=fm.InsuYear.value;
	         var InsuYearFlag=fm.InsuYearFlag.value;
	         mInsuYear=InsuYear;
	         mInsuYearFlag=InsuYearFlag;
	         if(InsuYear==null||InsuYear=="")
	         {
	            alert("请输入保险期间！");
	            return false;
	         }
	         if(InsuYearFlag==null||InsuYearFlag=="")
	         {
	            alert("请输入保险期间单位！");
	            return false;
	         }
     	  }
     	  else
     	  { 
     	     var m=-1;
     	     for(var i=0;i<DutyGrid.mulLineCount;i++)//判断是否选中记录
		     { 
				if( DutyGrid.getChkNo(i)==true)
				{ 
					m=i;
					break;    
			    }
		     }
			 if(m==-1)
			 {
				alert("请选择记录！");
				return false;
			 } 
     	  }
     }
     catch(re)
     {
     	   alert("Error");
     }
 }
 catch(re)
 {
 }
 var ShortRiskSql=" select 1 from lmriskapp where riskcode "//极短期
                  +" in (select riskcode from lcgrppol where "
                  +" prtno='"+prtNo+"') and risktype9='1' ";
  var ShortRiskResult = easyExecSql(ShortRiskSql); 	
    
  if(ShortRiskResult==null||ShortRiskResult.length==0)//说明不是极短期险
  {
	   var LongRiskSql=" select 1 from lmriskapp where riskcode "//长期险
	                  +" ='"+fm.RiskCode.value+"' and riskperiod='L' ";
	   var LongRiskResult = easyExecSql(LongRiskSql); 	
	   if(LongRiskResult==null||LongRiskResult.length==0)//说明不是长期险
	   {  
	   		var DutyInfoArray=new Array();
			var index=0;
			for(var i=0;i<DutyGrid.mulLineCount;i++)
			{ 
			   if( DutyGrid.getChkNo(i)==true)
			   {   
			        DutyInfoArray[index]=new Array();
			        DutyInfoArray[index][0]=fm.RiskCode.value;//险种号
			        DutyInfoArray[index][1]=DutyGrid.getRowColData(i,1);//责任号
			       
			        if(DutyGrid.getRowColData(i,3)==null||DutyGrid.getRowColData(i,3)=="")
			        {
			            alert("请输入期间！");
			            return false;
			        }
			        DutyInfoArray[index][2]=DutyGrid.getRowColData(i,3);//时间段
			        
			        if(DutyGrid.getRowColData(i,4)==null||DutyGrid.getRowColData(i,4)=="")
			        {
			            alert("请输入期间单位");
			            return false;
			        }
			        DutyInfoArray[index][3]=DutyGrid.getRowColData(i,4);//时间单位
			        mInsuYear=DutyGrid.getRowColData(i,3);
                    mInsuYearFlag=DutyGrid.getRowColData(i,4);
			        index++;
			   }
		   }
		    
	       if(EdorType!="null")
	       {       
	          var cvalidateSql="select cvalidate from lcgrppol where GrpContNo='"+fm.GrpContNo.value+"'  and rownum <= 1";  
	       }
	       else
	       {
	          var cvalidateSql="select cvalidate from lcgrppol where prtno='"+prtNo+"' and rownum <= 1";    
	       }
		   var cvalidateResult = easyExecSql(cvalidateSql); 	
		   if(cvalidateResult==null||cvalidateResult.length==0)//查找cvalidate
		   {
		        alert("cvalidate||edorvalidate查询异常！退出！");
		        return false;
		   }
		   else
		   {
		        var CvaliDate=cvalidateResult[0][0];
		        var StandEndDate="test";
		        var Flag=0;
		      
		        for(var i=0;i<index;i++)//主险
		        {
		        	var EndDateSql=" select (case '"+DutyInfoArray[i][3]+"' when 'M' then "
		        								+" add_months(to_date('"+CvaliDate+"','YYYY-MM-DD'),"+DutyInfoArray[i][2]+") "
		        								+" when 'Y' then "
		        								+" add_months(to_date('"+CvaliDate+"','YYYY-MM-DD'),12*"+DutyInfoArray[i][2]+") "
		        								+" when 'D' then "
		        								+" to_date('"+CvaliDate+"','YYYY-MM-DD')+"+DutyInfoArray[i][2]+" end ) from dual ";
		          	var EndDateResult = easyExecSql(EndDateSql); 	 
		         	if(EndDateResult==null||EndDateResult.length==0)
		          	{
		              	alert("EndDate查询异常！退出");
		              	return false;
		          	}
		          	else
		          	{
		             	if(StandEndDate=="test")
		             	{
		                	StandEndDate=EndDateResult[0][0];
		             	}
		             	else
		             	{
		                	if(StandEndDate!=EndDateResult[0][0])
		                	{
		                		Flag=1;
		                		break;
		                	}
		             	}
		          	}       								
		        }
		       // alert("StandEndDate="+StandEndDate);
		        if(index==0)//附加险
		        {
		           	var EndDateSql=" select (case '"+InsuYearFlag+"' when 'M' then "
		        								+" add_months(to_date('"+CvaliDate+"','YYYY-MM-DD'),"+InsuYear+") "
		        								+" when 'Y' then "
		        								+" add_months(to_date('"+CvaliDate+"','YYYY-MM-DD'),12*"+InsuYear+") "
		        								+" when 'D' then "
		        								+" to_date('"+CvaliDate+"','YYYY-MM-DD')+"+InsuYear+" end ) from dual ";
		          	var EndDateResult = easyExecSql(EndDateSql); 	 
		          	if(EndDateResult==null||EndDateResult.length==0)
		          	{
		              	alert("EndDate查询异常！退出");
		              	return false;
		          	}
		          	else
		          	{
		             	if(StandEndDate=="test")
		             	{
		                	StandEndDate=EndDateResult[0][0];
		             	}
		          	}
		        
		        }
		       // alert("Flag="+Flag);
		        if(Flag==1)//说明本次录入时间不相等
		        {
		           alert("本次录入保险期间不相等！请检查！");
		           return false;
		        }
		        else//本次录入时间相等
		        {
		        	var ThisPeopleEndDateSql =" select enddate from lcpol where prtno='"+prtNo+"' "
		        	                          +" and insuredno='"+fm.CustomerNo.value+"' and riskcode<>'"+fm.RiskCode.value+"' ";
					var ThisPeopleEndDateResult = easyExecSql(ThisPeopleEndDateSql); 	
					if(ThisPeopleEndDateResult!=null&&ThisPeopleEndDateResult.length!=0)
					{
						var ThisPoepleEndDate=ThisPeopleEndDateResult[0][0];
						if(StandEndDate!=ThisPoepleEndDate)
						{
							alert("录入保险期间与此人其它险种期间不同！请检查！");
							return false;
						}
					}						 		        	                          
		            var OtherPeopleEndDateSql=" select max(enddate) from lcpol where prtno='"+prtNo+"' "
		                                    +" and insuredno<>'"+fm.CustomerNo.value+"'";
		                                    
               		var OtherPeopleEndDateResult = easyExecSql(OtherPeopleEndDateSql); 	
               		if(OtherPeopleEndDateResult==null||OtherPeopleEndDateResult.length==0 || OtherPeopleEndDateResult[0][0]=="")//没有其他人
               		{ 
               	 		var contplansql=" select a.calfactorvalue from lccontplandutyparam a,lccontplan b,lcgrpcont c"
               	        	+" where a.grpcontno=b.grpcontno and a.contplancode=b.contplancode and a.grpcontno=c.grpcontno"
               	        	+" and b.contplancode<>'00' and c.prtno='"+prtNo+"' and a.calfactor in ('InsuYear','InsuYearFlag')";
                 		var ContPlanResult = easyExecSql(contplansql); 
                 		if(null==ContPlanResult||ContPlanResult.length==0)	
                 		{
                   			return true;
                 		}
                 		else
                 		{
                    		if(
                       			(mInsuYear==ContPlanResult[0][0]&&mInsuYearFlag==ContPlanResult[1][0])||
                       			(mInsuYear==ContPlanResult[1][0]&&mInsuYearFlag==ContPlanResult[0][0])
                      		)
                    		{
                       			return true;
                    		}
                    		else
                    		{
                      			alert("您输入的保险期间与保障层级不相符！请检查！");
                      			return false;
                    		}
                 		}
               		}
               		else
               		{
                  		var OtherPeopleEnd=OtherPeopleEndDateResult[0][0];
               			//   alert("OtherPeopleEnd="+OtherPeopleEnd);
                  		if(StandEndDate==OtherPeopleEnd)//时间相等
                  		{ 
                     		return true;
                  		}
                  		else
                  		{
                      		alert("此人录入保险期间与其它被保人录入保险期间不相等！请检查！");
                      		return false;
                  		}
               		} 	                  
		       }     
		    }
	    }
	    else
	    { 
	        return true;
	    }                                     
  }
  else
  { 
     return true;
  }
  return true;
}