<%
//程序名称：LCInuredListQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-07-27 17:39:01
//创建人  ：Yangming
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('GrpContNo').value = "";
    fm.all('InsuredID').value = "";
    fm.all('State').value = "";
//   fm.all('ContNo').value = "";
//    fm.all('BatchNo').value = "";
    fm.all('InusredNo').value = "";
    fm.all('Retire').value = "";
    fm.all('EmployeeName').value = "";
    fm.all('InsuredName').value = "";
    fm.all('Relation').value = "";
    fm.all('Sex').value = "";
    fm.all('Birthday').value = "";
    fm.all('IDType').value = "";
    fm.all('IDNo').value = "";
    fm.all('ContPlanCode').value = "";
    fm.all('OccupationType').value = "";
    fm.all('BankCode').value = "";
    fm.all('BankAccNo').value = "";
    fm.all('AccName').value = "";
/*    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
    */
  }
  catch(ex) {
    alert("在LCInuredListQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLCInuredListGrid();  
  }
  catch(re) {
    alert("LCInuredListQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LCInuredListGrid;
function initLCInuredListGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="集体合同号码";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=3;         		//列名
    
    
    iArray[2]=new Array();
    iArray[2][0]="被保人序号";         		//列名
    iArray[2][1]="60px";         		//列名
    iArray[2][3]=0;         		//列名
    
    
    iArray[3]=new Array();
    iArray[3][0]="状态";         		//列名
    iArray[3][1]="100px";         		//列名
    iArray[3][3]=3;         		//列名
    
    
    iArray[4]=new Array();
    iArray[4][0]="合同号码 ";         		//列名
    iArray[4][1]="100px";         		//列名
    iArray[4][3]=3;         		//列名
    
    
    iArray[5]=new Array();
    iArray[5][0]="批次号";         		//列名
    iArray[5][1]="100px";         		//列名
    iArray[5][3]=3;         		//列名
    
    
    iArray[6]=new Array();
    iArray[6][0]="被保人客户号";         		//列名
    iArray[6][1]="80px";         		//列名
    iArray[6][3]=0;         		//列名
    
    
    iArray[7]=new Array();
    iArray[7][0]="在职/退休";         		//列名
    iArray[7][1]="55px";         		//列名
    iArray[7][3]=0;         		//列名
    
    iArray[8]=new Array();
    iArray[8][0]="员工姓名";         		//列名
    iArray[8][1]="100px";         		//列名
    iArray[8][3]=0;         		//列名
    
    iArray[9]=new Array();
    iArray[9][0]="被保人姓名";         		//列名
    iArray[9][1]="100px";         		//列名
    iArray[9][3]=0;         		//列名
    
    iArray[10]=new Array();
    iArray[10][0]="与员工关系";         		//列名
    iArray[10][1]="60px";         		//列名
    iArray[10][3]=0;         		//列名
    
    iArray[11]=new Array();
    iArray[11][0]="性别";         		//列名
    iArray[11][1]="25px";         		//列名
    iArray[11][3]=0;         		//列名
    
    iArray[12]=new Array();
    iArray[12][0]="出生日期";         		//列名
    iArray[12][1]="100px";         		//列名
    iArray[12][3]=0;         		//列名
    
    iArray[13]=new Array();
    iArray[13][0]="证件类型";         		//列名
    iArray[13][1]="50px";         		//列名
    iArray[13][3]=0;         		//列名
    
    iArray[14]=new Array();
    iArray[14][0]="证件号码";         		//列名
    iArray[14][1]="130px";         		//列名
    iArray[14][3]=0;         		//列名
    
    iArray[15]=new Array();
    iArray[15][0]="保险计划";         		//列名
    iArray[15][1]="50px";         		//列名
    iArray[15][3]=0;         		//列名
    
    iArray[16]=new Array();
    iArray[16][0]="职业类别";         		//列名
    iArray[16][1]="100px";         		//列名
    iArray[16][3]=0;         		//列名
    
    iArray[17]=new Array();
    iArray[17][0]="理赔金转帐银行";         		//列名
    iArray[17][1]="100px";         		//列名
    iArray[17][3]=0;         		//列名
    
    iArray[18]=new Array();
    iArray[18][0]="帐号";         		//列名
    iArray[18][1]="130px";         		//列名
    iArray[18][3]=0;         		//列名
    
    iArray[19]=new Array();
    iArray[19][0]="户名";         		//列名
    iArray[19][1]="100px";         		//列名
    iArray[19][3]=0;         		//列名
    
    iArray[20]=new Array();
    iArray[20][0]="操作员";         		//列名
    iArray[20][1]="100px";         		//列名
    iArray[20][3]=3;         		//列名
    
    iArray[21]=new Array();
    iArray[21][0]="入机日期";         		//列名
    iArray[21][1]="100px";         		//列名
    iArray[21][3]=3;         		//列名
    
    iArray[22]=new Array();
    iArray[22][0]="入机时间";         		//列名
    iArray[22][1]="100px";         		//列名
    iArray[22][3]=3;         		//列名

		iArray[23]=new Array();
    iArray[23][0]="最后一次修改日期";         		//列名
    iArray[23][1]="100px";         		//列名
    iArray[23][3]=3;         		//列名

		iArray[24]=new Array();
    iArray[24][0]="最后一次修改时间";         		//列名
    iArray[24][1]="100px";         		//列名
    iArray[24][3]=3;         		//列名
    
    iArray[25]=new Array();
    iArray[25][0]="公共账户类型";         		//列名
    iArray[25][1]="100px";         		//列名
    iArray[25][3]=3;         		//列名
    
    iArray[26]=new Array();
    iArray[26][0]="公共账户";         		//列名
    iArray[26][1]="100px";         		//列名
    iArray[26][3]=3;         		//列名
    
    iArray[27]=new Array();
    iArray[27][0]="保全号";         		//列名
    iArray[27][1]="100px";         		//列名
    iArray[27][3]=3;         		//列名
    
    iArray[28]=new Array();
    iArray[28][0]="险种号码";         		//列名
    iArray[28][1]="100px";         		//列名
    iArray[28][3]=3;         		//列名
    
    iArray[29]=new Array();
    iArray[29][0]="保费计算方式";         		//列名
    iArray[29][1]="100px";         		//列名
    iArray[29][3]=3;         		//列名
    
    //by gzh 20101216
    iArray[30]=new Array();
    iArray[30][0]="入司时间";         		//列名
    iArray[30][1]="130px";         		//列名
    iArray[30][3]=3;         		//列名
    
    iArray[31]=new Array();
    iArray[31][0]="职级";         		//列名
    iArray[31][1]="100px";         		//列名
    iArray[31][3]=3;         		//列名
    
    iArray[32]=new Array();
    iArray[32][0]="归属规则";         		//列名
    iArray[32][1]="100px";         		//列名
    iArray[32][3]=3;         		//列名
    
    iArray[33]=new Array();
    iArray[33][0]="领取年龄年期";         		//列名
    iArray[33][1]="100px";         		//列名
    iArray[33][3]=3;         		//列名
    
    iArray[34]=new Array();
    iArray[34][0]="领取年龄年期标志";         		//列名
    iArray[34][1]="100px";         		//列名
    iArray[34][3]=3;         		//列名

	iArray[35]=new Array();
    iArray[35][0]="给付责任类型";         		//列名
    iArray[35][1]="100px";         		//列名
    iArray[35][3]=3;         		//列名

	iArray[36]=new Array();
    iArray[36][0]="投保人缴费";         		//列名
    iArray[36][1]="100px";         		//列名
    iArray[36][3]=3;         		//列名
    
    iArray[37]=new Array();
    iArray[37][0]="个人协议缴费";         		//列名
    iArray[37][1]="100px";         		//列名
    iArray[37][3]=3;         		//列名
    
    iArray[38]=new Array();
    iArray[38][0]="个人自愿缴费";         		//列名
    iArray[38][1]="100px";         		//列名
    iArray[38][3]=3;         		//列名
    
    iArray[39]=new Array();
    iArray[39][0]="个人直接缴费转账银行";         		//列名
    iArray[39][1]="100px";         		//列名
    iArray[39][3]=3;         		//列名
    
    iArray[40]=new Array();
    iArray[40][0]="个人直接缴费账号";         		//列名
    iArray[40][1]="100px";         		//列名
    iArray[40][3]=3;         		//列名
    
    iArray[41]=new Array();
    iArray[41][0]="个人直接缴费户名";         		//列名
    iArray[41][1]="100px";         		//列名
    iArray[41][3]=3; 
    
    LCInuredListGrid = new MulLineEnter( "fm" , "LCInuredListGrid" ); 
    //这些属性必须在loadMulLine前

    LCInuredListGrid.mulLineCount = 0;   
    LCInuredListGrid.displayTitle = 1;
    LCInuredListGrid.hiddenPlus = 1;
    LCInuredListGrid.hiddenSubtraction = 1;
    LCInuredListGrid.canSel = 1;
    LCInuredListGrid.canChk = 0;
    //LCInuredListGrid.selBoxEventFuncName = "showOne";

    LCInuredListGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LCInuredListGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
