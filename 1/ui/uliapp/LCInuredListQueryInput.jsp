<%
//程序名称：LCInuredListInput.jsp
//程序功能：功能描述
//创建日期：2005-07-27 17:39:01
//创建人  ：Yangming
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LCInuredListQueryInput.js"></SCRIPT> 
  <%@include file="LCInuredListQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
  var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLCInuredListGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title style="display:'none'">
      集体合同号码
    </TD>
    <TD  class= input style="display:'none'">
      <Input class= 'common' name=GrpContNo verify="集体合同号码|int&len=10">
    </TD>
    <TD  class= title>
      被保人序号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InsuredID verify="被保人序号|int">
    </TD>
    <TD  class= title style="display:'none'">
      状态
    </TD>
    <TD  class= input style="display:'none'">
      <Input class= 'common' name=State verify="状态|int">
    </TD>
    <!--TD  class= title>
      合同号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContNo >
    </TD-->
    <!--TD  class= title>
      批次号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BatchNo >
    </TD-->
    <TD  class= title>
      被保人客户号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InusredNo verify="被保人客户号|int&len=9">
    </TD>

    <TD  class= title>
      在职/退休
    </TD>
    <TD  class= input>
      <Input class= 'code' name=Retire verify="在职/退休|int" CodeData= "0|^1|在职^2|退休" ondblClick= "showCodeListEx('',[this],[0],null,null,null,1);" onkeyup= "showCodeListKeyEx('',[this],[0],null,null,null,1);">
    </TD>

    <TD  class= title>
      员工姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EmployeeName verify="员工姓名|len<=20">
    </TD>
              </TR>
  <TR  class= common>
    <TD  class= title>
      被保人姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InsuredName verify="被保人姓名|len<=20">
    </TD>
    <TD  class= title style="display:'none'">
      与员工关系
    </TD>
    <TD  class= input style="display:'none'">
    	<Input class= 'common' name=Relation verify="与员工关系|int" >
    </TD>

 <TD  class= title>
         性别
    </TD>
   <TD  class= input>
  <Input class=codeNo name=Sex  verify="投保人性别|code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true elementtype=nacessary>    
  </TD>

    <TD  class= title>
      出生日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=Birthday verify="出生日期|date">
    </TD>

    <TD  class= title>
      证件类型
    </TD>
    <TD  class= input>
      <Input class=codeNo name="IDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1]);"><input class=codename name=AppntIDTypeName readonly=true elementtype=nacessary>    
    </TD>
                </TR>
  <TR  class= common>
   <TD  class= title>
     证件号码
   </TD>
   <TD  class= input colspan=1>
     <Input class= common3 name="IDNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" >
   </TD> 
    </TD>

    <TD  class= title>
      保险计划
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContPlanCode >
    </TD>

    	<TD class=title> 职业类别
      </TD>
			 <TD class=input>
			<Input class="code" name=OccupationType  verify="被保险人职业类别|code:OccupationType" MAXLENGTH=0 ondblclick="return showCodeList('OccupationType',[this],[0]);" onkeyup="return showCodeListKey('OccupationType',[this],[0]);">
		</TD>
    <TD  class= title>
      理赔金转帐银行
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BankCode >
    </TD>
                  </TR>
  <TR  class= common>
    <TD  class= title>
      帐号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BankAccNo >
    </TD>

    <TD  class= title>
      户名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccName >
    </TD>
          </TR>
 
    <!--TD  class= title>
      操作员
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD-->
  </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInuredList1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLCInuredList1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLCInuredListGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
