<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	String tBigProjectNo = request.getParameter("BigProjectNo");
%>
<script>
	var ManageCom = "<%=tGI.ManageCom%>";
	var tOperator = "<%=tGI.Operator%>";
	var tBigProjectNo = "<%=tBigProjectNo%>";
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<script src="./BigProjectDetail.js"></script>
	<%@include file="./BigProjectDetailInit.jsp"%>
</head>
<body onload="initForm();">
	<form action="" method=post name=fm target="fraSubmit">
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divBPInfo);">
				</td>
				<td class=titleImg>大项目信息</td>
			</tr>
		</table>
		<div id="divBPInfo" style="display:''">
			<table class=common>
				<tr class=common>
					<TD class=title8>大项目名称</TD>
					<TD class=input>
						<Input type="text" class="common" name=BigProjectName>
					</TD>
					<TD class=title8>大项目编码</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=BigProjectNo>
					</TD>
					<td class=title>管理机构</td>
					<td class=input>
						<input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" 
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
						><input class=codename name=ManageComName readonly=true elementtype=nacessary>
					</td>
				</tr>
			</table>
		</div>
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divYearInfo);">
				</td>
				<td class=titleImg>年度信息</td>
			</tr>
		</table>
		<div id="divYearInfo" style="display:''">
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1>
						<span id="spanBPYearInfoGrid"></span>
					</td>
				</tr>
			</table>
		</div>
		<input type=button class=cssButton value="已归属保单" name=Save onclick="attachedPolicies();" style="display:''">
		<input type=button class=cssButton value=" 退  出 " name=Delete onclick="cancle();" style="display:''">
	</form>
	<span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>