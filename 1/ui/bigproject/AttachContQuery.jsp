<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	String tBigProjectNo = request.getParameter("BigProjectNo");
	String tBigProjectYear = request.getParameter("BigProjectYear");
%>
<script>
	var ManageCom = "<%=tGI.ManageCom%>";
	var tOperator = "<%=tGI.Operator%>";
	var tBigProjectNo = "<%=tBigProjectNo%>"; 
	var tBigProjectYear = "<%=tBigProjectYear%>";
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<script src="./AttachContQuery.js"></script>
	<%@include file="./AttachContQueryInit.jsp"%>
</head>
<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divBPinfo);">
				</td>
				<td class=titleImg>大项目信息</td>
			</tr>
		</table>
		<div id="divBPinfo" style="display:''">
			<table class=common>
				<tr class=common>
					<td class=title>大项目编号</td>
					<td class=input>
						<input class=common name=BPNumber>
					</td>
					<td class=title>大项目名称</td>
					<td class=input>
						<input class=common name=BPName>
					</td>
					<td class=title>建项日期</td>
					<td class=input>
						<input class=common name=CPDate>
					</td>
				</tr>
				<tr class=common>
					<td class=title>已归属保单数</td>
					<td class=input>
						<input class=readonly name=AttachedConts readonly=true>
					</td>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
				</tr>
			</table>
		</div>
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divQuery);">
				</td>
				<td class=titleImg>查询条件</td>
			</tr>
		</table>
		<div id="divQuery" style="display:''">
			<table class=common>
				<tr class=common>
					<td class=title>管理机构</td>
					<td class=input>
						<input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" 
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
						><input class=codename name=ManageComName readonly=true elementtype=nacessary>
					</td>
					<td class=title>承保起始日期</td>
					<td class=input>
						<input class=coolDatePicker name=StartDate>
					</td>
					<td class=title>承保终止日期</td>
					<td class=input>
						<input class=coolDatePicker name=EndDate>
					</td>
				</tr>
				<tr class=common>
					<td class=title>保单合同号</td>
					<td class=input>
						<input class=common name=ContNo>
					</td>
					<td class=title>项目年度</td>
					<td class=input>
						<input class=codeNo name=ProjectYear 
						ondblclick="return showCodeList('ProjectYear',[this,ProjectYearName],[0,1],null,null,null,1);"
						onkeyup="return showCodeListKey('ProjectYear',[this,ProjectYearName],[0,1],null,null,null,1);"
						><input class=codeName name=ProjectYearName>
					</td>
					<td class=title></td>
					<td class=input></td>
				</tr>
			</table>
		</div>
		<input type=button class=cssButton value="查询保单" onclick="query();">
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divPolList);">
				</td>
				<td class=titleImg>保单清单</td>
			</tr>
		</table>
		<div id="divPolList" style="display:''">
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1>
						<span id="spanPolListGrid"></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="turnPage1.firstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="turnPage1.previousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="turnPage1.nextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="turnPage1.lastPage();">
			</table>
		</div>
		<input type=button class=cssButton value="保单明细" onclick="PolicyDetail();">
		<input type=button class=cssButton value=" 返  回 " onclick="cancle();">
	</form>
	<span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>