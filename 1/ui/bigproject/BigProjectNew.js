var turnPage = new turnPageClass();
var showInfo;

/* 查询项目 *///暂不需要这个功能，注掉。
//function queryBPInfo(){
//	var BPManageCom = getBranchManagecom();
//	if(fm.ProjectName.value == "" || fm.ProjectName.value == null){
//		window.open("./BPQueryMain.jsp");
//	}else{
//		var tBigProjectName = fm.ProjectName.value;
//		var strSQL = "select bigprojectname,bigprojectno from lcbigprojectinfo where bigprojectname='"+tBigProjectName+"' and bpmanagecom='"+BPManageCom+"'";
//		var arr = easyExecSql(strSQL);
//		if(arr){
//			alert("分公司:"+BPManageCom+"下已有名为"+arr[0][0]+"的大项目");
//			fm.ProjectName.value = arr[0][0];
//			fm.ProjectNo.value = arr[0][1];
//		}else{
//			alert("分公司:"+BPManageCom+"下没有名为"+tBigProjectName+"的大项目");
//		}
//	}
//}

/* 保存 */
function save(){
	var BPManageCom = getBranchManagecom();
	if(fm.ProjectName.value=="" || fm.ProjectName.value==null || fm.ProjectName.value=="null"){
		alert("大项目名称不能为空！");
		return;
	}
	if(fm.ManageCom.value=="" || fm.ManageCom.value==null || fm.ManageCom.value=="null"){
		alert("管理机构不能为空！");
		return;
	}
	fm.OperatorFlag.value = OperatorFlag;
	//OperatorFlag为1时，表示新建大项目且只能保存新建的大项目信息
	if(OperatorFlag=="1"){
		if(!checkProjectEstimateRate()){
			return false;
		}
		var tBigProjectName = fm.ProjectName.value;
		var nSQL = "select bigprojectname,bigprojectno from lcbigprojectinfo where bigprojectname='"+tBigProjectName+"' and bpmanagecom='"+BPManageCom+"'";
		var arr = easyExecSql(nSQL);
		if(arr){
			alert("分公司:"+BPManageCom+"下已有名为"+arr[0][0]+"的大项目，不能作为新项目建立！");
			return;
		}
		fm.ProjectNo.value = getProjectNo();
	}
	//OperatorFlag为2时，表示修改大项目且只能保存要修改的大项目信息
	if(OperatorFlag=="2"){
		if(!checkProjectEstimateRate()){
			return false;
		}
		var tSQL = "select bigprojectname,ProjectEstimateRate from lcbigprojectinfo where bigprojectno='"+fm.ProjectNo.value+"'";
		var arr = easyExecSql(tSQL);
		if(arr){
			if(fm.ProjectName.value == arr[0][0]&&fm.ProjectEstimateRate.value == arr[0][1]){
				alert("录入的项目名称"+fm.ProjectName.value+"与"+fm.ProjectNo.value+"的项目名称及项目预估赔付率相同，不需修改！");
				return false;
			}
		}
		//同一分公司下，大项目名称不能相同
		var cSQL = "select 1 from lcbigprojectinfo where bigprojectname='"+fm.ProjectName.value+"' and bpmanagecom='"+BPManageCom+"' and bigProjectNo !='" +fm.ProjectNo.value+"'";
		var carr = easyExecSql(cSQL);
		if(carr){
			alert("分公司："+BPManageCom+"下已有名为"+fm.ProjectName.value+"的大项目，不能修改！");
			return;
		}
		
		if(arr){
			if(fm.ProjectName.value != arr[0][0]){
				//若该项目下已经归属有保单，不能修改项目名称
				var mSQL = "select 1 from lcbigprojectcont where bigprojectno='"+fm.ProjectNo.value+"'";
				var marr = easyExecSql(mSQL);
				if(marr){
					alert("该大项目已经归属过保单，不能修改名称！");
					return;
				}
			}
		}
	}
	fm.BPManageCom.value = getBranchManagecom();
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./BigProjectSave.jsp";
	fm.submit();
}

function afterSubmit(FlagStr,Content ){
	showInfo.close();
	if(FlagStr == "Fail"){             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

/* 为新建大项目创建大项目编号 */
function getProjectNo(){
	//获得分公司机构
	var loginManageCom = getBranchManagecom();
	//创建大项目编号规则：系统中该分公司下大项目信息中最大的大项目编号+1
	var oldProjectNo;
	var newProjectNo;
	//取出该分公司下的大项目信息中的最大编号
	var arr = easyExecSql("select max(substr(bigprojectno,8)) from lcbigprojectinfo");
	if(arr){
		oldProjectNo = arr[0][0];
		if(oldProjectNo!=null && oldProjectNo!=""){
			var lastSixChars = oldProjectNo;
			var lastEndsNums = Number(lastSixChars) + 1 + "";
			var headChars = "cgi"+loginManageCom;
			var len = Number(13 - lastEndsNums.length - headChars.length);
			newProjectNo = headChars;
			for(var i=0; i<len; i++){
				newProjectNo = newProjectNo + "0";
			}
			newProjectNo = newProjectNo + lastEndsNums;
		}else{
			newProjectNo = "cgi"+loginManageCom+"000001";
		}
	}
	return newProjectNo;
}

/* 依据登录机构来获取分公司机构 */
function getBranchManagecom(){
	var loginManageCom;
	if(ComCode=="86"){
		loginManageCom = "8600";
	}
	if(ComCode.length==4){
		loginManageCom = ComCode;
	}
	if(ComCode.length > 4){
		loginManageCom = ComCode.substring(0,4);
	}
	return loginManageCom;
}

function initProjectInfo(){
	if(OperatorFlag == "2"){
		var tSQL = "select bigprojectname,bpmanagecom,ProjectEstimateRate from lcbigprojectinfo "
				 + "where bigprojectno= '"+tBigProjectNo+"'";
		var arr = easyExecSql(tSQL);
		if(arr){
			fm.ProjectName.value = arr[0][0];
			fm.ManageCom.value = arr[0][1];
			fm.ProjectEstimateRate.value = arr[0][2];
		}
	}
}

function checkProjectEstimateRate(){
	if(fm.ProjectEstimateRate.value==""){
		return true;
	}
	var a =  /^([1-9]\d{0,15}|0)(\.\d{1,4})?$/g;
	var tProjectEstimateRate = fm.ProjectEstimateRate.value
	if(a.test(tProjectEstimateRate)){
	}else{
		alert("项目赔付率值应为数字或小数,且范围为小数点后四位！");
        return false;
	}
	if(tProjectEstimateRate<0){
		alert("项目赔付率录入值必须大于0！");
		return false;
	}
	return true;
}

function afterQuery2(arrQueryResult){
	var arrResult = new Array();
	if(arrQueryResult!=null){
		arrResult = arrQueryResult;
		fm.ProjectName.value = arrResult[0][0];
		fm.ProjectNo.value = arrResult[0][1];
	}
}