var showInfo;
var mDebug="0";
var arrDataSet; 
var turnPage = new turnPageClass();

function queryClick(){
	var bpManageCom = getBranchManagecom();
	var tSQL = "select bigprojectno,bigprojectname,bpmanagecom,createdate from lcbigprojectinfo "
			 + "where bpmanagecom='"+bpManageCom+"'"
			 + getWherePart('BigProjectName','BigProjectName','like')
			 + getWherePart('BigProjectNo','BigProjectNo');
	turnPage.queryModal(tSQL,BPInfoGrid);
	if(BPInfoGrid.mulLineCount == 0){
		alert("没有查询到相应信息！");
		return false;
	}
}

function returnParent(){
	var tSel = -1;
	if(BPInfoGrid.mulLineCount == 0){
		alert("请先查询信息！");
		return false;
	}else if (BPInfoGrid.mulLineCount >= 1){
		tSel = BPInfoGrid.getSelNo();
	}
	var arrReturn = new Array();
	if(tSel == -1 || tSel == 0){
		alert( "请先选择一条记录，再点击返回按钮。" );
	}else{
	    try{	
	    	arrReturn = getQueryResult();
	    	top.opener.afterQuery2(arrReturn);
	    }catch(ex){
	    	alert( "没有发现父窗口的afterQuery接口。" + ex );
	    }
	    top.close();
	}
}

function getQueryResult(){
	var arrSelected = new Array();
	var tRow = BPInfoGrid.getSelNo();
		if(BPInfoGrid.mulLineCount == 1){
			tRow = 1;
		}
		if(tRow == 0 || tRow == null){
			return arrSelected;
		}

		var	ResultSQL = "select bigprojectname,bigprojectno from lcbigprojectinfo "
					  + "where bigprojectno = '"+BPInfoGrid.getRowColData(tRow-1,1)+"'";
		turnPage.strQueryResult  = easyQueryVer3(ResultSQL, 1, 0, 1);  
		
	        //判断是否查询成功
		if (!turnPage.strQueryResult) {
	      alert("没有满足条件的记录！");
	      return false;
	    }
	  //查询成功则拆分字符串，返回二维数组
	  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	  return arrSelected;
}

/* 依据登录机构得到对应的分公司机构 */
function getBranchManagecom(){
	var BPManageCom;
	if(ComCode=="86"){
		BPManageCom = "8600";
	}
	if(ComCode.length==4){
		BPManageCom = ComCode;
	}
	if(ComCode.length > 4){
		BPManageCom = ComCode.substring(0,4);
	}
	return BPManageCom;
}

function cancle(){
	top.close();
}