var turnPage = new turnPageClass();
var showInfo;

/* 查询 */
/* 
 * 在归属保单页，只查询出
 * 只有市场类型是1或9的团单，才会归属进大项目
 */
function query(){
	if(fm.ManageCom.value==null || fm.ManageCom.value=="" || fm.ManageCom.value=="null"){
		alert("管理机构不能为空！");
		return;
	}
	initPolListGrid();
	var tSQL = "select lgc.grpcontno,lgc.managecom,lgc.signdate,codename('markettype',lgc.markettype),lgc.grpname,lgc.prtno "
		 	 + "from lcgrpcont lgc "
		 	 + "where lgc.ManageCom like '"+fm.ManageCom.value+"%' "
//		 	 + "and lgc.bigprojectflag='1' "
		 	 + getWherePart('lgc.SignDate','StartDate','>=')
		 	 + getWherePart('lgc.SignDate','EndDate','<=')
		 	 + getWherePart('lgc.GrpContNo','ContNo')
		 	 + "and lgc.markettype in ('1','9') "
		 	 + "and lgc.appflag='1' "
		 	 + "and not exists (select 1 from lcbigprojectcont where prtno=lgc.prtno)";
//		 	 + "minus "
//		 	 + "select lgc.grpcontno,lgc.managecom,lgc.signdate,codename('markettype',lgc.markettype),lgc.grpname,lgc.prtno "
//		 	 + "from lcgrpcont lgc,lcbigprojectcont lcbpc "
//		 	 + "where lgc.ManageCom like '"+fm.ManageCom.value+"%' "
//		 	 + getWherePart('lgc.SignDate','StartDate','>=')
//		 	 + getWherePart('lgc.SignDate','EndDate','<=')
//		 	 + getWherePart('lgc.GrpContNo','ContNo')
//		 	 + "and lgc.markettype in ('1','9') "
//		 	 + "and lgc.prtno = lcbpc.prtno "
//		 	 + "and lcbpc.bigprojectno='"+fm.BPNumber.value+"'";
	turnPage.queryModal(tSQL,PolListGrid);
}
/* 加入项目 */
function addToProject(){
	var count = 0;
	var tErrors = "";
	for(var i = 0; i < PolListGrid.mulLineCount; i++ )
	{
		if( PolListGrid.getChkNo(i) == true )
		{
			count ++;
			var tGrpContNo = PolListGrid.getRowColData(i,1);
			var tManageCom = PolListGrid.getRowColData(i,2);
			var tPrtNo = PolListGrid.getRowColData(i,6);
			if(tManageCom.substr(0,4) != fm.all('BPManageCom').value){
				tErrors = tErrors + "保单【"+tGrpContNo+"】所属分公司机构机构【"+tManageCom.substr(0,4)+"】与项目分公司机构【"+fm.all('BPManageCom').value+"】不一致，无法归属！\n";
			}
			//若保单已经归属到一个大项目下，则不能再次归属
			var tAttaSQL = "select bigprojectno from lcbigprojectcont where prtno='"+tPrtNo+"'";
			var arrAtta = easyExecSql(tAttaSQL);
			if(arrAtta){
				alert("印刷号为"+tPrtNo+"的单子已经归属到"+arrAtta[0][0]+"的大项目中！");
				return;
			}
		}
	}
	if(count == 0){
		alert("请选择需要处理的保单！");
		return;
	}
	if(tErrors != ""){
		alert(tErrors);
		return false;
	}
	if(fm.ProjectYear.value == null || fm.ProjectYear.value == ""){
		alert("项目年度不能为空！");
		return false;
	}
	
	fm.fmtransact.value = "INSERT||MAIN";
	fm.SeriaNo.value = getSeriaNo();
	fm.action = "ContAttachBigProjectSave.jsp";
	var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function afterSubmit(FlagStr, content, cOperate) {
	initBigProjectInfo();
	initPolListGrid();
	showInfo.close();
	if (FlagStr == "Fail"){
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

function getSeriaNo(){
	var seriaNo="";
	var newSeriaNo="";
	var tSql = "select max(serialno) from lcbigprojectconttrack";
	var arr = easyExecSql(tSql);
	if(arr){
		seriaNo = arr[0][0];
		if(seriaNo!=null && seriaNo!=""){
			var temp = Number(seriaNo) + 1 + "";
			var len = seriaNo.length - temp.length;
			for(var i=0; i<len; i++){
				newSeriaNo = newSeriaNo + "0";
			}
			newSeriaNo = newSeriaNo + temp;
		}else{
			newSeriaNo = "00000001";
		}
	}
	return newSeriaNo;
}

/* 退出 */
function cancle(){
	top.close();
}

function initBigProjectInfo(){
	fm.BPNumber.value = tBigProjectNo;
	var tSQL = "select bigprojectname,createdate,bpmanagecom from lcbigprojectinfo "
			 + "where bigprojectno = '"+tBigProjectNo+"'";
	var arr = easyExecSql(tSQL);
	if(arr){
		fm.BPName.value = arr[0][0];
		fm.CPDate.value = arr[0][1];
		fm.BPManageCom.value = arr[0][2];
	}
	var cSQL = "select nvl((select count(1) from lcbigprojectcont where bigprojectno='"+tBigProjectNo+"'),0) from dual";
	var count = easyExecSql(cSQL);
	fm.AttachedConts.value = count;
}
