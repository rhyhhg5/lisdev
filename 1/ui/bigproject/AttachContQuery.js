var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;

/* 查询保单 */
function query(){
	initPolListGrid();
	if(fm.ManageCom.value==null || fm.ManageCom.value=="" || fm.ManageCom.value=="null"){
		alert("管理机构不能为空！");
		return;
	}
	var tSQL = "select lgc.grpcontno,lgc.managecom,lgc.signdate,codename('markettype',lgc.markettype),lgc.grpname,lcbpc.bigprojectyear,lgc.prtno "
	 	 	 + "from lcgrpcont lgc,lcbigprojectcont lcbpc "
	 	 	 + "where lgc.prtno = lcbpc.prtno "
	 	 	 + getWherePart('lgc.SignDate','StartDate','>=')
	 	 	 + getWherePart('lgc.SignDate','EndDate','<=') 
	 	 	 + getWherePart('lgc.GrpContNo','ContNo') 
	 	 	 + getWherePart('lcbpc.BigProjectYear','ProjectYear') 
	 	 	 + "and lgc.markettype in ('1','9') "
	 	 	 + "and lgc.ManageCom like '"+fm.ManageCom.value+"%'";
	turnPage1.queryModal(tSQL, PolListGrid);
}

/* 保单明细 */
function PolicyDetail(){
	var cGrpContNo = "";
	var cPrtNo = "";
	var cManageCom = "";
	var count = 0;
	for (var i = 0; i < PolListGrid.mulLineCount; i++) {
		if (PolListGrid.getChkNo(i) == true) {
			cGrpContNo = PolListGrid.getRowColData(i, 1);
			cPrtNo = PolListGrid.getRowColData(i, 7);
			cManageCom = PolListGrid.getRowColData(i, 2);
			count++;
		}
	}
	if (count == 0) {
		alert("请选择需要查看明细的保单！");
		return;
	}
	if (count != 1) {
		alert("只能选择一个保单查看保单明细！");
		return;
	}
	window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+cGrpContNo+"&ContType=1");
}

/* 初始化大项目信息*/
function initProjectInfo(){
	if(tBigProjectNo == null || tBigProjectNo == ""){
		alert("获取大项目编码失败！");
		return false;
	}
	var tSQL = "select bigprojectname,bigprojectno,createdate "
		     + "from lcbigprojectinfo "
		     + "where bigprojectno = '"+tBigProjectNo+"' ";
	var arr = easyExecSql(tSQL);
	if(arr){
		fm.BPName.value = arr[0][0];
		fm.BPNumber.value = arr[0][1];
		fm.CPDate.value = arr[0][2];
	}
	var cSQL = "select nvl((select count(1) from lcbigprojectcont where bigprojectno='"+tBigProjectNo+"'),0) from dual";
	var count = easyExecSql(cSQL);
	fm.AttachedConts.value = count;
}

/* 返回 */
function cancle(){
	top.close();
}