var showInfo;
var turnPage = new turnPageClass();

/* 保存 */
function save(){
	var tBigProjectName = fm.BigProjectName.value;
	var tBigProjectYear = fm.BigProjectYear.value;
	if(tBigProjectName==null || tBigProjectName=="" || tBigProjectName=="null"){
		alert("大项目名称不能为空！");
		return false;
	}
	if(tBigProjectYear==null || tBigProjectYear=="" || tBigProjectYear=="null"){
		alert("大项目年度不能为空！");
		return false;
	}
	
	//生成了lcgrpcont的基本信息后才能录入商团大项目
	var lgcSQL = "select markettype,bigprojectflag from lcgrpcont where prtno='"+tPrtNo+"'";
	var lgcArr = easyExecSql(lgcSQL);
	var tMarketType = "";
	var tBPFlag = "";
	if(!lgcArr){
		alert("录入大项目前，请先保存保单信息！");
		return;
	}else{
		tMarketType = lgcArr[0][0];
		tBPFlag = lgcArr[0][1];
	}
	//如果团单的大项目标识不为‘1’，则无需录入大项目
	//如果团单的市场类型不为‘1’和‘9’，则无需录入大项目
	if(tBPFlag != "1"){
		alert("该团单的大项目标识为："+tBPFlag+",无需录入大项目！");
		return;
	}else{
		if(tMarketType!="1" && tMarketType!="9"){
			alert("该团单的市场类型为:"+tMarketType+",无需录入大项目");
			return;
		}
	}
	
	var BPManageCom = getBranchManagecom();
	//为新建大项目生成大项目编码
	if(fm.BigProjectNo.value==null || fm.BigProjectNo.value==""){
		var nameSQL = "select bigprojectno from lcbigprojectinfo where bpmanagecom='"+BPManageCom+"' and bigprojectname='"+tBigProjectName+"'";
		var arr1 = easyExecSql(nameSQL);
		if(!arr1){
			fm.BigProjectNo.value = getProjectNo();
		}else{
			fm.BigProjectNo.value = arr1[0][0];
		}
	}else{
		
		//同一分公司不能建立名称相同的大项目
		var strSQL = "select bigprojectno from lcbigprojectinfo where bigprojectname='"+tBigProjectName+"' and bpmanagecom='"+BPManageCom+"'";
		var arr = easyExecSql(strSQL);
		if(arr){
			if(arr[0][0] != fm.BigProjectNo.value){
				alert("分公司："+BPManageCom+"机构下已有名称为："+tBigProjectName+"的项目，不能建立同样名称的项目！");
				return;
			}
		}
		
		var ttSQL = "select bigprojectname from lcbigprojectinfo where bigprojectno='"+fm.BigProjectNo.value+"' and bpmanagecom='"+BPManageCom+"'";
		var ttarr = easyExecSql(ttSQL);
		var ttbpname = "";
		if(ttarr){
			ttbpname = ttarr[0][0];
		}
		if(ttbpname != fm.BigProjectName.value){
			var ccSQL = "select count(1) from lcbigprojectcont where bigprojectno='"+fm.BigProjectNo.value+"' and prtno<>'"+tPrtNo+"'";
			var ccarr = easyExecSql(ccSQL);
			var conts = 0;
			if(ccarr){
				conts = ccarr[0][0];
			}
			if(conts > 0){
				alert("大项目:"+ttbpname+"下已经归属过别的保单，不能改名为："+fm.BigProjectName.value);
				return;
			}
		}
	}
	
	//若保单已经归属到一个大项目下，则不能再次归属
	var tBigProjectNo = fm.BigProjectNo.value;
	var tAttaSQL = "select bigprojectno from lcbigprojectcont where prtno='"+tPrtNo+"'";
	var arrAtta = easyExecSql(tAttaSQL);
	if(arrAtta){
		alert("印刷号为"+tPrtNo+"的单子已经归属到"+arrAtta[0][0]+"的大项目中，不能再归属到其他大项目！");
		return;
	}
	
	fm.BPManageCom.value = BPManageCom;
	fm.fmtransact.value = "INSERT";
	fm.PrtNo.value = tPrtNo;
	fm.SerialNo.value = getSeriaNo();
	var showStr = "正在提交页面，请稍等...";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+showStr;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
/* 删除 */
function del(){
	var tBigProjectName = fm.BigProjectName.value;
	var tBigProjectNo = fm.BigProjectNo.value;
	var tBigProjectYear = fm.BigProjectYear.value;
	if(tBigProjectName==null || tBigProjectName=="" || tBigProjectName=="null"){
		alert("大项目名称不能为空！");
		return false;
	}
	if(tBigProjectNo==null || tBigProjectNo=="" || tBigProjectNo=="null"){
		alert("大项目编码不能为空！");
		return false;
	}
	if(tBigProjectYear==null || tBigProjectYear=="" || tBigProjectYear=="null"){
		alert("大项目年度不能为空！");
		return false;
	}
	//删除前查询这个保单是否归入大项目，若无，则不予以删除
	//若保单归入的大项目的大项目编号和大项目年度 与 页面录入的大项目编号和大项目年度不符合，不应该删除。
	var tSql = "select bigprojectno,bigprojectyear from lcbigprojectcont where prtno='"+tPrtNo+"'";
	var tResult = easyExecSql(tSql);
	if(!tResult){
		alert("印刷号："+tPrtNo+"的保单未加入任何大项目，无需删除！");
		return false;
	}else{
		if(tResult[0][0] != tBigProjectNo){
			alert("印刷号："+tPrtNo+"的保单已归入"+tResult[0][0]+"大项目，与录入"+tBigProjectNo+"不符，请核实！");
			return false;
		}
		if(tResult[0][1] != tBigProjectYear){
			alert("印刷号："+tPrtNo+"的保单已归入"+tResult[0][1]+"年度，与录入"+tBigProjectYear+"不符，请核实！");
			return false;
		}
	}
	
	fm.BPManageCom.value = getBranchManagecom();
	fm.fmtransact.value = "DELETE";
	fm.PrtNo.value = tPrtNo;
	fm.SerialNo.value = getSeriaNo();
	var showStr = "正在删除数据，请稍等...";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+showStr;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

/* 退出 */
function cancle(){
	top.close();
}

/* 查询项目 */
function QueryBigProject(){
	var bpManageCom = getBranchManagecom();
	if(fm.BigProjectName.value == "" || fm.BigProjectName.value == null){
		window.open("./BPQueryMain.jsp");
	}else{
		var tBigProjectName = fm.BigProjectName.value;
		var strSQL = "select bigprojectname,bigprojectno from lcbigprojectinfo where bigprojectname='"+tBigProjectName+"' and bpmanagecom='"+bpManageCom+"'";
		var arr = easyExecSql(strSQL);
		if(arr){
			alert("分公司:"+bpManageCom+"下有名为"+arr[0][0]+"的大项目");
			fm.BigProjectName.value = arr[0][0];
			fm.BigProjectNo.value = arr[0][1];
		}else{
			alert("分公司:"+bpManageCom+"下没有名为"+tBigProjectName+"的大项目");
		}
	}
}

function afterQuery2(arrQueryResult){
	var arrResult = new Array();
	if(arrQueryResult!=null){
		arrResult = arrQueryResult;
		fm.BigProjectName.value = arrResult[0][0];
		fm.BigProjectNo.value = arrResult[0][1];
	}
}

/* 依据登录机构来获取分公司机构 */
function getBranchManagecom(){
	var loginManageCom;
	if(ComCode=="86"){
		loginManageCom = "8600";
	}
	if(ComCode.length==4){
		loginManageCom = ComCode;
	}
	if(ComCode.length > 4){
		loginManageCom = ComCode.substring(0,4);
	}
	return loginManageCom;
}

/* 为新建大项目创建大项目编号 */
function getProjectNo(){
	//获得分公司机构
	var loginManageCom = getBranchManagecom();
	//创建大项目编号规则：系统中该分公司下大项目信息中最大的大项目编号+1
	var oldProjectNo;
	var newProjectNo;
	//取出该分公司下的大项目信息中的最大编号
	var arr = easyExecSql("select max(substr(bigprojectno,8)) from lcbigprojectinfo");
	if(arr){
		oldProjectNo = arr[0][0];
		if(oldProjectNo!=null && oldProjectNo!=""){
			var lastSixChars = oldProjectNo;
			var lastEndsNums = Number(lastSixChars) + 1 + "";
			var headChars = "cgi"+loginManageCom;
			var len = Number(13 - lastEndsNums.length - headChars.length);
			newProjectNo = headChars;
			for(var i=0; i<len; i++){
				newProjectNo = newProjectNo + "0";
			}
			newProjectNo = newProjectNo + lastEndsNums;
		}else{
			newProjectNo = "cgi"+loginManageCom+"000001";
		}
	}
	return newProjectNo;
}

/* 创建流水号 */
function getSeriaNo(){
	var seriaNo="";
	var newSeriaNo="";
	var tSql = "select max(serialno) from lcbigprojectconttrack";
	var arr = easyExecSql(tSql);
	if(arr){
		seriaNo = arr[0][0];
		if(seriaNo!=null && seriaNo!=""){
			var temp = Number(seriaNo) + 1 + "";
			var len = seriaNo.length - temp.length;
			for(var i=0; i<len; i++){
				newSeriaNo = newSeriaNo + "0";
			}
			newSeriaNo = newSeriaNo + temp;
		}else{
			newSeriaNo = "00000001";
		}
	}
	return newSeriaNo;
}

//人工核保、新单查询、综合查询时（LoadFlag=16），初始化页面
function initBPInfo(){
	var tSQL = "select lbi.bigprojectno,lbi.bigprojectname,lbc.bigprojectyear "
			 + "from lcbigprojectinfo lbi,lcbigprojectcont lbc "
			 + "where lbi.bigprojectno=lbc.bigprojectno "
			 + "and lbc.prtno='"+tPrtNo+"'";
	var arr = easyExecSql(tSQL);
	if(arr){
		fm.BigProjectNo.value = arr[0][0];
		fm.BigProjectName.value = arr[0][1];
		fm.BigProjectYear.value = arr[0][2];
	}
}

/* 返回页面 */
function afterSubmit(FlagStr, Content){
	showInfo.close();
	if (FlagStr == "Fail"){
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + Content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + Content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
//	if(fm.fmtransact.value == "DELETE"){
//		initInpBox();
//	}
}