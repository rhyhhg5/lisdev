<%@page import="com.sinosoft.utility.*"%>
<%
	GlobalInput tGlobalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = tGlobalInput.ManageCom;
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
	try{
		fm.ManageCom.value = <%=strManageCom%>;
		if(fm.ManageCom.value != null){
			var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
			if(arrResult != null){
				fm.ManageComName.value = arrResult[0][0];
			}
		}
	}catch(ex){
		alert("在BigProjectInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initBPInfoGrid(); 
  }
  catch(re)
  {
    alert("BPQueryInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化BPInfoGrid
 ************************************************************
 */
function initBPInfoGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         
    iArray[0][1]="30px";         
    iArray[0][2]=100;         
    iArray[0][3]=0;         

    iArray[1]=new Array();
    iArray[1][0]="大项目编码";         
    iArray[1][1]="100px";         
    iArray[1][2]=100;         
    iArray[1][3]=0;         

    iArray[2]=new Array();
    iArray[2][0]="大项目名称";         
    iArray[2][1]="100px";         
    iArray[2][2]=100;         
    iArray[2][3]=0;         
    
    iArray[3]=new Array();
    iArray[3][0]="分公司机构";         
    iArray[3][1]="100px";         
    iArray[3][2]=100;         
    iArray[3][3]=0;        
    
    iArray[4]=new Array();
    iArray[4][0]="创建日期";         
    iArray[4][1]="100px";         
    iArray[4][2]=100;         
    iArray[4][3]=0;
 
    BPInfoGrid = new MulLineEnter( "fm" , "BPInfoGrid" ); 

    //这些属性必须在loadMulLine前
    BPInfoGrid.mulLineCount = 0;   
    BPInfoGrid.displayTitle = 1;
    BPInfoGrid.canSel=1;
    BPInfoGrid.hiddenPlus = 1;
    BPInfoGrid.hiddenSubtraction = 1;
    BPInfoGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
        alert("初始化BPInfoGrid时出错："+ ex);
  }
}

</script>