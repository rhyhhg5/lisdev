<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String CurrentDate = PubFun.getCurrentDate();
	String CurrentTime = PubFun.getCurrentTime();
	
	//输入参数
	LCBigProjectContSet tLCBigProjectContSet = new LCBigProjectContSet();
	LCBigProjectContTrackSet tLCBigProjectContTrackSet = new LCBigProjectContTrackSet();
	BigProjectContUI mBigProjectContUI = new BigProjectContUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String tSeriaNo = "";
	String FlagStr = "";
	String Content = "";
	
	String mBigProjectNo = request.getParameter("BPNumber");
	String tYear[] = request.getParameterValues("PolListGrid7");
	String tChk[] = request.getParameterValues("InpPolListGridChk");
	String tGrpContNo[] = request.getParameterValues("PolListGrid1");
	String tPrtNo[] = request.getParameterValues("PolListGrid6");

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	tSeriaNo = request.getParameter("SeriaNo");
	System.out.println(transact);
	System.out.println(tSeriaNo);

	if(mBigProjectNo == null || "".equals(mBigProjectNo))
	{
		FlagStr = "Fail";
		Content = "获取项目编码失败！";
	}
	System.out.println("项目编号:" + mBigProjectNo);
	if(!"Fail".equals(FlagStr)){
		int Count = tChk.length;
		int n = 0;
		for (int i = 0; i < Count; i++)
		{
			LCBigProjectContSchema tLCBigProjectContSchema = new LCBigProjectContSchema();
			LCBigProjectContTrackSchema tLCBigProjectContTrackSchema = new LCBigProjectContTrackSchema();
			if( tPrtNo[i] != null && tChk[i].equals( "1" ))
			{
				tLCBigProjectContSchema.setPrtNo(tPrtNo[i]);
				tLCBigProjectContSchema.setBigProjectNo(mBigProjectNo);
				tLCBigProjectContSchema.setBigProjectYear(tYear[i]);
				tLCBigProjectContSchema.setAttachedDate(CurrentDate);
				tLCBigProjectContSchema.setAttachedTime(CurrentTime);
				tLCBigProjectContSchema.setManageCom(tG.ManageCom);
				tLCBigProjectContSchema.setOperator(tG.Operator);
				tLCBigProjectContSchema.setMakeDate(CurrentDate);
				tLCBigProjectContSchema.setMakeTime(CurrentTime);
				tLCBigProjectContSchema.setModifyDate(CurrentDate);
				tLCBigProjectContSchema.setModifyTime(CurrentTime);
				
				tLCBigProjectContSet.add(tLCBigProjectContSchema);
				
				//获得序列号
				String theSerialNo = tSeriaNo;
				int numSeriaNo = Integer.parseInt(theSerialNo);
				
				while(tChk[i].equals("1")){
					numSeriaNo += n;
					tChk[i] = "";
				}
				n++;
				theSerialNo = String.valueOf(numSeriaNo);
				int len = tSeriaNo.length() - theSerialNo.length();
				StringBuffer theStart = new StringBuffer();
				for(int j=0; j<len; j++){
					theStart.append("0");
				}
				String start = theStart.toString();
				start += theSerialNo;
				theSerialNo = start;
				
				tLCBigProjectContTrackSchema.setSerialNo(theSerialNo);
				tLCBigProjectContTrackSchema.setPrtNo(tPrtNo[i]);
				tLCBigProjectContTrackSchema.setBigProjectNo(mBigProjectNo);
				tLCBigProjectContTrackSchema.setBigProjectYear(tYear[i]);
				tLCBigProjectContTrackSchema.setAttachedDate(CurrentDate);
				tLCBigProjectContTrackSchema.setAttachedTime(CurrentTime);
				tLCBigProjectContTrackSchema.setAttachedState("01");
				tLCBigProjectContTrackSchema.setManageCom(tG.ManageCom);
				tLCBigProjectContTrackSchema.setOperator(tG.Operator);
				tLCBigProjectContTrackSchema.setMakeDate(CurrentDate);
				tLCBigProjectContTrackSchema.setMakeTime(CurrentTime);
				tLCBigProjectContTrackSchema.setModifyDate(CurrentDate);
				tLCBigProjectContTrackSchema.setModifyTime(CurrentTime);
				
				tLCBigProjectContTrackSet.add(tLCBigProjectContTrackSchema);
			}
		}
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.addElement(tLCBigProjectContSet);
		tVData.addElement(tLCBigProjectContTrackSet);
		try {
			mBigProjectContUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = mBigProjectContUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mBigProjectNo%>");
</script>
</html>