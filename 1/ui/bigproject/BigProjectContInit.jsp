<%@page import="com.sinosoft.utility.*"%>
<%
	GlobalInput tGlobalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = tGlobalInput.ManageCom;
%>

<script language="JavaScript">
	function initInpBox(){
		try{
			fm.ManageCom.value = <%=strManageCom%>;
			if(fm.ManageCom.value != null){
				var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
				if(arrResult != null){
					fm.ManageComName.value = arrResult[0][0];
				}
			}
		}catch(ex){
			alert("在BPACInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	function initForm(){
		try{
			initInpBox();
			initBigProjectContGrid();
			initElementtype();
		}catch(re){
			alert("在BPACInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	function initBigProjectContGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array();
		    iArray[0][0]="序号";   
		    iArray[0][1]="30px";    
		    iArray[0][2]=30;     
		    iArray[0][3]=0;        
		      
		    iArray[1]=new Array();
		    iArray[1][0]="项目编号";      
		    iArray[1][1]="80px";        
		    iArray[1][2]=200;           
		    iArray[1][3]=0;             
		    
		    iArray[2]=new Array();
		    iArray[2][0]="项目名称";      
		    iArray[2][1]="80px";        
		    iArray[2][2]=200;           
		    iArray[2][3]=0;            
		        
		    iArray[3]=new Array();
		    iArray[3][0]="建项日期";      
		    iArray[3][1]="80px";        
		    iArray[3][2]=200;           
		    iArray[3][3]=0;            
		    
		    iArray[4]=new Array();
		    iArray[4][0]="分公司机构";      
		    iArray[4][1]="120px";        
		    iArray[4][2]=200;           
		    iArray[4][3]=0;             
		      
		    iArray[5]=new Array();
		    iArray[5][0]="项目状态";      
		    iArray[5][1]="80px";        
		    iArray[5][2]=200;           
		    iArray[5][3]=3;            
		 
		    BigProjectContGrid = new MulLineEnter("fm", "BigProjectContGrid"); 
			  
		    BigProjectContGrid.mulLineCount = 0;
		    BigProjectContGrid.displayTitle = 1;
		    BigProjectContGrid.locked = 1;
		    BigProjectContGrid.canSel = 1;	
		    BigProjectContGrid.canChk = 0;
		    BigProjectContGrid.hiddenSubtraction = 1;
		    BigProjectContGrid.hiddenPlus = 1;
		    
		    BigProjectContGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>