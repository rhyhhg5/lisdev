var turnPage = new turnPageClass();
var showInfo;

/* 查询 */
function query(){
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == "null"){
		alert("管理机构必须填写！");
		return false;
	}
	
	var bpManageCom = getBranchManagecom();
	var strSQL = "select bigprojectno,bigprojectname,createdate,bpmanagecom, "
		   	   + "case when (createdate<>modifydate) then '已有' else '新建' end "
		   	   + "from lcbigprojectinfo "
		   	   + "where BPManageCom = '"+bpManageCom+"' "
		   	   + getWherePart('CreateDate','StartDate','>=')
		   	   + getWherePart('CreateDate','EndDate','<=')
		   	   + getWherePart('BigProjectNo','ProjectNo')
		   	   + getWherePart('BigProjectName','ProjectName');
	
	turnPage.queryModal(strSQL,BigProjectContGrid);
	if(!turnPage.strQueryResult){
		alert("没有查询到相关数据！");
		return false;
	}
}
/* 归属保单 */
function attachPolicy(){
	var checkFlag = 0;
	for(var i=0; i<BigProjectContGrid.mulLineCount; i++){
	    if(BigProjectContGrid.getSelNo(i)){
	    	checkFlag = BigProjectContGrid.getSelNo();
	    	break;
	    }
	}
	if(checkFlag == 0){
		alert("请选择需要归属保单的大项目！");
	  	return false;
	}else{
		var tRow = BigProjectContGrid.getSelNo() - 1;
	    if(tRow == null || tRow < 0)
	    {
	        alert("请选择一条记录。");
	        return false;
	    }
	    var tRowDatas = BigProjectContGrid.getRowData(tRow);
	    var tBigProjectNo = tRowDatas[0];
		window.open("./BigProjectContAttachMain.jsp?BigProjectNo="+tBigProjectNo);
	}
}
/* 取消保单归属 */
function removePolicy(){
	var checkFlag = 0;
	for(var i=0; i<BigProjectContGrid.mulLineCount; i++){
	    if(BigProjectContGrid.getSelNo(i)){
	    	checkFlag = BigProjectContGrid.getSelNo();
	    	break;
	    }
	}
	if(checkFlag == 0){
		alert("请选择需要解除保单归属的大项目！");
	  	return false;
	}else{
		var tRow = BigProjectContGrid.getSelNo() - 1;
	    if(tRow == null || tRow < 0)
	    {
	        alert("请选择一条记录。");
	        return false;
	    }
	    var tRowDatas = BigProjectContGrid.getRowData(tRow);
	    var tBigProjectNo = tRowDatas[0];
		window.open("./BigProjectContRemoveMain.jsp?BigProjectNo="+tBigProjectNo);
	}
}

/* 依据选择的管理机构得到对应的分公司机构 */
function getBranchManagecom(){
	var BPManageCom;
	var tManageCom = fm.ManageCom.value;
	if(tManageCom=="86"){
		BPManageCom = "8600";
	}
	if(tManageCom.length==4){
		BPManageCom = tManageCom;
	}
	if(tManageCom.length > 4){
		BPManageCom = tManageCom.substring(0,4);
	}
	return BPManageCom;
}