<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	String tPrtNo = request.getParameter("PrtNo");
%>
<script>
	var ComCode = "<%=tGI.ManageCom%>";
	var tOperator = "<%=tGI.Operator%>";
	var tPrtNo = <%=tPrtNo%>;
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<script src="./BPInput.js"></script>
	<%@include file="./BPInit.jsp"%>
</head>
<body onload="initForm();">
	<form action="BPSave.jsp" method=post name=fm target="fraSubmit">
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divQuery);">
				</td>
				<td class=titleImg>商团大项目信息</td>
			</tr>
		</table>
		<div id="divQuery" style="display:''">
			<table class=common>
				<tr class=common>
					<TD class=title8>大项目名称</TD>
					<TD class=input>
						<Input type="text" class="common" name=BigProjectName elementtype="nacessary"><input type =button id = "querybigprojectbutton" name = "bigqueryprojectbutton" class=cssButton value="查询项目" onclick="QueryBigProject();">
					</TD>
					<TD class=title8>大项目编码</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=BigProjectNo readonly=true>
					</TD>
				</tr>
				<tr class=common>
					<TD class=title8>大项目年度</TD>
					<TD class=input>
						<Input class=codeno name=BigProjectYear 
						ondblclick="return showCodeList('ProjectYear',[this,BigProjectYearName],[0,1],null,null,null,1);"
						onkeyup="return showCodeListKey('ProjectYear',[this,BigProjectYearName],[0,1],null,null,null,1);"
						><Input class=codename name=BigProjectYearName elementtype="nacessary">
					</TD>
				</tr>
			</table>
		</div>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="BPManageCom" name="BPManageCom">
		<input type=hidden id="PrtNo" name="PrtNo">
		<input type=hidden id="SerialNo" name="SerialNo">
		<input type=button class=cssButton value=" 保  存 " name=Save onclick="save();" style="display:''">
		<input type=button class=cssButton value=" 删  除 " name=Delete onclick="del();" style="display:''">
		<input type=button class=cssButton value=" 退  出 " name=Exit onclick="cancle();" style="display:''">
	</form>
	<span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>