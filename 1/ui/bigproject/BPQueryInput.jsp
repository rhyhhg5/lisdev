<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script>
	var ComCode = "<%=tGI.ManageCom%>";
	var tOperator = "<%=tGI.Operator%>";
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<script src="./BPQueryInput.js"></script>
	<%@include file="./BPQueryInputInit.jsp"%>
</head>
<body onload="initForm();">
	<form action="BPSave.jsp" method=post name=fm target="fraSubmit">
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divQuery);">
				</td>
				<td class=titleImg>查询条件</td>
			</tr>
		</table>
		<div id="divQuery" style="display:''">
			<table class=common>
				<tr class=common>
					<TD class=title8>大项目名称</TD>
					<TD class=input>
						<Input type="text" class="common" name=BigProjectName>
					</TD>
					<TD class=title8>大项目编码</TD>
					<TD class=input>
						<Input type="text" class="common" name=BigProjectNo>
					</TD>
				</tr>
				<tr class=common>
					<TD class=title8>管理机构</TD>
					<TD class=input>
						<Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" 
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
						><Input class=codename name=ManageComName readonly=true>
					</TD>
					<TD class=title8></TD>
					<TD class=input></TD>
				</tr>
			</table>
		</div>
		<input type=button class=cssButton value=" 查  询 " name=QueryClick onclick="queryClick();" style="display:''">
		<input type=button class=cssButton value=" 返  回 " name=ReturnResult onclick="returnParent();" style="display:''">
		<table>
     		<tr>
        		<td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBPInfoGrid);"></td>
    			<td class= titleImg> 查询结果 </td>
      		</tr>
    	</table>
    	<Div  id= "divBPInfoGrid" style= "display: ''">
      		<table  class= common>
       			<tr  class= common>
      	  			<td text-align: left colSpan=1>
  	    				<span id="spanBPInfoGrid" ></span> 
  	  				</td>
  				</tr>
      		</table>
      		<INPUT VALUE="首页" class = cssButton  TYPE=button onclick="turnPage.firstPage();"> 
      		<INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage();"> 					
      		<INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage();"> 
      		<INPUT VALUE="尾页" class = cssButton TYPE=button onclick="turnPage.lastPage();"> 					
    	</Div>
    	<br/>
    	<input type=button class=cssButton value=" 退  出 " name=Cancle onclick="cancle();">
	</form>
	<span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>