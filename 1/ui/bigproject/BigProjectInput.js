var turnPage = new turnPageClass();
var showInfo;

/* 查询 */
function query(){
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == "null"){
		alert("管理机构必须填写！");
		return false;
	}
	
	var bpManageCom = getBranchManagecom();
	var strSQL = "select bigprojectno,bigprojectname,createdate,bpmanagecom "
			   + "from lcbigprojectinfo "
			   + "where BPManageCom = '"+bpManageCom+"' "
			   + getWherePart('CreateDate','StartDate','>=')
			   + getWherePart('CreateDate','EndDate','<=')
			   + getWherePart('BigProjectNo','ProjectNo')
			   + getWherePart('BigProjectName','ProjectName');
	
	turnPage.queryModal(strSQL,BIGPROGrid);
	if(!turnPage.strQueryResult){
		alert("没有查询到相关数据！");
		return false;
	}
}

/* 新建项目 */
function addProject(){
	fm.OperatorFlag.value = "1";
	window.open("./BigProjectMain.jsp?OperatorFlag="+fm.OperatorFlag.value);
}
/* 修改项目 */
function modifyProject(){
	var checkFlag = 0;
	for(var i=0; i<BIGPROGrid.mulLineCount; i++){
	    if(BIGPROGrid.getSelNo(i)){
	    	checkFlag = BIGPROGrid.getSelNo();
	    	break;
	    }
	}
	if(checkFlag == 0){
		alert("请选择需要修改的大项目！");
	  	return false;
	}else{
		var	tBigProjectNo = BIGPROGrid.getRowColData(checkFlag - 1, 1);
		fm.OperatorFlag.value = "2";
		window.open("./BigProjectMain.jsp?OperatorFlag="+fm.OperatorFlag.value+"&BigProjectNo="+tBigProjectNo);
	}
}

/* 依据登录机构得到对应的分公司机构 */
function getBranchManagecom(){
	var BPManageCom;
	if(ComCode=="86"){
		BPManageCom = "8600";
	}
	if(ComCode.length==4){
		BPManageCom = ComCode;
	}
	if(ComCode.length > 4){
		BPManageCom = ComCode.substring(0,4);
	}
	return BPManageCom;
}

function afterSubmit(){
	
}