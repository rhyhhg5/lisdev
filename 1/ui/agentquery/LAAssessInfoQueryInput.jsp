<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAssessInfoQueryInput.jsp
//程序功能：
//创建日期：2002-3-19
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAAssessInfoQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAssessInfoQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title></title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssessInfo1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAAssessInfo1" style= "display: ''">
    
  <table  class= common>
    <tr  class= common>      
    <td  class= title>管理机构</td>
      <TD  class= input>
            <Input class="code" name=ManageCom verify="管理机构|code:comcode&NOTNULL" 
             ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" 
             onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
      </TD>
      <td  class= title>考核年月</td>
      <td  class= input><input name=IndexCalNo class=common verify="考核年月|notnull"> 
      </td>
      </tr>
      <tr  class= common>        
    
     <TD class= title>
          员工职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="code"  verify="员工职级|notnull&code:AgentGrade" 
                                               ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
                                               onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" > 
        </TD>
        <TD  class=title> 考核类型</TD>
      <TD  class=input> <select name="AssessType" >
          <option value="01">降级</option>
          <option value="02">维持</option>
          <option value="03">晋升</option> 
          <option value=""></option>          
        </select> 
       </TD>  
      </TR>  
      <TR class=common>
        <TD class = title>
          是否为基本法考核
        </TD>
        <TD class=input>
          <input name=StandAssessFlag class='code' verify="是否为基本法考核|notnull&code:yesno"
                                               ondblclick="return showCodeList('yesno',[this]);"
                                               onkeyup="return showCodeListKey('yesno',[this]);">
        </TD>
      </TR>
  </table>
          <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();">
          <INPUT VALUE="明细打印" class="cssbutton" TYPE=button onclick="DetailPrint();">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssessInfoGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLAAssessInfoGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLAAssessInfoGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage.lastPage();"> 				
  	</div>	  
  	
	      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	      <input type=hidden name=HiddenStrMoreResult value="">
	      <input type=hidden name=BranchType value="">
  </form>
</body>
</html>
