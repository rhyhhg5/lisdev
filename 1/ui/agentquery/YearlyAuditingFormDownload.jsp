<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：YearlyAuditingFormDownload.jsp
//程序功能：
//创建日期：2015-01-27
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "年度决算报表_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String querySql = request.getParameter("querySql");
    String lenh = request.getParameter("lenh");
    System.out.println("**************************");
    System.out.println(lenh);
    System.out.println(querySql);
    querySql = querySql.replaceAll("%25","%");
    if(lenh.equals("4")){
	    //设置表头
	    String[][] tTitle = {{"管理机构","机构名称","方案类别(A/B)","1-12月平均人力合计","1-12月合格人力合计","年度销售人力合格率","截至本月年度净增有效人力","年度计划净增有效人力","年度净增有效人力达成率","年度净增有效人力达成奖","合格人力达成特别奖","2016年获得人力发展费用总额","至上月累计实发总额","2015年欠款","年决算总计（扣除欠款）"}};
	    //表头的显示属性
	    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	    
	    //数据的显示属性
	    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	    //生成文件
	    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
	    createexcellist.createExcelFile();
	    String[] sheetName ={"list"};
	    createexcellist.addSheet(sheetName);
	    int row = createexcellist.setData(tTitle,displayTitle);
	    if(row ==-1) errorFlag = true;
	        createexcellist.setRowColOffset(row,0);//设置偏移
	        System.out.println(querySql);
	    if(createexcellist.setData(querySql,displayData)==-1)
	        errorFlag = true;
	    if(!errorFlag)
	        //写文件到磁盘
	        try{
	            createexcellist.write(tOutXmlPath);
	        }catch(Exception e)
	        {
	            errorFlag = true;
	            System.out.println(e);
	        }
    }
    else{
    	 //设置表头
        String[][] tTitle = {{"管理机构","机构名称","方案类别(A/B)","1-12月平均人力合计","1-12月合格人力合计","年度销售人力合格率","截至本月年度净增有效人力","年度计划净增有效人力","年度净增有效人力达成率","年度净增有效人力达成奖","合格人力达成特别奖","2016年获得人力发展费用总额","截止上月实际发放金额","年终差额"}};
        //表头的显示属性
        int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
        
        //数据的显示属性
        int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
        //生成文件
         CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
	    createexcellist.createExcelFile();
	    String[] sheetName ={"list"};
	    createexcellist.addSheet(sheetName);
	    int row = createexcellist.setData(tTitle,displayTitle);
	    if(row ==-1) errorFlag = true;
	        createexcellist.setRowColOffset(row,0);//设置偏移
	        System.out.println(querySql);
	    if(createexcellist.setData(querySql,displayData)==-1)
	        errorFlag = true;
	    if(!errorFlag)
	        //写文件到磁盘
	        try{
	            createexcellist.write(tOutXmlPath);
	        }catch(Exception e)
	        {
	            errorFlag = true;
	            System.out.println(e);
	        }
    }
    
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>