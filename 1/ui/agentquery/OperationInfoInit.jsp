<%
//程序名称：OperationInfoInit.jsp
//程序功能：
//创建日期：
//创建人  ：ll
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {    
    fm.all('agentcode').value = '';  
    fm.all('channel').value = ''; 
    fm.all('StartDay').value = ''; 
    fm.all('EndDay').value = '';   
  }
  catch(ex)
  {
    alert("在OperationInfoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                      

function initForm()
{
  try
  {
    initInpBox();    
    initAgentAddGrid();
  }
  catch(re)
  {
    alert("OperationInfoInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentAddGrid
 ************************************************************
 */
function initAgentAddGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="40px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="员工编码";         //列名
        iArray[1][1]="100px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="员工姓名";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="保单号";         //列名
        iArray[3][1]="200px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="投保人";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="被保人";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="险种";         //列名
        iArray[6][1]="80px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0; 
        
        iArray[7]=new Array();
        iArray[7][0]="保费";         //列名
        iArray[7][1]="100px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="缴费期";         //列名
        iArray[8][1]="50px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="签单日期";         //列名
        iArray[9][1]="100px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[10]=new Array();
        iArray[10][0]="网点代码";         //列名
        iArray[10][1]="200px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;        
        
        iArray[11]=new Array();
        iArray[11][0]="网点名称";         //列名
        iArray[11][1]="200px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;            

        AgentAddGrid = new MulLineEnter( "fm" , "AgentAddGrid" ); 

        //这些属性必须在loadMulLine前
        AgentAddGrid.mulLineCount = 10;   
        AgentAddGrid.displayTitle = 1;
        AgentAddGrid.canSel=0;
//      AgentAddGrid.canChk=0;
        AgentAddGrid.locked=1;
	      AgentAddGrid.hiddenPlus=1;
	      AgentAddGrid.hiddenSubtraction=1;
        AgentAddGrid.loadMulLine(iArray); 

      }
      catch(ex)
      {
        alert("初始化AgentAddGrid时出错："+ ex);
      }
    }


</script>