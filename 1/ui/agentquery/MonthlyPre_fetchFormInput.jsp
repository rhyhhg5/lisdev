<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<% 
//程序名称：MonthlyPre_fetchFormInput.jsp
//程序功能：
//创建日期：2015-01-21
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<html>
<%
 	String tFlag = "";
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	tFlag = request.getParameter("type");
%>
<script>
	var operator = <%=tGI.Operator%>;
	var manageCom = <%=tGI.ManageCom%>;
	var type = <%=tFlag%>;
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>   
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<title>月度预提报表</title>
  	<%@include file="MonthlyPre_fetchFormInit.jsp" %>
  	<script src="MonthlyPre_fetchFormInput.js"></script>
</head>
<body onload="initElementtype();initForm()">
	<form action="" method=post name=fm target="f1print">
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>查询条件：</td>
			</tr>
		</table>
		<table class=common >
			<tr class=common>
				<td class=title>管理机构</td>
				<td class=input>
					<input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode123',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
				</td>
				<td class=title>考核年月</td>
				<td>
				<Input class=common  name=AssessYearMonth ondblclick="return showCodeList('humandeveimonth',[this],[0],null,null,null,1,180);" onkeyup="return showCodeListKey('humandeveimonth',[this],[0],null,null,null,1,180);" elementtype=nacessary >
				</td>				
			</tr>
		</table>
		<input class=common type=hidden name=querySql1>
		<input class=common type=hidden name=querySql2>
<!--		<input class=cssButton type=button value="月度预提下载" onclick="monthlyFetchLoad();">&nbsp;&nbsp;&nbsp;&nbsp;-->
		<input class=cssButton type=button value="月度结算查询" onclick="querymonthlyAccountLoad();">&nbsp;&nbsp;&nbsp;&nbsp;
		<input class=cssButton type=button value="月度结算下载" onclick="monthlyAccountLoad();">
		<div id="loadTo">
			<table class= common>
       			<tr class= common>
      	  			<td text-align: left colSpan=1>
  						<span id="spanMonFetchGrid" ></span> 
  			  		</td>
  				</tr>
    		</table>
    		<input CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    		<input CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    		<input CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    		<input CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		</div>
		<table>
	 		<tr>
	 	 		<td>
	 	  			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 		 	</td>
		 		<td class= titleImg>统计口径说明：</td>
			</tr>
	 	</table>
	 	<div id="divCare" style="display:''">
	 		<table class=common>
	 			<tr><td><label style="color:red;">月度预提下载：</label>提取的是当前日期所在考核月的考核完成情况。下载此报表时无需选择考核年月，系统会根据当期日期自行判断。</td></tr>
	 			<tr><td><label style="color:red;">月度结算查询及月度结算下载：</label>是对考核指标计算完成的机构进行结算查询及下载，考核年月 为必选条件</td></tr>
	 		</table>
	 	</div>
  		<input class= common type=hidden name=tFlag value="<%=tFlag%>">
        <Input class= common type=hidden name=Operator >
        <Input calss= common type=hidden name=querySql/>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>