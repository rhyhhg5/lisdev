 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


//提交，保存按钮对应操作
function ListExecl()
{
  if (verifyInput() == false)
    return false;
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
 var strSQL = "";
	strSQL ="select a.branchattr aa,getunitecode(a.agentcode) ab,b.name,"
  +"(select agentgrade from latree bb where bb.agentcode=a.agentcode),a.grpcontno ac,a.contno,a.p11,a.riskcode,"
  +"(select riskname from lmrisk where lmrisk.riskcode=a.riskcode),"
  +"a.agentcom,(select name from lacom where agentcom=a.agentcom),"
  +"a.transmoney,"
  +"(select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears,a.paycount,"
  +"a.caldate ,"
//  +"a.commisionsn,"
  +"value((select nvl(c.chargerate,0) from lacharge c where c.commisionsn=a.commisionsn),0),"
  +"value((select nvl(c.charge,0) from lacharge c where c.commisionsn=a.commisionsn),0) "
  +"from LACommision a,LAAgent b where  a.agentcode=b.agentcode   "
  +"and a.ManageCom like '"+fm.all('ManageCom').value+"%'  "
	//+ getWherePart("a.AgentCode","AgentCode")
    + strAgent
	+ getWherePart("b.Name","AgentName")
	+ getWherePart("a.GRPContNo","GrpContNo") 
	+ getWherePart("a.ContNo","ContNo")
	+ getWherePart("a.WageNo","Month")
	+ getWherePart("a.RiskCode","RiskCode")
//	+ getWherePart("a.ManageCom","ManageCom")
//	+ getWherePart("b.branchType","BranchType")
//	+ getWherePart("b.branchType2","BranchType2")
	+ getWherePart("a.branchType","BranchType")
	+ getWherePart("a.branchType2","BranchType2");
  if(fm.all('Type').value=='01')
	{
    strSQL += "  and a.grpcontno<>'00000000000000000000'  ";
	}
	if(fm.all('Type').value=='02')
	{
	  strSQL += " and a.grpcontno='00000000000000000000'   ";
	}
  strSQL=strSQL+ " order by aa,ab,ac";	
  fm.querySql.value = strSQL;
  var oldAction = fm.action;
  fm.action = "ZJWageQueryListSave.jsp";
  fm.submit();
  fm.action = oldAction;
  }


//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;


  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    AgentGrid.clearData("AgentGrid");
    //fm.all('AdjustBranchCode').value = '';
    //BranchChange();
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//取消按钮对应操作
function cancelForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作


}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{

}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}



function easyQueryClick()
{
  initGrpPolGrid();
  if (verifyInput() == false)
  return false;
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  var strSQL = "";
	strSQL ="select a.branchattr aa,getunitecode(a.agentcode) ab,b.name,"
  +" (select agentgrade from latree bb where bb.agentcode=a.agentcode),a.grpcontno ac,a.contno,a.p11,a.riskcode,"
  +" (select riskname from lmrisk where lmrisk.riskcode=a.riskcode),"
  +" a.agentcom,(select name from lacom where agentcom=a.agentcom),"
  +" a.transmoney,"
  +" (select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears,a.paycount,"
  +" a.caldate,a.commisionsn,"
  +" value((select nvl(c.chargerate,0) from lacharge c where c.commisionsn=a.commisionsn),0),"
  +" value((select nvl(c.charge,0) from lacharge c where c.commisionsn=a.commisionsn),0) "
  +" from LACommision a,LAAgent b where  a.agentcode=b.agentcode   "
  +" and a.ManageCom like '"+fm.all('ManageCom').value+"%'  "
	//+ getWherePart("a.AgentCode","AgentCode")
    + strAgent
	+ getWherePart("b.Name","AgentName")
	+ getWherePart("a.GRPContNo","GrpContNo") 
	+ getWherePart("a.ContNo","ContNo")
	+ getWherePart("a.WageNo","Month")
	+ getWherePart("a.RiskCode","RiskCode")
//	+ getWherePart("a.ManageCom","ManageCom")
//	+ getWherePart("b.branchType","BranchType")
//	+ getWherePart("b.branchType2","BranchType2")
	+ getWherePart("a.branchType","BranchType")
	+ getWherePart("a.branchType2","BranchType2");
  if(fm.all('Type').value=='01')
	{
    strSQL += "  and a.grpcontno<>'00000000000000000000'  ";
	}
  if(fm.all('Type').value=='02')
	{
	  strSQL += " and a.grpcontno='00000000000000000000'   ";
	}
  strSQL=strSQL+ " order by aa,ab,ac";	

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
  	alert('不存在符合条件的有效纪录！');
  	return false;
  }
  //查询成功则拆分字符串，返回二维数组

  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);

}

function submitSave()
{

   var mCount = AgentGrid.mulLineCount;

   if ((mCount == null)||(mCount == 0))
   {
   	alert("请查询出要调动的人员！");
   	return false;
   }
   //submitForm();
  //在这里加上校验
   
//   if((ArchieveGrid.getRowColData(i,1) == null)||(ArchieveGrid.getRowColData(i,1) == ""))
//			{
//				alert("第"+(i+1)+"行职级代码不能为空");
//				ArchieveGrid.setFocus(i,1,ArchieveGrid);
//				selFlag = false;
//				break;
//			}
   var isChk=false;
   for (i=0;i<mCount;i++)
   {
   	if( AgentGrid.getSelNo( i ))
   	{
   	   isChk=true;
       /**
       var agentcode=ArchieveGrid.getRowColData(i,1);
       */
   	   break;
   	}
   }
   if (isChk==false)
   {
     alert("请选出要调动的人员！");
     return false;
   }
   else
     submitForm();

}

function clearAll()
{
//   fm.all('BranchCode').value = '';
//   //fm.all('BranchType').value = '';
//   fm.all('BranchName').value = '';
//   fm.all('BranchManager').value = '';
//   fm.all('ManagerName').value = '';
//   fm.all('hideAgentCode').value = '';
//   fm.all('hideBranchLevel').value = '';
//   fm.all('hideUpBranch').value='';
//   fm.all('hideAgentGroup').value='';
   clearGrid();
}
function clearGrid()
{
   fm.all('ManageCom').value = '';
   fm.all('ManageComName').value = '';
   fm.all('WageNo').value = '';
  // fm.all('AgentSeries').value = '';
   fm.all('AgentCode').value = '';
   AgentGrid.clearData("AgentGrid");
   AgentGrid.clearData("ContGrid");
}

 
 
