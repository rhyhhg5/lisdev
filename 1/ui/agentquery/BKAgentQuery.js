//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var arrDataSet; 
var turnPage = new turnPageClass();


//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  // 刷新查询结果
	easyQueryClick1();
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在BKAgentQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function getQueryResult()
{
	var arrSelected = null;	
	arrSelected = new Array();	
	// 书写SQL语句
	var strSQL = "";
  strSQL = "select a.groupagentcode,a.name,a.sex,a.birthday,a.Nationality,a.NativePlace,a.RgtAddress, "
  			+"a.PolityVisage,a.IDNo,a.Degree,a.GraduateSchool,a.Speciality,a.PostTitle,a.HomeAddress, "
  			+"a.ZipCode,a.Phone,a.BP,a.Mobile,a.EMail,a.OldCom,a.OldOccupation,a.HeadShip,a.EmployDate,a.Remark, "
  			+"a.AgentKind,a.Operator,a.agentcode, "
  			+"c.BranchManager,b.AgentGrade,c.BranchAttr,c.AgentGroup,b.upAgent,c.BranchLevel "
	        +" from LATree b,LABranchGroup c,LAAgent a where 1=1 "
	        +" and (c.state<>'1' or c.state is null) and a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup and a.BranchType='3' and a.AgentCode='"+tAgentCode+"'"; 

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);	
	return arrSelected;
}

/*********************************************************************
 *  调用EasyQuery1查询保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function easyQueryClick1()
{
	// 初始化表格
	initAgentGrade();
	
	// 书写SQL语句
	var strSql = "";
	//var tReturn = getManageComLimitlike("a.managecom");
	strSql = "select b.groupagentcode,b.name,a.agentgrade,a.startdate,a.upagent,a.operator2 from latreeb a,laagent b where a.agentcode=b.agentcode and a.agentcode='"+tAgentCode+"'"
	          +" union select b.groupagentcode,b.name,a.agentgrade,a.startdate,a.upagent,a.operator from latree a,laagent b where a.agentcode=b.agentcode and a.agentcode='"+tAgentCode+"' order by startdate";
	         
	turnPage.queryModal(strSql, AgentGrade);
}


/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}


