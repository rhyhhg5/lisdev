<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：LAActiveWrapRateSetInput.jsp
		//程序功能：设置录入界面
		//创建时间：2009-12-23
		//创建人  ：Elsa
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<%
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
		String BranchType = request.getParameter("BranchType");
		String BranchType2 = request.getParameter("BranchType2");
		System.out.println("BranchType:" + BranchType);
		System.out.println("BranchType2:" + BranchType2);
	%>
	<script language="JavaScript">
     var tsql=" 1 and char(length(trim(comcode))) in (#8#,#4#,#2#) ";
     var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LAAssessAgencStandPremCheckInit.jsp"%>
		<%@include file="../agent/SetBranchType.jsp"%>
		<%@include file="../common/jsp/ManageComLimit.jsp"%>
		<SCRIPT src="LAAssessAgencStandPremCheckInput.js"></SCRIPT>
	</head>

	<body onload="initForm();initElementtype();">
		<form action="./LAAssessAgencStandPremCheckSave.jsp" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
							OnClick="showPage(this,divQryModify);">
					</td>
					<td class=titleImg>
						查询条件
					</td>
				</tr>
			</table>
			<div id="divQryModify" style="display: ''">
				<table class=common>
				    <TR  class= common> 
				<TD  class= title>
					管理机构
				</TD>
				<TD  class= input>
					<Input class="codeno" name=ManageCom verify="管理机构|notnull"
					ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
					onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
					><Input class=codename name=ManageComName readOnly elementtype=nacessary  > 
				</TD> 
				<TD class=title>业务员代码</TD>
				<TD class=input><Input name=GroupAgentCode class='codeno'
					ondblclick="return showCodeList('agentcode',[this,AgentName],[0,1],null,msql,'1',1);" 
					onkeyup="return showCodeListKey('agentcode',[this,AgentName],[0,1],null,msql,'1',1);"
					><Input
					class=codename name=AgentName  readOnly></TD>
				</TR>	
				
				    <tr class=common>
				    <TD class=title>月份起期</TD>
				 <TD class=title>
			          <Input class=common name=WageNoStart verify="月份起期|len=6|NOTNULL" elementtype=nacessary><font color="red">YYYYMM</font>
		        </TD>
		            <TD class=title>月份止期</TD>
				 <TD class=title>
			          <Input class=common name=WageNoEnd verify="月份止期|len=6|NOTNULL" elementtype=nacessary><font color="red">YYYYMM</font>
		        </TD>
 				    </tr>
				</table>
			</div>
			<table>
				<tr>
					<td>
						<input type=button value="查  询" class=cssButton
							onclick="easyQueryClick();">
						<input type=button value="下  载" class=cssButton
							onclick="DoNewDownload();">
						<input type=button value="重  置" class=cssButton
							onclick="return DoReset();">
					</td>

				</tr>
			</table>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor: hand;"	OnClick="showPage(this, divSetGrid);">
					</td>
					<td class=titleImg>
						查询结果
					</td>
				</tr>
			</table>

			<div id="divSetGrid" style="display: ''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanSetGrid"></span>
						</td>
					</tr>
				</table>
				<INPUT VALUE=" 首页 " TYPE="button" class=cssButton onclick = "turnPage.firstPage();">
				<INPUT VALUE="上一页" TYPE="button" class=cssButton onclick ="turnPage.previousPage();">
				<INPUT VALUE="下一页" TYPE="button" class=cssButton onclick = "turnPage.nextPage();">
				<INPUT VALUE=" 尾页 " TYPE="button" class=cssButton onclick = "turnPage.lastPage();">
			</div>
			<Input type=hidden id="BranchType" name=BranchType value=''>
			<Input type=hidden id="BranchType2" name=BranchType value=''>
			<input type=hidden id="sql_where" name="sql_where">
			<input type=hidden id="fmAction" name="fmAction">
			<input type=hidden class=Common name=querySql >
			<input type=hidden class=Common name=flag>
		</form>
		<span id="spanCode" style="display: none; position: absolute; slategray"></span>
	</body>
</html>




