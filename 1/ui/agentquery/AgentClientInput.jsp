<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>客户识别查询</title>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>      
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="AgentClientInput.js"></SCRIPT>
  <%@include file="AgentClientInit.jsp"%>
    <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <script>
   var msql="length(trim(comcode))<=8 and char(8)";
  </script>
</head>

<body onload="initForm();initElementtype();">
<form action="AgentClientSave.jsp" method="post" name="fm" target="fraSubmit" id="fm">
<table >

	<tr class=common>
		<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssess);">
		 <td class=titleImg>
      查询条件
		</td>
		</td>
</table>
  <Div  id= "divAssess" style= "display: ''"> 
  <table class=common align="center">
    <tr class=common>
    	<TD class='title'>
           管理机构
        </TD>
        <TD class='input'>
             <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len<=8"
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,msql,1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,msql,1);"
             ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>             
    </tr>
    <tr class=common> 
      <TD  class=title> 查询年月起期</TD>
      <TD  class=input> <Input  name=StartDate class='common' verify="查询年月起期|NOTNULL&len=6" elementtype=nacessary>
      	<font color="red">(yyyymm) 
      </TD>    
      <TD  class=title> 查询年月止期</TD>
       <TD  class=input> <Input  name=EndDate class='common' verify="查询年月止期|NOTNULL&len=6" elementtype=nacessary> 
       	<font color="red">(yyyymm)
      </TD> 
    </tr >
    <tr><td><br/></td></tr>
    <tr>
    <TD class= common>
       <Input  type=button class= cssButton name=queryb value="对公新客户查询" style="width:150px" onclick="queryMark(1);">         
      <TD>   
    </tr>
     <tr class=common>   
      <TD  class=title>对公新客户总数</TD>
      <TD  class=input> <Input  name=AssessMark1 class='common' style="width:250px;" readonly > 
      </TD>
              
    </tr >
    <tr><td><br/></td></tr>
    <tr>
    <TD class= common>
       <Input type=button class= cssButton name=queryb value="对公其他查询" style="width:150px" onclick="queryMark(2);">         
      <TD> 
    </tr>
     <tr class=common>   
      <TD  class=title>对公其他情形</TD>
      <TD  class=input> <Input  name=AssessMark2 class='common' style="width:250px;" readonly > 
      </TD>
                
    </tr >
    <tr><td><br/></td></tr>
    <tr>
    <TD class= common>
       <Input type=button class= cssButton name=queryb value="对私新客户查询" style="width:150px" onclick="queryMark(3);">         
      <TD> 
    </tr>
     <tr class=common>   
      <TD  class=title>对私新客户总数</TD>
      <TD  class=input> <Input  name=AssessMark3 class='common' style="width:250px;" readonly > 
      </TD>
                
    </tr >
    <tr><td><br/></td></tr>
    <tr>
    <TD class= common>
       <Input type=button class= cssButton name=queryb value="对私其他查询" style="width:150px" onclick="queryMark(4);">         
      <TD>
    </tr>
     <tr class=common>   
      <TD  class=title>对私其他情形</TD>
      <TD  class=input> <Input  name=AssessMark4 class='common' style="width:250px;" readonly > 
      </TD>
                 
    </tr >
    <tr><td><br/></td></tr>
    <tr>
    <TD class= common>
       <Input type=button class= cssButton name=queryb value="重要资料变更客户查询" style="width:150px" onclick="queryMark(5);">         
      <TD> 
    </tr>
     <tr class=common>   
      <TD  class=title>重要资料变更客户</TD>
      <TD  class=input> <Input  name=AssessMark5 class='common' style="width:250px;" readonly > 
      </TD>
                
    </tr >
  </table>
  </div>
  <p><font color="red"> 新客户： 单个被保险人保险费金额人民币2万元以上或外币等值2千美元以上；20万元以上或外币等值2万美元以上转帐</p> 
  <p><font color="red"> 其他情形： 1、退保1万元以上或外币1千美元以上；2、给付保险金1万元以上或外币1千美元以上；3、保全身份识别；4、其他情形</p> 	
  

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
