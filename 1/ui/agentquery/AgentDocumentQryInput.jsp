<%@include file="../common/jsp/AgentCheck.jsp"%>
<html> 
<%
//程序名称：AgentDocumentQryInput.jsp
//程序功能：
//创建日期：2003-10-24
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <!--SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT-->
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="AgentDocumentQryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AgentDocumentQryInit.jsp"%>
  <title>代理人信息</title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
      <tr >
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
        </td>
        <td class=titleImg>
            代理人信息
        </td>            
      </tr> 
    </table>
    <Div  id= "divLAAgent1" style= "display: ''">
      
    <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          代理人编码 
        </TD>
        <TD  class= input> 
          <Input class= 'readonly' readonly name=AgentCode >
        </TD>        
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= 'readonly' readonly >
        </TD>
        <TD  class= title>
          性别 
        </TD>
        <TD  class= input>
          <Input name=Sex class='readonly' readonly >
        </TD> 
        <!--TD  class= title> 
          密码 
        </TD> 
        <TD  class= input>
          <Input name=Password class=common > 
        </TD--> 
      </TR> 
      <TR  class= common> 
        <!--TD  class= title> 
          推荐报名编号 
        </TD>
        <TD  class= input> 
          <Input name=EntryNo class= common > 
        </TD-->
        <TD  class= title>
          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class='readonly' readonly > 
        </TD>
        <TD  class= title>
          身份证号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class='readonly' readonly > 
        </TD>
        <TD  class= title>
          民族
        </TD>
        <TD  class= input>
          <Input name=Nationality class='readonly' readonly > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title> 
          籍贯
        </TD>
        <TD  class= input>
          <Input name=NativePlace class='readonly' readonly >
        </TD>   
        <TD  class= title>
          政治面貌 
        </TD>
        <TD  class= input> 
          <Input name=PolityVisage class='readonly' readonly > 
        </TD> 
        <TD  class= title>
          户口所在地
        </TD>
        <TD  class= input> 
          <Input name=RgtAddress class='readonly' readonly > 
        </TD>
      </TR>
      <!--TR  class= common> 
        <TD  class= title>
          来源地 
        </TD>
        <TD  class= input> 
          <Input name=Source class= common > 
        </TD>        
        <TD  class= title> 
          血型
        </TD>
        <TD  class= input> 
          <Input name=BloodType class="code" verify="血型|code:BloodType" id="BloodType" ondblclick="return showCodeList('BloodType',[this]);" onkeyup="return showCodeListKey('BloodType',[this]);"> 
        </TD>        
        <TD  class= title>
          婚姻状况 
        </TD>
        <TD  class= input>
          <Input name=Marriage class="code" verify="婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);" >
        </TD>   
      </TR>
      <tr class=common>           
        <TD  class= title> 
          结婚日期
        </TD>
        <TD  class= input> 
          <Input name=MarriageDate class="coolDatePicker" dateFormat="short" > 
        </TD>     
      </tr-->
      <TR  class= common> 
        <TD  class= title>
          学历 
        </TD>
        <TD  class= input> 
          <Input name=Degree class='readonly' readonly > 
        </TD>
       <TD  class= title> 
          毕业院校
        </TD>
        <TD  class= input> 
          <Input name=GraduateSchool class='readonly' readonly > 
        </TD>
        <TD  class= title>
          专业 
        </TD>
        <TD  class= input> 
          <Input name=Speciality class='readonly' readonly > 
        </TD>
      </TR>
      <TR  class= common>
        <!--TD  class= title> 
          外语水平 
        </TD>
        <TD  class= input> 
          <Input name=ForeignLevel class="code" verify="外语水平|code:EngLevel" id="EngLevel" ondblclick="return showCodeList('EngLevel',[this]);" onkeyup="return showCodeListKey('EngLevel',[this]);"> 
        </TD-->
        <TD  class= title>
          职称 
        </TD>
        <TD  class= input> 
          <Input name=PostTitle class='readonly' readonly > 
        </TD>
        <!--TD  class= title>
          家庭地址编码
        </TD>
        <TD  class= input>
          <Input name=HomeAddressCode class= common >
        </TD-->
        <TD  class= title>
          家庭地址 
        </TD>
        <TD  class= input> 
          <Input name=HomeAddress class='readonly' readonly > 
        </TD>
        <!--TD  class= title> 
          通讯地址 
        </TD>
        <TD  class= input> 
          <Input name=PostalAddress class= common > 
        </TD-->
        <TD  class= title>
          邮政编码 
        </TD>
        <TD  class= input> 
          <Input name=ZipCode class='readonly' readonly > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          电话 
        </TD>
        <TD  class= input> 
          <Input name=Phone class='readonly' readonly > 
        </TD>
        <TD  class= title> 
          传呼
        </TD>
        <TD  class= input>
          <Input name=BP class='readonly' readonly > 
        </TD>
        <TD  class= title>
          手机 
        </TD>
        <TD  class= input> 
          <Input name=Mobile class='readonly' readonly > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          E_mail 
        </TD>
        <TD  class= input> 
          <Input name=EMail class='readonly' readonly > 
        </TD>
        <!--TD  class= title>
          是否吸烟标志 
        </TD>
        <TD  class= input> 
          <Input name=SmokeFlag class='code' verify="是否吸烟标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);"> 
        </TD-->
        <TD  class= title>
          银行编码
        </TD>
        <TD  class= input> 
          <Input name=BankCode class='readonly' readonly > 
        </TD>
        <TD  class= title>
          工资存折 
        </TD>
        <TD  class= input> 
          <Input name=BankAccNo class='readonly' readonly > 
        </TD>
      </TR>
      <TR  class= common>
        <!--TD  class= title>
          从业年限 
        </TD>
        <TD  class= input> 
          <Input name=WorkAge class= common  > 
        </TD-->
        <TD  class= title>
          原工作单位 
        </TD>
        <TD  class= input> 
          <Input name=OldCom class='readonly' readonly > 
        </TD>
        <TD  class= title> 
          原职业 
        </TD>
        <TD  class= input>
        <Input name=OldOccupation class='readonly' readonly > 
        </TD>
        <TD  class= title>
          工作职务 
        </TD>
        <TD  class= input> 
          <Input name=HeadShip class='readonly' readonly > 
        </TD>
      </TR>
      <!--TR  class= common> 
        <TD  class= title>
          推荐代理人 
        </TD>
        <TD  class= input> 
          <Input name=RecommendAgent class= common  > 
        </TD>
        <TD  class= title> 
          工种/行业
        </TD>
        <TD  class= input> 
          <Input name=Business class=common > 
        </TD>
      </TR-->
      <TR  class= common>         
        <!--TD  class= title>
          信用等级 
        </TD>
        <TD  class= input> 
          <Input name=CreditGrade class=common > 
        </TD>
        <TD  class= title>
          销售资格 
        </TD>
        <TD  class= input> 
          <Input name=SaleQuaf class='code' verify="销售资格|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD-->
        <TD  class= title>
          代理人资格证号码 
        </TD>
        <TD  class= input> 
          <Input name=QuafNo class='readonly' readonly > 
        </TD>
        <TD  class= title>
          证书结束日期 
        </TD>
        <TD  class= input> 
          <Input name=QuafEndDate readonly=true class="coolDatePicker" dateFormat="short" > 
        </TD>
        <TD  class= title>
          展业证号码1 
        </TD>
        <TD  class= input> 
          <Input name=DevNo1 class='readonly' readonly > 
        </TD>
      </TR>
      <!--TR  class= common>         
        <TD  class= title> 
          证书开始日期 
        </TD>
        <TD  class= input> 
          <Input name=QuafStartDate class="coolDatePicker" dateFormat="short" > 
        </TD>
      </TR>
      <TR  class= common>        
        <TD  class= title> 
          展业证号码2
        </TD>
        <TD  class= input> 
          <Input name=DevNo2 class= common > 
        </TD>
        <TD  class= title>
          聘用合同号码 
        </TD>
        <TD  class= input> 
          <Input name=RetainContNo class= common > 
        </TD> 
        <TD  class= title>
          代理人类别 
        </TD>
        <TD  class= input> 
          <Input name=AgentKind class='code' verify="代理人类别|code:AgentKind" ondblclick="return showCodeList('AgentKind',[this]);" onkeyup="return showCodeListKey('AgentKind',[this]);"  > 
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          业务拓展级别
        </TD>
        <TD  class= input> 
          <Input name=DevGrade class=common > 
        </TD>
        <TD  class= title>
          内勤标志 
        </TD>
        <TD  class= input> 
          <Input name=InsideFlag class='code' verify="内勤标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
        <TD  class= title>
          是否专职标志 
        </TD>
        <TD  class= input> 
          <Input name=FullTimeFlag class='code' verify="是否专职标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
      </TR-->
      <TR  class= common> 
        <!--TD  class= title>
          是否有待业证标志 
        </TD>
        <TD  class= input> 
          <Input name=NoWorkFlag class='code' verify="是否有待业证标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD-->
        <TD  class= title>
          培训期数 
        </TD>
        <TD  class= input> 
          <Input name=TrainPeriods class='readonly' readonly > 
        </TD>
        <TD  class= title>
          入司时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class='readonly'readonly > 
        </TD>
        <TD  class= title>
          保证金 
        </TD>
        <TD  class= input> 
          <Input name=AssuMoney class='readonly' readonly > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          备注
        </TD>
        <TD  class=input colSpan= 3> 
          <Input name=Remark class='readonly' readonly > 
        </TD>
        <!--TD  class= title>
          转正日期 
        </TD>
        <TD  class= input> 
          <Input name=InDueFormDate class='coolDatePicker' dateFormat='short' > 
        </TD-->
        <!--TD  class= title>
          代理人状态 
        </TD>
        <TD  class= input> 
          <Input name=AgentState class='code' verify="代理人状态|code:agentstate" ondblclick="return showCodeList('agentstate',[this]);" onkeyup="return showCodeListKey('agentstate',[this]);" > 
        </TD-->
        <TD  class= title>
          操作员代码 
        </TD>
        <TD  class= input> 
          <Input name=Operator class= 'readonly' readonly > 
        </TD>
      </TR>
      <!--TR  class= common> 
        <TD  class= title>
          复核员
        </TD>
        <TD  class= input> 
          <Input name=Approver class= common > 
        </TD>
        <TD  class= title>
          复核日期
        </TD>
        <TD  class= input> 
          <Input name=ApproveDate class= common > 
        </TD>
      </TR>
      <TR class= common> 
        <TD  class= title>
          标志位
        </TD>
        <TD  class= input> 
          <Input name=QualiPassFlag class= common MAXLENGTH=1> 
        </TD>
      </TR-->
    </table>
        
    </Div>	
    <!--行政信息-->    
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
            <td class= titleImg>
                行政信息
            </td>
            </td>
    	</tr>
     </table>
     <Div id= "divLAAgent3" style= "display: ''">
       <table class=common>
        <tr class=common>        
        <TD class= title>
          代理人系列
        </TD>
        <TD class= input>
          <Input name=AgentSeries class= 'readonly' readonly > 
        </TD>
        <TD class= title>
          代理人职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class= 'readonly' readonly > 
        </TD>
        <TD class= title>
          管理机构
        </TD>
        <TD class= input>
          <Input name=ManageCom class= 'readonly' readonly > 
        </TD>
        </tr>
        <tr class=common>
        <TD class= title>
          销售机构 
        </TD>
        <TD class= input>
          <Input name=BranchAttr class= 'readonly' readonly > 
        </TD>
        <TD class= title>
          上级代理人
        </TD>
        <TD class= input>
          <Input name=UpAgent class='readonly' readonly > 
        </TD>
        <TD class= title>
          增员代理人 
        </TD>
        <TD class= input>
          <Input name=IntroAgency class='readonly' readonly > 
        </TD>
        </tr>
        <tr class=common>
        <TD class= title>
          育成代理人
        </TD>
        <TD class= input>
          <Input name=RearAgent class='readonly' readonly > 
        </TD>
        <TD class= title>
          增部代理人
        </TD>
        <TD class= input>
          <Input name=RearDepartAgent class='readonly' readonly > 
        </TD>
        <TD class= title>
          育成督导代理人 
        </TD>
        <TD class= input>
          <Input name=RearSuperintAgent class='readonly' readonly > 
        </TD>
        </tr>
       </table>
    </Div>
    <!--担保人信息（列表） -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
            <td class= titleImg>
                担保人信息
            </td>
            </td>
    	</tr>
    </table>
    <Div  id= "divLAAgent2" style= "display: ''">
    <table  class= common>
      <tr  class= common>                    
        <td text-align: left colSpan=1> 
				  <span id="spanWarrantorGrid" >
				  </span> 
        </td>
      </tr>
    </table>
    </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

        