<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAWelfareInfoQueryInput.jsp
//程序功能：
//创建日期：2002-3-14
//创建人  ：方波
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAWelfareInfoQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAWelfareInfoQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>代理人考勤信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./LAWelfareInfoQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAWelfareInfo1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <div style="display: none">
    <TD  class= title>
            展业机构类型
        </TD>
        <TD  class= input>
            <Input class='code' name=BranchType maxlength=2 ondblclick="return showCodeList('BranchType',[this]);" onkeyup="return showCodeListKey('BranchType',[this]);">
    </TD>
    </div>
    <Div  id= "divLAWelfareInfo1" style= "display: ''">
    
  <table  class= common>
	<tr  class= common> 
	  <td  class= title> 代理人编码 </td>
	  <td  class= input> 
		<input class= common name=AgentCode >
	  </td>
	  <td  class= title> 销售机构 </td>
	  <td  class= input> 
		<input class=common name=AgentGroup >
	  </td>
	</tr>
	<tr  class= common> 
	  <td  class= title>代理人职级 </td>
	  <td  class= input> 
		<input class=common name=AgentGrade >
	  </td>
	  <td  class= title> 管理机构</td>
	  <td  class= input> 
		<input class=common name=ManageCom >
	  </td>
	</tr>
  </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divWelfareInfoGrid1);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divWelfareInfoGrid1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanWelfareInfoGrid1" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();"> 				
  	</div>
	
	  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divWelfareInfoGrid2);">
    		</td>
    		<td class= titleImg>
    			 明细结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divWelfareInfoGrid2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanWelfareInfoGrid2" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();"> 				
  	</div>
	      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
