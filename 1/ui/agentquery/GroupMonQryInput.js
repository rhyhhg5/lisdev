//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();


function returnParent()
{
  var arrReturn = new Array();
  var tSel = GroupMonGrid.getSelNo();	
		
  if( tSel == 0 || tSel == null )		
	alert( "请先选择一条记录，再点击返回按钮。" );
  else
  {
	window.open("../agentquery/AgentDayDetailInput.jsp?CommisionSN=" + GroupMonGrid.getRowColData(tSel-1,9));
  }
}


// 查询按钮
function easyQueryClick()
{
  if ((fm.all('WageNo').value == '')||(fm.all('WageNo').value == null))
  {
    alert("请输入查询月份！");  	 
  	return;
  }
  
  // 初始化表格
  initGroupMonGrid();
	
  // 书写SQL语句
  var strSQL = "";
  strSQL = " select a.agentcode,b.name,getbranchattr(b.branchcode),a.PolNo,"
	     + " a.ReceiptNo,a.TransMoney,a.TpayDate,a.TconfDate,a.CommisionSN"
	     + "  from LACommision a,laagent b where 1=1 and a.agentcode = b.agentcode"
	     + " and a.branchattr like '" + trim(fm.all('AgentGroup').value) + "%%'"	 
	     + getWherePart('a.wageno','WageNo');
	         	         	         	              	
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合查询条件的数据！");
    return ;
  }

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  turnPage.arrDataCacheSet = arrDataSet;
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GroupMonGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

