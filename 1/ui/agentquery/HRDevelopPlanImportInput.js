var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询按钮
function easyQueryClick(){
	initHRPlanGrid();
	if(!verifyInput2()){
		return false;
	}
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == "null"){
		alert("管理机构必须填写！");
		return false;
	}
	/*if(fm.TypeChoose.value == null || fm.TypeChoose.value == "" || fm.TypeChoose.value == "null"){
		alert("请选择类别！");
		return false;
	}*/
	var strSQL = "";
	/*if(fm.TypeChoose.value == 'A'){
		//================待完善==================
		strSQL = "select '1','2','3','"+fm.TypeChoose.value+"','5','"+fm.RewardReachName.value+"','7' "
			   + "from dual with ur";
	}else if(fm.TypeChoose.value == 'B'){
		//================待完善==================
		strSQL = "select '一','二','三','"+fm.TypeChoose.value+"','五','"+fm.RewardReachName.value+"','七' "
			   + "from dual with ur";
	}*/
	strSQL = "select (select name from ldcom where comcode=substr(b.code,1,4)||'0000'),(select name from ldcom where comcode=b.code)," +
			 " (select count from LAhumanDesc d where year='2016' and Month='00' and "+//substr(mngcom,1,'8') =substr('"+fm.ManageCom.value+"',1,'8') and " +
			 " mngcom in (select code from ldcode where  code =  b.code  union select code1 from ldcode1 where codetype ='2016includemanagecom' and code in (select code from ldcode where code = b.code))),"
		     +"a.codealias,a.code1,a.comcode,a.codename"
	         +" from  ldcode1 a ,ldcode b where trim(a.code)=trim(b.codename) "
	         +" and a.codetype='2016humandeveiplan' and b.codetype='2016humandeveidesc' "
	         + getWherePart('substr(a.codealias,1,1)','TypeChoose')
	         +" and b.code like '"+fm.ManageCom.value+"%' ";
	 if(fm.RewardReach.value == "1"){
		strSQL =  strSQL +" and a.comcode='一档' "
	}
	 if(fm.RewardReach.value == "2"){
		strSQL =  strSQL +" and a.comcode='二档' "
	}
	if(fm.RewardReach.value == "3"){
		strSQL =  strSQL +" and a.comcode='三档' "
	}
	if(fm.RewardReach.value == "4"){
		strSQL =  strSQL +" and a.comcode='四档' "
	}
	if(fm.RewardReach.value == "5"){
		strSQL =  strSQL +" and a.comcode='五档' "
	}
	if(fm.RewardReach.value == "6"){
		strSQL =  strSQL +" and a.comcode='六档' "
	}

	turnPage.pageLineNum = 50;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    if (!turnPage.strQueryResult) {
        alert("没有查询到相关数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = HRPlanGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}
