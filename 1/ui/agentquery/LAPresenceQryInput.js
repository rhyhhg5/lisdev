//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();



// 查询按钮
function easyQueryClick()
{
	      
  // 初始化表格
  initPresenceGrid();
	
  var strSQL = "";
  strSQL = "select a.times,b.codename,a.times*c.param/c.param1,a.donedate,a.noti "
         + " from LAPresence a,ldcode b,ldparam c where 1=1 "
         + " and b.codetype = 'presenceaclass' and b.code = a.aclass and c.paramtype=a.aclass" 
	     + getWherePart('a.AgentCode','AgentCode')
	     + " order by idx";
	         	         
	         	         	         
  
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合查询条件的数据！");
    return false;
  }

  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);

  turnPage.arrDataCacheSet = arrDataSet;
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PresenceGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果    
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}


