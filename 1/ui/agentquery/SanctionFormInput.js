var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var queryFlag = false;
function download(){
	
	if(!queryFlag)
	{
		alert("请先查询");
		return false;
	}
	var formAction = fm.action;
	fm.action = "SanctionFormDownLoad.jsp";
	fm.submit();
	fm.target = "fraSubmit";
	fm.action = formAction;
}
//query
function query(){
	if(!verifyInput2()){
		return false;
	}
	if(fm.ManageComHierarchy.value == null ||fm.ManageComHierarchy.value == "" ){
		alert("管理机构层级不能为空");
		return false;
	}
	var month =fm.all('month').value;
	var len = fm.all('ManageCom').value.length;
	var lenh = "";
	var sumlenh = "";
	if(len=="2"){
	if(fm.ManageComHierarchy.value=="总公司"){
		lenh = "2";
		sumlenh="2";
	}
	if(fm.ManageComHierarchy.value=="省公司"){
		lenh = "4";
		sumlenh="2";
	}
	if(fm.ManageComHierarchy.value=="三级机构"){
		lenh = "8";
		sumlenh="2";
	}
	}
	 if(len=="4"){
	if(fm.ManageComHierarchy.value=="总公司"){
		alert("省公司机构不能选择总公司进行查询下载！");
		return false; 
	}
	if(fm.ManageComHierarchy.value=="省公司"){
		lenh = "4";
		sumlenh="4";
	}
	if(fm.ManageComHierarchy.value=="三级机构"){
		lenh = "8";
		sumlenh="4";
	}
	}
	if(len=="8"||len=="6"){
	if(fm.ManageComHierarchy.value=="总公司"){
		alert("三级机构不能选择总公司进行查询下载！");
		return false; 
	}
	if(fm.ManageComHierarchy.value=="省公司"){
		alert("三级机构不能选择省公司进行查询下载！");
		return false; 
	}
	if(fm.ManageComHierarchy.value=="三级机构"){
		lenh = "8";
		sumlenh="8";
	}
	}
	var tSQL ="select mngcom,plancode,max(sum(mfyc),(select codename from ldcode1 where codetype='humandeveiplan'  and code =plancode )) as YearFYc ," +
			"(case when sum(AveHu) = 0 then 0  when (cast(sum(qchu) as double )/sum(AveHu)-0.4)>0 then (cast(sum(qchu) as double )/sum(AveHu)-0.4) else 0 end) as speFYCRate," +
			"(select  cast(TMonthHu as double)/PlanHu from lahumandevei c where c.Month="+month+" and c.mngcom=a.mngcom) as fycrate from  lahumandevei a " +
			"where Month<="+month+" and mngcom like '"+fm.all('ManageCom').value+"%' and exists (select 1 from lahumandevei b where  b.Month="+month+" and b.mngcom=a.mngcom and b.TMonthHu>=b.PlanHu) group by mngcom,plancode  " +
			" union "+
			"select mngcom,plancode,-1*max(sum(mfyc),(select codename from ldcode1 where codetype='humandeveiplan'  and code =plancode )) as YearFYc ," +
			"0 as speFYCRate," +
			"(select  cast(TMonthHu as double)/PlanHu from lahumandevei c where c.Month="+month+" and c.mngcom=a.mngcom) as fycrate from  lahumandevei a " +
			"where Month<="+month+" and  mngcom like '"+fm.all('ManageCom').value+"%' and exists (select 1 from lahumandevei b where  b.Month="+month+" and b.mngcom=a.mngcom and b.TMonthHu<b.PlanHu)  group by mngcom,plancode ";
	var tShowSQL ="select managecom,(select name from ldcom where comcode =managecom),plancode,cast(round(sum(yearFyc),2) as decimal(12,2)),cast(round(sum(speFYC),2) as decimal(12,2)),cast(round(sum(sumFyc),2) as decimal(12,2)),"+
			"case when "+lenh+"=8 then 0 else cast(round(sum(provinceOrgan),2) as decimal(12,2)) end,"+
			"case when "+lenh+"=8 then 0 else cast(round(sum(provinceWages),2) as decimal(12,2)) end,"+
			"cast(round(sum(cityWages),2) as decimal(12,2)),"+
			"case when "+lenh+"=8 then cast(round(sum(cityManager),2) as decimal(12,2)) else cast(round(sum(provinceManager),2) as decimal(12,2)) end,"+
			"case when "+lenh+"=8 then cast(round(sum(cityAssistant),2) as decimal(12,2)) else cast(round(sum(provinceAssistant),2) as decimal(12,2)) end,"+
			"case when "+lenh+"=8 then cast(round(sum(cityPrincipal),2) as decimal(12,2)) else cast(round(sum(rpovincePrincipal),2) as decimal(12,2)) end,"+
			"cast(round(sum(allFYC),2) as decimal(12,2))"+
			" from (select substr(aa.mngcom,1,"+lenh+") as managecom ," +
			" (case when "+lenh+"=8 then substr(aa.plancode,1,1) else '' end) as plancode ," +
			"YearFYc,speFYCRate*YearFYc as speFYC,(YearFYc+speFYCRate*YearFYc) as sumFYC ," +
			"(YearFYc+speFYCRate*YearFYc)*0.02 as provinceOrgan,(YearFYc+speFYCRate*YearFYc)*0.08*0.4 as provinceWages,(YearFYc+speFYCRate*YearFYc)*0.08*0.6 as cityWages," +
			"(YearFYc+speFYCRate*YearFYc)*0.08*0.4*0.3 as provinceManager ,(YearFYc+speFYCRate*YearFYc)*0.08*0.4*0.3 as provinceAssistant ,(YearFYc+speFYCRate*YearFYc)*0.08*0.4*0.4 as rpovincePrincipal," +
			"(YearFYc+speFYCRate*YearFYc)*0.08*0.6*0.3 as cityManager,(YearFYc+speFYCRate*YearFYc)*0.08*0.6*0.3 as cityAssistant ,(YearFYc+speFYCRate*YearFYc)*0.08*0.6*0.4 as cityPrincipal,"+
			"case when "+lenh+"=8 then cast(round(((YearFYc+speFYCRate*YearFYc)*1.08),2) as decimal(12,2)) else cast(round(((YearFYc+speFYCRate*YearFYc)*1.1),2) as decimal(12,2)) end as allFYC "+
			" from ("+tSQL+")   aa) bb group by managecom,plancode order by managecom with ur";
	
//	alert(tShowSQL);
	var smArr = easyExecSql(tShowSQL);
	if(smArr!=null)
	{
		turnPage.queryModal(tShowSQL,SanctionGrid);
		fm.querySql.value = tShowSQL;
		queryFlag = true;
	}
	else
	{
		initSanctionGrid();
		alert("没有满足条件的数据");
	}


}
