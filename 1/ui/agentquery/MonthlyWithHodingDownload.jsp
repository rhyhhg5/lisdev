<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentOperationReport.jsp
//程序功能：F1报表生成
//创建日期：2006-02-20
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.agentquery.*"%>

<%
  String FlagStr = "";
  String Content = "";
  System.out.println("ssssss");
  
  //得到excel文件的保存路径
    String sql = "select SysvarValue "
                + " from ldsysvar "
                + " where sysvar='SaleXmlPath'";
    ExeSQL tExeSQL = new ExeSQL();
    String subPath = tExeSQL.getOneValue(sql);
    
  Calendar cal = new GregorianCalendar();
  String year = String.valueOf(cal.get(Calendar.YEAR));
  String month=String.valueOf(cal.get(Calendar.MONTH)+1);
  String date=String.valueOf(cal.get(Calendar.DATE));
  String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String now = year + month + date + hour + min + sec + "_" ;
  
  String  millis = String.valueOf(System.currentTimeMillis());  
  String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
  subPath+=fileName;
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
  String realPath=application.getRealPath("/")+"/"+subPath;
  System.out.println("..........download jsp getrealpath here :"+application.getRealPath("vtsfile"));
  //String tType = "3";
  System.out.println("OutXmlPath:" + tOutXmlPath);
   GlobalInput tG = new GlobalInput(); 
   tG=(GlobalInput)session.getValue("GI");
   String  tManageCom = request.getParameter("ManageCom");
   String tType = request.getParameter("type");
   String tsubLength = request.getParameter("subLength");
   String tManageComHierarchy = request.getParameter("ManageComHierarchy");
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",realPath);
  tTransferData.setNameAndValue("ManageCom",tManageCom);
  tTransferData.setNameAndValue("type",tType);
  tTransferData.setNameAndValue("subLength",tsubLength);
  tTransferData.setNameAndValue("ManageComHierarchy",tManageComHierarchy);
   VData tVData = new VData();
  	 //调用打印的类
  	 VData vData = new VData();
  	  vData.add(tG);
  	  vData.add(tTransferData);
  	  try{
  		LAMonthlyDownloadBL tLAMonthlyDownloadBL = new LAMonthlyDownloadBL();
      if (!tLAMonthlyDownloadBL.submitData(vData, ""))
      {
          Content = "报表下载失败，原因是:" + tLAMonthlyDownloadBL.mErrors.getFirstError();
          FlagStr = "Fail";
      }
	 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
	 	  //File file = new File(tOutXmlPath);
	 	  File file = new File(realPath);
	 	  System.out.println("..................report jsp here ");
	      response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[4096];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {      
              if (input != null) input.close();
              if (output != null) output.close();
              //file.delete();
          }
  }catch(Exception ex){
  	ex.printStackTrace();
  }
  if(FlagStr.equals("Fail")){

%>
<html>
<script language="javascript">
	alert(<%=Content%>);
	//top.close();
</script>
</html>
<%}%>