<%@include file="../common/jsp/AgentCheck.jsp"%>
 <html> 
<%
//程序名称：menuGrp.jsp
//程序功能：菜单组的输入
//创建日期：2002-10-10 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>

  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
</head>
<body >

<form action="" method=post name=fm target="fraSubmit">
   <Table class = common>
      <TR class= common >
          <TD  class= titleImg >
            险种费率列表
          </TD>  
      </TR>
      
     <TR  class= common> 
      <td class = common>
      <li><a href="./RiskRate/S2-3.htm" target="_blank">附加豁免保险费定期寿险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/2-3.htm" target="_blank">  附加每日住院给付保险费率表>></a></li>
	  </td>		
     </tr>
            
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/S1-3.htm" target="_blank">民生消费信贷定期寿险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/S1-7.htm" target="_blank"> 民生消费信贷定期寿险现金价值表>></a></li>
	  </td>					
     </TR>
      
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/1-7.htm" target="_blank"> 民生安康定期保险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/1-3.htm" target="_blank"> 民生安康定期保险费现金价值表>></a></li>
      </td>					
     </TR>
      
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/3-3.htm" target="_blank"> 民生康顺个人意外伤害保险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/5-3.htm" target="_blank"> 附加住院医疗保险费率表>></a></li>
      </td>					
     </TR>
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/6-3.htm" target="_blank"> 高倍给付特约保险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/9-3.htm" target="_blank"> 附加团体住院医疗保险费率表>></a></li>
	  </td>
	  			
     </TR>
	  
	 <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/8-8.htm" target="_blank"> 民生康吉重大疾病保险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/10-3.htm" target="_blank"> 附加团体每日住院给付保险费率表>></a></li>
      </td>					
     </TR>
      
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/11-3.htm" target="_blank">  附加团体意外伤害医疗保险特约费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/12-3.htm" target="_blank">  团体意外伤害保险费率表>></a></li>
      </td>					
     </TR> 
     
      <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/15-3.htm" target="_blank">  民生康泰重大疾病两全保险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/15-8.htm" target="_blank">  民生康泰重大疾病两全保险现金价值表>></a></li>
      </td>					
     </TR> 
      
      <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/16-3.htm" target="_blank">  民生康顺个人意外伤害保险(老年计划）费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/17-3.htm" target="_blank">  学生团体意外伤害保险费率表>></a></li>
      </td>					
     </TR> 
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/18-3.htm" target="_blank">  旅游意外伤害保险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/17-3.htm" target="_blank">  学生团体意外伤害保险费率表>></a></li>
      </td>					
     </TR> 
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/19-3.htm" target="_blank">  民生团体年金保险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/19-7.htm" target="_blank">  民生团体年金保险现金价值换算倍数表>></a></li>
      </td>					
     </TR> 
     
      <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/20-3.htm" target="_blank">  附加特种疾病每日住院给付保险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/21-3.htm" target="_blank"> 民生关爱特种疾病定期寿险费率表>></a></li>
      </td>					
     </TR>
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/23-3.htm" target="_blank">  民生长裕两全保险费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/23-9.htm" target="_blank"> 民生长裕两全保险现金价值表>></a></li>
      </td>					
     </TR>
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/24-3.htm" target="_blank">  民生长宁终身保险（分红型）费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/24-9.htm" target="_blank"> 民生长宁终身保险（分红型）现金价值表>></a></li>
      </td>					
     </TR>
       
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/25-3.htm" target="_blank">  民生长顺两全保险（分红型）费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/25-9.htm" target="_blank"> 民生长顺两全保险（分红型）现金价值表>></a></li>
      </td>					
     </TR>  
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/26-3.htm" target="_blank">  民生长乐两全保险（分红型）费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/26-9.htm" target="_blank"> 民生长乐两全保险（分红型）现金价值表>></a></li>
      </td>					
     </TR>  
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/27-3.htm" target="_blank">  民生金榜题名两全保险（分红型）费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/27-9.htm" target="_blank"> 民生金榜题名两全保险（分红型）现金价值表>></a></li>
      </td>					
     </TR>  
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/28-3.htm" target="_blank">  民生众悦团体年金保险（分红型）领取金额表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/28-7.htm" target="_blank"> 民生团体年金保险现金价值换算倍数表(5月29) >></a></li>
      </td>					
     </TR> 
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/29-3.htm" target="_blank">    民生金玉满堂两全保险（分红型）费率表>></a></li>
	  </td>
	  <td class = common>
      <li><a href="./RiskRate/29-9.htm" target="_blank">   民生金玉满堂两全保险（分红型）现金价值表>></a></li>
      </td>					
     </TR> 
     
     <tr class=common>
      <td class = common>
      <li><a href="./RiskRate/30-3.htm" target="_blank">  民生金喜年年两全保险（分红型）费率表>></a></li>
	  </td>                   
	  <td class = common>     
      <li><a href="./RiskRate/30-9.htm" target="_blank">  民生金喜年年两全保险（分红型）现金价值表>></a></li>
      </td>					
     </TR> 
        
   </table>
           
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
