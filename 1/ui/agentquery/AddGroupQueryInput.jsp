<%@include file="../common/jsp/AgentCheck.jsp"%>
<% 
//程序名称：AddGroupQueryInput.jsp
//程序功能：
//创建日期：2003-10-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./AddGroupQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./AddGroupQueryInit.jsp"%>
  <title>育成查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./AddGroupQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--增员查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAddGroup);">
            </td>
            <td class= titleImg>
                育成查询
            </td>            
    	</tr>
    </table>
  <Div  id= "divLAAddGroup" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          育成代理人编码 
        </TD>
        <TD  class= input> 
          <Input class=common  name=AgentCode readonly=true >
        </TD>     
        <td class=title>育成职级</td>
        <td class=input><input class=code name=AgentGrade
            CodeData="0|^A04|业务经理一级|^A05|业务经理二级|^A06|高级经理一级|^A07|高级经理二级|^A08|督导长|^A09|区域督导长"
            ondblClick="showCodeListEx('AgentGradeList',[this],[0,1]);"
            onkeyup="showCodeListKeyEx('AgentGradeList',[this],[0,1]);"> 
        </td>    
      </TR>         
   </table>
                    
	   <table> 
		    <tr>		
			    <td>
			      <INPUT class=common VALUE="查  询" TYPE=button onclick="easyQueryClick();"> 
			    </td>
			    <td>  
			      <INPUT class=common VALUE="代理人详细信息" TYPE=button onclick="agentDetailQry();">  
			    </td>
		    </tr>  		    
	   </table> 
  </Div>      
          				
    <table>
    	<tr>
                <td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAddGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 育成代理人信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAddGroupGrid" style= "display: ''">
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAddGroupGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table>
    		<tr>
    			<td>
			      <INPUT class=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			    </td>
			    <td>  
			      <INPUT class=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    </td>
			    <td> 			      
			      <INPUT class=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    </td>
			    <td> 			      
			      <INPUT class=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 						
			    </td>  			
  			</tr>
  		</table>
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
