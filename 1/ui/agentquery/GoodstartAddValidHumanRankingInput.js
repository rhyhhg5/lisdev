var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var queryFlag = false;
function query(){
	
	if(!verifyInput2()){
		return false;
	}
	if(fm.ExamineDate.value<201601||fm.ExamineDate.value>201603){
		alert("考核年月只能2016年前三个月以内的");
		return false;
		}
	var year = fm.ExamineDate.value.substr(0,4);
	var month = fm.ExamineDate.value.substr(4,2);
	var managecom = fm.ManageCom.value;
	var lastYear = parseInt(year)-1;
	var sqlWhere ="";
	if(managecom=='86'|| managecom.length==4)
	{
		sqlWhere = " and  a.managecom like '"+managecom+"%' ";
	}
	else if(managecom.length==8) 
	{
//		if(managecom.substr(6,2)=='00')
//		{
//			sqlWhere = " and (a.managecom in (select code1 from ldcode1 where code = '"+managecom+"' and codetype ='"+year+"includemanagecom') or a.managecom like '"+managecom.substr(0,6)+"%' )";
//		}
//		else
//		{
			sqlWhere = " and a.managecom in (select code1 from ldcode1 where code = '"+managecom+"' and codetype ='"+year+"includemanagecom' union select '"+managecom+"' from dual) "; 
//		}
	}
	var  tWorkSQL ="(select  agentcode  from lahumandesc where year ='"+year+"' and month ='"+month+"' )";
	var   tCurrentMonth = new Date().getMonth()+1;
	if(parseInt(tCurrentMonth, "10")<=parseInt(month, "10"))
	{
		tWorkSQL ="(select a.Agentcode from laagent a,LADimission f "
			+ "where a.agentcode=f.agentcode and a.BranchType='1' and a.branchtype2='01'  " 
			+sqlWhere+" and a.AgentState='02' and a.employdate >='2015-12-16' "
			+ "group by a.agentcode "
			+ "having  min(days(a.employdate)-days(f.DepartDate)) >= '181' "
			+ "union "
			+"select a.Agentcode from laagent a "
			+ "where  a.BranchType='1' and a.branchtype2='01' "+sqlWhere+" and  a.AgentState='02' and a.employdate <'2015-12-16' "
			+ "union "
			+ "select a.Agentcode from laagent a "
			+ "where a.BranchType='1' "+sqlWhere+" and  a.AgentState='01' and a.branchtype2='01') ";
	}
	var strSQL = "";
	if(month=='03'||month=='02'||month=='01'){
		strSQL=" select comno,(select name from ldcom where comcode=substr(ld.code,1,4)),(select name from ldcom where comcode=ld.code),coalesce(sum,0)," +
				"(case when sum>=30 then  " +
				"(case when comno<=10 then '30000元' " +
				"when comno between '11' and '20' then '25000元' " +
				"when comno between '21' and '30' then '20000元' " +
				"when comno between '31' and '40' then '15000元' " +
				"when comno between '41' and '50' then '10000元' " +
				"else '' end) else '' end) from( "
//				+" select char(0+rownumber() over()) comno,managecom, humandeveidesc ,count(1) sum, "
//				+" (select max(employdate) from laagent c where managecom=bb.managecom and employdate between (date('"+year+"-12-16') - 1 years) and '"+year+"-3-31'  and branchtype='1'  and branchtype2='01') maxemp "
//				+" from ( "
				+" select char(0+rownumber() over()) comno,managecom,count sum,max(employdate)  maxemp from ( "
				+" select managecom,employdate from ("
				+" select a.agentcode,(case when (select code from ldcode1 where code1 = a.managecom and codetype ='"+year+"includemanagecom') is null then  a.managecom  else (select code from ldcode1 where code1 = a.managecom and codetype ='"+year+"includemanagecom') end) managecom,"
				+" sum(fyc) as sumfyc,employdate "
				+" ,(case when employdate<='"+year+"-1-31' then '3' when employdate between '"+year+"-2-1' and  last_day(date('"+year+"-2-1')) then '2' else '1' end ) as month "
				+" from laagent a,lacommision b where a.agentcode=b.agentcode  "
				+" and a.employdate between '"+lastYear+"-12-16' and last_day(date('"+year+"-"+month+"-1')) "
				+" and a.agentcode in "+tWorkSQL+" and b.branchtype='1'   and b.branchtype2='01'  "
				+" and b.wageno between '"+lastYear+"12' and '"+fm.ExamineDate.value+"' and b.tmakedate>='"+lastYear+"-12-16' "
				+" and (b.Payyear =0 and b.renewcount = 0) "
				+" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=b.polno and d.wageno>'"+lastYear+"11' and d.managecom=b.managecom) "
				+sqlWhere
				+"  group by a.managecom,a.agentcode,employdate"
//				+" , ((case when employdate<='"+year+"-1-31' then '3' when employdate between '"+year+"-2-1' and  last_day(date('"+year+"-2-1')) then '2' else '1' end ) "
				+" )as aa "
				+" where sumfyc/month>=200 and managecom in(select code from ldcode where codetype='2016humandeveidesc')"
				+" )as bb "
				+"  group by managecom "
				+" order by sum  desc,maxemp   "
//				+" group by managecom,humandeveidesc "
				+" )as cc right join ldcode ld on cc.managecom=ld.code where codetype='2016humandeveidesc' order by int(comno)"
//				+" where managecom like '"+fm.ManageCom.value+"%' "
				+" with ur "; 
		} 
//		  else  if(month=='02'){
//		strSQL=" select comno,(select name from ldcom where comcode=managecom),humandeveidesc ,sum,(case when sum>=30 then  (case when comno<=10 then '30000元' when comno between '11' and '20' then '25000元' when comno between '21' and '30' then '20000元' else '' end) else '' end) from( "
//				+" select char(0+rownumber() over()) comno,managecom,humandeveidesc ,count(1) sum ,"
//				+" (select max(employdate) from laagent c where managecom=bb.managecom and employdate between '2014-12-16' and '2015-2-15' and branchtype='1'   and branchtype2='01' ) maxemp "
//				+" from (  select managecom,humandeveidesc  from ( "
//				+" select (case  when a.managecom='86120100' then '86120000' when a.managecom in ('86440102','86440103','86440104','86440101') then '86440100' else a.managecom end) managecom,a.agentcode,a.groupagentcode groupagentcode,((select substr(codename,1,1) from ldcode where codetype='humandeveidesc' and code=a.managecom and a.managecom not in('86120100','86440102','86440103','86440104','86440101')) union (select substr(codename,1,1) from ldcode where codetype='humandeveidesc' and code='86120000' and a.managecom='86120100') union (select substr(codename,1,1) from ldcode where codetype='humandeveidesc' and code='86440100' and a.managecom in('86440102','86440103','86440104','86440101'))) humandeveidesc, "
//				+" sum(fyc) as sumfyc  "
//				+" ,(case when employdate<='2015-1-15' then '2'  else '1' end ) as month "
//				+" from laagent a,lacommision b where a.agentcode=b.agentcode  "
//				+" and a.employdate between '2014-12-16' and '2015-2-15' "
//				+" and a.agentstate='01' and b.branchtype='1'  and b.branchtype2='01' "
//				+" and b.modifydate>='2014-12-16' "
//				+" and b.wageno between '201412' and '201502' and b.tmakedate>='2014-12-16' "
//				+" and (b.Payyear =0 and b.renewcount = 0) "
//				+" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=b.polno and d.wageno>'201411' and d.managecom=b.managecom) "
//				+" group by a.managecom,a.agentcode,a.groupagentcode,b.managecom  "
//				+" , (case when employdate<='2015-1-15' then '2'  else '1' end ) "
//				+" )as aa "
//				+" where (humandeveidesc='A' and (sumfyc/month)>=200 ) or (humandeveidesc='B' and (sumfyc/month)>=400) "
//				+" )as bb "
//				+" group by managecom,humandeveidesc "
//				+" order by count(1) desc,maxemp "
//				+" )as cc "
//				+" where managecom like '"+fm.ManageCom.value+"%' "
//				+" with ur ";
//		}
//			 else   if(month=='01'){
//		       strSQL=" select comno,(select name from ldcom where comcode=managecom),humandeveidesc ,sum,(case when sum>=30 then  (case when comno<=10 then '30000元' when comno between '11' and '20' then '25000元' when comno between '21' and '30' then '20000元' else '' end) else '' end) from( "
//				+" select char(0+rownumber() over()) comno,managecom,humandeveidesc ,count(1) sum, "
//				+" (select max(employdate) from laagent c where managecom=bb.managecom and employdate between '2014-12-16' and '2015-1-15' and branchtype='1'   and branchtype2='01') maxemp "
//				+"   from ( select managecom,humandeveidesc  from ( "
//				+" select (case  when a.managecom='86120100' then '86120000' when a.managecom in ('86440102','86440103','86440104','86440101') then '86440100' else a.managecom end) managecom,a.agentcode,a.groupagentcode groupagentcode,((select substr(codename,1,1) from ldcode where codetype='humandeveidesc' and code=a.managecom and a.managecom not in('86120100','86440102','86440103','86440104','86440101')) union (select substr(codename,1,1) from ldcode where codetype='humandeveidesc' and code='86120000' and a.managecom='86120100') union (select substr(codename,1,1) from ldcode where codetype='humandeveidesc' and code='86440100' and a.managecom in('86440102','86440103','86440104','86440101'))) humandeveidesc, "
//				+" sum(fyc) as sumfyc  "
//				+" from laagent a,lacommision b where a.agentcode=b.agentcode  "
//				+" and a.employdate between '2014-12-16' and '2015-1-15' "
//				+" and (b.Payyear =0 and b.renewcount = 0) "
//				+" and a.agentstate='01' and b.branchtype='1' and b.branchtype2='01' "
//				+" and b.wageno between '201412' and '201501' and b.tmakedate>='2014-12-16' "
//				+" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=b.polno and d.wageno>'201411' and d.managecom=b.managecom) "
//				+" and b.modifydate>='2014-12-16' "
//				+" and b.wageno = '201501'  "
//				+" group by a.managecom,a.agentcode,a.groupagentcode,b.managecom  "
//				+" )as aa "
//				+" where (humandeveidesc='A' and sumfyc>=200 ) or (humandeveidesc='B' and sumfyc>=400) "
//				+" )as bb "
//				+" group by managecom,humandeveidesc "
//				+" order by count(1) desc,maxemp "
//				+" )as cc "
//				+" where managecom like '"+fm.ManageCom.value+"%' "
//				+" with ur ";
//		}
		 
	turnPage.queryModal(strSQL,ExplainGrid);
	fm.querySql.value=strSQL;
	queryFlag= true;
}

function download(){
	if(!queryFlag)
	{
		alert("下载前请先进行【查询】操作！");
		return false;
	}
	
	if(!verifyInput2()){
		return false;
	}
	if(fm.querySql.value!=null & fm.querySql.value!=""){
		var formAction = fm.action;
		fm.action = "GoodstartDownload.jsp";
		fm.submit();
		fm.target = "fraSubmit";
		fm.action = formAction;
	}else{
		alert("没有相关数据！");
		return;
	}
}