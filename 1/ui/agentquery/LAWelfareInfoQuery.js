//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet1,arrDataSet2; 
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAWelfareInfoQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 




         

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


// 查询按钮
function easyQueryClick()
{
	//查询修改
	var old_AgentGroup=fm.all('AgentGroup').value;
	var new_AgentGroup="";
	if (old_AgentGroup!='')
	{
      strSQL_AgentGroup = "select AgentGroup from labranchgroup where 1=1 "
                         +"and BranchAttr='"+old_AgentGroup+"' and (state<>'1' or state is null)"
      var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
      if (!strQueryResult_AgentGroup)
      {
      	alert("查询失败");
      	return false;
      }
      if (strQueryResult_AgentGroup)
      {
        var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
        //var tArr_AgentGroup = new Array();
        //tArr_AgentGroup = chooseArray(arrDataSet_AgentGroup,[0,1,2]);
        //以备显示时使用
        //fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
        //new_AgentGroup=tArr_AgentGroup[0][0];
        new_AgentGroup=arrDataSet_AgentGroup[0][0];
      }
    }
    var str_new_AgentGroup="";
    if (new_AgentGroup!='')
      str_new_AgentGroup=" and LAWelfareInfo.AgentGroup='"+new_AgentGroup+"' ";
	//<addcode>############################################################//
	
	
  // 初始化表格
  initWelfareInfoGrid1();
	
  // 书写SQL语句
 var tReturn = getManageComLimitlike("a.ManageCom");
  var strSQL = "";
 var strManageCom=fm.all('ManageCom').value;
 if (strManageCom!="")
   strManageCom=" and LAWelfareInfo.ManageCom='"+strManageCom+"' ";
  //<addcode>############################################################//
 
	strSQL = "select a.AgentCode,b.BranchAttr,c.AgentGrade,a.ManageCom,Sum(a.SumMoney)"
			 + " from LAWelfareInfo a,LABranchGroup b ,latree c "
			 +" where c.Agentgroup=b.Agentgroup and a.agentcode=c.agentcode and b.BranchType='1'"			 
	     +tReturn
	     + getWherePart('AgentCode')
	         + getWherePart('b.BranchType','BranchType')
	         + str_new_AgentGroup
	         + getWherePart('AgentGrade')
	         + strManageCom
	         +" Group by b.BranchAttr,a.AgentCode,c.AgentGrade,a.ManageCom "
	         

  turnPage1.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage1.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage1.strQueryResult);
  
  turnPage1.arrDataCacheSet = chooseArray(arrDataSet1,[0,1,2,3,4]);

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage1.pageDisplayGrid = WelfareInfoGrid1;    
          
  //保存SQL语句
  turnPage1.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage1.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage1.pageDisplayGrid);
  
}


function getQueryResult1()
{
  var arrSelected = null;
  tRow = WelfareInfoGrid1.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet1 == null )
    return arrSelected;
  arrSelected = new Array();
  tRow = 10 * turnPage1.pageIndex + tRow; //10代表multiline的行数
  //设置需要返回的数组
  arrSelected[0] = arrDataSet1[tRow-1];
  return arrSelected;
}

function getQueryResult2()
{
  var arrSelected = null;
  tRow = WelfareInfoGrid2.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet2 == null )
    return arrSelected;
  arrSelected = new Array();
  tRow = 10 * turnPage2.pageIndex + tRow; //10代表multiline的行数
  //设置需要返回的数组
  arrSelected[0] = arrDataSet2[tRow-1];
  return arrSelected;
}


function selectItem(spanID,parm2)
{
  var arrSelect = new Array();
  arrSelect = getQueryResult1();
  var strAgentCode=arrSelect[0][0];
  var strAgentGroup=arrSelect[0][5];
  var strAgentGrade=arrSelect[0][2];
  var strManageCom=arrSelect[0][3];
  //alert(strAgentCode+'aa');
  if (strAgentCode!='')
    strAgentCode=" and AgentCode='"+strAgentCode+"'";
  if (strAgentGroup!='')
    strAgentGroup=" and AgentGroup='"+strAgentGroup+"'";
  if (strAgentGrade!='')
    strAgentGrade=" and AgentGrade='"+strAgentGrade+"'";
  if (strManageCom!='')
    strManageCom=" and ManageCom='"+strManageCom+"'";
  
  initWelfareInfoGrid2();
  // 书写SQL语句
 
  var strSQL = "";
  strSQL="select * from LAWelfareInfo"
	         +" where 1=1 "
	         + strAgentCode
	         + strAgentGroup
	         + strAgentGrade
	         + strManageCom
  
 //alert(strSQL);
  turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  
  //判断是否查询成功
  if (!turnPage2.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet2 = decodeEasyQueryResult(turnPage2.strQueryResult);
  turnPage2.arrDataCacheSet = chooseArray(arrDataSet2,[6,7,8,12,13,14,16]);

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage2.pageDisplayGrid = WelfareInfoGrid2;    
          
  //保存SQL语句
  turnPage2.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage2.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage2.pageDisplayGrid);

}








