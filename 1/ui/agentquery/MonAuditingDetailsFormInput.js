var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

function download(){
	if(!verifyInput2()){
		return false;
	}
	if(fm.AssessYearMonth.value==''||fm.ManageCom.value==''){
		alert("管理机构和考核年月不能为空");
		return false;
	}
	if(fm.AssessYearMonth.value<201601||fm.AssessYearMonth.value>201612){
		alert("考核年月只能2016年以内的");
		return false;
	}
	var allWhere ="  and BranchType='1' and branchtype2='01'  and  Payyear = 0 and renewcount ='0' ";
	var iSql = "select code1 from ldcode1 where code = '"+fm.ManageCom.value+"' and codetype = '2016includemanagecom'  ";
	var strQueryResult = easyQueryVer3(iSql, 1, 0, 1);
	if(!strQueryResult){
		allWhere += " and  managecom like '"+fm.ManageCom.value+"%'"+
		" and polno not in(select polno from lacommision  d where d.TRANSTYPE='WT'   and d.wageno>'201511' and d.managecom like '"+fm.ManageCom.value+"%')"+
		" and agentcode in (select agentcode from lahumandesc a where  mngcom  like '"+fm.ManageCom.value+"%'"+
		" and year=substr('"+fm.AssessYearMonth.value+"',1,4)  " +
		" and  month=substr('"+fm.AssessYearMonth.value+"',5,2))" ;
	}else{
		allWhere += " and  managecom  in(select code1 from ldcode1 where code = '"+fm.ManageCom.value+"' and codetype = '2016includemanagecom' )"+
		" and polno not in(select polno from lacommision  d where d.TRANSTYPE='WT'    and d.wageno>'201511' and d.managecom   in(select code1 from ldcode1 where code = '"+fm.ManageCom.value+"' and codetype = '2016includemanagecom' )) "+
		" and agentcode in (select agentcode from lahumandesc where   mngcom   in(select code1 from ldcode1 where code = '"+fm.ManageCom.value+"' and codetype = '2016includemanagecom' ) "+
		" and year=substr('"+fm.AssessYearMonth.value+"',1,4)  " +
		" and  month=substr('"+fm.AssessYearMonth.value+"',5,2))" ;
	}
	if(fm.AgentCode.value!='' && fm.AgentCode.value!=null){
		allWhere  +=" and agentcode =getAgentCode('"+fm.AgentCode.value+"')";
	}
	var ym = fm.AssessYearMonth.value;
	var tMonth = ym.substr(4,6);
	var hgSQL ="";
	var yxSQL=" and wageno between '201512' and '"+ym+"' and Tmakedate > '2015-12-15'" ;
	if("01"==tMonth){
		hgSQL +=" and wageno between '201512' and '201601' and Tmakedate > '2015-12-15'";
    }else if("12"==tMonth){
    	hgSQL +=" and wageno = '201612' and Tmakedate < '2017-1-1'";
    }else{
    	hgSQL +=" and wageno = '"+ym+"'";
    }
	
	var yxTable = "(select agentcode  from lacommision where 1=1 and agentcode in (select agentcode from laagent where employdate >='2015-12-16')  "+allWhere+yxSQL +" group by agentcode having sum(fyc)>0 ) ";
	
	var hgTable =" (select agentcode from  lacommision where 1=1 "+allWhere+hgSQL +" group by agentcode  "+
		" having  sum(fyc) >= (select riskwrapplanname from ldcode1 where codetype='2016humandeveiplan' and code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)))" ;
	var strSQL="";
	var strSQL0 = " select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) plan,"+
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc ,'是','是' from lacommision  a where  1=1 "+allWhere+hgSQL +
	"and a.agentcode in "+ yxTable +" " +
	"and a.agentcode in "+hgTable+" " +
	" group by tmakedate,contno,riskcode,managecom,agentcode  ";
	var strSQL1 = " select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) plan,"+
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc ,'是','否' from lacommision  a where  1=1 "+allWhere+hgSQL +
	"and a.agentcode not in "+ yxTable +" " +
	"and a.agentcode in "+hgTable+" " +
	" group by tmakedate,contno,riskcode,managecom,agentcode  ";
	var strSQL2 = " select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) plan,"+
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc ,'否','是'  from lacommision  a where  1=1 "+allWhere+hgSQL +
	"and a.agentcode  in "+ yxTable +" " +
	"and a.agentcode not in "+hgTable+" " +
	" group by tmakedate,contno,riskcode,managecom,agentcode  ";
	var strSQL3 = " select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) plan,"+
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc ,'否','否'  from lacommision  a where  1=1 "+allWhere+hgSQL + 
	"and a.agentcode not in "+ yxTable +" " +
	"and a.agentcode not in "+hgTable+" " +
	" group by tmakedate,contno,riskcode,managecom,agentcode  ";
	if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='0'){
		strSQL +=strSQL3;
	}
	else if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='1'){
		
		strSQL +=strSQL2;
	}
	else if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value==''){
		
		strSQL +=strSQL2+" union "+strSQL3;
	}
	else if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='0'){
		
		strSQL +=strSQL1;
	}	
	else if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='1'){
		
		strSQL +=strSQL0;
	}
	else if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value==''){
		strSQL +=strSQL0 +" union "+strSQL1;
	}
	else if(fm.PassedPeopleFlag.value==''&&fm.ValidPeopleFlag.value=='0'){
		
		strSQL +=strSQL1+" union "+strSQL3;
	}
	else if(fm.PassedPeopleFlag.value==''&&fm.ValidPeopleFlag.value=='1'){
	
	strSQL +=strSQL2+" union "+strSQL0;
	}
	else
	{
		strSQL =strSQL0+" union "+strSQL1+" union "+strSQL2+" union "+strSQL3;
	}
	strSQL+=" with ur";
	

	fm.querySql.value = strSQL;
	var tArr = easyExecSql(strSQL);
	if(tArr!=null){
		var formAction = fm.action;
		fm.action = "MonAuditingDetailsDownload.jsp";
		fm.submit();
		fm.target = "fraSubmit";
		fm.action = formAction;
	}else{
		alert("没有数据");
		return;
	}
}

function query(){
	if(!verifyInput2()){
		return false;
	}
	if(fm.AssessYearMonth.value==''||fm.ManageCom.value==''){
		alert("管理机构和考核年月不能为空");
		return false;
	}
	if(fm.AssessYearMonth.value<201601||fm.AssessYearMonth.value>201612){
		alert("考核年月只能2016年以内的");
		return false;
	}
	var allWhere ="  and BranchType='1' and branchtype2='01'  and  Payyear = 0 and renewcount ='0' ";
	var iSql = "select code1 from ldcode1 where code = '"+fm.ManageCom.value+"' and codetype = '2016includemanagecom'  ";
	var strQueryResult = easyQueryVer3(iSql, 1, 0, 1);
	if(!strQueryResult){
		allWhere += " and  managecom like '"+fm.ManageCom.value+"%'"+
		" and polno not in(select polno from lacommision  d where d.TRANSTYPE='WT'   and d.wageno>'201511' and d.managecom like '"+fm.ManageCom.value+"%')"+
		" and agentcode in (select agentcode from lahumandesc a where  mngcom  like '"+fm.ManageCom.value+"%'"+
		" and year=substr('"+fm.AssessYearMonth.value+"',1,4)  " +
		" and  month=substr('"+fm.AssessYearMonth.value+"',5,2))" ;
	}else{
		allWhere += " and  managecom  in(select code1 from ldcode1 where code = '"+fm.ManageCom.value+"' and codetype = '2016includemanagecom' )"+
		" and polno not in(select polno from lacommision  d where d.TRANSTYPE='WT'    and d.wageno>'201511' and d.managecom   in(select code1 from ldcode1 where code = '"+fm.ManageCom.value+"' and codetype = '2016includemanagecom' )) "+
		" and agentcode in (select agentcode from lahumandesc where   mngcom   in(select code1 from ldcode1 where code = '"+fm.ManageCom.value+"' and codetype = '2016includemanagecom' ) "+
		" and year=substr('"+fm.AssessYearMonth.value+"',1,4)  " +
		" and  month=substr('"+fm.AssessYearMonth.value+"',5,2))" ;
	}
	if(fm.AgentCode.value!='' && fm.AgentCode.value!=null){
		allWhere  +=" and agentcode =getAgentCode('"+fm.AgentCode.value+"')";
	}
	var ym = fm.AssessYearMonth.value;
	var tMonth = ym.substr(4,6);
	var hgSQL ="";
	var yxSQL=" and wageno between '201512' and '"+ym+"' and Tmakedate > '2015-12-15'" ;
	if("01"==tMonth){
		hgSQL +=" and wageno between '201512' and '201601' and Tmakedate > '2015-12-15'";
    }else if("12"==tMonth){
    	hgSQL +=" and wageno = '201612' and Tmakedate < '2017-1-1'";
    }else{
    	hgSQL +=" and wageno = '"+ym+"'";
    }
	
	var yxTable = "(select agentcode  from lacommision where 1=1 and agentcode in (select agentcode from laagent where employdate >='2015-12-16')  "+allWhere+yxSQL +" group by agentcode having sum(fyc)>0 ) ";
	
	var hgTable =" (select agentcode from  lacommision where 1=1 "+allWhere+hgSQL +" group by agentcode  "+
		" having  sum(fyc) >= (select riskwrapplanname from ldcode1 where codetype='2016humandeveiplan' and code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)))" ;
	var strSQL="";
	var strSQL0 = " select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) plan,"+
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc ,'是','是' from lacommision  a where  1=1 "+allWhere+hgSQL +
	"and a.agentcode in "+ yxTable +" " +
	"and a.agentcode in "+hgTable+" " +
	" group by tmakedate,contno,riskcode,managecom,agentcode  ";
	var strSQL1 = " select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) plan,"+
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc ,'是','否' from lacommision  a where  1=1 "+allWhere+hgSQL +
	"and a.agentcode not in "+ yxTable +" " +
	"and a.agentcode in "+hgTable+" " +
	" group by tmakedate,contno,riskcode,managecom,agentcode  ";
	var strSQL2 = " select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) plan,"+
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc ,'否','是'  from lacommision  a where  1=1 "+allWhere+hgSQL +
	"and a.agentcode  in "+ yxTable +" " +
	"and a.agentcode not in "+hgTable+" " +
	" group by tmakedate,contno,riskcode,managecom,agentcode  ";
	var strSQL3 = " select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) plan,"+
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc ,'否','否'  from lacommision  a where  1=1 "+allWhere+hgSQL + 
	"and a.agentcode not in "+ yxTable +" " +
	"and a.agentcode not in "+hgTable+" " +
	" group by tmakedate,contno,riskcode,managecom,agentcode  ";
	if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='0'){
		strSQL +=strSQL3;
	}
	else if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='1'){
		
		strSQL +=strSQL2;
	}
	else if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value==''){
		
		strSQL +=strSQL2+" union "+strSQL3;
	}
	else if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='0'){
		
		strSQL +=strSQL1;
	}	
	else if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='1'){
		
		strSQL +=strSQL0;
	}
	else if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value==''){
		
		strSQL =strSQL0+" union "+strSQL1;
	}
	else if(fm.PassedPeopleFlag.value==''&&fm.ValidPeopleFlag.value=='0'){
		
		strSQL +=strSQL1+" union "+strSQL3;
	}
	else if(fm.PassedPeopleFlag.value==''&&fm.ValidPeopleFlag.value=='1'){
	
	strSQL +=strSQL2+" union "+strSQL0;
	}
	else
	{
		strSQL =strSQL0+" union "+strSQL1+" union "+strSQL2+" union "+strSQL3;
	}
	strSQL+=" with ur";
	turnPage.queryModal(strSQL,MonAudGrid);
	

}

