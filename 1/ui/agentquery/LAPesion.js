//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet1,arrDataSet2; 
var turnPage = new turnPageClass();
var tArr_AgentInfo = new Array();

//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAComQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 




         

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


// 查询按钮
function easyQueryClick()
{
	strSQL = "select "
		 + "LAIndexInfo.IndexCalNo,"
		 + "LAIndexInfo.IndexType,"
		 + "LAIndexInfo.AgentCode,"
		 +"LABranchGroup.BranchAttr,"
		 + "LAIndexInfo.ManageCom,"
	         + "LAIndexInfo.StartDate,"
	         + "LAIndexInfo.StartEnd"
	         	        
	         +" from LAIndexInfo,LABranchGroup "
	         
	         +" where 1=1 and IndexType='01'"
	         +"and LABranchGroup.AgentGroup=LAIndexInfo.AgentGroup "	
	         + getWherePart('IndexCalNo')
	         + getWherePart('AgentCode')
  alert(strSQL);       

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //alert(strSQL);
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage.strQueryResult);
  
  turnPage.arrDataCacheSet = chooseArray(arrDataSet1,[0,1,2,3,4,5,6]);

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = IndexInfoGrid1;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}


function getQueryResult1()
{
  var arrSelected = null;
  tRow = IndexInfoGrid1.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet1 == null )
    return arrSelected;
  arrSelected = new Array();
  tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
  //设置需要返回的数组
  arrSelected[0] = arrDataSet1[tRow-1];
  return arrSelected;
}




function selectItem(spanID,parm2)
{
  var arrSelect = new Array();
  arrSelect = getQueryResult1();
  var strIndexCalNo=arrSelect[0][0];
  var strIndexType="01";
  var strAgentCode=tArr_AgentInfo[0];
  var strDate=fm.all('AgentDate');
  if (strIndexCalNo!="")
    strIndexCalNo=" and IndexCalNo='"+strIndexCalNo+"'";
  if (strIndexType!="")
    strIndexType=" and IndexType='"+strIndexType+"'";
  if (strAgentCode!="")
    strAgentCode=" and AgentCode='"+strAgentCode+"'";


  var strSQLMoreResult="select * from LAIndexInfo where 1=1 "
                 + strIndexCalNo
	         + strIndexType
	         + strAgentCode;
	        
  var strMoreResult=easyQueryVer3(strSQLMoreResult, 1, 1, 1);
  fm.all('HiddenStrMoreResult').value=strMoreResult; 
  fm.all('ManageCom').value=arrSelect[0][4];
  if (strMoreResult)
  {
   showInfo=window.open("./LAPesionShow.htm");
  }
  else
    alert("查询失败");

}

function QryAgentInfo()
{

      var strSQL_Agent = "select * from laagent where 1=1 "
                         +"and agentcode='"+fm.all('AgentCode').value+"'"
      var strQueryResult_Agent = easyQueryVer3(strSQL_Agent, 1, 1, 1);
      if (!strQueryResult_Agent)
      {
      	alert("查询失败");
      	return false;
      }
      if (strQueryResult_Agent)
      {
        var arrDataSet_Agent = decodeEasyQueryResult(strQueryResult_Agent);
        var i=0;
        while(arrDataSet_Agent[0][i]!=null)
        {
           tArr_AgentInfo[i]=arrDataSet_Agent[0][i];
           i=i+1;
        }  
      }
      fm.all('AgentName').value=tArr_AgentInfo[5];
}
