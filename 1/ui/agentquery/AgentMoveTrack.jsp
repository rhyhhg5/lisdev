<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>  
<%
//程序名称：AgentMoveTrack.jsp
//程序功能：
//创建日期：2003-07-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="AgentMoveTrack.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AgentMoveTrackInit.jsp"%>
  <title>代理人异动轨迹查询 </title>   
</head>
<body  onload="initForm();" >
  <form action="./AgentMoveTrackQuery.jsp" method=post name=fm target="fraSubmit">    
    <table class= common border=0 width=100%>
    	<tr>
	  <td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table class=common>
       <TR class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('ComCode',[this]);" 
                                               onkeyup="return showCodeListKey('ComCode',[this]);"
                                               verify = "管理机构|notnull&code:comcode">
          </TD>            
          <TD  class= title>
            代理人代码
          </TD>
          <TD class= input>
            <Input class=common name=AgentCode verify="代理人代码|len<=10" >
          </TD>    
        </TR>
      	<TR  class= common>
      	  <TD  class= title>
            代理人名称 
          </TD>
          <TD  class= input>
            <Input class=common name=Name >
          </TD>
          <TD class=common>
            <input class=common type=button name=confirmb value="确定" onclick="return submitForm();">
          </TD>
        </TR>
    </table>    
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=TableFlag value='-1'> <!--标记是备份表(0)还是正式表(1)-->
    <table>
    	<tr>
        <td class=common>
	   <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAssess1);">
    	</td>
    	<td class= titleImg>
    	   异动信息
        </td>
    	</tr>
    </table>
  	<Div  id= "divMoveTrack" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanMoveTrackGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 				
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html> 
