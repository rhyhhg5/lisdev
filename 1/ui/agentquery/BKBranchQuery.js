//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var arrDataSet; 
var turnPage = new turnPageClass();


//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在BKBranchQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function getQueryResult()
{
	var arrSelected = null;	
	arrSelected = new Array();	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select branchattr,a.Name,a.managecom,UpBranch,BranchLevel,"
	+"decode(BranchJobType,'1','Y','N'),BranchManager,b.name,BranchAddress,"
	+"BranchZipcode,BranchPhone,FoundDate,EndFlag,EndDate,(select decode(a,'1','专职','兼职') from ("
	+"select '1' a from labranchgroup a,laagent b where a.branchmanager=b.agentcode "
	+"and a.agentgroup=b.agentgroup and branchattr='"+tBranchAttr+"' union "
	+"select '2' a from labranchgroup a,laagent b where a.branchmanager=b.agentcode "
	+"and a.agentgroup<>b.agentgroup and branchattr='"+tBranchAttr+"'))  "
	+"from laagent b,labranchgroup a where (a.state<>'1' or a.state is null) "
	+"and a.branchmanager=b.agentcode and a.branchtype='3' and branchattr='"
	+tBranchAttr+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);	
	return arrSelected;
}


