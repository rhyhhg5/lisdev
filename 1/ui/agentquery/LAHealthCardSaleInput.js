 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


//提交，保存按钮对应操作
function ListExecl()
{
  if (verifyInput() == false)
    return false;

  var sql = fm.querySql.value;
  if(sql==""||sql==null)
  {
	  alert("请先查询");
	  return false;
  }
  var oldAction = fm.action;
  fm.action = "./LAHealthCardSaleSave.jsp";
  fm.submit();
  fm.action = oldAction;
  }


//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    AgentGrid.clearData("AgentGrid");
    //fm.all('AdjustBranchCode').value = '';
    //BranchChange();
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
}

//取消按钮对应操作
function cancelForm()
{
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function easyQueryClick()
{
  initGrpPolGrid();
  if (verifyInput() == false)
  return false;
  var whereSql =" and 1=1 ";
  var grpcontno = fm.all('GrpContNo').value;
  if(grpcontno!=""&&grpcontno!=null){
	  whereSql += " and (grpcontno ='"+grpcontno+"' or contno='"+grpcontno+"')";
  }
strSQL =
	"select wageno,managecom,branchattr,"
	+"(select name from labranchgroup where a.branchattr = branchattr fetch first 1 rows only), "
	+"getUniteCode(agentcode),(select name from laagent where a.agentcode = agentcode),"
	+"f3,(select WrapName from ldwrap where RiskWrapCode = a.f3 ),"
	+"contno,sum(transmoney),fycrate,sum(fyc) from "
	+"(select wageno,managecom,branchattr,agentcode,f3,(case when grpcontno='00000000000000000000' then contno else grpcontno end ) contno,transmoney,fycrate,fyc from "
	+"lacommision where branchtype ='7' and branchtype2='01' and f3 is not null " 
	+getWherePart('ManageCom','ManageCom','like')
	+getWherePart('WageNo','WageNo')
	+getWherePart('f3','RiskWrapCode')
	+getWherePart('BranchAttr','BranchAttr')
	+getWherePart('getUniteCode(agentcode)','GroupAgentCode')
	+whereSql
	+") a "
	+"group by  wageno,managecom,agentcode,f3,fycrate,branchattr,contno"
	+" order by wageno,managecom,agentcode,contno,f3 with ur";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
  	alert('不存在符合条件的有效纪录！');
  	return false;
  }
  //查询成功则拆分字符串，返回二维数组

  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  fm.all('querySql').value=strSQL;
}

function submitSave()
{
}

function clearAll()
{
   clearGrid();
}
function clearGrid()
{
//   fm.all('ManageCom').value = '';
//   fm.all('ManageComName').value = '';
//   fm.all('WageNo').value = '';
//  // fm.all('AgentSeries').value = '';
//   fm.all('AgentCode').value = '';
//   AgentGrid.clearData("AgentGrid");
//   AgentGrid.clearData("ContGrid");
}

 
 
