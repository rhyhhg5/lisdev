//               该文件中包含客户端需要处理的函数和事件

var arrDataSet;
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在BlacklistQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
  var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterBlackQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}


function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	
	if( tRow == 0 || tRow == null || arrDataSet == null )
	
	return arrSelected;
	
	arrSelected = new Array();
	tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
	
	return arrSelected;
}


function easyQueryClick()
{
	var strSQL1 = "";
	if(tflag=="YS")
			
			       
	strSQL1 = "select NVL(sum(ljtempfee_lmriskapp.prem),0) from ljtempfee_lmriskapp,LAbranchgroup where LAbranchgroup.agentgroup=ljtempfee_lmriskapp.agentgroup and (state<>'1' or state is null)"
                 + getWherePart('ljtempfee_lmriskapp.MakeDate','StartDay','>=')
                 + getWherePart('ljtempfee_lmriskapp.MakeDate','EndDay','<=')
                 + getWherePart('LAbranchgroup.BranchAttr','BranchCode','like');
                 
        
else if (tflag=="CB")                 
       	strSQL1 = "select NVL(sum(Transmoney),0) from LACommision where transmoney>0"
                 + getWherePart('signDate','StartDay','>=')
                 + getWherePart('signDate','EndDay','<=')
                 + getWherePart('BranchAttr','BranchCode','like');
                 
else if (tflag=="HD")	
	strSQL1 = "select NVL(sum(Transmoney),0) from LACommision where 1=1"
                 + getWherePart('getpolDate','StartDay','>=')
                 + getWherePart('getpolDate','EndDay','<=')
                 + getWherePart('BranchAttr','BranchCode','like');
                 
   else if (tflag=="CNH")	
	strSQL1 = "select NVL(sum(Transmoney),0) from LACommision where CommDire='1'"
                 + getWherePart('TMakeDate','StartDay','>=')
                 + getWherePart('TMakeDate','EndDay','<=')
                 + getWherePart('BranchAttr','BranchCode','like');       
	
			
	
	
else if (tflag=="YNC")
		strSQL1 = "select sum(b.Prem) from LAAgent a,LCPol b,LABranchGroup c where b.Agentcode=a.agentCode and c.AgentGroup=a.BranchCode and (c.state<>'1' or c.state is null)"	
							+ getWherePart('c.BranchAttr','BranchCode','like')
				 			+ getWherePart('b.MakeDate','StartDay','>=')
				 			+ getWherePart('b.MakeDate','EndDay','<=');
				 			
				 			
				 			
				 			
				 			
	else if(tflag=="ZT")
	{
		strSQL1 = "select sum(b.Prem) from LAAgent a,LBPol b,LPEdorMain c,LABranchGroup d where b.AgentGroup=a.BranchCode and b.Agentcode=a.agentCode and d.AgentGroup=a.BranchCode and b.PolNo=c.PolNo and (d.state<>'1' or d.state is null)"	
							+ getWherePart('d.BranchAttr','BranchCode','like')
				 			+ getWherePart('c.EdorValiDate','StartDay','>=')
				 			+ getWherePart('c.EdorValiDate','EndDay','<=');
	}
	
	else if(tflag=="WT")
	{
		strSQL1 = "select NVL(sum(Transmoney),0) from LACommision where transtype='WT'"
                 + getWherePart('signDate','StartDay','>=')
                 + getWherePart('signDate','EndDay','<=')
                 + getWherePart('BranchAttr','BranchCode','like');
	}
	
	if (tflag=="HD") 
	{
		strSQL1 = strSQL1+" and getpolDate is not null";
	}
	else if (tflag=="YNC") 
	{
		strSQL1 = strSQL1+" and b.AppFlag=0";
	}
	else if (tflag=="CNH") 
	{
		strSQL1 = strSQL1+" and CalDate is null";
	}
	else if (tflag=="ZT") 
	{
		strSQL1 = strSQL1 + " and c.EdorType='ZT'";
	}
	
	
	
		turnPage1.strQueryResult  = easyQueryVer3(strSQL1, 1, 0, 1);
	
	  //判断是否查询成功
  if (!turnPage1.strQueryResult) {

  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
  
  //设置初始化过的MULTILINE对象
          
  //保存SQL语句
  turnPage1.strQuerySql     = strSQL1; 
  
  //设置查询起始位置
  turnPage1.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet1 = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex, MAXSCREENLINES);
  
  fm.all('SumPrem').value = arrDataSet1[0][0];
  
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	if(tflag=="YS")	
	          
	strSQL = "select c.BranchAttr,a.agentcode,a.name,b.PolNo,b.AppntName,b.InsuredName,b.RiskCode,b.prem,b.PayYears,to_char(decode(payintv,'0',months_between(PaytoDate,CValiDate),'12',months_between(PaytoDate,CValiDate)/12,'-1',''),'999999999'),b.MakeDate from LAAgent a,LCPol b,LAbranchgroup c where a.agentcode=b.agentcode and a.branchcode=c.agentgroup  and (c.state<>'1' or c.state is null)"
         + getWherePart('c.BranchAttr','BranchCode','like') 
         + getWherePart('b.MakeDate','StartDay','>=')
	       + getWherePart('b.MakeDate','EndDay','<='); 
	       
else if (tflag=="CB") 
	          
	strSQL = "select distinct BranchAttr,a.AgentCode,a.Name,PolNo,p11,p13,RiskCode,d.Transmoney,PayYears,d.paycount,signDate from LAAgent a,LACommision d where a.AgentCode=d.AgentCode and d.branchtype='1' and transmoney>0"
	          + getWherePart('signDate','StartDay','>=')
				 		+ getWherePart('signDate','EndDay','<=')
				 		+ getWherePart('BranchAttr','BranchCode','like');
				 		
				 		
	else if (tflag=="HD") 
	          
	strSQL = "select distinct BranchAttr,a.AgentCode,a.Name,PolNo,p11,p13,RiskCode,d.Transmoney,PayYears,d.paycount,getpolDate from LAAgent a,LACommision d where a.AgentCode=d.AgentCode and d.branchtype='1'"
	          + getWherePart('BranchAttr','BranchCode','like')
	          + getWherePart('getpolDate','StartDay','>=')
				 		+ getWherePart('getpolDate','EndDay','<=');		 		
				 		
	else if (tflag=="CNH") 
	          
	strSQL = "select distinct d.BranchAttr,a.AgentCode,a.Name,b.PolNo,b.AppntName,b.InsuredName,b.RiskCode,d.Transmoney,b.PayYears,d.paycount,TMakeDate from LAAgent a,LCPol b,LACommision d where a.AgentCode=d.AgentCode and b.PolNo=d.PolNo and CommDire='1' and d.branchtype='1'"
	          + getWherePart('d.BranchAttr','BranchCode','like')
	          + getWherePart('TMakeDate','StartDay','>=')
				 		+ getWherePart('TMakeDate','EndDay','<=');					 		
	
else if (tflag=="YNC")
		strSQL = "select c.BranchAttr,a.AgentCode,a.Name,b.PolNo,b.AppntName,b.InsuredName,b.RiskCode,b.Prem,b.PayYears,to_char(decode(payintv,'0',months_between(PaytoDate,CValiDate),'12',months_between(PaytoDate,CValiDate)/12,'-1',''),'999999999'),b.MakeDate from LAAgent a,LCPol b,LABranchGroup c where b.Agentcode=a.agentCode and c.AgentGroup=a.BranchCode and (c.state<>'1' or c.state is null)"	
							+ getWherePart('c.BranchAttr','BranchCode','like')
				 			+ getWherePart('b.MakeDate','StartDay','>=')
				 			+ getWherePart('b.MakeDate','EndDay','<=');
				 			
				 			
				 			
				 			
				 			
	else if(tflag=="ZT")
	{
		strSQL = "select a.AgentGroup,a.AgentCode,a.Name,b.PolNo,b.AppntName,b.InsuredName,b.RiskCode,b.Prem,b.PayYears,to_char(decode(payintv,'0',months_between(b.PaytoDate,CValiDate),'12',months_between(b.PaytoDate,CValiDate)/12,'-1',''),'999999999'),c.EdorValiDate from LAAgent a,LBPol b,LPEdorMain c,LABranchGroup d where b.Agentcode=a.agentCode and d.AgentGroup=a.BranchCode and b.PolNo=c.PolNo and (d.state<>'1' or d.state is null)"	
							+ getWherePart('d.BranchAttr','BranchCode','like')
				 			+ getWherePart('c.EdorValiDate','StartDay','>=')
				 			+ getWherePart('c.EdorValiDate','EndDay','<=');
	}
	else if(tflag=="WT")
	{
		strSQL = "select distinct BranchAttr,a.AgentCode,a.Name,PolNo,p11,p13,RiskCode,d.Transmoney,PayYears,d.paycount,signDate from LAAgent a,LACommision d where a.AgentCode=d.AgentCode and d.branchtype='1'"
	          + getWherePart('signDate','StartDay','>=')
				 		+ getWherePart('signDate','EndDay','<=')
				 		+ getWherePart('BranchAttr','BranchCode','like');
	}
	
	if (tflag=="HD") 
	{
		strSQL = strSQL+" and getpolDate is not null";
	}
	else if (tflag=="YNC") 
	{
		strSQL = strSQL+" and b.AppFlag=0";
	}
	else if (tflag=="CNH") 
	{
		strSQL = strSQL+" and b.AppFlag=1 and CalDate is null";
	}
	else if (tflag=="ZT") 
	{
		strSQL = strSQL + " and c.EdorType='ZT'";
	}
	else if (tflag=="WT") 
	{
		strSQL = strSQL + " and transType='WT'";
	}
	
	
	
	//alert(strSQL);
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}
