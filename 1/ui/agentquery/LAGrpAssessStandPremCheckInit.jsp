<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2006-08-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.all('ManageCom').value='';
  	fm.all('ManageComName').value='';
  	fm.all('GroupAgentCode').value='';
  	fm.all('AgentName').value='';
  	fm.all('WageNoStart').value='';
  	fm.all('WageNoEnd').value='';
  }
  catch(ex)
  {
    alert("在LAGrpAssessStandPremCheckInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    
  
    
    
    iArray[2]=new Array();
    iArray[2][0]="业务员代码";          		//列名
    iArray[2][1]="80px";      	      		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[3]=new Array();
    iArray[3][0]="业务员姓名";          		//列名
    iArray[3][1]="80px";      	      		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
  
    
    iArray[4]=new Array();
    iArray[4][0]="月份"; //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许 


   
    iArray[5]=new Array();
    iArray[5][0]="财险交叉销售保费"; //列名
    iArray[5][1]="60px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许    

        
  
		
	iArray[6]=new Array();
    iArray[6][0]="寿险交叉销售保费"; //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="员工职级"; //列名
    iArray[7][1]="50px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[8]=new Array();
    iArray[8][0]="员工类型"; //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许
  

   
        
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.mulLineCount = 10;   
    SetGrid.displayTitle = 1;
    SetGrid.locked = 1;
    SetGrid.canSel = 0;
    SetGrid.hiddenPlus=1;       
    SetGrid.hiddenSubtraction=1;
    SetGrid.loadMulLine(iArray);      
  }
  catch(ex)
  {
    alert("在LAGrpAssessStandPremCheckInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    
  }
  catch(re)
  {
    alert("在LAGrpAssessStandPremCheckInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>