<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：MonAuditingDetailsFormInit.jsp
//程序功能：
//创建日期：2015-02-04
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script src="../common/javascript/Common.js"></script>
<%
	PubFun tpubFun = new PubFun();
	String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
	function initInpBox(){
		try{
			fm.all('ManageCom').value = <%=strManageCom%>;
		    if(fm.all('ManageCom').value==86){
		    	fm.all('ManageCom').readOnly=false;
		    } else{
		    	fm.all('ManageCom').readOnly=true;
		    }
		    if(fm.all('ManageCom').value!=null){
		    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
		        //显示代码选择中文
		        if (arrResult != null){
		        	fm.all('ManageComName').value=arrResult[0][0];
		        } 
		    }
		}catch(ex){
			alert("在MonAuditingDetailsFormInit.jsp-->IninInpBox函数中发生异常 ：初始化界面错误！");
		}
	}
	function initForm(){
		try{
			initElementtype();
			initInpBox();
			initMonAudGrid();
			showAllCodeName();
		}catch(re){
			alert("MonAuditingDetailsFormInit.jsp-->InitForm函数中发生异常:初始化界面错误");
		}
	}
	function initMonAudGrid(){
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "考核年月";
			iArray[1][1] = "40px";
			iArray[1][2] = 20;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "代理人姓名";
			iArray[2][1] = "40px";
			iArray[2][2] = 30;
			iArray[2][3] = 0;
			
			iArray[3] = new Array();
			iArray[3][0] = "代理人编码";
			iArray[3][1] = "40px";
			iArray[3][2] = 30;
			iArray[3][3] = 0;
			
			iArray[4] = new Array();
			iArray[4][0] = "管理机构";
			iArray[4][1] = "40px";
			iArray[4][2] = 30;
			iArray[4][3] = 0;
			
			iArray[5] = new Array();
			iArray[5][0] = "机构名称";
			iArray[5][1] = "40px";
			iArray[5][2] = 30;
			iArray[5][3] = 0;
			
			iArray[6] = new Array();
			iArray[6][0] = "方案类别";
			iArray[6][1] = "40px";
			iArray[6][2] = 30;
			iArray[6][3] = 0;
			
			iArray[7] = new Array();
			iArray[7][0] = "业务日期";
			iArray[7][1] = "40px";
			iArray[7][2] = 30;
			iArray[7][3] = 0;
			
			iArray[8] = new Array();
			iArray[8][0] = "保单号";
			iArray[8][1] = "40px";
			iArray[8][2] = 30;
			iArray[8][3] = 0;
			
			iArray[9] = new Array();
			iArray[9][0] = "险种号";
			iArray[9][1] = "40px";
			iArray[9][2] = 30;
			iArray[9][3] = 0;
			
			iArray[10] = new Array();
			iArray[10][0] = "FYC";
			iArray[10][1] = "40px";
			iArray[10][2] = 30;
			iArray[10][3] = 0;
			
			iArray[11] = new Array();
			iArray[11][0] = "是否合格人力";
			iArray[11][1] = "50px";
			iArray[11][2] = 30;
			iArray[11][3] = 0;
			
			iArray[12] = new Array();
			iArray[12][0] = "是否有效人力";
			iArray[12][1] = "50px";
			iArray[12][2] = 30;
			iArray[12][3] = 0;
			
			MonAudGrid = new MulLineEnter("fm","MonAudGrid");
		     
			MonAudGrid.mulLineCount = 10;   
			MonAudGrid.displayTitle = 1;
			MonAudGrid.canSel = 1;
			MonAudGrid.canChk = 0;
			MonAudGrid.locked = 1;
			MonAudGrid.hiddenSubtraction = 1;
			MonAudGrid.hiddenPlus = 1;
			MonAudGrid.selBoxEventFuncName ="";
		     
			MonAudGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>