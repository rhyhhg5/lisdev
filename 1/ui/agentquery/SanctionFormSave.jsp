<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输入参数
	String tRadio[] = request.getParameterValues("InpSanctionGridSel");					//序号
	String tManageCom[] = request.getParameterValues("SanctionGrid1");					//管理机构	
	String tManageName[] = request.getParameterValues("SanctionGrid2");					//机构名称
	String tPlanType[] = request.getParameterValues("SanctionGrid3");					//方案类别(A/B)
	String tAddValidHumanPassedRate[] = request.getParameterValues("SanctionGrid4");	//年度净增有效人力达成奖
	String tProFirmPromoteCost[] = request.getParameterValues("SanctionGrid5");			//省公司组织推动费用	
	String tProFirmBonus[] = request.getParameterValues("SanctionGrid6");				//省公司奖金
	String tCentralBranchFirmBonus [] = request.getParameterValues("SanctionGrid7");	//中心支公司奖金				
	String tTopManagerBonus[] = request.getParameterValues("SanctionGrid8");			//总经理奖金
	String tContTopManagerBonus[] = request.getParameterValues("SanctionGrid9");		//分管个险总经理奖金
	String tContPrincipalBonus[] = request.getParameterValues("SanctionGrid");		//个人保险部负责人奖金
	String tYear2015GainCost[] = request.getParameterValues("SanctionGrid");			//2015年获得人力发展费用总额
	
	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
		
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
		
	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();
	
	for (int i=0; i< tRadio.length;i++){
		System.out.println("tManageCom["+i+"]: "+tManageCom[i]);
		System.out.println("tManageName["+i+"]: "+tManageName[i]);
		System.out.println("tPlanType["+i+"]: "+tPlanType[i]);
		System.out.println("tAddValidHumanPassedRate["+i+"]: "+tAddValidHumanPassedRate[i]);
		System.out.println("tProFirmPromoteCost["+i+"]: "+tProFirmPromoteCost[i]);
		System.out.println("tProFirmBonus["+i+"]: "+tProFirmBonus[i]);
		System.out.println("tCentralBranchFirmBonus["+i+"]: "+tCentralBranchFirmBonus[i]);
		System.out.println("tTopManagerBonus["+i+"]: "+tTopManagerBonus[i]);
		System.out.println("tContTopManagerBonus["+i+"]: "+tContTopManagerBonus[i]);
		System.out.println("tContPrincipalBonus["+i+"]: "+tContPrincipalBonus[i]);
		System.out.println("tYear2015GainCost["+i+"]: "+tYear2015GainCost[i]);
	}
%>

<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>