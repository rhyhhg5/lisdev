<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：GoodStartDetailsInput.jsp
//程序功能：
//创建日期：2015-02-11
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<html>
<%
	String tFlag = "";
  	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	//tFlag = request.getParameter("type"); 
%>
<script>	         
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	<%--var type = "<%=tFlag%>";--%>
</script>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>开门红新增合格人力明细报表</title>
  <SCRIPT src="GoodStartDetailsInput.js"></SCRIPT>
  <%@include file="GoodStartDetailsInit.jsp"%>
</head>
<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGoodStart);">
				</td>
				<td class= titleImg>查询条件</td>
			</tr>
		</table>
		<div id="divGoodStart" style="display:''">
			<table class=common>
				<tr class=commom>
					<td class=title>管理机构</td>
					<td class=input>
						<input class=codeNo name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode123',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode123',[this,ManageComName],[0,1],null,null,null,1);" ><input class=codename name=ManageComName readonly=true elementtype=nacessary >
					</td>
					<td class=title>代理人编码</td>
					<td class=input >
						<input name=Groupagentcode>
					</td>
				</tr>
			</table>
		</div>
		<input class=common type=hidden name=querySql>
		<input class=common type=hidden name=year value =<%=PubFun.getCurrentDate().substring(0,4) %>>
		<input type=button value=" 查  询 " class="cssButton" onclick="query();">
		<input type=button value=" 下  载 " class="cssButton" onclick="download();">
		<hr/>
		<div>
			<table class= common>
       			<tr class= common>
      	  			<td text-align: left colSpan=1>
  						<span id="spanGoodStartGrid" ></span> 
  			  		</td>
  				</tr>
    		</table>
    		<table align=left>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
