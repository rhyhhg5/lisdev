
var turnPage = new turnPageClass();
var arrDataSet1;

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	var strAgentgroup=fm.all('Agentgroup').value;
	var tResultSet;
	var tStr;
	var tQueryMonth1;
	var tQueryMonth2;
	var tQueryMonth = fm.all('StartMonth').value ;
	if(tQueryMonth.length==0){alert("请输入月份！");return false;}
	
	if(tQueryMonth.length>=9)
		tQueryMonth2=tQueryMonth1=tQueryMonth;
	else
	{
		tQueryMonth1 = tQueryMonth +"-01";
		tQueryMonth2 = tQueryMonth +"-31";
	}
	
	if (strAgentgroup!="")
	{
	// 书写SQL语句
	var strSQL = "";
	/*查询管理机构*/
	strSQL = "select distinct(ManageCom) from LACommision where branchcode='"+strAgentgroup+"'";
	tResultSet=easyQueryVer3(strSQL,1,1,1);
//	alert(tResultSet);
	
	/*查询总单数，交易金额，直接佣金*/
	strSQL = "select count(PolNo),sum(TransMoney) ,sum(DirectWage) from LACommision  where 1=1 and branchcode='"
		+strAgentgroup+"'"
		+ " and TO_CHAR(tconfdate,'YYYY-MM-DD') >= '"+tQueryMonth1 
	         + "' and TO_CHAR(tconfdate,'YYYY-MM-DD') <= '"+tQueryMonth2 +"'";  
//		+ getWherePart('TPayDate','StartMonth','>=')
//	        + getWherePart('TPayDate','EndMonth','<=');
//	        + " group by AgentCode,Name,RiskCode "
//	        + " order by RiskCode";
	tStr=easyQueryVer3(strSQL, 1, 1, 1);
//	alert(tStr+"&"+tStr.substring(5,tStr.length));
	turnPage.strQueryResult  = tResultSet+"|"+tStr.substring(4,tStr.length);  
//  	turnPage.strQueryResult  = "0|8^0"+"|"+tStr.substring(4,tStr.length); 
  	/*查询新保单数等*/
  	strSQL = "select count(PolNo),sum(TransMoney) from lacommision where 1=1 and branchcode='"+strAgentgroup+"' and paycount='0'"
		+ " and TO_CHAR(tconfdate,'YYYY-MM-DD') >= '"+tQueryMonth1 
	        + "' and TO_CHAR(tconfdate,'YYYY-MM-DD') <= '"+tQueryMonth2 +"'"  ;
//  		+ getWherePart('TPayDate','StartMonth','>=')
//	        + getWherePart('TPayDate','EndMonth','<=');
	tStr=easyQueryVer3(strSQL, 1, 1, 1);
		
	turnPage.strQueryResult=turnPage.strQueryResult+"|"+tStr.substring(4,tStr.length);
	/*查询续保单数等*/
	strSQL = "select count(PolNo),sum(TransMoney) from lacommision where 1=1 and branchcode='"+strAgentgroup+"' and paycount!='0'"
		+ " and TO_CHAR(tconfdate,'YYYY-MM-DD') >= '"+tQueryMonth1 
	        + "' and TO_CHAR(tconfdate,'YYYY-MM-DD') <= '"+tQueryMonth2 +"'";  

//  		+ getWherePart('TPayDate','StartMonth','>=')
//	        + getWherePart('TPayDate','EndMonth','<=');
	tStr=easyQueryVer3(strSQL, 1, 1, 1);
	turnPage.strQueryResult=turnPage.strQueryResult+"|"+tStr.substring(4,tStr.length);
//	alert("will show:"+turnPage.strQueryResult);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,4,6,2,5,7,3]);
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.arrDataCacheSet = chooseArray(tArr,[0,1,2,3,4,5]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = commisionGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  }
}

function initagent()
{
	var strAgentgroup=fm.all('Agentgroup').value;
	var tQueryMonth1;
	var tQueryMonth2;
	var tQueryMonth = fm.all('StartMonth').value ;
	if(tQueryMonth.length==0){alert("请输入月份！");return false;}
	
	if(tQueryMonth.length>=9)
		tQueryMonth2=tQueryMonth1=tQueryMonth;
	else
	{
		tQueryMonth1 = tQueryMonth +"-01";
		tQueryMonth2 = tQueryMonth +"-31";
	}
	strSQL = "select a.agentcode,a.name,count(b.PolNo),sum(b.TransMoney),sum(b.DirectWage) from laagent a,lacommision b where 1=1 and b.agentcode=a.agentcode and a.branchcode='"+strAgentgroup+"' "
 	+ " and TO_CHAR(b.tconfdate,'YYYY-MM-DD') >= '"+tQueryMonth1 
	+ "' and TO_CHAR(b.tconfdate,'YYYY-MM-DD') <= '"+tQueryMonth2 +"' group by a.agentcode,a.name"  
 	tStr=easyQueryVer3(strSQL, 1, 1, 1);
	turnPage.strQueryResult=tStr;
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有该组成员业务统计信息！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(arrDataSet1,[0,1,2,3,4]);
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.arrDataCacheSet = chooseArray(tArr,[0,1,2,3,4,5]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = WageGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function selectItem(spanID,parm2)
{
  var arrSelect = new Array();
  arrSelect = getQueryResult1();
  var agentgroup=arrSelect[0][0];
  fm.all('agentcode').value=agentgroup;
  alert(agentgroup);
  Window.open('./AgentMonthQueryInput.jsp');
}

function getQueryResult1()
{
  var arrSelected = null;
  tRow = WageGrid.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet1 == null )
    return arrSelected;
  arrSelected = new Array();
  tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
  //设置需要返回的数组
  arrSelected[0] = arrDataSet1[tRow-1];
  return arrSelected;
}