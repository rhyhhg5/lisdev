var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询按钮
function query(){
	if(!verifyInput2()){
		return false;
	}
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" ){
		alert("管理机构必须填写！");
		return false;
	}
	if(fm.AssessYearMonth.value<201601||fm.AssessYearMonth.value>201612){
		alert("考核年月只能2016年以内的");
		return false;
		}
	var strSQL = "select a.year||a.month,b.name,b.groupagentcode,a.mngcom, " +
			" (select name from ldcom where comcode=a.mngcom) " +
			" from LAhumanDesc a,laagent b " +
			" where a.agentcode=b.agentcode and a.year=substr("+fm.AssessYearMonth.value+",1,4) and a.month=substr("+fm.AssessYearMonth.value+",5,2)" +
			" and a.mngcom like '"+fm.ManageCom.value+"%' ";
	turnPage.queryModal(strSQL,IncumbencyGrid);

}
//download
function download(){
	if(!verifyInput2()){
		return false;
	}
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" ){
		alert("管理机构必须填写！");
		return false;
	}
	if(fm.AssessYearMonth.value<201601||fm.AssessYearMonth.value>201612){
		alert("考核年月只能2016年以内的");
		return false;
		}
	var strSQL = "select a.year||a.month,b.name,b.groupagentcode,a.mngcom, " +
			" (select name from ldcom where comcode=a.mngcom) " +
			" from LAhumanDesc a,laagent b " +
			" where a.agentcode=b.agentcode and a.year=substr("+fm.AssessYearMonth.value+",1,4) and a.month=substr("+fm.AssessYearMonth.value+",5,2)" +
			" and a.mngcom like '"+fm.ManageCom.value+"%' ";
	fm.querySql.value = strSQL;
	var tArr = easyExecSql(strSQL);

	if(tArr!=null){
		var formAction = fm.action;
		fm.action = "IncumbencyPeoplesFormSave.jsp";
		fm.submit();
		fm.target = "fraSubmit";
		fm.action = formAction;
	}else{
		alert("没有数据");
		return;
	}
}