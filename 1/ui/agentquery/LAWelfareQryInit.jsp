<%
//程序名称：LAIndexInfoQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 10:07
//创建人  ：方波
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('AgentCode').value = '<%=tG.Operator%>'; 
    fm.all('IndexCalNo').value = '';
       
  }
  catch(ex)
  {
    alert("在LAWelfareQryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

                                  

function initForm()
{
  try
  {

    initInpBox();
    initWelfareGrid();
	  
  }
  catch(re)
  {
    alert("LAWelfareQryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}



function initWelfareGrid()
{                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名


 		iArray[1]=new Array();
        iArray[1][0]="年月代码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
 		
        iArray[2]=new Array();
        iArray[2][0]="福利类别";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="福利发放类型";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="服务年限";         //列名
        iArray[4][1]="90px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="福利金额";         //列名
        iArray[5][1]="90px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="福利个人提供金额";         //列名
        iArray[6][1]="120px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="福利集体提供金额";         //列名
        iArray[7][1]="120px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="福利保障";         //列名
        iArray[8][1]="80px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="福利生效日期";         //列名
        iArray[9][1]="100px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[10]=new Array();
        iArray[10][0]="福利发放日期";         //列名
        iArray[10][1]="100px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[11]=new Array();
        iArray[11][0]="批注";         //列名
        iArray[11][1]="200px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许

        WelfareGrid = new MulLineEnter( "fm" , "WelfareGrid" ); 

        //这些属性必须在loadMulLine前
        WelfareGrid.mulLineCount = 3;   
        WelfareGrid.displayTitle = 1;
        WelfareGrid.hiddenPlus=1;
        WelfareGrid.hiddenSubtraction=1;                
        
        WelfareGrid.loadMulLine(iArray);  
	
      }
      catch(ex)
      {
        alert("初始化WelfareGrid时出错："+ ex);
      }
}

</script>