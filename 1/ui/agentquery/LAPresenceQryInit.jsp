<%
//程序名称：LAPresenceQryInit.jsp
//程序功能：
//创建日期：2003-10-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                            
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {    
    fm.all('AgentCode').value = <%=tG.Operator%>;    
  }
  catch(ex)
  {
    alert("在LAPresenceQryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

                               

function initForm()
{
  try
  {
    initInpBox();
    initPresenceGrid();
  }
  catch(re)
  {
    alert("LAPresenceQryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化PresenceGrid
 ************************************************************
 */
function initPresenceGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

                    
        iArray[1]=new Array();
        iArray[1][0]="考勤执行次数";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="考勤类别";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="金额（元）";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[4]=new Array();
        iArray[4][0]="执行日期";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
               
        iArray[5]=new Array();
        iArray[5][0]="批注";         //列名
        iArray[5][1]="400px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
                     
        PresenceGrid = new MulLineEnter( "fm" , "PresenceGrid" ); 

        //这些属性必须在loadMulLine前
        PresenceGrid.mulLineCount = 3;   
        PresenceGrid.displayTitle = 1;
        
        PresenceGrid.locked=1;
		PresenceGrid.hiddenPlus=1;
		PresenceGrid.hiddenSubtraction=1;
		
        PresenceGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化PresenceGrid时出错："+ ex);
      }
    }


</script>