var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

function afterCodeSelect( cCodeName, Field ){
	if(cCodeName=="ManageComHierarchy"){
		if(fm.ManageComHierarchy.value=="总公司"||fm.ManageComHierarchy.value=="三级机构"){
			divCare.style.display = 'none';	
			divCare1.style.display = '';	
			}
		if(fm.ManageComHierarchy.value=="省公司"){
			divCare.style.display ='';
			divCare1.style.display = 'none';	
			}
	}
}
function download(){
	var strSQL = "";
	if(fm.ManageComHierarchy.value == null||fm.AssessYearMonth.value == null||fm.ManageComHierarchy.value == ""||fm.AssessYearMonth.value == "" ){
		alert("管理机构层级和考核年月不能为空");
		return false;
	}
	if(fm.AssessYearMonth.value<201601||fm.AssessYearMonth.value>201612){
		alert("考核年月只能2016年以内的");
		return false;
	}
	var lenh = "";
	var sumlenh = "";
	var len = fm.all('ManageCom').value.length;
	if(len=="2"){
	if(fm.ManageComHierarchy.value=="总公司"){
		lenh = "2";
		sumlenh="2";
	}
	if(fm.ManageComHierarchy.value=="省公司"){
		lenh = "4";
		sumlenh="2";
	}
	if(fm.ManageComHierarchy.value=="三级机构"){
		lenh = "8";
		sumlenh="2";
	}
	}
	 if(len=="4"){
	if(fm.ManageComHierarchy.value=="总公司"){
		alert("省公司机构不能选择总公司进行查询下载！");
		return false; 
	}
	if(fm.ManageComHierarchy.value=="省公司"){
		lenh = "4";
		sumlenh="4";
	}
	if(fm.ManageComHierarchy.value=="三级机构"){
		lenh = "8";
		sumlenh="4";
	}
	}
	if(len=="8"||len=="6"){
	if(fm.ManageComHierarchy.value=="总公司"){
		alert("三级机构不能选择总公司进行查询下载！");
		return false; 
	}
	if(fm.ManageComHierarchy.value=="省公司"){
		alert("三级机构不能选择省公司进行查询下载！");
		return false; 
	}
	if(fm.ManageComHierarchy.value=="三级机构"){
		lenh = "8";
		sumlenh="8";
	}
	}
	var tSQL ="select mngcom,plancode,sum(AveHu) sumAveHu,sum(QCHu) sumQCHu,max(sum(mfyc),(select codename from ldcode1 where codetype='2016humandeveiplan'  and code =plancode )) as YearFYc ," +
	"max(sum(mfyc),(select codename from ldcode1 where codetype='2016humandeveiplan'  and code =plancode )) as specYearFYc ," +
	"(case when sum(AveHu) = 0 then 0  when (cast(sum(qchu) as double )/sum(AveHu)-0.4)>0 then (cast(sum(qchu) as double )/sum(AveHu)-0.4) else 0 end) as speFYCRate," +
	"(select  TMonthHu from lahumandevei c where c.year=substr("+fm.AssessYearMonth.value+",1,4) and c.Month=substr("+fm.AssessYearMonth.value+",5,2) and c.mngcom=a.mngcom) as TMonthHu , " +
	"(select  PlanHu from lahumandevei c where c.year=substr("+fm.AssessYearMonth.value+",1,4) and  c.Month=substr("+fm.AssessYearMonth.value+",5,2) and c.mngcom=a.mngcom) as PlanHu ," +
	"(select  cast(TMonthHu as double)/PlanHu from lahumandevei c where c.year=substr("+fm.AssessYearMonth.value+",1,4) and  c.Month=substr("+fm.AssessYearMonth.value+",5,2) and c.mngcom=a.mngcom) as fycrate, " +
	"nvl((select sum(realpay) from lahumandevei where a.mngcom = mngcom and  year=substr("+fm.AssessYearMonth.value+",1,4) and month>'00' and month <substr("+fm.AssessYearMonth.value+",5,2)),0) as  summoney  from  lahumandevei a " +
	"where year=substr("+fm.AssessYearMonth.value+",1,4) and  month <=substr("+fm.AssessYearMonth.value+",5,2) and mngcom like '"+fm.all('ManageCom').value+"%' and exists (select 1 from lahumandevei b where b.year=substr("+fm.AssessYearMonth.value+",1,4) and   b.Month=substr("+fm.AssessYearMonth.value+",5,2) and b.mngcom=a.mngcom and b.TMonthHu>=b.PlanHu) group by mngcom,plancode  " +
	" union "+
	"select mngcom,plancode,sum(AveHu) sumAveHu,sum(QCHu) sumQCHu,-1 * max(sum(mfyc),(select codename from ldcode1 where codetype='2016humandeveiplan'  and code =plancode )) as YearFYc ," +
	"max(sum(mfyc),(select codename from ldcode1 where codetype='2016humandeveiplan'  and code =plancode )) as specYearFYc ," +
	"0 as speFYCRate," +
	"(select  TMonthHu from lahumandevei c where c.year=substr("+fm.AssessYearMonth.value+",1,4) and  c.Month=substr("+fm.AssessYearMonth.value+",5,2) and c.mngcom=a.mngcom) as TMonthHu , " +
	"(select  PlanHu from lahumandevei c where c.year=substr("+fm.AssessYearMonth.value+",1,4) and  c.Month=substr("+fm.AssessYearMonth.value+",5,2) and c.mngcom=a.mngcom) as PlanHu ," +
	"(select  cast(TMonthHu as double)/PlanHu from lahumandevei c where c.year=substr("+fm.AssessYearMonth.value+",1,4) and  c.Month=substr("+fm.AssessYearMonth.value+",5,2) and c.mngcom=a.mngcom) as fycrate," +
	"nvl((select sum(realpay) from lahumandevei where a.mngcom = mngcom and year=substr("+fm.AssessYearMonth.value+",1,4) and month>'00' and  month <substr("+fm.AssessYearMonth.value+",5,2)),0) as summoney  from  lahumandevei a " +
	"where  year=substr("+fm.AssessYearMonth.value+",1,4) and  month <=substr("+fm.AssessYearMonth.value+",5,2) and mngcom like '"+fm.all('ManageCom').value+"%' and exists (select 1 from lahumandevei b where  b.year=substr("+fm.AssessYearMonth.value+",1,4) and  b.Month=substr("+fm.AssessYearMonth.value+",5,2) and b.mngcom=a.mngcom and b.TMonthHu<b.PlanHu)  group by mngcom,plancode ";
	if(lenh=="4"){
	strSQL ="select a,b,c,d,e,f,g,h,i,j,k,l,"+ 
			"to_char(case when mod('"+fm.AssessYearMonth.value+"',2)=0 then round(zsumShouldFYC-(min(sumShouldFYC,arrears)-min(max(arrears-(sumShouldFYC-tmonthShouldFYC),0),tmonthShouldFYC)),2) else  round(zsumShouldFYC-min(sumShouldFYC,arrears),2) end), "+
			"arrears," +
			"to_char(case when mod('"+fm.AssessYearMonth.value+"',2)=0 then cast(round(l-(zsumShouldFYC-(min(sumShouldFYC,arrears)-min(max(arrears-(sumShouldFYC-tmonthShouldFYC),0),tmonthShouldFYC)))-arrears,2) as decimal(12,2))  else   cast(round(l-(zsumShouldFYC-min(sumShouldFYC,arrears))-arrears,2) as decimal(12,2)) end)"+
			"from ( "+
			"select managecom a,(select name from ldcom where comcode=managecom) b,plancode c,sum(sumAveHu) d,sum(sumQCHu) e,cast((case when sum(sumAveHu)>0 then  round(cast(sum(sumQCHu) as decimal(12,5))*100/sum(sumAveHu),4)  else 0 end) as decimal(12,2))||'%' f," +
			"sum(TMonthHu) g,sum(PlanHu) h,cast(round(cast(sum(TMonthHu) as decimal(12,5))*100/sum(PlanHu),4) as decimal(12,2))||'%' i," +
			"cast(round(sum(YearFYc),2) as decimal(12,2)) j,cast(round(sum(speFYC),2) as decimal(12,2)) k,cast(round((sum(YearFYc)+sum(speFYC)),2) as decimal(12,2)) l," +			
			"(select realpay from lahumandevei where year=substr("+fm.AssessYearMonth.value+",1,4) and month='00' and mngcom=substr(bb.managecom,1,4)) as  arrears,"+
			"(select coalesce(sum(cast(b.remark3 as decimal(10,2))),0) from  LAhumandevei b where substr(b.mngcom,1,'"+lenh+"') =substr(bb.managecom,1,'"+lenh+"') and b.year='2016' and mod(int(b.Remark1),2)=0 and b.Remark1 <='"+fm.AssessYearMonth.value+"') as sumShouldFYC," +
			"(select coalesce(sum(cast(b.remark3 as decimal(10,2))),0) from  LAhumandevei b where substr(b.mngcom,1,'"+lenh+"') =substr(bb.managecom,1,'"+lenh+"') and b.year='2016' and b.Remark1 < '"+fm.AssessYearMonth.value+"') as zsumShouldFYC," +
			"(select coalesce(sum(cast(b.remark3 as decimal(10,2))),0) from  LAhumandevei b where substr(b.mngcom,1,'"+lenh+"') =substr(bb.managecom,1,'"+lenh+"') and b.year='2016' and b.Remark1 = '"+fm.AssessYearMonth.value+"') as tmonthShouldFYC" +			
			" from (select substr(mngcom,1,"+lenh+") as managecom,(case when "+lenh+"=8 then substr(aa.plancode,1,1) else '' end) as plancode ," +
			"sumAveHu,sumQCHu,TMonthHu,PlanHu,(case when  (sumAveHu>0 and  ( (substr(plancode,1,1)='A'  and (cast(sumQCHu as double )/sumAveHu-0.2)>=0) or (substr(plancode,1,1)='B'  and (cast(sumQCHu as double )/sumAveHu-0.25)>=0))) then specYearFYc*min(1,max(cast(TMonthHu as double)/PlanHu,0)) else 0 end ) as YearFYc,cast(specYearFYc as double)*cast(speFYCRate as double) as speFYC," +
			"(case when "+lenh+"=8 then (cast(YearFYc as double)+cast(YearFYc as double)*cast(speFYCRate as double))*0.048 else (cast(YearFYc as double)+cast(YearFYc as double)*cast(speFYCRate as double))*0.1 end )as rewardFYC,summoney, " +
			"case when "+lenh+"=8 then cast(round(((YearFYc+speFYCRate*YearFYc)*1.08),2) as decimal(12,2)) else cast(round(((YearFYc+speFYCRate*YearFYc)*1.1),2) as decimal(12,2)) end as allFYC "+
			"from ("+tSQL+") aa) bb group by bb.managecom,bb.plancode) cc" +
			" union " +
			"select '合计','','',sum(sumAveHu),sum(sumQCHu),cast((case when sum(sumAveHu)>0 then  round(cast(sum(sumQCHu) as decimal(12,5))*100/sum(sumAveHu),4)  else 0 end) as decimal(12,2))||'%'," +
			"sum(TMonthHu),sum(PlanHu),cast(round(cast(sum(TMonthHu) as decimal(12,5))*100/sum(PlanHu),4) as decimal(12,2))||'%'," +
			"cast(round(sum(YearFYc),2) as decimal(12,2)),cast(round(sum(speFYC),2) as decimal(12,2)),cast(round((sum(YearFYc)+sum(speFYC)),2) as decimal(12,2)),'',(select sum(realpay) from lahumandevei where year=substr("+fm.AssessYearMonth.value+",1,4) and month='00' and mngcom in(select substr(mngcom,1,4) from lahumandevei b where b.year =substr("+fm.AssessYearMonth.value+",1,4) and b.Month=substr("+fm.AssessYearMonth.value+",5,2))) as  arrears,'' " +
			"from (select substr(mngcom,1,"+sumlenh+") as managecom,(case when "+sumlenh+"=8 then substr(aa.plancode,1,1) else '' end) as plancode ," +
			"sumAveHu,sumQCHu,TMonthHu,PlanHu,(case when  (sumAveHu>0 and  ( (substr(plancode,1,1)='A'  and (cast(sumQCHu as double )/sumAveHu-0.2)>=0) or (substr(plancode,1,1)='B'  and (cast(sumQCHu as double )/sumAveHu-0.25)>=0))) then specYearFYc*min(1,max(cast(TMonthHu as double)/PlanHu,0)) else 0 end ) as YearFYc,cast(specYearFYc as double)*cast(speFYCRate as double) as speFYC," +
			"(case when "+sumlenh+"=8 then (cast(YearFYc as double)+cast(YearFYc as double)*cast(speFYCRate as double))*0.048 else (cast(YearFYc as double)+cast(YearFYc as double)*cast(speFYCRate as double))*0.1 end )as rewardFYC,summoney, " +
			"case when "+sumlenh+"=8 then cast(round(((YearFYc+speFYCRate*YearFYc)*1.08),2) as decimal(12,2)) else cast(round(((YearFYc+speFYCRate*YearFYc)*1.1),2) as decimal(12,2)) end as allFYC "+
			"from ("+tSQL+") aa) bb with ur " ;
	}
	else{
		strSQL ="select managecom,(select name from ldcom where comcode =managecom),plancode,sum(sumAveHu),sum(sumQCHu),cast((case when sum(sumAveHu)>0 then  round(cast(sum(sumQCHu) as decimal(12,5))*100/sum(sumAveHu),4)  else 0 end) as decimal(12,2))||'%'," +
		"sum(TMonthHu),sum(PlanHu),cast(round(cast(sum(TMonthHu) as decimal(12,5))*100/sum(PlanHu),4) as decimal(12,2))||'%'," +
		"cast(round(sum(YearFYc),2) as decimal(12,2)),cast(round(sum(speFYC),2) as decimal(12,2)),cast(round((sum(YearFYc)+sum(speFYC)),2) as decimal(12,2)),cast(round(sum(summoney),2) as decimal(12,2)),cast(round((sum(YearFYc)+sum(speFYC)-sum(summoney)),2) as decimal(12,2))" +
		"from (select substr(mngcom,1,"+lenh+") as managecom,(case when "+lenh+"=8 then substr(aa.plancode,1,1) else '' end) as plancode ," +
		"sumAveHu,sumQCHu,TMonthHu,PlanHu,(case when  (sumAveHu>0 and  ( (substr(plancode,1,1)='A'  and (cast(sumQCHu as double )/sumAveHu-0.2)>=0) or (substr(plancode,1,1)='B'  and (cast(sumQCHu as double )/sumAveHu-0.25)>=0))) then specYearFYc*min(1,max(cast(TMonthHu as double)/PlanHu,0)) else 0 end ) as YearFYc,cast(specYearFYc as double)*cast(speFYCRate as double) as speFYC," +
		"(case when "+lenh+"=8 then (cast(YearFYc as double)+cast(YearFYc as double)*cast(speFYCRate as double))*0.048 else (cast(YearFYc as double)+cast(YearFYc as double)*cast(speFYCRate as double))*0.1 end )as rewardFYC,summoney, " +
		"case when "+lenh+"=8 then cast(round(((YearFYc+speFYCRate*YearFYc)*1.08),2) as decimal(12,2)) else cast(round(((YearFYc+speFYCRate*YearFYc)*1.1),2) as decimal(12,2)) end as allFYC "+
		"from ("+tSQL+") aa) bb group by bb.managecom,bb.plancode" +
		" union " +
		"select '合计','','',sum(sumAveHu),sum(sumQCHu),cast((case when sum(sumAveHu)>0 then  round(cast(sum(sumQCHu) as decimal(12,5))*100/sum(sumAveHu),4)  else 0 end) as decimal(12,2))||'%'," +
		"sum(TMonthHu),sum(PlanHu),cast(round(cast(sum(TMonthHu) as decimal(12,5))*100/sum(PlanHu),4) as decimal(12,2))||'%'," +
		"cast(round(sum(YearFYc),2) as decimal(12,2)),cast(round(sum(speFYC),2) as decimal(12,2)),cast(round((sum(YearFYc)+sum(speFYC)),2) as decimal(12,2)),cast(round(sum(summoney),2) as decimal(12,2)),cast(round((sum(YearFYc)+sum(speFYC)-sum(summoney)),2) as decimal(12,2))" +
		"from (select substr(mngcom,1,"+sumlenh+") as managecom,(case when "+sumlenh+"=8 then substr(aa.plancode,1,1) else '' end) as plancode ," +
		"sumAveHu,sumQCHu,TMonthHu,PlanHu,(case when  (sumAveHu>0 and  ( (substr(plancode,1,1)='A'  and (cast(sumQCHu as double )/sumAveHu-0.2)>=0) or (substr(plancode,1,1)='B'  and (cast(sumQCHu as double )/sumAveHu-0.25)>=0))) then specYearFYc*min(1,max(cast(TMonthHu as double)/PlanHu,0)) else 0 end ) as YearFYc,cast(specYearFYc as double)*cast(speFYCRate as double) as speFYC," +
		"(case when "+sumlenh+"=8 then (cast(YearFYc as double)+cast(YearFYc as double)*cast(speFYCRate as double))*0.048 else (cast(YearFYc as double)+cast(YearFYc as double)*cast(speFYCRate as double))*0.1 end )as rewardFYC,summoney, " +
		"case when "+sumlenh+"=8 then cast(round(((YearFYc+speFYCRate*YearFYc)*1.08),2) as decimal(12,2)) else cast(round(((YearFYc+speFYCRate*YearFYc)*1.1),2) as decimal(12,2)) end as allFYC "+
		"from ("+tSQL+") aa) bb with ur " ;
	}
	fm.lenh.value = lenh;
	fm.querySql.value = strSQL;
	if(fm.querySql.value != null && fm.querySql.value != ""){
		var formAction = fm.action;
        fm.action = "YearlyAuditingFormDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }else{
        alert("请先执行查询操作，再打印！");
        return ;
    }
}