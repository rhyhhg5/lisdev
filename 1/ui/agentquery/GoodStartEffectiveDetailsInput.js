var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var queryFlag = false;
function query(){
	
	if(!verifyInput()){
		return false;
	}
	var flag = fm.Flag.value;
	var managecom = fm.ManageCom.value;
	var tWorkSQL ="(select  agentcode  from lahumandesc where year ='20170331' and month ='20170331' and mngcom like '"+managecom+"%')";
	var tCurrentMonth = getCurrentDate("-");
	var tWhereSQL=" and wageno between '201701' and '201703' ";
	var tOperaDate ="2017-3-31";
	if("2"==flag)
	{
		tOperaDate = "2016-12-1";
	}
	fm.tFlag.value ="1";
	if(compareDate(tOperaDate,tCurrentMonth)<2)
	{
		fm.tFlag.value ="2";
		tWorkSQL ="(select a.Agentcode from laagent a,LADimission f "
			+ "where a.agentcode=f.agentcode and a.BranchType='1' and a.branchtype2='01'  " 
			+" and a.managecom like '"+managecom+"%' and a.AgentState='02' and a.employdate between '2016-12-1' and '2017-3-31' "
			+ "group by a.agentcode "
			+ "having  min(days(a.employdate)-days(f.DepartDate)) >= '181' "
			+ "union "
			+ "select a.Agentcode from laagent a "
			+ "where a.BranchType='1' and a.branchtype2='01' and a.managecom like '"+managecom+"%' and  a.AgentState='01'  " 
			+ " and a.employdate between '2016-12-1' and '2017-3-31') ";
		tWhereSQL =" and tmakedate between '2017-1-1' and '2017-3-31' ";
	}
	var strSQL=" select wageno,db2inst1.getunitecode(agentcode),(select name from laagent where laagent.agentcode = lacommision.agentcode),managecom,(select name from ldcom where comcode = managecom),tmakedate,(case when grpcontno ='00000000000000000000' then contno else grpcontno end),riskcode,fyc," 
			+" (case when sum(fyc) over (partition by agentcode)>=300 then '是' else '否' end) from lacommision where branchtype='1' and branchtype2='01' and payyear=0 and renewcount = 0" 
		 	+" and agentcode in "+tWorkSQL+ tWhereSQL
		 	+ getWherePart("db2inst1.getunitecode(agentcode)","Groupagentcode")
	 		+"  with ur";
	
	turnPage.queryModal(strSQL,GoodStartGrid);
	fm.querySql.value = strSQL;
	queryFlag= true;
}

function download(){
	if(!queryFlag)
	{
		alert("下载前请先进行【查询】操作！");
		return false;
	}
	if(!verifyInput2()){
		return false;
	}

	if(fm.querySql.value!=null && fm.querySql.value!=""){
		var formAction = fm.action;
		fm.action = "GoodStartEffectiveDetailsDownload.jsp";
		fm.submit();
		fm.target = "fraSubmit";
		fm.action = formAction;
	}else{
		alert("没有数据");
		return;
	}
}
function afterCodeSelect( cCodeName, Field )
{
	if("comcode123"==cCodeName||"flag"==cCodeName)
	{
		initExplainGrid();
		return false;
	}
	
}