<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：AgentQuery.jsp
//程序功能：
//创建日期：2003-5-14 
//创建人  ：zy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<script> 	
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="AgentQuery.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="AgentQueryInit.jsp"%>
	<%@include file="../agent/SetBranchType.jsp"%>
	
	<title>人员清单</title>
</head>

<body  onload="initForm();" >
  <form  name=fm >
  <table>
    	<tr>
    	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    	</td>
			<td class= titleImg>
				查询条件
			</td>
		</tr>
	</table>
	<Div  id= "divLDPerson1" style= "display: ''">
    <table  class= common align=center>
      	<TR  class= common>
					<TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=BranchAttr verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
          </TD>
          <TD  class= title>
            职级
          </TD>
          <TD  class= input>
            <Input name=AgentGrade class="code" ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" > 
          </TD>
        </TR>
	</table>
        <tr>		
					<td>
						 <!--INPUT class=common VALUE="人员信息查询" TYPE=button onclick="easyQueryClick();"--> 
						 <INPUT class=common VALUE="查询" TYPE=button onclick="easyQueryClick();">
					</td>
		   </tr>
  </Div>  
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		<td class= titleImg>
    			 人员信息
    		</td>
    	</tr>
    </table>
    <input type=hidden name=BranchType>
  	<Div  id= "divAgent1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT class=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


