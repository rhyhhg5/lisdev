<%
//程序名称：BKBranchQueryInit.jsp
//程序功能：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">

// 输入框的初始化（单记录部分）

function afterQuery( arrQueryResult )
{
  try
  {   
  var arrResult = new Array();	
  if( arrQueryResult != null )
	{
	  arrResult = arrQueryResult;                            
		
		fm.all('AgentCom').value = arrResult[0][0];
		fm.all('Name').value = arrResult[0][1];  
    fm.all('UpAgentCom').value = arrResult[0][2]; 
    fm.all('UpComName').value = arrResult[0][3]; 
    fm.all('Address').value = arrResult[0][4];                                                                                   
    fm.all('Phone').value = arrResult[0][5]; 
    fm.all('LinkMan').value = arrResult[0][6];
    fm.all('GrpNature').value = arrResult[0][7];
    fm.all('BankType').value = arrResult[0][8];
    fm.all('SellFlag').value = arrResult[0][9];
    fm.all('AreaType').value = arrResult[0][10];
    fm.all('ChannelType').value = arrResult[0][11];
    fm.all('ChangeCom').value =arrResult[0][12];
    fm.all('ACType').value = arrResult[0][13];
    fm.all('SiteManagerCode').value=arrResult[0][14];
    fm.all('SiteManager').value=arrResult[0][15];
    fm.all('ManageCom').value = arrResult[0][16];
    fm.all('CalFlag').value=arrResult[0][17];  
		fm.all('Operator').value = arrResult[0][18];
		}
  }
  catch(ex)
  {
    alert("在BranchPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {  
   var arrReturn = new Array();  
   arrReturn = getQueryResult();
	 afterQuery( arrReturn );        
  }
  catch(re)
  {
    alert("BranchPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initAgentGrade()
  {                               
    var iArray = new Array();
      
      try
      {
        
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="机构编号";         //列名
        iArray[1][1]="100px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="机构名称";         //列名
        iArray[2][1]="260px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="负责人姓名";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="变动时间";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="操作人";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

       

      AgentGrade = new MulLineEnter( "fm" , "AgentGrade" ); 
      //这些属性必须在loadMulLine前
      AgentGrade.mulLineCount = 10;   
      AgentGrade.displayTitle = 1;
      AgentGrade.locked = 1;
      AgentGrade.canSel = 0;
      AgentGrade.hiddenPlus = 1;
      AgentGrade.hiddenSubtraction = 1;
      AgentGrade.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //AgentGrade.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}





</script>