<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="BankLacom.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BankLacomInit.jsp"%>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>非所辖网点业务查询 </title>  
</head>
<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>
<script>
  Operator = "<%=tGlobalInput.Operator%>";
</script>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		  </tr>
	  </table>
    <table  class= common align=center>
   
      	<TR  class= common>
          <TD class= title> 
          员工编码 
          </TD>
          <TD  class= input> 
           <Input class=common  name=AgentCode >
          </TD>                   
        </TR>
        
        <TR  class= common>
         <TD  class= title>
          分支机构
         </TD>
         <TD  class= input>
          <Input name=BranchAttr class= common >
         </TD>
         <td  class= title> 
           代理机构
         </td>
         <td  class= input> 
         <Input class="code" name=AgentCom  ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);">  
         </td>       
      </TR>
      
      <TR  class= common>       
        <TD  class= title>
          开始日期 
        </TD>
        <TD  class= input> 
          <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="开始日期|notnull"> 
        </TD>
        <TD  class= title>
          结束日期 
        </TD>
        <TD  class= input> 
          <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="结束日期|notnull"> 
        </TD>
      </TR>
    </table>
          <INPUT CLASS=common VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT CLASS=common VALUE="打印信息明细" TYPE=button onclick="showPolDetail();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 查询结果明细
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>			
  	</div>    
  
  <input type=hidden  name=Operate>
  <input type=hidden  name=cAgentCode>
  <input type=hidden  name=cFYC>
  <input type=hidden  name=cGrpFYC>
  <input type=hidden  name=cDepFYC>
  </form>
  <INPUT CLASS=common VALUE="审核确认" TYPE=button onclick="ConfirmComm();"> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
