//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


function queryMark(tquerytype)
{
//alert(tquerytype);
//return false;
 if (verifyInput() == false)
 return false;
 var tManageCom=fm.all('ManageCom').value;
 var tStartDate=fm.all('StartDate').value;
 var tEndDate=fm.all('EndDate').value;
 var Sql0="select enddate from lastatsegment where stattype='5' and yearmonth="+tEndDate+"";
 var strQueryResult0  = easyQueryVer3(Sql0, 1, 0, 1); 
 var arr0 = decodeEasyQueryResult(strQueryResult0);
 tEndDate= arr0[0][0];
 tStartDate=tStartDate.substr(0,4)+"-"+tStartDate.substr(4)+"-01";
 if(tquerytype==1){
 	var Sql1="Select (select  count(distinct appntno) from lcgrpcont where "
       +" signdate between '"+tStartDate+"' and '"+tEndDate+"'"
       +" and paymode='1' and prem>20000 and managecom like '"+tManageCom
       +"%')+( select  count(1) "
       +" from lcgrpcont where signdate between '"+tStartDate+"' and '"+tEndDate+"'"
       +" and paymode='3' and prem>200000 and managecom like '"+tManageCom
       +"%') from dual with ur ";
 	var strQueryResult1  = easyQueryVer3(Sql1, 1, 0, 1); 
 	var arr1 = decodeEasyQueryResult(strQueryResult1);
 	fm.all("AssessMark1").value = arr1[0][0];
 	return true;
 }else if(tquerytype==2){
 	var Sql2="select ( select count(distinct(appntno)) from lpgrpcont where edortype in ('WT','CT') "
        +" and makedate between '"+tStartDate+"' and '"+tEndDate+"'and prem>10000 and managecom like '"+tManageCom
       +"%') +( "
        +" select count(distinct a.appntno) from lcgrpcont a, LJAGetclaim b "
        +" where  a.grpcontno=b.grpcontno and b.pay>10000 "
        +" and b.makedate between '"+tStartDate+"' and '"+tEndDate+"'"
        +" and b.grpcontno<>'00000000000000000000' and b.othernotype='5'and a.managecom like '"+tManageCom
       +"%') from dual with ur ";
 	var strQueryResult2  = easyQueryVer3(Sql2, 1, 0, 1);  
 	var arr2 = decodeEasyQueryResult(strQueryResult2);
 	fm.all("AssessMark2").value = arr2[0][0];
 	return true;
 }else if(tquerytype==3){
 	var Sql3="Select (select  count(distinct(appntno)) from lccont "
        +" where signdate between '"+tStartDate+"' and '"+tEndDate+"'"
        +" and paymode ='1' and prem>20000 and conttype='1' and managecom like '"+tManageCom
        +"%') +( select "
        +" count(distinct(appntno)) from lccont "
        +" where signdate between '"+tStartDate+"' and '"+tEndDate+"'"
        +" and paymode in ('4','11','12') and prem>200000 and conttype='1' and managecom like '"+tManageCom
        +"%')from dual with ur";
 	var strQueryResult3  = easyQueryVer3(Sql3, 1, 0, 1);  
 	var arr3 = decodeEasyQueryResult(strQueryResult3);
 	fm.all("AssessMark3").value = arr3[0][0];
 	return true;
 }else if(tquerytype==4){
 	var Sql4
     ="Select (select count(distinct a.appntno)  from lpcont a, LJAGetEndorse b  "
       +" where a.edortype in ('CT','WT') and a.contno=b.contno "
       +" and b.EndorsementNo=a.EdorNo and b.GetMoney<-10000 "
       +" and b.makedate between '"+tStartDate+"' and '"+tEndDate+"'"
       +" and a.managecom like '"+tManageCom
       +"%')+( select count(distinct a.appntno) from lccont a, LJAGetclaim b "
       +" where  a.contno=b.contno and b.pay>10000 "
       +" and b.makedate between '"+tStartDate+"' and '"+tEndDate+"'"
       +" and b.grpcontno='00000000000000000000' and b.othernotype='5' and a.managecom like '"+tManageCom
       +"%') from dual with ur ";
 	var strQueryResult4  = easyQueryVer3(Sql4, 1, 0, 1);  
 	var arr4 = decodeEasyQueryResult(strQueryResult4);
 	fm.all("AssessMark4").value = arr4[0][0];
 	return true;
 }else if(tquerytype==5){
 	var Sql5="select count(distinct(appntno)) from lpcont where edortype='CM' "
        +" and  makedate between '"+tStartDate+"' and '"+tEndDate+"' and managecom like '"+tManageCom
       +"%' with ur";
 	var strQueryResult5  = easyQueryVer3(Sql5, 1, 0, 1);  
 	var arr5 = decodeEasyQueryResult(strQueryResult5);
 	fm.all("AssessMark5").value = arr5[0][0];
 	return true;
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
//  fm.all('hideIsManager').value = false;
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LABankAgentInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 else 
 {
  	parent.fraMain.rows = "0,0,0,82,*";
 }
}




//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

