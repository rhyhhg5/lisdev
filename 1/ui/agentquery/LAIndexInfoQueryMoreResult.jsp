<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAIndexInfoQueryMoreResult.jsp
//程序功能：
//创建日期：2002-3-19
//创建人  ：方波
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAIndexInfoQueryMoreResult.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAIndexInfoQueryInit.jsp"%>
  <title></title>
</head>
<body onLoad="displayMoreResult();">
  <form action="./LAIndexInfoQuerySubmit.jsp" method=post name=fm target="fraSubmit"><Div  id= "divLAIndexInfo1" style= "display: ''"> 
  <table  class= common>
    <tr> 
      <td width="19%"  class= title><strong>指标计算编码</strong></td>
      <td width="25%"  class= input><input readonly name=IndexCalNo class=readonly> 
      </td>
      <td width="25%"  class= title><strong>指标类型</strong></td>
      <td width="31%"  class= input><input readonly name=IndexType class=readonly> 
      </td>
    </tr>
    <tr  class= common> 
      <td  class= title><strong>代理人编码</strong></td>
      <td  class= input><input readonly name=AgentCode class=readonly> 
      </td>
      <td  class= title>&nbsp;</td>
      <td  class= input>&nbsp;</td>
    </tr>
  </table>
  <br>
  <table>
    <tr> 
      <td class=common> <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAIndexInfoResult1);"> 
      </td>
      <td class= titleImg> 考核指标</td>
    </tr>
  </table>
  <Div  id= "divLAIndexInfoResult1" style= "display: ''">
    <table  class= common>
      <tr> 
        <td  class= title>个人累计FYC</td>
        <td  class= input><input readonly name=IndFYCSum class=readonly> 
        </td>
        <td  class= title>个人月平均FYC</td>
        <td  class= input><input readonly name=MonAvgFYC class=readonly > 
        </td>
      </tr>
      <tr  class= common> 
        <td  class= title>本人及所增人员累计FYC </td>
        <td  class= input><input readonly name=IndAddFYCSum class=readonly> 
        </td>
        <td  class= title>直辖组累计FYC </td>
        <td  class= input><input readonly name=DirTeamFYCSum class=readonly> 
        </td>
      </tr>
      <tr  class= common> 
        <td  class= title>直辖及育成组累计FYC</td>
        <td  class= input><input readonly name=DRFYCSum class=readonly></td>
        <td  class= title>直辖部累计FYC </td>
        <td  class= input><input readonly name=DepFYCSum class=readonly> 
        </td>
      </tr>
      <tr  class= common> 
        <td  class= title>直辖部月均FYC</td>
        <td  class= input><input readonly name=DirDepMonAvgFYC class=readonly></td>
        <td  class= title>FYC 挂零月数</td>
        <td  class= input><input readonly name=FYCZeroMon class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>个人继续率</td>
        <td  class= input><input readonly name=IndRate class=readonly></td>
        <td  class= title>直辖组平均继续率</td>
        <td  class= input><input readonly name=TeamAvgRate class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>直辖及育成组平均继续率</td>
        <td  class= input><input readonly name=DRTeamAvgRate class=readonly></td>
        <td  class= title>直辖部平均继续率</td>
        <td  class= input><input readonly name=DepAvgRate class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>直辖部及育成部平均继续率</td>
        <td  class= input><input readonly name=DRDepAvgRate class=readonly></td>
        <td  class= title>区平均继续率</td>
        <td  class= input><input readonly name=AreaRate class=readonly></td>
      </tr>
      <tr  class= common> 
        <td height="18"  class= title>个人累计客户人数</td>
        <td  class= input><input readonly name=IndCustSum class=readonly></td>
        <td  class= title>月平均客户人数</td>
        <td  class= input><input readonly name=MonAvgCust class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>直接增员人数</td>
        <td  class= input><input readonly name=DirAddCount class=readonly></td>
        <td  class= title>增员人数</td>
        <td  class= input><input readonly name=AddCount class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>增员人员中包括的理财主任人数</td>
        <td  class= input><input readonly name=IncFinaMngCount class=readonly></td>
        <td  class= title>所辖人数</td>
        <td  class= input><input readonly name=MngAgentCount class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>所辖人员中理财主任人数</td>
        <td  class= input><input readonly name=MngFinaCount class=readonly></td>
        <td  class= title>所辖人员中直接推荐人数</td>
        <td  class= input><input readonly name=DirRmdCount class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>直辖人数</td>
        <td  class= input><input readonly name=DirMngCount class=readonly></td>
        <td  class= title>直辖人员中理财主任人数</td>
        <td  class= input><input readonly name=DirMngFinaCount class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>所辖营业组数</td>
        <td  class= input><input readonly name=TeamCount class=readonly></td>
        <td  class= title>直接育成营业组数</td>
        <td  class= input><input readonly name=DRTeamCount class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>合计育成组数</td>
        <td  class= input><input readonly name=RearTeamSum class=readonly></td>
        <td  class= title>直接育成营销服务部个数</td>
        <td  class= input><input readonly name=DRearDepCount class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>育成营销服务部数</td>
        <td  class= input><input readonly name=RearDepCount class=readonly></td>
        <td  class= title>育成营销服务部中直接育成营销服务部数</td>
        <td  class= input><input readonly name=DInRCount class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>所辖营销服务部理财服务主任人数</td>
        <td  class= input><input readonly name=DepFinaCount class=readonly></td>
        <td  class= title>所辖支公司理财服务主任人数</td>
        <td  class= input><input readonly name=BranComFinaCount class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>育成督导长人数</td>
        <td  class= input><input readonly name=RearAdmCount class=readonly></td>
        <td  class= title>直接育成督导长人数</td>
        <td  class= input><input readonly name=DRearAdmCount class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>月均有效人力人数</td>
        <td  class= input><input readonly name=MonAvgLabor class=readonly></td>
        <td  class= title>直辖及育成组月均有效人力人数</td>
        <td  class= input><input readonly name=DRTeamMonLabor class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>直辖部及育成部月均有效人力人数</td>
        <td  class= input><input readonly name=DRDepMonLabor class=readonly></td>
        <td  class= title>&nbsp;</td>
        <td  class= input>&nbsp;</td>
      </tr>
    </table>
  </div>
          <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAIndexInfoResult2);">
    		</td>
    		
      <td class= titleImg> 佣金指标</td>
    	</tr>
    </table>
	
  <Div  id= "divLAIndexInfoResult2" style= "display: ''">
    <table  class= common>
      <tr> 
        <td  class= title>创业扶持金</td>
        <td  class= input><input readonly name=InitPension class=readonly> 
        </td>
        <td  class= title>创业金</td>
        <td  class= input><input readonly name=StartCareerRwd class=readonly> 
        </td>
      </tr>
      <tr  class= common> 
        <td  class= title>转正奖 </td>
        <td  class= input><input readonly name=ToFormalRwd class=readonly> 
        </td>
        <td  class= title>初年度佣金 </td>
        <td  class= input><input readonly name=FirstPension class=readonly> 
        </td>
      </tr>
      <tr  class= common> 
        <td  class= title>续年度佣金</td>
        <td  class= input><input readonly name=ContiuePension class=readonly></td>
        <td  class= title>个人年终奖 </td>
        <td  class= input><input readonly name=PersonBonus class=readonly> 
        </td>
      </tr>
      <tr  class= common> 
        <td  class= title>伯乐奖金</td>
        <td  class= input><input readonly name=WiseBonus class=readonly></td>
        <td  class= title>育才奖金</td>
        <td  class= input><input readonly name=RearBonus class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>增员年终奖</td>
        <td  class= input><input readonly name=AddBonus class=readonly></td>
        <td  class= title>见习业务经理特别津贴</td>
        <td  class= input><input readonly name=SpecialPension class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>职务津贴</td>
        <td  class= input><input readonly name=PosSdy class=readonly></td>
        <td  class= title>直辖组管理津贴</td>
        <td  class= input><input readonly name=TeamDirManaSdy class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>组育成津贴</td>
        <td  class= input><input readonly name=RearedSdy class=readonly></td>
        <td  class= title>小组年终奖金</td>
        <td  class= input><input readonly name=TeamBonus class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>部直辖管理津贴</td>
        <td  class= input><input readonly name=DepDirManaSdy class=readonly></td>
        <td  class= title>增部津贴</td>
        <td  class= input><input readonly name=AddDepSdy class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>营销部年终奖金</td>
        <td  class= input><input readonly name=DepBonus class=readonly></td>
        <td  class= title>督导长职务底薪</td>
        <td  class= input><input readonly name=BaseWage class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>区域督导长职务底薪</td>
        <td  class= input><input readonly name=DisBaseWage class=readonly></td>
        <td  class= title>督导长育成津贴</td>
        <td  class= input><input readonly name=RearAreaSdy class=readonly></td>
      </tr>
      <tr  class= common> 
        <td  class= title>补发初年度佣金</td>
        <td  class= input><input readonly name=AddFYC class=readonly></td>
        <td  class= title>&nbsp;</td>
        <td  class= input>&nbsp;</td>
      </tr>
    </table>
  </div>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanIndexInfoGrid1" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
	      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
