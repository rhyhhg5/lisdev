/** 
 * 程序名称：LAContInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-20 18:05:58
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick()
{
    if (!verifyInput())
     return false;
	if (document.fm.SpareDays.value.length>0){
	  if(!isNumeric(document.fm.SpareDays.value))
	  {
		  alert('剩余有效天数内请填写数字!');
		  return false;
	  }
	}
	//var tSysDate = document.fm.SysDate.value;
	var strSQL = "select (select name from ldcom where comcode=substr(a.managecom,1,4)),"
	+"(select name from ldcom where comcode=a.managecom), "
	+"a.name,getUniteCode(a.agentcode),(select agentgrade from latree b where b.agentcode=a.agentcode),"
	+"a.quafno,a.Quafstartdate,a.QuafEndDate,a.Mobile,a.IDNo "
	+" from laagent a  "
	+"where 1=1  and a.agentstate<'06' "
	//+"and branchtype='3' and branchtype2='01' "
	+"and a.managecom like '"+fm.all('ManageCom').value+"%'"
    //+ getWherePart("a.ManageCom", "ManageCom","like")
    + getWherePart("a.BranchType", "BranchType")
    + getWherePart("a.BranchType2", "BranchType2")
  if (document.fm.SpareDays.value.length>0)
  {
    strSQL += " and ((a.Quafenddate <= (current date + "+fm.all('SpareDays').value+" day) and a.Quafenddate>=current date) or (a.Quafenddate<=current date))";
  }
    strSQL=strSQL+" order by a.agentcode "
	turnPage.queryModal(strSQL, LABankAgentQuafGrid);
 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据！");
    return false;
    }
}
function showOne(parm1, parm2) {
  //判断该行是否确实被选中
  //alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}

function ListExecl()
{
	var strSQL = "select (select name from ldcom where comcode=substr(a.managecom,1,4)),"
	+"(select name from ldcom where comcode=a.managecom), "
	+"a.name,getUniteCode(a.agentcode),(select agentgrade from latree b where b.agentcode=a.agentcode),"
	+"a.quafno,a.Quafstartdate,a.QuafEndDate,a.Mobile,a.IDNo "
	+" from laagent a  "
	+"where 1=1  and a.agentstate<'06'"
	+" and branchtype='3' and branchtype2='01'"
    +"and a.managecom like '"+fm.all('ManageCom').value+"%'"
    + getWherePart("a.BranchType", "BranchType")
    + getWherePart("a.BranchType2", "BranchType2")
  if (document.fm.SpareDays.value.length>0)
  {
    strSQL += " and ((a.Quafenddate <= (current date + "+fm.all('SpareDays').value+" day) and a.Quafenddate>=current date) or (a.Quafenddate<=current date))";
  }
    strSQL=strSQL+" order by a.agentcode "
    
    fm.querySql.value = strSQL;

//定义列名
var strSQLTitle = "select '分公司','支公司','姓名','业务员编码','职级','资格证书号','初次取证日期','到期日期','手机号码','身份证号'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
//定义表名
fm.all("Title").value="select '银保销售序列保险代理从业资格证书有效期查询报表' from dual where 1=1  ";  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();
}

