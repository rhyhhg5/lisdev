//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
/*********************************************************************
 *  投保单复核的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	
	//解除印刷号的锁定
  var prtNo = PolGrid.getRowColData(PolGrid.getSelNo()-1, 2);
  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo="+prtNo+"&CreatePos=承保复核&PolState=1003&Action=DELETE";
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	
  // 刷新查询结果
	easyQueryClick();		
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
		cDiv.style.display="";
	else
		cDiv.style.display="none";  
}


/*********************************************************************
 *  调用EasyQuery查询保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSql = "";
	var tReturn = parseManageComLimitlike();
	strSql = "select BranchAttr,name,managecom,founddate,endflag,agentgroup from LABranchGroup where branchtype='3' and (state<>'1' or state is null)"
	         + tReturn	         
	         + getWherePart('ManageCom')
	         + getWherePart('BranchAttr')	         
	         + getWherePart('BranchLevel')
	         + getWherePart('BranchManager')
	         + getWherePart('FoundDate')
	         + getWherePart('EndDate')	         
	         +" order by BranchAttr";
	turnPage.queryModal(strSql, PolGrid);
}


/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  显示投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showPolDetail()
{
	var i = 0;
  var checkFlag = 0;  
  for (i=0; i<PolGrid.mulLineCount; i++) 
  {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
	
	if (checkFlag) { 
  	var cBranchattr = PolGrid.getRowColData( checkFlag - 1, 1 );  	
  	
  	window.open("./BKBranchQueryMain.jsp?BranchType=3&Branchattr="+cBranchattr);
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}           

