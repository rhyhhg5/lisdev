//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
/*********************************************************************
 *  投保单复核的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 
 function initEdorType(cObj)
{
	mEdorType = " #1# and actype =#01# ";
	showCodeList('AgentCom',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and actype =#01# ";
	showCodeListKey(' ',[cObj], null, null, mEdorType, "1");
}
 
function beforeSubmit()
{
  //添加操作	
  if (!verifyInput()) 
    return false;   
    
  return true;
} 

function submitForm()
{
	
if(verifyInput()) 
  {	  	
  	var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.submit(); //提交
	  showInfo.close();
	}
} 

 
function afterSubmit( FlagStr, content )
{
	//showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
  }
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
		cDiv.style.display="";
	else
		cDiv.style.display="none";  
}


/*********************************************************************
 *  调用EasyQuery查询保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function easyQueryClick()
{
	
	if (!verifyInput()) 
    return false;    
  
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSql = "";
	var tReturn = getManageComLimitlike("a.managecom");
	strSql = "select a.polno,c.riskname,a.transmoney,a.tmakedate,a.getpoldate,b.name,a.agentcode,a.Fyc,a.Grpfyc,a.Depfyc,a.commisionsn "
	        +"from lmriskapp c,lacom b,lacommision a where "
	         + "a.agentcom= b.agentcom and a.branchtype='3' and a.riskcode=c.riskcode "
	         +" and not exists (select 'X' from lacomtoagent where agentcode=a.agentcode and agentcom=a.agentcom and relatype='1')"
	         + tReturn
	         + getWherePart('a.AgentCode','AgentCode')	         
	         + getWherePart('a.BranchAttr','BranchAttr')
	         + getWherePart('a.AgentCom','AgentCom')
	         + getWherePart('a.tmakedate','StartDate','>=')
	         + getWherePart('a.tmakedate','EndDate','<=')
	         +" order by a.polno";
	         
	         
	turnPage.queryModal(strSql, PolGrid);
}


/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  显示投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function showPolDetail()
{	
	fm.action="../agentprint/BankLacomPrt.jsp";
	fm.target="f1print";
	submitForm();
}        
 
function ConfirmComm()
{	
	var i = 0;
  var checkFlag = 0;  
  for (i=0; i<PolGrid.mulLineCount; i++) 
  {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
	
	if (checkFlag) 
	{ 		
  	var cCommisionSN = PolGrid.getRowColData( checkFlag - 1, 11 );    	
  	var cAgentCode = PolGrid.getRowColData( checkFlag - 1, 7 );  	
  	var cFYC = PolGrid.getRowColData( checkFlag - 1, 8 );  	
  	var cGrpFYC = PolGrid.getRowColData( checkFlag - 1, 9 );  	
  	var cDepFYC = PolGrid.getRowColData( checkFlag - 1, 10 );
  	  	
  	if(!verifyInput()) return false;
	    fm.all('Operate').value=cCommisionSN;
	    fm.all('cAgentCode').value=cAgentCode;
	    fm.all('cFYC').value=cFYC;
	    fm.all('cGrpFYC').value=cGrpFYC;
	    fm.all('cDepFYC').value=cDepFYC;	    
	    fm.action="./BankLacomSave.jsp";	    
			fm.target="fraSubmit";
			fm.submit();
			showInfo.close();  	
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}           

