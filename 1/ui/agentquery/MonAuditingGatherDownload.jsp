<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "月度决算汇总报表_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String querySql = request.getParameter("querySql");
    String lenh = request.getParameter("lenh");
    querySql = querySql.replaceAll("%25","%");
    String type = request.getParameter("type");
     if("0".equals(type))
    {
    	if(lenh.equals("4")){
	    	//设置表头
		      String[][] tTitle = {{"分公司代码","分公司名称","管理机构代码","管理机构名称 ","基础人力","期初在职人力","期末在职人力","当月平均人力","月合格人力","2015-12-16（含）之后截至本月新增有效累计人力","基础人力中的离职人力","截至本月净增有效人力","净增有效人力标准","截至本月累计有效人力达成率","销售人力合格率","达成奖","2015年欠款","至上月累计应发总额","当月应发","当月扣减2015年欠款","累计月扣减2015年欠款","2015年欠款余额","至上月累计实发总额","当月实发","累计实发总额","当月fyc"}};
		   //表头的显示属性
		    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26};
		    
		    //数据的显示属性
		    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26};
		    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
		    createexcellist.createExcelFile();
		    String[] sheetName ={"list"};
		    createexcellist.addSheet(sheetName);
		    int row = createexcellist.setData(tTitle,displayTitle);
		    if(row ==-1) errorFlag = true;
		        createexcellist.setRowColOffset(row,0);//设置偏移
		    if(createexcellist.setData(querySql,displayData)==-1)
		        errorFlag = true;
		    if(!errorFlag)
		        //写文件到磁盘
		        try{
		            createexcellist.write(tOutXmlPath);
		        }catch(Exception e)
		        {
		            errorFlag = true;
		            System.out.println(e);
		        }
    	 }
    	 else{
    		//设置表头
   	        String[][] tTitle = {{"分公司代码","分公司名称","管理机构代码","管理机构名称 ","基础人力","期初在职人力","期末在职人力","当月平均人力","月合格人力","2015-12-16（含）之后截至本月新增有效累计人力","基础人力中的离职人力","截至本月净增有效人力","净增有效人力标准","截至本月累计有效人力达成率","销售人力合格率","达成奖","至上月累计应发总额","当月应发","至上月累计实发总额","当月实发","累计实发总额","当前差额","当月fyc"}};
	   	   //表头的显示属性
	   	    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
	   	    
	   	    //数据的显示属性
	   	    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
	   	    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
	   	    createexcellist.createExcelFile();
	   	    String[] sheetName ={"list"};
	   	    createexcellist.addSheet(sheetName);
	   	    int row = createexcellist.setData(tTitle,displayTitle);
	   	    if(row ==-1) errorFlag = true;
	   	        createexcellist.setRowColOffset(row,0);//设置偏移
	   	    if(createexcellist.setData(querySql,displayData)==-1)
	   	        errorFlag = true;
	   	    if(!errorFlag)
	   	        //写文件到磁盘
	   	        try{
	   	            createexcellist.write(tOutXmlPath);
	   	        }catch(Exception e)
	   	        {
	   	            errorFlag = true;
	   	            System.out.println(e);
	   	        }
    	 }
	    //返回客户端
	    if(!errorFlag)
	        downLoadFile(response,filePath,downLoadFileName);
	    out.clear();
	    out = pageContext.pushBody();
    }
    else if("1".equals(type))
    {
    	String[][] tTitle = {{"分公司代码","分公司名称","管理机构代码","管理机构名称","基础人力","期初在职人力","期末在职人力","当月平均人力","月合格人力","2015-12-16（含）之后截至本月新增有效累计人力","基础人力中的离职人力","截至本月年度净增有效人力","年度净增有效人力标准","截至本月年度累计有效人力达成率","截至当月年度销售人力合格率","达成奖","年度合格人力达成特别奖","上半年应发实发差额","截至当月年度累计应发金额 ","至上月累计实发总额","当月实发","截至当月年度累计实发总额","截至当月年度累计fyc","当前差额"}};
    //表头的显示属性
  		int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
    
    //数据的显示属性
    	int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
    	CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
	    createexcellist.createExcelFile();
	    String[] sheetName ={"list"};
	    createexcellist.addSheet(sheetName);
	    int row = createexcellist.setData(tTitle,displayTitle);
	    if(row ==-1) errorFlag = true;
	        createexcellist.setRowColOffset(row,0);//设置偏移
	    if(createexcellist.setData(querySql,displayData)==-1)
	        errorFlag = true;
	    if(!errorFlag)
	        //写文件到磁盘
	        try{
	            createexcellist.write(tOutXmlPath);
	        }catch(Exception e)
	        {
	            errorFlag = true;
	            System.out.println(e);
	        }
	    //返回客户端
	    if(!errorFlag)
	        downLoadFile(response,filePath,downLoadFileName);
	    out.clear();
	    out = pageContext.pushBody();
    }
    
%>