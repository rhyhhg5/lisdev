<%
//Creator :龙程彬
//Date :2011-02-18
%>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {        
  fm.all('IndexCalNo').value = '';  
                   
  //$initfield$
  }
  catch(ex)
  {
    alert("在Init.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      

function initForm()
{
  try
  {
    initInpBox();

	  initWageGrid();
  }
  catch(re)
  {
    alert("LAWageConfirmInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化WageGrid
 ************************************************************
 */
function initWageGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="中介机构";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="手续费";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

       
  
        WageGrid = new MulLineEnter( "fm2" , "WageGrid" ); 

        //这些属性必须在loadMulLine前
        WageGrid.mulLineCount = 0;   
        WageGrid.displayTitle = 1;
        WageGrid.locked=1;
        WageGrid.canSel=0;
        WageGrid.canChk=1;
       	WageGrid.hiddenPlus = 1;
      	WageGrid.hiddenSubtraction = 1;
        WageGrid.loadMulLine(iArray);   

      }
      catch(ex)
      {
        alert("初始化WageGrid时出错："+ ex);
      }
    }


</script>