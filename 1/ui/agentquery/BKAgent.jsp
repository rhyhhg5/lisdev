<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="BKAgent.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BKAgentInit.jsp"%>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>人员信息查询 </title>  
</head>
<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>
<script>
  Operator = "<%=tGlobalInput.Operator%>";
</script>
<body  onload="initForm();" >
  <form action="./BKBranchQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		  </tr>
	  </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD class= title> 
          员工编码 
          </TD>
          <TD  class= input> 
           <Input class=common  name=AgentCode >
          </TD>
          <TD class= title>
            销售机构 
          </TD>
          <TD class= input>
           <Input class=common name=AgentGroup > 
          </TD>       
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   
         
        </TR>
        <TR  class= common>
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD>
        <TD  class= title>
          性别
        </TD>
        <TD  class= input>
          <Input name=Sex class="codeno" verify="性别|code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,null,'100');" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" debug='10'
          ><Input name=SexName class="codename" elementtype=nacessary >
    </TD> 
        <TD  class= title>
          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class="coolDatePicker" dateFormat="short" > 
        </TD>
      </TR>
      <TR  class= common>         
        <TD  class= title>
          身份证号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common > 
        </TD>
        <TD  class= title>
          人员类别 
        </TD>
        <TD  class= input> 
          <Input name=AgentKind class='codeno' ondblclick="initEdorType(this,AgentKindName);" onkeyup="actionKeyUp(this,AgentKindName);"><Input name=AgentKindName class="codename" elementtype=nacessary > 
        </TD>
        <TD  class= title>
          入司时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' > 
        </TD>
      </TR>
    </table>
          <INPUT CLASS=common VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT CLASS=common VALUE="员工信息明细" TYPE=button onclick="showPolDetail();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 员工信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>			
  	</div>    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
