<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
	String tFlag = "";
  	GlobalInput tGI = new GlobalInput();
	tGI=(GlobalInput)session.getValue("GI");
	tFlag = request.getParameter("type"); 
%>
<script>	         
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var type = "<%=tFlag%>";
</script>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>开门红新增合格人力排名</title>
  <SCRIPT src="GoodstartAddValidHumanRankingInput.js"></SCRIPT>
  <%@include file="GoodstartAddValidHumanRankingInit.jsp"%>
 </head>
 <body onload="initForm();initElementtype();">
 	<form action="" method=post name=fm target=fraSubmit>
 		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCond);">
				</td>
				<td class= titleImg>查询条件</td>
			</tr>
		</table>
		<div id= "divCond" style= "display: ''">
			<table class=common >
				<tr class=common>
					<td class=title>管理机构</td>
					<td class=input>
					    <input class=codeNo name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode123',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode123',[this,ManageComName],[0,1],null,null,null,1);" ><input class=codename name=ManageComName readonly=true elementtype=nacessary >
					</td>
					<td class=title>考核年月</td>
					<td class=input>
						<input maxLength=6 name=ExamineDate verify="考核年月|notnull" elementtype=nacessary>
						<label style="font-size:11px;color:red;font:italic arial,sans-serif">考核年月录入格式如:201603</label>
					</td>
				</tr>
			</table>
		</div>
		<input type="hidden" class="common" name="querySql" >
		<input style="font-weight:bold" class=cssButton type=button value=" 查   询 " onclick="query();">
		<input style="font-weight:bold" class=cssButton type=button value=" 下   载 " onclick="download();">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLoadExplain);">
				</td>
				<td class= titleImg>下载清单说明</td>
			</tr>
		</table>
		<div id="divLoadExplain" style="display:''">
			<table class= common>
				<tr class= common>
					<td text-align:left colSpan=1>
						<span id="spanExplainGrid" ></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
		<input class= common type=hidden name=tFlag value="<%=tFlag%>">
        <Input class= common type=hidden name=Operator >
 	</form>
 	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
 </body>
 </html>
  