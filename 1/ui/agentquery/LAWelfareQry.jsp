<%@include file="../common/jsp/AgentCheck.jsp"%>
<%
//程序名称：LAIndexInfoQueryInput.jsp
//程序功能：
//创建日期：2002-3-19
//创建人  ：方波
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAWelfareQry.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="./LAWelfareQryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <title></title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAIndexInfo1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
  <Div  id= "divLAIndexInfo1" style= "display: ''">
  <table  class= common>
    <tr> 
      <td  class= title>代理人编码</td>
      <td  class= input><input name=AgentCode class=readonly > 
      </td>
      <td  class= title> 查询年月 </td>
      <td  class= input> <input class=common name=IndexCalNo > </td>      
    </tr>
    <tr class=common>
      <td class=input colspan=4 align=center><INPUT VALUE="查    询" TYPE=button class=common onclick="easyQueryClick();"></td>    
    </tr>
  </table>
         
  <Div  id= "divWelfareGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanWelfareGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 				
  </div>    
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
