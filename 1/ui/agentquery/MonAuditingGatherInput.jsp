<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：MonAuditingGatherInput.jsp
//程序功能：
//创建日期：2015-02-10
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<html>
<%
	String tFlag = "";
  	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	//tFlag = request.getParameter("type"); 
%>
<script>	         
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	<%--var type = "<%=tFlag%>";--%>
</script>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>月度决算汇总报表</title>
  <SCRIPT src="MonAuditingGatherInput.js"></SCRIPT>
  <%@include file="MonAuditingGatherInit.jsp"%>
</head>
<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divMonAudGa);">
				</td>
				<td class= titleImg>查询条件<span style="font-weight:normal;color:silver;font:italic arial,sans-serif"> *为必录项</span></td>
			</tr>
		</table>
		<div id="divMonAudGa" style="display:''">
			<table class=common>
				<tr class=common>
					<td class=title>考核年月</td>
					<td class=input>
						<input maxLength=6 name=AssessYearMonth verify="考核年月|YYYYMM&notnull" elementtype=nacessary>
						<label style="font-size:11px;color:red;font:italic arial,sans-serif">考核年月录入格式如:201603</label>
					</td>
					<td class=title>管理机构层级</td>
					<td class=input>
						<input class=code name=ManageComHierarchy CodeData="0|^1|总公司^2|分公司^3|三级机构" verify="管理机构层级|notnull" elementtype=nacessary
							ondblclick="return showCodeListEx('ManageComHierarchy',[this],[1],null,null,null,1);">
					</td>
				</tr>
			</table>
		</div>
		
		<input class=common type=hidden name=querySql>
		<input class=common type=hidden name=lenh>
		<input type=button value=" 查  询 " class="cssButton" onclick="query();">
		<input type=button value=" 下  载 " class="cssButton" onclick="download();">
		<hr/>
		<div id="monthId" style="display:none">
			<table class= common>
       			<tr class= common>
      	  			<td text-align: left colSpan=1>
  						<span id="spanMonAudGaGrid" ></span> 
  			  		</td>
  				</tr>
    		</table>
    		<table align=left>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
		<div id="PmonthId" style="display:">
			<table class= common>
       			<tr class= common>
      	  			<td text-align: left colSpan=1>
  						<span id="spanPMonAudGaGrid" ></span> 
  			  		</td>
  				</tr>
    		</table>
    		<table align=left>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
		<div id="yearId" style ="display:none">
			<table class= common>
       			<tr class= common>
      	  			<td text-align: left colSpan=1>
  						<span id="spanYearAudGaGrid" ></span> 
  			  		</td>
  				</tr>
    		</table>
    		<table align=left>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	<br>
	<div id="divCare" style="display:'none'">
	 		<table class=common>
	 			<tr><td><label style="color:red;">注译：</label>当前差额字段只是显示！</td></tr>
	 		</table>
	 </div>
</body>
</html>