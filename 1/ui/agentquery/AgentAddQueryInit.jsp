<%
//程序名称：AgentAddQueryInit.jsp
//程序功能：
//创建日期：2003-4-22
//创建人  ：ll
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                            
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {    
    fm.all('IntroAgency').value = <%=tG.Operator%>;    
  }
  catch(ex)
  {
    alert("在AgentAddQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AgentAddQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initAgentAddGrid();
  }
  catch(re)
  {
    alert("AgentAddQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentAddGrid
 ************************************************************
 */
function initAgentAddGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="代理人编码";         //列名
        iArray[1][1]="130px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="姓名";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="性别";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="出生日期";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="籍贯";         //列名
        iArray[5][1]="80px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="民族";         //列名
        iArray[6][1]="80px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

          
        AgentAddGrid = new MulLineEnter( "fm" , "AgentAddGrid" ); 

        //这些属性必须在loadMulLine前
        AgentAddGrid.mulLineCount = 6;   
        AgentAddGrid.displayTitle = 1;
        AgentAddGrid.canSel=1;
//      AgentAddGrid.canChk=0;
        AgentAddGrid.locked=1;
	AgentAddGrid.hiddenPlus=1;
	AgentAddGrid.hiddenSubtraction=1;
        AgentAddGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化AgentAddGrid时出错："+ ex);
      }
    }


</script>