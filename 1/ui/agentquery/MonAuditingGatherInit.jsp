<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：MonAuditingGatherInit.jsp
//程序功能：
//创建日期：2015-02-10
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script src="../common/javascript/Common.js"></script>
<%
	PubFun tpubFun = new PubFun();
	String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
	function initInpBox(){
		try{
			var cLen = manageCom.length;
		if(cLen==2)
			fm.ManageComHierarchy.value="总公司";
	    if(cLen==4)
			fm.ManageComHierarchy.value="分公司";
	    if(cLen==8)
			fm.ManageComHierarchy.value="三级机构";
		
		}catch(ex){
			alert("在-->IninInpBox函数中发生异常 ：初始化界面错误！");
		}
	}
	function initForm(){
		try{
			initElementtype();
			initInpBox();
			initMonAudGaGrid();
			showAllCodeName();
			initPMonAudGaGrid();
			showAllCodeName();
			initYearAudGaGrid();
		}catch(re){
			alert("MonAuditingGatherInit.jsp-->InitForm函数中发生异常:初始化界面错误");
		}
	}
	function initPMonAudGaGrid(){
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "分公司机构代码";
			iArray[1][1] = "80px";
			iArray[1][2] = 40;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "分公司机构名称";
			iArray[2][1] = "80px";
			iArray[2][2] = 40;
			iArray[2][3] = 0;
			        
			iArray[3] = new Array();
			iArray[3][0] = "管理机构代码";
			iArray[3][1] = "80px";
			iArray[3][2] = 40;
			iArray[3][3] = 0;
			        
			iArray[4] = new Array();
			iArray[4][0] = "管理机构名称";
			iArray[4][1] = "80px";
			iArray[4][2] = 40;
			iArray[4][3] = 0;
			        
			iArray[5] = new Array();
			iArray[5][0] = "基础人力";
			iArray[5][1] = "60px";
			iArray[5][2] = 40;
			iArray[5][3] = 0;
			        
			iArray[6] = new Array();
			iArray[6][0] = "期初在职人力";
			iArray[6][1] = "100px";
			iArray[6][2] = 40;
			iArray[6][3] = 0;
			        
			iArray[7] = new Array();
			iArray[7][0] = "期末在职人力";
			iArray[7][1] = "80px";
			iArray[7][2] = 40;
			iArray[7][3] = 0;
			        
			iArray[8] = new Array();
			iArray[8][0] = "当月平均人力";
			iArray[8][1] = "80px";
			iArray[8][2] = 40;
			iArray[8][3] = 0;
			        
			iArray[9] = new Array();
			iArray[9][0] = "月合格人力";
			iArray[9][1] = "80px";
			iArray[9][2] = 40;
			iArray[9][3] = 0;
			
			iArray[10] = new Array();
			iArray[10][0] = "2015-12-16(含)之后截至本月新增有效累计人力";
			iArray[10][1] = "150px";
			iArray[10][2] = 40;
			iArray[10][3] = 0;
			         
			iArray[11] = new Array();
			iArray[11][0] = "基础人力中的离职人力";
			iArray[11][1] = "120px";
			iArray[11][2] = 40;
			iArray[11][3] = 0;
			         
			iArray[12] = new Array();
			iArray[12][0] = "截至本月累计净增有效人力";
			iArray[12][1] = "60px";
			iArray[12][2] = 40;
			iArray[12][3] = 0;
						       
			iArray[13] = new Array();
			iArray[13][0] = "净增有效人力标准";
			iArray[13][1] = "100px";
			iArray[13][2] = 60;
			iArray[13][3] = 0;
			         
			iArray[14] = new Array();
			iArray[14][0] = "截至本月累计有效人力达成率";
			iArray[14][1] = "120px";
			iArray[14][2] = 40;
			iArray[14][3] = 0;
		           
			iArray[15] = new Array();
			iArray[15][0] = "销售人力合格率";
			iArray[15][1] = "80px";
			iArray[15][2] = 40;
			iArray[15][3] = 0;
			         
			iArray[16] = new Array();
			iArray[16][0] = "达成奖";
			iArray[16][1] = "100px";
			iArray[16][2] = 40;
			iArray[16][3] = 0;
               
			iArray[17] = new Array();
			iArray[17][0] = "2015年欠款";
			iArray[17][1] = "100px";
			iArray[17][2] = 40;
			iArray[17][3] = 0;
             
			iArray[18] = new Array();
			iArray[18][0] = "至上月累计应发总额";
			iArray[18][1] = "100px";
			iArray[18][2] = 40;
			iArray[18][3] = 0;
               
			iArray[19] = new Array();
			iArray[19][0] = "当月应发";
			iArray[19][1] = "100px";
			iArray[19][2] = 40;
			iArray[19][3] = 0;
               
			iArray[20] = new Array();
			iArray[20][0] = "当月扣减2015年欠款";
			iArray[20][1] = "100px";
			iArray[20][2] = 40;
			iArray[20][3] = 0;
			         
			iArray[21] = new Array();
			iArray[21][0] = "累计月扣减2015年欠款";
			iArray[21][1] = "100px";
			iArray[21][2] = 40;
			iArray[21][3] = 0;
			         
			iArray[22] = new Array();
			iArray[22][0] = "2015年欠款余额";
			iArray[22][1] = "100px";
			iArray[22][2] = 40;
			iArray[22][3] = 0;
               
			iArray[23] = new Array();
			iArray[23][0] = "至上月累计实发总额";
			iArray[23][1] = "100px";
			iArray[23][2] = 40;
			iArray[23][3] = 0;
			         
			iArray[24] = new Array();
			iArray[24][0] = "当月实发";
			iArray[24][1] = "100px";
			iArray[24][2] = 40;
			iArray[24][3] = 0;
			         
			iArray[25] = new Array();
			iArray[25][0] = "累计实发总额";
			iArray[25][1] = "100px";
			iArray[25][2] = 40;
			iArray[25][3] = 0;
			
			iArray[26] = new Array();
			iArray[26][0] = "当月fyc";
			iArray[26][1] = "100px";
			iArray[26][2] = 40;
			iArray[26][3] = 0;
		  
			
			PMonAudGaGrid = new MulLineEnter("fm","PMonAudGaGrid");
		     
			PMonAudGaGrid.mulLineCount = 10;   
			PMonAudGaGrid.displayTitle = 1;
			PMonAudGaGrid.canSel = 1;
			PMonAudGaGrid.canChk = 0;
			PMonAudGaGrid.locked = 1;
			PMonAudGaGrid.hiddenSubtraction = 1;
			PMonAudGaGrid.hiddenPlus = 1;
			PMonAudGaGrid.selBoxEventFuncName ="";
		     
			PMonAudGaGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
	function initMonAudGaGrid(){
		var iArray = new Array();
		try{
				iArray[0] = new Array();
				iArray[0][0] = "序号";
				iArray[0][1] = "30px";
				iArray[0][2] = 10;
				iArray[0][3] = 0;
				
				iArray[1] = new Array();
				iArray[1][0] = "分公司机构代码";
				iArray[1][1] = "80px";
				iArray[1][2] = 40;
				iArray[1][3] = 0;
				
				iArray[2] = new Array();
				iArray[2][0] = "分公司机构名称";
				iArray[2][1] = "80px";
				iArray[2][2] = 40;
				iArray[2][3] = 0;
				        
				iArray[3] = new Array();
				iArray[3][0] = "管理机构代码";
				iArray[3][1] = "80px";
				iArray[3][2] = 40;
				iArray[3][3] = 0;
				        
				iArray[4] = new Array();
				iArray[4][0] = "管理机构名称";
				iArray[4][1] = "80px";
				iArray[4][2] = 40;
				iArray[4][3] = 0;
				        
				iArray[5] = new Array();
				iArray[5][0] = "基础人力";
				iArray[5][1] = "60px";
				iArray[5][2] = 40;
				iArray[5][3] = 0;
				        
				iArray[6] = new Array();
				iArray[6][0] = "期初在职人力";
				iArray[6][1] = "100px";
				iArray[6][2] = 40;
				iArray[6][3] = 0;
				        
				iArray[7] = new Array();
				iArray[7][0] = "期末在职人力";
				iArray[7][1] = "80px";
				iArray[7][2] = 40;
				iArray[7][3] = 0;
				        
				iArray[8] = new Array();
				iArray[8][0] = "当月平均人力";
				iArray[8][1] = "80px";
				iArray[8][2] = 40;
				iArray[8][3] = 0;
				        
				iArray[9] = new Array();
				iArray[9][0] = "月合格人力";
				iArray[9][1] = "80px";
				iArray[9][2] = 40;
				iArray[9][3] = 0;
				
				iArray[10] = new Array();
				iArray[10][0] = "2015-12-16(含)之后截至本月新增有效累计人力";
				iArray[10][1] = "150px";
				iArray[10][2] = 40;
				iArray[10][3] = 0;
				         
				iArray[11] = new Array();
				iArray[11][0] = "基础人力中的离职人力";
				iArray[11][1] = "120px";
				iArray[11][2] = 40;
				iArray[11][3] = 0;
				         
				iArray[12] = new Array();
				iArray[12][0] = "截至本月累计净增有效人力";
				iArray[12][1] = "60px";
				iArray[12][2] = 40;
				iArray[12][3] = 0;
							       
				iArray[13] = new Array();
				iArray[13][0] = "净增有效人力标准";
				iArray[13][1] = "100px";
				iArray[13][2] = 60;
				iArray[13][3] = 0;
				         
				iArray[14] = new Array();
				iArray[14][0] = "截至本月累计有效人力达成率";
				iArray[14][1] = "120px";
				iArray[14][2] = 40;
				iArray[14][3] = 0;
			           
				iArray[15] = new Array();
				iArray[15][0] = "销售人力合格率";
				iArray[15][1] = "80px";
				iArray[15][2] = 40;
				iArray[15][3] = 0;
				         
				iArray[16] = new Array();
				iArray[16][0] = "达成奖";
				iArray[16][1] = "100px";
				iArray[16][2] = 40;
				iArray[16][3] = 0;              
	             
				iArray[17] = new Array();
				iArray[17][0] = "至上月累计应发总额";
				iArray[17][1] = "100px";
				iArray[17][2] = 40;
				iArray[17][3] = 0;
	               
				iArray[18] = new Array();
				iArray[18][0] = "当月应发";
				iArray[18][1] = "100px";
				iArray[18][2] = 40;
				iArray[18][3] = 0;
	               
				iArray[19] = new Array();
				iArray[19][0] = "至上月累计实发总额";
				iArray[19][1] = "100px";
				iArray[19][2] = 40;
				iArray[19][3] = 0;
				         
				iArray[20] = new Array();
				iArray[20][0] = "当月实发";
				iArray[20][1] = "100px";
				iArray[20][2] = 40;
				iArray[20][3] = 0;
				         
				iArray[21] = new Array();
				iArray[21][0] = "累计实发总额";
				iArray[21][1] = "100px";
				iArray[21][2] = 40;
				iArray[21][3] = 0;
	               
				iArray[22] = new Array();
				iArray[22][0] = "当前差额";
				iArray[22][1] = "100px";
				iArray[22][2] = 40;
				iArray[22][3] = 0;			         
				
				iArray[23] = new Array();
				iArray[23][0] = "当月fyc";
				iArray[23][1] = "100px";
				iArray[23][2] = 40;
				iArray[23][3] = 0;
				
				MonAudGaGrid = new MulLineEnter("fm","MonAudGaGrid");
			     
				MonAudGaGrid.mulLineCount = 10;   
				MonAudGaGrid.displayTitle = 1;
				MonAudGaGrid.canSel = 1;
				MonAudGaGrid.canChk = 0;
				MonAudGaGrid.locked = 1;
				MonAudGaGrid.hiddenSubtraction = 1;
				MonAudGaGrid.hiddenPlus = 1;
				MonAudGaGrid.selBoxEventFuncName ="";
			     
				MonAudGaGrid.loadMulLine(iArray);
			}catch(ex){
				alert(ex);
			}
		}
	
	
	function initYearAudGaGrid(){
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "分公司机构代码";
			iArray[1][1] = "80px";
			iArray[1][2] = 40;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "分公司机构名称";
			iArray[2][1] = "80px";
			iArray[2][2] = 40;
			iArray[2][3] = 0;
			        
			iArray[3] = new Array();
			iArray[3][0] = "管理机构代码";
			iArray[3][1] = "80px";
			iArray[3][2] = 40;
			iArray[3][3] = 0;
			        
			iArray[4] = new Array();
			iArray[4][0] = "管理机构名称";
			iArray[4][1] = "80px";
			iArray[4][2] = 40;
			iArray[4][3] = 0;
			        
			iArray[5] = new Array();
			iArray[5][0] = "基础人力";
			iArray[5][1] = "60px";
			iArray[5][2] = 40;
			iArray[5][3] = 0;
			        
			iArray[6] = new Array();
			iArray[6][0] = "期初在职人力";
			iArray[6][1] = "100px";
			iArray[6][2] = 40;
			iArray[6][3] = 0;
			        
			iArray[7] = new Array();
			iArray[7][0] = "期末在职人力";
			iArray[7][1] = "80px";
			iArray[7][2] = 40;
			iArray[7][3] = 0;
			        
			iArray[8] = new Array();
			iArray[8][0] = "当月平均人力";
			iArray[8][1] = "80px";
			iArray[8][2] = 40;
			iArray[8][3] = 0;
			        
			iArray[9] = new Array();
			iArray[9][0] = "月合格人力";
			iArray[9][1] = "80px";
			iArray[9][2] = 40;
			iArray[9][3] = 0;
			
			iArray[10] = new Array();
			iArray[10][0] = "2015-12-16(含)之后截至本月新增有效累计人力";
			iArray[10][1] = "150px";
			iArray[10][2] = 40;
			iArray[10][3] = 0;
			         
			iArray[11] = new Array();
			iArray[11][0] = "基础人力中的离职人力";
			iArray[11][1] = "120px";
			iArray[11][2] = 40;
			iArray[11][3] = 0;
			         
			iArray[12] = new Array();
			iArray[12][0] = "截至本月年度净增有效人力";
			iArray[12][1] = "120px";
			iArray[12][2] = 40;
			iArray[12][3] = 0;
               
			iArray[13] = new Array();
			iArray[13][0] = "年度净增有效人力标准";
			iArray[13][1] = "120px";
			iArray[13][2] = 40;
			iArray[13][3] = 0;
			         
			iArray[14] = new Array();
			iArray[14][0] = "截至本月年度累计有效人力达成率";
			iArray[14][1] = "100px";
			iArray[14][2] = 60;
			iArray[14][3] = 0;
			         
			iArray[15] = new Array();
			iArray[15][0] = "截至当月年度销售人力合格率";
			iArray[15][1] = "120px";
			iArray[15][2] = 40;
			iArray[15][3] = 0;
		           
			iArray[16] = new Array();
			iArray[16][0] = "达成奖";
			iArray[16][1] = "80px";
			iArray[16][2] = 40;
			iArray[16][3] = 0;
			         
			iArray[17] = new Array();
			iArray[17][0] = "年度合格人力达成特别奖";
			iArray[17][1] = "100px";
			iArray[17][2] = 40;
			iArray[17][3] = 0;
               
			iArray[18] = new Array();
			iArray[18][0] = "上半年应发实发差额";
			iArray[18][1] = "100px";
			iArray[18][2] = 40;
			iArray[18][3] = 0;
               
			iArray[19] = new Array();
			iArray[19][0] = "截至当月年度累计应发金额";
			iArray[19][1] = "100px";
			iArray[19][2] = 40;
			iArray[19][3] = 0;

			iArray[20] = new Array();
			iArray[20][0] = "至上月累计实发总额";
			iArray[20][1] = "100px";
			iArray[20][2] = 40;
			iArray[20][3] = 0;

			iArray[21] = new Array();
			iArray[21][0] = "当月实发";
			iArray[21][1] = "100px";
			iArray[21][2] = 40;
			iArray[21][3] = 0;

			iArray[22] = new Array();
			iArray[22][0] = "截至当月年度累计实发总额";
			iArray[22][1] = "100px";
			iArray[22][2] = 40;
			iArray[22][3] = 0;
			
			iArray[23] = new Array();
			iArray[23][0] = "截至当月年度累计fyc";
			iArray[23][1] = "100px";
			iArray[23][2] = 40;
			iArray[23][3] = 0;
			
			iArray[24] = new Array();
			iArray[24][0] = "当前差额";
			iArray[24][1] = "100px";
			iArray[24][2] = 40;
			iArray[24][3] = 0;
			
			
			YearAudGaGrid = new MulLineEnter("fm","YearAudGaGrid");
		     
			YearAudGaGrid.mulLineCount = 10;   
			YearAudGaGrid.displayTitle = 1;
			YearAudGaGrid.canSel = 1;
			YearAudGaGrid.canChk = 0;
			YearAudGaGrid.locked = 1;
			YearAudGaGrid.hiddenSubtraction = 1;
			YearAudGaGrid.hiddenPlus = 1;
			YearAudGaGrid.selBoxEventFuncName ="";
		     
			YearAudGaGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>