<%
//程序名称：AgentDayDetailInit.jsp
//程序功能：
//创建日期：2003-4-8 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            
 
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
//  fm.all('PolNo').value = '';
//  fm.all('RiskCode').value = '';
//  fm.all('PayMode').value = '';
//  fm.all('TransMoney').value = '';
//  fm.all('PayYear').value = '';
//  fm.all('PayCount').value = '';
//  fm.all('TpayDate').value = '';
//  fm.all('CommDate').value = '';
//  fm.all('TconfDate').value = '';
//  fm.all('ReceiptNo').value = '';
    
  }
  catch(ex)
  {
    alert("在AgentDayDetailInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AgentDayDetailInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initAddDepGrid();
    //afterQuery(tCommisionSN);  
  }
  catch(re)
  {
    alert("AgentDayDetailInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initAddDepGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="代理人编码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="代理人姓名";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="团队编码";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[4]=new Array();
        iArray[4][0]="保单号";         //列名
        iArray[4][1]="80px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="险种编码";         //列名
        iArray[5][1]="60px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="佣金提数日期";         //列名
        iArray[6][1]="80px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="错误信息";         //列名
        iArray[7][1]="180px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

          
        AddDepGrid = new MulLineEnter( "fm" , "AddDepGrid" ); 

        //这些属性必须在loadMulLine前
        AddDepGrid.mulLineCount = 0;   
        AddDepGrid.displayTitle = 1;
        AddDepGrid.canSel=1;
//      AddDepGrid.canChk=0;
        AddDepGrid.locked=1;
	AddDepGrid.hiddenPlus=1;
	AddDepGrid.hiddenSubtraction=1;
        AddDepGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化AddDepGrid时出错："+ ex);
      }
    }  
</script>