<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：WageQueryInput.jsp
//程序功能：
//创建日期：2003-02-16 15:12:44
//创建人  ：程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./GroupOperationInfo.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./GroupOperationInfoInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <title>展业机构 </title>
</head>
<body  onload="initForm();" >
  <form action="./WageQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
  <table>
    <tr class=common>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);"></IMG>
      </td>  
      <td class=titleImg>
      查询条件
      </td>
    </tr>
  </table>
  <Div  id= "divLABranchGroup1" style= "display: ''">
  <table  class= common>
    <tr  class= common> 
      <td  class= title> 本组代码</td>
      <td colspan="3"  class= input> <input class=common name=BranchAttr> </td>
    </tr>
    <tr  class= common> 
      <td  class= title> 起始月度</td>
      <td  class= input> 
        <input class= common name=StartMonth > 
      </td>
      <td  class= title>截止月度</td>
      <td  class= input> <input class= common name=EndMonth > </td>
    </tr>
   
   
  </table>
  <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		
      <td class= titleImg> 业务查询结果 </td>
    	</tr>
    </table>
  	<Div  id= "divAGroupGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spancommisionGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 					
  </div>
  <table>
    <tr class=common>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);"></IMG>
      </td>  
      <td class=titleImg>
      该组成员
      </td>
    </tr>
  </table>
  <Div  id= "divLAGroupMember" style= "display: 'none'">
  	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanWageGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 	
  </Div>	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <input class=common name=Agentgroup>
  </form>
</body>
</html>
