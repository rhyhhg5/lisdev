//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet1,arrDataSet2; 
var turnPage = new turnPageClass();


//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAComQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
     

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function DetailPrint()
{
  fm.action="../agentprint/LAIndexInfoQueryPrt.jsp";
  fm.target="f1print";
  submitForm();
}

//提交，保存按钮对应操作
function submitForm()
{  	
	if(!verifyInput())
	{
		return false;
	}
	var tWageNoEnd=fm.all('IndexCalNo').value;
	if(tWageNoEnd==''||tWageNoEnd==null){
		alert('请输入考核年月');
		return false;
	}
	var tWageNoBegin=parseInt(tWageNoEnd)-2;
	var tCondition = "" ;
	var tsql = "";
	var strAgent = " ";
	if(fm.AgentCode.value != null  && fm.AgentCode.value  != ""){
		strAgent = " and a.AgentCode = getAgentCode('"+fm.AgentCode.value+"') ";
	}
  if(fm.all('AgentSeries').value != '') 
	{
	 tCondition = " and exists(select 'X' from laagentgrade where gradecode = b.agentgrade and gradeproperty2 ='"+fm.all('AgentSeries').value+"')";
	}
  
  tsql = "select getUniteCode(a.AgentCode),c.Name,b.AgentGrade,a.ManageCom,a.BranchAttr,d.name,"
            +"(case when a.Indextype='02' then '维持'  when a.Indextype='03' then '晋升' else '' end ),";
  
  if(fm.all('AgentSeries').value =='0')
  {
  	tsql += "int(nvl(a.IndFYCSum/T33,0)),"
            +"int(nvl(MonAvgCust/T33,0)), ";
            if(tWageNoEnd.substr(4,2)=='03'||tWageNoEnd.substr(4,2)=='06'
               ||tWageNoEnd.substr(4,2)=='09'||tWageNoEnd.substr(4,2)=='12'){
            	tsql += "(select (value(sum(case when T50 = 0 then 0.75 else decimal(T49, 12, 2) / decimal(T50, 12, 2) end)/decimal((timestampdiff(64,char(timestamp(substr('"+tWageNoEnd+"', 1, 4) || '-' ||substr('"+tWageNoEnd+"', 5, 2) ||'-01') -timestamp(substr('"+tWageNoBegin+"', 1, 4) || '-' ||substr('"+tWageNoBegin+"', 5, 2) ||'-01')))+1),12,2),0))"
            	     +" from laindexinfo where agentcode = b.agentcode and IndexCalNo >='"+tWageNoBegin
            	     +"' and IndexCalNo <= '"+tWageNoEnd+"' and indextype = '00' and branchtype='1' and branchtype2='01'),"
            	
            }else{
            	tsql += "0,"
            	
            }
            tsql+="nvl(a.DInRCount,0),"  //任职月数
            +"nvl(a.DirRmdCount,0),nvl(a.AddCount,0),nvl(a.T34,0)";
  }
  else if(fm.all('AgentSeries').value =='1')
  {
  	tsql +="nvl(nvl(a.DirTeamFYCSum,0)+nvl(a.DRFYCSum,0),0),"
            +"nvl(nvl(a.MngAgentCount,0)+nvl(a.DRTeamMonLabor,0),0)," //人力
            +"(select GrpAvgContiRate(b.agentcode,b.agentgrade,'"+tWageNoBegin
            +"','"+tWageNoEnd+"') from dual),"
            +"nvl(a.DRTeamCount,0)";
  	
  }
  else if(fm.all('AgentSeries').value =='2')	
  {
  	tsql += "nvl(nvl(a.DepFYCSum,0)+nvl(a.DirDepMonAvgFYC,0),0),"
            +"nvl(nvl(a.DirMngCount,0)+nvl(a.DRDepMonLabor,0),0),"
            +"(select DcAvgContiRate(b.agentcode,b.agentgrade,'"+tWageNoBegin
            +"','"+tWageNoEnd+"') from dual),"  //区指标
            +"nvl(a.TeamCount,0),"
            +"nvl(a.RearDepCount,0)";
  } 
      
      tsql += "from LAIndexInfo a,LAAssess b,LAAgent c,LABranchGroup d "
	    +"where a.AgentCode = b.AgentCode and a.indexcalno = b.indexcalno "
	    +"and a.agentcode=c.agentcode and a.branchattr=d.branchattr "
	    +"and a.ManageCom = b.ManageCom  and a.IndexType in ('02','03') and a.managecom like '"+fm.all('ManageCom').value+"%' "
	    +" and a.branchtype='1' and a.branchtype2='01'"
	    +tCondition
	    +getWherePart('a.IndexCalNo','IndexCalNo')
	    +getWherePart('a.BranchAttr','BranchAttr')
	    +getWherePart('a.IndexType','IndexType')
	   // +getWherePart('a.AgentCode','AgentCode');
        +strAgent
      tsql += " ORDER BY a.managecom,a.agentcode , a.IndexType asc" ; 
      fm.all('querySql').value = tsql;
  	var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.submit(); //提交
	  showInfo.close();	
}

// 查询按钮
function easyQueryClick()
{
var tWageNoEnd=fm.all('IndexCalNo').value;
if(tWageNoEnd==''||tWageNoEnd==null){
	alert('请输入考核年月');
	return false;
}
var tWageNoBegin=parseInt(tWageNoEnd)-2;
	if(!verifyInput())
	{
		return false;
	}
	initIndexInfoGrid1();
	//IndexInfoGrid1.clearData();
	var tReturn = getManageComLimitlike("a.ManageCom");
  var tCondition = "" ;
  var tsql = "";
  var strAgent = " ";
  if(fm.AgentCode.value != null  && fm.AgentCode.value  != ""){
		strAgent = " and a.AgentCode = getAgentCode('"+fm.AgentCode.value+"') ";
	}
  if(fm.all('AgentSeries').value != '') 
	{
	 tCondition = " and exists(select 'X' from laagentgrade where gradecode = b.agentgrade and gradeproperty2 ='"+fm.all('AgentSeries').value+"')";
	}
  
  tsql = "select getUniteCode(a.AgentCode),c.Name,b.AgentGrade,a.ManageCom,a.BranchAttr,d.name,"
            +"a.StartDate,a.StartEnd,(case when a.Indextype='02' then '维持'  when a.Indextype='03' then '晋升' else '' end ),";
  
  if(fm.all('AgentSeries').value =='0')
  {
  	tsql += "int(nvl(a.IndFYCSum/T33,0)),"
            +"int(nvl(MonAvgCust/T33,0)), ";
            if(tWageNoEnd.substr(4,2)=='03'||tWageNoEnd.substr(4,2)=='06'
               ||tWageNoEnd.substr(4,2)=='09'||tWageNoEnd.substr(4,2)=='12'){
            	tsql += "(select (value(sum(case when T50 = 0 then 0.75 else decimal(T49, 12, 2) / decimal(T50, 12, 2) end)/decimal((timestampdiff(64,char(timestamp(substr('"+tWageNoEnd+"', 1, 4) || '-' ||substr('"+tWageNoEnd+"', 5, 2) ||'-01') -timestamp(substr('"+tWageNoBegin+"', 1, 4) || '-' ||substr('"+tWageNoBegin+"', 5, 2) ||'-01')))+1),12,2),0))"
            	     +" from laindexinfo where agentcode = b.agentcode and IndexCalNo >='"+tWageNoBegin
            	     +"' and IndexCalNo <= '"+tWageNoEnd+"' and indextype = '00' and branchtype='1' and branchtype2='01'),"
            	
            }else{
            	tsql += "0,"
            	
            }
            tsql+="nvl(a.DInRCount,0),"  //任职月数
            +"nvl(a.DirRmdCount,0),nvl(a.AddCount,0),nvl(a.T34,0),"
            +"nvl(a.T33,0) ";
  }
  else if(fm.all('AgentSeries').value =='1')
  {
  	tsql +="nvl(nvl(a.DirTeamFYCSum,0)+nvl(a.DRFYCSum,0),0),"
            +"nvl(nvl(a.MngAgentCount,0)+nvl(a.DRTeamMonLabor,0),0)," //人力
            +"(select GrpAvgContiRate(b.agentcode,b.agentgrade,'"+tWageNoBegin
            +"','"+tWageNoEnd+"') from dual),"
            +"nvl(a.DRTeamCount,0),"
            +"nvl(a.T33,0) ";
  	
  }
  else if(fm.all('AgentSeries').value =='2')	
  {
  	tsql += "nvl(nvl(a.DepFYCSum,0)+nvl(a.DirDepMonAvgFYC,0),0),"
            +"nvl(nvl(a.DirMngCount,0)+nvl(a.DRDepMonLabor,0),0),"
            +"(select DcAvgContiRate(b.agentcode,b.agentgrade,'"+tWageNoBegin
            +"','"+tWageNoEnd+"') from dual),"  //区指标
            +"nvl(a.TeamCount,0),"
            +"nvl(a.RearDepCount,0),"
            +"nvl(a.T33,0) ";
  }
  
      tsql += "from LAIndexInfo a,LAAssess b,LAAgent c,LABranchGroup d "
	    +"where a.AgentCode = b.AgentCode and a.indexcalno = b.indexcalno "
	    +"and a.agentcode=c.agentcode and a.branchattr=d.branchattr "
	    +"and a.ManageCom = b.ManageCom  and a.IndexType in ('02','03')"
	    +" and a.branchtype='1' and a.branchtype2='01'"
	    +tCondition
	    +getWherePart('a.ManageCom','ManageCom','like')
	    +getWherePart('a.IndexCalNo','IndexCalNo')
	    +getWherePart('a.BranchAttr','BranchAttr')
	    +getWherePart('a.IndexType','IndexType')
	    //+getWherePart('a.AgentCode','AgentCode')
	    +strAgent
	    +tReturn;         
      tsql += " ORDER BY a.managecom,a.agentcode , a.IndexType asc with ur" ; 
 

  turnPage.strQueryResult  = easyQueryVer3(tsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有查询到符合条件的考核指标！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage.strQueryResult);
  
turnPage.arrDataCacheSet = arrDataSet1;

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = IndexInfoGrid1;    
          
  //保存SQL语句
  turnPage.strQuerySql     = tsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  
}


function getQueryResult1()
{
  var arrSelected = null;
  tRow = IndexInfoGrid1.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet1 == null )
    return arrSelected;
  arrSelected = new Array();
  tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
  //设置需要返回的数组
  arrSelected[0] = arrDataSet1[tRow-1];
  return arrSelected;
}



/*function selectItem(spanID,parm2)
{
  var arrSelect = new Array();
  arrSelect = getQueryResult1();
  var strIndexCalNo=arrSelect[0][0];
  var strIndexType=arrSelect[0][1];
  var strAgentCode=arrSelect[0][2];
  if (strIndexCalNo!="")
    strIndexCalNo=" and IndexCalNo='"+strIndexCalNo+"'";
  if (strIndexType!="")
    strIndexType=" and IndexType='"+strIndexType+"'";
  if (strAgentCode!="")
    strAgentCode=" and AgentCode='"+strAgentCode+"'";

  var strSQLMoreResult="select * from LAIndexInfo where 1=1 "
                 + strIndexCalNo
	         + strIndexType
	         + strAgentCode;
	        
  var strMoreResult=easyQueryVer3(strSQLMoreResult, 1, 1, 1);
  fm.all('HiddenStrMoreResult').value=strMoreResult; 

  if (strMoreResult)
  {
   showInfo=window.open("./LAIndexInfoQueryMoreResult.jsp");
  }
  else
    alert("查询失败");

}*/








