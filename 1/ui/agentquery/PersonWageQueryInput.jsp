<%@include file="../common/jsp/AgentCheck.jsp"%>
<%
//程序名称：WageQueryInput.jsp
//程序功能：
//创建日期：2003-02-16 15:12:44
//创建人  ：程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./PersonWageQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./PersonWageQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>展业机构 </title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
  
<table>
  <tr class=common>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQueryCond);"></td>
    <td class=titleImg>查询条件</td>    
  </tr>
</table>
<div id="divQueryCond" style="display:''">
  <table  class= common>
    <tr  class= common> 
      <td  class= title> 业务员代码</td>
      <td class= input> <input class='readonly' readonly name=AgentCode > </td>
      <td  class= title> 发放年月编码</td>
      <td class= input> <input class=common name=IndexCalNo > </td>
    </tr>    
  </table>
</div>  
<INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();">

<table>
 <tr>
  <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divWageGrid);"></td>    		
  <td class= titleImg> 佣金查询结果 </td>
 </tr>
</table>
<Div  id= "divWageGrid" style= "display: ''">
  <table class=common>
   <tr class=common>
     <td text-align: left colSpan=1>
  		<span id="spanWageGrid" >
  		</span> 
  	 </td>
   </tr>
  </table>
  
  <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
  <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
  <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
  <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 					
</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
