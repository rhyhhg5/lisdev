<%
//程序名称：HospitalQueryInit.jsp
//程序功能：
//创建日期：2003-10-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
 
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
var turnPage = new turnPageClass(); 


function initHospitalGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="医院网点代码"; //列名
    iArray[1][1]="60px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许


    iArray[2]=new Array();
    iArray[2][0]="医院网点名称"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许

    
    HospitalGrid = new MulLineEnter( "fm" , "HospitalGrid" );
    
    HospitalGrid.mulLineCount = 0;
    HospitalGrid.displayTitle = 1;
    HospitalGrid.hiddenPlus = 1;
    HospitalGrid.hiddenSubtraction = 1;
    HospitalGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在HospitalQueryInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
	
    initHospitalGrid();
    initData();
  try
  {
    
  }
  catch(re)
  {
    
    alert("在HospitalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initData()
{ 
	getData(); 
}
</script>