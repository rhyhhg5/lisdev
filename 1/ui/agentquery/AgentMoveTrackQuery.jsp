<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AgentMoveTrackQuery.jsp
//程序功能：
//创建日期：2003-07-15 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentquery.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema mLAAgentSchema = new LAAgentSchema();
  AgentMoveTrackUI mAgentMoveTrackUI  = new AgentMoveTrackUI();
  LATreeSet tLATreeSet = new LATreeSet();

  //输出参数
  CErrors tError = null;
  String tOperate="QUERY||MAIN";
  String tRela  = "";
  String FlagStr = "";
  String Content = "";

  GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin prepare Agentcode and Name...");
  
  String tTableFlag = request.getParameter("TableFlag");
  mLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
  mLAAgentSchema.setAgentCode(request.getParameter("AgentCode"));
  mLAAgentSchema.setName(request.getParameter("Name"));
  mLAAgentSchema.setBranchType(request.getParameter("BranchType"));
  
  System.out.println("end 基本信息...");

  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.add(tTableFlag);
  tVData.addElement(mLAAgentSchema);
  System.out.println("add over");
  try
  {
    mAgentMoveTrackUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "查询失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mAgentMoveTrackUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
  tVData.clear();
  tVData = mAgentMoveTrackUI.getResult();
  tLATreeSet.set((LATreeSet)tVData.getObjectByObjectName("LATreeSet",0));
  int n = tLATreeSet.size();
  System.out.println("size:"+n);
  LATreeSchema mLATreeSchema;
  String recordString;
  for (int i = 1; i <= n; i++)
  {
     recordString = "";
     mLATreeSchema = tLATreeSet.get(i);
     recordString = recordString + mLATreeSchema.getAgentCode().trim()+"|";  //代理人代码
     recordString = recordString + mLATreeSchema.getAgentGroup()+"|"; //BranchAttr
     recordString = recordString + mLATreeSchema.getManageCom().trim()+"|";  //职级
     System.out.println("Record:"+recordString);
     //上级代理人名称
     if (mLATreeSchema.getAgentSeries() != null)
        //mLATreeSchema.setAgentSeries("");
        recordString=recordString+mLATreeSchema.getAgentSeries().trim()+"|";
     else
        recordString=recordString+""+"|";
     //recordString=recordString+mLATreeSchema.getAgentSeries().trim()+"|";
     //推荐人名称
     if ((mLATreeSchema.getAgentGrade() != null)&&(!mLATreeSchema.getAgentGrade().equals("")))
        recordString=recordString+mLATreeSchema.getAgentGrade().trim()+"|";
     else
        recordString=recordString+""+"|";
     //组育成人
     if ((mLATreeSchema.getAgentLastSeries() != null)&&(!mLATreeSchema.getAgentLastSeries().equals("")))
        recordString=recordString+mLATreeSchema.getAgentLastSeries().trim()+"|";
     else
        recordString=recordString+""+"|";     
     //部育成人
     if ((mLATreeSchema.getAgentLastGrade() != null)&&(!mLATreeSchema.getAgentLastGrade().equals("")))
        recordString=recordString+mLATreeSchema.getAgentLastGrade().trim()+"|";
     else
        recordString=recordString+""+"|";  
     //督导育成人  
     if ((mLATreeSchema.getIntroAgency() != null)&&(!mLATreeSchema.getIntroAgency().equals("")))
        recordString=recordString+mLATreeSchema.getIntroAgency().trim()+"|";
     else
        recordString=recordString+""+"|";      
     //区域督导育成人  
     if ((mLATreeSchema.getUpAgent() != null)&&(!mLATreeSchema.getUpAgent().equals("")))
        recordString=recordString+mLATreeSchema.getUpAgent().trim()+"|";
     else
        recordString=recordString+""+"|"; 
     //调整日期
     if (mLATreeSchema.getAstartDate() != null)
        //mLATreeSchema.setAstartDate("");
        recordString=recordString+mLATreeSchema.getAstartDate().trim()+"|";
     else
        recordString=recordString+""+"|"; 
     //recordString=recordString+mLATreeSchema.getAstartDate().trim()+"|";
                        %>
		   	<script language="javascript">
        parent.fraInterface.MoveTrackGrid.addOne("MoveTrackGrid");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,1,"<%=mLATreeSchema.getAgentCode()%>");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,2,"<%=mLATreeSchema.getManageCom()%>");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,3,"<%=mLATreeSchema.getAgentGroup()%>");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,4,"<%=mLATreeSchema.getAgentSeries()%>");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,5,"<%=mLATreeSchema.getAgentGrade()%>");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,6,"<%=mLATreeSchema.getAgentLastSeries()%>");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,7,"<%=mLATreeSchema.getAgentLastGrade()%>");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,8,"<%=mLATreeSchema.getIntroAgency()%>");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,9,"<%=mLATreeSchema.getUpAgent()%>");
parent.fraInterface.MoveTrackGrid.setRowColData(<%=i-1%>,10,"<%=mLATreeSchema.getAstartDate()%>");

        var innerHTML = "";
        var innerHTMLOld = "";
        innerHTML = innerHTML +"<tr>";
        innerHTML = innerHTML +"<input type=hidden name=AgentMoveTrackInfo<%=i-1%> value=<%=recordString%> > ";
        innerHTML = innerHTML +"</tr>";

        try
         {
          //隐藏字段，用于存放信息表的数据（字符串）
           innerHTMLOld = parent.fraInterface.fm.all('spanCode').innerHTML;
           parent.fraInterface.fm.all('spanCode').innerHTML = innerHTMLOld+innerHTML;
         }
        catch(ex)
        {
          alert(ex);
        }
			</script>
			<%
  } // end of for
	
  
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>