//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();



// 查询按钮
function easyQueryClick()
{
  initWelfareGrid();	
  strSQL = "select c.indexcalno,a.codename,c.welfaretype,c.serveyear,c.summoney,c.permoney,"
         + " c.grpmoney,c.wamnt,c.wvaliddate,c.wsenddate,c.noti"
         + " from ldcode a,lawelfareinfo c"
         + " where 1=1 and a.codetype = 'aclass' and a.code=c.aclass"
         + getWherePart("c.agentcode","AgentCode")
         + getWherePart("c.indexcalno","IndexCalNo");
         
  //alert(strSQL);       

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合查询条件的数据！");
    return false;
  }

  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);

  turnPage.arrDataCacheSet = arrDataSet;
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = WelfareGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果    
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  
  getCodeName();
}


function getCodeName()  
{
  var rowNum = WelfareGrid.mulLineCount;
  var index = 0;
  var tCodeName = "";
  var tCode = "";
  for ( index=0;index<rowNum;index++ )
  {
  	tCode = WelfareGrid.getRowColData(index,3);
  	//alert("---tCode = " + tCode);
    tCodeName = qryCodeName(tCode); 	
  	//alert("---tCodeName = " + tCodeName);
  	WelfareGrid.setRowColData(index,3,tCodeName);  	
  }				
}	


function qryCodeName(tCode)
{
  var tSql = "select codename from ldcode where codetype = 'welfaretype' "
           + " and code = '" + trim(tCode) + "'";
            
  turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 1, 1);  
  
    
  //判断是否查询成功
  if (!turnPage.strQueryResult) 
    return "";
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  return arrDataSet[0][0];	
}	