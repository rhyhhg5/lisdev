<%
//程序名称：LLWorkEfficientStaSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
ReportUI
</title>
<head>
</head>
<body>
<%

String flag = "0";
String FlagStr = "";
String Content = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String tManageCom=tG.ManageCom;
String ManageCom = request.getParameter("ManageCom");
String ContNo = request.getParameter("ContNo");
String RiskCode = request.getParameter("RiskCode");
String AgentCode = request.getParameter("AgentCode");
String AgentName = request.getParameter("AgentName");
String tBranchType = request.getParameter("BranchType");
String tBranchType2 = request.getParameter("BranchType2");
String sql="select name from ldcom where comcode='"+ManageCom+"'";
String WageNoStart="";
String WageNoEnd="";
String WageNo="";
String YYMMDD = "";

ExeSQL tExeSQL = new ExeSQL();
String tName = tExeSQL.getOneValue(sql);
//设置模板名称
String FileName ="";
if (tBranchType.equals("1") && tBranchType2.equals("01"))
{

 WageNoStart = request.getParameter("StartMonth");
 WageNoEnd = request.getParameter("EndMonth");
 YYMMDD = WageNoStart.substring(0,4) + "年"+WageNoStart.substring(4,6) + "月";
 FileName ="LAWageQueryList2";
}
else if (tBranchType.equals("2") && tBranchType2.equals("01"))
{

 WageNo = request.getParameter("Month");
 YYMMDD = WageNo.substring(0,4) + "年"+WageNo.substring(4,6) + "月";
 FileName ="LAGRPWageQueryList2";
}

//System.out.println(WageNoStart+"/"+WageNoEnd+"/"+WageNo+"/"+FileName);
//判断非必录项是否为空
String ContNoFlag="0";
String RiskCodeFlag="0";
String AgentCodeFlag="0";
String AgentNameFlag="0";

if (ContNo.equals("") || ContNo==null)
 ContNoFlag="1";
if (RiskCode.equals("") || RiskCode==null) 
 RiskCodeFlag="1";
if (AgentCode.equals("") || AgentCode==null) 
 AgentCodeFlag="1";
if (AgentName.equals("") || AgentName==null) 
 AgentNameFlag="1"; 

JRptList t_Rpt = new JRptList();
String tOutFileName = "";
System.out.println(flag);
if(flag.equals("0"))
{

t_Rpt.m_NeedProcess = false;
t_Rpt.m_Need_Preview = false;
t_Rpt.mNeedExcel = true;


t_Rpt.AddVar("MakeDate", YYMMDD);
System.out.println("77");
String CurrentDate = PubFun.getCurrentDate();
System.out.println(CurrentDate);
CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
if (tBranchType.equals("1") && tBranchType2.equals("01"))
{
t_Rpt.AddVar("WageNoStart", WageNoStart);
t_Rpt.AddVar("WageNoEnd", WageNoEnd);
}
else if (tBranchType.equals("2") && tBranchType2.equals("01"))
{
t_Rpt.AddVar("WageNo", WageNo);
}
t_Rpt.AddVar("tName", tName);
t_Rpt.AddVar("ManageCom",ManageCom);
t_Rpt.AddVar("ContNo", ContNo);
t_Rpt.AddVar("RiskCode", RiskCode);
t_Rpt.AddVar("AgentCode", AgentCode);
t_Rpt.AddVar("AgentName", AgentName);
t_Rpt.AddVar("ContNoFlag", ContNoFlag);
t_Rpt.AddVar("RiskCodeFlag", RiskCodeFlag);
t_Rpt.AddVar("AgentCodeFlag", AgentCodeFlag);
t_Rpt.AddVar("AgentNameFlag", AgentNameFlag);

t_Rpt.Prt_RptList(pageContext,FileName);
tOutFileName = t_Rpt.mOutWebReportURL;
String strVFFileName = FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
String strRealPath = application.getRealPath("/web/Generated").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName;
System.out.println("strVFPathName : "+ strVFPathName);
System.out.println("=======Finshed in JSP===========");
System.out.println(tOutFileName);

response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+"&RealPath="+strVFPathName);
}
%>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{

  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >
