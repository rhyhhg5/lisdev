<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>   
<%
//程序名称：BKAgentQuery.jsp
//程序功能：
//更新记录：  更新人    更新日期     更新原因/内容
%>     
<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  String AgentCode = request.getParameter("AgentCode");
%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">	  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>	
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>	
	<SCRIPT src="BKAgentQuery.js"></SCRIPT>
	<%@include file="BKAgentQueryInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>	
	<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
</head>

<script>
  var tAgentCode = "<%=AgentCode%>";
</script>


<body onload="initForm();  parent.fraInterface.showCodeName();">
  <form  method=post name=fm target="fraSubmit">
    <Div  id= "divButton" style= "display: ''">
    <%@include file="../common/jsp/ProposalOperateButton.jsp"%>
    </DIV>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      人员具体信息
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divRiskCode0">
    <table class=common>
    <TR  class= common>        
        <TD class= title> 
          员工代码
        </TD>
        <TD  class= input> 
          <Input class= 'readonly' readonly name=AgentCode >
        </TD>       
	      <TD  class= title>
          员工姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD> 
     </TR>
     <TR  class= common>
          <TD  class= title>
              性别 
          </TD>
          <TD  class= input>
              <Input name=Sex class="code" verify="性别|code:Sex" ondblclick="return showCodeList('Sex',[this]);" 
		                                                              onkeyup="return showCodeListKey('Sex',[this]);" >
          </TD> 
          <TD  class= title>
                  出生日期 
          </TD>
          <TD  class= input>
                  <Input name=Birthday class=common  verify="出生日期|NotNull&Date"> 
          </TD>
      </TR>
      <TR  class= common>
         <TD  class= title>
          民族
         </TD>
         <TD  class= input>
          <Input name=Nationality class="code" id="Nationality" ondblclick="return showCodeList('Nationality',[this]);" 			onkeyup="return showCodeListKey('Nationality',[this]);" > 
         </TD>
         <TD  class= title> 
          籍贯
         </TD>
         <TD  class= input>
          <Input name=NativePlace class="code" verify="籍贯|code:NativePlaceBak" id="NativePlaceBak" 
			                   ondblclick="return showCodeList('NativePlaceBak',[this]);" 
			                   onkeyup="return showCodeListKey('NativePlaceBak',[this]);">
         </TD>  
       </TR>
      <TR  class= common> 
         <TD  class= title>
          户口所在地
         </TD>
         <TD  class= input> 
          <Input name=RgtAddress class="code" ondblclick="return showCodeList('NativePlaceBak',[this]);" 
		                                          onkeyup="return showCodeListKey('NativePlaceBak',[this]);"> 
         </TD>
         <TD  class= title>
           政治面貌 
         </TD>
        <TD  class= input> 
           <Input name=PolityVisage class="code" id="polityvisage"   ondblclick="return showCodeList('polityvisage',[this]);" 
		                           onkeyup="return showCodeListKey('polityvisage',[this]);" > 
        </TD> 
      </TR>
      <TR  class= common>
	        <TD  class= title>
          身份证号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common  onchange="return changeIDNo();"> 
        </TD>
          <TD  class= title>
          学历 
        </TD>
        <TD  class= input> 
          <Input name=Degree class="code" id="Degree" ondblclick="return showCodeList('Degree',[this]);" 
		                                                  onkeyup="return showCodeListKey('Degree',[this]);"> 
        </TD>      
      </TR>
      <TR  class= common>
       <TD  class= title> 
          毕业院校
        </TD>
        <TD  class= input> 
          <Input name=GraduateSchool class= common > 
        </TD>
        <TD  class= title>
          专业 
        </TD>
        <TD  class= input> 
          <Input name=Speciality class= common > 
        </TD>
       </TR>
       <TR  class= common>
        <TD  class= title>
          职称 
        </TD>
        <TD  class= input> 
          <Input name=PostTitle class='code' 	ondblclick="return showCodeList('posttitle',[this]);" 
		                                           onkeyup="return showCodeListKey('posttitle',[this]);" > 
        </TD>
        <TD  class= title>
          家庭地址 
        </TD>
        <TD  class= input> 
          <Input name=HomeAddress class= common > 
        </TD>
      </TR>
      <TR  class= common>
          <TD  class= title>
          邮政编码 
        </TD>
        <TD  class= input> 
          <Input name=ZipCode class= common > 
        </TD>
	        <TD  class= title>
          住宅电话 
        </TD>
        <TD  class= input> 
          <Input name=Phone class= common > 
        </TD>
        </TR> 
      <TR  class= common>  
        <TD  class= title> 
          传呼
        </TD>
        <TD  class= input>
          <Input name=BP class= common > 
        </TD>
        <TD  class= title>
          手机 
        </TD>
        <TD  class= input> 
          <Input name=Mobile class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          E-mail 
        </TD>
        <TD  class= input> 
          <Input name=EMail class= common > 
        </TD>
         <TD  class= title>
          原工作单位 
        </TD>
        <TD  class= input> 
          <Input name=OldCom class= common > 
        </TD>  
      </TR>  
      <TR  class= common>   
        <TD  class= title> 
          原职业 
        </TD>
        <TD  class= input> 
          <Input name=OldOccupation class='code'  ondblclick="return showCodeList('occupation',[this]);" onkeyup="return showCodeListKey('occupation',[this]);"> 
        </TD>
        <TD  class= title>
          原工作职务 
        </TD>
        <TD  class= input> 
          <Input name=HeadShip class= common  > 
        </TD>
       </TR>  
      <TR  class= common>  
        <TD  class= title>
          入司时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' verify="入司时间|notnull&Date" > 
        </TD>
        <TD  class= title>
          备注
        </TD>
        <TD  class=input> 
          <Input name=Remark class= common > 
        </TD>
      </TR>
    </table>
    </Div>
    
    <!--行政信息-->    
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
            <td class= titleImg>
                行政信息
            </td>
            </td>
    	</tr>
     </table>
     <Div id= "divLAAgent3" style= "display: ''">
       <table class=common>
    <tr class=common>
        <TD  class= title>
          岗位名称 
        </TD>
        <TD  class= input> 
          <Input name=AgentKind class='code' verify="人员类别|notnull" ondblclick="return showCodeList('agentkind',[this]);" 
                 onkeyup="return showCodeListKey('agentkind',[this]);"> 
        </TD>
        <TD class= title>
          职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="code" verify="职级|notnull&code:AgentGrade" 
		ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
		onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" > 
        </TD>
     </tr>
     <tr class=common>  
        <TD class= title>
          上级人员
        </TD>
        <TD class= input>
          <Input name=UpAgent class='readonly'readonly > 
        </TD> 
        <TD class= title>
          所属机构 
        </TD>
        <TD class= input>
          <Input class=common name=AgentGroup onchange="return changeGroup();"> 
        </TD>
    </tr>
     <tr class=common> 
        <TD class= title>
          所属渠道 
        </TD>
        <TD class= input>
          <Input class=common name=ChannelName> 
        </TD>
        <TD  class= title>
          操作员代码 
        </TD>
        <TD  class= input> 
          <Input name=Operator class= 'readonly' readonly > 
        </TD>
      </tr>
      
    </table>    
    
          
          
       
    <table>
    <tr>
      <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
            <td class= titleImg>人员职级变动信息</td>
     </td>
        
    	</tr>
    </table>
    <INPUT CLASS=common VALUE="人员职级变动查询" TYPE=button onclick="easyQueryClick1();"> 
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrade" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>			
  	</div>    
    
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <span id="spanApprove"  style="display: none; position:relative; slategray"></span>
</body>
</html>
