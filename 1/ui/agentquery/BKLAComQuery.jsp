<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>   
<%
//程序名称：BKLAComQuery.jsp
//程序功能：
//更新记录：  更新人    更新日期     更新原因/内容
%>     
<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  String AgentCom = request.getParameter("AgentCom");
%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">	  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>	
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>	
	<SCRIPT src="BKLAComQuery.js"></SCRIPT>
	<%@include file="BKLAComQueryInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>	
	<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
</head>
<script>
  var tAgentCom= "<%=AgentCom%>";
</script>
<body onload="initForm();  parent.fraInterface.showCodeName();">
  <form method=post name=fm target="fraSubmit">
    <Div  id= "divButton" style= "display: ''">
    <%@include file="../common/jsp/ProposalOperateButton.jsp"%>
    </DIV>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      代理机构详细信息
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divRiskCode0">
    <table class=common>
    <tr  class= common> 
        <td  class= title> 银行机构编码 </td>
        <td  class= input> <input class=common name=AgentCom> </td>
        <td  class= title> 银行机构名称 </td>
        <td  class= input> <input class=common  name=Name > </td>
    </tr>
      
    <tr  class= common> 
        <td  class= title> 上级代理机构 </td>
        <td  class= input> <input class=common name=UpAgentCom onchange="getComName(UpComName)"> </td>
	      <td  class= title> 上级机构名称 </td>
        <td  class= input> <input class='readonly' readonly name=UpComName > </td>   
    </tr>   
      <tr  class= common> 
        <td  class= title> 地址 </td>
        <td  class= input> <input  class= common name=Address> </td>
	      <td  class= title> 电话 </td>
        <td  class= input> <input  class=common name=Phone> </td>
      </tr>
      <tr  class= common>
        <td  class= title> 联系人 </td>
        <td  class= input> <input  class= common name=LinkMan> </td>
	      <td  class= title> 单位性质 </td>
        <td  class= input> <input   name=GrpNature class='code'
		           ondblclick="return showCodeList('GrpNature',[this]);" 
		           onkeyup="return showCodeListKey('GrpNature',[this]);"></td>
      </tr>     
      <tr  class= common>
	      <td  class= title> 银行类型 </td>
        <td  class= input> <input name=BankType class='code' ondblclick="return showCodeList('BankType',[this]);" 
		           				     onkeyup="return showCodeListKey('BankType',[this]);"> </td> 
	      <td  class= title> 销售资格 </td>
        <td  class= input> <input  name=SellFlag class= 'code'
		           ondblclick="return showCodeList('YesNo',[this]);" 
		           onkeyup="return showCodeListKey('YesNo',[this]);"> </td>
      </tr>
      <tr  class= common>
        <td  class= title> 地区类型 </td>
        <td  class= input> <input name=AreaType class='code' ondblclick="return showCodeList('AreaType',[this]);" 
		         				  onkeyup="return showCodeListKey('AreaType',[this]);"></td>
	      <td  class= title> 渠道类型 </td>
        <td  class= input> <input name=ChannelType class='code' ondblclick="return showCodeList('ChannelType',[this]);" 
		          	                	onkeyup="return showCodeListKey('ChannelType',[this]);">   	</td>
      </tr>
      <tr  class= common>
	      <td  class= title> 对应渠道组 </td>
        <td  class= input> <input class='common' name=ChangeCom> </td>
        <td  class= title> 银行机构类别 </td>
        <td  class= input> <input class= readonly readonly name=ACType > </td> 
      </tr>
      <tr class = common >
         <td  class= title> 负责网点员工代码 </td>
         <td  class= input> <input class='common' name=SiteManagerCode onchange="getManagerName(SiteManager)"> </td>
         <td  class= title> 负责网点员工姓名 </td>
         <td  class= input> <input class='readonly' readonly name=SiteManager> </td>
      </tr>
      <tr class = common >
        <td  class= title> 负责机构 </td>
        <td  class= input> <input class='code' name=ManageCom ondblclick="return showCodeList('station',[this]);"> </td> 
	      <td  class= title> 是否统计网点合格率 </td>
        <td  class= input> <input  name=CalFlag class= 'code'
		           ondblclick="return showCodeList('YesNo',[this]);" 
		           onkeyup="return showCodeListKey('YesNo',[this]);">	</td>
	    </tr>      
	    <tr class = common>
	       <td class=title> 操作员代码 </td>
	       <td class=input ><input class='readonly' readonly name=Operator> </td>
      </tr>
    </table>
    </Div>
    <Div  id= "divALL0" style= "display: 'none'">
    </Div>
    
    
    
    
    
    
   <table>
    <tr>
      <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
            <td class= titleImg>机构负责人变动信息</td>
     </td>
        
    	</tr>
    </table>
    <INPUT CLASS=common VALUE="机构负责人变动查询" TYPE=button onclick="easyQueryClick1();"> 
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrade" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>			
  	</div>     
    
    
    
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <span id="spanApprove"  style="display: none; position:relative; slategray"></span>
</body>
</html>
