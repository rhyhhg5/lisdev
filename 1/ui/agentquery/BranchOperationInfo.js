var turnPage = new turnPageClass();
var arrDataSet1;

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	var strAgentgroup=fm.all('BranchAttr').value;
	var strbranchcode=fm.all('Agentgroup').value;
	var tResultSet;
	var tStr;
	var tQueryMonth1;
	var tQueryMonth2;
	var tQueryMonth = fm.all('StartMonth').value ;
	if(tQueryMonth.length==0){alert("请输入月份！");return false;}
	
	if(tQueryMonth.length>=9)
		tQueryMonth2=tQueryMonth1=tQueryMonth;
	else
	{
		tQueryMonth1 = tQueryMonth +"-01";
		tQueryMonth2 = tQueryMonth +"-31";
	}
	
	if (strAgentgroup!="")
	{
	// 书写SQL语句
	var strSQL = "";
	/*查询管理机构*/
	strSQL = "select distinct(ManageCom) from labranchgroup where agentgroup='"+strbranchcode+"'";
	tResultSet=easyQueryVer3(strSQL,1,1,1);
	if(!tResultSet) {alert("没有管理机构！");return false;}
	/*查询总单数，交易金额，直接佣金*/
	strSQL = "select count(PolNo),sum(TransMoney) ,sum(DirectWage) from LACommision  where 1=1 and branchcode in(select agentgroup from labranchgroup where substr(branchattr,1,"+strAgentgroup.length+")="
		+strAgentgroup+")"
		+ " and TO_CHAR(tconfdate,'YYYY-MM-DD') >= '"+tQueryMonth1 
	         + "' and TO_CHAR(tconfdate,'YYYY-MM-DD') <= '"+tQueryMonth2 +"'";  
//	alert(strSQL);
	tStr=easyQueryVer3(strSQL, 1, 1, 1);
	if(!tStr){alert("没有交易记录！");return false;}
	turnPage.strQueryResult  = tResultSet+"|"+tStr.substring(4,tStr.length);  
  	/*查询新保单数等*/
  	strSQL = "select count(PolNo),sum(TransMoney) from lacommision where 1=1 and branchcode in(select agentgroup from labranchgroup where substr(branchattr,1,"+strAgentgroup.length+")="+strAgentgroup+") and paycount='0'"
		+ " and TO_CHAR(tconfdate,'YYYY-MM-DD') >= '"+tQueryMonth1 
	        + "' and TO_CHAR(tconfdate,'YYYY-MM-DD') <= '"+tQueryMonth2 +"'"  ;
	tStr=easyQueryVer3(strSQL, 1, 1, 1);
		
	turnPage.strQueryResult=turnPage.strQueryResult+"|"+tStr.substring(4,tStr.length);
	/*查询续保单数等*/
	strSQL = "select count(PolNo),sum(TransMoney) from lacommision where 1=1 and branchcode in(select agentgroup from labranchgroup where substr(branchattr,1,"+strAgentgroup.length+")="+strAgentgroup+") and paycount!='0'"
		+ " and TO_CHAR(tconfdate,'YYYY-MM-DD') >= '"+tQueryMonth1 
	        + "' and TO_CHAR(tconfdate,'YYYY-MM-DD') <= '"+tQueryMonth2 +"'";  

	tStr=easyQueryVer3(strSQL, 1, 1, 1);
	turnPage.strQueryResult=turnPage.strQueryResult+"|"+tStr.substring(4,tStr.length);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,4,6,2,5,7,3]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = commisionGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  }
}