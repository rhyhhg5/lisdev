<%
//程序名称：LAAssessInfoQueryInit.jsp
//程序功能：
//创建日期：2003-08-05 10:07
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('ManageCom').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGrade').value = '';    
    fm.all('StartDate').value = '';
    fm.all('EndDate').value = '';
    fm.all('BranchType').value = getBranchType();
    
  }
  catch(ex)
  {
    alert("在LAIndexTempQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();  
  }
  catch(re)
  {
    alert("LAAssessInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>