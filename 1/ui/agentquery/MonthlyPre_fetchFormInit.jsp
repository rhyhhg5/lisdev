<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：MonthlyPre_fetchFormInput.jsp
//程序功能：
//创建日期：2015-01-21
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	PubFun tpubFun = new PubFun();
	String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<script src="../common/javascript/Common.js"></script>
<script language="JavaScript">

	function initInpBox(){
		try{
			fm.all('ManageCom').value = <%=strManageCom%>;
		    if(fm.all('ManageCom').value==86){
		    	fm.all('ManageCom').readOnly=false;
		    } else{
		    	fm.all('ManageCom').readOnly=true;
		    }
		    if(fm.all('ManageCom').value!=null){
		    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
		        //显示代码选择中文
		        if (arrResult != null){
		        	fm.all('ManageComName').value=arrResult[0][0];
		        } 
		    }

		}catch(ex){
			alert("在MonthlyPre_fetchFormInit.jsp-->IninInpBox函数中发生异常 ：初始化界面错误！");
		}
	}
	
	function initForm(){
		try{
			initInpBox();
			initMonFetchGrid();
			showAllCodeName();
		}catch(re){
			alert("MonthlyPre_fetchFormInit.jsp-->InitForm函数中发生异常:初始化界面错误");
		}
	}
	
	function initMonFetchGrid(){
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "管理机构";
			iArray[1][1] = "40px";
			iArray[1][2] = 30;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "机构名称";
			iArray[2][1] = "50px";
			iArray[2][2] = 100;
			iArray[2][3] = 0;
			
			iArray[3] = new Array();
			iArray[3][0] = "方案类别(A/B/C/D)";
			iArray[3][1] = "50px";
			iArray[3][2] = 50;
			iArray[3][3] = 0;
			
			iArray[4] = new Array();
			iArray[4][0] = "预算年月";
			iArray[4][1] = "50px";
			iArray[4][2] = 100;
			iArray[4][3] = 0;
			
			iArray[5] = new Array();
			iArray[5][0] = "至少合格力人数";
			iArray[5][1] = "50px";
			iArray[5][2] = 50;
			iArray[5][3] = 0;
			
			iArray[6] = new Array();
			iArray[6][0] = "上月在职人数";
			iArray[6][1] = "50px";
			iArray[6][2] = 50;
			iArray[6][3] = 0;
			
			iArray[7] = new Array();
			iArray[7][0] = "当月在职人数";
			iArray[7][1] = "50px";
			iArray[7][2] = 50;
			iArray[7][3] = 0;
			
			iArray[8] = new Array();
			iArray[8][0] = "月均平均人力数";
			iArray[8][1] = "50px";
			iArray[8][2] = 50;
			iArray[8][3] = 0;
			
			iArray[9] = new Array();
			iArray[9][0] = "月合格人力数";
			iArray[9][1] = "50px";
			iArray[9][2] = 50;
			iArray[9][3] = 0;

			iArray[10] = new Array();
			iArray[10][0] = "截至本月累计净增有效人力";
			iArray[10][1] = "50px";
			iArray[10][2] = 50;
			iArray[10][3] = 0;
			
			iArray[11] = new Array();
			iArray[11][0] = "本月净增有效人力";
			iArray[11][1] = "50px";
			iArray[11][2] = 50;
			iArray[11][3] = 0;
			
			iArray[12] = new Array();
			iArray[12][0] = "净增有效人力标准";
			iArray[12][1] = "50px";
			iArray[12][2] = 50;
			iArray[12][3] = 0;
			
			iArray[13] = new Array();
			iArray[13][0] = "截至本月累计有效人力达成率%";
			iArray[13][1] = "50px";
			iArray[13][2] = 50;
			iArray[13][3] = 0;
			
			iArray[14] = new Array();
			iArray[14][0] = "月获得发展费用";
			iArray[14][1] = "50px";
			iArray[14][2] = 50;
			iArray[14][3] = 0;
			
			MonFetchGrid = new MulLineEnter("fm","MonFetchGrid");
		     
			MonFetchGrid.mulLineCount = 10;   
			MonFetchGrid.displayTitle = 1;
			MonFetchGrid.canSel = 1;
			MonFetchGrid.canChk = 0;
			MonFetchGrid.locked = 1;
			MonFetchGrid.hiddenSubtraction = 1;
			MonFetchGrid.hiddenPlus = 1;
			MonFetchGrid.selBoxEventFuncName ="";
		     
			MonFetchGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}	
	
</script>