<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：GoodstartAddValidHumanRankingInit.jsp
//程序功能：
//创建日期：2015-1-22 
//创建人  ：CZ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="Javascript">
	var strSQL = "";
	function initInpBox(){
		try{
			fm.all('ManageCom').value = <%=strManageCom%>;
		    if(fm.all('ManageCom').value==86){
		    	fm.all('ManageCom').readOnly=false;
		    } else{
		    	fm.all('ManageCom').readOnly=true;
		    }
		    if(fm.all('ManageCom').value!=null){
		    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
		        //显示代码选择中文
		        if (arrResult != null){
		        	fm.all('ManageComName').value=arrResult[0][0];
		        } 
		    }

		
		}catch(ex){
			alert("在GoodstartAddValidHumanRankingInit.jsp-->initInpBox函数中发生异常 ：初始化界面错误！");
		}
	}
	
	function initForm(){
		try{
			initInpBox();
			initExplainGrid();
			showAllCodeName();
		}catch(re){
			alert("GoodstartAddValidHumanRankingInit.jsp-->initForm函数中发生异常:初始化界面错误");
		}
	}
	
	function initExplainGrid(){
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "排名";
			iArray[1][1] = "30px";
			iArray[1][2] = 10;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "分公司";
			iArray[2][1] = "60px";
			iArray[2][2] = 10;
			iArray[2][3] = 0;
			
			iArray[3] = new Array();
			iArray[3][0] = "管理机构";
			iArray[3][1] = "60px";
			iArray[3][2] = 100;
			iArray[3][3] = 0;			        
			        
			iArray[4] = new Array();
			iArray[4][0] = "新增合计合格人力";
			iArray[4][1] = "60px";
			iArray[4][2] = 50;
			iArray[4][3] = 0;
			
			iArray[5] = new Array();
			iArray[5][0] = "奖励";
			iArray[5][1] = "60px";
			iArray[5][2] = 50;
			iArray[5][3] = 0;

					
			
			ExplainGrid = new MulLineEnter("fm","ExplainGrid");
		     
			ExplainGrid.mulLineCount = 10;   
			ExplainGrid.displayTitle = 1;
			ExplainGrid.canSel = 0;
			ExplainGrid.canChk = 0;
			ExplainGrid.locked = 1;
			ExplainGrid.hiddenSubtraction = 1;
			ExplainGrid.hiddenPlus = 1;
			ExplainGrid.selBoxEventFuncName ="";
		     
			ExplainGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>