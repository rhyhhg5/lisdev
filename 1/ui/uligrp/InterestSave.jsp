<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：InterestSave.jsp
//程序功能：保证汇率的设定
//创建日期：2007-05-17 14:56:57
//创建人  ：李磊
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.uligrp.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  Interest000001Schema tInterest000001Schema   = new Interest000001Schema();  
  Interest000001Schema tInterest000001Schema1   = new Interest000001Schema();
  Interest000001Set tInterest000001Set = new Interest000001Set();
  InterestUI tInterestUI   = new InterestUI();
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    String Operator  = tG.Operator ;  //保存登陆管理员账号
    
    tInterest000001Schema.setRiskCode(request.getParameter("RiskCode"));
    tInterest000001Schema.setPromulgateDate(request.getParameter("PromulgateDate"));
    //tInterest000001Schema.setStartDate(request.getParameter("StartDate"));
    //tInterest000001Schema.setEndDate(request.getParameter("EndDate"));
    String BalaMonth = request.getParameter("BalaMonth");//BalaMonth
    String tStartBalaDate = BalaMonth + "-1";
	String tEndDate = PubFun.calDate(tStartBalaDate,1,"M",tStartBalaDate);
	tInterest000001Schema.setStartDate(tStartBalaDate);
    tInterest000001Schema.setEndDate(tEndDate);
    tInterest000001Schema.setRateType(request.getParameter("RateType"));
    tInterest000001Schema.setRateIntv(request.getParameter("RateIntv"));
    String GrpContNo = request.getParameter("GrpContNo");
    if(!"".equals(GrpContNo) && GrpContNo != null){
    	tInterest000001Schema.setGrpContNo(GrpContNo);
    }else{
    	tInterest000001Schema.setGrpContNo("000000");
    }
    
    if(transact.equals("DELETE"))
    {
    }
    else
    {
   	tInterest000001Schema.setRate(request.getParameter("Rate"));
   	tInterest000001Schema.setOperator(request.getParameter("operator"));
   	tInterest000001Schema.setMakeDate(request.getParameter("MakeDate"));
   	tInterest000001Schema.setMakeTime(request.getParameter("MakeTime"));
   	tInterest000001Schema.setModifyDate(request.getParameter("ModifyDate"));
   	tInterest000001Schema.setModifyTime(request.getParameter("ModifyTime"));
    }
    tInterest000001Set.add(tInterest000001Schema);
     if(transact.equals("UPDATE"))  
    {
   	 tInterest000001Schema1.setRiskCode(request.getParameter("DRiskCode"));
   	 tInterest000001Schema1.setStartDate(tStartBalaDate);
   	 tInterest000001Schema1.setEndDate(tEndDate);  
   	 tInterest000001Set.add(tInterest000001Schema1);
	}

  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tInterest000001Set);
  	tVData.add(tG);
  	tInterestUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tInterestUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
 
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>