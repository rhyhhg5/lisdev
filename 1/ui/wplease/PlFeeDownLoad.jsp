<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：downLoad.jsp
	//程序功能：用户手册,模板下载界面
	//创建日期：2007-11-23
	//创建人  ：shaoax
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
	<%
		GlobalInput tGI = new GlobalInput();
		tGI = (GlobalInput) session.getValue("GI");
	%>
	<script>
		var operator = "<%=tGI.Operator%>";   //记录操作员
		var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
		var fileType1 = "";
	</script>

	<head>
		<meta http-equiv="Content-Type" content="text/html charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="./PlFeeDownLoad.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="./PlFeeDownLoadInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form method=post name=fm target="fraSubmit">
						
			<table class=common>
			<tr>
			<td class=title >
			<hr>
						职场费用模板下载
			<span style="color:red">请注意：支付日期录入格式YYYY-MM-DD，摊销起止期格式YYYY-MM</span>
			</td>
				
			</tr>
			<tr>
			
			<td>
				<!--div id="filesList" style="margin-top:10px;"></div-->
					
				<input value="下  载" class=cssButton type=button onclick="downLoad();">
			</tr></td>
			</table>
			<!--div><a href="../temp/upload/3_8.rar">3_8.rar</a></div-->			
		</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>

