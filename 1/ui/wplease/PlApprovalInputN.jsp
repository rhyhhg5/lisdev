<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//name :PlApprovalInputN.jsp
	//function :
	//Creator :huodonglei
	//date :2011-6-8
	// 鞠成富  2012-09-06 修改
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="./PlApprovalInput.js"></SCRIPT>
<SCRIPT src="./PlShouBackFee.js"></SCRIPT>
<SCRIPT src = "ModifyInputMain.js"></SCRIPT> 
<SCRIPT src = "PlUpdateFee.js"></SCRIPT> 
<%@include file="./PlApprovalInit.jsp"%>
</head>
<body onload="initElementtype();initForm()">
	<form action="PlApprovalSave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<tr class=common>
				<td class=titleImg >待审核职场列表</td>
			</tr>
			<tr class=common>
				<td text-align: left colSpan=1>
					<span id="spanApproveGrid"></span>
				</td>
			</tr>
		</table>
		<Div id= "divPage" align=center style= "display: 'none' ">
			<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
		<table class=common>
			<tr class=common>
				<td colspan="2"><INPUT VALUE="确认" class=cssButton TYPE=button
					onclick="placeIn()"></td>
			</tr>
		</table>
		<div id="divPlaceIn1" style="display: none">
          <jsp:include page="ModifyInputMain.jsp" flush="true"/>
		  <br>
		  <input type=button value='批文下载' onclick='downloadmode();' class=cssButton>  
		  <input type=hidden name=filename>
		  <input type=hidden name=filepath>
		  <input type=hidden name=CTSerialno>
	      <input type=hidden name=codeno>
	      <input type=hidden name=appstate>
	      <input type=hidden name=appstateType value='4'>
	      <input type=hidden name=Placenorefer> 
          <input type=hidden name=flag>
	      <input type=hidden name=CurrentDate>
	      <input type=hidden name=shouldRenFee>
	      <input type=hidden name=shouldProFee>
	      <input type=hidden name=shouldDecFee>
			<br> 
		</div>

		<div id="divPlaceIn2" style="display: none">
			<hr>
			<table class=common>
			  	<tr class=common>
			      <td class=title>原职场编码</td>
			      <TD class=input><Input name=PreChanPlaceno class=common readonly></TD>
			    </tr>
			   <tr class=common>
			      <td class=title>停用时间</td>
			      <TD class=input><Input name=StopDate class='coolDatePicker' elementtype=nacessary dateFormat='short'></TD>
			    </tr>
			    <tr class=common>
			      <TD class=title>合同违约金</TD>
				  <TD class=input><Input class=common name=SWeiYueFee id="SWeiYueFee" elementtype=nacessary ></TD>
				  <Td class=title>实际违约金</Td>
				  <Td class=input><input class=common name=AWeiYueFee id="AWeiYueFee"  elementtype=nacessary ></Td>
				  <Td class=title>支付时间</Td>
				  <TD class=input><Input name=AWeiYueDate class='coolDatePicker' elementtype=nacessary dateFormat='short' ></TD>
				</tr>
				<tr class=common>
				  <Td class=title>合同保证金</Td>
				  <Td class=input><input class=common name=BaoZhFee id="BaoZhFee"  elementtype=nacessary ></Td>
				  <Td class=title>收回时间</Td>
				  <TD class=input><Input name=BaoZhDate class='coolDatePicker' dateFormat='short' elementtype=nacessary ></TD>
				  <td class=title></td>
				  <td class=input></td>
			    </tr>
			    <tr class=common>
				    <td class=title>系统计算应收已付金额</td>
				    <Td class=input><input class=common name=SYingFee id="SYingFee"  elementtype=nacessary></Td>
				    <td class=title>实际应收已付金额</td>
				    <Td class=input><input class=common name=AYingFee id="AYingFee"  elementtype=nacessary></Td>
				    <Td class=title>收回时间</Td>
					<TD class=input><Input name=YingDate class='coolDatePicker' dateFormat='short' elementtype=nacessary></TD>
			    <tr>
			  </table>
			</div>
		<div id="divPlaceIn4" style="display: none">
			<hr>
			<table class=common>
			    <tr class=common>
			      <td class=title>原职场编码</td>
			      <TD class=input><Input name=PreNextPlaceno class=common readonly></TD>
			    </tr>
				<tr class=common>
				  <Td class=title>合同保证金</Td>
				  <Td class=input><input class=common name=XBaoZhFee id="XBaoZhFee"  elementtype=nacessary ></Td>
				  <Td class=title>收回时间</Td>
				  <TD class=input><Input name=XBaoZhDate class='coolDatePicker' dateFormat='short' elementtype=nacessary ></TD>
				  <td class=title></td>
				  <td class=input></td>
			    </tr>
			  </table>
			</div>
		</form>
		<div id="divPlaceInFile" style="display: none">
		<form action="PlInFeeSave.jsp" method=post name=feeimp target="fraSubmit" ENCTYPE="multipart/form-data">
		<hr>
		    <jsp:include page="PlUpdateFee.jsp" flush="true"/>
		</form>
		<form action="PlInFileSave.jsp" method=post name=imp target="fraSubmit" ENCTYPE="multipart/form-data">
		<hr>
       <Table class= common>
  	    <TR class= common>
  	 	    <TD class= title>
			    文件名
			</TD>     
			<TD>
			  <Input type="file" name=impFileName class=common>
			  <INPUT VALUE="上载租赁合同及批文" class=cssButton TYPE=button onclick="fileImp();">
			</TD>
			<TD>
			</TD>
		</TR>
	   </Table>
	<span style="color:red">文件请不要超过4M</span>
     <br></br>
    </form>
    <div>
			<div id="divPlaceIn3" style="display: none">
			    <hr>
		
			    <Table class= common >
        			<tr class= common>
          			 	 <td class=title>
              	  			原因（审批不确认时必须填写） 
          			 	 </td>
         		  		 <td>
				    		<textarea name="Rremark" cols="100%" rows="4"  class="common">
				    		
				    		</textarea>
						</td>
      				</tr>
   				</Table>
		   
			    <br>
         	    <input type=button class=cssButton value="确认修改" onclick="RealChange()">

		    </div>
		<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>