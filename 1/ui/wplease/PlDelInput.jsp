<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//程序名称：PlUnlockInput.jsp
	//程序功能：
	//创建日期：2012-09-14
	//创建人  ：鞠成富
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="PlDelInput.js"></SCRIPT>
<%@include file="PlDelInit.jsp"%>
</head>
<body onload="initElementtype();initForm()">
	<form action="PlDelSave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<tr class=common>
				<td class=title>职场编码</td>
				<td><Input class=common name="DelPlaceNo" id="DelPlaceNo"
					verify="职场编码|NOTNULL" elementtype=nacessary></td>
			</tr>
			<tr class=common>
				<td colspan="2"><INPUT VALUE="确认" class=cssButton TYPE=button
					onclick="placeIn()"></td>
			</tr>
		</table>
		<font color="red" size="4px">说明：该功能限制只能删除叶子节点职场<font >
		<hr>
		<div id="divInfos" style="display: none">
		    <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divInfo);"></td>
					<td class=titleImg>原职场信息</td>
				</tr>
			</table>
			<div id="divInfo" style="display: ''">
			  <table class=common>
			    <tr class=common>
			      <TD class=title>公司名称</TD>
				  <TD class=input><Input class=common name=DelName id="DelName" readonly></TD>
				  <Td class=title>总面积</Td>
				  <Td class=input><input class=common name=DelArea id="DelArea"  readonly></Td>
				  <Td class=title>租期起期</Td>
				  <TD class=input><Input name=DelStartDate class=common  readonly></TD>
				</tr>
				<tr class=common>
				  <Td class=title>租期止期</Td>
				  <TD class=input><Input name=DelEndDate class=common  readonly></TD>
				  <Td class=title>地址</Td>
				  <Td class=input colspan="3"><input class=common name=DelAddress id="DelAddress"  readonly></Td>
			    </tr>
			  </table>
			</div>
    <br>
    <INPUT VALUE="删除" class=cssButton TYPE=button onclick="submitForm()">
    <Input name="ComCode" type=Hidden  value="<%=Comcode %>">
		</div>
		</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>