var showInfo;
var ImportPath;
var turnPage = new turnPageClass();
var rantSqlturnPage = new turnPageClass();
var profeeSqlturnPage = new turnPageClass();
var decorationSqlturnPage = new turnPageClass();

//显示列表
function modifyInit(comCode)
{
//	var modifyCheck="select placeno,Appno,begindate,actualenddate,operator,makedate from liplacerentinfo where islock='01' and managecom like '"+comCode+"%'";
	var modifyCheck="select placeno,Appno,begindate,actualenddate,operator,makedate from liplacerentinfo a where a.islock='01' and a.managecom like '"+comCode+"%'"
	+" and not exists(select 1 from LIPlaceChangeTrace where Placenorefer=a.placeno and Changestate in('01','02') and Changetype in('02','03'))"
	+" and not exists(select 1 from LIPlaceChangeTrace where Placeno=a.placeno and Changestate in('01','02') and Changetype in('04','05','06'))";
	turnPage.queryModal(modifyCheck, ModifyGrid);
}
function submitForm() 
{
	if(beforeSubmit()){
		if(sumFee()){
			if(verifyInput()){
				var r = confirm("请仔细核对您录入的信息!");
				if(r==true){
					var i = 0;
					var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
					var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
					showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
					fm.submit();
				}	else{
					return false;
				}
			}
		}
	}
}

function beforeSubmit() {
	var address = fm.Address.value;
	if(!testAddress(address)){
		return false;
	};
    sumArea();
	var StartDate = document.getElementsByName("Begindate")[0].value;
	var endData = document.getElementsByName("Enddate")[0].value;
	if (!checkdate(StartDate, endData)) {
		return false;
	}
	if(fm.Otherarea.value != "" && fm.Otherarea.value !=0)
	{
		if(fm.Explanation.value=="")
		{
			alert("其他面积不为空且大于零时需填写说明！");
			return false;
		}
	}		
	if (fm.all("HidFileName").value == null
			|| fm.all("HidFileName").value == "") {
		alert("请先上传批文");
		return false;
	}
	return true;

}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	initForm();
}

// 查询职场信息并返回到前台页面
function showPlaceInfo() 
{
	var codeno = fm.selectNO.value;
	var infoSql="select Placeno,Managecom,case length(Appno) when 13 then substr(Appno, 1, 3) else substr(Appno, 1, 2) end, case length(Appno) when 13 then substr(Appno, 4, 4) else substr(Appno, 3, 4) end,case length(Appno) when 13 then substr(Appno, 8, 4) else substr(Appno, 7, 4) end,case length(Appno) when 13 then substr(Appno, 12, 2) else substr(Appno, 11, 2) end,Comlevel,case Comlevel when '1' then '省级分公司' when '2' then '地市级分公司' when '3' then '四级机构-区' when '4' then '四级机构-县' end,Comname,Begindate,Enddate,Address,Lessor,Houseno,Province,City,Isrentregister,case Isrentregister when '1' then '是' when '0' then '否' end," +
	"Islessorassociate,case Islessorassociate when '1' then '是' when '0' then '否' end,Islessorright,case Islessorright when '1' then '是' when '0' then '否' end,Ishouserent,case Ishouserent when '1' then '是' when '0' then '否' end,Isplaceoffice,case Isplaceoffice when '1' then '是' when '0' then '否' end,Ishousegroup,case Ishousegroup when '1' then '是' when '0' then '否' end," +
	"Ishousefarm,case Ishousefarm when '1' then '是' when '0' then '否' end,Sigarea,Grparea,Bankarea,Healtharea,Servicearea,Jointarea,Manageroffice,Departpersno,Planfinance,Publicarea,Otherarea,(case when Explanation is null then '0' else Explanation end),Marginfee,Marginpaydate,Penaltyfee,Remake,Filename,Filepath" +
	" from LIPlaceRentInfo where placeno='"+fm.selectNO.value+"'";
	turnPage.strQueryResult = easyQueryVer3(infoSql, 1, 1, 1);
	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	//基本信息
	fm.Placeno.value=turnPage.arrDataCacheSet[0][0];
	fm.Managecom.value=turnPage.arrDataCacheSet[0][1];
	fm.ApprovalType.value=turnPage.arrDataCacheSet[0][2];
	fm.ApprovalYear.value=turnPage.arrDataCacheSet[0][3];
	fm.ApprovalCode.value=turnPage.arrDataCacheSet[0][4];
	fm.ApprovalSeria.value=turnPage.arrDataCacheSet[0][5];
//	document.all.Managecom.readonly=true;
//	document.all.ApprovalType.readonly=true;
//	document.all.ApprovalYear.readonly=true;
//	document.all.ApprovalCode.readonly=true;
//	document.all.ApprovalSeria.readonly=true;
	fm.Comlevel.value=turnPage.arrDataCacheSet[0][6];
	fm.Comlevelname.value=turnPage.arrDataCacheSet[0][7];
	fm.Comname.value=turnPage.arrDataCacheSet[0][8];
	fm.Begindate.value=turnPage.arrDataCacheSet[0][9];
	fm.Enddate.value=turnPage.arrDataCacheSet[0][10];
	fm.Address.value=turnPage.arrDataCacheSet[0][11];
	fm.Lessor.value=turnPage.arrDataCacheSet[0][12];
	fm.Houseno.value=turnPage.arrDataCacheSet[0][13];
	fm.Province.value=turnPage.arrDataCacheSet[0][14];
	fm.City.value=turnPage.arrDataCacheSet[0][15];
	fm.Isrentregister.value=turnPage.arrDataCacheSet[0][16];
	fm.IsRentRegisterName.value=turnPage.arrDataCacheSet[0][17];
	fm.Islessorassociate.value=turnPage.arrDataCacheSet[0][18];
	fm.IslessorassociateName.value=turnPage.arrDataCacheSet[0][19];
	fm.Islessorright.value=turnPage.arrDataCacheSet[0][20];
	fm.IslessorrightName.value=turnPage.arrDataCacheSet[0][21];
	fm.Ishouserent.value=turnPage.arrDataCacheSet[0][22];
	fm.IshouserentName.value=turnPage.arrDataCacheSet[0][23];
	fm.Isplaceoffice.value=turnPage.arrDataCacheSet[0][24];
	fm.IsplaceofficeName.value=turnPage.arrDataCacheSet[0][25];
	fm.Ishousegroup.value=turnPage.arrDataCacheSet[0][26];
	fm.IshousegroupName.value=turnPage.arrDataCacheSet[0][27];
	fm.Ishousefarm.value=turnPage.arrDataCacheSet[0][28];
	fm.IshousefarmName.value=turnPage.arrDataCacheSet[0][29];
	//职场面积
	fm.Sigarea.value=turnPage.arrDataCacheSet[0][30];
	fm.Grparea.value=turnPage.arrDataCacheSet[0][31];
	fm.Bankarea.value=turnPage.arrDataCacheSet[0][32];
	fm.Healtharea.value=turnPage.arrDataCacheSet[0][33];
	fm.Servicearea.value=turnPage.arrDataCacheSet[0][34];
	fm.Jointarea.value=turnPage.arrDataCacheSet[0][35];
	fm.Manageroffice.value=turnPage.arrDataCacheSet[0][36];
	fm.Departpersno.value=turnPage.arrDataCacheSet[0][37];
	fm.Planfinance.value=turnPage.arrDataCacheSet[0][38];
	fm.Publicarea.value=turnPage.arrDataCacheSet[0][39];	
	fm.Otherarea.value=turnPage.arrDataCacheSet[0][40];
	fm.Explanation.value=turnPage.arrDataCacheSet[0][41];
	//合同违约金及保证金
	fm.Marginfee.value=turnPage.arrDataCacheSet[0][42];
	fm.Marginpaydate.value=turnPage.arrDataCacheSet[0][43];
	fm.Penaltyfee.value=turnPage.arrDataCacheSet[0][44];
	//备注信息
	fm.Remark.value=turnPage.arrDataCacheSet[0][45];
	//上传文件名、路径
	fm.HidFileName.value=turnPage.arrDataCacheSet[0][46];
	fm.HidFilePath.value=turnPage.arrDataCacheSet[0][47];
	//计算租金，物业费，装修费
	showMoneyInfo();
}
function showMoneyInfo() {
	var rantSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '"
			+ fm.selectNO.value + "' and feetype = '01' order by paybegindate";
	var profeeSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '"
			+ fm.selectNO.value + "' and feetype = '02' order by paybegindate";
	var decorationSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '"
			+ fm.selectNO.value + "' and feetype = '03' order by paybegindate";

	var sumrantSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '"
			+ fm.selectNO.value + "' and feetype = '01'";
	var sumprofeeSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '"
			+ fm.selectNO.value + "' and feetype = '02'";
	var sumdecorationSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '"
			+ fm.selectNO.value + "' and feetype = '03'";

	//租金
	rantSqlturnPage.strQueryResult = easyQueryVer3(rantSql, 1, 1, 1);
	rantSqlturnPage.arrDataCacheSet = clearArrayElements(rantSqlturnPage.arrDataCacheSet);
	rantSqlturnPage.arrDataCacheSet = decodeEasyQueryResult(rantSqlturnPage.strQueryResult);
	for(var Relate=0;Relate<rantSqlturnPage.arrDataCacheSet.length;Relate=Relate+1){
	   RelateGrid.addOne();
	   RelateGrid.setRowColData(Relate,1,rantSqlturnPage.arrDataCacheSet[Relate][0]);
	   RelateGrid.setRowColData(Relate,2,rantSqlturnPage.arrDataCacheSet[Relate][1]);
	   RelateGrid.setRowColData(Relate,3,rantSqlturnPage.arrDataCacheSet[Relate][2]);
	   RelateGrid.setRowColData(Relate,4,rantSqlturnPage.arrDataCacheSet[Relate][3]);
	};
    //物业费
	profeeSqlturnPage.strQueryResult = easyQueryVer3(profeeSql, 1, 1, 1);
	profeeSqlturnPage.arrDataCacheSet = clearArrayElements(profeeSqlturnPage.arrDataCacheSet);
	profeeSqlturnPage.arrDataCacheSet = decodeEasyQueryResult(profeeSqlturnPage.strQueryResult);
	for(var Property=0;Property<profeeSqlturnPage.arrDataCacheSet.length;Property=Property+1){
       PropertyGrid.addOne();
       PropertyGrid.setRowColData(Property,1,profeeSqlturnPage.arrDataCacheSet[Property][0]);
       PropertyGrid.setRowColData(Property,2,profeeSqlturnPage.arrDataCacheSet[Property][1]);
       PropertyGrid.setRowColData(Property,3,profeeSqlturnPage.arrDataCacheSet[Property][2]);
       PropertyGrid.setRowColData(Property,4,profeeSqlturnPage.arrDataCacheSet[Property][3]);
	};
    //装修费
    decorationSqlturnPage.strQueryResult = easyQueryVer3(decorationSql, 1, 1, 1);
    decorationSqlturnPage.arrDataCacheSet = clearArrayElements(decorationSqlturnPage.arrDataCacheSet);
    decorationSqlturnPage.arrDataCacheSet = decodeEasyQueryResult(decorationSqlturnPage.strQueryResult);
    for(var Decoration=0;Decoration<decorationSqlturnPage.arrDataCacheSet.length;Decoration=Decoration+1){
       DecorationGrid.addOne();
       DecorationGrid.setRowColData(Decoration,1,decorationSqlturnPage.arrDataCacheSet[Decoration][0]);
       DecorationGrid.setRowColData(Decoration,2,decorationSqlturnPage.arrDataCacheSet[Decoration][1]);
       DecorationGrid.setRowColData(Decoration,3,decorationSqlturnPage.arrDataCacheSet[Decoration][2]);
       DecorationGrid.setRowColData(Decoration,4,decorationSqlturnPage.arrDataCacheSet[Decoration][3]);
    };
    
	turnPage.strQueryResult = easyQueryVer3(sumrantSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumRenFee.value = turnPage.arrDataCacheSet[0][0];

	turnPage.strQueryResult = easyQueryVer3(sumprofeeSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumProFee.value = turnPage.arrDataCacheSet[0][0];

	turnPage.strQueryResult = easyQueryVer3(sumdecorationSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumDecFee.value = turnPage.arrDataCacheSet[0][0];


}

function placeIn() 
{
    var placeIn1 = document.getElementById("divPlaceIn1");
	var placeIn2 = document.getElementById("divPlaceIn2");
	var placeInFile = document.getElementById("divPlaceInFile");
	placeIn1.style.display = 'none';
	placeIn2.style.display = 'none';
	placeInFile.style.display = "none";
	
	var tsel = ModifyGrid.getSelNo();
	if (tsel == null || tsel == "") {
		alert("请先选择一条数据！");
		return false;
	}	
	fm.selectNO.value = ModifyGrid.getRowColData(tsel - 1, 1);
	if (fm.selectNO.value == null || fm.selectNO.value == "") {
		alert("职场编码不能为空！");
		return false;
	}
	var isLookSQL = "select 1 from LIPlaceRentinfo where placeno='"+fm.selectNO.value+"' and Islock='01' ";
	turnPage.strQueryResult  = easyQueryVer3(isLookSQL, 1, 1, 1);  
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
	  	alert("请先解锁该职场编码！");
	    return false;
	}

	var isChanging = "select 1 from LIPlaceRentinfo where Placeno='" + fm.selectNO.value
	+"' and not exists(select 1 from LIPlaceChangeTrace where Placenorefer='" + fm.selectNO.value+ "' and Changestate in('01','02') and Changetype in('02','03'))"
	+" and not exists(select 1 from LIPlaceChangeTrace where Placeno='" + fm.selectNO.value+ "' and Changestate in('01','02') and Changetype in('04','05','06'))";

	
	turnPage.strQueryResult = easyQueryVer3(isChanging, 1, 1, 1);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("职场编码不存在或者正处于审批中，不能执行修改操作！");
		return false;
	}
	placeIn1.style.display = '';
	placeIn2.style.display = '';
	placeInFile.style.display = '';
	showPlaceInfo();
}
function checkdate(SDate, EData) {
	if (SDate == null || SDate == "") {
		alert("请输入租期起期");
		return false;
	}
	if (EData == null || EData == "") {
		alert("请输入租期止期");
		return false;
	}
	var aDate = EData.split("-");
	var oDate1 = new Date(aDate[1] + '-' + aDate[2] + '-' + aDate[0]); // 结束日期
	aDate = SDate.split("-");
	var oDate2 = new Date(aDate[1] + '-' + aDate[2] + '-' + aDate[0]); // 开始日期
	if (oDate1 <= oDate2) {
		alert("'租期止期'必须大于'租期起期'!");
		return false;
	} else {
		return true;
	}
}

//上传附件
function fileImp() {
	if (imp.all('impFileName').value ==null || imp.all("impFileName").value == "") {
		alert("请选择需要上传的文件");
		return false;
	}

	var i = 0;
	var ImportFile = imp.all('impFileName').value.toLowerCase();
	var Placeno = document.getElementsByName("Placeno")[0].value;
	if (Placeno == null || Placeno == "") {
		alert("编码信息尚未生成，请先录入其他信息！");
		return false;
	}
	getPath();
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	imp.action = "./PlInFileSave.jsp?ImportPath=" + ImportPath + "&ImportFile="+ ImportFile + "&CodeId=" + Placeno;
	imp.submit(); // 提交

}

function getPath() {
	// 书写SQL语句
	var strSQL = "select SysvarValue from ldsysvar where sysvar ='ApprovalPath'";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有找到上传路径");
		return false;
	}
	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	ImportPath = turnPage.arrDataCacheSet[0][0];
}
//上传附件返回
function afterSubmit1(FlagStr, content, filename) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ content;
		showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }else{
		var HidFileName = document.getElementsByName("HidFileName")[0];
		var HidFilePath = document.getElementsByName("HidFilePath")[0];
		HidFileName.value = filename;
		HidFilePath.value = ImportPath;
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="+ content;
		showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

