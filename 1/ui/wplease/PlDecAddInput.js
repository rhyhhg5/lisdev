var showInfo;
var ImportPath;
var turnPage = new turnPageClass();
function submitForm() {
	if (sumFee()) {
		if (verifyInput()) {
				var r = confirm("请仔细核对您录入的信息，如果信息录入错误将直接影响职场租金的及时下拨！");
				if (r == true) {
					var tOperate = "ADD";
					var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
					var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ showStr;
					showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
					fm.submit();
				} else {
					return false;
				}

			}
		}
}


function placeIn(){
    var divInfos=document.getElementById("divInfos");
    divInfos.style.display='none';
    
    var codeno=document.getElementById("codeno");
    if(codeno.value==null || codeno.value==""){
      alert("请先输入装修费补录职场编码！");
      return false;
    }	
    var strSQL = "select 1 from LIPlaceRentinfo where Placeno='" + fm.codeno.value
	+"' and not exists(select 1 from LIPlaceChangeTrace where Placenorefer='" + fm.codeno.value+ "' and Changestate in('01','02') and Changetype in('02','03'))"
	+" and not exists(select 1 from LIPlaceChangeTrace where Placeno='" + fm.codeno.value+ "' and Changestate in('01','02') and Changetype in('04','05','06'))";

    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
  	  alert("装修费补录职场编码不存在或该职场正处于审批状态，请重新输入！");
  	  codeno.value="";
      return false;
	}
	//获取职场信息
	var strSQL1 = "select Comname,(sigarea + grparea + bankarea + healtharea + servicearea +manageroffice + departpersno + planfinance + jointarea + publicarea +otherarea),"
                 +"begindate,enddate,address "
                 +"from LIPlaceRentInfo a where Placeno='"+codeno.value+"'";			 
	turnPage.strQueryResult  = easyQueryVer3(strSQL1, 1, 1, 1);  
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	//职场信息
	fm.SComName.value = turnPage.arrDataCacheSet[0][0];
	fm.SumArea.value = turnPage.arrDataCacheSet[0][1];
	fm.SStartDate.value = turnPage.arrDataCacheSet[0][2];
	fm.SEndDate.value = turnPage.arrDataCacheSet[0][3];
	fm.SAddress.value = turnPage.arrDataCacheSet[0][4];
	
	
    var divInfos=document.getElementById("divInfos");
    divInfos.style.display='';
}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	
	initForm();
}

function sumFee() {
	var i3 = 0; // 装修费
	var SumDecFee = 0;
	var rowNum3 = DecorationGrid.mulLineCount;
	var colNum3 = DecorationGrid.colCount;
	if (rowNum3 == 0) {
		alert("租期内装修费用表格中不能为空!");
		return false;
	}
	
	//租期内装修费用
	for ( var x = 0; x < rowNum3; x++) {
		for ( var y = 1; y < colNum3; y++) {
			if (DecorationGrid.getRowColData(x, y) == null
					|| DecorationGrid.getRowColData(x, y) == "") {
				alert("租期内装修费用表格中不能有空项");
				return false;
			}
			if (y == 1) {
				if (!IsDate(DecorationGrid.getRowColData(x, y))) {
					alert("租期内装修费表格中日期有误！");
					return false;
				}
			}
			if (y != 2&&y != 1) 
			{
				if (!checkDate(DecorationGrid.getRowColData(x, y))) {
					alert("支付租期摊销格式应该为：yyyy-mm!");
					return false;
				}
			}
		}
	}
	
	//租期内装修费用 日期校验
	for ( var x = 0; x < rowNum3; x = x + 1) {
		if(!testduobledate(x,DecorationGrid,rowNum3)){
			return false;
		}
	}
	while (rowNum3 > i3) {
		var m2 = DecorationGrid.getRowColData(i3, 2);
		if (m2 != null && m2 != "") {
			SumDecFee += DecorationGrid.getRowColData(i3, 2) * 1;
		}
		i3 = i3 + 1;
	}
	document.getElementsByName("SumDecFee")[0].value = SumDecFee;
	return true;
}

function IsDate(oTextbox) {
	var regDate = /^(\d{4})-(\d{1,2})-(\d{1,2})$/;
	if (!regDate.test(oTextbox)) {
		return false;
	}
	var arr = regDate.exec(oTextbox);
	return IsMonthAndDateCorrect(arr[1], arr[2], arr[3]);

}

// 判断年、月、日的取值范围是否正确
function IsMonthAndDateCorrect(nYear, nMonth, nDay) {
	// 月份是否在1-12的范围内，注意如果该字符串不是C#语言的，而是JavaScript的，月份范围为0-11
	if (nMonth > 12 || nMonth <= 0) {
		return false;
	}

	// 日是否在1-31的范围内，不是则取值不正确
	if (nDay > 31 || nDay <= 0) {
		return false;
	}

	// 根据月份判断每月最多日数
	var bTrue = false;
	if (nMonth == 12 || nMonth == 1 || nMonth == 3 || nMonth == 5
			|| nMonth == 7 || nMonth == 8 || nMonth == 10) {
		bTrue = true;
	}
	if (nMonth == 6 || nMonth == 9 || nMonth == 4 || nMonth == 11) {
		bTrue = (nDay <= 30);
	}
	if (bTrue) {
		return true;
	}
	// 2月的情况
	// 如果小于等于28天一定正确
	if (nDay <= 28) {
		return true;
	}
	// 闰年小于等于29天正确
	if (IsLeapYear(nYear))
		return (nDay <= 29);
	// 不是闰年，又不小于等于28，返回false
	return false;
}

// 是否为闰年，规则：四年一闰，百年不闰，四百年再闰
function IsLeapYear(nYear) {
	// 如果不是4的倍数，一定不是闰年
	if (nYear % 4 != 0)
		return false;
	// 是4的倍数，但不是100的倍数，一定是闰年
	if (nYear % 100 != 0)
		return true;

	// 是4和100的倍数，如果又是400的倍数才是闰年
	return (nYear % 400 == 0);
}
function checkDate(aaa){
	var regDate = /^(\d{4})-(\d{1,2})$/;
	if (!regDate.test(aaa)) {
		return false;
	}
	var arr = regDate.exec(aaa);
	return IsMonthAndDateCorrect(arr[1], arr[2], 1);
}
function comparedate(begindate,enddate,flag){
	var bdate = begindate.split('-');
	var edate = enddate.split('-');
	if(bdate[0]>edate[0]){
		return false;
	}
	if(bdate[0]==edate[0]){
		if(flag==1){
	       if(bdate[1]>edate[1]){
	    	   return false;
	    	   }
		}else{
		   if(bdate[1]>=edate[1]){return false;}
	    }
	}
	return true;
}
function diffMonths(bdate,edate){
	var b=bdate.split('-');
	var e=edate.split('-');
	return (e[0]-b[0])*12+(e[1]-b[1])+1;
}
function testduobledate(x,mulname,mulcount){
	//获取租期起期和止期
	var StartDate = document.getElementsByName("SStartDate")[0].value;
	var endData = document.getElementsByName("SEndDate")[0].value;
	
	//获取本次摊销起期和止期
	var xbegindate=mulname.getRowColData(x, 3);
	var xenddate=mulname.getRowColData(x, 4);
	
	//摊销起期应该小于摊销止期
	if(!comparedate(xbegindate,xenddate,1)){
		alert("摊销起期大于摊销止,摊销起期:"+xbegindate+";摊销止期："+xenddate);
		return false;
	}
	//摊销起期应该小于租期起期
	if(!comparedate(StartDate,xbegindate,1)){
		alert("摊销起期小于职场租期起期，请核实！");
		return false;
	}
	//摊销止期应该大于租期止期
	if(!comparedate(xenddate,endData,1)){
		alert("摊销止期大于职场租期止期，请核实!");
		return false;
	}
	return true;
}
