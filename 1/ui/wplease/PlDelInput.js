var turnPage = new turnPageClass();
var DelturnPage = new turnPageClass();
function submitForm(){
		if (verifyInput()) {
		        if(!checkLeaf()){
		           return false;
		        }
				var r = confirm("该操作将删除职场的所有信息，请确认是否进行删除操作！");
				if (r == true) {
					var tOperate = "ADD";
					var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
					var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ showStr;
					showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
					fm.submit();
				} else {
					return false;
				}

			}
}
//判断是否为叶子节点
function checkLeaf(){
    var DelPlaceNo=document.getElementById("DelPlaceNo");
    var checkSQL="select Placeno,Changetype,Changestate from LIPlaceChangeTrace where Placenorefer='"+DelPlaceNo.value+"' and changestate<>'DL'";
    turnPage.strQueryResult  = easyQueryVer3(checkSQL, 1, 1, 1);
  	if (turnPage.strQueryResult) {
       turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	   // 查询成功则拆分字符串，返回二维数组
	   turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	   var PlaceNO=turnPage.arrDataCacheSet[0][0];
	   var Changetype=turnPage.arrDataCacheSet[0][1];
	   if(Changetype=="02"){
	      alert("该职场已经操作续租，不能删除！续租职场号为："+PlaceNO+"");
	      fm.DelPlaceNo.value="";
	      fm.DelName.value = "";
		  fm.DelArea.value = "";
		  fm.DelStartDate.value = "";
		  fm.DelEndDate.value = "";
		  fm.DelAddress.value = "";
		  var divInfos=document.getElementById("divInfos");
	      divInfos.style.display='none';
	   }else if(Changetype=="03"){
	      alert("该职场已经操作换租，不能删除！换租职场号为："+PlaceNO+"");
	      fm.DelPlaceNo.value="";
	      fm.DelName.value = "";
		  fm.DelArea.value = "";
		  fm.DelStartDate.value = "";
		  fm.DelEndDate.value = "";
		  fm.DelAddress.value = "";
		  var divInfos=document.getElementById("divInfos");
	      divInfos.style.display='none';
	   }else{
	      alert("该职场非叶子节点，不能删除!");
	   }
       return false;
    }
    return true;
}
function placeIn(){
	 var divInfos=document.getElementById("divInfos");
	    divInfos.style.display='none';
	    
	    var DelPlaceNo=document.getElementById("DelPlaceNo");
	    if(DelPlaceNo.value==null || DelPlaceNo.value==""){
	      alert("请先输入待删除职场编码！");
	      return false;
	    }		 
	    var strSQL = "select 1 from LIPlaceRentinfo where Placeno='"+DelPlaceNo.value+"' union select 1 from LIPlaceRentinfoB where placeno='"+DelPlaceNo.value+"'" ;
	    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	    //判断是否查询成功
	    if (!turnPage.strQueryResult) {
	  	  alert("职场编码不存在，请重新输入！");
	  	  DelPlaceNo.value="";
	      return false;
		}
	    var checkmanageSQL = "select 1 from LIPlaceRentinfo where Placeno='"+DelPlaceNo.value+"' and Managecom like '"+fm.ComCode.value+"%' union select 1 from LIPlaceRentinfoB where placeno='"+DelPlaceNo.value+"' and managecom like '"+fm.ComCode.value+"%'" ;
	    turnPage.strQueryResult  = easyQueryVer3(checkmanageSQL, 1, 1, 1);
	    //判断是否查询成功
	    if (!turnPage.strQueryResult) {
	  	  alert("职场管理机构与登录机构不匹配，不能进行删除操作！");
	  	  DelPlaceNo.value="";
	      return false;
		}
		//获取职场信息
		var DelSQL = "select Comname,(sigarea + grparea + bankarea + healtharea + servicearea +manageroffice + departpersno + planfinance + jointarea + publicarea +otherarea),"
	                 +"begindate,enddate,address "
	                 +"from LIPlaceRentInfo a where Placeno='"+ DelPlaceNo.value+"' union select Comname,(sigarea + grparea + bankarea + healtharea + servicearea +manageroffice + departpersno + planfinance + jointarea + publicarea +otherarea),"
	                 +"begindate,enddate,address "
	                 +"from LIPlaceRentInfoB a where Placeno='"+ DelPlaceNo.value+"' fetch first 1 rows only";			 
		DelturnPage.strQueryResult  = easyQueryVer3(DelSQL, 1, 1, 1); 
		DelturnPage.arrDataCacheSet = clearArrayElements(DelturnPage.arrDataCacheSet);
		// 查询成功则拆分字符串，返回二维数组
		DelturnPage.arrDataCacheSet = decodeEasyQueryResult(DelturnPage.strQueryResult);
		//职场信息
		fm.DelName.value = DelturnPage.arrDataCacheSet[0][0];
		fm.DelArea.value = DelturnPage.arrDataCacheSet[0][1];
		fm.DelStartDate.value = DelturnPage.arrDataCacheSet[0][2];
		fm.DelEndDate.value = DelturnPage.arrDataCacheSet[0][3];
		fm.DelAddress.value = DelturnPage.arrDataCacheSet[0][4];
	    var divInfos=document.getElementById("divInfos");
	    divInfos.style.display='';
}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	
	initForm();
}