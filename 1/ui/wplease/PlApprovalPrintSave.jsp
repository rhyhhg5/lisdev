
<jsp:directive.page
	import="com.sinosoft.lis.certify.SysOperatorNoticeBL" />
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：按险种打印操作员日结
	//程序功能：
	//创建日期：2002-12-12
	//创建人  ：lh
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%
	System.out.println("start");
	String mDay[] = new String[2];
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	//输出参数
	CError cError = new CError();
	//后面要执行的动作：添加，修改，删除
	String tRela = "";
	String FlagStr = "";
	String Content = "";
	boolean operFlag = true;
	
//	String tPlaceNo = request.getParameter("CodeId");
	String tManageCom = request.getParameter("CompanyCode");
//	String tToDate = request.getParameter("ToDate");
	String tComlevel = request.getParameter("Comlevel");
	String tUseState = request.getParameter("UseState");  //1-全部/2-在用
	
	String strOperation = "PRINT";

	System.out.println("要打印的信息是：");
//	System.out.println("职场编码：" + tPlaceNo);
	System.out.println("管理机构：" + tManageCom);
//	System.out.println("距到期日：" + tToDate);
	System.out.println("机构级别：" + tComlevel);
	System.out.println("在用状态：" + tUseState);
	System.out.println(tG.Operator);
	System.out.println(tG.ManageCom);

	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	
//	tVData.addElement(tPlaceNo);
	tVData.addElement(tManageCom);
//	tVData.addElement(tToDate);
	tVData.addElement(tComlevel);
	tVData.addElement(tUseState);
	tVData.addElement(tG);

	XmlExport txmlExport = new XmlExport();

	{
		PlApprovalPrintUI tPlApprovalPrintUI = new PlApprovalPrintUI();
		if (!tPlApprovalPrintUI.submitData(tVData, strOperation)) {
			operFlag = false;
			mErrors.copyAllErrors(tPlApprovalPrintUI.mErrors);
			cError.moduleName = "FinDayCheckSave";
			cError.functionName = "submitData";
			cError.errorMessage = "FinDayCheckUI发生错误，但是没有提供详细的出错信息";
			mErrors.addOneError(cError);
		}
		mResult = tPlApprovalPrintUI.getResult();

		txmlExport = (XmlExport) mResult.getObjectByObjectName(
		"XmlExport", 0);
		if (txmlExport == null) {
			System.out.println("null");
			return;
		}
	}
	
	ExeSQL tExeSQL = new ExeSQL();

	//获取临时文件名
	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	String strFilePath = tExeSQL.getOneValue(strSql);
	String strVFFileName = strFilePath + tG.Operator + "_"
			+ FileQueue.getFileName() + ".vts";
	//获取存放临时文件的路径
	String strRealPath = application.getRealPath("/")
			.replace('\\', '/');
	String strVFPathName = strRealPath + "/" + strVFFileName;

	CombineVts tcombineVts = null;

	if (operFlag == true) {
		//合并VTS文件
		String strTemplatePath = application
		.getRealPath("f1print/picctemplate/")
		+ "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),
		strTemplatePath);

		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);

		//把dataStream存储到磁盘文件
		System.out.println("存储文件到" + strVFPathName);
		AccessVtsFile.saveToFile(dataStream, strVFPathName);
		System.out.println("==> Write VTS file to disk ");

		System.out.println("===strVFFileName : " + strVFFileName);
		//本来打算采用get方式来传递文件路径
		//缺点是文件路径被暴露
		System.out.println("---passing params by get method");
		response
		.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="
				+ strVFPathName);
	}
%>
