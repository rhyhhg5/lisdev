<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//name :PlchangeInput.jsp
	//function :
	//Creator :hdl
	//date :2012-6-12
	////* 更新记录：  更新人    更新日期     更新原因/内容
	//*             鞠成富  2012-09-03    修改
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="./InputMain.js"></SCRIPT>
<SCRIPT src="./PlChangeInput.js"></SCRIPT>
<SCRIPT src="./PlShouBackFee.js"></SCRIPT>
<%@include file="./PlChangeInit.jsp"%>
<SCRIPT src = "PlUpdateFee.js"></SCRIPT> 
</head>
<body onload="initElementtype();initForm()">
	<form action="PlChangeSave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<tr class=common>
				<td class=title>原编码</td>
				<td><Input class=common name="codeno" id="codeno"
					verify="续租职场编码|NOTNULL" elementtype=nacessary></td>
			</tr>
			<tr class=common>
				<td colspan="2"><INPUT VALUE="确认" class=cssButton TYPE=button
					onclick="placeIn()"></td>
			</tr>
		</table>
		<div id="divSourceInfo" style="display: none">
		    <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divInfo);"></td>
					<td class=titleImg>原合同信息</td>
				</tr>
			</table>
			<div id="divInfo" style="display: ''">
			  <table class=common>
			    <tr class=common>
			      <TD class=title>公司名称</TD>
				  <TD class=input><Input class=common name=SComName id="SComName" readonly></TD>
				  <Td class=title>总面积</Td>
				  <Td class=input><input class=common name=SumArea id="SumArea" readonly></Td>
				  <Td class=title>租期起期</Td>
				  <TD class=input><Input name=SStartDate class=common readonly></TD>
				</tr>
				<tr class=common>
				  <Td class=title>租期止期</Td>
				  <TD class=input><Input name=SEndDate class=common readonly></TD>
				  <Td class=title>地址</Td>
				  <Td class=input colspan="3"><input class=common name=SAddress id="SAddress"  readonly></Td>
			    </tr>
			  </table>
			</div>
		</div>
		<div id="divSource" style="display: none">
		    <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divSource1);"></td>
					<td class=titleImg>换租信息</td>
				</tr>
			</table>
			<div id="divSource1" style="display: ''">
			  <table class=common>
			   <tr class=common>
			      <td class=title>停用时间</td>
			      <TD class=input><Input name=StopDate class='coolDatePicker' elementtype=nacessary dateFormat='short' verify="停用时间|NOTNULL"></TD>
			      <td colspan="2"><INPUT VALUE="计算应收已付费用" class=cssButton TYPE=button onclick="getShoudFee()"></td>
			    </tr>
			    <tr class=common>
			      <TD class=title>合同违约金</TD>
				  <TD class=input><Input class=common name=SWeiYueFee id="SWeiYueFee" elementtype=nacessary verify="合同违约金|NOTNULL&NUM" readonly></TD>
				  <Td class=title>实际违约金</Td>
				  <Td class=input><input class=common name=AWeiYueFee id="AWeiYueFee"  elementtype=nacessary verify="实际违约金|NOTNULL&NUM"></Td>
				  <Td class=title>违约金支付时间</Td>
				  <TD class=input><Input name=AWeiYueDate class='coolDatePicker' elementtype=nacessary dateFormat='short' verify="违约金支付时间|NOTNULL"></TD>
				</tr>
				<tr class=common>
				  <Td class=title>合同保证金</Td>
				  <Td class=input><input class=common name=BaoZhFee id="BaoZhFee"  elementtype=nacessary verify="合同保证金|NOTNULL&NUM" readonly></Td>
				  <Td class=title>保证金收回时间</Td>
				  <TD class=input><Input name=BaoZhDate class='coolDatePicker' dateFormat='short' elementtype=nacessary verify="保证金收回时间|NOTNULL"></TD>
				  <td class=title></td>
				  <td class=input></td>
			    </tr>
			    <tr class=common>
				    <td class=title>系统计算应收已付租赁费用</td>
				    <Td class=input><input class=common name=SYingFee id="SYingFee"  elementtype=nacessary verify="系统计算应收已付租赁费用|NOTNULL&NUM" readonly></Td>
				    <td class=title>实际应收已付款租赁费用</td>
				    <Td class=input><input class=common name=AYingFee id="AYingFee"  elementtype=nacessary verify="实际应收已付款租赁费用|NOTNULL&NUM" readonly></Td>
				    <Td class=title>应收已付收回时间</Td>
					<TD class=input><Input name=YingDate class='coolDatePicker' dateFormat='short' elementtype=nacessary verify="应收已付收回时间|NOTNULL"></TD>
			    <tr>
			  </table>
			</div>
		</div>
		<hr>
		<div id="divPlaceIn1" style="display: none">
		    <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divInfoNew);"></td>
					<td class=titleImg>新合同信息</td>
				</tr>
			</table>
			<div id="divInfoNew" style="display: ''">
		    <jsp:include page="InputMain.jsp" flush="true"/>
		    </div>
		</div>		
	    <input type="Hidden" name="HidPlaceEndDate"> 
		<input type="Hidden" name="HidPenaltyFee2"> 
		<input type="Hidden" name="HidReBackFee"> 
		<input type="Hidden" name="HidReBackDate">
		<input type="hidden" name=CurrentDate >	
		<input type=hidden name=shouldRenFee>
		<input type=hidden name=shouldProFee>
		<input type=hidden name=shouldDecFee>
	</form>
	<div id="divPlaceIn2" style="display: none">
		<form action="PlInFeeSave.jsp" method=post name=feeimp target="fraSubmit" ENCTYPE="multipart/form-data">
    		<jsp:include page="PlUpdateFee.jsp" flush="true"/>
  		</form>
		<form action="PlInFileSave.jsp" method=post name=imp
			target="fraSubmit" ENCTYPE="multipart/form-data">
			<Table class=common>
				<TR class=common>
					<TD class=title>文件名</TD>
					<TD><Input type="file" name=FileName class=common> <INPUT
						VALUE="上载租赁合同及批文" class=cssButton TYPE=button onclick="ApprovalImp();">
					</TD>
					<TD><INPUT VALUE="保存" class=cssButton TYPE=button onclick="submitForm()"></TD>
				</TR>
			</Table>
		</form>
	</div>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>