<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
//name :PlaceInInput.jsp
//function :
//Creator :huodonglei
//date :2011-6-8

%>

	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "PlCostPrintInput.js"></SCRIPT> 
<%@include file="PlCostPrintInit.jsp"%>
</head>
<body onload="initElementtype();initForm()" >
    <form action="PlCostPrintSave.jsp" method=post name=fm target="fraSubmit">
    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divBasicInfo);"></td>
    	<td class= titleImg>基本信息</td></tr>
    </table>
    <Div id= "divBasicInfo" style= "display: ''">
		<Table class= common>
   			<TR class= common>
   				<TD class= title>职场编码</TD>
   				<TD class= input><Input class= common name= CodeId id="CodeId" > </TD>
   				<TD class= title>公司代码</TD>
    	        <TD class= input>
    	        <Input class= "codeno" readonly="readonly" name=CompanyCode  ondblclick="return showCodeList('comcode',[this,CompanyCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,CompanyCodeName],[0,1],null,null,null,1);" ><Input class=codename readonly="readonly" name=CompanyCodeName>
    	        </TD>
	        	<td class = title>公司级别</td>
	        	<td class= input> <Input class=code  NAME=CompanyLevel ondblClick="return showCodeList('companylevel',[this],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('companylevel',[this],[0,1],null,null,null,1);" ></td>
	        </Tr>
	        <tr>
	            <td class= title>统计起期</td>
	            <td class= input><Input name=BeginDate id=BeginDate  class='coolDatePicker' dateFormat='short' > </td>
	            <td class= title>统计止期</td>
	            <td class= input><Input name=EndDate id=EndDate class='coolDatePicker' dateFormat='short' > </td>
	        </tr>
	</table>
	<br>
	<INPUT VALUE="清单打印" class=cssButton TYPE=button onclick="print()">
	<input type="hidden" id="fmtransact" name="fmtransact" />
    </form>
	 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>