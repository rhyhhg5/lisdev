var showInfo;
var ImportPath;
var turnPage = new turnPageClass();  
function submitForm()
{ 
    if(beforeSubmit()){
    if(sumFee()){
    if(verifyInput()){
    var r=confirm("请仔细核对您录入的信息，如果信息录入错误将直接影响职场租金的及时下拨！");
     if (r==true)
     {
     var i = 0;
	 var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;     
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 	 
	 fm.submit();
     }
     else
     {
     return false;
     }
	
	}	
	}
	}
}
function getMoney(){ 
   var codeno=document.getElementById("codeno");
   var SYingFee=document.getElementById("SYingFee");            //应收已付款
   var SWeiYueFee=document.getElementById("SWeiYueFee");        //违约金
   var BaoZhFee=document.getElementById("BaoZhFee");            //保证金
   if(codeno.value==null || codeno.value==""){
      alert("换租职场编码不能为空！");
      return false;
    }
   // var strSQL="select a.Marginfee,a.Penaltyfee from LIPliceRentinfo a where a.Placeno='"+codeno.value+"'";
   var strSQL="select Penaltyfee,Marginfee from LIPlaceRentInfo where Placeno='"+codeno.value+"'";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  

	//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	//查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    SWeiYueFee.value = turnPage.arrDataCacheSet[0][0];    //退租职场违约金
    BaoZhFee.value=turnPage.arrDataCacheSet[0][1];        //保证金
              
}
function getShoudFee()
{
    calculateFee(fm.SEndDate.value,fm.codeno.value);
}
function placeIn(){
    var codeno=document.getElementById("codeno");
    if(codeno.value==null || codeno.value==""){
      alert("请先输入换租职场编码！");
      document.getElementById("codeno").focus();
      return false;
    }
    //var strSQL = "select 1 from LIPlaceRentinfo where Placeno='"+codeno.value+"' and not exists(select 1 from LIPlaceChangeTrace where (placeno='"+codeno.value+"' or Placenorefer='"+codeno.value+"') and Changestate in('01','02'))";				 
    var strSQL = "select 1 from LIPlaceRentinfo where Placeno='" + codeno.value
	+"' and not exists(select 1 from LIPlaceChangeTrace where Placenorefer='" + codeno.value+ "' and Changestate in('01','02') and Changetype in('02','03'))"
	+" and not exists(select 1 from LIPlaceChangeTrace where Placeno='" + codeno.value+ "' and Changestate in('01','02') and Changetype in('04','05','06'))";

    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
  	  alert("换租职场编码不存在或该职场正处于审批状态，请重新输入！");
  	  codeno.value="";
  	  document.getElementById("codeno").focus();
      return false;
	}
	
	//判断该职场是否已经换租过
	var strSQL = "select 1 from LIPlaceRentinfo where placeno = '" + fm.codeno.value 
		+"' and exists (select 1 from LIPlaceChangeTrace where Placenorefer = '" + fm.codeno.value+ "' and Changestate not in('DL') and Changetype in ('02','03') )";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	if(turnPage.strQueryResult){
		alert("职场编码已被换租或续租，请重新输入！");
		document.getElementById("codeno").focus();
  	 	codeno.value="";
      	return false;
	}
	
	//获取源职场信息
    var strSQL1="select Comname,(Sigarea+Grparea+Bankarea+Healtharea+Servicearea+Jointarea+Manageroffice+Departpersno+Planfinance+Publicarea+Otherarea),Begindate,Enddate," +
    		"Address,Managecom,Comlevel,(case Comlevel when '1' then '省级分公司' when '2' then '地市级分公司' when '3' then '四级机构-区' when '4' then '四级机构-县' end),Province,City from LIPlaceRentInfo where Placeno='"+codeno.value+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL1, 1, 1, 1);  
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//原职场信息
	fm.SComName.value = turnPage.arrDataCacheSet[0][0];
	fm.SumArea.value = turnPage.arrDataCacheSet[0][1];
	fm.SStartDate.value = turnPage.arrDataCacheSet[0][2];
	fm.SEndDate.value = turnPage.arrDataCacheSet[0][3];
	fm.SAddress.value = turnPage.arrDataCacheSet[0][4];
	fm.Managecom.value=turnPage.arrDataCacheSet[0][5];
	fm.Comlevel.value=turnPage.arrDataCacheSet[0][6];
	fm.Comlevelname.value=turnPage.arrDataCacheSet[0][7];
	fm.Province.value=turnPage.arrDataCacheSet[0][8];
	fm.City.value=turnPage.arrDataCacheSet[0][9];
	//fm.Comname.value=fm.SComName.value;
	//fm.Address.value=fm.SAddress.value;
	//获取职场换租信息
    getMoney();
	
    var placeIn1=document.getElementById("divPlaceIn1");
    var placeIn2=document.getElementById("divPlaceIn2");
    var divSourceInfo=document.getElementById("divSourceInfo");
    var divSource=document.getElementById("divSource");
    placeIn1.style.display='';
    placeIn2.style.display='';
    divSourceInfo.style.display='';
    divSource.style.display='';
    
}
function monthsBetween(bdate,edate){
	var b=bdate.split('-');
	var e=edate.split('-');
	return (e[0]-b[0])*12+(e[1]-b[1])*1;
}
function beforeSubmit() {
	var address = fm.Address.value;
	if(!testAddress(address)){
		return false;
	};
    sumArea();  //计算总面积
	var StartDate = document.getElementsByName("Begindate")[0].value;
	var endData = document.getElementsByName("Enddate")[0].value;
	var BaoZhDate = document.getElementsByName("BaoZhDate")[0].value;
	if (!checkdate(StartDate, endData)) {
		return false;
	}
	
	if((StartDate!=null && StartDate!="") && (BaoZhDate!=null && BaoZhDate!="")){
		if(BaoZhDate>StartDate){
			var months = monthsBetween(StartDate,BaoZhDate);
			var bdate = StartDate.split("-");
			var hdate = BaoZhDate.split("-");
			if(months>7){
				alert("原职场合同的【保证金收回时间】不能晚于新合同租期起期后7个月");
				return false;
			}else if(months==7 && (hdate[2]-bdate[2])*1>0){
					alert("原职场合同的【保证金收回时间】不能晚于新合同租期起期后7个月");
					return false;
			}
		}
	}
	
	if(fm.Otherarea.value != "" && fm.Otherarea.value !=0)
	{
		if(fm.Explanation.value=="")
		{
			alert("其他面积不为空且大于零时需填写说明！");
			return false;
		}
	}		
	if (fm.all("HidFileName").value == null
			|| fm.all("HidFileName").value == "") {
		alert("请先上传批文");
		return false;
	}
	
	if(!checkPlaceNO(fm.Placeno.value)){
	  document.getElementsByName("ApprovalType")[0].value="";
  	  document.getElementsByName("Managecom")[0].value="";
	  document.getElementsByName("ApprovalYear")[0].value="";
	  document.getElementsByName("ApprovalCode")[0].value="";
	  document.getElementsByName("ApprovalSeria")[0].value="";
	  return false;
	}
	return true;

}
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}
function afterSubmit1( FlagStr, content ,filename)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {          
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var HidFileName=document.getElementsByName("HidFileName")[0];
    var HidFilePath=document.getElementsByName("HidFilePath")[0];
    HidFileName.value=filename;
    HidFilePath.value=ImportPath;
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}