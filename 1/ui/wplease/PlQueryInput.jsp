<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//name :PlQueryInput.jsp
	//function :
	//Creator :huodonglei
	//date :2011-6-8
	//* 更新记录：  更新人    更新日期     更新原因/内容
	//*     鞠成富  2012-09-20   修改
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="./PlQueryInput.js"></SCRIPT>
<SCRIPT src="./PlShouBackFee.js"></SCRIPT>
<%@include file="./PlQueryInit.jsp"%>
</head>
<body onload="initElementtype();initForm()">
	<form action="PlChangeSave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<tr class=common>
				<td class=title>查询职场编码</td>
				<td><Input class=common name="codeno" id="codeno"
					verify="职场编码|NOTNULL" elementtype=nacessary></td>
			</tr>
			<tr class=common>
				<td colspan="2"><INPUT VALUE="确认" class=cssButton TYPE=button onclick="placeIn()"></td>
			</tr>
		</table>
		<div id="divPlaceIn1" style="display: none">
          <jsp:include page="InputMain.jsp" flush="true"/>
			<br>
			<input type=button value='批文下载' onclick='downloadmode();' class=cssButton>
			<input type=hidden name=Placenorefer>
			<input type=hidden name=filename>
			<input type=hidden name=filepath>
			<br> 
			<input type="hidden" name=CurrentDate >
			<input type=hidden name=shouldRenFee>
			<input type=hidden name=shouldProFee>
		    <input type=hidden name=shouldDecFee>
		</div>

		<div id="divPlaceIn2" style="display: none">
			<hr>
			<table class=common>
			   <tr class=common>
			      <td class=title>停用时间</td>
			      <TD class=input><Input name=StopDate class=common  readonly></TD>
			    </tr>
			    <tr class=common>
			      <TD class=title>合同违约金</TD>
				  <TD class=input><Input class=common name=SWeiYueFee id="SWeiYueFee" readonly></TD>
				  <Td class=title>实际违约金</Td>
				  <Td class=input><input class=common name=AWeiYueFee id="AWeiYueFee"  readonly></Td>
				  <Td class=title>违约金支付时间</Td>
				  <TD class=input><Input name=AWeiYueDate class=common readonly></TD>
				</tr>
				<tr class=common>
				  <Td class=title>合同保证金</Td>
				  <Td class=input><input class=common name=BaoZhFee id="BaoZhFee"  readonly></Td>
				  <Td class=title>保证金收回时间</Td>
				  <TD class=input><Input name=BaoZhDate class=common readonly></TD>
				  <td class=title></td>
				  <td class=input></td>
			    </tr>
			    <tr class=common>
				    <td class=title>系统计算应收已付租赁费用</td>
				    <Td class=input><input class=common name=SYingFee id="SYingFee"   ></Td>
				    <td class=title>实际应收已付款租赁费用</td>
				    <Td class=input><input class=common name=AYingFee id="AYingFee"   ></Td>
				    <Td class=title>应收已付收回时间</Td>
					<TD class=input><Input name=YingDate class=common  ></TD>
			    <tr>
			  </table>
			</div>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</form>
</body>
</html>