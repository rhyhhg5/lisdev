
var showInfo;
var ImportPath;
var turnPage = new turnPageClass();
var turnPageRelate = new turnPageClass();
var turnPageProperty = new turnPageClass();
var turnPageDecoration = new turnPageClass();
var tApptype;

//初始化审核列表
function appInit(Comcode) {
	var apptype = fm.appstateType.value;
	tApptype = apptype;
	if (apptype == "2") {
			appsql = "select lc.Placeno,(case when lc.Changetype in('04','06') then (select a.Appno from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.appno from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end)," +
					" (case lc.Changetype when '01' then '新增职场' when '02' then '续租职场' when '03' then '换租职场'  when '04' then  '撤销职场' when '05' then '职场修改' when '06' then '添加装修费' when '07' then '其他状态' end), " +
					"(case when lc.Changetype in('04','06') then (select a.Begindate from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.Begindate from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end), " +
					"(case when lc.Changetype in('04','06') then (select a.Enddate from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.Enddate from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end),  lc.Operator,  lc.Makedate, lc.Serialno, " +
					"(case when lc.changetype = '02' then (select m.Actualenddate from LIPlaceRentInfo m where m.placeno= lc.placenorefer) else null end) " +
					"from LIPlaceChangeTrace lc " +
					"where lc.Changestate='01' and (case when lc.Changetype in('04','06') then (select a.managecom from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.managecom from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end) like '"+Comcode+"%'";
	} else {
		if (apptype == "3") {
			appsql = "select lc.Placeno,(case when lc.Changetype in('04','06') then (select a.Appno from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.appno from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end)," +
			" (case lc.Changetype when '01' then '新增职场' when '02' then '续租职场' when '03' then '换租职场'  when '04' then  '撤销职场' when '05' then '职场修改' when '06' then '添加装修费' when '07' then '其他状态' end), " +
			"(case when lc.Changetype in('04','06') then (select a.Begindate from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.Begindate from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end), " +
			"(case when lc.Changetype in('04','06') then (select a.Enddate from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.Enddate from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end),  lc.Operator,  lc.Makedate, lc.Serialno, " +
			"(case when lc.changetype = '02' then (select m.Actualenddate from LIPlaceRentInfo m where m.placeno= lc.placenorefer) else null end)  " +
			"from LIPlaceChangeTrace lc " +
			"where lc.Changestate='02' and (case when lc.Changetype in('04','06') then (select a.managecom from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.managecom from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end) like '"+Comcode+"%'";
		} else {
			if (apptype == "4") {
				appsql = "select lc.Placeno,(case when lc.Changetype in('04','06') then (select a.Appno from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.appno from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end)," +
				" (case lc.Changetype when '01' then '新增职场' when '02' then '续租职场' when '03' then '换租职场'  when '04' then  '撤销职场' when '05' then '职场修改' when '06' then '添加装修费' when '07' then '其他状态' end), " +
				"(case when lc.Changetype in('04','06') then (select a.Begindate from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.Begindate from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end), " +
				"(case when lc.Changetype in('04','06') then (select a.Enddate from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.Enddate from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end),  lc.Operator,  lc.Makedate, lc.Serialno,'' " +
				"from LIPlaceChangeTrace lc " +
				"where lc.Changestate='04' and (case when lc.Changetype in('04','06') then (select a.managecom from LIPlaceRentInfo a where a.placeno=lc.placeno) else (select a.managecom from LIPlaceRentInfoB a where a.Standbystring1=lc.serialno) end) like '"+Comcode+"%'";
			}
		}
	}
		turnPage.queryModal(appsql, ApproveGrid);
}

//审核通过按钮
function AppYes() {
	fm.appstate.value = "Y";
	submitForm();
}
//审核不通过按钮
function AppNo() {
	if(BeforeAppNo()){
		fm.appstate.value = "N";
		submitForm();
	}
	
}
//审核不通过修改确认
function RealChange() {
    if(!(fm.Placeno.value==fm.codeno.value))
    {
        alert("职场编码为唯一标识号码，不允许修改职场编码，请重新输入！");
        return false;
    }
    if(!notNull())
    {
        return false;
    }
    if (beforeSubmit()) {
	if (sumFee()) {
		if(!checkNum(fm.Penaltyfee.value) || !checkNum(fm.Marginfee.value)) 
		{
		    alert("保证金金额或合同约定违约金格式错误，请重新输入！");
		    fm.Penaltyfee.value="";
		    fm.Marginfee.value="";
		    return false;
		}
		if (verifyInput()) {
				var r = confirm("请仔细核对您录入的信息，如果信息录入错误将直接影响职场租金的及时下拨！");
				if (r == true) {
					submitForm();
				} else {
					return false;
				}

			}
		}
	}
}

function notNull(){
    var tStopDate = document.getElementsByName("StopDate")[0].value;
    var tSWeiYueFee = document.getElementsByName("SWeiYueFee")[0].value;
    var tAWeiYueFee = document.getElementsByName("AWeiYueFee")[0].value;
    var tAWeiYueDate = document.getElementsByName("AWeiYueDate")[0].value;
    var tBaoZhFee = document.getElementsByName("BaoZhFee")[0].value;
    var tBaoZhDate = document.getElementsByName("BaoZhDate")[0].value;
    var tSYingFee = document.getElementsByName("SYingFee")[0].value;
    var tAYingFee = document.getElementsByName("AYingFee")[0].value;
    var tYingDate = document.getElementsByName("YingDate")[0].value;
    
    var tXBaoZhFee = document.getElementsByName("XBaoZhFee")[0].value;
    var tXBaoZhDate = document.getElementsByName("XBaoZhDate")[0].value;
    
	if (fm.flag.value== "03" || fm.flag.value == "04") {
		if (tStopDate==null || tStopDate=="" || tSWeiYueFee==null || tSWeiYueFee=="" || tAWeiYueFee==null || tAWeiYueFee==""
		 || tAWeiYueDate==null || tAWeiYueDate=="" || tBaoZhFee==null || tBaoZhFee=="" || tBaoZhDate==null || tBaoZhDate==""
		  || tSYingFee==null || tSYingFee=="" || tAYingFee==null || tAYingFee=="" || tYingDate==null || tYingDate=="" ) {
		    alert("职场换租/撤租，换租/撤租信息不能为空！");
		    return false;
	    }
	    return true;
	}
	if (fm.flag.value== "02" ) {
		if (tXBaoZhFee==null || tXBaoZhFee=="" || tXBaoZhDate==null || tXBaoZhDate=="" ) {
		    alert("职场续租，续租信息不能为空！");
		    return false;
	    }
	    return true;
	}
	return true;
}
function beforeSubmit() {
	var StartDate = document.getElementsByName("Begindate")[0].value;
	var endData = document.getElementsByName("Enddate")[0].value;
	if (!checkdate(StartDate, endData)) {
		return false;
	}
	if(fm.Otherarea.value != ""  && fm.Otherarea.value !=0)
	{
		if(fm.Explanation.value=="")
		{
			alert("其他面积不为空且大于零时需填写说明！");
			return false;
		}
	}		
	if (fm.all("filename").value == null
			|| fm.all("filename").value == "") {
		alert("请先上传批文");
		return false;
	}
	return true;

}
//校验审批不确认时必须录入原因
function BeforeAppNo(){	
	if(fm.Rremark.value == null || fm.Rremark.value == ""){
		alert("没有填写原因！审核不确认的原因不能为空!");
		return false;
	}
	return true;
}
//提交页面
function submitForm() {
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//fm.action="./PlApprovalSave.jsp";
	fm.submit();
}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	var placeIn1 = document.getElementById("divPlaceIn1");
	var placeIn2 = document.getElementById("divPlaceIn2");
	var placeIn3 = document.getElementById("divPlaceIn3");
	var placeIn4 = document.getElementById("divPlaceIn4");
	placeIn1.style.display = "none";
	placeIn2.style.display = "none";
	placeIn3.style.display = "none";
	placeIn4.style.display = "none";
	if(fm.appstateType.value=="4"){
	var placeInFile = document.getElementById("divPlaceInFile");
		placeInFile.style.display = "none";
	}
	initForm();
}

// 查询职场信息并返回到前台页面
function showPlaceInfo() {
	var codeno = fm.codeno.value;
	//alert("fm.flag.value="+fm.flag.value);
	if(fm.flag.value=="04" || fm.flag.value=="06"){
		var infoSql="select lri.Placeno,lri.Managecom,case length(lri.Appno) when 13 then substr(lri.Appno, 1, 3) else substr(lri.Appno, 1, 2) end, case length(lri.Appno) when 13 then substr(lri.Appno, 4, 4) else substr(lri.Appno, 3, 4) end,case length(lri.Appno) when 13 then substr(lri.Appno, 8, 4) else substr(lri.Appno, 7, 4) end,case length(lri.Appno) when 13 then substr(lri.Appno, 12, 2) else substr(lri.Appno, 11, 2) end ," +
				"lri.Comlevel,case lri.Comlevel when '1' then '省级分公司' when '2' then '地市级分公司' when '3' then '四级机构-区' when '4' then '四级机构-县' end,lri.Comname,lri.Begindate,lri.Enddate,lri.Address,lri.Lessor,lri.Houseno,lri.Province,lri.City,lri.Isrentregister,case lri.Isrentregister when '1' then '是' when '0' then '否' end," +
				"lri.Islessorassociate,case lri.Islessorassociate when '1' then '是' when '0' then '否' end,lri.Islessorright,case lri.Islessorright when '1' then '是' when '0' then '否' end,lri.Ishouserent,case lri.Ishouserent when '1' then '是' when '0' then '否' end,lri.Isplaceoffice,case lri.Isplaceoffice when '1' then '是' when '0' then '否' end,lri.Ishousegroup,case lri.Ishousegroup when '1' then '是' when '0' then '否' end," +
				"lri.Ishousefarm,case lri.Ishousefarm when '1' then '是' when '0' then '否' end,lri.Sigarea,lri.Grparea,lri.Bankarea,lri.Healtharea,lri.Servicearea,lri.Jointarea,lri.Manageroffice,lri.Departpersno,lri.Planfinance,lri.Publicarea,lri.Otherarea,lri.Explanation,lri.Marginfee,lri.Marginpaydate,lri.Penaltyfee,lri.Remake,lri.Filename,lri.Filepath,(case when lrc.Fremark is null then lrc.Zremark else lrc.Fremark end),lrc.Changetype,lrc.placenorefer from LIPlaceRentInfo lri ,LIPlaceChangeTrace lrc where lri.placeno='"+fm.codeno.value+"'and lri.placeno=lrc.placeno and lrc.Serialno='"+fm.CTSerialno.value+"'";
		turnPage.strQueryResult = easyQueryVer3(infoSql, 1, 1, 1);
		// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
		// 查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	}else{
		var infoSql="select lri.Placeno,lri.Managecom,case length(lri.Appno) when 13 then substr(lri.Appno, 1, 3) else substr(lri.Appno, 1, 2) end, case length(lri.Appno) when 13 then substr(lri.Appno, 4, 4) else substr(lri.Appno, 3, 4) end,case length(lri.Appno) when 13 then substr(lri.Appno, 8, 4) else substr(lri.Appno, 7, 4) end,case length(lri.Appno) when 13 then substr(lri.Appno, 12, 2) else substr(lri.Appno, 11, 2) end ," +
				"lri.Comlevel,case lri.Comlevel when '1' then '省级分公司' when '2' then '地市级分公司' when '3' then '四级机构-区' when '4' then '四级机构-县' end,lri.Comname,lri.Begindate,lri.Enddate,lri.Address,lri.Lessor,lri.Houseno,lri.Province,lri.City,lri.Isrentregister,case lri.Isrentregister when '1' then '是' when '0' then '否' end," +
				"lri.Islessorassociate,case lri.Islessorassociate when '1' then '是' when '0' then '否' end,lri.Islessorright,case lri.Islessorright when '1' then '是' when '0' then '否' end,lri.Ishouserent,case lri.Ishouserent when '1' then '是' when '0' then '否' end,lri.Isplaceoffice,case lri.Isplaceoffice when '1' then '是' when '0' then '否' end,lri.Ishousegroup,case lri.Ishousegroup when '1' then '是' when '0' then '否' end," +
				"lri.Ishousefarm,case lri.Ishousefarm when '1' then '是' when '0' then '否' end,lri.Sigarea,lri.Grparea,lri.Bankarea,lri.Healtharea,lri.Servicearea,lri.Jointarea,lri.Manageroffice,lri.Departpersno,lri.Planfinance,lri.Publicarea,lri.Otherarea,lri.Explanation,lri.Marginfee,lri.Marginpaydate,lri.Penaltyfee,lri.Remake,lri.Filename,lri.Filepath,(case when lrc.Fremark is null then lrc.Zremark else lrc.Fremark end),lrc.Changetype,lrc.placenorefer,lri.Serialno from LIPlaceRentInfoB lri ,LIPlaceChangeTrace lrc where lri.placeno='"+fm.codeno.value+"'and lri.Standbystring1=lrc.Serialno and lrc.Serialno='"+fm.CTSerialno.value+"'";
		turnPage.strQueryResult = easyQueryVer3(infoSql, 1, 1, 1);
		// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
		// 查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	}

	
	//基本信息
	fm.Placeno.value=turnPage.arrDataCacheSet[0][0];
	fm.Managecom.value=turnPage.arrDataCacheSet[0][1];
	fm.ApprovalType.value=turnPage.arrDataCacheSet[0][2];
	fm.ApprovalYear.value=turnPage.arrDataCacheSet[0][3];
	fm.ApprovalCode.value=turnPage.arrDataCacheSet[0][4];
	fm.ApprovalSeria.value=turnPage.arrDataCacheSet[0][5];
	fm.Comlevel.value=turnPage.arrDataCacheSet[0][6];
	fm.Comlevelname.value=turnPage.arrDataCacheSet[0][7];
	fm.Comname.value=turnPage.arrDataCacheSet[0][8];
	fm.Begindate.value=turnPage.arrDataCacheSet[0][9];
	fm.Enddate.value=turnPage.arrDataCacheSet[0][10];
	fm.Address.value=turnPage.arrDataCacheSet[0][11];
	fm.Lessor.value=turnPage.arrDataCacheSet[0][12];
	fm.Houseno.value=turnPage.arrDataCacheSet[0][13];
	fm.Province.value=turnPage.arrDataCacheSet[0][14];
	fm.City.value=turnPage.arrDataCacheSet[0][15];
	fm.Isrentregister.value=turnPage.arrDataCacheSet[0][16];
	fm.IsRentRegisterName.value=turnPage.arrDataCacheSet[0][17];
	fm.Islessorassociate.value=turnPage.arrDataCacheSet[0][18];
	fm.IslessorassociateName.value=turnPage.arrDataCacheSet[0][19];
	fm.Islessorright.value=turnPage.arrDataCacheSet[0][20];
	fm.IslessorrightName.value=turnPage.arrDataCacheSet[0][21];
	fm.Ishouserent.value=turnPage.arrDataCacheSet[0][22];
	fm.IshouserentName.value=turnPage.arrDataCacheSet[0][23];
	fm.Isplaceoffice.value=turnPage.arrDataCacheSet[0][24];
	fm.IsplaceofficeName.value=turnPage.arrDataCacheSet[0][25];
	fm.Ishousegroup.value=turnPage.arrDataCacheSet[0][26];
	fm.IshousegroupName.value=turnPage.arrDataCacheSet[0][27];
	fm.Ishousefarm.value=turnPage.arrDataCacheSet[0][28];
	fm.IshousefarmName.value=turnPage.arrDataCacheSet[0][29];
	//职场面积
	fm.Sigarea.value=turnPage.arrDataCacheSet[0][30];
	fm.Grparea.value=turnPage.arrDataCacheSet[0][31];
	fm.Bankarea.value=turnPage.arrDataCacheSet[0][32];
	fm.Healtharea.value=turnPage.arrDataCacheSet[0][33];
	fm.Servicearea.value=turnPage.arrDataCacheSet[0][34];
	fm.Jointarea.value=turnPage.arrDataCacheSet[0][35];
	fm.Manageroffice.value=turnPage.arrDataCacheSet[0][36];
	fm.Departpersno.value=turnPage.arrDataCacheSet[0][37];
	fm.Planfinance.value=turnPage.arrDataCacheSet[0][38];
	fm.Publicarea.value=turnPage.arrDataCacheSet[0][39];	
	fm.Otherarea.value=turnPage.arrDataCacheSet[0][40];
	fm.Explanation.value=turnPage.arrDataCacheSet[0][41];
	//职场总面积
	fm.ZSumarea.value=(fm.Sigarea.value*1)+(fm.Grparea.value*1)+(fm.Bankarea.value*1)+(fm.Healtharea.value*1)+(fm.Servicearea.value*1)+(fm.Jointarea.value*1)+(fm.Manageroffice.value*1)+(fm.Departpersno.value*1)+(fm.Planfinance.value*1)+(fm.Publicarea.value*1)+(fm.Otherarea.value*1);
	//合同违约金及保证金
	fm.Marginfee.value=turnPage.arrDataCacheSet[0][42];
	fm.Marginpaydate.value=turnPage.arrDataCacheSet[0][43];
	fm.Penaltyfee.value=turnPage.arrDataCacheSet[0][44];

	//备注信息
	fm.Remark.value=turnPage.arrDataCacheSet[0][45];
	//批文下载文件名及路径
	fm.filename.value=turnPage.arrDataCacheSet[0][46];
	fm.filepath.value=turnPage.arrDataCacheSet[0][47];
	//审批不通过时的备注
	document.getElementsByName("Rremark")[0].value=turnPage.arrDataCacheSet[0][48];

	//计算租金，物业费，装修费
	var state=turnPage.arrDataCacheSet[0][49];
	if(fm.flag.value=="02")
	{
	    fm.PreNextPlaceno.value=turnPage.arrDataCacheSet[0][50];
	}
    if(fm.flag.value=="03")
	{
	    fm.PreChanPlaceno.value=turnPage.arrDataCacheSet[0][50];
	}
	showMoneyInfo(state);
}

function showMoneyInfo(state) 
{
	var rantSql='';
	var profeeSql = '';
	var decorationSql = '';
	var sumrantSql = '';
	var sumprofeeSql = '';
	var sumdecorationSql = '';
	var sumdecorationSql1 = '';
	RelateGrid.clearData();
	PropertyGrid.clearData();
	DecorationGrid.clearData();
	if(fm.flag.value=="04")//撤租
	{
		rantSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '01' order by paybegindate";
		profeeSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '02' order by paybegindate";
		decorationSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '03' order by paybegindate";
		sumrantSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '01'";
		sumprofeeSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '02'";
		sumdecorationSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '03'";
	}
	else if(fm.flag.value=="06")//装修费补录
	{
		rantSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '01' order by paybegindate";
		profeeSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '02' order by paybegindate";
		decorationSql ="select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFeeB where placeno = '" + fm.codeno.value + "' and feetype = '03' and Standbystring1 = '"+fm.CTSerialno.value+"' order by paybegindate";
		sumrantSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '01'";
		sumprofeeSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '02'";
		sumdecorationSql = "select nvl(sum(money),0) from LIPlaceRentFeeB where placeno = '" + fm.codeno.value + "' and feetype = '03' and Standbystring1 = '"+fm.CTSerialno.value+"'";
		
	}
	else
	{
		rantSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFeeB where placeno = '" + fm.codeno.value + "' and feetype = '01'  and Standbystring1 = '"+fm.CTSerialno.value+"' order by paybegindate";
		profeeSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFeeB where placeno = '" + fm.codeno.value + "' and feetype = '02'   and Standbystring1 = '"+fm.CTSerialno.value+"' order by paybegindate";
		decorationSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFeeB where placeno = '" + fm.codeno.value + "' and feetype = '03'  and Standbystring1 = '"+fm.CTSerialno.value+"' order by paybegindate";
		sumrantSql = "select nvl(sum(money),0) from LIPlaceRentFeeB where placeno = '" + fm.codeno.value + "' and feetype = '01' and Standbystring1 = '"+fm.CTSerialno.value+"'";
		sumprofeeSql = "select nvl(sum(money),0) from LIPlaceRentFeeB where placeno = '" + fm.codeno.value + "' and feetype = '02' and Standbystring1 = '"+fm.CTSerialno.value+"'";
		sumdecorationSql = "select nvl(sum(money),0) from LIPlaceRentFeeB where placeno = '" + fm.codeno.value + "' and feetype = '03' and Standbystring1 = '"+fm.CTSerialno.value+"'";
	}
	if (tApptype == "4"){
		//租金
		turnPageRelate.strQueryResult = easyQueryVer3(rantSql, 1, 1, 1);
		if(turnPageRelate.strQueryResult!=false){
			turnPageRelate.arrDataCacheSet = clearArrayElements(turnPageRelate.arrDataCacheSet);
			turnPageRelate.arrDataCacheSet = decodeEasyQueryResult(turnPageRelate.strQueryResult);
			RelateGrid.clearData();
			for(var Relate=0;Relate<turnPageRelate.arrDataCacheSet.length;Relate=Relate+1){
			   RelateGrid.addOne();
			   RelateGrid.setRowColData(Relate,1,turnPageRelate.arrDataCacheSet[Relate][0]);
			   RelateGrid.setRowColData(Relate,2,turnPageRelate.arrDataCacheSet[Relate][1]);
			   RelateGrid.setRowColData(Relate,3,turnPageRelate.arrDataCacheSet[Relate][2]);
			   RelateGrid.setRowColData(Relate,4,turnPageRelate.arrDataCacheSet[Relate][3]);
			};
		}
	    //物业费
		turnPageProperty.strQueryResult = easyQueryVer3(profeeSql, 1, 1, 1);
		if(turnPageProperty.strQueryResult!=false){
			turnPageProperty.arrDataCacheSet = clearArrayElements(turnPageProperty.arrDataCacheSet);
			turnPageProperty.arrDataCacheSet = decodeEasyQueryResult(turnPageProperty.strQueryResult);
			PropertyGrid.clearData();
			for(var Property=0;Property<turnPageProperty.arrDataCacheSet.length;Property=Property+1){
		       PropertyGrid.addOne();
		       PropertyGrid.setRowColData(Property,1,turnPageProperty.arrDataCacheSet[Property][0]);
		       PropertyGrid.setRowColData(Property,2,turnPageProperty.arrDataCacheSet[Property][1]);
		       PropertyGrid.setRowColData(Property,3,turnPageProperty.arrDataCacheSet[Property][2]);
		       PropertyGrid.setRowColData(Property,4,turnPageProperty.arrDataCacheSet[Property][3]);
			};
		}
	    //装修费
	    turnPageDecoration.strQueryResult = easyQueryVer3(decorationSql, 1, 1, 1);
	    if(turnPageDecoration.strQueryResult!=false){
		    turnPageDecoration.arrDataCacheSet = clearArrayElements(turnPageDecoration.arrDataCacheSet);
		    turnPageDecoration.arrDataCacheSet = decodeEasyQueryResult(turnPageDecoration.strQueryResult);
		    DecorationGrid.clearData();
		    for(var Decoration=0;Decoration<turnPageDecoration.arrDataCacheSet.length;Decoration=Decoration+1){
		       DecorationGrid.addOne();
		       DecorationGrid.setRowColData(Decoration,1,turnPageDecoration.arrDataCacheSet[Decoration][0]);
		       DecorationGrid.setRowColData(Decoration,2,turnPageDecoration.arrDataCacheSet[Decoration][1]);
		       DecorationGrid.setRowColData(Decoration,3,turnPageDecoration.arrDataCacheSet[Decoration][2]);
		       DecorationGrid.setRowColData(Decoration,4,turnPageDecoration.arrDataCacheSet[Decoration][3]);
		    };
	    }
	}else{
		turnPageRelate.queryModal(rantSql, RelateGrid);
		turnPageProperty.queryModal(profeeSql, PropertyGrid);
		turnPageDecoration.queryModal(decorationSql, DecorationGrid);
	}
	
	
	//获取总费用
	turnPage.strQueryResult = easyQueryVer3(sumrantSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumRenFee.value = turnPage.arrDataCacheSet[0][0];
	
	turnPage.strQueryResult = easyQueryVer3(sumprofeeSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumProFee.value = turnPage.arrDataCacheSet[0][0];
	
	turnPage.strQueryResult = easyQueryVer3(sumdecorationSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumDecFee.value = turnPage.arrDataCacheSet[0][0];
	// 应收已付租金
	if (state == "03" || state == "04") {
		setMoney(state);
		if(state == "03" ){
		    calculateFee(fm.Enddate.value,fm.Placenorefer.value);
		    
		}else{
		    calculateFee(fm.Enddate.value,fm.codeno.value);
		}
		
	}
	//续租
	if (state == "02") {
		setMoneyX();
	}
}
function setMoneyX(){
    var xSql="select Marginfee,Marginfeegetdate from LIPlaceChangeTrace where Serialno='"+fm.CTSerialno.value+"'";
    turnPage.strQueryResult = easyQueryVer3(xSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	fm.XBaoZhFee.value = turnPage.arrDataCacheSet[0][0];//保证金
	fm.XBaoZhDate.value = turnPage.arrDataCacheSet[0][1];//保证金退还日期
}
function setMoney(state) {

	if (state == "03") {
		var tSql = "select lc.Enddate,lc.Shouldgetfee,lc.Shouldgetdate,lc.Apenaltyfee,lc.Apenaltyfeegetdate,lc.Marginfee,lc.Marginfeegetdate,lr.Penaltyfee from LIPlaceChangeTrace lc,LIPlaceRentInfo lr where lc.placeno = '" + fm.codeno.value + "' and lc.Placenorefer=lr.placeno  and lc.Serialno='"+fm.CTSerialno.value+"'";
	} else if(state=="04"){
		var tSql = "select lc.Enddate,lc.Shouldgetfee,lc.Shouldgetdate,lc.Apenaltyfee,lc.Apenaltyfeegetdate,lc.Marginfee,lc.Marginfeegetdate,lr.Penaltyfee from LIPlaceChangeTrace lc,LIPlaceRentInfo lr where lc.placeno = '" + fm.codeno.value + "' and lc.placeno=lr.placeno  and lc.Serialno='"+fm.CTSerialno.value+"'";
	}
	turnPage.strQueryResult = easyQueryVer3(tSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	fm.StopDate.value = turnPage.arrDataCacheSet[0][0];   //停用时间
	fm.AYingFee.value = turnPage.arrDataCacheSet[0][1];//应收已付款金额
	fm.YingDate.value = turnPage.arrDataCacheSet[0][2];//应收已付款收回日期
	fm.AWeiYueFee.value = turnPage.arrDataCacheSet[0][3];  //实际违约金
	fm.AWeiYueDate.value = turnPage.arrDataCacheSet[0][4];//违约金支付日期
	fm.BaoZhFee.value = turnPage.arrDataCacheSet[0][5];//保证金
	fm.BaoZhDate.value = turnPage.arrDataCacheSet[0][6];//保证金退还日期
	fm.SWeiYueFee.value= turnPage.arrDataCacheSet[0][7]//合同违约金
}

function placeIn() {

	var placeIn1 = document.getElementById("divPlaceIn1");
	var placeIn2 = document.getElementById("divPlaceIn2");
	var placeIn3 = document.getElementById("divPlaceIn3");
	var placeIn4 = document.getElementById("divPlaceIn4");
	if(fm.appstateType.value=="4"){
	var placeInFile = document.getElementById("divPlaceInFile");
	}
	placeIn1.style.display = "none";
	placeIn2.style.display = "none";
	placeIn3.style.display = "none";
	placeIn4.style.display = "none";
	if(fm.appstateType.value=="4"){
		placeInFile.style.display = "none";
	}

	var tsel = ApproveGrid.getSelNo();
	if (tsel == null || tsel == "") {
		alert("\u8bf7\u5148\u9009\u62e9\u4e00\u6761\u6570\u636e\uff01");
		return false;
	}
	fm.codeno.value = ApproveGrid.getRowColData(tsel - 1, 1);
	fm.CTSerialno.value = ApproveGrid.getRowColData(tsel - 1, 8);
	var changetypeSql="select lc.Changetype from LIPlaceChangeTrace lc where lc.placeno='"+fm.codeno.value+"' and lc.Serialno='"+fm.CTSerialno.value+"'";
	turnPage.strQueryResult = easyQueryVer3(changetypeSql, 1, 1, 1);
	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.flag.value=turnPage.arrDataCacheSet[0][0];
	if(fm.flag.value=="02" || fm.flag.value=="03"){
		var placenoreferSql = "select lc.Placenorefer from LIPlaceChangeTrace lc where lc.placeno='"+fm.codeno.value+"' and lc.Serialno='"+fm.CTSerialno.value+"'";
		turnPage.strQueryResult = easyQueryVer3(placenoreferSql, 1, 1, 1);
		// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
		// 查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		fm.Placenorefer.value=turnPage.arrDataCacheSet[0][0];
		//alert("fm.Placenorefer.value"+fm.Placenorefer.value);
	}
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("\u804c\u573a\u7f16\u7801\u4e0d\u5b58\u5728\uff0c\u8bf7\u91cd\u65b0\u8f93\u5165\uff01");
		fm.codeno.value = "";
		return false;
	}
	placeIn1.style.display = "";
	placeIn3.style.display = "";
	if(fm.appstateType.value=="4"){
		placeInFile.style.display = "";
	}
	if (fm.flag.value== "03" || fm.flag.value == "04") {
		placeIn2.style.display = "";
	}
	if (fm.flag.value== "02") {
	    placeIn4.style.display = "";
	}
	RelateGrid.clearData();
	PropertyGrid.clearData();
	DecorationGrid.clearData();
	showPlaceInfo();
}

//上传附件
function fileImp() {
	if (imp.all('impFileName').value ==null || imp.all("impFileName").value == "") {
		alert("请选择需要上传的文件");
		return false;
	}

	var i = 0;
	var ImportFile = imp.all('impFileName').value.toLowerCase();
	var Placeno = document.getElementsByName("Placeno")[0].value;
	if (Placeno == null || Placeno == "") {
		alert("编码信息尚未生成，请先录入其他信息！");
		return false;
	}
	getPath();
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	imp.action = "./PlInFileSave.jsp?ImportPath=" + ImportPath + "&ImportFile="+ ImportFile + "&CodeId=" + Placeno;
	imp.submit(); // 提交

}

function getPath() {
	// 书写SQL语句
	var strSQL = "select SysvarValue from ldsysvar where sysvar ='ApprovalPath'";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有找到上传路径");
		return false;
	}
	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	ImportPath = turnPage.arrDataCacheSet[0][0];
}
//上传附件返回
function afterSubmit1(FlagStr, content, filename) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ content;
		showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }else{
		var HidFileName = document.getElementsByName("filename")[0];
		var HidFilePath = document.getElementsByName("filepath")[0];
		HidFileName.value = filename;
		HidFilePath.value = ImportPath;
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="+ content;
		showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

//批文下载按钮
function downloadmode() {
	//alert("../"+fm.filepath.value+fm.filename.value);
	window.location.href = ".."+fm.filepath.value+ fm.filename.value;
}

//点击RadioBox时，触发该函数，若为续租职场，且原合同的实际租期止期大于新合同的租期起期，则提示信息，若不是，则不提示
function AllDays(){
	var ttsel = ApproveGrid.getSelNo();
	var Newcodeno = ApproveGrid.getRowColData(ttsel - 1, 1);
	var newStartDate = ApproveGrid.getRowColData(ttsel - 1, 4);
	var oldActualEndDate = ApproveGrid.getRowColData(ttsel - 1, 9);
	var type = ApproveGrid.getRowColData(ttsel - 1, 3);
	if(type == '续租职场'){
//		alert("是续租职场");
//		var ActualenddateSQL = "select Actualenddate from LIPlaceRentInfo where placeno=(select placenorefer from LIPlaceChangeTrace c where c.placeno = '"+Newcodeno+"') ";
//		var result1 = easyExecSql(ActualenddateSQL);
//		var Actualenddate = result1[0][0];
//		var daysSQL = "select days('"+newStartDate+"') - days('"+Actualenddate+"') from dual ";
//		var result2 = easyExecSql(daysSQL);
//		var Actualenddate = result2[0][0];
//		alert("原止："+oldActualEndDate+";新起："+newStartDate);
		if(oldActualEndDate > newStartDate){
			alert("请核实续租起期，通常情况下应为原合同结束次日");
		}
	}
}
