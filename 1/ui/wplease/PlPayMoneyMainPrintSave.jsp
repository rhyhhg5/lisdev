<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：按险种打印操作员日结
	//程序功能：
	//创建日期：2002-12-12
	//创建人  ：hdl
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%
	request.setCharacterEncoding("GBK");

	System.out.println("start");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	//后面要执行的动作：添加，修改，删除
	String FlagStr = "true";
	String Content = "打印成功";
	boolean operFlag = true;
	String tManageCom = request.getParameter("CompanyCode");
	String tBeginDate = request.getParameter("BeginDate");
	String tEndDate = request.getParameter("EndDate");
	String strOperation = "PRINT";
	
	System.out.println("要打印的信息是：");
	System.out.println("管理机构：" + tManageCom);
	System.out.println("统计起期：" + tBeginDate);
	System.out.println("统计止期：" + tEndDate);
	System.out.println(tG.Operator);


	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	
	TransferData transferdate=new TransferData();
	transferdate.setNameAndValue("CompanyCode",tManageCom);
	transferdate.setNameAndValue("BeginDate",tBeginDate);
	transferdate.setNameAndValue("EndDate",tEndDate);
	transferdate.setNameAndValue("tG",tG);
	
	XmlExport txmlExport = new XmlExport();
    try{
		PlPayMoneyMainPrintUI tPlPayMoneyMainPrintUI = new PlPayMoneyMainPrintUI();
		if (!tPlPayMoneyMainPrintUI.submitData(transferdate, strOperation)) {
		    if (tPlPayMoneyMainPrintUI.mErrors.needDealError()) {
					mErrors.copyAllErrors(tPlPayMoneyMainPrintUI.mErrors);
					FlagStr = "Fail";
	                Content = "打印失败，原因是"+mErrors.getFirstError();
	                operFlag = false;
				} else {
				    FlagStr = "Fail";
	                Content = "打印失败，没有获取到详细报错信息！";
	                operFlag = false;
				}
		}else{
		    mResult = tPlPayMoneyMainPrintUI.getResult();
		    txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);
		    if (txmlExport == null) {
			    System.out.println("null");
			    return;
		    }
		    ExeSQL tExeSQL = new ExeSQL();
			//获取临时文件名
			String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
			String strFilePath = tExeSQL.getOneValue(strSql);
			String strVFFileName = strFilePath + tG.Operator + "_"+ FileQueue.getFileName() + ".vts";
			//获取存放临时文件的路径
			String strRealPath = application.getRealPath("/").replace('\\', '/');
			String strVFPathName = strRealPath + "/" + strVFFileName;
			CombineVts tcombineVts = null;
			if (operFlag == true) {
				//合并VTS文件
				String strTemplatePath = application.getRealPath("f1print/picctemplate/")+ "/";
				tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
				ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
				tcombineVts.output(dataStream);
		
				//把dataStream存储到磁盘文件
				System.out.println("存储文件到" + strVFPathName);
				AccessVtsFile.saveToFile(dataStream, strVFPathName);
				System.out.println("==> Write VTS file to disk ");
				System.out.println("===strVFFileName : " + strVFFileName);
				//本来打算采用get方式来传递文件路径
				//缺点是文件路径被暴露
				System.out.println("---passing params by get method");
				response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="+ strVFPathName);
			}
		}
		
	}catch(Exception e){
	    e.printStackTrace();
	    FlagStr = "Fail";
	    Content = "打印失败，原因是："+e.toString();
	}
	
	
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>