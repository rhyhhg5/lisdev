
<%
//name :PlaceInInit.jsp
//function : 
//Creator :huodonglei
//date :2011-06-09
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 		GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
 	String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);
 %>
<script language="JavaScript">

function initForm(){
  try{

    initQueryGrid();
  }
  catch(re){
    alert("PlInInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initQueryGrid() {
  var iArray = new Array();
  try{
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="职场编码"; 
    iArray[1][1]="15px"; 
    iArray[1][2]=100; 
    iArray[1][3]=1; 

    iArray[2]=new Array();
    iArray[2][0]="管理机构";    //列名
    iArray[2][1]="15px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="批文号";    //列名
    iArray[3][1]="25px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="起租日期";    //列名
    iArray[4][1]="25px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="止租日期";    //列名
    iArray[5][1]="25px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="机构名称";    //列名
    iArray[4][1]="25px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="公司级别";    //列名
    iArray[4][1]="25px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="地址";    //列名
    iArray[4][1]="225px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    QueryGrid = new MulLineEnter( "fm" , "QueryGrid" );
    QueryGrid.mulLineCount = 0;
    QueryGrid.displayTitle = 1;
    QueryGrid.loadMulLine(iArray);
    QueryGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("QueryGrid初始化时出错:"+ex);
  }
}
</script>