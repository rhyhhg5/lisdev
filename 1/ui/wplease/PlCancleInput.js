var showInfo;
var ImportPath;
var turnPage = new turnPageClass();

//提交页面
function submitForm() 
{
	if(verifyInput()){
	    var r = confirm("该操作将撤销该职场，请仔细核对！");
	    if (r == true) {
			var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
			showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.submit();
		}else{
		   return false;
		}
	}
}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	initForm();
}

// 查询职场信息并返回到前台页面
function showPlaceInfo() {
	var codeno = fm.codeno.value;
	var infoSql="select Placeno,Managecom,case length(Appno) when 13 then substr(Appno, 1, 3) else substr(Appno, 1, 2) end, case length(Appno) when 13 then substr(Appno, 4, 4) else substr(Appno, 3, 4) end,case length(Appno) when 13 then substr(Appno, 8, 4) else substr(Appno, 7, 4) end,case length(Appno) when 13 then substr(Appno, 12, 2) else substr(Appno, 11, 2) end ," +
			"Comlevel,case Comlevel when '1' then '省级分公司' when '2' then '地市级分公司' when '3' then '四级机构-区' when '4' then '四级机构-县' end,Comname,Begindate,Enddate,Address,Lessor,Houseno,Province,City,Isrentregister,case Isrentregister when '1' then '是' when '0' then '否' end," +
			"Islessorassociate,case Islessorassociate when '1' then '是' when '0' then '否' end,Islessorright,case Islessorright when '1' then '是' when '0' then '否' end,Ishouserent,case Ishouserent when '1' then '是' when '0' then '否' end,Isplaceoffice,case Isplaceoffice when '1' then '是' when '0' then '否' end,Ishousegroup,case Ishousegroup when '1' then '是' when '0' then '否' end," +
			"Ishousefarm,case Ishousefarm when '1' then '是' when '0' then '否' end,Sigarea,Grparea,Bankarea,Healtharea,Servicearea,Jointarea,Manageroffice,Departpersno,Planfinance,Publicarea,Otherarea,(case when Explanation is null then '0' else Explanation end),Marginfee,Marginpaydate,Penaltyfee,Remake,Filename,Filepath" +
			" from LIPlaceRentInfo where placeno='"+fm.codeno.value+"'";
	turnPage.strQueryResult = easyQueryVer3(infoSql, 1, 1, 1);
	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	//基本信息
	fm.Placeno.value=turnPage.arrDataCacheSet[0][0];
	fm.Managecom.value=turnPage.arrDataCacheSet[0][1];
	fm.ApprovalType.value=turnPage.arrDataCacheSet[0][2];
	fm.ApprovalYear.value=turnPage.arrDataCacheSet[0][3];
	fm.ApprovalCode.value=turnPage.arrDataCacheSet[0][4];
	fm.ApprovalSeria.value=turnPage.arrDataCacheSet[0][5];
	fm.Comlevel.value=turnPage.arrDataCacheSet[0][6];
	fm.Comlevelname.value=turnPage.arrDataCacheSet[0][7];
	fm.Comname.value=turnPage.arrDataCacheSet[0][8];
	fm.Begindate.value=turnPage.arrDataCacheSet[0][9];
	fm.Enddate.value=turnPage.arrDataCacheSet[0][10];
	fm.Address.value=turnPage.arrDataCacheSet[0][11];
	fm.Lessor.value=turnPage.arrDataCacheSet[0][12];
	fm.Houseno.value=turnPage.arrDataCacheSet[0][13];
	fm.Province.value=turnPage.arrDataCacheSet[0][14];
	fm.City.value=turnPage.arrDataCacheSet[0][15];
	fm.Isrentregister.value=turnPage.arrDataCacheSet[0][16];
	fm.IsRentRegisterName.value=turnPage.arrDataCacheSet[0][17];
	fm.Islessorassociate.value=turnPage.arrDataCacheSet[0][18];
	fm.IslessorassociateName.value=turnPage.arrDataCacheSet[0][19];
	fm.Islessorright.value=turnPage.arrDataCacheSet[0][20];
	fm.IslessorrightName.value=turnPage.arrDataCacheSet[0][21];
	fm.Ishouserent.value=turnPage.arrDataCacheSet[0][22];
	fm.IshouserentName.value=turnPage.arrDataCacheSet[0][23];
	fm.Isplaceoffice.value=turnPage.arrDataCacheSet[0][24];
	fm.IsplaceofficeName.value=turnPage.arrDataCacheSet[0][25];
	fm.Ishousegroup.value=turnPage.arrDataCacheSet[0][26];
	fm.IshousegroupName.value=turnPage.arrDataCacheSet[0][27];
	fm.Ishousefarm.value=turnPage.arrDataCacheSet[0][28];
	fm.IshousefarmName.value=turnPage.arrDataCacheSet[0][29];
	//职场面积
	fm.Sigarea.value=turnPage.arrDataCacheSet[0][30];
	fm.Grparea.value=turnPage.arrDataCacheSet[0][31];
	fm.Bankarea.value=turnPage.arrDataCacheSet[0][32];
	fm.Healtharea.value=turnPage.arrDataCacheSet[0][33];
	fm.Servicearea.value=turnPage.arrDataCacheSet[0][34];
	fm.Jointarea.value=turnPage.arrDataCacheSet[0][35];
	fm.Manageroffice.value=turnPage.arrDataCacheSet[0][36];
	fm.Departpersno.value=turnPage.arrDataCacheSet[0][37];
	fm.Planfinance.value=turnPage.arrDataCacheSet[0][38];
	fm.Publicarea.value=turnPage.arrDataCacheSet[0][39];	
	fm.Otherarea.value=turnPage.arrDataCacheSet[0][40];
	fm.Explanation.value=turnPage.arrDataCacheSet[0][41];
	//职场总面积
	fm.ZSumarea.value=(fm.Sigarea.value*1)+(fm.Grparea.value*1)+(fm.Bankarea.value*1)+(fm.Healtharea.value*1)+(fm.Servicearea.value*1)+(fm.Jointarea.value*1)+(fm.Manageroffice.value*1)+(fm.Departpersno.value*1)+(fm.Planfinance.value*1)+(fm.Publicarea.value*1)+(fm.Otherarea.value*1);
	//合同违约金及保证金
	fm.Marginfee.value=turnPage.arrDataCacheSet[0][42];
	fm.Marginpaydate.value=turnPage.arrDataCacheSet[0][43];
	fm.Penaltyfee.value=turnPage.arrDataCacheSet[0][44];
	//备注信息
	fm.Remark.value=turnPage.arrDataCacheSet[0][45];
	//批文下载文件名及路径
	fm.filename.value=turnPage.arrDataCacheSet[0][46];
	fm.filepath.value=turnPage.arrDataCacheSet[0][47];
	fm.SWeiYueFee.value= fm.Penaltyfee.value;
	fm.BaoZhFee.value= fm.Marginfee.value;
	//fm.shouldFee.value = (1*fm.shouldRenFee.value+1*fm.shouldProFee.value).toFixed(2);
	//计算租金，物业费，装修费
	showMoneyInfo();
}

function showMoneyInfo() {
	var rantSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '"
			+ fm.codeno.value + "' and feetype = '01'";
	var profeeSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '"
			+ fm.codeno.value + "' and feetype = '02'";
	var decorationSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '"
			+ fm.codeno.value + "' and feetype = '03'";

	var sumrantSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '"
			+ fm.codeno.value + "' and feetype = '01'";
	var sumprofeeSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '"
			+ fm.codeno.value + "' and feetype = '02'";
	var sumdecorationSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '"
			+ fm.codeno.value + "' and feetype = '03'";

	turnPage.queryModal(rantSql, RelateGrid);
	turnPage.queryModal(profeeSql, PropertyGrid);
	turnPage.queryModal(decorationSql, DecorationGrid);

	turnPage.strQueryResult = easyQueryVer3(sumrantSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumRenFee.value = turnPage.arrDataCacheSet[0][0];

	turnPage.strQueryResult = easyQueryVer3(sumprofeeSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumProFee.value = turnPage.arrDataCacheSet[0][0];

	turnPage.strQueryResult = easyQueryVer3(sumdecorationSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumDecFee.value = turnPage.arrDataCacheSet[0][0];
                       
}

function getShoudFee()
{
    calculateFee(fm.Enddate.value,fm.codeno1.value);
}

function placeIn() 
{
    var placeIn1 = document.getElementById("divPlaceIn1");
	var placeIn2 = document.getElementById("divPlaceIn2");
	placeIn1.style.display = 'none';
	placeIn2.style.display = 'none';
	fm.codeno.value = fm.codeno1.value;
	if(fm.codeno1.value==null || fm.codeno1.value==""){
	      alert("请先输入拟撤租职场编码！");
	      return false;
	    }
	var strSQL = "select 1 from LIPlaceRentinfo where Placeno='" + fm.codeno.value
	+"' and not exists(select 1 from LIPlaceChangeTrace where Placenorefer='" + fm.codeno.value+ "' and Changestate in('01','02') and Changetype in('02','03'))"
	+" and not exists(select 1 from LIPlaceChangeTrace where Placeno='" + fm.codeno.value+ "' and Changestate in('01','02') and Changetype in('04','05','06'))";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("撤租职场编码不存在或该职场正处于审批状态，请重新输入！");
		fm.codeno1.value = "";
		return false;
	}
	
	placeIn1.style.display = '';
	placeIn2.style.display = '';
	showPlaceInfo();
}

//批文下载按钮
function downloadmode()
{
	//alert("../"+fm.filepath.value+fm.filename.value);
	window.location.href = ".."+fm.filepath.value+ fm.filename.value;
}
function cancleConf()
{
	submitForm();
}