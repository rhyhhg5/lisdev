<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PlDelSave.jsp
//程序功能：
//创建日期：2013-03-26
//创建人  ： hdl
//更新记录：  更新人    更新日期     更新原因/内容
//     	 
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.wpleace.*"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数

  PlDelUI mPlDelUI  = new PlDelUI();

  //输出参数
  CErrors tError = new CErrors();
  String tOperate="Del";
  String FlagStr = "true";
  String Content = "保存成功";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  System.out.println("开始获取数据==");
  //基本信息  职场编码
  String tCodeId = request.getParameter("DelPlaceNo");
  System.out.println("DelPlaceNo======="+tCodeId);
  System.out.println("基本信息获取完毕！");
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  System.out.println("vdate======="+tVData.size());
  System.out.println("add over");
  try
  {
	  if(!mPlDelUI.submitData(tVData,tOperate,tCodeId)){
	     if (mPlDelUI.mErrors.needDealError())
           {
               tError.copyAllErrors(mPlDelUI.mErrors);
               FlagStr = "Fail";
               Content=tError.getFirstError();
           }
           else
           {
               Content="保存失败，没有得到报错详细信息！";
               FlagStr = "Fail";
           }
	  }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //添加各种预处理

  %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>