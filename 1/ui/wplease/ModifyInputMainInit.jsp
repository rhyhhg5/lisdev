
<%
//name :PlaceInInit.jsp
//function : 
//Creator :huodonglei
//date :2011-06-09
%>
<script language="JavaScript">

function initRelateGrid() {
  var iArray = new Array();
  try{
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="支付日期"; 
    iArray[1][1]="15px"; 
    iArray[1][2]=100; 
    iArray[1][3]=1; 

    iArray[2]=new Array();
    iArray[2][0]="支付额度(元)";    //列名
    iArray[2][1]="15px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="支付租期摊销起期";    //列名
    iArray[3][1]="25px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="支付租期摊销止期";    //列名
    iArray[4][1]="25px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    RelateGrid = new MulLineEnter( "fm" , "RelateGrid" );
    RelateGrid.mulLineCount = 0;
    RelateGrid.displayTitle = 1;
    RelateGrid.loadMulLine(iArray);
    RelateGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("RelateGrid初始化时出错:"+ex);
  }
}
function initPropertyGrid() {
  var iArray = new Array();
  try{
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="支付日期"; 
    iArray[1][1]="15px"; 
    iArray[1][2]=100; 
    iArray[1][3]=1; 

    iArray[2]=new Array();
    iArray[2][0]="支付额度(元)";    //列名
    iArray[2][1]="15px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="支付租期摊销起期";    //列名
    iArray[3][1]="25px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="支付租期摊销止期";    //列名
    iArray[4][1]="25px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    PropertyGrid = new MulLineEnter( "fm" , "PropertyGrid" );
    PropertyGrid.mulLineCount = 0;
    PropertyGrid.displayTitle = 1;
    PropertyGrid.loadMulLine(iArray);
    PropertyGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("PropertyGrid初始化时出错:"+ex);
  }
}
function initDecorationGrid() {
  var iArray = new Array();
  try{
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="支付日期"; 
    iArray[1][1]="15px"; 
    iArray[1][2]=100; 
    iArray[1][3]=1; 

    iArray[2]=new Array();
    iArray[2][0]="支付额度(元)";    //列名
    iArray[2][1]="15px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="支付租期摊销起期";    //列名
    iArray[3][1]="25px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="支付租期摊销止期";    //列名
    iArray[4][1]="25px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=1;             //是否允许输入,1表示允许，0表示不允许
    
    DecorationGrid = new MulLineEnter( "fm" , "DecorationGrid" );
    DecorationGrid.mulLineCount = 0;
    DecorationGrid.displayTitle = 1;
    DecorationGrid.loadMulLine(iArray);
    DecorationGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("DecorationGrid初始化时出错:"+ex);
  }
}
</script>