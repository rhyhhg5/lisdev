//费用上传
var turnPageRelate = new turnPageClass();
var turnPageProperty = new turnPageClass();
var turnPageDecoration = new turnPageClass();
function FeeImp() {
	if (feeimp.all('FeeName').value == "") {
		alert("请选择需要上传的文件");
		return;
	}
	var i = 0;
	var ImportFile = feeimp.all('FeeName').value.toLowerCase();
	var Placeno = document.getElementsByName("Placeno")[0].value;
	if (Placeno == null || Placeno == "") {
		alert("编码信息尚未生成，请先录入其他信息！");
		return false;
	}
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	feeimp.action = "./PlInFeeSave.jsp?ImportFile="+ ImportFile + "&CodeId=" + Placeno;
	feeimp.submit(); // 提交

}
//费用上传返回
function afterSubmitFee(FlagStr, content, filename) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ content;
		showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }else{
	  	RelateGrid.clearData();
	  	PropertyGrid.clearData();
	  	DecorationGrid.clearData();
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="+ content;
		showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		var ttPlaceno = document.getElementsByName("Placeno")[0].value;
		var rentSQL="select PayDate,Money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from PlaceFeeImport where placeno='"+ttPlaceno+"' and FeeType='01' order by paybegindate";
		var wuSQL="select PayDate,Money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from PlaceFeeImport where placeno='"+ttPlaceno+"' and FeeType='02' order by paybegindate";
		var zhangSQL="select PayDate,Money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from PlaceFeeImport where placeno='"+ttPlaceno+"' and FeeType='03' order by paybegindate";
		
		//租金
		turnPageRelate.strQueryResult = easyQueryVer3(rentSQL, 1, 1, 1);
		if(turnPageRelate.strQueryResult!=false){
		    turnPageRelate.arrDataCacheSet = clearArrayElements(turnPageRelate.arrDataCacheSet);
		    turnPageRelate.arrDataCacheSet = decodeEasyQueryResult(turnPageRelate.strQueryResult);
		    RelateGrid.clearData();
		    for(var Relate=0;Relate<turnPageRelate.arrDataCacheSet.length;Relate=Relate+1){
		       RelateGrid.addOne();
		       RelateGrid.setRowColData(Relate,1,turnPageRelate.arrDataCacheSet[Relate][0]);
		       RelateGrid.setRowColData(Relate,2,turnPageRelate.arrDataCacheSet[Relate][1]);
		       RelateGrid.setRowColData(Relate,3,turnPageRelate.arrDataCacheSet[Relate][2]);
		       RelateGrid.setRowColData(Relate,4,turnPageRelate.arrDataCacheSet[Relate][3]);
		    };
	    }
        //物业费
		turnPageProperty.strQueryResult = easyQueryVer3(wuSQL, 1, 1, 1);
		if(turnPageProperty.strQueryResult!=false){
	        turnPageProperty.arrDataCacheSet = clearArrayElements(turnPageProperty.arrDataCacheSet);
	        turnPageProperty.arrDataCacheSet = decodeEasyQueryResult(turnPageProperty.strQueryResult);
	        PropertyGrid.clearData();
	        for(var Property=0;Property<turnPageProperty.arrDataCacheSet.length;Property=Property+1){
	           PropertyGrid.addOne();
	           PropertyGrid.setRowColData(Property,1,turnPageProperty.arrDataCacheSet[Property][0]);
	           PropertyGrid.setRowColData(Property,2,turnPageProperty.arrDataCacheSet[Property][1]);
	           PropertyGrid.setRowColData(Property,3,turnPageProperty.arrDataCacheSet[Property][2]);
	           PropertyGrid.setRowColData(Property,4,turnPageProperty.arrDataCacheSet[Property][3]);
	        };
	    }
	    //装修费
	    turnPageDecoration.strQueryResult = easyQueryVer3(zhangSQL, 1, 1, 1);
	    if(turnPageDecoration.strQueryResult!=false){
		    turnPageDecoration.arrDataCacheSet = clearArrayElements(turnPageDecoration.arrDataCacheSet);
		    turnPageDecoration.arrDataCacheSet = decodeEasyQueryResult(turnPageDecoration.strQueryResult);
		    DecorationGrid.clearData();
		    for(var Decoration=0;Decoration<turnPageDecoration.arrDataCacheSet.length;Decoration=Decoration+1){
		       DecorationGrid.addOne();
		       DecorationGrid.setRowColData(Decoration,1,turnPageDecoration.arrDataCacheSet[Decoration][0]);
		       DecorationGrid.setRowColData(Decoration,2,turnPageDecoration.arrDataCacheSet[Decoration][1]);
		       DecorationGrid.setRowColData(Decoration,3,turnPageDecoration.arrDataCacheSet[Decoration][2]);
		       DecorationGrid.setRowColData(Decoration,4,turnPageDecoration.arrDataCacheSet[Decoration][3]);
		    };
	    }
	}
}