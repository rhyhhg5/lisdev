
<%
	//name :PlaceInInit.jsp
	//function : 
	//Creator :huodonglei
	//date :2011-06-09
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="InputMainInit.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String Comcode = tG.ManageCom;
	String CurrentDate = PubFun.getCurrentDate();
	String tCurrentYear = StrTool.getVisaYear(CurrentDate);
	String tCurrentMonth = StrTool.getVisaMonth(CurrentDate);
	String tCurrentDate = StrTool.getVisaDay(CurrentDate);
%>
<script language="JavaScript">
	function initForm() 
	{
		try 
		{
			fm.all('CompanyCode').value = <%=tG.ManageCom%>;
			showAllCodeName();
			initMessageGrid();
		} 
		catch (re) 
		{
			alert("PlInInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	function initMessageGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;

			iArray[1] = new Array();
			iArray[1][0] = "职场编码";
			iArray[1][1] = "50px";
			iArray[1][2] = 100;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "公司代码";
			iArray[2][1] = "30px";
			iArray[2][2] = 100;
			iArray[2][3] = 0;

			iArray[3] = new Array();
			iArray[3][0] = "租期起期"; //列名
			iArray[3][1] = "25px"; //列宽
			iArray[3][2] = 200; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[4] = new Array();
			iArray[4][0] = "租期止期"; //列名
			iArray[4][1] = "25px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[5] = new Array();
			iArray[5][0] = "准租批文号"; //列名
			iArray[5][1] = "25px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[6] = new Array();
			iArray[6][0] = "公司级别"; //列名
			iArray[6][1] = "15px"; //列宽
			iArray[6][2] = 200; //列最大值
			iArray[6][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[7] = new Array();
			iArray[7][0] = "机构名称"; //列名
			iArray[7][1] = "25px"; //列宽
			iArray[7][2] = 200; //列最大值
			iArray[7][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[8] = new Array();
			iArray[8][0] = "地址"; //列名
			iArray[8][1] = "0px"; //列宽
			iArray[8][2] = 200; //列最大值
			iArray[8][3] = 3; //是否允许输入,1表示允许，0表示不允许

			MessageGrid = new MulLineEnter("fm", "MessageGrid");
			MessageGrid.mulLineCount = 0;
			MessageGrid.displayTitle = 1;
			MessageGrid.canSel = 1;
			MessageGrid.hiddenPlus = 1;
			MessageGrid.hiddenSubtraction = 1;
			MessageGrid.loadMulLine(iArray);
			MessageGrid.detailInfo = "单击显示详细信息";
		} catch (ex) {
			alert("MessageGrid初始化时出错:" + ex);
		}
	}
</script>