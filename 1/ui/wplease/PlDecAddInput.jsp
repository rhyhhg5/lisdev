<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//name :PlDecAddInput.jsp
	//function :
	//Creator :huodonglei
	//date :2011-6-8
	//更新记录：  更新人    更新日期     更新原因/内容
//      鞠成富  	2012-09-12 	修改
%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="PlDecAddInput.js"></SCRIPT>
<%@include file="PlDecAddInit.jsp"%>
</head>
<body onload="initElementtype();initForm()">
	<form action="PlDecAddSave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<tr class=common>
				<td class=title>职场编码</td>
				<td><Input class=common name="codeno" id="codeno"
					verify="职场编码|NOTNULL" elementtype=nacessary></td>
			</tr>
			<tr class=common>
				<td colspan="2"><INPUT VALUE="确认" class=cssButton TYPE=button
					onclick="placeIn()"></td>
			</tr>
		</table>
		<hr>
		<div id="divInfos" style="display: none">
		    <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divInfo);"></td>
					<td class=titleImg>原职场信息</td>
				</tr>
			</table>
			<div id="divInfo" style="display: ''">
			  <table class=common>
			    <tr class=common>
			      <TD class=title>公司名称</TD>
				  <TD class=input><Input class=common name=SComName id="SComName" readonly></TD>
				  <Td class=title>总面积</Td>
				  <Td class=input><input class=common name=SumArea id="SumArea"  readonly></Td>
				  <Td class=title>租期起期</Td>
				  <TD class=input><Input name=SStartDate class=common  readonly></TD>
				</tr>
				<tr class=common>
				  <Td class=title>租期止期</Td>
				  <TD class=input><Input name=SEndDate class=common  readonly></TD>
				  <Td class=title>地址</Td>
				  <Td class=input colspan="3"><input class=common name=SAddress id="SAddress"  readonly></Td>
			    </tr>
			  </table>
			</div>
	<table>
   	     <tr>
             <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	    OnClick= "showPage(this,divDecorationInput);"></td>
    	     <td class= titleImg>租期内装修费录入</td>
    	 </tr>
    </table>
     <span style="color:red">请注意：支付日期录入格式YYYY-MM-DD，摊销起止期格式YYYY-MM</span>
    <Div id= "divDecorationInput" style= "display: ''">
		    <table  class= common>
		        <tr  class= common>
		          <td text-align: left colSpan=1><span id="spanDecorationGrid" ></span>
		      	  </td>
		      	  <td class= common>
		      	      租期内装修费总额
		      	  </td>
		      	  <td class= input>
		      	      <Input class= common name= SumDecFee elementtype=nacessary verify="租期内装修费总额|NOTNULL" readonly > 
		      	  </td>
		    	</tr>
		  	</table>
    </Div>
    <br>
    <INPUT VALUE="保存" class=cssButton TYPE=button onclick="submitForm()">
		</div>
		</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>