
<jsp:directive.page
	import="com.sinosoft.lis.certify.SysOperatorNoticeBL" />
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：职场管理-》装修成本统计表
	//程序功能：
	//创建日期：2012-06-02
	//创建人  ：hdl
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%
	System.out.println("start");
	String mDay[] = new String[2];
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	//输出参数
	CError cError = new CError();
	
	boolean operFlag = true;
	
	String tPlaceNo = request.getParameter("CodeId");           //职场编号
	String tManageCom = request.getParameter("CompanyCode");    //公司代码
	String tCompanyLevel = request.getParameter("CompanyLevel");   //公司级别
	String tBeginDate = request.getParameter("BeginDate");      //统计起期
	String tEndDate = request.getParameter("EndDate");        //统计止期
	String strOperation = request.getParameter("fmtransact");   //获取操作类型

	System.out.println("要打印的信息是：");
	System.out.println("职场编码：" + tPlaceNo);
	System.out.println("管理机构：" + tManageCom);
	System.out.println("公司级别：" + tCompanyLevel);
	System.out.println("统计起期：" + tBeginDate);
	System.out.println("统计止期：" + tEndDate);
	System.out.println(tG.Operator);
	System.out.println(tG.ManageCom);

	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	

	tVData.addElement(tPlaceNo);
	tVData.addElement(tManageCom);
	tVData.addElement(tCompanyLevel);
	tVData.addElement(tBeginDate);
	tVData.addElement(tEndDate);
	tVData.addElement(tG);

	XmlExport txmlExport = new XmlExport();

	PlDecPrintUI tPlDecPrintUI = new PlDecPrintUI();
	if (!tPlDecPrintUI.submitData(tVData, strOperation)) {
		operFlag = false;
		mErrors.copyAllErrors(tPlDecPrintUI.mErrors);
		cError.moduleName = "PlDecPrintSave.jsp";
		cError.functionName = "submitData";
		cError.errorMessage = "FinDayCheckUI发生错误，但是没有提供详细的出错信息";
		mErrors.addOneError(cError);
	}
	mResult = tPlDecPrintUI.getResult();

	txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);
	if (txmlExport == null) {
		System.out.println("null");
		return;
	}
	
	ExeSQL tExeSQL = new ExeSQL();

	//获取临时文件名
	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	String strFilePath = tExeSQL.getOneValue(strSql);
	String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName() + ".vts";
	//获取存放临时文件的路径
	String strRealPath = application.getRealPath("/").replace('\\', '/');
	String strVFPathName = strRealPath + "/" + strVFFileName;

	CombineVts tcombineVts = null;

	if (operFlag == true) {
		//合并VTS文件
		String strTemplatePath = application
		.getRealPath("f1print/picctemplate/")
		+ "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),
		strTemplatePath);

		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);

		//把dataStream存储到磁盘文件
		System.out.println("存储文件到" + strVFPathName);
		AccessVtsFile.saveToFile(dataStream, strVFPathName);
		System.out.println("==> Write VTS file to disk ");

		System.out.println("===strVFFileName : " + strVFFileName);
		//本来打算采用get方式来传递文件路径
		//缺点是文件路径被暴露
		System.out.println("---passing params by get method");
		response
		.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="
				+ strVFPathName);
	}
%>
