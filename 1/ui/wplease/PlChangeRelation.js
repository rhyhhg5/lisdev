
var showInfo;
var turnPage = new turnPageClass();
var turnPageRelate = new turnPageClass();
var turnPageProperty = new turnPageClass();
var turnPageDecoration = new turnPageClass();
var manageCom;
var xmlHttp ; 

function appInit(Comcode){
	manageCom = Comcode;
} 

// 创建核心对象
function createXMLHttpRequest() {
	if(window.XMLHttpRequest) { //Mozilla 浏览器
		xmlHttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) { // IE浏览器
		try {
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}
}

function sendRequest(url) {
		createXMLHttpRequest();
		xmlHttp.open("POST", url);
		xmlHttp.onreadystatechange = processResponse;//指定响应函数
		xmlHttp.send();  // 发送请求
}

function placeIn() {
	
	var placeno=document.getElementById("placeno");
    if(placeno.value==null || placeno.value==""){
      alert("请先输入查询职场编码！");
      return false;
    }
    //var strSQL = "select 1 from LIPlaceRentinfo where Placeno='"+codeno.value+"' and not exists(select 1 from LIPlaceChangeTrace where (placeno='"+codeno.value+"' or Placenorefer='"+codeno.value+"') and Changestate in('01','02'))";			 
    var strSQL = "select 1 from LIPlaceRentinfo where Placeno='" + fm.placeno.value
	+"' and exists(select 1 from LIPlaceChangeTrace where (Placenorefer='" + fm.placeno.value+ "' or placeno='" + fm.placeno.value+ "'))";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
  	  alert("职场编码不存在，请重新输入！");
  	  ApproveGrid.clearData();
  	  placeno.value="";
      return false;
	}
	var strSQLa = "select 1 from LIPlaceRentinfo where Placeno='" + fm.placeno.value
	+"' and exists(select 1 from LIPlaceChangeTrace where (Placenorefer='" + fm.placeno.value+ "' or placeno='" + fm.placeno.value+ "'))" +
			" and managecom like '"+manageCom+"%'";
	turnPageRelate.strQueryResult  = easyQueryVer3(strSQLa, 1, 1, 1);  
	if (!turnPageRelate.strQueryResult) {
  	  alert("对不起，您无权查看除本机构外的职场编码，请重新输入！");
  	  ApproveGrid.clearData();
  	  placeno.value="";
      return false;
	}
	
	createXMLHttpRequest();
	xmlHttp.open("POST", "PlChangeRelationSaveM.jsp?placeno="+fm.placeno.value);
	xmlHttp.onreadystatechange = processResponse;//指定响应函数
	xmlHttp.send(); 
}

function processResponse() {
	if (xmlHttp.readyState == 4) { // 判断对象状态
    	if (xmlHttp.status == 200) { // 信息已经成功返回，开始处理信息
			showPlaceInfo();
	    } else { //页面不正常
	      	alert("职场编码不存在");
	    }
    }
}

String.prototype.trim = function(){
        return this.replace(/(^\s*)|(\s*$)/g,"");
};

function showPlaceInfo(){
	var allplaceno = "";
	allplaceno = xmlHttp.responseText;
//	var returnRes = xmlHttp.responseXML.getElementsByTagName("res") ;
//	for(var i = 0; i < returnRes.length; i++) {
//	   allplaceno =  returnRes[i].firstChild.data ;
//	}
	allplace = allplaceno.replace(/(^\s*)|(\s*$)/g,"");
	appsql = "select (select case changetype when '01' then '新增' when '02' then '续租' when '03' then '换租' else '其他' end from liplacechangetrace where placeno=a.placeno and changestate='03' and changetype in('01','02','03')) ,placeno ," +
				"a.address," +
				"LF_RENTPERIOD(a.placeno)," +
				"(nvl(a.Sigarea, 0) + nvl(a.Grparea, 0) + nvl(a.Healtharea, 0) +nvl(a.Servicearea, 0) + nvl(a.Manageroffice, 0) + nvl(a.bankarea, 0)+ nvl(a.Departpersno, 0) + nvl(a.Planfinance, 0) +nvl(a.Publicarea, 0) + nvl(a.Jointarea, 0) +nvl(a.Otherarea, 0))," +
				"LF_PLACEPERFEE(a.placeno,'01')," +
				"LF_PLACEPERFEE(a.placeno,'02')," +
				"LF_PLACEPERFEE(a.placeno,'03'), " +
				"(case when current date>a.actualenddate then '停用' when current date<a.Begindate then '未使用' else '在用' END)  " +
				"from LIPlaceRentInfo a where a.placeno in("+allplace+") order by a.begindate ";
	turnPage.queryModal(appsql, ApproveGrid);
}
	
function placeInfo(){
	if(!checkplace()){return false;}
	submitForm();
//	showInfo.close();
}

function checkplace(){
	var placeno=document.getElementById("placeno");
    if(placeno.value==null || placeno.value==""){
      alert("请先输入查询职场编码！");
      return false;
    }
    //var strSQL = "select 1 from LIPlaceRentinfo where Placeno='"+codeno.value+"' and not exists(select 1 from LIPlaceChangeTrace where (placeno='"+codeno.value+"' or Placenorefer='"+codeno.value+"') and Changestate in('01','02'))";			 
    var strSQL = "select 1 from LIPlaceRentinfo where Placeno='" + fm.placeno.value
	+"' and exists(select 1 from LIPlaceChangeTrace where (Placenorefer='" + fm.placeno.value+ "' or placeno='" + fm.placeno.value+ "'))";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
  	  alert("职场编码不存在，请重新输入！");
  	  ApproveGrid.clearData();
  	  placeno.value="";
      return false;
	}
	var strSQLa = "select 1 from LIPlaceRentinfo where Placeno='" + fm.placeno.value
	+"' and exists(select 1 from LIPlaceChangeTrace where (Placenorefer='" + fm.placeno.value+ "' or placeno='" + fm.placeno.value+ "'))" +
			" and managecom like '"+manageCom+"%'";
	turnPageRelate.strQueryResult  = easyQueryVer3(strSQLa, 1, 1, 1);  
	if (!turnPageRelate.strQueryResult) {
  	  alert("对不起，您无权查看除本机构外的职场编码，请重新输入！");
  	  ApproveGrid.clearData();
  	  placeno.value="";
      return false;
	}
	return true;
}

//数据提交
function submitForm()
{
//    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交
}

function afterSubmit( FlagStr, content ,returnplaceno)
{
//	showInfo.close();
//	if (FlagStr == "Fail" )
//	{
//		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
//	}
//	else
//	{
//		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
//	}
//	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	easyQuery(returnplaceno);
}

function easyQuery(allplaceno){
	appsql = "select (select case changetype when '01' then '新增' when '02' then '续租' when '03' then '换租' else '其他' end from liplacechangetrace where placeno=a.placeno and changestate='03' and changetype in('01','02','03')) ,placeno ," +
				"a.address," +
				"LF_RENTPERIOD(a.placeno)," +
				"(nvl(a.Sigarea, 0) + nvl(a.Grparea, 0) + nvl(a.Healtharea, 0) +nvl(a.Servicearea, 0) + nvl(a.Manageroffice, 0) + nvl(a.bankarea, 0)+ nvl(a.Departpersno, 0) + nvl(a.Planfinance, 0) +nvl(a.Publicarea, 0) + nvl(a.Jointarea, 0) +nvl(a.Otherarea, 0))," +
				"LF_PLACEPERFEE(a.placeno,'01')," +
				"LF_PLACEPERFEE(a.placeno,'02')," +
				"LF_PLACEPERFEE(a.placeno,'03'), " +
				"(case when current date>a.actualenddate then '停用' when current date<a.Begindate then '未使用' else '在用' END)  " +
				"from LIPlaceRentInfo a where a.placeno in("+allplaceno+") order by a.begindate ";
	turnPage.queryModal(appsql, ApproveGrid);
}
