<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//name :PlModifyInput.jsp
	//function :
	//Creator :hdl
	//date :2012-6-12
	// 鞠成富 2012-09-10 修改
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "ModifyInputMain.js"></SCRIPT> 
<SCRIPT src="./PlModifyInput.js"></SCRIPT>
<%@include file="./PlModifyInit.jsp"%>
</head>
<body onload="initElementtype();initForm()">
	<form action="PlModifySave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<tr class=common>
				<td class=titleImg >待审核职场列表</td>
			</tr>
			<tr class=common>
				<td text-align: left colSpan=1>
					<span id="spanModifyGrid"></span>
				</td>
			</tr>
		</table>
		<Div id= "divPage" align=center style= "display: 'none' ">
			<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
		<br>
		<table class=common>
			<tr class=common>
				<td colspan="2"><INPUT VALUE="确认" class=cssButton TYPE=button onclick="placeIn()"></td>
			</tr>
		</table>
		<hr>
		<div id="divPlaceIn1" style="display: none">
		    <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divInfoNew);"></td>
					<td class=titleImg>原合同信息</td>
				</tr>
			</table>
			<div id="divInfoNew" style="display: ''">
		    <jsp:include page="ModifyInputMain.jsp" flush="true"/>
		    </div>		
	    	<input type="Hidden" name="HidPlaceEndDate"> 
			<input type="Hidden" name="HidPenaltyFee2"> 
			<input type="Hidden" name="HidReBackFee"> 
			<input type="Hidden" name="HidReBackDate">
			<input type="hidden" name=CurrentDate >	
			<input type=hidden name=shouldRenFee>
			<input type=hidden name=shouldProFee>
			<input type=hidden name=shouldDecFee>
			<input type=hidden name=selectNO>
			<hr>
		</div>
		</form>
	 <div id="divPlaceInFile" style="display: none">
		<form action="PlInFileSave.jsp" method=post name=imp target="fraSubmit" ENCTYPE="multipart/form-data">
       <Table class= common>
  	    <TR class= common>
  	 	    <TD class= title>
			    文件名
			</TD>     
			<TD>
			  <Input type="file" name=impFileName class=common>
			  <INPUT VALUE="上载租赁合同及批文" class=cssButton TYPE=button onclick="fileImp();">
			</TD>
			<TD>
			</TD>
		</TR>
	   </Table>
	<span style="color:red">文件请不要超过4M</span>
     <br></br>
    </form>
    </div>
		<div id="divPlaceIn2" style="display: none" align="right">
			&nbsp;
			<table>
				<tr align=right>
					<td class=input>
						<input type=button class=cssButton value='确认修改' onclick="submitForm()">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table>
		</div>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>