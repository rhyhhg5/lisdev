<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PlApprovalSave.jsp
//程序功能：
//创建日期：2011-06-22 15:12:33
//创建人  ：huodonglei
//更新记录：  更新人    更新日期     更新原因/内容
//        鞠成富    2012-08-29 修改
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.wpleace.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.text.SimpleDateFormat" %>
<%
  //接收信息，并作校验处理。
  //输入参数
	
  	//租赁信息备份类
    LIPlaceRentInfoBSchema mLIPlaceRentInfoBSchema= new LIPlaceRentInfoBSchema();
	LIPlaceRentInfoBDB mLIPlaceRentInfoBDB=new LIPlaceRentInfoBDB();
	LIPlaceRentInfoBSet mLIPlaceRentInfoBSet=new LIPlaceRentInfoBSet();
	
	
	//租期内费用信息类
	LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchema;
	LIPlaceRentFeeBDB mLIPlaceRentFeeBDB= new LIPlaceRentFeeBDB();
	LIPlaceRentFeeBSet mLIPlaceRentFeeBSet=new LIPlaceRentFeeBSet();
	
	//职场变更轨迹类
	LIPlaceChangeTraceSchema mLIPlaceChangeTraceSchema=new LIPlaceChangeTraceSchema();
	LIPlaceChangeTraceDB mLIPlaceChangeTraceDB=new LIPlaceChangeTraceDB();
	LIPlaceChangeTraceSet mLIPlaceChangeTraceSet=new LIPlaceChangeTraceSet();
    
	//续租信息类
	LIPlaceNextInfoSchema mLIPlaceNextInfoSchema= new LIPlaceNextInfoSchema();
	LIPlaceNextInfoDB mLIPlaceNextInfoDB = new LIPlaceNextInfoDB();
	LIPlaceNextInfoSet mLIPlaceNextInfoSet = new LIPlaceNextInfoSet();
	//换租信息类
	LIPlaceChangeInfoSchema mLIPlaceChangeInfoSchema= new LIPlaceChangeInfoSchema();
	LIPlaceChangeInfoDB mLIPlaceChangeInfoDB = new LIPlaceChangeInfoDB();
	LIPlaceChangeInfoSet mLIPlaceChangeInfoSet = new LIPlaceChangeInfoSet();
	//撤租信息类
	LIPlaceEndInfoSchema mLIPlaceEndInfoSchema = new LIPlaceEndInfoSchema();
	LIPlaceEndInfoDB mLIPlaceEndInfoDB= new LIPlaceEndInfoDB();
	LIPlaceEndInfoSet mLIPlaceEndInfoSet = new LIPlaceEndInfoSet();
	
    PlApprovalUI mPlApprovalUI  = new PlApprovalUI();

  //输出参数
  CErrors tError = new CErrors();
  String tOperate="APPROVE";
  String FlagStr = "true";
  String Content = "保存成功";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  
  System.out.println("==开始获取数据==");
  //待审核职场编码
  String tCodeNo = request.getParameter("codeno");

  //审核信息
  String tflag = request.getParameter("flag");                     //审批类型
  String tPlacenorefer= request.getParameter("Placenorefer");      //关联职场编码
  String tCTSerialno=request.getParameter("CTSerialno");           //职场变更轨迹流水号
  String tappType=request.getParameter("appstateType");            //获取审批类型
  String tappstate=request.getParameter("appstate");               //获取是否审批成功
  System.out.println("== tPlacenorefer =="+tPlacenorefer);
  System.out.println("====tflag===="+ tflag);
  System.out.println("=====tCTSerialno====="+tCTSerialno);
  System.out.println("==基本信息获取完毕！=="); 
  
  //获取轨迹变更表信息
  mLIPlaceChangeTraceDB.setSerialno(tCTSerialno);
  mLIPlaceChangeTraceSchema = mLIPlaceChangeTraceDB.query().get(1);
  mLIPlaceChangeTraceSchema.setModifyDate(PubFun.getCurrentDate());   
  mLIPlaceChangeTraceSchema.setModifyTime(PubFun.getCurrentTime());
  
  
 if("2".equals(tappType))   //分公司审批
 {
     if("N".equals(tappstate))  //审批不通过
     {
        String tFremark=request.getParameter("Rremark");
        mLIPlaceChangeTraceSchema.setChangestate("04");
        mLIPlaceChangeTraceSchema.setFremark(tFremark);
     }else{                    //审批通过
        mLIPlaceChangeTraceSchema.setChangestate("02");
     }
     mLIPlaceChangeTraceSchema.setFcheckdate(PubFun.getCurrentDate());
 }
 
 if("3".equals(tappType))   //总公司审核
 {
     if("N".equals(tappstate))    //审批不通过
     {
         String tZremark=request.getParameter("Rremark");
	     mLIPlaceChangeTraceSchema.setChangestate("04");
	     mLIPlaceChangeTraceSchema.setZremark(tZremark);
     }else{                       //审批通过
         mLIPlaceChangeTraceSchema.setChangestate("03"); 
     }
     mLIPlaceChangeTraceSchema.setZcheckdate(PubFun.getCurrentDate());
 }
 
 //审批不通过
 if("4".equals(tappType))
  {
      mLIPlaceChangeTraceSchema.setChangestate("01"); //01 待审批状态
      //新增/续租/换租/职场修改
      if("01".equals(tflag) || "02".equals(tflag) || "03".equals(tflag) || "05".equals(tflag))
      {
          //基本信息
		  String tPlaceno = request.getParameter("Placeno");
		  String tManagecom = request.getParameter("Managecom");
		  String tApprovalType = request.getParameter("ApprovalType");
		  String tApprovalYear = request.getParameter("ApprovalYear");  
		  String tApprovalCode = request.getParameter("ApprovalCode");  
		  String tApprovalSeria = request.getParameter("ApprovalSeria");
		  String tComlevel = request.getParameter("Comlevel");  
		  String tComname = request.getParameter("Comname");  
		  String tBegindate = request.getParameter("Begindate");  
		  String tEnddate = request.getParameter("Enddate");  
		  String tAddress = request.getParameter("Address"); 
		  String tLessor = request.getParameter("Lessor"); 
		  String tHouseno = request.getParameter("Houseno"); 
		  String tProvince = request.getParameter("Province"); 
		  String tCity = request.getParameter("City"); 
		  String tIsrentregister = request.getParameter("Isrentregister"); 
		  String tIslessorassociate = request.getParameter("Islessorassociate"); 
		  String tIslessorright = request.getParameter("Islessorright"); 
		  String tIshouserent = request.getParameter("Ishouserent"); 
		  String tIsplaceoffice = request.getParameter("Isplaceoffice"); 
		  String tIshousegroup = request.getParameter("Ishousegroup"); 
		  String tIshousefarm = request.getParameter("Ishousefarm"); 
		  System.out.println("基本信息获取完毕！");
		  //职场面积
		  String tSigarea = request.getParameter("Sigarea");//个险
		  String tGrparea = request.getParameter("Grparea");//团险
		  String tBankarea = request.getParameter("Bankarea");//银行
		  String tHealtharea = request.getParameter("Healtharea");//健康管理
		  String tServiceArea = request.getParameter("Servicearea");//运营/客服
		  String tJointarea = request.getParameter("Jointarea");//联合办公
		  String tManageroffice = request.getParameter("Manageroffice");//经理室
		  String tDepartpersno = request.getParameter("Departpersno");//人事行政部
		  String tPlanfinance = request.getParameter("Planfinance");//财务部
		  String tPublicarea = request.getParameter("Publicarea");//公共区域
		  String tOtherarea = request.getParameter("Otherarea");//其他面积
		  String tExplanation = request.getParameter("Explanation"); //说明
		  System.out.println("职场面积信息获取完毕！");
		  //合同约定职场费用
		  String tMarginfee = request.getParameter("Marginfee");
		  String tMarginpaydate = request.getParameter("Marginpaydate");
		  String tPenaltyfee = request.getParameter("Penaltyfee");
		  System.out.println("合同约定职场费用信息获取完毕！");
		  //备注
		  String tRemark = request.getParameter("Remark");
		  String tHidFileName = request.getParameter("filename");    //文件名
		  String tHidFilePath = request.getParameter("filepath");    //文件路径
		  
		  //取得租期内租金费用Muline信息
		  int lineCount1 = 0;
		  //String tGridNo1[] = request.getParameterValues("RelateGridNo"); 
		  String tPayDate1[] = request.getParameterValues("RelateGrid1");
		  String tPayAmount1[] = request.getParameterValues("RelateGrid2");
		  String tPayStartDate1[] = request.getParameterValues("RelateGrid3");
		  String tPayEndDate1[] = request.getParameterValues("RelateGrid4");
		  lineCount1 = tPayDate1.length; //行数
		  System.out.println(tPayDate1[0]);
		  System.out.println(tPayAmount1[0]);
		  System.out.println(tPayStartDate1[0]);
		  System.out.println(tPayEndDate1[0]);
		  System.out.println("租金费用length= "+String.valueOf(lineCount1));
		  
		  //取得租期内物业费用Muline信息
		  int lineCount2 = 0;
		  //String tGridNo2[] = request.getParameterValues("PropertyGridNo");
		  String tPayDate2[] = request.getParameterValues("PropertyGrid1");
		  String tPayAmount2[] = request.getParameterValues("PropertyGrid2");
		  String tPayStartDate2[] = request.getParameterValues("PropertyGrid3");
		  String tPayEndDate2[] = request.getParameterValues("PropertyGrid4");
		  if(tPayDate2==null){
		     lineCount2=0;
		  }else{
		     lineCount2 = tPayDate2.length; //行数
		  }
		  System.out.println("物业费length= "+String.valueOf(lineCount2));
		  
		  //取得租期内装修费Muline信息
		  int lineCount3 = 0;
		  //String tGridNo3[] = request.getParameterValues("DecorationGridNo");
		  String tPayDate3[] = request.getParameterValues("DecorationGrid1");
		  String tPayAmount3[] = request.getParameterValues("DecorationGrid2");
		  String tPayStartDate3[] = request.getParameterValues("DecorationGrid3");
		  String tPayEndDate3[] = request.getParameterValues("DecorationGrid4");
		  if(tPayDate3==null){
		     lineCount3=0;
		  }else{
		     lineCount3 = tPayDate3.length; //行数
		  }
		  System.out.println("装修费length= "+String.valueOf(lineCount3));
		  
		  //对从界面取到的信息二次处理
		  String Appno=tApprovalType.trim()+tApprovalYear.trim()+tApprovalCode.trim()+tApprovalSeria.trim();  //对批次号进行二次处理
		  System.out.println(Appno);
		  SimpleDateFormat dDateFormat=new SimpleDateFormat("yyyy-MM-dd");    //获取当前日期 
		  String date=dDateFormat.format(new java.util.Date());
		  System.out.println(date);
		  SimpleDateFormat tDateFormat=new SimpleDateFormat("HH:mm:ss");      //获取当前时间
		  String time=tDateFormat.format(new java.util.Date());
		  System.out.println(time);
		  
		  //往职场租赁合同中添加信息
		  //基本信息
		  mLIPlaceRentInfoBSchema.setPlaceno(tPlaceno);
		  mLIPlaceRentInfoBSchema.setDealtype(tflag);
		  mLIPlaceRentInfoBSchema.setDealstate("01");
		  mLIPlaceRentInfoBSchema.setManagecom(tManagecom);
		  mLIPlaceRentInfoBSchema.setAppno(Appno);
		  mLIPlaceRentInfoBSchema.setState("02");
		  mLIPlaceRentInfoBSchema.setIslock("02");   // 01 非锁定  02 锁定 
		  mLIPlaceRentInfoBSchema.setComlevel(tComlevel);
		  mLIPlaceRentInfoBSchema.setComname(tComname);
		  mLIPlaceRentInfoBSchema.setBegindate(tBegindate);
		  mLIPlaceRentInfoBSchema.setEnddate(tEnddate); 
		  mLIPlaceRentInfoBSchema.setActualenddate(tEnddate);
		  mLIPlaceRentInfoBSchema.setAddress(tAddress);
		  mLIPlaceRentInfoBSchema.setLessor(tLessor);
		  mLIPlaceRentInfoBSchema.setHouseno(tHouseno);
		  mLIPlaceRentInfoBSchema.setProvince(tProvince);
		  mLIPlaceRentInfoBSchema.setCity(tCity);
		  mLIPlaceRentInfoBSchema.setIsrentregister(tIsrentregister);
		  mLIPlaceRentInfoBSchema.setIslessorassociate(tIslessorassociate);
		  mLIPlaceRentInfoBSchema.setIslessorright(tIslessorright);
		  mLIPlaceRentInfoBSchema.setIshouserent(tIshouserent);
		  mLIPlaceRentInfoBSchema.setIsplaceoffice(tIsplaceoffice);
		  mLIPlaceRentInfoBSchema.setIshousegroup(tIshousegroup);
		  mLIPlaceRentInfoBSchema.setIshousefarm(tIshousefarm);
		  
		  //职场面积
		  mLIPlaceRentInfoBSchema.setSigarea(tSigarea);
		  mLIPlaceRentInfoBSchema.setGrparea(tGrparea);
		  mLIPlaceRentInfoBSchema.setBankarea(tBankarea);
		  mLIPlaceRentInfoBSchema.setHealtharea(tHealtharea);
		  mLIPlaceRentInfoBSchema.setServicearea(tServiceArea);
		  mLIPlaceRentInfoBSchema.setJointarea(tJointarea);
		  mLIPlaceRentInfoBSchema.setManageroffice(tManageroffice);
		  mLIPlaceRentInfoBSchema.setDepartpersno(tDepartpersno);
		  mLIPlaceRentInfoBSchema.setPlanfinance(tPlanfinance);
		  mLIPlaceRentInfoBSchema.setPublicarea(tPublicarea);
		  mLIPlaceRentInfoBSchema.setOtherarea(tOtherarea);
		  mLIPlaceRentInfoBSchema.setExplanation(tExplanation);
		  
		  //合同违约金和保证金
		  mLIPlaceRentInfoBSchema.setMarginfee(tMarginfee);
		  mLIPlaceRentInfoBSchema.setMarginpaydate(tMarginpaydate);
		  mLIPlaceRentInfoBSchema.setMarginbackdate("");
		  mLIPlaceRentInfoBSchema.setPenaltyfee(tPenaltyfee);
		  //
		  mLIPlaceRentInfoBSchema.setRemake(tRemark);       //备注
		  mLIPlaceRentInfoBSchema.setFilename(tHidFileName);
		  mLIPlaceRentInfoBSchema.setFilepath(tHidFilePath);
		  mLIPlaceRentInfoBSchema.setStandbystring1(tCTSerialno);
		  mLIPlaceRentInfoBSchema.setOperator(tG.Operator);
		  mLIPlaceRentInfoBSchema.setMakedate(date);
		  mLIPlaceRentInfoBSchema.setMaketime(time);
		  mLIPlaceRentInfoBSchema.setModifydate(date);
		  mLIPlaceRentInfoBSchema.setModifytime(time);
		  mLIPlaceRentInfoBSet.add(mLIPlaceRentInfoBSchema); 
		
		  
		  //往费用表中添加租金信息
		  for(int i=0;i<lineCount1;i++)
		  {
			  mLIPlaceRentFeeBSchema=new LIPlaceRentFeeBSchema();
			  mLIPlaceRentFeeBSchema.setPlaceno(tPlaceno);
			  mLIPlaceRentFeeBSchema.setFeetype("01");
			  mLIPlaceRentFeeBSchema.setOrder(Integer.toString(i+1));
			  mLIPlaceRentFeeBSchema.setPayDate(tPayDate1[i]);
			  mLIPlaceRentFeeBSchema.setMoney(tPayAmount1[i]);
			  mLIPlaceRentFeeBSchema.setPaybegindate(tPayStartDate1[i]+"-01");
		      mLIPlaceRentFeeBSchema.setPayenddate(tPayEndDate1[i]+"-01");
		      mLIPlaceRentFeeBSchema.setDealtype(tflag); 
		      mLIPlaceRentFeeBSchema.setDealstate("01"); //审批状态
		      mLIPlaceRentFeeBSchema.setStandbystring1(tCTSerialno);
		      mLIPlaceRentFeeBSchema.setOperator(tG.Operator);
		      mLIPlaceRentFeeBSchema.setMakedate(date);
		      mLIPlaceRentFeeBSchema.setMaketime(time);
		      mLIPlaceRentFeeBSchema.setModifyDate(date);
		      mLIPlaceRentFeeBSchema.setModifyTime(time);
		      mLIPlaceRentFeeBSet.add(mLIPlaceRentFeeBSchema);   
		  }
		   //往费用表中添加物业费信息
		  for(int i=0;i<lineCount2;i++)
		  {
			  mLIPlaceRentFeeBSchema=new LIPlaceRentFeeBSchema();
			  mLIPlaceRentFeeBSchema.setPlaceno(tPlaceno);
			  mLIPlaceRentFeeBSchema.setFeetype("02");
			  mLIPlaceRentFeeBSchema.setOrder(Integer.toString(i+1));
			  mLIPlaceRentFeeBSchema.setPayDate(tPayDate2[i]);
			  mLIPlaceRentFeeBSchema.setMoney(tPayAmount2[i]);
			  mLIPlaceRentFeeBSchema.setPaybegindate(tPayStartDate2[i]+"-01");
		      mLIPlaceRentFeeBSchema.setPayenddate(tPayEndDate2[i]+"-01");   
		      mLIPlaceRentFeeBSchema.setDealtype(tflag); 
		      mLIPlaceRentFeeBSchema.setDealstate("01"); //审批状态
		      mLIPlaceRentFeeBSchema.setStandbystring1(tCTSerialno);
		      mLIPlaceRentFeeBSchema.setOperator(tG.Operator);
		      mLIPlaceRentFeeBSchema.setMakedate(date);
		      mLIPlaceRentFeeBSchema.setMaketime(time);
		      mLIPlaceRentFeeBSchema.setModifyDate(date);
		      mLIPlaceRentFeeBSchema.setModifyTime(time); 
		      mLIPlaceRentFeeBSet.add(mLIPlaceRentFeeBSchema);  
		  }
		   //往费用表中添加装修费信息
		  for(int i=0;i<lineCount3;i++)
		  {
			  mLIPlaceRentFeeBSchema=new LIPlaceRentFeeBSchema();
			  mLIPlaceRentFeeBSchema.setPlaceno(tPlaceno);
			  mLIPlaceRentFeeBSchema.setFeetype("03");
			  mLIPlaceRentFeeBSchema.setOrder(Integer.toString(i+1));
			  mLIPlaceRentFeeBSchema.setPayDate(tPayDate3[i]);
			  mLIPlaceRentFeeBSchema.setMoney(tPayAmount3[i]);
			  mLIPlaceRentFeeBSchema.setPaybegindate(tPayStartDate3[i]+"-01");
		      mLIPlaceRentFeeBSchema.setPayenddate(tPayEndDate3[i]+"-01");
		      mLIPlaceRentFeeBSchema.setDealtype(tflag); 
		      mLIPlaceRentFeeBSchema.setDealstate("01"); //审批状态 
		      mLIPlaceRentFeeBSchema.setStandbystring1(tCTSerialno);
			  mLIPlaceRentFeeBSchema.setOperator(tG.Operator);
		      mLIPlaceRentFeeBSchema.setMakedate(date);
		      mLIPlaceRentFeeBSchema.setMaketime(time);
		      mLIPlaceRentFeeBSchema.setModifyDate(date);
		      mLIPlaceRentFeeBSchema.setModifyTime(time);
		      mLIPlaceRentFeeBSet.add(mLIPlaceRentFeeBSchema);    
		  }
		  if("03".equals(tflag)){
		      //获取违约金/保证金/应收已付等信息(换租)
			  String tStopDate=request.getParameter("StopDate");
			  String tAWeiYueFee=request.getParameter("AWeiYueFee");
			  String tAWeiYueDate=request.getParameter("AWeiYueDate");
			  String tBaoZhFee=request.getParameter("BaoZhFee");
			  String tBaoZhDate=request.getParameter("BaoZhDate");
			  String tAYingFee=request.getParameter("AYingFee");
			  String tYingDate=request.getParameter("YingDate");
			  
			  mLIPlaceChangeTraceSchema.setEnddate(tStopDate);  
			  mLIPlaceChangeTraceSchema.setShouldgetfee(tAYingFee);
			  mLIPlaceChangeTraceSchema.setShouldgetdate(tYingDate);
			  mLIPlaceChangeTraceSchema.setApenaltyfee(tAWeiYueFee);
			  mLIPlaceChangeTraceSchema.setApenaltyfeegetdate(tAWeiYueDate);
			  mLIPlaceChangeTraceSchema.setMarginfee(tBaoZhFee);
			  mLIPlaceChangeTraceSchema.setMarginfeegetdate(tBaoZhDate);
		  }
		  if("02".equals(tflag)){
			  //获取保证金（续租）
			  String tXBaoZhFee=request.getParameter("XBaoZhFee");
			  String tXBaoZhDate=request.getParameter("XBaoZhDate");
			  
			  mLIPlaceChangeTraceSchema.setMarginfee(tXBaoZhFee);
			  mLIPlaceChangeTraceSchema.setMarginfeegetdate(tXBaoZhDate);
		  }
		  tVData.add(mLIPlaceRentInfoBSet); //租赁信息备份类向后传输
		  tVData.add(mLIPlaceRentFeeBSet);  //租赁费用备份类向后传输
      }
      //撤租
      if("04".equals(tflag))
      {
          	//获取违约金/保证金/应收已付等信息(换租)
			String tStopDate=request.getParameter("StopDate");
			String tAWeiYueFee=request.getParameter("AWeiYueFee");
			String tAWeiYueDate=request.getParameter("AWeiYueDate");
			String tBaoZhFee=request.getParameter("BaoZhFee");
			String tBaoZhDate=request.getParameter("BaoZhDate");
			String tAYingFee=request.getParameter("AYingFee");
			String tYingDate=request.getParameter("YingDate");
			  
			mLIPlaceChangeTraceSchema.setEnddate(tStopDate);  
			mLIPlaceChangeTraceSchema.setShouldgetfee(tAYingFee);
			mLIPlaceChangeTraceSchema.setShouldgetdate(tYingDate);
			mLIPlaceChangeTraceSchema.setApenaltyfee(tAWeiYueFee);
			mLIPlaceChangeTraceSchema.setApenaltyfeegetdate(tAWeiYueDate);
			mLIPlaceChangeTraceSchema.setMarginfee(tBaoZhFee);
			mLIPlaceChangeTraceSchema.setMarginfeegetdate(tBaoZhDate);
      }
      //装修费补录
      if("06".equals(tflag))
      {
          //取得租期内装修费Muline信息
		  int lineCount3 = 0;
		  //String tGridNo3[] = request.getParameterValues("DecorationGridNo");
		  String tPayDate3[] = request.getParameterValues("DecorationGrid1");
		  String tPayAmount3[] = request.getParameterValues("DecorationGrid2");
		  String tPayStartDate3[] = request.getParameterValues("DecorationGrid3");
		  String tPayEndDate3[] = request.getParameterValues("DecorationGrid4");
		  if(tPayDate3==null){
		     lineCount3=0;
		  }else{
		     lineCount3 = tPayDate3.length; //行数
		  }
		  System.out.println("装修费length= "+String.valueOf(lineCount3));
		  
		  //往费用表中添加装修费信息
		  for(int i=0;i<lineCount3;i++)
		  {
			  mLIPlaceRentFeeBSchema=new LIPlaceRentFeeBSchema();
			  mLIPlaceRentFeeBSchema.setPlaceno(mLIPlaceChangeTraceSchema.getPlaceno());
			  mLIPlaceRentFeeBSchema.setFeetype("03");
			  mLIPlaceRentFeeBSchema.setOrder(Integer.toString(i+1));
			  mLIPlaceRentFeeBSchema.setPayDate(tPayDate3[i]);
			  mLIPlaceRentFeeBSchema.setMoney(tPayAmount3[i]);
			  mLIPlaceRentFeeBSchema.setPaybegindate(tPayStartDate3[i]+"-01");
		      mLIPlaceRentFeeBSchema.setPayenddate(tPayEndDate3[i]+"-01");
		      mLIPlaceRentFeeBSchema.setDealtype(tflag); 
		      mLIPlaceRentFeeBSchema.setDealstate("01"); //审批状态 
		      mLIPlaceRentFeeBSchema.setStandbystring1(tCTSerialno);
			  mLIPlaceRentFeeBSchema.setOperator(tG.Operator);
		      mLIPlaceRentFeeBSchema.setMakedate(PubFun.getCurrentDate());
		      mLIPlaceRentFeeBSchema.setMaketime(PubFun.getCurrentTime());
		      mLIPlaceRentFeeBSchema.setModifyDate(PubFun.getCurrentDate());
		      mLIPlaceRentFeeBSchema.setModifyTime(PubFun.getCurrentTime());
		      mLIPlaceRentFeeBSet.add(mLIPlaceRentFeeBSchema);    
		  }
		  tVData.add(mLIPlaceRentFeeBSet);  //租赁费用备份类向后传输
      }
  }
 
  mLIPlaceChangeTraceSet.add(mLIPlaceChangeTraceSchema);
  tVData.add(mLIPlaceChangeTraceSet);  //审批轨迹表向后传输
 
 if("3".equals(tappType) || "2".equals(tappType)){
  //向续租表中插入数据
  if("02".equals(tflag)){
	  mLIPlaceNextInfoSchema.setPreplacno(mLIPlaceChangeTraceSchema.getPlacenorefer());
	  mLIPlaceNextInfoSchema.setPlaceno(mLIPlaceChangeTraceSchema.getPlaceno());
	  mLIPlaceNextInfoSchema.setMarginfee(mLIPlaceChangeTraceSchema.getMarginfee());
	  mLIPlaceNextInfoSchema.setMarginfeegetdate(mLIPlaceChangeTraceSchema.getMarginfeegetdate());
	  mLIPlaceNextInfoSchema.setOperator(mLIPlaceChangeTraceSchema.getOperator());
	  mLIPlaceNextInfoSchema.setMakedate(PubFun.getCurrentDate());
	  mLIPlaceNextInfoSchema.setMaketime(PubFun.getCurrentTime());
	  mLIPlaceNextInfoSchema.setModifydate(PubFun.getCurrentDate());
	  mLIPlaceNextInfoSchema.setModifytime(PubFun.getCurrentTime());
	  mLIPlaceNextInfoSet.add(mLIPlaceNextInfoSchema);
	  tVData.add(mLIPlaceNextInfoSet);
  }
  
  //向换租租表中插入信息
  if("03".equals(tflag)){
	  
     mLIPlaceChangeInfoSchema.setPreplacno(mLIPlaceChangeTraceSchema.getPlacenorefer());
	 mLIPlaceChangeInfoSchema.setPlaceno(mLIPlaceChangeTraceSchema.getPlaceno());
	 mLIPlaceChangeInfoSchema.setEnddate(mLIPlaceChangeTraceSchema.getEnddate());
 	 mLIPlaceChangeInfoSchema.setShouldgetfee(mLIPlaceChangeTraceSchema.getShouldgetfee());//应收已付
 	 if(mLIPlaceChangeTraceSchema.getShouldgetdate()==null){
 		mLIPlaceChangeInfoSchema.setshouldgetdate(mLIPlaceChangeTraceSchema.getEnddate());//应收已付日期
 	 }else{
 		mLIPlaceChangeInfoSchema.setshouldgetdate(mLIPlaceChangeTraceSchema.getShouldgetdate());//应收已付日期 
 	 }
 	 mLIPlaceChangeInfoSchema.setAPenaltyfee(mLIPlaceChangeTraceSchema.getApenaltyfee());//违约金
 	 if(mLIPlaceChangeTraceSchema.getApenaltyfeegetdate()==null){
 		mLIPlaceChangeInfoSchema.setAPenaltyfeegetdate(mLIPlaceChangeTraceSchema.getEnddate());//违约金支付日期
 	 }else{
 		mLIPlaceChangeInfoSchema.setAPenaltyfeegetdate(mLIPlaceChangeTraceSchema.getApenaltyfeegetdate());//违约金支付日期 
 	 }
 	 mLIPlaceChangeInfoSchema.setMarginfee(mLIPlaceChangeTraceSchema.getMarginfee());// 保证金
 	 if(mLIPlaceChangeTraceSchema.getMarginfeegetdate()==null){
 		mLIPlaceChangeInfoSchema.setMarginfeegetdate(mLIPlaceChangeTraceSchema.getEnddate());//保证金收回日期
 	 }else{
 		mLIPlaceChangeInfoSchema.setMarginfeegetdate(mLIPlaceChangeTraceSchema.getMarginfeegetdate());//保证金收回日期
 	 }
 	 mLIPlaceChangeInfoSchema.setOperator(mLIPlaceChangeTraceSchema.getOperator());//操作员
 	 mLIPlaceChangeInfoSchema.setMakedate(PubFun.getCurrentDate());
 	 mLIPlaceChangeInfoSchema.setMaketime(PubFun.getCurrentTime());
 	 mLIPlaceChangeInfoSchema.setModifydate(PubFun.getCurrentDate());
 	 mLIPlaceChangeInfoSchema.setModifytime(PubFun.getCurrentTime());
	 mLIPlaceChangeInfoSet.add(mLIPlaceChangeInfoSchema);
	 tVData.add(mLIPlaceChangeInfoSet);
  }
  //向撤租信息表中添加信息
  if("04".equals(tflag)){
	 mLIPlaceEndInfoSchema.setPlaceno(mLIPlaceChangeTraceSchema.getPlaceno());
	 mLIPlaceEndInfoSchema.setEnddate(mLIPlaceChangeTraceSchema.getEnddate());
	 mLIPlaceEndInfoSchema.setShouldgetfee(mLIPlaceChangeTraceSchema.getShouldgetfee());
	 if(mLIPlaceChangeTraceSchema.getShouldgetdate()==null){
		 mLIPlaceEndInfoSchema.setShouldgetdate(mLIPlaceChangeTraceSchema.getEnddate());
	 }else{
		 mLIPlaceEndInfoSchema.setShouldgetdate(mLIPlaceChangeTraceSchema.getShouldgetdate());
	 }
	 mLIPlaceEndInfoSchema.setApenaltyfee(mLIPlaceChangeTraceSchema.getApenaltyfee());
	 if(mLIPlaceChangeTraceSchema.getMarginfeegetdate()==null){
		 mLIPlaceEndInfoSchema.setApenaltyfeegetdate(mLIPlaceChangeTraceSchema.getEnddate());
	 }else{
		 mLIPlaceEndInfoSchema.setApenaltyfeegetdate(mLIPlaceChangeTraceSchema.getMarginfeegetdate());
	 }
	 mLIPlaceEndInfoSchema.setMarginfee(mLIPlaceChangeTraceSchema.getMarginfee());
	 if(mLIPlaceChangeTraceSchema.getMarginfeegetdate()==null){
		 mLIPlaceEndInfoSchema.setMarginfeegetdate(mLIPlaceChangeTraceSchema.getEnddate());
	 }else{
		 mLIPlaceEndInfoSchema.setMarginfeegetdate(mLIPlaceChangeTraceSchema.getMarginfeegetdate());
	 }
	 mLIPlaceEndInfoSchema.setOperator(mLIPlaceChangeTraceSchema.getOperator());
	 mLIPlaceEndInfoSchema.setMakedate(PubFun.getCurrentDate());
	 mLIPlaceEndInfoSchema.setMaketime(PubFun.getCurrentTime());
	 mLIPlaceEndInfoSchema.setModifydate(PubFun.getCurrentDate());
	 mLIPlaceEndInfoSchema.setModifytime(PubFun.getCurrentTime());
	 mLIPlaceEndInfoSet.add(mLIPlaceEndInfoSchema);
	 tVData.add(mLIPlaceEndInfoSet);
  }
 }

  tVData.add(tCodeNo);
  System.out.println("vdate======="+tVData.size());
  System.out.println("add over");
  try
  {
	  if(!mPlApprovalUI.submitData(tVData,tOperate,tappType)){
	       if (mPlApprovalUI.mErrors.needDealError())
              {
                  tError.copyAllErrors(mPlApprovalUI.mErrors);
                  FlagStr = "Fail";
                  Content = "保存失败"+tError.getFirstError();
              }
              else{
                  FlagStr = "Fail";
                  Content = "PlApprovalSave保存失败,但没有获取详细信息！";
              }
	  }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //添加各种预处理

  %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>