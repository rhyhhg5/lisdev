<%
//Creator :张斌
//Date :2007-2-7
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 GlobalInput tG = new GlobalInput();
 tG=(GlobalInput)session.getValue("GI");
 String CurrentDate	= PubFun.getCurrentDate();   
 String CurrentTime	= PubFun.getCurrentTime();
 String Operator   	= tG.Operator;
 String tContType  	= request.getParameter("ContType");
%>

<script language="JavaScript">
function initForm()
{
  try
  {
    initBusynessGrid();
  }
  catch(re)
  {
    alert("3在LRGetBsnsDataInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 新单续期列表的初始化
function initBusynessGrid()
{                               
	var iArray = new Array();   
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=40;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许   			     
		
		iArray[1]=new Array();
		iArray[1][0]="编码";
		iArray[1][1]="40px";  
		iArray[1][2]=100;     
		iArray[1][3]=0;
		
		iArray[2]=new Array();
		iArray[2][0]="租赁起期";          		 
		iArray[2][1]="60px";            		 
		iArray[2][2]=85;            			   
		iArray[2][3]=0;  
		
		iArray[3]=new Array();
		iArray[3][0]="租赁止期";          		 
		iArray[3][1]="60px";            		 
		iArray[3][2]=85;            			   
		iArray[3][3]=0;  
		
		iArray[4]=new Array();
		iArray[4][0]="停用时间";        		   
		iArray[4][1]="60px";            		 
		iArray[4][2]=85;            			   
		iArray[4][3]=0; 
		
		iArray[5]=new Array();
		iArray[5][0]="租期(月)";        		   
		iArray[5][1]="60px";            		 
		iArray[5][2]=85;            			   
		iArray[5][3]=0; 
		
		iArray[6]=new Array();
		iArray[6][0]="机构名称";        		   
		iArray[6][1]="60px";            		 
		iArray[6][2]=85;            			   
		iArray[6][3]=0; 
		
		iArray[7]=new Array();
		iArray[7][0]="地址";
		iArray[7][1]="40px";            		 
		iArray[7][2]=85;            			   
		iArray[7][3]=0;
		
		iArray[8]=new Array();
		iArray[8][0]="总面积(平方米)";
		iArray[8][1]="100px";            		 
		iArray[8][2]=85;            			   
		iArray[8][3]=0;              			   
		
		iArray[9]=new Array();
		iArray[9][0]="个险(平方米)";         		   
		iArray[9][1]="80px";            		 
		iArray[9][2]=100;            			   
		iArray[9][3]=0;  
		
		iArray[10]=new Array();
		iArray[10][0]="团险(平方米)";         		   
		iArray[10][1]="80px";            		 
		iArray[10][2]=100;            			   
		iArray[10][3]=0;  
		
		iArray[11]=new Array();
		iArray[11][0]="银保(平方米)";         		   
		iArray[11][1]="80px";            		 
		iArray[11][2]=100;            			   
		iArray[11][3]=0;      
		
		iArray[12]=new Array();
		iArray[12][0]="联合办公(平方米)";         		   
		iArray[12][1]="100px";            		 
		iArray[12][2]=100;            			   
		iArray[12][3]=0; 
		
		iArray[13]=new Array();
		iArray[13][0]="后援(平方米)";  
		iArray[13][1]="80px";  
		iArray[13][2]=100;     
		iArray[13][3]=0;      
		
		iArray[14]=new Array();
		iArray[14][0]="其他(平方米)";  
		iArray[14][1]="80px";  
		iArray[14][2]=100;     
		iArray[14][3]=0;       
		
		iArray[15]=new Array();
		iArray[15][0]="租金总额(元)";  
		iArray[15][1]="80px";  
		iArray[15][2]=100;     
		iArray[15][3]=0; 
		
		iArray[16]=new Array();
		iArray[16][0]="物业费总额(元)";  
		iArray[16][1]="100px";  
		iArray[16][2]=100;     
		iArray[16][3]=0; 
		
		iArray[17]=new Array();
		iArray[17][0]="装修费用总额(元)";  
		iArray[17][1]="100px";  
		iArray[17][2]=100;     
		iArray[17][3]=0; 
		
		iArray[18]=new Array();
		iArray[18][0]="违约金(元)";  
		iArray[18][1]="80px";  
		iArray[18][2]=100;     
		iArray[18][3]=0;		
		
		iArray[19]=new Array();
		iArray[19][0]="公司级别";  
		iArray[19][1]="80px";  
		iArray[19][2]=100;     
		iArray[19][3]=0;
		
		iArray[20]=new Array();
		iArray[20][0]="距到期日时间";  
		iArray[20][1]="100px";  
		iArray[20][2]=100;     
		iArray[20][3]=0;
		
		iArray[21]=new Array();
		iArray[21][0]="状态";  
		iArray[21][1]="40px";  
		iArray[21][2]=100;     
		iArray[21][3]=0;						
	
		BusynessGrid = new MulLineEnter("fm","BusynessGrid"); 
		//这些属性必须在loadMulLine前
		BusynessGrid.mulLineCount = 0;   
		BusynessGrid.displayTitle = 1;
		BusynessGrid.locked = 0;
		BusynessGrid.canSel =0; // 1 显示 ；0 隐藏（缺省值）
		//BusynessGrid.selBoxEventFuncName ="CardInfoGridClick"; //响应的函数名，不加扩号   
		BusynessGrid.hiddenPlus=1;
		BusynessGrid.hiddenSubtraction=1;
		BusynessGrid.loadMulLine(iArray); 
	}
	catch(ex)
	{
	    alert(ex);
	}
}
</script>