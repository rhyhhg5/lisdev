<%
//Creator :张斌
//Date :2007-2-7
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 GlobalInput tG = new GlobalInput();
 tG=(GlobalInput)session.getValue("GI");
 String CurrentDate	= PubFun.getCurrentDate();   
 String CurrentTime	= PubFun.getCurrentTime();
 String Operator   	= tG.Operator;
 String tContType  	= request.getParameter("ContType");
%>

<script language="JavaScript">
function initForm()
{
  try
  {
    initPlaceGrid();
    initNewPlaceGrid();
    initRemovePlaceGrid();
    initChangePlaceGrid();
  }
  catch(re)
  {
    alert("3在LRGetBsnsDataInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 职场呈现表的初始化
function initPlaceGrid()
{                               
	var iArray = new Array();   
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=40;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许   			     
		
		iArray[1]=new Array();
		iArray[1][0]="编码";
		iArray[1][1]="40px";  
		iArray[1][2]=100;     
		iArray[1][3]=0;
		
		iArray[2]=new Array();
		iArray[2][0]="机构名称";          		 
		iArray[2][1]="60px";            		 
		iArray[2][2]=85;            			   
		iArray[2][3]=0;  
		
		iArray[3]=new Array();
		iArray[3][0]="租金";          		 
		iArray[3][1]="40px";            		 
		iArray[3][2]=85;            			   
		iArray[3][3]=0;  
		
		iArray[4]=new Array();
		iArray[4][0]="物业费";        		   
		iArray[4][1]="60px";            		 
		iArray[4][2]=85;            			   
		iArray[4][3]=0; 
		
		iArray[5]=new Array();
		iArray[5][0]="装修费用(当前)";        		   
		iArray[5][1]="80px";            		 
		iArray[5][2]=85;            			   
		iArray[5][3]=0; 
		
		iArray[6]=new Array();
		iArray[6][0]="违约金";        		   
		iArray[6][1]="60px";            		 
		iArray[6][2]=85;            			   
		iArray[6][3]=0; 
		
		iArray[7]=new Array();
		iArray[7][0]="装修未摊销费用";
		iArray[7][1]="80px";            		 
		iArray[7][2]=85;            			   
		iArray[7][3]=0;			
	
		PlaceGrid = new MulLineEnter("fm","PlaceGrid"); 
		//这些属性必须在loadMulLine前
		PlaceGrid.mulLineCount = 1;   
		PlaceGrid.displayTitle = 1;
		PlaceGrid.locked = 0;
		PlaceGrid.canSel =0; // 1 显示 ；0 隐藏（缺省值）
		//BusynessGrid.selBoxEventFuncName ="CardInfoGridClick"; //响应的函数名，不加扩号   
		PlaceGrid.hiddenPlus=1;
		PlaceGrid.hiddenSubtraction=1;
		PlaceGrid.loadMulLine(iArray); 
	}
	catch(ex)
	{
	    alert(ex);
	}
}
// 新设职场呈现表的初始化
function initNewPlaceGrid()
{                               
	var iArray = new Array();   
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=40;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许   			     
		
		iArray[1]=new Array();
		iArray[1][0]="编码";
		iArray[1][1]="40px";  
		iArray[1][2]=100;     
		iArray[1][3]=0;
		
		iArray[2]=new Array();
		iArray[2][0]="机构名称";          		 
		iArray[2][1]="60px";            		 
		iArray[2][2]=85;            			   
		iArray[2][3]=0;  
		
		iArray[3]=new Array();
		iArray[3][0]="职场面积";          		 
		iArray[3][1]="60px";            		 
		iArray[3][2]=85;            			   
		iArray[3][3]=0;  
		
		iArray[4]=new Array();
		iArray[4][0]="租金及物业费";        		   
		iArray[4][1]="80px";            		 
		iArray[4][2]=85;            			   
		iArray[4][3]=0; 
		
		iArray[5]=new Array();
		iArray[5][0]="装修费用";        		   
		iArray[5][1]="60px";            		 
		iArray[5][2]=85;            			   
		iArray[5][3]=0; 
	
		NewPlaceGrid = new MulLineEnter("fm","NewPlaceGrid"); 
		//这些属性必须在loadMulLine前
		NewPlaceGrid.mulLineCount = 1;   
		NewPlaceGrid.displayTitle = 1;
		NewPlaceGrid.locked = 0;
		NewPlaceGrid.canSel =0; // 1 显示 ；0 隐藏（缺省值）
		//BusynessGrid.selBoxEventFuncName ="CardInfoGridClick"; //响应的函数名，不加扩号   
		NewPlaceGrid.hiddenPlus=1;
		NewPlaceGrid.hiddenSubtraction=1;
		NewPlaceGrid.loadMulLine(iArray); 
	}
	catch(ex)
	{
	    alert(ex);
	}
}
// 撤销职场详单初始化
function initRemovePlaceGrid()
{                               
	var iArray = new Array();   
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=40;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许   			     
		
		iArray[1]=new Array();
		iArray[1][0]="编码";
		iArray[1][1]="40px";  
		iArray[1][2]=100;     
		iArray[1][3]=0;
		
		iArray[2]=new Array();
		iArray[2][0]="机构名称";          		 
		iArray[2][1]="60px";            		 
		iArray[2][2]=85;            			   
		iArray[2][3]=0;  
		
		iArray[3]=new Array();
		iArray[3][0]="职场面积";          		 
		iArray[3][1]="60px";            		 
		iArray[3][2]=85;            			   
		iArray[3][3]=0;  
		
		iArray[4]=new Array();
		iArray[4][0]="年租金及物业费(原)";        		   
		iArray[4][1]="80px";            		 
		iArray[4][2]=85;            			   
		iArray[4][3]=0; 
		
		iArray[5]=new Array();
		iArray[5][0]="装修成本(总)";        		   
		iArray[5][1]="80px";            		 
		iArray[5][2]=85;            			   
		iArray[5][3]=0; 
		
		iArray[6]=new Array();
		iArray[6][0]="违约金";        		   
		iArray[6][1]="60px";            		 
		iArray[6][2]=85;            			   
		iArray[6][3]=0; 		
	
		RemovePlaceGrid = new MulLineEnter("fm","RemovePlaceGrid"); 
		//这些属性必须在loadMulLine前
		RemovePlaceGrid.mulLineCount = 1;   
		RemovePlaceGrid.displayTitle = 1;
		RemovePlaceGrid.locked = 0;
		RemovePlaceGrid.canSel =0; // 1 显示 ；0 隐藏（缺省值）
		//BusynessGrid.selBoxEventFuncName ="CardInfoGridClick"; //响应的函数名，不加扩号   
		RemovePlaceGrid.hiddenPlus=1;
		RemovePlaceGrid.hiddenSubtraction=1;
		RemovePlaceGrid.loadMulLine(iArray); 
	}
	catch(ex)
	{
	    alert(ex);
	}
}
// 变更职场详单初始化
function initChangePlaceGrid()
{                               
	var iArray = new Array();   
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=40;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许   			     
		
		iArray[1]=new Array();
		iArray[1][0]="编码";
		iArray[1][1]="40px";  
		iArray[1][2]=100;     
		iArray[1][3]=0;
		
		iArray[2]=new Array();
		iArray[2][0]="机构名称";          		 
		iArray[2][1]="60px";            		 
		iArray[2][2]=85;            			   
		iArray[2][3]=0;  
		
		iArray[3]=new Array();
		iArray[3][0]="新职场面积";          		 
		iArray[3][1]="60px";            		 
		iArray[3][2]=85;            			   
		iArray[3][3]=0;  
		
		iArray[4]=new Array();
		iArray[4][0]="原职场面积";        		   
		iArray[4][1]="60px";            		 
		iArray[4][2]=85;            			   
		iArray[4][3]=0; 
		
		iArray[5]=new Array();
		iArray[5][0]="年租金及物业费用(新职场)";        		   
		iArray[5][1]="100px";            		 
		iArray[5][2]=85;            			   
		iArray[5][3]=0; 
		
		iArray[6]=new Array();
		iArray[6][0]="年租金及物业费用(原职场)";        		   
		iArray[6][1]="100px";            		 
		iArray[6][2]=85;            			   
		iArray[6][3]=0; 
		
		iArray[7]=new Array();
		iArray[7][0]="装修成本总(新职场)";
		iArray[7][1]="80px";            		 
		iArray[7][2]=85;            			   
		iArray[7][3]=0;	
		
		iArray[7]=new Array();
		iArray[7][0]="装修成本总(原职场)";
		iArray[7][1]="80px";            		 
		iArray[7][2]=85;            			   
		iArray[7][3]=0;	
		
		iArray[8]=new Array();
		iArray[8][0]="违约金";
		iArray[8][1]="60px";            		 
		iArray[8][2]=85;            			   
		iArray[8][3]=0;			
	
		ChangePlaceGrid = new MulLineEnter("fm","ChangePlaceGrid"); 
		//这些属性必须在loadMulLine前
		ChangePlaceGrid.mulLineCount = 1;   
		ChangePlaceGrid.displayTitle = 1;
		ChangePlaceGrid.locked = 0;
		ChangePlaceGrid.canSel =0; // 1 显示 ；0 隐藏（缺省值）
		//BusynessGrid.selBoxEventFuncName ="CardInfoGridClick"; //响应的函数名，不加扩号   
		ChangePlaceGrid.hiddenPlus=1;
		ChangePlaceGrid.hiddenSubtraction=1;
		ChangePlaceGrid.loadMulLine(iArray); 
	}
	catch(ex)
	{
	    alert(ex);
	}
}
</script>