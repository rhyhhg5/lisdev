
<%
	//name :PlaceInInit.jsp
	//function : 
	//Creator :huodonglei
	//date :2011-06-09
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="ModifyInputMainInit.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String Comcode = tG.ManageCom;
	String CurrentDate = PubFun.getCurrentDate();
	String tCurrentYear = StrTool.getVisaYear(CurrentDate);
	String tCurrentMonth = StrTool.getVisaMonth(CurrentDate);
	String tCurrentDate = StrTool.getVisaDay(CurrentDate);
%>
<script language="JavaScript">
function initForm() 
{
	try 
	{
		fm.CurrentDate.value='<%=CurrentDate%>';
        initRelateGrid();
		initPropertyGrid();
		initDecorationGrid();
		
		divPlaceIn1.style.display='none';
		divPlaceIn2.style.display='none';
		fm.codeno1.value = '';
	} 
	catch (re) 
	{
		alert("PlCancleInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

</script>