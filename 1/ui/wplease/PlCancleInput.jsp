<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//name :PlCancleInput.jsp
	//function :
	//Creator :huodonglei
	//date :2011-6-8
	// 鞠成富 2012-09-07 修改
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="./PlCancleInput.js"></SCRIPT>
<SCRIPT src="./PlShouBackFee.js"></SCRIPT>
<%@include file="./PlCancleInit.jsp"%>
</head>
<body onload="initElementtype();initForm()">
	<form action="PlCancleSave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<tr class=common>
				<td class=title>拟撤租编码</td>
				<td><Input class=common name="codeno1" id="codeno" verify="撤租职场编码|NOTNULL" elementtype=nacessary></td>
			</tr>
			<tr class=common>
				<td colspan="2"><INPUT VALUE="确认" class=cssButton TYPE=button
					onclick="placeIn()"></td>
			</tr>
		</table>
		<div id="divPlaceIn2" style="display: none">
		    <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divCancelInfo);"></td>
					<td class=titleImg>撤租信息</td>
				</tr>
			</table>
			<div id="divCancelInfo" style="">
			<table class=common>
			  <tr class=common>
			      <td class=title>停用时间</td>
			      <TD class=input><Input name=StopDate class='coolDatePicker' elementtype=nacessary dateFormat='short' verify="停用时间|NOTNULL"></TD>
			      <td colspan="2"><INPUT VALUE="计算应收已付费用" class=cssButton TYPE=button onclick="getShoudFee()"></td>
			    </tr>
			    <tr class=common>
			      <TD class=title>合同违约金</TD>
				  <TD class=input><Input class=common name=SWeiYueFee id="SWeiYueFee" elementtype=nacessary verify="合同违约金|NOTNULL&NUM" readonly></TD>
				  <Td class=title>实际违约金</Td>
				  <Td class=input><input class=common name=AWeiYueFee id="AWeiYueFee"  elementtype=nacessary verify="实际违约金|NOTNULL&NUM"></Td>
				  <Td class=title>违约金支付时间</Td>
				  <TD class=input><Input name=AWeiYueDate class='coolDatePicker' elementtype=nacessary dateFormat='short' verify="违约金支付时间|NOTNULL"></TD>
				</tr>
				<tr class=common>
				  <Td class=title>合同保证金</Td>
				  <Td class=input><input class=common name=BaoZhFee id="BaoZhFee"  elementtype=nacessary verify="合同保证金|NOTNULL&NUM" readonly></Td>
				  <Td class=title>保证金收回时间</Td>
				  <TD class=input><Input name=BaoZhDate class='coolDatePicker' dateFormat='short' elementtype=nacessary verify="保证金收回时间|NOTNULL"></TD>
				  <td class=title></td>
				  <td class=input></td>
			    </tr>
			    <tr class=common>
				    <td class=title>系统计算应收已付租赁费用</td>
				    <Td class=input><input class=common name=SYingFee id="SYingFee"  elementtype=nacessary verify="系统计算应收已付租赁费用|NOTNULL&NUM" readonly></Td>
				    <td class=title>实际应收已付款租赁费用</td>
				    <Td class=input><input class=common name=AYingFee id="AYingFee"  elementtype=nacessary verify="实际应收已付款租赁费用|NOTNULL&NUM"></Td>
				    <Td class=title>应收已付收回时间</Td>
					<TD class=input><Input name=YingDate class='coolDatePicker' dateFormat='short' elementtype=nacessary verify="应收已付收回时间|NOTNULL"></TD>
			    <tr>
			</Table>
			</div>
		</div>
		<hr>
		<div id="divPlaceIn1" style="display: none">
		  <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divInfoPre);"></td>
					<td class=titleImg>原合同信息</td>
				</tr>
			</table>
			<div id="divInfoPre" style="">
          <jsp:include page="InputMain.jsp" flush="true"/>
            </div>
			<br>
			<input type=button value='批文下载' onclick='downloadmode();' class=cssButton>&nbsp;&nbsp;&nbsp;<input type=button class=cssButton value='确   认' onclick='cancleConf();'>
		</div>
		<input type=hidden name=codeno>
		<input type="Hidden" name="filename"> 
		<input type="Hidden" name="filepath"> 
		<input type="hidden" name=CurrentDate >
		<input type=hidden name=shouldRenFee>
		<input type=hidden name=shouldProFee>
		<input type=hidden name=shouldDecFee>
		
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</form>
</body>
</html>