<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//name :PlaceInInput.jsp
	//function :
	//Creator :huodonglei
	//date :2011-6-8
%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="PlNextInput.js"></SCRIPT>
<SCRIPT src="InputMain.js"></SCRIPT>
<SCRIPT src = "PlUpdateFee.js"></SCRIPT> 
<%@include file="PlNextInit.jsp"%>
</head>
<body onload="initElementtype();initForm()">
	<form action="PlNextSave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<tr class=common>
				<td class=title>原编码</td>
				<td><Input class=common name="codeno" id="codeno"
					verify="续租职场编码|NOTNULL" elementtype=nacessary></td>
			</tr>
			<tr class=common>
				<td colspan="2"><INPUT VALUE="确认" class=cssButton TYPE=button
					onclick="placeIn()"></td>
			</tr>
		</table>
		<div id="divSourceInfo" style="display: none">
		    <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divInfo);"></td>
					<td class=titleImg>原合同信息</td>
				</tr>
			</table>
			<div id="divInfo" style="display: ''">
			  <table class=common>
			    <tr class=common>
			      <TD class=title>公司名称</TD>
				  <TD class=input><Input class=common name=SComName id="SComName" readonly></TD>
				  <Td class=title>总面积</Td>
				  <Td class=input><input class=common name=SumArea id="SumArea"  readonly></Td>
				  <Td class=title>租期起期</Td>
				  <TD class=input><Input name=SStartDate class=common  readonly></TD>
				</tr>
				<tr class=common>
				  <Td class=title>租期止期</Td>
				  <TD class=input><Input name=SEndDate class=common  readonly></TD>
				  <Td class=title>地址</Td>
				  <Td class=input colspan="3"><input class=common style="width=98%" name=SAddress id="SAddress" readonly></Td>
                </tr>
                <tr class=common>
                  <Td class= title>保证金金额</Td>
                  <Td class= input><Input class= common name="SMarginfee" id="SMarginfee" readonly> </Td>
				  <Td class= title>保证金收回时间</Td>
                  <Td class= input><Input name=SMarginfeeDate class='coolDatePicker' dateFormat='short' elementtype=nacessary verify=" 保证金收回时间|notnull&Date"> </Td>
                </tr>
			  </table>
			</div>
		</div>
		<hr>
		<div id="divPlaceIn1" style="display: none">
		   <table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divInfopre);"></td>
					<td class=titleImg>新合同信息</td>
				</tr>
			</table>
			<div id="divInfopre" style="display: ''">
		   <jsp:include page="InputMain.jsp" flush="true"/>
		   </div>
		</div>
	</form>
	<div id="divPlaceIn2" style="display: none">
	<form action="PlInFeeSave.jsp" method=post name=feeimp target="fraSubmit" ENCTYPE="multipart/form-data">
    <jsp:include page="PlUpdateFee.jsp" flush="true"/>
  </form>
		<form action="PlInFileSave.jsp" method=post name=imp
			target="fraSubmit" ENCTYPE="multipart/form-data">
			<Table class=common>
				<TR class=common>
					<TD class=title>文件名</TD>
					<TD><Input type="file" name=FileName class=common> <INPUT
						VALUE="上载租赁合同及批文" class=cssButton TYPE=button onclick="ApprovalImp();">
					</TD>
					<TD><INPUT VALUE="保存" class=cssButton TYPE=button
						onclick="submitForm()"></TD>
				</TR>
			</Table>
		</form>
	</div>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>