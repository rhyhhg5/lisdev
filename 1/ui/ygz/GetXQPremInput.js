var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;

/* 查询 */
function EasyQueryClick(){
	
	if(!verifyInput2()){
		return false;
	}
	
	var policyType = fm.PolicyType.value;

	var strSQL = "";
	if(""==policyType){
		alert("请选择保单类型");
		return false;
	}
	if(""==fm.PolicyType.value && ""==fm.getnoticeno.value){
		var startDate = fm.StartDate.value;
		if(""==startDate){
			alert("请选择续期核销日期起期");
			return false;
		}
		var endDate = fm.EndDate.value;
		if(""==endDate){
			alert("请选择续期核销日期止期");
			return false;
		}
		//续期核销时间起止期三个月校验
		var t1 = new Date(startDate.replace(/-/g,"\/")).getTime();
		var t2 = new Date(endDate.replace(/-/g,"\/")).getTime();
		var tMinus = (t2-t1)/(24*60*60*1000);  
		if(tMinus>90 )
		{
			alert("续期核销时间起期和止期之间不能超过三个月!");
			return false;
		}
	}
		if(policyType=="1"){
		
		strSQL = "select a.incomeno,a.getnoticeno,a.sumactupaymoney,a.confdate,"
   	    +" case when  (select 1 from lyoutpaydetail where voucherid=a.incomeno and moneyno=a.payno fetch first 1 rows only ) is not null "
		+" then  (select case when state='01' then '提取未推送' when  state='02' then '推送成功' else  '推送失败' end from lyoutpaydetail "
		+" where voucherid=a.incomeno and moneyno=a.payno fetch first 1 rows only) "
		+" when (select 1 from LYPremSeparateDetail where policyno=a.incomeno and tempfeeno=a.getnoticeno fetch first 1 rows only) is not null then '价税分离' "
		+" else  '未价税分离' end, "
		+" nvl((select errorinfo from lyoutpaydetail where voucherid=a.incomeno and moneyno=a.payno fetch first 1 rows only),"
		+" (select errorinfo from LYPremSeparateDetail where policyno=a.incomeno and tempfeeno=a.getnoticeno fetch first 1 rows only)) "
    	+" from ljapay a,ljspayb b "
	    +" where a.incomeno=b.otherno "
	    +" and a.getnoticeno=b.getnoticeno " 
	    +" and a.sumactupaymoney>='0' " 
	    +" and exists (select 1 from lccont where contno=a.incomeno and conttype='1') "
	    +" and a.ManageCom like '"+fm.ManageCom.value+"%' "
		+ getWherePart('a.ConfDate','StartDate',">=")
		+ getWherePart('a.ConfDate','EndDate',"<=")
	    + getWherePart( 'a.incomeno ','incomeno') 
	    + getWherePart( 'a.getnoticeno ','getnoticeno') 	
	    +"	with ur";
	}else {
		strSQL = "select a.incomeno,a.getnoticeno,a.sumactupaymoney,a.confdate,"
	   	    +" case when  (select 1 from lyoutpaydetail where voucherid=a.incomeno and moneyno=a.payno fetch first 1 rows only ) is not null "
			+" then  (select case when state='01' then '提取未推送' when  state='02' then '推送成功' else  '推送失败' end from lyoutpaydetail "
			+" where voucherid=a.incomeno and moneyno=a.payno fetch first 1 rows only) "
			+" when (select 1 from LYPremSeparateDetail where policyno=a.incomeno and tempfeeno=a.getnoticeno fetch first 1 rows only) is not null then '价税分离' "
			+" else  '未价税分离' end, "
			+" nvl((select errorinfo from lyoutpaydetail where voucherid=a.incomeno and moneyno=a.payno fetch first 1 rows only),"
			+" (select errorinfo from LYPremSeparateDetail where policyno=a.incomeno and tempfeeno=a.getnoticeno fetch first 1 rows only)) "
	    	+" from ljapay a,ljspayb b "
		    +" where a.incomeno=b.otherno "
		    +" and a.getnoticeno=b.getnoticeno " 
		    +" and a.sumactupaymoney>='0' " 
		    +" and exists (select 1 from lcgrpcont where grpcontno=a.incomeno) "
		    +" and a.ManageCom like '"+fm.ManageCom.value+"%' "
			+ getWherePart('a.ConfDate','StartDate',">=")
			+ getWherePart('a.ConfDate','EndDate',"<=")
		    + getWherePart( 'a.incomeno ','incomeno') 
		    + getWherePart( 'a.getnoticeno ','getnoticeno') 	
		    +"	with ur ";
		
	}
	
	turnPage.queryModal(strSQL,ContGrid);
	
	if(!turnPage.strQueryResult){
		alert("没有查询到相关数据！");
		return false;
	}
}

/* 调用接口 */
function pushInterface(){
	
	fm.pushReceipt.disabled = true;
	
	var checkedRowNum = 0;
    var rowNum = ContGrid.mulLineCount;

    for (var i = 0; i < rowNum; i++){
        if(checkedRowNum > 1){
            break;
        }

        if(ContGrid.getChkNo(i)){
            checkedRowNum += 1;
        }
    }

    if(checkedRowNum < 1){
        alert("请至少选择一条记录!");
        return false;
    }
	
    var PolicyType = fm.PolicyType.value
    
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GetXQPremSave.jsp?PolicyType="+PolicyType;
	fm.submit();
	
}

function afterSubmit(FlagStr,Content ){
	
	EasyQueryClick();
	fm.pushReceipt.disabled = false;
	showInfo.close();
	
	if(FlagStr == "Fail"){
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}