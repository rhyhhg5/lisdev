<%@page import="com.sinosoft.utility.*"%>
<%
	GlobalInput tGlobalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = tGlobalInput.ManageCom;
%>

<script language="JavaScript">
	function initInpBox(){
		try{
			fm.ManageCom.value = <%=strManageCom%>;
			if(fm.ManageCom.value != null){
				var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
				if(arrResult != null){
					fm.ManageComName.value = arrResult[0][0];
				}
			}
		}catch(ex){
			alert("在GetPayMoneyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	function initForm(){
		try{
			initInpBox();
			initContGrid();
		}catch(re){
			alert("在GetPayMoneyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	function initContGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array();
		    iArray[0][0]="序号";
		    iArray[0][1]="30px";
		    iArray[0][2]=30;
		    iArray[0][3]=0;

		    iArray[1]=new Array();
		    iArray[1][0]="保全受理号";
		    iArray[1][1]="50px";
		    iArray[1][2]=200;
		    iArray[1][3]=0;
		      
		    iArray[2]=new Array();
		    iArray[2][0]="保单号";
		    iArray[2][1]="50px";        
		    iArray[2][2]=200;
		    iArray[2][3]=0;
		    
		  
		    iArray[3]=new Array();
		    iArray[3][0]="保全结案日期";      
		    iArray[3][1]="40px";        
		    iArray[3][2]=200;
		    iArray[3][3]=0;

		    iArray[4]=new Array();
		    iArray[4][0]="保全金额";      
		    iArray[4][1]="40px";
		    iArray[4][2]=200;
		    iArray[4][3]=0;
		    
		    iArray[5]=new Array();
		    iArray[5][0]="状态";      
		    iArray[5][1]="40px";
		    iArray[5][2]=200;
		    iArray[5][3]=0;

		    iArray[6]=new Array();
		    iArray[6][0]="返回错误信息";      
		    iArray[6][1]="40px";
		    iArray[6][2]=200;
		    iArray[6][3]=0;
		 
		    ContGrid = new MulLineEnter("fm", "ContGrid"); 
			  
		    ContGrid.mulLineCount = 0;
		    ContGrid.displayTitle = 1;
		    ContGrid.canChk = 1;
		    ContGrid.canSel = 0;	
		    ContGrid.hiddenSubtraction = 1;
		    ContGrid.hiddenPlus = 1;
		    
		    ContGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>