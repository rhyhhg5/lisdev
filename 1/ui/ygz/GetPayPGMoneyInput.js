var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;

/* 查询 */
function EasyQueryClick(){
	
	if(!verifyInput2()){
		return false;
	}
	
	var policyType = fm.PolicyType.value;
	
	var strSQL = "";
	
	var tStartDate = fm.CvaliStartDate.value;
	var tEndDate = fm.CvaliEndDate.value;
	var tPrtNo = fm.PrtNo.value;
	var tContNo = fm.ContNo.value;
	if((tPrtNo != null && !"" == tPrtNo) || (tContNo !=null && !"" == tContNo)){
		
	}else if((tContNo == null || "" == tContNo) && (tPrtNo == null || "" == tPrtNo)){
		if(tStartDate==null || ""==tStartDate){
			alert("起始日期必录项！");
			return false;
		}
		if(tEndDate==null || ""==tEndDate){
			alert("终止日期必录项！");
			return false;
		}
	}
	if((tStartDate != null && tStartDate != "") 
			&& (tEndDate != null && tEndDate != ""))
		{
			if(dateDiff(tStartDate,tEndDate,"M") > 1)
			{
				alert("查询间隔最多为一个月！");
				return false;
			}
		}
	
	if(policyType!=null && policyType=="2"){
		strSQL = "select grpcontno,prtno,'团单', "
			   + "(	select codename as ygzstate from ldcode where codetype='ygzstate' and code = ( nvl((select state from lypremseparatedetail where prtno=lcgrpcont.prtno and otherstate= '03' union select '02' from ljapaygrp lja where lja.moneynotax is not null and lja.moneynotax != '' and lja.grpcontno=lcgrpcont.grpcontno and lja.paytype='ZC' and lja.payno in (select payno from ljapay where incomeno = lja.grpcontno and duefeetype ='0' ) fetch first rows only ) ,'00') ))," 
			   + "( select codename as ygzfpstate from ldcode where codetype='ygzfpstate' and code = (select  nvl(( select state from LYOutPayDetail where insureno=lcgrpcont.prtno and busino in (select busino from lypremseparatedetail where prtno=lcgrpcont.prtno and otherstate= '03') union select state from LYOutPayDetail where insureno=lcgrpcont.prtno and busino in (select trim(lja.grppolno)||trim(lja.payno)||trim(lja.paytype) from ljapaygrp lja where lja.moneynotax is not null and lja.moneynotax != '' and lja.grpcontno=lcgrpcont.grpcontno and lja.paytype='ZC' and lja.payno in (select payno from ljapay where incomeno = lja.grpcontno and duefeetype ='0' )) fetch first rows only ),'01') from dual))"
			   + "from lcgrpcont "
			   + "where ManageCom like '"+fm.ManageCom.value+"%' "
			   + "and uwflag is not null and uwflag in ('4','9') "//已经核保且核保同意承保
			   + "and (cvalidate>='2016-05-01' or signdate>='2016-05-01') "
			   + getWherePart('PrtNo','PrtNo')
			   + getWherePart('grpcontno','ContNo')
			   + getWherePart('Cvalidate','CvaliStartDate',">=")
			   + getWherePart('Cvalidate','CvaliEndDate',"<=")
			   + " union "
			   + "select grpcontno,prtno,'团单', "
			   + "( select codename as ygzstate from ldcode where codetype='ygzstate' and code = ( nvl((select state from lypremseparatedetail where prtno=lbgrpcont.prtno and otherstate= '03' union select '02' from ljapaygrp lja where lja.moneynotax is not null and lja.moneynotax != '' and lja.grpcontno=lbgrpcont.grpcontno and lja.paytype='ZC' and lja.payno in (select payno from ljapay where incomeno = lja.grpcontno and duefeetype ='0' ) fetch first rows only ) ,'00') ))," 
			   + "( select codename as ygzfpstate from ldcode where codetype='ygzfpstate' and code = (select  nvl(( select state from LYOutPayDetail where insureno=lbgrpcont.prtno and busino in (select busino from lypremseparatedetail where prtno=lbgrpcont.prtno and otherstate= '03') union select state from LYOutPayDetail where insureno=lbgrpcont.prtno and busino in (select trim(lja.grppolno)||trim(lja.payno)||trim(lja.paytype) from ljapaygrp lja where lja.moneynotax is not null and lja.moneynotax != '' and lja.grpcontno=lbgrpcont.grpcontno and lja.paytype='ZC' and lja.payno in (select payno from ljapay where incomeno = lja.grpcontno and duefeetype ='0' )) fetch first rows only ),'01') from dual))"
			   + "from lbgrpcont "
			   + "where ManageCom like '"+fm.ManageCom.value+"%' "
			   + "and uwflag is not null and uwflag in ('4','9') "//已经核保且核保同意承保
			   + "and (cvalidate>='2016-05-01' or signdate>='2016-05-01') "
			   + getWherePart('PrtNo','PrtNo')
			   + getWherePart('grpcontno','ContNo')
			   + getWherePart('Cvalidate','CvaliStartDate',">=")
			   + getWherePart('Cvalidate','CvaliEndDate',"<=")
			   + " order by grpcontno ";
	}else if(policyType!=null && policyType=="1"){
		strSQL = "select contno,prtno,'个单', "
			   + "( case when (select 1 from ljapayperson lja where lja.moneynotax is not null  and lja.moneytax != '' and lja.paytype ='ZC' and lja.grpcontno = '00000000000000000000'and lja.contno = lccont.contno and lja.payno in (select payno from ljapay where incomeno = lja.contno and duefeetype ='0' ) fetch first rows only) = '1' then '价税分离' else '未提取' end)," 
			   + "( select codename as ygzfpstate from ldcode where codetype='ygzfpstate' and code = (select  nvl(( select lyo.state from LYOutPayDetail lyo where lyo.insureno=lccont.prtno and exists(select 1 from ljapayperson lyp where lyp.contno = lyo.voucherid and lyo.busino= trim(lyp.polno)||trim(lyp.payno)||trim(lyp.DUTYCODE)||trim(lyp.PAYPLANCODE)||trim(lyp.PAYTYPE) and lyp.grpcontno = '00000000000000000000'and lyp.moneynotax is not null and lyp.moneynotax != '' and lyp.payno in (select payno from ljapay where incomeno = lyp.contno and duefeetype ='0' )) fetch first rows only),'01') from dual))"
			   + "from lccont "
			   + "where ManageCom like '"+fm.ManageCom.value+"%' "
			   + "and conttype='1' " //个单
			   + "and appflag='1' "	 //已承保
			   + "and exists (select 1 from ljapayperson lja where lja.contno=lccont.contno and lja.enteraccdate>='2016-05-01' and lja.paytype ='ZC' and lja.grpcontno = '00000000000000000000' and lja.payno in (select payno from ljapay where incomeno = lja.contno and duefeetype ='0' )) "
			   + getWherePart('PrtNo','PrtNo')
			   + getWherePart('ContNo','ContNo')
			   + getWherePart('Cvalidate','CvaliStartDate',">=")
			   + getWherePart('Cvalidate','CvaliEndDate',"<=")
			   + " union "
			   + "select contno,prtno,'个单', "
			   + "( case when (select 1 from ljapayperson lja where lja.moneynotax is not null  and lja.moneytax != '' and lja.paytype ='ZC' and lja.grpcontno = '00000000000000000000' and lja.contno = lbcont.contno and lja.payno in (select payno from ljapay where incomeno = lja.contno and duefeetype ='0' ) fetch first rows only) = '1' then '价税分离' else '未提取' end)," 
			   + "( select codename as ygzfpstate from ldcode where codetype='ygzfpstate' and code = (select  nvl(( select lyo.state from LYOutPayDetail lyo where lyo.insureno=lbcont.prtno and exists(select 1 from ljapayperson lyp where lyp.contno = lyo.voucherid and lyo.busino= trim(lyp.polno)||trim(lyp.payno)||trim(lyp.DUTYCODE)||trim(lyp.PAYPLANCODE)||trim(lyp.PAYTYPE) and lyp.grpcontno = '00000000000000000000'and lyp.moneynotax is not null and lyp.moneynotax != '' and lyp.payno in (select payno from ljapay where incomeno = lyp.contno and duefeetype ='0' )) fetch first rows only),'01') from dual))"
			   + "from lbcont "
			   + "where ManageCom like '"+fm.ManageCom.value+"%' "
			   + "and conttype='1' " //个单
			   + "and exists (select 1 from ljapayperson lja where lja.contno=lbcont.contno and lja.enteraccdate>='2016-05-01' and lja.paytype ='ZC' and lja.grpcontno = '00000000000000000000' and lja.payno in (select payno from ljapay where incomeno = lja.contno and duefeetype ='0' )) "
			   + getWherePart('PrtNo','PrtNo')
			   + getWherePart('ContNo','ContNo')
			   + getWherePart('Cvalidate','CvaliStartDate',">=")
			   + getWherePart('Cvalidate','CvaliEndDate',"<=")
			   + " order by contno ";
	}
	
	turnPage.queryModal(strSQL,ContGrid);
	if(!turnPage.strQueryResult){
		alert("没有查询到相关数据！");
		return false;
	}
}

/* 调用接口，生成价税分离数据 */
function generateData(){
	var policyType = fm.PolicyType.value;
	fm.btnGen.disabled = true;
	
	var checkedRowNum = 0;
    var rowNum = ContGrid.mulLineCount;
    for (var i = 0; i < rowNum; i++){
        if(policyType!=null && policyType=="1"){
	        if(ContGrid.getChkNo(i)){
	            checkedRowNum += 1;
	            var tContNo = ContGrid.getRowColData(i,1);
	            var tSQL = "select lja.moneynotax from ljapayperson lja where lja.paytype='ZC' and lja.payno in (select payno from ljapay where incomeno = lja.contno and duefeetype ='0' ) and lja.contno = '"+tContNo+"' ";
	            var arr = easyExecSql(tSQL);
	            if(arr && arr[0][0] != null && arr[0][0] != ''){
	            	alert("印刷号为:"+tContNo+"的单子，已经做过价税分离了，不用再次推送！");
	            	fm.btnGen.disabled = false;
	            	return;
	            }
	            var tSQLX = "select lja.moneynotax from ljapayperson lja where lja.paytype='ZC' and lja.payno not in (select payno from ljapay where incomeno = lja.contno and duefeetype ='0' ) and lja.contno = '"+tContNo+"' ";
	            var arrx = easyExecSql(tSQLX);
	            if((arr[0][0] == null || arr[0][0] == '')&& arrx && arrx[0][0] != null && arrx[0][0] != ''){
	            	alert("未查到需要价税分离的数据或首期数据在2016-05-01之前!");
	            	fm.btnGen.disabled = false;
	            	return;
	            }
	        }
        }else if(policyType!=null && policyType=="2"){
        	if(ContGrid.getChkNo(i)){
	            checkedRowNum += 1;							
	            var tContNo = ContGrid.getRowColData(i,1);
	            var tSQL = " select nvl ((select state from lypremseparatedetail where policyno='"+tContNo+"' and otherstate= '03'and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') union select '02' from ljapaygrp lja where lja.moneynotax is not null and lja.moneynotax != '' and lja.grpcontno='"+tContNo+"' and lja.paytype='ZC' and lja.payno in (select payno from ljapay where incomeno = lja.grpcontno and duefeetype ='0' )) ,'00') as state from dual";
	            var arr = easyExecSql(tSQL);
	            if(arr && arr[0][0]=="02"){
	            	alert("印刷号为:"+tContNo+"的单子，已经做过价税分离了，不用再次推送！");
	            	fm.btnGen.disabled = false;
	            	return;
	            }
	        }
        }
    }

    if(checkedRowNum < 1){
        alert("请至少选择一条记录!");
        fm.btnGen.disabled = false;
        return false;
    }
	
    var PolicyType = fm.PolicyType.value;    
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GetPayPGMoneySave.jsp?PolicyType="+PolicyType;
	fm.submit();
	
}

/* 对选中的数据进行判断，打印 */
function printInvoice(){
	var policyType = fm.PolicyType.value;
	fm.btnPrt.disabled = true;
	
	var checkedRowNum = 0;
    var rowNum = ContGrid.mulLineCount;
    for (var i = 0; i < rowNum; i++){
        if(policyType!=null && policyType=="1"){
	        if(ContGrid.getChkNo(i)){
	            checkedRowNum += 1;
	            var tContNo = ContGrid.getRowColData(i,1);
	            var tSQL = "select lja.moneynotax from ljapayperson lja where lja.paytype='ZC' and lja.payno in (select payno from ljapay where incomeno = lja.contno and duefeetype ='0' ) and lja.contno = '"+tContNo+"' ";
	            var arr = easyExecSql(tSQL);
	            if(arr == null||arr == ""){
	            	alert("保单号为:"+tContNo+"的单子，尚没有做价税分离，不能推送打印数据！");
	            	fm.btnPrt.disabled = false;
	            	return;
	            }
	            if(arr[0][0] == null || arr[0][0] == ""){
	            	alert("保单号为:"+tContNo+"的单子，尚没有做价税分离，不能推送打印数据！");
	            	fm.btnPrt.disabled = false;
	            	return;
	            }
	            var tSQL1 = "select lyo.state from LYOutPayDetail lyo where exists (select 1 from ljapayperson lja where lja.contno = lyo.voucherid and trim(lja.polno)||trim(lja.payno)||trim(lja.DUTYCODE)||trim(lja.PAYPLANCODE)||trim(lja.PAYTYPE) = lyo.busino and lja.moneynotax is not null and lja.moneynotax !='' and lja.paytype ='ZC' and lja.payno in (select payno from ljapay where incomeno = lja.contno and duefeetype ='0' )) and lyo.voucherid = '"+tContNo+"' ";
	            var arr1 = easyExecSql(tSQL1);
	            if(arr1 && arr1[0][0] == "02"){
	            	alert("保单号为:"+tContNo+"的单子，已经推送成功了，不用再次推送！");
	            	fm.btnPrt.disabled = false;
	            	return;
	            }
	        }
        }else if(policyType!=null && policyType=="2"){
        	if(ContGrid.getChkNo(i)){
	            checkedRowNum += 1;
	            var tContNo = ContGrid.getRowColData(i,1);
	            var tSQL = "select nvl ((select state from lypremseparatedetail where policyno='"+tContNo+"' and otherstate= '03' union select '02' from ljapaygrp ljt where ljt.moneynotax is not null and ljt.moneynotax != '' and ljt.grpcontno='"+tContNo+"' and ljt.paytype='ZC' and ljt.payno in (select payno from ljapay where incomeno = ljt.grpcontno and duefeetype ='0' )) ,'00') as state from dual ";
	            var arr = easyExecSql(tSQL);
	            if(arr && arr[0][0]!="02"){
	            	alert("保单号为:"+tContNo+"的单子，尚没有做价税分离，不能推送打印数据！====");
	            	fm.btnPrt.disabled = false;
	            	return;
	            }
	            else if(arr == null||arr == ""){
	            	alert("保单号为:"+tContNo+"的单子，尚没有提取数据，不能推送打印数据！");
	            	fm.btnPrt.disabled = false;
	            	return;
	            }
	            var tSQL1 = "select lyo.state from LYOutPayDetail lyo where (exists (select 1 from lypremseparatedetail where otherstate = '03' and busino = lyo.busino) or exists(select 1 from ljapaygrp ljt where ljt.moneynotax is not null and ljt.moneynotax != '' and trim(ljt.grppolno)||trim(ljt.payno)||trim(ljt.paytype) = lyo.busino and ljt.paytype ='ZC' and ljt.payno in (select payno from ljapay where incomeno = ljt.grpcontno and duefeetype ='0')))and lyo.voucherid = '"+tContNo+"' ";
	            var arr1 = easyExecSql(tSQL1);
	            if(arr1 && arr1[0][0] == "02"){
	            	alert("保单号为:"+tContNo+"的单子，已经推送成功了，不用再次推送！");
	            	fm.btnPrt.disabled = false;
	            	return;
	            }
	        }
        }
    }

    if(checkedRowNum < 1){
        alert("请至少选择一条记录!");
        fm.btnPrt.disabled = false;
        return false;
    }
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GetPayPGInvoiceSave.jsp";
	fm.submit();
	
}

/* 生成价税分离数据后，save页面回调该方法 */
function afterGenerateData(FlagStr,Content ){
	
	EasyQueryClick();
	fm.btnGen.disabled = false;
	showInfo.close();
	
	if(FlagStr == "Fail"){             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

/* 报送打印发票数据后，save页面回调该方法 */
function afterPrintInvoice(FlagStr,Content ){
	
	EasyQueryClick();
	fm.btnPrt.disabled = false;
	showInfo.close();
	
	if(FlagStr == "Fail"){             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}