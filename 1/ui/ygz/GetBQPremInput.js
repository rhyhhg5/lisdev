var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;

/* 查询 */
function EasyQueryClick() {

	if (!verifyInput2()) {
		return false;
	}

	var policyType = fm.PolicyType.value;
	var edorNo = fm.EdorNo.value;

	var strSQL = "";
	if ("" == policyType) {
		alert("请选择保单类型");
		return false;
	}

	var startDate = fm.EdorValiStartDate.value;

	var endDate = fm.EdorValiEndDate.value;

	if( null==edorNo || ""==edorNo){
		if (null==startDate || "" == startDate) {
			alert("请选择保全结案日期起期");
			return false;
		}
		
		if (null== endDate || "" == endDate ) {
			alert("请选择保全结案日期止期");
			return false;
		}
	}
	//保全起止日期三个月校验
	var t1 = new Date(startDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(endDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>90 )
    {
		alert("保全结案日期起期和止期之间不能超过三个月!");
		return false;
    }


	if (policyType == "1") {

		strSQL = "select  distinct a.edorno,a.contno,b.confdate, b.getmoney, "
			+" case when  (select 1 from lyoutpaydetail where bqbatchno=a.edorno "
			+" fetch first 1 rows only) is not null"
			+" then  (select case when state='01' then '提取未推送' when  state='02' "
			+" then '推送成功' else  '推送失败' end from lyoutpaydetail where bqbatchno=a.edorno fetch first 1 rows only )"
			+" when (select 1 from LYPremSeparateDetail where "
			+" policyno=a.contno and otherno=a.edorno and state='02' fetch first 1 rows only) is not null  then '价税分离' 	else  '未价税分离' end ,"
			+" nvl((select errorinfo from lyoutpaydetail where bqbatchno=a.edorno fetch first 1 rows only),"
			+" (select errorinfo from LYPremSeparateDetail where otherno=a.edorno fetch first 1 rows only)) "
			+" from lpedoritem a,lpedorapp b "
			+" where a.edorno=b.edoracceptno "
			+" and b.edorstate = '0' "
			+" and exists(select 1 from lccont where contno=a.contno and conttype='1' union select 1 from lbcont where contno=a.contno and conttype='1') "
		    +" and a.managecom like '"+fm.ManageCom.value+"%' "
			+ getWherePart('a.EdorNo','EdorNo')
			+ getWherePart('b.ConfDate', 'EdorValiStartDate', ">=")
			+ getWherePart('b.ConfDate', 'EdorValiEndDate', "<=")
			+" with ur ";
	
	} else {
		strSQL = "select  distinct a.edorno,a.grpcontno, b.confdate, b.getmoney, "		
			+" case when  (select 1 from lyoutpaydetail where bqbatchno=a.edorno "
			+" fetch first 1 rows only ) is not null"
			+" then  (select case when state='01' then '提取未推送' when  state='02' "
			+" then '推送成功' else  '推送失败' end from lyoutpaydetail where  bqbatchno=a.edorno fetch first 1 rows only )"
			+" when (select 1 from LYPremSeparateDetail where "
			+" policyno=a.grpcontno and otherno=a.edorno and state='02' fetch first 1 rows only) is not null then '价税分离' 	else  '未价税分离' end, "
			+" nvl((select errorinfo from lyoutpaydetail where bqbatchno=a.edorno fetch first 1 rows only),"
			+" (select errorinfo from LYPremSeparateDetail where otherno=a.edorno fetch first 1 rows only)) "
			+" from lpgrpedoritem a,lpedorapp b"
			+" where a.edorno=b.edoracceptno "
			+" and b.edorstate = '0' "
			+" and a.managecom like '"+fm.ManageCom.value+"%' "
			+ getWherePart('a.EdorNo','EdorNo')
			+ getWherePart('b.ConfDate', 'EdorValiStartDate', ">=")
		    + getWherePart('b.ConfDate', 'EdorValiEndDate', "<=")
//		    +" union " //20170209特需满期发票
//		    +"select distinct lg.workno,lg.contno,lg.modifydate,(select sum(getmoney) from ljagetendorse where endorsementno=lg.workno), "
//		    +" nvl((select (case when state='01' then '提取未推送' when state='02' then '推送成功' else '推送失败' end) from lyoutpaydetail where bqbatchno=lg.workno fetch first 1 rows only),"
//		    +" nvl((select distinct (case when state='01' then '未价税分离' when state='02' then '价税分离' end) from LYPremSeparateDetail where otherno=lg.workno fetch first 1 rows only),('未价税分离'))) "
//		    +" from lgwork lg "
//		    +" where lg.statusno='5' "//已结案
//		    +" and typeno='070015' "//特需满期
//		    +" and exists (select 1 from ljaget where otherno=lg.workno and managecom like '"+fm.ManageCom.value+"%' "
//		    + getWherePart('makedate', 'EdorValiStartDate', ">=")
//		    + getWherePart('makedate', 'EdorValiEndDate', "<=")
//		    +")"
//		    + getWherePart('lg.workno','EdorNo')
			+" with ur ";
	}

	turnPage.queryModal(strSQL, ContGrid);

	if (!turnPage.strQueryResult) {
		alert("没有查询到相关数据！");
		return false;
	}
}

/* 调用接口 */
function callPremBill() {

	fm.pushPremBill.disabled = true;

	var checkedRowNum = 0;
	var rowNum = ContGrid.mulLineCount;

	for ( var i = 0; i < rowNum; i++) {
		if (checkedRowNum > 1) {
			break;
		}

		if (ContGrid.getChkNo(i)) {
			checkedRowNum += 1;
		}
	}

	if (checkedRowNum < 1) {
		alert("请至少选择一条记录!");
		return false;
	}

	var PolicyType = fm.PolicyType.value;

	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GetBQPremSave.jsp?PolicyType=" + PolicyType;
	fm.submit();

}

function afterSubmit(FlagStr, Content) {

	EasyQueryClick();
	fm.pushPremBill.disabled = false;
	showInfo.close();

	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}