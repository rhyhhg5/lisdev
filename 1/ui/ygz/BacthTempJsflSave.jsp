<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.ygz.*"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String tOperator = tG.Operator;
	String tComCode = tG.ComCode;
	
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	
	//得到前台页面的数据并进行封装
	String StartDate = request.getParameter("StartDate");
	String EndDate = request.getParameter("EndDate");
	String SL = request.getParameter("SL");
	try{
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("StartDate", StartDate);
		transferData.setNameAndValue("EndDate", EndDate);
		transferData.setNameAndValue("SL", SL);		
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		
		BacthTempJsflBL tBacthTempJsflBL = new BacthTempJsflBL();
		if(!tBacthTempJsflBL.submitData(tVData,"")){
			Content = " 处理失败，原因是: " + tBacthTempJsflBL.mErrors.getFirstError();
            FlagStr = "Fail";
		}else{
            Content = " 处理成功！";
            FlagStr = "Succ";
        }
		
	}catch(Exception e){
		Content = "数据提交失败";
		e.printStackTrace();
	}
	
%>

<html>
	<script language="javascript">
    	parent.fraInterface.afterGenerateData("<%=FlagStr%>","<%=Content%>");
  	</script>
</html>
