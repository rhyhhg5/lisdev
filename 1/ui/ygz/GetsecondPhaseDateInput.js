
var showInfo;

/* 调用接口，生成价税分离数据 */
function generateData(){
	
	var tStartDate = fm.CvaliStartDate.value;
	var tEndDate = fm.CvaliEndDate.value;
	if(tStartDate==null || ""==tStartDate){
		alert("起始日期必录项！");
		return false;
	}
	if(tEndDate==null || ""==tEndDate){
		alert("终止日期必录项！");
		return false;
	}
	
	if((tStartDate != null && tStartDate != "") 
			&& (tEndDate != null && tEndDate != ""))
		{
			if(dateDiff(tStartDate,tEndDate,"M") > 1)
			{
				alert("查询间隔最多为一个月！");
				return false;
			}
			
			if(dateDiff(tStartDate,"2016-05-01","D") >= 1)
			{
				alert("确定选择"+tStartDate+"为开始日期！");				
			}
		}

	fm.btnGen.disabled = true;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GetsecondPhaseDateSave.jsp?StartDate="+tStartDate+"&EndDate="+tEndDate;
	fm.submit();
	
}

/* 生成价税分离数据后，save页面回调该方法 */
function afterGenerateData(FlagStr,Content ){
	fm.btnGen.disabled = false;
	showInfo.close();
	if(FlagStr == "Fail"){             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}
