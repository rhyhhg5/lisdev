//程序名称：TaskService.js
//程序功能：
//创建日期：2004-12-15 
//创建人  ：ZhangRong
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var arrResult;
var mDebug = "0";
var mOperate = "";
var mAction = "";
var mSwitch = parent.VD.gVSwitch;
var k = 0;
 var turnPage = new turnPageClass();   
 var turnPage1 = new turnPageClass();
 var turnPage2 = new turnPageClass();
/*********************************************************************
 *  提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
	if( mAction == "")
		return;

	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit(); //提交
}

function appendOne()
{	
	if(checkDateTime()== false) return false;
	var mstartTime = fm.all("StartTime").value;
	var mendTime = fm.all("EndTime").value;
	if (!(mstartTime == null||mstartTime=='') && mstartTime.length != 19)
	{
		alert('起始日期格式错误，应为"YY-MM-DD HH:MM:SS"，请修改后再增加任务！');
		return;
	}
	if (!(mendTime == null||mendTime=='')&& mendTime.length != 19)
	{
		alert('终止日期格式错误，应为"YY-MM-DD HH:MM:SS"，请修改后再增加任务！');
		return;
	}
	mAction = "INSERT";
	if (fm.all("TaskCode").value == '' || fm.all("RunFlag").value == '' || fm.all("RecycleType").value == '')
	{
		alert("请输入任务编码，是否启动和循环类型信息！");
		return;
	}
	fm.all( 'fmAction' ).value = mAction;
	submitForm();
}

//对起始时间终止时间的校验
function checkDateTime()
{
	if( verifyInput2() == false ) return false;
	if(parseInt(fm.all('StartH').value) < 0 || parseInt(fm.all('StartH').value) > 23 )
	{
		alert("‘起始时间-小时’ 的取值范围是：0-23 ");
		return false;
	}
	if(parseInt(fm.all('StartM').value) < 0 || parseInt(fm.all('StartM').value) > 59 )
	{
		alert("‘起始时间-分’ 的取值范围是：0-59 ");
		return false;
	}
	if(parseInt(fm.all('StartS').value) < 0 || parseInt(fm.all('StartS').value) > 59 )
	{
		alert("‘起始时间-秒’ 的取值范围是：0-59 ");
		return false;
	}
	if(parseInt(fm.all('EndH').value) < 0 || parseInt(fm.all('EndH').value) > 23 )
	{
		alert("‘终止时间-小时’ 的取值范围是：0-23 ");
		return false;
	}
	if(parseInt(fm.all('EndM').value) < 0 || parseInt(fm.all('EndM').value) > 59 )
	{
		alert("‘终止时间-分’ 的取值范围是：0-59 ");
		return false;
	}
	if(parseInt(fm.all('EndS').value) < 0 || parseInt(fm.all('EndS').value) > 59 )
	{
		alert("‘终止时间-秒’ 的取值范围是：0-59 ");
		return false;
	}
//	fm.all('StartTime').value = fm.all('StartDate').value
//			+" "+(fm.all('StartH').value.length==1?"0"+fm.all('StartH').value:fm.all('StartH').value)
//			+":"+(fm.all('StartM').value.length==1?"0"+fm.all('StartM').value:fm.all('StartM').value)
//			+":"+(fm.all('StartS').value.length==1?"0"+fm.all('StartS').value:fm.all('StartS').value)
//			;
	fm.all('StartTime').value = fm.all('StartDate').value+" "+fm.all('StartH').value+":"+fm.all('StartM').value+":"+fm.all('StartS').value;
	fm.all('EndTime').value = fm.all('EndDate').value+" "+fm.all('EndH').value+":"+fm.all('EndM').value+":"+fm.all('EndS').value;
			
	if(fm.all('StartTime').value ==" ::") {fm.all('StartTime').value = "";}
	if(fm.all('EndTime').value ==" ::") {fm.all('EndTime').value = "";}
}           

function deleteOne()
{
	mAction = "DELETE";
	if (fm.all("TaskPlanCode").value == '')
	{
		alert("请选择一条任务计划！");
		return;
	}
	fm.all( 'fmAction' ).value = mAction;
	submitForm();
}

function activateOne()
{
	mAction = "ACTIVATE";
	if (fm.all("TaskPlanCode").value == '')
	{
		alert("请选择一条任务计划！");
		return;
	}
	fm.all( 'fmAction' ).value = mAction;
	submitForm();
}

function deactivateOne()
{
	mAction = "DEACTIVATE";
	if (fm.all("TaskPlanCode").value == '')
	{
		alert("请选择一条任务计划！");
		return;
	}
	fm.all( 'fmAction' ).value = mAction;
	submitForm();
}

function startEngine()
{
	mAction = "START";
	fm.all( 'fmAction' ).value = mAction;
	submitForm();
}

function stopEngine()
{
	mAction = "STOP";
	fm.all( 'fmAction' ).value = mAction;
	submitForm();
}
function appendTask()
{
	mAction = "INSERTTASK";
	if (fm.all("TaskDescribe").value == '' || fm.all("TaskClass").value == '')
	{
		alert("请输入任务描述和任务处理类");
		return;
	}
	fm.all( 'fmAction' ).value = mAction;
	submitForm();
}

function QueryTask()
{   
	
	turnPage1.pageDivName="divPage1";
	resetForm();
	initTaskStateGrid();
	var condition="";
	if(fm.all( 'StatusTaskCode' ).value!=""){
		condition+=" and a.taskcode='"+fm.all( 'StatusTaskCode' ).value+"'";
	}
	if(fm.all( 'BeginDate' ).value!=""){
		condition+=" and c.executedate>='"+fm.all( 'BeginDate' ).value+"'";
	}
	if(fm.all( 'AfterDate' ).value!=""){
		condition+=" and c.executedate<='"+fm.all( 'AfterDate' ).value+"'";
	}
	//fm.all( 'BeginDate' ).value="";
	//fm.all( 'AfterDate' ).value="";
	//fm.all( 'StatusTaskCode' ).value="";
  var strSQL="select distinct a.taskcode,a.taskdescribe,b.taskplancode,b.Starttime,c.executedate,c.executetime,  ";
  strSQL+="(case  when executestate = '1' then '执行正常' when executestate is null then '没有执行' when executestate = '4' then '终止执行' end ),  ";
  strSQL+="(case runstate when '0' then '正常等待状态' when '1' then '启动状态' when '2' then '暂停状态' when '3' then '正常终止状态' when '4' then '强行终止状态' when '5' then '异常终止状态' end)  ";
  strSQL+="from    ";
  strSQL+="ldtask a,ldtaskplan b,LDTASKRUNLOG c  ";
  strSQL+="where a.taskcode=b.taskcode  ";
  strSQL+="and b.taskplancode=c.taskplancode   ";
  strSQL+="and a.taskcode=c.taskcode  "+condition;
  strSQL+="order by a.taskcode with ur ";
	//turnPage1.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  turnPage1.queryModal(strSQL, TaskStateGrid);
	//查询成功则拆分字符串，返回二维数组
	//turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	//turnPage1.pageDisplayGrid = TaskStateGrid;
	//保存SQL语句
	//turnPage1.strQuerySql = strSQL;
	//设置查询起始位置
	//turnPage1.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	//turnPage1.pageLineNum = 15;
	//调用MULTILINE对象显示查询结果
	//var arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet,turnPage1.pageIndex, turnPage1.pageLineNum);
//	displayMultiline(arrDataSet, turnPage1.pageDisplayGrid, turnPage1);
	
}

function deleteTask()
{
	mAction = "DELETETASK";
	if (fm.all("BaseTaskCode").value == '')
	{
		alert("请选择一条任务！");
		return;
	}
	fm.all( 'fmAction' ).value = mAction;
	submitForm();
}


/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	
	mAction = ""; 
	queryTaskPlanInfo();
	queryTaskInfo();
	QueryTask();
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}


/*********************************************************************
 *  显示任务计划
 *  参数  ：  
 *  返回值：  无
 *********************************************************************
 */
function queryTaskPlanInfo()
{
	resetForm();
	initTaskPlanGrid();
	initParamGrid();
	var strSQL = "select a.TaskPlanCode, a.TaskCode, b.TaskDescribe, a.RunFlag, a.RecycleType, a.StartTime, a.EndTime, a.Interval, a.Times, a.RunState from LDTaskPlan a, LDTask b where a.TaskCode = b.TaskCode order by a.TaskPlanCode DESC";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  
	if (!turnPage.strQueryResult) 
	{
		return;
    }

	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = TaskPlanGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	turnPage.pageLineNum = 15;
	//调用MULTILINE对象显示查询结果
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,turnPage.pageIndex, turnPage.pageLineNum);
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid, turnPage);

}

function queryTaskInfo()
{
/*	var strSQL = "select TaskCode, TaskDescribe from LDTask order by TaskCode";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	alert(turnPage.strQueryResult);

	fm.TaskCode.CodeData = "";
	//判断是否查询成功
	if (turnPage.strQueryResult == "")
	{
	return "";
	}
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	var returnstr = "";
	var n = turnPage.arrDataCacheSet.length;
	if (n > 0)
	{
		for( i = 0;i < n; i++)
		{
			m = turnPage.arrDataCacheSet[i].length;
			if (m > 0)
			{
				for( j = 0; j< m; j++)
				{
					if (i == 0 && j == 0)
					{
						returnstr = "0|^" + turnPage.arrDataCacheSet[i][j];
					}
					if (i == 0 && j > 0)
					{
						returnstr = returnstr + "|" + turnPage.arrDataCacheSet[i][j];
					}
					if (i > 0 && j == 0)
					{
						returnstr = returnstr + "^" + turnPage.arrDataCacheSet[i][j];
					}
					if (i > 0 && j > 0)
					{
						returnstr = returnstr + "|" + turnPage.arrDataCacheSet[i][j];
					}
					
				}
			}
			else
			{
				return;
			}
		}
	}
	else
	{
	return;
	}

	fm.TaskCode.CodeData = returnstr;
//  alert(returnstr)
*/
	resetForm();
	initTaskGrid();
	turnPage2.pageDivName="divPage2";
	k = k + 1;
	var strSQL = "select TaskCode, TaskDescribe, TaskClass from LDTask where " + k + "=" + k + " order by TaskCode ";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	fm.TaskCode.CodeData = turnPage.strQueryResult;

	if (!turnPage.strQueryResult) 
	{
		return;
    }

	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = TaskGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	//调用MULTILINE对象显示查询结果
	displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
	turnPage2.queryModal(strSQL, TaskGrid);
}


/*********************************************************************
 *  选择任务计划后的响应事件
 *  参数  ：  
 *  返回值：  无
 *********************************************************************
 */
function onTaskPlanSelected(parm1,parm2)
{
	fm.all("TaskPlanCode").value = TaskPlanGrid.getRowColData(TaskPlanGrid.getSelNo() - 1, 1);
	fm.all("TaskCode").value = TaskPlanGrid.getRowColData(TaskPlanGrid.getSelNo() - 1, 2);
	fm.all("RunFlag").value = TaskPlanGrid.getRowColData(TaskPlanGrid.getSelNo() - 1, 4);
	fm.all("RecycleType").value = TaskPlanGrid.getRowColData(TaskPlanGrid.getSelNo() - 1, 5);
	fm.all("StartTime").value = TaskPlanGrid.getRowColData(TaskPlanGrid.getSelNo() - 1, 6);
		fm.all('StartDate').value = fm.all('StartTime').value.substring(0,10);
		fm.all('StartH').value = fm.all('StartTime').value.substring(11,13);
		fm.all('StartM').value = fm.all('StartTime').value.substring(14,16);
		fm.all('StartS').value = fm.all('StartTime').value.substring(17);
	fm.all("EndTime").value = TaskPlanGrid.getRowColData(TaskPlanGrid.getSelNo() - 1, 7);
		fm.all('EndDate').value = fm.all('EndTime').value.substring(0,10);
		fm.all('EndH').value = fm.all('EndTime').value.substring(11,13);  
		fm.all('EndM').value = fm.all('EndTime').value.substring(14,16);  
		fm.all('EndS').value = fm.all('EndTime').value.substring(17);     
	fm.all("Interval").value = TaskPlanGrid.getRowColData(TaskPlanGrid.getSelNo() - 1, 8);
	fm.all("Times").value = TaskPlanGrid.getRowColData(TaskPlanGrid.getSelNo() - 1, 9);

	initParamGrid();
	var strSQL = "select ParamName, ParamValue from LDTaskParam where TaskPlanCode = '" + fm.all("TaskPlanCode").value + "' and TaskCode = '" + fm.all("TaskCode").value + "'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	if (!turnPage.strQueryResult) 
	{
		return;
    }

	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = ParamGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	//调用MULTILINE对象显示查询结果
	displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
}

function onTaskSelected(parm1,parm2)
{
	fm.all("BaseTaskCode").value = TaskGrid.getRowColData(TaskGrid.getSelNo() - 1, 1);
	fm.all("TaskDescribe").value = TaskGrid.getRowColData(TaskGrid.getSelNo() - 1, 2);
	fm.all("TaskClass").value = TaskGrid.getRowColData(TaskGrid.getSelNo() - 1, 3);
}

function resetForm()
{
	fm.all("TaskPlanCode").value = "";
	fm.all("TaskCode").value = "";
	fm.all("RunFlag").value = "";
	fm.all("RecycleType").value = "";
	fm.all("StartTime").value = "";
	fm.all("EndTime").value = "";
	fm.all("Interval").value = "";
	fm.all("Times").value = "";

	fm.all("BaseTaskCode").value = "";
	fm.all("TaskDescribe").value = "";
	fm.all("TaskClass").value = "";
}

function refreshTask()
{
	initForm();
}

function taskPlanManage()
{
	divTaskPlan.style.display = "";
	divTaskPlanButton.style.display = "";
	divTask.style.display = "none";
	divTaskButton.style.display = "none";
	divTaskState.style.display = "none";
	divTaskStateButton.style.display = "none";
}

function taskManage()
{
	divTaskPlan.style.display = "none";
	divTaskPlanButton.style.display = "none";
	divTask.style.display = "";
	divTaskButton.style.display = "";
}
function StateManage()
{
	divTaskPlan.style.display = "none";
	divTaskPlanButton.style.display = "none";
	divTaskState.style.display = "";
	divTaskStateButton.style.display = "";
	
}