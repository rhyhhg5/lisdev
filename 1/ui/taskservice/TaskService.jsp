<%
//程序名称：TaskService.jsp
//程序功能：
//创建日期：2004-12-15 
//创建人  ：ZhangRong
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  	String CurrentDate= PubFun.getCurrentDate();   
    String AheadDays="-1";
    FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
    FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
 %>
<html> 
<script>
	function initDate(){
      fm.BeginDate.value="<%=afterdate%>";
      fm.AfterDate.value="<%=CurrentDate%>";
   }
</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>	
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <%@include file="TaskServiceInit.jsp"%>
  <SCRIPT src="TaskService.js"></SCRIPT>
  
</head>

<body  onload="initForm()" >
  <form action="./TaskServiceChk.jsp" method=post name=fm target="fraSubmit">
     
    <Div  id= "divTaskPlan" style= "display: ''">
      <table  class= common>
  		<TR>
  			<td class="titleImg">任务计划信息
  			</td>
  		</TR>
        <TR  class= common>
          <TD  class= title>
            任务计划编码
          </TD>
          <TD  class= input8>
            <Input class=common8 readonly name=TaskPlanCode >
          </TD>
          <TD  class= title8>
            基本任务编码
          </TD>
          <TD  class= code8>
            <Input class= codeNo name=TaskCode  CodeData = "" ondblclick="showCodeListEx('TaskCode',[this, TaskCodeName],[0,1],null,null,null,1,150);" onkeyup="showCodeListKeyEx('TaskCode',[this,TaskCodeName],[0,1],null,null,null,1,150);"><input class=codename name=TaskCodeName readonly=true >
          </TD>
        <TR  class= common>
          <TD  class= title8>
            是否启用
          </TD>
          <TD  class= code8>
            <Input class= codeNo name=RunFlag  CodeData = "0|^0|停用^1|启用" ondblclick="return showCodeListEx('RunFlag',[this,RunFlagName],[0,1],null,null,null,1,150);" onkeyup="return showCodeListEx('RunFlag',[this,RunFlagName],[0,1],null,null,null,1,150);"><input class=codename name=RunFlagName readonly=true >
          </TD>
          <TD  class= title8>
            循环类型
          </TD>
          <TD  class= code8>
            <Input class= codeNo name=RecycleType  CodeData = "0|^11|每分钟一次^21|每小时一次^31|每日一次^41|每周一次^51|每月一次^61|每年一次^71|一次^72|多次" ondblclick="return showCodeListEx('RecycleType',[this, RecycleTypeName],[0,1],null,null,null,1,150);" onkeyup="return showCodeListKeyEx('RecycleType',[this, RecycleTypeName],[0,1],null,null,null,1,150);"><input class=codename name=RecycleTypeName readonly=true >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起始时间             
			(YYYY-MM-DD HH:MM:SS)
          </TD>
          <TD  class= input8>  
          	<Input class=coolDatePicker style="width:90px" name=StartDate>&nbsp&nbsp
          	<Input class=common style="width:20px" name=StartH verify="起始时间(时)|len=2&INT">：
          	<Input class=common style="width:20px" name=StartM verify="起始时间(分)|len=2&INT">：
          	<Input class=common style="width:20px" name=StartS verify="起始时间(秒)|len=2&INT">
            <Input type=hidden name=StartTime >
          </TD>
          <TD  class= title8>
            终止时间             
			(YYYY-MM-DD HH:MM:SS)
          </TD>
          <TD  class= input8>
			<Input class=coolDatePicker style="width:90px" name=EndDate>&nbsp&nbsp
          	<Input class=common style="width:20px" name=EndH verify="终止时间(时)|len=2&INT">：
          	<Input class=common style="width:20px" name=EndM verify="终止时间(分)|len=2&INT">：
          	<Input class=common style="width:20px" name=EndS verify="终止时间(秒)|len=2&INT">
            <Input type=hidden name=EndTime >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            循环间隔
          </TD>
          <TD  class= input8>
            <Input class=common8  name=Interval >
          </TD>
          <TD  class= title8>
            循环次数
          </TD>
          <TD  class= input8>
            <Input class= common8  name=Times >
          </TD>
        </TR>
      </table>
      <table  class= common>
			   	<TR>
			   	</TR>
			   	<TR>
		    	  <TD text-align: left colSpan=1>
								<span id="spanTaskPlanGrid" > </span> 
					  </TD>
		   		</TR>
      </table>

			<Div id= "divPage" align=center style= "display: '' ">
		    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		  </Div>
      		
     
  		<TR>
  			<td class="titleImg">参数信息
  			</td>
  		</TR>
      <table  class= common>
	   	<TR>
	   	</TR>
	   	<TR>
    	  <TD text-align: left colSpan=1>
			<span id="spanParamGrid" > </span> 
		  </TD>
        </TR>
      </table>
    </Div>

    <input type= "hidden" name= "fmAction" value="">

	<Div id = "divTaskPlanButton">
	  <TR>
		  <INPUT VALUE="增加任务" class=cssButton TYPE=button name=addbutton onclick="appendOne();">
		  <INPUT VALUE="删除任务" class=cssButton TYPE=button name=delbutton onclick="deleteOne();">
		  <INPUT VALUE="启动任务" class=cssButton TYPE=button name=addbutton onclick="activateOne();">
		  <INPUT VALUE="停止任务" class=cssButton TYPE=button name=delbutton onclick="deactivateOne();">
		  <INPUT VALUE="启动服务" class=cssButton TYPE=button name=addbutton onclick="startEngine();">
		  <INPUT VALUE="停止服务" class=cssButton TYPE=button name=delbutton onclick="stopEngine();">
		  <INPUT VALUE="任务刷新" class=cssButton TYPE=button name=delbutton onclick="refreshTask();">
	  </TR>
	  <TR>
		  <INPUT VALUE="基本任务管理" class=cssButton TYPE=button name=delbutton onclick="taskManage();">
	  </TR>
	  <TR>
		  <INPUT VALUE="任务状态查询" class=cssButton TYPE=button name=delbutton onclick="StateManage();">
	  </TR>
	</Div>

    <Div  id= "divTask" style= "display: ''">
      <table  class= common>
  		<TR>
  			<td class="titleImg">任务信息
  			</td>
  		</TR>
        <TR  class= common>
          <TD  class= title8>
            基本任务编码
          </TD>
          <TD  class= common>
            <Input class= common readonly name=BaseTaskCode >
          </TD>
  		</TR>
        <TR  class= common>
          <TD  class= title8>
            任务描述
          </TD>
          <TD  class= code8>
            <Input class= common name=TaskDescribe >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            任务处理类             
          </TD>
          <TD  class= code8>
            <Input class= common name=TaskClass >
          </TD>
        </TR>
      </table>
      <table  class= common>
	   	<TR>
	   	</TR>
	   	<TR>
    	  <TD text-align: left colSpan=1>
			<span id="spanTaskGrid" > </span> 
		  </TD>
        </TR>
      </table>
		 <Div id= "divPage2" align=center style= "display: '' ">
		    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
		  </Div>
    </Div>
	<Div id = "divTaskButton">
	  <INPUT VALUE="增加任务" class=cssButton TYPE=button name=addbutton onclick="appendTask();">
	  <INPUT VALUE="删除任务" class=cssButton TYPE=button name=delbutton onclick="deleteTask();">
	  <INPUT VALUE="任务计划管理" class=cssButton TYPE=button name=delbutton onclick="taskPlanManage();">
    </Div> 
    
    <Div  id= "divTaskState" style= "display: ''">
      <table  class= common>
  		<TR>
  			<td class="titleImg">任务状态信息
  			</td>
  		</TR>
        <TR  class= common>
        	<TD  class= title >
							任务代码
						</TD>
							<TD  class= input> <Input name=StatusTaskCode class="code" MAXLENGTH=1 ondblclick="return showCodeList('TaskCode',[this]);" onkeyup="return showCodeListKey('TaskCode',[this]);" > </TD>       
						</TD>
          <TD  class= title8>
            起始日期
          </TD>
          <TD  class= common>
            <Input class=coolDatePicker style="width:90px" name=BeginDate>
          </TD>
  		
          <TD  class= title8>
            终止日期
          </TD>
          <TD  class= code8>
            <Input class=coolDatePicker style="width:90px" name=AfterDate>
          </TD>
        
          
        </TR>
      </table>
      <table  class= common>
	   	<TR>
	   	</TR>
	   	<TR>
    	  <TD text-align: left colSpan=1>
			<span id="spanTaskStateGrid" > </span> 
		  </TD>
        </TR>
      </table>
    </Div>
    
	<Div id = "divTaskStateButton">
		<Div id="divPage1" align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
	</Div>    
	  <INPUT VALUE="查询状态" class=cssButton TYPE=button name=addbutton onclick="QueryTask();">
	  <INPUT VALUE="返回" class=cssButton TYPE=button name=delbutton onclick="taskPlanManage();">
    </Div> 
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>
