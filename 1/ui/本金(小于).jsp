"select '' as TempFeeNo,
       '' as ActuGetNo,
       a.PayNo as PayNo,
       '' as EdorAcceptNo,
       '' as EndorsementNo,
       '' as CaseNo,
       'TXSQBF' as FeeOperationType,
       '01' as FeeFinaType,
       '2' as ListFlag,
       a.GrpContNo as GrpContNo,
       a.GrpPolNo as GrpPolNo,
       '' as ContNo,
       '' as PolNo,
       '' as PayIntvFlag,
       b.PayIntv as PayIntv,
       a.LastPayToDate as PayDate,
       '' as PayType,
       a.Enteraccdate as EnterAccDate,
       a.LastPayToDate as ConfDate,
       a.LastPayToDate as LastPayToDate,
       a.CurPayToDate as CurPayToDate,
       b.CValiDate as CValiDate,
       LF_GrpPolYear(a.GrpPolNo) as PolYear,
       'S' as FirstYearFlag,
       a.PayCount as PayCount,
       'S' as FirstTermFlag,
       LF_GrpYears(a.GrpPolNo) as Years,
       b.RiskCode as RiskCode,
       '' as RiskPeriod,
       '' as RiskType,
       '' as RiskType1,
       b.Salechnl as SaleChnl,
       a.Managecom as ManageCom,
       a.Managecom as ExecuteCom,
       a.Agentcom as AgentCom,
       a.Agentcode as AgentCode,
       a.Agentgroup as AgentGroup,
       '' as BankCode,
       '' as AccName,
       '' as BankAccNo,
       a.Sumduepaymoney as SumDueMoney,
       a.Sumactupaymoney as SumActuMoney,
       'R' as Currency,
       '' as PayMode,
       '' as OperationType,
       '' as Budget,
       b.SaleChnlDetail as SaleChnlDetail,
       b.customerno as StandByString1,
       '' as StandByString2,
       '' as StandByString3,
       0 as StandByNum1,
       0 as StandByNum2,
       a.makedate as StandByDate1,
       a.makedate as StandByDate2,
       LF_BClient('2', a.grpcontno) as BClient,
       LF_MarketType(a.grpcontno) as MarketType,
        '99' as PremiumType,
       (select substr(char(confdate), 1, 4)
          from ljapay
         where incomeno = c.incomeno
           and duefeetype = '0' fetch first 1 rows only) as FirstYear
  from LJAPayGrp a, LCGrpPol b, LJAPay c
 where a.Grppolno = b.Grppolno
   and a.riskcode in (select riskcode from lmriskapp where risktype3 = '7') 
   and a.Sumduepaymoney <> 0
   and not exists (select 1
          from licodetrans
         where codetype = 'TXRiskCode'
           and code = a.RiskCode)
   and c.payno = a.payno
   and c.incometype = '1'
   and c.DueFeeType in ('0', '2')
   and a.PayType = 'ZC'
   and a.ConfDate < a.LastPayToDate
   and not exists
 (select 1
          from LIDistillInfo
         where classid = '00000121'
           and KeyUnionValue = (trim(a.Payno) || ',' || trim(a.Grppolno)))
   and a.Managecom like '?ManageCom?%'
   and a.LastPayToDate >= '?StartDate?'
   and a.LastPayToDate <= '?EndDate?'
   and not exists
 (select 1
          from ljtempfeeclass
         where tempfeeno in
               ((select tempfeeno from ljtempfee where otherno = c.incomeno))
           and paymode = '13')
   and not exists (select 1
          from lcgrpcont
         where cardflag = '2'
           and grpcontno = c.incomeno
           and managecom like '8644%')
union all
select '' as TempFeeNo,
       '' as ActuGetNo,
       a.PayNo as PayNo,
       '' as EdorAcceptNo,
       '' as EndorsementNo,
       '' as CaseNo,
       'TXSQBF' as FeeOperationType,
       '01' as FeeFinaType,
       '2' as ListFlag,
       a.GrpContNo as GrpContNo,
       a.GrpPolNo as GrpPolNo,
       '' as ContNo,
       '' as PolNo,
       '' as PayIntvFlag,
       b.PayIntv as PayIntv,
       a.LastPayToDate as PayDate,
       '' as PayType,
       a.Enteraccdate as EnterAccDate,
       a.LastPayToDate as ConfDate,
       a.LastPayToDate as LastPayToDate,
       a.CurPayToDate as CurPayToDate,
       b.CValiDate as CValiDate,
       LF_GrpPolYear(a.GrpPolNo) as PolYear,
       'S' as FirstYearFlag,
       a.PayCount as PayCount,
       'S' as FirstTermFlag,
       LF_GrpYears(a.GrpPolNo) as Years,
       a.RiskCode as RiskCode,
       '' as RiskPeriod,
       '' as RiskType,
       '' as RiskType1,
       b.Salechnl as SaleChnl,
       a.Managecom as ManageCom,
       a.Managecom as ExecuteCom,
       a.Agentcom as AgentCom,
       a.Agentcode as AgentCode,
       a.Agentgroup as AgentGroup,
       '' as BankCode,
       '' as AccName,
       '' as BankAccNo,
       a.Sumduepaymoney as SumDueMoney,
       a.Sumactupaymoney as SumActuMoney,
       'R' as Currency,
       '' as PayMode,
       '' as OperationType,
       '' as Budget,
       b.SaleChnlDetail as SaleChnlDetail,
       b.customerno as StandByString1,
       '' as StandByString2,
       '' as StandByString3,
       0 as StandByNum1,
       0 as StandByNum2,
       a.makedate as StandByDate1,
       a.makedate as StandByDate2,
       LF_BClient('2', a.grpcontno) as BClient,
       LF_MarketType(a.grpcontno) as MarketType,
        '99' as PremiumType,
       (select substr(char(confdate), 1, 4)
          from ljapay
         where incomeno = c.incomeno
           and duefeetype = '0' fetch first 1 rows only) as FirstYear
  from LJAPayGrp a, LbGrpPol b, LJAPay c
 where a.Grppolno = b.Grppolno
   and a.riskcode in (select riskcode from lmriskapp where risktype3 = '7') 
   and a.Sumduepaymoney <> 0
   and not exists (select 1
          from licodetrans
         where codetype = 'TXRiskCode'
           and code = a.RiskCode)
   and c.payno = a.payno
   and c.incometype = '1'
   and c.DueFeeType in ('0', '2')
   and a.PayType = 'ZC'
   and a.ConfDate < a.LastPayToDate
   and not exists
 (select 1
          from LIDistillInfo
         where classid = '00000121'
           and KeyUnionValue = (trim(a.Payno) || ',' || trim(a.Grppolno)))
   and a.Managecom like '?ManageCom?%'
   and a.LastPayToDate >= '?StartDate?'
   and a.LastPayToDate <= '?EndDate?'
   and not exists
 (select 1
          from ljtempfeeclass
         where tempfeeno in
               ((select tempfeeno from ljtempfee where otherno = c.incomeno))
           and paymode = '13')
   and not exists (select 1
          from lcgrpcont
         where cardflag = '2'
           and grpcontno = c.incomeno
           and managecom like '8644%')
"
