Option Explicit
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Const SW_SHOW = 5
'定义开始和结束范围，行数
Const nStartRow = 1
Const nEndRow = 25535

Private Sub CommandButton1_Click()
     
     '撤销保护
     ActiveSheet.Unprotect "sino"
     '取消单元格的背景色
     Rows("3:999").Select
     Selection.Interior.ColorIndex = xlNone
     Selection.ClearComments
     
     Range("A3").Select
     
     
     Dim n As Integer
     Dim t As Integer
     Dim p As Integer
     Dim v As Integer
     Dim checkRows As Integer
     
     

     Dim key, keyNext As String
     
     t = 3
     v = 3
     checkRows = 3
     
     '删除LICertify和LICertifyInsured中历史数据


     Worksheets("LICertify").Cells.ClearContents
     Worksheets("LICertifyInsured").Cells.ClearContents
     
     Worksheets("LICertify").Cells.NumberFormatLocal = "@"
     Worksheets("LICertifyInsured").Cells.NumberFormatLocal = "@"

    '向sheet（Ver）中设置版本号：
    Worksheets("Ver").Cells(2, 1) = Trim(Range("B" & 1).Value)
     
     '获取要执行的文件总数
    Dim count As Integer
    Dim i As Integer
    Dim f As Integer
    Dim lastRow As Integer
    
    count = GetCurSheetRows(ActiveWorkbook.Sheets("单证导入清单"), 2)
    lastRow = count + 2
    'MsgBox "count = " & count
    If count = 0 Then
        MsgBox "没有要统计的记录，请核实！"
        Exit Sub
    Else
        MsgBox "记录总数为：" & count
    End If
    

    '排序
    Dim ws As Worksheet
    Dim myrange As Range
    Set ws = ActiveWorkbook.Sheets("单证导入清单")
    Set myrange = ws.UsedRange
    MsgBox "下面已第二列单证号排序"
    'myrange.Sort Key1:=ws.Range("B2"), order1:=xlDescending, Header:=xlYes, SortMethod:=xlStroke
    myrange.Sort Key1:=ws.Range("B2"), order1:=xlDescending, Header:=xlYes, SortMethod:=xlStroke
    Set myrange = Nothing
    Set ws = Nothing
     
    '开始校验
    '**************************************************************
    '**************************************************************
    
    '单证号
    Dim CardNo As String
    '单证类别
    Dim CertifyCode As String
    '管理机构
    Dim ManangeCom As String
    '生效日期
    Dim CValidate As String
    '保费
    Dim prem As String
    '保额
    Dim Amnt As String
    '档次
    Dim Mult As String
    '份数
    Dim Copys As String
    '销售渠道
    Dim SaleChnl As String
    '中介机构
    Dim agentCome As String
    '业务员代码
    Dim AgentCode As String
    '业务员姓名
    Dim AgentName As String
    
    
    '被保人姓名
    Dim InsuredName As String
    '性别
    Dim Sex As String
    '出生日期
    Dim InsuredBirthday As String
    '证件类别
    Dim IdCardType As String
    '证件号
    Dim IdNo As String
    '职业代码
    Dim OccupationCode As String
    '联系地址
    Dim Address As String
    '邮政编码
    Dim PostNO As String
    '家庭电话
    Dim HomeTel As String
    '移动电话
    Dim Mobile As String
    '工作单位
    Dim Company As String
    '办公电话
    Dim OfficeTel As String
    '电子邮箱
    Dim Email As String
    
    Dim flag As Integer
    flag = 0
    '客票号
    Dim TicketNo As String
    '班次
    Dim TeamNo As String
    '上车站
    Dim StartTeam As String
    '到达站
    Dim EndTeam As String
    '座位号
    Dim SeatNo As String
    '备注
    Dim Remark As String
    
    
    For f = checkRows To lastRow
        
        '设值
        CardNo = Trim(Range("B" & f).Value)
        CertifyCode = Trim(Range("C" & f).Value)
        ManangeCom = Trim(Range("D" & f).Value)
        CValidate = Trim(Range("E" & f).Value)
        prem = Trim(Range("F" & f).Value)
        Amnt = Trim(Range("G" & f).Value)
        Mult = Trim(Range("H" & f).Value)
        Copys = Trim(Range("I" & f).Value)
        SaleChnl = Trim(Range("J" & f).Value)
        agentCome = Trim(Range("K" & f).Value)
        AgentCode = Trim(Range("L" & f).Value)
        'AgentCode = Trim(Range("M" & f).Value)
        'AgentName = Trim(Range("N" & f).Value)
       
       
        InsuredName = Trim(Range("O" & f).Value)
        Sex = Trim(Range("P" & f).Value)
        InsuredBirthday = Trim(Range("Q" & f).Value)
        IdCardType = Trim(Range("R" & f).Value)
        IdNo = Trim(Range("S" & f).Value)
        OccupationCode = Trim(Range("T" & f).Value)
        Address = Trim(Range("U" & f).Value)
        PostNO = Trim(Range("V" & f).Value)
        HomeTel = Trim(Range("W" & f).Value)
        Mobile = Trim(Range("X" & f).Value)
        Company = Trim(Range("Y" & f).Value)
        OfficeTel = Trim(Range("Z" & f).Value)
        Email = Trim(Range("AA" & f).Value)
        TicketNo = Trim(Range("AB" & f).Value)
        TeamNo = Trim(Range("AC" & f).Value)
        StartTeam = Trim(Range("AD" & f).Value)
        EndTeam = Trim(Range("AE" & f).Value)
        SeatNo = Trim(Range("AF" & f).Value)
        Remark = Trim(Range("AG" & f).Value)
             
       
       'Dim flag As Integer
       Dim birthdayFlag As Integer
       'flag = 0
       birthdayFlag = 0
        '向Sheet（单证导入清单）中自动设置序列号
        ActiveWorkbook.Sheets("单证导入清单").Cells(f, 1) = f - 2
        
        
        '-------------------------------------------------------------
        '全角半角字符的转化功能，主要针对 份数、档次，证件类型，
        '证件号码，业务员代码，中介机构，销售渠道，职业代码
        '-------------------------------------------------------------
        
        '份数
        If Copys <> "" Then
            Copys = StrConv(Copys, vbNarrow)
            Worksheets("单证导入清单").Cells(f, 9) = Copys
        End If
        '档次
        If Mult <> "" Then
            Mult = StrConv(Mult, vbNarrow)
            Worksheets("单证导入清单").Cells(f, 8) = Mult
        End If
        '证件类型
        If IdCardType <> "" Then
            IdCardType = StrConv(IdCardType, vbNarrow)
            Worksheets("单证导入清单").Cells(f, 18) = IdCardType
        End If
        
        '证件号码
        If IdNo <> "" Then
            IdNo = StrConv(IdNo, vbNarrow)
            Worksheets("单证导入清单").Cells(f, 19) = IdNo
        End If
        
        '业务员代码
        If AgentCode <> "" Then
            AgentCode = StrConv(AgentCode, vbNarrow)
            Worksheets("单证导入清单").Cells(f, 12) = AgentCode
        End If
        '中介机构
        If agentCome <> "" Then
            agentCome = StrConv(agentCome, vbNarrow)
            Worksheets("单证导入清单").Cells(f, 11) = agentCome
        End If
        '销售渠道
         If SaleChnl <> "" Then
            SaleChnl = StrConv(SaleChnl, vbNarrow)
            Worksheets("单证导入清单").Cells(f, 10) = SaleChnl
        End If
        '职业代码
         If OccupationCode <> "" Then
            OccupationCode = StrConv(OccupationCode, vbNarrow)
            Worksheets("单证导入清单").Cells(f, 20) = OccupationCode
        End If
        
        '------------------------------------------------------------
        
        
        '管理机构不能为空
        If ManangeCom = "" Then
            Worksheets("单证导入清单").Cells(f, 4).Interior.ColorIndex = 39
            Worksheets("单证导入清单").Cells(f, 4).AddComment
            Worksheets("单证导入清单").Cells(f, 4).Comment.Text Text:="不能为空"
            flag = 1
        End If
        
        '销售渠道必须是06、02、03 三个值
        If SaleChnl <> "06" And SaleChnl <> "02" And SaleChnl <> "03" Then
            Worksheets("单证导入清单").Cells(f, 10).Interior.ColorIndex = 39
            Worksheets("单证导入清单").Cells(f, 10).AddComment
            Worksheets("单证导入清单").Cells(f, 10).Comment.Text Text:="值必须是06|02|03"
            flag = 1
        End If
         
        '----------------------------------------------------------
        
        '校验出生日期是否是8位
        
        '----------------------------------------------------------
        If InsuredBirthday <> "" Then
             If isRightDate(InsuredBirthday) = False Then
                Worksheets("单证导入清单").Cells(f, 17).Interior.ColorIndex = 39
                Worksheets("单证导入清单").Cells(f, 17).AddComment
                Worksheets("单证导入清单").Cells(f, 17).Comment.Text Text:="出生日期格式必须类似于20081210的格式"
                
                'Worksheets("单证导入清单").Cells(f, 17).Interior.ColorIndex = 39
                'Worksheets("单证导入清单").Cells(f, 17) = "出生日期格式必须类似于20081210的格式"
                flag = 1
                birthdayFlag = 1
            End If
             
        End If
        
        '-----------------------------------------------------------
        
        '校验生效日期格式是否正确,生效日期校验合格的情况下
        '被保人姓名，性别，出生日期，证件类别，证件号码不为空。
        
        '-----------------------------------------------------------
        If CValidate <> "" Then
             
             If isRightDate(CValidate) = False Then
                Worksheets("单证导入清单").Cells(f, 5).Interior.ColorIndex = 39
                Worksheets("单证导入清单").Cells(f, 5).AddComment
                Worksheets("单证导入清单").Cells(f, 5).Comment.Text Text:="日期格式必须类似于20081210的格式"
                
                'Worksheets("单证导入清单").Cells(f, 5).Interior.ColorIndex = 39
                'Worksheets("单证导入清单").Cells(f, 5) = "日期格式必须类似于20081210的格式"
                flag = 1
            
            '生效日期校验合格的情况下 被保人姓名，性别，出生日期，证件类别，证件号码不为空。
            Else
                '被保人姓名不能为空
                If InsuredName = "" Then
                     Worksheets("单证导入清单").Cells(f, 15).Interior.ColorIndex = 39
                     Worksheets("单证导入清单").Cells(f, 15).AddComment
                     Worksheets("单证导入清单").Cells(f, 15).Comment.Text Text:="因为生效日期不为空，所以被保人姓名不能为空"
    
                    'Worksheets("单证导入清单").Cells(f, 15).Interior.ColorIndex = 39
                    'Worksheets("单证导入清单").Cells(f, 15) = "因为生效日期不为空，所以被保人姓名不能为空"
                    flag = 1
                End If
                 '性别不能为空
                If Sex = "" Then
                    
                     Worksheets("单证导入清单").Cells(f, 16).Interior.ColorIndex = 39
                     Worksheets("单证导入清单").Cells(f, 16).AddComment
                     Worksheets("单证导入清单").Cells(f, 16).Comment.Text Text:="因为生效日期不为空，所以性别不能为空"
                    
                    'Worksheets("单证导入清单").Cells(f, 16).Interior.ColorIndex = 39
                    'Worksheets("单证导入清单").Cells(f, 16) = "因为生效日期不为空，所以性别不能为空"
                    flag = 1
                End If
                 '出生日期不能为空
                If InsuredBirthday = "" Then
                     Worksheets("单证导入清单").Cells(f, 17).Interior.ColorIndex = 39
                     Worksheets("单证导入清单").Cells(f, 17).AddComment
                     Worksheets("单证导入清单").Cells(f, 17).Comment.Text Text:="因为生效日期不为空，所以出生日期不能为空"
                    
                    'Worksheets("单证导入清单").Cells(f, 17).Interior.ColorIndex = 39
                    'Worksheets("单证导入清单").Cells(f, 17) = "因为生效日期不为空，所以出生日期不能为空"
                    flag = 1
                End If
                 '证件类别不能为空
                If IdCardType = "" Then
                     Worksheets("单证导入清单").Cells(f, 18).Interior.ColorIndex = 39
                     Worksheets("单证导入清单").Cells(f, 18).AddComment
                     Worksheets("单证导入清单").Cells(f, 18).Comment.Text Text:="因为生效日期不为空，所以证件类别不能为空"
                    
                    'Worksheets("单证导入清单").Cells(f, 18).Interior.ColorIndex = 39
                    'Worksheets("单证导入清单").Cells(f, 18) = "因为生效日期不为空，所以证件类别不能为空"
                    flag = 1
                End If
                
                 '证件号码不为空
                If IdNo = "" Then
                     Worksheets("单证导入清单").Cells(f, 19).Interior.ColorIndex = 39
                     Worksheets("单证导入清单").Cells(f, 19).AddComment
                     Worksheets("单证导入清单").Cells(f, 19).Comment.Text Text:="因为生效日期不为空，所以证件号码不能为空"
                    
                    'Worksheets("单证导入清单").Cells(f, 19).Interior.ColorIndex = 39
                    'Worksheets("单证导入清单").Cells(f, 19) = "因为生效日期不为空，所以证件号码不能为空"
                    flag = 1
                End If
                
            End If
        End If
        
        '----------------------------------------------------------
        
        '证件类别的校验，必须是0、1、2、3,如果是身份证，
        '则校验出生日期，以及性别是否合法
        
        '----------------------------------------------------------
        
        If IdCardType <> "" Then
            If IdCardType <> "0" And IdCardType <> "1" And IdCardType <> "2" And IdCardType <> "3" Then
                Worksheets("单证导入清单").Cells(f, 18).Interior.ColorIndex = 39
                Worksheets("单证导入清单").Cells(f, 18).AddComment
                Worksheets("单证导入清单").Cells(f, 18).Comment.Text Text:="证件类别必须是0|1|2|3|"
                
                'Worksheets("单证导入清单").Cells(f, 18).Interior.ColorIndex = 39
                'Worksheets("单证导入清单").Cells(f, 18) = "证件类别必须是0|1|2|3|"
                flag = 1
            
            Else
                 '若证件类别是0-身份证，则校验 出生日期，性别是否合法，不合法提示。
                If IdCardType = "0" Then
                    '判断身份证的位数，如果不是15位或者18位则出错
                    If chkGb(IdNo) <> 15 And chkGb(IdNo) <> 18 Then
                        Worksheets("单证导入清单").Cells(f, 19).Interior.ColorIndex = 39
                        Worksheets("单证导入清单").Cells(f, 19).AddComment
                        Worksheets("单证导入清单").Cells(f, 19).Comment.Text Text:="证件号码的位数不是15位或者18位"
                        
                        'Worksheets("单证导入清单").Cells(f, 19).Interior.ColorIndex = 39
                        'Worksheets("单证导入清单").Cells(f, 19) = "证件号码的位数不是15位或者18位"
                        flag = 1
                    
                    Else
                         '根据身份证校验出生日期是否合法
                        If birthdayFlag <> 1 Then
                            If chkBirthday(IdNo, InsuredBirthday) = False Then
                                Worksheets("单证导入清单").Cells(f, 17).Interior.ColorIndex = 39
                                Worksheets("单证导入清单").Cells(f, 17).AddComment
                                Worksheets("单证导入清单").Cells(f, 17).Comment.Text Text:="出生日期与身份证不符"
                                
                                'Worksheets("单证导入清单").Cells(f, 17).Interior.ColorIndex = 39
                                'Worksheets("单证导入清单").Cells(f, 17) = "出生日期与身份证不符"
                                flag = 1
                            End If
                        End If
                        
                        '根据身份证校验性别是否合法
                        If chkSex(IdNo, Sex) = False Then
                            Worksheets("单证导入清单").Cells(f, 16).Interior.ColorIndex = 39
                            Worksheets("单证导入清单").Cells(f, 16).AddComment
                            Worksheets("单证导入清单").Cells(f, 16).Comment.Text Text:="性别与身份证不符"
                            
                            'Worksheets("单证导入清单").Cells(f, 16).Interior.ColorIndex = 39
                            'Worksheets("单证导入清单").Cells(f, 16) = "性别与身份证不符"
                            flag = 1
                        End If
                    End If
                
                End If
            End If
        End If
        
        '-----------------------------------------------------------
        
        '证件号码， 家庭住址，提供最大长度的校验（根据数据库的最大字长）
        
        '----------------------------------------------------------
        '证件号码长度的校验
        Dim maxCardNo As Integer
        maxCardNo = 20
        If IdNo <> "" Then
            If chkGb(IdNo) > maxCardNo Then
                Worksheets("单证导入清单").Cells(f, 19).Interior.ColorIndex = 39
                Worksheets("单证导入清单").Cells(f, 19).AddComment
                Worksheets("单证导入清单").Cells(f, 19).Comment.Text Text:="证件号码的长度大于" & maxCardNo
                
                'Worksheets("单证导入清单").Cells(f, 19).Interior.ColorIndex = 39
                'Worksheets("单证导入清单").Cells(f, 19) = "证件号码的长度大于" & maxCardNo
                flag = 1
            End If
        End If
        
        '联系地址长度的校验
        Dim maxAdder As Integer
        maxAdder = 200
        If Address <> "" Then
            If chkGb(Address) > maxAdder Then
                Worksheets("单证导入清单").Cells(f, 21).Interior.ColorIndex = 39
                Worksheets("单证导入清单").Cells(f, 21).AddComment
                Worksheets("单证导入清单").Cells(f, 21).Comment.Text Text:="联系地址长度大于" & maxAdder
                
                'Worksheets("单证导入清单").Cells(f, 21).Interior.ColorIndex = 39
                'Worksheets("单证导入清单").Cells(f, 21) = "联系地址长度大于" & maxAdder
                flag = 1
            End If
        End If
        
        
        '------------------------------------------------------------
        '销售字段的校验：如果业务员代码不为空，则要求必须填写 销售渠道，
        '业务员代码是 ××01××××××  销售渠道必须填写 06
        '业务员代码是 ××02××××××   销售渠道必须填写 02
        '业务员代码是 ××03×××××× 销售渠道必须填写03,同时中介机构 字段不为空。
        '------------------------------------------------------------

        Dim rightSaleChnl As String

        '根据业务员代码返回正确的销售渠道
        rightSaleChnl = checkSaleChnl(AgentCode)
        
        If AgentCode <> "" Then
            '填写的销售渠道是否与返回的一致，不一致提示错误
            If rightSaleChnl <> "" And rightSaleChnl <> SaleChnl Then
                Worksheets("单证导入清单").Cells(f, 10).Interior.ColorIndex = 39
                Worksheets("单证导入清单").Cells(f, 10).AddComment
                Worksheets("单证导入清单").Cells(f, 10).Comment.Text Text:="销售渠道应该是：" & rightSaleChnl
                
                'Worksheets("单证导入清单").Cells(f, 10).Interior.ColorIndex = 39
                'Worksheets("单证导入清单").Cells(f, 10) = "销售渠道应该是：" & rightSaleChnl
                flag = 1
            End If
            
            '中介机构不为空
            If rightSaleChnl = "03" Then
                If agentCome = "" Then
                    Worksheets("单证导入清单").Cells(f, 11).Interior.ColorIndex = 39
                    Worksheets("单证导入清单").Cells(f, 11).AddComment
                    Worksheets("单证导入清单").Cells(f, 11).Comment.Text Text:="中介机构不为空"
                    
                    'Worksheets("单证导入清单").Cells(f, 11).Interior.ColorIndex = 39
                    'Worksheets("单证导入清单").Cells(f, 11) = "中介机构不为空"
                    flag = 1
                End If
            End If
        End If
       
    Next f
    
    If flag = 1 Then
        MsgBox "您所填写的数据当中有违法数据，请查看出现紫色单元格的批注说明，并进行修正。"
        flag = 0
        Exit Sub
    End If
    
    
    '校验结束
    '************************************************************************
    '************************************************************************
    
    
    
    '循环
    For i = nStartRow To lastRow
         
        
        '向Sheet（LICertify）中设置值
       If i = 1 Then
            Worksheets("LICertify").Cells(i, 1) = Trim(Range("A" & i).Value)
            Worksheets("LICertify").Cells(i, 2) = Trim(Range("B" & i).Value)
            
            Worksheets("LICertifyInsured").Cells(i, 1) = Trim(Range("A" & i).Value)
            Worksheets("LICertifyInsured").Cells(i, 2) = Trim(Range("B" & i).Value)
       End If
       
       '为以后添加校验用
       If i = 2 Then
            
            '添加表头
            Call writeToLICertify(i, i)
            Call writeToLICertifyInsured(i, i)
            
        End If
        
        If i > 2 Then
        
            '向Sheet（单证导入清单）中自动设置序列号
            'ActiveWorkbook.Sheets("单证导入清单").Cells(i, 1) = i - 2
            
            '判断是否是最后一行
            If i = lastRow Then
                
                '向 LICertify 写入数据
                Call writeToLICertify(v, i)
             
                 '向sheet（LICertifyInsured）中设置值
                 If (Trim(Range("O" & i).Value) <> "" And Trim(Range("P" & i).Value) <> "" And Trim(Range("Q" & i).Value) <> "" And Trim(Range("R" & i).Value) <> "" And Trim(Range("S" & i).Value) <> "") Or (Trim(Range("AC" & i).Value) <> "") Then
                   
                        Call writeToLICertifyInsured(t, i)
                    
                        t = t + 1
                  End If
                
            '不是最后一行
            Else
                '判断主键是否重复
                key = Trim(Worksheets("单证导入清单").Cells(i, 2))
                
                For p = i + 1 To lastRow
                    keyNext = Trim(Worksheets("单证导入清单").Cells(p, 2))
                    
                    'key值不重复的情况，直接输出
                    If key <> keyNext Then
                         
                        '向 LICertify 写入数据
                        Call writeToLICertify(v, i)
                        v = v + 1
                     
                         '向sheet（LICertifyInsured）中设置值
                         If (Trim(Range("O" & i).Value) <> "" And Trim(Range("P" & i).Value) <> "" And Trim(Range("Q" & i).Value) <> "" And Trim(Range("R" & i).Value) <> "" And Trim(Range("S" & i).Value) <> "") Or (Trim(Range("AC" & i).Value) <> "") Then
                           
                                Call writeToLICertifyInsured(t, i)
                            
                                t = t + 1
                          End If
                          '退出循环
                          Exit For

                        '=============================================================
                        '为今后扩展功能用
'                       Else
'                            'key值重复的情况，判断其他值是否相同，如果相同则只输出一条数据，不相同则报错，程序退出
'
'                            If Trim(Range("C" & i).Value) = Trim(Range("C" & p).Value) And _
'                                Trim(Range("D" & i).Value) = Trim(Range("D" & p).Value) And _
'                                Trim(Range("E" & i).Value) = Trim(Range("E" & p).Value) And _
'                                Trim(Range("F" & i).Value) = Trim(Range("F" & p).Value) And _
'                                Trim(Range("G" & i).Value) = Trim(Range("G" & p).Value) And _
'                                Trim(Range("H" & i).Value) = Trim(Range("H" & p).Value) And _
'                                Trim(Range("I" & i).Value) = Trim(Range("I" & p).Value) And _
'                                Trim(Range("J" & i).Value) = Trim(Range("J" & p).Value) And _
'                                Trim(Range("K" & i).Value) = Trim(Range("K" & p).Value) And _
'                                Trim(Range("L" & i).Value) = Trim(Range("L" & p).Value) And _
'                                Trim(Range("M" & i).Value) = Trim(Range("M" & p).Value) And _
'                                Trim(Range("N" & i).Value) = Trim(Range("N" & p).Value) _
'                            Then
'                                 '向sheet（LICertifyInsured）中设置值
'                                If Trim(Range("O" & i).Value) <> "" And Trim(Range("P" & i).Value) <> "" And Trim(Range("Q" & i).Value) <> "" And Trim(Range("R" & i).Value) <> "" And Trim(Range("S" & i).Value) <> "" Then
'
'                                    Call writeToLICertifyInsured(t, i)
'                                    t = t + 1
'                                End If
'
'                                Exit For
'
'                            Else
'                                '其他值不相同，程序退出
'                                MsgBox "单证号：" & key & "重复，但其他值却不相同，请确认！"
'
'                                'Worksheets("单证导入清单").Cells(i, 2).Interior.ColorIndex = 3
'                                'Worksheets("单证导入清单").Cells(p, 2).Interior.ColorIndex = 3
'
'                                Exit Sub
'                            End If
                       '=====================================================================
                       'key值重复直接退出
                       Else
                            MsgBox "单证号：" & key & "重复，请确认！"
                            Worksheets("单证导入清单").Cells(i, 2).Interior.ColorIndex = 3
                            Worksheets("单证导入清单").Cells(p, 2).Interior.ColorIndex = 3
                            Exit Sub
                       
                       End If
                    Next p
            End If
           
        End If
    Next i

    Sheets("LICertify").Visible = True
    Sheets("LICertifyInsured").Visible = True
    
    '重新设置保护状态
    Rows("1:2").Select
    ActiveSheet.Protect "sino"
    Range("A3").Select
    
End Sub







