var mOperate = "";
var showInfo;
var mPrintFlag = "";
window.onfocus = myonfocus;
var mode_flag;
var tAppealFlag = "0";
var turnPage = new turnPageClass();

//获得焦点
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}

// 查询
function Query() {
	if (!beforeSubmit()) {
		return false;
	}
	initAgentcodeGrid();
	var sql = "";

	sql = "select Agentcom,Name,Managecom,Bankcode,Endflag from lacom" +
	      " where Agentcom = '"+fm.all('Agentcom').value+"'";
	     /* +getWherePart('Name','Name')
	      +getWherePart('Managecom', 'Managecom')
	      +getWherePart('Bankcode', 'Bankcode')
	      +getWherePart('Endflag','Endflag');*/

	turnPage.queryModal(sql, AgentcodeGrid);
	afterSubmitl();
	return false;
}

function beforeSubmit() {
	if (fm.Agentcom.value == null || fm.Agentcom.value == '') {
		alert("请输入中介机构编码！");
		return false;
	}

	return true;
}

function submitForm() {

	var i = 0;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); 

}


function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ content;
		showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

	}
}

function afterSubmitl( FlagStr, content )
{
  showInfo.close();
  if (!turnPage.strQueryResult )
  {        
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "没有查询到数据" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
  }
  else
  { 
  	if(turnPage.strQueryResult){
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "查询成功" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
    } 
  }
}


function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}


function BankSelect() {
	alert('=========BankSelect()==========');
	var tSel = AgentcodeGrid.getSelNo();
	try {
		fm.all('Agentcom').value = AgentcodeGrid.getRowColData(tSel -1, 1);
		fm.all('Name').value = AgentcodeGrid.getRowColData(tSel -1, 2);
		
		fm.all('Managecom').value = AgentcodeGrid.getRowColData(tSel -1, 3);
		
		//fm.all('Upagentcom').value = AgentcodeGrid.getRowColData(tSel -1, 4);
		fm.all('Bankcode').value = AgentcodeGrid.getRowColData(tSel -1, 4);
		
	} catch (ex) {
		alert("读取数据错误,请刷新页面再使用。" + ex);
	}
}

// 添加
function Add(){
     alert("============添加=============");
	  if(!beforeSubmit())
	  {
		  return false;
	  }
	  	
	   if ((fm.all("Name").value == null)||(fm.all("Name").value == ''))
	    {
	      alert('请填写中介机构名称！');
	      return false;
	    }

	    if ((fm.all("Managecom").value == null)||(fm.all("Managecom").value == '')){
	    	alert('请填写管理机构！');
	    	return false;
	    }
	    
	    
	    	
	    
	 
	      fm.mOperate.value = "INSERT";
	      
	          var i = 0;
	          var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	          fm.action="./agentcodeSave.jsp";
	          fm.submit();
	    }

// 修改
function Update(){
	alert("=============修改==================");
	 if(!beforeSubmit()){
	    return false;
	  }
	  
	if (fm.Bankcode.value == null ||fm.Bankcode.value == "")
	{
		alert('请填写供应商编码！！！');
		return false;
	}
	
	if (fm.Managecom.value == null ||fm.Managecom.value == "")
	{
		alert('请填写管理机构！！！');
		return false;
	}
	
	fm.mOperate.value = "UPDATE";
	
		if (confirm("确定要修改该记录？"))
	    {
	            var i = 0;
	      	    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	      	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	      	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	      	  fm.action="./agentcodeSave.jsp";
	      	    fm.submit(); 
	      
	      }else
	    {
	        mOperate="";
	      alert("您取消了修改操作！");
	    } 
	  }



// 删除
function Delete(){
	  
  fm.mOperate.value = "DELETE";
	
  if (confirm("确定要删除该数据  ？"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    fm.action="./agentcodeSave.jsp";
    submitForm();
  }
  else
  {
     alert("请保存后再进行删除！！！");
  } 
	
}


