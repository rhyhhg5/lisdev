<%@page import="com.sinosoft.utility.*"%>
<script language="JavaScript">
  
// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        fm.all('manageCom').value = strManageCom;
        if(fm.all('manageCom').value != null)
        {
            var arrResult = easyExecSql("select Name from LDCom where ComCode = '" + strManageCom + "'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('manageComName').value=arrResult[0][0];
            }
        }

        //初始化生效日期
        var dateSql = "select Current Date from dual";
        var arr = easyExecSql(dateSql);
        if(arr){
            fm.all('dailyDate').value = arr[0][0];
        }
    }
    catch(ex)
    {
        alert("ContPrintDailyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initContPrintDailyGrid();
    }
    catch(re)
    {
        alert("ContPrintDailyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var ContPrintDailyGrid;

// 保单信息列表的初始化
function initContPrintDailyGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="管理机构";
        iArray[1][1]="60px";
        iArray[1][2]=60;
        iArray[1][3]=0;
        iArray[1][21]="manageCom";

        iArray[2]=new Array();
        iArray[2][0]="销售机构编码";
        iArray[2][1]="90px";
        iArray[2][2]=80;
        iArray[2][3]=0;
        iArray[2][21]="branchAttr";

        iArray[3]=new Array();
        iArray[3][0]="销售机构";
        iArray[3][1]="90px";
        iArray[3][2]=80;
        iArray[3][3]=0;
        iArray[3][21]="branchName";

        iArray[4]=new Array();
        iArray[4][0]="销售渠道";
        iArray[4][1]="60px";
        iArray[4][2]=60;
        iArray[4][3]=0;
        iArray[4][21]="saleChnl";

        iArray[5]=new Array();
        iArray[5][0]="业务员编码";
        iArray[5][1]="70px";
        iArray[5][2]=70;
        iArray[5][3]=0;
        iArray[5][21]="agentCode";

        iArray[6]=new Array();
        iArray[6][0]="业务员";
        iArray[6][1]="50px";
        iArray[6][2]=70;
        iArray[6][3]=0;
        iArray[6][21]="agentName";

        iArray[7]=new Array();
        iArray[7][0]="印刷号";
        iArray[7][1]="90px";
        iArray[7][2]=80;
        iArray[7][3]=0;
        iArray[7][21]="prtNo";

        iArray[8]=new Array();
        iArray[8][0]="合同号";
        iArray[8][1]="100px";
        iArray[8][2]=80;
        iArray[8][3]=0;
        iArray[8][21]="contNo";

        iArray[9]=new Array();
        iArray[9][0]="投保人";
        iArray[9][1]="70px";
        iArray[9][2]=80;
        iArray[9][3]=0;
        iArray[9][21]="appntName";

        iArray[10]=new Array();
        iArray[10][0]="保费";
        iArray[10][1]="60px";
        iArray[10][2]=80;
        iArray[10][3]=0;
        iArray[10][21]="prem";

        iArray[11]=new Array();
        iArray[11][0]="生效日期";
        iArray[11][1]="70px";
        iArray[11][2]=80;
        iArray[11][3]=0;
        iArray[11][21]="cValidate";

        iArray[12]=new Array();
        iArray[12][0]="签发日期";
        iArray[12][1]="70px";
        iArray[12][2]=80;
        iArray[12][3]=0;
        iArray[12][21]="signDate";

        iArray[13]=new Array();
        iArray[13][0]="打印日期";
        iArray[13][1]="70px";
        iArray[13][2]=80;
        iArray[13][3]=0;
        iArray[13][21]="printDate";

        iArray[14]=new Array();
        iArray[14][0]="打印时间";
        iArray[14][1]="70px";
        iArray[14][2]=80;
        iArray[14][3]=0;
        iArray[14][21]="printTime";

        ContPrintDailyGrid = new MulLineEnter( "fm" , "ContPrintDailyGrid" );
        //这些属性必须在loadMulLine前
        ContPrintDailyGrid.mulLineCount = 0;
        ContPrintDailyGrid.displayTitle = 1;
        ContPrintDailyGrid.hiddenPlus = 1;
        ContPrintDailyGrid.hiddenSubtraction = 1;
        ContPrintDailyGrid.canSel = 0;
        ContPrintDailyGrid.canChk = 0;
        ContPrintDailyGrid.loadMulLine(iArray);
        ContPrintDailyGrid.selBoxEventFuncName = "ContPrintDailyDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>