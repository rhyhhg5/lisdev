//程序名称：
//程序功能：
//创建日期：2011-02-10
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryWIIPolicyList()
{
    if(!verifyInput2())
    {
        return false;
    }

    var tStrSql = ""
        + " select "
        + " lwm.MissionProp1, lwm.MissionProp2, lwm.MissionProp3, lgc.InputDate, lgc.InputOperator, "
        + " lwm.MissionId, lwm.SubMissionId, lwm.ProcessId, lwm.ActivityId, lwm.ActivityStatus "
        + " from LWMission lwm "
        + " left join LCGrpCont lgc on lgc.PrtNo = lwm.MissionProp1 "
        + " where 1 = 1 "
        + " and lwm.ProcessId = '0000000012' "
        + " and lwm.ActivityId In ('0000012001') "
        + getWherePart("lwm.MissionProp1", "PrtNo")
        + getWherePart("lwm.MissionProp2", "ManageCom",'like')
        + getWherePart("lwm.MissionProp3", "ScanDate")
        ;
    
    turnPage1.pageDivName = "divWIIPolicyListGridPage";
    turnPage1.queryModal(tStrSql, WIIPolicyListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("未找到符合条件的保单信息！");
        return false;
    }
    
    return true;
}

/**
 * 进入卡折录入。
 */
function inputData()
{
    var tRow = WIIPolicyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = WIIPolicyListGrid.getRowData(tRow);
    
    var tPrtNo = tRowDatas[0];
    var tManageCom = tRowDatas[1];
    //var tScanDate = tRowDatas[2];
    //var tInputDate = tRowDatas[3];
    //var tInputOperator = tRowDatas[4];
    var tMissionId = tRowDatas[5];
    var tSubMissionId = tRowDatas[6];
    var tProcessId = tRowDatas[7];
    var tActivityId = tRowDatas[8];
    var tActivityStatus = tRowDatas[9];
    
    
    //校验印刷号是否符合规则
    if (tPrtNo != "") {
    	var srtPrtno = CheckPrtno(tPrtNo);
     	if (srtPrtno != "") {
     		alert(srtPrtno);
    	  return false;
     	}
	}
    
    if(!lockOperator(tPrtNo))
    {
        return false;
    }
    
    var tStrUrl = "./BriGrpContInputScanMain.jsp"
        + "?PrtNo=" + tPrtNo
        + "&MissionID=" + tMissionId
        + "&SubMissionID=" + tSubMissionId
        + "&ProcessID=" + tProcessId
        + "&ActivityID=" + tActivityId
        + "&ActivityStatus=" + tActivityStatus
        + "&ManageCom=" + tManageCom
        ;

    window.open(tStrUrl, "");
    
    WIIPolicyListGrid.clearData();
}

/**
 * 用户锁定
 */
function lockOperator(cPrtNo)
{
    var tState = "0";
    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    
    var urlStr = "../app/ProposalScanApply.jsp?prtNo=" + cPrtNo + "&operator=" + tOperator + "&state=" + tState;
    var strReturn = window.showModalDialog(urlStr, "", sFeatures);

    var strSql = "select * from ldsystrace where PolNo='" + cPrtNo + "' and PolState=1002 ";
    
    var arrResult = easyExecSql(strSql);
    if (arrResult != null && arrResult[0][1] != tOperator)
    {
        alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
        return false;
    }
    
    var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + cPrtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
    showModalDialog(urlStr, window, sFeatures);
    
    if (strReturn != "1")
    {
        return false;
    }
    
    return true;
}
