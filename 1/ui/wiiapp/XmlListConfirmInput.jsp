<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="XmlListConfirmInput.js"></script>
    <%@include file="XmlListConfirmInit.jsp" %>
</head>

<body onload="initForm();">
  
	<form action="" method="post" name="fm" target="fraSubmit">        
        <br />
        
        <table style="display:''">
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divCondCode);" />
                </td>
                <td class="titleImg">批次确认</td>
            </tr>
        </table>
        <div id="divCondCode" style="display:''">
            <table class="common">
            	<tr class="common">
                    <TD class= title>管理机构</TD>
                    <td class= input><Input class="codeno" name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><Input class=codename readonly name=ManageComName></td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                <tr class="common">
                    <td class="title">批次号</td>
                    <td class="input">
                        <input class="common" name="BatchNo"  />
                    </td>
                    <TD class= title>导入日期起期</TD>
                	<TD class= input><input class="coolDatePicker" name="importStartDate" /></TD>
                	<TD class= title>导入日期止期</TD>
                	<TD class= input><input class="coolDatePicker" name="importEndDate" /></TD>
                    
                </tr>
            </table>
        </div>

        <table style="display:''">
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryBatchList();" />
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnConfirm" name="btnConfirm" value="确  认" onclick="submitForm();" />
                </td>
            </tr>
        </table>
        
        <br />
        <div id="divBatchInfo" style="display:'none'">
            <table class="common">
                <tr class="common">
                    <td class="title">批次号</td>
                    <td class="input">
                        <input class="common" name="ConfirmBatchNo" readonly="readonly" />
                    </td>
                    <td class="title"></td>
                    <td class="input">
                    </td>
                    <td class="title"></td>
                    <td class="input">
                    </td>
                </tr>
            </table>
        </div>
        <div align="center" id="divConfirmBatchGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanConfirmBatchGrid"></span> 
                    </td>
                </tr>
            </table>
            <div id="divConfirmBatchGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" />
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" />
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />
            </div>
        </div>  
        
        <div id="divImportCondCode" style="display:''">
            <!-- Mulline Code -->
            <table>
                <tr>
                    <td class="common">
                        <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divConfirmLogGrid)" />
                    </td>
                    <td class="titleImg">批次确认日志</td>
                </tr>
            </table>
            <div align="center" id="divConfirmLogGrid" style="display: ''">
                <table class="common">
                    <tr class="common">
                        <td>
                            <span id="spanConfirmLogGrid"></span> 
                        </td>
                    </tr>
                </table>
                
                <div id="divConfirmLogGridPage" style="display: ''" align="center">
                    <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                    <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                    
                    <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                    <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                   
                </div>
                
            </div>
            <table>
                <tr>
                    <td>
                    	<input type="hidden" class="cssButton" value="下载清单" onclick="downloadList();">
                    </td>
                </tr>
            </table>
            
            <!-- Mulline Code End -->
        </div>      
        <!--input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value="返  回" onclick="goBack();" -->
        <input type="hidden" id="querySql" name="querySql" />
        <input type="hidden" id="fmOperatorFlag" name="fmOperatorFlag" />
    </form>
    
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
