<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2011-08-09
//创建人  ：zhangyang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="WIIPolicyQueryInput.js"></script>
    <%@include file="WIIPolicyQueryInputInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">请输入查询条件：</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">印刷号</td>
                    <td class="input8">
                        <input class="common" name="PrtNo" />
                    </td>
                    <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title8">业务员代码</td>
                	<td class="input8">
          				<input class=codeNo name=AgentCode  ondblclick="return showCodeList('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
          			</td>
          			<td class="title8" style="display:'none'">
			            业务员组别
			        </td>
			        <td class="input8" style="display:'none'">
			          	<input class="codeNo" name=AgentGroup verify="业务员组别|code:branchcode" ondblclick="return showCodeList('agentgroup2',[this,AgentGroupName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('agentgroup2',[this,AgentGroupName],[0,1]);"><input class=codename name=AgentGroupName readonly=true >
			        </td> 
                </tr>
                <tr class="common">
          			<td class="title8">投保起始日期</td>
		            <td class="input8">
		                <input class="coolDatePicker" dateFormat="short" name="StartPolApplyDate" verify="投保起始日期|date" />
		            </td>
		            <td class="title8">投保终止日期</td>
		            <td class="input8">
		                <input class="coolDatePicker" dateFormat="short" name="EndPolApplyDate" verify="投保终止日期|date" />
		            </td>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input type="hidden" class="common" name="querySql" >
                    <input class="cssButton" type="button" value="查  询" onclick="queryWIIPolicyList();" />
                </td>
                <td class="common">
                    <input class="cssButton" type="button" value="下  载" onclick="downLoadWIIPolicyList();" />
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divWIIPolicyListGrid);" />
                </td>
                <td class="titleImg">保单信息</td>
            </tr>
        </table>
        <div id="divWIIPolicyListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanWIIPolicyListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divWIIPolicyListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnInput" value="团体工伤险投保单明细" onclick="queryData();" />
                </td>
            </tr>
        </table>
        
        <input type="hidden" class="common" name="ActivityID" value="" />
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
