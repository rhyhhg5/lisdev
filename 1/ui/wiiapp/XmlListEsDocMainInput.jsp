<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="XmlListEsDocMainInput.js"></script>
    <%@include file="XmlListEsDocMainInit.jsp" %>
</head>

<body onload="initForm();">
  
	<form action="" method="post" name="fm" target="fraSubmit">        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divEsDocMainCondCode);" />
                </td>
                <td class="titleImg">未上传扫描件的保单查询</td>
            </tr>
        </table>
        
        <div id="divEsDocMainCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                	<TD class= title>管理机构</TD>
                	<TD class= input><input class="codeNo" name=manageCom ondblclick="return showCodeList('ComCode',[this,manageComName],[0,1],null,null,null,1);" ><input class=codename name=manageComName readonly ></TD>
                    <td class="title">导入批次号</td>
                    <td class="input">
                        <input class="common" name="EsDocMainBatchNo" />
                    </td>
                    <TD class= title>业务号</TD>
                	<TD class= input>
                		<input class="common" name="EsDocMainPrtNo" />
                	</TD>
                </tr>
                <tr class="common">
                	
                    <td class="title">保单号</td>
                    <td class="input">
                        <input class="common" name="EsDocMainContNo" />
                    </td>
                    <TD class= title>导入日期起期</TD>
                	<TD class= input><input class="coolDatePicker" name="importStartDate" /></TD>
                	<TD class= title>导入日期止期</TD>
                	<TD class= input><input class="coolDatePicker" name="importEndDate" /></TD>
                </tr>
            </table>

            <table>
                <tr>
                    <td class="common">
                    	<input type="hidden" class="common" name="querySql" >
                        <input class="cssButton" type="button" value="查  询" onclick="queryEsDocMain();" />
                    </td>
                    <td>
                    	<input type="button" class="cssButton" value="下载清单" onclick="downloadList();">
                    </td>
                </tr>
            </table>
            
            <!-- Mulline Code -->
            <table>
                <tr>
                    <td class="common">
                        <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divEsDocMainGrid);" />
                    </td>
                    <td class="titleImg">未上传扫描件的保单</td>
                </tr>
            </table>
            <div align="center" id="divEsDocMainGrid" style="display: ''">
                <table class="common">
                    <tr class="common">
                        <td>
                            <span id="spanEsDocMainGrid"></span> 
                        </td>
                    </tr>
                </table>
                
                <div id="divEsDocMainGridPage" style="display: ''" align="center">
                    <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                    <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                    
                    <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                    <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                   
                </div>
            </div>
            
            
            <!-- Mulline Code End -->
        </div>
        
        <hr />
        
        <br />
        <!--input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value="返  回" onclick="goBack();" -->
        
        <input type="hidden" id="fmOperatorFlag" name="fmOperatorFlag" />
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
