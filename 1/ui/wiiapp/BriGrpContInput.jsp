<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<html>
<script>
    var PrtNo = "<%=request.getParameter("PrtNo")%>";
    var scantype = "<%=request.getParameter("scantype")%>";
    
    var MissionID = "<%=request.getParameter("MissionID")%>";
    var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
    var ProcessID = "<%=request.getParameter("ProcessID")%>";
    var ActivityID = "<%=request.getParameter("ActivityID")%>";
    var ActivityStatus = "<%=request.getParameter("ActivityStatus")%>";
    var QueryFlag = "<%=request.getParameter("QueryFlag")%>";
    
    var ManageCom = "<%=request.getParameter("ManageCom")%>";
    var PolApplyDate = "<%=request.getParameter("PolApplyDate")%>";
    var LoadFlag = "<%=request.getParameter("LoadFlag")%>";
    var CurrentDate = "<%=PubFun.getCurrentDate()%>";
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <%@include file="BriGrpContInputInit.jsp"%>
    <script src="BriGrpContInput.js"></script>
</head>

<body  onload="initForm();initElementtype();" >
    <form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">保单信息</td>
            </tr>
        </table>
        
        <div id="divPolicyCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">印刷号</td>
                    <td class="input">
                        <input class="common" name="PrtNo" verify="印刷号|notnull" readonly="readonly" />
                    </td>
                    <td class="title">管理机构</td>
                    <td class="input">
                        <input class="codeno" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                <tr class="common">
                    <td class="title">销售渠道</td>
                    <td class="input">
                        <!--<input class="codeno" name="SaleChnl" verify="销售渠道|notnull" ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1],null,'1 and Code in (#02#, #03#,#14#,#15#)','1',1);" onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1],null,'1 and Code in (#02#, #03#,#14#,#15#)','1',1);" readonly="readonly" /><input class="codename" name="SaleChnlName" readonly="readonly" elementtype="nacessary" />-->
                        <Input class=codeNo name=SaleChnl verify="销售渠道|notnull" ondblclick="return showCodeList('unitesalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('unitesalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" readonly="readonly"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>
                    </td>
                    <td class="title">市场类型</td>
                    <td class="input">
                        <input class="codeno" name="MarketType" verify="市场类型|code:markettype&notnull" ondblclick="return showCodeList('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="MarketTypeName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                
                <tr>
           		 <td colspan="6"><font color="black">如果是交叉销售,请选择</font><INPUT TYPE="checkbox" NAME="MixComFlag" onclick="isMixCom();"></td>
        		</tr>
        		<%@include file="../sys/MixedSalesAgent.jsp"%>
                <tr class="common">
                    <td class="title">中介公司代码</td>
                    <td class="input">
                        <input class="code" name="AgentCom" ondblclick="return showCodeList('AgentComInput',[this],[0],null,fm.all('AgentVar').value,'branchtype',1);" onkeyup="return showCodeListKey('AgentComInput',[this],[0],null,fm.all('AgentVar').value,'branchtype',1);" readonly="readonly" />
                        <input class="code" name="AgentCom2" ondblclick="return showCodeList('agentcombank',[this,AgentCom],[0,0],null, fm.all('ManageCom').value, 'ManageCom',1);" onkeyup="return showCodeListKey('agentcombank',[this,AgentCom],[0,0],null, fm.all('ManageCom').value, 'ManageCom',1);" style="display: none">
                    </td>
                    <td class="title">业务员代码</td>
                    <td class="input">
                        <input class="code" name="GroupAgentCode" verify="业务员代码|notnull" ondblclick="qryAgentList();" readonly="readonly" elementtype="nacessary" />
                        <input class="code" name="AgentCode" verify="业务员代码|notnull" ondblclick="qryAgentList();" readonly="readonly" type="hidden" />
                    </td>
                    <td class="title">业务员姓名</td>
                    <td class="input">
                        <input class="readonly" name="AgentName" readonly="readonly" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title">投保单申请日期</td>
                    <td class="input">
                        <input class="coolDatePicker" dateFormat="short" name="PolApplyDate" verify="投保单申请日期|notnull&date" elementtype="nacessary" />
                    </td>
                    <td class="title">业务经办人</td>
                    <td class="input">
                        <input class="common" name="HandlerName" verify="业务经办人|notnull" elementtype="nacessary"/>
                    </td>
                    <td class="title">初审人员</td>
                    <td class="input">
                        <input class="codeno" name="FirstTrialOperator" verify="初审人员|code:firsttrialoperator&notnull" ondblclick="return showCodeList('FirstTrialOperator',[this,FirstTrialOperatorName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('FirstTrialOperator',[this,FirstTrialOperatorName],[0,1],null,null,null,1);" /><input class="codename" name="FirstTrialOperatorName" readonly="readonly" elementtype="nacessary" />
                    </td>
                </tr>
                <tr>
            <td colspan="6"><font color="black">综合开拓标示</font><INPUT TYPE="checkbox" NAME="ExtendFlag" onclick="isExtend();"></td>
        </tr>
        <tr class="common8" id="ExtendID" style="display: none">
            <td class="title8">协助销售渠道</td>
            <td class="input8">
                <input class="codeNo" name="AssistSaleChnl" id="AssistSaleChnl" verify="协助销售渠道|code:AssistSaleChnl" ondblclick="return showCodeList('AssistSaleChnl',[this,AssistSalechnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AssistSaleChnl',[this,AssistSalechnlName],[0,1],null,null,null,1);" /><input class="codename" name="AssistSalechnlName" readonly="readonly" elementtype=nacessary/>
            </td>
            <td CLASS="title">协助销售人员代码</td>
    		<td CLASS="input" COLSPAN="1">
			<input NAME="AssistAgentCode" VALUE MAXLENGTH="20" CLASS="code" elementtype=nacessary ondblclick="return queryAssistAgent();" >
    		</td>
            <td  class="title" >协助销售人员姓名</td>
	        <td  class="input" COLSPAN="1">
	            <Input  name=AssistAgentName CLASS="common" elementtype=nacessary readonly>
	        </td>
        </tr>
            </table>
        </div>
        
        <hr />
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">投保单位信息</td>
            </tr>
        </table>
        
        <div id="divGrpAppntCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">客户编码</td>
                    <td class="input">
                        <input class="common" name="CustomerNo" />
                        <input class="cssButton" type="button" id="btnQryGrpInfo" name="btnQryGrpInfo" value="查  询" onclick="qryGrpInfoView();" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
            <table class="common">
                <tr class="common">
                    <td class="title">投保单位名称</td>
                    <td class="input">
                        <input class="common" name="GrpName" verify="投保单位名称|notnull" onblur="qryGrpInfoList();" elementtype="nacessary" />
                    </td>
                    <td class="title">组织机构代码</td>
                    <td class="input">
                        <input class="common" name="OrgancomCode"  />
                    </td>
                    <td class="title">统一社会信用代码</td>
                    <td class="input">
                        <input class="common" name="UnifiedSocialCreditNo" verify="统一社会信用代码|len=18" />
                    </td>
                </tr>
                <tr class="common">
                	<td class="title">保险证编码/社保登记编号</td>
                    <td class="input">
                        <input class="common" name="OtherCertificates" verify="保险证编码/社保登记编号|notnull" elementtype="nacessary" />
                    </td>
                    
                    <td class="title">邮政编码</td>
                    <td class="input">
                        <input class="common" name="GrpZipCode" verify="邮政编码|notnull" elementtype="nacessary" />
                    </td>
                </tr>
                <tr>
                	<td class="title">投保单位地址</td>
                    <td class="input" colspan="3">
                        <input class="common3" name="GrpAddress" verify="投保单位地址|notnull" elementtype="nacessary" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                	<td  class= title8>省（自治区直辖市）</td>
          			<td  class= input8>
      					<Input name=ProvinceID class=codeNo ondblclick="return showCodeList('Province1',[this,Province],[0,1],null,'0','Code1',1);"  onkeyup="return showCodeListKey('Province1',[this,Province],[0,1],null,'0','Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=Province class= codeName  elementtype=nacessary readonly="readonly">
         			</td>
          			<td  class= title8>市</td>
          			<td  class= input8>
           			   <Input name=CityID class=codeNo ondblclick="return showCodeList('City1',[this,City],[0,1],null,fm.ProvinceID.value,'Code1',1);"  onkeyup="return showCodeListKey('City1',[this,City],[0,1],null,fm.ProvinceID.value,'Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=City class= codeName  elementtype=nacessary readonly="readonly">
         			</td>
         			<TD  class= title8>县</TD>
					<TD  class= input8>
					    <Input name=CountyID class=codeNo ondblclick="return showCodeList('County1',[this,County],[0,1],null,fm.CityID.value,'Code1',1);"  onkeyup="return showCodeListKey('County1',[this,County],[0,1],null,fm.CityID.value,'Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=County class= codeName  elementtype=nacessary readonly="readonly">
					</TD>
          			<td  class= title8>详细地址</td>
          			<td  class= input8>
      					<Input class= common8 name=DetailAddress elementtype=nacessary onblur="FillAddress()">
         			</td>
                </tr>
                <tr class="common">
                    <td class="title">联系人</td>
                    <td class="input">
                        <input class="common" name="LinkMan1" verify="联系人|notnull" elementtype="nacessary" />
                    </td>
                    <td class="title">联系电话</td>
                    <td class="input">
                        <input class="common" name="Phone1" verify="联系电话|notnull" elementtype="nacessary" />
                    </td>
                    <td class="title">手机</td>
                    <td class="input">
                        <input class="common" name="Mobile1"  />
                    </td>
                    
                    <td class="title">电子邮箱</td>
                    <td class="input">
         				<Input class= common8 name=E_Mail1 verify="保险联系人一E-MAIL|Email">               
					</td>
                </tr>
				<tr class="common">
                    <td class="title">纳税人类型</td>
                    <td class="input">
						<Input class=codeno name=TaxpayerType verify="纳税人类型|code:GrpTaxpayerType" ondblclick="return showCodeList('GrpTaxpayerType',[this,TaxpayerTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('GrpTaxpayerType',[this,TaxpayerTypeName],[0,1],null,null,null,1);"><input class=codename name=TaxpayerTypeName readonly=true > 		     		
                    </td>
                    <td class="title">税务登记证号码</td>
                    <td class="input">
        		      	<input class="common" name="TaxNo"/>
                    </td>
                    <td class="title">客户开户银行</td>
                    <td class="input">
						<Input name=CustomerBankCode class=codeNo ondblclick="return showCodeList('bank',[this,CustomerBankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bank',[this,CustomerBankCodeName],[0,1],null,null,null,1);"><input class=codename name=CustomerBankCodeName readonly=true >  
					</td>
                </tr>
				<tr class="common">
                    <td class="title">客户银行账户</td>
                    <td class="input">
					 	<Input class="common8" name=CustomerBankAccNo verify="客户银行账户|NUM">
                    </td>
                </tr>
            </table>
        </div>
        
        <hr />
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">参保人员情况</td>
            </tr>
        </table>
        
        <div id="divPoliInsuInfoCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">被保险人数（成员）</td>
                    <td class="input">
                        <input class="common" name="Peoples3" elementtype="nacessary" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                <tr class="common">
                    <td class="title">在职人数</td>
                    <td class="input">
                        <input class="common" name="OnWorkPeoples" />
                    </td>
                    <td class="title">退休人数</td>
                    <td class="input">
                        <input class="common" name="OffWorkPeoples" />
                    </td>
                    <td class="title">其他人员人数</td>
                    <td class="input">
                        <input class="common" name="OtherPeoples" />
                    </td>
                </tr>
            </table>
        </div>
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">缴费资料</td>
            </tr>
        </table>
        
        <div id="divPoliPayInfoCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">缴费方式</td>
                    <td class="input">
                        <input class="codeNo" name="PayMode" verify="缴费方式|notnull" CodeData="0|^1|现金^3|转帐支票^6|pos机" ondblclick="return showCodeListEx('paymode',[this,PayModyName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('paymode',[this,PayModyName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="PayModyName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title">缴费频次</td>
                    <td class="input">
                        <input class="codeNo" name="PayIntv" verify="缴费频次|notnull" ondblclick="return showCodeList('payintv',[this,PayIntvName],[0,1],null,'1 and Code in (#0#)','1',1);" onkeyup="return showCodeListKey('payintv',[this,PayIntvName],[0,1],null,'1 and Code in (#0#)','1',1);" readonly="readonly" /><input class="codename" name="PayIntvName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title">保费合计</td>
                    <td class="input">
                        <input class="common" name="PremScope" verify="保费合计|notnull&num" elementtype="nacessary" />
                    </td>
                </tr>
            </table>
        </div>
        
        <br />
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">保险责任</td>
            </tr>
        </table>
        
        <div id="divContInfoCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">保险责任生效日期</td>
                    <td class="input">
                        <input class="coolDatePicker" dateFormat="short" name="CValiDate" verify="保险责任生效日期|notnull&date" elementtype="nacessary" />
                    </td>
                    <td class="title">保险责任终止日期</td>
                    <td class="input">
                        <input class="coolDatePicker" dateFormat="short" name="CInValiDate" verify="保险责任终止日期|notnull&date" elementtype="nacessary" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>
        
        <br />
        
        <hr />
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">备注栏</td>
            </tr>
        </table>
        
        <div id="divContRemarkInfoCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">特别约定</td>
                </tr>
                <tr>
                    <td class="input" colspan="3"><textarea class="common3" name="Remark" rows="4"></textarea></td>
                </tr>
            </table>
        </div>
        
        <hr />
        
        <br />
        
        <table align="right">
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnCreateGrpCont" name="btnCreateGrpCont" value=" 保  存 " onclick="saveSGrpContInfo();" />   
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnEditGrpCont" name="btnEditGrpCont" value=" 修  改 " onclick="alert('暂不开放该功能');" disabled="disabled" style="display: none;" />
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value=" 下一步 " onclick="goNext();" />
                </td>
            </tr>
        </table>
        
        <input type="hidden" id="GrpContNo" name="GrpContNo" />
        <input type="hidden" id="AgentVar" name="AgentVar" />
        
        <input type="hidden" id="ProcessID" name="ProcessID" />
        <input type="hidden" id="MissionID" name="MissionID" />
        <input type="hidden" id="SubMissionID" name="SubMissionID" />
        <input type="hidden" id="ActivityID" name="ActivityID" />
        <input type="hidden" id="ActivityStatus" name="ActivityStatus" />
        <input type="hidden" id="QueryFlag" name="QueryFlag" />

    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>