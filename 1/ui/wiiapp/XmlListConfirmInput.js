//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 导入清单提交后动作。
 */
function afterConfirmBatch(FlagStr, Content, cBatchNo)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //alert("批次确认失败。");
        fm.BatchNo.value = cBatchNo;
        queryBatchList();
        ConfirmBatchGrid.radioBoxSel(1);
        queryConfirmLog();
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        fm.BatchNo.value = cBatchNo;
        queryBatchList();
        ConfirmBatchGrid.radioBoxSel(1);
        queryConfirmLog();
    }
    
    fm.btnConfirm.disabled = false;
}

function queryBatchList()
{
	if(!checkData()){
		return false;
	}
    var tBatchNo = fm.BatchNo.value;
    var tStrSql = ""
        + " select lbibi.BatchNo, lbibi.BranchCode, lbibi.SendOperator,lbibi.TaskCount,"
        +"(select codename from ldcode where codetype = 'wiiimportstate' and code = lbibi.ImportState),"
        +"ImportStartDate,ImportEndDate,"
        +"(select codename from ldcode where codetype = 'wiiconfstate' and code = lbibi.BatchConfState),"
        +"lbibi.BatchConfDate, varchar(MakeDate) || ' '|| MakeTime "
        + " from lcbrigrpimportbatchinfo lbibi "
        + " where 1 = 1 "
        //+ " and BatchConfState = '00'" 
        //+ getWherePart('ManageCom', 'manageCom', 'like')
        + getWherePart('importStartDate', 'importStartDate',">=")
        + getWherePart('importEndDate', 'importEndDate',"<=")
        + getWherePart('BatchNo', 'BatchNo')
        + " order by varchar(MakeDate) || ' '|| MakeTime desc"
        ;
//prompt('',tStrSql);
    turnPage1.pageDivName = "divConfirmBatchGridPage";
    turnPage1.queryModal(tStrSql, ConfirmBatchGrid);

    if (!turnPage1.strQueryResult)
    {
        alert("没有需确认批次信息！");
        return false;
    }else  	
    //fm.querySql.value = tStrSql;
    return true;
}

//查询前的校验
function checkData()
{
	var tBatchNo = fm.BatchNo.value; 					//导入批次号
	var tImportStartDate = fm.all('importStartDate').value;	//导入日期起期
	var tImportEndDate = fm.all('importEndDate').value;		//导入日期止期
	
	if(tBatchNo == null || tBatchNo == ""){
		if(tImportStartDate == null || tImportStartDate == "")
		{
			alert("请选择导入日期起期！");
			return false;
		}
	
	if(tImportEndDate == null || tImportEndDate == "")
		{
			alert("请选择导入日期止期！");
			return false;
		}
		
		
	if(tImportStartDate != null && tImportStartDate != ""
		&& tImportEndDate != null && tImportEndDate != "")
		{
			if(dateDiff(tImportStartDate,tImportEndDate,"M")>3)
			{
				alert("导入日期统计期最多为三个月！");
				return false;
			}
		}
	}	
	return true;
}

/**
 * 提交表单
 */
function submitForm()
{
   //var tRow = ConfirmBatchGrid.getSelNo() - 1;
    //if(tRow == null || tRow < 0)
    //{
        //alert("请选择一条记录。");
        //return false;
    //}
    if(!beforeSubmit()){
    	return false;
    }
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.action = "./XmlListConfirmSave.jsp?BatchNo="+fm.BatchNo.value;
    fm.submit(); //提交
}

function BatchNoShow()
{
    queryConfirmLog();
}

function queryConfirmLog()
{
    var tBatchNo = ConfirmBatchGrid.getRowColData(ConfirmBatchGrid.getSelNo() - 1, 1);
    var tStrSql = ""
        + " select lbid.BatchNo,lbid.BussNo,"
        + " (select codename from ldcode where codetype = 'wiiimportstate' and code = lbid.ImportState),"
        + " lbid.ImportDate,(select codename from ldcode where codetype = 'wiiconfstate' and code = lbid.ConfState),lbid.ConfDate,"
        + " (select codename from ldcode where codetype = 'wiidealflag' and code = lbid.DealFlag),"
        + " lbid.DealRemark,lbid.GrpContNo,lbid.GrpName,lbid.OrgancomCode,lbid.CValiDate,lbid.CInValidate,lbid.SignDate,lbid.ConfMakeDate,lbid.Prem"
        + " from lcbrigrpimportdetail lbid"
        + " where 1 = 1 "
        + " and BatchNo = '" + tBatchNo + "' "
        + " order by varchar(MakeDate) || ' '|| MakeTime desc "
        ;
//prompt('',tStrSql);
    turnPage2.pageDivName = "divConfirmLogGridPage";
    turnPage2.queryModal(tStrSql, ConfirmLogGrid);

    if (!turnPage2.strQueryResult)
    {
        alert("没有该批次确认日志信息！");
        return false;
    }
    fm.querySql.value = tStrSql;
    return true;
}
function beforeSubmit(){
	var tSel = ConfirmBatchGrid.getSelNo();
	if( tSel == null || tSel == 0 ){
		alert("请先选择需生成回送数据的批次！");
		return false;
	}
	var tBatchNo = ConfirmBatchGrid.getRowColData(ConfirmBatchGrid.getSelNo() - 1, 1);
	if(tBatchNo == null || tBatchNo == ""){
		alert("请先选择需生成回送数据的批次！");
        return false;
	}
	var tStrSql = "select importstate,batchconfstate from lcbrigrpimportbatchinfo where batchno = '"+tBatchNo+"'";
	var result = easyExecSql(tStrSql);
	if (!result || result.length !=1)
	{
		alert("获取批次信息失败！");
		return false;
	}
	if(result[0][0] != "02"){//校验是否导入成功
		alert("该批次导入未成功，请核查！");
		return false;
	}
	if(result[0][1] == "02"){ //校验是否已确认
		alert("该批次已确认！");
		return false;
	}
	return true;
}
//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && ConfirmLogGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "XmlListConfirmDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再下载！");
        return ;
    }
}

