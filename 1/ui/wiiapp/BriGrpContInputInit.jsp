<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>
<script language="JavaScript">
function initForm()
{
    try
    {
    	showMeid();
        initBox();
        displayConfirmDiv();
        
        qryGrpPolicy();
        initExtend();
        showAllCodeName();
        
        
    }
    catch(ex)
    {
        alert(ex.message);
    }
}
function showMeid(){
	// 交叉销售渠道 Crs_SaleChnl   交叉销售业务类型 Crs_BussType  对方业务员代码 GrpAgentCode
	// 对方机构代码 GrpAgentCom   对方业务员姓名  GrpAgentName  对方业务员证件号码 GrpAgentIDNo
	var QL = "select Crs_SaleChnl, Crs_BussType, GrpAgentCode,GrpAgentCom ,GrpAgentName, GrpAgentIDNo from  LCGrpCont where prtno = '" +PrtNo+" ' ";
	var arrResult = easyExecSql(QL);
	if (arrResult!=null && arrResult[0][1]!=""){
		
		fm.Crs_SaleChnl.value = arrResult[0][0];
		fm.Crs_BussType.value = arrResult[0][1];
		fm.GrpAgentCode.value = arrResult[0][2];
		fm.GrpAgentCom.value = arrResult[0][3];
		fm.GrpAgentName.value = arrResult[0][4];
		fm.GrpAgentIDNo.value = arrResult[0][5];
		fm.MixComFlag.checked = true;
		isMixCom();
		
	}
}

function initBox()
{
    try
    {
        fm.all("PrtNo").value = PrtNo;
        fm.all("ManageCom").value = ManageCom;

        fm.all("ProcessID").value = ProcessID;
        fm.all("MissionID").value = MissionID;
        fm.all("SubMissionID").value = SubMissionID;
        fm.all("ActivityID").value = ActivityID;
        fm.all("ActivityStatus").value = ActivityStatus;
        fm.all("QueryFlag").value = QueryFlag;
        
        fm.all("PayIntv").value = "0";
        
        if(QueryFlag != null && QueryFlag != "" && QueryFlag == "1")
        {
        	document.getElementById("btnCreateGrpCont").style.display = "none"; 
        }
    }
    catch(ex)
    {
        alert("初始化控件错误！");
        alert(ex);
    }
}
</script>
