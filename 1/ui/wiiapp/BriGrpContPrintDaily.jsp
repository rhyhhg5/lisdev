<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
    tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var strManageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Verifyinput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="BriGrpContPrintDaily.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="BriGrpContPrintDailyInit.jsp"%>
    <title>保单打印日结单</title>
</head>
<body onload="initForm();" >
    <form action="./BriGrpContPrintDailySave.jsp" method=post name=fm target="fraSubmit">
        <!-- 个人保单信息部分 -->
        <table class= common border=0 width=100%>
            <tr><td class= titleImg align= center>请输入保单打印查询条件：</td></tr>
        </table>
        <table class= common align=center>
            <TR class= common>
                <TD class= title>公司编码</TD>
                <TD class= input><input class="codeNo" name=manageCom ondblclick="return showCodeList('comcode',[this,manageComName],[0,1],null,null,null,1);" readonly><input class=codename name=manageComName readonly ></TD>
                <TD class= title>印刷号</TD>
                <TD class= input><input class="common" name=prtNo ></TD>
                <TD class= title>保单类型</TD>
                <TD class= input><input class="codeNo" name=contType value="2" CodeData="0|^2|团单" ondblclick="return showCodeListEx('contType',[this,contTypeName],[0,1]);" readonly><input class=codename name=contTypeName value="团单" readonly=true ></TD>
            </TR>
            <TR class= common>
                <TD class= title>日期</TD>
                <TD class= input><input class="coolDatePicker" name=dailyDate ></TD>
                <TD class= title>起始时间</TD>
                <TD class= input><input class="codeNo" name=startTime value="00" CodeData="0|^00|0点|^12|12点" ondblclick="return showCodeListEx('startTime',[this,startTimeName],[0,1]);" readonly><input class=codename name=startTimeName value="0点" readonly=true ></TD>
                <TD class= title>终止时间</TD>
                <TD class= input><input class="codeNo" name=endTime value="24" CodeData="0|^12|12点|^24|24点" ondblclick="return showCodeListEx('endTime',[this,endTimeName],[0,1]);" readonly><input class=codename name=endTimeName value="24点" readonly=true ></TD>
            </TR>
        </table>
        <input type="hidden" class="common" name="querySql" >
        <input value="查询打印信息" class="cssButton" type=button onclick="easyQueryClick();">
        <input value="下载清单" class="cssButton" type=button onclick="download();">
        <br>
        <table>
            <tr>
                <td class=common>
                    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContPrintDaily);">
                </td>
                <td class= titleImg>保单打印列表</td>
            </tr>
        </table>
        <div id= "divContPrintDaily" style= "display: ''">
            <table class= common>
                <tr class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanContPrintDailyGrid" ></span>
                    </td>
                </tr>
            </table>
            <div id= "divPage" align=center style= "display: 'none' ">
                <input value="首 页" class="cssButton" type=button onclick="turnPage.firstPage();">
                <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
                <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
                <input value="尾 页" class="cssButton" type=button onclick="turnPage.lastPage();">
            </div>
        </div>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
