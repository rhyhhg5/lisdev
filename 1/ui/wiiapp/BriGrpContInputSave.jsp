<%@page contentType="text/html;charset=GBK"%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.wiitb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%
System.out.println("BriGrpContInputSave.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{
    String tPrtNo = request.getParameter("PrtNo");
    String tManageCom = request.getParameter("ManageCom");
    String tSaleChnl = request.getParameter("SaleChnl");
    String tMarketType = request.getParameter("MarketType");
    String tAgentCom = request.getParameter("AgentCom");
    String tAgentCode = request.getParameter("AgentCode");
    String tPolApplyDate = request.getParameter("PolApplyDate");
    String tHandlerName = request.getParameter("HandlerName");
    String tFirstTrialOperator = request.getParameter("FirstTrialOperator");
    String tPeoples3 = request.getParameter("Peoples3");
    String tOnWorkPeoples = request.getParameter("OnWorkPeoples");
    String tOffWorkPeoples = request.getParameter("OffWorkPeoples");
    String tOtherPeoples = request.getParameter("OtherPeoples");
    String tPayMode = request.getParameter("PayMode");
    String tPayIntv = request.getParameter("PayIntv");
    String tPremScope = request.getParameter("PremScope");
    String tCValiDate = request.getParameter("CValiDate");
    String tCInValiDate = request.getParameter("CInValiDate");
    String tRemark = request.getParameter("Remark");
    
    String tCustomerNo = request.getParameter("CustomerNo");
    String tGrpName = request.getParameter("GrpName");
    String tOrgancomCode = request.getParameter("OrgancomCode");
    String tOtherCertificates = request.getParameter("OtherCertificates");
    
    String tGrpAddress = request.getParameter("GrpAddress");
    String tProvinceID = request.getParameter("ProvinceID");
    String tCityID = request.getParameter("CityID");
    String tCountyID = request.getParameter("CountyID");
    String tDetailAddress = request.getParameter("DetailAddress");
    String tGrpZipCode = request.getParameter("GrpZipCode");
    String tLinkMan1 = request.getParameter("LinkMan1");
    String tPhone1 = request.getParameter("Phone1");
    String tMobile1= request.getParameter("Mobile1");
    String tE_Mail1 = request.getParameter("E_Mail1");
    String tTaxpayerType = request.getParameter("TaxpayerType");
    String tTaxNo = request.getParameter("TaxNo");
    String tCustomerBankCode = request.getParameter("CustomerBankCode");
    String tCustomerBankAccNo = request.getParameter("CustomerBankAccNo");
    String tUnifiedSocialCreditNo = request.getParameter("UnifiedSocialCreditNo");
    
    //交叉销售的信息
    String Crs_SaleChnl = request.getParameter("Crs_SaleChnl");//交叉销售渠道
    String  Crs_BussType= request.getParameter("Crs_BussType");//交叉销售业务类型
    String  GrpAgentCode= request.getParameter("GrpAgentCode");//对方业务员代码
    String  GrpAgentCom= request.getParameter("GrpAgentCom");//对方机构代码
    String  GrpAgentName= request.getParameter("GrpAgentName");//对方业务员姓名
    String  GrpAgentIDNo= request.getParameter("GrpAgentIDNo");//对方业务员证件号码
    System.out.println(Crs_SaleChnl);
    System.out.println(Crs_BussType);
    System.out.println(GrpAgentCode);
    System.out.println(GrpAgentCom);
    System.out.println(GrpAgentName);
    System.out.println(GrpAgentIDNo);
    
    // 保单基本信息   
    LCGrpContSchema tGrpContInfo = new LCGrpContSchema();
    //交叉销售的信息保存
    tGrpContInfo.setCrs_SaleChnl(Crs_SaleChnl);
    tGrpContInfo.setCrs_BussType(Crs_BussType);
    tGrpContInfo.setGrpAgentCode(GrpAgentCode);
    tGrpContInfo.setGrpAgentCom(GrpAgentCom);
    tGrpContInfo.setGrpAgentName(GrpAgentName);
    tGrpContInfo.setGrpAgentIDNo(GrpAgentIDNo);
    
    //////////////
    
    tGrpContInfo.setPrtNo(tPrtNo);
    tGrpContInfo.setManageCom(tManageCom);
    tGrpContInfo.setSaleChnl(tSaleChnl);
    tGrpContInfo.setMarketType(tMarketType);
    tGrpContInfo.setAgentCom(tAgentCom);
    tGrpContInfo.setAgentCode(tAgentCode);
    
    tGrpContInfo.setPolApplyDate(tPolApplyDate);
    tGrpContInfo.setHandlerName(tHandlerName);
    tGrpContInfo.setFirstTrialOperator(tFirstTrialOperator);
    
    System.out.println("-----------------"+tPolApplyDate);
    
    tGrpContInfo.setPeoples3(tPeoples3);
    tGrpContInfo.setOnWorkPeoples(tOnWorkPeoples);
    tGrpContInfo.setOffWorkPeoples(tOffWorkPeoples);
    tGrpContInfo.setOtherPeoples(tOtherPeoples);
    
    tGrpContInfo.setPayMode(tPayMode);
    tGrpContInfo.setPayIntv(tPayIntv);
    tGrpContInfo.setPremScope(tPremScope);
    
    tGrpContInfo.setCValiDate(tCValiDate);
    tGrpContInfo.setCInValiDate(tCInValiDate);
    
    tGrpContInfo.setRemark(tRemark);
    // --------------------
    
    
    // 投保单位信息    
    LCGrpAppntSchema tGrpAppntInfo = new LCGrpAppntSchema();
    
    tGrpAppntInfo.setCustomerNo(tCustomerNo);
    tGrpAppntInfo.setName(tGrpName);
    tGrpAppntInfo.setOrganComCode(tOrgancomCode);
    tGrpAppntInfo.setOtherCertificates(tOtherCertificates);
    tGrpAppntInfo.setTaxpayerType(tTaxpayerType);
    tGrpAppntInfo.setTaxNo(tTaxNo);
    tGrpAppntInfo.setCustomerBankCode(tCustomerBankCode);
    tGrpAppntInfo.setCustomerBankAccNo(tCustomerBankAccNo);
    tGrpAppntInfo.setUnifiedSocialCreditNo(tUnifiedSocialCreditNo);
    // --------------------
    
    // 投保单位地址&联系方式相关信息
    LCGrpAddressSchema tGrpAddrInfo = new LCGrpAddressSchema();
    
    tGrpAddrInfo.setGrpAddress(tGrpAddress);
    tGrpAddrInfo.setPostalProvince(tProvinceID);
    tGrpAddrInfo.setPostalCity(tCityID);
    tGrpAddrInfo.setPostalCounty(tCountyID);
    tGrpAddrInfo.setDetailAddress(tDetailAddress);
    tGrpAddrInfo.setGrpZipCode(tGrpZipCode);
    tGrpAddrInfo.setLinkMan1(tLinkMan1);
    tGrpAddrInfo.setPhone1(tPhone1);
    tGrpAddrInfo.setMobile1(tMobile1);
    tGrpAddrInfo.setE_Mail1(tE_Mail1);
    // --------------------
    
    TransferData tTransData = new TransferData();
    
    VData tVData = new VData();
    
    tVData.add(tGI);
    tVData.add(tTransData);
    
    tVData.add(tGrpContInfo);
    tVData.add(tGrpAppntInfo);
    tVData.add(tGrpAddrInfo);
    
    //by gzh 20130408 增加对综合开拓数据的处理
  	String tAssistSaleChnl = request.getParameter("AssistSaleChnl");
  	String tAssistAgentCode = request.getParameter("AssistAgentCode");
	LCExtendSchema tLCExtendSchema = new LCExtendSchema();
	tLCExtendSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCExtendSchema.setAssistSalechnl(tAssistSaleChnl);
	tLCExtendSchema.setAssistAgentCode(tAssistAgentCode);
	tVData.add(tLCExtendSchema);
  
  //by gzh 20130408 增加对综合开拓数据的处理 end
  System.out.println("end setSchema:");
    BriGrpContUI tBriGrpContUI = new BriGrpContUI();
    if(!tBriGrpContUI.submitData(tVData, "Create"))
    {
        Content = " 保存失败! 原因是: " + tBriGrpContUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 保存成功！";
        FlagStr = "Succ";
    }
    
}
catch (Exception e)
{
    Content = " 操作失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}


System.out.println("BriGrpContInputSave.jsp End ...");
%>

<html>
    <script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
