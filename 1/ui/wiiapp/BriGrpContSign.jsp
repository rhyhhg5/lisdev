<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2011-03-23
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="BriGrpContSign.js"></script>
    <%@include file="BriGrpContSignInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">请输入查询条件：</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">印刷号</td>
                    <td class="input8">
                        <input class="common" name="PrtNo" />
                    </td>
                    <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title8">是否缴费</td>
                    <td class="input8">
					<Input class=codeno name=ISJF VALUE="1" CodeData="0|^0|全部^1|已缴费^2|未缴费" ondblclick="return showCodeListEx('ISJF',[this,ISJFName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ISJF',[this,ISJFName],[0,1],null,null,null,1);"><input class=codename name=ISJFName readonly=true > 		     		
					</td>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryBriGrpContList();" /> 	
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">待签发保单</td>
            </tr>
        </table>
        <div id="divCertifySettleListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanBriGrpContListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divBriGrpContListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnDelMission" name="btnSingConfirm" value="确认签发保单" onclick="signGrpPol(1);" />
                    <input class="cssButton" type="button" id="btnDelMission" name="btnSingBatch" value="批量签单" onclick="signGrpPol(2);" />
                </td>
            </tr>
        </table>
        
        <input type="hidden" class="common" name="ActivityID" value="" />
        <INPUT TYPE="hidden" NAME="workType">
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
