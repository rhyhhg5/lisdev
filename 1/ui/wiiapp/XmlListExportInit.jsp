<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;

%>

<script language="JavaScript">

function initInpBox()
{
    try
    {
        fm.all('ManageCom').value = <%=tGI.ManageCom%>;
    }
    catch(ex)
    {
        alert("初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInpBox();
        initExportBatchGrid();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

/**
 * 导入确认列表
 */
function initExportBatchGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="报送单位";
        iArray[2][1]="30px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="报送人员";
        iArray[3][1]="30px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="任务数量";
        iArray[4][1]="30px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="导入状态";
        iArray[5][1]="30px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="导入开始日期";
        iArray[6][1]="60px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="导入完成日期";
        iArray[7][1]="50px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="入机时间";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        ExportBatchGrid = new MulLineEnter("fm", "ExportBatchGrid"); 

        ExportBatchGrid.mulLineCount = 0;   
        ExportBatchGrid.displayTitle = 1;
        ExportBatchGrid.canSel = 1;
        ExportBatchGrid.hiddenSubtraction = 1;
        ExportBatchGrid.hiddenPlus = 1;
        ExportBatchGrid.canChk = 0;
        ExportBatchGrid.loadMulLine(iArray);
        ExportBatchGrid.selBoxEventFuncName = "BatchNoShow"; 
    }
    catch(ex)
    {
        alert("初始化ExportBatchGrid时出错：" + ex);
    }
}
</script>