<%@page contentType="text/html;charset=GBK"%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.wiitb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.ContInputAgentcomChkBL"%>

<%
System.out.println("BriGrpContChkConfirmSave.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{
    String tGrpContNo = request.getParameter("GrpContNo");
    String tProcessID = request.getParameter("ProcessID");
    String tActivityID = request.getParameter("ActivityID");
    String tMissionID = request.getParameter("MissionID");
    String tSubMissionID = request.getParameter("SubMissionID");

    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
    tTransferData.setNameAndValue("ProcessID", tProcessID);
    tTransferData.setNameAndValue("ActivityID", tActivityID);
    tTransferData.setNameAndValue("MissionID", tMissionID);
    tTransferData.setNameAndValue("SubMissionID", tSubMissionID);

    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tTransferData);

    GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
  	//江苏团险校验
    boolean tJSFlag = false;
    {
      String tSalechnlSQL = "select salechnl from lcgrpcont where grpcontno='"+tGrpContNo+"'";
  	  String tSaleChnl = new ExeSQL().getOneValue(tSalechnlSQL);
  	  String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
  			  	  + "and code = '"+tSaleChnl+"'";
  	  SSRS tSSRS = new ExeSQL().execSQL(tSql);
  	  String tManagecomSQL = "select managecom from lcgrpcont where grpcontno='"+tGrpContNo+"'";
  	  String tManageCom = new ExeSQL().getOneValue(tManagecomSQL);
  	  String tSubManageCom = tManageCom.substring(0,4);
  	  if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(tSubManageCom)){
  		  ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
      	  TransferData tJSTransferData = new TransferData();
      	  tJSTransferData.setNameAndValue("ContNo",tGrpContNo);
      	  VData tJSVData = new VData();
      	  tJSVData.add(tJSTransferData);
      	  if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
      		  Content = tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
      		  FlagStr = "Fail";
      		  tJSFlag = true;
      	  }
  	  }
    }
    if(!tJSFlag){
    	if (!tTbWorkFlowUI.submitData(tVData, tActivityID)){
      		Content = " 保存失败! 原因是: " + tTbWorkFlowUI.mErrors.getFirstError();
        	FlagStr = "Fail";
    	}else{
        	Content = " 保存成功！";
        	FlagStr = "Succ";
    	}
    }
}
catch (Exception e)
{
    Content = " 操作失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}


System.out.println("BriGrpContChkConfirmSave.jsp End ...");
%>

<html>
    <script language="javascript">
    parent.fraInterface.afterChkConfirmSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
