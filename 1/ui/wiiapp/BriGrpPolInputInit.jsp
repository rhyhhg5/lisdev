<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>
<script language="JavaScript">

function initForm()
{
    try
    {
        initBox();
        initWrapListGrid();
        initWrapDutyParamListGrid();
        displayConfirmDiv();
        
        qryGrpPolicy();
        
        showAllCodeName();
    }
    catch(ex)
    {
        alert(ex.message);
    }
}


function initBox()
{
    try
    {
        fm.all("PrtNo").value = PrtNo;
        fm.all("GrpContNo").value = GrpContNo;
        fm.all("ManageCom").value = ManageCom;

        fm.all("ProcessID").value = ProcessID;
        fm.all("MissionID").value = MissionID;
        fm.all("SubMissionID").value = SubMissionID;
        fm.all("ActivityID").value = ActivityID;
        fm.all("ActivityStatus").value = ActivityStatus;
        fm.all("QueryFlag").value = QueryFlag;
        
        if(QueryFlag != null && QueryFlag != "" && QueryFlag == "1")
        {
        	document.getElementById("btnInpConfirm").style.display = "none";
        	document.getElementById("btnChkConfirm").style.display = "none";
        	document.getElementById("btnInsuList").style.display = "none";
        	document.getElementById("btnCreateWrapPlan").style.display = "none";
        	document.getElementById("btnQueryInsuList").style.display = "";
        }
    }
    catch(ex)
    {
        alert("初始化控件错误！");
        alert(ex);
    }
}


function initWrapListGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="套餐编码";
        iArray[1][1]="60px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="套餐名称";
        iArray[2][1]="250px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="套餐归属机构";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=3;
        
        iArray[4]=new Array();
        iArray[4][0]="停售时间";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=3;
        

        WrapListGrid = new MulLineEnter("fm", "WrapListGrid"); 

        WrapListGrid.mulLineCount = 0;   
        WrapListGrid.displayTitle = 1;
        WrapListGrid.canSel = 0;
        WrapListGrid.canChk = 1;
        WrapListGrid.chkBoxEventFuncName = "qryWrapDutyParamInfo";
        WrapListGrid.hiddenSubtraction = 1;
        WrapListGrid.hiddenPlus = 1;
        WrapListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化initWrapListGrid时出错：" + ex);
    }
}


function initWrapDutyParamListGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="套餐编码";
        iArray[1][1]="60px";
        iArray[1][2]=100;
        iArray[1][3]=3;
        
        iArray[2]=new Array();
        iArray[2][0]="险种编码";
        iArray[2][1]="60px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="险种名称";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="所属主险编码";
        iArray[4][1]="60px";
        iArray[4][2]=100;
        iArray[4][3]=3;
        
        iArray[5]=new Array();
        iArray[5][0]="责任编码";
        iArray[5][1]="60px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="责任名称";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="要素代码";
        iArray[7][1]="60px";
        iArray[7][2]=100;
        iArray[7][3]=3;
        
        iArray[8]=new Array();
        iArray[8][0]="要素名称";
        iArray[8][1]="60px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="要素类型";
        iArray[9][1]="30px";
        iArray[9][2]=100;
        iArray[9][3]=3;
        
        iArray[10]=new Array();
        iArray[10][0]="要素值";
        iArray[10][1]="60px";
        iArray[10][2]=100;
        iArray[10][3]=1;
        
        iArray[11]=new Array();
        iArray[11][0]="要素说明";
        iArray[11][1]="60px";
        iArray[11][2]=100;
        iArray[11][3]=0;
        

        WrapDutyParamListGrid = new MulLineEnter("fm", "WrapDutyParamListGrid"); 

        WrapDutyParamListGrid.mulLineCount = 0;   
        WrapDutyParamListGrid.displayTitle = 1;
        WrapDutyParamListGrid.canSel = 0;
        WrapDutyParamListGrid.canChk = 0;
        WrapDutyParamListGrid.hiddenSubtraction = 1;
        WrapDutyParamListGrid.hiddenPlus = 1;
        WrapDutyParamListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化initWrapDutyParamListGrid时出错：" + ex);
    }
}
</script>
