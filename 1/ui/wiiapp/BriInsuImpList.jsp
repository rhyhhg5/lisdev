<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2011-04-25
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<script>
    var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
</script>

<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="BriInsuImpList.js"></script>
    <%@include file="BriInsuImpListInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">基本信息：</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">印刷号</td>
                    <td class="input8">
                        <input class="readonly" name="PrtNo" readonly="readonly" />
                    </td>
                    <td class="title8">&nbsp;</td>
                    <td class="input8">&nbsp;</td>
                    <td class="title8">&nbsp;</td>
                    <td class="input8">&nbsp;</td>
                </tr>
            </table>
        </div>
    
        <table style="display:'none'">
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="qryInsuImpList();" />
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divInsuImpListGridPage);" />
                </td>
                <td class="titleImg">被保人清单</td>
            </tr>
        </table>
        <div id="divInsuImpListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanInsuImpListGrid"></span> 
                    </td>
                </tr>
            </table>

            <div id="divInsuImpListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>
        
        <input class="common" type="hidden" name="GrpContNo" value="" />
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
