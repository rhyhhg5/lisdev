<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.wiitb.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = true;
	String Content = "";
	String mExportPath = null;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    String tBatchNo = request.getParameter("BatchNo");
    try
    {
        if(tBatchNo != null && !"".equals(tBatchNo))
        {
        	String tBatchImportDir = new ExeSQL().getOneValue("select SysVarValue from LDSysVar where SysVar = 'WiiXmlExportDir'");
        	if(tBatchImportDir == null || "".equals(tBatchImportDir))
        	{
        		Content = " 下载失败! 原因是: 获取生成文件路径失败！" ;
                //Content = " 导入清单失败! 原因详见“导入批次错误日志”！";
                errorFlag = false;
        	}
        	mExportPath = application.getRealPath("").replace('\\','/') + '/';
        	mExportPath += tBatchImportDir;
        	System.out.println("生成下载文件的路径为："+mExportPath);
        	
        	VData tVData = new VData();
            
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("BatchNo", tBatchNo);
            tTransferData.setNameAndValue("ExportPath", mExportPath);
            
            tVData.add(tTransferData);
            tVData.add(tG);
        
            BriGrpDownLoadUI tBriGrpDownLoadUI = new BriGrpDownLoadUI();
            
            if (!tBriGrpDownLoadUI.submitData(tVData, "Confrim"))
            {
                Content = " 下载失败! 原因是: " + tBriGrpDownLoadUI.mErrors.getLastError();
                //Content = " 导入清单失败! 原因详见“导入批次错误日志”！";
                errorFlag = false;
            }
            else
            {
            	System.out.println("工伤险生成返回xml文件成功！");
            }
        }
        else
        {
            Content = " 批次号获取失败！";
            errorFlag = false;
        }
    }
    catch(Exception ex)
    {
    	Content = " 批次号获取失败！";
        errorFlag = false;
    }
    //生成文件名
    
    String downLoadFileName = tBatchNo+"_return.xml";
    
    System.out.println("生成文件名称："+downLoadFileName);
    //errorFlag = false;
    if(errorFlag){
    	downLoadFile(response,mExportPath,downLoadFileName);
    }else{
    	Content = " 下载失败！";
        errorFlag = false;
    }
    out.clear();
    out = pageContext.pushBody();
    
%>
<html>
<script language="javascript">
parent.fraInterface.afterExportBatch("<%=errorFlag%>", "<%=Content%>", "<%=tBatchNo%>");
</script>
</html>
