<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2011-03-23
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Succ";
	String Content = "";
	String Priview = "PREVIEW";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
  
  	//接收信息
  	// 投保单列表
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	String tGrpContNo[] = request.getParameterValues("BriGrpContListGrid1");
	String tPrtNo[] = request.getParameterValues("BriGrpContListGrid2");
	//String tRadio[] = request.getParameterValues("InpBriGrpContListGridSel");
	String tChk[] = request.getParameterValues("InpBriGrpContListGridChk");
	String tMissionID[] = request.getParameterValues("BriGrpContListGrid7");   
	String tSubMissionID[] = request.getParameterValues("BriGrpContListGrid8");
	String workType = request.getParameter("workType");
	
	int grouppolCount = tGrpContNo.length;
	for (int i = 0; i < grouppolCount; i++)
	{
		if( tGrpContNo[i] != null && tChk[i].equals( "1" ))
		{
            System.out.println("GrpContNo:"+i+":"+tGrpContNo[i]);
		    tLCGrpContSchema.setGrpContNo( tGrpContNo[i] );
		    if("PREVIEW".equals(workType))
		    {
		      tLCGrpContSchema.setAppFlag("9");
		    }
		    TransferData tTransferData = new TransferData();
		    tTransferData.setNameAndValue("GrpContNo",tGrpContNo[i] );
		    tTransferData.setNameAndValue("MissionID",tMissionID[i] );
	     	tTransferData.setNameAndValue("SubMissionID",tSubMissionID[i] );
			//break;
			String tActivityId = "0000012002";
        
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add( tLCGrpContSchema );
			tVData.add( tTransferData );
			tVData.add( tG );
		
			// 数据传输
	        GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
			boolean bl= tTbWorkFlowUI.submitData( tVData, tActivityId);
			int n = tTbWorkFlowUI.mErrors.getErrorCount();
			if( n == 0 ) 
		    { 
			    if ( bl )
			    {
		    		System.out.println("保单（"+tPrtNo[i]+"）签单成功！");
		    		Content += "<br /> 保单（"+tPrtNo[i]+"）签单成功";
		    	}
		    	else
		    	{
		    	   FlagStr = "Fail";
		    	   Content += "<br /> 保单（"+tPrtNo[i]+"）签单失败";
		    	}
		    }
		    else
		    {
		    	String strErr = "";
				for (int j = 0; j < n; j++)
				{
					strErr += (j+1) + ": " + tTbWorkFlowUI.mErrors.getError(j).errorMessage + "; ";
					System.out.println(tTbWorkFlowUI.mErrors.getError(j).errorMessage );
				}
				 if ( bl )
				 {
		    		Content += "<br /> 保单（"+tPrtNo[i]+"）部分签单成功,但是有如下信息: " +strErr;
		    	}
		    	else
		    	{
			 		  FlagStr = "Fail";
			 		  Content += "<br /> 保单（"+tPrtNo[i]+"）签单失败，原因是: " + strErr;
				}
			} // end of if
		}
	}
%>                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
