//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


/**
 * 提交表单
 */
function submitForm()
{
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}

/**
 * 初始化批次号。
 */
function initBatchNoInput(cBatchNo)
{    
    if(cBatchNo != null && cBatchNo != "")
    {
        fmImport.BatchNo.value = cBatchNo;
    }
    else
    {
        fmImport.BatchNo.value = "";
    }
}

/**
 * 导入清单。
 */
function importBatchList()
{
    if(fmImport.FileName.value ==""){
    	alert("请选择上传数据文件！");
    	return false;
    }
    fmImport.btnImport.disabled = true;
    
    var showStr = "正在导入清单数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fmImport.action = "./XmlListImportSave.jsp";
    fmImport.submit();

    fmImport.action = "";
}

/**
 * 导入清单提交后动作。
 */
function afterImportCertifyList(FlagStr, Content, cBatchNo)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //alert("导入批次失败，请尝试重新进行申请。");
        fm.ImportBatchNo.value = cBatchNo;
        queryImportLog();
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        fm.ImportBatchNo.value = cBatchNo;
        queryImportLog();
    }
    
    fmImport.btnImport.disabled = false;
}

function queryImportLog()
{
    var tBatchNo = fm.ImportBatchNo.value;
    if(tBatchNo =="" || tBatchNo == null){
    	alert("批次号不能为空！");
    	return false;
    }
    var tStrSql = ""
        + " select licil.BatchNo, licil.CardNo, licil.ErrorInfo, varchar(licil.MakeDate) || ' '|| licil.MakeTime "
        + " from LICertifyImportLog licil "
        + " where 1 = 1 "
        + " and licil.BatchNo = '" + tBatchNo + "' "
        + " and licil.errortype != 'S' "
        + " and licil.errortype != 'E' "
        + " order by varchar(licil.MakeDate) || ' '|| licil.MakeTime desc, licil.CardNo "
        ;
//prompt('',tStrSql);
    turnPage2.pageDivName = "divImportLogGridPage";
    turnPage2.queryModal(tStrSql, ImportLogGrid);

    if (!turnPage2.strQueryResult)
    {
        alert("没有该批次导入日志信息！");
        return false;
    }
    fm.querySql.value = tStrSql;
    return true;
}
// 查询错误日志
function queryErrImportLog()
{
    var tBatchNo = fm.ImportBatchNo.value;
    var tStrSql = ""
        + " select licil.BatchNo, licil.CardNo, licil.ErrorInfo, varchar(licil.MakeDate) || ' '|| licil.MakeTime "
        + " from LICertifyImportLog licil "
        + " where 1 = 1 "
        + " and licil.BatchNo = '" + tBatchNo + "' "
        // + " and licil.errortype = '1' "
        + " order by varchar(licil.MakeDate) || ' '|| licil.MakeTime desc, licil.CardNo "
        ;
//prompt('',tStrSql);
    turnPage2.pageDivName = "divImportLogGridPage";
    turnPage2.queryModal(tStrSql, ImportLogGrid);

    if (!turnPage2.strQueryResult)
    {
        alert("没有该批次导入日志信息！");
        return false;
    }
    fm.querySql.value = tStrSql;
    return true;
}

//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && ImportLogGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "XmlQueryDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再下载！");
        return ;
    }
}

