<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServerItemPriceSave.jsp
//程序功能：
//创建日期：2006-03-22 16:59:27
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LHServerItemPriceSet tLHServerItemPriceSet   = new LHServerItemPriceSet();
  OLHServerItemPriceUI tOLHServerItemPriceUI   = new OLHServerItemPriceUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
  	
  	String tServItemCode[] = request.getParameterValues("ItemPriceGrid1");
  	String tServItemName[] = request.getParameterValues("ItemPriceGrid2");
  	String tPriceCode[] = request.getParameterValues("ItemPriceGrid3");
  	String tPriceName[] = request.getParameterValues("ItemPriceGrid4");
  	String tMakeDate[] = request.getParameterValues("ItemPriceGrid5");
  	String tMakeTime[] = request.getParameterValues("ItemPriceGrid6");
  	
	int LHItemPriceCount = 0;
	if(tServItemCode != null)
	{	
		LHItemPriceCount = tServItemCode.length;
	}
	for(int i = 0; i < LHItemPriceCount; i++)
	{
	    LHServerItemPriceSchema tLHServerItemPriceSchema = new LHServerItemPriceSchema();
	    
	    tLHServerItemPriceSchema.setServItemCode(tServItemCode[i]);
	    tLHServerItemPriceSchema.setServItemName(tServItemName[i]);
	    tLHServerItemPriceSchema.setServPriceCode(tPriceCode[i]);
	    tLHServerItemPriceSchema.setServPriceName(tPriceName[i]);
	    tLHServerItemPriceSchema.setComID(request.getParameter("ComID"));
	    tLHServerItemPriceSchema.setMakeDate(tMakeDate[i]);
	    tLHServerItemPriceSchema.setMakeTime(tMakeTime[i]);
	    
	    tLHServerItemPriceSet.add(tLHServerItemPriceSchema);
	}

	try
	{
	  // 准备传输数据 VData
	  	VData tVData = new VData();
		tVData.add(tLHServerItemPriceSet);
	  	tVData.add(tG);
	    tOLHServerItemPriceUI.submitData(tVData,transact);
	}
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHServerItemPriceUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
