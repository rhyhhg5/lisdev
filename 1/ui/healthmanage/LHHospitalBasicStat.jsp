
<%
  //程序名称：LHHospitalBasicStat.jsp
  //程序功能：医院基本情况的统计
  //创建日期：2007-09-19 16:02:27
  //创建人  ：liuli
  //更新人  ：
%>
<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput) session.getValue("GI");
%>
<script type="">
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LHHospitalBasicStat.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body>
<table>
  <tr>
    <td class=titleImg>医院基本情况统计</td>
  </tr>
</table>
<form action="./LHHospitalBasicPrint.jsp" method=post name=fm>
<table class="common">
  <tr>
    <td class=title8>管理机构</td>
    <td >
      <Input class=codeno readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true><font color=red>*</font>
    </td>
    <TD width="5%" align="right" class=title8>合作时间</TD>
    <TD>
      <Input class='coolDatePicker' dateFormat="Short" name=AssociateDate verify="入机时间|DATE">
    </TD>
    <td>    </td>
    <td>    </td>
  </tr>
  <tr>
    <TD class=title8>入机时间    </TD>
    <TD class=input8>
      <Input class='coolDatePicker' dateFormat="Short" name=MakeDate1 value="" verify="入机时间|DATE">
    </td>
    <td class="title8">至</td>
    <TD class=input8>
      <Input class='coolDatePicker' dateFormat="Short" name=MakeDate2 value="" verify="入机时间|DATE">
    </TD>
    <td>    </td>
    <td>    </td>
  </tr>
  <tr>
    <td>
      <br>
    </td>
  </tr>
  <tr>
    <TD class=title>
      <Input value="基本情况统计" style="width:130px" type=button class=cssbutton onclick="aLLHosptialSearch();">
    </TD>
    <TD class=title>
      <Input value="分合作级别统计" style="width:130px" type=button class=cssbutton onclick="HosptialSearch();">
    </TD>
    <td></td>
  </tr>
  <input name="statflag" type="hidden">
</table>
  <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
