<%
//程序名称：LHQueryMenu.jsp
//程序功能：健康查询左边导航条
//创建日期：2005-12-19 14:47:24
//创建人  ：ctrHTML
//更新人  ：林仁令  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>

<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHQueryMain.js"></SCRIPT>
      
  <title>查询分类列表</title>
	<style type="text/css">
	<!--
	body {
		background-color: #eaeaf0;
	}
	.style1 {color: #0066FF}
	-->
	</style>
</head>
 <%
	SSRS tSSRS = null;
	ExeSQL tExeSQL = new ExeSQL();
	String sql = "select distinct menucode, menuname from lhquerymenu where menulevel = '1' ";
	
	tSSRS = tExeSQL.execSQL(sql); 
	int length = tSSRS.getMaxRow();
	String arr[][] = tSSRS.getAllData();                                                                                                              
%> 
<style type="text/css">
<!--
.mainmenu{font-size: 13px}
.submenu {font-size: 12px}

-->
</style>
<SCRIPT language=javascript1.2>
function showsubmenu(sid)
{
    whichEl = eval('submenu' + sid);
    if (whichEl.style.display == 'none')
    {
        submenu0.style.display='none';
        submenu1.style.display='none';
        submenu2.style.display='none';
        eval("submenu" + sid + ".style.display='';");
    }
    else
    {
        eval("submenu" + sid + ".style.display='none';");
    }
}
</SCRIPT>


<body leftmargin="0">
<table class=menu style="width:200px" bgcolor="#cbecfc" cellspacing="0" cellpadding="0">          
	<tr bgcolor="#cbecfc" >			
		<td  class=gridtitle3 id=Disease name=Disease bgcolor="#eaeaf0" style= "border-bottom:0pt" onclick="changeColorDiv(this);">
			疾病 
		</td>
		<td class=gridtitle4 id=Drug name=Drug  bgcolor="#cbecfc" onclick="changeColorDiv(this);">
			药品
		</td>
		<td class=gridtitle4 id=Operation name=Operation  bgcolor="#cbecfc" onclick="changeColorDiv(this);">
			手术
		</td>
		<td class=gridtitle4 id=Testitem name=Testitem  bgcolor="#cbecfc" onclick="changeColorDiv(this);">
			检验
		</td>
		<td class=gridtitle4 id=Hospital name=Hospital  bgcolor="#cbecfc" onclick="changeColorDiv(this);">
			医院
		</td>
	</tr>
</table>
<Div id=DiseaseDiv style="display:">
<%  for (int i = 0; i < length; i++)
	{  
%>
	<table cellpadding=0 cellspacing=0>
		<br>
		<tr>
			<td class=common style="width:150" onclick='showsubmenu(<%=i%>)' onmouseout=this.style.backgroundColor='#eaeaf0' style='cursor:hand;'>    		
			&nbsp;&nbsp;<img src="../common/images/butCollapse.gif" border="0" align="absmiddle">&nbsp;<span class=mainmenu><%=arr[i][1]%></span>
			</td>
		</tr>
        <tr>
        	<td style='display:none' id='submenu<%=i%>'>
			<div>
				<table cellpadding=0 cellspacing=0 width=180>   
	  		
	  	        <%
	  	        SSRS tSSRS2 = null;
				ExeSQL tExeSQL2 = new ExeSQL();
				String sql_2 = " select distinct submenucode,submenuname from lhquerymenu where menulevel = '1' and menucode= '"+arr[i][0]+"'";
				
				tSSRS2 = tExeSQL2.execSQL(sql_2); 
				String arr_2[][] = tSSRS2.getAllData();
				int length2 = arr_2.length;
				
				for (int j = 0; j < length2; j++)
				{
				%>
				 <tr >
				 	<td height=13>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=<%=arr_2[j][0]%>" target="main"><span class="submenu style1"><%=arr_2[j][1]%></span></a></td>
				 </tr>
				<%
				}
				%>
	  	 		</table>
     		</div>
			</td>
		</tr>
	</table>
	<%
	}
	%> 
</Div>

<Div id=DrugDiv style="display:none">
</Div>	
<Div id=OperationDiv style="display:none">
	<table cellpadding=0 cellspacing=0>
		<br>
		<tr >
			<td height=16>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=" target="main"><span class="submenu style1">肛肠外科神经外科</span></a></td>
		</tr>
		<tr >
			<td height=16>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=" target="main"><span class="submenu style1">耳鼻喉科</span></a></td>
		</tr>
		<tr >
			<td height=16>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=" target="main"><span class="submenu style1">口腔科</span></a></td>
		</tr>
		<tr >
			<td height=16>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=" target="main"><span class="submenu style1">眼科</span></a></td>
		</tr>
		<tr >
			<td height=16>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=" target="main"><span class="submenu style1">胸心外科普通外科</span></a></td>
		</tr>
		<tr >
			<td height=16>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=" target="main"><span class="submenu style1">骨科</span></a></td>
		</tr>
		<tr >
			<td height=16>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=" target="main"><span class="submenu style1">泌尿外科</span></a></td>
		</tr>
		<tr >
			<td height=16>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=" target="main"><span class="submenu style1">妇产科</span></a></td>
		</tr>
		<tr >
			<td height=16>&nbsp;&nbsp;<img src="../common/images/menu4.gif" border="0" align="absmiddle"> <a href="LHQueryruslt.jsp?MenuCode=" target="main"><span class="submenu style1">临床常用诊断技术</span></a></td>
		</tr>
		
	</table>
</Div>
<Div id=TestitemDiv style="display:none">
</Div>	
<Div id=HospitalDiv style="display:none">
</Div>	

</body>
</html>