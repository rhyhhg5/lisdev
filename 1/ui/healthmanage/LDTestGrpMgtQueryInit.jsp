<%
//程序名称：LDTestGrpMgtQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-06-13 10:00:52
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('TestGrpCode').value = "";

  }
  catch(ex) {
    alert("在LDTestGrpMgtQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDTestGrpMgtGrid();  
  }
  catch(re) {
    alert("LDTestGrpMgtQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDTestGrpMgtGrid;
function initLDTestGrpMgtGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();                    
    iArray[1][0]="体检套餐代码";         		//列名       
    iArray[1][1]="60px";         		//列名       
    iArray[1][3]=0;         		//列名           
    
    iArray[2]=new Array();                            
    iArray[2][0]="体检套餐名称";         		//列名            
    iArray[2][1]="150px";         		//列名            
    iArray[2][3]=0;         		//列名     
    
    iArray[3]=new Array();                            
    iArray[3][0]="体检套餐类型code";         		//列名            
    iArray[3][1]="0px";         		//列名            
    iArray[3][3]=3;         		//列名     
    
    iArray[4]=new Array();      
    iArray[4][0]="MakeDate";
    iArray[4][1]="0px";                                
    iArray[4][3]=3; 
    
    iArray[5]=new Array();      
    iArray[5][0]="MakeTime";
    iArray[5][1]="0px";       
    iArray[5][3]=3;      
    
    iArray[6]=new Array();      
    iArray[6][0]="hospitcode";
    iArray[6][1]="0px";       
    iArray[6][3]=3;       	
    
    iArray[7]=new Array();      
    iArray[7][0]="所属医院";
    iArray[7][1]="260px";       
    iArray[7][3]=0;      
    
	iArray[8]=new Array();                            
    iArray[8][0]="体检套餐类型";         		//列名            
    iArray[8][1]="150px";         		//列名            
    iArray[8][3]=0;  	        		                                                                                                    
    
    LDTestGrpMgtGrid = new MulLineEnter( "fm" , "LDTestGrpMgtGrid" ); 
    //这些属性必须在loadMulLine前

    LDTestGrpMgtGrid.mulLineCount = 0;   
    LDTestGrpMgtGrid.displayTitle = 1;
    LDTestGrpMgtGrid.hiddenPlus = 1;
    LDTestGrpMgtGrid.hiddenSubtraction = 1;
    LDTestGrpMgtGrid.canSel = 1;
    LDTestGrpMgtGrid.canChk = 0;
//    LDTestGrpMgtGrid.selBoxEventFuncName = "showOne";

    LDTestGrpMgtGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDTestGrpMgtGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
