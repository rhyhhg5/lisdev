//该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}
function easyQueryClick()
{

	var strSql="select case when ServCaseType = '1' then '个人服务事件' when ServCaseType = '2' then '集体服务事件' end,"
			+" ServCaseCode,ServCaseName,MakeDate,MakeTime from LHServCaseDef where 1=1 "
	       + getWherePart("ServCaseType","ServeEventStyle")
	       + getWherePart("ServCaseCode","ServeEventNum")
	       + getWherePart("ServCaseName","ServeEventName")
	       ;
	   
	       turnPage.queryModal(strSql, LHServEventGrid);

}
function returnParent()
{
  var arrReturn = new Array();
	var tSel = LHServEventGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
			try
			{	
				arrReturn = getQueryResult();
				top.opener.afterQuery1( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery1接口。" + ex );
			}

			parent.opener.top.window.focus();//top的父窗口得到焦点
			top.window.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHServEventGrid.getSelNo();
	
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LHServEventGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
