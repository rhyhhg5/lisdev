<%
//程序名称：LHEventTypeSettingInit.jsp
//程序功能：
//创建日期：2006-07-04 10:24:40
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ComID').value = "";
    fm.all('ServItemNO').value = "";
    fm.all('ServItemName').value = "";
    fm.all('EventType').value = "";
  }
  catch(ex)
  {
    alert("在LHEventTypeSettingInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                        
function initForm()
{
  try
  {
    initInpBox();
    initLHQueryDetailGrid();
  }
  catch(re)
  {
    alert("LHEventTypeSettingInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var d = new Date();
var h = d.getYear();
var m = d.getMonth(); 
var day = d.getDate();  
var Date2;       
if(h<10){h = "0"+d.getYear();}  
if(m<9){ m++; m = "0"+m;}
else{m++;}
if(day<10){day = "0"+d.getDate();}
Date2 = h+"-"+m+"-"+day;
var LHQueryDetailGrid;
function initLHQueryDetailGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="服务任务编号";   
	  iArray[1][1]="80px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="服务任务名称";   
	  iArray[2][1]="80px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="服务任务状态";   
	  iArray[3][1]="70px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=2;
	  iArray[3][4]="taskstatus";               
	  iArray[3][5]="3|8";     //引用代码对应第几列，'|'为分割符
    iArray[3][6]="1|0";     //上面的列中放置引用代码中第几位
    iArray[3][14]="等待执行"; 
    iArray[3][17]="1"; 
    iArray[3][18]="140";
    iArray[3][19]="1" ;

	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="操作员代码";   
	  iArray[4][1]="70px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=1;
	  
	  iArray[5]=new Array(); 
	  iArray[5][0]="操作员名称";   
	  iArray[5][1]="70px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=1;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="计划实施时间(天)";   
	  iArray[6][1]="65px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=1;  
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="任务完成时间";   
	  iArray[7][1]="70px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=1;
	  iArray[7][14]=Date2; 

	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="服务任务说明";   
	  iArray[8][1]="140px";   
	  iArray[8][2]=20;        
	  iArray[8][3]=1;
	  
	  
	  iArray[9]=new Array(); 
	  iArray[9][0]="taskstatus";   
	  iArray[9][1]="0px";   
	  iArray[9][2]=20;        
	  iArray[9][3]=3;
	  iArray[9][14]="1";

	
	                               
    LHQueryDetailGrid = new MulLineEnter( "fm" , "LHQueryDetailGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHQueryDetailGrid.mulLineCount = 0;                             
    LHQueryDetailGrid.displayTitle = 1;                          
    LHQueryDetailGrid.hiddenPlus = 0;                          
    LHQueryDetailGrid.hiddenSubtraction = 0;                          
    LHQueryDetailGrid.canSel = 1;                          
    LHQueryDetailGrid.canChk = 0;                          
                    
                                                             
    LHQueryDetailGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHQueryDetailGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
