<%
//程序名称：LHFamilyDoctorServiceInput.jsp
//程序功能：功能描述
//创建日期：2005-02-27 18:16:36
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHFamilyDoctorServiceQueryInput.js"></SCRIPT> 
  <%@include file="LHFamilyDoctorServiceQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLHFamilyDoctorServiceGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
<!--
    <TD  class= title>
      家庭医生服务登记号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FDSNo >
    </TD>
-->    
    <TD  class= title>
      客户号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerNo >
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Name >
    </TD>
    <TD class= title>
    </TD>
    <TD class = input>
    </TD>
  </TR>
<!--  <TR  class= common>
    
    <TD  class= title>
      家庭医生代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FamilyDoctorNo >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      家庭医生姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FamilyDoctorName >
    </TD>
    <TD  class= title>
      服务主题
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FDSSubject >
    </TD>
  </TR>
-->
  <TR  class= common>
  	<TD  class= title>
      服务项目
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FDSCode >
    </TD>
    
    <TD  class= title>
      服务情况
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FDSContent >
    </TD>
    <TD class= title>
    </TD>
    <TD class = input>
    </TD>
  </TR>
<!--
  <TR  class= common>
    <TD  class= title>
      状态
    </TD>
    <TD  class= input>
      <Input class= 'common' name=State >
    </TD>
    
  </TR>
-->  
</table>
  </Div>
  <div id="div1" style="display: 'none'">
  	<table >
  		<TD  class= title>
	      操作员代码
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=Operator >
	    </TD>
  		<TR  class= common>
	    <TD  class= title>
	      入机日期
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MakeDate >
	    </TD>
	    <TD  class= title>
	      入机时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MakeTime >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      最后一次修改日期
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=ModifyDate >
	    </TD>
	    <TD  class= title>
	      最后一次修改时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=ModifyTime >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      服务日期
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=FDSDate >
	    </TD>
	  </TR>
	</table>
</div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHFamilyDoctorService1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHFamilyDoctorService1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHFamilyDoctorServiceGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
