<%
//程序名称：LHScanReasonFosterInit.jsp
//程序功能：扫描件录入页面
//创建日期：2006-06-27 14:40:48
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
//     String ScanNo = request.getParameter("ScanNo");
//     String ScanType = request.getParameter("ScanType");
//     String prtNo = request.getParameter("prtNo");
//     System.out.println("AAAAAAa"+ScanType);
%>  

<script language="JavaScript">                                       
function initForm2()
{
  try
  {
    fms.all('RelationType').value="<%=ScanType%>"; 
         var sqlType="select SerialNo,Comment,InsertState,"
                +"(case InsertState when '1' then '全部录入' when '2' then '部分录入' when '3' then '尚未录入'  else '无' end ),"
                +"RelationNo,RelationNo2,RelationType "
                +" from LHScanInfo "
                +"where SerialNo='"+ fms.all('ScanNo').value+"' ";
          //alert(easyExecSql(sqlType));
          var arrResult = easyExecSql(sqlType);
          if(arrResult!="null"&&arrResult!=""&&arrResult!=null)
          {
            // fms.all('ScanNo').value				      = arrResult[0][0];
		     	   fms.all('ScanName').value			      = arrResult[0][1];
		         fms.all('ScanStatus').value         = arrResult[0][2];
		         fms.all('ScanStatusName').value     = arrResult[0][3];
		         if(arrResult[0][4]!="null"&&arrResult[0][4]!=""&&arrResult[0][4]!=null)
		         {
		            fms.all('RelationNo').value          = arrResult[0][4];
		            fm.all('CustomerNo').value          = arrResult[0][4];
		            fms.all('RelationNo2').value          = arrResult[0][5];
		            fm.all('InHospitNo').value          = arrResult[0][5];
		            getCustomerNo();
		         }
		      }
		      
  }
  catch(re)
  {
    alert("LHScanReasonFosterInit.jsp-->InitForm函数中发生异常:初始化界面错误111!");
  }
}
function initScanInfo()
{
	  fms.all('ScanNo').value="<%=prtNo%>"; 
    fms.all('RelationType').value="<%=ScanType%>"; 
         var sqlType="select SerialNo,Comment,InsertState,"
                +"(case InsertState when '1' then '全部录入' when '2' then '部分录入' when '3' then '尚未录入'  else '无' end ),"
                +"RelationNo,RelationNo2,RelationType "
                +" from LHScanInfo "
                +"where SerialNo='"+ fms.all('ScanNo').value+"' ";
          var arrResult = easyExecSql(sqlType);
          //alert(arrResult);
          if(arrResult!="null"&&arrResult!=""&&arrResult!=null)
          {
             fms.all('ScanNo').value				      = arrResult[0][0];
		     	   fms.all('ScanName').value			      = arrResult[0][1];
		     	   if(arrResult[0][2]!=""&&arrResult[0][2]!="null"&&arrResult[0][2]!=null)
		     	   {
		            fms.all('ScanStatus').value         = arrResult[0][2];
		            fms.all('ScanStatusName').value     = arrResult[0][3];
		         }
		         fms.all('RelationNo').value          = arrResult[0][4];
		         //fm.all('CustomerNo').value          = arrResult[0][4];
		         fms.all('RelationNo2').value          = arrResult[0][5];
		         //fm.all('InHospitNo').value          = arrResult[0][5];
		      }
}
function initForm3()
{
  try
  {
          initScanInfo();
		      getCustomerNo2();
		       
		      
  }
  catch(re)
  {
    alert("LHScanImportPageInit.jsp-->InitForm函数中发生异常:初始化界面错误BB!");
  }
}
</script>                  
<script language="JavaScript">
function getCustomerNo()
{
    //alert(fm.all('CustomerNo').value);
	  //alert(fms.all('RelationNo2').value);
	//	try
	//	{
			var strSql=" select a.CustomerNo,b.name, a.InHospitNo, a.HospitCode,'',a.InHospitDate,"
      					+" (select distinct c.DoctNo from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"
      					+" '',a.InHospitMode,(select distinct c.MainCureMode from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),'',"
//      					+" (select distinct c.DiagnoseNo from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"     					
      					+" (select distinct c.CureEffect from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"
      					+" a.InHospitalDays,a.MakeDate,a.MakeTime ,a.MainItem "
      					+" from LHCustomInHospital a, ldperson b "     					
      					+" where a.CustomerNo=b.CustomerNo and a.CustomerNo='"+ fm.all('CustomerNo').value +"' "
      					+" and a.InHospitNo='"+ fms.all('RelationNo2').value +"'"      
      					;
      					//alert(strSql);
      arrResult = easyExecSql(strSql); 
		
	 // }
	 // catch(ex){  alert("LHScanCustomInHospitalInit.js->afterquery0出错");}
      
      
      fm.all('CustomerNo').value= arrResult[0][0];fm.all('CustomerNo').readOnly = true;
      fm.all('CustomerName').value= arrResult[0][1];
      fm.all('InHospitNo').value= arrResult[0][2];
      fm.all('HospitCode').value= arrResult[0][3];
      if(arrResult[0][3] != "")
      {  fm.all('HospitName').value= easyExecSql("select HospitName from LDHospital where hospitcode = '"+arrResult[0][3]+"'");}
      fm.all('InHospitDate').value= arrResult[0][5];
      fm.all('DoctNo').value= arrResult[0][6];
      if(arrResult[0][6] != "")
      {  fm.all('DoctName').value= easyExecSql("select DoctName from LDDoctor where DoctNo = '"+arrResult[0][6]+"'");}
      fm.all('InHospitModeCode').value= arrResult[0][8];if(arrResult[0][8]=="1"){fm.all('InHospitMode').value="门诊";strsqlinit=" code like #1%# and ";initFeeInfoGrid();};if(arrResult[0][8]=="2"){fm.all('InHospitMode').value="住院";strsqlinit=" code like #2%# and ";initFeeInfoGrid();}if(arrResult[0][8]=="3"){fm.all('InHospitMode').value="急诊";}
      fm.all('MainCureModeCode').value= arrResult[0][9];
      var tempsql="select codename from ldcode where codetype='maincuremode' and code='"+arrResult[0][9]+"'";
      var curemodeResult=easyExecSql(tempsql);
      if(curemodeResult!=null)
      {
      	fm.all('MainCureMode').value= curemodeResult[0][0];
      }
      //fm.all('MainCureModeCode').value= arrResult[0][10];
      //fm.all('DiagnoseNo').value= arrResult[0][10];
      //alert(arrResult[0][11]);
      if(arrResult[0][11]!=""&&arrResult[0][11]!="null"&&arrResult[0][11]!=null)
      {
        fm.all('CureEffectCode').value= arrResult[0][11];fm.all('CureEffect').value= easyExecSql("select codename from ldcode where codetype='diagnosecureeffect' and code ='"+arrResult[0][11]+"'");
      }
      fm.all('InHospitalDays').value= arrResult[0][12];if(arrResult[0][8]=="2"){fm.InHospitalDays.style.display='';fm.CureEffect.style.display='';}else{fm.CureEffect.style.display='none';fm.InHospitalDays.style.display='none';}
      fm.all('MakeDate').value= arrResult[0][13];
      fm.all('MakeTime').value= arrResult[0][14];
      fm.all('MainItem').value= arrResult[0][15];
       
      strSql="select a.ICDCode,b.ICDName, a.IsMainDiagno,c.CodeName,a.DiagnoseNo "         
     +" from LHDiagno a, LDDisease b,LDCode c where 1=1  "            
     +" and a.ICDCode=b.ICDCode and a.IsMainDiagno=c.Code and c.codetype='ismaindiagno'"
     +getWherePart("a.CustomerNo","CustomerNo")                       
     +getWherePart("a.InHospitNo","InHospitNo");                      
		  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
		  turnPage.pageLineNum = 100;  
		  turnPage.queryModal(strSql, DiagnoInfoGrid);
		  
		   strSql=" select b.MedicaItemName,a.MedicaItemCode,"
						 +" a.TestDate,a.TestResult,a.ResultUnitNum,"
				     +" a.TestNo from LHCustomTest a, LHCountrMedicaItem b where 1=1  "
				     +" and a.MedicaItemCode=b.MedicaItemCode "
				     + getWherePart("a.CustomerNo","CustomerNo") 
				     + getWherePart("a.InHospitNo","InHospitNo");
				     //  a.DoctNo,(select c.DoctName from LDDoctor c where a.DoctNo=c.DoctNo ),a.TestFeeAmount,
			//alert(strSql);
					  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
	     //alert(easyExecSql(strSql));
	     turnPage.pageLineNum = 100;  
	  turnPage.queryModal(strSql, TestInfoGrid);
	  
	   strSql="select b.ICDOPSName,a.ICDOPSCode, a.DoctNo,(select c.DoctName from LDDoctor c where a.DoctNo=c.DoctNo ),a.ExecutDate,a.CureEffect,"
     +" a.OPSNo from LHCustomOPS a, LDICDOPS b where 1=1  "
     +" and a.ICDOPSCode=b.ICDOPSCode   "
     +getWherePart("a.CustomerNo","CustomerNo") 
     +getWherePart("a.InHospitNo","InHospitNo");
	  //var   arrLHDiagnoResult = easyExecSql(strSQL); a.OPSFeeAmount,
	  turnPage.pageLineNum = 100;  
	  turnPage.queryModal(strSql, OPSInfoGrid);
	  
	    
	   strSql="select b.MedicaItemName,a.MedicaItemCode, a.DoctNo,(select c.DoctName from LDDoctor c where a.DoctNo=c.DoctNo ),a.ExecutDate,a.CureEffect,"
     +" a.OtherFeeAmount,a.OtherCureNo from LHCustomOtherCure a, LHCountrMedicaItem b where 1=1  "
     +" and a.MedicaItemCode=b.MedicaItemCode "
     +getWherePart("a.CustomerNo","CustomerNo") 
     +getWherePart("a.InHospitNo","InHospitNo");
     //alert(strSql);
     //alert(easyExecSql(strSql));
	  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
	  turnPage.pageLineNum = 100;  
	  turnPage.queryModal(strSql,OtherCureInfoGrid);
	  
	    strSql="select b.codename,a.FeeCode, a.FeeAmount,'',a.FeeNo "
     +"from LHFeeInfo a, ldcode b where 1=1  "
     +" and a.FeeCode=b.code and b.codetype='llfeeitemtype' "
     +getWherePart("a.CustomerNo","CustomerNo") 
     +getWherePart("a.InHospitNo","InHospitNo");
   turnPage.pageLineNum = 100;  
	  turnPage.queryModal(strSql, FeeInfoGrid);
	  
//计算分费用比例	   
	    var totalfee=0;
			var tempProportion;
			var rowcount=FeeInfoGrid.mulLineCount;
		
			for (i=0;i<=rowcount-1; i++)
			{
		   tempProportion=FeeInfoGrid.getRowColData(i,3)/1;
		   totalfee+= tempProportion     ;
			}

			for(i=0;i<=rowcount-1; i++)
			{   			
				tempProportion=FeeInfoGrid.getRowColData(i,3)/totalfee*100;
				tempProportion=Math.round(tempProportion*100)/100;  
				tempProportion=tempProportion+"%";
				FeeInfoGrid.setRowColData(i,4,tempProportion);
			}
			fm.all('TotalFee').value=totalfee;
   
//	  fm.all('iscomefromquery').value="1";
	//}
} 
function  getCustomerNo2()//通过理赔按钮导入初始化   
{
	  var strSQL = " select count( distinct caserelano) from llcaserela a"
			     	   +"  where a.caseno='"+fms.all('ScanNo').value+"'"
			     	   ;
		arrResult =easyExecSql(strSQL);
		//alert(arrResult);
		if(arrResult!=null&&arrResult!="null"&&arrResult!=null)
		{
		   	var strSQL5 = " select distinct caserelano from llcaserela a"
			     	             +"  where a.caseno='"+fms.all('ScanNo').value+"'"
			     	             ;
			  arrResult5 =easyExecSql(strSQL5);
			  //alert(arrResult5);
        for(var k=0;k<arrResult;k++)//循环输入多个值
        {
        	 //alert(arrResult5[k]);
        	 fms.all('CaseRelaNo').value=arrResult5[k];
        	 
        	 alert(fms.all('CaseRelaNo').value);
        	 fms.all('Maxno').value =fms.all('CaseRelaNo').value+k;
        	 //alert(fms.all('Maxno').value);
        	 if(fms.all('Maxno').value!=""&&fms.all('Maxno').value!=null&&fms.all('Maxno').value!="null")
        	 {
        	   //initMul();
             //submitForm();
             //fms.all('Maxno').value="";
           }
           
        }
    }
    
} 

function initMul()
{ 
     var strSQL2 = " select distinct  Hospitalcode ,HospitalName,InHospitalDays,"
               +" DoctorNo,DoctorName ,"
               +" CustomerNo,CustomerName,InHospitalDate "
               +" from LLCaseCure "
			     	   +"  where CaseRelaNo='"+fms.all('txtCaseRelaNo').value+"'"
			     	   ;
		arrResult2 =easyExecSql(strSQL2);
		//alert(arrResult2);
		if(arrResult2!=null&&arrResult2!="null"&&arrResult2!=null)
		{
        fm.all('HospitCode').value= arrResult2[0][0];
        fm.all('HospitName').value= arrResult2[0][1];
        if(arrResult2[0][7]!=""&&arrResult2[0][7]!="null"&&arrResult2[0][7]!=null)
        {
           fm.all('InHospitalDays').value= arrResult2[0][2];
           fm.InHospitalDays.style.display=''; 
        }
        else
        {
        	 fm.all('InHospitalDays').value="";
        	 fm.InHospitalDays.style.display='none'; 
        }
        fm.all('DoctNo').value= arrResult2[0][3];
        fm.all('DoctName').value= arrResult2[0][4];
        fm.all('CustomerNo').value= arrResult2[0][5];
        fm.all('CustomerName').value= arrResult2[0][6];
        fm.all('InHospitDate').value= arrResult2[0][7];
    }
     var strSQL5 = " select distinct  FeeType"
               +" from LLFeeMain "
			     	   +"  where CaseRelaNo='"+fms.all('txtCaseRelaNo').value+"'"
			     	   ;
		arrResult5 =easyExecSql(strSQL5);
		//alert(arrResult5);
		if(arrResult5!=null&&arrResult5!="null"&&arrResult5!=null)
		{ 
			fm.all('InHospitModeCode').value= arrResult5[0][0];
			//alert(fm.all('InHospitModeCode').value);
			if(fm.all('InHospitModeCode').value=="1")
			{
				fm.all('InHospitMode').value="门诊";
				fm.CureEffect.style.display='none';     
	      fm.InHospitalDays.style.display='none'; 
			}
			if(fm.all('InHospitModeCode').value=="2")
			{
				fm.all('InHospitMode').value="住院";
				fm.CureEffect.style.display='';     
	      fm.InHospitalDays.style.display=''; 
			}
			if(fm.all('InHospitModeCode').value=="3")
			{
				fm.all('InHospitMode').value="急诊";
				fm.CureEffect.style.display='none';     
	      fm.InHospitalDays.style.display='none'; 
			}
		}
       strSql="select a.DiseaseCode ,a.DiseaseName,'2',"
               +" '次要诊断' ,"
               +" '' "         
					     +"  from llcasecure a  "            
					     +" where "
					     +"  a.CaseRelaNo='"+fms.all('txtCaseRelaNo').value+"'"                       
					     ;                
					     turnPage.pageLineNum = 100;       
							  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
							  turnPage.queryModal(strSql, DiagnoInfoGrid);
							  
      strSql="select a.OperationName,a.OperationCode,'','',"
					     +" '','','' "
					     +" from lloperation a "
					     +" where " 
					     +"a.CaseRelaNo='"+fms.all('txtCaseRelaNo').value+"'";
						  turnPage.pageLineNum = 100;  
						 // alert(strSql);
			turnPage.queryModal(strSql, OPSInfoGrid);
    
//     strSql="select a.FeeItemName,a.FeeItemCode, a.Fee ,'',''"
//           +" from llcasereceipt a ,llcase b "
//           +" where a.caseno=b.caseno and  "
//           +" b.caseno='"+fms.all('ScanNo').value+"' "
//           ;
     strSql3="select a.FeeItemName,a.FeeItemCode, a.Fee ,'',''"           
           +" from llcasereceipt a ,llfeemain b "                         
           +" where a.mainfeeno=b.mainfeeno and  "                        
           +" b.caserelano='"+fms.all('txtCaseRelaNo').value+"' "   
           +" and  (feeitemcode like '1%%' or feeitemcode like '2%%') " 
           ;                                                              		
        
       turnPage.pageLineNum = 100;  
	      turnPage.queryModal(strSql3, FeeInfoGrid);
	  
//计算分费用比例	   
	    var totalfee=0;
			var tempProportion;
			var rowcount=FeeInfoGrid.mulLineCount;
		
			for (i=0;i<=rowcount-1; i++)
			{
		   tempProportion=FeeInfoGrid.getRowColData(i,3)/1;
		   totalfee+= tempProportion     ;
			}

			for(i=0;i<=rowcount-1; i++)
			{   			
				tempProportion=FeeInfoGrid.getRowColData(i,3)/totalfee*100;
				tempProportion=Math.round(tempProportion*100)/100;  
				tempProportion=tempProportion+"%";
				FeeInfoGrid.setRowColData(i,4,tempProportion);
			}
			fm.all('TotalFee').value=totalfee;
   
     
}
</script>                  
<script language="JavaScript">
	var d = new Date();
	var h = d.getYear();
	var m = d.getMonth(); 
	var day = d.getDate();  
	var Date1;       
	if(h<10){h = "0"+d.getYear();}  
	if(m<9){ m++; m = "0"+m;}
	else{m++;}
	if(day<10){day = "0"+d.getDate();}
	Date1 = h+"-"+m+"-"+day;
  
	function initTestInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
	{
		var iArray = new Array(); //数组放置各个列
		try
		{
			iArray[0]=new Array();
			iArray[0][0]="序号";  	//列名（序号列，第1列）
		    iArray[0][1]="25px";  	//列宽
		    iArray[0][2]=10;        //列最大值
		    iArray[0][3]=0;   			//1表示允许该列输入，0表示只读且不响应Tab键
		                  				  // 2 表示为容许输入且颜色加深.
		    iArray[1]=new Array();
		    iArray[1][0]="检查项目名称";  //列名（第2列）
		    iArray[1][1]="90px";  	  		//列宽
		    iArray[1][2]=10;        			//列最大值
		    iArray[1][3]=2;          			//是否允许输入,1表示允许，0表示不允许
		    iArray[1][4]="lhmedicaitemname";
		    iArray[1][5]="1|2|7";     		//引用代码对应第几列，'|'为分割符
		    iArray[1][6]="0|1|2";    		  //上面的列中放置引用代码中第几位值
		    iArray[1][9]="检查项目名称|len<=120";
		    iArray[1][15]="MedicaItemName";
		    iArray[1][17]="1";  
		    iArray[1][19]="1" ;
		    
		    iArray[2]=new Array();                                                    
		    iArray[2][0]="检查项目代码";  //列名（第2列）
		    iArray[2][1]="0px";  	  		  //列宽
		    iArray[2][2]=10;       		    //列最大值
		    iArray[2][3]=3;         			//是否允许输入,1表示允许，0表示不允许
		            
		           
		    iArray[3]=new Array();
		    iArray[3][0]="检查时间";  //列名（第2列）
		    iArray[3][1]="70px";  	  //列宽
		    iArray[3][2]=10;        	//列最大值
		    iArray[3][3]=1;           //是否允许输入,1表示允许，0表示不允许
		    iArray[3][9]="检查时间|len<=120";
		    iArray[3][14]=Date1;
		    
		    iArray[4]=new Array();
		    iArray[4][0]="检查结果";  //列名（第2列）
		    iArray[4][1]="60px";  	  //列宽
		    iArray[4][2]=10;        	//列最大值
		    iArray[4][3]=1;     
		    iArray[4][7]="inputTestResult";//你写的JS函数名，不加扩号
		    
		    iArray[5]=new Array();
		    iArray[5][0]="检查单位";  //列名（第2列）
		    iArray[5][1]="80px";  	  //列宽
		    iArray[5][2]=30;        	//列最大值
		    iArray[5][3]=1;     
		    iArray[5][9]="检查单位|len<=30";
		    
		    iArray[6]=new Array();
		    iArray[6][0]="流水号";  				//列名（第2列）
		    iArray[6][1]="0px";  	  				//列宽
		    iArray[6][2]=10;        				//列最大值
		    iArray[6][3]=3; 
         
			//生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
			TestInfoGrid= new MulLineEnter( "fm" , "TestInfoGrid" ); 
			//设置属性区 (需要其它特性，在此设置其它属性)
			TestInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
			TestInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
			//对象初始化区：调用对象初始化方法，属性必须在此前设置
			TestInfoGrid.loadMulLine(iArray); 
		}
		catch(ex)
		{ alert(ex); }
    }
    
</script>
<script language="JavaScript">
  function initOPSInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;      //列最大值
      iArray[0][3]=0;   //1表示允许该列输入，0表示只读且不响应Tab键
                     // 2 表示为容许输入且颜色加深.
      
      iArray[1]=new Array();
      iArray[1][0]="手术名称";  //列名（第2列）
      iArray[1][1]="80px";  	  //列宽
      iArray[1][2]=10;        //列最大值
      iArray[1][3]=2;          //是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="lhICDOPSName";
      iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[1][9]="手术名称|len<=100";
      iArray[1][15]="ICDOPSName";
      iArray[1][17]="1";  
      iArray[1][19]="1" ;
      
      iArray[2]=new Array();
      iArray[2][0]="手术代码";  //列名（第2列）
      iArray[2][1]="0px";  	  //列宽
      iArray[2][2]=10;        //列最大值
      iArray[2][3]=3;          //是否允许输入,1表示允许，0表示不允许
         //后续可以添加N列，如上设置
      iArray[3]=new Array();
      iArray[3][0]="手术医师代码";  //列名（第2列）
      iArray[3][1]="0px";  	  //列宽
      iArray[3][2]=10;        //列最大值
      iArray[3][3]=3;   
             //是否允许输入,1表示允许，0表示不允许
      iArray[4]=new Array();
      iArray[4][0]="医师姓名";  //列名（第2列）
      iArray[4][1]="70px";  	  //列宽
      iArray[4][2]=10;        //列最大值
      iArray[4][3]=2;          //是否允许输入,1表示允许，0表示不允许
      iArray[4][4]="lhDoctName";
      iArray[4][5]="4|3";     //引用代码对应第几列，'|'为分割符
      iArray[4][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[4][9]="手术医师姓名|len<=20";
      iArray[4][15]="DoctName";
      iArray[4][17]="4";  
      iArray[4][19]="1" ;         //是否允许输入,1表示允许，0表示不允许
             //是否允许输入,1表示允许，0表示不允许
      
         
      iArray[5]=new Array();
      iArray[5][0]="手术时间";  //列名（第2列）
      iArray[5][1]="50px";  	  //列宽
      iArray[5][2]=10;        //列最大值
      iArray[5][3]=1;          //是否允许输入,1表示允许，0表示不允许
//      iArray[5][7]="getCurrentTime2";
      iArray[5][9]="手术时间|len<=120";
      iArray[5][14]=Date1;
      
      iArray[6]=new Array();
      iArray[6][0]="手术情况";  //列名（第2列）
      iArray[6][1]="100px";  	  //列宽
      iArray[6][2]=10;        //列最大值
      iArray[6][3]=1;          //是否允许输入,1表示允许，0表示不允许
      iArray[6][7]="inputOPSResult";//你写的JS函数名，不加扩号
      
      iArray[7]=new Array();
      iArray[7][0]="流水号";  //列名（第2列）
      iArray[7][1]="0px";  	  //列宽
      iArray[7][2]=10;        //列最大值
      iArray[7][3]=3;
         
               //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
      OPSInfoGrid= new MulLineEnter( "fm" , "OPSInfoGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            OPSInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            OPSInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //OPSInfoGrid.canSel =1; 
           
            
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      OPSInfoGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script>
 <script language="JavaScript">
  function initOtherCureInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;      //列最大值
      iArray[0][3]=0;   //1表示允许该列输入，0表示只读且不响应Tab键
                     // 2 表示为容许输入且颜色加深.
      iArray[1]=new Array();
      iArray[1][0]="治疗项目名称";  //列名（第2列）
      iArray[1][1]="90px";  	  //列宽
      iArray[1][2]=10;        //列最大值
      iArray[1][3]=2;          //是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="lhmedicaitemname";
      iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[1][9]="治疗项目名称|len<=120";
      iArray[1][15]="MedicaItemName";
      iArray[1][17]="1";  
      iArray[1][19]="1" ;
        
      iArray[2]=new Array();
      iArray[2][0]="治疗项目代码";  //列名（第2列）
      iArray[2][1]="0px";  	  //列宽
      iArray[2][2]=10;        //列最大值
      iArray[2][3]=3;          //是否允许输入,1表示允许，0表示不允许
         //后续可以添加N列，如上设置  
     
      
      iArray[3]=new Array();                                                    
      iArray[3][0]="治疗医师代码";  //列名（第2列）                                 
      iArray[3][1]="0px";  	  //列宽                                          
      iArray[3][2]=10;        //列最大值                                        
      iArray[3][3]=3;          //是否允许输入,1表示允许，0表示不允许            
             
      iArray[4]=new Array();
      iArray[4][0]="医师姓名";  //列名（第2列）
      iArray[4][1]="50px";  	  //列宽
      iArray[4][2]=10;        //列最大值
      iArray[4][3]=2;          //是否允许输入,1表示允许，0表示不允许
      iArray[4][4]="lhDoctName";
      iArray[4][5]="4|3";     //引用代码对应第几列，'|'为分割符
      iArray[4][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[4][9]="主检医师姓名|len<=20";
      iArray[4][15]="DoctName";
      iArray[4][17]="4";  
      iArray[4][19]="1" ;         //是否允许输入,1表示允许，0表示不允许
      
         
          
      iArray[5]=new Array();
      iArray[5][0]="治疗时间";  //列名（第2列）
      iArray[5][1]="30px";  	  //列宽
      iArray[5][2]=10;        //列最大值
      iArray[5][3]=1;          //是否允许输入,1表示允许，0表示不允许
//      iArray[5][7]="getCurrentTime3";
      iArray[5][9]="治疗时间|len<=120";
      iArray[5][14]=Date1;
     
      iArray[6]=new Array();
      iArray[6][0]="治疗结果";  //列名（第2列）
      iArray[6][1]="100px";  	  //列宽
      iArray[6][2]=10;        //列最大值
      iArray[6][3]=1;     
      iArray[6][7]="inputCureResult";//你写的JS函数名，不加扩号
      
      iArray[7]=new Array();
      iArray[7][0]="治疗费用(元)";  //列名（第2列）
      iArray[7][1]="30px";  	  //列宽
      iArray[7][2]=10;        //列最大值
      iArray[7][3]=1;
      
      iArray[8]=new Array();
      iArray[8][0]="流水号";  //列名（第2列）
      iArray[8][1]="0px";  	  //列宽
      iArray[8][2]=10;        //列最大值
      iArray[8][3]=3;
      
               //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
      OtherCureInfoGrid= new MulLineEnter( "fm" , "OtherCureInfoGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            OtherCureInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            OtherCureInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //OtherCureInfoGrid.canSel =1; 
           
            
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      OtherCureInfoGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script>          
<script language="JavaScript">
  function initDiagnoInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;      //列最大值
      iArray[0][3]=0;   //1表示允许该列输入，0表示只读且不响应Tab键
                     // 2 表示为容许输入且颜色加深.
                     
      iArray[1]=new Array();
      iArray[1][0]="诊断疾病代码";  //列名（第2列）
      iArray[1][1]="70px";  	  //列宽
      iArray[1][2]=10;        //列最大值
      iArray[1][3]=0;          //是否允许输入,1表示允许，0表示不允许
   
      iArray[2]=new Array();
      iArray[2][0]="诊断疾病名称";  //列名（第2列）
      iArray[2][1]="180px";  	  //列宽
      iArray[2][2]=10;        //列最大值
      iArray[2][3]=2;    
      iArray[2][4]="lhdisease";      //是否允许输入,1表示允许，0表示不允许            
      iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
      iArray[2][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[2][9]="疾病名称|len<=120";
      iArray[2][15]="ICDName";
      iArray[2][17]="2";     
      iArray[2][18]="430";
      iArray[2][19]="1" ;

      
      iArray[3]=new Array();
      iArray[3][0]="诊断标志代码";  	
      iArray[3][1]="0px";  	
      iArray[3][2]=10;      
      iArray[3][3]=3;  
      iArray[3][14]="2";     
      
      iArray[4]=new Array();
      iArray[4][0]="诊断标志";  	//列名（序号列，第1列）
      iArray[4][1]="50px";  	//列宽
      iArray[4][2]=10;      //列最大值
      iArray[4][3]=2;
      iArray[4][4]="IsMainDiagno";
      iArray[4][5]="4|3";     //引用代码对应第几列，'|'为分割符
      iArray[4][6]="0|1";     //上面的列中放置引用代码中第几位值
      iArray[4][7]="aaa";	
      iArray[4][9]="诊断标志|len<=120";
      iArray[4][18]="170";
      
      iArray[5]=new Array();
      iArray[5][0]="流水号";  	//列名（序号列，第1列）
      iArray[5][1]="0px";  	//列宽
      iArray[5][2]=10;      //列最大值
      iArray[5][3]=3;
     	
      
               //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
      DiagnoInfoGrid= new MulLineEnter( "fm" , "DiagnoInfoGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            DiagnoInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            DiagnoInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //DiagnoInfoGrid.canSel =1; 
           
            
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      DiagnoInfoGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script> 
<script language="JavaScript">
 var strsqlinit = null;
 function afterCodeSelect (strCodeName,Field)
 {
 		 if( strCodeName=="hminhospitalmode")
		 { 
		 	 var flag="<%=flag%>";  
		 	  //alert(flag);
		 	  if(flag==""||flag==null||flag=="null")   //从录入按钮进入些页
		 	  {
		 	       //alert(fm.all('InHospitModeCode').value);
	 		       if(fm.all('InHospitModeCode').value=="2") 
	           {
	           		  fm.CureEffect.style.display='';     
	           		  fm.InHospitalDays.style.display=''; 
	           		  fm.all('CureEffect').value="";
	           		  strsqlinit=" code like #2%# and ";//initFeeInfoGrid();return;
	           }
	           if(fm.all('InHospitModeCode').value=="1" )
	           { 
	           			fm.CureEffect.style.display='none';    
	                fm.InHospitalDays.style.display='none';
	           			strsqlinit=" code like #1%# and ";//initFeeInfoGrid();
	           }
	           if(fm.all('InHospitModeCode').value=="3" )
	           { 
	           			fm.CureEffect.style.display='none';    
	                fm.InHospitalDays.style.display='none';
	           			strsqlinit=" code like #1%# and ";//initFeeInfoGrid();
	           }
	      }
	      if(flag!=""&&flag!=null&&flag!="null")  //从导入按钮进入 
	      {
	      	   //alert(fm.all('TotalFee').value);
	      	   if(fm.all('TotalFee').value=="0"||fm.all('TotalFee').value=="null"||fm.all('TotalFee').value==null||fm.all('TotalFee').value=="")
	      	   {
	 		          if(fm.all('InHospitModeCode').value=="2") 
	              {
	              		  fm.CureEffect.style.display='';     
	              		  fm.InHospitalDays.style.display=''; 
	              		  fm.all('CureEffect').value="";
	              		  strsqlinit=" code like #2%# and ";initFeeInfoGrid();return;
	              }
	              if(fm.all('InHospitModeCode').value=="1" )
	              { 
	              			fm.CureEffect.style.display='none';    
	                   fm.InHospitalDays.style.display='none';
	              			strsqlinit=" code like #1%# and ";initFeeInfoGrid();
	              }
	              if(fm.all('InHospitModeCode').value=="3" )
	              { 
	              			fm.CureEffect.style.display='none';    
	                   fm.InHospitalDays.style.display='none';
	              			strsqlinit=" code like #1%# and ";initFeeInfoGrid();
	              }  
	           }
	           if(fm.all('TotalFee').value!="0")
	      	   {
	 		          if(fm.all('InHospitModeCode').value=="2") 
	              {
	              		  fm.CureEffect.style.display='';     
	              		  fm.InHospitalDays.style.display=''; 
	              		  fm.all('CureEffect').value="";
	              		  strsqlinit=" code like #2%# and ";
	              }
	              if(fm.all('InHospitModeCode').value=="1" )
	              { 
	              			fm.CureEffect.style.display='none';    
	                   fm.InHospitalDays.style.display='none';
	              			strsqlinit=" code like #1%# and ";
	              }
	              if(fm.all('InHospitModeCode').value=="3" )
	              { 
	              			fm.CureEffect.style.display='none';    
	                   fm.InHospitalDays.style.display='none';
	              			strsqlinit=" code like #1%# and ";
	              }  
	           }
	      }
  	 }                                           
}
</script>                  
<script language="JavaScript">  
  
  function initFeeInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
 
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;      //列最大值
      iArray[0][3]=0;   //1表示允许该列输入，0表示只读且不响应Tab键
                     // 2 表示为容许输入且颜色加深.
      	
      iArray[1]=new Array();
      iArray[1][0]="分费用项目名称";  	//列名（序号列，第1列）
      iArray[1][1]="120px";  	//列宽
      iArray[1][2]=10;      //列最大值
      iArray[1][3]=2;
      iArray[1][4]="lhfeeitemtype";
      iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[1][9]="分费用项目名称|len<=120";     
      iArray[1][15]="1";       
      iArray[1][16]=strsqlinit;
      iArray[1][18]="300";
     
      iArray[2]=new Array();
      iArray[2][0]="分费用项目代码";  	
      iArray[2][1]="0px";  	
      iArray[2][2]=10;      
      iArray[2][3]=3; 
                           
      iArray[3]=new Array();
      iArray[3][0]="分项目费用（元）";  //列名（第2列）
      iArray[3][1]="90px";  	  //列宽
      iArray[3][2]=10;        //列最大值
      iArray[3][3]=1;          //是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="分项目费用（元）|num&len<=12";
      
      iArray[4]=new Array();
      iArray[4][0]="分费用比例";  //列名（第2列）
      iArray[4][1]="90px";  	  //列宽
      iArray[4][2]=10;        //列最大值
      iArray[4][3]=0;          //是否允许输入,1表示允许，0表示不允许
      iArray[4][7]="countProportion"; 
      
      iArray[5]=new Array();
      iArray[5][0]="流水号";  //列名（第2列）
      iArray[5][1]="0px";  	  //列宽
      iArray[5][2]=10;        //列最大值
      iArray[5][3]=3;          //是否允许输入,1表示允许，0表示不允许
     
     
      
               //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
      FeeInfoGrid= new MulLineEnter( "fm" , "FeeInfoGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            FeeInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            FeeInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //FeeInfoGrid.canSel =1; 
           
            
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      FeeInfoGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script>                  
<script language="JavaScript">  
function initInpBox()
{ 
  try
  {                                   
    fm.all('CustomerNo').value = "";
    fm.all('CustomerName').readOnly=true;
    fm.all('CustomerName').value = ""
    
    fm.all('InHospitNo').value = "";

    fm.all('MainItem').value = "";
    fm.all('InHospitMode').value = "";
    fm.all('InHospitDate').value = "";
    fm.CureEffect.style.display='none';
		fm.InHospitalDays.style.display='none';
		fm.all('CureEffect').value="";
		
		fm.all('InHospitalDays').value="";
		fm.all('iscomefromquery').value="0";
		
    fm.all('HospitCode').value = "";
    fm.all('HospitName').value = "";
    fm.all('MainCureModeCode').value = "";
    fm.all('MainCureMode').value="";
    fm.all('DoctName').value = "";

    fm.all('TotalFee').value = "";

   
   
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHScanReasonFosterInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LHScanReasonFosterInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
  	
    initInpBox();
    initTestInfoGrid();
    initOPSInfoGrid();
    initOtherCureInfoGrid();
    initDiagnoInfoGrid();
    initFeeInfoGrid();
    var flag="<%=flag%>";  
  	//alert(flag);
  	fms.all('QueryBtn').disabled = true;
  	if(flag!=""&&flag!=null&&flag!="null")
    {
    	 fms.all('updateBtn').disabled = true;
    	 divCaseNo.style.display='';
    	 //initForm3();//从导入核保按钮进入1
    	 initScanInfo();
    	 //alert(fms.all('txtCaseRelaNo').value);
    	 var strSQL = " select count( distinct caserelano) from llcaserela a"
			     	   +"  where a.caseno='"+fms.all('ScanNo').value+"'"
			     	   ;
		   arrResult =easyExecSql(strSQL);
		  // alert(arrResult);
		   if(arrResult=="1")//从导入按钮进入，并且只有一个受理事件号
		   {
		   	 fms.all('RelaNo').value=fms.all('txtCaseRelaNo').value;
		   	 
		   	 initMul();
		   	  //initForm2();
       }
    }
    if(flag==""||flag==null||flag=="null")
    {
    	divCaseNo.style.display='none';
    	fms.all('ScanNo').value="<%=SerialNo%>"; 
    	fms.all('OtherNo').value="<%=prtNo%>"; 
    	initForm2();//从录入按钮进入
    }
  }
  catch(re)
  {
    alert("LHScanReasonFosterInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
             
</script> 
	<script>
//  通过综合查询进入此页面的处理
		var turnPage = new turnPageClass(); 
		function passQuery()
		{
			alert("KK");
//    如果不是综合查询进入则跳过此函数
		  if("<%=request.getParameter("Customerno")%>" == "" || "<%=request.getParameter("Customerno")%>" == "null" || "<%=request.getParameter("InhospitNo")%>" =="" || "<%=request.getParameter("InhospitNo")%>" =="null")
		  {
		  	  alert("LL");
		  		return;
		  }
//    进入此函数
	 	  else
		  { 		 
		  	  alert("DFD"); 
				  var CustomerNo_s = "<%=request.getParameter("Customerno")%>";
				  var InhospitNo_s = "<%=request.getParameter("InhospitNo")%>";
	  		//alert(CustomerNo_s+"-----"+InhospitNo_s);
				
			  	if(CustomerNo_s!=null && InhospitNo_s!=null) 
			  	{
			  			try
							{
								var strSql="select a.CustomerNo,b.name, a.InHospitNo,a.Hospitcode, "
								+"(select d.hospitname from ldhospital d where d.hospitcode = a.Hospitcode),"
								+" a.InHospitDate,"
					      +" c.DoctNo,(select e.DoctName from LDDoctor e where e.DoctNo = c.DoctNo),"
					      +"a.InHospitMode,c.MainCureMode,c.DiagnoseNo,"
					//      +"(select c.CureEffect from LHDiagno c where c.customerno='"+CustomerNo_s+"' and c.InHospitNo='"+ InhospitNo_s+"'),"
					      +" c.CureEffect,a.InHospitalDays,a.MakeDate,a.MakeTime "
					      +" from LHCustomInHospital a, ldperson b,LHDiagno c  where 1=1  "
					      +" and a.CustomerNo=b.CustomerNo and a.InHospitNo = c.InHospitNo "
					//      +" and c.IsMainDiagno='1' "
					      +" and a.Customerno = c.Customerno and a.CustomerNo='"+CustomerNo_s+"' and a.InHospitNo='"+ InhospitNo_s+"' " 
					      //+getWherePart("a.CustomerNo","CustomerNo") 
					     ;
					      arrResult = easyExecSql(strSql);//alert(strSql);
							
						  }
						  catch(ex){  alert("LHCustomInHospitalInput.js->afterquery0出错");}
					      
					      
					      fm.all('CustomerNo').value= arrResult[0][0];
					      fm.all('CustomerName').value= arrResult[0][1];
					      fm.all('InHospitNo').value= arrResult[0][2];
					      fm.all('HospitCode').value= arrResult[0][3];
					      fm.all('HospitName').value= arrResult[0][4];
					      fm.all('InHospitDate').value= arrResult[0][5];
					      fm.all('DoctNo').value= arrResult[0][6];
					      fm.all('DoctName').value= arrResult[0][7];
					      fm.all('InHospitModeCode').value= arrResult[0][8];if(arrResult[0][8]=="1"){fm.all('InHospitMode').value="门诊";strsqlinit=" code like #1%# and ";initFeeInfoGrid();}else{fm.all('InHospitMode').value="住院";strsqlinit=" code like #2%# and ";initFeeInfoGrid();}
					      fm.all('MainCureModeCode').value= arrResult[0][9];
					      var tempsql="select codename from ldcode where codetype='maincuremode' and code='"+arrResult[0][9]+"'";
					      var curemodeResult=easyExecSql(tempsql);
					      if(curemodeResult!=null)
					      {
					      	fm.all('MainCureMode').value= curemodeResult[0][0];
					      }
					      //fm.all('MainCureModeCode').value= arrResult[0][10];
					      //fm.all('DiagnoseNo').value= arrResult[0][10];
					      fm.all('CureEffectCode').value= arrResult[0][11];fm.all('CureEffect').value= easyExecSql("select codename from ldcode where codetype='diagnosecureeffect' and code ='"+arrResult[0][11]+"'");
					      fm.all('InHospitalDays').value= arrResult[0][12];if(arrResult[0][8]=="2"){fm.InHospitalDays.style.display='';fm.CureEffect.style.display='';}else{fm.CureEffect.style.display='none';fm.InHospitalDays.style.display='none';}
					      fm.all('MakeDate').value= arrResult[0][13];
					      fm.all('MakeTime').value= arrResult[0][14];
					       
					      strSql="select a.ICDCode,b.ICDName, a.IsMainDiagno,c.CodeName,a.DiagnoseNo "         
					     +" from LHDiagno a, LDDisease b,LDCode c where 1=1  "            
					     +" and a.ICDCode=b.ICDCode and a.IsMainDiagno=c.Code and c.codetype='ismaindiagno'"
					     +getWherePart("a.CustomerNo","CustomerNo")                       
					     +getWherePart("a.InHospitNo","InHospitNo");                      
							  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
							  turnPage.queryModal(strSql, DiagnoInfoGrid);
							  
							   strSql="select b.MedicaItemName,a.MedicaItemCode, a.DoctNo,c.DoctName,a.TestDate,a.TestResult,a.ResultUnitNum,"
					     +" a.TestFeeAmount,a.TestNo from LHCustomTest a, LHCountrMedicaItem b,LDDoctor c where 1=1  "
					     +" and a.MedicaItemCode=b.MedicaItemCode and a.DoctNo=c.DoctNo "
					     +getWherePart("a.CustomerNo","CustomerNo") 
					     +getWherePart("a.InHospitNo","InHospitNo");
						  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
						  
						  turnPage.queryModal(strSql, TestInfoGrid);
						  
						   strSql="select b.ICDOPSName,a.ICDOPSCode, a.DoctNo,c.DoctName,a.ExecutDate,a.CureEffect,"
					     +" a.OPSFeeAmount,a.OPSNo from LHCustomOPS a, LDICDOPS b,LDDoctor c where 1=1  "
					     +" and a.ICDOPSCode=b.ICDOPSCode and a.DoctNo=c.DoctNo "
					     +getWherePart("a.CustomerNo","CustomerNo") 
					     +getWherePart("a.InHospitNo","InHospitNo");
						  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
						  
						  turnPage.queryModal(strSql, OPSInfoGrid);
						  
						    
						   strSql="select b.MedicaItemName,a.MedicaItemCode, a.DoctNo,c.DoctName,a.ExecutDate,a.CureEffect,"
					     +" a.OtherFeeAmount,a.OtherCureNo from LHCustomOtherCure a, LHCountrMedicaItem b,LDDoctor c where 1=1  "
					     +" and a.MedicaItemCode=b.MedicaItemCode and a.DoctNo=c.DoctNo "
					     +getWherePart("a.CustomerNo","CustomerNo") 
					     +getWherePart("a.InHospitNo","InHospitNo");
						  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
						  
						  turnPage.queryModal(strSql, OtherCureInfoGrid);
						  
						    strSql="select b.codename,a.FeeCode, a.FeeAmount,'',a.FeeNo "
					     +"from LHFeeInfo a, ldcode b where 1=1  "
					     +" and a.FeeCode=b.code and b.codetype='llfeeitemtype' "
					     +getWherePart("a.CustomerNo","CustomerNo") 
					     +getWherePart("a.InHospitNo","InHospitNo");

						  turnPage.queryModal(strSql, FeeInfoGrid);
						  
					//计算分费用比例	   
						    var totalfee=0;
								var tempProportion;
								var rowcount=FeeInfoGrid.mulLineCount;
							
								for (i=0;i<=rowcount-1; i++)
								{
							   tempProportion=FeeInfoGrid.getRowColData(i,3)/1;
							   totalfee+= tempProportion     ;
								}
					
								for(i=0;i<=rowcount-1; i++)
								{   			
									tempProportion=FeeInfoGrid.getRowColData(i,3)/totalfee*100;
									tempProportion=Math.round(tempProportion*100)/100;  
									tempProportion=tempProportion+"%";
									FeeInfoGrid.setRowColData(i,4,tempProportion);
								}
								fm.all('TotalFee').value=totalfee;
				  } 
				  else 
				  {
				  	 alert("LHMainCustomerHealth.js->afterquery()出现错误");
				  }
		  }
	 }
		
	</script>
    