<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LOMsgReceiveGrpSetSave.jsp
//程序功能：
//创建日期：2005-03-08 17:08:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	  //接收信息，并作校验处理。
	  //输入参数
		LHMsgCustomGrpSchema tLHMsgCustomGrpSchema = new LHMsgCustomGrpSchema();
		LHMsgCustomGrpDetailSet tLHMsgCustomGrpDetailSet = new LHMsgCustomGrpDetailSet();
		OLOMsgReceiveGrpSetUI	tOLOMsgReceiveGrpSetUI = new OLOMsgReceiveGrpSetUI();
	  //输出参数
	  CErrors tError = null;
	  String tRela  = "";                
	  String FlagStr = "";
	  String Content = "";
	  String transact = "";
	  String GrpNo = "";
	  GlobalInput tG = new GlobalInput(); 
	  tG=(GlobalInput)session.getValue("GI");
		
	  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	  transact = request.getParameter("fmtransact");
	  
	  if(transact.equals("DELETE||MAIN"))
	  {
	  		String GrpNoD = request.getParameter("GrpNo");
	  		tLHMsgCustomGrpSchema.setCustomGrpNo(GrpNoD);
	  }
	  
	  else
	  {
//	  	GrpNo = request.getParameter("grpNo");
//		  if(GrpNo.equals(""))
//			{
//					System.out.println("-------INSERT||MAIN-------");
//					transact = "INSERT||MAIN";
//			}
//			else
//			{
//					transact = "UPDATE||MAIN";
//					System.out.println("-------UPDATE||MAIN-GrpNo:"+GrpNo+"------");
//			}
		  tLHMsgCustomGrpSchema.setCustomGrpNo(request.getParameter("grpNo"));
		  tLHMsgCustomGrpSchema.setCustomGrpName(request.getParameter("CustomerGrpName"));
		  tLHMsgCustomGrpSchema.setContent(request.getParameter("Content"));
		  tLHMsgCustomGrpSchema.setMakeDate(request.getParameter("MakeDate"));
		  tLHMsgCustomGrpSchema.setMakeTime(request.getParameter("MakeTime"));System.out.println("%%%%%%%%%%%%"+tLHMsgCustomGrpSchema.getMakeTime());
		  		  
		  String[] tCustomerNo=request.getParameterValues("LOMsgEachGrpGrid1");
			String[] tGrpContNo=request.getParameterValues("LOMsgEachGrpGrid6");
			String[] tContNo=request.getParameterValues("LOMsgEachGrpGrid7");
			
			int LHMsgCustomGrpDetailCount = 0;
			if (tCustomerNo!= null) LHMsgCustomGrpDetailCount = tCustomerNo.length;
			
			for(int i = 0; i < LHMsgCustomGrpDetailCount; i++)
			{
					LHMsgCustomGrpDetailSchema tLHMsgCustomGrpDetailSchema = new LHMsgCustomGrpDetailSchema();
					tLHMsgCustomGrpDetailSchema.setCustomerNo(tCustomerNo[i]);
					tLHMsgCustomGrpDetailSchema.setGrpContNo(tGrpContNo[i]);
					tLHMsgCustomGrpDetailSchema.setContNo(tContNo[i]);
					
	        tLHMsgCustomGrpDetailSchema.setMakeDate(request.getParameter("MakeDate"));
	        tLHMsgCustomGrpDetailSchema.setMakeTime(request.getParameter("MakeTime"));
	        tLHMsgCustomGrpDetailSet.add(tLHMsgCustomGrpDetailSchema);
			}
		}//else
System.out.println("%%%%%%%%%%%%");
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();System.out.println("+++++++++++++++++++++++");
		tVData.add(tLHMsgCustomGrpSchema);
  	tVData.add(tLHMsgCustomGrpDetailSet); 
  	tVData.add(tG); 
    tOLOMsgReceiveGrpSetUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLOMsgReceiveGrpSetUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
