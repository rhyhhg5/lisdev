<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHEvalueMgtInput.jsp
//程序功能：健康评估上传管理
//创建日期：2006-09-20 14:50:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="LHEvalueMgtInput.js"></SCRIPT>
  
  <%@include file="LHEvalueMgtInit.jsp"%>
  
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        	服务事件任务信息
       	</td>
       		<td style="width:690px;">
		  		</td>
       		<TD  class=common>
						<INPUT VALUE="修改执行状态"   TYPE=button name=update   class=cssbutton onclick="updateClick();">
					</td>
    	</tr>
    </table>
	<Div  id= "divLHEvalueMgtGrid" style= "display: ''">
	 		<Div id="LHCaseInfo" style="display:'' ">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHEvalueMgtGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>
	</Div>
	<table>
	    	<tr>
		    		<td>
		    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
		    		</td>
	    		  <td class= titleImg>
	        		 健康评估信息
	       	  </td>   		 
	    	</tr>
    </table>
   <Div  id= "divLHEvalueMgtGrid" style= "display: ''">
		<table  class= common align='center' >
		  <TR  class= common>   
				<TD class= title >
				  评估问卷类型
				</TD>   		 
				<TD  class= input>
				 	<Input class= 'codename' readonly style="width:40px" name=EvalueType><Input class= 'codeno'  style="width:140px" verify="评估问卷类型|NOTNULL"  name=EvalueTypeName  ondblclick="showCodeList('lhevaluetype',[this,EvalueType],[1,0],null,null,null,'1',220);" codeClear(EvalueType,EvalueTypeName);">
				</TD>
					<TD  class= title>
						起始时间
					</TD>
          <TD  class= input>
             <Input name=StartDate class='coolDatePicker' dateFormat='short' style="width:142px" verify="起始时间|NOTNULL&Date" > 
          </TD> 
          <TD  class= title>
          	终止时间
          </TD>
          <TD  class= input> 
          	<Input name=EndDate class='coolDatePicker' dateFormat='short' style="width:142px" verify="终止时间|NOTNULL&Date" >
          </TD> 
		  </TR>
		</table>
	<table  class= common border=0 width=100%>
			<TR  class= common>
		      	</td>
					<TD  class=common>
						<INPUT VALUE="评估数据导出"   TYPE=button name=EvalueExport   class=cssbutton onclick="submitForm();">
					</td>
					<TD  class=common>
						<INPUT VALUE="评估数据下载"   TYPE=button name=EvalueDown   class=cssbutton onclick="DownEvalueInfo();">
					</td>
					<TD class=title  style="width:700px"></TD>
			</tr>
	</table>
</div>
	<Div  id= "divLHFailGrid" style= "display: 'none'">
<table>
	    	<tr>
		    		<td>
		    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomEvalueGrid);">
		    		</td>
	    		  <td class= titleImg>
	        		 失败客户信息
	       	  </td>   		 
	    	</tr>
    </table>
	<Div  id= "divLHCustomEvalueGrid" style= "display: ''">
	 		<Div id="LHCustomEvalue" style="display:'' ">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHCustomEvalueGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>

	<table  class= common align='center' >
			<TR  class= common>
			<TD  class= title>
				  失败原因
			</TD>
		    <TD  class= input colspan='6'>
		      <textarea class= 'common' rows="3" cols="100" name=FailReason  verify="失败原因|len<=500" ></textarea>
		    </TD>
		    <TD class=title></TD>
		  </TR>
		</table>	
 </Div>
	</Div>
	</Div>
	 <input type=hidden id=queryString name=queryString>
	 <input type=hidden   id="fmtransact" name="fmtransact">
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="TaskExecNo" name="TaskExecNo">
	 <input type=hidden id="ServTaskNo" name="ServTaskNo" VALUE="<%=request.getParameter("ServTaskNo")%>">
	 <input type=hidden id="ServCaseCode" name="ServCaseCode">
	 <input type=hidden id="ServTaskCode" name="ServTaskCode">
	 <input type=hidden id="CustomerNo" name="CustomerNo">
	 <input type=hidden id="ServItemNo" name="ServItemNo">
	 <input type=hidden id="manageComPrt" name="manageComPrt">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
