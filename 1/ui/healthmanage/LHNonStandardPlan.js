//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var prtNo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var StrSql;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput2() == false ) return false;
	if(checkDate() == false) return false;
	for(var i = 0; i < LHServPlanGrid.mulLineCount; i++)
	{
		if(LHServPlanGrid.getRowColData(i, 9) == "")
		{
			alert( "第"+(i+1)+"行服务事件类型未定义，请先定义");
			return false;	
		}	
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.all('CaseState').value="1";
  fm.action="./LHNonStandardPlanSave.jsp";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,inServPlanNo)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		return false;
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		//执行下一步操作
		fm.all('saveButton').disabled = true;
		afterQuery0( inServPlanNo );
		//passQuery();
	}
  	//if(ServPlayType=="1H")
  	//{
  	//	NQuery();
  	//}
  	//else
  	//{
  	//	
  	//}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHServPlan.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}        
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	//下面增加相应的代码
	if(fm.all('ServPlanNo').value == "")
	{
		alert("请先进行保存");	
		return false;
	}  
	if( verifyInput2() == false ) return false;
	if (confirm("您确实想修改该记录吗?"))
  	{
		if(checkDate() == false) return false;
  	
	    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	    fm.all('fmtransact').value = "UPDATE||MAIN"; //fm.fmtransact.value = "UPDATE||MAIN";
	    fm.action="./LHNonStandardPlanSave.jsp";
	    //fm.all('CaseState').value="1";
	    fm.submit(); //提交
	}
    else
    {
      alert("您取消了修改操作！");
    }
} 
function updateClick3()
{
	//下面增加相应的代码
	if( verifyInput2() == false ) return false;
	if (confirm("您确实想修改契约事件状态吗?"))
	{
		if(checkDate() == false) return false;
		
  		var i = 0;
  		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  		
  		//showSubmitFrame(mDebug);
  		fm.all('Name').value = fm.all('CustomerName').value
  		fm.all('fmtransact').value = "UPDATE||MAIN"; //fm.fmtransact.value = "UPDATE||MAIN";
  		fm.all('CaseState').value="2";
  		fm.action="./LHServPlanUpSave.jsp";
  		fm.submit(); //提交
//		  showInfo.close();
	}
	else
	{
	  //mOperate="";
	  alert("您取消了修改操作！");
	}
}  

function afterSubmitUp(FlagStr,content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    	return false;
	}
	else
	{ 
    	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}


        
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  showInfo=window.open("./LHNonStandardPlanQuery.html","信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}                    
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
      
        fm.all('ServPlanNo').value= arrQueryResult; 
        var strSql2 = " select distinct  b.GrpServPlanNo ,b.ContNo,b.CustomerNo,b.Name,b.ServPlanCode,b.ServPlanName,b.ServPlanLevel ,"
                    +" b.ServPlayType,b.ComID,b.StartDate,b.EndDate,b.ServPrem,b.MakeDate,b.MakeTime ,b.CaseState,"
                    +" (case b.ServPlayType when  '1' then  '个人保单' when '2' then  '团体保单' else '非标业务' end)"
                    +" from LHServPlan b where b.ServPlanNo = '"+fm.all('ServPlanNo').value+"'"
					;
      	arrResult=easyExecSql(strSql2);
      	if( arrResult != null ){
      		
      	if(arrResult[0][14] == "1")
      	{
      		fm.all('saveButton').disabled = true;	
      	}
      	if(arrResult[0][14] == "2" || arrResult[0][14] == "3")
      	{
      		fm.all('saveButton').disabled = true;	
      		fm.all('modifyButton').disabled = true;	
      	}
      	//alert(arrResult);
        fm.all('GrpServPlanNo').value=arrResult[0][0];
        fm.all('ContNo').value= arrResult[0][1];
        fm.all('CustomerNo').value= arrResult[0][2];
        fm.all('CustomerName').value= arrResult[0][3];
        fm.all('ServPlanCode').value= arrResult[0][4];
        fm.all('ServPlanName').value= arrResult[0][5];
        fm.all('ServPlanLevel').value= arrResult[0][6];
        fm.all('ServPlanLevelName').value= easyExecSql("select CodeName  from LDCode where codetype = 'servplanlevel'  and code = '"+fm.all('ServPlanLevel').value+"'");
        if(fm.all('ServPlanLevelName').value=="null") fm.all('ServPlanLevelName').value="";
        fm.all('ServPlayType').value= arrResult[0][7];
        fm.all('ServPlayTypeName').value= arrResult[0][15];
        fm.all('ComID').value= arrResult[0][8];
        fm.all('ComID_cn').value= easyExecSql("select showname from ldcom where  sign = '1' and length(trim(comcode)) = 4 and  Comcode = '"+fm.all('ComID').value+"'");
        fm.all('StartDate').value= arrResult[0][9];
        fm.all('EndDate').value= arrResult[0][10];
        fm.all('ServPrem').value= arrResult[0][11];
        fm.all('MakeDate').value= arrResult[0][12];
        fm.all('MakeTime').value= arrResult[0][13];
        fm.all('CaseState').value= arrResult[0][14];
        fm.all('CaseStateName').value= easyExecSql("select  CodeName  from LDCode where codetype = 'lhcasestate' and code = '"+fm.all('CaseState').value+"'");
        //alert(fm.all('CaseState').value);
        var strSql3 = " select distinct  b.ServItemCode ,"
                    +" (select distinct a.servitemname from LHHealthServItem a where a.servitemcode=b.servitemcode),"
                    +" b.ServItemNo,b.ServItemType,"
                    +" (case b.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end),"
                    +" b.ComID,b.ServPriceCode,"
                    +" (select c.servpricename from LHServerItemPrice c where c.servpricecode = b.servpricecode) ,"
                    +" b.ServCaseType "
                    +" from LHServItem b where ServPlanNo = '"+fm.all('ServPlanNo').value+"'"
					+" order by ServItemNo"
					;
        
		turnPage.pageLineNum = 15;
        turnPage.queryModal(strSql3, LHServPlanGrid); 
      }
	}
}               
        
function toServTrace(arrQueryResult)
{
	if(fm.all('ComID').value == "")
	{
			alert("请先填写信息并保存!");
			return false;
	}
	if(LHServPlanGrid.getRowColData(0,3) == "")
	{
			alert("请先保存!");
			return false;
	}
	window.open("./LHServExeTrace.jsp?ServPlanNo="+fm.all('ServPlanNo').value,"","width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto");
}

function checkDate()
{
	if(fm.all('StartDate').value != "" && fm.all('EndDate').value != "")
	{
			if(compareDate(fm.all('StartDate').value,fm.all('EndDate').value) == true)
			{
				alert("服务计划起始时间应早于服务计划结束时间 ");
				return false;
			}
	}
}

function getPersonContCopy()
{
			showInfo=window.open("LHServPlanMain.jsp?ServPlanNo="+fm.all('ServPlanNo').value);
}
//选择类型为医院时的处理
function afterCodeSelect(codeName,Field)
{
	if(codeName=="lhservplanname")
	{
    	var strSql2 = " select servitemcode,servitemname,'',servitemsn,comid,'',''"
					 +" from LHHealthServPlan where ServPlanCode = '"+fm.all('ServPlanCode').value+"'"
					;      											
      	 turnPage.queryModal(strSql2, LHServPlanGrid); 
	}

}

function getOtherNo()
{
	if(fm.all('ServPlayType').value == "3")
	{//是非标业务进入
		if(fm.all('CustomerNo').value == "")
		{
			alert("请先选择客户");
			return false;
		}
		else
		{
			var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + fm.all('CustomerNo').value +"'";
			var arrResult = easyExecSql(strSql);
			if (arrResult == null || arrResult == "null" || arrResult == "") 
		    {
		      alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
		      return false;
		    }	
		}
		
		var sql = " select max(ContNo) from LHServPlan where ContNo like 'Z"+fm.all('CustomerNo').value+"%%' "
		var arrOtherno = easyExecSql(sql);
		if(arrOtherno=="" || arrOtherno==null || arrOtherno=="null")
		{
			fm.all('ContNo').value = "Z"+fm.all('CustomerNo').value+"001";
		}
		else
		{
			var sNo = parseInt(arrOtherno[0][0].toString().substring(10))+1;
			if(sNo.toString().length == "1")
			{
				sNo = "00"+sNo;
			}
			else if(sNo.length == "2")
			{
				sNo = "0"+sNo;
			}
			fm.all('ContNo').value ="Z"+fm.all('CustomerNo').value+ sNo;
		}
	}
}

function ServSetSucced()
{      
	var mulSql4 = "select CaseState from LHServPlan a where a.ServPlanNo='"+fm.all('ServPlanNo').value+"'";
	var arrResult4=new Array;
	arrResult4=easyExecSql(mulSql4);
	var ContNo = "";
	var CustomerNo = "";
	var PrtNo = "";

    if(arrResult4==""||arrResult4=="null"||arrResult4==null)
	{
		alert("请先对事件类型及项目信息进行保存或修改!");
	}
	if(arrResult4=="1")
	{
		updateClick3();//使契约状态改为2
		fm.all('modifyButton').disabled = true;
		ContNo=fm.all('ContNo').value;
		CustomerNo=fm.all('CustomerNo').value;
		PrtNo=getPrtNo();
	}
	if(arrResult4=="2")
	{
		fm.all('saveButton').disabled = true;
		fm.all('modifyButton').disabled = true;
		ContNo=fm.all('ContNo').value;
	    CustomerNo=fm.all('CustomerNo').value;
	    PrtNo=getPrtNo();		
	}
	if(arrResult4=="3")
	{
		fm.all('saveButton').disabled = true;
		fm.all('modifyButton').disabled = true;
		ContNo=fm.all('ContNo').value;
	    CustomerNo=fm.all('CustomerNo').value;
	    PrtNo=getPrtNo();
	}
//	if(fm.all('ServPlayType').value=="3")
//	{
		window.open("./LHContPlanSettingMain.jsp?ContNo="+ContNo+"&PrtNo="+PrtNo+"&CustomerNo="+CustomerNo+"&flag=UCONT","个人服务计划事件设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');  
//	}
//	else
//	{
//		window.open("./LHContPlanSettingMain.jsp?ContNo="+ContNo+"&PrtNo="+PrtNo+"&CustomerNo="+CustomerNo,"个人服务计划事件设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');  
//	}
}

//获取PrtNo
function getPrtNo()
{
	var strSql = "select distinct PrtNo from lccont where ContNo='"+fm.all('ContNo').value+"'";
	prtNo = easyExecSql(strSql);
	return prtNo;
}
function ServExeState()
{
	 window.open("./LHEventTypeSettingMain.jsp?","事件类型自动设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}
function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	
	{  
	  var newWindow = window.open("../sys/LDPersonQuery.html");	  
	}
	
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	    if (arrResult != null) 
	    {
	    	fm.CustomerNo.value = arrResult[0][0];
	    	fm.CustomerName.value = arrResult[0][1];
	    	alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
		}
    	else
    	{
    	  alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
    	}
    }

}

function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}

function queryCustomerNo2()
{	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
   	 var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	   var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
     var arrResult = easyExecSql(strSql);
     if (arrResult != null) 
     {
       fm.CustomerNo.value = arrResult[0][0];
       alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
     }
     else
     {
      //fm.DiseasCode.value="";
      alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}