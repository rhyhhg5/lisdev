<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHScanImportPageSave.jsp
//程序功能：
//创建日期：2006-06-12 10:48:52
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHScanInfoSchema tLHScanInfoSchema   = new LHScanInfoSchema();
  LHScanCodeInfoUI tLHScanCodeInfoUI   = new LHScanCodeInfoUI();
  System.out.println("QQQQQQQQQQQ PPPPPPPPP");
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHScanInfoSchema.setSerialNo(request.getParameter("ScanNo"));//扫描编号（流水号）
    System.out.println("AAAAA AAAAA"+tLHScanInfoSchema.getSerialNo());//关联号1
    tLHScanInfoSchema.setComment(request.getParameter("ScanName")); //扫描件名称
    tLHScanInfoSchema.setInsertState(request.getParameter("ScanStatus"));//扫描状态
    tLHScanInfoSchema.setOtherNoType(request.getParameter("TechPostCode"));//扫描件类型
    tLHScanInfoSchema.setRelationType(request.getParameter("RelationType"));//关联类型
    tLHScanInfoSchema.setRelationNo(request.getParameter("RelationNo"));//关联号1 主键
    System.out.println("AAAAAAAAAA  "+request.getParameter("RelationNo"));//关联号1
    tLHScanInfoSchema.setRelationNo2(request.getParameter("RelationNo2"));//关联号2 主键2
    System.out.println("BBBBBBBBBB  "+request.getParameter("RelationNo2"));//关联号2
    //tLHScanInfoSchema.setContent(request.getParameter("RelationNo"));//关联的客户号
    //System.out.println("CCCCCCCCcc "+request.getParameter("Content"));//关联的客户号
    
    
    tLHScanInfoSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHScanInfoSchema.setMakeTime(request.getParameter("MakeTime"));

	try
  	{
	  	// 准备传输数据 VData
	  	VData tVData = new VData();
		tVData.add(tLHScanInfoSchema);
	  	tVData.add(tG);
	    tLHScanCodeInfoUI.submitData(tVData,transact);
 	}
	catch(Exception ex)
	{
	    Content = "操作失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHScanCodeInfoUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = "操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	    
	      parent.fraInterface.s_afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>
