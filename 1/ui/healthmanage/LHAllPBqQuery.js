//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 

//批单打印下拉框险种条件已经被写死
function initEdorType(cObj)
{
	var tRiskCode = fm.all('RiskCode').value;
	mEdorType = " 1 and RiskCode=#112103# and AppObj=#I#";
	//alert(mEdorType);
	showCodeList('EdorCode',[cObj], null, null, mEdorType, "1");
	//alert('bbb');
}

function actionKeyUp(cObj)
{
	var tRiskCode = fm.all('RiskCode').value;
	mEdorType = " 1 and RiskCode=#112103# and AppObj=#I#";
	//alert(mEdorType);
	showCodeListKey('EdorCode',[cObj], null, null, mEdorType, "1");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
var clickTimes = 0;
function easyQueryClick()
{

	var strSQL = "";
	if (tflag=="0")
	{
		
		strSQL = "  select LPEdorMain.EdorNo, min(LPEdorMain.PolNo), "
		         + "    min(LPEdorMain.InsuredNo), min(LPEdorMain.InsuredName), "
		         + "    sum(LPEdorMain.GetMoney), "
		         + "    decode(max(LPEdorMain.EdorState) || min(LPEdorMain.EdorState), "
		         + "        '00', '保全确认', "
		         + "        '11', '正在申请', "
		         + "        '22', '申请确认', "
		         + "        '21', '部分申请确认', "
		         + "        '20', '部分保全确认') "
		         + "from LPEdorMain "
		         + "where 1=1 "			 
			     + getWherePart( 'LPEdorMain.PolNo','PolNo' )
			     + "group by LPEdorMain.EdorNo ";
	}
	else
	{   
		var tEdorType = "";
		if(fm.all('EdorType').value != "")
		{
			tEdorType = " and a.edorAcceptNo in (select distinct edoracceptno from lpedoritem where edortype = '"+fm.all('EdorType').value+"') "
		}
		strSQL = " select a.EdorAcceptNo, a.otherno, '个单客户号',(select distinct p.name from ldperson p where p.customerno = a.otherno), a.GetMoney, '保全确认' "
				+" from LPEdorApp a where a.otherNoType = '1' and a.EdorState = '0' "
				+ getWherePart("a.ConfDate","EdorStartDate",">=")
				+ getWherePart("a.ConfDate","EdorEndDate","<=")
				+tEdorType
				+" order by edorAcceptNo desc  fetch first 3000 rows only "
				;
		//alert(strSQL);
/*	    var condition = "";
	    if(fm.EdorAppName.value != "")
	    {
	        condition = " and a.edorAppName like '%%" + fm.EdorAppName.value + "%%' "
	    }
	    
		strSQL = "  select a.EdorAcceptNo, a.otherno, '个单客户号', a.EdorAppName, "
		         + "    a.GetMoney, "
		         + "    (select userName "
		         + "    from LDUser c "
		         + "    where c.userCode = b.acceptorno), "
		         + "    (select userName "
		         + "    from LDUser d "
		         + "    where d.userCode = b.Operator), "
		         + "    b.statusNo, "
		         + "    case edorState"
    		     + "        when '0' then '保全确认' "
    		     + "        when '9' then '待收费' "
    		     + "        else '正在申请' "
    		     + "    end "
    		     + "from LPEdorApp a, LGWork b "
    		     + "where a.edorAcceptNo = b.workNo "
    		     + "    and otherNoType = '1'   and a.EdorState in ('0','9') "
                 + getWherePart('a.EdorAcceptNo','EdorAcceptNo')
                 + getWherePart('a.OtherNo','OtherNo')
                 + getWherePart('a.OtherNoType','OtherNoType')
                 + condition
                 + getWherePart('a.AppType','AppType')
                 + getWherePart('a.EdorAppDate','EdorAppDate')
                 + "order by edorAcceptNo desc ";
                 */
	}		
	//查询SQL，返回结果字符串
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  showCodeName();
  
  if(clickTimes == 0)
  {
    clickTimes = clickTimes + 1;
    easyQueryClick();
  }
}


//查询按钮
function QueryClick()
{
	initPolGrid();
	fm.all('Transact').value ="QUERY|EDOR";
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
}



// 项目明细查询
function ItemQuery()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击明细查询按钮。" );
	else
	{
	    var cEdorNo = PolGrid.getRowColData(tSel - 1,1);	
	    var cSumGetMoney = 	PolGrid.getRowColData(tSel - 1,5);			
        var cPolNo = PolGrid.getRowColData(tSel - 1,2);	
        
		if (cEdorNo == ""||cPolNo=="")
		    return;
		    
		window.open("../sys/AllPBqItemQueryMain.jsp?EdorNo=" + cEdorNo + "&SumGetMoney=" + cSumGetMoney+ "&PolNo=" + cPolNo);		
							
	}
}


	//打印批单
function PrtEdor()
{
	var arrReturn = new Array();
				
		var tSel = PolGrid.getSelNo();
			
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击查看按钮。" );
		else
		{
			var state =PolGrid. getRowColData(tSel-1,6) ;
	        
			if (state=="正在申请")
				alert ("所选批单处于正在申请状态，不能打印！");
			else{
			  var EdorAcceptNo=PolGrid. getRowColData(tSel-1,1) ;
				
				window.open("../f1print/AppEndorsementF1PJ1.jsp?EdorAcceptNo="+EdorAcceptNo+"");
				//fm.target="f1print";	
			
			//	fm.submit();

			}
		}
}


function PBqQueryClick()
{
//	
//    arrReturn = getQueryResult();
//	fm.all('EdorNo').value = arrReturn[0][0];
//	fm.all('PolNo').value=arrReturn[0][1];		
	
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	//arrSelected[0] = arrGrid[tRow-1];
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	//alert(arrSelected[0][0]);
	return arrSelected;
}