<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="java.io.*"%>
<%
  System.out.println("start");
  CError cError = new CError();
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";

  String ManageCom = ""; //管理机构
  String MakeDate1 = ""; //入机日期,起始日期
  String MakeDate2 = ""; //入机日期,终止日期
  String AssociateDate = ""; //合作级别日期点
  String statflag = "";

  ManageCom = (String) request.getParameter("ManageCom");
  //if(request.getParameter("MakeDate1") != null){
    MakeDate1 = (String) request.getParameter("MakeDate1");
  //}
  if(request.getParameter("MakeDate2") != null){
    MakeDate2 = (String) request.getParameter("MakeDate2");
  }
  AssociateDate = (String) request.getParameter("AssociateDate");
  statflag = (String) request.getParameter("statflag");

  LDHospitalSchema tLDHospitalSchema = new LDHospitalSchema();
  tLDHospitalSchema.setMakeDate(MakeDate1);
  tLDHospitalSchema.setModifyDate(MakeDate2); //借用修改日期传递入机的终止时间
  tLDHospitalSchema.setLastModiDate(AssociateDate); //借用最后修改日期传递合作时间
  tLDHospitalSchema.setManageCom(ManageCom);
  tLDHospitalSchema.setUrbanOption(statflag); //借用城乡属性字段传递统计方式
  GlobalInput tG = new GlobalInput();

  tG = (GlobalInput) session.getValue("GI");
  VData tVData = new VData();
  VData mResult = new VData();
  CErrors mErrors = new CErrors();
  tVData.addElement(tLDHospitalSchema);
  tVData.addElement(tG);

  HospitalBasicUI tHospitalBasicUI = new HospitalBasicUI();
  XmlExport txmlExport = new XmlExport();
  if (!tHospitalBasicUI.submitData(tVData, "PRINT")) {
    operFlag = false;
    System.out.println("if_one");
    //Content=tCsuRplPrtUI.mErrors.getFirstError().toString();
  }
  else {
    System.out.println("if_two");
    mResult = tHospitalBasicUI.getResult();
    txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);
    if (txmlExport == null) {
      System.out.println("if_three");
      operFlag = false;
      //Content="没有得到要显示的数据文件";
    }
  }

  LDSysVarDB tLDSysVarDB = new LDSysVarDB();
  tLDSysVarDB.setSysVar("VTSFilePath");
  tLDSysVarDB.getInfo();
  String vtsPath = tLDSysVarDB.getSysVarValue();
  if (vtsPath == null)
  {
    vtsPath = "vtsfile/";
  }

  String filePath = application.getRealPath("/").replace('\\', '/') + "/" + vtsPath;
  String fileName = "TASK" + FileQueue.getFileName()+".vts";
  String realPath = filePath + fileName;

	if (operFlag==true)
	{
    //合并VTS文件
    String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);

    //把dataStream存储到磁盘文件
    AccessVtsFile.saveToFile(dataStream, realPath);
    response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath=" + realPath);
	}
	else
	{
    	 FlagStr = "Fail";

%>
<html>
<script language="javascript">
	alert("<%=Content%>");
	//top.close();


</script>
</html>
<%
  	}

%>
