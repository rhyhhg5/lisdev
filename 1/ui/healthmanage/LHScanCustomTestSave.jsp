<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHScanCustomTestSave.jsp
//程序功能：
//创建日期：2006-06-13 08:38:42
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.health.*"%>
<%
//接收信息，并作校验处理。
//输入参数

// System.out.println(" begin Testsave");
String inhospitno="";
String feeno="";
String CustomerNo = "";
String Makedate = "";
String Maketime = "";

LHCustomInHospitalSchema tLHCustomInHospitalSchema = new LHCustomInHospitalSchema();
//LHFeeInfoSchema tLHFeeInfoSchema = new LHFeeInfoSchema();

LHCustomTestSet tLHCustomTestSet = new LHCustomTestSet();
LCPENoticeResultSet tLCPENoticeResultSet = new LCPENoticeResultSet();
OLHCustomTestUI tOLHCustomTestUI = new OLHCustomTestUI();

//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
String transact = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
transact = request.getParameter("fmtransact");


if(transact.equals("INSERT||MAIN"))
{                                  
		String tTestMedicaItemCode[]=request.getParameterValues("CustomTestInfoGrid4"); 
		String tTestResult[]=request.getParameterValues("CustomTestInfoGrid3");         
		String tTestNo[]=request.getParameterValues("CustomTestInfoGrid6");             
		String tUnit[]=request.getParameterValues("CustomTestInfoGrid5");  
		String tIsNormal[] = request.getParameterValues("CustomTestInfoGrid2");        
		
		
		int LHCustomTestCount = 0;                                                     
		if(tTestMedicaItemCode != null )    
		{                                 
				LHCustomTestCount = tTestMedicaItemCode.length;System.out.println("****************"+LHCustomTestCount);
				                                                                           
				for(int i = 0; i < LHCustomTestCount; i++)                                 
				{                                                                          
						LHCustomTestSchema tLHCustomTestSchema = new LHCustomTestSchema();     
						                                                                       
						tLHCustomTestSchema.setCustomerNo(request.getParameter("CustomerNo")); 
					  tLHCustomTestSchema.setInHospitNo(request.getParameter("InHospitNo")); 
					                                                                         
					  tLHCustomTestSchema.setMedicaItemCode(tTestMedicaItemCode[i]);                  
					  tLHCustomTestSchema.setTestResult(tTestResult[i]);                   
			//		  tLHCustomTestSchema.setDoctNo(request.getParameter("DoctNo"));         
					  tLHCustomTestSchema.setTestDate(request.getParameter("InHospitDate")); 
		//			  tLHCustomTestSchema.setTestNo(tTestNo[i]);                           
					  tLHCustomTestSchema.setResultUnitNum(tUnit[i]);	
					  tLHCustomTestSchema.setIsNormal(tIsNormal[i]);
					                                                                         
					  tLHCustomTestSchema.setOperator(request.getParameter("Operator"));     
					  tLHCustomTestSchema.setMakeDate(request.getParameter("MakeDate"));     
					  tLHCustomTestSchema.setMakeTime(request.getParameter("MakeTime"));     
					  tLHCustomTestSchema.setModifyDate(request.getParameter("ModifyDate")); 
					  tLHCustomTestSchema.setModifyTime(request.getParameter("ModifyTime")); 
					                                                                         
					  tLHCustomTestSet.add(tLHCustomTestSchema);
				}   
		}
}

	String tDiseaseCode[] = request.getParameterValues("TestResultGrid1");
	String tDiseaseName[] = request.getParameterValues("TestResultGrid2");
	
	int TestResultCount = 0;
	if(tDiseaseCode != null)
	{
			TestResultCount = tDiseaseCode.length;
			for(int i = 0; i < TestResultCount; i++)                                 
			{
					LCPENoticeResultSchema tLCPENoticeResultSchema = new LCPENoticeResultSchema();
					tLCPENoticeResultSchema.setProposalContNo(request.getParameter("CustomerNo"));
					tLCPENoticeResultSchema.setICDCode(tDiseaseCode[i]);
					tLCPENoticeResultSchema.setSortDepart("02");
					
					tLCPENoticeResultSchema.setOperator(request.getParameter("Operator"));     
					tLCPENoticeResultSchema.setMakeDate(request.getParameter("MakeDate"));     
					tLCPENoticeResultSchema.setMakeTime(request.getParameter("MakeTime"));     
					tLCPENoticeResultSchema.setModifyDate(request.getParameter("ModifyDate")); 
					tLCPENoticeResultSchema.setModifyTime(request.getParameter("ModifyTime")); 
					
					tLCPENoticeResultSet.add(tLCPENoticeResultSchema);
			}
	}


	tLHCustomInHospitalSchema.setCustomerNo(request.getParameter("CustomerNo"));
	tLHCustomInHospitalSchema.setIsPhysicalExam("1");
	tLHCustomInHospitalSchema.setInHospitNo(request.getParameter("InHospitNo"))   ;
	tLHCustomInHospitalSchema.setInHospitMode(request.getParameter("TestModeCode"));
	tLHCustomInHospitalSchema.setInHospitDate(request.getParameter("InHospitDate"));
	tLHCustomInHospitalSchema.setHospitCode(request.getParameter("HospitCode"));
	
	
	
	tLHCustomInHospitalSchema.setOperator(request.getParameter("Operator"));
	tLHCustomInHospitalSchema.setMakeDate(request.getParameter("MakeDate"));
	tLHCustomInHospitalSchema.setMakeTime(request.getParameter("MakeTime"));
	tLHCustomInHospitalSchema.setModifyDate(request.getParameter("ModifyDate"));
	tLHCustomInHospitalSchema.setModifyTime(request.getParameter("ModifyTime"));
	
	//tLHFeeInfoSchema.setCustomerNo(request.getParameter("CustomerNo"));
	//tLHFeeInfoSchema.setFeeCode("5");
	//tLHFeeInfoSchema.setFeeAmount(request.getParameter("TestFee"));
	//tLHFeeInfoSchema.setInHospitNo(request.getParameter("InHospitNo"));
	//tLHFeeInfoSchema.setFeeNo(request.getParameter("FeeNo"));
	
	//tLHFeeInfoSchema.setOperator(request.getParameter("Operator"));
	//tLHFeeInfoSchema.setMakeDate(request.getParameter("MakeDate"));
	//tLHFeeInfoSchema.setMakeTime(request.getParameter("MakeTime"));
	//tLHFeeInfoSchema.setModifyDate(request.getParameter("ModifyDate"));
	//tLHFeeInfoSchema.setModifyTime(request.getParameter("ModifyTime"));
	
	if(transact.equals("UPDATE||MAIN") || transact.equals("DELETE||MAIN"))
	{
	  String tTestMedicaItemCode[]=request.getParameterValues("CustomTestInfoGrid4");
	  String tTestResult[]=request.getParameterValues("CustomTestInfoGrid3");
	  String tTestNo[]=request.getParameterValues("CustomTestInfoGrid6");
	  String tUnit[]=request.getParameterValues("CustomTestInfoGrid5");
 		String tIsNormal[] = request.getParameterValues("CustomTestInfoGrid2");        
 		
	  int LHCustomTestCount = 0;
	  if (tTestMedicaItemCode != null)
	    LHCustomTestCount = tTestMedicaItemCode.length;
	  
	  for (int i = 0; i < LHCustomTestCount; i++) {
	    LHCustomTestSchema tLHCustomTestSchema = new LHCustomTestSchema();
	    tLHCustomTestSchema.setCustomerNo(request.getParameter("CustomerNo"));
	    tLHCustomTestSchema.setInHospitNo(request.getParameter("InHospitNo"));
	    System.out.println("############savepage ready"+tLHCustomTestSchema.getInHospitNo());
	    tLHCustomTestSchema.setMedicaItemCode(tTestMedicaItemCode[i]);
	    tLHCustomTestSchema.setTestResult(tTestResult[i]);
	  //  tLHCustomTestSchema.setDoctNo(request.getParameter("DoctNo"));
	    tLHCustomTestSchema.setTestDate(request.getParameter("InHospitDate"));
	    tLHCustomTestSchema.setTestNo(tTestNo[i]);
	    tLHCustomTestSchema.setResultUnitNum(tUnit[i]);
			tLHCustomTestSchema.setIsNormal(tIsNormal[i]);  
	  
	    tLHCustomTestSchema.setOperator(request.getParameter("Operator"));
	    tLHCustomTestSchema.setMakeDate(request.getParameter("MakeDate"));
	    tLHCustomTestSchema.setMakeTime(request.getParameter("MakeTime"));
	    tLHCustomTestSchema.setModifyDate(request.getParameter("ModifyDate"));
	    tLHCustomTestSchema.setModifyTime(request.getParameter("ModifyTime"));
	  	
	  
	    tLHCustomTestSet.add(tLHCustomTestSchema);
	    }
	}
	
	
	try {
	//System.out.println(tLHCustomTestSet.size());
	  // 准备传输数据 VData
	  VData tVData = new VData();
	  tVData.add(tLHCustomInHospitalSchema);
	  //tVData.add(tLHFeeInfoSchema);
	  tVData.add(tLHCustomTestSet);
	  tVData.add(tLCPENoticeResultSet);
	  tVData.add(tG);
	
	  tOLHCustomTestUI.submitData(tVData,transact);
	  
	} catch(Exception ex) {
	  Content = "操作失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
	}
	
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="") 
	{
	  tError = tOLHCustomTestUI.mErrors;
	  if (!tError.needDealError()) 
	  {
	    Content = " 操作成功! ";
	    FlagStr = "Success";
	//    System.out.println("222222222222222222"+FlagStr+"222222222222222222");
	  } else 
	  {
	    Content = " 操作失败，原因是:" + tError.getFirstError();
	    FlagStr = "Fail";
	  }
	}
	if ( FlagStr.equals("Success") && !transact.equals("DELETE||MAIN")) 
	{ 
	
					VData res = tOLHCustomTestUI.getResult();
			  	System.out.println("············"+transact.toString());
			  	if(transact.equals("DELETE||MAIN"))
			  	{
			  			System.out.println("^^^^^^^^^^^^^^"+transact.toString());
			  	}
			   
			    LHCustomInHospitalSchema mLHCustomInHospitalSchema = new LHCustomInHospitalSchema();
					mLHCustomInHospitalSchema.setSchema((LHCustomInHospitalSchema) res.
			                                        getObjectByObjectName("LHCustomInHospitalSchema",
			                                                              0));
			                                                              
					//LHFeeInfoSchema mLHFeeInfoSchema=new LHFeeInfoSchema();
					//mLHFeeInfoSchema.setSchema((LHFeeInfoSchema) res.
			                                        //getObjectByObjectName("LHFeeInfoSchema",
			                                                             // 0));
					
							System.out.println("CustomerNo: "+(String)mLHCustomInHospitalSchema.getCustomerNo());

			if(transact.equals("INSERT||MAIN"))
			{
					CustomerNo = (String)mLHCustomInHospitalSchema.getCustomerNo(); 
					
			}
					//feeno=(String )mLHFeeInfoSchema.getFeeNo();
			    inhospitno = (String)mLHCustomInHospitalSchema.getInHospitNo();
			    System.out.println("inhospitno: "+inhospitno);
			    Makedate = (String)mLHCustomInHospitalSchema.getMakeDate();
					Maketime = (String)mLHCustomInHospitalSchema.getMakeTime();
	}
	
	//添加各种预处理
	%>
	<%=Content%>
	   <html>
	   <script language="javascript">
			
			<%
			  System.out.println("sdfdsgdfhg   "+inhospitno);
			  if(inhospitno.equals("")||transact.equals("DELETE||MAIN")) 
			  { 
		  %>
			     parent.fraInterface.fm.all("InHospitNo").value="";
			<%
			  } 
			  else 
			  {
		  %>
			  parent.fraInterface.fm.all("InHospitNo").value = "<%=inhospitno%>";
			  parent.fraInterface.fm.all("tempCustomerno").value = "<%=CustomerNo%>";
			  parent.fraInterface.fm.all("MakeDate").value = "<%=Makedate%>";
			  parent.fraInterface.fm.all("MakeTime").value = "<%=Maketime%>";
			  parent.fraInterface.fms.all("RelationNo").value = parent.fraInterface.fm.all("CustomerNo").value;
	      parent.fraInterface.fms.all("RelationNo2").value = "<%=inhospitno%>";	
			<%
			  System.out.println(inhospitno);
			  }
	    %>
				parent.fraInterface.txtSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
	</html>
