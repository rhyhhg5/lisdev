<%
//程序名称：LHHealthServPlanInputInit.jsp
//程序功能：个人服务计划内容定义(初始化页面)
//创建日期：2006-03-20 10:15:30
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                             
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('Riskcode').value = "";
    fm.all('Riskname').value = "";
    fm.all('ServPlanLevelCode').value = "";
    fm.all('ServPlanLevelName').value = "";
    fm.all('ComIDCode').value = "";
    fm.all('ComIDName').value = "";
    fm.all('ServPlayType').value = "1";
    fm.all('FeeUpLimit').value = "";
    fm.all('FeeLowLimit').value = "";
    fm.all('PlanType').value = "";
    fm.all('PlanTypeName').value = "";
    
  }
  catch(ex)
  {
    alert("在LHHealthServPlanInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                        
function initForm()
{
  try
  {
    initInpBox();
  //  getFirstPageNo();
    initLHFeeGrid()
    initLHHealthServPlanGrid();
  }
  catch(re)
  {
    alert("LHHealthServPlanInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}  

	
function initLHHealthServPlanGrid() 
{                            
	var iArray = new Array();                               
                                                           
	try 
    {                                                   
    	iArray[0]=new Array();                                
    	iArray[0][0]="序号";         		//列名               
    	iArray[0][1]="30px";         		//列名               
    	iArray[0][3]=0;         				//列名                 
    	iArray[0][4]="";         //列名   
    
	    iArray[1]=new Array(); 
		iArray[1][0]="标准服务项目代码";   
		iArray[1][1]="167px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
	    iArray[1][4]="hmservitemcode";         //列名  
	    iArray[1][5]="1|2";    //引用MulLine的对应第几列，'|'为分割符 
		iArray[1][6]="0|1";    //上面的列中放置下拉菜单中第几位值 
		iArray[1][15]="ServItemCode"; 
		iArray[1][17]="1"; 
		iArray[1][18]="310";     //下拉框的宽度 
		iArray[1][19]="1" ;      //强制刷新数据源
  
	    iArray[2]=new Array(); 
		iArray[2][0]="标准服务项目名称";   
		iArray[2][1]="167px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="hmservitemcode";         //列名  
	    iArray[2][5]="2|1";    //引用MulLine的对应第几列，'|'为分割符 
		iArray[2][6]="1|0";    //上面的列中放置下拉菜单中第几位值 
		iArray[2][15]="ServItemName"; 
		iArray[2][17]="1"; 
		iArray[2][18]="310";     //下拉框的宽度 
		iArray[2][19]="1" ;      //强制刷新数据源
		
			
		iArray[3]=new Array(); 
		iArray[3][0]="服务项目序号";   
		iArray[3][1]="167px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;
     
     
    	LHHealthServPlanGrid = new MulLineEnter( "fm" , "LHHealthServPlanGrid" );                         
    	//这些属性必须在loadMulLine前                                                                                              
    	                                                                                                          
    	LHHealthServPlanGrid.mulLineCount = 0;                                          
    	LHHealthServPlanGrid.displayTitle = 1;                                          
    	LHHealthServPlanGrid.hiddenPlus = 0;                                            
    	LHHealthServPlanGrid.hiddenSubtraction = 0;                                     
    	LHHealthServPlanGrid.canSel = 1;                                                
                                                   
    	LHHealthServPlanGrid.loadMulLine(iArray);                                       
    	//这些操作必须在loadMulLine后面                                          
  }                                                                          
  catch(ex) {                                                                
    alert(ex);                                                               
  }                                                                          
}                                                                        
    
    
    
function initLHFeeGrid() 
{                            
	var iArray = new Array();                               
                                                           
	try 
	{                                                   
		iArray[0]=new Array();                                
		iArray[0][0]="序号";         		//列名               
		iArray[0][1]="30px";         		//列名               
		iArray[0][3]=0;         				//列名                 
		iArray[0][4]="";         //列名   
    
		iArray[1]=new Array(); 
		iArray[1][0]="保费下限";   
		iArray[1][1]="150px";   
		iArray[1][2]=12;        
		iArray[1][3]=1;
		//iArray[1][9]="保费下限|INT";
  
	  iArray[2]=new Array(); 
		iArray[2][0]="保费上限";   
		iArray[2][1]="150px";   
		iArray[2][2]=12;        
		iArray[2][3]=1;
		
		iArray[3]=new Array(); 
		iArray[3][0]="Level";   
		iArray[3][1]="0px";   
		iArray[3][2]=12;        
		iArray[3][3]=3;

     
		LHFeeGrid = new MulLineEnter( "fm" , "LHFeeGrid" );                         
		//这些属性必须在loadMulLine前                                                                                              
                                                                                                              
    	LHFeeGrid.mulLineCount = 0;                                          
    	LHFeeGrid.displayTitle = 1;                                          
    	LHFeeGrid.hiddenPlus = 0;                                            
    	LHFeeGrid.hiddenSubtraction = 0;                                     
    	LHFeeGrid.canSel = 1;                                                
		LHFeeGrid.selBoxEventFuncName = "showOneFee";                
    	                                        
    	LHFeeGrid.loadMulLine(iArray);                                       
    	//这些操作必须在loadMulLine后面                                          
  }                                                                          
  catch(ex) {                                                                
    alert(ex);                                                               
  }                                                                          
}  
      
      
</script>
  