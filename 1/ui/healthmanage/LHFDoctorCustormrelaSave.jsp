<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHFDoctorCustormrelaSave.jsp
//程序功能：
//创建日期：2005-01-17 15:00:30
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LHFDoctorCustormrelaSchema tLHFDoctorCustormrelaSchema   = new LHFDoctorCustormrelaSchema();
  LHFDoctorCustormrelaUI tLHFDoctorCustormrelaUI   = new LHFDoctorCustormrelaUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHFDoctorCustormrelaSchema.setSerialNo(request.getParameter("SerialNo"));
    tLHFDoctorCustormrelaSchema.setFamilyDoctorNo(request.getParameter("FamilyDoctorNo"));
    tLHFDoctorCustormrelaSchema.setFamilyDoctorNo(request.getParameter("FamilyDoctorNo"));
    tLHFDoctorCustormrelaSchema.setFamilyDoctorName(request.getParameter("FamilyDoctorName"));
    tLHFDoctorCustormrelaSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLHFDoctorCustormrelaSchema.setName(request.getParameter("Name"));
    tLHFDoctorCustormrelaSchema.setFDSStart(request.getParameter("FDSStart"));
    tLHFDoctorCustormrelaSchema.setFDSEnd(request.getParameter("FDSEnd"));
    tLHFDoctorCustormrelaSchema.setOperator(request.getParameter("Operator"));
    tLHFDoctorCustormrelaSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHFDoctorCustormrelaSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHFDoctorCustormrelaSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHFDoctorCustormrelaSchema.setModifyTime(request.getParameter("ModifyTime"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLHFDoctorCustormrelaSchema);
  	tVData.add(tG);
    tLHFDoctorCustormrelaUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHFDoctorCustormrelaUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
