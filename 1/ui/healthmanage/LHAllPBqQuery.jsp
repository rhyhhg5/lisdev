<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%

	String tPolNo = "";
	try
	{
		tPolNo = request.getParameter("PolNo");
	}
	catch( Exception e )
	{
		tPolNo = "";
	}
	String tflag = "";
		try
	{
		tflag = request.getParameter("flag");

	}
	catch( Exception e )
	{
		tflag = "";
	}
%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>   
<Script>
var tflag = "<%=tflag%>"
var tPolNo = "<%=tPolNo%>"
var comCode = <%=tG.ComCode%>
</Script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LHAllPBqQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHAllPBqQueryInit.jsp"%>
  <title>保全查询 </title>
</head>
<body  onload="initForm();">
  <form action="./AllPBqQuerySubmit.jsp" method=post name=fm target="fraSubmit" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入个人保全查询条件：
			</td>
		</tr>
	</table>
	
    <table  class= common align=center>
		<TR  class= common>
            <td class=title>
				保全业务
            </td>
            <td class= input>
                <Input class="codeno"  name=EdorType ondblClick="showCodeList('edorcodeforview',[this,EdorTypeName],[0,1]);"><Input class="codename" name=EdorTypeName >
            </td>
			<TD  class= title>
                保全结案起期
            </TD>
            <TD  class= input>
                <Input class= "coolDatePicker" dateFormat="short" name=EdorStartDate >
            </TD>
            <TD  class= title>
                保全结案止期
            </TD>
            <TD  class= input>
                <Input class= "coolDatePicker" dateFormat="short" name=EdorEndDate >
            </TD>            
        </TR>     
      	<!--TR  class= common>
            <td class=title>
            保全受理号
            </td>
            <td class= input>
                <Input type="input" class="common" name=EdorAcceptNo>
            </td>
            <td class=title>
                   客户号
            </td>
            <td class= input>
                <Input class="common"  name=OtherNo>
            </td>
            <td class=title style="display: none">
                    号码类型
            </td>
            <td class= input style="display: none">
                <Input type="input" class="code" name=OtherNoType value=1 CodeData="0|^1|个人客户号^3|个人保单号" ondblClick="showCodeListEx('111',[this,OtherNoType],[0,0]);" onkeyup="showCodeListKeyEx('111',[this,OtherNoType],[0,0]);">
            </td>
            <td class=title>
                    申请人姓名
            </td>
            <td class= input>
                <Input type="input" class="common" name=EdorAppName>
            </td>
            <td class=title>
                申请方式
            </td>
            <td class= input >
                <Input type="input" class="codeno" name=AppType CodeData="0|^1|客户上门办理^2|业务员代办^3|其它人代办^4|信函^5|电话申请^6|部门转办" ondblClick="showCodeListEx('AppType',[this,AppTypeName],[0,1]);" onkeyup="showCodeListKeyEx('AppType',[this,AppTypeName],[0,1]);"><input class =codename name = AppTypeName>
            </td>
            <TD  class= title>
                保全申请日期
            </TD>
            <TD  class= input>
                <Input class= "coolDatePicker" dateFormat="short" name=EdorAppDate >
            </TD>
        </TR-->
    </table>

          <INPUT VALUE="查询" class=cssButton TYPE=button onclick="easyQueryClick();"> 

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 保全信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table class= common align=center>
    		<tr class= common>
    			<td  class= input>
    				<INPUT class=cssButton VALUE="明细查询" TYPE=button onclick="PrtEdor();">     			
    			</td>
    		
    		</tr>
    	</table>
    	<input type=hidden name=Transact >
      <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage(); showCodeName();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage(); showCodeName();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage(); showCodeName();"> 
      <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage(); showCodeName();">				
  	</div>
  	 <input type=hidden id ="RiskCode" name = "RiskCode">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
