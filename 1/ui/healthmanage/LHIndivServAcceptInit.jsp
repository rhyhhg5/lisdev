<%
//程序名称：LHIndivServAcceptInit.jsp
//程序功能：
//创建日期：2006-08-24 10:47:23
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类--> 
<%
        String ServAccepNo = request.getParameter("ServAccepNo");
        String testFlag = request.getParameter("test");
%>                    
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('CustomerNo').value = "";
    fm.all('CustomerName').value = "";
    //fm.all('ServAccepChannel').value = "";
    //fm.all('ServAccepChannelName').value = "";
    fm.all('ServItemCode').value = "";
    fm.all('ServItemName').value = "";
    fm.all('ServAceptDate').value = "";
    fm.all('ServAccepNo').value = "";
    fm.all('ServCaseCode').value = "";
    fm.all('TaskModelType').value = "";
    fm.all('ModelTypeNo').value = "";
    fm.all('ServFinDate').value = "";
    fm.all('ServTaskCode').value = "00401";
    fm.all('ServTaskName').value = "体检申请受理";
    
  }
  catch(ex)
  {
    alert("在LHIndivServAcceptInit.jsp->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    initLHServCaseGrid() ;
    initLHNoteSendTime();
    divLHBespeakServAccdeptGrid.style.display='none';
		divLHNoteServAccdeptGrid.style.display='none';
		divLHOtherServAccdeptGrid.style.display='none';
		divLHTaskSettingFGrid.style.display='';
		fm.all('ServAceptDate').value =Date1;
    var testFlag="<%=testFlag%>";
    //alert(testFlag);
    if(testFlag=="1")//从服务事件实施管理的一般任务管理有服务受理信息的操作
    {
    	 initAcceptInfo();
    	 fm.all('SaveButton').disabled=true;
    	 fm.all('QueryButton').disabled=true;
    	 fm.all('CaseQuery').disabled=true;
    	 fm.all('ServTaskSend').disabled=true;
    	 fm.all('DeleteButton').disabled=true;
    }
  }
  catch(re)
  {
    alert("LHIndivServAcceptInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initAcceptInfo()
{
	var acceptNo="<%=ServAccepNo%>";
	var arrResult = new Array();
	if( acceptNo != null )
	{
    		fm.all('ServAccepNo').value=acceptNo;
				var mulSql = " select  "
				            +" a.CustomerNo,"
				            +" (select d.Name from ldperson d where d.CustomerNo=a.CustomerNo),"
				            +"  a.ServAccepChannel ,b.ServItemCode ,"
				            +" ( select distinct c.ServItemName from   LHHealthServItem c  where  c.ServItemCode = b.ServItemCode)," 
				            +"  a.ServAceptDate ,a.ServAccepNo ,a.ServCaseCode,"
				            +" a.ModelTypeNo,a.ServTaskCode, a.TaskModelType ,a.BespeakAcceptType,A.SERVTASKCODE "
										+"  from LHIndiServAccept a ,LHServCaseRela b  "
										+"  where a.ServCaseCode=b.ServCaseCode and  "
										+"  a.ServItemCode=b.ServItemCode and  "
										+" a.ServAccepNo ='"+ acceptNo+"' ";
       //alert(mulSql);
       arrResult=easyExecSql(mulSql);
       //alert(arrResult);
       if(arrResult!=""&&arrResult!=null&&arrResult!="null")
       {
		      fm.all('CustomerNo').value			= arrResult[0][0];
		      fm.all('CustomerName').value			= arrResult[0][1];
          fm.all('ServAccepChannel').value			= arrResult[0][2];
          fm.all('ServAccepChannelName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhacceptchannel' and d.code='"+ fm.all('ServAccepChannel').value +"'");
          fm.all('ServItemCode').value      = arrResult[0][3];
          fm.all('ServItemName').value			= arrResult[0][4];
          fm.all('ServAceptDate').value	= arrResult[0][5];
          fm.all('ServAccepNo').value				= arrResult[0][6];
          fm.all('ServCaseCode').value			= arrResult[0][7];
          fm.all('ModelTypeNo').value			= arrResult[0][8];
          //fm.all('ServTaskCode').value			= arrResult[0][9];
          //fm.all('ServTaskName').value			= easyExecSql("select  ServTaskName from LHServTaskDef where LHServTaskDef.ServTaskCode='"+ fm.all('ServTaskCode').value +"'");
          fm.all('TaskModelType').value       = arrResult[0][10];
          fm.all('BespeakAcceptType').value       = arrResult[0][11];
          fm.all('ServTaskNO').value       = arrResult[0][12];
          fm.all('BespeakAcceptTypeName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhbespeaktype' and d.code='"+ fm.all('BespeakAcceptType').value +"'");
          fm.all('ServFinDate').value			= arrResult[0][5];
           if(fm.all('TaskModelType').value=="1")
           {
           	   if(fm.all('BespeakAcceptType').value=="1")
           	   {
           	   	  fm.all('BespeakAcceptTypeName').ondblclick = "";
           	      //fm.all('ServTaskName').value="健康体检预约受理";
           	   }
           	   if(fm.all('BespeakAcceptType').value=="2")
           	   {
           	   	  fm.all('BespeakAcceptTypeName').ondblclick = "";
           	      //fm.all('ServTaskName').value="绿色通道预约受理";
           	   }
           	   var ssSql = "select a.CusServRequest "
										+" from LHIndiServAccept a where "
										+" a.ModelTypeNo ='"+fm.all('ModelTypeNo').value+"'"
										+" and a.TaskModelType='"+ fm.all('TaskModelType').value + "'";
                //alert(ssSql);
               //alert(easyExecSql(ssSql));
               var arrRR=easyExecSql(ssSql);
               if(arrRR!=""&&arrRR!=null&&arrRR!="null")
               {
                  fm.all('CusOtherRequest').value 		= arrRR[0][0]; 
               }
           }
           if(fm.all('TaskModelType').value=="3")
           {
           	  var ssSql = "select a.CusServRequest "
										+" from LHIndiServAccept a where "
										+" a.ModelTypeNo ='"+fm.all('ModelTypeNo').value+"'"
										+" and a.TaskModelType='"+ fm.all('TaskModelType').value + "'";
               var arrRR=easyExecSql(ssSql);
               if(arrRR!=""&&arrRR!=null&&arrRR!="null")
               {
                 fm.all('CusServRequest').value 		= arrRR[0][0]; 
               }
           }
           var mulSql2 = "select a.ServCaseCode,"
                   +"(select d.ServCaseName from LHServCaseDef d where d.ServCaseCode=a.ServCaseCode),"
                   +" a.ServAccepNo "
				   				 +" from LHIndiServAccept a "
				   				 +" where a.ServAccepNo ='"+ acceptNo +"'"
				   				;
				  turnPage.queryModal(mulSql2, LHServCaseGrid);     
				  var mulSql3 = " select a.TaskModelType "
										+"  from LHIndiServAccept a "
										+"  where a.ServAccepNo ='"+ acceptNo+"' "; 
				  var modelType=easyExecSql(mulSql3);
				  if(modelType=="1")//受理类型为预约服务受理
				  {
				  	  divLHBespeakServAccdeptGrid.style.display='';
				  	  divLHOtherServAccdeptGrid.style.display='none';
				  	  var mulSql4 = " select a.TaskModelType ,a.ModelTypeNo,"
				  	        +"  a.BespeakComID, a.BespeakAcceptType ,"
				  	        +" BespeakDateRequest ,CusServRequest"
										+"  from LHIndiServAccept a "
										+"  where a.ServAccepNo ='"+acceptNo+"' "; 
				      var arrResult4=new Array;
				      arrResult4=easyExecSql(mulSql4);
						  if(arrResult4!=""&&arrResult!=null&&arrResult!="null")
              {
								  fm.all('TaskModelType').value			= arrResult4[0][0];
								  fm.all('BespeakAcceptTypeName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhbespeaktype' and d.code='"+ fm.all('TaskModelType').value +"'");
		              fm.all('ModelTypeNo').value			= arrResult4[0][1];
                  fm.all('HospitCode').value			= arrResult4[0][2];
                  fm.all('HospitName').value=easyExecSql("select  HospitName from LDHospital where LDHospital.HospitCode='"+ fm.all('HospitCode').value +"'");
                  fm.all('BespeakAcceptType').value			= arrResult4[0][3];
		              fm.all('BespeakDateRequest').value			= arrResult4[0][4];
		              fm.all('BespeakAcceptTypeName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhbespeaktype' and d.code='"+ fm.all('BespeakAcceptType').value +"'");
                  fm.all('CusOtherRequest').value			= arrResult4[0][5];
				      }
				   }
				  if(modelType=="3")//受理类型为其它服务受理
				  {
				  	  divLHOtherServAccdeptGrid.style.display='';
				  	  divLHBespeakServAccdeptGrid.style.display='none';
				  }
				}
	} 
}
function testNullAccept()
{
	
}
		var d = new Date();
    var h = d.getYear();
    var m = d.getMonth(); 
    var day = d.getDate();  
    var Date1;       
    if(h<10){h = "0"+d.getYear();}  
    if(m<9){ m++; m = "0"+m;}
    else{m++;}
    if(day<10){day = "0"+d.getDate();}
    Date1 = h+"-"+m+"-"+day;
var LHServCaseGrid;
function initLHServCaseGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
	  iArray[1]=new Array(); 
		iArray[1][0]="服务事件编号";   
		iArray[1][1]="66px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
	  iArray[1][4]="lhservcasecode";         //列名  
	  iArray[1][5]="1|2";    //引用MulLine的对应第几列，'|'为分割符 
		iArray[1][6]="0|1";    //上面的列中放置下拉菜单中第几位值 
		iArray[1][15]="ServCaseCode"; 
		iArray[1][17]="1"; 
		iArray[1][18]="330";     //下拉框的宽度 
		iArray[1][19]="1" ;      //强制刷新数据源
  
	  iArray[2]=new Array(); 
		iArray[2][0]="服务事件名称";   
		iArray[2][1]="66px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="lhservcasecode";         //列名  
	  iArray[2][5]="2|1";    //引用MulLine的对应第几列，'|'为分割符 
		iArray[2][6]="1|0";    //上面的列中放置下拉菜单中第几位值 
		iArray[2][15]="ServCaseName"; 
		iArray[2][17]="1"; 
		iArray[2][18]="330";     //下拉框的宽度 
		iArray[2][19]="1" ;      //强制刷新数据源
		
	  iArray[3]=new Array();          
		iArray[3][0]="ServAccepNo";
		iArray[3][1]="0px";             
		iArray[3][2]=2;                 
		iArray[3][3]=3;            
	  

	                               
    LHServCaseGrid = new MulLineEnter( "fm" , "LHServCaseGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHServCaseGrid.mulLineCount = 0;                             
    LHServCaseGrid.displayTitle = 1;                          
    LHServCaseGrid.hiddenPlus = 1;                          
    LHServCaseGrid.hiddenSubtraction = 1;                          
    LHServCaseGrid.canSel = 1;                          
    LHServCaseGrid.canChk = 0;                          
    LHServCaseGrid.selBoxEventFuncName = "showOne";                       
                                                             
    LHServCaseGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHServCaseGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}

var LHNoteSendTime;
function initLHNoteSendTime() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="定时发送日期";   
	  iArray[1][1]="100px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
	  iArray[1][14]=Date1;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="具体时间";   
	  iArray[2][1]="110px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=2;
	  iArray[2][4]="lhmaterialdate";               
	  iArray[2][5]="2|4";     //引用代码对应第几列，'|'为分割符
    iArray[2][6]="1|0";     //上面的列中放置引用代码中第几位
    iArray[2][17]="1"; 
    iArray[2][18]="260";
    iArray[2][19]="1" ;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="具体时刻";   
	  iArray[3][1]="110px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=2;
	  iArray[3][4]="lhmaterialtime";               
	  iArray[3][5]="3|5";     //引用代码对应第几列，'|'为分割符
    iArray[3][6]="1|0";     //上面的列中放置引用代码中第几位
    iArray[3][17]="1"; 
    iArray[3][18]="260";
    iArray[3][19]="1" ;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="materialdate";   
	  iArray[4][1]="10px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=3;
	  
	  iArray[5]=new Array(); 
	  iArray[5][0]="materialtime";   
	  iArray[5][1]="10px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=3;
	                               
    LHNoteSendTime = new MulLineEnter( "fm" , "LHNoteSendTime" );           
    //这些属性必须在loadMulLine前                          
                          
    LHNoteSendTime.mulLineCount = 0;                             
    LHNoteSendTime.displayTitle = 1;                          
    LHNoteSendTime.hiddenPlus = 0;                          
    LHNoteSendTime.hiddenSubtraction = 0;                          
    LHNoteSendTime.canSel = 0;                          
    LHNoteSendTime.canChk = 1;                          
             
                                                             
    LHNoteSendTime.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHNoteSendTime.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}
function afterCodeSelect(codeName,Field)
{
}
 
</script>
