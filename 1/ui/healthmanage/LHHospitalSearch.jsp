<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHHosptalSearch.jsp
//程序功能：
//创建日期：2006-05-24 14:25:18
//创建人  ：CrtHtml程序创建
//更新记录：
//更新人 :郭丽颖
//更新日期 :   2006-06-13 11:00:18
//更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHHospitalSearch.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHHospitalSearchInit.jsp"%>
</head>

<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
  var msql = "1 and char(length(trim(comcode)))=#8#";
</script>

<body onload="initForm(); initElementtype();" >
  <form method=post name=fm target="fraSubmit">
     <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDHospital1);">
    		</td>
    		 <td class= titleImg>
        		 一般查询条件
       	 </td>   		 
    	</tr>
    </table>
   
   <Div  id= "divLDHospital1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      医疗服务机构类型
    </TD>
    <TD  class= input>
      <Input class=codeno name=HospitalType verify="医疗服务机构类型|NOTNULL&len<=20" CodeData= "0|^医疗机构|1^健康体检机构|2^健康管理公司|3^紧急救援公司|4" ondblClick= "showCodeListEx('HospitalType',[this,HospitalType_ch],[1,0],null,null,null,1);" onkeyup= "showCodeListKeyEx('HospitalType',[this,HospitalType_ch],[1,0],null,null,null,1);"><Input class= 'codename' name=HospitalType_ch >
    </TD>
    <TD  class= title>
      	医疗服务机构代码
    </TD>
     <TD  class= input>
     	  <Input class= 'common' name=HospitCode> 
    </TD> 
    <TD  class= title>
      医疗服务机构名称
    </TD>
    <TD  class= input colSpan=3>
      <Input class= 'code' name= HospitName ondblclick="return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,300); ">
    </TD>    
  
  </TR>
  <TR  class= common>
    <TD  class= title>
      地区代码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=AreaCode  verify="地区代码|notnull&len<=20" ondblClick= "showCodeList('hmareacode',[this,AreaName],[0,1],null,null,null,1);" onkeyup= "showCodeListKeyEx('AreaCode',[this,AreaCode],[0,1],null,null,null,1);">
    </TD>
    <TD  class= title>
      地区名称
    </TD>
    <TD  class= input>
				 <Input class= 'code' name=AreaName verify="地区名称|notnull&len<=20"  ondblClick= "showCodeList('hmareaname',[this,AreaCode],[0,1],null,fm.AreaName.value,'codename',1);" onkeyup= "showCodeListKeyEx('AreaName',[this,AreaName],[0,1],null,null,null,1);">
    </TD>
    <TD  class= title>   
    		管理机构
    </TD>
    <TD  class= input>
    	  <Input class='common'style="width:29%" name=MngCom readonly verify="管理机构|NOTNULL"><Input class='code' style="width:66%" name=MngCom_ch  ondblclick="showCodeList('comcode',[MngCom,MngCom_ch],[0,1],null,msql,1,1);" onkeyup= "showCodeListKeyEx('comcode',[MngCom,MngCom_ch],[0,1],null,msql,1,1);">
    </TD>
  <tr>
   <TR  class= common>
   	 <TD  class= title>
         专科名称
    </TD>
     <TD  class= input>
	    <Input class='codeno' name=SpecialCode ondblclick="showCodeList('lhspecname',[this,SpecialName],[0,1],null,fm.SpecialName.value,'SpecialName',1,200);"><Input class= codename name=SpecialName >  
	    </TD>
  </tr>
    <!--TD  class= title>
       流水号
    </TD>
    <TD  class= input>
      <Input class= 'code' name=FlowNo verify="流水号|len<=20"  elementtype=nacessary  ondblclick="generateFlowNo();">
    </TD>
    <!--
    <TD  class= title>
      签约标识
    </TD>
    <TD  class= input>
      <Input class= 'code' name=FixFlag verify="定点属性标识|len<=2" CodeData= "0|^1|签约^2|未签约" ondblClick="showCodeListEx('FixFlag',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('FixFlag',[this,''],[0,1]);">
    </TD>
    -->
  </TR>
  
  </table>
  </Div>
       <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,servPart);">
    		</td>
    		 <td class= titleImg>
        		 扩展查询条件
       	 </td>   		 
    	</tr>
    </table>
<Div id= servPart style="display: ''">  
	<table class= common align='center'>
  <TR  class= common>
    <TD  class= title>
      社保定点属性
    </TD>
    <TD  class= input>
      <Input class=codeno name=CommunFixFlag verify="社保定点属性|len<=20" CodeData= "0|^0|社保非定点医院^1|社保定点医院" ondblClick= "showCodeListEx('CommunFixFlag',[this,CommunFixFlag_ch],[0,1]);" onkeyup= "showCodeListKeyEx('CommunFixFlag',[this,CommunFixFlag_ch],[0,1]);"><Input class= 'codename' name=CommunFixFlag_ch >
    </TD>
    <TD  class= title>
      所属系统
    </TD>
    <TD  class= input>
      <Input class=codeno name=AdminiSortCode verify="|len<=20" ondblClick= "showCodeList('hmadminisortcode',[this,AdminiSortCode_ch],[0,1]);" ><Input class= 'codename' name=AdminiSortCode_ch >
    </TD>
    <TD  class= title>
      经济类型
    </TD>
    <TD  class= input>
      <Input class=codeno name=EconomElemenCode verify="|len<=20"  ondblClick= "showCodeList('hmeconomelemencode',[this,EconomElemenCode_ch],[0,1],null,null,null,1,200);" onkeyup= "showCodeListKey('hmeconomelemencode',[this,EconomElemenCode_ch],[0,1]);"><Input class= 'codename' name=EconomElemenCode_ch >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      医院等级
    </TD>
    <TD  class= input>
      <Input class=codeno name=LevelCode verify="医院等级|len<=20" ondblClick= "showCodeList('hospitalclass',[this,LevelCode_ch],[0,1]);" onkeyup= "showCodeListKeyEx('hospitalclass',[this,LevelCode_ch],[0,1]);"><Input class= 'codename' name=LevelCode_ch >
    </TD>
    <TD  class= title>
      业务类型
    </TD>
    <TD  class= input>
    	<Input class=codeno name=BusiTypeCode verify="业务类型|len<=20"  ondblClick= "showCodeList('hmBusiType',[this,BusiTypeCode_ch],[0,1]);" onkeyup= "showCodeListKey('hmBusiType',[this,BusiTypeCode_ch],[0,1]);"><Input class= 'codename' name=BusiTypeCode_ch >
    </TD>
    <TD  class= title>
      合作级别
    </TD>
    <TD  class= input>
			<Input class=codeno name=AssociateClass   verify="合作级别|len<=20&notnull" ondblClick= "showCodeList('llhospiflag',[this,AssociateClass_ch],[0,1]);" onkeyup= "showCodeListKey('llhospiflag',[this,AssociateClass_ch],[0,1]);"><Input class= 'codename' name=AssociateClass_ch  verify="合作级别|notnull">
    </TD>
  </TR>
  </table>
</DIV>
	<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,contPart);">
    		</td>
			<td class= titleImg>
				责任查询条件
			</td>   		 
    	</tr>
    </table>
 <Div id= contPart style="display: ''">  
	<table class= common align='center'> 
  <TR>
    <TD  class= title>
      合同当前状态
    </TD>
    <TD  class= input>
      <Input  class=codeno name=ContraState verify="合同当前状态|len<=4" CodeData= "0|^1|有效^2|无效" ondblClick= "showCodeListEx('',[this,ContraState_ch],[0,1],null,null,null,'1',200);" codeClear(ContraState_ch,ContraState);"><Input class= 'codename' name=ContraState_ch >	
    </TD>
    <TD  class= title>
      合同项目责任类型
    </TD>
    <TD  class= input>
    	<Input class=codeno name=DutyItem verify = "责任项目代码|notnull&len<=20" ondblClick= "showCodeList('hmdeftype',[this,DutyItemCode],[0,1],null,null,null,1);"><Input class= 'codename' name=DutyItemCode >
    </TD>
    <TD  class= title>
      合同责任项目名称
    </TD>
      <TD  class= input>
      <Input class=codeno name=DutyName ondblClick= "showCodeList('hmdutyitemdef',[this,DutyItemName],[1,0],null,fm.DutyItem.value,'l',1);"><Input class= 'codename' name=DutyItemName  >
    </TD>
  </TR>
    <TR>
    <TD  class= title>
      责任当前状态
    </TD>
    <TD  class= input>
      <Input  class=codeno name=DutyItemState verify="责任当前状态|len<=4" CodeData= "0|^1|有效^2|无效" ondblClick= "showCodeListEx('',[this,DutyItemState_ch],[0,1],null,null,null,'1',200);"><Input class= 'codename' name=DutyItemState_ch >	
    </TD>
    <TD  class= title></TD>
    <TD  class= input></TD>
    <TD  class= title></TD>
    <TD  class= input></TD>
  </TR>
 </table>
</DIV>
<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,HighPart);">
    		</td>
			<td class= titleImg>
				高级查询条件
			</td>   		 
    	</tr>
    </table>
 <Div id= HighPart style="display: ''">  
	<table class= common align='center'> 
  <TR>
    <TD  class= title>
      检索方式
    </TD>
    <TD  class= input>
    	<Input class=codeno name=SerchMode  ondblClick= "showCodeList('searchesmode',[this,SerchModeName],[0,1],null,null,null,1);"><Input class= 'codename' name=SerchModeName >
    </TD>
    <TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
    <TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
  </tr>
</table>
</div>
<Div id= HighTypeGrid style="display: 'none'">  
	<table class= common align='center'>    
		<TD  class= title>
        赔付险种类别
    </TD>
    <TD  class= input>
       	 <Input class= 'code'  type = hidden name=Riskcode>
         <Input class= 'code' id=Riskname name=Riskname  ondblclick="showCodeList('riskcode1',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskname','1',300);"  codeClear(Riskname,Riskcode);">
    </TD>
    <TD  class= title>
      诊断疾病类别
    </TD>
    <TD  class= input>
      <Input class= 'code' type = hidden name=ICDCode >
      <Input class= 'code' id=ICDName name=ICDName  ondblclick=" return showCodeList('lddiseasename',[this,ICDCode],[1,0],null,fm.ICDName.value,'ICDName',1,350);" onkeyup=" if(event.keyCode == 13) return showCodeList('lddiseasename',[this,ICDCode],[1,0],null,fm.ICDName.value,'ICDName',1,350);" verify="疾病标准名称|len<=30" >
    </TD>
    <TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
  </tr>
</table>
</div>
 <Div id="div1" style= "display: ''">
 <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHHighPartGrid">
	  				</span> 
			    </td>
				</tr>
 </table>
</Div>
</DIV>
<br>
 <table class= common border=0 width=100%>
  	<TR class= common>
  		<TD class=title>
			<Input value="查询(按机构)" style="width:130px"  type=button class=cssbutton onclick="mainQuery();">
			<Input value="查询（按机构-合同）"  style="width:130px"  type=button class=cssbutton onclick="healthQuery();">	
			<Input value="查询(按机构)结果导出"  style="width:150px"  type=button class=cssbutton onclick="ExportHospital();">
			<Input value="重置" type=hidden class=cssbutton onclick="inputClear();">
     
  	 	</TD>
  	</TR>
 </table>  

<Div id="div1" style= "display: ''">
 <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanHospitalComGrid">
	  				</span> 
			    </td>
				</tr>
 </table>
</Div>
    <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
   <table class= common border=0 width=100%>
  	<TR class= common>
  		<TD class=title>
			<!--Input value="医疗机构信息" type=button class=cssbutton onclick="ToHospitalInfo();">
			<Input value="团体合同信息" type=button class=cssbutton onclick="ToCompactInfo();"-->
			<Input value="医疗机构详细信息" style="width:130px"  type=button class=cssbutton onclick="SpeciOfHospit();">
  	 	</TD>
  	</TR>
 </table>  
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>



