<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHGrpServPlanSave.jsp
//程序功能：
//创建日期：2006-03-09 11:34:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数

  OLHGrpServPlanUI tOLHGrpServPlanUI   = new OLHGrpServPlanUI();
  LHGrpServPlanSet tLHGrpServPlanSet = new LHGrpServPlanSet();	
  String tServPlanLevel[] = request.getParameterValues("LHGrpServPlanGrid1");					//MulLine的列存储数组
  String tServPrem[] = request.getParameterValues("LHGrpServPlanGrid2");					//MulLine的列存储数组
  String tGrpServPlanNo[] = request.getParameterValues("LHGrpServPlanGrid3");					//MulLine的列存储数组
  String tMakeDate[] = request.getParameterValues("LHGrpServPlanGrid4");					//MulLine的列存储数组
  String tMakeTime[] = request.getParameterValues("LHGrpServPlanGrid5");					//MulLine的列存储数组
  String tCustomNumInDate[] = request.getParameterValues("LHGrpServPlanGrid6");
  String tCustomNumActrually[] = request.getParameterValues("LHGrpServPlanGrid7");
  String tLevelDescription[] = request.getParameterValues("LHGrpServPlanGrid8");
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");

    
    
    int LHGrpServPlanCount = 0;
		if(tServPlanLevel != null)
		{	
			LHGrpServPlanCount = tServPlanLevel.length;
		}	
		
		System.out.println(" LHGrpServPlanCount is : "+LHGrpServPlanCount);
		
		for(int i = 0; i < LHGrpServPlanCount; i++)
		{
		    
				LHGrpServPlanSchema tLHGrpServPlanSchema = new LHGrpServPlanSchema();			
				tLHGrpServPlanSchema.setServPlanLevel(tServPlanLevel[i]);	
				tLHGrpServPlanSchema.setServPrem(tServPrem[i]);	
        tLHGrpServPlanSchema.setGrpServPlanNo(tGrpServPlanNo[i]);
        tLHGrpServPlanSchema.setGrpContNo(request.getParameter("GrpContNo"));
        tLHGrpServPlanSchema.setGrpCustomerNo(request.getParameter("GrpCustomerNo"));
        tLHGrpServPlanSchema.setGrpName(request.getParameter("GrpName"));
        tLHGrpServPlanSchema.setServPlanCode("0");
        tLHGrpServPlanSchema.setServPlanName("0");
        tLHGrpServPlanSchema.setServPlayType("0");
        tLHGrpServPlanSchema.setComID(request.getParameter("ComID"));
        tLHGrpServPlanSchema.setStartDate(request.getParameter("StartDate"));
        tLHGrpServPlanSchema.setEndDate(request.getParameter("EndDate"));
        tLHGrpServPlanSchema.setMakeDate(tMakeDate[i]);
        tLHGrpServPlanSchema.setMakeTime(tMakeTime[i]);
        tLHGrpServPlanSchema.setCustomNumInDate(tCustomNumInDate[i]);
        tLHGrpServPlanSchema.setCustomNumActrually(tCustomNumActrually[i]);
        tLHGrpServPlanSchema.setLevelDescription(tLevelDescription[i]);

        tLHGrpServPlanSet.add(tLHGrpServPlanSchema);
     }
				
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
//  tVData.add(tLHGrpServPlanSchema);
	  tVData.add(tLHGrpServPlanSet);
  	tVData.add(tG);
    tOLHGrpServPlanUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHGrpServPlanUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
