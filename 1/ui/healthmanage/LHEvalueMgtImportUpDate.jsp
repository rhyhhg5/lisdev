<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHEvalueMgtImportUpDate.jsp
//程序功能：
//创建日期：2006-12-13 10:20:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHEvaReportUI tLHEvaReportUI   = new LHEvaReportUI();
  
  LHEvaReportSet tLHEvaReportSet = new LHEvaReportSet();		//服务预约管理表
  LHTaskCustomerRelaSet tLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();		//客户任务实施管理表
    
    String tTaskExecNo[] = request.getParameterValues("LHEvalueMgtImportGrid6");           //任务实施号码流水号
    String tServTaskNo[] = request.getParameterValues("LHEvalueMgtImportGrid4");           //服务任务编号
    String tServCaseCode[] = request.getParameterValues("LHEvalueMgtImportGrid5");					//服务事件号  
    String tServTaskCode[] = request.getParameterValues("LHEvalueMgtImportGrid8");           //服务任务代码
    String tServItemNo[] = request.getParameterValues("LHEvalueMgtImportGrid7");					//服务项目序号
    String tCustomerNo[] = request.getParameterValues("LHEvalueMgtImportGrid1");					//客户号  
    String tExecState[] = request.getParameterValues("LHEvalueMgtImportGrid10");					//服务执行状态  	 
     
    String tChk[] = request.getParameterValues("InpLHEvalueMgtImportGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  
    
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");

   int LHEvaReportCount = 0;
   System.out.println("R R  "+tTaskExecNo.length);
	 if(tTaskExecNo != null)
	 {	
		    LHEvaReportCount = tTaskExecNo.length;
	 }	
	 System.out.println(" LHEvaReportCount is : "+LHEvaReportCount);
    for(int j = 0; j < LHEvaReportCount; j++)
	  {
	     
		    LHEvaReportSchema tLHEvaReportSchema = new LHEvaReportSchema();
		    
		     tLHEvaReportSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		     tLHEvaReportSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		     tLHEvaReportSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		     tLHEvaReportSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		     tLHEvaReportSchema.setServItemNo(tServItemNo[j]);	//项目号码
		     tLHEvaReportSchema.setCustomerNo(tCustomerNo[j]);	//客户号  
		          
         
         LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema();
		 
		     tLHTaskCustomerRelaSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		     tLHTaskCustomerRelaSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		     tLHTaskCustomerRelaSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		     tLHTaskCustomerRelaSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		     tLHTaskCustomerRelaSchema.setServItemNo(tServItemNo[j]);	//项目号码    
         String state=request.getParameter("ExecState");
		     if(state.equals("")||state.equals("null")||state.equals(null))
		     {
		        tLHTaskCustomerRelaSchema.setTaskExecState(tExecState[j]);  //任务执行状态  
		     }
		     else
		     {
		       tLHTaskCustomerRelaSchema.setTaskExecState(request.getParameter("ExecState"));     //任务执行状态  
		     }        
         if(transact.equals("INSERT||MAIN"))
         {
            if(tChk[j].equals("1"))  
            {             
               tLHEvaReportSet.add(tLHEvaReportSchema); 
               tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);             
           }
        } 
        if(transact.equals("DELETE||MAIN"))
        {
          if(tChk[j].equals("1"))  
          {   
             tLHEvaReportSet.add(tLHEvaReportSchema); 
          }
        }         
      }
                              
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHEvaReportSet);
  	tVData.add(tLHTaskCustomerRelaSet);
  	tVData.add(tG);
    tLHEvaReportUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHEvaReportUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	  var transact = "<%=transact%>";
	      parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>