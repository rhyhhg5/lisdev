<%
//程序名称：LDDrugClassQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-19 13:58:55
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ClassCode').value = "";

    fm.all('ClassName').value = "";

    fm.all('ClassLevel').value = "";
  }
  catch(ex) {
    alert("在LDDrugClassQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDDrugClassGrid();  
  }
  catch(re) {
    alert("LDDrugClassQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDDrugClassGrid;
function initLDDrugClassGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="类别代码";   
		iArray[1][1]="40px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;
		
		iArray[2]=new Array(); 
		iArray[2][0]="类别名称";   
		iArray[2][1]="40px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="类别等级";   
		iArray[3][1]="40px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="MakeDate";   
		iArray[4][1]="0px";   
		iArray[4][2]=20;        
		iArray[4][3]=3;
		
		iArray[5]=new Array(); 
		iArray[5][0]="MakeTime";   
		iArray[5][1]="0px";   
		iArray[5][2]=20;        
		iArray[5][3]=3;

		iArray[6]=new Array();   
    iArray[6][0]="ModifyDate"; 
    iArray[6][1]="0px";      
    iArray[6][2]=20;         
    iArray[6][3]=3;          
                             
    iArray[7]=new Array();   
    iArray[7][0]="ModifyTime"; 
    iArray[7][1]="0px";      
    iArray[7][2]=20;         
    iArray[7][3]=3;          


		
    
    LDDrugClassGrid = new MulLineEnter( "fm" , "LDDrugClassGrid" ); 
    //这些属性必须在loadMulLine前

      LDDrugClassGrid.mulLineCount = 0;   
      LDDrugClassGrid.displayTitle = 1;
      LDDrugClassGrid.hiddenPlus = 1;
      LDDrugClassGrid.hiddenSubtraction = 1;
      LDDrugClassGrid.canSel = 1;
      LDDrugClassGrid.canChk = 0;
    //LDDrugClassGrid.selBoxEventFuncName = "showOne";

    LDDrugClassGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDrugClassGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
