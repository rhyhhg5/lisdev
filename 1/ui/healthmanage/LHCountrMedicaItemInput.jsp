<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-14 16:36:50
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LHCountrMedicaItemInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHCountrMedicaItemInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
  <form  method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <Div id = "divQueryButton" style="display: 'none'" align=right>
    	<table>
    		<tr>
    			<td class=button width="10%" align=right>
						<INPUT class=cssButton name="querybutton1" VALUE="查  询"  TYPE=button onclick="return queryClick();">
					</td>	
				</tr>	
			</table>
			<hr>
		</Div>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCountrMedicaItem1);">
    		</td>
    		 <td class= titleImg>
        		 基本医疗项目信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCountrMedicaItem1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      服务项目代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MedicaItemCode elementtype=nacessary verify="服务项目代码|NOTNULL&len<=12">
    </TD>
    <TD  class= title>
      服务项目名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MedicaItemName elementtype=nacessary verify="服务项目名称|notnull&len<=50">
    </TD>
    <TD  class= title>
      代码分类等级
    </TD>
    <TD  class= input>
    	
      <Input class='common' name=ClassLevel style="width:25px"><Input class= 'code' name=ClassLevel_ch style="width:135px" CodeData="0|^1|1级代码^2|2级代码^3|3级代码^4|4级代码" ondblClick= "showCodeListEx('ClassLevell',[this,ClassLevel],[1,0],null,null,null,1);">
    </TD>   
  </TR>
 </table>
 
<Div  id= "divStyle4" style= "display: 'none'">
 <table  class= common align='center' >
  <TR  class= common>
  	<TD  class= title>
      计价单位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PriceUnit verify="计价单位|len<=60">
    </TD>
    <TD  class= title>
      项目内涵
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MedicaMeanin verify="项目内涵|len<=600">
    </TD>
    <TD  class= title>
      除外内容
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ExcConten verify="除外内容|len<=600">
    </TD>
  </TR>
  <TR>
     <TD  class= title>
      补充说明
    </TD>
    <TD  class= input colSpan=6>
      <Input class= 'common' style="width:682px" name=Explai verify="补充说明|len<=600">
    </TD>
  </TR>
  <TR>
  	<TD  class= title>
      结果描述类型
    </TD>
    <TD  class= input>
		<Input type=hidden name=ResuDescriptStyle>
		<Input class= 'code' name=ResuDescriptStyle_ch  CodeData="0|^1|数值描述^2|文字描述" ondblClick= "showCodeListEx('',[this,ResuDescriptStyle],[1,0],null,null,null,1);">
    </TD>
    <TD  class= title>
      标准计量单位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StandardMeasureUnit  verify="标准计量单位|len<=20">
    </TD>
    <TD  class= title>
     正常值（或结果）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StandardValue>
    </TD>
  </TR>
  <TR>
    <TD class=title>
    	是否空腹
    </TD>
    <TD class= input>
    		<Input type=hidden name=InBelly>
    		<Input class= 'code' name=InBelly_ch CodeData="0|^0|否^1|是" ondblclick=" showCodeListEx('',[this,InBelly],[1,0],null,null,null,1);"  >
    </TD>
    </TD>
    <TD class=title>
    	换算系数
    </TD>
    <TD class= input>
    	<Input class= 'common' name=ClassCode>
    </TD>
	<TD class=title>
    </TD>
    <TD class= input>
    </TD>
<!--    <TD  class= title>
      地区属性
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AreaTpye  verify="">
    </TD>
-->
  </TR>
<!--
  <TR  class= common>
    
    <TD  class= title>
      收费单位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ChargeUnit verify="收费单位|len<=10">
    </TD>
  </TR>
  <TR  class= common>
   
    <TD  class= title>
      医疗费用类别代码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=MedicaFeeCode verify="医疗费用类别代码|len<=20" ondblClick="showCodeListEx('MedicaFeeCode',[this,''],[0,1]);" CodeData="0|^1|^2|^3">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      使用频率级别
    </TD>
    <TD  class= input>
      <Input class= 'code' name=FrequencyFlag verify="使用频率级别|NUM&len<10" value="1" CodeData= "0|^1|较少^2|普通^3|较多" ondblClick="showCodeListEx('FrequencyFlag',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('FrequencyFlag',[this,''],[0,1]);">
    </TD>
  </TR>
-->
</table>
</Div>
    </Div>
        
    <table style="display:'none'">
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCountrMedicaItem2);">
    		</td>
    		 <td class= titleImg>
        		 费用信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCountrMedicaItem2" style= "display: ''">
<table  class= common align='center' style="display:'none'"> 
	<TR  class= common>
    <TD  class= title>
      基础收费标准（元）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BasicCharge  verify="">
    </TD>
    <TD  class= title>
      卫生材料费用（元）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SanitationCost verify="">
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      一级医院收费标准（元）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FirstChargeStd  verify="">
    </TD>
    <TD  class= title>
      二级医院收费标准（元）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SecondChargeStd verify="">
    </TD>
    <TD  class= title>
      三级医院收费标准（元）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ThirdChargeStd verify="">
    </TD>
  </TR>
</table>
    </Div> 
    
    <!--table style= "display: ''">
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCountrMedicaItem3);">
    		</td>
    		 <td class= titleImg>
        		 医疗服务项目分类信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCountrMedicaItem3" style= "display: ''">
<table  class= common align='center' style="display:''"> 
	<TR  class= common>
    <TD  class= title>
      类别等级
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ClassLevel  verify="|len<=50" CodeData= "0|^1|a^2|b" ondblClick= "showCodeListEx('',[this,''],[0,1]);" onkeyup= "showCodeListKeyEx('',[this,''],[0,1]);">
    </TD>
    <TD  class= title>
      类别代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ClassCode  verify="|len<=50">
    </TD>
    <TD  class= title>
      类别名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ClassName verify="len<=50">
    </TD>
  </TR>
</TABLE>
	</Div-->
    <div id="div1" style="display: 'none'">
    <table>
    <TD  class= title>
      项目类型
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ItemType verify="项目类型|len<=2" CodeData= "0|^1|中医^2|西医^3|综合" ondblClick="showCodeListEx('ItemType',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('ItemType',[this,''],[0,1]);">
    </TD>
	</table>
    </div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden name = "ClassInfoCode" value= "MedicalServiceClass">
    <input type=hidden name=Operator>
    <input type=hidden name=MakeDate>
    <input type=hidden name=MakeTime>
    <input type=hidden name="ModifyDate">
    <input type=hidden name="ModifyTime">
    <input type=hidden name="LoadFlag">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
