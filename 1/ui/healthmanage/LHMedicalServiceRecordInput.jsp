<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-02-27 18:16:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHMedicalServiceRecordInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHMedicalServiceRecordInputInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();passQuery();" >
  <form action="./LHMedicalServiceRecordSave.jsp" method=post name=fm target="fraSubmit">
	    <%@include file="../common/jsp/OperateButton.jsp"%>
	    <%@include file="../common/jsp/InputButton.jsp"%>
    
   		<table  class= common align='center'>
  <TR  class= common>   
    	<TD  class= title>
      		客户号
    	</TD>
    	<TD  class= input>    
    		<Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号|notnull&len<=24" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
	    </TD>
	    <TD  class= title>
	      客户姓名
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' readonly name=CustomerName  verify="客户姓名|len<=20">
	    </TD>
	    <TD  class= title>
	      首次记录时间
	    </TD>
	    <TD  class= input>
	       <Input class= 'coolDatePicker' style='width:90' dateFormat="Short" name=FirstRecoDate  verify="首次记录时间|DATE">
      	 <Input class= 'common' name=FirstRecoTime style='width:44' ondblclick=" dFirstRecoTime();">
      </TD>
	</TR>
	
	<TR class = common>
			<TD class = title>
				服务状态
			</TD>
			<TD class = input>
				<input class=codeno name=ServiceState verify="服务状态|len<=4" CodeData= "0|^1|进行中^2|服务结束" ondblClick= "showCodeListEx('ServiceState',[this,ServiceState_ch],[0,1],null,null,null,1);" ><Input class = 'codename' name =ServiceState_ch >
			</TD>
			<TD class = title>
			</TD>
			<TD class = input>
			</TD>
			<TD class = title>
			</TD>
			<TD class = input>
			</TD>
	</TR>
    	</table>
    
    <table>
	    	<tr>
		    		<td>
		    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHMedicalServiceRecord1);">
		    		</td>
	    		  <td class= titleImg>
	        		 服务申请记录
	       	  </td>   		 
	    	</tr>
    </table>

	      	<div id="divDescriptionDetail"  style="display: none; position:absolute; slategray">
	    	     <textarea name="textDescriptionDetail" cols="100%" rows="3" witdh=25% class="common" onkeydown="backDescriptionDetail();">
	    	     </textarea>
	    	     <td class=button width="10%" align=right>
								<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backDescriptionDetail1();">
						 </td>
				  </div>
<table  class= common align='center' id=divLHMedicalServiceRecord1>
		  <TR  class= common>   
			    	<TD  class= title>
			      	服务项目
			      </TD>
			      <TD  class= input>
					    	<input class=codeno name=ApplyDutyItemCode onclick="showCodeList('healthservice',[this,DutyItemName],[0,1],null,null,null,1);"><Input class=codename name=DutyItemName verify="服务项目|len<=100"> 
				    </TD>
				    <TD  class= title>
				      详细描述
				    </TD>
				    <TD  class= input>
				      <Input class= 'common'  name=DescriptionDetail ondblclick="return inputDescriptionDetail();" verify="详细描述|len<=600">
				    </TD>
				    <TD class = title >
				    	服务时间
				    </TD>
				    <TD class = input>
					    	<Input class= 'coolDatePicker' style='width:90' dateFormat="Short" name=ServiceDate verify="服务时间|DATE">
					    	<Input class= 'common' name=ServiceTime style='width:44' ondblclick=" return dServiceTime();">
			   	  </TD>
		  </TR>
			<TR  class= common>    
					<TD  class= title>
			      服务机构	
			    </TD>
			    <TD  class= input>
			    	<input class=codeno name=HospitCode ondblclick="return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');" verify="医疗机构名称|len<=100" ><Input class= 'codename' name=HospitName  >
			    </TD>                                                                                                                                                                                                                                    
			    <TD  class= title>
			      服务人员
			    </TD>
			    <TD  class= input>
			    	<input class=codeno name =DoctNo ondblclick="return showCodeList('lhdoctname',[this,DoctName],[1,0],null,fm.DoctName.value,'DoctName');" onkeyup=" if(event.keyCode ==13) return showCodeList('lhdoctname',[this,DoctName],[1,0],null,fm.DoctName.value,'DoctName');" verify="诊断医师姓名|len<=20"><Input class= 'codename' name=DoctName  >
			    </TD>
			  	<TD  class= title>
			      申请受理方式
			    </TD>
			    <TD  class= input>
			       <Input class=codeno name=ApplyReceiveType verify="|len<=10" CodeData= "0|^1|电话^2|电子邮件^3|其他" ondblClick= "showCodeListEx('hmapplyreceivetype',[this,ApplyReceiveType_ch],[0,1],null,null,null,1);"><Input class= 'codename' name=ApplyReceiveType_ch >
			    </TD>
		  </TR>
			<TR  class= common> 
			    <TD  class= title>
			      是否需要预约
			    </TD>
			    <TD  class= input>
			      <Input class=codeno name=ServiceFlag verify="是否需要预约|len<=20" CodeData="0|^1|是^0|否" ondblclick="showCodeListEx('ServerFlag',[this,ServiceFlag_ch],[0,1],null,null,null,1);"><Input class= 'codename' name=ServiceFlag_ch  >
			    </TD>
			    <TD class = title>
					</TD>
					<TD class = input>
					</TD>
					<TD class = title>
					</TD>
					<TD class = input>
					</TD>
		  </TR>
</table>
		</div>

	 <table id = "ServBookReco" style= "display: ''">
    	<tr>
	    		 <td>
	    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= " ">
	    		 </td>
	    		 <td class= titleImg>
	        		 服务预约记录
	       	 </td>   		 
    	</tr>
    </table>

    	 <div id="divApplyServiceDes"  style="display: none; position:absolute; slategray">
    	     <textarea name="textApplyServiceDes" cols="100%" rows="3" witdh=25% class="common" onkeydown="backApplyServiceDes();">
    	     </textarea>
    	     <td class=button width="10%" align=right>
								<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backApplyServiceDes1();">
						 </td>
			 </div>
<table  class= common align='center' id= isBookOk style= "display: 'none'">
		<TR class = comnon>
				<TD class = title>
					是否预约成功
				</TD>
				<TD class = input>
					 <Input class=codeno name=IsBespeakOk verify="|len<=20" CodeData= "0|^1|预约成功^2|预约失败" ondblClick= "showCodeListEx('IsBespeakOk',[this,IsBespeakOk_ch],[0,1],null,null,null,1);" ><Input class= 'codename' name=IsBespeakOk_ch  >
				</TD>
				<TD class = title>
					是否相符
				</TD>
				<TD class = input>
					<input class=codeno name=IsApplaySame CodeData="0|^0|否^1|是" ondblclick=" showCodeListEx('IsSame',[this,IsApplaySame_ch],[0,1],null,null,null,1);"><Input class='codename' name= IsApplaySame_ch >
				</TD>
				<TD class = title>
				</TD>
				<TD class = input>
				</TD>
		</TR>
</table>	
	<TR><TD><br></TD></TR>
<div id="divBespeaLostCause"  style="display: none; position:absolute; slategray">
    	     <textarea name="textBespeaLostCause" cols="100%" rows="3" witdh=25% class="common" onkeydown="backBespeaLostCause();">
    	     </textarea>
    	     <td class=button width="10%" align=right>
								<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backBespeaLostCause1();">
						 </td>
			 </div>
<table  class= common align='center' id=bookItem style="display:'none'">	
		<TR  class= common>    
		    <TD  class= title>
		      预约服务项目
		    </TD>
		    <TD  class= input>
		    	<input class=codeno name=BespeakServiceItem onclick="showCodeList('healthservice',[this,BespeakServiceItemName],[0,1],null,null,null,1);"><Input class=codename name=BespeakServiceItemName verify="预约服务项目|len<=100"> 
		    </TD>
		    <TD  class= title>
		      详细描述
		    </TD>
		    <TD  class= input>
		      <Input class= 'common' name=ApplyServiceDes ondblclick="return inputApplyServiceDes();" verify=" ">
		    </TD>
		  	<TD  class= title>
		      预约服务时间
		    </TD>
		    <TD  class= input>
		      <Input class= 'coolDatePicker' style='width:90' dateFormat="Short" name=BespeakServiceDate verify="预约服务时间|DATE">
		      <Input class= 'common' name=BespeakServiceTime style='width:44' ondblclick=" return dBespeakServiceTime();"> 
		    </TD>
	  </TR>
		<TR  class= common>    
	
		   	 <TD  class= title>
		      预约服务机构
		    </TD>
		    <TD  class= input>
 	  	      <input type=hidden name=BespeakServiceHospit>
			      <Input class= 'code' name=BespeakServiceHospitName  ondblclick="return showCodeList('lhhospitname',[this,BespeakServiceHospit],[0,1],null,fm.BespeakServiceHospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,BespeakServiceHospit],[0,1],null,fm.BespeakServiceHospitName.value,'HospitName');" verify="医疗机构名称|len<=100">
		    </TD>
		    <TD  class= title>
		      预约服务人员
		    </TD>
		    <TD  class= input>
		    	<input type=hidden name=BespeakServiceDoctNo>
		      <Input class= 'code' name=BespeakServiceDoctName   ondblclick="showCodeList('lhdoctname',[this,BespeakServiceDoctNo],[0,1],null,fm.BespeakServiceDoctName.value,'DoctName');" onkeyup=" if(event.keyCode ==13) return showCodeList('lhdoctname',[this,BespeakServiceDoctNo],[0,1],null,fm.BespeakServiceDoctName.value,'DoctName');" verify="诊断医师姓名|len<=20">
		    </TD>
		  	<TD  class= title>
		      不相符原因
		    </TD>
		    <TD  class= input id=NoCompareCause>
		      <Input class= 'common'  name=NoCompareCause  verify=" " ondblclick=" return inputNoCompareCause();">
		    </TD>	
		    		
	  </TR>
	</table>
	<div id="divNoCompareCause"  style="display: none; position:absolute; slategray">
    	     <textarea name="textNoCompareCause" cols="100%" rows="3" witdh=25% class="common" onkeydown="backNoCompareCause();">
    	     </textarea>
    	     <td class=button width="10%" align=right>
								<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backNoCompareCause1();">
						 </td>
			 </div>
	<table  class= common align='center' id=bookFail style="display:'none'">		  
	  <TR class = common >
		  	<TD class = title>
		  		预约失败原因
		  	</TD>
		  	<TD class = input>
		  		<Input class = 'common' name=BespeaLostCause ondblclick=" return inputBespeaLostCause();">
		  	</TD>
		  	<TD class = title>
				</TD>
				<TD class = input>
				</TD>
				<TD class = title>
				</TD>
				<TD class = input>
				</TD>	
		</TR>
  </table>



		<table id = "ServImplement" style = "display:''">
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHMedicalServiceRecord3);">
    		</td>
    		 <td class= titleImg>
        		 服务执行情况
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHMedicalServiceRecord3" style= "display: ''">
    	 
<table   class= common align='center' >
		<table class=common align='center' id=serviceStatetable style="display:'none'"	>
				<TR class = common>
					<TD class = title>
						服务执行状态
					</TD>
					<TD class = input>
						<Input type=hidden name=ServiceExecState>
						<Input class= 'code' name=ServiceExecState_ch verify="|len<=20" ondblClick= "showCodeList('servicestate',[this,ServiceExecState],[1,0],null,null,null,1);" >
					</TD>
					<TD class = title>
						是否相符
					</TD>
					<TD class = input>
						<input type=hidden name=IsExecSame>
						<Input class='code' name= IsExecSame_ch CodeData="0|^0|否^1|是" ondblclick=" showCodeListEx('IsExecSame',[this,IsExecSame],[1,0],null,null,null,1);">
					</TD>
					<TD class = title>
					</TD>
					<TD class = input>
					</TD>
				</TR>
				<TR><TD><br></TD></TR>
		</table>
		<table class = common  align='center' id=bookLeftTime style="display:'none'"	>
				<TR class = common>
					<TD class = title>
						预约剩余时间（天）
					</TD>
					<TD class = input>
						<Input class = 'common' name=BespeaLLeaveTime verify= "预约剩余时间（天）|num">
					</TD>
					<TD class = title>
					</TD>
					<TD class = input>
					</TD>
					<TD class = title>
					</TD>
					<TD class = input>
					</TD>
				</TR> 
		</table>
		
		<div id="divExcuteDescription3"  style="display: none; position:absolute; slategray">
		    	     <textarea name="textExcuteDescription3" cols="100%" rows="3" witdh=25% class="common" onkeydown="backExcuteDescription();">
		    	     </textarea>
		    	     <td class=button width="10%" align=right>
									<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backExcuteDescription1();">
							 </td>
		</div>
		<div id="divExNoCompareCause"  style="display: none; position:absolute; slategray">
		    	     <textarea name="textExNoCompareCause" cols="100%" rows="3" witdh=25% class="common" onkeydown="backExNoCompareCause();">
		    	     </textarea>
		    	     <td class=button width="10%" align=right>
									<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backExNoCompareCause1();">
							 </td>
    </div>
		<table class = common  align='center' id=serviceExecute style="display:'none'"	>
				<TR class = common>
					<TD class = title>
						执行服务项目
					</TD>
					<TD class = input>
						<input class=codeno name=ExcuteServiceItem onclick="showCodeList('healthservice',[this,ExcuteServiceItemName],[0,1],null,null,null,1);"><Input class=codename name=ExcuteServiceItemName verify="执行服务项目|len<=100"> 
					</TD>
					<TD class = title>
						详细描述
					</TD>
					<TD class = input>
						<Input class = 'common' name=ExcuteDescription ondblclick="return inputExcuteDescription();">						
					</TD>
					<TD class = title>
						执行服务时间
					</TD>
					<TD class = input>
				      <Input class= 'coolDatePicker'  style='width:90' dateFormat="Short" name=ExcuteServiceDate verify="执行服务时间|DATE">
				      <Input class= 'common' name=ExcuteServiceTime style='width:44' ondblclick=" return dExcuteServiceTime();">
					</TD>
				</TR>
				<TR class = common>
						<TD class = title>
							执行服务机构
						</TD>
						<TD class = input>
							<input type=hidden name=ExcuteServiceHospit>
							<Input class = 'code' name=ExcuteServiceHospitName ondblclick=" return showCodeList('lhhospitname',[this,ExcuteServiceHospit],[0,1],null,fm.ExcuteServiceHospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13) return showCodeList('lhhospitname',[this,ExcuteServiceHospit],[0,1],null,fm.ExcuteServiceHospitName.value,'HospitName');" verify="医疗机构名称|len<=100">
						</TD>
						<TD class = title>
							执行服务人员
						</TD>
						<TD class = input>
							<input type=hidden name=ExcuteServiceDocNo>
							<Input class = 'code' name=ExcuteServiceDocName ondblclick="showCodeList('lhdoctname',[this,ExcuteServiceDocNo],[0,1],null,fm.ExcuteServiceDocName.value,'DoctName');" onkeyup=" if(event.keyCode ==13) return showCodeList('lhdoctname',[this,ExcuteServiceDocNo],[0,1],null,fm.ExcuteServiceDocName.value,'DoctName');" verify="诊断医师姓名|len<=20">
						</TD>
						<TD class = title>
							不相符原因
						</TD>
						<TD class = input id=ExNoCompareCause >
							<Input class = 'common' name=ExNoCompareCause ondblclick=" return inputExNoCompareCause(); ">
						</TD>
				</TR>
				<TR class = common>
							<TD class = title>
								实际支付费用（元）
							</TD>
							<TD class = input>
								<Input class = 'common' name=ExcutePay verify="实际支付费用（元）|num&len<12">
							</TD>
							<TD class = title>
								客户满意度
							</TD>
							<TD class = input>
								<Input type=hidden name=SatisfactionDegree>
								<Input class= 'code' name=SatisfactionDegree_ch ondblclick=" showCodeList('hmsatisfy',[this,SatisfactionDegree],[0,1],null,null,null,1); " >
							</TD>
							<TD class = title>
								其他服务信息
							</TD>
							<TD class = input>
								<Input class = 'common' name=OtherServiceInfo>
							</TD>
				</TR>
		</table>
		
		<div id="divCancleServiceCause"  style="display: none; position:absolute; slategray">
    	     <textarea name="textCancleServiceCause" cols="100%" rows="3" witdh=25% class="common" onkeydown="backCancleServiceCause();">
    	     </textarea>
    	     <td class=button width="10%" align=right>
								<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backCancleServiceCause1();">
						 </td>
			 </div>
		
		<table class = common  align='center' id=serviceCancel style="display:'none'"	>
				<TR class = common>	
					<TD class = title>
						服务取消原因
					</TD>
					<TD class = input>
						<Input class = 'common' name=CancleServiceCause ondblclick=" return inputCancleServiceCause();">
					<TD class = title>
					</TD>
					<TD class = input>
					</TD>
					<TD class = title>
					</TD>
					<TD class = input>
					</TD>
				</TR>
				<TR><TD><br></TD></TR>
		</table>
		
</table>

	

     <div id="div1" style="display: 'none'">
      <table>
        <TR  class= common>
            <TD  class= title>
              操作员代码
            </TD>
            <TD  class= input>
              <Input class= 'common' name=Operator >
            </TD>
            <TD  class= title>
              入机日期
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeDate >
            </TD>
            <TD  class= title>
              入机时间
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeTime >
            </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最后一次修改日期
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyDate >
          </TD>
          <TD  class= title>
            最后一次修改时间
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyTime >
          </TD>
        </TR>
      </table>
     </div>
     <input type=hidden id="fmtransact" name="fmtransact">
     <input type=hidden id="fmAction" name="fmAction">
     <input type=hidden id="RecordNo" name="RecordNo">
     <input type=hidden id="iscomefromquery" name="iscomefromquery">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

		<script>
//			通过综合查询进入此页面的处理
	function passQuery()
	{
						var trecordno = "<%=request.getParameter("recordno")%>";
						if(trecordno == "" || trecordno == "null")
					  {
					  		return;
					  }
					  else
						{
								
								
								
								var arrResult = new Array();
	
								
								if( trecordno != null )
								{
									  	
									fm.RecordNo.value = trecordno;		 
									
									var strSQL = " select a.CustomerNo, b.Name,a.FirstRecoDate,a.ServiceState,a.ApplyDutyItemCode,a.DescriptionDetail,"
				   		+" a.ServiceDate,a.HospitCode,a.DoctNo,a.ApplyReceiveType,a.ServiceFlag,a.IsBespeakOk,a.BespeakServiceItem,"
				   		+" a.ApplyServiceDes,a.BespeakServiceDate,a.BespeakServiceHospit,a.BespeakServiceDoctNo,a.NoCompareCause,"
				   		+" a.BespeaLostCause,a.ServiceExecState,'',a.ExcuteServiceItem,a.ExcuteDescription,"
				   		+" a.ExcuteServiceDate,a.ExcuteServiceHospit,a.ExcuteServiceDocNo,a.ExNoCompareCause,a.ExcutePay,"
				   		+" a.SatisfactionDegree,a.OtherServiceInfo,a.CancleServiceCause,a.RecordNo,a.MakeDate,a.MakeTime, "
				   		+" a.FirstRecoTime,a.ServiceTime,a.BespeakServiceTime,a.ExcuteServiceTime,a.RecordNo,a.IsBookSame,a.IsExecSame "
				   		+" from LHMedicalServiceRecord a,LDPerson b"                                             
				   		+" where a.CustomerNo = b.CustomerNo "     
				   		+" and a.RecordNo = '"+trecordno+"'"
		//	   		+" and a.HospitCode = c.HospitCode"
		//	   		+" and a.BespeakServiceHospit = c.HospitCode "
		//	   		+" and a.DoctNo = d.DoctNo "
		//	   		+" and a.BespeakServiceDoctNo = d.DoctNo "
		//	   		+" and a.ExcuteServiceDocNo = d.DoctNo "
			   		;
		//     alert(arrResult);
		          arrResult = easyExecSql(strSQL);
		
		          if(arrResult[0][10] == "1")
		          {
				          	isBookOk.style.display = '';
				          	if(arrResult[0][11] == "1")
				          	{
				          		bookItem.style.display='';
											serviceStatetable.style.display='';
				          	}
				          	if(arrResult[0][11] == "2") 
				          	{
				          		serviceExecute.style.display='none';
				          		bookFail.style.display='';
				          	}
		          }
		          if(arrResult[0][10] == "0")
		          { 
		          			serviceStatetable.style.display = '';
		          }
		          if(arrResult[0][19] == "1")
		          {
		          			bookLeftTime.style.display='';					
		          }
		          if(arrResult[0][19] == "2" && arrResult[0][11] != "2")
		          { 
		          			serviceExecute.style.display='';
		          }
		          if(arrResult[0][19] == "3")
		          {
		          			serviceCancel.style.display='';
		          }         
		        ; 
		          
		          
		          fm.all('CustomerNo').value = arrResult[0][0];
		          fm.all('CustomerName').value = arrResult[0][1];
		          fm.all('FirstRecoDate').value = arrResult[0][2];
		          fm.all('ServiceState').value = arrResult[0][3];if(arrResult[0][3]=="1"){fm.all('ServiceState_ch').value="进行中";}if(arrResult[0][3]=="2"){fm.all('ServiceState_ch').value="服务结束";}
		          fm.all('ApplyDutyItemCode').value = arrResult[0][4];if(arrResult[0][4] !="" ){fm.all('DutyItemName').value = easyExecSql(" select codename from ldcode where codetype = 'healthservice' and code = '"+arrResult[0][4]+"'");}
		          fm.all('DescriptionDetail').value = arrResult[0][5];
		          fm.all('ServiceDate').value = arrResult[0][6];
		          fm.all('HospitCode').value = arrResult[0][7];
		          if(arrResult[0][7] !="" )
		          	{fm.all('HospitName').value = easyExecSql("select hospitName from ldhospital where hospitcode = '"+arrResult[0][7]+"'");}
		          fm.all('DoctNo').value = arrResult[0][8];
		          if(arrResult[0][8] !="" )
		          	{fm.all('DoctName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][8]+"'");}
		          fm.all('ApplyReceiveType').value = arrResult[0][9];if(arrResult[0][9]=="1"){fm.all('ApplyReceiveType_ch').value ="电话";}if(arrResult[0][9]=="2"){fm.all('ApplyReceiveType_ch').value ="电子邮件";}if(arrResult[0][9]=="3"){fm.all('ApplyReceiveType_ch').value ="其他";}
		          fm.all('ServiceFlag').value = arrResult[0][10];if(arrResult[0][10]=="0"){fm.all('ServiceFlag_ch').value ="否";}if(arrResult[0][10]=="1"){fm.all('ServiceFlag_ch').value ="是";}
		          fm.all('IsBespeakOk').value = arrResult[0][11];if(arrResult[0][11]=="1"){fm.all('IsBespeakOk_ch').value ="预约成功";}if(arrResult[0][11]=="2"){fm.all('IsBespeakOk_ch').value ="预约失败";}
		          fm.all('BespeakServiceItem').value = arrResult[0][12];if(arrResult[0][12] !="" ){fm.all('BespeakServiceItemName').value = easyExecSql(" select codename from ldcode where codetype = 'healthservice' and code = '"+arrResult[0][12]+"'");}
		          fm.all('ApplyServiceDes').value = arrResult[0][13];
		          fm.all('BespeakServiceDate').value = arrResult[0][14];
		          fm.all('BespeakServiceHospit').value = arrResult[0][15];
		          if(arrResult[0][15] !="" )
		          	{fm.all('BespeakServiceHospitName').value = easyExecSql("select hospitName from ldhospital where hospitcode = '"+arrResult[0][15]+"'");}
		          fm.all('BespeakServiceDoctNo').value = arrResult[0][16];
		          if(arrResult[0][16] !="" )
		          	{fm.all('BespeakServiceDoctName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][16]+"'");}
		          fm.all('NoCompareCause').value = arrResult[0][17];
		          fm.all('BespeaLostCause').value = arrResult[0][18];
		          fm.all('ServiceExecState').value = arrResult[0][19];
		          if(arrResult[0][19] !="" )
		          	{fm.all('ServiceExecState_ch').value = easyExecSql("select codename from ldcode where codetype = 'servicestate' and code = '"+arrResult[0][19]+"'");}
		//          fm.all('BespeaLLeaveTime').value = arrResult[0][20];
		          fm.all('ExcuteServiceItem').value = arrResult[0][21];if(arrResult[0][21] !="" ){fm.all('ExcuteServiceItemName').value = easyExecSql(" select codename from ldcode where codetype = 'healthservice' and code = '"+arrResult[0][21]+"'");}
		          fm.all('ExcuteDescription').value = arrResult[0][22];
		          fm.all('ExcuteServiceDate').value = arrResult[0][23];
		          fm.all('ExcuteServiceHospit').value = arrResult[0][24];
		          if(arrResult[0][24] !="" )
		          	{fm.all('ExcuteServiceHospitName').value = easyExecSql("select hospitName from ldhospital where hospitcode = '"+arrResult[0][24]+"'");}
		          fm.all('ExcuteServiceDocNo').value = arrResult[0][25];
		          if(arrResult[0][25] !="" )
		          	{fm.all('ExcuteServiceDocName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][25]+"'");}
		          fm.all('ExNoCompareCause').value = arrResult[0][26];
		          fm.all('ExcutePay').value = arrResult[0][27];
		          fm.all('SatisfactionDegree').value = arrResult[0][28];
		          if(arrResult[0][28] !="" )
		         		 {fm.all('SatisfactionDegree_ch').value = easyExecSql("select codename from ldcode where codetype = 'hmsatisfy' and code = '"+arrResult[0][28]+"'");}
		          fm.all('OtherServiceInfo').value = arrResult[0][29];
		          fm.all('CancleServiceCause').value = arrResult[0][30];
		          fm.all('RecordNo').value = arrResult[0][31];
		          fm.all('MakeDate').value = arrResult[0][32];
		          fm.all('MakeTime').value = arrResult[0][33];
		          fm.all('FirstRecoTime').value = arrResult[0][34];
		          fm.all('ServiceTime').value = arrResult[0][35];
		          fm.all('BespeakServiceTime').value = arrResult[0][36];
		          fm.all('ExcuteServiceTime').value = arrResult[0][37];
		          fm.all('RecordNo').value = arrResult[0][38];
		          fm.all('IsApplaySame').value = arrResult[0][39];
		          fm.all('IsExecSame').value = arrResult[0][40];  
		          fm.all('IsApplaySame_ch').value=  (arrResult[0][39]=="1")?"是":"否";if(arrResult[0][11] == "2"){fm.all('IsApplaySame').value="";fm.all('IsApplaySame_ch').value="";}
		          fm.all('IsExecSame_ch').value=  (arrResult[0][40]=="1")?"是":"否";
       
									
									
//								  var strSQL =  " select a.CustomerNo, b.Name,a.FirstRecoDate,a.ServiceState,a.ApplyDutyItemCode,a.DescriptionDetail,"
//								 			    	 	+ " a.ServiceDate,a.HospitCode,a.DoctNo,a.ApplyReceiveType,a.ServiceFlag,a.IsBespeakOk,a.BespeakServiceItem,"
//								 			    	 	+ " a.ApplyServiceDes,a.BespeakServiceDate,a.BespeakServiceHospit,a.BespeakServiceDoctNo,a.NoCompareCause,"
//								 			    	 	+ " a.BespeaLostCause,a.ServiceExecState,a.ServiceExecState,a.ExcuteServiceItem,a.ExcuteDescription,"
//								 			    	 	+ " a.ExcuteServiceDate,a.ExcuteServiceHospit,a.ExcuteServiceDocNo,a.ExNoCompareCause,a.ExcutePay,"
//								 			    	 	+ " a.SatisfactionDegree,a.OtherServiceInfo,a.CancleServiceCause,a.RecordNo,a.MakeDate,a.MakeTime, "
//								 			    	 	+ " a.FirstRecoTime,a.ServiceTime,a.BespeakServiceTime,a.ExcuteServiceTime,a.RecordNo "
//								 			    	 	+ " from LHMedicalServiceRecord a,LDPerson b"
//								 			    	 	+ " where a.CustomerNo = b.CustomerNo  "     
//								 			    	 	+ " and a.RecordNo = '"+trecordno+"'"
//								 		//	    	+ " and a.HospitCode = c.HospitCode"
//								 		//	    	+ " and a.BespeakServiceHospit = c.HospitCode "
//								 		//	    	+ " and a.DoctNo = d.DoctNo "
//								 		//	    	+ " and a.BespeakServiceDoctNo = d.DoctNo "
//								 		//	    	+ " and a.ExcuteServiceDocNo = d.DoctNo "
//								 			    		;
//							     
//							          arrResult = easyExecSql(strSQL);
//							
//							          if(arrResult[0][10] == "1")
//							          {
//									          	isBookOk.style.display = '';
//									          	if(arrResult[0][11] == "1")
//									          	{
//									          		bookItem.style.display='';
//																serviceStatetable.style.display='';
//									          	}
//									          	if(arrResult[0][11] == "0")
//									          	{
//									          		bookFail.style.display='';
//									          	}
//							          }
//							          if(arrResult[0][10] == "0")
//							          { 
//							          			serviceStatetable.style.display = '';
//							          }
//							          if(arrResult[0][19] == "1")
//							          {
//							          			bookLeftTime.style.display='';					
//							          }
//							          if(arrResult[0][19] == "2")
//							          {
//							          			serviceExecute.style.display='';
//							          }
//							          if(arrResult[0][19] == "3")
//							          {
//							          			serviceCancel.style.display='';
//							          }         
//							        ; 
//							          
//							          
//							          fm.all('CustomerNo').value = arrResult[0][0];
//							          fm.all('CustomerName').value = arrResult[0][1];
//							          fm.all('FirstRecoDate').value = arrResult[0][2];
//							          fm.all('ServiceState').value = arrResult[0][3];if(arrResult[0][3]=="1"){fm.all('ServiceState_ch').value="进行中";}if(arrResult[0][3]=="2"){fm.all('ServiceState_ch').value="服务结束";}
//							          fm.all('ApplyDutyItemCode').value = arrResult[0][4];
//							          fm.all('DescriptionDetail').value = arrResult[0][5];
//							          fm.all('ServiceDate').value = arrResult[0][6];
//							          fm.all('HospitCode').value = arrResult[0][7];
//							          	fm.all('HospitName').value = easyExecSql("select hospitName from ldhospital where hospitcode = '"+arrResult[0][7]+"'");
//							          fm.all('DoctNo').value = arrResult[0][8];
//							          	fm.all('DoctName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][8]+"'");
//							          fm.all('ApplyReceiveType').value = arrResult[0][9];if(arrResult[0][9]=="1"){fm.all('ApplyReceiveType_ch').value ="电话";}if(arrResult[0][9]=="2"){fm.all('ApplyReceiveType_ch').value ="电子邮件";}if(arrResult[0][9]=="3"){fm.all('ApplyReceiveType_ch').value ="其他";}
//							          fm.all('ServiceFlag').value = arrResult[0][10];if(arrResult[0][10]=="0"){fm.all('ServiceFlag_ch').value ="否";}if(arrResult[0][10]=="1"){fm.all('ServiceFlag_ch').value ="是";}
//							          fm.all('IsBespeakOk').value = arrResult[0][11];if(arrResult[0][11]=="1"){fm.all('IsBespeakOk_ch').value ="预约成功";}if(arrResult[0][11]=="2"){fm.all('IsBespeakOk_ch').value ="预约失败";}
//							          fm.all('BespeakServiceItemName').value = arrResult[0][12];
//							          fm.all('ApplyServiceDes').value = arrResult[0][13];
//							          fm.all('BespeakServiceDate').value = arrResult[0][14];
//							          fm.all('BespeakServiceHospit').value = arrResult[0][15];
//							          	fm.all('BespeakServiceHospitName').value = easyExecSql("select hospitName from ldhospital where hospitcode = '"+arrResult[0][15]+"'");
//							          fm.all('BespeakServiceDoctNo').value = arrResult[0][16];
//							          	fm.all('BespeakServiceDoctName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][16]+"'");
//							          fm.all('NoCompareCause').value = arrResult[0][17];
//							          fm.all('BespeaLostCause').value = arrResult[0][18];
//							          fm.all('ServiceExecState').value = arrResult[0][19];
//							          fm.all('ServiceExecState_ch').value = easyExecSql("select codename from ldcode where codetype = 'servicestate' and code = '"+arrResult[0][19]+"'");
//							          fm.all('BespeaLLeaveTime').value = arrResult[0][20];
//							          fm.all('ExcuteServiceItemName').value = arrResult[0][21];
//							          fm.all('ExcuteDescription').value = arrResult[0][22];
//							          fm.all('ExcuteServiceDate').value = arrResult[0][23];
//							          fm.all('ExcuteServiceHospit').value = arrResult[0][24];
//							          	fm.all('ExcuteServiceHospitName').value = easyExecSql("select hospitName from ldhospital where hospitcode = '"+arrResult[0][24]+"'");
//							          fm.all('ExcuteServiceDocNo').value = arrResult[0][25];
//							          	fm.all('ExcuteServiceDocName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][25]+"'");
//							          fm.all('ExNoCompareCause').value = arrResult[0][26];
//							          fm.all('ExcutePay').value = arrResult[0][27];
//							          fm.all('SatisfactionDegree').value = arrResult[0][28];
//							          fm.all('OtherServiceInfo').value = arrResult[0][29];
//							          fm.all('CancleServiceCause').value = arrResult[0][30];
//							          fm.all('RecordNo').value = arrResult[0][31];
//							          fm.all('MakeDate').value = arrResult[0][32];
//							          fm.all('MakeTime').value = arrResult[0][33];
//							          fm.all('FirstRecoTime').value = arrResult[0][34];
//							          fm.all('ServiceTime').value = arrResult[0][35];
//							          fm.all('BespeakServiceTime').value = arrResult[0][36];
//							          fm.all('ExcuteServiceTime').value = arrResult[0][37];
//							          fm.all('RecordNo').value = arrResult[0][38];
								}							
						}
	}
</script>
</html>
