<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LHAddressCaseInput.jsp
//程序功能：
//创建日期：2008-08-27
//创建人  ：xiep
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LHAddressCaseInput.js"></SCRIPT>
	<%@include file="LHAddressCaseInit.jsp"%>
</head>

<body onload="initElementtype();initForm();">    
  <form action="AddressCaseList.jsp" method=post name=fm target="fraSubmit">
    <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">通讯事件/短信服务管理</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divInfoGetData" style= "display: ''">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
						<TD  class= title>保单有效时间</TD>
	        	  <TD  class= input> 
	         	 	<Input name=ValiDate class='coolDatePicker' dateFormat='short' verify="保单有效时间|notnull&Date" elementtype=nacessary> 
	        	  </TD> 
				  <TD class= title >保单机构</TD>
				  <TD  class= input>
					
					<Input class= "codeno"  name=MngCom  ondblclick="return showCodeList('hmhospitalmng',[this,MngCom_ch],[1,0],null,null,null,1,180);" onkeyup="return showCodeListKey('hmhospitalmng',[this,MngCom_ch],[1,0],null,null,null,1,180);" readonly verify="管理机构|NOTNULL"><Input class=codename  name=MngCom_ch elementtype=nacessary>
				  
				  </TD>
	        </TR>
	    </table>
	    <br>	
		<Input type=hidden  name="tFlag">
		<INPUT class=cssButton  VALUE="通讯管理查询" TYPE=button onClick="AddressCaseQuery();">
		<INPUT class=cssButton  VALUE="短信服务查询" TYPE=button onClick="MobilesServQuery();">	
		<INPUT class=cssButton  VALUE="健管讲座服务查询" TYPE=button onClick="LectureCourseQuery();">	
		<INPUT class=cssButton  VALUE="个人咨询服务查询" TYPE=button onClick="ConsultServQuery();">	
		</Div> 
		<div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">健管服务计划的手动流转</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divRunGetData" style= "display: ''">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
						<TD  class= title>起始时间</TD>
	        	  <TD  class= input> 
	         	 	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始时间|notnull&Date" elementtype=nacessary> 
	        	  </TD> 
	        	  <TD  class= title>终止时间</TD>
	        	  <TD  class= input> 
	         	 	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止时间|notnull&Date" elementtype=nacessary> 
	        	  </TD> 
				  <TD class= title >机构</TD>
				  <TD  class= input>
					<Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('hmhospitalmng',[this,ManageCom_ch],[1,0],null,null,null,1,180);" onkeyup="return showCodeListKey('hmhospitalmng',[this,ManageCom_ch],[1,0],null,null,null,1,180);" readonly verify="管理机构|NOTNULL"><Input class=codename  name=ManageCom_ch elementtype=nacessary>
				  </TD>
	        </TR>
	    </table>
	    <br>		
		<INPUT class=cssButton  VALUE="开始流转" TYPE=button onClick="StartRun();">
		</Div> 
				
		
  </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 