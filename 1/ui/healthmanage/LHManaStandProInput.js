//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass(); 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	  var sqlStandCode="select StandCode  from LHManaStandPro"
	                +" where  StandCode='"+fm.all('StandCode').value+"'";
	  var arrResult = new Array(); 
    arrResult=easyExecSql(sqlStandCode);
    if(fm.all('StandCode').value==arrResult)
    {
    	 alert("此问题(分类)代码的信息已经存在,不允许重复保存!");
    	 return false;
    }
    else
    {
	     var i = 0;
	     if( verifyInput2() == false ) return false;
	     fm.fmtransact.value="INSERT||MAIN";
	     //var mulSql = "select distinct ServPlanCode,ServPlanLevel,ComID from  LHHealthServPlan ";		    
	     //var arrResult=easyExecSql(mulSql);
         
	     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	     fm.submit(); //提交   
	  }
}    
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
    var muySql = "select QuOptionNum,QuOptionCont,PertainInfo, "
				         +" QuOptionNo,StandCode "                                  
				         +" from  LHStaQuOption a  "
				         +" where a.StandCode ='"+fm.all('StandCode').value+"'"
				         ;
				turnPage.pageLineNum = 50;
				//alert(muySql);	 
			  //alert(easyExecSql(muySql));
				turnPage.queryModal(muySql, LHStaQuOptionGrid);
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHHealthServPlanInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}         
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	//下面增加相应的代码
	if (confirm("您确实想修改该记录吗?"))
	{
		  if( verifyInput2() == false ) return false;
  		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  		fm.fmtransact.value = "UPDATE||MAIN";
  		fm.submit(); //提交
	}
	else
	{
    	//mOperate="";
    	alert("您取消了修改操作！");
	}
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  showInfo=window.open("./LHManaStandProQueryInput.html","信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  	var i = 0;
  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  	fm.fmtransact.value = "DELETE||MAIN";
  	fm.submit(); //提交
  	initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
  //alert(arrQueryResult);
	if( arrQueryResult != null )
	{
		  arrResult = arrQueryResult;
			var strSql = " select distinct  StandCode,StandContent,"
	           +" a.StandLevel, "
	           +" a.StandUnit,"
	           +" a.QuesStauts,"
	           +" a.QuerAffiliated, "
	           +" a.QuerResType ,"
	           +" ResuAffiInfo,"
	           +" a.StandQuerType "
	           +" from  LHManaStandPro a  "
			    	 +" where a.StandCode ='"+arrResult[0][0]+"'"	
			    	 ;	
			//alert(strSql);	 
			arrResult=easyExecSql(strSql);
			//alert(arrResult);
			if(arrResult!=null&&arrResult!=""&&arrResult!="null")
			{
        	fm.all('StandCode').value		= arrResult[0][0];
        	fm.all('StandContent').value		= arrResult[0][1];
        	fm.all('StandLevel').value   = arrResult[0][2];
        	fm.all('StandLevelName').value=easyExecSql("select b.codename from ldcode b where codetype='lhcodelabelgrade' and b.code='"+fm.all('StandLevel').value+"'");
        	fm.all('StandUnit').value		= arrResult[0][3];
        	fm.all('QuesStauts').value	= arrResult[0][4];
        	fm.all('QuesStautsName').value=easyExecSql("select b.codename from ldcode b where codetype='lhquestand' and b.code='"+fm.all('QuesStauts').value+"'");
        	fm.all('QuerAffiliated').value		= arrResult[0][5];
        	fm.all('QuerAffiliatedName').value=easyExecSql("select b.codename from ldcode b where codetype='lhincludeques' and b.code='"+fm.all('QuerAffiliated').value+"'");
        	//alert(arrResult[0][6]);
        	if(arrResult[0][6]=="2"||arrResult[0][6]=="3")
        	{
        		 divLHStaQuOptionInfo.style.display='none';
        	}
        	else
        	{
        		divLHStaQuOptionInfo.style.display='';
        	}
        	fm.all('QuerResType').value	= arrResult[0][6];
        	fm.all('QuerResTypeName').value=easyExecSql("select b.codename from ldcode b where codetype='lhqueresultype' and b.code='"+fm.all('QuerResType').value+"'");
        	fm.all('ResuAffiInfo').value    = arrResult[0][7];
        	fm.all('StandQuerType').value    = arrResult[0][8];
        	fm.all('StandQuerTypeName').value=easyExecSql("select b.codename from ldcode b where codetype='lhquesrestype' and b.code='"+fm.all('StandQuerType').value+"'");
      }
			var muySql = "select QuOptionNum,QuOptionCont,PertainInfo, "
				         +" QuOptionNo,StandCode "                                  
				         +" from  LHStaQuOption a  "
				         +" where a.StandCode ='"+arrResult[0][0]+"'"
				         ;
				turnPage.pageLineNum = 50;
				//alert(muySql);	 
			  //alert(easyExecSql(muySql));
				turnPage.queryModal(muySql, LHStaQuOptionGrid);
	}  
}      
                
function afterCodeSelect(codeName,Field)
{ 
	if(codeName=="lhqueresultype")
	{
		 if(Field.value=="2"||Field.value=="3")
		 {
		 	  divLHStaQuOptionInfo.style.display='none';
		 }
		 else
		 {
		 	 divLHStaQuOptionInfo.style.display='';
		 }
	}
}