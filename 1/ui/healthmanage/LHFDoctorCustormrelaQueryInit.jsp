<%
//程序名称：LHFDoctorCustormrelaQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-17 15:00:30
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('FamilyDoctorNo').value = "";
    fm.all('FamilyDoctorName').value = "";
    fm.all('CustomerNo').value = "";
    fm.all('Name').value = "";
    fm.all('FDSStart').value = "";
    fm.all('FDSEnd').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LHFDoctorCustormrelaQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHFDoctorCustormrelaGrid();  
  }
  catch(re) {
    alert("LHFDoctorCustormrelaQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHFDoctorCustormrelaGrid;
function initLHFDoctorCustormrelaGrid() {                               
  var iArray = new Array();
    
  try {
		    iArray[0]=new Array();
		    iArray[0][0]="序号";         		//列名
		    iArray[0][1]="30px";         		//列名
		    iArray[0][3]=0;         		    //列名
		    iArray[0][4]="station";         //列名
    
 				iArray[1]=new Array();
		    iArray[1][0]="医师代码";         		//列名
		    iArray[1][1]="80px";         		//列名
		    iArray[1][3]=0;         		    //列名
		            		    
		    iArray[2]=new Array();
		    iArray[2][0]="医师姓名";         		//列名
		    iArray[2][1]="80px";         		//列名
		    iArray[2][3]=0;         		    //列名

		    iArray[3]=new Array();
		    iArray[3][0]="客户号";         		//列名
		    iArray[3][1]="60px";         		//列名
		    iArray[3][3]=0;         		    //列名
		    
		    iArray[4]=new Array();
		    iArray[4][0]="客户姓名";         		//列名
		    iArray[4][1]="60px";         		//列名
		    iArray[4][3]=0;         		    //列名
		    
		    iArray[5]=new Array();
		    iArray[5][0]="服务起始时间";         		//列名
		    iArray[5][1]="80px";         		//列名
		    iArray[5][3]=0;         		    //列名
		    
		    iArray[6]=new Array();
		    iArray[6][0]="服务终止时间";         		//列名
		    iArray[6][1]="80px";         		//列名
		    iArray[6][3]=0;         		    //列名
		    
		    iArray[7]=new Array();
		    iArray[7][0]="MakeDate";         		//列名
		    iArray[7][1]="0px";         		//列名
		    iArray[7][3]=3;         		    //列名  
    
    		iArray[8]=new Array();                        
        iArray[8][0]="MakeTime";         		//列名
        iArray[8][1]="0px";         		//列名
        iArray[8][3]=3;         		    //列名
        
        iArray[9]=new Array();                        
        iArray[9][0]="ManageCom";         		//列名
        iArray[9][1]="0px";         		//列名
        iArray[9][3]=3;         		    //列名
    
    LHFDoctorCustormrelaGrid = new MulLineEnter( "fm" , "LHFDoctorCustormrelaGrid" ); 
    //这些属性必须在loadMulLine前

    LHFDoctorCustormrelaGrid.mulLineCount = 0;   
    LHFDoctorCustormrelaGrid.displayTitle = 1;
    LHFDoctorCustormrelaGrid.canSel = 1;
    LHFDoctorCustormrelaGrid.hiddenPlus = 1;
    LHFDoctorCustormrelaGrid.hiddenSubtraction = 1;
/*        
    LHFDoctorCustormrelaGrid.canChk = 0;
    LHFDoctorCustormrelaGrid.selBoxEventFuncName = "showOne";
*/
    LHFDoctorCustormrelaGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHFDoctorCustormrelaGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
