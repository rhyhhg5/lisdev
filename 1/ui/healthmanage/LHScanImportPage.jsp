<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHScanImportPage.jsp
//程序功能：扫描件录入页面
//创建日期：2006-06-08 9:51:12
//创建人  ：郭丽颖
//更新记录： 
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <%@include file="LHScanImportPageInit.jsp"%>
  <SCRIPT src="LHScanImportPage.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>

<body  onload="initForm();">
	<form action="./LHScanImportPageSave.jsp" method=post name=fm target="fraSubmit">
		<div  id='info'>
     <table class= common  align='center' >
    	<TR  class= common>          
          <TD  class= title>
                扫描件编号
           </TD>
           <TD  class= input>
                <Input class= 'common' name=ScanNo verify="扫描件编号|NOTNULL&len<=50" readonly>
            </TD>
            <TD  class= title>
                扫描件名称
            </TD>
            <TD  class= input>
                <Input class= 'common' name=ScanName  verify="扫描件名称|len<=24">
            </TD>
            <TD  class=title >
            	 扫描件维护状态
            </TD>
            <TD>
                <Input class= 'codename' style="width:50px" name=ScanStatus><Input class= 'codeno'  style="width:130px" name=ScanStatusName verify="扫描件维护状态|NOTNULL&len<=200" CodeData= "0|^1|全部录入^2|部分录入^3|尚未录入"   ondblclick="showCodeListEx('',[this,ScanStatus],[1,0],null,null,null,'1',160);">
            </TD>
            <td class=button width="60%" align=right >
			   	   <INPUT class=cssButton VALUE="保 存"  style="width:60px"  TYPE=button onclick="submitForm();">
			      </td>
       </tr>
     </table>
   </Div>
   <input type=hidden  name="RelationType">
<input   name="RelationNo">
<input  name="RelationNo2">
<input type = hidden name = "MakeDate">
<input type = hidden name = "MakeTime">
 <input type=hidden id="fmtransact" name="fmtransact">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>