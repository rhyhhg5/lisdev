<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-11-02 14:02:31
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LOMsgReceiveGrpInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

</head>
	<body  onload="initForm();" >
	  <form name="fm" method="post" action="LOMsgReceiveGrpSetSave.jsp"  target="fraSubmit"  >	
	  <%@include file="LOMsgReceiveGrpInputInit.jsp"%>
	 			<table  class=common align=center>
					<tr align=right>
						<td class=button >
							&nbsp;&nbsp;
						</td>
						<td class=button width="10%" align=right>
							<INPUT class=cssButton name="SaveButton" VALUE="新建分组"  TYPE=button onclick="SaveEvent();">
						</td>
						<td class=button width="10%" align=right>
							<INPUT class=cssButton name="UpdateButton" VALUE="修改分组"  TYPE=button onclick="updateClick();">
						</td>			
						<td class=button width="10%" align=right>
							<INPUT class=cssButton name="DeleteButton" VALUE="删除分组"  TYPE=button onclick="deleteClick();">
						</td>			
					</tr>
				</table>
		 		<table>
		    	<tr>
		    		<td>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,GrpQueryInfo);">
		    		</td>
		    		<td class= titleImg>
		        	分组查询条件
		       	</td>   		 
		    	</tr>
		    </table>
				<Div id=GrpQueryInfo>
					<table class=common>
						<tr class=common>
							<td class=title>
								客户组号码
							</td>
							<td class=input>
								<input class=common name=CustomGrpNo>
							</td>
							<td class=title>
								客户组名称
							</td>
							<td class=input>
								<input class=common name=CustomGrpName>
							</td>
							<td class=title>
								短信类型
							</td>
							<td class=input>
								<input class=common>
							</td>
							<td class=title>
								设置时间
							</td>
							<td class=input>
								<input class=coolDatePicker name=MakeDate>
							</td>
						</tr>
					</table>
					<table class=common>
						<tr class=common>
							<td class=input>
								<input type=button class=cssButton value="查 询" OnClick="queryGrp()">
								<input type=button class=cssButton value="返 回" OnClick="returnParent();">
							</td>		
						</tr>
					</table>
				</Div>
			<table>
		    <tr>
		    	<td>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivGrpInfo);">
		    	</td>
		    	<td class= titleImg>
		        	分组信息
		      </td>   		 
		    </tr>
		  </table>
		  <Div id="DivGrpInfo" style="display:''">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLOMsgReceiveGrpGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>
			<Div id= "divPage" align=center style= "display: '' ">
		    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  		</Div>
			<table>
		    <tr>
		    	<td>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivEachGrpInfo);">
		    	</td>
		    	<td class= titleImg>
		        	客户组详细信息
		      </td>   		 
		    </tr>
		  </table>
		  <Div id="DivEachGrpInfo" style="display:''">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLOMsgEachGrpInfoGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>
		<input type=hidden name=GrpNo>
		<input type=hidden name=fmtransact>
	  </form>
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>