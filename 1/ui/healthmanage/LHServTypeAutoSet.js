//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass();
//window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}
//提交，保存按钮对应操作
function submitForm() {

  var i = 0;
  if( verifyInput2() == false )
    return false;

     // if( checkValue2() == false )
  //  return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
  
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
function queryClick() 
{
  showInfo=window.open("./LHServTypeQuery.html");
}
function afterQuery1(arrResult)
{
	var strSql="select ServItemCode,ContType,ComID,ServCaseType from LHServCaseTypeDef where ServItemCode ='"+ arrResult[0][0]+"' and ContType ='"+arrResult[0][2]+"' and ComID ='"+arrResult[0][4]+"'"
  	         	;

  	  var arrLHServEventStatus=easyExecSql(strSql);
  	  
  	   	  
  	  if(arrLHServEventStatus!=null) 
  	  {
 
  	  	fm.all('ServItemCode').value=arrLHServEventStatus[0][0];
  	  	fm.all('ServItemName').value=easyExecSql("select Servitemname from LHHealthServItem where Servitemcode ='"+arrLHServEventStatus[0][0]+"'");
  	    fm.all('ContType').value=arrLHServEventStatus[0][1];
  	    if (arrLHServEventStatus[0][1]==1)
  	  	fm.all('ContType_ch').value="个人保单"
  	  	else
  	  	  fm.all('ContType_ch').value="团体保单"	
  	    fm.all('ComID').value=arrLHServEventStatus[0][2];
  	    fm.all('ComID_ch').value=easyExecSql("select Name from ldcom where Comcode = '"+arrLHServEventStatus[0][2]+"'");
  	    fm.all('ServCaseType').value=arrLHServEventStatus[0][3];
  	    if (arrLHServEventStatus[0][3]==1)
  	  	fm.all('ServCaseType_ch').value="个人服务事件"
  	  	else
  	  	  fm.all('ServCaseType_ch').value="集体服务事件"	
  	    
  	  }
}
function updateClick()
{
	//下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
   fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}
function deleteClick()
{
	if(fm.all('ContType').value == "")
	{
		alert("请先查询返回，再删除！");
		return false;
	}
	
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}