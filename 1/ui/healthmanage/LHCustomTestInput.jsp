<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-17 18:32:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHCustomTestInput.js"></SCRIPT>
  <%@include file="LHCustomTestInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >

  <form action="./LHCustomTestSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomTestInput1);">
    		</td>
    		<td class= titleImg>
        		 一般信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomTestInput1" style= "display: ''">
    	
      <table  class= common align='center' style= "width: '94%'" >
          <TR  class= common>
           <TD  class= title>
             客户号码
           </TD>
           <TD  class= input>
             <Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=24" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
           </TD>
           <TD  class= title>
             客户姓名
           </TD>
           <TD  class= input>
             <Input class= 'common' name=CustomerName  >
           </TD>
            <TD  class= title>
              体检日期
            </TD>
            <TD  class= input>
              <Input class= 'coolDatePicker' dateFromat="Short" name=InHospitDate verify="体检日期|DATE">
            </TD>
          </TR>
          <TR  class= common>
			<TD  class= title>
              体检机构名称
            </TD>
            <TD  class= input>
            	<Input class=codeno name=HospitCode ondblclick="return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');"  onkeydown="  return showCodeListKey('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName')	; codeClear(HospitName,HospitCode); "  verify="体检机构名称|len<=60"><Input class= 'codename' name=HospitName >
            </TD>
            <TD  class= title>
            	体检方式
            </TD>
            <TD  class= input >
            	<Input class=codeno name=TestModeCode style="width:60px" readonly style="background-color:#F7F7F7;" verify="体检方式|notnull" ><Input class=codeName style="width:88px; background-color:#D7E1F6;" name=TestMode ondblclick="return showCodeList('lhtestmode',[this,TestModeCode],[0,1]);" onkeydown="return showCodeListKey('lhtestmode',[this,TestModeCode],[0,1]);" verify="体检方式代码|len<=10"><font color=red> *</font>
            </TD>
            <!--TD  class= input type = hidden -->
				<Input type=hidden name=DoctNo >
				<Input class= 'code' type = hidden name=DoctName ondblclick="return showCodeList('lhdoctname',[this,DoctNo],[0,1],null,fm.DoctName.value,'DoctName');" onkeyup=" if(event.keyCode ==13) return showCodeList('lhdoctname',[this,DoctNo],[0,1],null,fm.DoctName.value,'DoctName');" onkeydown=" return showCodeListKey('lhdoctname',[this,DoctNo],[0,1],null,fm.DoctName.value,'DoctName');  codeClear(DoctName,DoctNo); "  verify="主诊医师姓名|len<=20">
            
        <TD class=title>      	
			</TD>
			<TD class=input>      	
			</TD>
       </TR>
      <tr>
      	<TD  class= title>
          备注
        </TD>
        <TD  class= input colSpan=5>
          <textarea class= 'common'  name=MainItem verify="主诉|len<=5000" style="width:683px;height:50px" ></textarea>    
        </TD>
     </tr>   
      </table>
    </Div>
   
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomTestInput2);">
    		</td>
    		<td class= titleImg>
        		 体检信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomTestInput2" style= "display: ''">
        <table class=common align='center' style= "width: '94%'" >
		      <tr>		      	
   	      	<TD  class= title>
              选择套餐
            </TD>
            <TD  class= input >
            	<Input class= 'codeno' id=MedicaItemName readonly name=MedicaItemGroup ondblclick="if(checkTestMode()==false) return false;fm.all('queryString').value = fm.all('TestModeCode').value + fm.all('HospitCode').value; showCodeList('hmchoosetest',[this,MedicaItemGroup],[1,0],null,fm.queryString.value,'TestModeCode',1);" onkeydown=" showCodeListKey('hmchoosetest',[this,MedicaItemGroup],[1,0],null,fm.queryString.value,'TestModeCode',1);" verify="选择套餐|len<=10"><Input class= codename name=MedicaItemGroup>
            </TD>
						<TD class=title>      	
						</TD>
						<TD class=input>      	
						</TD>
					  <TD  class= title >
      			</TD>
			      <TD  class= input type = hidden>
			        <Input class= 'common' type = hidden name=TestFee verify="体检总费用|num&len<=12">
			      </TD>
            <!--TD  class= title>
            	填写结果
            </TD>
            <TD  class= input>
            	<Input type=hidden name=SelectedItemCode>
            	<Input class= 'code' id=SelectedItem name=SelectedItem ondblclick="return showCodeListEx('SelectedItem',[this,SelectedItemCode],[0,1],null,null,null,1,240);"  verify="填写结果|len<=50" onFocus="easyQueryClickSingle();">
            </TD-->

           
            
		      </tr>
	      </table>
	      <TABLE style=" cellSpacing=0 cellPadding=0 width=300 border=0 SCROLLBAR-FACE-COLOR: #799AE1; SCROLLBAR-HIGHLIGHT-COLOR: #799AE1; SCROLLBAR-SHADOW-COLOR: #799AE1; SCROLLBAR-DARKSHADOW-COLOR: #799AE1; SCROLLBAR-3DLIGHT-COLOR: #799AE1; SCROLLBAR-ARROW-COLOR: #FFFFFF; SCROLLBAR-TRACK-COLOR: #AABFEC ">			
						<TR>
							<TD>
									<!--DIV id=slightbar name=slightbar style="OVERFLOW:auto; height:230 "-->
										<table class=common>
									    	<tr class=common>
									    		<td width='30px'>
		        	 	           </td>
										  		<td text-align:left colSpan=1 >
									  				<span id="spanCustomTestInfoGrid">
									  				</span> 
											    </td>
												</tr>
										</table>
									<!--/DIV-->
							</TD>
						</TR>
			</TABLE>

    
		  <div id="divTestResult"  style="display: 'none'; position:absolute; slategray">
		       <textarea name="textTestResult" cols="100%" rows="3" witdh=25% class="common" onkeydown="backTestResult();">
		       </textarea>
		  		 <td class=button width="10%" align=right>
		  		 	<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="return getBack();">
		  		 </td>
		  </div>
     <Div id= "divPage"  align=center style= "display: 'none' ">
		    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		  </Div>    

    <!--table  class=common align='center' style= "width: '94%'" >
     <TR class=common>
     	
      <TD  class= title >
        体检总费用
      </TD>
      <TD  class= input>
        <Input class= 'common' name=TestFee verify="体检总费用|num&len<=12">
      </TD>
      <TD   class= title > 
        
      </TD>
      <td class=input  width="10%" align=right>
			</td>  
      <TD  class= title>      	
      </TD>
      <td class=input>
			</td>
     </TR>
    </table-->
  </div>    
 
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTestResult2);">
    		</td>
    		<td class= titleImg>
        		 诊断结果
       	</td>   		 
    	</tr>
    </table>
    <Div id= "divTestResult2" style= "display: ''">
		    <table>
		    		<TR  class= common >
		        	 	<td width='18px'>
		        	 	</td>
		            <TD>
		                <span id="spanTestResultGrid"></span>
		            </TD>
		     		</TR>
		    </table>
	  </Div>
   
    
    <Div  id= "divLHCustomTestCopy" style= "display: 'none'">
        <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomTestCopy);">
    		</td>
    		<td class= titleImg>
        		 核保体检扫描件查看
       	</td>   		 
    	</tr>
    </table>
    <table  class=common align='center' style= "width: '94%'" >
     <TR class=common>     	
      <TD  class= title >
        体检合同号
      </TD>
      <TD  class= input>
        <Input class= 'code' name=ContNo verify="体检合同号|num&len<=20" ondblclick="return showCodeList('lhtestprtno',[this],[0],null,fm.CustomerNo.value,'CustomerNo');" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhtestprtno',[this],[0],null,fm.CustomerNo.value,'CustomerNo');">
      </TD>      
      <td class=button width="10%" align=right>
						<INPUT class=cssButton VALUE="核保体检扫描录入"  TYPE=button onclick=" getTestCopy();">
			</td>
			<TD class=title>
			</TD>
			<TD class=title>
			</TD>
			<TD class=input>
			</TD>
     </TR>
    </table>
    </Div>
     <table align= center>
       	  <tr align= center>
       	  	<td class=button align= center>
		      			<INPUT class=cssButton VALUE="增加体检记录"  TYPE=hidden onclick="return saveTest();">
		      	</td>		
		      	
		      </tr>
		    </table>
    
   
    <div id="div1" style="display: 'none'">
      <table>
        <TR  class= common>
            <TD  class= title>
              操作员代码
            </TD>
            <TD  class= input>
              <Input class= 'common' name=Operator >
            </TD>
            <TD  class= title>
              入机日期
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeDate >
            </TD>
            <TD  class= title>
              入机时间
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeTime >
            </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最后一次修改日期
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyDate >
          </TD>
          <TD  class= title>
            最后一次修改时间
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyTime >
          </TD>
        </TR>     
      </table>
    </div>  
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <Input type=hidden  name=InHospitNo >
    <input type=hidden id="iscomefromquery" name="iscomefromquery">
    <input type=hidden id=queryString name=queryString>
    <input type=hidden id="FeeNo" name="FeeNo">
    <input type=hidden name=TestNo>       <!-- 记录每次体检TestNo，即每个体检项目的编号，以填充到Mulline中-->
    <input type=hidden name=tempCustomerno>		<!-- 记录每次体检tempGroup，即为那一组套餐，以填充到Mulline中-->
 </form>
 <form action="" method=post name=upfm target="fraSubmit" ENCTYPE="multipart/form-data">  
 	<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divupfile);">
    		</td>
    		<td class= titleImg>
        		 文件上传
       	</td>   		 
    	</tr>
    </table>
 		<Div id="divupfile">
	 <table class=common>
 	 		<tr class=common>
		 	 	  <TD  class= input  style="width:50%">
							<input class=common type=file name=getFile  style="width:80%">
							<input class=cssButton type=button name=submitFile value="上传" onclick="fileUpload();">
					</TD>
			</tr>
 	 </table>
 		</Div>
 </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

		<script>
		//通过综合查询进入此页面的处理
		var turnPage = new turnPageClass(); 
		function passQuery()
		{ 
			  //如果不是综合查询进入则跳过此函数
			  if("<%=request.getParameter("Customerno")%>" == "" || "<%=request.getParameter("Customerno")%>" == "null" || "<%=request.getParameter("Healthno")%>" =="" || "<%=request.getParameter("Healthno")%>" =="null")
			  {
			  		return;
			  }
			  
			  //进入此函数
			  else
			  { 		  
					  var CustomerNo_s = "<%=request.getParameter("Customerno")%>";
					  var Inhospitno_s = "<%=request.getParameter("Healthno")%>";
						//alert(CustomerNo_s+"-----"+Inhospitno_s);
					
				  	if(CustomerNo_s!=null && Inhospitno_s!=null) 
				  	{
				  			var strSql = " select a.Customerno,"
				  									+" (select b.name from ldperson b where b.Customerno = a.customerno),"
				  									+" a.Inhospitdate,"
				  									+" (select c.Hospitname from ldhospital c where c.Hospitcode = a.Hospitcode),"
				  									+" "
				  									+" (select e.codename from ldcode e where e.codetype = 'inhospitmode' and e.code = a.inhospitmode) "
				  									+" from lhcustominhospital a "
				  									+" where a.Customerno = '"+CustomerNo_s+"'"
				  									+" and   a.Inhospitno = '"+Inhospitno_s+"'"
				  									;
				  			var arrResult = easyExecSql(strSql);
 
								
								fm.all('CustomerNo').value = arrResult[0][0];
								fm.all('CustomerName').value = arrResult[0][1];
								fm.all('InHospitDate').value = arrResult[0][2];
								fm.all('HospitName').value = arrResult[0][3];
								fm.all('TestMode').value = arrResult[0][4];
								fm.all('DoctName').value = easyExecSql("select distinct a.DoctName from lddoctor a, lhcustomtest b where b.customerno = '"+CustomerNo_s+"' and b.inhospitno = '"+Inhospitno_s+"' and b.Doctno = a.Doctno ");
								fm.all('TestFee').value = easyExecSql(" select a.Feeamount from lhfeeinfo a where a.customerno = '"+CustomerNo_s+"' and a.inhospitno = '"+Inhospitno_s+"'");
 
 							  var strSql2 = " select distinct c.Medicaitemname,a.TestNo,a.TestResult,a.Medicaitemcode,a.Resultunitnum,a.IsNormal "
 							  						 +" from LHCustomTest a,LHCustomInHospital b,lhcountrmedicaitem c "
      											 +" where a.CustomerNo = '"+CustomerNo_s+"'"
      											 +" and a.InHospitNo = '"+Inhospitno_s+"'"
      											 +" and c.Medicaitemcode = a.MedicaItemCode"
      											 +" order by c.Medicaitemname"
      											 ;
      											 turnPage.queryModal(strSql2, CustomTestInfoGrid); 
								
					  } 
					  else 
					  {
					  	  alert("LHMainCustomerHealth.js->没有此次体检");
					  }
		    }
	}
		
	</script>

</html>
