<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDDrugstoreSave.jsp
//程序功能：
//创建日期：2005-01-15 11:16:51
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LDDrugstoreSchema tLDDrugstoreSchema   = new LDDrugstoreSchema();
  LDDrugstoreUI tLDDrugstoreUI   = new LDDrugstoreUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLDDrugstoreSchema.setDrugStoreCode(request.getParameter("DrugStoreCode"));
    tLDDrugstoreSchema.setSuperDrugStoreCode(request.getParameter("SuperDrugStoreCode"));
    tLDDrugstoreSchema.setDrugstoreName(request.getParameter("DrugstoreName"));
    tLDDrugstoreSchema.setFixFlag(request.getParameter("FixFlag"));
    tLDDrugstoreSchema.setCommunFixFlag(request.getParameter("CommunFixFlag"));
    tLDDrugstoreSchema.setAreaCode(request.getParameter("AreaCode"));
    tLDDrugstoreSchema.setEconomElemenCode(request.getParameter("EconomElemenCode"));
    tLDDrugstoreSchema.setBusiScope(request.getParameter("BusiScope"));
    tLDDrugstoreSchema.setStartDate(request.getParameter("StartDate"));
    tLDDrugstoreSchema.setaddress(request.getParameter("address"));
    tLDDrugstoreSchema.setZipCode(request.getParameter("ZipCode"));
    tLDDrugstoreSchema.setPhone(request.getParameter("Phone"));
    tLDDrugstoreSchema.setWebAddress(request.getParameter("WebAddress"));
    tLDDrugstoreSchema.setFax(request.getParameter("Fax"));
    tLDDrugstoreSchema.setDrugstoreLicencNo(request.getParameter("DrugstoreLicencNo"));
    tLDDrugstoreSchema.setbankCode(request.getParameter("bankCode"));
    tLDDrugstoreSchema.setAccName(request.getParameter("AccName"));
    tLDDrugstoreSchema.setbankAccNO(request.getParameter("bankAccNO"));
    tLDDrugstoreSchema.setSatrapName(request.getParameter("SatrapName"));
    tLDDrugstoreSchema.setLinkman(request.getParameter("Linkman"));
    tLDDrugstoreSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDDrugstoreSchema.setMakeTime(request.getParameter("MakeTime"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLDDrugstoreSchema);
  	tVData.add(tG);
    tLDDrugstoreUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDDrugstoreUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
