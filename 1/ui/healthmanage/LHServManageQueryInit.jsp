<%
//程序名称：LHServManageQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-27 18:16:36
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('FDSNo').value = "";
    fm.all('CustomerNo').value = "";
    fm.all('Name').value = "";
    fm.all('FamilyDoctorNo').value = "";
    fm.all('FamilyDoctorName').value = "";
    fm.all('FDSCode').value = "";
    fm.all('FDSSubject').value = "";
    fm.all('FDSContent').value = "";
    fm.all('State').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
    fm.all('FDSDate').value = "";
  }
  catch(ex) {
    alert("在LHServManageQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHServManageGrid();  
  }
  catch(re) {
    alert("LHServManageQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHServManageGrid;
function initLHServManageGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="家庭医生服务登记号";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=0;         		//列名

    iArray[2]=new Array();
    iArray[2][0]="客户姓名";         		//列名
    iArray[2][1]="60px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="家庭医生姓名";         		//列名
    iArray[3][1]="80px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="服务项目";         		//列名
    iArray[4][1]="80px";         		//列名
    iArray[4][3]=0;         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="服务评估";         		//列名
    iArray[5][1]="100px";         		//列名
    iArray[5][3]=0;         		//列名
    
    iArray[6]=new Array();
    iArray[6][0]="服务内容";         		//列名
    iArray[6][1]="100px";         		//列名
    iArray[6][3]=0;         		//列名
    LHServManageGrid = new MulLineEnter( "fm" , "LHServManageGrid" ); 
    //这些属性必须在loadMulLine前

    LHServManageGrid.mulLineCount = 0;   
    LHServManageGrid.displayTitle = 1;
    LHServManageGrid.hiddenPlus = 1;
    LHServManageGrid.hiddenSubtraction = 1;
    LHServManageGrid.canSel = 1;
    LHServManageGrid.canChk = 0;
    //LHServManageGrid.selBoxEventFuncName = "showOne";

    LHServManageGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHServManageGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
