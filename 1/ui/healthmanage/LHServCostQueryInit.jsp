<%
//程序名称：LHServCostQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-03-29 10:45:23
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ServPriceCode').value = "";
    fm.all('ServPriceName').value = "";
    fm.all('BalanceObjCode').value = "";
    fm.all('BalanceObjName').value = "";
    fm.all('ContraNo').value = "";
    fm.all('ContraItemNo').value = "";
  }
  catch(ex) {
    alert("在LHServCostQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHServCostGrid();  
  }
  catch(re) {
    alert("LHServCostQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//领取项信息列表的初始化
var LHServCostGrid;
function initLHServCostGrid() 
{
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";		//列名
	    iArray[0][1]="30px";		//列名
	    iArray[0][3]=0;				//列名
	    
	    iArray[1]=new Array();		
		iArray[1][0]="定价方式代码";		//列名    
		iArray[1][1]="80px";		//列名    
		iArray[1][3]=2;				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="定价方式名称";	//列名    
		iArray[2][1]="120px";       //列名    
		iArray[2][3]=2;         	//列名       
	    
	    
	    
	    iArray[3]=new Array();		
		iArray[3][0]="费用结算对象代码";		//列名    
		iArray[3][1]="80px";		//列名    
		iArray[3][3]=2;				//列名        
		
		iArray[4]=new Array();                    
		iArray[4][0]="费用结算对象名称";	//列名    
		iArray[4][1]="120px";       //列名    
		iArray[4][3]=2;         	//列名        
			
		iArray[5]=new Array();                    
		iArray[5][0]="合同责任项目代码";    //列名    
		iArray[5][1]="80px";       //列名    
		iArray[5][3]=2;         	//列名   
		
		iArray[6]=new Array();  
		iArray[6][0]="合同责任项目名称";
		iArray[6][1]="120px";   
		iArray[6][3]=2;         
		
		iArray[7]=new Array();  
		iArray[7][0]="MakeDate";
		iArray[7][1]="0px";   
		iArray[7][3]=3;         
		
		iArray[8]=new Array();  
		iArray[8][0]="MakeTime";
		iArray[8][1]="0px";   
		iArray[8][3]=3;         
			     
	    
	    LHServCostGrid = new MulLineEnter( "fm" , "LHServCostGrid" ); 
	    //这些属性必须在loadMulLine前           
	    
		LHServCostGrid.canSel = 1;
		LHServCostGrid.hiddenPlus = 0;
		LHServCostGrid.hiddenSubtraction = 0;
	/*
	    LHServCostGrid.mulLineCount = 0;   
	    LHServCostGrid.displayTitle = 1;
	    LHServCostGrid.canSel = 1;
	    LHServCostGrid.canChk = 0;
	    LHServCostGrid.selBoxEventFuncName = "showOne";
	*/
	    LHServCostGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //LHServCostGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
		alert(ex);
	}
}
</script>
