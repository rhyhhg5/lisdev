/** 
 * 程序名称：Init.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-11-02 14:02:34
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			      			    
	var strSql = " select '','','', case when a.msgsend = '1' then '　已发送' else '　未发送' end, "
				       +" a.sendtype, a.MakeDate, '', a.MsgNo, a.msgcontent, char(rownumber() over()) "
			    +" from LOMsgInfo a where MsgType = '1' and department = 'HM' "
			    +getWherePart("a.MakeDate","MakeDate")
			    +getWherePart("a.Msgsend","MsgSend")
			    +" order by a.MakeDate desc"; 
	
	var cTurnPage = turnPage;
	  
	//查询前先把LOMsgInfoGrid放入cTurnPage中
	cTurnPage.pageDisplayGrid = LOMsgInfoGrid;  
	   
	//查询SQL，返回结果字符串
	//LargeFlag接口参数处理
		if (typeof(LargeFlag) == "undefined" || LargeFlag == "0" || (typeof(LargeFlag) == "string" && LargeFlag == ""))
		{
			//查询SQL，返回结果字符串
			cTurnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1, 0, cTurnPage);
		}
		else
		{
			//alert(strSql);
			//查询SQL，返回结果字符串
			alert(easyQueryVer3(strSql, 1, 0, 1, 1, cTurnPage));
			cTurnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1, 1, cTurnPage);
		}	

		mCodeType = ""; //把mCodeType清空

		cTurnPage.strQueryResult = cTurnPage.strQueryResult.replace(/\n/g,"");
      
	  //判断是否查询成功
	  if (!cTurnPage.strQueryResult) {  
	    //清空MULTILINE，使用方法见MULTILINE使用说明 
	    LOMsgInfoGrid.clearData();  
	    //alert("没有查询到任何信息！");
	    return false;
	  }
	  
	  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	  cTurnPage.arrDataCacheSet = clearArrayElements(cTurnPage.arrDataCacheSet);
	
	  //查询成功则拆分字符串，返回二维数组
	  cTurnPage.arrDataCacheSet = decodeEasyQueryResult(cTurnPage.strQueryResult, 0, 0, cTurnPage);
	
	  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	  cTurnPage.pageDisplayGrid = LOMsgInfoGrid;   
	
	  LOMsgInfoGrid.SortPage=cTurnPage;  
	
	  //保存SQL语句
	  cTurnPage.strQuerySql = strSql; 
	 
	  //设置查询起始位置
	  cTurnPage.pageIndex       = 0;  
	
	  //在查询结果数组中取出符合页面显示大小设置的数组
	  var arrDataSet = cTurnPage.getData(cTurnPage.arrDataCacheSet, cTurnPage.pageIndex, cTurnPage.pageLineNum);
	
	  //调用MULTILINE对象显示查询结果
	  displayMultiline(arrDataSet, cTurnPage.pageDisplayGrid, cTurnPage);
	
	  //控制是否显示翻页按钮
	  if (cTurnPage.queryAllRecordCount > cTurnPage.pageLineNum) {
	    try { window.document.all(cTurnPage.pageDivName).style.display = "";} catch(ex) { }
	    try
			{
				LOMsgInfoGrid.setPageMark(cTurnPage);	
			}
			catch(ex)
			{}
	
	  } 
	  else {
	    try { window.document.all(cTurnPage.pageDivName).style.display = "none"; } catch(ex) { }
	  }

	//turnPage.queryModal(strSql, LOMsgInfoGrid);
}
function showOne(parm1, parm2) {	
//判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
// }
}
function returnParent()
{
	var arrReturn = new Array();
	var tSel = LOMsgInfoGrid.getSelNo();
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{	
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LOMsgInfoGrid.getSelNo();
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = LOMsgInfoGrid.getRowData(tRow-1);
	return arrSelected;
}

function getCustomerInfo(sql)
{
		var CustomerNo = "";
		var CustomerName = "";
		var MobileNo = "";
		var SeqNo = "";
		
		
		var arr_MsgNo = new Array();
		arr_MsgNo = easyExecSql(sql);//符合条件的短信序号

	  for(var i = parseInt(LOMsgInfoGrid.getRowColData(0,10))-1; i < parseInt(LOMsgInfoGrid.getRowColData(9,10)); i++)
	  {//alert("第"+i+"个短信");
			  var infocount = 1;
			  var sql_customerinfo = " select '"+arr_MsgNo[i]+"', Seqno, Customerno, Customername, Mobileno, Managecom, Operator, Makedate, Maketime, Modifydate, Modifytime"
			  										 + " from LOMsgReceive where MsgNo = '"+arr_MsgNo[i]+"'"
			  										 ;
			  var arr_CustomerInfo = easyExecSql(sql_customerinfo);
				//alert(arr_CustomerInfo.length);
			  for(var j = 0; j < arr_CustomerInfo.length; j++)
			  {//拼字符串
						//alert("第"+j+"个客户");//alert(arr_CustomerInfo);
						if(j != arr_CustomerInfo.length-1)
						{
				  		SeqNo = SeqNo + arr_CustomerInfo[j][infocount] + ",";//alert(arr_CustomerInfo[j][infocount]);
				  		CustomerNo = CustomerNo + arr_CustomerInfo[j][infocount+1] + ",";//alert(CustomerNo + arr_CustomerInfo[j][infocount+1]);
				  		CustomerName = CustomerName + arr_CustomerInfo[j][infocount+2] + ",";//alert(arr_CustomerInfo[j][infocount+2]);
				  		MobileNo = MobileNo + arr_CustomerInfo[j][infocount+3] + ",";//alert(arr_CustomerInfo[j][infocount+3]);
			  		}
			  		else
			  		{
			  			SeqNo = SeqNo + arr_CustomerInfo[j][infocount];
			  			CustomerNo = CustomerNo + arr_CustomerInfo[j][infocount+1];
			  			CustomerName = CustomerName + arr_CustomerInfo[j][infocount+2];
			  			MobileNo = MobileNo + arr_CustomerInfo[j][infocount+3];
			  			
			  		}
			  			
			  		//infocount = j*11;
			  }
 				//插值
			  LOMsgInfoGrid.setRowColData(i,1,CustomerNo);
			  LOMsgInfoGrid.setRowColData(i,2,CustomerName);
			  LOMsgInfoGrid.setRowColData(i,3,MobileNo);         
			  LOMsgInfoGrid.setRowColData(i,9,SeqNo);     
			  CustomerNo = "";  
			  CustomerName = "";
			  MobileNo = "";    
			  SeqNo = "";
		}   
				 
}       

function showCustomer()
{
		initLOMsgInfoReceiveGrid();
		var rowno = LOMsgInfoGrid.getSelNo();
		var sql = " select a.customerno, b.insuredname, a.mobileno, a.grpcontno, a.contno, b.appntname, a.msgno, a.seqno "
				 +" from   lomsgreceive a, lccont b "
				 +" where  a.customerno = b.insuredno "
				 +" and    a.grpcontno = b.grpcontno "
				 +" and    a.contno = b.contno "
				 +" and    a.msgno = '"+LOMsgInfoGrid.getRowColData(rowno-1, 8)+"'"
				 ;

		turnPage1.queryModal(sql, LOMsgInfoReceiveGrid);           
}                   
                    