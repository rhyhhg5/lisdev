<%
//程序名称：LHMessSendPrt.jsp
//程序功能：F1报表生成
//创建日期：2006-09-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
ReportUI
</title>
<head>
		<SCRIPT src="LHHealthTestRptInput.js"></SCRIPT>
</head>
<body>
<%

String flag = "0";
String FlagStr = "";
String Content = "";

//设置模板名称
JRptList t_Rpt = new JRptList();

String FileName ="LHMessageSend";
String ManageCom = request.getParameter("manageComPrt");
String sqlServTaskNo = request.getParameter("ServTaskNo");
String ServTaskNo = "'"+sqlServTaskNo.replaceAll(",","','")+"'";
System.out.println("ServTaskNoServTaskNo "+ServTaskNo);
String tOutFileName = "";

if(flag.equals("0"))
{
	t_Rpt.m_NeedProcess = false;
	t_Rpt.m_Need_Preview = false;
	t_Rpt.mNeedExcel = true;
	

	String CurrentDate = PubFun.getCurrentDate();
	CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");

	t_Rpt.AddVar("ManageCom",ManageCom);
	t_Rpt.AddVar("ManageComName", ReportPubFun.getMngName(ManageCom));
	
	String YYMMDD = "";
	YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
	       + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
	       + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
	t_Rpt.AddVar("CurrentDate", YYMMDD);
	t_Rpt.AddVar("ServTaskNo",ServTaskNo);
	System.out.println("======="+ServTaskNo+"===========");
t_Rpt.Prt_RptList(pageContext,FileName);
tOutFileName = t_Rpt.mOutWebReportURL;
String strVFFileName = FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
String strRealPath = application.getRealPath("/web/Generated").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName;
System.out.println("strVFPathName : "+ strVFPathName);
System.out.println("=======Finshed in JSP===========");
System.out.println(tOutFileName);

response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+"&RealPath="+strVFPathName);
}
%>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
	top.close();
}
</script >
