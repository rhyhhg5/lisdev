<%
//程序名称：LHAddNewHmPlanQuery.jsp
//程序功能：功能描述
//创建日期：2006-07-06 10:36:44
//创建人  ：郭丽颖
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHAddNewHmPlanQuery.js"></SCRIPT> 
  <%@include file="LHAddNewHmPlanQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLHHealthServItemGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      服务事件号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ServCaseCode >
    </TD>
    <TD  class= title>
      服务事件名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ServCaseName >
    </TD>
    <TD  class= title>
		  服务事件类型
    </TD>
		<TD  class= input>
		 <Input class= 'codename' style="width:40px" name=ServEventType value='2'><Input class= 'codeno' style="width:120px" value="集体服务事件" name=ServEventTypeName  CodeData= "0|^1|个人服务事件^2|集体服务事件"   ondblclick="showCodeListEx('(',[this,ServEventType],[1,0],null,null,null,'1',160);" codeClear(ServEventTypeName,ServEventType);">
	  </TD>
  </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查 询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
        <INPUT VALUE="关 联"   TYPE=button   class=cssbutton onclick="AddNewSetting();">		
        <INPUT VALUE="返 回"   TYPE=hidden   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHHealthServItem1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHHealthServItem1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHHealthServItemGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
   <input type=hidden id="fmtransact" name="fmtransact">
   <input type=hidden  id="ServItemNo" name="ServItemNo">
   <input  type=hidden id="ServCaseCode2" name="ServCaseCode2">
   <input  type=hidden id="ServItemCode" name="ServItemCode">
    <input type=hidden id="ServPlanNo" name="ServPlanNo">
    <input type=hidden id="ServPlanName" name="ServPlanName">
    <input type=hidden id="ComID" name="ComID">
    <input type=hidden id="ServPlanLevel" name="ServPlanLevel">
    <input type=hidden id="ContNo" name="ContNo">
    <input type=hidden id="CustomerNo" name="CustomerNo">
    <input type=hidden  id="CustomerName" name="CustomerName">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
