<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServCostSave.jsp
//程序功能：
//创建日期：2006-03-29 10:45:21
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	//接收信息，并作校验处理。
	//输入参数
	LHServCostSet tLHServCostSet   = new LHServCostSet();
	OLHServCostUI tOLHServCostUI   = new OLHServCostUI();
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	
 	 //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
	String tBalanceObjCode[] = request.getParameterValues("LHServCostGrid1");
  	String tBalanceObjName[] = request.getParameterValues("LHServCostGrid2");
  	String tContraNo[] = request.getParameterValues("LHServCostGrid7");
  	String tContraItemNo[] = request.getParameterValues("LHServCostGrid3");
  	String tMakeDate[] = request.getParameterValues("LHServCostGrid5");
  	String tMakeTime[] = request.getParameterValues("LHServCostGrid6");
  	
	int LHServCostCount = 0;
	if(tBalanceObjCode != null)
	{	
		LHServCostCount = tBalanceObjCode.length;
	}
	for(int i = 0; i < LHServCostCount; i++)
	{
		LHServCostSchema tLHServCostSchema = new LHServCostSchema();
	   
	    tLHServCostSchema.setServPriceCode(request.getParameter("ServPriceCode"));
	    tLHServCostSchema.setServPriceName(request.getParameter("ServPriceName"));
	    tLHServCostSchema.setBalanceObjCode(tBalanceObjCode[i]);
	    tLHServCostSchema.setBalanceObjName(tBalanceObjName[i]);
	    tLHServCostSchema.setContraNo(tContraNo[i]);
	    tLHServCostSchema.setContraItemNo(tContraItemNo[i]);
	    tLHServCostSchema.setMakeDate(tMakeDate[i]);
	    tLHServCostSchema.setMakeTime(tMakeTime[i]);
	    
	    tLHServCostSet.add(tLHServCostSchema);
	}
    
	try
	{
	 	 // 准备传输数据 VData
	  	VData tVData = new VData();
		tVData.add(tLHServCostSet);
	  	tVData.add(tG);
	    tOLHServCostUI.submitData(tVData,transact);
  	}
	catch(Exception ex)
	{
	    Content = "保存失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}
  
	//如果在Catch中发现异常，则不从错误类中提取错误信息
  	if (FlagStr=="")
  	{
	    tError = tOLHServCostUI.mErrors;
	    if (!tError.needDealError())
	    {                          
	    	Content = " 保存成功! ";
	    	FlagStr = "Success";
	    }
	    else                                                                           
	    {
	    	Content = " 保存失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
  	}
	//添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
