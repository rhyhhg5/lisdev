<%
//程序名称：LHManaStandProInit.jsp
//程序功能：个人服务计划内容定义(初始化页面)
//创建日期：2006-09-11 11:22:30
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                             
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('StandCode').value = "";
    fm.all('StandContent').value = "";
    fm.all('StandLevel').value = "";
    fm.all('StandLevelName').value = "";
    fm.all('StandUnit').value = "";
    fm.all('QuesStauts').value = "";
    fm.all('QuesStautsName').value = "";
    fm.all('QuerAffiliated').value = "";
    fm.all('QuerAffiliatedName').value = "";
    fm.all('QuerResType').value = "";
    fm.all('QuerResTypeName').value = "";
    fm.all('ResuAffiInfo').value = "";
    fm.all('StandQuerType').value = "";
    fm.all('StandQuerTypeName').value = "";
  }
  catch(ex)
  {
    alert("在LHManaStandProInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                        
function initForm()
{
  try
  {
    initInpBox();
    initLHStaQuOptionGrid();
  }
  catch(re)
  {
    alert("LHManaStandProInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}  

	
function initLHStaQuOptionGrid() 
{                            
	var iArray = new Array();                               
                                                           
	try 
    {                                                   
    	iArray[0]=new Array();                                
    	iArray[0][0]="序号";         		//列名               
    	iArray[0][1]="30px";         		//列名               
    	iArray[0][3]=0;         				//列名                 
    	iArray[0][4]="";         //列名   
    	
    	iArray[1]=new Array(); 
		  iArray[1][0]="唯一标识号";   
		  iArray[1][1]="170px";   
		  iArray[1][2]=20;        
		  iArray[1][3]=1;
    
	    iArray[2]=new Array(); 
		  iArray[2][0]="备选项";   
		  iArray[2][1]="170px";   
		  iArray[2][2]=20;        
		  iArray[2][3]=1;
  
	    iArray[3]=new Array(); 
		  iArray[3][0]="问题附属信息";   
		  iArray[3][1]="170px";   
		  iArray[3][2]=20;        
		  iArray[3][3]=1;
		  
		  iArray[4]=new Array(); 
		  iArray[4][0]="QuOptionNo";   
		  iArray[4][1]="0px";   
		  iArray[4][2]=20;        
		  iArray[4][3]=3;
		  
		  iArray[5]=new Array(); 
		  iArray[5][0]="StandCode";   
		  iArray[5][1]="0px";   
		  iArray[5][2]=20;        
		  iArray[5][3]=3;
     
     
    	LHStaQuOptionGrid = new MulLineEnter( "fm" , "LHStaQuOptionGrid" );                         
    	//这些属性必须在loadMulLine前                                                                                              
    	                                                                                                          
    	LHStaQuOptionGrid.mulLineCount = 0;                                          
    	LHStaQuOptionGrid.displayTitle = 1;                                          
    	LHStaQuOptionGrid.hiddenPlus = 0;                                            
    	LHStaQuOptionGrid.hiddenSubtraction = 0;                                     
    	LHStaQuOptionGrid.canSel = 1;                                                
                                                   
    	LHStaQuOptionGrid.loadMulLine(iArray);                                       
    	//这些操作必须在loadMulLine后面      
                               
  }                                                                          
  catch(ex) {                                                                
    alert(ex);                                                               
  }                                                                          
}                                                                        
      
      
</script>
  