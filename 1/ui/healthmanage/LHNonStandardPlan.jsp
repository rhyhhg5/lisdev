<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHNonStandardPlan.jsp
//程序功能：
//创建日期：2006-11-20 17:04:55
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="LHNonStandardPlan.js"></SCRIPT>
	<%@include file="LHNonStandardPlanInit.jsp"%>
</head>
<body  onload="initForm()" >
	<form  method=post name=fm target="fraSubmit">
		<table  class=common align=center>
			<tr align=right>
				<td style="width:600px;">
				</td>
				<td class=button  align=right>
					<INPUT class=cssButton name="saveButton" style="width:50px;" VALUE="保  存"  TYPE=button onclick="return submitForm();">
				</td>
				<td class=button align=right>
					<INPUT class=cssButton name="modifyButton"  style="width:50px;" VALUE="修  改"  TYPE=button onclick="return updateClick();">
				</td>	
				<td class=button align=right >
					<INPUT class=cssButton VALUE="查 询" style="width:50px;"  TYPE=button onclick="queryClick();">
				</td>
			</tr>
		</table>
	<div style="display:''" id='submenu1'>
		<table>
			<tr>
				<td>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServPlan1);">
    	 		</td>
    	 		<td class= titleImg>
        			健管非标业务基本信息
       			</td>   		 
       
			</tr>
		</table>
    <Div  id= "divLHServPlan1" style= "display: ''">
		<table  class= common align='center' >
			<tr class=common>
				<TD class= title >
		          契约事件类型
		        </TD>   		 
				<TD  class= input> 
				<input class=codeno CodeData="0|3^1|个人保单^2|团体保单^3|非标业务"  name=ServPlayType 
				ondblclick="return showCodeListEx('DealWith',[this,ServPlayTypeName],[0,1],null,null,null,1);" 
				onkeyup="return showCodeListKeyEx('DealWith',[this,ServPlayTypeName],[0,1],null,null,null,1);" verify="契约事件类型|notnull&len=1" ><input class=codename name=ServPlayTypeName>
				</TD>
				<TD  class= title style="width:120">
	            	事件原始编号
	        	</TD>
				<TD  class= input>
		        <Input class= 'common' name=ContNo verify="事件原始编号|notnull&len<=20" ondblclick="getOtherNo()">
				</TD>
				<TD id=aa1 class= title>
					契约事件状态
				</TD>
	        	<TD id=aa2 class= input>
	         	   <Input class= 'codename' style="width:40px" readonly name=CaseState verify="契约事件状态|notnull&len=1"><Input class= 'codeno'  style="width:120px" name=CaseStateName ondblclick="return showCodeList('lhcasestate',[this,CaseState],[1,0],null,null,null,null,160);" >
				</TD>
			</tr>
			<TR  class= common>  
			    <TD  class= title>
             		客户号码
          		</TD>
          		<TD  class= input>
          		  <Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=20" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
          		</TD>
          		<TD  class= title>
          		  客户姓名
          		</TD>
			    <TD  class= input>
					<Input class= 'code' name=CustomerName verify="客户姓名|NOTNULL&len<=300" ondblclick="return queryCustomerNo();" >
			    </TD>
			    <TD  class= title style="width:120" >
	          		契约机构标识
	        	</TD>
	        	<TD  class= input>
	        	    <Input class= 'codename' readonly style="width:40px" name=ComID verify="契约机构标识|notnull"><Input class= 'code' style="width:120px" name=ComID_cn ondblclick="showCodeList('hmhospitalmng', [this, ComID], [0,1], null, null, null, 1,150);">
	        	</TD>
			</TR>
			<TR  class= common>
				<TD  class= title>
        			服务计划代码
       			</TD>
       			<TD  class= input>
  					<Input class= 'code' name=ServPlanCode  verify="服务计划代码|NOTNULL&len<=20" ondblclick="showCodeList('riskcode1',[this,ServPlanName],[0,1],null,fm.ServPlanCode.value,'Riskcode','1',300);" >
       			</TD>
       			<TD  class= title>
       			  服务计划名称
       			</TD>
       			<TD  class= input>
					<Input class= 'code' name=ServPlanName  verify="服务计划名称|NOTNULL&len<=300" ondblclick="showCodeList('riskcode1',[this,ServPlanCode],[1,0],null,fm.ServPlanCode.value,'Riskcode','1',300);" >
       			</TD>
       			<TD class= title>
					服务计划档次
       			</TD>
       			<TD  id=tdServPlanLevelCode class= input>
					<Input class = codeno name= ServPlanLevel style="width:40px" readonly verify="服务计划档次|NOTNULL&len=1" ondblclick="showCodeList('hmservplanlevel',[this,ServPlanLevelName],[1,0],null,fm.ServPlanLevelName.value,'ServPlanLevelName','',160);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservplanlevel',[this,ServPlanLevelName],[1,0],null,fm.ServPlanLevelName.value,'ServPlanLevelName');"   onkeydown="return showCodeListKey('hmservplanlevel',[this,ServPlanLevelName],[1,0],null,fm.ServPlanLevelName.value,'ServPlanLevelName'); codeClear(ServPlanLevelName,ServPlanLevelCode);"><Input class= 'codename' style="width:120px"  name=ServPlanLevelName >
				</TD> 
			</TR> 
			<TR  class= common>
				<TD  class= title>
				  契约起始时间
				</TD>
				<TD  class= input>
				  <Input  name=StartDate verify="契约起始时间|DATE&NOTNULL" class=CoolDatePicker style="width:141px">
				</TD>
				<TD  class= title>
				  契约终止时间
				</TD>
				<TD  class= input>
				  <Input name=EndDate verify="契约终止时间|DATE&NOTNULL" class=CoolDatePicker style="width:141px">
				</TD>				
				<TD  class= title>
					健管服务保费（元）
				</TD>
				<TD  class= input>
					<Input class= 'common' name=ServPrem verify="健管服务保费(元)|NOTNULL">
				</TD>	
			</TR> 
		</table>
    </Div>
	<table>
		<tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServPlanGrid);">
    		</td>
			<td class= titleImg>
				非标业务服务设置列表
			</td>
			<td>
       		 	<input type= hidden class=cssButton name=ServTrace value="个人服务列表" OnClick="toServTrace();">
			</td>   		 
		</tr>
    </table>
    
	<Div id="divLHServPlanGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServPlanGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
    	<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmAction" name="fmAction">
		<!--团体服务计划号码-->
		<Input type=hidden name=GrpServPlanNo >

		<!--个人服务计划号码-->
		<Input type=hidden  name=ServPlanNo >
		<Input type=hidden  name=Name >

		<!--服务计划类型-->
		<Input type=hidden name=GrpContNo >
		<Input type=hidden name=MakeDate >
	    <Input type=hidden name=MakeTime >
	    <Input type=hidden name=HiddenCustom >
	    <Input type=hidden name=prtNo >
    <table>
		<tr>
			<td>
         		<input type= button class=cssButton  style="width:120px" name=QieSuccess value="服务信息设置完成" OnClick="ServSetSucced();">
        	</td> 
       </tr>
    </table>
</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>

</html>
