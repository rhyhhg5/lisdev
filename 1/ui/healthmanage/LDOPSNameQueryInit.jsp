<%
//程序名称：LDOPSNameQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-05-24 14:33:37
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('OPSCommonName').value = "";
    fm.all('ICDOPSCode').value = "";
    fm.all('MainteinFlag').value = "";
  }
  catch(ex) {
    alert("在LDOPSNameQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDOPSNameGrid();  
  }
  catch(re) {
    alert("LDOPSNameQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDOPSNameGrid;
function initLDOPSNameGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名

		iArray[1]=new Array();
    iArray[1][0]="手术俗称";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="手术ICD代码";         		//列名
    iArray[2][1]="60px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="手术标准名称";         		//列名
    iArray[3][1]="100px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();                        
    iArray[4][0]="维护标记";         		//列名
    iArray[4][1]="30px";         		//列名      
    iArray[4][3]=0;         		//列名            

    LDOPSNameGrid = new MulLineEnter( "fm" , "LDOPSNameGrid" ); 
    LDOPSNameGrid.canSel = 1;
    //这些属性必须在loadMulLine前
/*
    LDOPSNameGrid.mulLineCount = 0;   
    LDOPSNameGrid.displayTitle = 1;
    LDOPSNameGrid.hiddenPlus = 1;
    LDOPSNameGrid.hiddenSubtraction = 1;
    LDOPSNameGrid.canSel = 1;
    LDOPSNameGrid.canChk = 0;
    LDOPSNameGrid.selBoxEventFuncName = "showOne";
*/
    LDOPSNameGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDOPSNameGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
