<%
  //程序名称：LHHospitalBasicStat.jsp
  //程序功能：医院就诊情况的统计
  //创建日期：2007-10-9
  //创建人  ：liuli
  //更新人  ：
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
      GlobalInput tGI = new GlobalInput();
      tGI = (GlobalInput)session.getValue("GI");
%>
<script type="">
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <!--<SCRIPT src="../common/javascript/VerifyInput2.js"></SCRIPT>-->
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="HospitalDiagnose.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();">
<table>
  <tr>
    <td class=titleImg>医院就诊情况统计</td>
  </tr>
</table>
<form action="./HospitalDiagnoseSave.jsp" method=post name=fm target=f1print>
<table class= common align='center'>
  <tr class="common">
    <td class=title8>管理机构</td>
    <td>
      <Input class=codeNo  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName elementtype=nacessary>
    </td>
    <TD class=title8></TD>
    <TD>
    </TD>
  </tr>
  <tr>
  	<TD  class= title8>
     统计时间
    </TD>
  	<TD  class= input8>
      <Input class= 'coolDatePicker'  dateFormat="Short" name=MakeDate1 value="" verify="入机时间|DATE">&nbsp;&nbsp;至
   </TD>

<TD class=input8>
      <Input class= 'coolDatePicker'  dateFormat="Short" name=MakeDate2 value="" verify="入机时间|DATE">
</TD>
</tr>
<tr>
	<td><br></td>
</tr>
  <tr>
  	<TD class=title8>
  			<Input value=" 分机构推荐医院" style="width:130px"  type=button class=cssbutton onclick="fenHospdiag1();">&nbsp;&nbsp;
  	 	</TD>
  	<TD class=title8>
  			<Input value="不同级别推荐医院" style="width:130px"  type=button class=cssbutton onclick="fenHospdiag2();">
    </TD>
  </tr>
  <tr>
  	<TD class=title8>
  			<Input value="全系统推荐医院" style="width:130px"  type=button class=cssbutton onclick="aLLHospdiag1();">&nbsp;&nbsp;
  	 	</TD>
  	<TD class=title8>
  			<Input value="全系统指定医院" style="width:130px"  type=button class=cssbutton onclick="aLLHospdiag2();">
    </TD>
  </tr>
  <input name ="statflag" type="hidden">
  <input type=hidden id="fmAction" name="fmAction">
</table>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
