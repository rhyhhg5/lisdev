<%
//程序名称：LDDrugstoreQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-15 11:16:51
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('DrugStoreCode').value = "";
//    fm.all('SuperDrugStoreCode').value = "";
    fm.all('DrugstoreName').value = "";
    fm.all('address').value = "";

  }
  catch(ex) {
    alert("在LDDrugstoreQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDDrugstoreGrid();  
  }
  catch(re) {
    alert("LDDrugstoreQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDDrugstoreGrid;
function initLDDrugstoreGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="药店代码";   
		iArray[1][1]="50px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;
		
		iArray[2]=new Array();  
		iArray[2][0]="药店名称";
		iArray[2][1]="200px";    
		iArray[2][2]=20;         
		iArray[2][3]=0;       

		iArray[3]=new Array();  
		iArray[3][0]="地址";
		iArray[3][1]="150px";    
		iArray[3][2]=200;         
		iArray[3][3]=0;

		iArray[4]=new Array();  
		iArray[4][0]="电话";
		iArray[4][1]="80px";    
		iArray[4][2]=2;         
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="所属地区";  
		iArray[5][1]="0px";  
		iArray[5][2]=2;       
    iArray[5][3]=3;     
    
    iArray[6]=new Array();
    iArray[6][0]="邮编";  
    iArray[6][1]="0px";  
    iArray[6][2]=2;       
    iArray[6][3]=3;   
    
    iArray[7]=new Array();
    iArray[7][0]="MakeDate";  
    iArray[7][1]="0px";  
    iArray[7][2]=2;       
    iArray[7][3]=3;
    
    iArray[8]=new Array();
    iArray[8][0]="MakeTime";  
    iArray[8][1]="0px";  
    iArray[8][2]=2;       
    iArray[8][3]=3;      
    
    LDDrugstoreGrid = new MulLineEnter( "fm" , "LDDrugstoreGrid" ); 
    //这些属性必须在loadMulLine前
    
    LDDrugstoreGrid.mulLineCount = 0;   
    LDDrugstoreGrid.displayTitle = 1;
    LDDrugstoreGrid.hiddenPlus = 1;
    LDDrugstoreGrid.hiddenSubtraction = 1;
    LDDrugstoreGrid.canSel = 1;
    LDDrugstoreGrid.canChk = 0;
   // LDDrugstoreGrid.selBoxEventFuncName = "showOne";

    LDDrugstoreGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDrugstoreGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
