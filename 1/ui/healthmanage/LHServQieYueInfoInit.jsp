<%
//程序名称：LHServQieYueInfoInit.jsp
//程序功能：
//创建日期：2006-07-12 17:29:56
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     String ServCaseCode = request.getParameter("ServCaseCode");
     String flag=request.getParameter("flag");
%>                          
<script language="JavaScript">
function initInpBox()
{ 
	try
	{                                      
    	fm.all('flag').value = "<%=flag%>";
	}
	catch(ex)
	{
		alert("在LHSettingSlipQueryInit.jsp->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}

function initForm()
{
	try
	{
		initInpBox();
	    initLHSettingSlipQueryGrid() ;
	    initLHQueryDetailGrid();
	    
	    if(fm.all('flag').value=="1")
	    {//从事件查询入口进入，标记flag为'1'
			fm.all('ServCaseCode').value = "<%=ServCaseCode%>";
	    }
	    var sqlCode="select h.ServCaseState from LHServCaseDef h where h.ServCaseCode='"+ fm.all('ServCaseCode').value +"'";
		var ServCaseState=easyExecSql(sqlCode);
	  
		if(ServCaseState=="0"||ServCaseState==""||ServCaseState=="null"||ServCaseState==null)//为空时是从实施管理进入无服务事件状态时
		{
			 fm.all('CaseShiShi').disabled=false;
			 fm.all('AddItemAll').disabled=false; 
		}
		else
		{//服务事件已启动
			 fm.all('CaseShiShi').disabled=true;  
			 fm.all('AddItemAll').disabled=true; 
			 fm.all('ServCaseStutesName').ondblclick = "";
			 fm.all('ServCaseStutes').readOnly = true;
		}
		if(fm.all('flag').value=="1")
		{//从事件查询入口进入，事件信息自动带出，不需新建或查询
			fm.all('AddNewServEvent').disabled=true;
			fm.all('QueryServEvent').disabled=true;
			fm.all('ServCaseTypeName').ondblclick = "";
			fm.all('ServCaseStateName').ondblclick = "";
		}
		if(fm.all('flag').value=="1")//从服务事件查询功能进入
		{ //填写“服务事件信息”
			var strSql=" select (case a.ServCaseType when '1' then '个人服务事件' when '2' then '集体服务事件' when '3' then '费用结算事件' else '无' end ),"
				  +"  a.ServCaseCode ,a.ServCaseName, a.ServCaseType , "
	              +" (case a.ServCaseState when '0' then '未进行启动设置' when '1' then '服务事件已启动' when '2' then '服务事件已完成'  when '3' then '服务事件失败' else '无' end ) ,"
	              +" a.ServCaseState "
	              +" from LHServCaseDef a "
                  +" where  a.ServCaseCode= '"+fm.all('ServCaseCode').value+"'"
                   ;
			var Result=new Array;
			Result=easyExecSql(strSql);
			if(Result!="" && Result!="null" && Result!=null)
			{
	        	fm.all('ServCaseTypeName').value=Result[0][0];
	        	fm.all('ServCaseCode').value=Result[0][1];
	        	fm.all('ServCaseName').value=Result[0][2];
	        	fm.all('ServCaseType').value=Result[0][3];
	        	fm.all('ServCaseStateName').value=Result[0][4];
	        	fm.all('ServCaseState').value=Result[0][5];
			}
    	}
		if(fm.all('flag').value=="2")//从服务事件实施管理进入
		{
			  fm.all('tServCaseCode').value = "<%=ServCaseCode%>";
			  fm.all('tServCaseName').value = easyExecSql("select a.ServCaseName from LHServCaseDef  a where   a.ServCaseCode='"+fm.all('tServCaseCode').value+"'");
			  fm.all('AddNewServEvent').disabled=false;
			  fm.all('QueryServEvent').disabled=false;
	     	fm.all('ServCaseCode').value="";
	     	fm.all('ServCaseName').value="";
	     	
		}
	}
	catch(re)
	{
		alert("LHSettingSlipQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误AA!");
	}
}

var LHSettingSlipQueryGrid;
function initLHSettingSlipQueryGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="40px";         		//列名
	    iArray[0][3]=1;         		//列名
	    iArray[0][4]="station";         		//列名
	    
	    iArray[1]=new Array(); 
	 	iArray[1][0]="客户号";   
	 	iArray[1][1]="55px";   
	 	iArray[1][2]=20;        
	 	iArray[1][3]=0;
	 	
	 	iArray[2]=new Array(); 
	 	iArray[2][0]="客户姓名";   
	 	iArray[2][1]="55px";   
	 	iArray[2][2]=20;        
	 	iArray[2][3]=0;
	 	
	 	iArray[3]=new Array(); 
	 	iArray[3][0]="保单号";   
	 	iArray[3][1]="60px";   
	 	iArray[3][2]=20;        
	 	iArray[3][3]=1;
     	
	 	
	 	iArray[4]=new Array(); 
	 	iArray[4][0]="服务项目代码";   
	 	iArray[4][1]="0px";   
	 	iArray[4][2]=20;        
	 	iArray[4][3]=3;
     	
	 	iArray[5]=new Array(); 
	 	iArray[5][0]="服务项目名称";   
	 	iArray[5][1]="110px";   
	 	iArray[5][2]=20;        
	 	iArray[5][3]=0;
	 	
	 	iArray[6]=new Array(); 
	 	iArray[6][0]="序号";   
	 	iArray[6][1]="50px";   
	 	iArray[6][2]=20;        
	 	iArray[6][3]=0;
   
    	iArray[7]=new Array(); 
	 	iArray[7][0]="服务关联状态";   
	 	iArray[7][1]="70px";   
	 	iArray[7][2]=20;        
	 	iArray[7][3]=0;


    	iArray[8]=new Array(); 
    	iArray[8][0]="服务机构";                             
    	iArray[8][1]="50px";                     
    	iArray[8][2]=20;                                  
    	iArray[8][3]=0;      
    	//iArray[8][4]="hmhospitalmng";
    	//iArray[8][5]="8|9";     //引用代码对应第几列，'|'为分割符
    	//iArray[8][6]="1|0";     //上面的列中放置引用代码中第几位值
    	//iArray[8][14]="个人服务事件";
    	//iArray[8][17]="1"; 
    	//iArray[8][18]="160";
    	//iArray[8][19]="1" ;
    
    	iArray[9]=new Array();        
    	iArray[9][0]="hmhospitalmng";     
    	iArray[9][1]="0px";           
    	iArray[9][2]=20;              
    	iArray[9][3]=3;               
    	iArray[9][14]="1";            

    	iArray[10]=new Array();                           
    	iArray[10][0]="定价方式代码";                             
    	iArray[10][1]="0px";                             
    	iArray[10][2]=20;                                  
    	iArray[10][3]=3;  
    	iArray[10][4]="lhserveritemcode";
    	
    	
    	iArray[11]=new Array(); 
		  iArray[11][0]="定价方式名称";   
		  iArray[11][1]="0px";   
		  iArray[11][2]=20;        
		  iArray[11][3]=3;   
    	iArray[11][4]="lhserveritemcode";
 
     	iArray[12]=new Array();        
     	iArray[12][0]="服务项目号码";     
     	iArray[12][1]="100px";           
     	iArray[12][2]=20;              
     	iArray[12][3]=0;               
     	iArray[12][14]="1";  
     	
     	iArray[13]=new Array();        
     	iArray[13][0]="ServPlanNo";     
     	iArray[13][1]="0px";           
     	iArray[13][2]=20;              
     	iArray[13][3]=3;               
     	iArray[13][14]="1";  
     	
     	iArray[14]=new Array();        
     	iArray[14][0]="服务计划起始日期";     
     	iArray[14][1]="90px";           
     	iArray[14][2]=20;              
     	iArray[14][3]=0;               
     	
     	iArray[15]=new Array();        
     	iArray[15][0]="服务计划终止日期";     
     	iArray[15][1]="90px";           
     	iArray[15][2]=20;              
     	iArray[15][3]=0;               
                                  
    	LHSettingSlipQueryGrid = new MulLineEnter( "fm" , "LHSettingSlipQueryGrid" );           
    	//这些属性必须在loadMulLine前                          
    	                      
    	LHSettingSlipQueryGrid.mulLineCount = 0;                             
    	LHSettingSlipQueryGrid.displayTitle = 1;                          
    	LHSettingSlipQueryGrid.hiddenPlus = 1;                          
    	LHSettingSlipQueryGrid.hiddenSubtraction = 1;                          
    	LHSettingSlipQueryGrid.canSel = 0;                          
    	LHSettingSlipQueryGrid.canChk = 1;                          
    	LHSettingSlipQueryGrid.selBoxEventFuncName = "showTwo";                       
    	                                                         
    	LHSettingSlipQueryGrid.loadMulLine(iArray);                                     
    	//这些操作必须在loadMulLine后面                                         
    	//LHSettingSlipQueryGrid.setRowColData(1,1,"asdf");                                   
	}
	catch(ex) 
	{alert(ex);}
}                 

var LHQueryDetailGrid;
function initLHQueryDetailGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         		//列名
    	iArray[0][1]="40px";         		//列名
    	iArray[0][3]=1;         		//列名
    	iArray[0][4]="station";         		//列名
    	
    	iArray[1]=new Array(); 
		iArray[1][0]="客户号";   
		iArray[1][1]="150px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;
		
		iArray[2]=new Array(); 
		iArray[2][0]="客户姓名";   
		iArray[2][1]="150px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="服务计划名称";   
		iArray[3][1]="150px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="服务计划档次";   
		iArray[4][1]="150px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
	                               
    	LHQueryDetailGrid = new MulLineEnter( "fm" , "LHQueryDetailGrid" );           
    	//这些属性必须在loadMulLine前                          
    	                      
    	LHQueryDetailGrid.mulLineCount = 0;                             
    	LHQueryDetailGrid.displayTitle = 1;                          
    	LHQueryDetailGrid.hiddenPlus = 1;                          
    	LHQueryDetailGrid.hiddenSubtraction = 1;                          
    	LHQueryDetailGrid.canSel = 1;                          
    	LHQueryDetailGrid.canChk = 0;                          
    	                
    	                                                         
    	LHQueryDetailGrid.loadMulLine(iArray);                                     
    	//这些操作必须在loadMulLine后面                                         
    	//LHQueryDetailGrid.setRowColData(1,1,"asdf");                                   
	}
	catch(ex) {alert(ex);}
}
 
</script>
