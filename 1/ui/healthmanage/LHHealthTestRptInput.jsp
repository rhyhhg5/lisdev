<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LHHealthTestRptSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LHHealthTestRptInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body  onload="">    
  <form action="./LHHealthTestRpt.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input>
          	<input class="common" style="width:35%"  name=ManageCom  verify="管理机构|notnull&code:comcode&INT" ><Input class="code" style="width:42%" name=ManageComName elementtype=nacessary ondblclick="return showCodeList('comcode',[this,ManageCom],[1,0],null,null,null,1);" onkeydown="return showCodeListKey('comcode',[this,ManageCom],[1,0],null,null,null,1);">        
         	</TD>
          <TD  class= title>
              医疗机构名称
          </TD>
          <TD  class= input8>
              <Input class= 'code8' name=HospitName ondblclick="return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName'); " onkeydown="  return showCodeListKey('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName'); " verify="医疗机构名称|len<=60">
          </TD>
          <TD class=title>医疗机构代码</TD>
          <TD class=input><Input  class=common name=HospitCode readonly verify="医疗机构代码|notnull"></TD>          
        </TR>
        <TR  class= common>          
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        	<TD class=title></TD>
          <TD class=input></TD> 
        </TR>
        
    </table>

    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		<INPUT VALUE="下  载" class="cssButton" TYPE="hidden" onclick="download()"> 	
		<Input type=hidden name=PersonAmount>
		<Input type=hidden name=Fee>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 