<%
//程序名称：LOMsgReceiveGrpSetInputInit.jsp
//程序功能：功能描述
//创建日期：2005-12-27 11:29:02
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try 
  {     

  }
  catch(ex) 
  {
    alert("LOMsgReceiveGrpSetInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initLOMsgReceiveTempGrid();  
    initLOMsgEachGrpGrid();
  }
  catch(re) {
    alert("LOMsgReceiveGrpSetInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LOMsgReceiveTempGrid;
function initLOMsgReceiveTempGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    
    iArray[1]=new Array();                    
		iArray[1][0]="客户号码";			//列名    
		iArray[1][1]="60px";         		//列名    
		iArray[1][3]=0;         				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="客户姓名";			//列名    
		iArray[2][1]="70px";						//列名    
		iArray[2][3]=0;									//列名        
	
		iArray[3]=new Array();  
		iArray[3][0]="性别";
		iArray[3][1]="30px";   
		iArray[3][3]=0;         
		
		iArray[4]=new Array();  
		iArray[4][0]="生日";
		iArray[4][1]="70px";   
		iArray[4][3]=0;         
		
		iArray[5]=new Array();  
		iArray[5][0]="单位名称";
		iArray[5][1]="300px";   
		iArray[5][3]=0;         
		     
		iArray[6]=new Array();  
		iArray[6][0]="团体保单号";
		iArray[6][1]="80px";   
		iArray[6][3]=0;  
		
		iArray[7]=new Array();  
		iArray[7][0]="保单号";
		iArray[7][1]="80px";   
		iArray[7][3]=0;     
		
		iArray[8]=new Array();  
		iArray[8][0]="保单生效日";
		iArray[8][1]="60px";   
		iArray[8][3]=0;   
		
		iArray[9]=new Array();  
		iArray[9][0]="手机号码";
		iArray[9][1]="60px";   
		iArray[9][3]=0;  
		     
    LOMsgReceiveTempGrid = new MulLineEnter( "fm" , "LOMsgReceiveTempGrid" ); 
    //这些属性必须在loadMulLine前           
    
		LOMsgReceiveTempGrid.mulLineCount = 0;
    LOMsgReceiveTempGrid.canChk = 1;		
		LOMsgReceiveTempGrid.canSel = 0;
		LOMsgReceiveTempGrid.hiddenPlus = 1;
		LOMsgReceiveTempGrid.hiddenSubtraction = 1;
/*
    LOMsgReceiveTempGrid.displayTitle = 1;
    LOMsgReceiveTempGrid.canChk = 0;
    LOMsgReceiveTempGrid.selBoxEventFuncName = "showOne";
*/
    LOMsgReceiveTempGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LOMsgReceiveTempGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

var LOMsgEachGrpGrid;
function initLOMsgEachGrpGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    
    iArray[1]=new Array();                    
		iArray[1][0]="客户号码";			//列名    
		iArray[1][1]="60px";         		//列名    
		iArray[1][3]=0;         				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="客户姓名";			//列名    
		iArray[2][1]="70px";						//列名    
		iArray[2][3]=0;									//列名        
	
		iArray[3]=new Array();  
		iArray[3][0]="性别";
		iArray[3][1]="30px";   
		iArray[3][3]=0;         
		
		iArray[4]=new Array();  
		iArray[4][0]="生日";
		iArray[4][1]="70px";   
		iArray[4][3]=0;         
		
		iArray[5]=new Array();  
		iArray[5][0]="投保人名称";
		iArray[5][1]="300px";   
		iArray[5][3]=0;         
		     
		iArray[6]=new Array();  
		iArray[6][0]="团体保单号";
		iArray[6][1]="80px";   
		iArray[6][3]=0;  
		
		iArray[7]=new Array();  
		iArray[7][0]="保单号";
		iArray[7][1]="80px";   
		iArray[7][3]=0;     
		
		iArray[8]=new Array();  
		iArray[8][0]="保单生效日";
		iArray[8][1]="60px";   
		iArray[8][3]=0;  
		
		iArray[9]=new Array();  
		iArray[9][0]="手机号码";
		iArray[9][1]="70px";   
		iArray[9][3]=0; 
	
		     
    LOMsgEachGrpGrid = new MulLineEnter( "fm" , "LOMsgEachGrpGrid" ); 
    //这些属性必须在loadMulLine前           
    
		LOMsgEachGrpGrid.mulLineCount = 0;
		LOMsgEachGrpGrid.canSel = 0;
		LOMsgEachGrpGrid.hiddenPlus = 1;
		LOMsgEachGrpGrid.hiddenSubtraction = 0;
		LOMsgEachGrpGrid.canChk = 1;
/*
    LOMsgEachGrpGrid.displayTitle = 1;
    LOMsgEachGrpGrid.canChk = 0;
    LOMsgEachGrpGrid.selBoxEventFuncName = "showOne";
*/
    LOMsgEachGrpGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LOMsgEachGrpGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>