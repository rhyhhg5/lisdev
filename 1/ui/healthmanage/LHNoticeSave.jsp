<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLNoticeSave.jsp
//程序功能：
//创建日期：2005-01-12 16:10:51
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLNoticeSchema tLLNoticeSchema   = new LLNoticeSchema();
  OLLNoticeUI tOLLNoticeUI   = new OLLNoticeUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    String Path = application.getRealPath("config//Conversion.config");	
    tLLNoticeSchema.setNoticeNo(request.getParameter("NoticeNo"));
    tLLNoticeSchema.setLogNo(request.getParameter("LogNo"));
    //tLLNoticeSchema.setCustomerNo("8611111111111");
    
    tLLNoticeSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLLNoticeSchema.setCustomerName(request.getParameter("CustomerName"));
    tLLNoticeSchema.setCustomerType(request.getParameter("CustomerType"));
    tLLNoticeSchema.setNSubject(StrTool.Conversion(request.getParameter("NSubject"),Path));
    tLLNoticeSchema.setNContent(StrTool.Conversion(request.getParameter("NContent"),Path));
    tLLNoticeSchema.setAccCode(request.getParameter("AccCode"));
    tLLNoticeSchema.setAccDesc(request.getParameter("AccDesc"));
    
    tLLNoticeSchema.setHospitalCode(request.getParameter("HospitalCode"));
    tLLNoticeSchema.setHospitalName(request.getParameter("HospitalName"));
    
    tLLNoticeSchema.setInHospitalDate(request.getParameter("InHospitalDate"));
    tLLNoticeSchema.setOutHospitalDate(request.getParameter("OutHospitalDate"));
    
    
   
   
    //tLLNoticeSchema.setReplyState(request.getParameter("ReplyState"));
    //tLLNoticeSchema.setAskGrade(request.getParameter("AskGrade"));
    //tLLConsultSchema.setExpRDate(request.getParameter("ExpRDate"));
    tLLNoticeSchema.setDiseaseCode(request.getParameter("DiseaseCode"));
    tLLNoticeSchema.setDiseaseDesc(request.getParameter("DiseaseDesc"));
    
    tLLNoticeSchema.setCustStatus(request.getParameter("CustStatus"));
    
    
    //tLLNoticeSchema.setAvaiFlag("1");
  //  tLLConsultSchema.setAvaliReason(request.getParameter("AvaliReason"));
    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLLNoticeSchema);
  	tVData.add(tG);
  	System.out.println(transact );
    tOLLNoticeUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLLNoticeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 通知保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  String NoticeNo =tLLNoticeSchema.getNoticeNo();
  LLNoticeSchema rLLNoticeSchema = null;
  System.out.println("------Notice----"+FlagStr+Content);
  if ( FlagStr.equals("Success") && "INSERT||MAIN".equals(transact))
  {
  
    VData res = tOLLNoticeUI.getResult();
    rLLNoticeSchema=(LLNoticeSchema)res.getObjectByObjectName("LLNoticeSchema", 0);
    if ( rLLNoticeSchema!=null )
    {
    	NoticeNo= rLLNoticeSchema.getNoticeNo();
    }
  }
  System.out.println(NoticeNo);
  //添加各种预处理
%>                      

<html>
<script language="javascript">
  
  parent.fraInterface.fm.NoticeNo.value = "<%=NoticeNo%>" ;
	alert(parent.fraInterface.fm.NoticeNo.value);
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	alert(222);
	parent.fraInterface.updateCustomer("<%=NoticeNo%>");
</script>
</html>
