<%
//程序名称：LHGrpServExeTraceInputinit.jsp
//程序功能：
//创建日期：2006-03-09 16:05:14
//创建人  ：林仁令
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('GrpServPlanNo').value = "";
    fm.all('GrpServItemNo').value = "";
    fm.all('GrpContNo').value = "";

  }
  catch(ex)
  {
    alert("在LHGrpServExeTraceInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {}
  catch(ex)
  {
    alert("在LHGrpServExeTraceInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initLHServItemGrid();
  }
  catch(re)
  {
    alert("LHGrpServExeTraceInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHServItemGrid;
function initLHServItemGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="标准服务项目代码";   
	  iArray[1][1]="60px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="标准服务项目名称";   
	  iArray[2][1]="120px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="服务项目序号";   
	  iArray[3][1]="70px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=0;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="服务执行状态";   
	  iArray[4][1]="50px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=2;
	  iArray[4][4]="lhservplanexestate";
    iArray[4][5]="4";     //引用代码对应第几列，'|'为分割符
    iArray[4][6]="0";     //上面的列中放置引用代码中第几位值
    iArray[4][9]="服务执行状态|len<=10";
	  	  	  
	  
	  iArray[5]=new Array(); 
	  iArray[5][0]="服务执行时间";   
	  iArray[5][1]="50px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=1;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="服务详细描述";   
	  iArray[6][1]="50px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=1;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="入机日期";   
	  iArray[7][1]="0px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=3;
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="入机时间";   
	  iArray[8][1]="0px";   
	  iArray[8][2]=20;        
	  iArray[8][3]=3;
	  
	  iArray[9]=new Array(); 
	  iArray[9][0]="团体服务计划号码";   
	  iArray[9][1]="0px";   
	  iArray[9][2]=20;        
	  iArray[9][3]=3;
	  
	  iArray[10]=new Array(); 
	  iArray[10][0]="团体服务项目号码";   
	  iArray[10][1]="0px";   
	  iArray[10][2]=20;        
	  iArray[10][3]=3;
    
    LHServItemGrid = new MulLineEnter( "fm" , "LHServItemGrid" ); 
    //这些属性必须在loadMulLine前

    LHServItemGrid.mulLineCount = 0;   
    LHServItemGrid.displayTitle = 1;
    LHServItemGrid.hiddenPlus = 1;
    LHServItemGrid.hiddenSubtraction = 1;
    LHServItemGrid.canSel = 1;
    LHServItemGrid.canChk = 0;
    //LHServItemGrid.selBoxEventFuncName = "showOne";

    LHServItemGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHServItemGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
