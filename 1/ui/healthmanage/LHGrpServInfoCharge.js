//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var flag;
var GrpServItemNo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
function SearchCustom()
{
	var strState = "";
	if(fm.all('flag').value == "1")
	{
		strState = " and b.GrpServItemNo ='"+fm.all('GrpServItemNo').value+"'";	
	}
	else if(fm.all('flag').value == "0")
	{
		strState = "";	
	}
	var sql;
	if(fm.all('ServCaseStutes').value=="1")//未和事件关联表建立关联
	{
		fm.all('AddToServ').disabled=false;
	
		 sql = "select a.CustomerNo,a.Name,b.GrpContNo,b.ServItemCode,"
			         +"(select ServItemName from LHHealthServItem c where c.ServItemCode = b.ServItemCode),"
			         +"b.ServItemSerial,0,b.ComID,b.ServPriceCode,"
			         +"(select ServPriceName from LHServerItemPrice d where d.ServPriceCode = b.ServPriceCode),a.ServItemNo "            
			         +"from LHServItem a,LHGrpServItem b where a.GrpServItemNo = b.GrpServItemNo"
			         +strState
			         +getWherePart("a.CustomerNo","CustomerNo")
			         +getWherePart("a.Name","CustomerName")
			         +getWherePart("b.GrpContNo","GrpContNo")
			         +getWherePart("b.ServItemCode","ServItemCode")
			         + " and a.ServItemNo not in ( select distinct m.ServItemNo from LHServCaseRela m)"
			         ;
			         //alert(sql);
	}
	if(fm.all('ServCaseStutes').value=="2")//和事件关联表建立关联
	{
		fm.all('AddToServ').disabled=true;
	
		 sql = "select a.CustomerNo,a.Name,b.GrpContNo,b.ServItemCode,"
			         +"(select ServItemName from LHHealthServItem c where c.ServItemCode = b.ServItemCode),"
			         +"b.ServItemSerial,0,b.ComID,b.ServPriceCode,"
			         +"(select ServPriceName from LHServerItemPrice d where d.ServPriceCode = b.ServPriceCode),a.ServItemNo "            
			         +"from LHServItem a,LHGrpServItem b where a.GrpServItemNo = b.GrpServItemNo"
			         +strState
			         +getWherePart("a.CustomerNo","CustomerNo")
			         +getWherePart("a.Name","CustomerName")
			         +getWherePart("b.GrpContNo","GrpContNo")
			         +getWherePart("b.ServItemCode","ServItemCode")
			         + " and a.ServItemNo  in ( select distinct m.ServItemNo from LHServCaseRela m)"
			         ;
			         //alert(sql);
	}
	turnPage.queryModal(sql,LHCustomInfoGrid); 
}
function AddToServCase()
{
	if(fm.all('ServeEventNum').value == "" || fm.all('ServeEventName').value == "")
	{
			alert("请填写完整的 ‘服务事件号码’ 和 ‘服务事件名称’");
			return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
	fm.action = "./LHGrpChargeInfoSave.jsp";
	fm.all('fmtransact').value = "INSERT||MAIN";
	fm.submit(); //提交
}

function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
    
    var strState = "";
	if(fm.all('flag').value == "1")
	{
		strState = " and b.GrpServItemNo ='"+fm.all('GrpServItemNo').value+"'";	
	}
	else if(fm.all('flag').value == "0")
	{
		strState = "";	
	}
//	alert();
	var sql = "select a.CustomerNo,a.Name,b.GrpContNo,b.ServItemCode,"
			         +"(select ServItemName from LHHealthServItem c where c.ServItemCode = b.ServItemCode),"
			         +"b.ServItemSerial,0,b.ComID,b.ServPriceCode,"
			         +"(select ServPriceName from LHServerItemPrice d where d.ServPriceCode = b.ServPriceCode),a.ServItemNo "            
			         +"from LHServItem a,LHGrpServItem b where a.GrpServItemNo = b.GrpServItemNo"
			         +strState
			         +getWherePart("a.CustomerNo","CustomerNo")
			         +getWherePart("b.GrpContNo","GrpContNo")
			         +getWherePart("b.ServItemCode","ServItemCode")
			         + " and a.ServItemNo not in ( select distinct m.ServItemNo from LHServCaseRela m)"
			         ;
			         //alert(sql);
			turnPage.queryModal(sql,LHCustomInfoGrid); 
  }
}
function AddServCase()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
	fm.action = "./LHAddServEventSave.jsp";
  	fm.all('fmtransact').value = "INSERT||MAIN";
  	fm.submit(); //提交
}
function QueryServCase()
{
	window.open("./LHSettingQueryMain.jsp?Come=LHGrpServInfoCharge","","width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes");
}
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	//alert();
	if( arrQueryResult != null )
	{
		var sql="select ServCaseType,ServCaseCode,ServCaseName,ServCaseState from LHServCaseDef where ServCaseCode='"+arrQueryResult[0][0]+"'";
 				arrResult = easyExecSql(sql); 
 				fm.all('ServeEventStyle').value = arrResult[0][0];
 				if (arrResult[0][0]==1)
 				   fm.all('ServeEventStyle_ch').value = "个人服务事件";
 				   else 
	 				   fm.all('ServeEventStyle_ch').value = "集体服务事件";
        fm.all('ServeEventNum').value = arrResult[0][1];
        fm.all('ServeEventName').value = arrResult[0][2];
        fm.all('ServCaseState').value = arrResult[0][3];
        
 				
	}
}
		