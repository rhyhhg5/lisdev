var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
    fm.fmtransact.value="INSERT||MAIN";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    //fm.action="./LHExecFeeBalanceSave.jsp";
    fm.action="./LHExecFeeBalanceSave.jsp";
    fm.target="fraSubmit";
    fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	initForm();
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCommTrans.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
          
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}                                          
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */              
function updateClick()
{
	var rowNum=LHExecFeeBalanceGrid.mulLineCount ; //行数 	
	var arrChkedRow = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHExecFeeBalanceGrid.getChkNo(row); 
		if(tChk == true)
		{
			arrChkedRow[xx++] = row;
		}
	}
	if(arrChkedRow.length=="0"||arrChkedRow.length==""||arrChkedRow.length=="null"||arrChkedRow.length==null)
	{
		alert("请选择要修改的信息记录!");
		return false;
	}	
	if(arrChkedRow.length>="1")
	{
    	if (confirm("您确实想修改该记录吗?"))
    	{
        	var i = 0;
        	var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        	
        	fm.fmtransact.value = "UPDATE||MAIN";
        	fm.action="./LHExecFeeBalanceSave.jsp";
        	fm.target="fraSubmit";
        	fm.submit(); //提交
        	initForm();
		}
		else
		{
			alert("您取消了修改操作！");
		} 
	}
}

function showOne()
{
  var rowNum=LHExecFeeBalanceGrid. mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHExecFeeBalanceGrid.getChkNo(row); 
		if(tChk == true)
		{
			aa[xx++] = row;	
	  }  
	}
	if(aa.length=="1")
	{
		 //alert("LHExecFeeBalanceGrid");
		 var HospitCode =LHExecFeeBalanceGrid.getRowColData(aa,15); 
		 fm.all('HospitCode').value=HospitCode;
		 if(fm.all('HospitCode').value!=""&&fm.all('HospitCode').value!="null"&&fm.all('HospitCode').value!=null)
		 {
		    fm.all('HospitName').value=easyExecSql("select  HospitName from LDHospital where LDHospital.HospitCode='"+ fm.all('HospitCode').value +"'");
		 }
		 var ContraNo =LHExecFeeBalanceGrid.getRowColData(aa,14); 
		 fm.all('ContraNo').value=ContraNo;
		 if(fm.all('ContraNo').value!=""&&fm.all('ContraNo').value!="null"&&fm.all('ContraNo').value!=null)
		 {
		    fm.all('ContraName').value=easyExecSql("select  a.ContraName from LHGroupCont  a where a.ContraNo='"+ fm.all('ContraNo').value +"'");
		 }
		 var ContraItemNo =LHExecFeeBalanceGrid.getRowColData(aa,12); 
		 //alert(ContraItemNo);
		 fm.all('DutyItemCode').value=ContraItemNo;
		 if(fm.all('DutyItemCode').value!=""&&fm.all('DutyItemCode').value!="null"&&fm.all('DutyItemCode').value!=null)
		 {
		    fm.all('DutyItemName').value=easyExecSql("select  a.DutyItemName from LDContraItemDuty  a where a.DutyItemCode='"+ fm.all('DutyItemCode').value +"'");
		 }
		  var TaskExecNo =LHExecFeeBalanceGrid.getRowColData(aa,16); 
      fm.all('TaskExecNo').value=TaskExecNo;
      //alert(fm.all('TaskExecNo').value);
      if(fm.all('TaskExecNo').value!=""&&fm.all('TaskExecNo').value!="null"&&fm.all('TaskExecNo').value!=null)
		 {
		    fm.all('ContraItemNo').value=easyExecSql("select  a.ServPlanNo from LHExecFeeBalance  a where a.TaskExecNo='"+ fm.all('TaskExecNo').value +"'");
		 }

	}
	if(aa.length>"1")
	{
		 fm.all('HospitCode').value="";
		 fm.all('ContraNo').value="";
		 fm.all('DutyItemCode').value="";
		 fm.all('HospitName').value="";
		 fm.all('ContraName').value="";
		 fm.all('DutyItemName').value="";
	}
}       

function afterCodeSelect(codeName,Field)
{
	if(codeName == "hmexecstate")	
	{
		for(var row=0; row < LHExecFeeBalanceGrid.mulLineCount; row++)
		{
			var tChk =LHExecFeeBalanceGrid.getChkNo(row); 
			if(tChk == true)
			{
				LHExecFeeBalanceGrid.setRowColData(row,19,fm.all('ExecState_ch').value);	
				LHExecFeeBalanceGrid.setRowColData(row,20,fm.all('ExecState').value);
	  		}  
		}	
	}
}