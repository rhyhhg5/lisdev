<%
    //程序名称：LHHealthServItemQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-03-16 10:49:43
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@ page import="com.sinosoft.lis.pubfun.*" %>
<script language="JavaScript">
    function initInpBox() {
        try {
            fm.all('ServItemCode').value = "";
            fm.all('ServItemName').value = "";
            fm.all('ServItemType').value = "";
        }
        catch(ex) {
            alert("在LHHealthServItemQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
        }
    }
    function initForm() {
        try {
            initInpBox();
            initLHHealthServItemGrid();
        }
        catch(re) {
            alert("LHHealthServItemQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
        }
    }
    //领取项信息列表的初始化
    var LHHealthServItemGrid;
    function initLHHealthServItemGrid() {
        var iArray = new Array();

        try {
            iArray[0] = new Array();
            iArray[0][0] = "序号";
            //列名
            iArray[0][1] = "30px";
            //列名
            iArray[0][3] = 0;
            //列名

            iArray[1] = new Array();
            iArray[1][0] = "标准服务项目代码";
            //列名
            iArray[1][1] = "100px";
            //列名
            iArray[1][3] = 0;
            //列名

            iArray[2] = new Array();
            iArray[2][0] = "标准服务项目名称";
            //列名
            iArray[2][1] = "100px";
            //列名
            iArray[2][3] = 0;
            //列名

            iArray[3] = new Array();
            iArray[3][0] = "服务项目类型代码";
            //列名
            iArray[3][1] = "100px";
            //列名
            iArray[3][3] = 3;
            //列名

            iArray[4] = new Array();
            iArray[4][0] = "服务项目类型";
            //列名
            iArray[4][1] = "100px";
            //列名
            iArray[4][3] = 0;
            //列名


            iArray[5] = new Array();
            iArray[5][0] = "标准服务项目释义";
            //列名
            iArray[5][1] = "100px";
            //列名
            iArray[5][3] = 0;
            //列名

            iArray[6] = new Array();
            iArray[6][0] = "创建日期";
            //列名
            iArray[6][1] = "100px";
            //列名
            iArray[6][3] = 0;
            //列名

            iArray[7] = new Array();
            iArray[7][0] = "修改日期";
            //列名
            iArray[7][1] = "100px";
            //列名
            iArray[7][3] = 0;
            //列名

            LHHealthServItemGrid = new MulLineEnter("fm", "LHHealthServItemGrid");
            //这些属性必须在loadMulLine前

            //    LHHealthServItemGrid.mulLineCount = 0;
            //    LHHealthServItemGrid.displayTitle = 1;
            LHHealthServItemGrid.hiddenPlus = 1;
            LHHealthServItemGrid.hiddenSubtraction = 1;
            LHHealthServItemGrid.canSel = 1;
            //    LHHealthServItemGrid.canChk = 0;
            //    LHHealthServItemGrid.selBoxEventFuncName = "showOne";

            LHHealthServItemGrid.loadMulLine(iArray);
            //这些操作必须在loadMulLine后面
            //LHHealthServItemGrid.setRowColData(1,1,"asdf");
        }
        catch(ex) {
            alert(ex);
        }
    }
</script>
