<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHScanInfoSave.jsp
//程序功能：
//创建日期：2005-01-19 13:58:55
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>  
<%
    //接收信息，并作校验处理。
    //输入参数
    TransferData tSendFactor = new TransferData();
    OLHScanInfoUI tOLHScanInfoUI   = new OLHScanInfoUI();
    
    //输出参数
    CErrors tError = null;
    String CustomerNo="";
    String SerialNo="";
    String ScanType = "";
    
    String FlagStr = "";
    String Content = "";
    String transact = "";
    String GrpFlag = request.getParameter("GrpFlag");//1-团体, 0-个人
    GlobalInput tG = new GlobalInput(); 
    tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");  
    tSendFactor.setNameAndValue("GrpFlag",GrpFlag);       
    
    if(GrpFlag.equals("1"))
    {
		tSendFactor.setNameAndValue("GrpContNo",request.getParameter("GrpContNo")); 
		tSendFactor.setNameAndValue("ScanType",request.getParameter("ScanTypeGrp"));
    }
    
    if(GrpFlag.equals("0"))
    {
    	tSendFactor.setNameAndValue("CustomerNo",request.getParameter("CustomerNo")); 
		tSendFactor.setNameAndValue("ScanTypePerson",request.getParameter("ScanTypePerson"));
    } 
    try
    {
		// 准备传输数据 VData
    	VData tVData = new VData();
		tVData.add(tSendFactor);
    	tVData.add(tG);
		tOLHScanInfoUI.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
    }

  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHScanInfoUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    	VData v = tOLHScanInfoUI.getResult();
    	LHScanInfoSet mLHScanInfoSet=new LHScanInfoSet();
    	mLHScanInfoSet.set((LHScanInfoSet)v.getObjectByObjectName("LHScanInfoSet",0));
    	CustomerNo = mLHScanInfoSet.get(1).getOtherNo();System.out.println("--------CustomerNo---------"+CustomerNo);
    	SerialNo = mLHScanInfoSet.get(1).getSerialNo();System.out.println("--------SerialNo---------"+SerialNo);
    	ScanType = mLHScanInfoSet.get(1).getOtherNoType();System.out.println("--------ScanType---------"+ScanType);
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
		parent.fraInterface.afterSubmitQueue("<%=CustomerNo%>","<%=SerialNo%>","<%=ScanType%>");
</script>
</html>
