<%
//程序名称：LHHealthMainQueryInput.jsp
//程序功能：功能描述
//创建日期：2005-05-18 09:08:27
//创建人  ：hm
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHHealthMainQueryInput.js"></SCRIPT> 
  <%@include file="LHHealthMainQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <br>
  <table>
    <tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,CustomHealthQuery);">
    		</td>
			<td class=titleImg align=center> 基础健康信息查询条件： </td>
		</tr>
	</table>
	<Div  id= "CustomHealthQuery" style= "display: ''">    
<table  class= common align='center' >
  			<TR  class= common>
    		  <TD  class= title>
                客户号码
              </TD>
              <TD  class= input>
                <Input class= 'code' name=CustomerNo ondblclick=" return showCodeList('hmldperson',[this,CustomerName],[0,1],null,fm.CustomerNo.value,'CustomerNo');"  onkeyup=" if( event.keyCode==13 ) return showCodeList('hmldperson',[this,CustomerName],[0,1],null,fm.CustomerNo.value,'CustomerNo');" verify="客户姓名|len<=24">
              </TD>
              <TD  class= title>
                客户姓名
              </TD>
              <TD  class= input>
                <Input class= 'code' name=CustomerName ondblclick=" return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name');"  onkeyup=" if( event.keyCode==13 ) return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name');" verify="客户姓名|len<=24">
              </TD>
              <TD  class= title>
                记录时间
              </TD>
              <TD  class= input>
                <Input type=hidden name=AddDate_h>
              	<Input style='width=20' class= 'code' name=AddDate_sp CodeData= "0|^早于|<^早于等于|<=^等于|=^晚于等于|>=^晚于|>" onclick="return showCodeListEx('simble',[this,AddDate_h],[1,1],null,null,null,1,75);" ><Input style='width=114' class= 'coolDatePicker' name=AddDate >                 
              </TD>
  			</TR>  
  			<TR  class= common>
    		  <TD  class= title>
                年龄	
              </TD>
              <TD  class= input>
                <Input type=hidden name=Age_h>
              	<Input style='width=20' class= 'code' name=Age_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,Age_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=Age >
              </TD>
              <TD  class= title>
                性别
              </TD>
              <TD  class= input>
                <Input class=codeno name=Sex ondblclick=" return showCodeList('sex',[this,Sexch],[0,1]);" ><Input class= 'codename' name=Sexch >
              </TD>
              <TD  class= title>
                血压舒张压
              </TD>
              <TD  class= input>
                <Input type=hidden name=BloodPressLow_h>
              	<Input style='width=20' class= 'code' name=BloodPressLow_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,BloodPressLow_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=BloodPressLow >
  			</TR>
  			<TR  class= common>
    					<TD  class= title>
                血压收缩压	
              </TD>
              <TD  class= input>
                <Input type=hidden name=BloodPressHigh_h>
              	<Input style='width=20' class= 'code' name=BloodPressHigh_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,BloodPressHigh_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=BloodPressHigh >
              </TD>
              <TD  class= title>
                脉压差
              </TD>
              <TD  class= input>
                <Input type=hidden name=BloodPress_h>
              	<Input style='width=20' class= 'code' name=BloodPress_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,BloodPress_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=BloodPress >
              </TD>
              <TD  class= title>
                体重指数
              </TD>
              <TD  class= input>
                <Input type=hidden name=AvoirdIndex_h>
              	<Input style='width=20' class= 'code' name=AvoirdIndex_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,AvoirdIndex_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=AvoirdIndex >
              </TD>
  			</TR>
  			<TR  class= common>
    		  <TD  class= title>
                吸烟（支/天）	
              </TD>
              <TD  class= input>
                <Input type=hidden name=Smoke_h>
              	<Input style='width=20' class= 'code' name=Smoke_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,Smoke_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=Smoke >
              </TD>
              <TD  class= title>
                饮酒（两/周）
              </TD>
              <TD  class= input>
                <Input type=hidden name=KissCup_h>
              	<Input style='width=20' class= 'code' name=KissCup_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,KissCup_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=KissCup >
              </TD>
              <TD  class= title>
                熬夜（次/月）
              </TD>
              <TD  class= input>
                <Input type=hidden name=SitUp_h>
              	<Input style='width=20' class= 'code' name=SitUp_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,SitUp_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=SitUp >
              </TD>
  			</TR>
  			<TR  class= common>
    					<TD  class= title>
                饮食不规律（餐/周）	
              </TD>
              <TD  class= input>
                <Input type=hidden name=DiningNoRule_h>
              	<Input style='width=20' class= 'code' name=DiningNoRule_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,DiningNoRule_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=DiningNoRule >
              </TD>
              <TD  class= title>
                是否有家族病史
              </TD>
              <TD  class= input>
                <Input class=codeno  name=IsFamilyDisease CodeData="0|^是|1^否|2" ondblclick=" return showCodeListEx('',[this,IsFamilyDisease_ch],[1,0],null,null,null,1); "><Input class= 'codename' name=IsFamilyDisease_ch >
              </TD>
              <TD  class= title>
                家族病史
              </TD>
              <TD  class= input>
                <Input class=codeno name=FamilyDiseaseCode ondblclick=" return showCodeList('lhdisease',[this,FamilyDisease],[1,0],null,fm.FamilyDisease.value,'ICDName',1,200);"><Input class= 'codename' name=FamilyDisease >
              </TD>
  			</TR>
</table>
  </Div>
  <br>
  <table>
        <tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,CustomInHospitalQuery);">
    		</td>
			<td class=titleImg>
				客户就诊信息条件查询：
			</td>
		</tr>
	</table>
	<Div  id= "CustomInHospitalQuery" style= "display: ''">    
<table  class= common align='center' >
  			<!--TR  class= common>
    		  		<TD  class= title>
                就诊时间
              </TD>
              <TD  class= input>
                <Input type=hidden name=InHospitalDate_h>
              	<Input style='width=20' class= 'code' name=InHospitalDate_sp CodeData= "0|^早于|>^早于等于|>=^等于|=^晚于等于|<=^晚于|<" onclick="return showCodeListEx('simble',[this,InHospitalDate_h],[1,1],null,null,null,1,75);" >
              	<Input style='width=114' class= 'coolDatePicker' name=InHospitalDate >
              </TD>
              <TD  class= title>
    
              </TD>
              <TD  class= input>
                
              </TD>
              <TD  class= title>
              
              </TD>
              <TD  class= input>
         
              </TD>
  			</TR-->  
  			<TR  class= common>
    		  <TD  class= title>
                医疗机构名称	
              </TD>
              <TD  class= input>
                <Input class=codeno name=HospitCode ondblclick="return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName',1,300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName',1,300)	; " verify="医疗机构名称|len<=60"><Input class= 'codename' name=HospitName >
              </TD>
              <!--TD  class= title>
                是否就诊
              </TD>
              <TD  class= input>
                <Input class= 'code' name=IsToHospital ondblclick="showCodeListEx('',[this,IsToHospital_ch],[0,1],null,null,null,1)" CodeData= "0|^是|1^否|0">
								<input type=hidden name=IsToHospital_ch >              
              </TD-->
              <TD  class= title>
                就诊方式
              </TD>
              <TD  class= input>
                <Input class=codeno  name=InHospitModeCode ondblclick=" return showCodeList('hminhospitalmode',[this,InHospitMode],[1,0]);"  verify="就诊方式代码|len<=10"><Input class= 'codename' name=InHospitMode >
              </TD>
              <TD  class= title>
                就诊时间
              </TD>
              <TD  class= input>
                <Input type=hidden name=InHospitalDate_h>
              	<Input style='width=20' class= 'code' name=InHospitalDate_sp CodeData= "0|^早于|<^早于等于|<=^等于|=^晚于等于|>=^晚于|>" onclick="return showCodeListEx('simble',[this,InHospitalDate_h],[1,1],null,null,null,1,75);" ><Input style='width=114' class= 'coolDatePicker' name=InHospitalDate >
              </TD>
  			</TR>
  			<TR  class= common>
    		  <TD  class= title>
                主要治疗方式	
              </TD>
              <TD  class= input>
                <Input class=codeno  name="MainCureModeCode" ondblclick="return showCodeList('maincuremode',[this,MainCureMode_ch],[1,0]);"   verify="主要治疗方式|len<=10"><Input class= 'codename' name="MainCureMode_ch" >
              </TD>
              <TD  class= title>
                疾病治疗转归
              </TD>
              <TD  class= input>
                <Input class=codeno  name=CureEffectCode ondblclick="return showCodeList('diagnosecureeffect',[this,CureEffect],[0,1]);" verify="疾病转归|len<=10"><Input class= 'codename' id=CureEffect name=CureEffect >
              </TD>
              <TD  class= title>
                住院天数 
              </TD>
              <TD  class= input>
                <Input type=hidden name=InHospitalDays_h>
              	<Input style='width=20' class= 'code' name=InHospitalDays_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,InHospitalDays_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=InHospitalDays >
              </TD>
  			</TR>
  			<TR  class= common>
    		  <TD  class= title>
                诊断疾病名称	
              </TD>
              <TD  class= input>
                <Input class=codeno name=ICDCode ondblclick=" return showCodeList('lddiseasename',[this,ICDName],[0,1],null,fm.ICDName.value,'ICDName',1,350);" onkeyup=" if(event.keyCode == 13) return showCodeList('lddiseasename',[this,ICDName],[0,1],null,fm.ICDName.value,'ICDName',1,350);" verify="疾病名称|len<=30"><Input class=codename name=ICDName >
              </TD>
              <TD  class= title>
                检查项目费用
              </TD>
              <TD  class= input>
                <Input type=hidden name=testfeeamount_h>
              	<Input style='width=20' class= 'code' name=testfeeamount_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,testfeeamount_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=testfeeamount>
              </TD>
              <TD  class= >
                手术费用
              </TD>
              <TD  class= input>
                <Input type=hidden name=OPSFeeAmount_h>
              	<Input style='width=20' class= 'code' name=OPSFeeAmount_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,OPSFeeAmount_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=OPSFeeAmount>
              </TD>
  			</TR>
  			<TR  class= common>
    		  <TD  class= title>
                治疗项目费用	
              </TD>
              <TD  class= input>
                <Input type=hidden name=CustomOtherCure_h>
              	<Input style='width=20' class= 'code' name=CustomOtherCure_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,CustomOtherCure_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=CustomOtherCure>
              </TD>
              <TD  class= title>
                总费用
              </TD>
              <TD  class= input>
                <Input type=hidden name=FeeAmount_h>
              	<Input style='width=20' class= 'code' name=FeeAmount_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,FeeAmount_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=FeeAmount>
              </TD>
              <TD  class= title>
<!--                分项目费用比例-->
              </TD>
              <TD  class= input>
                <!--Input type=hidden name=ItemFeeRate_h>
              	<Input style='width=20' class= 'code' name=ItemFeeRate_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,ItemFeeRate_h],[1,1],null,null,null,1,75);" >
              	<Input style='width=134' class= 'common' name=ItemFeeRate-->
              </TD>
  			</TR>
</table>
  </Div>
  <br>
  <table>
   		<tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,CustomTestQuery);">
    		</td>
			<td class=titleImg>客户检查信息：</td>
		</tr>
	</table>
	<Div  id= "CustomTestQuery" style= "display: ''">    
<table  class= common align='center' >
  			<!--TR  class= common>
    		  <TD  class= title>
                是否体检
              </TD>
              <TD  class= input>
                <Input class= 'code' name=IsToTest >
              </TD>
              <TD  class= title>
    
              </TD>
              <TD  class= input>
                
              </TD>
              <TD  class= title>
              
              </TD>
              <TD  class= input>
         
              </TD>
  			</TR-->  
  			<TR  class= common>
    		  <TD  class= title>
                体检时间	
              </TD>
              <TD  class= input>
                <Input type=hidden name=TestDate_h>
              	<Input style='width=20' class= 'code' name=TestDate_sp CodeData= "0|^早于|<^早于等于|<=^等于|=^晚于等于|>=^晚于|>" onclick="return showCodeListEx('simble',[this,TestDate_h],[1,1],null,null,null,1,75);" ><Input style='width=114' class= 'coolDatePicker' name=TestDate>
              </TD>
              <TD  class= title>
                体检机构名称
              </TD>
              <TD  class= input>
                <Input class=codeno name=TestHospitCode ondblclick="return showCodeList('lhhospitname',[this,TestHospitName],[1,0],null,fm.TestHospitName.value,'HospitName',1,300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,TestHospitName],[1,0],null,fm.TestHospitName.value,'HospitName',1,300)	; " verify="医疗机构名称|len<=60"><Input class= 'codename' name=TestHospitName >
              </TD>
              <TD  class= title>
                体检方式
              </TD>
              <TD  class= input>
                <Input  class=codeno name=TestModeCode ondblclick="return showCodeList('lhtestmode',[this,TestMode],[1,0]);" onkeyup="return showCodeListKey('lhtestmode',[this,TestMode],[1,0]);" verify="体检方式代码|len<=10"><Input class= 'codename' name=TestMode >
              </TD>
  			</TR>
  			<TR  class= common>
    		  <TD  class= title>
                体检总费用
              </TD>
              <TD>
              	<Input type=hidden name=TestAllFeeAmount_h>
              	<Input style='width=20' class= 'code' name=TestAllFeeAmount_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,TestAllFeeAmount_h],[1,1],null,null,null,1,75);" ><Input style='width=134' class= 'common' name=TestAllFeeAmount >  
              </TD>
            </TR>
</table>
  </Div>
	
  <br>  
<!--
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick=""> 					
      </TD>
    </TR>
  </table>
-->        
  <hr>   
  <table class= common border=0 width=100%>
  	<TR class= common>
  		<TD class=title>
  			<Input value="查询" type=hidden class=cssbutton onclick="mainQuery();">
  			
  	 		<Input value="基础健康信息" type=button class=cssbutton onclick="healthQuery();">
  	 		<Input value="客户就诊信息" type=button class=cssbutton onclick="diagnoseQuery();">
  	 		<Input value="客户检查信息" type=button class=cssbutton onclick="testQuery();">
  	 		<Input value="重置" type=button class=cssbutton onclick="inputClear();">
  	 		<Input value="保全查询" type=button class=cssbutton onclick="edorQuery();">
  	 	</TD>
  	 </TR>
  </table>
  
  
  <INPUT VALUE="" type=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
