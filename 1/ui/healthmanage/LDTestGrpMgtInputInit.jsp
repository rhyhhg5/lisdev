<%
//程序名称：LDTestGrpMgtInput.jsp
//程序功能：
//创建日期：2005-06-13 10:00:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('TestGrpCode').value = "";
    fm.all('TestGrpName').value = "";
    fm.all('TestGrpType').value = "";
    fm.all('TestGrpType_ch').value = "";
    fm.all('HospitCode').value = "";
    fm.all('HospitName').value = "";
    fm.all('GrpInBelly').value = "";

  }
  catch(ex)
  {
    alert("在LDTestGrpMgtInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LDTestGrpMgtInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();  
    initTestGrpItemGrid();  
    initTestGrpCombineGrid();
  }
  catch(re)
  {
    alert("LDTestGrpMgtInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initTestGrpItemGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="检查项目代码";   
		iArray[1][1]="150px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
		iArray[1][4]="lhmedicalitem";
		iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
		iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值
		iArray[1][9]="检查项目代码|LEN<20";
		iArray[1][15]="MedicaItemCode";
		iArray[1][17]="1"; 
		iArray[1][19]="1" ;
		
		
		iArray[2]=new Array(); 
		iArray[2][0]="检查项目名称";   
		iArray[2][1]="250px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="lhmedicalitem2";                                                              
		iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符                                  
		iArray[2][6]="0|1";    //上面的列中放置引用代码中第几位值
		iArray[2][15]="MedicaItemName";    
		iArray[2][17]="2"; 
		iArray[2][19]="1" ;                                                        
		
		
    TestGrpItemGrid = new MulLineEnter( "fm" , "TestGrpItemGrid" ); 
    //这些属性必须在loadMulLine前

    TestGrpItemGrid.mulLineCount = 0;   
    TestGrpItemGrid.displayTitle = 1;
    TestGrpItemGrid.hiddenPlus = 0;
    TestGrpItemGrid.hiddenSubtraction = 0;
    TestGrpItemGrid.canSel = 0;
    TestGrpItemGrid.canChk = 0;
    //TestGrpItemGrid.selBoxEventFuncName = "showOne";

    TestGrpItemGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //TestGrpItemGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

//套餐-组合
function initTestGrpCombineGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="组合项代码";   
		iArray[1][1]="123px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
		iArray[1][4]="lhmedicalitem3";				//codeQuery中匹配代码
		iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
		iArray[1][6]="0|1";     //上面的列中放置引用代码中第几位值
		iArray[1][9]="组合项代码|LEN<20";
		iArray[1][15]="TestGrpCode";
		iArray[1][17]="1"; 
		iArray[1][19]="1" ;
		
		
		iArray[2]=new Array(); 
		iArray[2][0]="组合项名称";   
		iArray[2][1]="222px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="lhmedicalitem4";        //codeQuery中匹配代码                                                      
		iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符                                  
		iArray[2][6]="0|1";     //上面的列中放置引用代码中第几位值
		iArray[2][15]="TestGrpName";    
		iArray[2][17]="2"; 
		iArray[2][19]="1" ;                     
		
		iArray[3] = new Array(); 
		iArray[3][0] = "是否空腹";
		iArray[3][1] = "50px";
		iArray[3][2] = 20;
		iArray[3][3] = 3;
		iArray[3][4]="yesno";        //codeQuery中匹配代码 
		
    TestGrpCombineGrid = new MulLineEnter( "fm" , "TestGrpCombineGrid" ); 
    //这些属性必须在loadMulLine前

    TestGrpCombineGrid.mulLineCount = 0;   
    TestGrpCombineGrid.displayTitle = 1;
    TestGrpCombineGrid.hiddenPlus = 0;
    TestGrpCombineGrid.hiddenSubtraction = 0;
    TestGrpCombineGrid.canSel = 0;
    TestGrpCombineGrid.canChk = 0;
    //TestGrpCombineGrid.selBoxEventFuncName = "showOne";

    TestGrpCombineGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //TestGrpCombineGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
