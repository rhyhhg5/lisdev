<%
//程序名称：LHCustomInHospitalQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-18 09:08:27
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try 
  {     

   
  }
  catch(ex) {
    alert("在LHCustomInHospitalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() 
{
	  try 
	  {
	    initInpBox();
	    initLHMainCustomHealthGrid();  
	    initLHMainCustomHealth2Grid(); 
	  }
	  catch(re) 
	  {
	    alert("LHCustomInHospitalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	  }
}
//领取项信息列表的初始化
var LHMainCustomHealthGrid;
function initLHMainCustomHealthGrid() {      //这个实际是就诊信息                         
  var iArray = new Array();
    
  try {
        
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="客户号码";   
	  iArray[1][1]="60px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
    
    iArray[2]=new Array(); 
	  iArray[2][0]="客户姓名";   
	  iArray[2][1]="40px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="就诊时间";   
	  iArray[3][1]="60px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=0;
    
    iArray[4]=new Array(); 
	  iArray[4][0]="医疗机构";   
	  iArray[4][1]="140px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	         
	  iArray[5]=new Array(); 
	  iArray[5][0]="就诊方式";   
	  iArray[5][1]="40px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=0;
	
	  iArray[6]=new Array(); 
	  iArray[6][0]="主要治疗方式";   
	  iArray[6][1]="60px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=0;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="疾病治疗转归";   
	  iArray[7][1]="60px";   
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="住院天数";   
	  iArray[8][1]="40px";   
     iArray[8][3]=0;
    
    iArray[9]=new Array(); 
	  iArray[9][0]="诊断疾病名称";   
	  iArray[9][1]="0px";   
    iArray[9][3]=3;
    
    iArray[10]=new Array(); 
	  iArray[10][0]="检查费用";   
	  iArray[10][1]="40px";   
    iArray[10][3]=1;
    
    iArray[11]=new Array(); 
	  iArray[11][0]="手术费用";   
	  iArray[11][1]="40px";   
    iArray[11][3]=1;
    
    iArray[12]=new Array(); 
	  iArray[12][0]="治疗费用";   
	  iArray[12][1]="40px";   
    iArray[12][3]=1;
    
    iArray[13]=new Array(); 
	  iArray[13][0]="总费用";   
	  iArray[13][1]="40px";   
    iArray[13][3]=1;	
    
    iArray[14]=new Array(); 
	  iArray[14][0]="流水号";   
	  iArray[14][1]="40px";   
    iArray[14][3]=1;    
    
            
    LHMainCustomHealthGrid = new MulLineEnter( "fm" , "LHMainCustomHealthGrid" ); 
    //这些属性必须在loadMulLine前

    LHMainCustomHealthGrid.mulLineCount = 3;   
    LHMainCustomHealthGrid.displayTitle = 1;
    LHMainCustomHealthGrid.hiddenPlus = 1;
    LHMainCustomHealthGrid.hiddenSubtraction = 1;
    LHMainCustomHealthGrid.canSel = 1;
    LHMainCustomHealthGrid.canChk = 0;
    //LHCustomInHospitalGrid.selBoxEventFuncName = "showOne";

    LHMainCustomHealthGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomInHospitalGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

var LHMainCustomHealth2Grid;
function initLHMainCustomHealth2Grid() {      //这个实际是就诊信息                         
  var iArray = new Array();
    
  try {
        
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="客户号码";   
	  iArray[1][1]="60px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
    
    iArray[2]=new Array(); 
	  iArray[2][0]="客户姓名";   
	  iArray[2][1]="40px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="第几次记录";   
	  iArray[3][1]="60px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=0;
    
    iArray[4]=new Array(); 
	  iArray[4][0]="记录时间";   
	  iArray[4][1]="140px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	         
	  iArray[5]=new Array(); 
	  iArray[5][0]="身高";   
	  iArray[5][1]="40px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=0;
	
	  iArray[6]=new Array(); 
	  iArray[6][0]="体重";   
	  iArray[6][1]="60px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=0;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="体重指数";   
	  iArray[7][1]="60px";   
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="血压舒张压";   
	  iArray[8][1]="40px";   
     iArray[8][3]=0;
    
    iArray[9]=new Array(); 
	  iArray[9][0]="血压收缩压";   
	  iArray[9][1]="0px";   
    iArray[9][3]=3;
    
    iArray[10]=new Array(); 
	  iArray[10][0]="吸烟（支/天）";   
	  iArray[10][1]="40px";   
    iArray[10][3]=1;
    
    iArray[11]=new Array(); 
	  iArray[11][0]=" 饮酒（两/周）";   
	  iArray[11][1]="40px";   
    iArray[11][3]=1;
    
    iArray[12]=new Array(); 
	  iArray[12][0]="熬夜（次/月）";   
	  iArray[12][1]="40px";   
    iArray[12][3]=1;
    
    iArray[13]=new Array(); 
	  iArray[13][0]="饮食不规律（餐/周）";   
	  iArray[13][1]="40px";   
    iArray[13][3]=1;	
      
    
            
    LHMainCustomHealth2Grid = new MulLineEnter( "fm" , "LHMainCustomHealth2Grid" ); 
    //这些属性必须在loadMulLine前

    LHMainCustomHealth2Grid.mulLineCount = 3;   
    LHMainCustomHealth2Grid.displayTitle = 1;
    LHMainCustomHealth2Grid.hiddenPlus = 1;
    LHMainCustomHealth2Grid.hiddenSubtraction = 1;
    LHMainCustomHealth2Grid.canSel = 1;
    LHMainCustomHealth2Grid.canChk = 0;
    //LHCustomInHospitalGrid.selBoxEventFuncName = "showOne";

    LHMainCustomHealth2Grid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomInHospitalGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}


</script>
