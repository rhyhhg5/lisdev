<%
  //程序名称：LHHospitalBasicStat.jsp
  //程序功能：理赔费用医院分布情况统计
  //创建日期：2008-01-17
  //创建人  ：liuli
  //更新人  ：
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
      GlobalInput tGI = new GlobalInput();
      tGI = (GlobalInput)session.getValue("GI");
%>
<script type="">
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="CustomerDiagnose.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

</head>
<body onload="initform();initElementtype();">
<table>
  <tr>
    <td class=titleImg>客户就诊情况</td>
  </tr>
</table>
<form action="./CustomerDiagnoseSave.jsp" method=post name=fm target=f1print>
<table class= common align='center'>
	<TR>
  	<TD  class= title8>
     开始日期
    </TD>
  	<TD  class= input8>
      <Input class= 'coolDatePicker'  dateFormat="Short" name=MakeDate1 value="" verify="入机时间|DATE">
    </TD>
     <TD  class= title8>
     截止日期
    </TD>
    <TD class=input8>
      <Input class= 'coolDatePicker'  dateFormat="Short" name=MakeDate2 value="" verify="入机时间|DATE">
    </TD>
    <td>    </td>
    <td>    </td>
   </TR>
   <TR class="common">
    <TD class=title8>机构名称</TD>
    <TD>
      <Input class=fcodeno  name=ManageCom verify="机构名称|code:comcode&NOTNULL" value="<%=tGI.ManageCom%>" ondblclick="return showCodeList('comcode4',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=fcodename name=ManageComName elementtype=nacessary>
    </TD>
    <TD class=title8>费用类型</TD>
    <TD class=input8>
    	<input class="fcodeno" name="FeeType" value='0' CodeData="0|4^0|全选^1|门诊^2|住院^3|门诊特殊病"  ondblclick="return showCodeListEx('feetype',[this,FeeTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('feetype',[this,FeeTypeName],[0,1]);"><input class=fcodename name=FeeTypeName value='全选' elementtype=nacessary verify="账单种类|notnull">
    </TD>
     <TD class=title8>客户类型</TD>
     <TD class=input8>
     	<input class="fcodeno" name="conttype" value='1' CodeData="0|2^1|个人保单^2|团体保单" ondblclick="return showCodeListEx('conttype',[this,contTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('conttype',[this,contTypeName],[0,1]);"><input class=fcodename name=contTypeName value='个人保单' elementtype=nacessary verify="客户类型｜notnull">
     </TD>
  </TR>
  <tr>
  	<TD  class= title8>
       <input type="checkbox" value="1" name="hospitalnum" onclick="">给付一家医院</input>
	   
    </TD>
  	<TD  class= title8>
      <input type="checkbox" value="2" name="hospitalnum" onclick="">给付多家医院</input>
   </TD>
  </tr>
<tr>
	<td><br></td>
</tr>
  <tr>
  	<TD class=title8>
  			<Input value="客户就诊情况统计" style="width:130px"  type=button class=cssbutton onclick="CustomerSubmit();">&nbsp;&nbsp;
  	</TD>
  	<TD ></TD>
  </tr>
  <input type=hidden id="hospital" name="hospital">
</table>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
<script>
	function initform(){ 
    var   str="select Current Date  from dual";  
    fm.all("MakeDate2").value = easyExecSql(str);  
    var sql = "select Current Date - 1 month from dual ";
    fm.MakeDate1.value = easyExecSql(sql); 	 
	}
</script>