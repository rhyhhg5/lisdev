<%
//程序名称：LHServerItemPriceInput.jsp
//程序功能：
//创建日期：2006-03-22 16:59:26
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ServItemCode').value = "";
    fm.all('ServItemName').value = "";

	if(manageCom.length == 2)
	{
	    fm.all('ComID').value = manageCom;
	    fm.all('ComID_cn').value = "总公司";            
	}
	else
	{
		fm.all('ComID').value = manageCom.substring(0, 4);
		var sql = " select showname from ldcom where  sign = '1' and trim(comcode) = '"+fm.all('ComID').value+"' ";
	    fm.all('ComID_cn').value = easyExecSql(sql);
	}
			
  }
  catch(ex)
  {
    alert("在LHServerItemPriceInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try{}
  catch(ex)
  {
    alert("在LHServerItemPriceInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
  	fm.all('querybutton').disabled=true;
    initInpBox();   
    initItemGrid();
    initItemPriceGrid();
    
    if(manageCom != "86")
    {
    	fm.all('ComID_cn').ondblclick = "";	
    }
    ;
  }
  catch(re)
  {
    alert("LHServerItemPriceInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var ItemGrid;
function initItemGrid() 
{
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";		//列名
	    iArray[0][1]="30px";		//列名
	    iArray[0][3]=0;				//列名
	    
	    iArray[1]=new Array();		
		iArray[1][0]="标准服务项目代码";		//列名    
		iArray[1][1]="90px";		//列名    
		iArray[1][3]=0;				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="标准服务项目名称";	//列名    
		iArray[2][1]="90px";       //列名    
		iArray[2][3]=0;         	//列名        
			
		iArray[3]=new Array();                    
		iArray[3][0]="标准服务项目释义";    //列名    
		iArray[3][1]="280px";       //列名    
		iArray[3][3]=0;         	//列名   
		
		iArray[4]=new Array();  
		iArray[4][0]=" ";
		iArray[4][1]="0px";   
		iArray[4][3]=3;         
	    
	    ItemGrid = new MulLineEnter( "fm" , "ItemGrid" ); 
	    //这些属性必须在loadMulLine前           
	    
		ItemGrid.canSel = 1;
		ItemGrid.hiddenPlus = 1;
		ItemGrid.hiddenSubtraction = 1;
		ItemGrid.selBoxEventFuncName = "showOne";
	/*
	    ItemGrid.mulLineCount = 0;   
	    ItemGrid.displayTitle = 1;
	    ItemGrid.canSel = 1;
	    ItemGrid.canChk = 0;
	    ItemGrid.selBoxEventFuncName = "showOne";
	*/
	    ItemGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //ItemGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
		alert(ex);
	}
}



var ItemPriceGrid;
function initItemPriceGrid() 
{
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";		//列名
	    iArray[0][1]="30px";		//列名
	    iArray[0][3]=0;				//列名
	    
	    iArray[1]=new Array();		
		iArray[1][0]="标准服务项目代码";		//列名    
		iArray[1][1]="80px";		//列名    
		iArray[1][3]=0;				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="标准服务项目名称";	//列名    
		iArray[2][1]="120px";       //列名    
		iArray[2][3]=0;         	//列名        
			
		iArray[3]=new Array();                    
		iArray[3][0]="定价方式代码";    //列名    
		iArray[3][1]="80px";       //列名    
		iArray[3][3]=1;         	//列名   
		
		iArray[4]=new Array();  
		iArray[4][0]="定价方式名称";
		iArray[4][1]="120px";   
		iArray[4][3]=1;         
		
		iArray[5]=new Array();  
		iArray[5][0]="MakeDate";
		iArray[5][1]="0px";   
		iArray[5][3]=3;         
		
		iArray[6]=new Array();  
		iArray[6][0]="MakeTime";
		iArray[6][1]="0px";   
		iArray[6][3]=3;         
			     
	    
	    ItemPriceGrid = new MulLineEnter( "fm" , "ItemPriceGrid" ); 
	    //这些属性必须在loadMulLine前           
	    
		ItemPriceGrid.canSel = 1;
		ItemPriceGrid.hiddenPlus = 0;
		ItemPriceGrid.hiddenSubtraction = 0;
		ItemPriceGrid.addEventFuncName = "addEvent";

	/*
	    ItemPriceGrid.mulLineCount = 0;   
	    ItemPriceGrid.displayTitle = 1;
	    ItemPriceGrid.canSel = 1;
	    ItemPriceGrid.canChk = 0;
	*/
	    ItemPriceGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //ItemPriceGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
		alert(ex);
	}
}

</script>
