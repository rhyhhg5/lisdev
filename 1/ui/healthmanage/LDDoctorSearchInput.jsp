<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2007-12-6 10:10:52
//创建人  ：ll
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDDoctorSearchInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDDoctorSearchInputInit.jsp"%>
</head>
<script language="javaScript">
  function selected()
  {
     fm.BirthYear.value=fm.BirthYear.value;
  }
  
  function selected2()
  { 
     fm.BirthYear.value="1976"; 
  }
</script>
<body  onload="initForm();" >
  <form action="./LDDoctorSearchSave.jsp" method=post name=fm target="f1print">
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDDoctor1);">
    		</td>
    		 <td class= titleImg>
        		 医师信息检索
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDDoctor1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>  
    	管理机构    
    </TD>
    <TD  class= input>
    	<Input class=codeNo  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName elementtype=nacessary>
    </TD>
	 	<TD  class= title>  
    	地区名称    
    </TD>
    <TD  class= input>
    	<Input class= codeNo name=AreaCode ondblClick= "showCodeList('hmareaname',[this,AreaName],[1,0],null,fm.AreaName.value,'codename',1);" onkeyup= "showCodeListKeyEx('AreaCode',[this,AreaName],[1,0],null,null,null,1);"><Input class=codename name=AreaName verify="地区名称|notnull&len<=20"  elementtype=nacessary>
    </TD> 
    <TD  class= title>
      所属医疗机构
    </TD>
    <TD  class= input >
      <Input class=codeno id=HospitCode name=HospitCode ondblclick="return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName')	; " verify="医疗机构名称|len<=60"><Input class= 'codename' name=HospitName >
    </TD> 
  </TR>
  <TR  class=common>
  	<TD  class= title>  
    	聘用方式    
    </TD>
    <TD  class= input>
    	<Input class=codeno name=EngageClass verify="聘用方式|len<=20" ondblClick="showCodeList('engageclasscode',[this,EngageClass_ch],[0,1],null,null,null,1,160);" ><Input class= 'codename' name=EngageClass_ch >
    </TD>  
  	<TD  class= title>
        科室专业
    </TD>
    <TD  class= input>
      <Input class=codeno name=SecName verify="科室|len<=40"  ondblclick="return showCodeList('hmsecname',[this,SecName_cn],[0,1],null,null,null,1);"><Input class= 'codename' name=SecName_cn elementtype=nacessary >
    </TD>
    <TD class= title>
	    职业类型
	</TD>
    <TD class= input>
	  	<Input class=codeno name=CarreerClass verify="职业类型|len<=20" " ondblClick= "showCodeList('carreertype',[this,CarreerClass_ch],[0,1],null,null,null,1);"><Input class= 'codename' name=CarreerClass_ch >
	</TD>
</TR>
<TR class= common>  
	  
	  <TD  class= title>
      职称
    </TD>
    <TD  class= input>
      <Input class=codeno name=TechPostCode verify="职称|len<=10" ondblClick="showCodeList('techpost',[this,TechPostCode_ch],[1,0],null,fm.CarreerClass.value,'code',1);" ><input class= 'codename' name=TechPostCode_ch>
    </TD>
    <TD  class= title>
      专家标识
    </TD>
    <TD  class= input>
      <Input class=codeno name=ExpertFlag  verify="专家标志|len<=2" CodeData= "0|^0|否^1|是" ondblClick="showCodeListEx('ExpertFlag',[this,ExpertFlag_ch],[0,1],null,null,null,1);"><Input class= 'codename' name=ExpertFlag_ch elementtype=nacessary >
    </TD> 
     <TD  class= title>
      学历
    </TD>
    <TD  class= input>
    	<Input class=codeno name=EduLevelCode verify="学历|len<=20" ondblClick="showCodeList('hmedulevelcode',[this,EduLevelCode_ch],[0,1],null,null,null,1,160);" ><Input class= 'codename' name=EduLevelCode_ch >
    </TD>     
   
  </TR>
  <TR  class= common>
  	<TD  class= title>
      医师姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Doctname verify="医师姓名|len<=20"  >
    </TD> 
  	<TD  class= title>
      出生年份
    </TD>
    <TD  class= input>
      <select class=healthYear name="BirthYear" size="1" onchange="javaScript:selected()" style="font-size: 12;width:162px" >
      	                 <option value="1921">1921</option>   
                         <option value="1922">1922</option>   
                         <option value="1923">1923</option>   
                         <option value="1924">1924</option>   
                         <option value="1925">1925</option>   
                         <option value="1926">1926</option>   
                         <option value="1927">1927</option>   
                         <option value="1928">1928</option>   
                         <option value="1929">1929</option> 
      	                 <option value="1930">1930</option>   
                         <option value="1931">1931</option>   
                         <option value="1932">1932</option>   
                         <option value="1933">1933</option>   
                         <option value="1934">1934</option>   
                         <option value="1935">1935</option>   
                         <option value="1936">1936</option>   
                         <option value="1937">1937</option>   
                         <option value="1938">1938</option>   
                         <option value="1939">1939</option> 
      	                 <option value="1940">1940</option>   
                         <option value="1941">1941</option>   
                         <option value="1942">1942</option>   
                         <option value="1943">1943</option>   
                         <option value="1944">1944</option>   
                         <option value="1945">1945</option>   
                         <option value="1946">1946</option>   
                         <option value="1947">1947</option>   
                         <option value="1948">1948</option>   
                         <option value="1949">1949</option>     
      	                 <option value="1950">1950</option>   
                         <option value="1951">1951</option>   
                         <option value="1952">1952</option>   
                         <option value="1953">1953</option>   
                         <option value="1954">1954</option>   
                         <option value="1955">1955</option>   
                         <option value="1956">1956</option>   
                         <option value="1957">1957</option>   
                         <option value="1958">1958</option>   
                         <option value="1959">1959</option>   
      	                 <option value="1960">1960</option>
                         <option value="1961">1961</option>
                         <option value="1962">1962</option>
                         <option value="1963">1963</option>
                         <option value="1964">1964</option>
                         <option value="1965">1965</option>
                         <option value="1966">1966</option>
                         <option value="1967">1967</option>
                         <option value="1968">1968</option>
                         <option value="1969">1969</option>
                         <option value="1970">1970</option>
                         <option value="1971">1971</option>
                         <option value="1972">1972</option>
                         <option value="1973">1973</option>
                         <option value="1974">1974</option>
                         <option value="1975">1975</option>
                         <option value="1976">1976</option>
                         <option value="1977">1977</option>
                         <option value="1978">1978</option>
                         <option value="1979">1979</option>
                         <option value="1980">1980</option>
                         <option value="1981">1981</option>
                         <option value="1982">1982</option>
                         <option value="1983">1983</option>
                         <option value="1984">1984</option>
                         <option value="1985">1985</option>
                         <option value="1986">1986</option>
                         <option value="1987">1987</option>
                         <option value="1988">1988</option>
                         <option value="1989">1989</option>
                         <option value="1990">1990</option>

         </select>
    </TD>
    <TD  class= title>
      性别
    </TD>
    <TD  class= input>
    	<Input class=codeno name=Sex ondblclick="return showCodeList('Sex',[this,Sex_ch],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,Sex_ch],[0,1]);" verify="姓名|len<=10"><Input class= 'codename' name=Sex_ch >
    </TD>
  </TR> 
  <TR>
  	<TD class=title>
      <Input value="医师信息检索" style="width:130px" type=button class=cssbutton onclick="aDoctorSearch();">
   </TD>
   <TD></TD>  <TD></TD>
  </TR>
</table>
    
    <table >
    	<tr class=common>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDDoctor1);">
    		</td>
    		 <td class= titleImg>
        		 医师情况统计
       		 </td>   		 
    	</tr>
    </table>
  	<table class=common>
  		<tr  class=common>
  			<td class=title >
  				起始时间
  			</td>
  			<td>
  				 <Input class='coolDatePicker' dateFormat="Short" name=MakeDate1 value="" verify="入机时间|DATE">
  			</td>
  			<td class=title >
  				结束时间
  			</td>
  			<td>
  				 <Input class='coolDatePicker' dateFormat="Short" name=MakeDate2 value="" verify="入机时间|DATE">
  			</td>
  		</tr>
  		<tr>
  			<td COLSPAN=2>
  				<Input value="医师情况统计" style="width:130px" type=button class=cssbutton onclick="aDoctorStat();">
  			</td>
  			<td>
  			</td>
  			<td>
  			</td>
  		</tr>
  	</table>
  	<input type=hidden id="fmAction" name="fmAction">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

