//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var StrSql;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在LHSettingSlipQuery.js-->resetForm函数中发生异常:初始化界面错误!");
	}
} 
//取消按钮对应操作
function cancelForm()
{
    showDiv(inputButton,"false"); 
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
            
        
function checkDate()
{
	if(fm.all('StartDate').value != "" && fm.all('EndDate').value != "")
	{
		if(compareDate(fm.all('StartDate').value,fm.all('EndDate').value) == true)
		{
			alert("保单起始时间应早于保单终止时间 ");
			return false;
		}
	}
}
function showQueryInfo()
{    
	var strSql=" select b.ServCaseCode ,a.ServCaseName, "
	          +" (case a.ServCaseType when '1' then '个人服务事件' when '2' then '集体服务事件' when '3' then '费用结算事件' else '无' end ),"
	          +" (case a.ServCaseState when '0' then '未进行启动设置' when '1' then '待其他事件完成' when '1+' then '等待事件已完成'when '2' then '服务事件已启动'  when '3' then '服务事件已完成' when '4' then '服务事件失败' else '无' end ) ,"
	          +" a.ServCaseType , "
	          +" a.ServCaseState"
	          +" from LHServCaseRela b  ,LHServCaseDef a "
              +" where a.ServCaseCode=b.ServCaseCode and "
              +" 1=1 "
              +getWherePart("b.ContNo","ContNo","like" )  
              +getWherePart("b.CustomerNo","CustomerNo")
              +getWherePart("b.CustomerName","CustomerName" )
              +getWherePart("b.ServItemCode","ServItemCode")
              +getWherePart("b.ServCaseCode","ServCaseCode")
              +getWherePart("a.ServCaseName","ServCaseName" )
              +getWherePart("a.ServCaseType","ServCaseType")
              +getWherePart("a.ServCaseState","ServCaseState")    
			  ;
         //alert(strSql);
         //alert(easyExecSql(strSql));
         turnPage.queryModal(strSql, LHSettingSlipQueryGrid);
}
function showOne()
{
	var getSelNo = LHSettingSlipQueryGrid.getSelNo();
	var ContNo = LHSettingSlipQueryGrid.getRowColData(getSelNo-1, 1);

	divLHQueryDetailGrid.style.display='';
	divPage2.style.display='';
	try
	{ 
      var strSql=" select InsuredNo,"
	            +"InsuredName,"
	            +"(select ServPlanName from LHServPlan b  where b.ContNo=a.ContNo), "
	            +"(select case when ServPlanLevel='1' then  '一档' when ServPlanLevel='2' then  '二档'when ServPlanLevel='3' then  '三档' when  ServPlanLevel='4' then  '四档' when ServPlanLevel='5' then  '五档' when ServPlanLevel='6' then  '六档' when ServPlanLevel='7' then  '七档'  else '无' end  from LHServPlan c  where c.ContNo=a.ContNo) "
                +" from lccont a "
                +" where ContNo='"+ContNo+"'and  " 
                +"  1=1 "
                ;
         //alert(strSql);
         //alert(easyExecSql(strSql));
       turnPage2.queryModal(strSql, LHQueryDetailGrid);
	}
	catch(ex)
	{
		alert("LHItemPriceFactorInputInit.jsp-->传输入信息Info中发生异常:初始化界面错误!");
	}
}
function showContPlan()
{
	if(fm.all('ContType').value==""||fm.all('ContType').value=="null"||fm.all('ContType').value==null)
	{
		alert("请选择保单类型!");
	}
	if(fm.all('ContType').value=="1")
	{
		if(LHSettingSlipQueryGrid.getSelNo() >= 1)
		{
			if(LHQueryDetailGrid.getSelNo() >= 1)
			{
				var ContNo = LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1);//保单号
		    	var strSql = "select distinct PrtNo from lccont where ContNo='"+ContNo+"'";
		    	var prtNo = easyExecSql(strSql);//印刷号
		    	var CustomerNo = LHQueryDetailGrid.getRowColData(LHQueryDetailGrid.getSelNo()-1,1);//客户号
	        	window.open("./LHContPlanSetting.jsp?ContNo="+ContNo+"&prtNo="+prtNo+"&CustomerNo="+CustomerNo+"","个人服务计划设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
       		}
			else
			{
				alert("请选择一条客户信息!");
			}
    	}
	    else
	    {
	    	alert("请选择一条保单信息!");
	    }
  	}
  	if(fm.all('ContType').value=="2")
	{
		if(LHSettingSlipQueryGrid.getSelNo() >= 1)
		{
			var ContNo = LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1);//保单号
	     	window.open("./LHGroupPlanSetting.jsp?ContNo="+ContNo+"","团体服务计划设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
    	}
    	else
    	{
    		alert("请选择一条保单信息!");
    	}
	}
}
function QueryQieYueInfo()
{
	if(fm.all('ServCaseStutes').value != "")
	{
		if(fm.all('ServCaseStutes').value=="1")//未和事件关联表建立关联
		{
			fm.all('CaseShiShi').disabled=false;
			fm.all('AddItemAll').disabled=false;
			var ServItemName="";
  	        if(fm.all('ServItemName').value != "")
  	        {
  	        	  ServItemName =" and a.ServItemCode in ( select distinct ServItemCode from  LHHealthServItem  where  ServItemName = '"+fm.all('ServItemName').value+"')";	
  	        }
  	        var Riskcode="";
  	        if(fm.all('Riskcode').value != "")
  	        {
  	        	  Riskcode =" and a.ServPlanNo in ( select distinct s.Servplanno from  lhservplan s  where  s.Servplancode = '"+fm.all('Riskcode').value+"')";	
  	        }
  	        var Riskname="";
  	        if(fm.all('Riskname').value != "")
  	        {
  	        	  Riskname =" and a.ServPlanNo in ( select distinct s.Servplanno from  lhservplan s  where  s.ServPlanName = '"+fm.all('Riskname').value+"')";	
  	        }
  	        
  	        var FeeCharge = "";
  	        if(fm.all('ServCaseType').value == "3")
  	        {
  	        	FeeCharge = " ( a.ServItemNo not  in ( select b.ServItemNo from  LHServCaseRela b , LHServCaseDef a where a.ServCaseCode = b.ServCaseCode and a.ServCaseType = '3' ) ) ";
  	        }
  	        else
  	        {
  	        	FeeCharge = " ( a.ServItemNo not  in ( select b.ServItemNo from  LHServCaseRela b , LHServCaseDef a where a.ServCaseCode = b.ServCaseCode and a.ServCaseType in ('1','2') ) ) ";
  	        }
  	      
	  	    var tServCaseCode="";
	  	    if(fm.all('tServCaseCode').value != "")
	  	    {
	  	    	  tServCaseCode =" and a.ServItemNo in ( select distinct m.ServItemNo from  LHServCaseRela m ,LHServCaseDef n where m.ServCaseCode=n.ServCaseCode and n.ServCaseCode = '"+fm.all('tServCaseCode').value+"')";	
	  	    }
	  	    var tServCaseName="";
	  	    if(fm.all('tServCaseName').value != "")
	  	    {
	  	    	  tServCaseName =" and a.ServItemNo in ( select distinct m.ServItemNo from  LHServCaseRela m ,LHServCaseDef n where m.ServCaseCode=n.ServCaseCode and n.ServCaseName = '"+fm.all('tServCaseName').value+"')";	
	  	    }
	        var strSql=" select distinct a.CustomerNo ,a.Name, a.ContNo, a.ServItemCode ,"
	                  +" (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode),"
	                  +" a.servitemtype, '已结束', a.ComID, '', a.servpricecode ,"
	                  +" (select b.servpricename from lhserveritemprice b where  b.servpricecode = a.servpricecode),"
	                  +" a.ServItemNo ,a.servplanno , "
	                  +"(select b.StartDate from LHservPlan b where b.servplanno=a.servplanno),"
	                  +"(select b.EndDate from LHservPlan b where b.servplanno=a.servplanno)"
	                  +"from lhservitem a  where "
					          +" a.ServItemNo not in ( select distinct b.ServItemNo from  LHServCaseRela b where b.ServCaseCode = '"+fm.all('ServCaseCode').value+"')"
					          //+" and a.ServItemNo in ( select distinct r.servitemno from lhservcaserela r, lhservcasedef d where  r.servcasecode = d.servcasecode and d.servcasestate  in ('2','3'))"
					          +" and a.ServItemNo not in ( select distinct r.servitemno from lhservcaserela r, lhservcasedef d where  r.servcasecode = d.servcasecode and d.servcasestate in ('0','1'))"
                    +getWherePart("a.CustomerNo","CustomerNo")  
                    +getWherePart("a.Name","CustomerName")
                    +getWherePart("a.ServItemCode","ServItemCode" )
                    +getWherePart("a.ContNo","ContNo")
                    +getWherePart("a.ComID","MngCom")
                    +ServItemName
                    +Riskcode
                    +Riskname
                    +tServCaseCode
                    +tServCaseName
                    ;
	        //alert(strSql);
	        fm.all('ResultSql').value=strSql;
	        //alert(easyExecSql(strSql));
	        turnPage.queryModal(strSql, LHSettingSlipQueryGrid);
		}
		if(fm.all('ServCaseStutes').value=="2")//在和事件关联表建立关联
		{
			 fm.all('CaseShiShi').disabled=true;
			 fm.all('AddItemAll').disabled=true;
			 var ServItemName="";
	  	     if(fm.all('ServItemName').value != "")
	  	     {
	  	     	  ServItemName =" and a.ServItemCode in ( select distinct ServItemCode from  LHHealthServItem  where  ServItemName = '"+fm.all('ServItemName').value+"')";	
	  	     }
	  	     var Riskcode="";
	  	     if(fm.all('Riskcode').value != "")
	  	     {
	  	     	  Riskcode =" and a.ServPlanNo in ( select distinct s.Servplanno from  lhservplan s  where  s.Servplancode = '"+fm.all('Riskcode').value+"')";	
	  	     }
	  	     var Riskname="";
	  	     if(fm.all('Riskname').value != "")
	  	     {
	  	     	  Riskname =" and a.ServPlanNo in ( select distinct s.Servplanno from  lhservplan s  where  s.ServPlanName = '"+fm.all('Riskname').value+"')";	
	  	     }
	  	     var ServCaseCode="";
	  	     if(fm.all('ServCaseCode').value != "")
	  	     {
	  	     	  ServCaseCode =" and ( a.ServItemNo  in ( select b.ServItemNo from  LHServCaseRela b where b.ServCaseCode='"+fm.all('ServCaseCode').value +"' )) ";	
	  	     }
	  	    var tServCaseCode="";
	  	     if(fm.all('tServCaseCode').value != "")
	  	     {
	  	     	  tServCaseCode =" and a.ServItemNo in ( select distinct m.ServItemNo from  LHServCaseRela m ,LHServCaseDef n where m.ServCaseCode=n.ServCaseCode and n.ServCaseCode = '"+fm.all('tServCaseCode').value+"')";	
	  	     }
	  	     var tServCaseName="";
	  	     if(fm.all('tServCaseName').value != "")
	  	     {
	  	     	  tServCaseName =" and a.ServItemNo in ( select distinct m.ServItemNo from  LHServCaseRela m ,LHServCaseDef n where m.ServCaseCode=n.ServCaseCode and n.ServCaseName = '"+fm.all('tServCaseName').value+"')";	
	  	     }
	       	 var strSql=" select distinct a.CustomerNo ,a.Name,  a.ContNo, a.ServItemCode ,"
	                 +" (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode),"
	                 +" a.servitemtype , '未结束' , a.ComID , '', a.servpricecode ,"
	                 +" (select b.servpricename from lhserveritemprice b where  b.servpricecode = a.servpricecode),"
	                 +" a.ServItemNo ,a.servplanno ,"
	                 +"(select b.StartDate from LHservPlan b where b.servplanno=a.servplanno),"
	                 +"(select b.EndDate from LHservPlan b where b.servplanno=a.servplanno)"
	                 +" from lhservitem a  where  1=1 "
                     +getWherePart("a.CustomerNo","CustomerNo")  
                     +getWherePart("a.Name","CustomerName")
                     +getWherePart("a.ServItemCode","ServItemCode" )
                     +getWherePart("a.ContNo","ContNo")
                     +ServItemName
                     +Riskcode
                     +Riskname
                     +ServCaseCode
                     +tServCaseCode
                     +tServCaseName
        //alert(strSql);
        //alert(easyExecSql(strSql));
        turnPage.queryModal(strSql, LHSettingSlipQueryGrid);
		}
	}
	else
	{
		var ServItemName="";
  	 	if(fm.all('ServItemName').value != "")
  	 	{
  	 		  ServItemName =" and a.ServItemCode in ( select distinct ServItemCode from  LHHealthServItem  where  ServItemName = '"+fm.all('ServItemName').value+"')";	
  	 	}
  	 	var Riskcode="";
  	 	if(fm.all('Riskcode').value != "")
  	 	{
  	 		  Riskcode =" and a.ServPlanNo in ( select distinct s.Servplanno from  lhservplan s  where  s.Servplancode = '"+fm.all('Riskcode').value+"')";	
  	 	}
  	 	var Riskname="";
  	 	if(fm.all('Riskname').value != "")
  	 	{
  	 		  Riskname =" and a.ServPlanNo in ( select distinct s.Servplanno from  lhservplan s  where  s.ServPlanName = '"+fm.all('Riskname').value+"')";	
  	 	}

	    var strSql=" select distinct a.CustomerNo ,a.Name,   "
	    		  +"    a.ContNo,"
	    		  +" a.ServItemCode ,"
	    		  +" (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode),"
	    		  +"  a.servitemtype ,"
	    		  +"  '已结束' ,"
	    		  +" a.ComID , '',"
	    		  +"    a.servpricecode ,"
	    		  +"  (select b.servpricename from lhserveritemprice b where  b.servpricecode = a.servpricecode),"
	    		  +" a.ServItemNo ,a.servplanno ,"
	    		  +"(select b.StartDate from LHservPlan b where b.servplanno=a.servplanno),"
	          +"(select b.EndDate from LHservPlan b where b.servplanno=a.servplanno)"
	    		  +" from lhservitem a "
            +" where "
            +" (a.ServItemNo not  in ( select b.ServItemNo from  LHServCaseRela b ) ) and 1=1"
            +getWherePart("a.CustomerNo","CustomerNo")  
            +getWherePart("a.Name","CustomerName")
            +getWherePart("a.ServItemCode","ServItemCode" )
            +getWherePart("a.ContNo","ContNo")
            //+getWherePart("a.ServPlanNo","Riskcode")
            +ServItemName
            +Riskcode
            +Riskname
        //alert(strSql);
        //alert(easyExecSql(strSql));
        turnPage.queryModal(strSql, LHSettingSlipQueryGrid);
   }
}
function AddNewHmPlan()
{
	window.open("./LHAddNewHmPlanMain.jsp","新增服务事件设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}
function AddNewQuery()
{
	window.open("./LHAddNewHmPlanQuery.html","新增服务事件查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}
function showTwo()
{
	var getSelNo = LHSettingSlipQueryGrid.getSelNo();
	var ContNo = LHSettingSlipQueryGrid.getRowColData(getSelNo-1, 3);
	var ServPlanNo = LHSettingSlipQueryGrid.getRowColData(getSelNo-1, 13);
	var ComID = LHSettingSlipQueryGrid.getRowColData(getSelNo-1, 8);
	var ServItemNo = LHSettingSlipQueryGrid.getRowColData(getSelNo-1, 12);
	var ServItemCode = LHSettingSlipQueryGrid.getRowColData(getSelNo-1, 4);
	var CustomerNo = LHSettingSlipQueryGrid.getRowColData(getSelNo-1, 1);
	var CustomerName = LHSettingSlipQueryGrid.getRowColData(getSelNo-1, 2);
	fm.all('ContNo2').value=ContNo;
	fm.all('ServPlanNo2').value=ServPlanNo;
	fm.all('ComID2').value=ComID;
	fm.all('ServItemNo2').value=ServItemNo;
	fm.all('ServItemCode2').value=ServItemCode;
	fm.all('CustomerNo2').value=CustomerNo;
	fm.all('CustomerName2').value=CustomerName;
	var strSql="select c.ServPlanName, c.ServPlanLevel from LHServPlan c where  c.ServPlanNo='"+fm.all('ServPlanNo2').value+"'";
	var Result=easyExecSql(strSql);
	//alert(Result);
	if(Result!=""&&Result!=null&&Result!=null)
	{
	  fm.all('ServPlanName2').value=Result[0][0];
	  fm.all('ServPlanLevel2').value=Result[0][1];
	}
}
function AddServCaseOld()
{
	var arrSelected = null;
	arrSelected = new Array();
	var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
		if(tChk == true)
		{
			aa[xx++] = row;	
		}
	}

	if(aa.length=="0"||aa.length=="null"||aa.length==null||aa.length=="")
	{
		 alert("请选择要添加的服务事件的客户项目信息!");
		 return false;
	}
	if(aa.length>="1")
	{
		   var i = 0;
       var showStr="正在添加数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
       showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
       fm.all('fmtransact').value = "INSERT||MAIN";
       fm.action="./LHServQieYueInfoSave.jsp";
       fm.submit(); //提交
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) 
{
	showInfo.close();
	if (FlagStr == "Fail" ) 
	{
    	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else 
  {
  	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  	 // fm.all('AddItemAll').disabled=true;
  }
}
function AddServCase()
{
	if( verifyInput2() == false ) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./LHServQieYueAddEventSave.jsp";
  	fm.all('fmtransact').value = "INSERT||MAIN";
  	fm.submit(); //提交
}
function QueryServCase()
{
	window.open("./LHSettingQueryMain.jsp?Come=LHGrpServInfoCharge","","width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes");
}
function afterQuery( arrQueryResult )
{
	fm.all('ServCaseTypeName').ondblclick = "";
	fm.all('ServCaseStateName').ondblclick = "";
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		var sql="select ServCaseType,ServCaseCode,ServCaseName,ServCaseState from LHServCaseDef where ServCaseCode='"+arrQueryResult[0][0]+"'";
		arrResult = easyExecSql(sql); 
		fm.all('ServCaseType').value = arrResult[0][0];
		if (arrResult[0][0]==1)
		{
			fm.all('ServCaseTypeName').value = "个人服务事件";
		}
		else
		{ 
			    fm.all('ServCaseTypeName').value = "集体服务事件";
        	fm.all('ServCaseCode').value = arrResult[0][1];
        	fm.all('ServCaseName').value = arrResult[0][2];
        	fm.all('ServCaseState').value = arrResult[0][3];
        	fm.all('ServCaseStateName').value = easyExecSql("select a.codename from ldcode  a where a.codetype='eventstate' and  a.code='"+fm.all('ServCaseState').value+"'");
    }
	}
	//alert(fm.all('ServCaseState').value);
	if(fm.all('ServCaseState').value=="0")//对于查询返回的未启动事件可再关联，否则不允许再关联事件
	{
		  fm.all('CaseShiShi').disabled=false;
	    fm.all('AddItemAll').disabled=false; 
	    fm.all('ServCaseStutes').readOnly = false;
	}
	else
	{
		  fm.all('CaseShiShi').disabled=true;  
	    fm.all('AddItemAll').disabled=true; 
	    fm.all('ServCaseStutesName').ondblclick = "";
	    fm.all('ServCaseStutes').readOnly = true;
	}
}
function AddAllCaseInfo()
{
	if (confirm("您确认要添加所有服务项目信息吗?"))
  {
	     var i = 0;
       var showStr="正在添加数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
       showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
       fm.all('fmtransact').value = "INSERT||MAIN";
       fm.action="./LHServQieYueAllSave.jsp";
       fm.submit(); //提交
   }
   else
   {
     alert("您取消了添加所有服务项目信息的操作！");
     return false;
   }
}