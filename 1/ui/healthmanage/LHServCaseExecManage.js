//该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var StrSql;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHSettingSlipQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    //showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
          
//Click事件，当点击增加图片时触发该函数
  

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
            
       
function showQueryInfo()
{      
	var strSql=" select b.ServCaseCode ,a.ServCaseName, "
	          +"(case a.ServCaseType when '1' then '个人服务事件' when '2' then '集体服务事件' else '无' end ),"
	          +" (case a.ServCaseState when '0' then '未进行启动设置' when '1' then '待其他事件完成' when '1+' then '等待事件已完成'when '2' then '服务事件已启动'  when '3' then '服务事件已完成' when '4' then '服务事件失败' else '无' end ) ,"
	          +" a.ServCaseType , "
	          +" a.ServCaseState"
	          +" from LHServCaseRela b  ,LHServCaseDef a "
              +" where a.ServCaseCode=b.ServCaseCode and "
              +"  1=1 "
              +getWherePart("b.ContNo","ContNo","like" )  
              +getWherePart("b.CustomerNo","CustomerNo")
              +getWherePart("b.CustomerName","CustomerName" )
              +getWherePart("b.ServItemCode","ServItemCode")
              +getWherePart("b.ServCaseCode","ServCaseCode")
              +getWherePart("a.ServCaseName","ServCaseName" )
              +getWherePart("a.ServCaseType","ServCaseType")
              +getWherePart("a.ServCaseState","ServCaseState")
         //alert(strSql);
         //alert(easyExecSql(strSql));
         turnPage.queryModal(strSql, LHSettingSlipQueryGrid);
}

function showContPlan()
{
	if(fm.all('ContType').value==""||fm.all('ContType').value=="null"||fm.all('ContType').value==null)
	{
		alert("请选择保单类型!");
	}
	if(fm.all('ContType').value=="1")
	{
		if(LHSettingSlipQueryGrid.getSelNo() >= 1)
		{
		   if(LHQueryDetailGrid.getSelNo() >= 1)
		   {
		      var ContNo = LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1);//保单号
		      var strSql = "select distinct PrtNo from lccont where ContNo='"+ContNo+"'";
		      var prtNo = easyExecSql(strSql);//印刷号
		      var CustomerNo = LHQueryDetailGrid.getRowColData(LHQueryDetailGrid.getSelNo()-1,1);//客户号
	        window.open("./LHContPlanSetting.jsp?ContNo="+ContNo+"&prtNo="+prtNo+"&CustomerNo="+CustomerNo+"","个人服务计划设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
       }
       else
       {
       	 alert("请选择一条客户信息!");
       }
    }
    else
    {
    	alert("请选择一条保单信息!");
    }
  }
  if(fm.all('ContType').value=="2")
	{
		if(LHSettingSlipQueryGrid.getSelNo() >= 1)
		{
		   var ContNo = LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1);//保单号
	    window.open("./LHGroupPlanSetting.jsp?ContNo="+ContNo+"","团体服务计划设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
    }
    else
    {
    	alert("请选择一条保单信息!");
    }
  }
}
function ServQieYueInfo()
{
	if (LHSettingSlipQueryGrid.getSelNo() >= 1)
	{     // alert(LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1));
		if(LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1)=="")
		{
			alert("你选择了空的列，请重新选择!");
			return false;
		}
		else
		{
			var ServCaseCode = LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1);
			var EventCode=ServCaseCode;
            var caseCode=EventCode.toString().substring(8);
            var code=caseCode.toString().substring(0,1);
            if(code=="2")
            {
				window.open("./LHServQieYueInfoMain.jsp?ServCaseCode="+ServCaseCode+"&flag=1","服务契约信息管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			}
			if(code=="1")
			{
				alert("此事件类型不能进行服务契约信息管理!");
			}
		}
	} 
	else
	{
		alert("请选择一条要传输的记录！");    
	}
}
function showOne()
{	
	var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
		if(tChk == true)
		{
			aa[xx++] = row;	
//			if(LHSettingSlipQueryGrid.getRowColData(row,6)=="0")
//			{
				//alert("您选择的事件存在未启动事件!");
//				divShowServTaskInfo.style.display='';
//				divShowServTaskInfo2.style.display='none';
//				return false;
//			}            
		}  
	}

	if(aa.length=="0")
	{
		divShowServTaskInfo.style.display='none';
		divShowServTaskInfo2.style.display='none';
	}
	//alert(aa.length);
	if(aa.length=="1")
	{
		divShowServTaskInfo.style.display='';
		divShowServTaskInfo2.style.display='';
	   	var ServCaseCode =LHSettingSlipQueryGrid.getRowColData(aa,2); 
	   	fm.all('ServCaseCode').value=ServCaseCode; 
	   	//alert(fm.all('ServCaseCode').value);
	   	var strSql="select a.ServCaseState from LHServCaseDef  a  where a.ServCaseCode='"+ServCaseCode+"'";
	   	//alert(strSql);
	   	var ServTaskState=easyExecSql(strSql);
	   	//alert(ServTaskState);
	   	if(ServTaskState=="2"||ServTaskState=="3")
	   	{
         fm.all('changeCaseState').disabled=true;
	   	}
	   	if(ServTaskState=="0"||ServTaskState=="1")
	   	{
         fm.all('changeCaseState').disabled=false;
	   	}
	   	

	   	var sql="select a.ServCaseCode from LHCaseTaskRela  a  where a.ServCaseCode='"+ServCaseCode+"'";
	   	var code=easyExecSql(sql);

	   	if(code==null||code=="null"||code=="")
	   	{
	   	    var ServCaseType =LHSettingSlipQueryGrid.getRowColData(aa,6);//事件类型
	   	    var ComID =LHSettingSlipQueryGrid.getRowColData(aa,7);//事件类型
	   	    var ServItemCode =LHSettingSlipQueryGrid.getRowColData(aa,8);//事件类型

	   	    var d = new Date();
	        var h = d.getYear();
	        var m = d.getMonth(); 
	        var day = d.getDate();  
	        var Date1;       
	        if(h<10){h = "0"+d.getYear();}  
	        if(m<9){ m++; m = "0"+m;}
	        else{m++;}
	        if(day<10){day = "0"+d.getDate();}
	        Date1 = h+"-"+m+"-"+day;
           //根据表可以自动为一个事件生成任务，目前没有用到。08/05/07
	   	    var strSql=" select a.ServTaskCode,"
	   	              +"  a.ServTaskName,"
	                  +"  (select d.codename from ldcode  d where d.codetype='taskstatus' and d.code=a.TaskState),"
	                  +"  '未确认',"
	                  +"  a.Operator,"
	                  +" (select Username from lduser where Usercode=a.Operator),  "
	                  +"  a.PlanExeTime,  "
	                  +" '', "
	                  +"  a.TaskDes , "
	                  +" a.TaskState,"
	                  +" '1', "
	                  +" SerialNo "
                      +" from LHServTaskRela a "
                      +" where ServCaseType='"+ServCaseType+"'and  " 
                      +" ComID='"+ComID+"' and ServItemCode='"+ServItemCode+"'  and "
                      +"  1=1 "
			//alert(strSql);
			//alert(easyExecSql(strSql));
			turnPage.queryModal(strSql, LHQueryDetailGrid); 
		}
        else
        {
        	var strSql=" select ServTaskCode,"
	   	              +" (select b.ServTaskName from  LHServTaskDef b where b.ServTaskCode=a.ServTaskCode),"
	                  +" (case a.ServTaskState when '1' then '等待执行' when '2' then '正在执行' when '3' then '任务完成' when '4' then '任务失败' when '5' then '任务取消' else '无' end ),"
	                  +" (case a. ServTaskAffirm when '1' then '未确认' when '2' then '已确认' else '无' end ),"
	                  +" ExecuteOperator,"
	                  +"(select Username from lduser where Usercode=a.Operator), "
	                  +" PlanExeDate, "
	                  +" TaskFinishDate, "
	                  +" TaskDesc ,"
	                  +" ServTaskState,"
	                  +" ServTaskAffirm,"
	                  +" ServTaskNo "
                      +" from LHCaseTaskRela a "
                      +" where ServCaseCode='"+ServCaseCode+"'and  " 
                      +"  1=1 "
                      //alert(strSql);
                      //alert(easyExecSql(strSql));
			turnPage.queryModal(strSql, LHQueryDetailGrid);
		}
	}
	if(aa.length>"1")
	{
		 divShowServTaskInfo.style.display='none';
		 divShowServTaskInfo2.style.display='';
	}
	
}
//提交，保存按钮对应操作
function submitForm()
{
	      //var sqlContrano="select ServTaskCode from LHServTaskDef where ServTaskCode='"+fm.all('ContraNo').value+"'";
        ////alert(sqlContrano);
        //// alert(easyExecSql(sqlContrano));
        //contrano=easyExecSql(sqlContrano);
        //if(fm.all('ContraNo').value==contrano)
        //{
    	  //  alert("此合同编号的信息已经存在,不允许重复保存!");
        //}
        //if(contrano==null||contrano==""||contrano=="null")
        //{
           var i = 0;
           fm.fmtransact.value="INSERT||MAIN";
           var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
           var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
           showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
           fm.action="./LHServCaseExecManageSave.jsp";
           fm.submit(); //提交
        //}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}
function initTaskInfo()
{
	  var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	   var xx = 0;
	   for(var row=0; row < rowNum; row++)
	   {
	         var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
	         if(tChk == true)
	         {
	         	   aa[xx++] = row;	
	         	}
	    }
	     var ServCaseCode =LHSettingSlipQueryGrid.getRowColData(aa,2);
	       var strSql=" select ServTaskCode,"
	   	               +"  (select b.ServTaskName from  LHServTaskDef b where b.ServTaskCode=a.ServTaskCode),"
	                   +"  (case a.ServTaskState when '1' then '等待执行' when '2' then '正在执行' when '3' then '任务完成' when '4' then '任务失败' when '5' then '任务取消' else '无' end ),"
	                   +"  (case a. ServTaskAffirm when '1' then '未确认' when '2' then '已确认' else '无' end ),"
	                   +" ExecuteOperator,"
	                   +" (select Username from lduser where Usercode=a.Operator), "
	                   +"  PlanExeDate,  "
	                   +" TaskFinishDate, "
	                   +"  TaskDesc ,"
	                   +"  ServTaskState,"
	                   +"  ServTaskAffirm,"
	                   +"  ServTaskNo "
                     +" from LHCaseTaskRela a "
                     +" where ServCaseCode='"+ServCaseCode+"'and  " 
                     +"  1=1 "
           //alert(strSql);
           //alert(easyExecSql(strSql));
          turnPage.queryModal(strSql, LHQueryDetailGrid);
//         var strSql2=" select ServTaskCode,"
//	   	               +" (select b.ServTaskName from  LHServTaskDef b where b.ServTaskCode=a.ServTaskCode),"
//	                   +" (case a.ServTaskState when '1' then '等待执行' when '2' then '正在执行' when '3' then '任务完成' when '4' then '任务失败' when '5' then '任务取消' else '无' end ),"
//	                   +" (case a. ServTaskAffirm when '1' then '未确认' when '2' then '已确认' else '无' end ),"
//	                   +" ExecuteOperator,"
//	                   +"(select Username from lduser where Usercode=a.Operator), "
//	                   +" PlanExeDate, "
//	                   +" TaskFinishDate, "
//	                   +" TaskDesc, "
//	                   +" ServTaskState,"
//	                   +" ServTaskAffirm,"
//	                   +" ServTaskNo "
//                     +" from LHCaseTaskRela a "
//                     +" where ServCaseCode='"+ServCaseCode+"'and  " 
//                     +"  1=1 "
//           //alert(strSql);
//           //alert(easyExecSql(strSql));
//          turnPage.queryModal(strSql2, LHQueryDetailInfoGrid);
}

function checkAffirm()
{
	var rowNum=LHQueryDetailGrid.mulLineCount ; //行数 	
	for (var i = 0; i < rowNum; i++)
	{
		if(LHQueryDetailGrid.getRowColData(i,10) == "1" || LHQueryDetailGrid.getRowColData(i,10) == "2")	
		{
			if(LHQueryDetailGrid.getRowColData(i,11) == "2")	
			{
				alert("服务任务状态为 ‘等待执行’ 或 ‘正在执行’ 时，不能进行确认操作!");	
				showOne();
				return false;
				
			}
		}
	}
}

function updateClick()
{
	//对任务状态及确认状态的校验
	if(checkAffirm()==false)
	return false;
	         
	//	         
	var rowNum=LHSettingSlipQueryGrid.mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
	      var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
	      if(tChk == true)
	      {
	      	   aa[xx++] = row;	
	      }
	}
    
    var ServCaseCode =LHSettingSlipQueryGrid.getRowColData(aa,2);  
	var sql="select a.ServCaseCode from LHCaseTaskRela  a  where a.ServCaseCode='"+ServCaseCode+"'";
	var code=easyExecSql(sql);
	//alert(code);
	if(code=="null"||code==null|code==null)//事件号在关联表中不存在
	{
		var i = 0;
        fm.fmtransact.value="INSERT||MAIN";
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.action="./LHServCaseExecManageUpdate.jsp";
        fm.submit(); //提交
	}
	else
	{
    	if (confirm("您确实想修改该记录吗?"))
    	{
              var i = 0;
              var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
              var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
              showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

              fm.fmtransact.value = "UPDATE||MAIN";
              fm.action="./LHServCaseExecManageUpdate.jsp";
              fm.submit(); //提交
		}
        else
        {
          alert("您取消了修改操作！");
        }  
    }
}  
function deleteClick()
{
	//if (LHQueryDetailGrid.getSelNo() >= 1)
	//{    
		//var TaskState=LHQueryDetailGrid.getRowColData(LHQueryDetailGrid.getSelNo()-1,10);
		//if(TaskState=="1"||TaskState=="5")
		//{
        	if (confirm("您确实想删除该记录吗?"))
			    {
                var i = 0;
                var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
                var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
                showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

                fm.fmtransact.value = "DELETE||MAIN";
                fm.action="./LHServCaseExecManageDelete.jsp";
                fm.submit(); //提交
                initForm();
			    }
          else
          {
              alert("您取消了删除操作！");
          }
		//}
		//else
		//{
		//	  alert("此任务状态的信息记录不允许删除!");
		//}
	//}
	//else
	//{
	//	  alert("请选择一条删除的记录！");
	//}

}  

//修改事件状态按钮
function changeCaseStateFun()
{
	var rowNum=LHSettingSlipQueryGrid.mulLineCount ; //行数 	
	var RowNo = new Array();
	var i = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
		if(tChk == true)
		{
			RowNo[i++] = row+1;	
			
			if(LHSettingSlipQueryGrid.getRowColData(row,5) == "0" || LHSettingSlipQueryGrid.getRowColData(row,5) == "1")
			{
				alert("第"+(row+1)+"行事件状态无法更改，需先将事件状态设置为'已完成'或'失败'状态");
				aa();	
				return false;
			}
			if(checkTaskState(row) == false)
			{
				alert("第"+(row+1)+"行事件存在未完成的任务，请确认");	
				aa();
				return false;
			}
		}
	}
	
	if(RowNo.length == 0)
	{
		alert("请先选择服务事件信息");
		return false;	
	}
	if (confirm("确实要修改第"+RowNo+"行事件的状态吗?"))
	{                          
        var showStr="正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
                               
//        fm.fmtransact.value = "DELETE||MAIN";
        fm.action="./LHServCaseExecManage_CaseStateSave.jsp";  
        fm.submit(); 
	}                          
    else                       
    {                          
		alert("您取消了修改操作！");
    }
}

function afterChangeCaseStateFun(FlagStr,content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  fm.all('changeCaseState').disabled=true;
	  if (confirm("您是否要生成新的服务事件?"))
		{
			//window.open("./LHServCaseAddNewCaseMain.jsp","新增实施服务事件设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			//window.open("./LHGrpServSetMain.jsp","新增实施服务事件设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	 		   var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数 
	       var aa = new Array();
		     var xx = 0;
		     for(var row=0; row < rowNum; row++)
		     {
		    	   var tChk =LHSettingSlipQueryGrid.getChkNo(row); 	
		    			if(tChk == true)
		    			{
		    					aa[xx++] = row;
		    		  }
		     }
	       if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		    	{
		    		alert("请选择要传输的事件信息！");
		    		return false;
		    	}		
	       if(aa.length >"1")
		      {
		      	alert( "请选择一条事件信息!");
		      	return false;
		      } 
	       if (aa.length=="1")
		     {   
		    	  var ServCaseCode =LHSettingSlipQueryGrid.getRowColData(aa,2);
			      //alert(ServCaseCode);
			    	window.open("./LHServQieYueInfoMain.jsp?ServCaseCode="+ServCaseCode+"&flag=2","新增实施服务事件设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			    
		     }
	 	}
		else
		{
			alert("您取消了生成新事件的操作!");
		}
	}
}

function checkTaskState(row)
{
	var sql = "select servtaskno from LHCaseTaskRela where  servcasecode = '"+
			  LHSettingSlipQueryGrid.getRowColData(row,2)+"' and servtaskaffirm = '1'";
	var arr = new Array();	
	arr = easyExecSql(sql);
	
	if(arr != null && arr != "null")
	{
		return false;
	}
}
function showTaskInfo()
{
		var arrSelected = null;
	  arrSelected = new Array();
	  var rowNum=LHQueryDetailGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHQueryDetailGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx] = row;
	        	   
	            arrSelected[xx++] = LHQueryDetailGrid.getRowColData(aa,12);
	            //alert("AA "+arrSelected);
	        }
	  }
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要传输的事件信息！");
			  return false;
		}	
		//alert(aa.length);
		var caseCode=fm.all('ServCaseCode').value;
		if(aa.length=="1")
		{
			    
			    //alert(caseCode);
	        window.open("./LHServiceManageMain.jsp?caseCode="+caseCode+"&arrSelected="+arrSelected+"&flag=1","一般服务任务管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
		    
		}
		if(aa.length>"1")
		{
			alert("您只能对一条服务任务信息进行实施管理!");
			return false;
		}
}
