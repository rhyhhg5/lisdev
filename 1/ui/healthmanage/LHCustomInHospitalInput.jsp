<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-18 09:08:27
//创建人  ：CrtHtml程序创建
//更新记录：  更新人  	St.GN  更新日期   2005-4-4 9:37  更新原因/内容 重新布局
%>
<%@page contentType="text/html;charset=GBK" %>

<%
	String tCustomerno = "";
	String tInhospitNo = "";
	
	try
	{
		tCustomerno = request.getParameter("Customerno");
		tInhospitNo = request.getParameter("InhospitNo");
		System.out.println(tCustomerno+"----"+tInhospitNo);
	}
	catch( Exception e )
	{
		tCustomerno = "";
		tInhospitNo = "";
		System.out.println(e);
	}
%>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHCustomInHospitalInput.js"></SCRIPT>

  <%@include file="LHCustomInHospitalInputInit.jsp"%>
</head>
<body  onload="initForm();passQuery_test();initElementtype();" >
  <form action="./LHCustomInHospitalSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital1);">
    		</td>
    		<td class= titleImg>
        		 一般信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomInHospital1" style= "display: ''">
      <table  class= common align='center' style= "width: '94%'">
          <TR  class= common>
           <TD  class= title>
              客户号码
           </TD>
           <TD  class= input>
             <Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=24" ondblclick="return queryCustomerNo();">
           </TD>
           <TD  class= title>
              客户姓名
           </TD>
           <TD  class= input>
             <Input class= 'common' name=CustomerName  >
           </TD>
           <TD  class= title>
           	  就诊时间
           </TD>
           <TD  class= input>
           	   <Input class= 'coolDatePicker' dateFromat="Short" name=InHospitDate verify="就诊时间|DATE">
           </TD>
          </TR>
          <TR  class= common>           
            <TD  class= title>
               医疗机构名称
            </TD>
            <TD  class= input8>
              <Input class= 'codeno' name=HospitCode  ondblclick="return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');"  onkeyup=" if(event.keyCode ==13) return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName')	; codeClear(HospitName,HospitCode); " onkeydown="  return showCodeListKey('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName')	; codeClear(HospitName,HospitCode); " verify="医疗机构名称|len<=60"><Input class= 'codename'  name=HospitName >
            </TD>
            <TD  class= title>
               主治医师姓名
            </TD>
            <TD  class= input>
            	<Input  class=codeno name=DoctNo ondblclick="return showCodeList('lhdoctname',[this,DoctName],[1,0],null,fm.DoctName.value,'DoctName');" onkeyup=" if(event.keyCode ==13) return showCodeList('lhdoctname',[this,DoctName],[0,1],null,fm.DoctName.value,'DoctName');  codeClear(DoctName,DoctNo); " onkeydown=" return showCodeListKey('lhdoctname',[this,DoctNo],[1,0],null,fm.DoctName.value,'DoctName');  codeClear(DoctName,DoctNo); " verify="诊断医师姓名|len<=20"><Input class= 'codename' name=DoctName >
            </TD>
            <TD  class= title>
            	 就诊方式
            </TD>
            <TD  class= input>
            	<Input class=codeno name=InHospitModeCode ondblclick=" return showCodeList('hminhospitalmode',[this,InHospitMode],[0,1]);" onkeydown=" return showCodeListKey('hminhospitalmode',[this,InHospitMode],[0,1]);codeClear(InHospitMode,InHospitModeCode);"  verify="就诊方式代码|len<=10"><Input class= 'codename' name=InHospitMode >
            </TD>
          </TR>
          <TR  class= common>
            
            <TD  class= title>
                  主要治疗方式
            </TD>
            <TD  class= input>
            	 <Input class=codeno name="MainCureModeCode" ondblclick="return showCodeList('maincuremode',[this,MainCureMode],[1,0]);"  onkeydown="return showCodeListKey('maincuremode',[this,MainCureMode],[1,0]);"  verify="主要治疗方式|len<=10"><Input class= 'codename' name="MainCureMode" >
            </TD>
            <TD  class= title>
             	疾病转归
            </TD>
            <TD  class= input>
            	  <Input class=codeno  name="CureEffectCode" ondblclick="return showCodeList('diagnosecureeffect',[this,CureEffect],[0,1]);" onkeydown="return showCodeListKey('diagnosecureeffect',[this,CureEffect],[0,1]);" verify="疾病转归|len<=10"><Input class= 'codename'  name="CureEffect" >
            </TD>
            <TD  class= title>
              住院天数（天）
            </TD>
            <TD  class= input>
              <Input class= 'common' id=InHospitalDays name=InHospitalDays verify="住院天数（天）|len<=20">
            </TD>
            
          </TR>
    <TR  class= common>
  	<TD  class= title>
      主诉
    </TD>
    <TD  class= input colSpan=5>
      <textarea class= 'common'  name=MainItem verify="主诉|len<=5000" style="width:683px;height:50px" ></textarea>    
    </TD>
  </TR>
      </table>
    </Div>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital2);">
    		</td>
    		<td class= titleImg>
        		 诊断信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomInHospital2" style= "display: ''">
    	 
    	<table  class= common align='center'  >
      	<tr class=common>
      		<td width='18px'>
        	</td>
      		<td>
      		  <span id="spanDiagnoInfoGrid"></span>
          </td>
        </tr>  
         
      </table>
    
    </Div>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital3);">
    		</td>
    		<td class= titleImg>
        	检查信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomInHospital3" style= "display: ''">
       <div id="divTestResult"  style="display: none; position:absolute; slategray">
    	     <textarea name="textTestResult" cols="100%" rows="3" witdh=25% class="common" onkeydown="backTestResult();">
    	     </textarea>
    	     <td class=button width="10%" align=right>
						<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backTestResult2();">
					 </td>
			 </div>
      <table  class= common align='center'  >
      	<tr class=common>
      		<td width='18px'>
        	</td>
      		<td>
      		  <span id="spanTestInfoGrid"></span>
          </td>
        </tr>  
         
      </table>
    </Div>
 
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital4);">
    		</td>
    		<td class= titleImg>
        	手术信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomInHospital4" style= "display: ''">
      <div id="divOPSResult"  style="display: none; position:absolute; slategray">
    	     <textarea name="textOPSResult" cols="100%" rows="3" witdh=25% class="common" onkeydown="backOPSResult();">
    	     </textarea>
    	     <td class=button width="10%" align=right>
						<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backOPSResult2();">
					 </td>
			 </div>
      <table  class= common align='center'  >
      	<tr class=common>
      		<td width='18px'>
        	 		</td>
      		<td>
      		  <span id="spanOPSInfoGrid"></span>
          </td>
        </tr>  
         
      </table>
    </Div>
    

    <Div  id= "divLHCustomInHospital5" style= "display: 'none'">
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital5);">
    		</td>
    		<td class= titleImg>
        	其他治疗信息
       	</td>   		 
    	</tr>
    </table>
       <div id="divCureResult"  style="display: none; position:absolute; slategray">
    	     <textarea name="textCureResult" cols="100%" rows="3" witdh=25% class="common" onkeydown="backCureResult();">
    	     </textarea>
    	     <td class=button width="10%" align=right>
						<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backCureResult2();">
					 </td>
			 </div>
      <table  class= common align='center'  >
      	<tr class=common>
      		<td width='18px'>
        	 		</td>
      		<td>
      		  <span id="spanOtherCureInfoGrid"></span>
          </td>
        </tr>  
         
      </table>
    </Div>
   
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital6);">
    		</td>
    		<td class= titleImg>
        		 费用信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomInHospital6" style= "display: ''">
    	<table  class= common align='center'  >
      	<tr class=common>
      		<td width='18px'>
        	 		</td>
      		<td>
      		  <span id="spanFeeInfoGrid"></span>
          </td>
        </tr>
      </table> 
      <table class= common align='center'style= "width: '94%'" > 
        <TR  class= common>
            <TD  class= title>
              总费用（元）
            </TD>
            <TD  class= input>
              <Input class= 'common' name=TotalFee verify="总费用（元）|len<=20">
            </TD>
            <TD  class= title>
             
            </TD>
            <TD  class= input>
             
            </TD>
            <TD  class= title>
            	
            </TD>
            <TD  class= input>
            	
            </TD>
         </TR>
      </table>
      
    </Div>
    <div id="div1" style="display: 'none'">
      <table>
        <TR  class= common>
            <TD  class= title>
              操作员代码
            </TD>
            <TD  class= input>
              <Input class= 'common' name=Operator >
            </TD>
            <TD  class= title>
              入机日期
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeDate >
            </TD>
            <TD  class= title>
              入机时间
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeTime >
            </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最后一次修改日期
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyDate >
          </TD>
          <TD  class= title>
            最后一次修改时间
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyTime >
          </TD>
        </TR>      
      </table>
    </div>
    
  
    
     <input type=hidden id="fmtransact" name="fmtransact">
     <input type=hidden id="fmAction" name="fmAction">
     <Input type=hidden id="InHospitNo" name="InHospitNo" >  
     <input type=hidden id="iscomefromquery" name ="iscomefromquery">               
   
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

	<script>
//  通过综合查询进入此页面的处理
		var turnPage = new turnPageClass(); 
		function passQuery_test()
		{
//    如果不是综合查询进入则跳过此函数
		  if("<%=request.getParameter("Customerno")%>" == "" || "<%=request.getParameter("Customerno")%>" == "null" || "<%=request.getParameter("InhospitNo")%>" =="" || "<%=request.getParameter("InhospitNo")%>" =="null")
		  {
		  		return;
		  }
//    进入此函数
	 	  else
		  { 		  
				  var CustomerNo_s = "<%=request.getParameter("Customerno")%>";
				  var InhospitNo_s = "<%=request.getParameter("InhospitNo")%>";
	  		//alert(CustomerNo_s+"-----"+InhospitNo_s);
				
			  	if(CustomerNo_s!=null && InhospitNo_s!=null) 
			  	{
			  			try
							{
								var strSql="select a.CustomerNo,b.name, a.InHospitNo,a.Hospitcode, "
								+"(select d.hospitname from ldhospital d where d.hospitcode = a.Hospitcode),"
								+" a.InHospitDate,"
					      +" c.DoctNo,(select e.DoctName from LDDoctor e where e.DoctNo = c.DoctNo),"
					      +"a.InHospitMode,c.MainCureMode,c.DiagnoseNo,"
					//      +"(select c.CureEffect from LHDiagno c where c.customerno='"+CustomerNo_s+"' and c.InHospitNo='"+ InhospitNo_s+"'),"
					      +" c.CureEffect,a.InHospitalDays,a.MakeDate,a.MakeTime "
					      +" from LHCustomInHospital a, ldperson b,LHDiagno c  where 1=1  "
					      +" and a.CustomerNo=b.CustomerNo and a.InHospitNo = c.InHospitNo "
					//      +" and c.IsMainDiagno='1' "
					      +" and a.Customerno = c.Customerno and a.CustomerNo='"+CustomerNo_s+"' and a.InHospitNo='"+ InhospitNo_s+"' " 
					      //+getWherePart("a.CustomerNo","CustomerNo") 
					      //+getWherePart("c.CustomerNo","CustomerNo")
					      // +getWherePart("b.CustomerNo","CustomerNo")
					     ;
					      arrResult = easyExecSql(strSql);//alert(strSql);
							
						  }
						  catch(ex){  alert("LHCustomInHospitalInput.js->afterquery0出错");}
					      
					      
					      fm.all('CustomerNo').value= arrResult[0][0];
					      fm.all('CustomerName').value= arrResult[0][1];
					      fm.all('InHospitNo').value= arrResult[0][2];
					      fm.all('HospitCode').value= arrResult[0][3];
					      fm.all('HospitName').value= arrResult[0][4];
					      fm.all('InHospitDate').value= arrResult[0][5];
					      fm.all('DoctNo').value= arrResult[0][6];
					      fm.all('DoctName').value= arrResult[0][7];
					      fm.all('InHospitModeCode').value= arrResult[0][8];if(arrResult[0][8]=="1"){fm.all('InHospitMode').value="门诊";strsqlinit=" code like #1%# and ";initFeeInfoGrid();}else{fm.all('InHospitMode').value="住院";strsqlinit=" code like #2%# and ";initFeeInfoGrid();}
					      fm.all('MainCureModeCode').value= arrResult[0][9];
					      var tempsql="select codename from ldcode where codetype='maincuremode' and code='"+arrResult[0][9]+"'";
					      var curemodeResult=easyExecSql(tempsql);
					      if(curemodeResult!=null)
					      {
					      	fm.all('MainCureMode').value= curemodeResult[0][0];
					      }
					      //fm.all('MainCureModeCode').value= arrResult[0][10];
					      //fm.all('DiagnoseNo').value= arrResult[0][10];
					      fm.all('CureEffectCode').value= arrResult[0][11];fm.all('CureEffect').value= easyExecSql("select codename from ldcode where codetype='diagnosecureeffect' and code ='"+arrResult[0][11]+"'");
					      fm.all('InHospitalDays').value= arrResult[0][12];if(arrResult[0][8]=="2"){fm.InHospitalDays.style.display='';fm.CureEffect.style.display='';}else{fm.CureEffect.style.display='none';fm.InHospitalDays.style.display='none';}
					      fm.all('MakeDate').value= arrResult[0][13];
					      fm.all('MakeTime').value= arrResult[0][14];
					       
					      strSql="select a.ICDCode,b.ICDName, a.IsMainDiagno,c.CodeName,a.DiagnoseNo "         
					     +" from LHDiagno a, LDDisease b,LDCode c where 1=1  "            
					     +" and a.ICDCode=b.ICDCode and a.IsMainDiagno=c.Code and c.codetype='ismaindiagno'"
					     +getWherePart("a.CustomerNo","CustomerNo")                       
					     +getWherePart("a.InHospitNo","InHospitNo");                      
							  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
							  turnPage.queryModal(strSql, DiagnoInfoGrid);
							  
							   strSql="select b.MedicaItemName,a.MedicaItemCode, a.DoctNo,c.DoctName,a.TestDate,a.TestResult,a.ResultUnitNum,"
					     +" a.TestFeeAmount,a.TestNo from LHCustomTest a, LHCountrMedicaItem b,LDDoctor c where 1=1  "
					     +" and a.MedicaItemCode=b.MedicaItemCode and a.DoctNo=c.DoctNo "
					     +getWherePart("a.CustomerNo","CustomerNo") 
					     +getWherePart("a.InHospitNo","InHospitNo");
						  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
						  
						  turnPage.queryModal(strSql, TestInfoGrid);
						  
						   strSql="select b.ICDOPSName,a.ICDOPSCode, a.DoctNo,c.DoctName,a.ExecutDate,a.CureEffect,"
					     +" a.OPSFeeAmount,a.OPSNo from LHCustomOPS a, LDICDOPS b,LDDoctor c where 1=1  "
					     +" and a.ICDOPSCode=b.ICDOPSCode and a.DoctNo=c.DoctNo "
					     +getWherePart("a.CustomerNo","CustomerNo") 
					     +getWherePart("a.InHospitNo","InHospitNo");
						  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
						  
						  turnPage.queryModal(strSql, OPSInfoGrid);
						  
						    
						   strSql="select b.MedicaItemName,a.MedicaItemCode, a.DoctNo,c.DoctName,a.ExecutDate,a.CureEffect,"
					     +" a.OtherFeeAmount,a.OtherCureNo from LHCustomOtherCure a, LHCountrMedicaItem b,LDDoctor c where 1=1  "
					     +" and a.MedicaItemCode=b.MedicaItemCode and a.DoctNo=c.DoctNo "
					     +getWherePart("a.CustomerNo","CustomerNo") 
					     +getWherePart("a.InHospitNo","InHospitNo");
						  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
						  
						  turnPage.queryModal(strSql, OtherCureInfoGrid);
						  
						    strSql="select b.codename,a.FeeCode, a.FeeAmount,'',a.FeeNo "
					     +"from LHFeeInfo a, ldcode b where 1=1  "
					     +" and a.FeeCode=b.code and b.codetype='llfeeitemtype' "
					     +getWherePart("a.CustomerNo","CustomerNo") 
					     +getWherePart("a.InHospitNo","InHospitNo");
					   
						  turnPage.queryModal(strSql, FeeInfoGrid);
						  
					//	  arrResult=null;
					//	  strSql="select FeeAmount from LHFeeInfo where FeeCode='7'"
					//	  +getWherePart("CustomerNo","CustomerNo") 
					//    +getWherePart("InHospitNo","InHospitNo");
					//     
					//     arrResult = easyExecSql(strSql);
					//     if(arrResult!=null)
					//     {
					//     		fm.all('TotalFee').value= arrResult[0][0];
					//	   }
					//计算分费用比例	   
						    var totalfee=0;
								var tempProportion;
								var rowcount=FeeInfoGrid.mulLineCount;
							
								for (i=0;i<=rowcount-1; i++)
								{
							   tempProportion=FeeInfoGrid.getRowColData(i,3)/1;
							   totalfee+= tempProportion     ;
								}
					
								for(i=0;i<=rowcount-1; i++)
								{   			
									tempProportion=FeeInfoGrid.getRowColData(i,3)/totalfee*100;
									tempProportion=Math.round(tempProportion*100)/100;  
									tempProportion=tempProportion+"%";
									FeeInfoGrid.setRowColData(i,4,tempProportion);
								}
								fm.all('TotalFee').value=totalfee;
				  } 
				  else 
				  {
				  	 alert("LHMainCustomerHealth.js->afterquery()出现错误");
				  }
		  }
	 }
		
	</script>


</html>