<%
//程序名称：LDDrugQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-19 13:58:55
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('DrugCode').value = "";
//    fm.all('SuperDrugCode').value = "";
//    fm.all('IndexFlag').value = "";
//    fm.all('DrugClass').value = "";
    fm.all('DrugName').value = "";
//    fm.all('DrugLtName').value = "";
    fm.all('DrugEnName').value = "";
//    fm.all('DrugNameAbbr').value = "";
//    fm.all('DrugSpecCode').value = "";
    fm.all('DrugDoseCode').value = "";
//    fm.all('DrugMetrCode').value = "";
//     fm.all('DrgUsage').value = "";
//     fm.all('LimitUse').value = "";
//    fm.all('RecipeFlag').value = "";
//    fm.all('SpecialFLag').value = "";
divLDDrug1.style.display='none';
divLDDrug3.style.display='none';
  }
  catch(ex) {
    alert("在LDDrugQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDDrugGrid();  
    initLDDrugClassGrid();
  }
  catch(re) {
    alert("LDDrugQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDDrugGrid;
function initLDDrugGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="药品编号";   
		iArray[1][1]="40px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;
		
		iArray[2]=new Array(); 
		iArray[2][0]="英文名称";   
		iArray[2][1]="60px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="通用名";   
		iArray[3][1]="150px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="曾用名";   
		iArray[4][1]="40px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		
		iArray[5]=new Array(); 
		iArray[5][0]="药品剂型";   
		iArray[5][1]="40px";   
		iArray[5][2]=20;        
		iArray[5][3]=0;

		iArray[6]=new Array(); 
		iArray[6][0]="名称注释";   
		iArray[6][1]="100px";   
		iArray[6][2]=20;        
		iArray[6][3]=0;
		
		iArray[7]=new Array(); 
		iArray[7][0]="IndexFlag";   
		iArray[7][1]="0px";   
		iArray[7][2]=20;        
		iArray[7][3]=3;
		
		iArray[8]=new Array(); 
		iArray[8][0]="LimitUse";   
		iArray[8][1]="0px";   
		iArray[8][2]=20;        
		iArray[8][3]=3;		
    
    LDDrugGrid = new MulLineEnter( "fm" , "LDDrugGrid" ); 
    //这些属性必须在loadMulLine前

    LDDrugGrid.mulLineCount = 0;   
    LDDrugGrid.displayTitle = 1;
    LDDrugGrid.hiddenPlus = 1;
    LDDrugGrid.hiddenSubtraction = 1;
    LDDrugGrid.canSel = 1;
    LDDrugGrid.canChk = 0;
    //LDDrugGrid.selBoxEventFuncName = "showOne";

    LDDrugGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDrugGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
var LDDrugClassGrid;
function initLDDrugClassGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="类别代码";   
		iArray[1][1]="40px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;
		
		iArray[2]=new Array(); 
		iArray[2][0]="类别名称";   
		iArray[2][1]="40px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="类别等级";   
		iArray[3][1]="40px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="MakeDate";   
		iArray[4][1]="0px";   
		iArray[4][2]=20;        
		iArray[4][3]=3;
		
		iArray[5]=new Array(); 
		iArray[5][0]="MakeTime";   
		iArray[5][1]="0px";   
		iArray[5][2]=20;        
		iArray[5][3]=3;

		iArray[6]=new Array();   
    iArray[6][0]="ModifyDate"; 
    iArray[6][1]="0px";      
    iArray[6][2]=20;         
    iArray[6][3]=3;          
                             
    iArray[7]=new Array();   
    iArray[7][0]="ModifyTime"; 
    iArray[7][1]="0px";      
    iArray[7][2]=20;         
    iArray[7][3]=3;          


		
    
    LDDrugClassGrid = new MulLineEnter( "fm" , "LDDrugClassGrid" ); 
    //这些属性必须在loadMulLine前

      LDDrugClassGrid.mulLineCount = 0;   
      LDDrugClassGrid.displayTitle = 1;
      LDDrugClassGrid.hiddenPlus = 1;
      LDDrugClassGrid.hiddenSubtraction = 1;
      LDDrugClassGrid.canSel = 1;
      LDDrugClassGrid.canChk = 0;
    //LDDrugClassGrid.selBoxEventFuncName = "showOne";

    LDDrugClassGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDrugClassGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
