<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHChargeBalanceInput.jsp
//程序功能：费用结算方式设置信息管理(页面显示)
//创建日期：2006-03-01 9:21:30
//创建人  ：GuoLiying
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHChargeBalanceInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHChargeBalanceInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHChargeBalanceSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    
    <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHGroupCont1);">
    		</td>
    		<td class= titleImg>
        		 费用结算方式设置
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHGroupCont1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      合同编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraNo verify="合同编号|NOTNULL&len<=50" readonly>
    </TD>
    <TD  class= title>
      合同名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraName verify="合同名称|NOTNULL&len<=100" readonly>
    </TD>
    <TD class= title>
    	合作机构名称/<br>
    	签约人代码
    </TD>
    <TD  class= input>
    	<Input type=hidden  name= HospitCode>
    	<Input class= 'common' name=HospitName verify="合作机构名称|len<=100" readonly>	
    </TD>   
  </TR>
  <TR  class= common>
  	 <TD  class= title>
      合同责任项目类型
    </TD>
    <TD  class= input>
      <Input type=hidden name=ContraType>
      <Input class= 'common' name=ContraType_ch verify="合同责任项目类型|len<=100"  readonly>	
    </TD>
    <TD  class= title>
      合同责任项目名称
    </TD>
    <TD  class= input>
      <Input type=hidden name=ContraItemNo><Input type=hidden name=ContraItemCode>
      <Input class= 'common' name=ContraItemName verify="合同责任项目名称|len<=100"  readonly>	
    </TD>
    <TD  class= title>
      责任当前状态
    </TD>
    <TD  class= input>
      <Input type=hidden name=ContraNowState>
      <Input class= 'common' name=ContraNowState_ch verify="责任当前状态|len<=4" readonly>	
    </TD>

   </TR>
   <TR  class= common>
  	 <TD  class= title>
      银行名称
    </TD>
    <TD  class= input >
      <Input name=BankCode class='codename' style="width:50px"><Input class='code'  style="width:110px" name=BankName ondblclick="showCodeList('sendbank',[this,BankCode],[1,0],null,null,null,1,300);">
    </TD>
    <TD  class= title>
      银行账号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Accounts >	
    </TD>
    <TD  class= title>
      帐户名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccName>	
    </TD>
   </TR>
   <TR  class= common>
  	 <TD   class= title>
      结算方式
    </TD>
    <TD  class= input >
      <Input name=BalanceType class='codename' style="width:50px"><Input name=BalanceTypeName class='code' style="width:110px" ondblclick="showCodeList('paymode',[this,BalanceType],[1,0],null,null,null,1,300);">  
    </TD>
    <TD  class= title>
      领取人
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Drawer >	
    </TD>
    <TD  class= title></TD>
    <TD  class= input></TD>
   </TR>
   <TR  class= common>
  	 <TD   class= title>
      费用结算时间
    </TD>
    <TD  class= input >
      <Input name=BalanceDate class='coolDatePicker' >
    </TD>
    <TD  class= title>
      费用提醒时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=BalanceRemindDate >	
    </TD>
    <TD  class= title></TD>
    <TD  class= input></TD>
   </TR>
</table>
    </Div>

     
      <!--table>
		  	<tr>
		      <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHChargeBalance);">
		  		</td>
		  		<td class=titleImg>
		  			 信息
		  		</td>
	
		  	</tr>
		  </table>
		  < 信息（列表） -->
		<Div id="divLHChargeBalance" style="display:''">
	     <div id="divDutyExolai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textDutyExolai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backDutyExolai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backDutyExolaiButton()">
			 </div>
			 <div id="divFeeExplai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textFeeExplai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backFeeExplai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backFeeExplaiButton()">
			 </div>
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHChargeBalanceGrid">
	  				</span> 
			    </td>
				</tr>
		  </table>

		</div>
			<!--Div  align=center >
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div-->
		<div id="div1" style="display: 'none'">
			  <table class=common>
			  <TR  class= common>
			    <TD  class= title>
			      执行者
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=linkman >
			    </TD>
			    <TD  class= title>
			      责  任
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=Phone >
			    </TD>
			    <TD >
			    </TD>
			  </TR>
			  </table>
		</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type = hidden name = "Operator">
    <input type = hidden name = "MakeDate">
    <input type = hidden name = "MakeTime">
    <input type = hidden name = "ModifyDate">
    <input type = hidden name = "ModifyTime">
    <input type = hidden name = HiddenBtn>
    <input type = hidden name = ContBalanceNo>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
