<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHCustomDiseasSave.jsp
//程序功能：
//创建日期：2005-01-17 21:09:38
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHCustomDiseasSchema tLHCustomDiseasSchema   = new LHCustomDiseasSchema();
  OLHCustomDiseasUI tOLHCustomDiseasUI   = new OLHCustomDiseasUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHCustomDiseasSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLHCustomDiseasSchema.setDiseasNo(request.getParameter("DiseasNo"));
    tLHCustomDiseasSchema.setDiseasCode(request.getParameter("DiseasCode"));
    tLHCustomDiseasSchema.setDiseasState(request.getParameter("DiseasState"));
    tLHCustomDiseasSchema.setDiseasBeginDate(request.getParameter("DiseasBeginDate"));
    tLHCustomDiseasSchema.setDiseasEndDate(request.getParameter("DiseasEndDate"));
    tLHCustomDiseasSchema.setHospitCode(request.getParameter("HospitCode"));
    tLHCustomDiseasSchema.setDoctor(request.getParameter("Doctor"));
    tLHCustomDiseasSchema.setImpartFlag(request.getParameter("ImpartFlag"));
    tLHCustomDiseasSchema.setImpartDate(request.getParameter("ImpartDate"));
    tLHCustomDiseasSchema.setOperator(request.getParameter("Operator"));
    tLHCustomDiseasSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHCustomDiseasSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHCustomDiseasSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHCustomDiseasSchema.setModifyTime(request.getParameter("ModifyTime"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLHCustomDiseasSchema);
  	tVData.add(tG);
    tOLHCustomDiseasUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHCustomDiseasUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
