<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2006-07-04 16:18:13
//创建人  ：CrtHtml程序创建
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHAddServEvent.js"></SCRIPT>

  <%@include file="LHAddServEventInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form  action="./LHAddServEventSave.jsp" method=post name=fm target="fraSubmit">
  	<%@include file="../common/jsp/OperateButton.jsp"%>
  	<%@include file="../common/jsp/InputButton.jsp"%>
  	<table>
    	<tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServCase);">
    	 	</td>
    	 	<td class= titleImg>
        	 服务事件关联信息
       		</td>   		 
    	</tr>
    </table>
<Div  id= "divLHServCase" style= "display: ''">
  	<table  class= common align='center'>
  	<TR  class= common>
  		<TD  class= title>
	      	服务事件类型
	    </TD>
	    <TD  class= input>
	    	  <Input type=hidden name=ServeEventStyle>
	     	  <Input class= 'code' name=ServeEventStyle_ch verify="服务事件类型|notnull" elementtype=nacessary CodeData= "0|^个人服务事件|1^集体服务事件|2^费用结算事件|3" ondblClick= "showCodeListEx('',[this,ServeEventStyle],[0,1],null,null,null,1);"> 
	    </TD> 
	    <TD  class= title>
	      	服务事件编号
	    </TD>
	    <TD  class= input>
	     	  <Input class= 'common' name=ServeEventNum verify="服务事件编号|notnull" elementtype=nacessary ondblclick="return getNo();"> 
	    </TD> 
	    <TD  class= title>
	      	服务事件名称
	    </TD>
	    <TD  class= input>
	     	  <Input class= 'common' name=ServeEventName verify="服务事件名称|notnull" elementtype=nacessary > 
	    </TD> 
  	</TR>
  	<TR  class= common>
  		<TD  class= title>
	      	服务事件状态
	    </TD>
	    <TD  class= input>
	    	<Input class= 'codename' readonly style="width:50px" name=ServCaseState verify="服务事件状态|notnull"><Input class= 'codeno' readonly style="width:110px"  name=ServCaseStateName  ondblclick="showCodeList('eventstate',[this,ServCaseState],[1,0],null,null,null,'1',160);">	 
	    </TD> 
	    <TD  class= title>
	    	机构属性标识
	    </TD>
	    <TD  class= input>
	    	<Input  class = codeno  name=ComCode verify="机构属性标识|NOTNULL&len<=10" ondblclick="showCodeList('hmhospitalmng',[this,ComName],[1,0],null,fm.ComName.value,'ComName','1',260);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmhospitalmng',[this,ComName],[1,0],null,fm.ComName.value,'ComName');" onkeydown="return showCodeListKey('hmhospitalmng',[this,ComName],[1,0],null,fm.ComName.value,'ComName'); codeClear(ComName,ComCode);"><Input class= 'codename' name=ComName >	
	    </TD> 
	    <TD  class= title>
	    	服务项目名称
	    </TD>
	    <TD  class= input>
	       <Input class= 'common' style="width:50px" name=ServItemCode ><Input class= 'code' style="width:110px" name=ServItemName ondblclick="return showCodeList('hmservitemcode',[ServItemCode,this],[0,1],null,fm.ServItemName.value,'ServItemName',1);" onkeyup="return showCodeListKey('hmservitemcode',[ServItemCode,this],[0,1],null,fm.ServItemName.value,'ServItemName',1);">
	    </TD> 
	    
  	</TR>  	
  	</table>
</Div>
  	<hr>
	<table>
    	<tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServCaseRela);">
    	 	</td>
    	 	<td class= titleImg>
        	 服务事件关联信息
       		</td>  
       		<TD  class= common><INPUT class=cssButton name="delRelaInfo" VALUE="删除选定事件信息"  TYPE=button onclick="delCaseRelaInfo();"></TD> 		 
    	</tr>
    </table>
<Div  id= "divLHServCaseRela" style= "display: ''">  	
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHCaseRelaGrid">
					</span> 
		    	</td>
			</tr>
		</table>
		<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
</Div>		

  	
  	<!--table  class= common align='left' >
  		<TR class= common>
  			<TD class=title>  &nbsp;&nbsp&nbsp;&nbsp					
		  	 	<Input value="新增服务事件保存" type=button class=cssbutton onclick="SaveNewEvent();">
		  	</TD>
		  </TR>
		</table-->
	
		   <input type=hidden id="fmtransact" name="fmtransact">
       <input type=hidden id="fmAction" name="fmAction">
		   <input type=hidden name="Operator" value="hm">
       <input type=hidden name="MakeDate">
       <input type=hidden name="MakeTime">
 		   <input type=hidden name="ModifyDate">
 		   <input type=hidden name="ModifyTime">
 		   <input type=hidden name="HiddenCaseCode">
 		   <input type=hidden name="Date2">
 		   <input type=hidden id="EventCode" name="EventCode">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>