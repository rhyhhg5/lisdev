<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServQieYueInfoSave.jsp
//程序功能：
//创建日期：2006-07-13 11:46:41
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  
 
  LHServContractUI tLHServContractUI   = new LHServContractUI();
  
  LHServCaseRelaSet tLHServCaseRelaSet   = new LHServCaseRelaSet();
  
    String tServPlanNo[] = request.getParameterValues("LHSettingSlipQueryGrid13");           //计划号
    String tComID[] = request.getParameterValues("LHSettingSlipQueryGrid8");					//机构标识  
    String tServItemNo[] = request.getParameterValues("LHSettingSlipQueryGrid12");           //项目序号
    String tServItemCode[] = request.getParameterValues("LHSettingSlipQueryGrid4");					//标准服务项目代码
    String tContNo[] = request.getParameterValues("LHSettingSlipQueryGrid3");					//保单号
    String tCustomerNo[] = request.getParameterValues("LHSettingSlipQueryGrid1");					//客户号
    String tCustomerName[] = request.getParameterValues("LHSettingSlipQueryGrid2");					//客户名
    
    String tChk[] = request.getParameterValues("InpLHSettingSlipQueryGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  
       	 
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
   int LHServCaseRelaCount = 0;
   System.out.println("R R  "+tServPlanNo.length);
	 if(tServPlanNo != null)
	 {	
		    LHServCaseRelaCount = tServPlanNo.length;
	 }	
	  System.out.println(" LHServCaseRelaCount is : "+LHServCaseRelaCount);
    for(int j = 0; j < LHServCaseRelaCount; j++)
	  {
    
               LHServCaseRelaSchema tLHServCaseRelaSchema   = new LHServCaseRelaSchema();
               tLHServCaseRelaSchema.setServCaseCode(request.getParameter("ServCaseCode"));//事件号
               tLHServCaseRelaSchema.setServPlanNo(tServPlanNo[j]);	 //计划号
               tLHServCaseRelaSchema.setComID(tComID[j]);	 //机构标识
               tLHServCaseRelaSchema.setServItemNo(tServItemNo[j]);	 //项目序号
               tLHServCaseRelaSchema.setServItemCode(tServItemCode[j]);	 //项目代码
               tLHServCaseRelaSchema.setContNo(tContNo[j]);	 //保单号码
               tLHServCaseRelaSchema.setCustomerNo(tCustomerNo[j]);	 //客户号码
               tLHServCaseRelaSchema.setCustomerName(tCustomerName[j]);	 //客户姓名
               if(tChk[j].equals("1"))  
                {        
                       
                     System.out.println(":BB cccccccc BB "+tServItemNo[j]);
                     System.out.println("该行被选中 "+tServItemNo[j]);     
                     tLHServCaseRelaSet.add(tLHServCaseRelaSchema);         
                     System.out.println("OOOOOOOOOOOOOOOOOOOOO");  
                }
                if(tChk[j].equals("0"))  
                {     System.out.println(":D cccccccc DD "+tServItemNo[j]);
                      System.out.println("该行未被选中 "+tServItemNo[j]);
                }
    }

    
    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLHServCaseRelaSet);
  	tVData.add(tG);
    tLHServContractUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHServContractUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	  var FlagStr="<%=FlagStr%>";
		if(FlagStr!="Fail")
    {
	     var transact = "<%=transact%>";
	     if(transact!="DELETE||MAIN")
	     {
	        parent.fraInterface.QueryQieYueInfo(); 
	     }
	  }
</script>
</html>
