var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;       
var turnPage = new turnPageClass();
function initgo()
{ 
	var ServCaseCode=LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,2);
	var ServTaskNo=LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,4);
	var strsql="select * from LHMedicalServiceRecord where BespeakServiceDoctNo='"+ServCaseCode+"'and ExcuteServiceItem='"+ServTaskNo+"'";
	var arrResult=new Array();
	arrResult=easyExecSql(strsql);

	if(arrResult!=null)
	{
		fm.all('RecordNo').value=arrResult[0][0];
		fm.all('HospitCode').value=arrResult[0][9];                 
		fm.all('HospitName').value=arrResult[0][18];                
		fm.all('ServiceDate').value=arrResult[0][7];           
		fm.all('BespeakServiceItem').value=arrResult[0][23];
   }
   else
   {
		fm.all('RecordNo').value="RecordNo";
        fm.all('HospitCode').value="";                 
        fm.all('HospitName').value="";      
        fm.all('ServiceDate').value="";           
        fm.all('BespeakServiceItem').value="";
		fm.all('ServCaseCode').value= ServCaseCode;
		fm.all('ServTaskNo').value=ServTaskNo;
	}
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
	    	showInfo.focus();  
		}
		catch(ex)
		{
	    	showInfo=null;
		}
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	var RecordNo=fm.all('RecordNo').value;

	if(RecordNo!="RecordNo")
	{
		alert("请不要重复保存!");
		return;
	}
	fm.fmtransact.value = "INSERT||MAIN" ;
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
    	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  	}
	else
	{ 
    	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCommTrans.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick2()
{
	var RecordNo=fm.all('RecordNo').value;
	if(RecordNo=="RecordNo"){
		alert("请先保存数据后,才有数据修改!");return;
	} 
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHCommTransQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	var RecordNo=fm.all('RecordNo').value;
	if(RecordNo=="RecordNo")
	{
		alert("没有数据可删除!");return;
	} 
	
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */              
function ServInfoSee()
{
		var caseCode = null;
	  caseCode = new Array();
	  var rowNum2=LHCaseInfoGrid. mulLineCount ; //行数 	
	  var bb2 = new Array();
	  var yy2 = 0;
	  for(var row2=0; row2 < rowNum2; row2++)
	  {
	        	   bb2[yy2] = row2;
	            caseCode[yy2++]= LHCaseInfoGrid.getRowColData(bb2,2);
	            //alert("NN "+caseCode);
	  }
		var arrSelected = null;
	  arrSelected = new Array();
	  var rowNum=LHCaseInfoGrid. mulLineCount ; //行数 	
	  var bb = new Array();
	  var yy = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        	   bb[yy] = row;
	             arrSelected[yy++] = LHCaseInfoGrid.getRowColData(bb,4);
	  }
	   //alert("MM "+caseCode);
	   //alert("BB "+arrSelected);
	   window.open("./LHServiceManageLookMain.jsp?caseCode="+caseCode+"&arrSelected="+arrSelected+"","事件服务信息查看",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
}  
function showTwo()
{

	try
	{ 
				var arrSelected = null;
	      arrSelected = new Array();
	      var rowNum=LHCaseInfoGrid. mulLineCount ; //行数 	
	      var aa = new Array();
	      var xx = 0;
	      for(var row=0; row < rowNum; row++)
	      {
	            var tChk =LHCaseInfoGrid.getChkNo(row); 
	            if(tChk == true)
	            {
	            	   aa[xx] = row;
	            	   
	                arrSelected[xx++] = LHCaseInfoGrid.getRowColData(aa,4);
	                //alert("AA "+arrSelected);
	            }
	      }
	      if(aa.length=="1")
	      {
           var strsql="select b.ServTaskState,b.TaskDesc ,b.TaskFinishDate "
	              +" from LHCaseTaskRela b "
	              +" where b.ServTaskNo='"+ arrSelected +"'";
	          //alert(strsql);
	          //alert(easyExecSql(strsql));
	          var result=new Array;
	          result=easyExecSql(strsql);
	          //alert(result);
	          if(result!=""&&result!="null"&&result!=null)
	          {
	            fm.all('ServTaskState').value=result[0][0];
	            fm.all('ServTaskStateName').value=easyExecSql("select d.codename from ldcode d where d.codetype='taskstatus' and d.code='"+fm.all('ServTaskState').value+"'");
	            fm.all('ServItemNote').value=result[0][1];
	            fm.all('TaskFinishDate').value=result[0][2];
	          }
	      }
	}
	catch(ex)
	{
		alert("LHItemPriceFactorInputInit.jsp-->传输入信息Info中发生异常:初始化界面错误!");
	}
}
function updateClick()
{
	if(fm.all('ServTaskState').value=="1")//不能将服务状态重新改为等待执行
	{
		 alert("您不能将服务任务状态重新改为等待执行!");
		 fm.all('ServTaskState').value="2";
		 fm.all('ServTaskStateName').value="正在执行";
		 return false;
	}
	else
	{
	      var arrSelected = null;
	      arrSelected = new Array();
	      var rowNum=LHCaseInfoGrid. mulLineCount ; //行数 	
	      var aa = new Array();
	      var xx = 0;
	      for(var row=0; row < rowNum; row++)
	      {
	            	   aa[xx] = row;
	            	   
	                arrSelected[xx++] = LHCaseInfoGrid.getRowColData(aa,4);
	                var no= LHCaseInfoGrid.getRowColData(aa,4);
	                 var sqlRela="select h.TaskExecNo from LHTaskCustomerRela h"
	                            +" where h.ServTaskNo='"+ no+"'";
	                //alert(easyExecSql(sqlRela));
	                var pp=easyExecSql(sqlRela);
	                //alert(pp);
	                if(pp=="null"||pp==null||pp=="")
	                {
	                	 alert("任务编号为 "+no+" 的:任务执行状态,还未保存，不能保存任务状态");
	                	 return false;
	                }
	      }

    	    if (confirm("您确实想修改该记录吗?"))
    	    {
               var i = 0;
               var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
               var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
               showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
          
               fm.fmtransact.value = "UPDATE||MAIN";
               fm.action="./LHServiceManageSave.jsp";
               fm.submit(); //提交
		      }
          else
          {
           alert("您取消了修改操作！");
          }  
    }
}
function saveClick()
{
	initForm();
	if(fm.all('ServTaskState').value=="1")//不能将服务状态重新改为等待执行
	{
		 alert("您不能将服务任务状态重新改为等待执行!");
		 fm.all('ServTaskState').value="2";
		 fm.all('ServTaskStateName').value="正在执行";
		 return false;
	}
	else
	{
		var arrSelected = null;
	    arrSelected = new Array();
	    var rowNum=LHCaseInfoGrid. mulLineCount ; //行数 	
//	    var aa = new Array();
//	    var xx = 0;
	    for(var row=0; row < rowNum; row++)
	    {
//	    	aa[xx] = row;                 
	    	var no= LHCaseInfoGrid.getRowColData(row,4);
	    	var sqlRela="select h.TaskExecNo from LHTaskCustomerRela h"
	    	           +" where h.ServTaskNo='"+ no+"'";
	    	var pp=easyExecSql(sqlRela);
        //alert(pp);
	    	if(pp=="null"||pp==null||pp=="")
	    	{
	    		alert("任务编号为 "+no+" 的任务执行状态还未保存，不能修改任务状态");
	    		return false;
	    	}
	    }
    	if (confirm("您确实想保存此信息记录吗?"))
    	{
			var i = 0;
            var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
            var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
            showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
            
            fm.fmtransact.value = "UPDATE||MAIN";
            fm.action="./LHServiceManageSave.jsp";
            fm.submit(); //提交
            fm.all('save').disabled=true;
		}
        else
        {
			alert("您取消了保存操作！");
        } 
    }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
} 


function afterSubmitParentState()
{
	//fm.all('ServTaskState').value = "2";	
	//fm.all('ServTaskStateName').value = "正在执行";
	saveClick();
}


//任务实施按钮

function taskExeDeal()
{
	//确保服务任务已分配到人保存
	var arrSelected = new Array();
	var ServTaskCode = new Array();
	var rowNum=LHCaseInfoGrid.mulLineCount ; //行数 	

	for(var row=0; row < rowNum; row++)
	{     
		var tServTaskNo= LHCaseInfoGrid.getRowColData(row,4);
		var sqlRela="select h.TaskExecNo from LHTaskCustomerRela h where h.ServTaskNo='"+ tServTaskNo+"'";
		var ServTaskNoResult = easyExecSql(sqlRela);
    	
		if(ServTaskNoResult == "null" || ServTaskNoResult==null || ServTaskNoResult=="")
		{
			alert("任务编号为 "+tServTaskNo+" 的任务执行状态还未保存，不能修改任务状态");
			return false;
		}
		ServTaskCode[row] = tServTaskNo;
	}
	if(LHCaseInfoGrid.canSel!="1")
	{
	  var url = easyExecSql(" select moduleurl from LHSERVTASKDEF  where ServTaskCode = '"+LHCaseInfoGrid.getRowColData(0,8)+"' ");
	  //alert(url);
	  if(url!=""&&url!="null"&&url!=null)
	  {
	    window.open("./"+url+"?ServTaskCode="+ServTaskCode+"&flag=1",LHCaseInfoGrid.getRowColData(0,5),'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
	  }
	  else
	  {
	  	 alert("此服务任务信息暂时无对应的具体功能页面,请原谅!");
	  }
  }
  //alert(LHCaseInfoGrid.canSel);
  if(LHCaseInfoGrid.canSel=="1")//问卷录入类型的任务名称
  {
  	if (LHCaseInfoGrid.getSelNo() >= 1)
	 {     
	 	    //alert(LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,8));
	 	    var testTaskCode=LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,8);
				if(testTaskCode=="00203")//评估问卷录入类型
				{
					//alert(LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,4));
					var tServTaskNo=LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,4);
					var sqlRela="select h.TaskExecNo from LHTaskCustomerRela h where h.ServTaskNo='"+ tServTaskNo+"'";
					var ServTaskNoResult = easyExecSql(sqlRela);
					if(ServTaskNoResult == "null" || ServTaskNoResult==null || ServTaskNoResult=="")
					{
							alert("任务编号为 "+tServTaskNo+" 的任务执行状态还未保存，请先对其进行保存!");
							return false;
					}
					else
					{
							window.open("./LHQuesKineMain.jsp?ServTaskNo="+tServTaskNo+"&custtype=6","评估问卷录入",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
				  }
				}
				if(testTaskCode=="00401")//体检申请受理
				{
					  var tServTaskNo=LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,4);
					  var sqlRela="select h.TaskExecNo from LHTaskCustomerRela h where h.ServTaskNo='"+ tServTaskNo+"'";
					  var ServTaskNoResult = easyExecSql(sqlRela);
					  if(ServTaskNoResult == "null" || ServTaskNoResult==null || ServTaskNoResult=="")
					  {
					  		alert("任务编号为 "+tServTaskNo+" 的任务执行状态还未保存，请先对其进行保存!");
					  		return false;
					  }
					  else
					  {
					  	   var sServTaskNo=LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,4);
					  	   //alert(sServTaskNo);
					  	   var strSql=" select distinct  a.ServCaseCode,a.ServItemNO  from LHTASKCUSTOMERRELA a "
    	            +" where a.ServTaskno = '"+ sServTaskNo +"'";
    	           //alert(strSql);
    	           //alert(easyExecSql(strSql));
    	           var relaRest=new Array;
    	           relaRest=easyExecSql(strSql);
    	           if(relaRest!=""&&relaRest!=null&&relaRest!="null")
    	           {  
    	           	    var saSql=" select  a.ServItemCode,a.Customerno  from LHSERVCASERELA a "
    	                         +" where a.Servcasecode = '"+ relaRest[0][0] +"'"
    	                         +" and a.ServItemNO ='"+ relaRest[0][1] +"' ";
    	                //alert(saSql);
    	                var tRest=new Array;
    	                tRest=easyExecSql(saSql); 
    	                //alert(tRest);
    	                if(tRest!=""&&tRest!="null"&&tRest!=null)
    	                {
    	                	    var tServCaseCode=LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,2);
    	                	    var tServTaskNo=LHCaseInfoGrid.getRowColData(LHCaseInfoGrid.getSelNo()-1,4);
    	                	    //alert(tServTaskNo);
    	                	    var mulSql = "select DISTINCT a.ServAccepNo "
	                                   + " from LHIndiServAccept a ,LHServCaseRela b "
	                                   + " where  b.ServCaseCode = a.ServCaseCode "
	                                   + " and b.ServCaseCode in a.ServCaseCode   "
	                                   + " and b.ServItemCode = a.ServItemCode   "
	                                   +" and b.ServItemCode ='"+ tRest[0][0] +"'"
	                                   +" and  b.Servcasecode ='"+ tServCaseCode +"'"
	                                   +" and a.Servtaskcode='"+tServTaskNo+"'"
	                                   ;
	                          //alert(mulSql);
	                          var ServAccepNo=easyExecSql(mulSql);
	                          if(ServAccepNo!=""&&ServAccepNo!=null&&ServAccepNo!="null")
	                          {
	                             window.open("./LHIndivServAcceptMain.jsp?ServAccepNo="+ServAccepNo+"&test=1","返回受理页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
    	                      }
    	                      else
    	                      {
    	                      	 alert("此条服务任务没有经过服务受理,因此不能进入具体页面!");
    	                      }
    	                }
    	           }
    	           else
    	           {
    	           	   alert("此条服务任务对应的服务事件在服务受理功能中未存储，请先对其进行保存!");
    	           	   return false;
    	           }
					  		//window.open("./LHQuesKineMain.jsp?ServTaskNo="+tServTaskNo+"&custtype=6","评估问卷录入",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
				    }
				}
				if(testTaskCode!="00203"&&testTaskCode!="00401")//体检申请受理
				{
							alert("您选择的服务任务名称不为评估问卷录入或体检申请受理,不能进入具体页面!");
						  return false;
				}
		} 
		else
		{
						 alert("请选择一条要传输的记录！");    
		}
  }
	
}
function showOne()
{
	 
}