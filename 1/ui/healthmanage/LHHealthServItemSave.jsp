<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHHealthServItemSave.jsp
//程序功能：
//创建日期：2006-03-16 10:49:41
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LHHealthServItemSchema tLHHealthServItemSchema   = new LHHealthServItemSchema();
  OLHHealthServItemUI tOLHHealthServItemUI   = new OLHHealthServItemUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHHealthServItemSchema.setServItemCode(request.getParameter("ServItemCode"));
    tLHHealthServItemSchema.setServItemName(request.getParameter("ServItemName"));
    tLHHealthServItemSchema.setServItemType(request.getParameter("ServItemTypeCode"));
    tLHHealthServItemSchema.setServItemNote(request.getParameter("ServItemNote"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLHHealthServItemSchema);
  	tVData.add(tG);
    tOLHHealthServItemUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHHealthServItemUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
