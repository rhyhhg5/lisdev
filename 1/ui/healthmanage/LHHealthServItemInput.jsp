<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2006-03-16 10:49:39
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHHealthServItemInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHHealthServItemInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LHHealthServItemSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 服务项目定义表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHHealthServItem1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      标准服务项目代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ServItemCode   verify="标准服务项目代码|notnull">
    </TD>
    <TD  class= title>
      标准服务项目名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ServItemName   verify="标准服务项目名称|notnull&len<=50" >
    </TD>
    <TD  class= title>
      服务项目类型
    </TD>
    <TD  class= input>
    	<Input class=codeno name=ServItemTypeCode CodeData="0|0^1|健康咨询类^2|健康维护类^3|就诊服务类^4|诊疗保障类"  ondblclick=" return showCodeListEx('ServItemType',[this,ServItemType],[0,1],null,null,null,1);" onkeydown=" return showCodeListKeyEx('ServItemType',[this,ServItemType],[0,1],null,null,null,1);codeClear(ServItemType,ServItemTypeCode);"  verify="服务项目类型|notnull"><Input class= 'codename' name=ServItemType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title colspan='6'>
      标准服务项目释义（300汉字以内）:
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= input colspan='6'>
      <textarea class= 'common' rows="3" cols="100" name=ServItemNote verify="标准服务项目释义|len<=300" ></textarea>
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
