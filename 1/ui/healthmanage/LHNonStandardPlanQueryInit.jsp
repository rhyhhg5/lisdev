<%
//程序名称：LHNonStandardPlanQueryInit.jsp
//程序功能：郭丽颖
//创建日期：2006-11-21 09:57:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期    : 
// 更新原因/内容:  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() 
{ 
	try{}
  	catch(ex) 
  	{
    	alert("在LHNonStandardPlanQueryInput.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}                                    
function initForm()
{
	try 
	{
    	initInpBox();
    	initLHServPlanGrid();  
    }
  	catch(re) 
  	{
    	alert("LHNonStandardPlanQueryInput.jsp-->InitForm函数中发生异常:初始化界面错误!");
  	}                                                      
}
//领取项信息列表的初始化
var LHServPlanGrid;
function initLHServPlanGrid() 
{                               
	var iArray = new Array();
	try 
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         		//列名
    	iArray[0][1]="40px";         		//列名
    	iArray[0][3]=1;         		//列名
    	iArray[0][4]="station";         		//列名
    
    	iArray[1]=new Array(); 
	    iArray[1][0]="服务项目代码";   
	    iArray[1][1]="60px";   
	    iArray[1][2]=20;        
	    iArray[1][3]=0;
	               
		iArray[2]=new Array(); 
		iArray[2][0]="服务项目名称";   
		iArray[2][1]="120px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
      
        iArray[4]=new Array(); 
		iArray[4][0]="服务项目序号";   
		iArray[4][1]="60px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		iArray[4][14]="1";
		  
		iArray[7]=new Array();                           
	    iArray[7][0]="定价方式代码";                             
	    iArray[7][1]="0px";                             
	    iArray[7][2]=20;                                  
	    iArray[7][3]=3;  
	    
	    iArray[3]=new Array(); 
	    iArray[3][0]="个人服务项目号码";   
	    iArray[3][1]="0px";   
	    iArray[3][2]=20;        
	    iArray[3][3]=3;
	    
	    iArray[5]=new Array(); 
        iArray[5][0]="服务事件类型";                             
        iArray[5][1]="60px";                     
        iArray[5][2]=20;                                  
        iArray[5][3]=0;      
	      
		iArray[6]=new Array(); 
	    iArray[6][0]="服务机构";                             
	    iArray[6][1]="40px";                     
	    iArray[6][2]=20;                                  
	    iArray[6][3]=0;      
      
        iArray[8]=new Array(); 
	    iArray[8][0]="定价方式名称";   
	    iArray[8][1]="0px";   
	    iArray[8][2]=20;        
	    iArray[8][3]=3;      
	              
        iArray[9]=new Array(); 
	    iArray[9][0]="eventtype";   
	    iArray[9][1]="0px";   
	    iArray[9][2]=20;        
	    iArray[9][3]=3;
	    iArray[9][14]="1";
	    
	    iArray[10]=new Array(); 
	    iArray[10][0]="servplanno";   
	    iArray[10][1]="0px";   
	    iArray[10][2]=20;        
	    iArray[10][3]=3;
	    
	    iArray[11]=new Array(); 
	    iArray[11][0]="事件原始编号";   
	    iArray[11][1]="80px";   
	    iArray[11][2]=20;        
	    iArray[11][3]=0;

    	LHServPlanGrid = new MulLineEnter( "fm" , "LHServPlanGrid" );                    
		LHServPlanGrid.mulLineCount = 0;                             
      	LHServPlanGrid.displayTitle = 1;                          
      	LHServPlanGrid.hiddenPlus = 1;                          
      	LHServPlanGrid.hiddenSubtraction = 1;                          
      	LHServPlanGrid.canSel = 1;                          
      	LHServPlanGrid.canChk = 0;                                                	

    	LHServPlanGrid.loadMulLine(iArray);                                     
    	//这些操作必须在loadMulLine后面                                                                         
	}
	catch(ex) {alert(ex);}
}
</script>





