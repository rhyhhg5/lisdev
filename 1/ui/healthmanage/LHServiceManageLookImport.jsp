<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LHExcuteStateImportSave.jsp
//程序功能：磁盘导入上传
//创建日期：2006-12-04
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.health.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>

<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="java.util.*"%>

<%
	 String taskNo = request.getParameter("UptaskNo");
%>
<%
	String flag = "";
	String content = "";
	String path = "";
	String fileName = ""; 			 //上传文件名
	String configFileName = "";
	String localFile = "";			 //上传的文件路径
  String SheetName = "ExcuteState";

	//得到全局变量
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	VData data = new VData();
 
 		
		
		String ImportPath = "";
	  //得到excel文件的保存路径
 		path = application.getRealPath("/");
 		path = path+ "/temp/";
 		System.out.println("-----------------"+path);
		//上传excel文件并保存，得到文件名   
	  File dir1 = new File(path);
		if (!dir1.exists()){
        dir1.mkdirs();
    }
		
	 
		DiskFileUpload fu = new DiskFileUpload();
		// 设置允许用户上传文件大小,单位:字节
		fu.setSizeMax(10000000);
		// maximum size that will be stored in memory?
		// 设置最多只允许在内存中存储的数据,单位:字节
		fu.setSizeThreshold(4096);
		// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
		fu.setRepositoryPath(path);
		//开始读取上传信息
		List fileItems = fu.parseRequest(request);
	 
		// 依次处理每个上传的文件
		Iterator iter = fileItems.iterator();
		while (iter.hasNext())
		{
			FileItem item = (FileItem) iter.next();
			if (item.getFieldName().compareTo("ImportPath")==0)
			{
				ImportPath = item.getString();
			}
			//忽略其他不是文件域的所有表单信息
			if (!item.isFormField())
			{
				String name = item.getName();
				
				long size = item.getSize();
				if((name==null||name.equals("")) && size==0)
					continue;
				ImportPath= path + ImportPath;
	 			fileName = name.replace('\\','/');
				fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
	
				//保存上传的文件到指定的目录
				try
				{
					 item.write(new File(ImportPath + fileName));
				}catch(Exception e){
					 e.printStackTrace();
				   System.out.println("upload file error ...");
				}
			}
		}
	fileName = path +fileName;
	System.out.println("______________fileName:"+fileName);	
	
	
	
	path = path.replace('\\','/');
	configFileName = path + "LHExcuteState.xml";		//xml文件路径
  data.add(tGI);
 
	//从磁盘导入被保人清单
	LHServiceManageLookExportImport tImport= new LHServiceManageLookExportImport(fileName, configFileName,taskNo,SheetName);
	if (!tImport.submitImport())
	{
	  flag = "Fail";
		//content = tLHServiceManageLookExportUI.mErrors.getFirstError();;
		content = "磁盘导入失败！";
	}
	else
	{
	  	flag = "Succ";
		  content = "磁盘导入成功！";
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit2("<%=flag%>", "<%=content%>");
</script>
</html>

