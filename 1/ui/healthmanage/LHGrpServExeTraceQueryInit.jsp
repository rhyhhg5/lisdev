<%
//程序名称：LHGrpServExeTraceQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-03-09 16:05:16
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('GrpServPlanNo').value = "";
  //  fm.all('GrpServItemNo').value = "";
    fm.all('GrpContNo').value = "";
    fm.all('ExeState').value = "";
  //  fm.all('ExeDate').value = "";
 //   fm.all('ServDesc').value = "";
 //   fm.all('ManageCom').value = "";
 //   fm.all('Operator').value = "";
 //   fm.all('MakeDate').value = "";
 //   fm.all('MakeTime').value = "";
 //   fm.all('ModifyDate').value = "";
 //   fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LHGrpServExeTraceQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHGrpServExeTraceGrid();  
  }
  catch(re) {
    alert("LHGrpServExeTraceQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHGrpServExeTraceGrid;
function initLHGrpServExeTraceGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="团体服务计划号码";   
	  iArray[1][1]="80px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=1;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="团单号";   
	  iArray[2][1]="120px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=1;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="服务执行状态";   
	  iArray[3][1]="70px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=1;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="服务执行时间";   
	  iArray[4][1]="50px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=1;
  
	  iArray[5]=new Array(); 
	  iArray[5][0]="服务详细描述";   
	  iArray[5][1]="50px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=1;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="入机日期";   
	  iArray[6][1]="50px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=3;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="入机时间";   
	  iArray[7][1]="0px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=3;
	  
	  
    LHGrpServExeTraceGrid = new MulLineEnter( "fm" , "LHGrpServExeTraceGrid" ); 
    //这些属性必须在loadMulLine前

    LHGrpServExeTraceGrid.mulLineCount = 0;   
    LHGrpServExeTraceGrid.displayTitle = 1;
    LHGrpServExeTraceGrid.hiddenPlus = 1;
    LHGrpServExeTraceGrid.hiddenSubtraction = 1;
    LHGrpServExeTraceGrid.canSel = 1;
    LHGrpServExeTraceGrid.canChk = 0;
 //   LHGrpServExeTraceGrid.selBoxEventFuncName = "showOne";

    LHGrpServExeTraceGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHGrpServExeTraceGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
