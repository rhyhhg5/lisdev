<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-06-11 15:49:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="LDTestPriceMgtInput.js"></SCRIPT>
  <%@include file="LDTestPriceMgtInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LDTestPriceMgtSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDTestPriceMgt1);">
    		</td>
    		<td class= titleImg>
        		 体检项目价格管理基本信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLDTestPriceMgt1" style= "display: ''">
				<table  class= common align='center' >
				  <TR  class= common>
				    <TD  class= title>
				      体检机构代码
				    </TD>
				    <TD  class= input>
				      <Input class= 'common' readonly name=HospitCode>
				    </TD>
				    <TD  class= title>
				      体检机构名称
				    </TD>
				    <TD  class= input>
				      <Input class= 'code' name=HospitName ondblclick="return showCodeList('lhhospitnameht',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,350);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitnameht',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,350)	; ">
				    </TD>
				    <TD  class= title>
				      记录时间
				    </TD>
				    <TD  class= input>
				      <Input class= 'coolDatePicker' dateFromat="Short"  name="RecordDate" verify="记录时间|DATE">
				    </TD>
				  </TR>
				</table> 
	  		<table>
			  	<tr>
			      <td class=common>
					    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,testItemPrice1);">
			  		</td>
			  		<td class=titleImg>
			  			 检查项目单价
			  		</td>
			  	</tr>
			  </table>
			  <!-- 信息（列表） -->
				<Div id="testItemPrice1" style="display:'' ">
			    <table class=common>
			    	<tr class=common>
				  		<td text-align:left colSpan=1>
			  				<span id="spanTestItemPriceGrid">
			  				</span> 
					    </td>
						</tr>
					</table>
				</div>

	
  		<table>
		  	<tr>
		      <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,testGrpPrice1);">
		  		</td>
		  		<td class=titleImg>
		  			 体检套餐价格
		  		</td>
		  	</tr>
		  </table>
		  <!-- 信息（列表） -->
			<Div id="testGrpPrice1" style="display:''">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanTestGrpPriceGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>
		

		
<table  class= common align='center' style="display:'none'"> 
  <TR  class= common >
    <TD>序列号</TD>
    <TD  class= input>
      <Input class= 'common' name=SerialNo >
    </TD>
    <TD  class= title>
      操作员
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>     
    <TD  class= title>       
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
