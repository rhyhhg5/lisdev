<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServQieYueAllSave.jsp
//程序功能：
//创建日期：2006-11-28 15:18:41
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LHServContractUI tLHServContractUI   = new LHServContractUI();
  LHServCaseRelaSet tLHServCaseRelaSet   = new LHServCaseRelaSet();
  String ServCaseCode=request.getParameter("ServCaseCode");  
       	 
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String tItemNo[][]; 
  SSRS tSSRS_Result = new SSRS();
  ExeSQL tExeResultSQL = new ExeSQL();
  String ResultSql=request.getParameter("ResultSql");  //页面的查询条件的Sql
         //String sqlResult =" select distinct a.CustomerNo ,a.Name, a.ContNo, a.ServItemCode , "
	       //+" a.ComID, a.ServItemNo ,a.servplanno "
	       //+" from lhservitem a  where "
				 //+" a.ServItemNo not in ( select distinct b.ServItemNo from  LHServCaseRela b where b.ServCaseCode = '"+ ServCaseCode +"')"
				 //+" and a.ServItemNo not in ( select distinct r.servitemno from lhservcaserela r, lhservcasedef d where  r.servcasecode = d.servcasecode and d.servcasestate in ('0','1'))"
         //;
  String sqlResult=ResultSql;
    System.out.println("sqlResult "+sqlResult);
    tSSRS_Result = tExeResultSQL.execSQL(sqlResult);
    tItemNo = tSSRS_Result.getAllData(); 
    for(int j = 0; j < tItemNo.length; j++)
    {
               LHServCaseRelaSchema tLHServCaseRelaSchema   = new LHServCaseRelaSchema();
               tLHServCaseRelaSchema.setServCaseCode(ServCaseCode);//事件号
               tLHServCaseRelaSchema.setServPlanNo(tItemNo[j][12]);	 //计划号
               tLHServCaseRelaSchema.setComID(tItemNo[j][7]);	 //机构标识
               tLHServCaseRelaSchema.setServItemNo(tItemNo[j][11]);	 //项目序号
               tLHServCaseRelaSchema.setServItemCode(tItemNo[j][3]);	 //项目代码
               tLHServCaseRelaSchema.setContNo(tItemNo[j][2]);	 //保单号码
               tLHServCaseRelaSchema.setCustomerNo(tItemNo[j][0]);	 //客户号码
               tLHServCaseRelaSchema.setCustomerName(tItemNo[j][1]);	 //客户姓名
               tLHServCaseRelaSet.add(tLHServCaseRelaSchema);  
    }
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLHServCaseRelaSet);
  	tVData.add(tG);
    tLHServContractUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHServContractUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	  var FlagStr="<%=FlagStr%>";
		if(FlagStr!="Fail")
    {
	     var transact = "<%=transact%>";
	     if(transact!="DELETE||MAIN")
	     {
	        parent.fraInterface.QueryQieYueInfo(); 
	     }
	  }
</script>
</html>
