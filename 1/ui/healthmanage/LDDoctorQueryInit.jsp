<%
//程序名称：LDDoctorQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-19 10:10:52
//创建人  ：ctrHTML
//更新人  ： zhsj
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('DoctNo').value = "";
    fm.all('HospitCode').value = "";
    fm.all('DoctName').value = "";
    fm.all('Birthday').value = "";
    fm.all('Sex').value = "";
  //  fm.all('ManageCom').value = "";
    fm.all('TechPostCode').value = "";
    fm.all('EduLevelCode').value = "";
    fm.all('SecName').value = "";
   // fm.all('AreaName').value = "";
    fm.all('PartTimeFlag').value = "";
    fm.all('DoctPhone').value = "";
    fm.all('DoctEmail').value = "";
    fm.all('SpecialFLag').value = "";
    //fm.all('CarreerClass').value = "";
	  //fm.all('Contact').value = "";
	 // fm.all('FixFlag').value = "";  
    fm.all('ExpertFlag').value = "";
    fm.all('CExportFlag').value = "";
    fm.all('ModifyDate').value = "";
    
  }
  catch(ex) {
    alert("在LDDoctorQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDDoctorGrid();  
  }
  catch(re) {
    alert("LDDoctorQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDDoctorGrid;
function initLDDoctorGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="医务人员编号";   
		iArray[1][1]="60px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;

		
		iArray[2]=new Array(); 
		iArray[2][0]="医务人员姓名";   
		iArray[2][1]="50px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="出生年份";   
		iArray[3][1]="50px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array();
		iArray[4][0]="性别";
		iArray[4][1]="40px";
		iArray[4][2]=20;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="职称";  
		iArray[5][1]="40px";  
		iArray[5][2]=20;      
		iArray[5][3]=0;       

		iArray[6]=new Array();
		iArray[6][0]="学历";  
		iArray[6][1]="40px";  
		iArray[6][2]=20;      
		iArray[6][3]=0;       
		
		
		iArray[7]=new Array();
		iArray[7][0]="科室专业";  
		iArray[7][1]="40px";  
		iArray[7][2]=20;      
		iArray[7][3]=0;           
		
	  iArray[8]=new Array();  
	  iArray[8][0]="专家标识";
	  iArray[8][1]="40px";     
		iArray[8][2]=20;        
		iArray[8][3]=0;  
		
		iArray[9]=new Array();  
		iArray[9][0]="职业类型";
		iArray[9][1]="40px";     
		iArray[9][2]=20;        
		iArray[9][3]=0;
		   
		iArray[10]=new Array();  
		iArray[10][0]="业务专长";
		iArray[10][1]="40px";     
		iArray[10][2]=20;        
		iArray[10][3]=0;        
		
	  iArray[11]=new Array(); 
		iArray[11][0]="所属机构";   
		iArray[11][1]="40px";   
		iArray[11][2]=20;        
		iArray[11][3]=0;    
		
	  iArray[12]=new Array();    
	  iArray[12][0]="MakeDate";  
	  iArray[12][1]="0px";       
	  iArray[12][2]=20;          
	  iArray[12][3]=3;  
	//zhsj于2007.07.05修改  
	  iArray[13]=new Array(); 
		iArray[13][0]="管理机构";   
		iArray[13][1]="40px";   
		iArray[13][2]=20;        
		iArray[13][3]=0;          
  	
  	iArray[14]=new Array(); 
		iArray[14][0]="工作地点";   
		iArray[14][1]="40px";   
		iArray[14][2]=20;        
		iArray[14][3]=0; 	
		
		iArray[15]=new Array(); 
		iArray[15][0]="聘用方式";   
		iArray[15][1]="40px";   
		iArray[15][2]=20;        
		iArray[15][3]=0; 
		
		iArray[16]=new Array(); 
		iArray[16][0]="联系方式";   
		iArray[16][1]="40px";   
		iArray[16][2]=20;        
		iArray[16][3]=0; 
		
		iArray[17]=new Array(); 
		iArray[17][0]="签约状态";   
		iArray[17][1]="40px";   
		iArray[17][2]=40;        
		iArray[17][3]=0; 
		
		iArray[18]=new Array(); 
		iArray[18][0]="地区号码";   
		iArray[18][1]="0px";   
		iArray[18][2]=20;        
		iArray[18][3]=0; 
		
	  //iArray[6]=new Array();
		//iArray[6][0]="DoctTitCode";
		//iArray[6][1]="0px";
		//iArray[6][2]=20;  
		//iArray[6][3]=3;  
		
		//iArray[10]=new Array();
		//iArray[10][0]="TechSpec";  
		//iArray[10][1]="0px";  
		//iArray[10][2]=20;      
		//iArray[10][3]=3;   
		//iArray[13]=new Array();   
		//iArray[13][0]="MakeTime"; 
		//iArray[13][1]="0px";      
		//iArray[13][2]=20;         
		//iArray[13][3]=3;          
		
		//iArray[14]=new Array();  
		//iArray[14][0]="签约标识";
		//iArray[14][1]="40px";     
		//iArray[14][2]=20;        
		//iArray[14][3]=0;         
		
		//iArray[16]=new Array();      
		//iArray[16][0]="JobExplain";
		//iArray[16][1]="0px";         
		//iArray[16][2]=20;            
		//iArray[16][3]=3;             		           
		
		//iArray[17]=new Array();      
		//iArray[17][0]="WorkPlace";
		//iArray[17][1]="0px";         
		//iArray[17][2]=20;            
		//iArray[17][3]=3;             
		
		//iArray[18]=new Array();      
		//iArray[18][0]="Contact";
		//iArray[18][1]="0px";         
		//iArray[18][2]=20;            
		//iArray[18][3]=3;           
		
		//iArray[19]=new Array();  
		//iArray[19][0]="Operator";   
		//iArray[19][1]="0px";             
		//iArray[19][2]=20;        
		//iArray[19][3]=3;   
		
				

    
    LDDoctorGrid = new MulLineEnter( "fm" , "LDDoctorGrid" ); 
    //这些属性必须在loadMulLine前

    LDDoctorGrid.mulLineCount = 0;   
    LDDoctorGrid.displayTitle = 1;
    LDDoctorGrid.hiddenPlus = 1;
    LDDoctorGrid.hiddenSubtraction = 1;
    LDDoctorGrid.canSel = 1;
    LDDoctorGrid.canChk = 0;
    //LDDoctorGrid.selBoxEventFuncName = "showOne";

    LDDoctorGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDoctorGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
