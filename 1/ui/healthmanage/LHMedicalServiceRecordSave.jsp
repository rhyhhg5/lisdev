<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHMedicalServiceRecordSave.jsp
//程序功能：
//创建日期：2005-04-22 09:47:51
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
 
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHMedicalServiceRecordSchema tLHMedicalServiceRecordSchema   = new LHMedicalServiceRecordSchema();
  OLHMedicalServiceRecordUI tOLHMedicalServiceRecordUI   = new OLHMedicalServiceRecordUI();
  DutyItemSchema tDutyItemSchema = new DutyItemSchema();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHMedicalServiceRecordSchema.setRecordNo(request.getParameter("RecordNo"));
    tLHMedicalServiceRecordSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLHMedicalServiceRecordSchema.setFirstRecoDate(request.getParameter("FirstRecoDate"));
    tLHMedicalServiceRecordSchema.setFirstRecoTime(request.getParameter("FirstRecoTime"));
    tLHMedicalServiceRecordSchema.setServiceState(request.getParameter("ServiceState"));
    tLHMedicalServiceRecordSchema.setApplyDutyItemCode(request.getParameter("ApplyDutyItemCode"));
    tLHMedicalServiceRecordSchema.setDescriptionDetail(request.getParameter("DescriptionDetail"));
    tLHMedicalServiceRecordSchema.setServiceDate(request.getParameter("ServiceDate"));
    tLHMedicalServiceRecordSchema.setServiceTime(request.getParameter("ServiceTime"));
    tLHMedicalServiceRecordSchema.setHospitCode(request.getParameter("HospitCode"));
    tLHMedicalServiceRecordSchema.setDoctNo(request.getParameter("DoctNo"));
    tLHMedicalServiceRecordSchema.setApplyReceiveType(request.getParameter("ApplyReceiveType"));
    tLHMedicalServiceRecordSchema.setServiceFlag(request.getParameter("ServiceFlag"));
    tLHMedicalServiceRecordSchema.setIsBespeakOk(request.getParameter("IsBespeakOk"));
    tLHMedicalServiceRecordSchema.setBespeakServiceItem(request.getParameter("BespeakServiceItem"));
    tLHMedicalServiceRecordSchema.setApplyServiceDes(request.getParameter("ApplyServiceDes"));
    tLHMedicalServiceRecordSchema.setBespeakServiceDate(request.getParameter("BespeakServiceDate"));
    tLHMedicalServiceRecordSchema.setBespeakServiceTime(request.getParameter("BespeakServiceTime"));
    tLHMedicalServiceRecordSchema.setBespeakServiceHospit(request.getParameter("BespeakServiceHospit"));
    tLHMedicalServiceRecordSchema.setBespeakServiceDoctNo(request.getParameter("BespeakServiceDoctNo"));
    tLHMedicalServiceRecordSchema.setNoCompareCause(request.getParameter("NoCompareCause"));
    tLHMedicalServiceRecordSchema.setBespeaLostCause(request.getParameter("BespeaLostCause"));
    tLHMedicalServiceRecordSchema.setServiceExecState(request.getParameter("ServiceExecState"));
    tLHMedicalServiceRecordSchema.setExcuteServiceItem(request.getParameter("ExcuteServiceItem"));
    tLHMedicalServiceRecordSchema.setExcuteDescription(request.getParameter("ExcuteDescription"));
    tLHMedicalServiceRecordSchema.setExcuteServiceDate(request.getParameter("ExcuteServiceDate"));
    tLHMedicalServiceRecordSchema.setExcuteServiceTime(request.getParameter("ExcuteServiceTime"));
    tLHMedicalServiceRecordSchema.setExcuteServiceHospit(request.getParameter("ExcuteServiceHospit"));System.out.println(request.getParameter("ExcuteServiceHospit"));
    tLHMedicalServiceRecordSchema.setExcuteServiceDocNO(request.getParameter("ExcuteServiceDocNo"));
    tLHMedicalServiceRecordSchema.setExNoCompareCause(request.getParameter("ExNoCompareCause"));
    tLHMedicalServiceRecordSchema.setExcutePay(request.getParameter("ExcutePay"));
    tLHMedicalServiceRecordSchema.setSatisfactionDegree(request.getParameter("SatisfactionDegree"));
    tLHMedicalServiceRecordSchema.setOtherServiceInfo(request.getParameter("OtherServiceInfo"));
    tLHMedicalServiceRecordSchema.setCancleServiceCause(request.getParameter("CancleServiceCause"));
//    tLHMedicalServiceRecordSchema.setOperator(request.getParameter("Operator"));
    tLHMedicalServiceRecordSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHMedicalServiceRecordSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHMedicalServiceRecordSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHMedicalServiceRecordSchema.setModifyTime(request.getParameter("ModifyTime"));  
    tLHMedicalServiceRecordSchema.setIsBookSame(request.getParameter("IsApplaySame"));   
    tLHMedicalServiceRecordSchema.setIsExecSame(request.getParameter("IsExecSame"));
    tLHMedicalServiceRecordSchema.setManageCom(tG.ComCode);
    
    tDutyItemSchema.setDutyItemName(request.getParameter("DutyItemName"));
    System.out.println("---------tLHMedicalServiceRecordSchema11");
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  
	  tVData.add(tDutyItemSchema);
	  tVData.add(tLHMedicalServiceRecordSchema);
  	tVData.add(tG); System.out.println("---------tLHMedicalServiceRecordSchema");
    tOLHMedicalServiceRecordUI.submitData(tVData,transact);	
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHMedicalServiceRecordUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
