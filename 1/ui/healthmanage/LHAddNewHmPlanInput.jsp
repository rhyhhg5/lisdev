<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHAddNewHmPlanInput.jsp
//程序功能：
//创建日期：2006-06-26 13:46:39
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHAddNewHmPlanInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHAddNewHmPlanInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LHAddNewHmPlanSave.jsp"  method=post name=fm target="fraSubmit">
    <br>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHHealthServItem1);">
    		</td>
    		 <td class= titleImg>
        		 新增健管服务事件信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHHealthServItem1" style= "display: ''">
<table  class= common align='center' >

  <TR  class= common>
    <TD  class= title>
		服务事件类型
    </TD>
		<TD  class= input>
			<Input type=hidden name=ServEventType value='2'>
			
		 <Input class= 'common' style="width:155px"  name=ServEventTypeName verify="服务事件类型|NOTNULL&len<=20" value=集体服务事件>
	  </TD>
    <TD  class= title>
    	服务事件编号
    </TD>
	  <TD  class= input>
    	<Input class= 'common' style="width:160px"  name=ServCaseCode>
    </TD> 
    <TD  class= title>
		服务事件名称
	</TD>
	<TD  class= input>
    	<Input class= 'common' style="width:160px"  name=ServCaseName>
    </TD> 
  </TR>
  </TR>
</table>
   <br>
    <table>
    	<tr>
       		 <td>
       		 	<input type= button class=cssButton name=ServTrace value="保 存" OnClick="submitForm();">
       		 </td>   
       		  <td>
       		 	   <input type= button class=cssButton name=ServTrace value="返 回" OnClick="returnParent();">
       		 </td>   		 
    	</tr>
    </table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden  id="ServItemNo" name="ServItemNo">
    <input type=hidden  id="ServItemCode" name="ServItemCode">
    <input type=hidden id="ServPlanNo" name="ServPlanNo">
    <input type=hidden id="ServPlanName" name="ServPlanName">
    <input type=hidden id="ComID" name="ComID">
    <input type=hidden id="ServPlanLevel" name="ServPlanLevel">
    <input type=hidden id="ContNo" name="ContNo">
    <input type=hidden id="CustomerNo" name="CustomerNo">
    <input type=hidden  id="CustomerName" name="CustomerName">
    <input type=hidden  id="HiddenCaseCode" name="HiddenCaseCode">
    <input  type=hidden  id="ServItemName" name="ServItemName">
    <input  type=hidden  id="ServCaseState" name="ServCaseState">
    <input  type=hidden  id="Date2" name="Date2">
    <input  type=hidden  id="EventCode" name="EventCode">
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
