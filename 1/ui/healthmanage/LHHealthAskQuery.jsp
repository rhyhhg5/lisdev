<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-06-18 09:08:27
//创建人  ：
//更新记录：  更新人  	  更新日期     更新原因/内容 重新布局
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHHealthAskQuery.js"></SCRIPT>

</head>

<body  onload="" >
  <BR>
  <form action="" method=post name=fm target="fraSubmit">
  	<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,basicInfo1);">
    		</td>
    		<td class= titleImg>
        		 查询条件
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "basicInfo1" style= "display: ''">
      <table  class= common align='center' style= "width: '94%'">
  				<TR  class= common>
		          <TD  class= title>
		      			客户号
				    	</TD>
				    	<TD  class= input>    
				    		<Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号|notnull&len<=24" ondblclick="return queryCustomerNo();" >
					    </TD>
					    <TD  class= title>
					      客户姓名
					    </TD>
					    <TD  class= input>
					      <Input class= 'common' name=CustomerName  verify="客户姓名|len<=20">
					    </TD>
					    <TD  class= title>
                咨询时间
              </TD>
              <TD  class= input>
                <Input type=hidden name=LogDate_h>
              	<Input style='width=20' class= 'code' name=LogDate_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,LogDate_h],[1,1],null,null,null,1,75);" >
              	<Input style='width=114' class= 'coolDatePicker' name=LogDate >                
              </TD>
          </TR>
          <TR  class= common>
          		<TD class = title>
								咨询作业状态
							</TD>
							<TD class = input>
								<input type=hidden name=ReplyState>
								<Input class = 'code' name =ReplyState_ch verify="咨询作业状态|len<=4" ondblClick= "showCodeList('hmreplystate',[this,ReplyState],[0,1],null,null,null,1);" >
							</TD>
							<TD class = title>
								客户咨询方式
							</TD>
							<TD class = input>
								<Input type=hidden name=AskMode >
          		  <Input class= 'code' name=AskModeName ondblclick="return showCodeList('hmaskmode',[this,AskMode],[0,1],null,null,null,1);" verify="客户咨询方式|len<=20">            
							</TD>
							<TD class = title>
								咨询问题类别
							</TD>
							<TD class = input>
								<Input type=hidden name=AskStyle>
           			<Input class= 'code' name=AskStyle_ch ondblclick=" return showCodeList('hmconsulttype',[this,AskStyle],[1,0],null,null,null,1);" >
							</TD>
          </TR>
      </table>
  				<br>
  		<table class= common align='center' style= "width: '94%'">		
  				<TR  class= common>
           		<TD  class= title>
				      	解答方式
				      </TD>
				      <TD  class= input>
						    	<Input  type=hidden name=AnswerMode >
          				<Input class= 'code' name=AnswerMode_ch ondblclick="return showCodeList('hmaskmode',[this,AnswerMode],[0,1],null,null,null,1);" verify="解答方式|len<=20">
	   					</TD>
					    <TD class = title >
					    	约定回复方式
					    </TD>
					    <TD class = input>
									<Input type=hidden name=PromiseAnswerType >
            			<Input class= 'code' name=PromiseAnswerType_ch ondblclick="return showCodeList('hmaskmode',[this,PromiseAnswerType],[0,1]);"  verify="约定回复方式|len<=20">
	   					</TD>
				   	  <TD  class= title>
					      咨询总延时	
					    </TD>
					    <TD  class= input>
					    	<Input type=hidden name=AskTotalTime_h>                                                                                                                                                                                
								<Input style='width=20' class= 'code' name=AskTotalTime_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,AskTotalTime_h],[1,1],null,null,null,1,75);" >  
								<Input style='width=134' class= 'common' name=AskTotalTime >                                                                                                                                                           

					    </TD>   
          </TR>
          <TR class= common>
          		<TD  class= title>
					      是否即时答疑
					    </TD>
					    <TD  class= input>
					       <Input type=hidden name=AnswerType>
	      				 <Input class = 'code' name=AnswerType_ch verify="是否即时答疑|len<=2" CodeData= "0|^1|是^2|否"  ondblClick= "showCodeListEx('AnswerType',[this,AnswerType],[1,0]);" >
	      			</TD>
					    <TD  class= title>
					      延时答疑方式
					    </TD>
					    <TD  class= input>
					      <input type=hidden name=DelayAnswerType>
	       				<Input class = 'code' name=DelayAnswerType_ch verify="延时答疑方式|len<=10" CodeData= "0|^1|短时延时^2|长时延时"  ondblClick= "showCodeListEx('DelayAnswerType',[this,DelayAnswerType],[1,0],null,null,null,1);" >
	       			</TD>
					    <TD class = title>
								
							</TD>
							<TD class = input>
							</TD>
          </TR>
          <TR class= common>
          		<TD  class= title>
					      解答花费时间
					    </TD>
					    <TD  class= input>
					      <Input type=hidden name=AnswerCostTime_h>
              	<Input style='width=20' class= 'code' name=AnswerCostTime_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,AnswerCostTime_h],[1,1],null,null,null,1,75);" >
              	<Input style='width=134' class= 'common' name=AnswerCostTime > 
					    </TD>
					    <TD  class= title>
					      预约剩余时间
					    </TD>
					    <TD  class= input>
					      <Input type=hidden name=BookLeftTime_h>
              	<Input style='width=20' class= 'code' name=BookLeftTime_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,BookLeftTime_h],[1,1],null,null,null,1,75);" >
              	<Input style='width=134' class= 'common' name=BookLeftTime >
					    </TD>
					    <TD class = title>
								
							</TD>
							<TD class = input>
							</TD>
          </TR>
       </table>		
    </Div>
    <BR>
  	<hr>
  	<Input type=button class=cssbutton value= "查询" onclick="query();">
  	<Input type=button class=cssbutton value= "重置" onclick="clearInput();"> 
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>