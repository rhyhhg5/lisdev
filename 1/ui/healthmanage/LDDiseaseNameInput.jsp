<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-05-24 10:05:39
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="LDDiseaseNameInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDDiseaseNameInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDDiseaseNameSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 疾病通用名称对照表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDDiseaseName1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      疾病俗称
    </TD>
    <TD  class= input>
      <Input class= 'common' elementtype=nacessary name=DiseaseCommonName verify="疾病俗称|notnull&len<=30" >
    </TD>
    <TD  class= title>
      疾病ICD代码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ICDCode elementtype=nacessary ondblclick=" return showCodeList('lddiseasename',[this,ICDName],[0,1],null,fm.ICDCode.value,'ICDCode',1,350);" onkeyup=" if(event.keyCode == 13) return showCodeList('lddiseasename',[this,ICDName],[0,1],null,fm.ICDCode.value,'ICDCode',1,350);" verify="疾病ICD代码|notnull&len<=18" >
    </TD>
    <TD  class= title>
      疾病标准名称
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ICDName  ondblclick=" return showCodeList('lddiseasename',[this,ICDCode],[1,0],null,fm.ICDName.value,'ICDName',1,350);" onkeyup=" if(event.keyCode == 13) return showCodeList('lddiseasename',[this,ICDCode],[1,0],null,fm.ICDName.value,'ICDName',1,350);" verify="疾病标准名称|len<=30" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      维护标记
    </TD>
    <TD  class= input>
      <Input class=codeno name=MainteinFlag CodeData="0|^0|未维护^1|已维护 "; ondblclick="  showCodeListEx('',[this,MainteinFlag_ch],[0,1],null,null,null,1);" ><Input class= 'codename' name=MainteinFlag_ch >
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
