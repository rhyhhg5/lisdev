<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHGrpPerServExeTraceInput.jsp
//程序功能：
//创建日期：2006-03-09 14:31:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<% String GrpServPlanNo=request.getParameter("GrpServPlanNo");%>
<head >

	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="LHGrpPerServExeTraceInput.js"></SCRIPT>
  <%@include file="LHGrpPerServExeTraceInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LHGrpPerExeTraceSave.jsp" method=post name=fm target="fraSubmit">
 <span id="operateButton" >
	<table  class=common align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
				<td class=button width="10%" align=right>
				<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
			</td>
		</tr>
	</table>
</span>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 个单项目执行轨迹信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHGrpPerServExeTrace1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
			    <TD  class= title>
			      客户号码
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=CustomerNo>
			    </TD>
			    <TD  class= title>
			       客户姓名
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=Name>
			    </TD>
			    <TD  class= title>
			      标准服务项目代码
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServItemCode>
			    </TD>
		  	</TR>
		  	<TR  class= common>
			    <TD  class= title>
			      标准服务项目名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=servitemname>
			    </TD>
			    <TD  class= title>
			      服务项目序号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServItemSerial>
			    </TD>
			</TR>
		</table>
    </Div>
     <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=Button   class=cssButton onclick="easyQueryClick();">				
      </TD>
    </TR>
  </table>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServItemGrid);">
    		</td>
    		 <td class= titleImg>
        		 项目执行状态信息
       		 </td>   		 
    	</tr>
    </table>
    
	<Div id="divLHGrpPerServExeTraceGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHGrpPerServExeTraceGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
    
    
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <Input type=hidden name=ManageCom >
	<Input type=hidden name=Operator >
	  
    <Input type=hidden name=MakeDate >
    <Input type=hidden name=MakeTime >
    <Input type=hidden name=ModifyDate >
    <Input type=hidden name=ModifyTime >
    <Input type=hidden  name=GrpServPlanNo value = <%=GrpServPlanNo%>><!--团体服务计划号码-->

  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
		<script>
		//通过综合查询进入此页面的处理
		var turnPage = new turnPageClass(); 
		function passQuery()
		{ 	
			  //进入此函数
			  		  
					  var GrpServPlanNo = "<%=request.getParameter("GrpServPlanNo")%>";
					
					
				  	if(GrpServPlanNo!=null||GrpServPlanNo>0) 
				  	{
				  			var strSql2 = "select ww.*,xx.* from ( select distinct c.CustomerNo as aa, c.Name as bb, b.ServItemCode as cc, "
				  									 +" (select distinct a.servitemname from LHHealthServItem a where a.servitemcode=b.ServItemCode) as dd,b.GrpServItemNo as ff ,b.ServItemSerial as gg "
      											 +" from LHGrpPersonUnite c,LHGrpServItem b"
				  									 +" where  "
				  									 +" b.GrpServPlanNo = '"+GrpServPlanNo+"'"
				  									 +" and c.GrpServPlanNo = '"+GrpServPlanNo+"' and b.OrgType='1') as ww"
      											// +" order by b.ServItemCode"
      											+" left outer join "
      											+" "
      											+" (select d.ExeState aa ,d.ExeDate bb, d.ServDesc cc , "
      											+" d.MakeDate dd , d.MakeTime ee , d.ContNo ff , d.ServItemNo gg, d.ServPlanNo ii "
      											+" from LHServExeTrace d) as xx "
      											+" on (xx.ff = ww.aa and  ww.ff = xx.ii)"
      											//+" on d.ContNo = c.CustomerNo and d.ServPlanNo = b.ServItemCode "     
      										 ;
                      //alert(strSql2);
                      turnPage.pageLineNum = 30;
      				  turnPage.queryModal(strSql2,LHGrpPerServExeTraceGrid); 
								
					  } 
					  else 
					  {
					  	 // alert("LHGrpPerMainCustomerHealth.js->没有此个人服务");
					  }		    
	}
		
	</script>
</html>
