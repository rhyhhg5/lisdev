//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var StrSql;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHIndivServAcceptInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
    //showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHServAcceptQuery.html","服务受理信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}  
function ServCaseQuery()
{
	if(fm.all('ServItemCode').value==""||fm.all('ServItemName').value=="")
	{
		 alert("请选择要受理的服务项目信息!");
	   if( verifyInput2() == false ) return false;
	   fm.all('ServItemName').focus();
		 return false;
	}
	    var strSql=" select a.ServItemCode "
		             +" from LHIndiServAccept a"
		             +" where a.ServItemCode='"+ fm.all('ServItemCode').value+"'";
		   //alert(strSql);
		   //alert(easyExecSql(strSql));
		   var itemCode=easyExecSql(strSql);
		   if(itemCode==""||itemCode=="null"||itemCode==null)
		   {
		       var ServItemName="";
  	       if(fm.all('ServItemName').value != "")
  	       {
  	       	  ServItemName =" and b.ServItemCode in ( select distinct c.ServItemCode from   LHHealthServItem c  where  ServItemName = '"+fm.all('ServItemName').value+"')";	
  	       }
		       var strSql=" select distinct  b.ServCaseCode ,"
		                 +" (select a.ServCaseName from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode),"
		                 +" ''"
	                   +"  from LHServCaseRela b "
                     +" where  "
                     +"  1=1  "
                     +" and b.ServCaseCode in ( select distinct c.ServCaseCode from   LHServCaseDef c  where  ServCaseState='1')"
                     +getWherePart("b.CustomerNo","CustomerNo")  
                     +getWherePart("b.CustomerName","CustomerName")
                     +getWherePart("b.ServItemCode","ServItemCode")
                     +ServItemName
                     ;
            //alert(strSql);
            //alert(easyExecSql(strSql));
            if(easyExecSql(strSql)!=""&&easyExecSql(strSql)!="null"&&easyExecSql(strSql)!=null)
            {
              turnPage.queryModal(strSql, LHServCaseGrid);
            }
            if(easyExecSql(strSql)==""||easyExecSql(strSql)==null||easyExecSql(strSql)=="null")
            {
       	       alert("此查询条件无服务事件信息，因此无法进行服务受理!");
       	       fm.all('saveButton').disabled =true;	
       	       fm.all('modifyButton').disabled =true;	
       	       fm.all('querybutton').disabled =true;	
       	       fm.all('deleteButton').disabled =true;	
       	       fm.all('ServTaskSend').disabled =true;	
       	       initLHServCaseGrid();
       	       return false;
            }
         }
         if(itemCode!=""&&itemCode!="null"&&itemCode!=null)    
         {
         	     if(fm.all('ServItemCode').value=="004")//健康体检服务
         	     {       
         	     	  var ServItemName="";
  	              if(fm.all('ServItemName').value != "")
  	              {
  	              	  ServItemName =" and b.ServItemCode in ( select distinct c.ServItemCode from   LHHealthServItem c  where  ServItemName = '"+fm.all('ServItemName').value+"')";	
  	              } 
                  var mulSql2 = "select  a.ServCaseCode,"
                          +"(select d.ServCaseName from LHServCaseDef d where d.ServCaseCode=a.ServCaseCode),"
                          +" a.ServAccepNo"
				   				        +" from LHIndiServAccept a "
				   				        +" where   a.ServCaseCode in (select d.ServCaseCode from LHServCaseRela  d where d.ServItemCode='"+ fm.all('ServItemCode').value +"')"
				   				        +getWherePart("a.CustomerNo","CustomerNo")  
				   				        +" union"
				   				        +" select distinct b.ServCaseCode ,"
		                      +" (select a.ServCaseName from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode),"
		                      +" ''"
	                        +"  from LHServCaseRela b "
                          +" where b.ServItemCode='"+ fm.all('ServItemCode').value +"' "
                          +" and  b.ServCaseCode not in (select ServCaseCode from LHIndiServAccept)"
                          +"and b.ServCaseCode in ( select distinct c.ServCaseCode from   LHServCaseDef c  where  ServCaseState='1')"
                          +getWherePart("b.CustomerNo","CustomerNo")  
                          +getWherePart("b.CustomerName","CustomerName")
                          +getWherePart("b.ServItemCode","ServItemCode")
                          +ServItemName
                          ;
				         //alert(mulSql2);
				         //alert(easyExecSql(mulSql2));
				         turnPage.queryModal(mulSql2, LHServCaseGrid);  
				       } 
				       if(fm.all('ServItemCode').value=="010")//绿色通道服务
         	     {      
         	     	  var ServItemName="";
  	              if(fm.all('ServItemName').value != "")
  	              {
  	              	  ServItemName =" and b.ServItemCode in ( select distinct c.ServItemCode from   LHHealthServItem c  where  ServItemName = '"+fm.all('ServItemName').value+"')";	
  	              }   
                  var mulSql2 = "select  a.ServCaseCode,"
                          +"(select d.ServCaseName from LHServCaseDef d where d.ServCaseCode=a.ServCaseCode),"
                          +" a.ServAccepNo"
				   				        +" from LHIndiServAccept a "
				   				        +" where   a.ServCaseCode in (select d.ServCaseCode from LHServCaseRela  d where d.ServItemCode='"+ fm.all('ServItemCode').value +"')"
				   				        +getWherePart("a.CustomerNo","CustomerNo")  
				   				        +" union"
				   				        +" select distinct b.ServCaseCode ,"
		                      +" (select a.ServCaseName from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode),"
		                      +" ''"
	                        +"  from LHServCaseRela b "
                          +" where b.ServItemCode='"+ fm.all('ServItemCode').value +"' "
                          +" and  b.ServCaseCode not in (select ServCaseCode from LHIndiServAccept)"
                          +"and b.ServCaseCode in ( select distinct c.ServCaseCode from   LHServCaseDef c  where  ServCaseState='1')"
                          +getWherePart("b.CustomerNo","CustomerNo")  
                          +getWherePart("b.CustomerName","CustomerName")
                          +getWherePart("b.ServItemCode","ServItemCode")
                          +ServItemName
				         //alert(mulSql2);
				         //alert(easyExecSql(mulSql2));
				         turnPage.queryModal(mulSql2, LHServCaseGrid);  
				       }
         	    // if(fm.all('ServItemCode').value=="015")//预约类受理服务
         	    // {        
         	    // 	 	var ServItemName="";
  	          //    if(fm.all('ServItemName').value != "")
  	          //    {
  	          //    	  ServItemName =" and b.ServItemCode in ( select distinct c.ServItemCode from   LHHealthServItem c  where  ServItemName = '"+fm.all('ServItemName').value+"')";	
  	          //    } 
              //    var mulSql2 = "select a.ServCaseCode,"
              //            +"(select d.ServCaseName from LHServCaseDef d where d.ServCaseCode=a.ServCaseCode),"
              //            +" a.ServAccepNo"
				   		//		        +" from LHIndiServAccept a "
				   		//		        +" where   a.ServCaseCode in (select d.ServCaseCode from LHServCaseRela  d where d.ServItemCode='"+ fm.all('ServItemCode').value +"' )"
				   		//		        +getWherePart("a.CustomerNo","CustomerNo")  
				   		//		        +" union"
				   		//		        +" select distinct b.ServCaseCode ,"
		          //            +" (select a.ServCaseName from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode),"
		          //            +" ''"
	            //            +"  from LHServCaseRela b "
              //            +" where b.ServItemCode='"+ fm.all('ServItemCode').value +"' "
              //            +" and  b.ServCaseCode not in (select ServCaseCode from LHIndiServAccept)"
              //            +getWherePart("b.CustomerNo","CustomerNo")  
              //            +getWherePart("b.CustomerName","CustomerName")
              //            +getWherePart("b.ServItemCode","ServItemCode")
              //            +ServItemName
				      //   //alert(mulSql2);
				      //   //alert(easyExecSql(mulSql2));
				      //   turnPage.queryModal(mulSql2, LHServCaseGrid);  
				      // } 
				       //alert(fm.all('ServItemCode').value);
				       if(fm.all('ServItemCode').value!="004"&&fm.all('ServItemCode').value!="010")//其它类服务
         	     { 
         	       	var ServItemName="";
  	              if(fm.all('ServItemName').value != "")
  	              {
  	              	  ServItemName =" and b.ServItemCode in ( select distinct c.ServItemCode from   LHHealthServItem c  where  ServItemName = '"+fm.all('ServItemName').value+"')";	
  	              } 
         	     	   var mulSql5 = "select a.ServCaseCode,"
                          +" (select d.ServCaseName from LHServCaseDef d where d.ServCaseCode=a.ServCaseCode),"
                          +"  a.ServAccepNo "
				   				        +" from LHIndiServAccept a  "
				   				        +" where   a.ServCaseCode in (select d.ServCaseCode from LHServCaseRela  d where d.ServItemCode='"+ fm.all('ServItemCode').value +"')"
				   				        + getWherePart("a.CustomerNo","CustomerNo")  
				   				        +"  union "
				   				        +"  select distinct b.ServCaseCode ,"
		                      +"  (select a.ServCaseName from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode),"
		                      +" ''"
	                        +"   from LHServCaseRela b  "
                          +"  where b.ServItemCode='"+ fm.all('ServItemCode').value +"' "
                          +"  and  b.ServCaseCode not in (select ServCaseCode from LHIndiServAccept)"
                          +"and b.ServCaseCode in ( select distinct c.ServCaseCode from   LHServCaseDef c  where  ServCaseState='1')"
                          +getWherePart("b.CustomerNo","CustomerNo")  
                          +getWherePart("b.CustomerName","CustomerName")
                          +getWherePart("b.ServItemCode","ServItemCode")
                          +ServItemName
				          //alert(mulSql5);
				           //alert(easyExecSql(mulSql5));
				          turnPage.queryModal(mulSql5, LHServCaseGrid);  
         	     }
				      // if(fm.all('ServItemCode').value=="017")//其它类服务
         	    // { 
         	    //   	var ServItemName="";
  	          //    if(fm.all('ServItemName').value != "")
  	          //    {
  	          //    	  ServItemName =" and b.ServItemCode in ( select distinct c.ServItemCode from   LHHealthServItem c  where  ServItemName = '"+fm.all('ServItemName').value+"')";	
  	          //    } 
         	    // 	   var mulSql2 = "select a.ServCaseCode,"
              //            +"(select d.ServCaseName from LHServCaseDef d where d.ServCaseCode=a.ServCaseCode),"
              //            +" a.ServAccepNo"
				   		//		        +" from LHIndiServAccept a "
				   		//		        +" where   a.ServCaseCode in (select d.ServCaseCode from LHServCaseRela  d where d.ServItemCode='"+ fm.all('ServItemCode').value +"')"
				   		//		        + getWherePart("a.CustomerNo","CustomerNo")  
				   		//		        +" union"
				   		//		        +" select distinct b.ServCaseCode ,"
		          //            +" (select a.ServCaseName from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode),"
		          //            +" ''"
	            //            +"  from LHServCaseRela b "
              //            +" where b.ServItemCode='"+ fm.all('ServItemCode').value +"' "
              //            +" and  b.ServCaseCode not in (select ServCaseCode from LHIndiServAccept)"
              //            +getWherePart("b.CustomerNo","CustomerNo")  
              //            +getWherePart("b.CustomerName","CustomerName")
              //            +getWherePart("b.ServItemCode","ServItemCode")
              //            +ServItemName
				      //    //alert(mulSql2);
				      //    //alert(easyExecSql(mulSql2));
				      //    turnPage.queryModal(mulSql2, LHServCaseGrid);  
         	    // }
         }
}
//提交，保存按钮对应操作
function submitForm()
{
	 if (LHServCaseGrid.getSelNo() >= 1)
	 {
	     if(fm.all('ServAccepNo').value!=""&fm.all('ServAccepNo').value!=null&&fm.all('ServAccepNo').value!="null")
	     {
	     	 alert("此服务事件已经过受理，不允许重复受理!");
	     	 return false;
	     }
       var i = 0;
       if( verifyInput2() == false ) return false;
       fm.fmtransact.value="INSERT||MAIN";
       var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
       showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
       fm.action="./LHIndivServAcceptSave.jsp";
       fm.submit(); //提交
   }
   else
   {
   	   alert("请选择要进行受理的服务事件!");
   	   return false;
   }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
function showOne()
{
	  //alert(fm.all('ServItemCode').value);
	  if (LHServCaseGrid.getSelNo() >= 1)
		{ 
			  var ServCaseCode=LHServCaseGrid.getRowColData(LHServCaseGrid.getSelNo()-1,1);  
			  var mulItemNo = "select a.ServItemNo"
						    				 +" from LHServCaseRela a "
						    				 +" where a.ServCaseCode ='"+ServCaseCode+"' "
						    				 +" AND ServItemCode='"+ fm.all('ServItemCode').value +"' ";
                //alert(mulItemNo);
                var ServItemNo=easyExecSql(mulItemNo);
                //alert(ServItemNo);
                //var ItemNo = null;
	              //ItemNo = new Array();
                // ItemNo=ServItemNo.toString().split(",");
        if(ServItemNo!=""&&ServItemNo!=null&&ServItemNo!="null")
        {
        	 fm.all('ServItemNo').value=ServItemNo;
        }
                
			  var ServAccepNo=LHServCaseGrid.getRowColData(LHServCaseGrid.getSelNo()-1,3); 
	      fm.all('ServCaseCode').value=ServCaseCode;
	      fm.all('ServAccepNo').value=ServAccepNo;
	      if(fm.all('ServAccepNo').value!=""&&fm.all('ServAccepNo').value!=null&&fm.all('ServAccepNo').value!="null")
	      {
	      	 
	     	       var mulSql = "select a.TaskModelType"
						    				 +" from LHIndiServAccept a "
						    				 +" where a.ServAccepNo ='"+fm.all('ServAccepNo').value+"' ";
                //alert(mulSql);
                arrResult=easyExecSql(mulSql);
                //alert(arrResult);
             if(arrResult[0][0]=="1")//预约服务受理
	      	   {
	      	   	   divLHBespeakServAccdeptGrid.style.display='';//服务受理列表
		             divLHNoteServAccdeptGrid.style.display='none';//短信息受理列表
		             divLHOtherServAccdeptGrid.style.display='none';//其他受理信息列表
		             divLHTaskSettingFGrid.style.display='';//任务信息列表
		             var mulSql = "select a.BespeakAcceptType,a.BespeakComID,"
	     	                +" a.BespeakDateRequest,"
				                +"a.CusServRequest,a.TaskModelType,a.ServTaskCode ,"
				                +" a.ServAceptDate ,a.ModelTypeNo"
						    				 +" from LHIndiServAccept a "
						    				 +" where a.ServAccepNo ='"+fm.all('ServAccepNo').value+"' ";
                 //alert(mulSql);
                 var arrResult2=new Array;
                 arrResult2=easyExecSql(mulSql);
                 //alert(arrResult2);
                 if(arrResult2!=""&&arrResult2!=null&&arrResult2!="null")
                 {
		               //alert("bb "+arrResult[0][1]);
                   fm.all('BespeakAcceptType').value				= arrResult2[0][0];
                   fm.all('BespeakAcceptTypeName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhbespeaktype' and d.code='"+ fm.all('BespeakAcceptType').value +"'");
                   fm.all('HospitCode').value			= arrResult2[0][1];
                   fm.all('HospitName').value=easyExecSql("select  HospitName from LDHospital where LDHospital.HospitCode='"+ fm.all('HospitCode').value +"'");
                   fm.all('BespeakDateRequest').value      = arrResult2[0][2];
                   fm.all('CusOtherRequest').value			= arrResult2[0][3];
                   fm.all('TaskModelType').value			= arrResult2[0][4];
                   //alert(arrResult2[0][5]);
                   //fm.all('ServTaskCode').value			= arrResult2[0][5];
                   //fm.all('ServTaskCode').value			= easyExecSql("select  ServTaskCode from LHCaseTaskRela where LHCaseTaskRela.ServTaskNo='"+ fm.all('ServTaskNo').value +"'");
                   fm.all('ServTaskName').value			= easyExecSql("select  ServTaskName from LHServTaskDef where LHServTaskDef.ServTaskCode='"+ fm.all('ServTaskCode').value +"'");
                   fm.all('ServFinDate').value			= arrResult2[0][6];
                   fm.all('ModelTypeNo').value			= arrResult2[0][7];
                     //if(fm.all('BespeakAcceptType').value=="1")
           	         //{
           	         // fm.all('ServTaskName').value="健康体检预约受理";
           	         //}
           	         //if(fm.all('BespeakAcceptType').value=="2")
           	         //{
           	         // fm.all('ServTaskName').value="绿色通道预约受理";
           	         //}
                 }
             }
             if(arrResult[0][0]=="3")//其它服务受理
             {
            	   divLHBespeakServAccdeptGrid.style.display='none';
		             divLHNoteServAccdeptGrid.style.display='none';
		             divLHOtherServAccdeptGrid.style.display='';
		             divLHTaskSettingFGrid.style.display='';
            	    var mulSql2 = "select "
				                 +" a.CusServRequest,a.TaskModelType,"
				                 +" a.ServTaskCode,a.ServAceptDate,"
				                 +" a.ModelTypeNo "
						    				 +" from LHIndiServAccept a "
						    				 +" where a.ServAccepNo ='"+fm.all('ServAccepNo').value+"' ";
						    	//alert(mulSql2);
						    	var arrResult3=new Array;
                  arrResult3=easyExecSql(mulSql2);
                  //alert(arrResult3);
                  if(arrResult3!=""&&arrResult3!=null&&arrResult3!="null")
                  {
                 	    fm.all('CusServRequest').value			= arrResult3[0][0];
            	        fm.all('TaskModelType').value			= arrResult3[0][1];
            	        //fm.all('ServTaskCode').value			= arrResult3[0][2];
            	        //fm.all('ServTaskName').value			=  fm.all('ServItemName').value+"受理";
            	        fm.all('ServFinDate').value			= arrResult3[0][3];
            	        fm.all('ModelTypeNo').value			= arrResult3[0][4];
            	    }
            	    
             }
	      }
	       if(fm.all('ServAccepNo').value==""||fm.all('ServAccepNo').value==null||fm.all('ServAccepNo').value=="null")
	       {
	       	  if(fm.TaskModelType.value="1")
	       	  {
	       	  	     fm.all('BespeakAcceptType').value				= "";
                   fm.all('BespeakAcceptTypeName').value="";
                   fm.all('HospitCode').value			= "";
                   fm.all('HospitName').value="";
                   fm.all('BespeakDateRequest').value      = "";
                   fm.all('CusOtherRequest').value			= "";
                   fm.all('TaskModelType').value			= "";
                   //fm.all('ServTaskCode').value			= "";
                   //fm.all('ServTaskName').value			= "";
                   fm.all('ServFinDate').value			= "";
                   fm.all('ModelTypeNo').value			= "";
	       	  }
	       	  if(fm.TaskModelType.value="3")
	       	  {
	       	  	 fm.all('CusServRequest').value			= "";
	       	  	 //fm.all('ServTaskCode').value			= "";
               //fm.all('ServTaskName').value			= "";
               fm.all('ServFinDate').value			= "";
               fm.all('ModelTypeNo').value			= "";
	       	  }
	       }
	  } 
		else
		{
				alert("请选择一条服务事件信息！");    
		}
		//if(fm.all('ServItemCode').value=="015")//服务项目名称为预约服务受理
		//{
		//	divLHBespeakServAccdeptGrid.style.display='';//服务受理列表
		//  divLHNoteServAccdeptGrid.style.display='none';//短信息受理列表
		//  divLHOtherServAccdeptGrid.style.display='none';//其他受理信息列表
		//  divLHTaskSettingFGrid.style.display='';//任务信息列表
		//  fm.TaskModelType.value="1";//预约受理模块
		//}
	  if(fm.all('ServItemCode').value=="004")//健康体检服务
		{
			divLHBespeakServAccdeptGrid.style.display='';//服务受理列表
		  divLHNoteServAccdeptGrid.style.display='none';//短信息受理列表
		  divLHOtherServAccdeptGrid.style.display='none';//其他受理信息列表
		  divLHTaskSettingFGrid.style.display='';//任务信息列表
		  fm.TaskModelType.value="1";//预约受理模块
		  fm.all('BespeakAcceptType').value="1";//受理类型
		  fm.all('BespeakAcceptTypeName').value="健康体检服务";//受理类型
		  fm.all('BespeakAcceptTypeName').ondblclick = "";
		}
		if(fm.all('ServItemCode').value=="010")//绿色通道服务
		{
			divLHBespeakServAccdeptGrid.style.display='';//服务受理列表
		  divLHNoteServAccdeptGrid.style.display='none';//短信息受理列表
		  divLHOtherServAccdeptGrid.style.display='none';//其他受理信息列表
		  divLHTaskSettingFGrid.style.display='';//任务信息列表
		  fm.TaskModelType.value="1";//预约受理模块
		  fm.all('BespeakAcceptType').value="2";//受理类型
		  fm.all('BespeakAcceptTypeName').value="绿色通道服务";//受理类型
		  fm.all('BespeakAcceptTypeName').ondblclick = "";
		}
		if(fm.all('ServItemCode').value!="004"&&fm.all('ServItemCode').value!="010")//服务项目名称为其它服务受理
		{
			divLHBespeakServAccdeptGrid.style.display='none';
		  divLHNoteServAccdeptGrid.style.display='none';
		  divLHOtherServAccdeptGrid.style.display='';
		  divLHTaskSettingFGrid.style.display='';
		  fm.TaskModelType.value="3";//预约受理模块
		}
		//if(fm.all('ServItemCode').value=="017")//服务项目名称为其它服务受理
		//{
		//	divLHBespeakServAccdeptGrid.style.display='none';
		//  divLHNoteServAccdeptGrid.style.display='none';
		//  divLHOtherServAccdeptGrid.style.display='';
		//  divLHTaskSettingFGrid.style.display='';
		//  fm.TaskModelType.value="3";//预约受理模块
		//}
	  //if(fm.all('ServItemCode').value=="016")//服务项目名称为短信服务受理
		//{
		//	divLHBespeakServAccdeptGrid.style.display='none';
		//  divLHNoteServAccdeptGrid.style.display='';
		//  divLHOtherServAccdeptGrid.style.display='none';
		//  divLHTaskSettingFGrid.style.display='';
		//  fm.TaskModelType.value="2";//预约受理模块
		//}
		
}
function afterQueryAdd(arrQueryResult)
{
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
    		arrResult = arrQueryResult;
    		//alert("aa  "+arrQueryResult[0][0]);
				var mulSql = "select a.ServAccepNo,a.ServAccepChannel,a.ServAceptDate,"
				            +"a.CustomerNo,a.ServItemCode,a.TaskModelType,a.ModelTypeNo,"
				            +"a.BespeakComID,a.BespeakDateRequest,a.CusServRequest,a.ServTaskCode "
										+" from LHIndiServAccept a where a.ServCaseCode ='"+arrResult[0][0]+"' ";
        //alert(mulSql);
        arrResult=easyExecSql(mulSql);
        // alert(arrResult);
        if(arrResult!=""&&arrResult!=null&&arrResult!="null")
        {
		       //alert("bb "+arrResult[0][1]);
           fm.all('ServAccepNo').value				= arrResult[0][0];
           fm.all('ServAccepChannel').value			= arrResult[0][1];
           fm.all('ServAceptDate').value      = arrResult[0][2];
           fm.all('CustomerNo').value			= arrResult[0][3];
           fm.all('ServItemCode').value			= arrResult[0][4];
           fm.all('TaskModelType').value	= arrResult[0][5];
           fm.all('ModelTypeNo').value		= arrResult[0][6];
           fm.all('HospitCode').value 		= arrResult[0][7];
           fm.all('BespeakDateRequest').value 		= arrResult[0][8];
           //fm.all('CusServRequest').value 		= arrResult[0][9];
           //fm.all('ServTaskCode').value 		= arrResult[0][10];
           fm.all('ServFinDate').value 		= arrResult[0][2];
           if(fm.all('TaskModelType').value=="1")
           {
           	   //if(fm.all('BespeakAcceptType').value=="1")
           	   //{
           	   //   fm.all('ServTaskName').value="健康体检预约受理";
           	   //}
           	   //if(fm.all('BespeakAcceptType').value=="2")
           	   //{
           	   //   fm.all('ServTaskName').value="绿色通道预约受理";
           	   //}
           	   var ssSql = "select a.CusServRequest "
										+" from LHIndiServAccept a where "
										+" a.ModelTypeNo ='"+fm.all('ModelTypeNo').value+"'"
										+" and a.TaskModelType='"+ fm.all('TaskModelType').value + "'";
                //alert(ssSql);
               //alert(easyExecSql(ssSql));
               var arrRR=easyExecSql(ssSql);
               fm.all('CusOtherRequest').value 		= arrRR[0][0]; 
           }
           if(fm.all('TaskModelType').value=="3")
           {
           	  //fm.all('ServTaskName').value="其它预约受理";
           	  //fm.all('ServTaskName').value=fm.all('ServItemName').value+"受理";
           	  var ssSql = "select a.CusServRequest "
										+" from LHIndiServAccept a where "
										+" a.ModelTypeNo ='"+fm.all('ModelTypeNo').value+"'"
										+" and a.TaskModelType='"+ fm.all('TaskModelType').value + "'";
               //alert(ssSql);
               //alert(easyExecSql(ssSql));
               var arrRR=easyExecSql(ssSql);
               if(arrRR!=""&&arrRR!=null&&arrRR!="null")
               {
                 fm.all('CusOtherRequest').value 		= arrRR[0][0]; 
               }
           }
           
           var mulSql2 = "select a.ServCaseCode,"
                   +"(select d.ServCaseName from LHServCaseDef d where d.ServCaseCode=a.ServCaseCode),"
                   +" a.ServAccepNo"
				   				 +" from LHIndiServAccept a "
				   				 +" where a.ServAccepNo ='"+ fm.all('ServAccepNo').value +"'"
				   				 //+" union"
				   				 //+" select b.ServCaseCode ,"
		               //+" (select a.ServCaseName from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode),"
		               //+" ''"
	                 //+"  from LHServCaseRela b "
                   //+" where ServItemCode='"+ fm.all('ServItemCode').value +"' "
                   //+" and b.ServCaseCode not in (select ServCaseCode from LHIndiServAccept)"
                   ;
				  //alert(mulSql2);
				  //alert(easyExecSql(mulSql2));
				  turnPage.queryModal(mulSql2, LHServCaseGrid);      
				}
	}     
}
function afterQueryUpdate(arrQueryResult)
{
	 //afterQuery2(arrQueryResult);
}
function afterQuery2(arrQueryResult)
{
	
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
    		arrResult = arrQueryResult;
    		//alert("aa  "+arrQueryResult[0][7]);
    		fm.all('ServAccepNo').value=arrQueryResult[0][7];
				var mulSql = " select  "
				            +" a.CustomerNo,"
				            +" (select d.Name from ldperson d where d.CustomerNo=a.CustomerNo),"
				            +"  a.ServAccepChannel ,b.ServItemCode ,"
				            +" ( select distinct c.ServItemName from   LHHealthServItem c  where  c.ServItemCode = b.ServItemCode)," 
				            +"  a.ServAceptDate ,a.ServAccepNo ,a.ServCaseCode,"
				            +" a.ModelTypeNo,a.ServTaskCode, a.TaskModelType ,a.BespeakAcceptType"
										+"  from LHIndiServAccept a ,LHServCaseRela b  "
										+"  where a.ServCaseCode=b.ServCaseCode and  "
										+"  a.ServItemCode=b.ServItemCode and  "
										+" a.ServAccepNo ='"+arrQueryResult[0][7]+"' ";
       //alert(mulSql);
       arrResult=easyExecSql(mulSql);
       //alert(arrResult);
       //alert(arrResult[0][3]);
       if(arrResult!=""&&arrResult!=null&&arrResult!="null")
       {
		      //alert("bb "+arrResult[0][1]);
		      fm.all('CustomerNo').value			= arrResult[0][0];
		      fm.all('CustomerName').value			= arrResult[0][1];
          fm.all('ServAccepChannel').value			= arrResult[0][2];
          fm.all('ServAccepChannelName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhacceptchannel' and d.code='"+ fm.all('ServAccepChannel').value +"'");
          fm.all('ServItemCode').value      = arrResult[0][3];
          fm.all('ServItemName').value			= arrResult[0][4];
          fm.all('ServAceptDate').value	= arrResult[0][5];
          fm.all('ServAccepNo').value				= arrResult[0][6];
          fm.all('ServCaseCode').value			= arrResult[0][7];
          fm.all('ModelTypeNo').value			= arrResult[0][8];
          //fm.all('ServTaskCode').value			= arrResult[0][9];
          //fm.all('ServTaskCode').value			= easyExecSql("select  ServTaskCode from LHCaseTaskRela where LHCaseTaskRela.ServTaskNo='"+ fm.all('ServTaskNo').value +"'");
          //fm.all('ServTaskName').value			= easyExecSql("select  ServTaskName from LHServTaskDef where LHServTaskDef.ServTaskCode='"+ fm.all('ServTaskCode').value +"'");
          fm.all('TaskModelType').value       = arrResult[0][10];
          fm.all('BespeakAcceptType').value       = arrResult[0][11];
          //alert(fm.all('BespeakAcceptType').value);
          fm.all('BespeakAcceptTypeName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhbespeaktype' and d.code='"+ fm.all('BespeakAcceptType').value +"'");
          //alert(fm.all('BespeakAcceptTypeName').value);
          fm.all('ServFinDate').value			= arrResult[0][5];
          // alert(fm.all('TaskModelType').value);
          fm.all('ServTaskNO').value			= arrResult[0][9];
           if(fm.all('TaskModelType').value=="1")
           {
           	   //alert(fm.all('BespeakAcceptType').value);
           	   if(fm.all('BespeakAcceptType').value=="1")
           	   {
           	   	  fm.all('BespeakAcceptTypeName').ondblclick = "";
           	      //fm.all('ServTaskName').value="健康体检预约受理";
           	   }
           	   if(fm.all('BespeakAcceptType').value=="2")
           	   {
           	   	  fm.all('BespeakAcceptTypeName').ondblclick = "";
           	      //fm.all('ServTaskName').value="绿色通道预约受理";
           	   }
           	   var ssSql = "select a.CusServRequest "
										+" from LHIndiServAccept a where "
										+" a.ModelTypeNo ='"+fm.all('ModelTypeNo').value+"'"
										+" and a.TaskModelType='"+ fm.all('TaskModelType').value + "'";
                //alert(ssSql);
               //alert(easyExecSql(ssSql));
               var arrRR=easyExecSql(ssSql);
               if(arrRR!=""&&arrRR!=null&&arrRR!="null")
               {
                  fm.all('CusOtherRequest').value 		= arrRR[0][0]; 
               }
           }
           if(fm.all('TaskModelType').value=="3")
           {
           	  //fm.all('ServTaskName').value=fm.all('ServItemName').value+"受理";
           	  var ssSql = "select a.CusServRequest "
										+" from LHIndiServAccept a where "
										+" a.ModelTypeNo ='"+fm.all('ModelTypeNo').value+"'"
										+" and a.TaskModelType='"+ fm.all('TaskModelType').value + "'";
               //alert(ssSql);
               //alert(easyExecSql(ssSql));
               var arrRR=easyExecSql(ssSql);
               if(arrRR!=""&&arrRR!=null&&arrRR!="null")
               {
                 fm.all('CusServRequest').value 		= arrRR[0][0]; 
               }
           }
           var mulSql2 = "select a.ServCaseCode,"
                   +"(select d.ServCaseName from LHServCaseDef d where d.ServCaseCode=a.ServCaseCode),"
                   +" a.ServAccepNo "
				   				 +" from LHIndiServAccept a "
				   				 +" where a.ServAccepNo ='"+arrQueryResult[0][7]+"'"
				   				;
				  //alert(mulSql2);
				  //alert(easyExecSql(mulSql2));
				  turnPage.queryModal(mulSql2, LHServCaseGrid);     
				  var mulSql3 = " select a.TaskModelType "
										+"  from LHIndiServAccept a "
										+"  where a.ServAccepNo ='"+arrQueryResult[0][7]+"' "; 
					//alert(mulSql3);
				  //alert(easyExecSql(mulSql3));
				  var modelType=easyExecSql(mulSql3);
				  //alert(modelType);
				  if(modelType=="1")//受理类型为预约服务受理
				  {
				  	  divLHBespeakServAccdeptGrid.style.display='';
				  	  divLHOtherServAccdeptGrid.style.display='none';
				  	  var mulSql4 = " select a.TaskModelType ,a.ModelTypeNo,"
				  	        +"  a.BespeakComID, a.BespeakAcceptType ,"
				  	        +" BespeakDateRequest ,CusServRequest"
										+"  from LHIndiServAccept a "
										+"  where a.ServAccepNo ='"+arrQueryResult[0][7]+"' "; 
							//alert(mulSql4);
				      //alert(easyExecSql(mulSql4));
				      var arrResult4=new Array;
				      arrResult4=easyExecSql(mulSql4);
						  if(arrResult4!=""&&arrResult!=null&&arrResult!="null")
              {
								  fm.all('TaskModelType').value			= arrResult4[0][0];
								  fm.all('BespeakAcceptTypeName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhbespeaktype' and d.code='"+ fm.all('TaskModelType').value +"'");
		              fm.all('ModelTypeNo').value			= arrResult4[0][1];
                  fm.all('HospitCode').value			= arrResult4[0][2];
                  fm.all('HospitName').value=easyExecSql("select  HospitName from LDHospital where LDHospital.HospitCode='"+ fm.all('HospitCode').value +"'");
                  fm.all('BespeakAcceptType').value			= arrResult4[0][3];
		              fm.all('BespeakDateRequest').value			= arrResult4[0][4];
		              fm.all('BespeakAcceptTypeName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhbespeaktype' and d.code='"+ fm.all('BespeakAcceptType').value +"'");
                  fm.all('CusOtherRequest').value			= arrResult4[0][5];
				      }
				   }
				  if(modelType=="3")//受理类型为其它服务受理
				  {
				  	  divLHOtherServAccdeptGrid.style.display='';
				  	  divLHBespeakServAccdeptGrid.style.display='none';
				  }
				}
	}     
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
    
     //下面增加相应的代码
     if (confirm("您确实想修改该记录吗?"))
     {
         var i = 0;
         if( verifyInput2() == false ) return false;
         var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
         var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
         showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
         fm.fmtransact.value = "UPDATE||MAIN";
         fm.action="./LHIndivServAcceptSave.jsp";
         fm.submit(); //提交
     }
     else
     {
       //mOperate="";
       alert("您取消了修改操作！");
     }
} 
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
     var i = 0;
     var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
     
     //showSubmitFrame(mDebug);
     fm.fmtransact.value = "DELETE||MAIN";
     fm.action="./LHIndivServAcceptSave.jsp";
     fm.submit(); //提交
     initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
} 
function ServCaseSend()
{
	  if (LHServCaseGrid.getSelNo() >= 1)
		{ 
			   if(fm.all('ServAccepNo').value!=""&&fm.all('ServAccepNo').value!=null&&fm.all('ServAccepNo').value!="null")
			   {
			   	   var strTaskNo=" select a.ServTaskCode  from LHIndiServAccept a"
		              +" where a.ServAccepNo='"+ fm.all('ServAccepNo').value+"'"
		              ;
		         var taskNO=easyExecSql(strTaskNo);
		         var sqlTaskNo=" select a.Servtaskno  from LHCaseTaskRela a"
		              +" where a.Servtaskno='"+ taskNO+"'"
		              ;
		         var taskCode=easyExecSql(sqlTaskNo);
		         if(taskCode!=""&&taskCode!=null&&taskCode!="null")
		         {
		         	  alert("此服务任务已经发送过,不允许重复发送!");
		         	  return false;
		         }
		         else
		         {
		         	 
			   	      var strSql=" select a.ServCaseCode,a.ServTaskCode "
		                +" from LHCaseTaskRela a"
		                +" where a.ServCaseCode='"+ fm.all('ServCaseCode').value+"'"
		                +" and a.ServTaskCode='"+ fm.all('ServTaskCode').value+"'"
		                ;
		            //alert(strSql);
		            //alert(easyExecSql(strSql));
		            var itemCode=fm.all('ServTaskCode').value;
			   	      if(itemCode!=""&&itemCode!="null"&&itemCode!=null)
			   	      {
		               var i = 0;
                   if( verifyInput2() == false ) return false;
                   fm.fmtransact.value="INSERT||MAIN";
                   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
                   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
                   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
                   fm.action="./LHIndivServCaseSendSave.jsp";
                   fm.submit(); //提交
                 }
                 else
                 {
                	  alert("请选择服务任务编号!");
                	  if( verifyInput2() == false ) return false;
	                  fm.all('ServTaskCode').focus();
                	  return false;
                 }
             }
          } 
          else
          { 
          	 alert("此事件未经过服务受理，不能进行任务发送!");   
          	 return false;
          } 
		}       
		else
		{
				alert("请选择一条要进行任务发送的信息！");    
		}
	   
} 

function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	
	{  
		var newWindow = window.open("../sys/LDPersonQuery.html");	  
	}
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	
	    if (arrResult != null) 
	    {
	    	fm.CustomerNo.value = arrResult[0][0];
	      	alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    	}
	    else
	    {
	         alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
		}
	}	
}
function queryCustomerNo2()
{
	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户号码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);

	    if (arrResult != null) 
	    {
      		fm.CustomerNo.value = arrResult[0][0];
      		alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    	}
    	else
    	{
     	   alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     	}
	}	
}
function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}
function selectServTaskName()
{
	 if(fm.ServTaskCode.value=="null"||fm.ServTaskCode.value==null||fm.ServTaskCode.value=="")
	 {
	 	 alert("请先选择任务编号!");
	 	 return false;
   }
	 else
   {
	    var strSql = "select ServTaskName from LHServTaskDef where ServTaskCode='" + fm.ServTaskCode.value +"'";
      //alert(strSql);
      var arrResult = easyExecSql(strSql);
      // alert(arrResult);
      if (arrResult != null) 
      {
        // fm.ServTaskName.value = arrResult[0][0];
      }
   }
}        
        
