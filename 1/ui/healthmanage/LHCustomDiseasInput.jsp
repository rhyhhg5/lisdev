<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-17 21:09:38
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHCustomDiseasInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHCustomDiseasInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHCustomDiseasSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomDiseas1);">
    		</td>
    		 <td class= titleImg>
        		 客户疾病信息基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomDiseas1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      客户号码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=24" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerName verify="客户姓名|len<=24" >
    </TD>
    
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      疾病代码
    </TD>
    <TD  class= input>
      <!--Input class= 'common' name=DiseasCode verify="疾病代码|NUM&len<=20"-->
      <Input name=DiseasCode class="code" verify="疾病代码|len<=20" ondblclick="return queryDiseas();" onkeyup="return queryDiseas2();">
    </TD>
    <TD  class= title>
      疾病名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasName >
    </TD>
   
  </TR>
  <TR  class= common>
     <TD  class= title>
      疾病情况
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasState verify="疾病情况|len<=200">
    </TD>
    <TD  class= title>
      患病开始日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=DiseasBeginDate dateformat= "Short">
    </TD>
    
  </TR>
  
  
  <TR  class= common>
    <TD  class= title>
      患病结束日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat= "Short" name=DiseasEndDate >
    </TD>
    <TD  class= title>
      医疗机构代码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=HospitCode verify="医疗机构代码|len<=20" ondblclick="getHospitCode();return showCodeListEx('GetHospitCode',[this,HospitalName],[0,1],'', '', '', true);" onkeyup="getHospitCode();return showCodeListKeyEx('GetHospitCode',[this,HospitalName],[0,1],'', '', '', true);">
    </TD>
    
  </TR>
  <tr class=common>
  <TD  class= title>
      医疗机构名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HospitalName >
    </TD>
  <TD  class= title>
      医师姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Doctor verify="医生姓名|len<=20">
    </TD>
    </tr>
  
</table>
    </Div>
    <div id="div1" style="display: 'none'">
    <table>
    <TR  class= common>
    <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
  </TR>
  
  <TD  class= title>
      序号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasNo >
    </TD>
    <TR  class= common>
    
    <TD  class= title>
      告知标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ImpartFlag verify="告知标志|len<=2">
    </TD>
    
  </TR>
  <TR  class= common>
  	<TD  class= title>
      告知日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=ImpartDate >
    </TD>
  </TR>
  
    
  </table>
  </div>
    <hr/>
    <INPUT VALUE="客户健康信息" class=cssButton TYPE=button onclick="CustomHealth();">
    <INPUT VALUE="客户就诊信息" class=cssButton TYPE=button onclick="CustomInHospital();">
    <!--INPUT VALUE="客户疾病信息" class=cssButton TYPE=button onclick="CustomDiseas();"-->
    <INPUT VALUE="客户检查信息" class=cssButton TYPE=button onclick="CustomTest();">
    <INPUT VALUE="客户治疗信息" class=cssButton TYPE=button onclick="CustomOPS();">
    <INPUT VALUE="客户家族病史" class=cssButton TYPE=button onclick="CustomFamily();">
    <INPUT VALUE="客户健身信息" class=cssButton TYPE=button onclick="CustomGym();">
  <hr/>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
