<%
//程序名称：LHCustomFamilyDiseasInput.jsp
//程序功能：
//创建日期：2005-01-17 16:25:10
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
	String tName = "";
	String tNo = "";
	try
	{
		tName = request.getParameter("tCustomerName");
	        tName = new String(tName.getBytes("ISO-8859-1"), "GBK");
	}
	catch( Exception e )
	{
		tName = "";
	}
	if(request.getParameter("tCustomerNo")!=null)
	{
		tNo=request.getParameter("tCustomerNo");
	}
%>                                 
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('CustomerNo').value = "<%=tNo%>";
    fm.all('CustomerName').readOnly=true;
    fm.all('CustomerName').value = "<%=tName%>"
    fm.all('FamilyCode').value = "";
    fm.all('FamilyName').readOnly = true;
    fm.all('DiseasCode').value = "";
    fm.all('DiseasName').readOnly = true;
    fm.all('Explai').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHCustomFamilyDiseasInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LHCustomFamilyDiseasInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("LHCustomFamilyDiseasInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
