<%
//程序名称：LHStatuteInput.jsp
//程序功能：功能描述
//创建日期：2005-01-19 13:04:32
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHStatuteQueryInput.js"></SCRIPT> 
  <%@include file="LHStatuteQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLHStatuteGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      政策法规文件名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StatuteTitle elementtype=nacessary verify="政策法规文件名称|notnull&len<=200">
    </TD>
    <TD  class= title>
      发文号
    </TD>   
    <TD  class= input>
      <Input class= 'common' name=StatuteNo verify="发文号|len<=50">
    </TD>
    <TD class=title>
    </TD>
    <TD class=input>
    </TD>
  </TR>  
  <TR  class= common>
<!--    
		<TD  class= title>
      内容分类代码
    </TD>
    <TD  class= input>
       <Input class= 'code' name=ContType ondblClick="showCodeListEx('ContType',[this,''],[0,1]);" verify="定点属性标识符|len<=2"  CodeData="0|^1|^2|" onkeyup="showCodeListEx('ContType',[this,''],[0,1]);">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      颁布单位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IssueOrganCode verify="颁布单位|len<=20">
    </TD>
-->    
    <TD  class= title>
      颁布日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=IssueDate >
    </TD>
    <TD  class= title>
      实施日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=StartDate >
    </TD>
    <TD  class= title>
      终止日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=EndDate >
    </TD>
  </TR>
  <TR  class= common style="display:'none'">
    <TD  class= title>
      内容简介
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DocContent verify="内容简介|len<=2000">
    </TD>
  </TR>
</table>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHStatute1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHStatute1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHStatuteGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
  <Div id= "div123" align=center style="display: 'none' ">
    
    <TextArea class=common value="政策法规全文"  name=TextArea1 cols="100%" rows="10" witdh=25%></TextArea>
  </Div> 
 
 
 
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
