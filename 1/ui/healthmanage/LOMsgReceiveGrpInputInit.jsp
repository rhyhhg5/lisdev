<%
//程序名称：LOMsgReceiveGrpInputInit.jsp
//程序功能：功能描述
//创建日期：2005-12-27 11:29:02
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try 
  {     

  }
  catch(ex) 
  {
    alert("LOMsgReceiveGrpInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLOMsgReceiveGrpGrid();  
    initLOMsgEachGrpInfoGrid();
  }
  catch(re) {
    alert("LOMsgReceiveGrpInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LOMsgReceiveGrpGrid;
function initLOMsgReceiveGrpGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    
    iArray[1]=new Array();                    
		iArray[1][0]="客户组号码";			//列名    
		iArray[1][1]="70px";         		//列名    
		iArray[1][3]=0;         				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="客户组名称";			//列名    
		iArray[2][1]="150px";						//列名    
		iArray[2][3]=0;									//列名        
	
		iArray[3]=new Array();  
		iArray[3][0]="短信类型";
		iArray[3][1]="60px";   
		iArray[3][3]=0;         
		
		iArray[4]=new Array();  
		iArray[4][0]="说明";
		iArray[4][1]="300px";   
		iArray[4][3]=0;         
		
		iArray[5]=new Array();  
		iArray[5][0]="设置时间";
		iArray[5][1]="60px";   
		iArray[5][3]=0;         
		     
    LOMsgReceiveGrpGrid = new MulLineEnter( "fm" , "LOMsgReceiveGrpGrid" ); 
    //这些属性必须在loadMulLine前           
    
		LOMsgReceiveGrpGrid.mulLineCount = 0;
		LOMsgReceiveGrpGrid.canSel = 1;
		LOMsgReceiveGrpGrid.hiddenPlus = 1;
		LOMsgReceiveGrpGrid.hiddenSubtraction = 1;
		LOMsgReceiveGrpGrid.selBoxEventFuncName ="showDetail"
/*
    LOMsgReceiveGrpGrid.displayTitle = 1;
    LOMsgReceiveGrpGrid.canChk = 0;
    LOMsgReceiveGrpGrid.selBoxEventFuncName = "showOne";
*/
    LOMsgReceiveGrpGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LOMsgReceiveGrpGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

var LOMsgEachGrpInfoGrid;
function initLOMsgEachGrpInfoGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    
    iArray[1]=new Array();                    
		iArray[1][0]="客户号码";			//列名    
		iArray[1][1]="50px";         		//列名    
		iArray[1][3]=0;         				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="客户姓名";			//列名    
		iArray[2][1]="50px";						//列名    
		iArray[2][3]=0;									//列名        
	
		iArray[3]=new Array();  
		iArray[3][0]="证件号码";
		iArray[3][1]="80px";   
		iArray[3][3]=0;         
		
		iArray[4]=new Array();  
		iArray[4][0]="团体保单号码";
		iArray[4][1]="50px";   
		iArray[4][3]=0;         
		
		iArray[5]=new Array();  
		iArray[5][0]="保单号码";
		iArray[5][1]="50px";   
		iArray[5][3]=0;  
		
		iArray[6]=new Array();  
		iArray[6][0]="单位名称";
		iArray[6][1]="120px";   
		iArray[6][3]=0; 
		
		iArray[7]=new Array();  
		iArray[7][0]="服务计划名称";
		iArray[7][1]="60px";   
		iArray[7][3]=0;   
		
		iArray[8]=new Array();  
		iArray[8][0]="手机号码";
		iArray[8][1]="60px";   
		iArray[8][3]=0;     
		     
    LOMsgEachGrpInfoGrid = new MulLineEnter( "fm" , "LOMsgEachGrpInfoGrid" ); 
    //这些属性必须在loadMulLine前           
    
		LOMsgEachGrpInfoGrid.mulLineCount = 0;
		LOMsgEachGrpInfoGrid.canSel = 0;
		LOMsgEachGrpInfoGrid.hiddenPlus = 1;
		LOMsgEachGrpInfoGrid.hiddenSubtraction = 1;
/*
    LOMsgEachGrpInfoGrid.displayTitle = 1;
    LOMsgEachGrpInfoGrid.canChk = 0;
    LOMsgEachGrpInfoGrid.selBoxEventFuncName = "showOne";
*/
    LOMsgEachGrpInfoGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LOMsgEachGrpInfoGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>