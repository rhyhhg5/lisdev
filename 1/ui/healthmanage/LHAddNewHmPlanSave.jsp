<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHAddNewHmPlanSave.jsp
//程序功能：
//创建日期：2006-07-06 8:43:41
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LHServCaseDefSchema tLHServCaseDefSchema   = new LHServCaseDefSchema();
  LHServCaseRelaSchema tLHServCaseRelaSchema   = new LHServCaseRelaSchema();
  LHAddNewHmPlanUI tLHAddNewHmPlanUI   = new LHAddNewHmPlanUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHServCaseDefSchema.setServCaseType(request.getParameter("ServEventType"));//事件类型
    System.out.println("QQQQQQQQQ"+tLHServCaseDefSchema.getServCaseType());
    tLHServCaseDefSchema.setServCaseCode(request.getParameter("ServCaseCode"));//事件编号
    System.out.println("QQRRRRRRRRRRRQ"+tLHServCaseDefSchema.getServCaseCode());
    tLHServCaseDefSchema.setServCaseName(request.getParameter("ServCaseName"));//事件名称
    System.out.println("QNNNNNNNNNNNNQQ"+tLHServCaseDefSchema.getServCaseName());
    tLHServCaseDefSchema.setServCaseState(request.getParameter("ServCaseState"));//事件状态
    
    
    tLHServCaseRelaSchema.setServCaseCode(request.getParameter("ServCaseCode"));//事件号
    tLHServCaseRelaSchema.setServPlanNo(request.getParameter("ServPlanNo"));//计划号
    tLHServCaseRelaSchema.setServPlanName(request.getParameter("ServPlanName"));//计划名字
    tLHServCaseRelaSchema.setComID(request.getParameter("ComID"));//机构标识
    tLHServCaseRelaSchema.setServPlanLevel(request.getParameter("ServPlanLevel"));//计划档次
    tLHServCaseRelaSchema.setServItemNo(request.getParameter("ServItemNo"));//项目序号
    tLHServCaseRelaSchema.setServItemCode(request.getParameter("ServItemCode"));//标准服务项目代码
    tLHServCaseRelaSchema.setContNo(request.getParameter("ContNo"));//保单号
    tLHServCaseRelaSchema.setCustomerNo(request.getParameter("CustomerNo"));//客户号
    tLHServCaseRelaSchema.setCustomerName(request.getParameter("CustomerName"));//客户名
    
    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHServCaseDefSchema);
	  tVData.add(tLHServCaseRelaSchema);
  	tVData.add(tG);
    tLHAddNewHmPlanUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHAddNewHmPlanUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
