// 该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var DataCache = new Array();


//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function SaveEvent()
{
		var	showInfo3=window.open("./LOMsgReceiveGrpSetInputMain.jsp","客户组",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto');
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail")
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    initForm();   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
    
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCustomTest.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
        
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}
        
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
		
		 //为空不删除
 	  if(LOMsgReceiveGrpGrid.getSelNo() < 1)
    {
				alert("请先选中一条信息");
				return false;
	  }
		
		//下面增加相应的代码
	  else  
	  {
				var groupNo = LOMsgReceiveGrpGrid.getRowColData(LOMsgReceiveGrpGrid.getSelNo()-1,1);
				var	showInfo3=window.open("./LOMsgReceiveGrpSetInputMain.jsp?groupNo="+groupNo,"客户组",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto');
	  }
}           
          
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //为空不删除
  if(LOMsgReceiveGrpGrid.getSelNo() < 1)
	{
			alert("请先选中一条信息");
			return false;
	}
  
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
	  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.all('GrpNo').value = LOMsgReceiveGrpGrid.getRowColData(LOMsgReceiveGrpGrid.getSelNo()-1,1);
	  fm.fmtransact.value = "DELETE||MAIN";
	  fm.submit(); //提交
	  
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function returnParent()
{
	var arrReturn = new Array();
	var tSel = LOMsgReceiveGrpGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
			try
			{
				
				var arrReturn = getQueryResult();
				if(arrReturn == false)
				{
					return false;
				}
				else
				{
					 
					top.opener.afterQuery0( arrReturn );
					top.close();
					parent.opener.window.focus();
				}
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery0接口。" + ex );
			}
			
	}
}

function getQueryResult()
{
	var arrSelected = new Array();
	var tRow = LOMsgReceiveGrpGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return false;
	
	//设置需要返回的数组	    
	arrSelected[0] = new Array();//手机号
	arrSelected[1] = new Array();//客户号
	arrSelected[2] = new Array();//团单号
	arrSelected[3] = new Array();//保单号
	for(var i = 0; i < LOMsgEachGrpInfoGrid.mulLineCount; i++)
	{
			if(LOMsgEachGrpInfoGrid.getRowColData(i,8) == "" || LOMsgEachGrpInfoGrid.getRowColData(i,8).length != 11)
			{
					alert("客户 '"+LOMsgEachGrpInfoGrid.getRowColData(i,2)+"' 的手机号码有误，请确认");
					return false;
			}
			arrSelected[0][i] = LOMsgEachGrpInfoGrid.getRowColData(i,8);
			arrSelected[1][i] = LOMsgEachGrpInfoGrid.getRowColData(i,1);
			arrSelected[2][i] = LOMsgEachGrpInfoGrid.getRowColData(i,4);
			arrSelected[3][i] = LOMsgEachGrpInfoGrid.getRowColData(i,5);
	}

	return arrSelected;
}

//查询客户组
function queryGrp()
{ 
		var sql = " select customgrpno, customgrpname, '', content, makedate from LHMsgCustomGrp where 1=1 "
						 +getWherePart("CustomGrpName","CustomGrpName","like")
						 +getWherePart("MakeDate","MakeDate")
						 +getWherePart("CustomGrpNo","CustomGrpNo")
						 +" order by customgrpno desc "
						 ;
		turnPage.queryModal(sql,LOMsgReceiveGrpGrid);
		initLOMsgEachGrpInfoGrid();
}

function showDetail()
{
		if(easyExecSql(" select distinct grpcontno from lhmsgcustomgrpdetail where customgrpno = '"+LOMsgReceiveGrpGrid.getRowColData(LOMsgReceiveGrpGrid.getSelNo()-1,1)+"'" ) == "00000000000000000000")
		{
			var sql = " select a.customerno, b.name, b.idno, a.grpcontno, a.contno, c.Appntname,'', "
							+" (select xx.mobile from dual, "
							+" (select l.customerno customerno, x.mobile mobile from ldperson l "
		  			 	+" left join  "
		  		    +" (select distinct c.customerno as customerno, c.mobile mobile "
              +" from   lcaddress c, (select customerno, max(addressno) as maxno "
							+" from lcaddress group by customerno ) b  "
			        +" where c.customerno = b.customerno and c.addressno = b.maxno "
              +" ) as x  on l.customerno = x.customerno "
              +" ) as xx "
        			+" where  xx.customerno = a.customerno )"
							+" from LHMsgCustomGrpDetail a, LDPerson b, LCCont c"
							+" where a.customgrpno = '"+LOMsgReceiveGrpGrid.getRowColData(LOMsgReceiveGrpGrid.getSelNo()-1,1)+"'"
							+" and a.customerno = b.customerno"
							+" and a.contno = c.contno"
							+" and c.grpcontno = '00000000000000000000' "
						;  
			
		}
		
		else
		{
			var sql = " select a.customerno, b.name, b.idno, a.grpcontno, a.contno, c.grpname,'', "
							+" (select xx.mobile from dual, "
							+" (select l.customerno customerno, x.mobile mobile from ldperson l "
		  			 	+" left join  "
		  		    +" (select distinct c.customerno as customerno, c.mobile mobile "
              +" from   lcaddress c, (select customerno, max(addressno) as maxno "
							+" from lcaddress group by customerno ) b  "
			        +" where c.customerno = b.customerno and c.addressno = b.maxno "
              +" ) as x  on l.customerno = x.customerno "
              +" ) as xx "
        			+" where  xx.customerno = a.customerno )"
							+" from LHMsgCustomGrpDetail a, LDPerson b, LCGrpCont c"
							+" where a.customgrpno = '"+LOMsgReceiveGrpGrid.getRowColData(LOMsgReceiveGrpGrid.getSelNo()-1,1)+"'"
							+" and a.customerno = b.customerno"
							+" and a.grpcontno = c.grpcontno"
						;  
		}
		//				alert(sql);
	  turnPage2.pageLineNum = 200;
		turnPage2.queryModal(sql,LOMsgEachGrpInfoGrid);
}