<%
//程序名称：LHRespDefiQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-19 15:05:48
//创建人  ：ctrHTML
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-01 10:38:48
// 更新原因/内容: 插入新的字段  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    //fm.all('DutyItemCode').value = "";
    //fm.all('DutyItemName').value = "";
    fm.all('DutyItemTye').value = "";
    //fm.all('DutyItemExplain').value = "";
//    fm.all('ContraBeginDate').value = "";
//    fm.all('ContraEndDate').value = "";
//    fm.all('ContraState').value = "";
//    fm.all('FirstPerson').value = "";
//    fm.all('SecondPerson').value = "";
//    fm.all('linkman').value = "";
//    fm.all('Phone').value = "";
  }
  catch(ex) {
    alert("在LHRespDefiQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() 
	{
  	try 
  	{
    		initInpBox();
    		initLHRespDefiGrid();  
  	}
  	catch(re) {
    alert("LHRespDefiQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHRespDefiGrid;
function initLHRespDefiGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名

		iArray[1]=new Array();                
    iArray[1][0]="责任项目代码";         		//列名
    iArray[1][1]="60px";         		//列名
    iArray[1][3]=0;         				//列名

		iArray[2]=new Array();                
    iArray[2][0]="责任项目名称";         		//列名
    iArray[2][1]="60px";         		//列名
    iArray[2][3]=0;         				//列名

		iArray[3]=new Array();                
    iArray[3][0]="项目责任类型";         		//列名
    iArray[3][1]="60px";         		//列名
    iArray[3][3]=0;         				//列名

		iArray[4]=new Array();                
    iArray[4][0]="责任项目一般性释义";         		//列名
    iArray[4][1]="100px";         		//列名
    iArray[4][3]=0;         				//列名

		iArray[5]=new Array();                              
		iArray[5][0]="更改日期";        //列名
    iArray[5][1]="0px";         		//列名            
    iArray[5][3]=3;         				//列名   
    
    iArray[6]=new Array();                  
    iArray[6][0]="更改日期";        //列名                          
    iArray[6][1]="0px";         		//列名  
    iArray[6][3]=3;         				//列名  

    LHRespDefiGrid = new MulLineEnter( "fm" , "LHRespDefiGrid" ); 
    //这些属性必须在loadMulLine前

    LHRespDefiGrid.mulLineCount = 0;   
    LHRespDefiGrid.displayTitle = 1;
    LHRespDefiGrid.canSel = 1;
    LHRespDefiGrid.hiddenPlus = 1;
    LHRespDefiGrid.hiddenSubtraction = 1;
/*    
    LHRespDefiGrid.canChk = 0;
    LHRespDefiGrid.selBoxEventFuncName = "showOne";
*/
    LHRespDefiGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHRespDefiGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
