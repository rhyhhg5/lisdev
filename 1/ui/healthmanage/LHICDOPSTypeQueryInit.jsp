<%
//程序名称：LHICDOPSTypeQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-08 08:45:03
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('OPSTypeLevel').value = "";
    fm.all('ICDBeginCode').value = "";
    fm.all('ICDEndCode').value = "";
    fm.all('OPSType').value = "";
  }
  catch(ex) {
    alert("在LHICDOPSTypeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHICDOPSTypeGrid();  
  }
  catch(re) {
    alert("LHICDOPSTypeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHICDOPSTypeGrid;
function initLHICDOPSTypeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="代码分类等级";         		//列名
    iArray[1][1]="50px";         		//列名
    iArray[1][3]=0;         		//列名
             		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="分类起始代码";         		//列名
    iArray[2][1]="100px";         		//列名
    iArray[2][3]=0;         		//列名
    
    
    iArray[3]=new Array();
    iArray[3][0]="分类终止代码";         		//列名
    iArray[3][1]="100px";         		//列名
    iArray[3][3]=0;         		//列名
    
    
    iArray[4]=new Array();
    iArray[4][0]="手术分类名称";         		//列名
    iArray[4][1]="150px";         		//列名
    iArray[4][3]=0;         		//列名
    
    LHICDOPSTypeGrid = new MulLineEnter( "fm" , "LHICDOPSTypeGrid" ); 
    //这些属性必须在loadMulLine前

    LHICDOPSTypeGrid.mulLineCount = 0;   
    LHICDOPSTypeGrid.displayTitle = 1;
    LHICDOPSTypeGrid.hiddenPlus = 1;
    LHICDOPSTypeGrid.hiddenSubtraction = 1;
    LHICDOPSTypeGrid.canSel = 1;
    LHICDOPSTypeGrid.canChk = 0;
    //LHICDOPSTypeGrid.selBoxEventFuncName = "showOne";

    LHICDOPSTypeGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHICDOPSTypeGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
