<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHScanInfoPrt.jsp
//程序功能：F1报表生成
//创建日期：2005-07-16
//创建人  ： 
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%@page import="java.io.*"%>
<%
  	boolean operFlag=true;
    CError cError = new CError();
    	
  	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
  	
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	
	String SerialNo     = request.getParameter("SerialNo");	
  	String CustomerNo   = request.getParameter("CustomerNo");	
  	System.out.println("CustomerName: "+request.getParameter("CustomerName"));
//  	String CustomerName = new String(request.getParameter("CustomerName").getBytes("ISO-8859-1"),"GB2312");
	String CustomerName = request.getParameter("CustomerName");	//正式机用
	System.out.println("CustomerName: "+CustomerName);
	TransferData CustomerInfo = new TransferData();
    
	CustomerInfo.setNameAndValue("SerialNo",SerialNo);
	CustomerInfo.setNameAndValue("CustomerNo",CustomerNo);
	CustomerInfo.setNameAndValue("CustomerName",CustomerName);
	
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
		
	tVData.addElement(tG);
	tVData.addElement(CustomerInfo);
 
	LHScanInfoPrtUI tLHScanInfoPrtUI = new LHScanInfoPrtUI();
	XmlExport txmlExport = new XmlExport();    
	if(!tLHScanInfoPrtUI.submitData(tVData,"PRINT"))
	{
		operFlag=false;
		Content=tLHScanInfoPrtUI.mErrors.getFirstError().toString();                 
	}
	else
	{  
		mResult = tLHScanInfoPrtUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
		
	ExeSQL tExeSQL = new ExeSQL();
	
	//获取临时文件名
	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	String strFilePath = tExeSQL.getOneValue(strSql);
	String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
	
	//获取存放临时文件的路径
	String strRealPath = application.getRealPath("/").replace('\\','/');
	String strVFPathName = strRealPath + "//" +strVFFileName;
	
	CombineVts tcombineVts = null;	
	if (operFlag==true)
	{
		//合并VTS文件
		String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
		
		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);
		
		//把dataStream存储到磁盘文件
		AccessVtsFile.saveToFile(dataStream,strVFPathName);
		System.out.println("==> Write VTS file to disk ");
		System.out.println("===strVFFileName : "+strVFFileName);
		
		//本来打算采用get方式来传递文件路径
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=03&RealPath="+strVFPathName);
	}
	
	else
	{
    	FlagStr = "Fail";

%>
		<html>
		<%@page contentType="text/html;charset=GBK" %>
		<script language="javascript">	
			alert("<%=Content%>");
			top.close();
			//window.opener.afterSubmit("<%=FlagStr%>","<%=Content%>");	
		</script>
		</html>
<%
  	}

%>
