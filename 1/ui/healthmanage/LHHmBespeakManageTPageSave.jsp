<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHHmBespeakManageTPageSave.jsp
//程序功能：
//创建日期：2006-11-01 10:03:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  String TServTaskNo="";//从页面获取的传入的多个服务任务编号
  String TServInfoNo[][]; 
  String tCustomerNo[][];
  LHHmBespeakManageUI tLHHmBespeakManageUI   = new LHHmBespeakManageUI();
  LHHmServBeManageSet tLHHmServBeManageSet = new LHHmServBeManageSet();		//服务预约管理表
  LHTaskCustomerRelaSet tLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();		//客户任务实施管理表
  TServTaskNo= request.getParameter("TPageTaskNo");	
   String tChk[] = request.getParameterValues("InpLHHmServBeManageGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  
    
  //输出参数
  CErrors tError = null;              
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
	  String[] tServTaskNo  = null;
  	tServTaskNo=TServTaskNo.split(",");
  	SSRS tSSRS_Result = new SSRS();
    ExeSQL tExeResultSQL = new ExeSQL();
    String sqlResult =" select TaskExecNo, ServCaseCode, ServTaskNo,ServItemNo,TaskExecState  from LHTaskCustomerRela "
                         + " where ServTaskNo in ( "+ TServTaskNo +" ) order by ServTaskNo";
    System.out.println("sqlResult "+sqlResult);
    tSSRS_Result = tExeResultSQL.execSQL(sqlResult);
    TServInfoNo = tSSRS_Result.getAllData();
    //System.out.println("AAAAAAAAAAAAAAAAAAAAA " +TServInfoNo.length);
    for(int j = 0; j < TServInfoNo.length; j++)
	  {
        ExeSQL tExeCustomerNo = new ExeSQL();
        String sqlCustomerNo =" select (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode='"+ TServInfoNo[j][1]+"' and a.ServItemNo='"+ TServInfoNo[j][3] +"')"
                             +" from LHTaskCustomerRela d "
                             +" where d.ServCaseCode='"+ TServInfoNo[j][1] +"'"
		                         +" and d.ServTaskNo='"+TServInfoNo[j][2] +"'"
		                         +" and d.TaskExecNo='"+ TServInfoNo[j][0] +"'"
		                         +" and d.ServItemNo='"+TServInfoNo[j][3] +"'"
		                         ;
        String CustomerNo = tExeCustomerNo.getOneValue(sqlCustomerNo);//最大日期下的客户相关流水号
        String sqlTaskCode =" select ServTaskCode from LHCaseTaskRela a  where a.ServTaskNo ='" + TServInfoNo[j][2]+"'";
        String ServTaskCode= new ExeSQL().getOneValue(sqlTaskCode);
	     
		    LHHmServBeManageSchema tLHHmServBeManageSchema = new LHHmServBeManageSchema();
		    tLHHmServBeManageSchema.setCustomerNo(CustomerNo);
		    tLHHmServBeManageSchema.setTaskExecNo(TServInfoNo[j][0]);
		    //System.out.println("getTaskExecNo "+ tLHHmServBeManageSchema.getTaskExecNo());
		    tLHHmServBeManageSchema.setServCaseCode(TServInfoNo[j][1]);
		    tLHHmServBeManageSchema.setServTaskNo(TServInfoNo[j][2]);
		    tLHHmServBeManageSchema.setServItemNo(TServInfoNo[j][3]);
		    tLHHmServBeManageSchema.setServTaskCode(ServTaskCode);		     
		     
		    tLHHmServBeManageSchema.setBespeakComID(request.getParameter("HospitCode"));//预约机构
				tLHHmServBeManageSchema.setServBespeakDate(request.getParameter("ServiceDate"));  //预约日期
				tLHHmServBeManageSchema.setServBespeakTime(request.getParameter("ServiceTime"));//预约时间 
				tLHHmServBeManageSchema.setTestGrpCode(request.getParameter("MedicaItemGroup"));  //体检代码
				tLHHmServBeManageSchema.setBalanceManner(request.getParameter("BalanceManner"));//结算方式
				tLHHmServBeManageSchema.setServDetail(request.getParameter("ServItemNote"));     //备注   
				
				LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema();
		 
		    tLHTaskCustomerRelaSchema.setTaskExecNo(TServInfoNo[j][0]);	 //流水号
		    tLHTaskCustomerRelaSchema.setServTaskNo(TServInfoNo[j][2]);	 //任务编号
		    tLHTaskCustomerRelaSchema.setServCaseCode(TServInfoNo[j][1]);	 //事件号码
		    tLHTaskCustomerRelaSchema.setServTaskCode(ServTaskCode);	 //任务代码 
		    tLHTaskCustomerRelaSchema.setServItemNo(TServInfoNo[j][3]);	//项目号码
		    String state=request.getParameter("ExecState");
		    if(state.equals("")||state.equals("null")||state.equals(null))
		    {
		        tLHTaskCustomerRelaSchema.setTaskExecState(TServInfoNo[j][4]);  //任务执行状态  
		    }
		    else
		    {
		       tLHTaskCustomerRelaSchema.setTaskExecState(request.getParameter("ExecState"));     //任务执行状态  
		    }       
        if(transact.equals("INSERT||MAIN"))
        {
          tLHHmServBeManageSet.add(tLHHmServBeManageSchema);   
          tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);     
        }  
        if(transact.equals("UPDATE||MAIN"))
        {
           if(tChk[j].equals("1"))  
           {            
              tLHHmServBeManageSet.add(tLHHmServBeManageSchema);         
           }
        }         
      }
                              
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHHmServBeManageSet);
  	tVData.add(tLHTaskCustomerRelaSet);
  	tVData.add(tG);
    tLHHmBespeakManageUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHHmBespeakManageUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	  var transact = "<%=transact%>";
	      parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>