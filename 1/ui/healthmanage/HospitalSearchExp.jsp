<%
//程序名称：HospitalSearchExp.jsp
//程序功能：医疗机构检索后导出功能
//创建日期：2007-06-05
//创建人  ：zsjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="java.io.*"%>
<%
		System.out.println("start");
  	CError cError = new CError();
  	boolean operFlag=true;
  	  	
		String tMngCom  = request.getParameter("MngCom");
		String tHospitalType  = request.getParameter("HospitalType");
		String tHospitCode  = request.getParameter("HospitCode");
		String tHospitName  = request.getParameter("HospitName");
		String tAreaCode  = request.getParameter("AreaCode");
		String tAreaName  = request.getParameter("AreaName");
		String tCommunFixFlag  = request.getParameter("CommunFixFlag");
		String tAdminiSortCode  = request.getParameter("AdminiSortCode");
		String tEconomElemenCode  = request.getParameter("EconomElemenCode");
		String tLevelCode  = request.getParameter("LevelCode");
		String tBusiTypeCode  = request.getParameter("BusiTypeCode");
		String tAssociateClass  = request.getParameter("AssociateClass");
		
		String tSpecialCode  = request.getParameter("SpecialCode");
		String tRiskcode  = request.getParameter("Riskcode");
		String tICDCode = request.getParameter("ICDCode");
		
		String tContraState = request.getParameter("ContraState");
		String tDutyName = request.getParameter("DutyName");
		String tDutyItem = request.getParameter("DutyItem");
		String tDutyItemState = request.getParameter("DutyItemState");
		String FlagStr = "";
		String Content = "";
		String strOperation = "";
		
		
		TransferData tTransferData  = new TransferData();
		tTransferData.setNameAndValue("MngCom",tMngCom);
		tTransferData.setNameAndValue("HospitalType",tHospitalType);
		tTransferData.setNameAndValue("HospitCode",tHospitCode);
		tTransferData.setNameAndValue("HospitName",tHospitName);
		tTransferData.setNameAndValue("AreaCode",tAreaCode);
		tTransferData.setNameAndValue("AreaName",tAreaName);
		tTransferData.setNameAndValue("CommunFixFlag",tCommunFixFlag);
		tTransferData.setNameAndValue("AdminiSortCode",tAdminiSortCode);
		tTransferData.setNameAndValue("EconomElemenCode",tEconomElemenCode);
		tTransferData.setNameAndValue("LevelCode",tLevelCode);
		tTransferData.setNameAndValue("BusiTypeCode",tBusiTypeCode);
		tTransferData.setNameAndValue("AssociateClass",tAssociateClass);
		tTransferData.setNameAndValue("SpecialCode",tSpecialCode);
		tTransferData.setNameAndValue("Riskcode",tRiskcode);
		tTransferData.setNameAndValue("ICDCode",tICDCode);
		tTransferData.setNameAndValue("ContraState",tContraState);
		tTransferData.setNameAndValue("DutyName",tDutyName);
		tTransferData.setNameAndValue("DutyItem",tDutyItem);
		tTransferData.setNameAndValue("DutyItemState",tDutyItemState);

	  	GlobalInput tG = new GlobalInput();
			tG = (GlobalInput)session.getValue("GI");
			//System.out.println(tG.Operator);
			
			VData tVData = new VData();
			VData mResult = new VData();
			CErrors mErrors = new CErrors();
			tVData.addElement(tTransferData);
	    tVData.addElement(tG);
	    HospitalSearchExpBL tHospitalSearchExpBL = new HospitalSearchExpBL();
		  XmlExport txmlExport = new XmlExport();    
	    if(!tHospitalSearchExpBL.submitData(tVData,"PRINT"))
	    {
	        //System.out.println("zsj");
	       	operFlag=false;
	       	Content=tHospitalSearchExpBL.mErrors.getFirstError().toString();                 
	    }
	    else
	    { 
	    	System.out.println("--------成功----------");  
				mResult = tHospitalSearchExpBL.getResult();			
		  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
		  	if(txmlExport==null)
		  	{
		   		operFlag=false;
		   		Content="没有得到要显示的数据文件";	  
		  	}
			}
		
		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
  tLDSysVarDB.setSysVar("VTSFilePath");
  tLDSysVarDB.getInfo();
  String vtsPath = tLDSysVarDB.getSysVarValue();
  if (vtsPath == null)
  {
    vtsPath = "vtsfile/";
  }
  
  String filePath = application.getRealPath("/").replace('\\', '/') + "/" + vtsPath;
  String fileName = "TASK" + FileQueue.getFileName()+".vts";
  String realPath = filePath + fileName;
	
	if (operFlag==true)
	{
    //合并VTS文件 
    String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    //System.out.println("templatePath =>" + templatePath);
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    //System.out.println("888888888888888888888");
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);
    
    //把dataStream存储到磁盘文件
    AccessVtsFile.saveToFile(dataStream, realPath);
    response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath=" + realPath);
	}
	else
	{
    	FlagStr = "Fail";

%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>