<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：EsPicDownload.jsp
//程序功能：扫描单证下载的主页面
//创建日期：2005-08-25
//创建人  
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="EsPicDownload.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="EsPicDownloadInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./EsPicDownloadSave.jsp" method=post name=fm target="fraSubmit">
    <%//@include file="../common/jsp/OperateButton.jsp"%>
    <%//@include file="../common/jsp/InputButton.jsp"%>
    
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 扫描单证信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divCode1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
          </TD>
          <!--TD  class= title>
            扫描日期
          </TD>
          <TD  class= input>
            <Input class= coolDatePicker name=MakeDate >
          <TD  class= title>
            扫描员
          </TD>
          <TD  class= input>
            <Input class= common name=ScanOperator >
          </TD>
        </TR>
        <TR  class= common-->
          <TD  class= title>
            单证类型
          </TD>
          <TD  class= input>
          	<input class=codename style="width:50px" name=conttype><Input class=code style="width:100px" name=conttypename ondblclick="return showCodeList('lhscantype',[conttype,this],[0,1],null, null, null, 1);" onkeyup="return showCodeListKey('lhscantype',[conttype,this],[0,1],null, null, null, 1);">
          </TD>
          <TD  class= title>
            单证号码
          </TD>
          <TD  class= input>
            <Input class= common name=DocCode >
          </TD>
        </TR>       
      </table>
      <!--Input class=code name=ComCode ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);" >
      <!--Input class= common name=ComCode -->
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <table>
    	<tr>
        	<td class=common>
			        <INPUT VALUE="查 询" TYPE=button class=cssButton onclick="easyqueryClick()"> 	
			        <!--INPUT VALUE="未下载过的单证" TYPE=button class=cssButton onclick="easyqueryClick()"> 	
			        <INPUT VALUE="已下载过的单证" TYPE=button class=cssButton onclick="easyqueryClick2()"--> 	
    		</td>
    	</tr>
    </table>
    
    <!-- ES_DOC_PAGES数据区 MultiLine -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCodeGrid);">
    		</td>
    		<td class= titleImg>
    			 扫描件单证页信息
    		</td>
    	</tr>
    </table>
    <Input TYPE=hidden  name=docid value="">
    <Input TYPE=hidden  name=prtNo value="">
    <Input TYPE=hidden  name=SubType value="">
    <Input TYPE=hidden  name=BussNoType value="">
    <Input TYPE=hidden  name=BussType value="">
    
  	<Div align=center id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCodeGrid" >
  					</span> 
  			  	</td>
  			</tr>
  			</table>
    	<br>
      	<INPUT VALUE="首页" TYPE=button class=cssButton onclick="getFirstPage();"> 
      	<INPUT VALUE="上一页" TYPE=button class=cssButton onclick="getPreviousPage();"> 					
      	<INPUT VALUE="下一页" TYPE=button class=cssButton onclick="getNextPage();"> 
      	<INPUT VALUE="尾页" TYPE=button class=cssButton onclick="getLastPage();"> 					
  	</div>
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
    <!--<INPUT VALUE="查看" TYPE=button class=cssButton onclick="ShowPagesDetails()"> -->
     
    <INPUT VALUE="下载选中单证" TYPE=button name=DownloadButton class=cssButton onclick="DownloadPic()"> 
    <INPUT VALUE="下载所有单证" TYPE=button name=DownloadAllButton class=cssButton onclick="DownloadAllPic()"> 
    <INPUT VALUE="" type=hidden name=DownloadFlag> 
    <INPUT VALUE="" type=hidden name=DownloadType> 
		<INPUT VALUE="" type=hidden name=EXESql> 
  			  	</td>
  			</tr>
  			  </table>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
