<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-06-13 10:00:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="LDTestGrpMgtInput.js"></SCRIPT>
  <%@include file="LDTestGrpMgtInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDTestGrpMgtSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDTestGrpMgt1);">
    		</td>
    	  <td class= titleImg>
        		 体检套餐基本信息
        </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDTestGrpMgt1" style= "display: ''">
	<table  class= common align='center' >
	  <TR  class= common>
	    <TD  class= title>
	      体检套餐代码
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=TestGrpCode elementtype=nacessary verify="体检套餐代码|NOTNULL&len<=20">
	    </TD>
	    <TD  class= title>
	      体检套餐名称
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=TestGrpName elementtype=nacessary verify="体检套餐名称|NOTNULL&len<=50">
	    </TD>
	    <TD  class= title>
	      体检套餐类型
	    </TD>
	    <TD  class= input>
	      <input class=codeno name=TestGrpType ondblclick="showCodeList('hmtestgrptype',[this,TestGrpType_ch],[0,1],null,null,null,1);" onkeyup=" if (event.keyCode=='13') return showCodeList('hmtestgrptype',[this,TestGrpType_ch],[0,1],null,null,null,1);" ><Input class= 'codename' name=TestGrpType_ch >
	    </TD>
	  </TR>
	  <TR  class= common id=belly style="display:'none'">
	    <TD  class= title>
	      是否空腹
	    </TD>
	    <TD  class= input>
	       <input class=codeno name=GrpInBelly_ch id=GrpInBelly_ch CodeData="0|^Y|是^N|否"; ondblclick="showCodeListEx('',[this,GrpInBelly],[0,1],null,null,null,1);"><input class=codename name = GrpInBelly>
	    </TD>
	    <TD  class= title>
	    </TD>
	    <TD  class= input>
	    </TD> 
	    <TD  class= title>
	    </TD>
	    <TD  class= input>
	    </TD>
	  </TR>
	  <TR  class= common id=HosptialInfo name=HosptialInfo style="display:'none'">
     <TD class= title>
    	 医疗机构代码
     </TD>
     <TD  class= input>
       <Input class= 'code' name=HospitCode style="width:160px" ondblclick="showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName','',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');"   onkeydown="return showCodeListKey('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName'); codeClear(HospitName,HospitCode);" >
      </TD> 
     <TD class= title>
    	 医疗机构名称
     </TD>
     <TD  class= input>
       <Input class= 'code' name=HospitName style="width:160px"   ondblclick="showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName','',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName');"   onkeydown="return showCodeListKey('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName'); codeClear(HospitName,HospitCode);" >
      </TD>  
	   <TD  class= title> 
	    </TD>
	    <TD  class= input>
	    </TD>
	  </TR>
	</table>
    </Div>
    
    	<table>
		  	<tr>
		      <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,TestGrpItem1);">
		  		</td>
		  		<td class=titleImg>
		  			 套餐项目基本信息
		  		</td>
		  	</tr>
		  </table>
		
			<TABLE style="" cellSpacing=0 cellPadding=0 width=300 border=0 SCROLLBAR-FACE-COLOR: #799AE1; SCROLLBAR-HIGHLIGHT-COLOR: #799AE1; SCROLLBAR-SHADOW-COLOR: #799AE1; SCROLLBAR-DARKSHADOW-COLOR: #799AE1; SCROLLBAR-3DLIGHT-COLOR: #799AE1; SCROLLBAR-ARROW-COLOR: #FFFFFF; SCROLLBAR-TRACK-COLOR: #AABFEC>
					
						<TR>
							<TD id=mulline1>
									<span id="spanTestGrpItemGrid">
									</span> 
							</TD>
						</TR>
						<TR id=mulline2 style="display:'none'">
							<TD>
								<span id="spanTestGrpCombineGrid">
									  			
							</TD>
						</TR>
			</TABLE>
		
    <Div id= "divPage"  align=center style= "display: 'none' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	  </Div>    
    
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden name="MakeDate">
    <input type=hidden name="MakeTime">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
