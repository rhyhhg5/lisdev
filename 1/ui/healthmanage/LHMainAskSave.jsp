<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLMainAskSave.jsp
//程序功能：
//创建日期：2005-01-12 16:10:37
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLMainAskSchema tLLMainAskSchema   = new LLMainAskSchema();
  OLLMainAskUI tOLLMainAskUI   = new OLLMainAskUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
 String Path = application.getRealPath("config//Conversion.config");	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLLMainAskSchema.setLogNo(request.getParameter("LogNo"));
    tLLMainAskSchema.setLogState(request.getParameter("LogState"));
    tLLMainAskSchema.setAskType(request.getParameter("AskType"));
    tLLMainAskSchema.setOtherNo(request.getParameter("OtherNo"));
    tLLMainAskSchema.setOtherNoType(request.getParameter("OtherNoType"));
    tLLMainAskSchema.setAskMode(request.getParameter("AskMode"));
    tLLMainAskSchema.setLogerNo(request.getParameter("LogerNo"));
    tLLMainAskSchema.setLogName(request.getParameter("LogName"));
    tLLMainAskSchema.setLogComp(request.getParameter("LogComp"));
    tLLMainAskSchema.setLogCompNo(request.getParameter("LogCompNo"));
    tLLMainAskSchema.setLogDate(request.getParameter("LogDate"));
    tLLMainAskSchema.setLogTime(request.getParameter("LogTime"));
    tLLMainAskSchema.setPhone(request.getParameter("Phone"));
    tLLMainAskSchema.setMobile(request.getParameter("Mobile"));
    tLLMainAskSchema.setPostCode(request.getParameter("PostCode"));
    tLLMainAskSchema.setAskAddress(request.getParameter("AskAddress"));
    tLLMainAskSchema.setEmail(request.getParameter("Email"));
    tLLMainAskSchema.setAnswerType(request.getParameter("AnswerType"));
   // tLLMainAskSchema.setAnswerMode(request.getParameter("AnswerMode"));
    tLLMainAskSchema.setSendFlag(request.getParameter("SendFlag"));
    tLLMainAskSchema.setSwitchCom(request.getParameter("SwitchCom"));
    tLLMainAskSchema.setSwitchDate(request.getParameter("SwitchDate"));
    tLLMainAskSchema.setSwitchTime(request.getParameter("SwitchTime"));
    tLLMainAskSchema.setReplyFDate(request.getParameter("ReplyFDate"));
    tLLMainAskSchema.setDealFDate(request.getParameter("DealFDate"));
    tLLMainAskSchema.setRemark(request.getParameter("Remark"));
    tLLMainAskSchema.setAvaiFlag(request.getParameter("AvaiFlag"));
   // tLLMainAskSchema.setNotAvaliReason(request.getParameter("NotAvaliReason"));
       tLLMainAskSchema.setMakeDate(request.getParameter("MakeDate"));
    tLLMainAskSchema.setMakeTime(request.getParameter("MakeTime"));
    tLLMainAskSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLLMainAskSchema.setModifyTime(request.getParameter("ModifyTime"));
    tLLMainAskSchema.setOperator(request.getParameter("Operator"));
    tLLMainAskSchema.setMngCom(request.getParameter("MngCom"));
    
       //工作流标记
  		TransferData mf = new TransferData();
  		mf.setNameAndValue("WFFLAG","0");
  		String logNo = tLLMainAskSchema.getLogNo();
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLLMainAskSchema);
  	tVData.add(tG);
  	tVData.add(mf);
    tOLLMainAskUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLLMainAskUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    	 
    	
    }
    else                                                                           
    {
 
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  if ( FlagStr.equals("Success") && "INSERT||MAIN".equals(transact) )
  {
    VData res = tOLLMainAskUI.getResult();
    TransferData tf = (TransferData) res.getObjectByObjectName("TransferData", 0);
    logNo =(String) tf.getValueByName("logNo");
  
  }
  //添加各种预处理
  
%>                      

<html>
<script language="javascript">
    <%if(logNo.equals("")){%>
    	parent.fraInterface.fm.all("LogNo").value="";
        <%}else{%>	
	parent.fraInterface.fm.all("LogNo").value = <%=logNo%>;		
	<%}%>
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.displayMain();
</script>
</html>
