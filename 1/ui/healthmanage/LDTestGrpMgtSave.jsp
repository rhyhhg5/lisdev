<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDTestGrpMgtSave.jsp
//程序功能：
//创建日期：2005-06-13 10:00:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期          更新原因/内容
//           郭丽颖  2006-06-23 11:00:12     
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDTestGrpMgtSchema tLDTestGrpMgtSchema   = new LDTestGrpMgtSchema();
  LDTestGrpMgtSet tLDTestGrpMgtSet = new LDTestGrpMgtSet();
  OLDTestGrpMgtUI tOLDTestGrpMgtUI   = new OLDTestGrpMgtUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String TestGrpType = request.getParameter("TestGrpType");
  transact = request.getParameter("fmtransact");
    
    tLDTestGrpMgtSchema.setTestGrpCode(request.getParameter("TestGrpCode"));
    tLDTestGrpMgtSchema.setTestGrpName(request.getParameter("TestGrpName"));
    tLDTestGrpMgtSchema.setTestGrpType(request.getParameter("TestGrpType"));
    tLDTestGrpMgtSchema.setExplain(request.getParameter("HospitCode"));//医疗机构代码
    tLDTestGrpMgtSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDTestGrpMgtSchema.setMakeTime(request.getParameter("MakeTime"));
    
    System.out.println(tLDTestGrpMgtSchema.getTestGrpCode());
    if(TestGrpType.equals("5"))
    {
	    	String tTestGrpCombine[] = request.getParameterValues("TestGrpCombineGrid1");
	    	String tTestGrpIsEmpty[] = request.getParameterValues("TestGrpCombineGrid3");
			    
		    int TestGrpCombineGridCount = 0;
		    if(tTestGrpCombine != null)
		    {
		    		TestGrpCombineGridCount = tTestGrpCombine.length;
		    		
		    		for(int i = 0; i < TestGrpCombineGridCount; i++)
		    		{
		    				LDTestGrpMgtSchema sLDTestGrpMgtSchema   = new LDTestGrpMgtSchema();
		    				
		    				sLDTestGrpMgtSchema.setTestGrpCode(request.getParameter("TestGrpCode"));
		    				sLDTestGrpMgtSchema.setTestGrpName(request.getParameter("TestGrpName"));
		    				sLDTestGrpMgtSchema.setTestGrpType(request.getParameter("TestGrpType"));
		    				sLDTestGrpMgtSchema.setExplain(request.getParameter("HospitCode"));//医疗机构代码
		    				sLDTestGrpMgtSchema.setTestItemCode(tTestGrpCombine[i]);
//		    				sLDTestGrpMgtSchema.setOtherSign(tTestGrpIsEmpty[i]);
		    				tLDTestGrpMgtSet.add(sLDTestGrpMgtSchema);
		    		}
		    }
    }
    if(TestGrpType.equals("4"))
    {
	    	String tTestGrpCombine[] = request.getParameterValues("TestGrpCombineGrid1");
	    	String tTestGrpIsEmpty[] = request.getParameterValues("TestGrpCombineGrid3");
			    
		    int TestGrpCombineGridCount = 0;
		    if(tTestGrpCombine != null)
		    {
		    		TestGrpCombineGridCount = tTestGrpCombine.length;
		    		
		    		for(int i = 0; i < TestGrpCombineGridCount; i++)
		    		{
		    				LDTestGrpMgtSchema sLDTestGrpMgtSchema   = new LDTestGrpMgtSchema();
		    				
		    				sLDTestGrpMgtSchema.setTestGrpCode(request.getParameter("TestGrpCode"));
		    				sLDTestGrpMgtSchema.setTestGrpName(request.getParameter("TestGrpName"));
		    				sLDTestGrpMgtSchema.setTestGrpType(request.getParameter("TestGrpType"));
		    				
		    				sLDTestGrpMgtSchema.setTestItemCode(tTestGrpCombine[i]);
//		    				sLDTestGrpMgtSchema.setOtherSign(tTestGrpIsEmpty[i]);
		    				tLDTestGrpMgtSet.add(sLDTestGrpMgtSchema);
		    		}
		    }
    }
    
    else
    {
		    String tTestItemCode[] = request.getParameterValues("TestGrpItemGrid1");
				    
		    int TestGrpItemGridCount = 0;
		    if(tTestItemCode != null)
		    {
		    		TestGrpItemGridCount = tTestItemCode.length;
		    		
		    		for(int i = 0; i < TestGrpItemGridCount; i++)
		    		{
		    				LDTestGrpMgtSchema sLDTestGrpMgtSchema   = new LDTestGrpMgtSchema();
		    				
		    				sLDTestGrpMgtSchema.setTestGrpCode(request.getParameter("TestGrpCode"));
		    				sLDTestGrpMgtSchema.setTestGrpName(request.getParameter("TestGrpName"));
		    				sLDTestGrpMgtSchema.setTestGrpType(request.getParameter("TestGrpType"));
		    				sLDTestGrpMgtSchema.setOtherSign(request.getParameter("GrpInBelly"));
		    				sLDTestGrpMgtSchema.setExplain(request.getParameter("HospitCode"));//医疗机构代码
		    				sLDTestGrpMgtSchema.setTestItemCode(tTestItemCode[i]);
		    				tLDTestGrpMgtSet.add(sLDTestGrpMgtSchema);
		    		}
		    }
    }
    
  try
  {
  // 准备传输数据 VData
    System.out.println("------准备传输数据 VData--------");
  	VData tVData = new VData();
	  tVData.add(tLDTestGrpMgtSchema);
	  tVData.add(tLDTestGrpMgtSet);
  	tVData.add(tG);
    tOLDTestGrpMgtUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDTestGrpMgtUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
