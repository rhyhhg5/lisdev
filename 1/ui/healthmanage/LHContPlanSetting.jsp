<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHContPlanSetting.jsp
//程序功能：
//创建日期：2006-06-26- 12:44 
//创建人  ：郭丽颖 
//更新记录：  更新人    更新日期     更新原因/内容
//             
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<SCRIPT src="LHContPlanSetting.js"></SCRIPT>
	<%@include file="LHContPlanSettingInit.jsp"%>
</head>
<SCRIPT language=javascript1.2>
function showsubmenu()
{
    whichEl = eval('submenu' );
    if (whichEl.style.display == 'none')
    {
		//submenu1.style.display='';
		evalval("submenu1.style.display='';");
		submenu.style.display='';
    }
    else
    {
		submenu.style.display='none';
        eval("submenu1.style.display='';");
    }
    var strSql = "select PrtNo from lccont where ContNo='"+fm.all('ContNo').value+"'";
        prtNo = easyExecSql(strSql);
    var url="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo="+prtNo+"&SubType=TB1001&BussType=TB&BussNoType=11"
    document.getElementById("iframe0").src=url;
}

function ScanFrameCtrl()
{
	if(parent.fraMain.rows == "0,0,0,0,0,*")
	{	parent.fraMain.rows = "0,48%,0,0,0,*";}
	else if(parent.fraMain.rows == "0,48%,0,0,0,*")
	{	parent.fraMain.rows = "0,0,0,0,0,*";}	
}
</SCRIPT>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
   <span id="operateButton" >
	<table  class=common align=center>
		<tr align=right>
			<td class=button >
				<INPUT class=cssButton VALUE="个人保单扫描件"  TYPE=hidden onclick="ScanFrameCtrl();">
			</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="saveButton3" VALUE="保  存"  TYPE=hidden onclick="return submitForm();">
			</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=hidden onclick="return updateClick();">
			</td>	
		</tr>
	</table>
</span>
    <%@include file="../common/jsp/InputButton.jsp"%>
		<div style="display:''" id='submenu1'>
    <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServPlan1);">
    	 	</td>
    	 <td class= titleImg>
        	 个人服务计划表基本信息
       	</td>   		 
       
    	</tr>
    </table>
    <Div  id= "divLHServPlan1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>  

			    <TD  class= title>
			      客户号码
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=CustomerNo verify="客户号码|NOTNULL&len<=20" readonly>
			    </TD>
			    <TD  class= title>
			       客户姓名
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=Name verify="客户姓名|NOTNULL&len<=300" readonly>
			    </TD>
			   <TD  class= title>
			      保单号
			    </TD>   
				 <TD  class= input>
			      <Input class= 'common' name=ContNo verify="保单号|NOTNULL&len<=20" readonly>
			    </TD>
			</TR>
			<TR  class= common>
					<TD  class= title>
			      个人服务计划名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServPlanName readonly verify="个人服务计划名称|NOTNULL&len<=300" ><!--ondblclick="return showCodeList('lhservplanname',[this,ServPlanLevel,ServPlanCode,ServPlayType],[0,1,2,4],null,fm.ServPlanName.value,'ServPlanName');"-->
			      <Input class= 'common' type=hidden name=ServPlanCode ><!--服务计划代码-->
			    </TD>
			    <TD  class= title>
			      个人服务计划档次
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' readonly name=ServPlanLevel verify="个人服务计划档次|NOTNULL&len<=10" >
			    </TD>
				<TD  class= title>
			      保单所属机构标识
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ComID  verify="保单所属机构标识|NOTNULL&len<=10" readonly>
			      
			    </TD>

			</TR> 
			<TR  class= common>
			    <TD  class= title>
			      服务计划起始时间
			    </TD>
			    <TD  class= input>
			      <Input class= 'CoolDatePicker' name=StartDate verify="服务计划起始时间|DATE&NOTNULL" >
			    </TD>
			    <TD  class= title>
			      服务计划终止时间
			    </TD>
			    <TD  class= input>
			      <Input class= 'CoolDatePicker' name=EndDate verify="服务计划终止时间|DATE&NOTNULL">
			    </TD>
				<TD  class= title>
					健管保费（元）
				</TD>
				<TD  class= input>
					<Input class= 'common' style="width:150px" name=ServPrem verify="健管保费（元）|NOTNULL&NUM" ><font color="#FF0000"> *</font>
				</TD>
			</TR> 
		</table>
    </Div>
    
	<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServPlanGrid);">
    		</td>
    		 <td class= titleImg>
        		 个人服务计划事件信息
       		 </td>
       		 <td>
       		 	<input type= hidden class=cssButton name=ServTrace value="个人服务列表" OnClick="toServTrace();">
       		 </td>   		 
    	</tr>
    </table>
    
	<Div id="divLHServPlanGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServPlanGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
    
    
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmAction" name="fmAction">
		<!--团体服务计划号码-->
		<Input class= 'common' type=hidden  name=GrpServPlanNo >

		<!--个人服务计划号码-->
		<Input class= 'common' type=hidden   name=ServPlanNo >
	      
		<!--服务计划类型-->
		<Input class= 'common' type=hidden name=ServPlayType >
		<Input class= 'common' type=hidden name=CaseState >
		<Input class= 'common' type=hidden name=GrpContNo >
		<Input class= 'common' type=hidden name=ManageCom >
	    <Input class= 'common' type=hidden name=Operator >
	    <Input class= 'common' type=hidden name=MakeDate >
	    <Input class= 'common' type=hidden name=MakeTime >
	    <Input class= 'common' type=hidden name=HiddenCustom >
	    <Input class= 'common' type=hidden name=prtNo >
  	</form>
  	<form action=""  method=post name=fms target="fraSubmit">
  		<table>
  			<tr>
  				<td>
         	<input type= button class=cssButton  style="width:120px" name=saveButton value="个人服务事件设置" OnClick="submitForm();">
         	
         </td>
        </tr>
    </table>
<Div  id= "divShowInfo" style= "display: 'none'">	
			  <table class=common>
	    	<tr class=common>
	    		<table>
		    		<tr>
				      <td class=common>
						    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
				  		</td>
				  		<td class=titleImg>
				  			 服务事件信息设置
				  		</td>
				  	</tr>
			  	</table>	
			  	<table>
        	 <tr>
           <td>
             	<input type= button class=cssButton  style="width:120px" name=DetailsInfo value="集体服务事件查询" OnClick="AddNewQuery();">
            </td> 
            <td>
             	<input type= hidden class=cssButton style="width:120px"  name=QieStatus value="新增服务事件" OnClick="AddNewHmPlan();">
            </td> 
           </tr>
        </table>
        <table>
		<table  class=common align=center>
			<tr align=right>
				<td class=button >
					&nbsp;&nbsp;
				</td>
				<td class=button width="10%" align=right>
					<INPUT class=cssButton name="saveButton2" VALUE="保  存"  TYPE=hidden onclick="return submitForm2();">
				</td>
				<td class=button width="10%" align=right>
					<INPUT class=cssButton name="modifyButton2" VALUE="修  改"  TYPE=hidden onclick="return updateClick2();">
				</td>			
			</tr>
		</table>

</div> 
<Div  id= "divLHItemPriceFactorGrid" style= "display: 'none'">	
	 
	 <table class=common> 	
	 		<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHItemPriceFactorGrid"></span> 
	  				<br>
	  			</td>
	  	</tr>
			</table>
</div>
	 <tr>
   <td>
     	<input type= hidden class=cssButton  style="width:120px" name=DetailsInfo value="详细情况查看" OnClick="">
    </td> 
   </tr>
</table>
<table>
	 <tr>
	  <td>
     	<input type= button class=cssButton  style="width:120px" name=QieSuccess value="服务事件设置完成" OnClick="ServSetSucced();">
    </td> 
    
   </tr>
</table>
    <input type=hidden id="fmtransact" name="fmtransact2">
		<input type=hidden id="fmAction" name="fmAction2">  
			<Input class= 'common' type=hidden name=ServItemCode2 >
	    <Input class= 'common' type=hidden name=ComID2 >
	    <Input class= 'common' type=hidden name=ServItemNo2 >
			<Input class= 'common' type=hidden name=ManageCom2 >
	    <Input class= 'common' type=hidden name=Operator2 >
	    <Input class= 'common' type=hidden name=MakeDate2 >
	    <Input class= 'common' type=hidden name=MakeTime2 >
</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>

</html>
