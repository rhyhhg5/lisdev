<%
//程序名称：LHStandardTaskDefineInit.jsp
//程序功能：
//创建日期：2006-07-29 14:55:40
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('TaskNo').value = "";
    fm.all('TaskName').value = "";
    fm.all('ServTaskModule').value = "";
    fm.all('ServTaskModuleName').value = "";
    fm.all('ServTaskDes').value = "";
  }
  catch(ex)
  {
    alert("在LHEventTypeSettingInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                        
function initForm()
{
  try
  {
    initInpBox();
    initLHQueryDetailGrid();
  }
  catch(re)
  {
    alert("LHEventTypeSettingInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var d = new Date();
var h = d.getYear();
var m = d.getMonth(); 
var day = d.getDate();  
var Date2;       
if(h<10){h = "0"+d.getYear();}  
if(m<9){ m++; m = "0"+m;}
else{m++;}
if(day<10){day = "0"+d.getDate();}
Date2 = h+"-"+m+"-"+day;
var LHQueryDetailGrid;
function initLHQueryDetailGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
	  
	  
	  iArray[1]=new Array(); 
		iArray[1][0]="标准服务项目代码";   
		iArray[1][1]="88px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
	  iArray[1][4]="hmservitemcode";         //列名  
	  iArray[1][5]="1|2";    //引用MulLine的对应第几列，'|'为分割符 
		iArray[1][6]="0|1";    //上面的列中放置下拉菜单中第几位值 
		iArray[1][15]="ServItemCode"; 
		iArray[1][17]="3"; 
		iArray[1][18]="310";     //下拉框的宽度 
		iArray[1][19]="1" ;      //强制刷新数据源
  
	  iArray[2]=new Array(); 
		iArray[2][0]="标准服务项目名称";   
		iArray[2][1]="88px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="hmservitemcode";         //列名  
	  iArray[2][5]="2|1";    //引用MulLine的对应第几列，'|'为分割符 
		iArray[2][6]="1|0";    //上面的列中放置下拉菜单中第几位值 
		iArray[2][15]="ServItemName"; 
		iArray[2][17]="3"; 
		iArray[2][18]="310";     //下拉框的宽度 
		iArray[2][19]="1" ;      //强制刷新数据源
    
    iArray[3]=new Array(); 
		iArray[3][0]="SerialNo";   
		iArray[3][1]="0px";   
		iArray[3][2]=20;        
		iArray[3][3]=3; 
	
	                               
    LHQueryDetailGrid = new MulLineEnter( "fm" , "LHQueryDetailGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHQueryDetailGrid.mulLineCount = 0;                             
    LHQueryDetailGrid.displayTitle = 1;                          
    LHQueryDetailGrid.hiddenPlus = 0;                          
    LHQueryDetailGrid.hiddenSubtraction = 0;                          
    LHQueryDetailGrid.canSel = 1;                          
    LHQueryDetailGrid.canChk = 0;                          
                    
                                                             
    LHQueryDetailGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHQueryDetailGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
