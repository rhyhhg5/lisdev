<%
//程序名称：LHCustomInHospitalQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-18 09:08:27
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CustomerNo').value = "";
    fm.all('CustomerName').value = "";
  // fm.all('InHospitNo').value = "";
  // fm.all('InHospitMode').value = "";
  // fm.all('InHospitDate').value = "";
  // fm.all('HospitCode').value = "";
  // fm.all('HospitSectio').value = "";
  // fm.all('Doctor').value = "";
  // fm.all('DiseasCode').value = "";
  // fm.all('DiagnoCircs').value = "";
  // fm.all('CureEffectCode').value = "";
  // fm.all('CureEffect').value = "";
  // fm.all('TotalFee').value = "";
  // fm.all('TestFee').value = "";
  // fm.all('OPSFee').value = "";
  // fm.all('MedicaFee').value = "";
  // fm.all('BedFee').value = "";
  // fm.all('OtherFee').value = "";
  // fm.all('Operator').value = "";
  // fm.all('MakeDate').value = "";
  // fm.all('MakeTime').value = "";
  // fm.all('ModifyDate').value = "";
  // fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LHCustomInHospitalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHCustomInHospitalGrid();  
  }
  catch(re) {
    alert("LHCustomInHospitalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHCustomInHospitalGrid;
function initLHCustomInHospitalGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="客户号码";   
	  iArray[1][1]="40px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
    
    iArray[2]=new Array(); 
	  iArray[2][0]="客户姓名";   
	  iArray[2][1]="40px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
    
    iArray[3]=new Array(); 
	  iArray[3][0]="就诊及住院号码";   
	  iArray[3][1]="0px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=3;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="医疗机构名称";   
	  iArray[4][1]="140px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	         
	  iArray[5]=new Array(); 
	  iArray[5][0]="就诊时间";   
	  iArray[5][1]="40px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=0;
	  
	  
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="诊断医师姓名";   
	  iArray[6][1]="0px";   
	  //iArra6[5][2]=20;        
	  iArray[6][3]=3;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="就诊方式 ";   
	  iArray[7][1]="40px";   
	  //iArra7[6][2]=20;        
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="主要治疗方式 ";   
	  iArray[8][1]="0px";   
	  iArray[8][3]=3;
	  
	  iArray[9]=new Array(); 
	  iArray[9][0]="主要诊断流水号 ";   
	  iArray[9][1]="0px";   
	  iArray[9][3]=3;
	    
	  iArray[10]=new Array();         
	  iArray[10][0]="Hospitcode";
	  iArray[10][1]="0px";            
	  iArray[10][3]=3;  
	  
	  iArray[11]=new Array();         
	  iArray[11][0]="Doctno";
	  iArray[11][1]="0px";                          
	  iArray[11][3]=3;                  
	  
    
    LHCustomInHospitalGrid = new MulLineEnter( "fm" , "LHCustomInHospitalGrid" ); 
    //这些属性必须在loadMulLine前

    LHCustomInHospitalGrid.mulLineCount = 0;   
    LHCustomInHospitalGrid.displayTitle = 1;
    LHCustomInHospitalGrid.hiddenPlus = 1;
    LHCustomInHospitalGrid.hiddenSubtraction = 1;
    LHCustomInHospitalGrid.canSel = 1;
    LHCustomInHospitalGrid.canChk = 0;
    //LHCustomInHospitalGrid.selBoxEventFuncName = "showOne";

    LHCustomInHospitalGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomInHospitalGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
