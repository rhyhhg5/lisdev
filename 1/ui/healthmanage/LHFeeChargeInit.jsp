<%
//程序名称：LHFeeChargeInit.jsp
//程序功能：健管服务预约管理
//创建日期：2006-09-01 15:51:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
	String ServTaskNo = request.getParameter("ServTaskNo");
	String sqlServTaskNo = "'"+ServTaskNo.replaceAll(",","','")+"'";
    String flag = request.getParameter("flag");
%>       
           
<script language="JavaScript">     
var turnPage = new turnPageClass();    

function initBox()
{
	fm.all('Fee').value = "";	
}

function initForm()
{
	try
	{
		initLHFeeChargeGrid();
		initBox();

		var sFlag="";
		var nFlag="";
		var arrServTaskNo = "<%=ServTaskNo%>".split(",");
		for(var i=0; i<arrServTaskNo.length; i++)
		{
			var	cSql=" select count(*) from LHFeeCharge d where d.SERVTASKNO ='"+arrServTaskNo[i]+"' " ;	
			var arrcSql = easyExecSql(cSql);
			if(arrcSql == "0")
			{//此任务未被保存在LHFeeCharge
				nFlag="1";
			}
			else
			{//此任务已被保存在LHFeeCharge
				sFlag="1";
			}
		}
		
		if(nFlag=="1" && sFlag=="1")
		{
			alert("传入的任务中，同时出现已保存和未保存在结算表的数据，传入的任务必须状态一致！");
			return false;
		}

		var Sql = " select A.SERVITEMNO,S.Customerno from LHTASKCUSTOMERRELA A, LHSERVITEM S WHERE  A.SERVTASKNO in ("+"<%=sqlServTaskNo%>"+") AND A.SERVITEMNO = S.SERVITEMNO "
		var arrSql = easyExecSql(Sql);
		
		if(arrSql=="" || arrSql==null || arrSql=="null")
		{//校验数据在LHTASKCUSTOMERRELA存在，在LHSERVITEM不存在的情况
			alert("数据信息严重错误！服务项目表没有数据！");	
			return false;
		}
		
		for(var i=0; i<arrSql.length; i++)
		{//校验一次结算信息是否对应不等于1个的结算对象信息的情况
			var sqlCount = "select count(TASKEXECNO) from LHTASKCUSTOMERRELA where SERVTASKCODE = '01601' and SERVITEMNO = '"+arrSql[i][0]+"'";
			if (easyExecSql(sqlCount) != "1")
			{
				alert("客户 "+arrSql[i][1]+" 的"+arrSql[i][0]+"号服务项目数据有误，不存在或者存在多行，请检查");
				return false;
			}
		}

	    var flag= "<%=flag%>";
		if(flag=="1")
	    {//针对第三方费用结算的处理
			var result=new Array;
			var	strsql=" select TaskExecNo from LHFeeCharge d where d.SERVTASKNO in ("+"<%=sqlServTaskNo%>"+")" ;
	        result=easyExecSql(strsql);
	        if(result=="" || result==null || result=="null")
	        {//未保存过
    	       displayInfo();
    	       fm.all('save').disabled=false;
    	    }
    	    if(result!=""&&result!="null"&&result!=null)
    	    {
				displayConetent();
				fm.all('save').disabled=true;
    	    }
	    }
	}
	catch(re)
	{      
    	alert("LHHmBespeakManageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}      
}        
function displayInfo()
{
	var strsql=" select  (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo) ,"
		      +" (select a.CustomerName from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo),"
		      +" d.ServCaseCode,(select b.ServCaseName from LHServCaseDef b where b.ServCaseCode=d.ServCaseCode),"
		      +" (select c.ServTaskName from LHServTaskDef c where c.ServTaskCode=d.ServTaskCode),d.ServTaskCode,"
		      +" (select  h.ServItemName from LHHealthServItem h ,LHServItem a where h.ServItemCode=a.ServItemCode and a.ServItemNo=d.ServItemNo),"
		      +" (select a.ServItemType from LHServItem a where a.ServItemNo=d.ServItemNo),"
		      +" (select a.ContNo from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo),"
		      +" '',(select ContraName from LHGroupCont where LHGroupCont.ContraNo=p.ContraNo),"
		      +" '',(select b.DutyItemName from LDContraItemDuty b where b.DutyItemCode=(select t.DutyItemCode from LHContItem t where t.ContraItemNo = p.servplanno)),"
		      +" p.contrano, p.hospitcode,d.TaskExecNo,d.ServItemNo, d.ServTaskNo, "
		      +" value((select x.MoneySum from LHContServPrice x where  x.contrano = p.ContraNo and x.contraitemno = p.Servplanno),0),"
		      +" value((select x.MoneySum from LHContServPrice x where  x.contrano = p.ContraNo and x.contraitemno = p.Servplanno),0),"
		      +" (select case  when  a.TaskExecState='1' then '未执行' when a.TaskExecState='2' then '已执行'  else '' end from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo ),"
		      +" (select a.TaskExecState from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo),p.TaskExecNo,"
		      +" (select   x.ExpType from LHContServPrice x where  x.contrano = p.contrano and x.contraitemno = p.Servplanno),'',''"
		      +" from LHTaskCustomerRela d,LHExecFeeBalance p where d.servtaskno in ("+"<%=sqlServTaskNo%>"+") "
		      +" and p.TaskExecNo = (select A.TaskExecNo from LHTASKCUSTOMERRELA A where A.SERVTASKCODE = '01601' and A.SERVITEMNO = D.SERVITEMNO) "
		      +" order by d.servitemno with ur"
		      ;   
	turnPage.pageLineNum = 15;  
	turnPage.queryModal(strsql,LHFeeChargeGrid)
}
function displayConetent()
{
	strsql=" select d.CustomerNo,(select a.Name from LDPerson a where a.CustomerNo=d.CustomerNo),"
		  +" d.ServCaseCode,(select b.ServCaseName from LHServCaseDef b where b.ServCaseCode=d.ServCaseCode),"
		  +" (select c.ServTaskName from LHServTaskDef c where c.ServTaskCode=d.ServTaskCode),d.ServTaskCode,"
		  +" (select h.ServItemName from LHHealthServItem h ,LHServItem a where h.ServItemCode=a.ServItemCode and a.ServItemNo=p.ServItemNo),"
		  +" (select a.ServItemType from LHServItem a where a.ServItemNo=p.ServItemNo),d.ContNo,"
		  +" '',(select e.ContraName from LHGroupCont e where e.ContraNo = (select x.ContraNo as hospitcode from LHExecFeeBalance x where x.TaskExecNo = p.TaskExecNo ) ),"
		  +" '',(select b.DutyItemName from LDContraItemDuty b where b.DutyItemCode=(select x.ContraItemNo as hospitcode from LHExecFeeBalance x where x.TaskExecNo = p.TaskExecNo ) ),"
		  +" p.ContraNo,p.hospitcode,d.TaskExecNo,d.ServItemNo,d.ServTaskNo,d.feenormal,d.feepay,"
		  +" (select case  when  a.TaskExecState='1' then '未执行' when a.TaskExecState='2' then '已执行'  else '' end from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo ),"
		  +" (select a.TaskExecState from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo),p.TaskExecNo,"
		  +" d.FeeType,d.makedate,d.maketime from LHFeeCharge d,LHExecFeeBalance p where d.servtaskno in ("+"<%=sqlServTaskNo%>"+")"
		  +" and p.TaskExecNo = (select A.TaskExecNo from LHTASKCUSTOMERRELA A where A.SERVTASKCODE = '01601' and A.SERVITEMNO = D.SERVITEMNO) order by d.servitemno with ur"
		  ;
	turnPage.pageLineNum = 15;  
	turnPage.queryModal(strsql,LHFeeChargeGrid)
}
function initLHFeeChargeGrid() 
{
	var iArray = new Array();

	try
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         		//列名
    	iArray[0][1]="30px";         		//列名
    	iArray[0][3]=0;
    	 
    	iArray[1]=new Array();
    	iArray[1][0]="客户号";         		//列名
    	iArray[1][1]="55px";         		//列名
    	iArray[1][2]=20;        
    	iArray[1][3]=0;         		//列名
    	
    	iArray[2]=new Array(); 
		iArray[2][0]="客户姓名";   
		iArray[2][1]="50px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
			
		iArray[3]=new Array(); 
		iArray[3][0]="服务事件编号";   
		iArray[3][1]="0px";   
		iArray[3][2]=20;        
		iArray[3][3]=3;
			
		iArray[4]=new Array(); 
		iArray[4][0]="服务事件名称";   
		iArray[4][1]="120px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
			
		iArray[5]=new Array(); 
		iArray[5][0]="服务任务名称";   
		iArray[5][1]="0px";   
		iArray[5][2]=20;        
		iArray[5][3]=3;		
			
		iArray[6]=new Array(); 
		iArray[6][0]="ServTaskCode";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3;
		
		iArray[7]=new Array(); 
		iArray[7][0]="服务项目名称";   
		iArray[7][1]="90px";   
		iArray[7][2]=20;        
		iArray[7][3]=0;
		
		iArray[8]=new Array(); 
		iArray[8][0]="序号";   
		iArray[8][1]="0px";   
		iArray[8][2]=20;        
		iArray[8][3]=3;
		
		iArray[9]=new Array(); 
		iArray[9][0]="保单号";   
		iArray[9][1]="65px";   
		iArray[9][2]=20;        
		iArray[9][3]=0;
		
		iArray[10]=new Array(); 
		iArray[10][0]="费用结算机构";   
		iArray[10][1]="0px";   
		iArray[10][2]=20;        
		iArray[10][3]=3;
		
		iArray[11]=new Array(); 
		iArray[11][0]="合同名称";   
		iArray[11][1]="100px";   
		iArray[11][2]=20;        
		iArray[11][3]=0;
		
		iArray[12]=new Array(); 
		iArray[12][0]="合同责任代码";   
		iArray[12][1]="0px";   
		iArray[12][2]=20;        
		iArray[12][3]=3;
		
		
		iArray[13]=new Array(); 
		iArray[13][0]="合同责任名称";   
		iArray[13][1]="90px";   
		iArray[13][2]=20;        
		iArray[13][3]=0;
		
		iArray[14]=new Array(); 
		iArray[14][0]="ContraNo";   
		iArray[14][1]="0px";   
		iArray[14][2]=20;        
		iArray[14][3]=3;
		
		iArray[15]=new Array(); 
		iArray[15][0]="HospitCode";   
		iArray[15][1]="0px";   
		iArray[15][2]=20;        
		iArray[15][3]=3;
		
		iArray[16]=new Array(); 
		iArray[16][0]="TaskExecNo";   
		iArray[16][1]="0px";   
		iArray[16][2]=20;        
		iArray[16][3]=3;
		
		iArray[17]=new Array(); 
		iArray[17][0]="ServItemNo";   
		iArray[17][1]="0px";   
		iArray[17][2]=20;        
		iArray[17][3]=3;
			  
		iArray[18]=new Array(); 
		iArray[18][0]="ServTaskNo";   
		iArray[18][1]="0px";   
		iArray[18][2]=20;        
		iArray[18][3]=3;
		  
		iArray[19]=new Array(); 
		iArray[19][0]="合同费用";   
		iArray[19][1]="40px";   
		iArray[19][2]=20;        
		iArray[19][3]=0;  
		
		iArray[20]=new Array(); 
		iArray[20][0]="应付费用";   
		iArray[20][1]="40px";   
		iArray[20][2]=20;        
		iArray[20][3]=0; 
		
		iArray[21]=new Array(); 
		iArray[21][0]="执行状态";   
		iArray[21][1]="40px";   
		iArray[21][2]=20;        
		iArray[21][3]=2;
		iArray[21][4]="taskexecstuas";
		iArray[21][5]="21|22";     //引用代码对应第几列，'|'为分割符
	    iArray[21][6]="1|0";     //上面的列中放置引用代码中第几位
	    iArray[21][14]="未执行"; 
	    iArray[21][17]="1"; 
	    iArray[21][18]="120";
	    iArray[21][19]="1" ;
    
    	iArray[22]=new Array(); 
	    iArray[22][0]="TaskExecState";   
	    iArray[22][1]="0px";   
	    iArray[22][2]=20;        
	    iArray[22][3]=3;
	    
	    iArray[23]=new Array(); 
	    iArray[23][0]="OtTaskExecNo";   
	    iArray[23][1]="0px";   
	    iArray[23][2]=20;        
	    iArray[23][3]=3;

	    iArray[24]=new Array(); 
	    iArray[24][0]="ExpType";   
	    iArray[24][1]="0px";   
	    iArray[24][2]=20;        
	    iArray[24][3]=3;
	    
	    iArray[25]=new Array(); 
	    iArray[25][0]="MakeDate";   
	    iArray[25][1]="0px";   
	    iArray[25][2]=20;        
	    iArray[25][3]=3;
	    
	    iArray[26]=new Array(); 
	    iArray[26][0]="MakeTime";   
	    iArray[26][1]="0px";   
	    iArray[26][2]=20;        
	    iArray[26][3]=3;
	 	  
    	LHFeeChargeGrid = new MulLineEnter( "fm" , "LHFeeChargeGrid" ); 
    	//这些属性必须在loadMulLine前
    	
    	LHFeeChargeGrid.mulLineCount = 0;   
    	LHFeeChargeGrid.displayTitle = 1;
    	LHFeeChargeGrid.hiddenPlus = 1;
    	LHFeeChargeGrid.hiddenSubtraction = 1;
    	LHFeeChargeGrid.canSel = 0;
    	LHFeeChargeGrid.canChk = 1;
    	//LHFeeChargeGrid.selBoxEventFuncName = "showOne";              
    	LHFeeChargeGrid.chkBoxEventFuncName = "showOne";  
    	LHFeeChargeGrid.loadMulLine(iArray);  
    	//这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{	
  		alert(ex);
	}
} 

function initCustomTestInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
{
	var iArray = new Array(); //数组放置各个列
	try
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";  					//列名（序号列，第1列）
    	iArray[0][1]="30px";  					//列宽
    	iArray[0][2]=10;      					//列最大值
    	iArray[0][3]=0;   							//1表示允许该列输入，0表示只读且不响应Tab键
    	               									// 2 表示为容许输入且颜色加深.             
    	iArray[1]=new Array();
    	iArray[1][0]="检查项目名称";  	//列名（第2列）
    	iArray[1][1]="200px";  	  			//列宽
    	iArray[1][2]=10;        				//列最大值
    	iArray[1][3]=0;          				//是否允许输入,1表示允许，0表示不允许
    	                   
    	iArray[2]=new Array();                             
    	iArray[2][0]="检查项目价格";        //列名（第2列）          
    	iArray[2][1]="120px";  	        //列宽                   
    	iArray[2][2]=10;        	      //列最大值
    	iArray[2][3]=0;
    	                                          
    	
    	//生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
    	CustomTestInfoGrid= new MulLineEnter( "fm" , "CustomTestInfoGrid" ); 
    	//设置属性区 (需要其它特性，在此设置其它属性)
		CustomTestInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
		CustomTestInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0 隐藏标题       
		//CustomTestInfoGrid.canSel =1; 
           
		//对象初始化区：调用对象初始化方法，属性必须在此前设置
		CustomTestInfoGrid.loadMulLine(iArray); 
	}
	catch(ex)
	{ alert(ex); }
}

</script>