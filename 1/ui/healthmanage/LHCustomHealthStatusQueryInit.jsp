<%
//程序名称：LHCustomHealthStatusQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-18 10:11:20
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CustomerNo').value = "";
    fm.all('HealthNo').value = "";
    fm.all('AddDate').value = "";
    fm.all('Stature').value = "";
    fm.all('Avoirdupois').value = "";
    fm.all('AvoirdIndex').value = "";
    fm.all('BloodPressHigh').value = "";
    fm.all('BloodPressLow').value = "";
    fm.all('Smoke').value = "";
    fm.all('KissCup').value = "";
    fm.all('SitUp').value = "";
    fm.all('DiningNoRule').value = "";
    fm.all('BadHobby').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LHCustomHealthStatusQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHCustomHealthStatusGrid();  
  }
  catch(re) {
    alert("LHCustomHealthStatusQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHCustomHealthStatusGrid;
function initLHCustomHealthStatusGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="客户号码";   
	iArray[1][1]="60px";   
	iArray[1][2]=20;        
	iArray[1][3]=0;
	
	iArray[2]=new Array(); 
	iArray[2][0]="客户姓名";   
	iArray[2][1]="60px";   
	iArray[2][2]=20;        
	iArray[2][3]=0;
	
	iArray[3]=new Array(); 
	iArray[3][0]="客户序号";   
	iArray[3][1]="40px";   
	iArray[3][2]=20;        
	iArray[3][3]=0;
	
	iArray[4]=new Array(); 
	iArray[4][0]="增加日期";   
	iArray[4][1]="40px";   
	iArray[4][2]=20;        
	iArray[4][3]=0;
	
	iArray[5]=new Array(); 
	iArray[5][0]="身高";   
	iArray[5][1]="40px";   
	iArray[5][2]=20;        
	iArray[5][3]=0;
	
	iArray[6]=new Array(); 
	iArray[6][0]="体重";   
	iArray[6][1]="40px";   
	iArray[6][2]=20;        
	iArray[6][3]=0;
	
	iArray[7]=new Array(); 
	iArray[7][0]="体重指数";   
	iArray[7][1]="40px";   
	iArray[7][2]=20;        
	iArray[7][3]=0;
    
    LHCustomHealthStatusGrid = new MulLineEnter( "fm" , "LHCustomHealthStatusGrid" ); 
    //这些属性必须在loadMulLine前

    LHCustomHealthStatusGrid.mulLineCount = 0;   
    LHCustomHealthStatusGrid.displayTitle = 1;
    LHCustomHealthStatusGrid.hiddenPlus = 1;
    LHCustomHealthStatusGrid.hiddenSubtraction = 1;
    LHCustomHealthStatusGrid.canSel = 1;
    LHCustomHealthStatusGrid.canChk = 0;
   // LHCustomHealthStatusGrid.selBoxEventFuncName = "showOne";

    LHCustomHealthStatusGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomHealthStatusGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
