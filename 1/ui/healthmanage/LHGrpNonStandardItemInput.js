//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	//SetServEventStyle();
	if (ServCaseTypeIsNull()==false)
	   return false;
	else
	{
		  var i = 0;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		  fm.all('fmtransact').value = "INSERT||MAIN"; 
		  fm.submit(); //提交
  }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	  showInfo.close();
	  if (FlagStr == "Fail" )
	  {             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  }
	  else
	  { 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	    fm.all('saveButton').disabled = true;
	  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHGrpServItem.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if (ServCaseTypeIsNull()==false)
  return false;
	else
	{
			//下面增加相应的代码
			if (confirm("您确实想修改该记录吗?"))
			{
			   var i = 0;
			   var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			   fm.all('fmtransact').value = "UPDATE||MAIN";
			   fm.submit(); //提交
			}
			else
			{
			  //mOperate="";
			  alert("您取消了修改操作！");
			}
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHGrpServItemQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
      var i = 0;
      var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      
      //showSubmitFrame(mDebug);
      fm.all('fmtransact').value = "DELETE||MAIN";
      fm.submit(); //提交
      initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	//alert(arrResult);
	if( arrQueryResult != null )
	{
		    arrResult = arrQueryResult;
        fm.all('GrpServPlanNo').value= arrResult[0][8];
        fm.all('GrpCustomerNo').value= easyExecSql("select GrpCustomerNo from LHGrpServPlan where GrpServPlanNo = '"+fm.all('GrpServPlanNo').value+"' ");
        fm.all('GrpName').value= easyExecSql("select GrpName from LHGrpServPlan where GrpServPlanNo = '"+fm.all('GrpServPlanNo').value+"' "); 
        fm.all('GrpContNo').value= easyExecSql("select GrpContNo from LHGrpServPlan where GrpServPlanNo = '"+fm.all('GrpServPlanNo').value+"' "); 
        fm.all('CustomerClass').value= easyExecSql("select ServPlanLevel from LHGrpServPlan where GrpServPlanNo = '"+fm.all('GrpServPlanNo').value+"' "); 

        var strSql2 = " select ( select distinct b.servitemname from LHHealthServItem b where b.ServItemCode = a.ServItemCode),a.ServItemSerial,a.OrgType,a.AttribFlag,a.ComID,(select distinct c.servpricename from LHServerItemPrice c where c.ServPriceCode=a.ServPriceCode),a.MakeDate,a.MakeTime,a.ServItemCode,a.GrpServItemNo,a.ServPriceCode"
      							+" from LHGrpServItem a"
				  					+" where a.GrpServPlanNo = '"+fm.all('GrpServPlanNo').value+"'"
      							; 										
      							// alert(strSql2);
      	turnPage.queryModal(strSql2,LHGrpItemGrid); 
        var Servtype = LHGrpItemGrid.getRowColData(0,3);
        if (Servtype == 1)
              LHGrpItemGrid.setRowColData(0,12,"个人服务事件");
        if (Servtype == 2)
        	    LHGrpItemGrid.setRowColData(0,12,"集体服务事件");
        if (Servtype == 3)
              LHGrpItemGrid.setRowColData(0,12,"费用结算事件");
	 }
}               
        
function SetServEventStyle()
{
	var RowCount = LHGrpItemGrid.mulLineCount;
	//	alert(RowCount);
	var i;
	for( i = 0; i < RowCount; i++)
	{
		var manCom = LHGrpItemGrid.getRowColData(i,5);
		var ServCode = LHGrpItemGrid.getRowColData(i,9);
		var strSql = "select ServCaseType from LHServCaseTypeDef where ContType='2' and ComID = '"+manCom+"' and ServItemCode = '"+ServCode+"'";
		var a = easyExecSql(strSql);
		if (a == 1)
		{
		   LHGrpItemGrid.setRowColData(i,3,"1");
		   LHGrpItemGrid.setRowColData(i,12,"个人服务事件");
	  }
		if (a == 2)
	  {
			LHGrpItemGrid.setRowColData(i,3,"2");	
		  LHGrpItemGrid.setRowColData(i,12,"集体服务事件");	
		}
	  if (a == 3)
	  {
			LHGrpItemGrid.setRowColData(i,3,"3");	
		  LHGrpItemGrid.setRowColData(i,12,"费用结算事件");	
		}
	}
}
function ServCaseTypeIsNull()
{
	var RowCount = LHGrpItemGrid.mulLineCount;
	//	alert(RowCount);
	var i;
	for( i = 0; i < RowCount; i++)
	{
		 var ServCaseType = LHGrpItemGrid.getRowColData(i,5);
		 if (ServCaseType == "" || ServCaseType == null || ServCaseType == "null")
		 {
		    alert("服务事件类型不能为空!");
		     return false;
	   }
	   else
		 return true;
	}
}