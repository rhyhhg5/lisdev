<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHMessSendMgInput.jsp
//程序功能：健管通讯递送管理
//创建日期：2006-09-07 15:50:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="LHMessSendMgInput.js"></SCRIPT>
  
  <%@include file="LHMessSendMgInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm >
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHMessSendMgGrid);">
    		</td>
    		<td class= titleImg>
        	服务事件任务信息
       	</td>
    	</tr>
    </table>
	<Div  id= "divLHMessSendMgGrid" style= "display: ''">
	 		<Div id="LHCaseInfo" style="display:'' ">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHMessSendMgGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>
	</Div>
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	<table>
	    	<tr>
		    		<td>
		    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServBespeakManageInfoGrid);">
		    		</td>
	    		  <td class= titleImg>
	        		 服务通讯递送信息
	       	  </td>   		 
	    	</tr>
    </table>
   <Div  id= "divLHServBespeakManageGrid" style= "display: ''">
		<table  class= common align='center' >
		  <TR  class= common>   
			   <TD  class= title>
            健康通讯代码
         </TD>
         <TD  class= input>
           <Input class= 'code' name=HmMessCode  ondblclick="showCodeList('lhmessagecode',[this,HmMessName],[0,1],null,fm.HmMessCode.value,'HmMessCode','1',300);"  codeClear(HmMessName,HmMessCode);">
         </TD>
         <TD  class= title>
            健康通讯名称
         </TD>
             <TD  class= input>
           <Input class= 'code' name=HmMessName   ondblclick="showCodeList('lhmessagecode',[this,HmMessCode],[1,0],null,fm.HmMessName.value,'HmMessName','1',300);"  codeClear(HmMessName,HmMessCode);">
         </TD>
				 <TD  class= title>
        		服务执行状态
       	</TD>
        <TD  class= input>
        		<Input class=codeno name=ExecState value="2" CodeData= "0|^未执行|1^已执行|2" ondblClick= "showCodeListEx('hmexecstate',[this,ExecState_ch],[1,0],null,null,null,1,100);"><Input class= 'codename' name=ExecState_ch  value="已执行"  style="width:110px">
        </TD>
		  </TR>
			<TR  class= common>
			<TD  class= title>
				  备注说明
			</TD>
		    <TD  class= input colspan='6'>
		      <textarea class= 'common' rows="3" cols="100"  style="width:796px;" name=ReMarkExp verify="备注说明|len<=500" ></textarea>
		    </TD>
		  </TR>
		</table>	
</div>

		<table  class= common border=0 width=100%>
			<TR  class= common>
		      	</td>
					<TD  class=common>
						<INPUT VALUE="添加通讯信息"   TYPE=button name=save   class=cssbutton onclick="submitForm();">
					</td>
					<TD  class=common>
						<INPUT VALUE="修改通讯信息"   TYPE=button name=update  class=cssbutton onclick="updateClick();">
					</td>
					<TD  class=common>
						<INPUT VALUE="邮寄列表打印"   TYPE=button name=print  class=cssbutton onclick="printClick();">
					</td>
					<TD  class=common>
						<INPUT VALUE="导出全部手机号码"   TYPE=button name=gMobile  class=cssbutton style="width:120px" onclick="getMobile();">
					</td>
					<TD  class=common>
						<INPUT VALUE="导出投保人手机号码"   TYPE=button name=gMobile  class=cssbutton style="width:120px" onclick="getAppMobile();">
					</td>
				 <TD class=title></TD>
				 <TD  class= input style='width:580'></TD>
			</tr>
		</table>
	</Div>
	 <input type=hidden id=queryString name=queryString>
	 <input type=hidden   id="fmtransact" name="fmtransact">
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="TaskExecNo" name="TaskExecNo">
	 <input type=hidden id="ServTaskNo" name="ServTaskNo" VALUE="<%=request.getParameter("ServTaskNo")%>">
	 <input type=hidden id="ServCaseCode" name="ServCaseCode">
	 <input type=hidden id="ServTaskCode" name="ServTaskCode">
	 <input type=hidden id="CustomerNo" name="CustomerNo">
	 <input type=hidden id="ServItemNo" name="ServItemNo">
	 <input type=hidden id="manageComPrt" name="manageComPrt">
	 <input type=hidden id="TPageTaskNo" name="TPageTaskNo">
	 <input type=hidden id="flag" name="flag">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
