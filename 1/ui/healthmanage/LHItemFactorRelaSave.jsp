<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHItemFactorRelaSave.jsp
//程序功能：服务项目要素设定表保存页面 
//创建日期：2006-06-20 13:54:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHItemFactorRelaUI tLHItemFactorRelaUI   = new LHItemFactorRelaUI();
  
   LHItemFactorRelaSet tLHItemFactorRelaSet = new  LHItemFactorRelaSet();		//服务项目要素设定表
  
		
    String tPriceFactorCode[] = request.getParameterValues("LHItemPriceFactorGrid1");					//MulLine的列存储数组
		String tPriceFactorCon[] = request.getParameterValues("LHItemPriceFactorGrid3");					//MulLine的列存储数组
			
		
								
  
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact2");
    System.out.println("Qddddddddddddd "+transact);
    int LHItemFactorRelaCount = 0;

		if(tPriceFactorCode != null)
		{	
			LHItemFactorRelaCount = tPriceFactorCode.length;
		}	
		System.out.println(" LHItemFactorRelaCount is : "+LHItemFactorRelaCount);
		
		for(int i = 0; i < LHItemFactorRelaCount; i++)
		{
			
				LHItemFactorRelaSchema tLHItemFactorRelaSchema = new LHItemFactorRelaSchema();
			
          tLHItemFactorRelaSchema.setComID(request.getParameter("ComID2"));
          tLHItemFactorRelaSchema.setServItemNo(request.getParameter("ServItemNo2"));
              System.out.println("QQQQQQQQQQ  VVvvvvv "+tLHItemFactorRelaSchema.getServItemNo());
          tLHItemFactorRelaSchema.setServItemCode(request.getParameter("ServItemCode2"));
          
				  tLHItemFactorRelaSchema.setPriceFactorCode(tPriceFactorCode[i]);	
				  tLHItemFactorRelaSchema.setPriceFactorCon(tPriceFactorCon[i]);	

				 
        
        tLHItemFactorRelaSchema.setOperator(request.getParameter("ManageCom2"));                     
				tLHItemFactorRelaSchema.setOperator(request.getParameter("Operator2"));
				tLHItemFactorRelaSchema.setMakeDate(request.getParameter("MakeDate2"));
				tLHItemFactorRelaSchema.setMakeTime(request.getParameter("MakeTime2"));
				
                              
			                        
				tLHItemFactorRelaSet.add(tLHItemFactorRelaSchema);
			                        
		}
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHItemFactorRelaSet);
  	tVData.add(tG);
    
    tLHItemFactorRelaUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHItemFactorRelaUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>