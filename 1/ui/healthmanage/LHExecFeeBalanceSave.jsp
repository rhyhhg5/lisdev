<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHExecFeeBalanceSave.jsp
//程序功能：
//创建日期：2006-09-21 15:08:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
    //接收信息，并作校验处理。
    //输入参数
    TransferData mTransferData = new TransferData();
    LHExecFeeBalanceUI tLHExecFeeBalanceUI   = new LHExecFeeBalanceUI();
    LHExecFeeBalanceSet tLHExecFeeBalanceSet = new LHExecFeeBalanceSet();				//服务预约管理表
    LHTaskCustomerRelaSet tLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();			//服务预约管理表

    String tTaskExecNo[] = request.getParameterValues("LHExecFeeBalanceGrid16");		//任务实施号码流水号
    String tServTaskNo[] = request.getParameterValues("LHExecFeeBalanceGrid18");		//服务任务编号
    String tServCaseCode[] = request.getParameterValues("LHExecFeeBalanceGrid3");		//服务事件号  
    String tServTaskCode[] = request.getParameterValues("LHExecFeeBalanceGrid6");		//服务任务代码
    String tServItemNo[] = request.getParameterValues("LHExecFeeBalanceGrid17");		//服务项目序号
    String tContNo[] = request.getParameterValues("LHExecFeeBalanceGrid9");				//客户号  
    String tCustomerNo[] = request.getParameterValues("LHExecFeeBalanceGrid1");			//客户号  
    String tTaskExecState[] = request.getParameterValues("LHExecFeeBalanceGrid20");		//服务执行状态		 
    String tMakeDate[] = request.getParameterValues("LHExecFeeBalanceGrid21");					//客户号  	 
    String tMakeTime[] = request.getParameterValues("LHExecFeeBalanceGrid22");	
     
    String tChk[] = request.getParameterValues("InpLHExecFeeBalanceGridChk"); //参数格式=” Inp+MulLine对象名+Chk”     	 
	mTransferData.setNameAndValue("ServTaskNo",request.getParameter("ServTaskNo"));     
	mTransferData.setNameAndValue("HospitCode",request.getParameter("HospitCode"));     
	mTransferData.setNameAndValue("ContraNo",request.getParameter("ContraNo"));     
	mTransferData.setNameAndValue("DutyItemCode",request.getParameter("DutyItemCode"));     
	mTransferData.setNameAndValue("Contraitemno",request.getParameter("Contraitemno"));   
	mTransferData.setNameAndValue("ExecState",request.getParameter("ExecState"));  
	
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");

	int LHExecFeeBalanceCount = 0;
	if(tTaskExecNo != null)
	{	
		LHExecFeeBalanceCount = tTaskExecNo.length;
	}	

    for(int j = 0; j < LHExecFeeBalanceCount; j++)
	{
		LHExecFeeBalanceSchema tLHExecFeeBalanceSchema = new LHExecFeeBalanceSchema();

		tLHExecFeeBalanceSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		tLHExecFeeBalanceSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		tLHExecFeeBalanceSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		tLHExecFeeBalanceSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		tLHExecFeeBalanceSchema.setServItemNo(tServItemNo[j]);	//项目号码
		tLHExecFeeBalanceSchema.setCustomerNo(tCustomerNo[j]);	//客户号
		tLHExecFeeBalanceSchema.setContNo(tContNo[j]);	//保单号 
		tLHExecFeeBalanceSchema.setHospitCode(request.getParameter("HospitCode"));//机构代码
		tLHExecFeeBalanceSchema.setContraNo(request.getParameter("ContraNo"));  //合同编号
		tLHExecFeeBalanceSchema.setContraItemNo(request.getParameter("DutyItemCode"));  //合同责任号码
		tLHExecFeeBalanceSchema.setServPlanNo(request.getParameter("Contraitemno"));  //将责任流水号存到空余字段计划号中
		tLHExecFeeBalanceSchema.setMakeDate(tMakeDate[j]);	 
		tLHExecFeeBalanceSchema.setMakeTime(tMakeTime[j]);
		LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema();
		tLHTaskCustomerRelaSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		tLHTaskCustomerRelaSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		tLHTaskCustomerRelaSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		tLHTaskCustomerRelaSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		tLHTaskCustomerRelaSchema.setServItemNo(tServItemNo[j]);	//项目号码
		tLHTaskCustomerRelaSchema.setTaskExecState(tTaskExecState[j]);//服务执行状态
  
        if(transact.equals("INSERT||MAIN"))
        {
        	tLHExecFeeBalanceSet.add(tLHExecFeeBalanceSchema); 
            tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);            
        }  
        if(transact.equals("UPDATE||MAIN"))
        {
            if(tChk[j].equals("1"))  
            {
               System.out.println("该行被选中 "+tTaskExecNo[j]);     
               tLHExecFeeBalanceSet.add(tLHExecFeeBalanceSchema); 
               tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);              
			}
        }   
	}
		                      
	try
	{
		// 准备传输数据 VData
  		VData tVData = new VData();
  		tVData.add(tG);
  		tVData.add(tLHExecFeeBalanceSet);
  		tVData.add(tLHTaskCustomerRelaSet);
  		tVData.addElement(mTransferData);
    	tLHExecFeeBalanceUI.submitData(tVData,transact);
	} 
	catch(Exception ex)
	{ 
    	Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tLHExecFeeBalanceUI.mErrors;
	if (!tError.needDealError())
	{                          
		Content = " 操作成功! ";
		FlagStr = "Success";
	}
	else                                                                           
	{
		Content = " 操作失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
  
//添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	var transact = "<%=transact%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>