<%
//程序名称：LHGrpNonStandardItemInit.jsp
//程序功能：
//创建日期：2006-12-14 17:44:06
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    //fm.all('ServPlanLevel').value = "";
  }
  catch(ex)
  {
    alert("在LHGrpNonStandardItemInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {}
  catch(ex)
  {
    alert("在LHGrpNonStandardItemInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initLHGrpItemGrid();
  }
  catch(re)
  {
    alert("LHGrpNonStandardItemInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHGrpItemGrid;
function initLHGrpItemGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="40px";         		//列名
	    iArray[0][3]=0;         		//列名
	    iArray[0][4]="station";         		//列名
    
    
    	iArray[1]=new Array(); 
		  iArray[1][0]="服务项目代码";   
		  iArray[1][1]="100px";   
		  iArray[1][2]=20;        
		  iArray[1][3]=0;
		  
    	iArray[2]=new Array(); 
		  iArray[2][0]="服务项目名称";   
		  iArray[2][1]="220px";   
		  iArray[2][2]=20;        
		  iArray[2][3]=2;
		  iArray[2][4]="hmservitemcode";         //列名  
		  iArray[2][5]="2|1";    //引用MulLine的对应第几列，'|'为分割符 
		  iArray[2][6]="1|0";    //上面的列中放置下拉菜单中第几位值 
		  iArray[2][15]="ServItemName"; 
		  iArray[2][17]="1"; 
		  iArray[2][18]="310";     //下拉框的宽度 
		  iArray[2][19]="1" ;      //强制刷新数据源
	    
		  iArray[3]=new Array(); 
		  iArray[3][0]="服务项目序号";   
		  iArray[3][1]="80px";   
		  iArray[3][2]=20;        
		  iArray[3][3]=1;
      iArray[3][14]="1" ;
      
		  iArray[4]=new Array(); 
	    iArray[4][0]="服务事件类型";                             
	    iArray[4][1]="100px";                     
	    iArray[4][2]=20;                                  
	    iArray[4][3]=2;      
	    iArray[4][4]="eventtype";
	    iArray[4][5]="4|5";     //引用代码对应第几列，'|'为分割符
	    iArray[4][6]="1|0";     //上面的列中放置引用代码中第几位值
	    iArray[4][17]="1"; 
	    iArray[4][18]="160";
	    iArray[4][19]="1" ;
  
		  iArray[5]=new Array(); 
		  iArray[5][0]="servcasetype";   
		  iArray[5][1]="0px";   
		  iArray[5][2]=20;        
		  iArray[5][3]=3;
	    
	  
		  iArray[6]=new Array(); 
		  iArray[6][0]="服务机构标识";   
		  iArray[6][1]="120px";   
		  iArray[6][2]=20;        
		  iArray[6][3]=2;		
		  iArray[6][4]="hmhospitalmng";
		  iArray[6][5]="6";     //引用代码对应第几列，'|'为分割符
		  iArray[6][6]="1";     //上面的列中放置引用代码中第几位值
		  iArray[6][18]="230";

		  iArray[7]=new Array(); 
		  iArray[7][0]="入机日期";   
		  iArray[7][1]="0px";   
		  iArray[7][2]=20;        
		  iArray[7][3]=3;
		  
		  iArray[8]=new Array(); 
		  iArray[8][0]="入机时间";   
		  iArray[8][1]="0px";   
		  iArray[8][2]=20;        
		  iArray[8][3]=3;
		  
		  iArray[9]=new Array(); 
		  iArray[9][0]="GrpServItemNo";   
		  iArray[9][1]="0px";   
		  iArray[9][2]=20;        
		  iArray[9][3]=3;

    	LHGrpItemGrid = new MulLineEnter( "fm" , "LHGrpItemGrid" ); 
    	//这些属性必须在loadMulLine前
    	
    	LHGrpItemGrid.mulLineCount = 0;   
    	LHGrpItemGrid.displayTitle = 1;
    	LHGrpItemGrid.hiddenPlus = 0;
    	LHGrpItemGrid.hiddenSubtraction = 0;
    	LHGrpItemGrid.canSel = 0;
    	LHGrpItemGrid.canChk = 0;
    	//LHGrpItemGrid.selBoxEventFuncName = "showOne";

		  LHGrpItemGrid.loadMulLine(iArray);  
		  //这些操作必须在loadMulLine后面
		  //LHGrpItemGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}


</script>
