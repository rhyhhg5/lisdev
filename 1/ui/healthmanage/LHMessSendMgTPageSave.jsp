<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHMessSendMgTPageSave.jsp
//程序功能：
//创建日期：2006-11-01 13:53:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	  String TServTaskNo="";//从页面获取的传入的多个服务任务编号
    String TServInfoNo[][]; 
    String tCustomerNo[][];
	  LHMessSendMgUI tLHMessSendMgUI   = new LHMessSendMgUI(); 
    LHMessSendMgSet tLHMessSendMgSet = new LHMessSendMgSet();		//服务预约管理表
    TServTaskNo= request.getParameter("TPageTaskNo");	
    String tChk[] = request.getParameterValues("InpLHMessSendMgGridChk"); //参数格式=” Inp+MulLine对象名+Chk”     	 
     
    //输出参数
    CErrors tError = null;            
    String FlagStr = "";
    String Content = "";
    String transact = "";
    GlobalInput tG = new GlobalInput(); 
    tG=(GlobalInput)session.getValue("GI");
    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
	
    String[] tServTaskNo  = null;
  	tServTaskNo=TServTaskNo.split(",");
  	SSRS tSSRS_Result = new SSRS();
    ExeSQL tExeResultSQL = new ExeSQL();
    String sqlResult =" select TaskExecNo, ServCaseCode, ServTaskNo,ServItemNo from LHTaskCustomerRela "
                     + " where ServTaskNo in ( "+ TServTaskNo +" ) order by ServTaskNo";
    System.out.println("sqlResult "+sqlResult);
    tSSRS_Result = tExeResultSQL.execSQL(sqlResult);
    TServInfoNo = tSSRS_Result.getAllData();
    //System.out.println("AAAAAAAAAAAAAAAAAAAAA " +TServInfoNo.length);
    for(int j = 0; j < TServInfoNo.length; j++)
	  {
        ExeSQL tExeCustomerNo = new ExeSQL();
        String sqlCustomerNo =" select (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode='"+ TServInfoNo[j][1]+"' and a.ServItemNo='"+ TServInfoNo[j][3] +"')"
                             +" from LHTaskCustomerRela d "
                             +" where d.ServCaseCode='"+ TServInfoNo[j][1] +"'"
		                         +" and d.ServTaskNo='"+TServInfoNo[j][2] +"'"
		                         +" and d.TaskExecNo='"+ TServInfoNo[j][0] +"'"
		                         +" and d.ServItemNo='"+TServInfoNo[j][3] +"'"
		                         ;
        String CustomerNo = tExeCustomerNo.getOneValue(sqlCustomerNo);//最大日期下的客户相关流水号
        String sqlTaskCode =" select ServTaskCode from LHCaseTaskRela a  where a.ServTaskNo ='" + TServInfoNo[j][2]+"'";
        String ServTaskCode= new ExeSQL().getOneValue(sqlTaskCode);
        
		    LHMessSendMgSchema tLHMessSendMgSchema = new LHMessSendMgSchema();
		    tLHMessSendMgSchema.setCustomerNo(CustomerNo);
		    tLHMessSendMgSchema.setTaskExecNo(TServInfoNo[j][0]);
		    tLHMessSendMgSchema.setServCaseCode(TServInfoNo[j][1]);
		    tLHMessSendMgSchema.setServTaskNo(TServInfoNo[j][2]);
		    tLHMessSendMgSchema.setServItemNo(TServInfoNo[j][3]);
		    tLHMessSendMgSchema.setServTaskCode(ServTaskCode);
		    tLHMessSendMgSchema.setHmMessCode(request.getParameter("HmMessCode"));//通讯代码
		    tLHMessSendMgSchema.setReMarkExp(request.getParameter("ReMarkExp"));  //备注说明
		    
        if(transact.equals("INSERT||MAIN"))
        {
			         tLHMessSendMgSet.add(tLHMessSendMgSchema);     
        }  
        if(transact.equals("UPDATE||MAIN"))
        {
            if(tChk[j].equals("1"))  
            {           
               tLHMessSendMgSet.add(tLHMessSendMgSchema);         
            }
        }   
	}

	try
	{
		// 准备传输数据 VData
  		VData tVData = new VData();
  		tVData.add(tLHMessSendMgSet);
  		tVData.add(tG);
    	tLHMessSendMgUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
    	Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
    	tError = tLHMessSendMgUI.mErrors;
    	if (!tError.needDealError())
    	{                          
    		Content = " 操作成功! ";
    		FlagStr = "Success";
    	}
    	else                                                                           
    	{
    		Content = " 操作失败，原因是:" + tError.getFirstError();
    		FlagStr = "Fail";
    	}
 	 }
%>                      
<%=Content%>
<html>
<script language="javascript">
	var transact = "<%=transact%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>