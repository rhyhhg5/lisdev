<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHCaseLaunchSetSave.jsp
//程序功能：
//创建日期：2006-03-09 11:34:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	OLHCaseLaunchUI tOLHCaseLaunchUI = new OLHCaseLaunchUI();
	LHServTaskRelaSet tLHServTaskRelaSet = new LHServTaskRelaSet();

	String tServTaskCode[] = request.getParameterValues("LHCaseLaunchGrid1");
	String tServTaskState[] = request.getParameterValues("LHCaseLaunchGrid3");
	String tOperator[] = request.getParameterValues("LHCaseLaunchGrid4");
	String tPlanExeTime[] = request.getParameterValues("LHCaseLaunchGrid5");
	String tTaskDes[] = request.getParameterValues("LHCaseLaunchGrid6");
	String tMakeDate[] = request.getParameterValues("LHCaseLaunchGrid7");
	String tMakeTime[] = request.getParameterValues("LHCaseLaunchGrid8");
	String tSerialNo[] = request.getParameterValues("LHCaseLaunchGrid9");

//输出参数
    CErrors tError = null;
    String tRela  = "";                
    String FlagStr = "";
    String Content = "";
    String transact = "";
    GlobalInput tG = new GlobalInput(); 
    tG=(GlobalInput)session.getValue("GI");
  	
//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
	
	
	int LHCaseLaunchCount = 0;
	if(tServTaskCode != null)
	{
		LHCaseLaunchCount = tServTaskCode.length;
	}
	
	for(int i = 0; i < LHCaseLaunchCount; i++)
	{
		LHServTaskRelaSchema tLHServTaskRelaSchema = new LHServTaskRelaSchema();
		tLHServTaskRelaSchema.setServCaseType(request.getParameter("ServCaseType"));
		tLHServTaskRelaSchema.setComID(request.getParameter("ComID"));
		tLHServTaskRelaSchema.setServItemCode(request.getParameter("ServItemCode"));
		tLHServTaskRelaSchema.setServTaskCode(tServTaskCode[i]);
//		tLHServTaskRelaSchema.setServTaskName();
		tLHServTaskRelaSchema.setTaskState(tServTaskState[i]);
		tLHServTaskRelaSchema.setPlanExeTime(tPlanExeTime[i]);
		tLHServTaskRelaSchema.setTaskDes(tTaskDes[i]);
		tLHServTaskRelaSchema.setMakeDate(tMakeDate[i]);
		tLHServTaskRelaSchema.setMakeTime(tMakeTime[i]);
		tLHServTaskRelaSchema.setSerialNo(tSerialNo[i]);
		
		tLHServTaskRelaSet.add(tLHServTaskRelaSchema);
	}
	
	try
  	{
  		// 准备传输数据 VData
  		VData tVData = new VData();
		tVData.add(tLHServTaskRelaSet);
  		tVData.add(tG);
    	tOLHCaseLaunchUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
  
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
    	tError = tOLHCaseLaunchUI.mErrors;
    	if (!tError.needDealError())
    	{                          
    		Content = " 操作成功! ";
    		FlagStr = "Success";
    	}
    	else                                                                           
    	{
    		Content = " 操作失败，原因是:" + tError.getFirstError();
    		FlagStr = "Fail";
    	}
	}
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
