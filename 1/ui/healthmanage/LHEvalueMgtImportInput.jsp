<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHEvalueMgtImportInput.jsp
//程序功能：健管评估报告导入
//创建日期：2006-12-12 15:50:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="LHEvalueMgtImportInput.js"></SCRIPT>
  <%@include file="LHEvalueMgtImportInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHEvalueMgtImportGrid);">
    		</td>
    		<td class= titleImg>
        	服务事件任务信息
       	</td>
    	</tr>
    </table>
	<Div  id= "divLHEvalueMgtImportGrid" style= "display: ''">
	 		<Div id="LHCaseInfo" style="display:'' ">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHEvalueMgtImportGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>
	</Div>
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	<table>
	    	<tr>
		    		<td>
		    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServBespeakManageInfoGrid);">
		    		</td>
	    		  <td class= titleImg>
	        		 服务执行状态信息
	       	  </td>   		 
	    	</tr>
    </table>
<Div  id= "divLHEvalueMgtImportGrid" style= "display: ''">
	<table  class= common align='center' >
		  <tr class=common>
		  	<TD  class= title>
        			服务执行状态
       	</TD>
        <TD  class= input>
        		<Input class=codeno name=ExecState CodeData= "0|^未执行|1^已执行|2"  ondblClick= "showCodeListEx('hmexecstate',[this,ExecState_ch],[1,0],null,null,null,1,100);"><Input class= 'codename' name=ExecState_ch  style="width:110px">
        </TD>
        <TD  class= title>
        	<INPUT VALUE="修改执行状态"  name=updateSate style="width:115px"  TYPE=button   class=cssbutton onclick="updateStatus();">
       	</TD>
        <TD  class= input>
        </TD>
        <td style="width:400px;">
		  	</td>
     </tr>
   </table>
 </div>	
</div>	
		<table  class= common border=0 width=100%>
			<TR  class= common>
					<TD  class=common>
						<INPUT VALUE="全部报告关联"   TYPE=button name=save style="width:97px"  class=cssbutton onclick="submitForm();">
					</td>
					<TD  class=common>
						<INPUT VALUE="查看评估报告"   TYPE=button name=look style="width:97px" class=cssbutton onclick="LookReport();">
					</td>
					<TD  class=common>
						<INPUT VALUE="进行关联"   TYPE=button name=update style="width:97px"  class=cssbutton onclick="updateClick();">
					</td>
					<TD  class=common>
						<INPUT VALUE="删除关联关系"   TYPE=button name=delete style="width:97px" class=cssbutton onclick="deleteClick();">
					</td>
				 <TD class=title></TD>
				 <TD  class= input style='width:800'></TD>
			</tr>
		</table>
	</Div>
	 <input type=hidden id=queryString name=queryString>
	 <input type=hidden   id="fmtransact" name="fmtransact">
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="TaskExecNo" name="TaskExecNo">
	 <input type=hidden id="ServTaskNo" name="ServTaskNo">
	 <input type=hidden id="ServCaseCode" name="ServCaseCode">
	 <input type=hidden id="ServTaskCode" name="ServTaskCode">
	 <input type=hidden id="CustomerNo" name="CustomerNo">
	 <input type=hidden id="ServItemNo" name="ServItemNo">
	 <input type=hidden id="TPageTaskNo" name="TPageTaskNo">
	 <input type=hidden id="flag" name="flag">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
