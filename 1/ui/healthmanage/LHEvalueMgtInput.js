var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;


//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	 var arrSelected = new Array();
	 var ServCaseCode = new Array();
	 var rowNum=LHEvalueMgtGrid.mulLineCount ; //行数 	
	 for(var row=0; row < rowNum; row++)
	 {     
		  var tCustomerNo= LHEvalueMgtGrid.getRowColData(row,1);
		  var sqlCustomerNo="select h.CustomerNo from LHQuesImportMain h where h.CustomerNo='"+ tCustomerNo+"'";
		  var CustomerNo = easyExecSql(sqlCustomerNo);
    	
		  if(CustomerNo == "null" || CustomerNo==null || CustomerNo=="")
		  {
			   alert("客户号为"+tCustomerNo+"的问卷信息还未录入，不能对其导出数据!");
			   return false;
		  }
		  var sqlWhethAchieve="select h.WhethAchieve from LHQuesImportMain h where h.CustomerNo='"+ tCustomerNo+"'";
		  var WhethAchieve = easyExecSql(sqlWhethAchieve);
    	//alert(WhethAchieve);
		  if(WhethAchieve=="1")
		  {
			   alert("客户号为"+tCustomerNo+"的问卷信息还未提交，不能对其导出数据!");
			   return false;
		  }
	 }
   var i = 0;
   if( verifyInput2() == false ) return false;
   fm.fmtransact.value="INSERT||MAIN";
   var showStr="正在导出数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   fm.action="./LHEvalueMgtSave.jsp";
   fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCommTrans.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}                                          
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
*/
function updateClick()
{
	  var rowNum=LHEvalueMgtGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHEvalueMgtGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx] = row;
	        }
	  }
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要修改的信息记录!");
			  return false;
		}	
		if(aa.length>="1")
		{
    	if (confirm("您确实想修改该记录吗?"))
    	{
          var i = 0;
          var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
          fm.fmtransact.value = "UPDATE||MAIN";
          fm.action="./LHEvalueMgtStateSave.jsp";
          fm.submit(); //提交
          //initLHEvalueMgtGrid();
          
         
		  }
      else
      {
        alert("您取消了修改操作！");
      } 
   }
}


function afterCodeSelect(codeName,Field)
{
}
function DownEvalueInfo()
{
		var rowNum=LHEvalueMgtGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHEvalueMgtGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx] = row;
	        }
	  }
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要下载的客户信息记录!");
			  return false;
		}	
		if(aa.length>="1")
		{
			//alert(LHEvalueMgtGrid.getRowColData(0,13));
			var CusEvalCode=LHEvalueMgtGrid.getRowColData(0,13);//查询评估号是否存过
			var sql = " select CusEvalCode from LHEvalueExpMain where CusEvalCode = '"+CusEvalCode+"' ";
     	var re = easyExecSql(sql);
     	//alert(re);
     	if(re!=""&&re!=null&&re!=null)
     	{
	        var showStr="正在下载数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
          fm.all('fmtransact').value = "INSERT||MAIN";
          fm.action="./LHEvalueMgtDownSave.jsp";
          fm.submit(); //提交
      }
      else
      {
      		alert("请您先将客户的评估数据导出,然后再下载!");
      		return false;
      }
    }
}
