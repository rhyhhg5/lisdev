<%
//程序名称：LHHmBespeakManageInit.jsp
//程序功能：健管评估上传管理
//创建日期：2006-09-01 15:51:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
	
	  String ServTaskNo = request.getParameter("ServTaskNo");
	  String sqlServTaskNo = "'"+ServTaskNo.replaceAll(",","','")+"'";
    String flag = request.getParameter("flag");
%>       
           
<script language="JavaScript">     
var turnPage = new turnPageClass();    
var aResult=new Array;                              
function initForm()
{
	try
	{
	    initLHEvalueMgtGrid();
	    initLHCustomEvalueGrid();
		//取得传入任务流水号的所有任务执行信息
		var sqlExecNo  = " select TaskExecNo, ServCaseCode, ServTaskNo,ServItemNo from LHTaskCustomerRela "
						+" where ServTaskNo in ("+"<%=sqlServTaskNo%>"+") order by ServTaskNo";
		//alert(sqlExecNo);
		aResult = easyExecSql(sqlExecNo);
		  //alert(aResult[0][0]);
	    var flag= "<%=flag%>";
	    //alert(flag);
      if(flag=="1")
	    {
	    	 if(aResult!=""&&aResult!=null&&aResult!="null")
	    	 {
	    	     var    strsql="  select TaskExecNo from LHEvalueExpMain d where d.TaskExecNo='"+ aResult[0][0]+"'" ;
		         var result=new Array;
		         result=easyExecSql(strsql);
		         //alert(result);
		         if(result=="" || result==null || result=="null")
		         {//未保存过
	    	        displayInfo();
	    	        //fm.all('save').disabled=false;
	    	     }
	    	     if(result!=""&&result!="null"&&result!=null)
	    	     {
	    	     	  displayConetent();
	    	        //fm.all('save').disabled=true;
	    	     }
	    	 }
	    }
	}      
  catch(re)
  {      
    alert("LHHmBespeakManageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }      
}        
function displayInfo()
{
	for (var i=0;i < aResult.length;i++)
	{
		strsql=" select distinct (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode='"+ aResult[i][1] +"' and a.ServItemNo='"+ aResult[i][3] +"') ,"
		      +" (select a.CustomerName from LHServCaseRela a where a.ServCaseCode='"+ aResult[i][1] +"' and a.ServItemNo='"+ aResult[i][3] +"'),"
		      +" '',"
		      +" (case d.TaskExecState when '1' then '未执行' when '2' then '已执行' else '无' end ),"
		      +" '',"
		      +" '',"
		      +" d.TaskExecState,"
		      +" ServTaskNo,ServCaseCode,"
		      +" TaskExecNo,ServItemNo, ServTaskCode,'' "
		      +" from LHTaskCustomerRela d "
		      +" where d.ServCaseCode='"+ aResult[i][1] +"'"
		      +" and d.ServTaskNo='"+ aResult[i][2] +"'"
		      +" and d.TaskExecNo='"+ aResult[i][0] +"'"
		      +" and d.ServItemNo='"+ aResult[i][3] +"'"
		      ;
	
    //alert(strsql);
		turnPage.pageLineNum = 200;  
	    var re = easyExecSql(strsql);
	    if(re!=""&&re!=null&&re!="null")
	    {
			  for(var n=0; n<re.length; n++)
			  {
			  	LHEvalueMgtGrid.addOne();
			  	for(var m=0; m < 13; m++)
			  	{
			  		LHEvalueMgtGrid.setRowColData(LHEvalueMgtGrid.mulLineCount-1, m+1, re[n][m]);
			  	}
			  }  
		  }    
	}
}
function displayConetent()
{		

 	for (var i=0;i < aResult.length;i++)
	{
		strsql=" select d.CustomerNo ,"
		      +" (select a.Name from LDPerson a where a.CustomerNo=d.CustomerNo),"
          +" (select b.codename from ldcode b where b.codetype='lhevaluetype' and b.code=d.EvalType)," 
          +"(select case  when  a.TaskExecState='1' then '未执行' when a.TaskExecState='2' then '已执行'  else '' end from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo ),"
		      +" d.FailReason,"
		      +" d.EvalType,"
		      +"(select   a.TaskExecState  from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo ),"
		      +" d.ServTaskNo,d.ServCaseCode ,"
		      +" d.TaskExecNo ,d.ServItemNo,"
		      +" d.ServTaskCode ,d.CusEvalCode "
		      +"  from   LHEvalueExpMain d "
		      +" where d.ServCaseCode='"+ aResult[i][1] +"'"
		      +" and d.ServTaskNo='"+ aResult[i][2] +"'"
		      +" and d.TaskExecNo='"+ aResult[i][0] +"'"
		      +" and d.ServItemNo='"+ aResult[i][3] +"'"
		      ;
		      //alert(strsql);
		turnPage.pageLineNum = 200;  
	    var re = easyExecSql(strsql);
	    if(re!=""&&re!=null&&re!="null")
	    {
			  for(var n=0; n<re.length; n++)
			  {
			  	LHEvalueMgtGrid.addOne();
			  	for(var m=0; m < 13; m++)
			  	{
			  		LHEvalueMgtGrid.setRowColData(LHEvalueMgtGrid.mulLineCount-1, m+1, re[n][m]);
			  	}
			  }  
		  }    
	}
}
function initLHEvalueMgtGrid() 
{                               
  var iArray = new Array();
    
  try
   {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;
     
    iArray[1]=new Array();
    iArray[1][0]="客户号";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][2]=20;        
    iArray[1][3]=0;         		//列名
            		//列名
    
    iArray[2]=new Array(); 
		iArray[2][0]="客户姓名";   
		iArray[2][1]="100px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
	
		
		iArray[3]=new Array(); 
		iArray[3][0]="评估问卷类型";   
		iArray[3][1]="230px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
	
		
		iArray[4]=new Array(); 
		iArray[4][0]="服务执行状态";   
		iArray[4][1]="130px";   
		iArray[4][2]=20;        
		iArray[4][3]=2;
		iArray[4][4]="taskexecstuas";
	  iArray[4][5]="4|7";     //引用代码对应第几列，'|'为分割符
    iArray[4][6]="1|0";     //上面的列中放置引用代码中第几位
    iArray[4][14]="未执行"; 
    iArray[4][17]="1"; 
    iArray[4][18]="120";
    iArray[4][19]="1" ;
	   
	  iArray[5]=new Array(); 
		iArray[5][0]="失败原因";   
		iArray[5][1]="0px";   
		iArray[5][2]=20;        
		iArray[5][3]=3;
		
		iArray[6]=new Array();
		iArray[6][0]="EvalueType";
		iArray[6][1]="0px";         
		iArray[6][2]=20;            
		iArray[6][3]=3;       
		
		iArray[7]=new Array(); 
	  iArray[7][0]="TaskExecState";   
	  iArray[7][1]="0px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=3;
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="ServTaskNo";   
	  iArray[8][1]="0px";   
	  iArray[8][2]=20;        
	  iArray[8][3]=3;
	  
	  iArray[9]=new Array(); 
	  iArray[9][0]="ServCaseCode";   
	  iArray[9][1]="0px";   
	  iArray[9][2]=20;        
	  iArray[9][3]=3;
	  
	  iArray[10]=new Array(); 
	  iArray[10][0]="TaskExecNo";   
	  iArray[10][1]="0px";   
	  iArray[10][2]=20;        
	  iArray[10][3]=3;
	  
	  iArray[11]=new Array(); 
	  iArray[11][0]="ServItemNo";   
	  iArray[11][1]="0px";   
	  iArray[11][2]=20;        
	  iArray[11][3]=3;
	  
	  	  
	  iArray[12]=new Array(); 
	  iArray[12][0]="ServTaskCode";   
	  iArray[12][1]="0px";   
	  iArray[12][2]=20;        
	  iArray[12][3]=3;
	  
	  iArray[13]=new Array(); 
	  iArray[13][0]="CusEvalCode";   
	  iArray[13][1]="0px";   
	  iArray[13][2]=20;        
	  iArray[13][3]=3;
	  
    LHEvalueMgtGrid = new MulLineEnter( "fm" , "LHEvalueMgtGrid" ); 
    //这些属性必须在loadMulLine前

    LHEvalueMgtGrid.mulLineCount = 0;   
    LHEvalueMgtGrid.displayTitle = 1;
    LHEvalueMgtGrid.hiddenPlus = 1;
    LHEvalueMgtGrid.hiddenSubtraction = 1;
    LHEvalueMgtGrid.canSel = 0;
    LHEvalueMgtGrid.canChk = 1;
    //LHEvalueMgtGrid.selBoxEventFuncName = "showOne";              
    //LHEvalueMgtGrid.chkBoxEventFuncName = "showOne";  
    LHEvalueMgtGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHEvalueMgtGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
  	
    alert(ex);
  }
} 
function initLHCustomEvalueGrid() 
{                               
  var iArray = new Array();
    
  try
   {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;
     
    iArray[1]=new Array();
    iArray[1][0]="客户号";         		//列名
    iArray[1][1]="30px";         		//列名
    iArray[1][2]=20;        
    iArray[1][3]=0;         		//列名
            		//列名
    
    iArray[2]=new Array(); 
		iArray[2][0]="客户姓名";   
		iArray[2][1]="30px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
	
		
		iArray[3]=new Array(); 
		iArray[3][0]="评估问卷类型";   
		iArray[3][1]="53px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array();
		iArray[4][0]="EvalueType";
		iArray[4][1]="0px";         
		iArray[4][2]=20;            
		iArray[4][3]=3;       
		
		LHCustomEvalueGrid = new MulLineEnter( "fm" , "LHCustomEvalueGrid" ); 
    //这些属性必须在loadMulLine前

    LHCustomEvalueGrid.mulLineCount = 0;   
    LHCustomEvalueGrid.displayTitle = 1;
    LHCustomEvalueGrid.hiddenPlus = 1;
    LHCustomEvalueGrid.hiddenSubtraction = 1;
    LHCustomEvalueGrid.canSel = 1;
    LHCustomEvalueGrid.canChk = 0;              
    //LHCustomEvalueGrid.chkBoxEventFuncName = "showOne";  
    LHCustomEvalueGrid.loadMulLine(iArray);  
  }
  catch(ex) {
  	
    alert(ex);
  }
} 

</script>