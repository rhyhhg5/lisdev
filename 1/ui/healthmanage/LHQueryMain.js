//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var whichIsOut ;
var itemgroup = "Disease";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDDisease.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDiseaseQuery.html");
}           
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
 
function changeColor(InputName)
{
	if( InputName.bgColor == "#cbecfc")
	{		
		Disease.bgColor = "#cbecfc";
		Disease.style.borderBottom  = "#799AE1 1pt solid";
		Drug.bgColor = "#cbecfc";
		Drug.style.borderBottom  = "#799AE1 1pt solid";
		Operation.bgColor = "#cbecfc";
		Operation.style.borderBottom  = "#799AE1 1pt solid";
		Testitem.bgColor = "#cbecfc";
		Testitem.style.borderBottom  = "#799AE1 1pt solid";
		Hospital.bgColor = "#cbecfc";
		Hospital.style.borderBottom  = "#799AE1 1pt solid";

		InputName.style.borderBottom  = "0pt";
		InputName.bgColor = "#eaeaf0";

		itemgroup = InputName.name;
//		alert(itemgroup);
	}
}

function getParamSubmit()
{
	var keyword = fm.all('keyWord').value;
	var itemname = fm.all('itemName').value;
//	if(keyword == "FaLsEiTiS")
//	{return false;}
	
   	if(keyword.length > 800){alert("条件过长!");return false;}
	window.open("./LHQueryDiseaseListMain.jsp?itemname="+itemname+"&keyword="+keyword+"&itemgroup="+itemgroup,"resultlist"); 
}

function changeColorDiv(InputName)
{
	if( InputName.bgColor == "#cbecfc")
	{		
		Disease.bgColor = "#cbecfc";
		Disease.style.borderBottom  = "#799AE1 1pt solid";
		Drug.bgColor = "#cbecfc";
		Drug.style.borderBottom  = "#799AE1 1pt solid";
		Operation.bgColor = "#cbecfc";
		Operation.style.borderBottom  = "#799AE1 1pt solid";
		Testitem.bgColor = "#cbecfc";
		Testitem.style.borderBottom  = "#799AE1 1pt solid";
		Hospital.bgColor = "#cbecfc";
		Hospital.style.borderBottom  = "#799AE1 1pt solid";

		InputName.style.borderBottom  = "0pt";
		InputName.bgColor = "#eaeaf0";

		itemgroup = InputName.name;
	}
	var currentDiv = eval(InputName.id+"Div");
	
	DiseaseDiv.style.display='none';
	DrugDiv.style.display='none';
	OperationDiv.style.display='none';
	TestitemDiv.style.display='none';
	HospitalDiv.style.display='none';
	currentDiv.style.display='';
}