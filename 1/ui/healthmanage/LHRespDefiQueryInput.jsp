<%
//程序名称：LHRespDefiInput.jsp
//程序功能：功能描述
//创建日期：2005-03-19 15:05:48
//创建人  ：ctrHTML
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-01 10:38:48
// 更新原因/内容: 插入新的字段
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHRespDefiQueryInput.js"></SCRIPT> 
  <%@include file="LHRespDefiQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLHRespDefiGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <!--<TD  class= title>
      合同责任项目代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DutyItemCode >
    </TD>
    <TD  class= title>
      合同责任项目名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DutyItemName verify = "责任项目名称|len<=50">
    </TD> --> 
    <TD  class= title>
      合同项目责任类型代码
    </TD>
    <TD  class= input> 	
    	<Input class= 'common' name=DutyItemTye> 
    </TD>
    <TD  class= title>
      合同项目责任类型名称
    </TD>
     <TD  class= input> 	
     	<Input class= 'code' name=DutyItemTye_ch verify="项目责任类型|len<=10" ondblClick="showCodeList('hmdeftype',[this,DutyItemTye],[1,0]);" onkeyup="if(event.keyCode ==13) return showCodeList('hmdeftype',[this,DutyItemTye],[0,1],null,fm.DutyItemTye_ch.value,'DutyItemTye_ch')";  onkeydown="return showCodeListKey('hmdeftype',[this,DutyItemTye],[0,1],null,fm.DutyItemTye_ch.value,'DutyItemTye_ch'); codeClear(DutyItemTye_ch,DutyItemTye);" >
    </TD>
  </TR>              
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD>
        <INPUT CLASS = cssButton VALUE="查询"   TYPE=button   class=common onclick="easyQueryClick();">
        <INPUT CLASS = cssButton VALUE="返回" TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHRespDefi1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHRespDefi1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHRespDefiGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
