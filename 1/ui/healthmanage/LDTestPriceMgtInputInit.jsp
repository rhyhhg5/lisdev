<%
//程序名称：LDTestPriceMgtInput.jsp
//程序功能：
//创建日期：2005-06-11 15:49:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('HospitCode').value = "";
		fm.all('HospitName').value = "";
    fm.all('RecordDate').value = "";


    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LDTestPriceMgtInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LDTestPriceMgtInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();   
    initTestItemPriceGrid();
    initTestGrpPriceGrid(); 
  }
  catch(re)
  {
    alert("LDTestPriceMgtInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initTestItemPriceGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="检查项目代码";   
		iArray[1][1]="60px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;													//选‘2’背景色为蓝色
		iArray[1][4]="hmtestgrpquery";           //CodeQueryBL中的引用参数                 
		iArray[1][5]="1|2";     								//引用代码对应第几列，'|'为分割符
		iArray[1][6]="0|1";    									//上面的列中放置引用代码中第几位值
		iArray[1][9]="检查项目代码|LEN<20";                      
		iArray[1][15]="code";         //数据库中字段，用于模糊查询                 
		iArray[1][17]="1";                                       
		iArray[1][19]="1" ;                                      
		
		
		iArray[2]=new Array(); 
		iArray[2][0]="检查项目名称";   
		iArray[2][1]="200px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;   
		iArray[2][4]="hmtestgrpquery";                           
		iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
		iArray[2][6]="1|0";    //上面的列中放置引用代码中第几位值
		iArray[2][15]="name";                          
		iArray[2][17]="2";                                       
		iArray[2][19]="1" ;                                      
		
		
		
		iArray[3]=new Array(); 
		iArray[3][0]="检查项目单价（元）";   
		iArray[3][1]="90px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="补充说明";
		iArray[4][1]="150px";         
		iArray[4][2]=20;            
		iArray[4][3]=1;
		
		iArray[5]=new Array();
		iArray[5][0]="价格类别";
		iArray[5][1]="0px";         
		iArray[5][2]=20;            
		iArray[5][3]=3;
		iArray[5][14]="JCXM";
		             
		
    TestItemPriceGrid = new MulLineEnter( "fm" , "TestItemPriceGrid" ); 
    //这些属性必须在loadMulLine前

    TestItemPriceGrid.mulLineCount = 0;   
    TestItemPriceGrid.displayTitle = 1;
    TestItemPriceGrid.hiddenPlus = 0;
    TestItemPriceGrid.hiddenSubtraction = 0;
    TestItemPriceGrid.canSel = 0;
    TestItemPriceGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    TestItemPriceGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //TestItemPriceGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

function initTestGrpPriceGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="体检套餐代码";   
		iArray[1][1]="60px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
		iArray[1][9]="体检套餐代码|LEN<60"
		
		iArray[2]=new Array(); 
		iArray[2][0]="体检套餐名称";   
		iArray[2][1]="200px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="hmchoosetest";                           
		iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
		iArray[2][6]="0|1";    //上面的列中放置引用代码中第几位值
		iArray[2][18]=300;
		
		iArray[3]=new Array(); 
		iArray[3][0]="体检套餐总价格（元）";   
		iArray[3][1]="90px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="补充说明";
		iArray[4][1]="150px";         
		iArray[4][2]=20;            
		iArray[4][3]=1;    
		
		iArray[5]=new Array();  
		iArray[5][0]="价格类别";         
		iArray[5][1]="0px";    
    iArray[5][2]=20;        TestGrpPriceGrid = new MulLineEnter( "fm" , "TestGrpPriceGrid" ); 
    iArray[5][3]=3;         //这些属性必须在loadMulLine前
		iArray[5][14]="TJTC";


    TestGrpPriceGrid.mulLineCount = 0;   
    TestGrpPriceGrid.displayTitle = 1;
    TestGrpPriceGrid.hiddenPlus = 0;
    TestGrpPriceGrid.hiddenSubtraction = 0;
    TestGrpPriceGrid.canSel = 0;
    TestGrpPriceGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    TestGrpPriceGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //TestGrpPriceGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}


</script>
