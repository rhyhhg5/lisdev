<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDTestPriceMgtSave.jsp
//程序功能：
//创建日期：2005-06-11 15:49:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
//  LDTestPriceMgtSchema tLDTestPriceMgtSchema   = new LDTestPriceMgtSchema();
  LDTestPriceMgtSet tLDTestPriceMgtSet   = new LDTestPriceMgtSet();
  OLDTestPriceMgtUI tOLDTestPriceMgtUI   = new OLDTestPriceMgtUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
//    tLDTestPriceMgtSchema.setHospitCode(request.getParameter("HospitCode"));
//    tLDTestPriceMgtSchema.setSerialNo(request.getParameter("SerialNo"));    
//    tLDTestPriceMgtSchema.setRecordDate(request.getParameter("RecordDate"));
//    
//    tLDTestPriceMgtSchema.setOperator(request.getParameter("Operator"));
//    tLDTestPriceMgtSchema.setMakeDate(request.getParameter("MakeDate"));
//    tLDTestPriceMgtSchema.setMakeTime(request.getParameter("MakeTime"));
//    tLDTestPriceMgtSchema.setModifyDate(request.getParameter("ModifyDate"));
//    tLDTestPriceMgtSchema.setModifyTime(request.getParameter("ModifyTime"));
    
    String tItemCode[] = request.getParameterValues("TestItemPriceGrid1");
    String tItemPrice[] = request.getParameterValues("TestItemPriceGrid3");
    String tExplain[] = request.getParameterValues("TestItemPriceGrid4");
    String tPriceClass[] = request.getParameterValues("TestItemPriceGrid5");
    
    String tItemCode2[] = request.getParameterValues("TestGrpPriceGrid1");  
    String tItemPrice2[] = request.getParameterValues("TestGrpPriceGrid3"); 
    String tExplain2[] = request.getParameterValues("TestGrpPriceGrid4");   
    String tPriceClass2[] = request.getParameterValues("TestGrpPriceGrid5");
    
    
    int ItemCodeCount = 1;
    if (tItemCode != null)
    {
    		ItemCodeCount = tItemCode.length;
		    for (int i = 0; i < ItemCodeCount; i++)
		    {
		    			LDTestPriceMgtSchema tLDTestPriceMgtSchema   = new LDTestPriceMgtSchema();
		    			tLDTestPriceMgtSchema.setHospitCode(request.getParameter("HospitCode")); 
		    			tLDTestPriceMgtSchema.setRecordDate(request.getParameter("RecordDate"));
		    			
		    			tLDTestPriceMgtSchema.setMedicaItemCode(tItemCode[i]);
		    			tLDTestPriceMgtSchema.setMedicaItemPrice(tItemPrice[i]);
		    			tLDTestPriceMgtSchema.setExplain(tExplain[i]);
		    			tLDTestPriceMgtSchema.setPriceClass(tPriceClass[i]);  
		    			tLDTestPriceMgtSet.add(tLDTestPriceMgtSchema);  
		    			System.out.println(tItemCode[i]+"-------"+tItemPrice[i]);
		    }                                 
     }
     else                                 
     {
     		LDTestPriceMgtSchema tLDTestPriceMgtSchema   = new LDTestPriceMgtSchema();
				tLDTestPriceMgtSchema.setHospitCode(request.getParameter("HospitCode")); 
				tLDTestPriceMgtSchema.setRecordDate(request.getParameter("RecordDate"));
				tLDTestPriceMgtSet.add(tLDTestPriceMgtSchema);  
     }
     	
		
		int ItemCodeCount2 = 1;                                                          
		if (tItemCode2 != null)                                                          
		{
				ItemCodeCount2 = tItemCode2.length;                                           
				for (int i = 0; i < ItemCodeCount2; i++)                                         
				{                                                                               
							LDTestPriceMgtSchema tLDTestPriceMgtSchema   = new LDTestPriceMgtSchema();
							tLDTestPriceMgtSchema.setHospitCode(request.getParameter("HospitCode"));  
							tLDTestPriceMgtSchema.setRecordDate(request.getParameter("RecordDate"));  
							
							tLDTestPriceMgtSchema.setMedicaItemCode(tItemCode2[i]);                    
							tLDTestPriceMgtSchema.setMedicaItemPrice(tItemPrice2[i]);                  
							tLDTestPriceMgtSchema.setExplain(tExplain2[i]);                            
							tLDTestPriceMgtSchema.setPriceClass(tPriceClass2[i]);    		              
							tLDTestPriceMgtSet.add(tLDTestPriceMgtSchema);
							System.out.println(tItemCode2[i]+"-------"+tItemPrice2[i]);
				}                                                                               
		}
		else                                 
     {
     		LDTestPriceMgtSchema tLDTestPriceMgtSchema   = new LDTestPriceMgtSchema();
				tLDTestPriceMgtSchema.setHospitCode(request.getParameter("HospitCode")); 
				tLDTestPriceMgtSchema.setRecordDate(request.getParameter("RecordDate"));
				tLDTestPriceMgtSet.add(tLDTestPriceMgtSchema);  
     }
		
                                      
  try                                 
  {                                   
  // 准备传输数据 VData               
  	VData tVData = new VData();       
//	tVData.add(tLDTestPriceMgtSchema); 
	tVData.add(tLDTestPriceMgtSet); 
  	tVData.add(tG);                   
    tOLDTestPriceMgtUI.submitData(tVData,transact);
  }                                   
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDTestPriceMgtUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
