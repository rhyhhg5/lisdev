<%
//程序名称：LHCustomOPSInput.jsp
//程序功能：
//创建日期：2005-01-17 17:17:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
	String tName = "";
	String tNo = "";
	try
	{
		tName = request.getParameter("tCustomerName");
	        tName = new String(tName.getBytes("ISO-8859-1"), "GBK");
	}
	catch( Exception e )
	{
		tName = "";
	}
	if(request.getParameter("tCustomerNo")!=null)
	{
		tNo=request.getParameter("tCustomerNo");
	}
%>                               
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('CustomerNo').value = "<%=tNo%>";
    fm.all('CustomerName').readOnly=true;
    fm.all('CustomerName').value = "<%=tName%>"
    fm.all('OPSNo').readOnly=true;
    fm.all('OPSNo').value = "";
    fm.all('MedicaItemCode').value = "";
    fm.all('RxSummar').value = "";
    fm.all('ExecutDate').value = "";
    fm.all('HospitCode').value = "";
    fm.all('Doctor').value = "";
    fm.all('ImpartFlag').value = "";
    fm.all('ImpartDate').value = "";
    fm.all('InHospitNo').value = "";
    fm.all('InHospitFee').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHCustomOPSInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LHCustomOPSInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("LHCustomOPSInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
