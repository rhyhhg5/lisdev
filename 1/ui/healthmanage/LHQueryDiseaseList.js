//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var pageNum = "";
var pageFlag = "";
var needReNew = false;
var LHDiseaseListGrid; 
var gridCode;
var gridName;
var gridInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDDisease.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
          
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
    }
	else 
	{
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
            
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDiseaseQuery.html");
}           
            
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  通过传入的结果参数进行显示
 *  参数  ：  传入的包含关键字的文件名字符串
 *  返回值：  无
 *********************************************************************
 */

function submitForm()
{
	if(keyword == "" )
	{
		var itemgroupcode = "";
		if(itemgroup == "Disease")
		{
			itemgroupcode = "1";
		}
		else if(itemgroup == "Drug")
			{
				itemgroupcode = "2";
			}
			else if(itemgroup == "Operation")
				{
					itemgroupcode = "3";
				}
				else if(itemgroup == "Testitem")
					{
						itemgroupcode = "4";
					}
					else{itemgroupcode = "5";}
						
		if( itemname == "")
		{				
			var sql = " select itemcode, itemname, itemkeyword from lhqueryinfo where 1=1 and itemtype = '"+itemgroupcode+"'"
					 +" order by itemName"
					 ;
			turnPage.pageLineNum = 25;
			turnPage.queryModal(sql, LHDiseaseListGrid);
		}
		else
		{
			var sql = " select itemcode, itemname, itemkeyword from lhqueryinfo where itemname like '%%"+itemname+"%%' and itemtype = '"+itemgroupcode+"'"
					 +" order by itemName"
					 ;
			turnPage.pageLineNum = 25;
			turnPage.queryModal(sql, LHDiseaseListGrid);
		}
		
		if(easyExecSql(sql) == null)
		{
			alert("无符合条件数据");
			top.close();				
		}
	}
	else
	{		
		fm.all('keyWord').value = keyword;
		fm.all('itemName').value = itemname;
		fm.all('startIndex').value = "0";
		fm.all('itemGroup').value = itemgroup;
		fm.submit();
	}
}

function submitA()
{

		strURL =  "./LHQueryDiseaseSave.jsp?keyword="  +  keyword+"&itemname="+itemname ;

   		Request  =  new  ActiveXObject("Microsoft.XMLHTTP");
		Request.open("GET",  strURL,  false);
		Request.send(null);
		try
		{
			var fileList = "";
			fileList = Request.responseText;
			showFileList( fileList)
		}
		catch(e1)
		{
		  alert(e1.message);
		}
}

//计算页数,看是否需要重新查询
function pageCalculate(pageFlag)
{
	pageNum = turnPage.pageIndex;
	if(pageFlag==1)
	{
		pageNum=0;
		if(Math.floor(pageNum/8) != Math.floor(turnPage.pageIndex/8))
		{
			needToReQuery();
		}
		else{turnPage.firstPage();}
	}
	if(pageFlag == 2 && pageNum != 0)
	{
		pageNum--;
		if(Math.floor(pageNum/8) != Math.floor(turnPage.pageIndex/8))
		{
			needToReQuery();
		}
		else{turnPage.previousPage();}
	}
	if(pageFlag == 3 && pageNum != Math.ceil(turnPage.queryAllRecordCount/25))
	{
		pageNum++;
		if(Math.floor(pageNum/8) != Math.floor(turnPage.pageIndex/8))
		{
			needToReQuery();
		}
		else{turnPage.nextPage();}
	}
	if(pageFlag==4)
	{
		pageNum= Math.floor(turnPage.queryAllRecordCount/25);
		if(Math.floor(pageNum/8) != Math.floor(turnPage.pageIndex/8))
		{
			needToReQuery();
		}	
		else{turnPage.lastPage();}
	}
	
}

function needToReQuery()
{
		needReNew = true;
		fm.all('keyWord').value = keyword;
		fm.all('itemName').value = itemname;
		fm.all('startIndex').value = (pageNum)*25;	
		fm.all('itemGroup').value = itemgroup;
		fm.submit();
}

function showFileList( fileList)
{
		if(fileList == "")
		{
			alert("无符合条件数据");
			top.close();
			return false;
		}
//		
//		var sql_getName = "";
//		var fileArray = new Array();
//		fileArray = fileList.split(",");
//
//		for(var i = 0; i < fileArray.length; i++)
//		{
//			sql_getName = sql_getName +"'"+fileArray[i]+"', "
//		}
//
//		sql_getName = sql_getName.substring(0,sql_getName.length-2);
//
//		var sql = " select itemcode, itemname, itemkeyword from lhqueryinfo where itemcode in ("+sql_getName+")"	;
//    
//		turnPage.pageLineNum = 25;
//		turnPage.queryModal(sql, LHDiseaseListGrid);
		
		
//		if (typeof(turnPage) == "object")
//		{
//		    codeType = getCodeTypeParam(turnPage.pageDisplayGrid.arraySave);
//		    pageRowNum = turnPage.pageLineNum;
//		    hiddenPlus = turnPage.pageDisplayGrid.hiddenPlus;
//		    hiddenSubtraction = turnPage.pageDisplayGrid.hiddenSubtraction;
//		}
		
		if(needReNew == false)
		{
			var strResult = fileList;
			strQueryResult = strResult.substring(strResult.lastIndexOf("\n")+1);
			strQueryResult = Conversion(strQueryResult);
			if (typeof(strQueryResult) == "string" && strQueryResult.substring(0, strQueryResult.indexOf("|")) != "0") 
			{
	    		return false;
	 		} 
	 		
	 		var cTurnPage = turnPage;
	 		cTurnPage.pageLineNum = 25;
	 		cTurnPage.strQueryResult = strQueryResult;
		
			mCodeType = ""; //把mCodeType清空
		
	
			//判断是否查询成功
			if (!cTurnPage.strQueryResult)
			{  
			    //清空MULTILINE，使用方法见MULTILINE使用说明 
			    LHDiseaseListGrid.clearData();  
			    //alert("没有查询到任何信息！");
			    return false;
			}
			
			
			  
		    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
			cTurnPage.arrDataCacheSet = clearArrayElements(cTurnPage.arrDataCacheSet);
			
			//查询成功则拆分字符串，返回二维数组
			cTurnPage.arrDataCacheSet = decodeEasyQueryResult(cTurnPage.strQueryResult, 0, 0, cTurnPage);
			
			//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
			cTurnPage.pageDisplayGrid = LHDiseaseListGrid;   
			
			LHDiseaseListGrid.SortPage=cTurnPage;  
	
			 
			//设置查询起始位置
			cTurnPage.pageIndex       = 0;  
			
			//在查询结果数组中取出符合页面显示大小设置的数组
			var arrDataSet = cTurnPage.getData(cTurnPage.arrDataCacheSet, cTurnPage.pageIndex, cTurnPage.pageLineNum);
			
			//调用MULTILINE对象显示查询结果
			displayMultiline(arrDataSet, cTurnPage.pageDisplayGrid, cTurnPage);
		}
		
		else
		{
			
			var strResult = fileList;
			strQueryResult = strResult.substring(strResult.lastIndexOf("\n")+1);
			strQueryResult = Conversion(strQueryResult);
			if (typeof(strQueryResult) == "string" && strQueryResult.substring(0, strQueryResult.indexOf("|")) != "0") 
			{
	    		return false;
	 		} 
	 		
	 		var cTurnPage = turnPage;
	 		turnPage.pageIndex = pageNum;
	 		cTurnPage.pageLineNum = 25;
	 		cTurnPage.cacheStart = pageNum*25;
	 		cTurnPage.strQueryResult = strQueryResult;

			
			//查询成功则拆分字符串，返回二维数组
			cTurnPage.arrDataCacheSet = decodeEasyQueryResult(cTurnPage.strQueryResult, 0, 0, cTurnPage);
		    var nextDateBlockLineIndex = cTurnPage.pageIndex * cTurnPage.pageLineNum - cTurnPage.cacheStart ;
		    displayMultiline(getPageDisplayData(cTurnPage.arrDataCacheSet, nextDateBlockLineIndex, cTurnPage.pageLineNum), LHDiseaseListGrid, cTurnPage);
		}

	    //控制是否显示翻页按钮
		if (cTurnPage.queryAllRecordCount > cTurnPage.pageLineNum) 
		{
		    try 
		    { 
		    	window.document.all(cTurnPage.pageDivName).style.display = "";
				LHDiseaseListGrid.setPageMark(cTurnPage);	
			}
			catch(ex)
			{}
		}
		else 
		{
		    try { window.document.all(cTurnPage.pageDivName).style.display = "none"; } catch(ex) { }
		}
}
                                 
function initForm() 
{ 
  try 
  {
    initGridItemName();
    initLHDiseaseListGrid();  
  }
  catch(re) 
  {
    alert("LHQueryDiseaseList.js-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGridItemName()
{
	if(itemgroup == "Disease")
	{
		gridCode = "疾病代码";
		gridName = "疾病名称";
		gridInfo = "疾病信息";
		 
	}
	else if(itemgroup == "Drug")
	{
		gridCode = "药品代码";
		gridName = "药品名称";
		gridInfo = "药品信息";		
	}
	else if(itemgroup == "Operation")
	{
		gridCode = "手术代码";
		gridName = "手术名称";
		gridInfo = "手术信息";		
	}
	else if(itemgroup == "Testitem")
	{
		gridCode = "检验代码";
		gridName = "检验名称";
		gridInfo = "检验信息";		
	}
	else if(itemgroup == "Hospital")
	{
		gridCode = "医院代码";
		gridName = "医院名称";
		gridInfo = "医院信息";		
	}
	else
	{
		gridCode = "";
		gridName = "";
		gridInfo = "";		
	}
}

//项信息列表的初始化
function initLHDiseaseListGrid() 
{                               
  var iArray = new Array();
    
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]=gridCode;   
	iArray[1][1]="50";   
	iArray[1][2]=20;        
	iArray[1][3]=0;
		
	iArray[2]=new Array(); 
	iArray[2][0]=gridName;   
	iArray[2][1]="160px";   
	iArray[2][2]=20;        
	iArray[2][3]=0;
		
	iArray[3]=new Array(); 
	iArray[3][0]=gridInfo;   
	iArray[3][1]="400px";   
	iArray[3][2]=20;        
	iArray[3][3]=0;
    
    LHDiseaseListGrid = new MulLineEnter( "fm" , "LHDiseaseListGrid" ); 
    //这些属性必须在loadMulLine前

    LHDiseaseListGrid.mulLineCount = 0;   
    LHDiseaseListGrid.displayTitle = 1;
    LHDiseaseListGrid.hiddenPlus = 1;
    LHDiseaseListGrid.hiddenSubtraction = 1;
    LHDiseaseListGrid.canSel = 1;
    LHDiseaseListGrid.canChk = 0;
   	LHDiseaseListGrid.selBoxEventFuncName ="showDetail"
    //LHDiseaseListGrid.selBoxEventFuncName = "showOne";

    LHDiseaseListGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHDiseaseListGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

 