//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass()
var turnPage2 = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if(LHServCostGrid.mulLineCount == 0)
	{
		alert("请录入数据");
		return false;
	}
	fm.fmtransact.value="INSERT||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHServCost.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
//	if(LHServCostGrid.mulLineCount == 0)
//	{
//		alert("请录入数据");
//		return false;
//	}
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
	  var i = 0;
	  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
	  //showSubmitFrame(mDebug);
	  fm.fmtransact.value = "UPDATE||MAIN";
	  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHServCostQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
        fm.all('ServPriceCode').value= arrResult[0][0];
        fm.all('ServPriceName').value= arrResult[0][1];
		
		var sqls = " select ServItemName from LHServerItemPrice where ServPriceCode = '"+arrResult[0][0]+"'";
		var arr	= easyExecSql(sqls);
		fm.all('ServItemName').value = arr[0][0];
		var sql = " select a.balanceobjcode, a.balanceobjname,  a.contraitemno, "
		 		  +" (select c.dutyitemname  from lhcontitem b, ldcontraitemduty c "
		  		  +" where b.contraitemno = a.contraitemno and b.dutyitemcode = c.dutyitemcode),a.MakeDate,a.MakeTime,a.contrano "
			  	  +" from LHServCost a where ServPriceCode = '"+arrResult[0][0]+"'"
			  	  ;
		turnPage.queryModal(sql, LHServCostGrid);
	}   
}               
        
function showOne()
{
	      
	      var getSelNo = LHServCostGrid.getSelNo();
	      var ContraItemNo = LHServCostGrid.getRowColData(getSelNo-1, 3);
	      //alert(ContraItemNo);
	            var sqlType="select ServPriceType,ContraType from LHContServPrice "
                      +"where  ContraItemNo='"+ ContraItemNo+"'";
         // alert(sqlType);
          //alert(easyExecSql(sqlType));
          var arrResult = new Array();
          arrResult=easyExecSql(sqlType);
          if(arrResult!=""||arrResult!="null"||arrResult!=null)
          {
          	 divLHContServPrice.style.display='';
             fm.ServicePriceCode.value=arrResult[0][0];
             fm.all('ContraType').value=arrResult[0][1];
              
          }
          //alert(fm.ServicePriceCode.value);
          if(fm.all('ContraType').value=="01")
          {
          	divTestServPrice.style.display='';
          }
          else
          {
          	divTestServPrice.style.display='none';
          } 
          if(fm.all('ContraType').value== "02")//核保体检 && fm.all('ContraNowName').value== "HB0001"
		      {
		      	//alert("2");
		      	fm.ServicePriceCode.value='2';
		      	fm.ServicePriceName.value='核保体检价格';
		      	//alert(fm.ServicePriceName.value);
		        fm.BalanceTypeCode.value='5';
		      	fm.BalanceTypeName.value='核保体检费用';
		      	divTestMoney.style.display='none';
		      	divTestItemPriceGrid.style.display='';
		      	divTestGrpPriceGrid.style.display='';
		      	divTestMoney2.style.display='none';
		      	divShowInfo.style.display='';
		      	divTestServPrice.style.display='none';
		      	//initTestItemPriceGrid(); 
            //initTestGrpPriceGrid(); 
           
                if(fm.ServicePriceCode.value=="2")
				        {
					      	  divTestMoney.style.display='none';
					      	  divTestServPrice.style.display='none';
					      	  divTestMoney2.style.display='none';
					      	  divTestMoney3.style.display='none';
					      		divTestItemPriceGrid.style.display='';
					      		divTestGrpPriceGrid.style.display='';
					      		 divShowInfo.style.display='';
					      		initTestItemPriceGrid(); 
                    initTestGrpPriceGrid(); 
					      		 var itemPriceSql= "select a.MedicaItemCode,"
					                       +"  CASE  WHEN "
 	                               +"(select b.MedicaItemCode from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) not like 'ZH%%' "
     			      								 +" THEN (select b.MedicaItemname from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) "
                                 +" ELSE (select distinct c.Testgrpname from ldtestgrpmgt c where c.testgrpcode = a.MedicaItemCode ) END ,"
					                       +" MedicaItemPrice ,Explain,PriceClass,SerialNo "
					                       +" from LDTestPriceMgt a "
		                             +" where a.ContraItemNo='"+ ContraItemNo +"'"
		                             +" and a.PriceClass='JCXM'";
		                             //alert(itemPriceSql);
		                             //alert(easyExecSql(itemPriceSql));
		                      turnPage2.pageLineNum = 100;
				              turnPage2.queryModal(itemPriceSql, TestItemPriceGrid);
				               var testGrpPriceSql= "select a.MedicaItemCode,"
					                       +" (select distinct b.Testgrpname from ldtestgrpmgt b where b.Testgrpcode = a.MedicaItemCode and  testgrptype='1' ) ,"
					                       +" a.MedicaItemPrice ,a.Explain,PriceClass,SerialNo "
					                       +" from LDTestPriceMgt  a"
		                             +" where a.ContraItemNo='"+ ContraItemNo+"'"
		                             +" and a.PriceClass='TJTC'";
		                  //alert(testGrpPriceSql);
		                  turnPage3.pageLineNum = 100;
		                  turnPage3.queryModal(testGrpPriceSql, TestGrpPriceGrid);
                 
				        }
				   }
          else
          {
                var sql2Type="select a.ServPriceType,a.ExpType,"
                            //+"(select  case when a.ServPriceType = '1' then '服务单价' else '体检价格' end from dual),"
					      			      +"( case a.ServPriceType when '2' then '核保体检价格' when '1' then '按次计费价格(元/次)' when '3' then '按人计费价格(元/人)' when '4' then '按人包干价格' else '未知价格' end ),"
					      			      +"( case a.ExpType when '1' then '健康体检费用' when '2' then '健康管理技术费用' when '3' then '医师费用' when '4' then '其他费用' when '5' then '核保体检费用' else '未知费用' end )"
                            +" from LHContServPrice a " 
                            +"where a.ContraItemNo='"+ ContraItemNo +"'";
                var arrResult2 = new Array();
                arrResult2 = easyExecSql(sql2Type);		
                if(arrResult2!="null"&&arrResult2!=""&&arrResult2!=null)
                {
                  fm.ServicePriceCode.value=arrResult2[0][0];
                  fm.BalanceTypeCode.value=arrResult2[0][1];
                  fm.ServicePriceName.value=arrResult2[0][2];
                  fm.BalanceTypeName.value=arrResult2[0][3];
                }
               // alert(fm.ServicePriceCode.value);
                if(fm.ServicePriceCode.value=="2")
				        {
					      	  divTestMoney.style.display='none';
					      	  divTestMoney2.style.display='none';
					      	  divTestMoney3.style.display='none';
					      		divTestItemPriceGrid.style.display='';
					      		divTestGrpPriceGrid.style.display='';
					      		 divShowInfo.style.display='';
					      		initTestItemPriceGrid(); 
                    initTestGrpPriceGrid(); 
					      		 var itemPriceSql= "select a.MedicaItemCode,"
					                       +"  CASE  WHEN "
 	                               +"(select b.MedicaItemCode from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) not like 'ZH%%' "
     			      								 +" THEN (select b.MedicaItemname from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) "
                                 +" ELSE (select distinct c.Testgrpname from ldtestgrpmgt c where c.testgrpcode = a.MedicaItemCode ) END ,"
					                       +" MedicaItemPrice ,Explain,PriceClass,SerialNo "
					                       +" from LDTestPriceMgt a "
		                             +" where a.ContraItemNo='"+ ContraItemNo +"'"
		                             +" and a.PriceClass='JCXM'";
		                      turnPage2.pageLineNum = 100;
				              turnPage2.queryModal(itemPriceSql, TestItemPriceGrid);

		                  if(fm.all('ContraType').value=="02")
				              {
				               var testGrpPriceSql= "select a.MedicaItemCode,"
					                       +" (select distinct b.Testgrpname from ldtestgrpmgt b where b.Testgrpcode = a.MedicaItemCode and  testgrptype='1' ) ,"
					                       +" a.MedicaItemPrice ,a.Explain,PriceClass,SerialNo "
					                       +" from LDTestPriceMgt  a"
		                             +" where a.ContraItemNo='"+ ContraItemNo+"'"
		                             +" and a.PriceClass='TJTC'";
		                  }
		                  //alert(testGrpPriceSql);
		                  turnPage3.pageLineNum = 100;
		                  turnPage3.queryModal(testGrpPriceSql, TestGrpPriceGrid);
                 
				        }
				        //alert(fm.ServicePriceCode.value);
				        if(fm.ContraType.value=="01")
				        {
				        	  var MedicaItemCode= "select a.MedicaItemCode,"
					               +" (select distinct b.Testgrpname from ldtestgrpmgt b where b.Testgrpcode = a.MedicaItemCode ) "
					               //+" PriceClass"
					               +" from LHContServPrice  a"
		                     +" where  a.ContraItemNo='"+ ContraItemNo+"'";
		                     //+" and a.PriceClass='TJTC'";

				        		var arrResult4=new Array;
					      		 arrResult4 = easyExecSql(MedicaItemCode);	
					      		 		 		if(arrResult4!="null"&&arrResult4!=""&&arrResult4!=null) 
					      				 		{ 
					      				 			fm.all('MedicaItemCode').value   = arrResult4[0][0];
					      		          fm.all('MedicaItemName').value   = arrResult4[0][1];	
					      		        }
				        }
				        if(fm.ServicePriceCode.value=="1")
				        {
				        	  divTestMoney.style.display='';
				        	  divTestMoney2.style.display='none';
				        	  divTestMoney3.style.display='none';
					      		divTestItemPriceGrid.style.display='none';
					      		divTestGrpPriceGrid.style.display='none';
					      		divShowInfo.style.display='none';
					      		var mulSql3 = "select a.ContraNo ,"
					      						            +"a.ContraItemNo ,"
					      			                  +"a.MoneySum ,a.ExplanOther"
					      			                  +" from LHContServPrice a "
					      		                    +" where  "
					      		                    +"  a.ContraItemNo ='"+ContraItemNo+"'";
					      		        // alert(mulSql3);
					      		        // alert(easyExecSql(mulSql3));
					      		         var arrResult3=new Array;
					      				 		arrResult3 = easyExecSql(mulSql3);		
					      				 		if(arrResult3!="null"&&arrResult3!=""&&arrResult3!=null) 
					      				 		{ 
					      		          fm.all('MoneySum').value              = arrResult3[0][2];	
					      		          fm.all('ExplanOther').value              = arrResult3[0][3];	
					      		          
					      		        }
				        }
				        if(fm.ServicePriceCode.value=="3")
				        {
				        	  divTestMoney2.style.display='';
				        	  divTestMoney3.style.display='none';
				        	  divTestMoney.style.display='none';
					      		divTestItemPriceGrid.style.display='none';
					      		divTestGrpPriceGrid.style.display='none';
					      		divShowInfo.style.display='none';
					      		var mulSql3 = "select a.ContraNo ,"
					      						            +"a.ContraItemNo ,"
					      			                  +"a.MoneySum ,a.ServPros,a.ExplanOther,a.MedicaItemCode"
					      			                  +" from LHContServPrice a "
					      		                    +" where  "
					      		                    +" a.ContraItemNo ='"+ ContraItemNo +"'";
					      		 var arrResult3=new Array;
					      				 		arrResult3 = easyExecSql(mulSql3);		
					      				 		if(arrResult3!="null"&&arrResult3!=""&&arrResult3!=null) 
					      				 		{ 
					      		          fm.all('MoneySum2').value              = arrResult3[0][2];	
					      		          fm.all('ServPros2').value              = arrResult3[0][3];	
					      		          fm.all('ExplanOther2').value           = arrResult3[0][4];	
					      		        }
					         
				        }
				        if(fm.ServicePriceCode.value=="4")
				        {
				        	  divTestMoney3.style.display='';
				        	  divTestMoney2.style.display='none';
				        	  divTestMoney.style.display='none';
					      		divTestItemPriceGrid.style.display='none';
					      		divTestGrpPriceGrid.style.display='none';
					      		divShowInfo.style.display='none';
					      		var mulSql3 = "select a.ContraNo ,"
					      						            +"a.ContraItemNo ,"
					      			                  +"a.MoneySum ,a.ServPros,a.MaxServNum,a.ExplanOther,a.MedicaItemCode"
					      			                  +" from LHContServPrice a "
					      		                    +" where  "
					      		                    +"  a.ContraItemNo ='"+ ContraItemNo+"'";
					      		                    //alert(mulSql3);
					      		                    //alert(easyExecSql(mulSql3));
					      		 var arrResult3=new Array;
					      				 		arrResult3 = easyExecSql(mulSql3);
					      				 				
					      				 		if(arrResult3!="null"&&arrResult3!=""&&arrResult3!=null) 
					      				 		{ 
					      		          fm.all('MoneySum3').value              = arrResult3[0][2];	
					      		          fm.all('ServPros3').value              = arrResult3[0][3];	
					      		          fm.all('MaxServNum3').value            = arrResult3[0][4];	
					      		          fm.all('ExplanOther3').value           = arrResult3[0][5];	
					      		        }
					         
				        }
            } 
}      