<%
//程序名称：LHServTypeQueryInput.jsp
//程序功能：功能描述
//创建日期：2006-07-05 13:44:27
//创建人  ：ctrHTML
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHServTypeQueryInput.js"></SCRIPT> 
  <%@include file="LHServTypeQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
  <table  class= common align='center' >
  	<TR  class= common>
  		<TD  class= title>
	      	保单类型
	    </TD>
	    <TD  class= input>
	    	  <Input type=hidden name=ContType>
	     	  <Input class= 'code' name=ContType_ch verify="保单类型|notnull" elementtype=nacessary CodeData= "0|^个人保单|1^团体保单|2" ondblClick= "showCodeListEx('',[this,ContType],[0,1],null,null,null,1);"> 
	    </TD> 
	    <TD  class= title >
	      	保单所属机构标识
	    </TD>
	    <TD  class= input>
	    	  <Input class= 'common' name=ComID style="width:40px"><Input class= 'code' name=ComID_ch style="width:100px" verify="保单所属机构标识|notnull" elementtype=nacessary ondblclick="showCodeList('hmhospitalmng',[this,ComID],[0,1],null,null,null,1);" > 
	    </TD> 
	    <TD  class= title>
	      	服务项目名称
	    </TD>
	    <TD  class= input>
	     	  <Input class= 'common' name=ServItemCode style="width:40px" ><Input class='code' name=ServItemName verify="服务项目名称|notnull" elementtype=nacessary ondblclick="showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemName.value,'ServItemName','1',300);" codeClear(ServItemName,ServItemCode);" > 
	    </TD> 
	  </TR>
  	<TR  class= common>
  		<TD  class= title>
	      	服务事件类型
	    </TD>
	    <TD  class= input>
	    	  <Input type=hidden name=ServCaseType>
	     	  <Input class= 'code' name=ServCaseType_ch verify="服务事件类型|notnull" elementtype=nacessary CodeData= "0|^个人服务事件|1^集体服务事件|2" ondblClick= "showCodeListEx('',[this,ServCaseType],[0,1],null,null,null,1);" > 
	    </TD> 
	    <TD  class= title>
	    </TD>
	    <TD  class= input>
	    </TD> 
	    <TD  class= title>
	    </TD>
	    <TD  class= input>
	    </TD> 
    </TR>	
  	</table>
  	
	<table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
  
  <Div id="divLHCustomInHospital1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHServTypeGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>