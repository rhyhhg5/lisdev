<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHRespDefiSave.jsp
//程序功能：
//创建日期：2005-03-19 15:05:48
//创建人  ：CrtHtml程序创建
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-01 10:38:48
// 更新原因/内容: 插入新的字段
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  OLDContraItemDutyUI tOLDContraItemDutyUI = new OLDContraItemDutyUI();
  LDContraItemDutySchema tLDContraItemDutySchema = new LDContraItemDutySchema();

  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
    tLDContraItemDutySchema.setDutyItemCode(request.getParameter("DutyItemCode"));
    tLDContraItemDutySchema.setDutyItemName(request.getParameter("DutyItemName"));
    tLDContraItemDutySchema.setDutyItemTye(request.getParameter("DutyItemTye"));
    tLDContraItemDutySchema.setDutyItemExplain(request.getParameter("DutyItemExplain"));
 
 		tLDContraItemDutySchema.setOperator(request.getParameter("Operator"));
		tLDContraItemDutySchema.setMakeDate(request.getParameter("MakeDate"));
		tLDContraItemDutySchema.setMakeTime(request.getParameter("MakeTime"));
//		tLDContraItemDutySchema.setModifyDate(request.getParameter("ModifyDate"));
//		tLDContraItemDutySchema.setModifyTime(request.getParameter("ModifyTime"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLDContraItemDutySchema);
  	tVData.add(tG);
    tOLDContraItemDutyUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDContraItemDutyUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
