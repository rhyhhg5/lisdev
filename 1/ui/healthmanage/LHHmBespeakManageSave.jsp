<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHHmBespeakManageSave.jsp
//程序功能：
//创建日期：2006-09-06 14:14:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHHmBespeakManageUI tLHHmBespeakManageUI   = new LHHmBespeakManageUI();
  
  LHHmServBeManageSet tLHHmServBeManageSet = new LHHmServBeManageSet();		//服务预约管理表
  LHTaskCustomerRelaSet tLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();		//客户任务实施管理表
    
    String tTaskExecNo[] = request.getParameterValues("LHHmServBeManageGrid12");           //任务实施号码流水号
    String tServTaskNo[] = request.getParameterValues("LHHmServBeManageGrid10");           //服务任务编号
    String tServCaseCode[] = request.getParameterValues("LHHmServBeManageGrid11");					//服务事件号  
    String tServTaskCode[] = request.getParameterValues("LHHmServBeManageGrid14");           //服务任务代码
    String tServItemNo[] = request.getParameterValues("LHHmServBeManageGrid13");					//服务项目序号
    String tCustomerNo[] = request.getParameterValues("LHHmServBeManageGrid1");					//客户号  	 
     
    String tChk[] = request.getParameterValues("InpLHHmServBeManageGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  
    
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");

   int LHHmServBeManageCount = 0;
   System.out.println("R R  "+tTaskExecNo.length);
	 if(tTaskExecNo != null)
	 {	
		    LHHmServBeManageCount = tTaskExecNo.length;
	 }	
	  //System.out.println(" LHHmServBeManageCount is : "+LHHmServBeManageCount);
    for(int j = 0; j < LHHmServBeManageCount; j++)
	  {
	     
		    LHHmServBeManageSchema tLHHmServBeManageSchema = new LHHmServBeManageSchema();
		    
		     tLHHmServBeManageSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		     tLHHmServBeManageSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		     tLHHmServBeManageSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		     tLHHmServBeManageSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		     tLHHmServBeManageSchema.setServItemNo(tServItemNo[j]);	//项目号码
		     tLHHmServBeManageSchema.setCustomerNo(tCustomerNo[j]);	//客户号  
		     
		     tLHHmServBeManageSchema.setBespeakComID(request.getParameter("HospitCode"));//预约机构
				 tLHHmServBeManageSchema.setServBespeakDate(request.getParameter("ServiceDate"));  //预约日期
				 tLHHmServBeManageSchema.setServBespeakTime(request.getParameter("ServiceTime"));//预约时间 
				 tLHHmServBeManageSchema.setTestGrpCode(request.getParameter("MedicaItemGroup"));  //体检代码
				 
				 tLHHmServBeManageSchema.setBalanceManner(request.getParameter("BalanceManner"));//结算方式
				 tLHHmServBeManageSchema.setServDetail(request.getParameter("ServItemNote"));     //备注         
         //tLHHmServBeManageSet.add(tLHHmServBeManageSchema);  
         
         LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema();
		 
		     tLHTaskCustomerRelaSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		     tLHTaskCustomerRelaSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		     tLHTaskCustomerRelaSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		     tLHTaskCustomerRelaSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		     tLHTaskCustomerRelaSchema.setServItemNo(tServItemNo[j]);	//项目号码
		     tLHTaskCustomerRelaSchema.setTaskExecState(request.getParameter("ExecState"));     //任务执行状态      
         
         if(transact.equals("INSERT||MAIN"))
         {
           tLHHmServBeManageSet.add(tLHHmServBeManageSchema);   
         }  
         if(transact.equals("UPDATE||MAIN"))
         {
            if(tChk[j].equals("1"))  
            {        
               System.out.println("该行被选中 "+tTaskExecNo[j]);     
               tLHHmServBeManageSet.add(tLHHmServBeManageSchema); 
               tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);             
           }
        }         
      }
                              
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHHmServBeManageSet);
  	tVData.add(tLHTaskCustomerRelaSet);
  	tVData.add(tG);
    tLHHmBespeakManageUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHHmBespeakManageUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	  var transact = "<%=transact%>";
	      parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>