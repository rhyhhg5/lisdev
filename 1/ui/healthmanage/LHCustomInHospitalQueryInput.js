/** 
 * 程序名称：LHCustomInHospitalInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-01-18 09:08:27
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     

  var strSql="select distinct a.CustomerNo,b.name, a.InHospitNo, "
  +" (select distinct d.HospitName from LDHospital d where d.Hospitcode = a.HospitCode),a.InHospitDate,"
  +" (select distinct e.DoctName from LDDoctor e, LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo and e.DoctNo = c.DoctNo ), a.InHospitMode, "
  +" (select distinct c.MainCureMode from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo), a.HospitCode,"
  +" (select distinct c.DoctNo from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo) "
  +" from LHCustomInHospital a, ldperson b where   "
  +" a.CustomerNo=b.CustomerNo "//and b.CustomerNo=c.CustomerNo and a.InHospitNo = c.InHospitNo "
  +getWherePart("a.CustomerNo","CustomerNo") 
  +getWherePart("b.CustomerNo","CustomerNo")
  +getWherePart("a.Hospitcode","HospitCode")
  +getWherePart("a.InHospitMode","InHospitModeCode")
  +getWherePart("a.InHospitDate","InHospitStartDate",">")
  +getWherePart("a.InHospitDate","InHospitEndDate","<")
  +getWherePart("b.name","CustomerName")
  +" and (a.inhospitmode is null or a.inhospitmode in ('1','2','3','')) "
  +" and a.Managecom like '"+manageCom+"%%'"
  +" order by a.InHospitDate DESC"
  ;
  //alert(strSql);
  //alert(easyExecSql(strSql));
	turnPage.queryModal(strSql, LHCustomInHospitalGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	      var tSel = LHCustomInHospitalGrid.getSelNo();

		
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{
				arrReturn = getQueryResult();//alert(arrReturn);
				//alert("12")
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery0接口。" + ex );
			}
			parent.opener.top.window.focus();//top的父窗口得到焦点
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHCustomInHospitalGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LHCustomInHospitalGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function queryDiseas()
{
	if(fm.all('DiseasCode').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("LDDiseaseQueryInput.jsp");	  
	  }
	if(fm.all('DiseasCode').value != "")	 
	{
		var cDiseasCode = fm.DiseasCode.value;  //疾病代码	
		var strSql = "select ICDCode,ICDName from LDDisease where ICDCode='" + cDiseasCode +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.DiseasCode.value = arrResult[0][0];
	      alert("查询结果:  疾病代码:["+arrResult[0][0]+"] 疾病名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("疾病代码为:["+fm.all('DiseasCode').value+"]的疾病不存在，请确认!");
     }
	}	
}

function afterQuery00(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.DiseasCode.value = arrResult[0][0];
  }
}

function queryDiseas2()
{
	if(fm.all('DiseasCode').value != ""&& fm.all('DiseasCode').value.length==20)	 
	{
	var cDiseasCode = fm.DiseasCode.value;  //疾病代码	
	var strSql = "select ICDCode,ICDName from LDDisease where trim(ICDCode)='" + cDiseasCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.DiseasCode.value = arrResult[0][0];
      alert("查询结果:  疾病代码:["+arrResult[0][0]+"] 疾病名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("疾病代码为:["+fm.all('DiseasCode').value+"]的疾病名称不存在，请确认!");
     }
	}	
}
function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.jsp");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}
function queryCustomerNo2()
{
	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
	var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}
function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  	//var strSql="select name from ldperson  where 1=1 "+getWherePart("CustomerNo","CustomerNo");
	  
  }
}
