<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHCustomFamilyDiseasSave.jsp
//程序功能：
//创建日期：2005-01-17 16:25:10
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
   <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHCustomFamilyDiseasSchema tLHCustomFamilyDiseasSchema   = new LHCustomFamilyDiseasSchema();
  OLHCustomFamilyDiseasUI tOLHCustomFamilyDiseasUI   = new OLHCustomFamilyDiseasUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHCustomFamilyDiseasSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLHCustomFamilyDiseasSchema.setFamilyCode(request.getParameter("FamilyCode"));
    tLHCustomFamilyDiseasSchema.setDiseasCode(request.getParameter("DiseasCode"));
    tLHCustomFamilyDiseasSchema.setExplai(request.getParameter("Explai"));
    tLHCustomFamilyDiseasSchema.setOperator(request.getParameter("Operator"));
    tLHCustomFamilyDiseasSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHCustomFamilyDiseasSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHCustomFamilyDiseasSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHCustomFamilyDiseasSchema.setModifyTime(request.getParameter("ModifyTime"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLHCustomFamilyDiseasSchema);
  	tVData.add(tG);
    tOLHCustomFamilyDiseasUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHCustomFamilyDiseasUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
