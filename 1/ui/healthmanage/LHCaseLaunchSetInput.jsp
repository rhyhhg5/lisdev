<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称： LHCaseLaunchSetInput.jsp
//程序功能：
//创建日期：2006-07-19 10:19:55
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<SCRIPT src="LHCaseLaunchSetInput.js"></SCRIPT>
	<%@include file="LHCaseLaunchSetInputInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="LHCaseLaunchSetSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <div style="display:''" id='submenu1'>  
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseInfoHead);">
    		</td>
    		 <td class= titleImg>
        		 服务事件类型信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divCaseInfoHead" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
			    <TD  class= title>
			      服务事件类型
			    </TD>
			    <TD  class= input>
			      <Input class= 'codename' style="width:40px" name=ServCaseType verify="服务事件类型|NOTNULL" ><Input class= 'codeno' style="width:120px"  name=ServCaseTypeName ondblclick="showCodeList('eventtype',[this,ServCaseType],[1,0],null,null,null,'1',160);" >
			    </TD>
			    <TD  class= title>
			      保单所属机构
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' style="width:50px" name=ComID verify="保单所属机构|NOTNULL"><Input class= 'code' style="width:100px" name=ComIDName verify="保单所属机构|NOTNULL"  ondblclick="return showCodeList('hmhospitalmng',[ComID,this],[1,0],null,null,null,1);" onkeyup="return showCodeListKey('hmhospitalmng',[ComID,this],[1,0],null,null,null,1);" >
			    </TD>
				<TD  class= title>
			      服务项目名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' style="width:50px" name=ServItemCode verify="服务项目名称|NOTNULL" ><Input class= 'code' style="width:100px" name=ServItemName ondblclick="return showCodeList('hmservitemcode',[ServItemCode,this],[0,1],null,fm.ServItemName.value,'ServItemName',1);" onkeyup="return showCodeListKey('hmservitemcode',[ServItemCode,this],[0,1],null,fm.ServItemName.value,'ServItemName',1);" verify="服务项目名称|NOTNULL" >
			    </TD>
		  	</TR>
		</table>
    </Div>
    
    
	<Div id="divCaseInfoTitle" style="display:''">
	    <table>
	    	<tr>
				<td>
	    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseInfo);">
	    	 	</td>
				<td class= titleImg>
		        	 事件启动信息
				</td>   		 
	    	</tr>
	    </table>
		<Div id="divCaseInfo" style="display:''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1>
						<span id="spanLHCaseLaunchGrid">
						</span> 
			    	</td>
				</tr>
			</table>
		</div>
	</div>
	<Div id= "divPage2" align=center style= "display: 'none' ">
		<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	<br><br>


	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="fmAction" name="fmAction">
	<Input class= 'common' type=hidden name=ManageCom >
	<Input class= 'common' type=hidden name=Operator >
	<Input class= 'common' type=hidden name=MakeDate >
	<Input class= 'common' type=hidden name=MakeTime >
  	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
</html>
