<%
//程序名称：LHCustomFamilyDiseasQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-17 16:25:10
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CustomerNo').value = "";
    fm.all('FamilyCode').value = "";
    fm.all('DiseasCode').value = "";

  }
  catch(ex) {
    alert("在LHCustomFamilyDiseasQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHCustomFamilyDiseasGrid();  
  }
  catch(re) {
    alert("LHCustomFamilyDiseasQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHCustomFamilyDiseasGrid;
function initLHCustomFamilyDiseasGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		
    iArray[0][3]=0;         		        
    iArray[0][4]="station";         		
    
                iArray[1]=new Array(); 
		iArray[1][0]="客户号码";   
		iArray[1][1]="30px";   
		iArray[1][2]=24;        
		iArray[1][3]=0;
		
		iArray[2]=new Array(); 
		iArray[2][0]="客户姓名";   
		iArray[2][1]="30px";   
		iArray[2][2]=6;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="亲属关系代码";   
		iArray[3][1]="40px";   
		iArray[3][2]=6;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="疾病代码";   
		iArray[4][1]="30px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		
		iArray[5]=new Array(); 
		iArray[5][0]="说明";   
		iArray[5][1]="180px";   
		iArray[5][2]=200;        
		iArray[5][3]=0;

    LHCustomFamilyDiseasGrid = new MulLineEnter( "fm" , "LHCustomFamilyDiseasGrid" ); 
    //这些属性必须在loadMulLine前
    
    LHCustomFamilyDiseasGrid.mulLineCount = 0;   
    LHCustomFamilyDiseasGrid.displayTitle = 1;
    LHCustomFamilyDiseasGrid.hiddenPlus = 1;
    LHCustomFamilyDiseasGrid.hiddenSubtraction = 1;
    LHCustomFamilyDiseasGrid.canSel = 1;
    LHCustomFamilyDiseasGrid.canChk = 0;
    //LHCustomFamilyDiseasGrid.selBoxEventFuncName = "showOne";

    LHCustomFamilyDiseasGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomFamilyDiseasGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
