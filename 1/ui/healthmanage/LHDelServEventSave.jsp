<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHDelServEventSave.jsp
//程序功能：
//创建日期：2006-07-04 18:07:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.health.*"%>
  
<%
	LHServCaseDefSchema tLHServCaseDefSchema=new LHServCaseDefSchema();
	LHServCaseRelaSet tLHServCaseRelaSet = new LHServCaseRelaSet();
    OLHServCaseDefUI tOLHServCaseDefUI   = new OLHServCaseDefUI();
	String tServItemNo[] = request.getParameterValues("LHCaseRelaGrid8");	                                       
	String tChk[] = request.getParameterValues("InpLHCaseRelaGridChk"); //参数格式=” Inp+MulLine对象名+Chk” 
	
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");
    tLHServCaseDefSchema.setServCaseType(request.getParameter("ServeEventStyle"));
    tLHServCaseDefSchema.setServCaseCode(request.getParameter("ServeEventNum"));
    tLHServCaseDefSchema.setServCaseName(request.getParameter("ServeEventName"));
    tLHServCaseDefSchema.setServCaseState(request.getParameter("ServCaseState"));
    tLHServCaseDefSchema.setOperator(request.getParameter("Operator"));
    tLHServCaseDefSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHServCaseDefSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHServCaseDefSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHServCaseDefSchema.setModifyTime(request.getParameter("ModifyTime"));
    tLHServCaseDefSchema.setManageCom(request.getParameter(tG.ManageCom));
    
    int tCount = tServItemNo.length;
    System.out.println("--------- tServItemNo.length: "+tCount);  
	for(int j = 0; j < tCount; j++)
	{
		if(tChk[j].equals("1"))  
		{
			LHServCaseRelaSchema tLHServCaseRelaSchema = new LHServCaseRelaSchema();
			tLHServCaseRelaSchema.setServCaseCode(request.getParameter("ServeEventNum"));
			tLHServCaseRelaSchema.setServItemNo(tServItemNo[j]);
			System.out.println("--------- ServCaseCode: "+request.getParameter("ServeEventNum")); 
			System.out.println("--------- ServItemNo: "+tServItemNo[j]); 
			tLHServCaseRelaSet.add(tLHServCaseRelaSchema);
		}
  	}
    
	try
	{
  		// 准备传输数据 VData
  		VData tVData = new VData();
  		tVData.add(tLHServCaseDefSchema);
  		tVData.add(tLHServCaseRelaSet);
  		tVData.add(tG);
    	tOLHServCaseDefUI.submitData(tVData,transact);

	}
	catch(Exception ex)
	{
	  Content = "操作失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
	}
  
	//如果在Catch中发现异常，则不从错误类中提取错误信息
  	if (FlagStr=="")
  	{
  		tError = tOLHServCaseDefUI.mErrors;
		if (!tError.needDealError())
  		{                          
  	  		Content = " 操作成功! ";
  	  		FlagStr = "Success";
  	  	}
  	  	else                                                                           
  	  	{
  	  		Content = " 操作失败，原因是:" + tError.getFirstError();
  	  		FlagStr = "Fail";
  	  	}
  	}
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
