<%
//程序名称：LDTestPriceMgtQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-06-11 15:49:24
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('HospitName').value = "";
    fm.all('RecordDate').value = "";

  }
  catch(ex) {
    alert("在LDTestPriceMgtQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDTestPriceMgtGrid();  
  }
  catch(re) {
    alert("LDTestPriceMgtQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDTestPriceMgtGrid;
function initLDTestPriceMgtGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="station";         //列名
    
    iArray[1]=new Array();
    iArray[1][0]="体检机构代码";         		//列名
    iArray[1][1]="30px";         		//列名
    iArray[1][3]=0;         				//列名
    
    iArray[2]=new Array();
    iArray[2][0]="体检机构名称";    //列名
    iArray[2][1]="120px";         	//列名
    iArray[2][3]=0;         				//列名
    
    iArray[3]=new Array();
    iArray[3][0]="记录时间";         		//列名
    iArray[3][1]="60px";         		//列名
    iArray[3][3]=0;         				//列名
    
    iArray[4]=new Array();                    
		iArray[4][0]="Serialno";        //列名
		iArray[4][1]="0px";         		//列名    
		iArray[4][3]=3;         				//列名    
		
    
    
    LDTestPriceMgtGrid = new MulLineEnter( "fm" , "LDTestPriceMgtGrid" ); 
    //这些属性必须在loadMulLine前

    LDTestPriceMgtGrid.mulLineCount = 0;   
    LDTestPriceMgtGrid.displayTitle = 1;
    LDTestPriceMgtGrid.hiddenPlus = 1;
    LDTestPriceMgtGrid.hiddenSubtraction = 1;
    LDTestPriceMgtGrid.canSel = 1;
    LDTestPriceMgtGrid.canChk = 0;
/*    LDTestPriceMgtGrid.selBoxEventFuncName = "showOne";
*/
    LDTestPriceMgtGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDTestPriceMgtGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}





function initSpecSecDiaGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="专科名称";   
		iArray[1][1]="150px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
		iArray[1][9]="专科名称|LEN<60"
		
		iArray[2]=new Array(); 
		iArray[2][0]="特色介绍";   
		iArray[2][1]="150px";   
		iArray[2][2]=20;        
		iArray[2][3]=1;
		
		iArray[3]=new Array(); 
		iArray[3][0]="SpecialClass";   
		iArray[3][1]="0px";   
		iArray[3][2]=20;        
		iArray[3][3]=3;
		iArray[3][14]="SpecSecDia";    
		
		iArray[4]=new Array();
		iArray[4][0]="FlowNo";
		iArray[4][1]="0px";         
		iArray[4][2]=20;            
		iArray[4][3]=3;             
		
    SpecSecDiaGrid = new MulLineEnter( "fm" , "SpecSecDiaGrid" ); 
    //这些属性必须在loadMulLine前

    SpecSecDiaGrid.mulLineCount = 0;   
    SpecSecDiaGrid.displayTitle = 1;
    SpecSecDiaGrid.hiddenPlus = 0;
    SpecSecDiaGrid.hiddenSubtraction = 0;
    SpecSecDiaGrid.canSel = 0;
    SpecSecDiaGrid.canChk = 1;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    SpecSecDiaGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDiseaseGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>