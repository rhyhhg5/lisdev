<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHIndivServAcceptInput.jsp
//程序功能：
//创建日期：2006-08-24 10:10:55
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="LHIndivServAcceptInput.js"></SCRIPT>
	<%@include file="LHIndivServAcceptInit.jsp"%>
</head>
<body  onload="initForm();" >
<form action=""  method=post name=fm target="fraSubmit">
<%@include file="../common/jsp/InputButton.jsp"%>
<%@include file="../common/jsp/OperateButton.jsp"%>
<div id= "divLHBaseInfoGrid" style="display:''">
    <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHBaseInfo);">
    	 	 </td>
    	   <td class= titleImg>
        	 基本信息
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHBaseInfo" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
			 <TD  class= title>
			      客户号码
			    </TD>
			    <TD  class= input>
			      <Input class= 'code' name=CustomerNo elementtype=nacessary  ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
			    </TD>
			    <TD  class= title>
			      客户姓名
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=CustomerName >
			    </TD>
			    <TD class= title >
          		 服务受理渠道
       	  </TD>   		 
				  <TD  class= input>
				    <Input class= 'codename' style="width:40px" verify="服务受理渠道|NOTNULL"  value="1" name=ServAccepChannel><Input class= 'codeno' style="width:120px" value="总公司呼叫中心"  name=ServAccepChannelName  ondblclick="showCodeList('lhacceptchannel',[this,ServAccepChannel],[1,0],null,null,null,'1',160);" codeClear(ServAccepChannel,ServAccepChannelName);">
				  </TD>
			</TR> 
			<TR  class= common>
			  <TD  class= title style="width:120">
           服务项目代码
        </TD>
        <TD  class= input>
           <Input class= 'code' name=ServItemCode  ondblclick="showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
        </TD>
         <TD  class= title style="width:120">
           服务项目名称
        </TD>
        <TD  class= input>
           <Input class= 'code' name=ServItemName  verify="服务项目名称|NOTNULL" ondblclick="showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
        </TD>
				<TD  class= title>
					服务受理时间
				</TD>
        <TD  class= input>
           <Input class= 'coolDatePicker' dateFormat="Short"  style="width:152px" name=ServAceptDate verify="服务受理时间|NOTNULL&DATE">
        </TD> 
			</TR> 
			
		</table>
		<table  class= common align='center' >
			<TR  class= common>
				<td>
           <input type= button class=cssButton name=CaseQuery style="width:130px" value="服务事件查询" OnClick="ServCaseQuery();">
       	</td>  
       	</tr>
     </table>
</Div>  
<div id="divLHServCaseInfoGrid" style="display:''">
    <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServCaseInfo);">
    	 	</td>
    	 <td class= titleImg>
        	 事件信息列表
       	</td>   		 
    	</tr>
   </table>  
	 <Div id="divLHServCaseInfo" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServCaseGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	 </div>
	 		<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
</div>
 <div id="divLHBespeakServAccdeptGrid"  style="display:''">
    <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHBespeakServAccdept);">
    	 	</td>
    	 <td class= titleImg>
        	 预约类服务受理信息
       	</td>   		 
    	</tr>
   </table> 
  <Div id="divLHBespeakServAccdept" style="display:''"> 
	<table  class= common align='center' >
		 <TR  class= common>
		     <TD  class= title>
	          受理类型
	       </TD>
	       <TD  class= input>
	         <Input class= 'codename' readonly style="width:45px" name=BespeakAcceptType><Input class= 'code' style="width:130px" name=BespeakAcceptTypeName ondblclick="showCodeList('lhbespeaktype', [this, BespeakAcceptType], [1,0], null, null, null, 1);">
	       </TD>
	       <TD  class= title>
	      </td>
	      <TD  class= input>
	      </td>
	      <TD  class= title>
	      </td>
	   </tr>
	   <TR  class= common>
		     <TD  class= title>
	          预约服务机构  
	       </TD>
	       <TD  class= input>
	         <Input class=codeno name=HospitCode   style='width:45'ondblclick="return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');"  onkeydown="  return showCodeListKey('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName')	; codeClear(HospitName,HospitCode); "><Input class= 'codename' name=HospitName  style='width:130' >
	       </TD>
	       <TD  class= title>
            预约时间要求
         </TD>
         <TD  class= input >
           <Input class= 'common' name=BespeakDateRequest  >
         </TD>
         <TD  class= title>
         </td>

     </tr>
     <TR  class= common>
      <TD  class= title>
        客户其他需求
      </TD>
      <TD  class= input colspan='6'>
        <textarea class= 'common' rows="3" cols="90" name=CusOtherRequest ></textarea>
      </TD>
     </TR>
  </table> 
 </div>
</div>
<div id="divLHNoteServAccdeptGrid"  style="display:'none'">
    <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHNoteServAccdept);">
    	 	</td>
    	  <td class= titleImg>
        	 短信类服务受理信息
       	</td>   		 
    	</tr>
    </table> 
  <Div id="divLHNoteServAccdept" style="display:''"> 
	 <table  class= common align='center' >
	  <TR  class= common>
		  <TD  class= title>
	      短信内容类型
	    </TD>
	    <TD  class= input>
	      <Input class= 'codename' readonly style="width:40px" name=NoteContentType ><Input class= 'code' style="width:120px" name=NoteContentTypeName ondblclick="showCodeList('lhnoteconttype', [this, NoteContentType], [1,0], null, null, null, 1);">
	    </TD>
	    <TD  class= title>
       短信发送方式
      </TD>
	    <TD  class= input>
	      <Input class= 'codename' readonly style="width:40px" name=NoteSendMode ><Input class= 'code' style="width:120px" name=NoteSendModeName ondblclick="showCodeList('lhnotesendmode', [this, NoteSendMode], [1,0], null, null, null, 1);">
	    </TD>
      <TD  class= title>
      </td>
      <TD  class= input>
      </td>
   </tr>
   <TR  class= common>
    <TD  class= title>
     短信具体内容
    </TD>
    <TD  class= input colspan='6'>
      <textarea class= 'common' rows="3" cols="85" name=NoteStyleContent ></textarea>
    </TD>
   </TR>
 </table> 
   <Div id="divLHNoteSendTime" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHNoteSendTime">
					</span> 
		    </td>
			</tr>
		</table>
	 </div> 
	<Div  id= "divLHTimeInfo" style= "display: ''">
		<table  class= common align='center'>
			<TR  class= common>
				<TD  class= title>
						发送开始日期
			  </TD>
        <TD  class= input>
             <Input name=SendStartDate class='coolDatePicker' dateFormat='short' style="width:142px"  > 
        </TD> 
        <TD  class= title>
						发送终止日期
			  </TD>
        <TD  class= input>
             <Input name=SendEndDate class='coolDatePicker' dateFormat='short' style="width:142px" > 
        </TD> 
        <TD  class= title>
	        具体时间
	      </TD>
	      <TD  class= input>
	        <Input class= 'codename' readonly style="width:40px" name=ReifierDate ><Input  class= 'code' style="width:120px" name=ReifierDateName ondblclick="showCodeList('lhmaterialdate', [this, ReifierDate], [1,0], null, null, null, 1,150);">
	      </TD>
		</TR> 
			<TR  class= common>
			 <TD  class= title>
         具体时刻
       </TD>
	     <TD  class= input>
	       <Input class= 'codename' readonly style="width:40px" name=ReifierTime ><Input class= 'code' style="width:120px" name=ReifierTimeName ondblclick="showCodeList('lhmaterialtime', [this, ReifierTime], [1,0], null, null, null, 1,150);">
	     </TD>
			 	<TD  class= title>
	       循环方式
	     </TD>
	     <TD  class= input>
	       <Input class= 'codename' readonly style="width:40px" name=CircleMode ><Input  class= 'code' style="width:120px" name=CircleModeName ondblclick="showCodeList('lhcirclemode', [this, CircleMode], [1,0], null, null, null, 1,160);">
	     </TD>
	     <TD  class= title>
        循环间隔
       </TD>
       <TD  class= input>
        <Input class= 'common' name=CircleDistance  >
       </TD>
		 </tr>
		 <TR  class= common>
			<TD  class= title>
       循环次数
      </TD>
      <TD  class= input>
       <Input class= 'common' name=CircleDegree  >
      </TD>
			<TD  class= title>
				循环截止日期
			</TD>
      <TD  class= input>
        <Input name=CircleEndDate class='coolDatePicker' dateFormat='short' style="width:142px" > 
      </TD> 
     </tr>
     <TR  class= common>
       <TD  class= title>
        客户其他需求
       </TD>
       <TD  class= input colspan='6'>
          <textarea class= 'common' rows="3" cols="90" name=CusOtherRequest2></textarea>
       </TD>
    </TR>
	  </table>	
 </div>
</div>
</div>
   <div id= "divLHOtherServAccdeptGrid" style="display:'none'">
    <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHOtherServAccdept);">
    	 	</td>
    	  <td class= titleImg>
        	 其他服务受理
       	</td>   		 
    	</tr>
    </table> 	
   <Div  id= "divLHOtherServAccdept" style= "display: ''">
   <table  class= common align='center'>
    <TR  class= common>
      <TD  class= title>
        客户服务需求
      </TD>
      <TD  class= input colspan='6'>
         <textarea class= 'common' rows="3" cols="85" name=CusServRequest></textarea>
      </TD>
      <TD  class= title>
      </td>
   </TR>
	</table>	
 </div>
</div>
<div  id= "divLHTaskSettingFGrid"  style="display:''">
    <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHTaskSetting);">
    	 	</td>
    	 <td class= titleImg>
        	 任务设置
       	</td>   		 
    	</tr>
    </table> 	
<Div  id= "divLHTaskSetting" style= "display: ''">
	 <table  class= common align='center'>
    <TR  class= common>
		    <TD  class= title style="width:120">
	            服务任务编号
	         	</TD>
				<TD  class= input>
		           <Input class= 'common' readonly name=ServTaskCode verify="服务任务编号|NOTNULL" ><!-- ondblclick="showCodeList('lhservtaskcode',[this,ServTaskName],[0,1],null,fm.ServTaskCode.value,'ServTaskCode','1',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhservtaskcode',[this,ServTaskName],[0,1],null,fm.ServTaskCode.value,'ServTaskCode','1',200);"   onkeydown="return showCodeListKey('lhservtaskcode',[this,ServTaskName],[0,1],null,fm.ServTaskCode.value,'ServTaskCode','1',200); ">-->
				</TD>
				<TD  class= title style="width:120">
		            服务任务名称
				</TD>
				<TD  class= input>
		           <Input class= 'common' readonly name=ServTaskName  ><!-- ondblclick="showCodeList('lhservtaskcode',[this,ServTaskCode],[1,0],null,fm.ServTaskName.value,'ServTaskName','1',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhservtaskcode',[this,ServTaskCode],[1,0],null,fm.ServTaskName.value,'ServTaskName','1',200);"   onkeydown="return showCodeListKey('lhservtaskcode',[this,ServTaskCode],[1,0],null,fm.ServTaskName.value,'ServTaskName','1',200);">-->
				</TD>
         <TD  class= title>
						计划完成时间
				 </TD>
         <TD  class= input>
             <Input name=ServFinDate class='coolDatePicker' dateFormat='short' style="width:142px" > 
          </TD> 
     </tr> 
    
  </table>
   <table  class= common align='center'>
   <tr style="width:50px" > 
     	<td >
     		<input type= button class=cssButton name=ServTaskSend   style="width:130px" value="服务任务发送" OnClick="ServCaseSend();">
       </td>
		 </tr> 
		</table>
</div>
</div>
		<input type=hidden id="fmtransact" name="fmtransact">
		<Input class= 'common' type=hidden name=ServAccepNo>
		<Input class= 'common' type=hidden  name=ServCaseCode>
		<input type=hidden id="fmAction" name="fmAction">
		<Input class= 'common' type=hidden name=TaskModelType>
		<Input class= 'common' type=hidden name=ModelTypeNo>
		<Input class= 'common' type=hidden name=ManageCom >
	  <Input class= 'common' type=hidden name=Operator >
	  <Input class= 'common' type=hidden name=MakeDate >
	  <Input class= 'common' type=hidden name=MakeTime >
	  <Input class= 'common' type=hidden name=ServItemNo >
	  <Input class= 'common' type=hidden name=ServTaskNO >
  	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
</html>
