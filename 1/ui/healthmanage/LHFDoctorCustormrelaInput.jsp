<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-17 15:00:30
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>            
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>          
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>         
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>       
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput2.js"></SCRIPT>     
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>          
  <SCRIPT src="LHFDoctorCustormrelaInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  
  <%@include file="LHFDoctorCustormrelaInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LHFDoctorCustormrelaSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 家庭医生服务客户对应关系表基本信息
       		 </td>   		 
    	</tr>
    </table>
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      医师姓名
    </TD>
    <TD  class= input>
      <Input class= 'code' name=FamilyDoctorName ondblclick="return showCodeList('lhdoctname',[this,FamilyDoctorNo],[0,1],null,fm.FamilyDoctorName.value,'DoctName');" verify="医师姓名|notnull&len<=20"><font color=red>*</font>
    </TD>
    <TD  class= title>
      医师代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FamilyDoctorNo readonly  verify="医师代码|NUM&len<=20"><!--ondblclick="return showCodeList('lhdoctname',[this,FamilyDoctorNo],[0,1],null,fm.FamilyDoctorNo.value,'DoctNo');"-->
    </TD>
    <TD  class= title>
      
    </TD>
    <TD  class= input>
      
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= 'code' name=Name  ondblclick=" showCodeList('hmldperson',[this,CustomerNo],[1,0],null,fm.Name.value,'Name',1) "  onkeyup="if( event.keyCode== 13 ) return showCodeList('hmldperson',[this,CustomerNo],[1,0],null,fm.Name.value,'Name',1);if( event.keyCode== 40 ) return showCodeListKey('hmldperson',[this,CustomerNo],[1,0],null,fm.Name.value,'Name',1)  " >
    </TD>
    <TD  class= title>
      客户号
    </TD>
    <TD  class= input>
      <Input class= 'code' name=CustomerNo  elementtype=nacessary  verify="客户号|NOTNULL&len<=10" ondblclick="return queryCustomerNo();"><font color=red>*</font>
    </TD>
    <TD  class= title>
      
    </TD>
    <TD  class= input>
     
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      起始时间
    </TD>
    <TD  class= input>
      <Input class= 'CoolDatePicker' dateFormat="Short"  name=FDSStart verify="起始时间|DATE">
    </TD>
    <TD  class= title>
      终止时间
    </TD>
    <TD  class= input>
      <Input class= 'CoolDatePicker' dateFormat="Short"  name=FDSEnd verify="终止时间|DATE">
    </TD>
    <TD  class= title>
      
    </TD>
    <TD  class= input>
      
    </TD>
  </TR>
</table>
<table  class= common style="display:'none'"> 
  <TR  class= common>
    <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD> 
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>                         
      <Input class= 'common' name=ModifyDate > 
    </TD>                                      
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
    <input type=hidden name=SerialNo>
  </TR> 
</table>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
