//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{	
  var i = 0;
  if( verifyInput2() == false ) return false;
  if( queryCustomerNo2() == false) return false;
  if(checkDate() == false) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHFDoctorCustormrela.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if(fm.all('FDSStart').value != "" && fm.all('FDSEnd').value != "")
	{//alert("1");
			if(compareDate(fm.all('FDSStart').value,fm.all('FDSEnd').value) == true)
			{
				alert("起始时间晚于终止时间");
				return false;
			}
	}
	if( verifyInput2() == false ) return false;
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHFDoctorCustormrelaQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function queryCustomerNo2()
{
	  var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	  var arrResult = easyExecSql(strSql);
	  
	  if (arrResult == null) 
	  {
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     return false;
    }
}
function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	
	{
	  var newWindow = window.open("../sys/LDPersonQuery.html");	  
	}
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	  var arrResult = easyExecSql(strSql);
	  
	  if (arrResult != null) 
	  {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
	      fm.all('Name').value = arrResult[0][1];
    }
    else
    {
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     return false;
    }
	}	
}



/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
        fm.all('FamilyDoctorNo').value= arrResult[0][0];
        fm.all('FamilyDoctorName').value= arrResult[0][1];
        fm.all('CustomerNo').value= arrResult[0][2];
        fm.all('Name').value= arrResult[0][3];
//        fm.all('Name').readOnly = true;
        fm.all('FDSStart').value= arrResult[0][4];
        fm.all('FDSEnd').value= arrResult[0][5];
        fm.all('MakeDate').value= arrResult[0][6];
        fm.all('MakeTime').value= arrResult[0][7];
        fm.all('SerialNo').value= arrResult[0][8];
	}
}               
        
function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.Name.value = arrResult[0][1];
  }
}

//function compareDate(DateOne,DateTwo)
//{
//	if(DateOne.length == 10)
//	alert();
//	
//	var OneMonth = DateOne.substring(5,DateOne.lastIndexOf ("-"));
//	var OneDay = DateOne.substring(DateOne.length,DateOne.lastIndexOf ("-")+1);
//	var OneYear = DateOne.substring(0,DateOne.indexOf ("-"));
//	
//	var TwoMonth = DateTwo.substring(5,DateTwo.lastIndexOf ("-"));
//	var TwoDay = DateTwo.substring(DateTwo.length,DateTwo.lastIndexOf ("-")+1);
//	var TwoYear = DateTwo.substring(0,DateTwo.indexOf ("-"));
//	
//	if (Date.parse(OneMonth+"/"+OneDay+"/"+OneYear) >
//	Date.parse(TwoMonth+"/"+TwoDay+"/"+TwoYear))
//	{ 
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}

function checkDate()
{
	var date1 = fm.all('FDSStart').value;
	var date2 = fm.all('FDSEnd').value;

	if(date1.length == 8)
	{date1 = modifydate(date1);}
	if(date2.length == 8)    
	{date2 = modifydate(date2); }   
	
	if(compareDate(date1,date2) == 1) 
	{                                                                        
		alert("起始时间应早于终止时间");                                         
		return false;                                                          
	}
} 
  
  
  