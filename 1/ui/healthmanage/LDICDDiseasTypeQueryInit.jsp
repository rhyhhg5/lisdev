<%
//程序名称：LDICDDiseasTypeQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-03 20:24:53
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('DiseasTypeLevel').value = "";
    fm.all('ICDBeginCode').value = "";
    fm.all('ICDEndCode').value = "";
    fm.all('DiseasType').value = "";
  }
  catch(ex) {
    alert("在LDICDDiseasTypeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDICDDiseasTypeGrid();  
  }
  catch(re) {
    alert("LDICDDiseasTypeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDICDDiseasTypeGrid;
function initLDICDDiseasTypeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="代码分类等级";         		//列名
    iArray[1][1]="30px";         		//列名
    iArray[1][3]=0;         		//列名
    iArray[1][2]=0;   
    
    iArray[2]=new Array();
    iArray[2][0]="分类起始代码";         		//列名
    iArray[2][1]="30px";         		//列名
    iArray[2][3]=0;         		//列名
    iArray[2][2]=0; 
    
    iArray[3]=new Array();
    iArray[3][0]="分类终止代码";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=0;         		//列名
    iArray[3][2]=0;  
    
    iArray[4]=new Array();
    iArray[4][0]="疾病分类名称";         		//列名
    iArray[4][1]="100px";         		//列名
    iArray[4][3]=0;         		//列名
    iArray[4][2]=0;   
    
    
    LDICDDiseasTypeGrid = new MulLineEnter( "fm" , "LDICDDiseasTypeGrid" ); 
    //这些属性必须在loadMulLine前

    LDICDDiseasTypeGrid.mulLineCount = 0;   
    LDICDDiseasTypeGrid.displayTitle = 1;
    LDICDDiseasTypeGrid.hiddenPlus = 1;
    LDICDDiseasTypeGrid.hiddenSubtraction = 1;
    LDICDDiseasTypeGrid.canSel = 1;
    LDICDDiseasTypeGrid.canChk = 0;
    //LDICDDiseasTypeGrid.selBoxEventFuncName = "showOne";

    LDICDDiseasTypeGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDICDDiseasTypeGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
