<%
//程序名称：LHServCaseQueryInput.jsp
//程序功能：功能描述
//创建日期：2006-07-04 15:10:27
//创建人  ：ctrHTML
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHServCaseQueryInput.js"></SCRIPT> 
  <%@include file="LHServCaseQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
  <table  class= common align='center' >
  <TR  class= common>
  	<TD  class= title>
	      	服务事件类型
	    </TD>
	    <TD  class= input>
	    	  <Input type=hidden name=ServeEventStyle>
	     	  <Input class= 'code' name=ServeEventStyle_ch verify="服务事件类型|notnull" CodeData= "0|^个人服务事件|1^集体服务事件|2^费用结算事件|3" ondblClick= "showCodeListEx('',[this,ServeEventStyle],[0,1],null,null,null,1);"> 
	    </TD> 
	    <TD  class= title>
	      	服务事件编号
	    </TD>
	    <TD  class= input>
	     	  <Input class= 'common' name=ServeEventNum verify="服务事件编号|notnull" > 
	    </TD> 
	    <TD  class= title>
	      	服务事件名称
	    </TD>
	    <TD  class= input>
	     	  <Input class= 'common' name=ServeEventName verify="服务事件名称|notnull" > 
	    </TD> 
  	</TR>
  </table>
  
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
  
  <Div id="divLHCustomInHospital1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHServEventGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	 </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>