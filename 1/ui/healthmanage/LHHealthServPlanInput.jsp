<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHHealthServPlanInput.jsp
//程序功能：个人服务计划内容定义(页面显示)
//创建日期：2006-03-20 10:09:30
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHHealthServPlanInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHHealthServPlanInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHHealthServPlanSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHHealthServPlanGrid1);">
    		</td>
    		<td class= titleImg>
        		 个人服务计划内容定义
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHHealthServPlanGrid1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      服务计划代码
    </TD>
    <TD  class= input>
     
      <Input class= 'code' name=Riskcode  verify="服务计划代码|NOTNULL&len<=50" ondblclick="showCodeList('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode');"   onkeydown="return showCodeListKey('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode'); codeClear(Riskname,Riskcode);">
    </TD>
    <TD  class= title>
      服务计划名称
    </TD>
    <TD  class= input>
      <Input class= 'code' name=Riskname  verify="个人服务计划名称|NOTNULL&len<=300" ondblclick="showCodeList('hmriskcode',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskcode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmriskcode',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskcode');"   onkeydown="return showCodeListKey('hmriskcode',[this,Riskcode],[0,1],null,fm.Riskcode.value,'Riskcode'); codeClear(Riskname,Riskcode);">
    </TD>
    <TD class= title>
    	服务计划档次
    </TD>
    <TD  id=tdServPlanLevelCode class= input>
    	<Input class = codeno name= ServPlanLevelCode verify="服务计划档次|NOTNULL&len<=10" ondblclick="showCodeList('hmservplanlevel',[this,ServPlanLevelName],[1,0],null,fm.ServPlanLevelName.value,'ServPlanLevelName','',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservplanlevel',[this,ServPlanLevelName],[1,0],null,fm.ServPlanLevelName.value,'ServPlanLevelName');"   onkeydown="return showCodeListKey('hmservplanlevel',[this,ServPlanLevelName],[1,0],null,fm.ServPlanLevelName.value,'ServPlanLevelName'); codeClear(ServPlanLevelName,ServPlanLevelCode);"><Input class= 'codename' name=ServPlanLevelName >
   </TD>   
  </TR>
  <TR  class= common>
  	 <TD  class= title>
      机构属性标识
    </TD>
    <TD  class= input>
      <Input  class = codeno  name=ComIDCode verify="机构属性标识|NOTNULL&len<=10" ondblclick="showCodeList('hmhospitalmng',[this,ComIDName],[1,0],null,fm.ComIDName.value,'ComIDName','1',260);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmhospitalmng',[this,ComIDName],[1,0],null,fm.ComIDName.value,'ComIDName');" onkeydown="return showCodeListKey('hmhospitalmng',[this,ComIDName],[1,0],null,fm.ComIDName.value,'ComIDName'); codeClear(ComIDName,ComIDCode);"><Input class= 'codename' name=ComIDName >	
    </TD>
    <TD  class= title></TD>
    <TD  class= input></TD>
     <TD  class= title>
      计划类型
    </TD>
    <TD  class= input>
      <Input  class = codeno  name=PlanType CodeData= "0|^1|按档次^2|按保费" verify="计划类型|NOTNULL&len=1" ondblclick="showCodeListEx('hmPlanType',[this,PlanTypeName],[0,1],null,null,null,'1',200);" onkeyup=" if(event.keyCode ==13)  return showCodeListEx('hmPlanType',[this,PlanTypeName],[0,1],null,null,null,1,200);" onkeydown="return showCodeListKeyEx('hmPlanType',[this,PlanTypeName],[0,1],null,null,null,1,200); "><Input class= 'codename' name=PlanTypeName >	
    </TD>
   </TR>
</table>
    </Div>

     
	<table>
		<tr>
			<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHHealthServPlanGrid);">
			</td>
			<td class=titleImg>
		  			 信息
			</td>
		</tr>
	</table>
		  <!-- 信息（列表） -->
	<Div id="divLHHealthServPlanGrid" style="display:''">   
		<Div id=divLHFeeGrid style="display:'none'">
			<table class=common>
				<tr class=common>
		  			<td text-align:left colSpan=1>
						<span id="spanLHFeeGrid">
						</span> 
			    	</td>
				</tr>
			</table>
		</div>
		<Div style="display:''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1>
		  					<span id="spanLHHealthServPlanGrid">
		  					</span> 
					</td>
				</tr>
			</table>
		</div>
	</Div>
		<div  style="display: 'none'">
			  <table class=common>
			  <TR  class= common>
			    <TD  class= title>
			      执行者
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=linkman >
			    </TD>
			    <TD  class= title>
			      责  任
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=Phone >
			    </TD>
			    <TD >
			    </TD>
			  </TR>
			  </table>
		</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type = hidden name = "Operator">
    <input type = hidden name = "MakeDate">
    <input type = hidden name = "MakeTime">
    <input type = hidden name = "ModifyDate">
    <input type = hidden name = "ModifyTime">
    <input type = hidden name = ServPlayType>
    <input type = hidden name = ContraItemNo>
    <input type = hidden name = FeeUpLimit>
    <input type = hidden name = FeeLowLimit>
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
