<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHGroupContSave.jsp
//程序功能：
//创建日期：2005-03-19 15:05:48
//创建人  ：CrtHtml程序创建
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-03 10:38:48
// 更新原因/内容: 插入新的字段
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHGroupContSchema tLHGroupContSchema   = new LHGroupContSchema();
  LHGroupContUI tLHGroupContUI   = new LHGroupContUI();
  
  LHContItemSet tLHContItemSet = new LHContItemSet();		//合同责任信息
  
    String tContraType[] = request.getParameterValues("GroupContGrid6");					//MulLine的列存储数组
		String tDutyItemCode[] = request.getParameterValues("GroupContGrid7");					//MulLine的列存储数组
		
//		String tDutyItemName[] = request.getParameterValues("GroupContGrid6");					//MulLine的列存储数组
		String tDutyState[] = request.getParameterValues("GroupContGrid8");         //MulLine的列存储数组
		
//	String tDutyExolai[] = request.getParameterValues("GroupContGrid3");         //MulLine的列存储数组
//	String tDoctNo[] = request.getParameterValues("GroupContGrid4");         //MulLine的列存储数组
		
		String tDutylinkman[] = request.getParameterValues("GroupContGrid4");    //MulLine的列存储数组
		String tDutyContact[] = request.getParameterValues("GroupContGrid5");      //MulLine的列存储数组
		
		//String tFeeExplai[] = request.getParameterValues("GroupContGrid7");        //MulLine的列存储数组
		String tContraItemNo[] = request.getParameterValues("GroupContGrid10");
		
		
								
  
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
    tLHGroupContSchema.setContraNo(request.getParameter("ContraNo"));
    tLHGroupContSchema.setHospitCode(request.getParameter("HospitCode"));
    tLHGroupContSchema.setContraName(request.getParameter("ContraName"));
    tLHGroupContSchema.setIdiogrDate(request.getParameter("IdiogrDate"));
    tLHGroupContSchema.setContraBeginDate(request.getParameter("ContraBeginDate"));
    tLHGroupContSchema.setContraEndDate(request.getParameter("ContraEndDate"));
    tLHGroupContSchema.setContraState(request.getParameter("ContraState"));
    



			tLHGroupContSchema.setOperator(request.getParameter("Operator"));    
    	tLHGroupContSchema.setMakeDate(request.getParameter("MakeDate"));    
    	tLHGroupContSchema.setMakeTime(request.getParameter("MakeTime"));    
  //	tLHGroupContSchema.setModifyDate(request.getParameter("ModifyDate"));
  // 	tLHGroupContSchema.setModifyTime(request.getParameter("ModifyTime"));  
    
    int LHGroupContCount = 0;
		if(tDutyItemCode != null)
		{	
			LHGroupContCount = tDutyItemCode.length;
		}	
		System.out.println(" LHGroupContCount is : "+LHGroupContCount);
		
		for(int i = 0; i < LHGroupContCount; i++)
		{
			
				LHContItemSchema tLHContItemSchema = new LHContItemSchema();
			
				  tLHContItemSchema.setContraNo(request.getParameter("ContraNo"));
				  tLHContItemSchema.setContraFlag(request.getParameter("LHFlag"));
				  tLHContItemSchema.setContraType(tContraType[i]);	
				  tLHContItemSchema.setDutyItemCode(tDutyItemCode[i]);	
				  tLHContItemSchema.setContraItemNo(tContraItemNo[i]);	
				  tLHContItemSchema.setDutyState(tDutyState[i]);
			   	tLHContItemSchema.setDutylinkman(tDutylinkman[i]);
		      tLHContItemSchema.setDutyContact(tDutyContact[i]);
		      
                              
				tLHContItemSchema.setOperator(request.getParameter("Operator"));
				tLHContItemSchema.setMakeDate(request.getParameter("MakeDate"));
				tLHContItemSchema.setMakeTime(request.getParameter("MakeTime"));
				tLHContItemSchema.setModifyDate(request.getParameter("ModifyDate"));
				tLHContItemSchema.setModifyTime(request.getParameter("ModifyTime"));
                              
			                        
				tLHContItemSet.add(tLHContItemSchema);
			                        
		}
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLHGroupContSchema);
	// tVData.add(tLHContItemSchema);
  	tVData.add(tLHContItemSet);
  	tVData.add(tG);
    
    tLHGroupContUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHGroupContUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
		var FlagStr="<%=FlagStr%>";
		if(FlagStr!="Fail")
    {
	     var transact = "<%=transact%>";
	     if(transact!="DELETE||MAIN")
	     {
	   	    var arrResult = new Array();
	        arrResult[0] =new Array();
	        arrResult[0][0] = parent.fraInterface.fm.all("ContraNo").value;
	        //alert(arrResult[0][0]);
	        parent.fraInterface.afterQuery0(arrResult); 
	     }
	  }
</script>
</html>