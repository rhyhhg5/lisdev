<%
//程序名称：LHQuesKineInit.jsp
//程序功能：
//创建日期：2006-09-20 10:16:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期    : 
// 更新原因/内容: 
%>
<!--用户校验类-->                           
<script language="JavaScript">
	var custid = "<%=custid%>"; //1-核心系统客户；0-准客户
	var serviceItemNo = "<%=serviceItemNo%>";
	var custtype = "<%=custtype%>";//6为从一般服务任务管理进入1-CRM传入，客户为核心系统客户；0-CRM传入，客户为准客户
	var ServTaskNo = "<%=ServTaskNo%>";
function initInpBox()
{ 
	if(custtype!=""&&custtype!=null&&custtype!="null")
	{
	   fm.all('custtype').value = custtype;//判断入口标记
	}
	try
	{                                   
		if(custtype=="1")
		{
			  fm.all('CustomerNo').value = custid;
			  var strSql = "select Name from LDPerson where CustomerNo='" + custid +"'";
    	  var arrResult = easyExecSql(strSql);
		    if (arrResult != null) 
		    {
      			fm.CustomerName.value = arrResult[0][0];
      	} 
      	else 
      	{
      			alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
    		}
    		Source_Js="1";//CRM传入，客户为核心系统客户可以查询出相关的客户信息
    		fm.all('ServItemNo').value = serviceItemNo;
		}
		else if(custtype=="0")
		{
			fm.all('CustomerNo').value = custid;
			alert("代码为:["+fm.all('CustomerNo').value+"]的客户为准客户，请填写您的姓名!");
			Source_Js="0";//CRM传入，客户为准客户不能查询出相关的客户信息
			fm.all('ServItemNo').value = serviceItemNo;
		}
		else
		{
			Source_Js="6";//健管人员内部录入，客户为核心系统客户从实施管理的一般服务任务管理进入
			//alert(Source_Js);
		}	
	}
	catch(ex)
	{
		alert("在LHQuesKineInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}
                                       
function initForm()
{
  try
  {
     initInpBox();
     divManInfo.style.display="none";
     fm.all('03010003').className="gray2";
     fm.all('03010003').readOnly=true;
     fm.all('03010008').readOnly=true;//高血压的患者的药物名称
     fm.all('03010008').className = "gray2"; 
     fm.all('07010001').className = "gray2"; 
     fm.all('07010003').className = "gray2"; 
     fm.all('07010005').className = "gray2";      
     fm.all('07010007').className = "gray2"; 
     fm.all('07010009').className = "gray2"; 
     fm.all('07010011').className = "gray2"; 
     fm.all('07010013').className = "gray2"; 
     fm.all('07010015').className = "gray2"; 
     fm.all('07010017').className = "gray2"; 
     fm.all('07010019').className = "gray2"; 
     fm.all('07010021').className = "gray2"; 
     fm.all('07010023').className = "gray2"; 
     fm.all('07010025').className = "gray2"; 
     fm.all('07010027').className = "gray2"; 
     fm.all('07010029').className = "gray2"; 
     fm.all('07010031').className = "gray2"; 
     fm.all('07010033').className = "gray2"; 
     fm.all('07010035').className = "gray2"; 
     fm.all('07010037').className = "gray2"; 
     fm.all('07010039').className = "gray2"; 
     fm.all('07010069').className = "gray2"; 
     fm.all('07010071').className = "gray2"; 
     fm.all('07010041').className = "gray2"; 
     fm.all('07010043').className = "gray2"; 
     fm.all('07010045').className = "gray2"; 
     fm.all('07010047').className = "gray2"; 
     fm.all('07010049').className = "gray2"; 
     fm.all('07010051').className = "gray2"; 
     fm.all('07010053').className = "gray2"; 
     fm.all('07010055').className = "gray2"; 
     fm.all('07010057').className = "gray2"; 
     fm.all('07010059').className = "gray2"; 
     fm.all('07010061').className = "gray2"; 
     fm.all('07010063').className = "gray2"; 
     fm.all('07010066').className = "gray2"; 
     fm.all('07010067').className = "gray2";
     fm.all('02020002').className = "gray2";  
     fm.all('02020006').className = "gray2"; 
     fm.all('02020007').className = "gray2"; 
     fm.all('02020003').className = "gray2"; 
     fm.all('02020008').className = "gray2"; 
     fm.all('02020009').className = "gray2"; 
     fm.all('02020010').className = "gray2"; 
     fm.all('02040006').className = "gray2"; 
     fm.all('02040008').className = "gray2"; 
     fm.all('02020002').readOnly=true;
	 fm.all('02020006').readOnly=true;
	 fm.all('02020007').readOnly=true;
	 fm.all('02020009').readOnly=true;
	 fm.all('02020003').readOnly=true;
	 fm.all('02020008').readOnly=true;
	 fm.all('02020009').readOnly=true;
	 fm.all('02020010').readOnly=true;
	 fm.all('02040006').readOnly=true;
	 fm.all('02040008').readOnly=true;
		 
	 fm._03010001_1.disabled = true; 
	 fm._03010001_2.disabled = true; 
     //糖尿病患者，是否有人在40 岁以前?
     fm._01030001_1.disabled = true; 
     fm._01030001_2.disabled = true; 
     fm._01030001_3.disabled = true; 
     //冠心病患者，是否有人在50岁以前
     fm._01040002_1.disabled = true; 
     fm._01040002_2.disabled = true; 
     fm._01040002_3.disabled = true; 
     //中风患者，是否有人在60岁以前
     fm._01050002_1.disabled = true; 
     fm._01050002_2.disabled = true; 
     fm._01050002_3.disabled = true; 
     //吸烟情况
     fm._02020011_1.disabled=false;
     fm._02020011_2.disabled=false;
     fm._02020012_1.disabled=true;
     fm._02020012_2.disabled=true;
     fm._02020012_3.disabled=true;
     //锻炼方式
     fm._02040007_1.disabled=true;
     fm._02040007_2.disabled=true;
     fm._02040007_3.disabled=true;
     fm._02040007_4.disabled=true;
     fm._02040007_5.disabled=true;
     fm._02040007_6.disabled=true;
     //alert(custtype);
     if(custtype=="6")//从服务任务实施管理进入来的(即从一般服务任务管理进入)
     {
     	   fm.all('custtype').value=custtype;
     	   fm.all('ServTaskNo').value=ServTaskNo;
     	   var arrResult = new Array();
     	   var sqlRela="select h.TaskExecNo ,h.ServCaseCode,h.ServTaskCode,h.ServItemNo"
     	              +" from LHTaskCustomerRela h where h.ServTaskNo='"+ ServTaskNo+"'";
			   var arrResult = easyExecSql(sqlRela);
			   if(arrResult != "null" && arrResult!=null && arrResult!="")
			   {
			   	  fm.all('TaskExecNo').value=arrResult[0][0];
			   	  fm.all('ServCaseCode').value=arrResult[0][1];
			   	  fm.all('ServTaskCode').value=arrResult[0][2];
			   	  fm.all('ServItemNo').value=arrResult[0][3];
			   }
			   strsql="  select  b.CustomerNo,b.ContNo,b.ServPlanNo ,b.CustomerName "
	             +"    from LHServCaseRela b , LHTaskCustomerRela m"
	             +"  where  m.ServCaseCode=b.ServCaseCode and m.ServItemNo=b.ServItemNo"
	             +" and m.TaskExecNo='"+fm.all('TaskExecNo').value +"'";
	       //alert(strsql);
	       var arrRelaInfo = new Array();
	       var arrRelaInfo = easyExecSql(strsql);
	       //alert(arrRelaInfo);
	       if(arrRelaInfo != "null" && arrRelaInfo!=null && arrRelaInfo!="")
			   {
			   	  fm.all('CustomerNo').value=arrRelaInfo[0][0];
			   	  fm.all('ContNo').value=arrRelaInfo[0][1];
			   	  fm.all('ServPlanNo').value=arrRelaInfo[0][2];
			   	  fm.all('CustomerName').value=arrRelaInfo[0][3];
			   }
	        var mulNoSql = "select a.CusRegistNo"
		 		  				+" from LHQuesImportMain a where a.ServItemNo  = '"+ fm.all('ServItemNo').value +"' ";
          //alert(mulNoSql);
          var mainNoRest=easyExecSql(mulNoSql);//服务项目代码对应的客户登记号码
          //alert(mainNoRest);
          if(mainNoRest != "null" && mainNoRest!=null && mainNoRest!="")
          {
          	fm.all('CusRegistNo').value=mainNoRest[0][0];
          	queryClick();
          }
          else
          {
              var BirSql = "select a.Sex,a.Birthday ,a.OccupationCode from ldperson a where a.Customerno='"+ fm.all('Customerno').value +"'";
              //alert(BirSql);
              var BirRest=new Array;
              BirRest=easyExecSql(BirSql);//性别，出生日期
              //alert(BirRest);
              if(BirRest!=""&&BirRest!=null&&BirRest!="null")
              {
              	fm.all('BirthDate').value=BirRest[0][0];
              	if(BirRest[0][0]=="0")
              	{
              		fm.sex[0].checked=true;
              		divManInfo.style.display="";
		              divWomanInfo.style.display="none";
              	}
              	if(BirRest[0][0]=="1")
              	{
              		fm.sex[1].checked=true;
              		divWomanInfo.style.display="";
		              divManInfo.style.display="none";
              	}
              	fm.all('BirthDate').value=BirRest[0][1];
              	//alert(BirRest[0][2]);
              	if(BirRest[0][2]!=""&&BirRest[0][2]!=null&&BirRest[0][2]!="null")
              	{
              		  fm.all('08010003').value=BirRest[0][2];
              		  var occSql = " select trim(OccupationName)||'-'||workname "
		 						                +" from LDOccupation a where  a.OccupationCode = '"+ fm.all('08010003').value +"' ";
     	              var occRest=easyExecSql(occSql);
		                if(occRest!=""&&occRest!=null&&occRest!="null")
		                {
                           fm.all('_08010003_1').value=occRest[0][0];
                    }
              	}
              }
              
           }
           
     }
     if(custtype=="1")//从服务任务实施管理进入来的(即从一般服务任务管理进入)
     {
      	 var mulNoSql = "select a.CusRegistNo"
		 						+" from LHQuesImportMain a where a.ServItemNo  = '"+ fm.all('ServItemNo').value +"' ";
         //alert(mulNoSql);
         var mainNoRest=easyExecSql(mulNoSql);
         //alert(mainNoRest);
         if(mainNoRest != "null" && mainNoRest!=null && mainNoRest!="")
         {
        	  fm.all('CusRegistNo').value=mainNoRest[0][0];
        	  queryClick();
         }
         else
         {
         	    var BirSql = "select a.Sex,a.Birthday from ldperson a where a.Customerno='"+ fm.all('Customerno').value +"'";
              //alert(BirSql);
              var BirRest=new Array;
              BirRest=easyExecSql(BirSql);//性别，出生日期
              //alert(BirRest);
              if(BirRest!=""&&BirRest!=null&&BirRest!="null")
              {
              	fm.all('BirthDate').value=BirRest[0][0];
              	if(BirRest[0][0]=="0")
              	{
              		fm.sex[0].checked=true;
              		divManInfo.style.display="";
		              divWomanInfo.style.display="none";
              	}
              	if(BirRest[0][0]=="1")
              	{
              		fm.sex[1].checked=true;
              		divWomanInfo.style.display="";
		              divManInfo.style.display="none";
              	}
              	fm.all('BirthDate').value=BirRest[0][1];
              }
         }
     }
      //alert(custtype);
     if(custtype=="0")
     {
      	 var mulNoSql = "select a.CusRegistNo"
		 						+" from LHQuesImportMain a where a.ServItemNo  = '"+ fm.all('ServItemNo').value +"' ";
         //alert(mulNoSql);
         var mainNoRest=easyExecSql(mulNoSql);
         //alert(mainNoRest);
         if(mainNoRest != "null" && mainNoRest!=null && mainNoRest!="")
         {
        	 fm.all('CusRegistNo').value=mainNoRest[0][0];
        	 queryClick();
         }
         else
         {
         	    var BirSql = "select a.Sex,a.Birthday from ldperson a where a.Customerno='"+ fm.all('Customerno').value +"'";
              //alert(BirSql);
              var BirRest=new Array;
              BirRest=easyExecSql(BirSql);//性别，出生日期
              //alert(BirRest);
              if(BirRest!=""&&BirRest!=null&&BirRest!="null")
              {
              	fm.all('BirthDate').value=BirRest[0][0];
              	if(BirRest[0][0]=="0")
              	{
              		fm.sex[0].checked=true;
              		divManInfo.style.display="";
		              divWomanInfo.style.display="none";
              	}
              	if(BirRest[0][0]=="1")
              	{
              		fm.sex[1].checked=true;
              		divWomanInfo.style.display="";
		              divManInfo.style.display="none";
              	}
              	fm.all('BirthDate').value=BirRest[0][1];
              }
         }
     }
     var mulSql = "select a.WhethAchieve"
		 						+" from LHQuesImportMain a where a.CusRegistNo  = '"+ fm.all('CusRegistNo').value +"' ";
     //alert(mulSql);
     var mainRest=easyExecSql(mulSql);
     //alert(mainRest);
     if(mainRest=="1"||mainRest==""||mainRest==null||mainRest=="null")
     {
       	fm.all('modifyButton').disabled=false;//保存之后可再改
        fm.all('saveButton').disabled=false;
     }
     else
     {
        fm.all('modifyButton').disabled=true;//提交之后不可再改
        fm.all('saveButton').disabled=true;
      }
      //var d = new Date();
	    //var h = d.getYear();
	    //var m = d.getMonth(); 
	    //var day = d.getDate();  
	    //var Date1;       
	    //if(h<10){h = "0"+d.getYear();}  
	    //if(m<9){ m++; m = "0"+m;}
	    //else{m++;}
	    //if(day<10){day = "0"+d.getDate();}
	    //Date1 = h+"-"+m+"-"+day;
	    //fm.all('BirthDate').value=Date1;
  }
  catch(re)
  {
    alert("LHQuesKineInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}    
</script>
  