<%
//程序名称：LHManaStandProQueryInput.jsp
//程序功能：功能描述
//创建日期：2006-06-11 20:28:48
//创建人  ：郭丽颖
//更新记录： 对新字段内容的操作
// 更新人 : 
// 更新日期 : 
// 更新原因/内容: 
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHManaStandProQueryInput.js"></SCRIPT> 
  <%@include file="LHManaStandProQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLHStaQuOptionGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      问题(分类)代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StandCode style="width:160px"   verify="健康问题(分类)代码|notnull">
    </TD>
    <TD  class= title>
      问题(分类)名称(问题内容)
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StandContent >
    </TD>
    <TD class= title>
    	代码分类等级
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=StandLevel><Input class= 'codeno' style="width:120px"  name=StandLevelName  ondblclick="showCodeList('lhcodelabelgrade',[this,StandLevel],[1,0],null,null,null,'1',130);" codeClear(StandLevel,StandLevelName);">
		</TD>   
  </TR>
  <TR  class= common>
  	 <TD  class= title>
      标准单位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StandUnit >
    </TD>
  
     <TD  class= title>
      问题所属问卷
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=QuerAffiliated><Input class= 'codeno' style="width:120px"  name=QuerAffiliatedName  ondblclick="showCodeList('lhincludeques',[this,QuerAffiliated],[1,0],null,null,null,'1',200);" codeClear(QuerAffiliated,QuerAffiliatedName);">
		</TD> 
		<TD class= title>
    	问题状态
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=QuesStauts><Input class= 'codeno' style="width:120px"  name=QuesStautsName  ondblclick="showCodeList('lhquestand',[this,QuesStauts],[1,0],null,null,null,'1',130);" codeClear(QuesStauts,QuesStautsName);">
		</TD>  
   </TR>
   <TR  class= common>
    	<TD class= title>
    	问题答案类型
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=QuerResType><Input class= 'codeno' style="width:120px"  name=QuerResTypeName  ondblclick="showCodeList('lhqueresultype',[this,QuerResType],[1,0],null,null,null,'1',130);" codeClear(QuerResType,QuerResTypeName);">
		</TD>   
   	<TD class= title>
    	问题附属信息
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ResuAffiInfo >
    </TD> 
    <TD  class= title>
      问题类型
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=StandQuerType><Input class= 'codeno' style="width:120px"  name=StandQuerTypeName  ondblclick="showCodeList('lhquesrestype',[this,StandQuerType],[1,0],null,null,null,'1',130);" codeClear(StandQuerType,StandQuerTypeName);">
		</TD> 
    </tr>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT class =cssbutton VALUE="查询"   TYPE=button   class=common onclick="easyQueryClick();">
        <INPUT class =cssbutton VALUE="返回"   TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHStaQuOptionGrid1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHStaQuOptionGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
