<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHEvalueMgtDownSave.jsp
//程序功能：
//创建日期：2006-10-23 15:59:51
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 
<%
  //接收信息，并作校验处理。
  //输入参数
  LHEvalueMgtDownBL tLHEvalueMgtDownBL   = new LHEvalueMgtDownBL();
  LHEvalueExpDetailSet tLHEvalueExpDetailSet = new LHEvalueExpDetailSet();//健康评估导出明细表
  LHEvalueExpDetailXls tLHEvalueExpDetailXls = new LHEvalueExpDetailXls();//要提交类文件的名称
  LHEvalueExpMainSet tLHEvalueExpMainSet = new LHEvalueExpMainSet();//健管评估导出中间主表
  String tCusEvalCode[] = request.getParameterValues("LHEvalueMgtGrid13");           //客户评估号码
  String tChk[] = request.getParameterValues("InpLHEvalueMgtGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  

   //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  int LHEvalueExpMainCount = 0;
		if(tCusEvalCode != null)
		{	
			LHEvalueExpMainCount = tCusEvalCode.length;
		}	
		System.out.println(" LHEvalueExpMainCount is : "+LHEvalueExpMainCount);
		for(int i = 0; i < LHEvalueExpMainCount; i++)
		{
		     LHEvalueExpMainSchema tLHEvalueExpMainSchema = new LHEvalueExpMainSchema();
		     tLHEvalueExpMainSchema.setCusEvalCode(tCusEvalCode[i]);	 //客户评估号码
		     if(tChk[i].equals("1"))  
         {        
             tLHEvalueExpMainSet.add(tLHEvalueExpMainSchema);   
         }
    }
  try
  {
  // 准备传输数据 VData
       String sysPath = application.getRealPath("/")+"/" ;
       tLHEvalueExpDetailXls.DownloadSubmit(tLHEvalueExpMainSet,sysPath);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  String FullPath ="./"+tLHEvalueExpDetailXls.getResult();
  if (FullPath!="")
  {
  	System.out.println("导出数据成功");
  }
  else
	{
	 System.out.println("导出数据失败");
	}
//如果在Catch中发现异常，则不从错误类中提取错误信息
  
  if (FlagStr=="")
  {
    tError = tLHEvalueMgtDownBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = "下载数据成功!";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	  var transact = "<%=transact%>";
	  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 	
	  <%if(Content.equals("下载数据成功!")){%>  
          window.open("<%=FullPath%>");
    <%}else{%>	
	  	alert("下载数据失败！");
    <%}%>	
</script>
</html>
