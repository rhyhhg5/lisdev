//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput2() == false ) return false;
	if(checkDate() == false) return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "./LHGrpNonStandardSave.jsp";
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

	fm.all('saveButton').disabled = true;  
    var strSql2 = " select  (select l.codename from ldcode l where l.codetype='grpbaddish' and l.code=a.ServPlanLevel),"
					 +" CustomNumInDate,GrpServPlanNo,CustomNumActrually,ServPlanLevel,MakeDate,MakeTime,LevelDescription"    
 					 +" from LHGrpServPlan a where grpcontno = '"+fm.all('contno').value+"' order by ServPlanLevel"                                                  
					;  
       turnPage.queryModal(strSql2,LHGrpServPlanGrid); 
    
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHGrpServPlan.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if( verifyInput2() == false ) return false;
  if(checkDate() == false) return false;
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.action = "./LHGrpNonStandardSave.jsp";
   fm.all('fmtransact').value = "UPDATE||MAIN"; 
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHGrpNonStandardQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
 fm.all('fmtransact').value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	//alert(arrQueryResult);
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		    arrResult = arrQueryResult;
		fm.all('ServPlayType').value= arrResult[0][11];if(arrResult[0][11]=="2"){fm.all('ServPlayTypeName').value = "团体保单";}else{fm.all('ServPlayTypeName').value = "非标业务";}
        fm.all('GrpServPlanNo').value= arrResult[0][10];
        fm.all('ContNo').value= arrResult[0][0];
        fm.all('GrpCustomerNo').value= arrResult[0][1];
        fm.all('GrpName').value= arrResult[0][2];
        fm.all('ServPlanCode').value= easyExecSql("select ServPlancode from LHGrpServPlan where GrpServplanno = '"+fm.all('GrpServplanno').value+"' "); 
        fm.all('ServPlanName').value= easyExecSql("select ServPlanname from LHGrpServPlan where GrpServplanno = '"+fm.all('GrpServplanno').value+"' ");
        fm.all('ComID').value= arrResult[0][5];
        fm.all('ComID_cn').value= easyExecSql("select showname from ldcom where  sign = '1' and length(trim(comcode)) = 4 and  Comcode = '"+fm.all('ComID').value+"'");
        fm.all('StartDate').value= arrResult[0][6];
        fm.all('EndDate').value= arrResult[0][7];
        fm.all('ServPrem').value= easyExecSql("select sum(ServPrem) from LHGrpServPlan where GrpServplanno = '"+fm.all('GrpServplanno').value+"' "); 
        fm.all('MakeDate').value= arrResult[0][8];
        fm.all('MakeTime').value= arrResult[0][9];
        var strSql2 = " select  (select l.codename from ldcode l where l.codetype='grpbaddish' and l.code=a.ServPlanLevel),"
					 +" CustomNumInDate,GrpServPlanNo,CustomNumActrually,ServPlanLevel,MakeDate,MakeTime,LevelDescription"    
 					 +" from LHGrpServPlan a where grpcontno = '"+fm.all('contno').value+"' order by ServPlanLevel"                                                  
					;  
       turnPage.queryModal(strSql2,LHGrpServPlanGrid); 
	}
	getButtonState();
	
}   


function getButtonState()
{
	//只要从查询返回的，就是保存在LHGrpServPlan中的，saveButton失效。
	fm.all('saveButton').disabled = true;
	//一定是所有的GrpServPlanNo都在LHServItem中存在，或者都不存在，所以只看第一个就可以
	var planNo = LHGrpServPlanGrid.getRowColData(0,3);
	var sql_i3 = "select count(GrpServPlanNo) from lhservitem where GrpServPlanNo ='"+planNo+"'";
	if (easyExecSql(sql_i3)==0)
	{//未设置契约信息
		fm.all('CaseState').value = "1";
		fm.all('CaseStateName').value = "未进行服务信息设置";
		fm.all('ServFinish').disabled = false;
		fm.all('ServSet').disabled = true;
		fm.all('modifyButton').disabled = false;
		return false;
	}
	else
	{//设置了契约信息，未设置事件信息
		fm.all('CaseState').value = "2";
		fm.all('CaseStateName').value = "未进行服务事件设置";
		fm.all('ServFinish').disabled = true;
		fm.all('ServSet').disabled = false;
		fm.all('modifyButton').disabled = true;
	}
}
        
function toGrpItemSet()
{
	var selno = LHGrpServPlanGrid.getSelNo(); 
		if(selno < 1)
	{
		alert("请先选择一个客户分组");	
		return false;
	}
	var GrpServPlanNo=LHGrpServPlanGrid.getRowColData(LHGrpServPlanGrid.getSelNo()-1,3);
	window.open("./LHGrpServItem.jsp?GrpServPlanNo="+GrpServPlanNo+"&ComID="+fm.all('ComID').value,"","width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto");
}

function toGrpCustomerSet()
{
	var selno = LHGrpServPlanGrid.getSelNo(); 
	if(selno < 1)
	{
		alert("请先选择一个客户分组");	
		return false;
	}
	var arr = new Array();
	
	//arr = LHGrpServPlanGrid.getRowData(selno-1,4);
	arr=LHGrpServPlanGrid.getRowColData(LHGrpServPlanGrid.getSelNo()-1,5);
	//alert(arr);
	var GrpServPlanNo=LHGrpServPlanGrid.getRowColData(LHGrpServPlanGrid.getSelNo()-1,3);
	window.open("./LHGrpNonStandardUnitMain.jsp?GrpServPlanNo="+GrpServPlanNo+"&Level="+arr,"","width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto");
}

function toServTrace()
{
	var selno = LHGrpServPlanGrid.getSelNo();   
	if( selno < 1)
	{
		alert("请先选择一个客户分组");	
		return false;
	}
	var arr = new Array();
	
	arr = LHGrpServPlanGrid.getRowData(selno-1);
	window.open("./LHGrpServExeTrace.jsp?GrpServPlanNo="+arr[2]+"","","width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto");
}
function toGrpPerServTrace()
{
	var selno = LHGrpServPlanGrid.getSelNo(); 
	if( selno < 1)
	{
		alert("请先选择一个客户分组");	
		return false;
	}
	var arr = new Array();
	
	arr = LHGrpServPlanGrid.getRowData(selno-1);
	window.open("./LHGrpPerServExeTrace.jsp?GrpServPlanNo="+arr[2]+"","","width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto");
}
function checkDate()
{
	if(fm.all('StartDate').value != "" && fm.all('EndDate').value != "")
	{
			if(compareDate(fm.all('StartDate').value,fm.all('EndDate').value) == true)
			{
				alert("服务计划起始时间应早于服务计划结束时间 ");
				return false;
			}
	}
}
function ServRecordFinish()
{
	if(checkSet() == false)
	{
		return false;
	}
	if (confirm("设置后将无法再修改服务项目及客户信息！确定执行此操作吗?"))
	{
  		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  		
		fm.action="./LHServItemNoSave.jsp";
		fm.submit();
  }
	//var sql = "update lwmission set missionprop20 = '1' where missionprop1 ='"+fm.all('GrpContNo').value+"'";
	//easyExecSql(sql);
	//window.open("./LHGrpServEventSetTrace.jsp?GrpServlevel="+arr[0]+"&GrpContNo="+fm.all('GrpContNo').value+"","","width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto");

}

function afterServRecordFinish(FlagStr, content)
{//ServRecordFinish函数提交后的处理
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
		  fm.all('modifyButton').disabled = true;
		  fm.all('ServFinish').disabled = true;
		  fm.all('ServSet').disabled = false; 
	    //执行下一步操作
	}
}


function checkSet()
{
	var rowNum = LHGrpServPlanGrid.mulLineCount;
	//alert(rowNum);
	if (rowNum == 0)
	{
	  alert("请录入档次！")
	  return false;
	}
	else
		{
			for(i = 0; i < rowNum; i++ )
			{
		    var planno = LHGrpServPlanGrid.getRowColData(i,3);
				var sql_p = "select count(grpservitemno) from lhgrpservitem where grpservplanno ='"+planno+"'";
			  //alert(sql_p);
				if(easyExecSql(sql_p)=='0')
				{
					var level = LHGrpServPlanGrid.getRowColData(i,5);
					alert(level+"档未设定服务项目");
					return false;
				}
				
				var sql_q = "select count(*) from LHGrpPersonUnite where grpservplanno ='"+planno+"'";
				if(easyExecSql(sql_q)=='0')
				{
					var level = LHGrpServPlanGrid.getRowColData(i,1);
					alert(level+"档未设定客户名单");
					return false;
				}
			}
    }
}
function ServEventSet()
{
	var selno = LHGrpServPlanGrid.getSelNo(); 
	if( selno < 1)
	{
		alert("请先选择一个客户分组");	
		return false;
	}
	var rowNum = LHGrpServPlanGrid.mulLineCount;
	for(i = 0; i < rowNum; i++ )
	{
     var Levle = LHGrpServPlanGrid.getRowColData(i,5);
     var GrpContNo = LHGrpServPlanGrid.getRowColData(i,3);
	   window.open("./LHGrpServEventSetTrace.jsp?GrpServlevel="+Levle+"&GrpContNo="+GrpContNo+"&flag=UCONT","","width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto");
	}
}
function CaseSetFinish()
{
	var mulline = LHGrpServPlanGrid.mulLineCount;
	if(mulline==0)
	{
		alert("请先设置客户分组");
		return false;	
	}
	for (var i = 0; i < mulline; i++)
	{
		var GrpPlanNo = LHGrpServPlanGrid.getRowColData(i,3);
		
		//先校验服务信息是否已保存
		var sql_exist = "select ServItemNo from LHServItem where GrpServPlanNo ='"+GrpPlanNo+"'";
		var arr_exist = easyExecSql(sql_exist);
		if( arr_exist == null || arr_exist == "null" )
		{  
		  alert("请先设置服务信息");
		  return false;
		}
		//校验服务信息是否已设置为事件
		var sql = "select ServItemNo from LHServItem where GrpServPlanNo ='"+GrpPlanNo+"' and ServItemNo not in (select ServItemNo from LHServCaseRela)";
		var sql_result = easyExecSql(sql);
		if (sql_result != "" && sql_result != null && sql_result != "null")
		{  
		  alert(LHGrpServPlanGrid.getRowColData(i,1)+"档客户组仍存在未归入事件的服务项目！");
		  return false;
		}
//		else
//		{
//			alert(LHGrpServPlanGrid.getRowColData(i,1)+"档客户组服务事件设置完成！")
//		}
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.action="./LHCaseStateSave.jsp";
	fm.submit();
}   

function afterCaseSetFinish(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
	}
}
function getOtherNo()
{
	if(fm.all('ServPlayType').value == "3")
	{//是非标业务进入
		if(fm.all('GrpCustomerNo').value == "")
		{
			alert("请先填写客户");
			return false;
		}
		
		var sql = " select max(Grpcontno) from LHGrpServPlan where Grpcontno like 'Z"+fm.all('GrpCustomerNo').value+"%%' "
		var arrOtherno = easyExecSql(sql);
		if(arrOtherno=="" || arrOtherno==null || arrOtherno=="null")
		{
			fm.all('ContNo').value = "Z"+fm.all('GrpCustomerNo').value+"001";
		}
		else
		{
			var sNo = parseInt(arrOtherno[0][0].toString().substring(10))+1;
			if(sNo.toString().length == "1")
			{
				sNo = "00"+sNo;
			}
			else if(sNo.length == "2")
			{
				sNo = "0"+sNo;
			}
			fm.all('ContNo').value ="Z"+fm.all('GrpCustomerNo').value+ sNo;
		}
	}
}