<%
//程序名称：LHStatuteQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-19 13:04:32
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('StatuteNo').value = "";
    fm.all('StatuteTitle').value = "";
//    fm.all('DocumentNum').value = "";
//    fm.all('ContType').value = "";
//    fm.all('IssueOrganCode').value = "";
    fm.all('IssueDate').value = "";
    fm.all('StartDate').value = "";
    fm.all('EndDate').value = "";
    fm.all('DocContent').value = "";
    
  }
  catch(ex) {
    alert("在LHStatuteQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHStatuteGrid();  
  }
  catch(re) {
    alert("LHStatuteQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHStatuteGrid;
function initLHStatuteGrid() {                               
  var iArray = new Array();  
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
//    iArray[1]=new Array(); 
//		iArray[1][0]="政策法规文件代码";   
//		iArray[1][1]="120px";   
//		iArray[1][2]=20;        
//		iArray[1][3]=0;
		
		iArray[1]=new Array(); 
		iArray[1][0]="政策法规文件名称";   
		iArray[1][1]="120px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;
		
		iArray[2]=new Array(); 
		iArray[2][0]="发文号";   
		iArray[2][1]="80px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="颁布日期";   
		iArray[3][1]="80px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="实施日期 ";   
		iArray[4][1]="80px";   
		iArray[4][2]=80;        
		iArray[4][3]=0;
		
		iArray[5]=new Array(); 
		iArray[5][0]="终止日期";   
		iArray[5][1]="80px";   
		iArray[5][2]=20;        
		iArray[5][3]=0;	    
		
		iArray[6]=new Array();  
		iArray[6][0]="内容简介";
		iArray[6][1]="240px";   
		iArray[6][2]=20;       
    iArray[6][3]=0;	   
    
    iArray[7]=new Array();  
		iArray[7][0]="MakeDate";
		iArray[7][1]="0px";   
		iArray[7][2]=20;       
    iArray[7][3]=3;
    
    iArray[8]=new Array();  
		iArray[8][0]="MakeTime";
		iArray[8][1]="0px";   
		iArray[8][2]=20;       
    iArray[8][3]=3;     
    
    LHStatuteGrid = new MulLineEnter( "fm" , "LHStatuteGrid" ); 
    //这些属性必须在loadMulLine前

    LHStatuteGrid.mulLineCount = 0;   
    LHStatuteGrid.displayTitle = 1;
    LHStatuteGrid.hiddenPlus = 1;
    LHStatuteGrid.hiddenSubtraction = 1;
    LHStatuteGrid.canSel = 1;
    LHStatuteGrid.canChk = 0;
//    LHStatuteGrid.selBoxEventFuncName = "showthat";

    LHStatuteGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHStatuteGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
