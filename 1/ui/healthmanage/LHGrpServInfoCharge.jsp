<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHGrpServEventSet.jsp
//程序功能：
//创建日期：2006-07-18 10:19:05
//创建人  ：CrtHtml程序创建
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
  
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
	<SCRIPT src="LHGrpServInfoCharge.js"></SCRIPT>
	<%@include file="LHGrpServInfoInit.jsp"%>
</head>
<body  onload="initForm();passQuery();" >
  <form method=post name=fm target="fraSubmit">
  	<table  class= common align='center' >
			<TR  class= common>
			    <TD  class= title>
			      服务事件类型
			    </TD>
			    <TD  class= input>
			      <Input type=hidden name=ServeEventStyle>
	     	    <Input class= 'code' name=ServeEventStyle_ch verify="服务事件类型|notnull" elementtype=nacessary CodeData= "0|^个人服务事件|1^集体服务事件|2" ondblClick= "showCodeListEx('',[this,ServeEventStyle],[0,1],null,null,null,1);"> <!--ondblClick= "showCodeListEx('',[this,ServeEventStyle],[0,1],null,null,null,1);"--> 
			    </TD>
			     <TD  class= title>
			     服务事件号码
			    </TD>
			    <TD  class= input>
			    	<Input class= 'common' name=ServeEventNum>
			    </TD>
			    <TD  class= title>
			     服务事件名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServeEventName >
			    </TD>
		</TR>
		<TR  class= common>
				<TD  class= title>
			      服务事件状态
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServCaseState readOnly>
			    </TD>
			    <TD  class= title>
			    </TD>
			    <TD  class= input>
			    </TD>
			    <TD  class= title>
			    </TD>
			    <TD  class= input>
			    </TD>
		  	</TR>
		 </table>
		 
		 <table class=common>
		<tr align=left>
			<td>
			   <input  type=button class=cssButton name=AddCase value="新增集体服务事件" onclick="AddServCase();">
			   <input  type=button class=cssButton name=QueryCase value="集体服务事件查询" onclick="QueryServCase();">
			</td>
		</tr>
	</table>
	
	<hr>
	<table  class= common align='center' >
		<TR  class= common>
			<TD class= title >
				是否已关联
			</TD>   		 
			<TD  class= input>
				<Input class= 'codename' style="width:40px" name=ServCaseStutes value=1><Input class= 'codeno' style="width:120px"  name=ServCaseStutesName ondblclick="showCodeList('caseassociate',[this,ServCaseStutes],[1,0],null,null,null,'1',160);" value="未关联">
			</TD>
			<TD  class= title>
			</TD>
			<TD  class= input>
			</TD>
			<TD  class= title>
			</TD>
			<TD  class= input>
			</TD>
		</TR>
		<TR  class= common>
			<TD  class= title>
				客户姓名
          	</TD>
	    	<TD  class= input>
	    	  	<Input class= 'code' name=CustomerName ondblclick=" return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name');"  onkeyup=" if( event.keyCode==13 ) return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name');" >
	    	</TD>   
        	<TD  class= title>
	    	  	客户号码
        	</TD>
        	<TD  class= input>
        		<Input class= 'common' name=CustomerNo>
        	</TD> 
        	<TD  class= title>
				保单号
			</TD>
			<TD  class= input>
			   <Input class= 'common' name=GrpContNo >
			</TD>
		</TR>
		<TR  class= common>
			<TD  class= title>
        		服务计划代码
        	 </TD>
			 <TD  class= input>
			    <Input class= 'code' name=Riskcode  verify="服务计划代码|NOTNULL&len<=50" ondblclick="showCodeList('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode');"   onkeydown="return showCodeListKey('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode'); codeClear(Riskname,Riskcode);">
			 </TD>
			 <TD  class= title>
			     服务计划名称
			 </TD>
			 <TD  class= input>
			     <Input class= 'code' name=Riskname  verify="个人服务计划名称|NOTNULL&len<=300" ondblclick="showCodeList('hmriskcode',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskcode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmriskcode',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskcode');"   onkeydown="return showCodeListKey('hmriskcode',[this,Riskcode],[0,1],null,fm.Riskcode.value,'Riskcode'); codeClear(Riskname,Riskcode);">
			 </TD>
			 <TD  class= title>
			   	服务项目名称
			 </TD>
			 <TD  class= input>
			   <Input class= 'common' name=ServItemCode  verify="标准服务项目代码|len<=50" style="width:30px"><Input class= 'code' name=ServItemName verify="标准服务项目名称|len<=50" style="width:130px" ondblclick="showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode','1',200);">
			 </TD>
		</TR>
		<TR  class= common>
			<TD  class= title>
			  服务执行状态
			</TD>
			<TD  class= input>
				<Input type=hidden name=ServExeState>
			  <Input class= 'code' name=ServExeState_ch CodeData= "0|^尚未执行|1^正在执行|2^已执行完毕|3" ondblClick= "showCodeListEx('',[this,ServExeState],[0,1],null,null,null,1);" >
			</TD>
			<!--TD  class= title>
	      	 保单类型
	        </TD>
	        <TD  class= input>
	           <Input type=hidden name=ContType>
	     	     <Input class= 'code' name=ContType_ch verify="保单类型|notnull" elementtype=nacessary CodeData= "0|^个人保单|1^团体保单|2" ondblClick= "showCodeListEx('',[this,ContType],[0,1],null,null,null,1);"> 
	        </TD-->
	        <TD  class= title>
			</TD>
			<TD  class= input>
			</TD> 
			<TD  class= title>
			</TD>
			<TD  class= input>
			</TD>
		</TR>
	</table>
		 
	<table class=common>
		<tr align=left>
			<td>
			   <input  type=button class=cssButton name=AddCase value="查询" onclick="SearchCustom();">
			</td>
		</tr>
	</table>
	
	<Div id="divLHCustomerInfoGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHCustomInfoGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
		
	<table class=common>
		<tr align=left>
			<td>
			   <input  type=button class=cssButton name=AddToServ value="添加到服务事件" onclick="AddToServCase();">
			</td>
		</tr>
	</table>

  	<Input type=hidden name=flag >
	<Input type=hidden name=GrpServItemNo >
	<Input type=hidden name=fmtransact >

  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script>
		function passQuery()
		{ 	
			//进入此函数
			if (flag == 1)
			{
			fm.all('flag').value = flag;
			fm.all('GrpServItemNo').value = GrpServItemNo;
			var sql = "select a.CustomerNo,a.Name,b.GrpContNo,b.ServItemCode,"
			         +"(select ServItemName from LHHealthServItem c where c.ServItemCode = b.ServItemCode),"
			         +"b.ServItemSerial,0,b.ComID,b.ServPriceCode,"
			         +"(select ServPriceName from LHServerItemPrice d where d.ServPriceCode = b.ServPriceCode),a.ServItemNo "            
			         +"from LHServItem a,LHGrpServItem b where a.GrpServItemNo = b.GrpServItemNo and b.GrpServItemNo ='"+GrpServItemNo+"'"
			         + " and a.ServItemNo not in ( select distinct m.ServItemNo from LHServCaseRela m)"
			         ;
			        // alert(sql);
			turnPage.queryModal(sql,LHCustomInfoGrid); 
			fm.all('GrpContNo').value = LHCustomInfoGrid.getRowColData(0,3);
			fm.all('ServItemName').value = LHCustomInfoGrid.getRowColData(0,5);
			fm.all('ServItemCode').value = LHCustomInfoGrid.getRowColData(0,4);
			// var sql_c = "select Riskcode from lcpol where Grpcontno = '"+fm.all('GrpContNo').value+"'";
			// fm.all('Riskcode').value = easyExecSql(sql_c);
      // fm.all('Riskname').value = easyExecSql("select Riskname from lmriskapp where Riskcode = '"+easyExecSql(sql_c)+"'");
    }
	  }
</script>
</html>