<%
//程序名称：LHContraAssoSettingQueryInputInit.jsp
//程序功能：功能描述
//创建日期：2006-03-14 16:59:48
//创建人  ：郭丽颖
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ContraNo').value = "";
   
    fm.all('ContraName').value = "";
    
  }
  catch(ex) {
    alert("在LHContraAssoSettingContQueryInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHContraAssoSettingContQueryGrid();  
  }
  catch(re) {
    alert("LHContraAssoSettingContQueryInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHContraAssoSettingContQueryGrid;
function initLHContraAssoSettingContQueryGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";        
    iArray[0][1]="30px";        
    iArray[0][3]=0;         		
    
    iArray[1]=new Array();
    iArray[1][0]="合同编号";     
    iArray[1][1]="38px";         
    iArray[1][3]=0;  
    
    iArray[2]=new Array();
    iArray[2][0]="合同名称";     
    iArray[2][1]="38px";         
    iArray[2][3]=0;      
    
    iArray[3]=new Array();
    iArray[3][0]="签约人代码";     
    iArray[3][1]="38px";         
    iArray[3][3]=0;   
    
    iArray[4]=new Array();
    iArray[4][0]="签订日期";     
    iArray[4][1]="38px";         
    iArray[4][3]=0;   
    
    iArray[5]=new Array();
    iArray[5][0]="合同起始日期";     
    iArray[5][1]="38px";         
    iArray[5][3]=0;   
    
    iArray[6]=new Array();
    iArray[6][0]="合同终止日期";     
    iArray[6][1]="38px";         
    iArray[6][3]=0;   
    
    iArray[7]=new Array();
    iArray[7][0]="合同状态";     
    iArray[7][1]="36px";         
    iArray[7][3]=0;   
    
  
    
    LHContraAssoSettingContQueryGrid = new MulLineEnter( "fm" , "LHContraAssoSettingContQueryGrid" ); 
    //这些属性必须在loadMulLine前

   LHContraAssoSettingContQueryGrid.mulLineCount = 0;   
   LHContraAssoSettingContQueryGrid.displayTitle = 1;
   LHContraAssoSettingContQueryGrid.hiddenPlus = 1;
   LHContraAssoSettingContQueryGrid.hiddenSubtraction = 1;
   LHContraAssoSettingContQueryGrid.canSel = 1;
   LHContraAssoSettingContQueryGrid.canChk = 0;
  // LHContraAssoSettingContQueryGrid.selBoxEventFuncName ="showDetail"
   
   LHContraAssoSettingContQueryGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHContraAssoSettingContQueryGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
