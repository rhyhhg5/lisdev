//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交

}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHMedicalServiceRecord.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if( verifyInput2() == false ) return false;
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHMedicalServiceRecordQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0(arrQueryResult)
{
	var arrResult = new Array();
	
	
	if( arrQueryResult != null )
	{
		//alert(arrQueryResult[0][1]);   	
		fm.RecordNo.value = arrQueryResult[0][0];		
	 var strSQL = " select a.CustomerNo, b.Name,a.FirstRecoDate,a.ServiceState,a.ApplyDutyItemCode,a.DescriptionDetail,"
				   		+" a.ServiceDate,a.HospitCode,a.DoctNo,a.ApplyReceiveType,a.ServiceFlag,a.IsBespeakOk,a.BespeakServiceItem,"
				   		+" a.ApplyServiceDes,a.BespeakServiceDate,a.BespeakServiceHospit,a.BespeakServiceDoctNo,a.NoCompareCause,"
				   		+" a.BespeaLostCause,a.ServiceExecState,'',a.ExcuteServiceItem,a.ExcuteDescription,"
				   		+" a.ExcuteServiceDate,a.ExcuteServiceHospit,a.ExcuteServiceDocNo,a.ExNoCompareCause,a.ExcutePay,"
				   		+" a.SatisfactionDegree,a.OtherServiceInfo,a.CancleServiceCause,a.RecordNo,a.MakeDate,a.MakeTime, "
				   		+" a.FirstRecoTime,a.ServiceTime,a.BespeakServiceTime,a.ExcuteServiceTime,a.RecordNo,a.IsBookSame,a.IsExecSame "
				   		+" from LHMedicalServiceRecord a,LDPerson b"                                             
				   		+" where a.CustomerNo = b.CustomerNo and a.CustomerNo = '"+arrQueryResult[0][1]+"' "     
				   		+" and a.RecordNo = '"+arrQueryResult[0][0]+"'"
//	   		+" and a.HospitCode = c.HospitCode"
//	   		+" and a.BespeakServiceHospit = c.HospitCode "
//	   		+" and a.DoctNo = d.DoctNo "
//	   		+" and a.BespeakServiceDoctNo = d.DoctNo "
//	   		+" and a.ExcuteServiceDocNo = d.DoctNo "
	   		;
//     alert(arrResult);
          arrResult = easyExecSql(strSQL);

          if(arrResult[0][10] == "1")
          {
		          	isBookOk.style.display = '';
		          	if(arrResult[0][11] == "1")
		          	{
		          		bookItem.style.display='';
									serviceStatetable.style.display='';
		          	}
		          	if(arrResult[0][11] == "2") 
		          	{
		          		serviceExecute.style.display='none';
		          		bookFail.style.display='';
		          	}
          }
          if(arrResult[0][10] == "0")
          { 
          			serviceStatetable.style.display = '';
          }
          if(arrResult[0][19] == "1")
          {
          			bookLeftTime.style.display='';					
          }
          if(arrResult[0][19] == "2" && arrResult[0][11] != "2")
          { 
          			serviceExecute.style.display='';
          }
          if(arrResult[0][19] == "3")
          {
          			serviceCancel.style.display='';
          }         
        ; 
          
          
          fm.all('CustomerNo').value = arrResult[0][0];
          fm.all('CustomerName').value = arrResult[0][1];
          fm.all('FirstRecoDate').value = arrResult[0][2];
          fm.all('ServiceState').value = arrResult[0][3];if(arrResult[0][3]=="1"){fm.all('ServiceState_ch').value="进行中";}if(arrResult[0][3]=="2"){fm.all('ServiceState_ch').value="服务结束";}
          fm.all('ApplyDutyItemCode').value = arrResult[0][4];if(arrResult[0][4] !="" ){fm.all('DutyItemName').value = easyExecSql(" select codename from ldcode where codetype = 'healthservice' and code = '"+arrResult[0][4]+"'");}
          fm.all('DescriptionDetail').value = arrResult[0][5];
          fm.all('ServiceDate').value = arrResult[0][6];
          fm.all('HospitCode').value = arrResult[0][7];
          if(arrResult[0][7] !="" )
          	{fm.all('HospitName').value = easyExecSql("select hospitName from ldhospital where hospitcode = '"+arrResult[0][7]+"'");}
          fm.all('DoctNo').value = arrResult[0][8];
          if(arrResult[0][8] !="" )
          	{fm.all('DoctName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][8]+"'");}
          fm.all('ApplyReceiveType').value = arrResult[0][9];if(arrResult[0][9]=="1"){fm.all('ApplyReceiveType_ch').value ="电话";}if(arrResult[0][9]=="2"){fm.all('ApplyReceiveType_ch').value ="电子邮件";}if(arrResult[0][9]=="3"){fm.all('ApplyReceiveType_ch').value ="其他";}
          fm.all('ServiceFlag').value = arrResult[0][10];if(arrResult[0][10]=="0"){fm.all('ServiceFlag_ch').value ="否";}if(arrResult[0][10]=="1"){fm.all('ServiceFlag_ch').value ="是";}
          fm.all('IsBespeakOk').value = arrResult[0][11];if(arrResult[0][11]=="1"){fm.all('IsBespeakOk_ch').value ="预约成功";}if(arrResult[0][11]=="2"){fm.all('IsBespeakOk_ch').value ="预约失败";}
          fm.all('BespeakServiceItem').value = arrResult[0][12];if(arrResult[0][12] !="" ){fm.all('BespeakServiceItemName').value = easyExecSql(" select codename from ldcode where codetype = 'healthservice' and code = '"+arrResult[0][12]+"'");}
          fm.all('ApplyServiceDes').value = arrResult[0][13];
          fm.all('BespeakServiceDate').value = arrResult[0][14];
          fm.all('BespeakServiceHospit').value = arrResult[0][15];
          if(arrResult[0][15] !="" )
          	{fm.all('BespeakServiceHospitName').value = easyExecSql("select hospitName from ldhospital where hospitcode = '"+arrResult[0][15]+"'");}
          fm.all('BespeakServiceDoctNo').value = arrResult[0][16];
          if(arrResult[0][16] !="" )
          	{fm.all('BespeakServiceDoctName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][16]+"'");}
          fm.all('NoCompareCause').value = arrResult[0][17];
          fm.all('BespeaLostCause').value = arrResult[0][18];
          fm.all('ServiceExecState').value = arrResult[0][19];
          if(arrResult[0][19] !="" )
          	{fm.all('ServiceExecState_ch').value = easyExecSql("select codename from ldcode where codetype = 'servicestate' and code = '"+arrResult[0][19]+"'");}
//          fm.all('BespeaLLeaveTime').value = arrResult[0][20];
          fm.all('ExcuteServiceItem').value = arrResult[0][21];if(arrResult[0][21] !="" ){fm.all('ExcuteServiceItemName').value = easyExecSql(" select codename from ldcode where codetype = 'healthservice' and code = '"+arrResult[0][21]+"'");}
          fm.all('ExcuteDescription').value = arrResult[0][22];
          fm.all('ExcuteServiceDate').value = arrResult[0][23];
          fm.all('ExcuteServiceHospit').value = arrResult[0][24];
          if(arrResult[0][24] !="" )
          	{fm.all('ExcuteServiceHospitName').value = easyExecSql("select hospitName from ldhospital where hospitcode = '"+arrResult[0][24]+"'");}
          fm.all('ExcuteServiceDocNo').value = arrResult[0][25];
          if(arrResult[0][25] !="" )
          	{fm.all('ExcuteServiceDocName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][25]+"'");}
          fm.all('ExNoCompareCause').value = arrResult[0][26];
          fm.all('ExcutePay').value = arrResult[0][27];
          fm.all('SatisfactionDegree').value = arrResult[0][28];
          if(arrResult[0][28] !="" )
         		 {fm.all('SatisfactionDegree_ch').value = easyExecSql("select codename from ldcode where codetype = 'hmsatisfy' and code = '"+arrResult[0][28]+"'");}
          fm.all('OtherServiceInfo').value = arrResult[0][29];
          fm.all('CancleServiceCause').value = arrResult[0][30];
          fm.all('RecordNo').value = arrResult[0][31];
          fm.all('MakeDate').value = arrResult[0][32];
          fm.all('MakeTime').value = arrResult[0][33];
          fm.all('FirstRecoTime').value = arrResult[0][34];
          fm.all('ServiceTime').value = arrResult[0][35];
          fm.all('BespeakServiceTime').value = arrResult[0][36];
          fm.all('ExcuteServiceTime').value = arrResult[0][37];
          fm.all('RecordNo').value = arrResult[0][38];
          fm.all('IsApplaySame').value = arrResult[0][39];
          fm.all('IsExecSame').value = arrResult[0][40];  
          fm.all('IsApplaySame_ch').value=  (arrResult[0][39]=="1")?"是":"否";if(arrResult[0][11] == "2"){fm.all('IsApplaySame').value="";fm.all('IsApplaySame_ch').value="";}
          fm.all('IsExecSame_ch').value=  (arrResult[0][40]=="1")?"是":"否";
        			
	}
}
function afterQuery(arrResult)
{  
	var arrResult2 = new Array();
  if(arrResult!=null)
  {
  	arrResult2 = arrResult;
  	fm.all('CustomerNo').value = arrResult2[0][0];
  	fm.all('CustomerName').value = arrResult2[0][1];
  }
}



function queryCustomerNo2()
{
	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
	var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}
function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.html");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.all('CustomerNo').value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
//  	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo like '%" + cCustomerNo +"%'";
	    var arrResult = easyExecSql(strSql);
	       
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}

//需要预约时,显示服务预约纪录与服务执行情况
function afterCodeSelect(codeName,Field)
{		
  	if(codeName=="ServerFlag")
		{
			if(Field.value=="1")
			{
				isBookOk.style.display='';
				serviceCancel.style.display='none';
				bookLeftTime.style.display='none';
//				serviceState.style.display = 'none';
				serviceExecute.style.display='none';
				serviceStatetable.style.display='none';
			}
			else
			{
				if(Field.value=="0")
				{
					serviceStatetable.style.display='';
					isBookOk.style.display='none';
					serviceCancel.style.display='none';
					bookLeftTime.style.display='none';
					serviceExecute.style.display='none';
					bookItem.style.display='none';
					bookFail.style.display='none';
					
				}
			}
		}
		
		if(codeName == "IsBespeakOk")
		{
			if(Field.value == "1")
			{
				bookItem.style.display='';
				serviceStatetable.style.display='';
				bookFail.style.display='none'; 
				
				

			}
		  else
		  {
		  	bookFail.style.display='';
		  	bookItem.style.display='none';
		  	serviceStatetable.style.display='none';
		  	bookLeftTime.style.display='none';
				serviceExecute.style.display='none';	
				serviceCancel.style.display='none';
		  }
		}
		
		if(codeName == "IsSame")
		{
			if(Field.value == "1")
			{
				fm.all('BespeakServiceItem').value			=fm.all('ApplyDutyItemCode').value;   
				fm.all('BespeakServiceItemName').value	=fm.all('DutyItemName').value;
				fm.all('ApplyServiceDes').value					=fm.all('DescriptionDetail').value;
				fm.all('BespeakServiceDate').value			=fm.all('ServiceDate').value;
				fm.all('BespeakServiceTime').value			=fm.all('ServiceTime').value;
				fm.all('BespeakServiceHospit').value		=fm.all('HospitCode').value;
				fm.all('BespeakServiceHospitName').value=fm.all('HospitName').value;
				fm.all('BespeakServiceDoctNo').value		=fm.all('DoctNo').value;
				fm.all('BespeakServiceDoctName').value	=fm.all('DoctName').value;
				NoCompareCause.style.display='none'; 
			}
		  else
		  {
		  	fm.all('BespeakServiceItem').value			="";
				fm.all('BespeakServiceItemName').value	="";
				fm.all('ApplyServiceDes').value					="";
				fm.all('BespeakServiceDate').value			="";
				fm.all('BespeakServiceTime').value			="";
				fm.all('BespeakServiceHospit').value		="";
				fm.all('BespeakServiceHospitName').value="";
				fm.all('BespeakServiceDoctNo').value		="";
				fm.all('BespeakServiceDoctName').value	="";  
				NoCompareCause.style.display='';

		  }
		}
		
		if(codeName == "IsExecSame")
		{
			if(Field.value == "1")
			{
				fm.all('ExcuteServiceItem').value				=fm.all('ApplyDutyItemCode').value;   
				fm.all('ExcuteServiceItemName').value		=fm.all('DutyItemName').value;
				fm.all('ExcuteDescription').value				=fm.all('DescriptionDetail').value;
				fm.all('ExcuteServiceDate').value				=fm.all('ServiceDate').value;
				fm.all('ExcuteServiceTime').value				=fm.all('ServiceTime').value;
				fm.all('ExcuteServiceHospit').value			=fm.all('HospitCode').value;
				fm.all('ExcuteServiceHospitName').value =fm.all('HospitName').value;
				fm.all('ExcuteServiceDocNo').value			=fm.all('DoctNo').value;
				fm.all('ExcuteServiceDocName').value		=fm.all('DoctName').value;
				ExNoCompareCause.style.display='none';   
			}
		  else
		  {
		  	fm.all('ExcuteServiceItem').value				="";
				fm.all('ExcuteServiceItemName').value		="";
				fm.all('ExcuteDescription').value				="";
				fm.all('ExcuteServiceDate').value				="";
				fm.all('ExcuteServiceTime').value				="";
				fm.all('ExcuteServiceHospit').value			="";
				fm.all('ExcuteServiceHospitName').value ="";
				fm.all('ExcuteServiceDocNo').value			="";
				fm.all('ExcuteServiceDocName').value		="";
				ExNoCompareCause.style.display='';   
		  }
		}
		
		if(codeName == "servicestate")
		{
			if(Field.value == "1")
			{
					fm.ServiceState.value = '1'          
					fm.ServiceState_ch.value = '进行中'
					bookLeftTime.style.display='';
					serviceExecute.style.display='none';
					serviceCancel.style.display='none';
			}
			if(Field.value == "2")
			{
				fm.ServiceState.value = '2'
				fm.ServiceState_ch.value = '服务结束'
				serviceExecute.style.display='';
				bookLeftTime.style.display='none';
				serviceCancel.style.display='none';
			}
			if(Field.value == "3")
			{                       
				fm.ServiceState.value = '1'        
				fm.ServiceState_ch.value = '进行中'
				serviceCancel.style.display='';
				bookLeftTime.style.display='none';
				serviceExecute.style.display='none';				
			}
		}
}

//将字符串str按照字符串ch拆分产生数组
function str2Array(str,ch)
{
 var a=new Array();
 var i=0,j=-1,k=0;
 while (i<str.length)
 {  
  j=str.indexOf(ch,j+1);   
  if (j!=-1)
  {
   if (j==0){a[k]="";}else{a[k]=str.substring(i,j);}
   i=j+1;
  }else
  {
   a[k]=str.substring(i,str.length);
   i=str.length;
  }
  k++;
 }
 return a;
}

function inputDescriptionDetail()
{ 
	divDescriptionDetail.style.display='';
	 fm.all('textDescriptionDetail').value=fm.all('DescriptionDetail').value;	
}
function backDescriptionDetail()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('DescriptionDetail').value=fm.all('textDescriptionDetail').value;
		fm.all('textDescriptionDetail').value="";
		divDescriptionDetail.style.display='none';		
	}
}

function backDescriptionDetail1()
{
		fm.all('DescriptionDetail').value=fm.all('textDescriptionDetail').value;
		fm.all('textDescriptionDetail').value="";
		divDescriptionDetail.style.display='none';		
}

function inputApplyServiceDes()
{ 
	divApplyServiceDes.style.display='';
	fm.all('textApplyServiceDes').value = fm.all('ApplyServiceDes').value;	
}

function backApplyServiceDes()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('ApplyServiceDes').value=fm.all('textApplyServiceDes').value;
		fm.all('textApplyServiceDes').value="";
		divApplyServiceDes.style.display='none';	
	}
}

function backApplyServiceDes1()
{
		fm.all('ApplyServiceDes').value=fm.all('textApplyServiceDes').value;
		fm.all('textApplyServiceDes').value="";
		divApplyServiceDes.style.display='none';	
}

function inputExcuteDescription()
{ 
	divExcuteDescription3.style.display='';
	fm.all('textExcuteDescription3').value = fm.all('ExcuteDescription').value;	
}


function backExcuteDescription()
{

	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('ExcuteDescription').value=fm.all('textExcuteDescription3').value;
		fm.all('textExcuteDescription3').value="";
		divExcuteDescription3.style.display='none';
		
	}
}

function backExcuteDescription1()
{
		fm.all('ExcuteDescription').value=fm.all('textExcuteDescription3').value;
		fm.all('textExcuteDescription3').value="";
		divExcuteDescription3.style.display='none';
}


//预约失败原因文本框
function inputBespeaLostCause()
{
	divBespeaLostCause.style.display='';
	fm.all('textBespeaLostCause').value = fm.all('BespeaLostCause').value;
}
function backBespeaLostCause()
{
		var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('BespeaLostCause').value=fm.all('textBespeaLostCause').value;
		fm.all('textBespeaLostCause').value="";
		divBespeaLostCause.style.display='none';		
	}
}
function backBespeaLostCause1()
{
		fm.all('BespeaLostCause').value=fm.all('textBespeaLostCause').value;
		fm.all('textBespeaLostCause').value="";
		divBespeaLostCause.style.display='none';
}


//服务取消原因文本框
function inputCancleServiceCause()
{
	divCancleServiceCause.style.display='';
	fm.all('textCancleServiceCause').value = fm.all('CancleServiceCause').value;
}
function backCancleServiceCause()
{
		var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('CancleServiceCause').value=fm.all('textCancleServiceCause').value;
		fm.all('textCancleServiceCause').value="";
		divCancleServiceCause.style.display='none';		
	}
}
function backCancleServiceCause1()
{
		fm.all('CancleServiceCause').value=fm.all('textCancleServiceCause').value;
		fm.all('textCancleServiceCause').value="";
		divCancleServiceCause.style.display='none';
}


//服务不相符原因文本框
function inputNoCompareCause()
{
	divNoCompareCause.style.display='';
	fm.all('textNoCompareCause').value = fm.all('NoCompareCause').value;
}
function backNoCompareCause()
{
		var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('NoCompareCause').value=fm.all('textNoCompareCause').value;
		fm.all('textNoCompareCause').value="";
		divNoCompareCause.style.display='none';		
	}
}
function backNoCompareCause1()
{
		fm.all('NoCompareCause').value=fm.all('textNoCompareCause').value;
		fm.all('textNoCompareCause').value="";
		divNoCompareCause.style.display='none';
}


//执行不相符原因文本框
function inputExNoCompareCause()
{
	divExNoCompareCause.style.display='';
	fm.all('textExNoCompareCause').value = fm.all('ExNoCompareCause').value;
}
function backExNoCompareCause()
{
		var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('ExNoCompareCause').value=fm.all('textExNoCompareCause').value;
		fm.all('textExNoCompareCause').value="";
		divExNoCompareCause.style.display='none';		
	}
}
function backExNoCompareCause1()
{
		fm.all('ExNoCompareCause').value=fm.all('textExNoCompareCause').value;
		fm.all('textExNoCompareCause').value="";
		divExNoCompareCause.style.display='none';
}


//双击显示当前时间
function dFirstRecoTime()
{
		var d = new Date();
		var h = d.getHours();
		var m = d.getMinutes();
		if(h<10){h = "0"+d.getHours();}
		if(m<10){m = "0"+d.getMinutes();}
		fm.FirstRecoTime.value = h+":"+m;
}

function dServiceTime()
{
	var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.ServiceTime.value = h+":"+m;
	
} 
  
function dBespeakServiceTime()
{ 
	var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.BespeakServiceTime.value = h+":"+m;	
}

function dExcuteServiceTime()
{
	var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.ExcuteServiceTime.value = h+":"+m;	
}