<%
//程序名称：LHCustomDiseasQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-17 21:09:38
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CustomerNo').value = "";
    fm.all('DiseasNo').value = "";
    fm.all('DiseasCode').value = "";
    fm.all('DiseasState').value = "";
    fm.all('DiseasBeginDate').value = "";
    fm.all('DiseasEndDate').value = "";
    fm.all('HospitCode').value = "";
    fm.all('Doctor').value = "";
    fm.all('ImpartFlag').value = "";
    fm.all('ImpartDate').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LHCustomDiseasQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHCustomDiseasGrid();  
  }
  catch(re) {
    alert("LHCustomDiseasQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHCustomDiseasGrid;
function initLHCustomDiseasGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="客户号码";   
	iArray[1][1]="80px";   
	iArray[1][2]=24;        
	iArray[1][3]=0;
	
	iArray[2]=new Array(); 
	iArray[2][0]="客户姓名";   
	iArray[2][1]="60px";   
	iArray[2][2]=24;        
	iArray[2][3]=0;
	
	iArray[3]=new Array(); 
	iArray[3][0]="序号";   
	iArray[3][1]="40px";   
	iArray[3][2]=20;        
	iArray[3][3]=0;
	
	iArray[4]=new Array(); 
	iArray[4][0]="疾病代码";   
	iArray[4][1]="40px";   
	iArray[4][2]=20;        
	iArray[4][3]=0;
	
	iArray[5]=new Array(); 
	iArray[5][0]="疾病情况";   
	iArray[5][1]="80px";   
	iArray[5][2]=20;        
	iArray[5][3]=0;
	
	iArray[6]=new Array(); 
	iArray[6][0]="开始日期";   
	iArray[6][1]="40px";   
	iArray[6][2]=20;        
	iArray[6][3]=0;
    
    LHCustomDiseasGrid = new MulLineEnter( "fm" , "LHCustomDiseasGrid" ); 
    //这些属性必须在loadMulLine前

    LHCustomDiseasGrid.mulLineCount = 0;   
    LHCustomDiseasGrid.displayTitle = 1;
    LHCustomDiseasGrid.hiddenPlus = 1;
    LHCustomDiseasGrid.hiddenSubtraction = 1;
    LHCustomDiseasGrid.canSel = 1;
    LHCustomDiseasGrid.canChk = 0;
    //LHCustomDiseasGrid.selBoxEventFuncName = "showOne";

    LHCustomDiseasGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomDiseasGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
