<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：HealthContCheckRpt.jsp
//程序功能：F1报表生成
//创建日期：2006-12-08
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%@page import="java.io.*"%>
<%
		System.out.println("start");
  	CError cError = new CError();
  	boolean operFlag=true;
  	
		String tRela  = "";
		String FlagStr = "";
		String Content = "";
		String strOperation = "";
  	String CustomerNo=request.getParameter("CustomerNoH");	
  	String transact = request.getParameter("fmtransact");
  	//String Begin = request.getParameter("cStartDate");
  	//String End = request.getParameter("cEndDate");
  	String InHospitNo = request.getParameter("InHospitNoH");
  	//System.out.println("FFFFFFFFfff "+InHospitNo);
  	
  	if (operFlag==true)
		{
	  	TransferData StartEnd = new TransferData();
      
	  	//StartEnd.setNameAndValue("Begin",Begin);
	  	//StartEnd.setNameAndValue("End",End);
	  	StartEnd.setNameAndValue("InHospitNo",InHospitNo);
   
	  	
	  	GlobalInput tG = new GlobalInput();
			tG = (GlobalInput)session.getValue("GI");
	
			LDPersonSchema tLDPersonSchema = new LDPersonSchema();
			tLDPersonSchema.setCustomerNo(CustomerNo);
			
			VData tVData = new VData();
			VData mResult = new VData();
			CErrors mErrors = new CErrors();
			
			tVData.addElement(tLDPersonSchema);
	    tVData.addElement(tG);
	    
	    tVData.addElement(StartEnd);
 
	    HealthContCheckRptUI tHealthContCheckRptUI = new HealthContCheckRptUI();
		  XmlExport txmlExport = new XmlExport();    
	    if(!tHealthContCheckRptUI.submitData(tVData,"PRINT"))
	    {
	       	operFlag=false;
	       	Content=tHealthContCheckRptUI.mErrors.getFirstError().toString();                 
	    }
	    else
	    {  
	    	System.out.println("--------成功----------");  
				mResult = tHealthContCheckRptUI.getResult();			
		  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
		  	if(txmlExport==null)
		  	{
		   		operFlag=false;
		   		Content="没有得到要显示的数据文件";	  
		  	}
			}
		
			ExeSQL tExeSQL = new ExeSQL();
			//获取临时文件名
			String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
			String strFilePath = tExeSQL.getOneValue(strSql);
			String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
			//获取存放临时文件的路径
			//strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
			//String strRealPath = tExeSQL.getOneValue(strSql);
			String strRealPath = application.getRealPath("/").replace('\\','/');
			String strVFPathName = strRealPath + "//" +strVFFileName;
	
			CombineVts tcombineVts = null;	
		
			//合并VTS文件
			String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
			tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
		
			ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
			tcombineVts.output(dataStream);
		
			//把dataStream存储到磁盘文件
			//System.out.println("存储文件到"+strVFPathName);
			AccessVtsFile.saveToFile(dataStream,strVFPathName);
			System.out.println("==> Write VTS file to disk ");
		
			System.out.println("===strVFFileName : "+strVFFileName);
		//本来打算采用get方式来传递文件路径
			response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=03&RealPath="+strVFPathName);
	}
	
	else
	{
    	FlagStr = "Fail";

%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
	
	//window.opener.afterSubmit("<%=FlagStr%>","<%=Content%>");	
	
</script>
</html>
<%
  	}

%>