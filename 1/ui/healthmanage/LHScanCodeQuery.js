	//该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
	if(fm.CustomerNo.value == null || fm.CustomerNo.value == "")
	{
		alert("请选择客户");
		return false;
	}
	if( CustomerExit()== false )
	{return false;}
	if (verifyInput() == false)
    return false;
	var i = 0;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.submit();
	showInfo.close();
}
function download()
{
	if (verifyInput() == false)
    return false;
	var i = 0;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

//修改保存时校验客户是否存在
function CustomerExit()
{
	var cCustomerNo = fm.CustomerNo.value;  //客户代码
  var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
  var arrResult = easyExecSql(strSql);
  //	  	alert(arrResult);
  if (arrResult == null)
  {
   	  alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
   	  return false;
  }
}
function ScanQuery()
{
	//alert("查询开始!");
	if(fm.all('ScanType').value==""||fm.all('ScanType').value=="null"||fm.all('ScanType').value==null)
	{
		alert("请选择扫描件类型!");
	}
         var com="";
        if(manageCom != null || !manageCom.equals(""))
       {
              com=" and m.Managecom like '"+manageCom+"%'";
        }
	if(fm.all('ScanType').value=="1")
	{
		var CustomerName4="";

		if(fm.all('CustomerName').value != "")
  	 	{
			CustomerName4 =" and exists ( select distinct customerno from  ldperson  where b.RelationNo=customerno and name = '"+fm.all('CustomerName').value+"')";
  	 	}
        if(fm.all('ScanStatus').value=="3")
		{
			var strSQL = "select distinct (select distinct ldperson.name||'--'||ldperson.customerno from ldperson where ldperson.customerno = b.RelationNo)  ,"
	          	+ "(select distinct CodeName from ldcode a where a.Code=m.Subtype and codetype = 'lhscantype' ),"
	          	+" SerialNo,m.Makedate,m.Maketime,"
	          	+"  (case InsertState when '1' then '全部录入' when '2' then '部分录入' when '3' then '尚未录入'  else '' end ),"
	          	+" m.Subtype ,SerialNo,busstype,b.RelationType,RelationNo2, "
	          	+"(select distinct ldperson.customerno from ldperson where ldperson.customerno = b.RelationNo), b.Comment "
	          	+ " from LHScanInfo  b ,ES_DOC_MAIN m  "
	          	+" where  b.SerialNo=m.doccode and m.Subtype='HM20' and (b.InsertState='' or b.InsertState='3') and 1=1  "
	          	+ getWherePart("b.RelationNo","CustomerNo","like" )
	          	//+ getWherePart("b.InsertState" ,"ScanStatus","like")
	          	+ getWherePart("m.doccode","ScanTestNO","like")
	          	+ getWherePart("b.makedate","StartDate",">")
	          	+ getWherePart("b.makedate","EndDate","<")
	          	+CustomerName4
                +com
	          	;
	           turnPage.queryModal(strSQL,LHScanCodeQueryGrid);
         }else {
                var strSQL = " select distinct (select distinct ldperson.name||'--'||ldperson.customerno from ldperson where ldperson.customerno = b.RelationNo)  ,"
                +" (select distinct CodeName from ldcode a where a.Code=m.Subtype and codetype = 'lhscantype'),"
                +" SerialNo,m.Makedate,m.Maketime,"
                +" (case InsertState when '1' then '全部录入' when '2' then '部分录入' when '3' then '尚未录入'  else '' end ),"
                +" m.Subtype ,SerialNo,busstype,b.RelationType,RelationNo2, "
                +" (select distinct ldperson.customerno from ldperson where ldperson.customerno = b.RelationNo),b.Comment "
                +" from LHScanInfo  b ,ES_DOC_MAIN m  "
                +" where  b.SerialNo=m.doccode and m.Subtype='HM20' and 1=1  "
                + getWherePart("b.RelationNo","CustomerNo","like" )
                + getWherePart("b.InsertState" ,"ScanStatus","like")
                + getWherePart("m.doccode","ScanTestNO","like")
                + getWherePart("b.makedate","StartDate",">")
                + getWherePart("b.makedate","EndDate","<")
                +CustomerName4
                +com
                ;
                turnPage.queryModal(strSQL,LHScanCodeQueryGrid);
                }

	}
	if(fm.all('ScanType').value=="2")
	{
		var CustomerName6="";
		if(fm.all('CustomerName').value != "")
		{
			CustomerName6 =" and  exists ( select 1 from  ldperson  where b.RelationNo=customerno and name = '"+fm.all('CustomerName').value+"')";
  	 	}
               if(fm.all('ScanStatus').value=="3")
      	       {
			var strSQL = "select distinct (select distinct ldperson.name||'--'||ldperson.customerno from ldperson where ldperson.customerno = b.RelationNo) ,"
	   				+ "(select distinct CodeName from ldcode a where a.Code=m.Subtype and codetype = 'lhscantype'),"
	   				+" SerialNo,m.Makedate,m.Maketime,"
	   				+"  (case InsertState when '1' then '全部录入' when '2' then '部分录入' when '3' then '尚未录入'  else '' end ),"
	   				+" m.Subtype ,SerialNo,busstype,b.RelationType,RelationNo2,"
	   				+"(select distinct ldperson.customerno from ldperson where ldperson.customerno = b.RelationNo), b.Comment "
	   				+ " from LHScanInfo  b ,ES_DOC_MAIN m  "
	   				+" where  b.SerialNo=m.doccode   and m.Subtype='HM10' and (b.InsertState='' or b.InsertState is null or b.InsertState='3') and 1=1"
	   				+ getWherePart("b.RelationNo","CustomerNo","like" )
	   				//+ getWherePart("b.InsertState" ,"ScanStatus","like")
	   				+ getWherePart("m.doccode","ScanTestNO","like")
	   				+ getWherePart("b.makedate","StartDate",">")
	   				+ getWherePart("b.makedate","EndDate","<")
	   				+CustomerName6
                    +com
	   				;
	   	//alert(strSQL);
	   	//alert(easyExecSql(strSQL));
		turnPage.queryModal(strSQL,LHScanCodeQueryGrid);
               }else {
               var strSQL = "select distinct (select distinct ldperson.name||'--'||ldperson.customerno from ldperson where ldperson.customerno = b.RelationNo) ,"
               + "(select distinct CodeName from ldcode a where a.Code=m.Subtype and codetype = 'lhscantype' ),"
               +" SerialNo,m.Makedate,m.Maketime,"
               +" (case InsertState when '1' then '全部录入' when '2' then '部分录入' when '3' then '尚未录入'  else '' end ),"
               +" m.Subtype ,SerialNo,busstype,b.RelationType,RelationNo2,"
               +"(select distinct ldperson.customerno from ldperson where ldperson.customerno = b.RelationNo),b.Comment "
               +" from LHScanInfo  b ,ES_DOC_MAIN m  "
               +" where  b.SerialNo=m.doccode   and m.Subtype='HM10'  and 1=1"
               + getWherePart("b.RelationNo","CustomerNo","like" )
               + getWherePart("b.InsertState" ,"ScanStatus","like")
               + getWherePart("m.doccode","ScanTestNO","like")
               + getWherePart("b.makedate","StartDate",">")
               + getWherePart("b.makedate","EndDate","<")
               +CustomerName6
               +com
               ;
               turnPage.queryModal(strSQL,LHScanCodeQueryGrid);
               }

	 }
	if(fm.all('ScanType').value=="3")
	{
		var CustomerNo3 = "";
		var ScanStatus3="";
		var CustomerName7="";
	    if(fm.all('CustomerNo').value != "")
	    {
		     CustomerNo3 =" and exists (select 1 from LHScanInfo b where b.SerialNo=c.Doccode and b.RelationNo = '"+fm.all('CustomerNo').value+"')";
	    }
	    if(fm.all('ScanStatus').value != "")
	    {
		     ScanStatus3 =" and exists (select 1 from LHScanInfo b where b.SerialNo=c.Doccode and b.InsertState = '"+fm.all('ScanStatus').value+"')";
	    }	    
  	    if(fm.all('CustomerName').value != "")
  	    {
  	 	      CustomerName7 =" and exists (select 1 from LHScanInfo b where b.SerialNo=c.Doccode and exists ( select distinct customerno from  ldperson a"
  	 	                    +" where a.customerno=b.RelationNo and a.name = '"+fm.all('CustomerName').value+"'))";
  	    }
  	    if(fm.all('ScanStatus').value=="3")
		{
			//ScanStatus4 =" and c.doccode in ( select distinct SerialNo from  LHScanInfo where InsertState = '3' or InsertState = '')";
			var strSQL = "select '',"
			           //   +"(select distinct ldperson.name||'--'||ldperson.customerno from ldperson ,lhscaninfo b where ldperson.customerno = b.RelationNo and b.serialno=c.doccode) ,"
				      	+"(select distinct CodeName from ldcode a where a.Code=c.Subtype and codetype = 'subtypedetail'  and c.Subtype like 'TB%%'),"
				      	+" doccode,Makedate,Maketime,''"
				      //	+" (select case  when  a.InsertState='1' then '全部录入' when a.InsertState='2' then '部分录入' when a.InsertState='3' then '尚未录入'  else '' end from lhscaninfo a where a.serialno=c.doccode ),"
				      	+" ,Subtype, doccode, Busstype,'','','','' "
				      //	+" (select RelationType from LHScanInfo  a where a.SerialNo=c.Doccode),"
				      //	+"(select RelationNo2 from LHScanInfo  a where a.SerialNo=c.Doccode),"
				      //    +"(select distinct ldperson.customerno from ldperson ,lhscaninfo b where ldperson.customerno = b.RelationNo and b.serialno=c.doccode), "
				      //    +"(select Comment from LHScanInfo d where d.SerialNo=c.doccode) "
				      	+" from ES_DOC_MAIN c "
				      	+" where Subtype='TB15' and c.doccode not in ( select distinct SerialNo from  LHScanInfo a ) "
                                        +" and c.Managecom like '"+manageCom+"%' "
				      	+ getWherePart("c.doccode","ScanTestNO","like")
				      	+ getWherePart("c.makedate","StartDate",">")
				      	+ getWherePart("c.makedate","EndDate","<")
				      	//+ CustomerNo3
				      	//+ ScanStatus4
				      	//+CustomerName7

				      	;
				      	//alert(strSQL);
			turnPage.queryModal(strSQL,LHScanCodeQueryGrid);
		}
		else
		{
			var strSQL = "select (select distinct ldperson.name||'--'||ldperson.customerno from ldperson,LHScanInfo b where ldperson.customerno = b.RelationNo and b.serialno=c.doccode) ,"
				   	+"(select distinct CodeName from ldcode a where a.Code=c.Subtype and codetype = 'subtypedetail'),"
				   	+" doccode,c.Makedate,c.Maketime,"
				   	+" (select case  when  b.InsertState='1' then '全部录入' when b.InsertState='2' then '部分录入' when b.InsertState='3' then '尚未录入'  else '' end from LHScanInfo b where c.doccode=b.serialno),"
				   	+" c.Subtype,doccode,c.Busstype,"
				   	+" (select b.RelationType from LHScanInfo b where c.doccode=b.serialno),"
				   	+" (select b.RelationNo2 from LHScanInfo b where c.doccode=b.serialno),"
				   	+"(select distinct ldperson.customerno from ldperson,LHScanInfo b where ldperson.customerno = b.RelationNo and c.doccode=b.serialno), "
				   	+" (select b.Comment from LHScanInfo b where c.doccode=b.serialno)"
				   	+" from ES_DOC_MAIN c"
				   	+" where c.Subtype='TB15' and 1=1 "
                    +" and c.Managecom like '"+manageCom+"%'"
				   	+ getWherePart("c.doccode","ScanTestNO","like")
				   	+ getWherePart("c.makedate","StartDate",">")
				   	+ getWherePart("c.makedate","EndDate","<")
				   	+ CustomerNo3
				   	+ ScanStatus3
				   	+CustomerName7
				   	;
				   	//alert(strSQL);
			turnPage.queryModal(strSQL,LHScanCodeQueryGrid);
		}

	 }
	if(fm.all('ScanType').value=="4")
	{
		var CustomerNo2 = "";
	 	var CustomerName9="";
	 	var ScanStatus2="";
		if(fm.all('CustomerNo').value != "")
		{
 	       CustomerNo2 =" and exists ( select 1 from  LLCaseCure where CaseNo = c.doccode and CustomerNo = '"+fm.all('CustomerNo').value+"')";
		}
	    if(fm.all('CustomerName').value != "")
	    {
  	      CustomerName9 =" and exists ( select 1 LLCaseCure  where  CaseNo = c.doccode and CustomerName = '"+fm.all('CustomerName').value+"')";
	    }
	    if(fm.all('ScanStatus').value != "")
	    {
	  		ScanStatus2 =" and exists (select 1 from LHScanInfo where SerialNo = c.doccode and InsertState = '"+fm.all('ScanStatus').value+"')";
	    }
	    var aFeeAtti="";//账单属性
	    var bFeeAtti="";
		if(fm.all('FeeAtti').value != "")
		{
 	       aFeeAtti = " and exists (select 1 from  llfeemain where caseno = s.otherno and FeeAtti ='"+fm.all('FeeAtti').value+"')";
		   bFeeAtti = " and exists (select 1 from llfeemain where  caseno = c.doccode and FeeAtti ='"+fm.all('FeeAtti').value+"')";
		}
		var aFeeType="";//账单种类
		var bFeeType="";
		if(fm.all('FeeType').value != "")
		{
 	       aFeeType = " and exists (select 1 from  llfeemain where caseno = s.otherno and FeeType ='"+fm.all('FeeType').value+"')";
		   bFeeType = " and exists (select 1 from llfeemain where  caseno = c.doccode and FeeType ='"+fm.all('FeeType').value+"')";
		}
		var HelthRisk="";//是否为健管险种
		var HelthRiskCode="";
        if(fm.all('HelthRisk').value != "")
        {
            if(fm.all('HelthRisk').value=="1")
            {
 	           HelthRisk =" and exists (select 1 from llcasepolicy y where y.caseno=s.otherno and exists (select 1 from lmriskapp p where p.riskcode=y.riskcode and p.risktype2='5' ))";
 	           HelthRiskCode =" and exists (select 1 from llcasepolicy y where y.caseno=c.doccode and exists (select 1 from lmriskapp p where p.riskcode=y.riskcode and p.risktype2='5' ))";
 	        }
 	        if(fm.all('HelthRisk').value=="2")
            {
 	           HelthRisk =" and exists (select 1 from llcasepolicy y where y.caseno=s.otherno and not exists (select 1 from lmriskapp p where p.riskcode=y.riskcode and p.risktype2='5' ))";
 	           HelthRiskCode =" and exists (select 1 from llcasepolicy y where y.caseno=c.doccode and not exists (select 1from lmriskapp p where p.riskcode=y.riskcode and p.risktype2='5' ))";
            }
        }
       if(fm.all('ScanStatus').value=="3")
		{
			//ScanStatus4 =" and c.doccode in ( select distinct SerialNo from  LHScanInfo where InsertState = '3' or InsertState = '')";
			var strSQL = " select (select distinct ldperson.name||'--'||ldperson.customerno from   ldperson  where  ldperson.customerno =s.RelationNo) as  AA, "
					+"  (select distinct a.CodeName from  ldcode a  where  a.Code=c.Subtype and a.codetype = 'subtypedetail' and c.Subtype='LP01'), "
					+"  s.serialno,c.Makedate,c.Maketime,"
					+" (case s.InsertState when '1' then '全部录入' when '2' then '部分录入' when '3' then '尚未录入'  else '' end ), "
					+"  c.Subtype, s.Otherno, c.Busstype,'','', "
					+" (select distinct ldperson.customerno from ldperson where ldperson.customerno = s.RelationNo ),  "
					+"  s.Comment  "
					+"  from ES_DOC_MAIN c, lhscaninfo s "
					+"  where c.Subtype='LP01'   and (s.InsertState='' or s.InsertState='3') and c.doccode = s.otherno "
                    +" and c.Managecom like '"+manageCom+"%'"
					+ getWherePart("s.SerialNo","ScanTestNO")
					+ getWherePart("c.makedate","StartDate",">")
					+ getWherePart("c.makedate","EndDate","<")
	                + getWherePart("s.RelationNo","CustomerNo")
	                + aFeeAtti
					+ aFeeType
					+ HelthRisk
				 	+"  union "
				 	+"  select (select distinct ldperson.name||'--'||ldperson.customerno from ldperson ,lhscaninfo b  where  ldperson.customerno = b.RelationNo and b.serialno=c.doccode) as AA, "
					+"  (select distinct a.CodeName from ldcode a  where  a.Code=c.Subtype and codetype = 'subtypedetail' ), "
					+"   c.doccode,c.Makedate,c.Maketime,"
					+ "	(select case  when  a.InsertState='1' then '全部录入'  when a.InsertState='2'  then '部分录入' when a.InsertState='3' then '尚未录入' else '' end  from lhscaninfo a where a.serialno=c.doccode ), "
					+"   c.Subtype,c.doccode, c.Busstype,'','', "
					+"  (select distinct ldperson.customerno from ldperson ,lhscaninfo b  where ldperson.customerno = b.RelationNo and b.serialno=c.doccode), "
					+"  (select d.Comment from LHScanInfo d where d.SerialNo=c.doccode) "
					+"  from ES_DOC_MAIN c"
					+"  where c.Subtype='LP01' "
                                        +" and c.Managecom like '"+manageCom+"%'"
					+"  and c.doccode not in  (select distinct otherno from  lhscaninfo where relationtype = '4')  "
					+" and c.doccode not in ( select distinct SerialNo from  LHScanInfo a )"
					+ getWherePart("c.doccode","ScanTestNO")
					+ getWherePart("c.makedate","StartDate",">")
					+ getWherePart("c.makedate","EndDate","<")
					//+ getWherePart("f.FeeAtti","FeeAtti")
					//+ getWherePart("f.FeeType","FeeType")
					+ bFeeAtti
					+ bFeeType
					+ CustomerNo2
					+ HelthRiskCode  
 				    //+" and NOT (f.feeatti in ('2','3') and f.feetype = '1') "
					;
					//fm.all('HelthRiskName').value=strSQL;
					//alert(strSQL);
					//alert(easyExecSql(strSQL));
					turnPage.queryModal(strSQL,LHScanCodeQueryGrid);
		}else{
	 	var strSQL = " select (select distinct ldperson.name||'--'||ldperson.customerno from   ldperson  where  ldperson.customerno =s.RelationNo) as AA, "
					  +"  (select distinct a.CodeName from  ldcode a  where  a.Code=c.Subtype and a.codetype = 'subtypedetail'), "
				    +"  s.serialno,c.Makedate,c.Maketime,"
				    +" (case s.InsertState when '1' then '全部录入' when '2' then '部分录入' when '3' then '尚未录入'  else '' end ), "
					  +"  c.Subtype, s.Otherno, c.Busstype,'','', "
				    +" (select distinct ldperson.customerno from ldperson where ldperson.customerno = s.RelationNo ),  "
				    +"  s.Comment  "
				    +"  from ES_DOC_MAIN c, lhscaninfo s "
				    +"  where c.Subtype='LP01' and c.doccode = s.otherno "
                    +" and c.Managecom like '"+manageCom+"%'"
				    + getWherePart("s.otherno","ScanTestNO")
				    + getWherePart("s.InsertState","ScanStatus")
				    + getWherePart("c.makedate","StartDate",">")
				    + getWherePart("c.makedate","EndDate","<")
				   	+ getWherePart("s.RelationNo","CustomerNo")
				   	+ aFeeAtti
				   	+ aFeeType
				   	+ HelthRisk
				   	+" union "
				   	+" select (select distinct ldperson.name||'--'||ldperson.customerno from ldperson ,lhscaninfo b  where  ldperson.customerno = b.RelationNo and b.serialno=c.doccode) as AA, "
				   	+" (select distinct a.CodeName from ldcode a  where  a.Code=c.Subtype and codetype = 'subtypedetail'), "
				   	+" c.doccode,c.Makedate,c.Maketime,"
				   	+"(select case  when  a.InsertState='1' then '全部录入'  when a.InsertState='2'  then '部分录入' when a.InsertState='3' then '尚未录入' else '' end  from lhscaninfo a where a.serialno=c.doccode ), "
				   	+" c.Subtype,c.doccode, c.Busstype,'','', "
				   	+" (select distinct ldperson.customerno from ldperson ,lhscaninfo b  where ldperson.customerno = b.RelationNo and b.serialno=c.doccode), "
				   	+" (select d.Comment from LHScanInfo d where d.SerialNo=c.doccode) "
				   	+" from ES_DOC_MAIN c"
				   	+" where c.Subtype='LP01' "
                    +" and c.Managecom like '"+manageCom+"%'"
				   	+" and not exists (select 1 from  lhscaninfo where otherno=c.doccode and relationtype = '4')  "
				   	+ getWherePart("c.doccode","ScanTestNO")
				   	+ getWherePart("c.makedate","StartDate",">")
				   	+ getWherePart("c.makedate","EndDate","<")
				//  + getWherePart("f.FeeAtti","FeeAtti")
				//  + getWherePart("f.FeeType","FeeType")
				    + bFeeAtti
					+ bFeeType
				   	+ CustomerNo2
				   	+ ScanStatus2
				   	+ HelthRiskCode
				//	+"  and f.caseno = c.doccode "
				//  +" and NOT (f.feeatti in ('2','3') and f.feetype = '1') "
				   	;

				   	//alert(strSQL);
				   	//fm.all('HelthRiskName').value=strSQL;
				   	//alert(easyExecSql(strSQL));
				   	turnPage.queryModal(strSQL,LHScanCodeQueryGrid);
		}
	}
}

function ScanImport()
{
	  if (LHScanCodeQueryGrid.getSelNo() >= 1)
		{
			  if(fm.all('ScanType').value=="4")
			  {
			    var CustomerNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,1);
		        //alert(CustomerNo);
		        if(CustomerNo==""||CustomerNo==null||CustomerNo=="null")
		        {
			      alert("请选择一条理赔信息不为空的记录（即客户号不为空！）");
			      return false;
	            }
	          }
				 var RelationType=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,10);
				 var RelationNo2=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,11);
							//alert(RelationNo2);
				 fm.all('RelationType').value=RelationType;
				 if(fm.all('ScanType').value==null||fm.all('ScanType').value=="null"||fm.all('ScanType').value=="")
				 {
					 alert("请选择扫描件类型!");
					 return false;
				 }
				 else
				 {
					 //alert(fm.all('ScanType').value);
					var ScanType=fm.all('ScanType').value;//文本框扫描件类型1,就诊2,体检3,核保体检
				    var ScanNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,7);//扫描件类型编号 subtype

					var BussType=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,9);//扫描件类型编号 //busstype

					var prtNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,3);////扫描印刷号
				    var docid = "";
					docid = easyExecSql("select distinct docid from es_doc_main where doccode = '"+prtNo+"'");
				     if(fm.all('ScanType').value=="1")
					{
						showInfo=window.open("./LHScanCodeInfoMain.jsp?DocID="+docid+"&ScanType="+ScanType+"&ScanNo="+ScanNo+"&prtNo="+prtNo+"&BussType="+BussType+"","扫描件录入页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		            }
		            if(fm.all('ScanType').value=="2")
				    {
						showInfo=window.open("./LHScanCodeInfoMain.jsp?DocID="+docid+"&ScanType="+ScanType+"&ScanNo="+ScanNo+"&prtNo="+prtNo+"&BussType="+BussType+"","扫描件录入页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		            }
				   if(fm.all('ScanType').value=="3")
					{
						showInfo=window.open("./LHScanCodeInfoMain.jsp?DocID="+docid+"&ScanType="+ScanType+"&ScanNo="+ScanNo+"&prtNo="+prtNo+"&BussType="+BussType+"","扫描件录入页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		            }
		            if(fm.all('ScanType').value=="4")
				   {
						var OtherNo = "";
						var SerNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,8);
						//alert("PP"+SerNo);
						OtherNo = easyExecSql("select distinct OtherNo from LHScanInfo where OtherNo = '"+SerNo+"'");
						//alert("OO"+OtherNo);
						var SerialNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,3);////扫描印刷号
						//alert(SerialNo);
						var docid2 = "";
						docid2 = easyExecSql("select distinct docid from es_doc_main where doccode = '"+OtherNo+"'");
						showInfo=window.open("./LHScanCodeInfoMain.jsp?DocID="+docid2+"&ScanType="+ScanType+"&ScanNo="+ScanNo+"&prtNo="+OtherNo+"&BussType="+BussType+"&SerialNo="+SerialNo+"","扫描件录入页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		           }
		          }

		 }
		else
		{
			alert("请选择一条要扫描的记录！");
		}

}
function afterCodeSelect(codeName,Field)
{
	//alert(codeName);
	//alert(Field.value);
	if(codeName=="ScanType" )
	{
		if(Field.value=="1")
		{
		  divInsertHeBao.style.display="none";
		  divInsertLiPei.style.display="none";
		  divContInput.style.display="none";
		  divLHContTypeGrid.style.display='none';//不显示保单类型查询控件
		}
		if(Field.value=="2")
		{
			divInsertHeBao.style.display="none";
			divInsertLiPei.style.display="none";
			divContInput.style.display="none";
			divLHContTypeGrid.style.display='none';//不显示保单类型查询控件
		}
		if(Field.value=="3")
		{
			divInsertHeBao.style.display="";
			divInsertLiPei.style.display="none";
			divContInput.style.display="";
			divLHContTypeGrid.style.display='none';//不显示保单类型查询控件
		}
	  if(Field.value=="4")
		{
		  divInsertHeBao.style.display="none";
		  divInsertLiPei.style.display="";
		  divContInput.style.display="none";
		  divLHContTypeGrid.style.display='';//显示按扫描件类型控件
		}

	}
}
function InsertHeBao()
{
	if (LHScanCodeQueryGrid.getSelNo() >= 1)
	{
		var CustomerNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,1);
		//alert(CustomerNo);
		  if(CustomerNo!=""&&CustomerNo!=null&&CustomerNo!="null")
		  {
			   alert("请选择一条空的核保记录信息 (即客户姓名为空)");
	    }
	    else
	    {
	  	     var prtNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,3);////扫描印刷号
	  	     var strSQL = " select a.Customerno from lcpenotice a,  es_doc_main b"
			     	   				+"  where a.prtseq = b.doccode and b.doccode='"+prtNo+"'"
			     	   				;
			     //alert(strSQL);
		       var arrResult = easyExecSql(strSQL);
		       //alert(arrResult);
		       if(arrResult==""||arrResult=="null"||arrResult==null)
		       {
		       	  alert("此条核保信息不存在，不能导入!");
		       }
		       else
		       {
		       	    var RelationType=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,10);
							  var RelationNo2=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,11);
							  //alert(RelationNo2);
							  fm.all('RelationType').value=RelationType;
							  if(fm.all('ScanType').value==null||fm.all('ScanType').value=="null"||fm.all('ScanType').value=="")
							  {
							  	alert("请选择扫描件类型!");
							  	return false;
							  }
							  else
						    {
						    	  //alert(fm.all('ScanType').value);
							      var ScanType=fm.all('ScanType').value;//文本框扫描件类型1,就诊2,体检3,核保体检
							      var ScanNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,7);//扫描件类型编号 subtype

							      var BussType=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,9);//扫描件类型编号 //busstype

							      var prtNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,3);////扫描印刷号
							      var docid = "";
							      docid = easyExecSql("select distinct docid from es_doc_main where doccode = '"+prtNo+"'");
							      //alert(BussType);
							      showInfo=window.open("./LHScanCodeInfoMain.jsp?DocID="+docid+"&ScanType="+ScanType+"&ScanNo="+ScanNo+"&prtNo="+prtNo+"&BussType="+BussType+"&flag=1","扫描件录入页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		            }
		       }

	    }
   }
   else
   {
			alert("请选择一条要导入的记录！");
	 }
}

function InsertLiPei()
{
	if (LHScanCodeQueryGrid.getSelNo() >= 1)
	{
		var CustomerNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,1);
		//alert(CustomerNo);
		if(CustomerNo!=""&&CustomerNo!=null&&CustomerNo!="null")
		{
			alert("请选择一条空的理赔记录信息 (即客户姓名为空)");
	    }
	    else
	    {
			var prtNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,3);////扫描印刷号
			var strSQL = " select a.Customerno from llcase a,  es_doc_main b"
						+"    where a.caseno = b.doccode and b.doccode='"+prtNo+"'"
     	   				;
			var arrResult = easyExecSql(strSQL);
			if(arrResult==""||arrResult=="null"||arrResult==null)
			{
				alert("此条理赔信息下的客户信息不存在，不能导入!");
			}
			else
			{
		   		var RelationType=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,10);
				var RelationNo2=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,11);
    			fm.all('RelationType').value=RelationType;
				if(fm.all('ScanType').value==null||fm.all('ScanType').value=="null"||fm.all('ScanType').value=="")
				{
				  	alert("请选择扫描件类型!");
				  	return false;
				}
				else
				{
					var ScanType=fm.all('ScanType').value;//文本框扫描件类型1,就诊2,体检3,核保体检,4,理赔
					var ScanNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,7);//扫描件类型编号 subtype
					var BussType=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,9);//扫描件类型编号 //busstype
					var prtNo=LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,3);////扫描印刷号
					var docid = "";
					docid = easyExecSql("select distinct docid from es_doc_main where doccode = '"+prtNo+"'");
       			    showInfo=window.open("./LHScanCodeInfoMain.jsp?DocID="+docid+"&ScanType="+ScanType+"&ScanNo="+ScanNo+"&prtNo="+prtNo+"&BussType="+BussType+"&flag=2","扫描件录入页面理赔",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		        }
		    }
	    }
	}
	else
	{
		alert("请选择一条要导入的记录！");
	}
}

function inputCont()
{	//从核保体检进入契约录入
	if(LHScanCodeQueryGrid.getSelNo() < 1)
	{
		alert("请先选择一条保单信息");
		return false;
	}

	var TempPrtNo = LHScanCodeQueryGrid.getRowColData(LHScanCodeQueryGrid.getSelNo()-1,3);
	var Sql = " select contno, Customerno,ProposalContNo from LCPENotice where prtseq = '"+TempPrtNo+"'";

	var arrResult = easyExecSql(Sql);
	var tContNo = arrResult[0][0];
	var tCustomerNo = arrResult[0][1];
	//由于契约在核保后的签单会发生变号，所以用 ProposalContNo
	var tProposalContNo = arrResult[0][2];

	Sql = " select sum(Prem) from lcpol where  ProposalContNo = '"+tProposalContNo+"' and insuredno = '"+tCustomerNo+"' ";
	var PersonFee =  easyExecSql(Sql);

	if(tProposalContNo == "" || tProposalContNo=="null" || tCustomerNo =="" || tCustomerNo == "null" || TempPrtNo == "" || TempPrtNo =="null")
	{
		alert("扫描件严重错误:保单号、客户号或印刷号错误，与管理员联系");
		return false;
	}
	window.open("LHServPlanInputMain.jsp?ContNo="+tProposalContNo+"&PrtNo="+TempPrtNo+"&CustomerNo="+tCustomerNo+"&PersonFee="+PersonFee+"&flag=NCONT","",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
}
