/** 
 * 程序名称：LHServiceRecordQuery.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-06-18 09:08:27
 * 创建人  ：hm
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     

//  var strSql="select distinct a.CustomerNo,b.name, a.InHospitNo, d.HospitName,a.InHospitDate,"
//  +" e.DoctName,a.InHospitMode,c.MainCureMode"
//  +" from LHCustomInHospital a, ldperson b,LHDiagno c,LDHospital d,LDDoctor e where   "
//  +" a.CustomerNo=b.CustomerNo and b.CustomerNo=c.CustomerNo and a.InHospitNo = c.InHospitNo and a.HospitCode=d.HospitCode"
//  +" and c.DoctNo=e.DoctNo "
//  +getWherePart("a.CustomerNo","CustomerNo") 
//  +getWherePart("c.CustomerNo","CustomerNo")
//  +getWherePart("b.CustomerNo","CustomerNo")
//  ;

//	turnPage.queryModal(strSql,  LHCustomInHospitalGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}


function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}


function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.jsp");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}


function queryCustomerNo2()
{
	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
	var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}

function query()
{
		var SQL;
		
//记录时间
	var str_FirstRecoDate = null;
	if(fm.FirstRecoDate.value != "" )
		{str_FirstRecoDate = " a.FirstRecoDate "+fm.FirstRecoDate_h.value+"'"+fm.FirstRecoDate.value+"'";}
	else{str_FirstRecoDate = "1=1";}

//服务时间		
	var str_ServiceDate = null;
	if(fm.ServiceDate.value != "" )
		{str_ServiceDate = " a.ServiceDate "+fm.ServiceDate_h.value+"'"+fm.ServiceDate.value+"'";}
	else{str_ServiceDate = "1=1";}	
		
		
		
		SQL =  " select a.CustomerNo,c.Name,a.FirstRecoDate,a.ServiceState,a.ApplyDutyItemCode,"
					+" b.HospitName,a.ServiceFlag,a.IsBespeakOk,a.ServiceExecState,a.RecordNo "
					+" from LHMedicalServiceRecord a,LDHospital b ,LDPerson c "
					+"where a.CustomerNo = c.CustomerNo   and   a.HospitCode = b.HospitCode "
					+getWherePart("a.CustomerNo","CustomerNo")
//					+getWherePart("c.Name","CustomerName")
					+" and " +str_FirstRecoDate
					+getWherePart("a.ServiceState","ServiceState")
					+" and "+str_ServiceDate
					+getWherePart("a.HospitCode","HospitCode")
					+getWherePart("a.ApplyReceiveType","ApplyReceiveType")
					+getWherePart("a.ServiceFlag","ServiceFlag")
					+getWherePart("a.IsBespeakOk","IsBespeakOk")
					+getWherePart("a.ExcuteServiceHospit","ExcuteServiceHospit")
					+getWherePart("a.ServiceExecState","ServiceExecState")
					;
		
		
		window.open("../healthmanage/LHServiceRecordQueryOpen.jsp?SQL="+SQL);
}


function clearInput()      
{
	fm.all('CustomerNo').value = "";
	fm.all('CustomerName').value = "";
	fm.all('FirstRecoDate_h').value = "";
	fm.all('FirstRecoDate_sp').value = "";
	fm.all('FirstRecoDate').value = "";
	fm.all('ServiceState').value = "";
	fm.all('ServiceState_ch').value = "";
	fm.all('ServiceDate_h').value = "";
	fm.all('ServiceDate_sp').value = "";
	fm.all('ServiceDate').value = "";
	fm.all('HospitCode').value = "";
	fm.all('HospitName').value = "";
	fm.all('ApplyReceiveType').value = "";
	fm.all('ApplyReceiveType_ch').value = "";
	fm.all('ServiceFlag').value = "";
	fm.all('ServiceFlag_ch').value = "";
	fm.all('IsBespeakOk').value = "";
	fm.all('IsBespeakOk_ch').value = "";
	fm.all('ExcuteServiceItem').value = "";
	fm.all('ExcuteServiceItemName').value = "";
	fm.all('ExcuteServiceHospit').value = "";
	fm.all('ExcuteServiceHospitName').value = "";
	fm.all('ServiceExecState').value = "";
	fm.all('ServiceExecState_ch').value = "";
//	fm.all('').value = "";
//	fm.all('').value = "";
//	fm.all('').value = "";
//	fm.all('').value = "";
//	fm.all('').value = "";
//	fm.all('').value = "";
//	fm.all('').value = "";
//	fm.all('').value = "";
}


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	//var arrResult = new Array();
	var strSQL = "select ICDCode,ICDName,FrequencyFlag,ChronicMark from LDDisease where ICDCode='"+arrQueryResult[0][0]+"'";
	arrResult = easyExecSql(strSQL);
	
	if( arrResult != null )
	{
        fm.all('ICDCode').value= arrResult[0][0];                                   
        fm.all('ICDName').value= arrResult[0][1];  
        fm.all('FrequencyFlag').value= arrResult[0][2];                    
        fm.all("ChronicMark").value= arrResult[0][3]; 
        if(arrResult[0][2]=="5")
        {
        		ChronicMark1.style.display='';
        		if(arrResult[0][3]=="0"){fm.all("ChronicMark_ch").value="否"}
        		if(arrResult[0][3]=="1"){fm.all("ChronicMark_ch").value="是"}
        } 
	}
} 
