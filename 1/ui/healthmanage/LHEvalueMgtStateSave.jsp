<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHEvalueMgtStateSave.jsp
//程序功能：
//创建日期：2006-09-20 16:15:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHEvalueMgtStateUI tLHEvalueMgtStateUI   = new LHEvalueMgtStateUI();
  
  LHTaskCustomerRelaSet tLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();		//服务预约管理表
    
    String tTaskExecNo[] = request.getParameterValues("LHEvalueMgtGrid10");           //任务实施号码流水号
    String tServTaskNo[] = request.getParameterValues("LHEvalueMgtGrid8");           //服务任务编号
    String tServCaseCode[] = request.getParameterValues("LHEvalueMgtGrid9");					//服务事件号  
    String tServTaskCode[] = request.getParameterValues("LHEvalueMgtGrid12");           //服务任务代码
    String tServItemNo[] = request.getParameterValues("LHEvalueMgtGrid11");					//服务项目序号
    String tTaskExecState[] = request.getParameterValues("LHEvalueMgtGrid7");					//服务执行状态	 
     
    String tChk[] = request.getParameterValues("InpLHEvalueMgtGridChk"); //参数格式=” Inp+MulLine对象名+Chk”     	 
     
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
	 System.out.println(" KKKKKKKKKKKKKKKKKKKKKKKKK "+transact);
   int LHEvalueMgtCount = 0;
   System.out.println("R R  "+tTaskExecNo.length);
	 if(tTaskExecNo != null)
	 {	
		    LHEvalueMgtCount = tTaskExecNo.length;
	 }	
	  System.out.println(" LHEvalueMgtCount is : "+LHEvalueMgtCount);
    for(int j = 0; j < LHEvalueMgtCount; j++)
	  {

		    LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema();
		    
		     tLHTaskCustomerRelaSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		     tLHTaskCustomerRelaSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		     //System.out.println("AAAAAAAAAAAAAAAAAAAAAAA " +tLHTaskCustomerRelaSchema.getServTaskNo());//任务代码
		     tLHTaskCustomerRelaSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		     tLHTaskCustomerRelaSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		     tLHTaskCustomerRelaSchema.setServItemNo(tServItemNo[j]);	//项目号码
		     tLHTaskCustomerRelaSchema.setTaskExecState(tTaskExecState[j]);	//服务执行状态  
         if(transact.equals("UPDATE||MAIN"))
         {
            if(tChk[j].equals("1"))  
            {        
               System.out.println("该行被选中 "+tTaskExecNo[j]);     
               tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);         
           }
        }   
      }
                              
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHTaskCustomerRelaSet);
  	tVData.add(tG);
    tLHEvalueMgtStateUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHEvalueMgtStateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	  var transact = "<%=transact%>";
	      parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>