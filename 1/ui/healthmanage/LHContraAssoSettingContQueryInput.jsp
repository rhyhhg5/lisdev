<%
//程序名称：LHContraAssoSettingContQueryInput.jsp
//程序功能：功能描述
//创建日期：2006-03-14 17:04:48
//创建人  ：郭丽颖
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHContraAssoSettingContQueryInput.js"></SCRIPT> 
  <%@include file="LHContraAssoSettingContQueryInputInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLHContraAssoSettingContQueryGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      合同编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraNo >
    </TD>
    <TD  class= title>
      合同名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraName >
    </TD>
    <!--TD  class= title>
      合作机构代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HospitCode >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      签订日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IdiogrDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      合同起始日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraBeginDate >
    </TD>
    <TD  class= title>
      合同终止日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraEndDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      合同状态
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraState >
    </TD>
    <TD  class= title>
      甲方签订人代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FirstPerson >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      乙方签订人
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SecondPerson >
    </TD>
    <TD  class= title>
      联系人
    </TD>
    <TD  class= input>
      <Input class= 'common' name=linkman >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      联系电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Phone >
    </TD-->
  </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"  class='button' TYPE=button   class=common onclick="easyQueryClick();">
          <INPUT VALUE="返回" class='button' TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHContraAssoSettingContQueryGrid1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHCont1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHContraAssoSettingContQueryGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>   
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
