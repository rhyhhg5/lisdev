<%
//程序名称：LHContPlanSettingMain.jsp
//程序功能：
//创建日期：2006-06-29 20:21:58
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%
	String ContNo = request.getParameter("ContNo");
	String PrtNo = request.getParameter("PrtNo");
	String CustomerNo = request.getParameter("CustomerNo");
	System.out.println("%%%%%%%%%%%%%%%%%%%%%%"+PrtNo);
	String flag = request.getParameter("flag");
	String subtype=null;
	String sql = "select subtype from es_doc_main where doccode='"+PrtNo+"'";
	SSRS res = new ExeSQL().execSQL(sql);
	if(res!=null && res.getMaxNumber()>0){
        if(res.GetText(1,1).equals("TB04")){
		  subtype="TB1004";
	    }else {
	    	 subtype="TB1001";
	    }
	}
%>
<!--Root="../../" -->
<html>
<head>
<title>Sinosoft </title>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<script language="javascript">
	var intPageWidth=screen.availWidth;
	var intPageHeight=screen.availHeight;
	window.resizeTo(intPageWidth,intPageHeight);
	window.focus();

	var initWidth = 0;
  	//图片的队列数组
	var pic_name = new Array();
	var pic_place = 0;
	var b_img	= 0;  //放大图片的次数
	var s_img = 0;	//缩小图片的次数

  	window.onunload = afterInput;

  	function afterInput()
  	{
    	try
    	{
      		top.opener.afterInput();
    	}
    	catch(e) {}
	}

  	var mainPolNo = "";
  	var mainRisk = "";
  	//查询扫描图片的描述
  	function queryScanType()
  	{
    	var strSql = "select code1, codename, codealias from ldcode1 where codetype='scaninput'";
    	return easyExecSql(strSql);
	}

</script>
</head>
<frameset name="fraMain" rows="0,45%,0,0,0,*" frameborder="no" border="1" framespacing="0" cols="*">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">

	<frameset name="SMJSet" cols="100%,*" frameborder="no" border="0" framespacing="0" rows="*">
    	<frame name="fraPic" src="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo=<%=PrtNo%>&BussNoType=11&BussType=TB&SubType=<%=subtype%>">
    </frameset>

	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">

	<frame name="fraSubmit"  scrolling="yes" noresize src="about:blank" >
	<frame name="fraTitle"  scrolling="no" noresize src="" >
	<frameset name="fraSet" rows="100%,*" frameborder="no" border="1" framespacing="0" rows="*">
		<!--菜单区域>
		<frame name="fraMenu" scrolling="yes" noresize src="">
		<!--交互区域-->
		<!--frame id="fraPic" name="fraPic" scrolling="auto" src="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo=<%=PrtNo%>&SubType=TB1001&BussType=TB&BussNoType=11"-->
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="./LHContPlanSetting.jsp?ContNo=<%=ContNo%>&flag=<%=flag%>&CustomerNo=<%=CustomerNo%>">
	    <!--下一步页面区域-->
	    <frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
