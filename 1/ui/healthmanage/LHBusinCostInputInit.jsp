<%
//程序名称：LHBusinCostInput.jsp
//程序功能：
//创建日期：2006-04-06 09:56:12
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
	try
	{           
		fm.all('querybutton').disabled=true;                        
    	fm.all('ServPriceCode').value = "";
    	fm.all('ServPriceName').value = "";
//  	  fm.all('BusinCostType').value = "";
//  	  fm.all('BusinCostTypeName').value = "";
//  	  fm.all('BusinCost').value = "";
//  	  fm.all('Remark').value = "";
    	fm.all('MakeDate').value = "";
    	fm.all('MakeTime').value = "";
	}
	catch(ex)
	{
		alert("在LHBusinCostInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}
function initSelBox()
{  
	try {}
	catch(ex)
	{
		alert("在LHBusinCostInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
	}
}                                        
function initForm()
{
	try
	{
    	initInpBox();   
    	initBusinCostGrid();
    	initQuery();
	}
	catch(re)
	{
		alert("LHBusinCostInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initQuery()
{
	var sql = " select ServPriceCode, ServPriceName, ServItemName from LHServerItemPrice where ServPriceCode = '"+"<%=PriceCode%>"+"'";
	var arr	= easyExecSql(sql);
	
	fm.all('ServPriceCode').value = arr[0][0];
	fm.all('ServPriceName').value = arr[0][1];
//	fm.all('ServItemName').value = arr[0][2];

	var sql_grid = " select busincosttypename, busincost, Remark, busincosttype, makedate, maketime from lhbusincost where ServPriceCode ='"+arr[0][0]+"' ";
	
	if(easyExecSql(sql_grid) == null || easyExecSql(sql_grid) == "")
	{
		BusinCostGrid.setRowColData(0,1,"人力成本(元)");BusinCostGrid.setRowColData(0,4,"101");
		BusinCostGrid.setRowColData(1,1,"印刷成本(元)");BusinCostGrid.setRowColData(1,4,"102");
		BusinCostGrid.setRowColData(2,1,"寄送成本(元)");BusinCostGrid.setRowColData(2,4,"103");
		BusinCostGrid.setRowColData(3,1,"其他成本(元)");BusinCostGrid.setRowColData(3,4,"104");

		return false;
	}   
	    
	turnPage.queryModal(sql_grid, BusinCostGrid);	
}


//领取项信息列表的初始化
var BusinCostGrid;
function initBusinCostGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";		//列名
	    iArray[0][1]="30px";		//列名
	    iArray[0][3]=0;				//列名
	    
	    iArray[1]=new Array();		
		iArray[1][0]="运营成本类型";		//列名    
		iArray[1][1]="60px";		//列名    
		iArray[1][3]=0;				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="运营成本金额";	//列名    
		iArray[2][1]="80px";       //列名    
		iArray[2][3]=1;         	//列名        
			
		iArray[3]=new Array();                    
		iArray[3][0]="备注说明";    //列名    
		iArray[3][1]="400px";       //列名    
		iArray[3][3]=1;         	//列名   
		
		iArray[4]=new Array();  
		iArray[4][0]="CostType";
		iArray[4][1]="0px";   
		iArray[4][3]=3;
		
		iArray[5]=new Array();  
		iArray[5][0]="MakeDate";
		iArray[5][1]="0px";   
		iArray[5][3]=3;
		
		iArray[6]=new Array();  
		iArray[6][0]="MakeTime";
		iArray[6][1]="0px";   
		iArray[6][3]=3;
		
	    BusinCostGrid = new MulLineEnter( "fm" , "BusinCostGrid" ); 
	    //这些属性必须在loadMulLine前           
	    
		BusinCostGrid.canSel = 0;
		BusinCostGrid.hiddenPlus = 1;
		BusinCostGrid.hiddenSubtraction = 1;
		BusinCostGrid.mulLineCount = 4;   
	/*
	    BusinCostGrid.mulLineCount = 0;   
	    BusinCostGrid.displayTitle = 1;
	    BusinCostGrid.canSel = 1;
	    BusinCostGrid.canChk = 0;
	    BusinCostGrid.selBoxEventFuncName = "showOne";
	*/
	    BusinCostGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //BusinCostGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
		alert(ex);
	}
}






</script>
