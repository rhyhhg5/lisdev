<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHServCaseExecManage.jsp
//程序功能：
//创建日期：2006-07-19 10:19:55
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<SCRIPT src="LHServCaseExecManage.js"></SCRIPT>
	<%@include file="LHServCaseExecManageInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
		<div style="display:''">
			<table>
				<tr>
					<td>
           				<input type= hidden class=cssButton  style="width:130px" name=ServTrace value="服务事件查询" OnClick="showQueryInfo();">
       				</td>  
       			</tr>
			</table>
		</div>
	<Div id="divShowServCaseInfo" style="display:''">
    <table>
		<tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHSettingSlipQueryGrid);">
    	 	</td>
    	 	<td class= titleImg style="width:130px">
				服务事件信息列表
       		</td>  
       	 	<td style="width:750px"></td> 		 
       	 	<td >
       			<input type= button class=cssButton name=changeCaseState   style="width:130px" value="事件状态修改" OnClick="changeCaseStateFun();">
			</td> 
    	</tr>
    </table>
    
	<Div id="divLHSettingSlipQueryGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHSettingSlipQueryGrid">
					</span> 
		    	</td>
			</tr>
		</table>
  </div>
 </div>
	<br><br>
	<Div id="divShowServTaskInfo" style="display:'none'">
	 <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHQueryDetailGrid);">
    	 	</td>
    	 <td class= titleImg>
        	 服务任务信息列表
       	</td>  
       <td style="width:700px;">
		   </td> 		 
       <td >
       	<input type= button class=cssButton name=UpdateTask   style="width:130px" value="修改服务任务" OnClick="updateClick();">
		<!--input type= button class=cssButton name=DeleteTask   style="width:130px" value="删除服务任务" OnClick="deleteClick();"-->
     		
       </td>  		 
    	</tr>
    </table>
  <Div id="divLHQueryDetailGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHQueryDetailGrid">
					</span> 
		    	</td>
			</tr>
		</table>
		<Div id= "divPage2" align=center style= "display: 'none' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	  </Div>
	</div>
	<table>
		<tr style="width:50px" > 
       		<td>
       		 	<input type= button class=cssButton name=ServTaskExec  style="width:130px" value="服务任务实施管理" OnClick="showTaskInfo();">
       		</td> 
		</tr> 
  </table>
</div>	

 <Div id="divShowServTaskInfo2" style="display:'none'">
	 <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHQueryDetailInfoGrid);">
    	 	</td>
    	 <td class= titleImg>
        	 新增服务任务信息列表
       	</td>   		 
    	</tr>
    </table>
  <Div id="divLHQueryDetailInfoGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHQueryDetailInfoGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
		<table>
     <tr style="width:50px" > 
     	<td >
     		<input type= button class=cssButton name=AddNewTask   style="width:130px" value="新增服务任务" OnClick="submitForm();">
       </td>
      </tr>
     </table>
</div>	
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmAction" name="fmAction">
		<!--团体服务计划号码-->
		<Input class= 'common' type=hidden  name=GrpServPlanNo >

		<!--个人服务计划号码-->
		<Input class= 'common' type=hidden   name=ServPlanNo >
	      
		<!--服务计划类型-->
		<Input class= 'common' type=hidden name=ServPlayType >
		<Input class= 'common' type=hidden name=ManageCom >
	    <Input class= 'common' type=hidden name=Operator >
	    <Input class= 'common' type=hidden name=MakeDate >
	    <Input class= 'common' type=hidden name=MakeTime >
	    <Input class= 'common' type=hidden name=ServCaseCode >
  	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
</html>
