<%
//程序名称：LHServTypeQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-07-05 13:50:27
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
	function initForm() {
  try {
   
    initLHServTypeGrid();  
  }
  catch(re) {
    alert("LHServTypeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHServTypeGrid;
function initLHServTypeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="服务项目代码";   
	  iArray[1][1]="0px";       
	  iArray[1][3]=3;
    
    iArray[2]=new Array(); 
	  iArray[2][0]="服务项目名称";   
	  iArray[2][1]="80px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
    
    iArray[3]=new Array(); 
	  iArray[3][0]="保单类型代码";   
	  iArray[3][1]="0px";   
	  iArray[3][3]=3;
    
    iArray[4]=new Array(); 
	  iArray[4][0]="保单类型";   
	  iArray[4][1]="40px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
    
    iArray[5]=new Array(); 
	  iArray[5][0]="所属机构代码";   
	  iArray[5][1]="0px";   
	  iArray[5][3]=3;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="保单所属机构标识";   
	  iArray[6][1]="60px";   
	  iArray[6][2]=20;  
	  iArray[6][3]=0;
    
    iArray[7]=new Array(); 
	  iArray[7][0]="服务事件类型代码";   
	  iArray[7][1]="0px";         
	  iArray[7][3]=3;
	             
    iArray[8]=new Array(); 
	  iArray[8][0]="服务事件类型";   
	  iArray[8][1]="60px";   
	  iArray[8][2]=20;  
    iArray[8][3]=0;
  
    LHServTypeGrid = new MulLineEnter( "fm" , "LHServTypeGrid" ); 
    //这些属性必须在loadMulLine前

    LHServTypeGrid.mulLineCount = 0;   
    LHServTypeGrid.displayTitle = 1;
    LHServTypeGrid.hiddenPlus = 1;
    LHServTypeGrid.hiddenSubtraction = 1;
    LHServTypeGrid.canSel = 1;
    LHServTypeGrid.canChk = 0;
   // LHServEventGrid.selBoxEventFuncName = "showOne";

    LHServTypeGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomInHospitalGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
