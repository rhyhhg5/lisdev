<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHGrpPerExeTraceSave.jsp
//程序功能：
//创建日期：2006-03-09 14:31:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
//  LHServExeTraceSchema tLHServExeTraceSchema   = new LHServExeTraceSchema();
  OLHGrpPerServExeTraceUI tOLHGrpPerServExeTraceUI   = new OLHGrpPerServExeTraceUI();
  LHServExeTraceSet tLHServExeTraceSet = new LHServExeTraceSet();
  
  String tServPlanNo[] = request.getParameterValues("LHGrpPerServExeTraceGrid5");					//MulLine的列存储数组
  String tServItemNo[] = request.getParameterValues("LHGrpPerServExeTraceGrid13");					//MulLine的列存储数组  
  String tContNo[] = request.getParameterValues("LHGrpPerServExeTraceGrid1");					//MulLine的列存储数组
  String tExeState[] = request.getParameterValues("LHGrpPerServExeTraceGrid7");					//MulLine的列存储数组
  String tExeDate[] = request.getParameterValues("LHGrpPerServExeTraceGrid8");					//MulLine的列存储数组
  String tServDesc[] = request.getParameterValues("LHGrpPerServExeTraceGrid9");					//MulLine的列存储数组
  String tMakeDate[] = request.getParameterValues("LHGrpPerServExeTraceGrid10");					//MulLine的列存储数组
  String tMakeTime[] = request.getParameterValues("LHGrpPerServExeTraceGrid11");					//MulLine的列存储数组
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
//    tLHServExeTraceSchema.setServPlanNo(request.getParameter("ServPlanNo"));
//    tLHServExeTraceSchema.setServItemNo(request.getParameter("ServItemNo"));
//    tLHServExeTraceSchema.setContNo(request.getParameter("ContNo"));
//    tLHServExeTraceSchema.setExeState(request.getParameter("ExeState"));
//    tLHServExeTraceSchema.setExeDate(request.getParameter("ExeDate"));
//    tLHServExeTraceSchema.setServDesc(request.getParameter("ServDesc"));
//    tLHServExeTraceSchema.setManageCom(request.getParameter("ManageCom"));
//    tLHServExeTraceSchema.setOperator(request.getParameter("Operator"));
//    tLHServExeTraceSchema.setMakeDate(request.getParameter("MakeDate"));
//    tLHServExeTraceSchema.setMakeTime(request.getParameter("MakeTime"));
 //   tLHServExeTraceSchema.setModifyDate(request.getParameter("ModifyDate"));
//    tLHServExeTraceSchema.setModifyTime(request.getParameter("ModifyTime"));
   
    int LHServExeTraceCount = 0;
		if(tServItemNo != null)
		{	
			LHServExeTraceCount = tServItemNo.length;
		}	
		System.out.println(" LHServItemCount is : "+LHServExeTraceCount);
		
		for(int i = 0; i < LHServExeTraceCount; i++)
		{
		 		LHServExeTraceSchema tLHServExeTraceSchema = new LHServExeTraceSchema();			
  			tLHServExeTraceSchema.setServPlanNo(tServPlanNo[i]);	
  			tLHServExeTraceSchema.setServItemNo(tServItemNo[i]);
			  tLHServExeTraceSchema.setContNo(tContNo[i]);
				tLHServExeTraceSchema.setExeState(tExeState[i]);	
				tLHServExeTraceSchema.setExeDate(tExeDate[i]);	
				tLHServExeTraceSchema.setServDesc(tServDesc[i]);	                                                               
        tLHServExeTraceSchema.setManageCom(request.getParameter("ManageCom"));                                                                    
        tLHServExeTraceSchema.setOperator(request.getParameter("Operator"));                                                                    
        tLHServExeTraceSchema.setMakeDate(tMakeDate[i]);  
        tLHServExeTraceSchema.setMakeTime(tMakeTime[i]);
				tLHServExeTraceSet.add(tLHServExeTraceSchema);
		}
  try
  {
    System.out.println("--------Deal---Data------");
  // 准备传输数据 VData
  	VData tVData = new VData();
//	  tVData.add(tLHServExeTraceSchema);
	  tVData.add(tLHServExeTraceSet);
	
  	tVData.add(tG);
    tOLHGrpPerServExeTraceUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
    System.out.println(Content);
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  { System.out.println("$$$$$$$$$$$$---FlagStr: "+FlagStr+"---$$$$$$$$$$$");
    tError = tOLHGrpPerServExeTraceUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";System.out.println("$$$$$$$$$$$$---FlagStr: "+FlagStr+"---$$$$$$$$$$$");
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";System.out.println("$$$$$$$$$$$$---FlagStr: "+FlagStr+"---$$$$$$$$$$$");
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
