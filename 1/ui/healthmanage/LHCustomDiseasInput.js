//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCustomDiseas.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHCustomDiseasQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		//arrResult = arrQueryResult;
		var strSQL = "select a.*,b.name from LHCustomDiseas a,ldperson b where trim(a.CustomerNo)=trim(b.CustomerNo) and a.CustomerNo='"+arrQueryResult[0][0]+"'";
		arrResult = easyExecSql(strSQL);
        fm.all('CustomerNo').value= arrResult[0][0];
        fm.all('DiseasNo').value= arrResult[0][1];
        fm.all('DiseasCode').value= arrResult[0][2];
        fm.all('DiseasState').value= arrResult[0][3];
        fm.all('DiseasBeginDate').value= arrResult[0][4];
        fm.all('DiseasEndDate').value= arrResult[0][5];
        fm.all('HospitCode').value= arrResult[0][6];
        fm.all('Doctor').value= arrResult[0][7];
        fm.all('ImpartFlag').value= arrResult[0][8];
        fm.all('ImpartDate').value= arrResult[0][9];
        fm.all('Operator').value= arrResult[0][10];
        fm.all('MakeDate').value= arrResult[0][11];
        fm.all('MakeTime').value= arrResult[0][12];
        fm.all('ModifyDate').value= arrResult[0][13];
        fm.all('ModifyTime').value= arrResult[0][14];
        fm.all('CustomerName').value= arrResult[0][15];
	}
}               
        
function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function queryDiseas()
{
	if(fm.all('DiseasCode').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("LDDiseaseQueryInput.jsp");	  
	  }
	if(fm.all('DiseasCode').value != "")	 
	{
		var cDiseasCode = fm.DiseasCode.value;  //疾病代码	
		var strSql = "select ICDCode,ICDName from LDDisease where ICDCode='" + cDiseasCode +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.DiseasCode.value = arrResult[0][0];
	      alert("查询结果:  疾病代码:["+arrResult[0][0]+"] 疾病名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("疾病代码为:["+fm.all('DiseasCode').value+"]的疾病不存在，请确认!");
     }
	}	
}

function afterQuery00(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.DiseasCode.value = arrResult[0][0];
  	fm.DiseasName.value = arrResult[0][1];
  }
}

function queryDiseas2()
{
	if(fm.all('DiseasCode').value != ""&& fm.all('DiseasCode').value.length==20)	 
	{
	var cDiseasCode = fm.DiseasCode.value;  //疾病代码	
	var strSql = "select ICDCode,ICDName from LDDisease where trim(ICDCode)='" + cDiseasCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.DiseasCode.value = arrResult[0][0];
      alert("查询结果:  疾病代码:["+arrResult[0][0]+"] 疾病名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("疾病代码为:["+fm.all('DiseasCode').value+"]的疾病名称不存在，请确认!");
     }
	}	
}

function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.jsp");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      fm.CustomerName.value = arrResult[0][1];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}

function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}

function queryCustomerNo2()
{
	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
	var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}

function CustomHealth()
{
	parent.fraInterface.window.location = "LHCustomHealthStatusInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
}
function CustomInHospital()
{
	parent.fraInterface.window.location = "LHCustomInHospitalInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
}
//function CustomDiseas()
//{
//	parent.fraInterface.window.location = "LHCustomDiseasInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
//}
function CustomTest()
{
	parent.fraInterface.window.location = "LHCustomTestInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
}
function CustomOPS()
{
	parent.fraInterface.window.location = "LHCustomOPSInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
}
function CustomFamily()
{
	parent.fraInterface.window.location = "LHCustomFamilyDiseasInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
}
function CustomGym()
{
	parent.fraInterface.window.location = "LHCustomGymInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
	
}