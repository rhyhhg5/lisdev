//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var selectedSpecSecDiaRow; 
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
	    	showInfo.focus();  
		}
		catch(ex)
		{
		    showInfo=null;
		}
	}
}

/*
function generateFlowNo()
{
	var str = "select max(hospitcode) from ldhospital where hospitcode like '"+fm.AreaCode.value+"%%'";
	var shortFlowNo = easyExecSql(str);
	if(fm.AreaCode.value=="")
	{
		alert("请先输入地区代码");
	}
	else
	{
		if(shortFlowNo == null || shortFlowNo=="")
		{
			fm.FlowNo.value="001";
			fm.HospitCode.value = fm.AreaCode.value+"001";
					//	fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
		}
		else
		{
			fm.HospitCode.value = parseInt(shortFlowNo)+1;
			fm.FlowNo.value= fm.HospitCode.value.substr(4,3);
			//			fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
		}
	}
}
*/
function inputClear()
{
  	fm.all('HospitalType').value = "";
    fm.all('HospitalType_ch').value = "";
    fm.all('HospitCode').value = "";
    fm.all('HospitName').value = "";
    fm.all('AreaCode').value = "";
    fm.all('AreaName').value = "";
    fm.all('LevelCode').value = "";
    fm.all('LevelCode_ch').value = "";
    fm.all('BusiTypeCode').value = "";
    fm.all('BusiTypeCode_ch').value = "";
    fm.all('CommunFixFlag').value = "";
    fm.all('CommunFixFlag_ch').value = "";
    fm.all('adminisortcode').value = "";
    fm.all('adminisortcode_ch').value = "";
    fm.all('Economelemencode').value = "";
    fm.all('Economelemencode_ch').value = "";
    fm.all('AssociateClass').value = "";
    fm.all('AssociateClass_ch').value = "";
    fm.all('MngCom').value = "";
    fm.all('MngCom_ch').value = "";
    fm.all('DutyItemCode').value = "";
    fm.all('DutyItem').value = "";
    fm.all('ContraState_ch').value = "";
    fm.all('ContraState').value = "";
    fm.all('DutyItemName').value = "";
    fm.all('DutyName').value = "";
}

function mainQuery()
{
	var sql_case = "";
	var sql_icd = "";
	var sql_item = "";
	var sql_ques = "";
	
	
	if (manageCom.toString().length == 8)
	   var mlen = manageCom.substring(0,4);
	else
	   var mlen = manageCom ;
	   
	//服务事件
	if(ServerGrid.mulLineCount == 0){}
	else
	{
		sql_case = "";
		for(var i=0; i<ServerGrid.mulLineCount; i++)
		{
			sql_case = sql_case +",'"+ ServerGrid.getRowColData(i,1) +"'";
		}
		sql_case = sql_case.substring(1);
		sql_case = " and R.SERVCASECODE in ("+sql_case+") "
	}
	
	//诊疗项目条件
	if(HealtItemGrid.mulLineCount == 0){}
	else
	{
		sql_item="";
		sql_item = " select distinct customerno from LHCustomTest where ";
		for(var i=0; i<HealtItemGrid.mulLineCount; i++)
		{
			if(HealtItemGrid.getRowColData(i,1) == "" || HealtItemGrid.getRowColData(i,3) == "" || HealtItemGrid.getRowColData(i,4) == "")
			{
				alert("第"+(i+1)+"行查询信息不完整，请重新编辑该行或删除该行");
				continue;
			}
//zhousj修改
			var temp_item= "";
				temp_item = " medicaitemcode = '"+HealtItemGrid.getRowColData(i,1)+"' "	
				temp_item = temp_item + " and testresult "+HealtItemGrid.getRowColData(i,3)+" '"+HealtItemGrid.getRowColData(i,4)+"'"	
			if(HealtItemGrid.getRowColData(i,7) != "")
			{
				temp_item = temp_item + " and isnormal = '"+HealtItemGrid.getRowColData(i,7)+"'";
			}
			if( temp_item != "")
			{
				temp_item = "("+temp_item+")"
				if(i != 0 )
				{
					sql_item = sql_item + " or " + temp_item;
				}
				else{ sql_item = sql_item + temp_item 
					; }
			}
		}
		//alert(sql_item);
		var re_item = easyExecSql(sql_item);
		sql_item = " and R.CUSTOMERNO in ('" + re_item.toString().replace(/\,/g,"','") + "')";
		//alert(sql_item);
	}
	
	
	//疾病条件
	if(DiseaseItemGrid.mulLineCount == 0){}
	else
	{
		sql_icd="";
		sql_icd = " select distinct customerno from lhdiagno where ";
		for(var i=0; i<DiseaseItemGrid.mulLineCount; i++)
		{
			var temp_icd = "";
			if(DiseaseItemGrid.getRowColData(i,1) != "")
			{
				temp_icd = " icdcode > '"+DiseaseItemGrid.getRowColData(i,1)+"'"	
			}
			if(DiseaseItemGrid.getRowColData(i,2) != "")
			{
				if(temp_icd != ""){ temp_icd = temp_icd + " and ";}
				temp_icd = temp_icd + " icdcode < '"+DiseaseItemGrid.getRowColData(i,2)+"'"	
			}
			if(DiseaseItemGrid.getRowColData(i,3) != "")
			{
				if(temp_icd != ""){ temp_icd = temp_icd + " and ";}
				temp_icd = temp_icd + " ismaindiagno = '"+DiseaseItemGrid.getRowColData(i,3)+"'"	
			}
			
			if( temp_icd != "")
			{
				temp_icd = "("+temp_icd+")"
				if(i != 0 )
				{
					sql_icd = sql_icd + " or " + temp_icd;
				}
				else{ sql_icd = sql_icd + temp_icd; }
			}
		}
		var re_icd = easyExecSql(sql_icd);
		sql_icd = " and R.CUSTOMERNO in ('" + re_icd.toString().replace(/\,/g,"','") + "')";
	}
	
	//健康问题条件
	if(HealthQuesGrid.mulLineCount == 0){}
	else
	{
		sql_ques="";
		sql_ques = " select distinct CUSTOMERNO from LHQuesImportmain where cusregistno in (select distinct cusregistno from LHQuesImportdetail where ";
		for(var i=0; i<HealthQuesGrid.mulLineCount; i++)
		{
			if(HealthQuesGrid.getRowColData(i,1) == "" || HealthQuesGrid.getRowColData(i,3) == "" || HealthQuesGrid.getRowColData(i,4) == "")
			{
				alert("第"+(i+1)+"行查询信息不完整，请重新编辑该行或删除该行");
				continue;
			}

			var temp_ques = "";
				temp_ques = " standcode = '"+HealthQuesGrid.getRowColData(i,1)+"'"	
				temp_ques = temp_ques + " and letterresult "+HealthQuesGrid.getRowColData(i,3)+" '"+HealthQuesGrid.getRowColData(i,4)+"'"	

			if( temp_ques != "")
			{
				temp_ques = "("+temp_ques+")"
				if(i != 0 )
				{
					sql_ques = sql_ques + " or " + temp_ques;
				}
				else{ sql_ques = sql_ques + temp_ques ; }
			}
		}
		//alert(sql_ques);
		var re_ques = easyExecSql(sql_ques+")");
		sql_ques = " and R.CUSTOMERNO in ('" + re_ques.toString().replace(/\,/g,"','") + "')";
		//alert(sql_ques);
	}
	var sql_cont = " Select R.CUSTOMERNO,R.CUSTOMERNAME,( SELECT distinct I.SERVITEMTYPE FROM LHSERVITEM I WHERE I.SERVITEMNO = R.SERVITEMNO),"
		+" ( SELECT distinct H.SERVITEMNAME FROM LHHEALTHSERVITEM H WHERE H.SERVITEMCODE = R.SERVITEMCODE), "
		+" case when (SELECT distinct D.servcasetype FROM LHSERVCASEDEF D WHERE D.SERVCASECODE = R.SERVCASECODE) = '1' then '个人服务事件' "
		+" when (SELECT distinct D.servcasetype FROM LHSERVCASEDEF D WHERE D.SERVCASECODE = R.SERVCASECODE) = '2' then '集体服务事件' "
		+" else '费用结算事件' end, R.SERVCASECODE, "
		+" (SELECT distinct D.SERVCASENAME FROM LHSERVCASEDEF D WHERE D.SERVCASECODE = R.SERVCASECODE),R.comid,R.ServItemNo,R.Contno,R.ServPlanNo, "
		+" ( SELECT distinct H.SERVITEMCODE FROM LHHEALTHSERVITEM H WHERE H.SERVITEMCODE = R.SERVITEMCODE) "
		+" FROM LHSERVCASERELA R, LDPerson p WHERE 1=1 and R.CUSTOMERNO = p.CUSTOMERNO " 
		+ getWherePart("R.contno","ContNo")
		+ getWherePart("p.sex" , "Sex" ) 
		+ getWherePart("p.birthday", "StartDate", ">=" ) 
		+ getWherePart("p.birthday", "EndDate", "<=" ) 
		+ getWherePart( "R.Servitemcode" , "ServItemCode" )
		+ sql_case + sql_icd + sql_item + sql_ques
	;
	//alert(sql_cont);
	turnPage.queryModal(sql_cont, LHSettingSlipQueryGrid);
	
}

function healthQuery()
{
	if (manageCom.toString().length == 8)
	   var mlen = manageCom.substring(0,4);
	else
	   var mlen = manageCom ;
	var Riskcode="";
	if(fm.all('Riskcode').value != "")//赔付险种类别不为空
	{
		Riskcode =" and a.HospitCode in ( select distinct m.HospitalCode from llfeemain m, llcase c"
  	             +" where m.caseno = c.caseno and c.rgtstate in in ('09','11','12') "
  	             +" and m.caseno in (select  distinct  n.Caseno from llclaimpolicy n  "
  	             +" where  n.riskcode in ('"+fm.all('Riskcode').value+"')))";	
	}  
	var ICDCode="";
	if(fm.all('ICDCode').value != "")//诊断疾病类别不为空
	{
  	      ICDCode =" and a.HospitCode in ( select distinct b.HospitalCode from llfeemain b, llcase c "
  	      		   +" where m.caseno = c.caseno and c.rgtstate in ('09','11','12') and b.caserelano  in "
  	               +" (select distinct c.caserelano from llcasecure c where  "
  	               +" c.Diseasecode in ('"+fm.all('ICDCode').value+"')))";	
	}
	var rowNum=LHHighPartGrid. mulLineCount ; //行数 	
	var testSignNo=new Array;
	var testValue=new Array;
	var xx = 0;
	var SumFee="";
	var testAll=new Array;
	for(var row=0; row< rowNum; row++)
	{
		testSignNo=LHHighPartGrid.getRowColData(row,2);//逻辑判断符号
	 	testValue=LHHighPartGrid.getRowColData(row,3);//比较值
		testAll[xx++]=testSignNo+testValue;
	}
	testAll=testAll.toString().split(",");
	var sqlSignValue = "";
	for(var j=0; j < testAll.length; j++)
	{
		sqlSignValue= sqlSignValue+" and b.SumFee "+testAll[j]+"";//给取得的值加上连接值
	}
	if(testSignNo!=""&&testSignNo!="null"&&testSignNo!=null)
	{
		SumFee =" and a.HospitCode in   ( select distinct   b.HospitalCode  from llfeemain b where  "
               +" 1=1 "
               +" "+ sqlSignValue.substring(1) +") ";
	}
	var sql="select distinct a.HospitCode,a.HospitName,b.ContraNo from LDHospital a,LHGroupCont b, LHContItem c where 1=1 and "
	     + " a.HospitCode=b.HospitCode and b.ContraNo=c.ContraNo "
	     + getWherePart("a.HospitalType","HospitalType")
	     + getWherePart("a.HospitCode","HospitCode")
	     + getWherePart("a.HospitName","HospitName")
	     + getWherePart("a.AreaCode","AreaCode")
	     + getWherePart("a.AreaName","AreaName")
	     + getWherePart("a.ManageCom","MngCom")
	     + getWherePart("a.CommunFixFlag","CommunFixFlag")
	     + getWherePart("a.AdminiSortCode","AdminiSortCode")
	     + getWherePart("a.EconomElemenCode","EconomElemenCode")
	     + getWherePart("a.LevelCode","LevelCode")
	     + getWherePart("a.BusiTypeCode","BusiTypeCode")
	     + getWherePart("a.AssociateClass","AssociateClass")
	     + getWherePart("c.contratype","DutyItem")
	     + getWherePart("b.ContraState","ContraState")
	     + " and a.Managecom like '" + mlen +"%%'"
	     + Riskcode   
	     + ICDCode
	     + SumFee
	     ;
	//alert(sql);
	turnPage.queryModal(sql, HospitalComGrid);
}

function ToHospitalInfo()
{
	if (HospitalComGrid.getSelNo() >= 1)
	{     
		if(HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,1)=="")
		{	
			alert("你选择了空的列，请重新选择!");
			return false;
		}
		else
		{
			var ContraNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,3);
			if(ContraNo==""||ContraNo=="null"||ContraNo==null)
			{
				var HospitalNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,1);
				var HospitalName = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,2);
		        window.open("./LDHospitalInputMain.jsp?HospitalNo="+HospitalNo+"&flag=1","医疗机构信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	        }
	    }
	} 
	else
	{
		alert("请选择一条要传输的记录！");    
	}
}

function ToCompactInfo()
{
	if (HospitalComGrid.getSelNo() >= 1)
	{     
		// alert(HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,1));
		if(HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,3)=="")
		{
			alert("请选择正确的合同编号!");
			return false;
		}
		else
		{
			var ContNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,3);
			if(ContNo!=""||ContNo!="null"||ContNo!=null)
			{
				var HospitalNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,1);
				var ContraNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,3);
				//alert(ContraNo);
				window.open("./LHGroupContInputMain.jsp?HospitalNo="+HospitalNo+"&ContraNo="+ContraNo+"&ContFlag=1","医疗机构信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			}
		}
	} 
	else
	{
		alert("请选择一条要传输的记录！");    
	}
}

function SpeciOfHospit()
{
	var arrSelected = null;
	var tSel = HospitalComGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
		return false;
	}
	else
	{		
		try
		{	
	    	var HospitalCode = HospitalComGrid.getRowColData(tSel-1,1);
	  		window.open("./LHHospitalInfoMain.jsp?HospitalCode="+HospitalCode,"医疗机构详细信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}
function afterCodeSelect(codeName,Field)
{
	if(codeName=="searchesmode" )
	{
		if(Field.value=="1")//按险种;按疾病
		{
			HighTypeGrid.style.display="";
			fm.ICDName.style.display='';
			fm.Riskname.style.display='';
		}
		if(Field.value=="2")//不按险种;按疾病
		{
			fm.ICDName.style.display='';
			fm.Riskname.style.display='none';
		}
		if(Field.value=="3")//按险种;不按疾病
		{
			fm.ICDName.style.display='none';
			fm.Riskname.style.display='';
		}
		if(Field.value=="4")//不按险种/疾病
		{
			HighTypeGrid.style.display="none";
			fm.ICDName.style.display='none';
			fm.Riskname.style.display='none';
		}
	}
}


function queryClick() 
{
	window.open("./LHServCaseQuery.html");
}


function afterQuery1(arrResult)
{
	var strSql="select ServCaseType,ServCaseCode,ServCaseName,MakeDate,MakeTime  from LHServCaseDef where ServCaseCode ='"+ arrResult[0][1]+"'";
	var arrLHServEventStatus=easyExecSql(strSql);
    if(arrLHServEventStatus!=null) 
    {
  	  	fm.all('ServeEventStyle').value=arrLHServEventStatus[0][0];
  	  	if (arrLHServEventStatus[0][0]==1)
  	  		fm.all('ServeEventStyle_ch').value="个人服务事件"
  	  	else
  	  		fm.all('ServeEventStyle_ch').value="集体服务事件"	
  	    fm.all('ServeEventNum').value=arrLHServEventStatus[0][1];
  	    fm.all('ServeEventName').value=arrLHServEventStatus[0][2];
  	    fm.all('MakeDate').value=arrLHServEventStatus[0][3];
  	    fm.all('MakeTime').value=arrLHServEventStatus[0][4];
//  	    fm.all('ServCaseState').value=arrLHServEventStatus[0][5];
//  	    if(arrLHServEventStatus[0][5]==0){fm.all('ServCaseStateName').value = "未进行启动设置";}
//  	    else if(arrLHServEventStatus[0][5]==1){fm.all('ServCaseStateName').value = "服务事件已启动";}
//  	    else if(arrLHServEventStatus[0][5]==2){fm.all('ServCaseStateName').value = "服务事件已完成";}
//  	    else if(arrLHServEventStatus[0][5]==3){fm.all('ServCaseStateName').value = "服务事件失败";}

	}
}


function AddServCaseOld()
{
	var arrSelected = null;
	arrSelected = new Array();
	var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
		if(tChk == true)
		{
			aa[xx++] = row;	
		}
	}

	if(aa.length=="0"||aa.length=="null"||aa.length==null||aa.length=="")
	{
		 alert("请选择要添加的服务事件的客户项目信息!");
		 return false;
	}
	if(aa.length>="1")
	{
		var i = 0;
       var showStr="正在添加数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
       showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
       fm.all('fmtransact').value = "INSERT||MAIN";
       fm.action="./LHServCaseQuerySave.jsp";
       fm.submit(); //提交
	}
}


function afterSubmit( FlagStr, content ) {
  showInfo.close();
  if (FlagStr == "Fail" ) 
  {
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } 
  else 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}