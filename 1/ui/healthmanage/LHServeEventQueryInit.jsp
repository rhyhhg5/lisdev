<%
//程序名称：LHServeEventQuery.jsp
//程序功能：
//创建日期：2006-06-26 14:50:27
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<Script language=javascript>
function initForm()
{
  try
  {
  	initLHServMissionGrid();
  }
  catch(re)
  {
    alert("LHServeEventQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initInpBox()
{ 
  try
  {                                   
  divLHServMissionGrid.style.display='none';
	
  }
  catch(ex)
  {
    alert("在LHServPlanInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initLHServMissionGrid()
{
	 var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="服务事件编号";         		//列名
    iArray[1][1]="0px";         		//列名
    iArray[1][3]=1;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="服务事件名称";         		//列名
    iArray[2][1]="50px";         		//列名
    iArray[2][3]=1;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="服务任务名称";         		//列名
    iArray[3][1]="50px";         		//列名
    iArray[3][3]=1;         		//列名
       
    iArray[4]=new Array(); 
	  iArray[4][0]="（计划）完成时间";   
	  iArray[4][1]="50px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	  
	  iArray[5]=new Array(); 
	  iArray[5][0]="说明";   
	  iArray[5][1]="60px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=0;
	    
    LHServMissionGrid = new MulLineEnter( "fm" , "LHServMissionGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHServMissionGrid.mulLineCount = 0;                             
    LHServMissionGrid.displayTitle = 1;                          
    LHServMissionGrid.hiddenPlus = 1;                          
    LHServMissionGrid.hiddenSubtraction = 1;                          
    LHServMissionGrid.canSel = 1;                          
    LHServMissionGrid.canChk = 0;                          
    LHServMissionGrid.selBoxEventFuncName = "showOne";                       
                                                             
    LHServMissionGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHServPlanGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}
</script>