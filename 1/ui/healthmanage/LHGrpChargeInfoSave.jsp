<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHGrpChargeInfoSave.jsp
//程序功能：
//创建日期：2006-07-19 15:39:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%

  OLHServCaseRelaUI tOLHServCaseRelaUI = new OLHServCaseRelaUI();
  LHServCaseRelaSet tLHServCaseRelaSet = new LHServCaseRelaSet();	
  String tCustomerNo[] = request.getParameterValues("LHCustomInfoGrid1");					//MulLine的列存储数组
  String tCustomerName[] = request.getParameterValues("LHCustomInfoGrid2");		
  String tGrpContNo[] = request.getParameterValues("LHCustomInfoGrid3");	
  String tServItemCode[] = request.getParameterValues("LHCustomInfoGrid4");	
  String tServItemNo[] = request.getParameterValues("LHCustomInfoGrid11");
  String tComID[] = request.getParameterValues("LHCustomInfoGrid8");
  String tChk[] = request.getParameterValues("InpLHCustomInfoGridChk"); 
  
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  int j=0;
  int[] arr = new int[20];

   	for(int index = 0; index < tChk.length; index++)
   	{
   		if(tChk[index].equals("1"))
   		{
   		  arr[j++]=index;           
   		  //System.out.println(index+"行被选中");
   		}
   	
   	}        
	
	int LHGrpServPlanCount = 0;
	j=0;
	LHGrpServPlanCount = tServItemNo.length;
	for(int i = 0; i < LHGrpServPlanCount; i++)
	{
	    if (arr[j]==i)
	    {
	    	LHServCaseRelaSchema tLHServCaseRelaSchema = new LHServCaseRelaSchema();			
			tLHServCaseRelaSchema.setCustomerNo(tCustomerNo[i]);	
			tLHServCaseRelaSchema.setCustomerName(tCustomerName[i]);	
	        tLHServCaseRelaSchema.setGrpContNo(tGrpContNo[i]);
	        tLHServCaseRelaSchema.setServItemNo(tServItemNo[i]);
	        tLHServCaseRelaSchema.setServCaseCode(request.getParameter("ServeEventNum"));
	        tLHServCaseRelaSchema.setComID(tComID[i]);
	        //tLHServCaseRelaSchema.setServPlanNo(request.getParameter("Riskcode"));
	        tLHServCaseRelaSchema.setServPlanName(request.getParameter("Riskname"));
	        tLHServCaseRelaSchema.setServItemCode(tServItemCode[i]);
	        tLHServCaseRelaSchema.setContType(request.getParameter("ContType"));
	        tLHServCaseRelaSet.add(tLHServCaseRelaSchema);
	        j++;
    	}
    }
	try
  	{
  	// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tLHServCaseRelaSet);
  		tVData.add(tG);
		tOLHServCaseRelaUI.submitData(tVData,"INSERT||MAIN");
  	}
	catch(Exception ex)
	{
	  Content = "保存失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
	}
	
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHServCaseRelaUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
