<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHManaStandProSave.jsp
//程序功能：
//创建日期：2006-09-11 14:26:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
	LHManaStandProUI tLHManaStandProUI   = new LHManaStandProUI();  
  LHManaStandProSchema tLHManaStandProSchema = new LHManaStandProSchema();
	LHStaQuOptionSet tLHStaQuOptionSet = new  LHStaQuOptionSet();		

	VData rVData = new VData();
	
    String tQuOptionNum[] = request.getParameterValues("LHStaQuOptionGrid1");					//MulLine的列存储数组
	  String tQuOptionCont[] = request.getParameterValues("LHStaQuOptionGrid2");					//MulLine的列存储数组		
	  String tPertainInfo[] = request.getParameterValues("LHStaQuOptionGrid3");         //MulLine的列存储数组
	  String tQuOptionNo[] = request.getParameterValues("LHStaQuOptionGrid4");					//MulLine的列存储数组		
	  //String tStandCode[] = request.getParameterValues("LHStaQuOptionGrid5");         //MulLine的列存储数组
		  
    //输出参数
    CErrors tError = null;
    String tRela  = "";                
    String FlagStr = "";
    String Content = "";
    String transact = "";
    String StandCode="";
    GlobalInput tG = new GlobalInput(); 
    tG=(GlobalInput)session.getValue("GI");
	  
    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
    //System.out.println("FFFFFFFFFFFf "+transact);
	
			
		tLHManaStandProSchema.setStandCode(request.getParameter("StandCode"));
		tLHManaStandProSchema.setStandContent(request.getParameter("StandContent"));
		tLHManaStandProSchema.setStandLevel(request.getParameter("StandLevel"));
		tLHManaStandProSchema.setQuerAffiliated(request.getParameter("QuerAffiliated"));
		tLHManaStandProSchema.setStandQuerType(request.getParameter("StandQuerType"));
		tLHManaStandProSchema.setQuerResType(request.getParameter("QuerResType"));
		tLHManaStandProSchema.setResuAffiInfo(request.getParameter("ResuAffiInfo"));
		tLHManaStandProSchema.setStandUnit(request.getParameter("StandUnit"));
		tLHManaStandProSchema.setQuesExplain(request.getParameter("QuesExplain"));
		tLHManaStandProSchema.setQuesStauts(request.getParameter("QuesStauts"));
		
    int LHManaStandProCount = 0;
	 if(tQuOptionCont != null)
	 {	
		 LHManaStandProCount = tQuOptionCont.length;
		
	 }
	  System.out.println(" LHManaStandProCount is : "+LHManaStandProCount);
	 for(int i = 0; i < LHManaStandProCount; i++)
	 {
      LHStaQuOptionSchema tLHStaQuOptionSchema = new LHStaQuOptionSchema();
      
      tLHStaQuOptionSchema.setStandCode(request.getParameter("StandCode"));
		  tLHStaQuOptionSchema.setQuOptionNum(tQuOptionNum[i]);	
		  //System.out.println("AAAAAAAAAAAAAAAAAAA "+ tLHStaQuOptionSchema.getQuOptionNum());	
		  tLHStaQuOptionSchema.setQuOptionCont(tQuOptionCont[i]);	
		  tLHStaQuOptionSchema.setPertainInfo(tPertainInfo[i]);	
		  tLHStaQuOptionSchema.setQuOptionNo(tQuOptionNo[i]);	
		  tLHStaQuOptionSet.add(tLHStaQuOptionSchema);
		  
	  }
	try	
	{
  		VData tVData = new VData();
	  	tVData.add(tLHManaStandProSchema);
	  	tVData.add(tLHStaQuOptionSet);
	  	tVData.add(tG);       
	    tLHManaStandProUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
    	Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
  	if (FlagStr=="")
  	{
	    tError = tLHManaStandProUI.mErrors;
	    if (!tError.needDealError())
	    {                          
	    	Content = " 操作成功! ";
	    	FlagStr = "Success";      
	    }
	    else                                                                           
	    {
	    	Content = " 操作失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	}
	if(transact.equals("INSERT||MAIN")) 
	{ 
		VData res = tLHManaStandProUI.getResult();
		LHManaStandProSchema mLHManaStandProSchema = new LHManaStandProSchema();
		mLHManaStandProSchema.setSchema((LHManaStandProSchema)res.getObjectByObjectName("LHManaStandProSchema",0));
		StandCode = (String)mLHManaStandProSchema.getStandCode();
		//System.out.println("AAAAAAAAAAAAAAAAAAA "+StandCode);
	}
  
	//添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
		<%if(StandCode.equals("")||transact.equals("DELETE||MAIN")) 
	  {  
	  %>
			   //parent.fraInterface.fm.all("StandCode").value="";
	  <%
	  } 
	  else 
	  {
    %>
          parent.fraInterface.fm.all("StandCode").value = "<%=StandCode%>";
    <%
	  }
    %>
    
</script>
</html>