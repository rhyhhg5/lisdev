<%
//程序名称：LHSettingSlipQueryInit.jsp
//程序功能：
//创建日期：2006-06-26 9:40:56
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类--> 
                          
<script language="JavaScript">
function initInpBox()
{ 
    try
    {       
    	Come = "<%=Come%>";
    	if(Come == "LHGrpServInfoCharge")       
    	{
    		fm.all('ServCaseType').value = '2';
    		fm.all('ServCaseTypeName').value = '集体服务事件';
//    		fm.all('ServCaseTypeName').ondblclick = '';
    	}              
        fm.all('GrpServPlanNo').value = "";
        fm.all('ServCaseState').value = "1";
        fm.all('ServCaseStateName').value = "服务事件已启动";
        
	    var tempCom = manageCom.length==8?manageCom.substring(0,4):manageCom;
	    fm.all('MngCom').value = tempCom;
        fm.all('MngCom_ch').value = easyExecSql("  select distinct showname from ldcom where  sign = '1' and comcode = '"+tempCom+"' ");
        if(tempCom != "86")
        {
        	fm.all('MngCom').ondblclick = "";
        	fm.all('MngCom').onkeyup = "";	
        }
    }
    catch(ex)
    {
      alert("在LHSettingSlipQueryInit.jsp->InitInpBox函数中发生异常:初始化界面错误!");
    }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    initLHSettingSlipQueryGrid();
    //initLHQueryDetailGrid();
  }
  catch(re)
  {
    alert("LHSettingSlipQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHSettingSlipQueryGrid;
function initLHSettingSlipQueryGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="服务事件编号 ";   
	  iArray[1][1]="80px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="服务事件名称";   
	  iArray[2][1]="300px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="服务事件类型";   
	  iArray[3][1]="80px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=1;
	  iArray[3][5]="3|5";     //引用代码对应第几列，'|'为分割符
    iArray[3][6]="1|0";     //上面的列中放置引用代码中第几位
    iArray[3][17]="1"; 
    iArray[3][18]="160";
    iArray[3][19]="1" ;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="服务事件状态";   
	  iArray[4][1]="80px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=1;
	  iArray[4][5]="4|6";     //引用代码对应第几列，'|'为分割符
    iArray[4][6]="1|0";     //上面的列中放置引用代码中第几位
    iArray[4][17]="1"; 
    iArray[4][18]="160";
    iArray[4][19]="1" ;
    
	  iArray[5]=new Array(); 
	  iArray[5][0]="eventtype";   
	  iArray[5][1]="0px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=3;
	  iArray[5][14]="1";
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="eventstate";   
	  iArray[6][1]="0px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=3;

	                               
    LHSettingSlipQueryGrid = new MulLineEnter( "fm" , "LHSettingSlipQueryGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHSettingSlipQueryGrid.mulLineCount = 0;                             
    LHSettingSlipQueryGrid.displayTitle = 1;                          
    LHSettingSlipQueryGrid.hiddenPlus = 1;                          
    LHSettingSlipQueryGrid.hiddenSubtraction = 1;                          
    LHSettingSlipQueryGrid.canSel = 0;                          
    LHSettingSlipQueryGrid.canChk = 1;                                            
//    LHSettingSlipQueryGrid.chkBoxEventFuncName = "showOne";                                                             
    LHSettingSlipQueryGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHSettingSlipQueryGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}
var LHQueryDetailGrid;
function initLHQueryDetailGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="客户号";   
	  iArray[1][1]="150px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="客户姓名";   
	  iArray[2][1]="150px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="服务计划名称";   
	  iArray[3][1]="150px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=0;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="服务计划档次";   
	  iArray[4][1]="150px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	                               
    LHQueryDetailGrid = new MulLineEnter( "fm" , "LHQueryDetailGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHQueryDetailGrid.mulLineCount = 0;                             
    LHQueryDetailGrid.displayTitle = 1;                          
    LHQueryDetailGrid.hiddenPlus = 1;                          
    LHQueryDetailGrid.hiddenSubtraction = 1;                          
    LHQueryDetailGrid.canSel = 1;                          
    LHQueryDetailGrid.canChk = 0;                          
                    
                                                             
    LHQueryDetailGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHQueryDetailGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}
 
</script>
