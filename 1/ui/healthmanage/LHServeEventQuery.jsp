<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2006-06-26 09:54:27
//创建人  ：CrtHtml程序创建
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHServeEventQuery.js"></SCRIPT>

  <%@include file="LHServeEventQueryInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
  	<table  class= common align='center'>
  	<TR  class= common>
  	<TD  class= title>
	       	服务事件类型
     </TD>
     <TD  class= input>
     	  <Input type=hidden name=ServeEventStyle>
        <Input class= 'code' name=ServeEventStyle_ch CodeData= "0|^1|个人服务事件^2|集体服务事件" ondblclick= "showCodeListEx('',[this,ServeEventStyle],[1,0],null,null,null,1);" >
     </TD>
      <TD  class= title>
         服务事件编号
      </TD>
      <TD  class= input>
         <Input class= 'code' name=ServeEventNum ondblclick="showCodeListEx('',[this],[0],null,null,null,1);" > 
      </TD>
      <TD  class= title>
         服务事件名称
      </TD>
      <TD  class= input>
         <Input class= 'code' name=ServeEventName ondblclick="showCodeListEx('',[this],[0],null,null,null,1);" > 
      </TD>
    </TR>
    <TR  class= common>
      <TD  class= title>
	       	服务事件状态
      </TD>
      <TD  class= input>
     	   <Input type=hidden name=ServeEventStatus>
         <Input class= 'code' name=ServeEventStatus_ch CodeData= "0|^1|未进行启动设置^2|待其它事件完成^3|服务事件已启动^4|服务事件已完成^5|服务事件失败" ondblclick="showCodeListEx('',[this,ServeEventStatus],[0,1],null,null,null,1);" >
      </TD>
      <TD  class= title>
	       	服务任务状态
      </TD>
      <TD  class= input>
     	   <Input type=hidden name=ServeMissionStatus>
         <Input class= 'code' name=ServeMissionStatus_ch CodeData= "0|^1|未完成^2|已完成" ondblclick="showCodeListEx('',[this,ServeMissionStatus],[0,1],null,null,null,1);" >
      </TD>
      <TD  class= title>
	    </TD> 
      <TD  class= input>
      </TD>
    </TR>
    <TR  class= common>
    	<TD  class= title>
	       	服务操作方式
      </TD>
      <TD  class= input>
     	   <Input type=hidden name=ServeOptionStyle>
         <Input class= 'code' name=ServeOptionStyle_ch CodeData= "0|^1|未完成^2|已完成" ondblclick="showCodeListEx('',[this,ServeOptionStyle],[0,1],null,null,null,1);" >
      </TD>
      <TD  class= title>
	       任务起始时间
     </TD>	
     <TD  class= input>
			   <Input class= 'CoolDatePicker' name=EventStartDate>
		 </TD>
		 <TD  class= title>
	       任务终止时间
     </TD>	
     <TD  class= input>
			   <Input class= 'CoolDatePicker' name=EventEndDate>
		 </TD>
    </TR>
  </table>
  <br>
  <table  class= common align='center'>
   <TR class = common>
	 	 <Input value="服务事件查询" type=button class=cssbutton style="width:158px;"  onclick="ServeQuery();">&nbsp;&nbsp;
  	 <Input value="服务任务查询" type=button class=cssbutton style="width:158px;" onclick="ServeMissionQuery();">
	 </TR>
	</table>
	
	<Div id="divLHServMissionGrid" style="display:'none'">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServMissionGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
	
	<br>
	<table  class= common align='center'>
   <TR class = common>
	 	 <Input value="服务事件管理" type=button class=cssbutton style="width:158px;"  onclick="ServeEventCharge();">&nbsp;&nbsp;
	 </TR>
	</table>
	
	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>