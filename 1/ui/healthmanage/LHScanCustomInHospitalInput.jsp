<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHScanCustomInHospitalInput.jsp
//程序功能：
//创建日期：2006-06-13 08:19:27
//创建人  ：郭丽颖
//更新记录：  更新人  	
%>
<%@page contentType="text/html;charset=GBK" %>

<%
		      
		 String ScanType = request.getParameter("ScanType");   
		 String prtNo = request.getParameter("prtNo"); 
		 String RelationNo2 = request.getParameter("RelationNo2");  
		 System.out.println(">>>>>>>>>"+RelationNo2); 
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LHScanCustomInHospitalInput.js"></SCRIPT>
	<%@include file="LHScanImportPageInit.jsp"%>
</head>
<script language="JavaScript">
       var InHospitNo="<%=RelationNo2%>";    
</script>
<body  onload="initForm();initElementtype();initForm2();" >
	<form action="./LHScanImportPageSave.jsp" method=post name=fms target="fraSubmit">
		<div  id='info'>
			<table class= common  align='center' >
		    	<TR  class= common>          
					<TD  class= title>
		                扫描件编号
					</TD>
					<TD  class= input>
		                <Input class= 'common' name=ScanNo verify="扫描件编号|NOTNULL&len<=50" readonly>
		            </TD>
		            <TD  class= title>
		                扫描件名称
		            </TD>
		            <TD  class= input>
		                <Input class= 'common' name=ScanName  verify="扫描件名称|len<=24">
		            </TD>
		            <TD  class=title >
		            	 扫描件维护状态
		            </TD>
		            <TD>
		                <Input class= 'codename' style="width:50px" name=ScanStatus value=3><Input class= 'codeno'  style="width:130px" name=ScanStatusName  value="尚未录入"  CodeData= "0|^1|全部录入^2|部分录入^3|尚未录入"   ondblclick="showCodeListEx('',[this,ScanStatus],[1,0],null,null,null,'1',160);">
		            </TD>
		            <td class=button width="60%" align=right >
						<INPUT class=cssButton VALUE="保 存"  style="width:40px"  TYPE=button name=saveButton onclick="submitForm();">
						<INPUT class=cssButton VALUE="修 改"  style="width:40px"  TYPE=button onclick="updateClick();">
						<INPUT class=cssButton VALUE="调取已录入信息"  style="width:90px"  TYPE=button onclick="QueryFirst();">
					</td>
		       </tr>
			</table>
		</Div>
		<input type=hidden  name="RelationType">
		<input type = hidden   name="RelationNo">
		<input type = hidden  name="RelationNo2">
		<input type = hidden name = "MakeDate">
		<input type = hidden name = "MakeTime">
		<input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <form action="./LHScanCustomInHospitalSave.jsp" method=post name=fm target="fraSubmit">

    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital1);">
    		</td>
    		<td class= titleImg>
        		 一般信息
			</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomInHospital1" style= "display: ''">
	<table  class= common align='center' style= "width: '94%'">
		<TR  class= common>
			<TD  class= title>
              客户号码
			</TD>
			<TD  class= input>
				<Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=24" ondblclick="return queryCustomerNo();">
			</TD>
			<TD  class= title>
              客户姓名
			</TD>
			<TD  class= input>
				<Input class= 'common' name=CustomerName  >
			</TD>
			<TD  class= title>
           	  就诊时间
			</TD>
			<TD  class= input>
				<Input class= 'coolDatePicker' dateFromat="Short" name=InHospitDate verify="就诊时间|DATE">
			</TD>
		</TR>
		<TR  class= common>           
            <TD  class= title>
               医疗机构名称
            </TD>
            <TD  class= input8>
              <Input class= 'code8' name=HospitName  ondblclick="return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName');"  onkeyup=" if(event.keyCode ==13) return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName')	; codeClear(HospitName,HospitCode); " onkeydown="  return showCodeListKey('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName')	; codeClear(HospitName,HospitCode); " verify="医疗机构名称|len<=60">
            	<Input type=hidden  name=HospitCode >
            </TD>
            <TD  class= title>
               主治医师姓名
            </TD>
            <TD  class= input>
            	<Input  type=hidden name=DoctNo >
               <Input class= 'code' name=DoctName  ondblclick="return showCodeList('lhdoctname',[this,DoctNo],[0,1],null,fm.DoctName.value,'DoctName');" onkeyup=" if(event.keyCode ==13) return showCodeList('lhdoctname',[this,DoctNo],[0,1],null,fm.DoctName.value,'DoctName');  codeClear(DoctName,DoctNo); " onkeydown=" return showCodeListKey('lhdoctname',[this,DoctNo],[0,1],null,fm.DoctName.value,'DoctName');  codeClear(DoctName,DoctNo); " verify="诊断医师姓名|len<=20">
            </TD>
            <TD  class= title>
            	 就诊方式
            </TD>
            <TD  class= input>
            	<Input type=hidden name=InHospitModeCode >            
            	<Input class= 'code' name=InHospitMode ondblclick=" return showCodeList('hminhospitalmode',[this,InHospitModeCode],[1,0]);" onkeydown=" return showCodeListKey('hminhospitalmode',[this,InHospitModeCode],[1,0]);codeClear(InHospitMode,InHospitModeCode);"  verify="就诊方式代码|len<=10">
            </TD>
		</TR>
		<TR  class= common>
    		<TD  class= title>
                  主要治疗方式
            </TD>
            <TD  class= input>
            	 <Input type=hidden  name="MainCureModeCode" >
               <Input class= 'code' name="MainCureMode" ondblclick="return showCodeList('maincuremode',[this,MainCureModeCode],[0,1]);"  onkeydown="return showCodeListKey('maincuremode',[this,MainCureModeCode],[0,1]);"  verify="主要治疗方式|len<=10">
            </TD>
            <TD  class= title>
             	疾病转归
            </TD>
            <TD  class= input>
            	  <Input type=hidden  name=CureEffectCode >
            	  <Input class= 'code'id=CureEffect name=CureEffect ondblclick="return showCodeList('diagnosecureeffect',[this,CureEffectCode],[1,0],null,null,null,1);" verify="疾病转归|len<=10">
            </TD>
            <TD  class= title>
              住院天数（天）
            </TD>
            <TD  class= input>
              <Input class= 'common' id=InHospitalDays name=InHospitalDays verify="住院天数（天）|len<=20">
            </TD>
		</TR>
		<TR  class= common>
		  	<TD  class= title>
		      主诉
		    </TD>
			<TD  class= input colSpan=5>
				<textarea class= 'common'  name=MainItem verify="主诉|len<=5000" style="width:683px;height:50px" ></textarea>    
			</TD>
		</TR>
	</table>
    </Div>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital2);">
    		</td>
    		<td class= titleImg>
        		 诊断信息
			</td>   		 
		</tr>
    </table>
    <Div  id= "divLHCustomInHospital2" style= "display: ''">
		<table  class= common align='center'  >
			<tr class=common>
	      		<td width='18px'>
	        	</td>
      			<td>
					<span id="spanDiagnoInfoGrid"></span>
				</td>
			</tr>  
	   </table>
    </Div>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital3);">
    		</td>
    		<td class= titleImg>
        	检查信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomInHospital3" style= "display: ''">
		<div id="divTestResult"  style="display: none; position:absolute; slategray">
			<textarea name="textTestResult" cols="100%" rows="3" witdh=25% class="common" onkeydown="backTestResult();">
			</textarea>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backTestResult2();">
			</td>
		</div>
		<table  class= common align='center'  >
	      	<tr class=common>
	      		<td width='18px'>
	        	</td>
	      		<td>
	      		  <span id="spanTestInfoGrid"></span>
				</td>
	        </tr>  
        </table>
    </Div>
 
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital4);">
    		</td>
    		<td class= titleImg>
        		手术信息
       		</td>   		 
		</tr>
    </table>
	<Div  id= "divLHCustomInHospital4" style= "display: ''">
		<div id="divOPSResult"  style="display: none; position:absolute; slategray">
			<textarea name="textOPSResult" cols="100%" rows="3" witdh=25% class="common" onkeydown="backOPSResult();">
			</textarea>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backOPSResult2();">
			</td>
		</div>
		<table  class= common align='center'  >
	      	<tr class=common>
	      		<td width='18px'>
				</td>
	      		<td>
					<span id="spanOPSInfoGrid"></span>
				</td>
	        </tr>  
        </table>
    </Div>
  
    <Div  id= "divLHCustomInHospital5" style= "display: 'none'">
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital5);">
    		</td>
    		<td class= titleImg>
        	其他治疗信息
       	</td>   		 
    	</tr>
    </table>
       <div id="divCureResult"  style="display: none; position:absolute; slategray">
    	     <textarea name="textCureResult" cols="100%" rows="3" witdh=25% class="common" onkeydown="backCureResult();">
    	     </textarea>
    	     <td class=button width="10%" align=right>
						<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backCureResult2();">
					 </td>
			 </div>
      <table  class= common align='center'  >
      	<tr class=common>
      		<td width='18px'>
        	 		</td>
      		<td>
      		  <span id="spanOtherCureInfoGrid"></span>
          </td>
        </tr>  
         
      </table>
    </Div>
   
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital6);">
    		</td>
    		<td class= titleImg>
        		 费用信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomInHospital6" style= "display: ''">
    	<table  class= common align='center'  >
      	<tr class=common>
      		<td width='18px'>
        	 		</td>
      		<td>
      		  <span id="spanFeeInfoGrid"></span>
          </td>
        </tr>
      </table> 
      <table class= common align='center'style= "width: '94%'" > 
        <TR  class= common>
            <TD  class= title>
              总费用（元）
            </TD>
            <TD  class= input>
              <Input class= 'common' name=TotalFee verify="总费用（元）|len<=20" readonly>
            </TD>
            <TD  class= title>
             
            </TD>
            <TD  class= input>
             
            </TD>
            <TD  class= title>
            	
            </TD>
            <TD  class= input>
            	
            </TD>
         </TR>
      </table>
      
    </Div>
    <div id="div1" style="display: 'none'">
      <table>
        <TR  class= common>
            <TD  class= title>
              操作员代码
            </TD>
            <TD  class= input>
              <Input class= 'common' name=Operator >
            </TD>
            <TD  class= title>
              入机日期
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeDate >
            </TD>
            <TD  class= title>
              入机时间
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeTime >
            </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最后一次修改日期
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyDate >
          </TD>
          <TD  class= title>
            最后一次修改时间
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyTime >
          </TD>
        </TR>      
      </table>
    </div>
    
  
    
     <input type=hidden id="fmtransact" name="fmtransact">
     <input type=hidden id="fmAction" name="fmAction">
     <Input type=hidden id="InHospitNo" name="InHospitNo" >  
     <input type=hidden id="iscomefromquery" name ="iscomefromquery">               
   
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>



</html>