<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-02-27 18:16:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>


<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHConsultInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHConsultInputInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();passQuery();" >
  <form action="./LHConsultSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
   <div id="divQuestion"  style="display: none; position:absolute; slategray">
    	     <textarea name="textQuestion" cols="100%" rows="10" witdh=25% class="common" onkeydown="backQuestion();">
    	     </textarea>
    	     <td class=button width="10%" align=right>
							<INPUT class=cssButton VALUE="返  回"  TYPE=button onclick="backQuestion2();">
					 </td>
	 </div> 
   <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHConsult1);">
    		</td>
    		<td class= titleImg>
        	 咨询受理
       	</td>   		 
    	</tr>
   </table>
   <Div  id= "divLHConsult1" style= "display: ''">
     <table  class= common align='center' >
      <TR  class= common>   
         <TD  class= title>
           客户号
         </TD>
         <TD  class= input> 
       
           <Input class= 'code' name=LogerNo elementtype=nacessary verify="客户号码|notnull&len<=10" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
         </TD>
         <TD  class= title>
           客户姓名
         </TD>
         <TD  class= input>
           <Input class= 'common' name=CustomerName readonly verify="客户姓名|len<=200">
         </TD>
         <TD class = title >
         	
         </TD>
         <TD class = input>
     		</TD>
     	</TR>
     	<TR  class= common>   
         <TD  class= title>
           客户咨询时间 
         </TD>
         <TD  class= input>
           <Input class= 'coolDatePicker' style='width:90' name=LogDate   verify="客户咨询时间|DATE">
      	   <Input class= 'common' name=LogTime style='width:44' ondblclick=" dLogTime();">
         </TD>
         <TD  class= title>
           客户咨询方式
         </TD>
         <TD  class= input>
           <Input class=codeno name=AskMode ondblclick="return showCodeList('hmaskmode',[this,AskModeName],[1,0]);" onkeyup="return showCodeListKey('hmaskmode',[this,AskModeName],[1,0]);" verify="客户咨询方式|len<=8"><Input class= 'codename' name=AskModeName >            
         </TD>
         <TD class = title >         	
         </TD>
         <TD class = input>         	
         </TD>    	
     	</TR>    	
     	<TR  class= common>   
         <TD  class= title>
           咨询问题类别
         </TD>
         <TD  class= input>
           <Input class=codeno name=AskStyle ondblclick=" return showCodeList('hmconsulttype',[this,AskStyle_ch],[0,1],null,null,null,1);" ><Input class= 'codename' name=AskStyle_ch >
         </TD>
         <TD  class= title>
           咨询问题
           <!--LLConsult--咨询内容-->
         </TD>
         <TD  class= input>
           <Input class= 'common' name=CContent  ondblclick="return inputQuestion();">
         </TD>
         <TD class = title >
         	
         </TD>
         <TD class = input>
         	
         </TD>
     	</TR>
     </table>
   </Div>    
	 <table>
    	<tr>
    		 <td>
    		      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHConsult2);">
    		 </td>
    		 <td class= titleImg>
        		 咨询状态
       	 </td>   		 
    	</tr>
   </table>
   <Div  id= "divLHConsult2" style= "display: ''">
     <table  class= common align='center' >
	    <TR class = comnon>
		     <TD class = title>		     	
		     	咨询作业状态
		     	<!--LLConsult
							回复状态	ReplyState	"hm：咨询作业状态
							0-提起询问，未进行回复
							1-该询问已经回复，但不完全，可以对其修改；
							2-该询问已经进行了完整的回复
					-->		     	
		     </TD>
		     <TD class = input>
		     	 <Input class=codeno name=ReplyState  ondblclick="return showCodeList('hmreplystate',[this,ReplyStateName],[1,0],null,null,null,1);" onkeyup="return showCodeListKey('hmreplystate',[this,ReplyStateName],[1,0]);" verify="客户咨询方式|len<=20"><Input class= 'codename' name=ReplyStateName >
         </TD>
         <TD  class= title>
         	 <Div  id= "divConsultDelay1" style= "display: 'none'">
           		延时剩余时间（小时）
           </div>
           <Div  id= "divConsultDelay2" style= "display: 'none'">
            	咨询总延时（小时）
           </div>
         </TD>
         <TD  class= input>
         	  <Div  id= "divConsultDelay3" style= "display: 'none'">
               <Input class= 'common' name=DelayRemain  >
            </div>
            <Div  id= "divConsultDelay4" style= "display: 'none'">
               <Input class= 'common' name=AskTotalTime >
            </div>
         </TD>
         <TD  class= title>
            <Div  id= "divConsultDelay5" style= "display: 'none'">
            咨询回复方式
           </div>
         </TD>
         <TD  class= input> 
         	 <Div  id= "divConsultDelay6" style= "display: 'none'">
             <Input class= 'common' name=AnswerMode_1 ><!--LLMainAsk解答方式-->
           </div>
         </TD>
      </TR>
     </table>
	 </div>
	 <table>
   	<tr>
   		<td>
   		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHConsult3);">
   		</td>
   	  <td class= titleImg>
       		 咨询回复
      </td>   		 
   	</tr>
   </table>
   <Div  id= "divLHConsult3" style= "display: ''">
      <table  class= common align='center' >
      		<TR class = common>
      			<TD class = title>
      				是否即时答疑
      			</TD>
      			<TD class = input><!--答疑类型-->
      				<Input class=codeno name=AnswerType verify="是否即时答疑|len<=2" CodeData= "0|^1|是^2|否"  ondblClick= "showCodeListEx('AnswerType',[this,AnswerType_ch],[0,1],null,null,null,1);" ><Input class = 'codename' name=AnswerType_ch >
      			</TD>
      			<TD class = title>
      			</TD>
      			<TD class = input>
      			</TD>
      			<TD class = title>
      			</TD>
      			<TD class = input>
      			</TD>
      		</TR>
      </table>		
	 </div>
	 <Div  id= "DelayAnswerTypeTable" style= "display: 'none'">
       <TABLE class=common  align='center'>		
       		<TR class = common>
       			<TD class = title>
       				延时答疑方式<!--没有-->
       			</TD>
       			<TD class = input>
       				<input type=hidden name=DelayAnswerType>
       				<Input class = 'code' name=DelayAnswerType_ch verify="延时答疑方式|len<=10" CodeData= "0|^1|短时延时^2|长时延时"  ondblClick= "showCodeListEx('DelayAnswerType',[this,DelayAnswerType],[1,0],null,null,null,1);" >
       			</TD>
       			<TD class = title>
       			</TD>
       			<TD class = input>
       			</TD>
       			<TD class = title>
       			</TD>
       			<TD class = input>
       			</TD>
       		</TR>
       		<TR class = common>
	   			<TD class = title>
	   				延时起始时间
	   			</TD>
	   			<TD class = input>
	   				<Input class= 'coolDatePicker' style='width:90' name=DelayBeginDate   verify="延时起始时间|DATE">
      	    <Input class= 'common' name=DelayBeginTime style='width:44' ondblclick=" dDelayBeginTime();">
	   			</TD>
	   			<TD class = title>
	   				延时截止时间
	   			</TD>
	   			<TD class = input>
					  <Input class= 'coolDatePicker' style='width:90' name=DelayEndDate   verify="延时截止时间|DATE">
      	    <Input class= 'common' name=DelayEndTime style='width:44' ondblclick=" dDelayEndTime();">
	   			</TD>
	   			<TD class = title>
	   			</TD>
	   			<TD class = input>
	   			</TD>
	   		</TR>
	   		<TR class = common>
	   			<TD class = title>
	   				约定解答时间
	   			</TD>
	   			<TD class = input>
	   				<Input class= 'coolDatePicker' style='width:90' name=ExpectRevertDate   verify="约定解答时间|DATE">
      	    <Input class= 'common' name=ExpectRevertTime style='width:44' ondblclick=" dExpectRevertTime();">
	   			</TD>
	   			<TD class = title>
	   				约定回复方式
	   			</TD>
	   			<TD class = input>
	   				<Input type=hidden name=PromiseAnswerType >
            <Input class= 'code' name=PromiseAnswerType_ch ondblclick="return showCodeList('hmaskmode',[this,PromiseAnswerType],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('hmaskmode',[this,PromiseAnswerType],[0,1]);" verify="约定回复方式|len<=20">
	   			</TD>
	   			<TD class = title>
	   			</TD>
	   			<TD class = input>
	   			</TD>
	   		</TR>
	   		<TR class = common>
	   			<TD class = title>
	   				是否解答
	   			</TD>
	   			<TD class = input>
	   				<Input type=hidden name=IsAnswer>
<!--SendFlag-->	   				<Input class = 'code' name=IsAnswer_ch verify="是否解答|len<=2" CodeData= "0|^1|是^2|否"  ondblClick= "showCodeListEx('IsAnswer',[this,IsAnswer],[1,0],null,null,null,1);" >
	   			</TD>
	   			<TD class = title>
	   			</TD>
	   			<TD class = input>
	   			</TD>
	   			<TD class = title>
	   			</TD>
	   			<TD class = input>
	   			</TD>
	   		</TR>
	
       </TABLE>
	 </Div>
   		
	<Div class= common id= "NeedContinueAnswerTable" style= "display: 'none'">
     <Table  class= common  style= "display: ''" align=center>
     	 <TR class = common>
     	 	<TD class = title>
<!--AvaiFlag-->	 		是否继续解答
     	 	</TD>
     	 	<TD class = input>
     	 		<Input type=hidden name=IsContinueAnswer>
     	 		<Input class = 'code' name=IsContinueAnswer_ch verify="是否继续解答|len<=2" CodeData= "0|^1|是^2|否"  ondblClick= "showCodeListEx('IsContinueAnswer',[this,IsContinueAnswer],[1,0],null,null,null,1);" >
     	 	</TD>
     	 	<TD class = title>
     	 	</TD>
     	 	<TD class = input>
     	 	</TD>
     	 	<TD class = title>
     	 	</TD>
     	 	<TD class = input>
     	 	</TD>
     	 </TR>
     </table>
	 </Div>	
	 <Div class= common id="NeedAnswerTable" style= "display: 'none'" align='center'>
	   <TABLE class= common >
	   	<TR class = common>
	   		<TD class = title>
	   			解答方式
	   		</TD>
	   		<TD class = input>
	   			<Input  class=codeno name=AnswerMode  ondblclick="return showCodeList('hmaskmode',[this,AnswerMode_ch],[1,0]);" onkeyup="return showCodeListKey('hmaskmode',[this,AnswerMode_ch],[1,0]);" verify="解答方式|len<=20"><Input class= 'codename' name=AnswerMode_ch >
	   		</TD>
	   		<TD class = title>
	   		</TD>
	   		<TD class = input>
	   		</TD>
	   		<TD class = title>
	   		</TD>
	   		<TD class = input>
	   		</TD>
	   	</TR>
	   	<TR class = common>
	   		<TD class = title>
	   			解答开始时间
	   		</TD>
	   		<TD class = input>
	   				<Input class= 'coolDatePicker' style='width:90' name=AnswerDate   verify="解答开始时间|DATE">
      	    <Input class= 'common' name=AnswerTime style='width:44' ondblclick=" dAnswerTime() ">
	   		</TD>
	   		<TD class = title>
	   			解答结束时间
	   		</TD>
	   		<TD class = input>
	   				<Input class= 'coolDatePicker' style='width:90' name=AnswerEndDate   verify="解答结束时间|DATE">
      	    <Input class= 'common' name=AnswerEndTime style='width:44' ondblclick=" dAnswerEndTime() ">
	   		</TD>
	   		<TD class = title>
	   			解答花费时间（分钟）
	   		</TD>
	   		<TD class = input>
	   			<Input class = 'common' name=AnswerCostTime ondblclick=" dAnswerEndTime2() "><!--LLAnswerInfo-->
	   		</TD>
	   	</TR>
	   	<TR class = common>
	   		<TD class = title>
	   			解答内容
	   		</TD>
	   	</tr>
     </TABLE>
	  <table class=common align='center'>
	  <tr>
	  	<td><!--LLAnswerInfo-->
	   <TextArea class= 'common' name=Answer cols="100%" rows="4" witdh=200px verify="解答内容|len<=1000"></TextArea>
	 	</td>
	 	</tr>
	 	</table>	
   </Div>	
	 <Div class= common id= "NoAnswerCauseTable" style= "display: 'none'">
      <TABLE class= common  style= "display: '' " align=center>
      		<TR class = common>
      			<TD class = title>
      				未解答原因
      			</TD>
      		</TR>
      		<TR class= common>
      			<TD class = input><!--LLAnswerInfo-->
      				<TextArea class= 'common' name=Remark cols="100%" rows="4" witdh=100% verify="未解答原因|len<=1000"></TextArea>
      			</TD>
      
      		</TR>
      </TABLE>		
	 </Div>
	 
	 
	 
	 
	  
	 <div id="div1" style="display: 'none'">
      <table>
        <TR  class= common>
            <TD  class= title>
              操作员代码
            </TD>
            <TD  class= input>
              <Input class= 'common' name=Operator >
            </TD>
            <TD  class= title>
              入机日期
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeDate >
            </TD>
            <TD  class= title>
              入机时间
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeTime >
            </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最后一次修改日期
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyDate >
          </TD>
          <TD  class= title>
            最后一次修改时间
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyTime >
          </TD>
          <TD  class= input>
            <Input class= 'common' name=MngCom >
          </TD>
        </TR>
      </table>
   </div>

    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="ConsultNo" name="ConsultNo">
    <input type=hidden id="iscomefromquery" name="iscomefromquery">
    <input type=hidden id="LogNo" name="LogNo">
    <input type=hidden id="AskType" name="AskType">
    
    
    
    <input type=hidden name="SerialNo">
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
		<script>
//			通过综合查询进入此页面的处理
				function passQuery()
				{
						
						if("<%=request.getParameter("logno")%>" == "" || "<%=request.getParameter("logno")%>" == "null")
					  {
					  		return;
					  }
					  else
						{
								var tLogno = "<%=request.getParameter("logno")%>";
								
								
									var arrResult = new Array();
									var strSQL="select a.LogNo,a.LogState,a.AskType,a.AskMode,a.LogerNo,b.name,a.LogDate,a.AnswerType,a.AnswerMode, "
									+"a.ExpectRevertDate,a.AskStyle,a.PromiseAnswerType,a.DelayRemain,a.DelayEndDate,a.DelayBeginDate ,a.makedate,a.maketime, "
									+"a.SendFlag,a.AvaiFlag,a.DelayRemain,a.LogTime,a.DelayBeginTime,a.DelayEndTime,a.ExpectRevertTime,a.OtherNoType "
									+" from LLMainAsk a,ldperson b where a.LogerNo=b.customerno and LogNo='"+tLogno+"'";
									                   	
									arrResult = easyExecSql(strSQL);
									var arrResult1 = arrResult;
									
									if( arrResult != null )
									{
								        switch(arrResult[0][3])
								        {
											        case "1" : 
											        		fm.all('AskModeName').value = "电话";break;
															case "2" :
											        		fm.all('AskModeName').value = "电子邮件";break;
											        case "3" :
											        		fm.all('AskModeName').value = "传真";break;
											        case "4" :
											        		fm.all('AskModeName').value = "短信";break;
											        case "5" :
											        		fm.all('AskModeName').value = "其他";break;  	
											        default  : alert("1");
								        } //switch 
								                    
								        fm.all('LogNo').value= arrResult[0][0];
								        //alert(arrResult[0][0]);
								 //       fm.all('LogState').value= arrResult[0][1];
								        fm.all('AskType').value= arrResult[0][2];
								        fm.all('AskMode').value= arrResult[0][3];
								        fm.all('LogerNo').value= arrResult[0][4];
								        fm.all('CustomerName').value= arrResult[0][5];
								        fm.all('LogDate').value= arrResult[0][6];
								        fm.all('AnswerType').value= arrResult[0][7];if(arrResult[0][7]=="2"){fm.all('AnswerType_ch').value="否";}if(arrResult[0][7]=="1"){fm.all('AnswerType_ch').value="是";}
								        fm.all('AnswerMode').value= arrResult[0][8];fm.all('AnswerMode_ch').value=easyExecSql("select codename from ldcode where codetype='hmaskmode' and code = '"+arrResult[0][8]+"'");
								        fm.all('ExpectRevertDate').value= arrResult[0][9];
								        fm.all('AskStyle').value= arrResult[0][10];fm.all('AskStyle_ch').value=easyExecSql("select codename from ldcode where codetype='hmconsulttype' and code ='"+arrResult[0][10]+"'");
								        fm.all('PromiseAnswerType').value= arrResult[0][11];
								        fm.all('PromiseAnswerType_ch').value= easyExecSql("select codename from ldcode where codetype='hmaskmode' and code='"+arrResult[0][11]+"'");
								        fm.all('DelayRemain').value= arrResult[0][12];
								        fm.all('DelayEndDate').value= arrResult[0][13];
								        fm.all('DelayBeginDate').value= arrResult[0][14];
								        fm.all('MakeDate').value= arrResult[0][15];
								        fm.all('MakeTime').value= arrResult[0][16];
								        fm.all('IsAnswer').value= arrResult[0][17];if(arrResult[0][17]=="1"){fm.all('IsAnswer_ch').value="是";}if(arrResult[0][17]=="2"){fm.all('IsAnswer_ch').value="否";}
								        fm.all('IsContinueAnswer').value= arrResult[0][18];if(arrResult[0][18]=="1"){fm.all('IsContinueAnswer_ch').value="是";}if(arrResult[0][18]=="2"){fm.all('IsContinueAnswer_ch').value="否";}
												fm.all('DelayRemain').value= arrResult[0][19];
												fm.all('LogTime').value= arrResult[0][20];
												fm.all('DelayBeginTime').value= arrResult[0][21];
												fm.all('DelayEndTime').value= arrResult[0][22];
												fm.all('ExpectRevertTime').value= arrResult[0][23];
												fm.all('DelayAnswerType').value= arrResult[0][24];if(arrResult[0][24]=="1"){fm.all('DelayAnswerType_ch').value="短时延时";}if(arrResult[0][24]=="2"){fm.all('DelayAnswerType_ch').value="长时延时";}
												
								        arrResult=null;
								        strSQL="select ConsultNo,CContent,ReplyState from LLConsult where 1=1 "
								        +  getWherePart("LogNo", "LogNo");
								        +  getWherePart("ConsultNo","LogerNo");
								        arrResult = easyExecSql(strSQL); 
								        fm.all('ConsultNo').value= arrResult[0][0];            
								        fm.all('CContent').value= arrResult[0][1];         
								         
								         switch(arrResult[0][2])                             
								         {                                                   
										         	case "1" :                                          
										         		fm.all('ReplyStateName').value = "进行中";
										         		divConsultDelay1.style.display= '';
																divConsultDelay3.style.display='';
																divConsultDelay2.style.display='none';         
																divConsultDelay4.style.display='none';
																divConsultDelay5.style.display='none';         
																divConsultDelay6.style.display='none';
																break;      
										         case "2" :                                          
										         		fm.all('ReplyStateName').value = "咨询结束--问题已解答";
										         		divConsultDelay1.style.display= 'none';
																divConsultDelay3.style.display='none';
																divConsultDelay2.style.display='';           
																divConsultDelay4.style.display= ''; 
																divConsultDelay5.style.display='';           
																divConsultDelay6.style.display= ''; 
										         		break;  
										         case "3" :                                          
										         		fm.all('ReplyStateName').value = "咨询结束--问题未解答";
										         		divConsultDelay1.style.display= 'none';
														    divConsultDelay3.style.display='none';
																divConsultDelay2.style.display='none';        
																divConsultDelay4.style.display='none'; 
																divConsultDelay5.style.display='none';        
																divConsultDelay6.style.display='none';
										         		break;    
										         default :   	   
								         }	//switch
								         
								         
								                                                            
								        arrResult=null;                                                       
								        strSQL="select Answer ,AnswerDate,AnswerEndDate ,AnswerCostTime,Remark,AnswerTime,AnswerEndTime from LLAnswerInfo where 1=1 "                       
								        +  getWherePart("LogNo", "LogNo")   
								        +  getWherePart("ConsultNo", "ConsultNo");                                
								        arrResult = easyExecSql(strSQL);                                     
								                              
								        fm.all('Answer').value= arrResult[0][0]; 
								        fm.all('AnswerDate').value= arrResult[0][1];
								        fm.all('AnswerEndDate').value= arrResult[0][2];
								        fm.all('AnswerCostTime').value= arrResult[0][3];
								        fm.all('Remark').value= arrResult[0][4];
								        fm.all('AnswerTime').value= arrResult[0][5];
								        fm.all('AnswerEndTime').value= arrResult[0][6];
								        	             
								        fm.all('iscomefromquery').value="1";	 
								        
								        
								        if(arrResult1[0][7]==1)
												{
														NeedAnswerTable.style.display='';
												}
												if(arrResult1[0][7]==2)
												{//alert(arrResult1[0][17]);
															if(arrResult1[0][17]==1)
															{
																NeedAnswerTable.style.display='';
															}
															if(arrResult1[0][17]==2)
															{
																		NeedContinueAnswerTable.style.display='';
																		if(arrResult1[0][18]==1)
																		{
																			NeedAnswerTable.style.display='';
																		}
																		if(arrResult1[0][18]==2)
																		{
																			NoAnswerCauseTable.style.display= '';
																		}
															}
															DelayAnswerTypeTable.style.display='';						
												}
											        	                    
										}//if    	                

								
						}//else
				}	//function		
		</script>
</html>