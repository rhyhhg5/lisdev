<%
//程序名称：LDDiseaseNameQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-05-24 10:05:39
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('DiseaseCommonName').value = "";
    fm.all('ICDCode').value = "";
    fm.all('ICDName').value = "";
    fm.all('MainteinFlag').value = "";
  }
  catch(ex) {
    alert("在LDDiseaseNameQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDDiseaseNameGrid();  
  }
  catch(re) {
    alert("LDDiseaseNameQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDDiseaseNameGrid;
function initLDDiseaseNameGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="疾病俗称";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="疾病ICD代码";         		//列名
    iArray[2][1]="60px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="疾病标准名称";         		//列名
    iArray[3][1]="100px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();                        
    iArray[4][0]="维护标记";         		//列名
    iArray[4][1]="30px";         		//列名      
    iArray[4][3]=0;         		//列名            
    

    LDDiseaseNameGrid = new MulLineEnter( "fm" , "LDDiseaseNameGrid" ); 
    
    LDDiseaseNameGrid.canSel = 1;
    //这些属性必须在loadMulLine前
/*
    LDDiseaseNameGrid.mulLineCount = 0;   
    LDDiseaseNameGrid.displayTitle = 1;
    LDDiseaseNameGrid.hiddenPlus = 1;
    LDDiseaseNameGrid.hiddenSubtraction = 1;
    LDDiseaseNameGrid.canSel = 1;
    LDDiseaseNameGrid.canChk = 0;
    LDDiseaseNameGrid.selBoxEventFuncName = "showOne";
*/
    LDDiseaseNameGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDiseaseNameGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
