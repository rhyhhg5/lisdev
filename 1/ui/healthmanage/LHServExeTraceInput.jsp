<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHServExeTraceInput.jsp
//程序功能：
//创建日期：2006-03-09 14:31:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >

	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="LHServExeTraceInput.js"></SCRIPT>
  <%@include file="LHServExeTraceInputInit.jsp"%>
</head>
<body  onload="initForm();passQuery();" >
  <form action="./LHServExeTraceSave.jsp" method=post name=fm target="fraSubmit">
 <span id="operateButton" >
	<table  class=common align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
				<td class=button width="10%" align=right>
				<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
			</td>		
		</tr>
	</table>
</span>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 个单项目执行轨迹信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHServExeTrace1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
			    <TD  class= title>
			      客户号码
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=CustomerNo readonly>
			    </TD>
			    <TD  class= title>
			       客户姓名
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=Name  readonly>
			    </TD>
			    <TD  class= title>
			      保单号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ContNo readonly>
			      <INPUT class= 'common' type=hidden name=ServPlanNo1>
		  	</TR>
			<!--TR  class= common>
			    <TD  class= title>
			      个人保单号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ContNo >
			    </TD>
			    <TD  class= title>
			      服务执行状态
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ExeState >
			    </TD>
			</TR>
			<TR  class= common>
			    <TD  class= title>
			      服务执行时间
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ExeDate >
			    </TD>
			    <TD  class= title>
			      服务详细描述
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServDesc >
			    </TD>
			</TR-->
		</table>
    </Div>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServItemGrid);">
    		</td>
    		 <td class= titleImg>
        		 项目执行状态信息
       		 </td>   		 
    	</tr>
    </table>
    
	<Div id="divLHServExeTraceGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServExeTraceGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
    
    
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <Input type=hidden name=ManageCom >
	<Input type=hidden name=Operator >
	  
    <Input type=hidden name=MakeDate >
    <Input type=hidden name=MakeTime >
    <Input type=hidden name=ModifyDate >
    <Input type=hidden name=ModifyTime >
    <Input type=hidden name=ServPlanNo ><!--个人服务计划号码-->
    <Input type=hidden name=ServItemNo ><!--个人服务项目号码-->

  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
		<script>
		//通过综合查询进入此页面的处理
		var turnPage = new turnPageClass(); 
		function passQuery()
		{ 	
			  //进入此函数
			  		  
					  var ServPlanNo = "<%=request.getParameter("ServPlanNo")%>";
					
					
				  	if(ServPlanNo!=null||ServPlanNo>0) 
				  	{
				  	//	alert(ServPlanNo+"-----"+ServPlanNo);
				  			var strSql = " select distinct CustomerNo,Name,ContNo,ServPlanNo"
				  									+" from LHServPlan "
				  									+" where ServPlanNo = '"+ServPlanNo+"'"
				  									;
				  			var arrResult = easyExecSql(strSql);
 
								
								fm.all('CustomerNo').value = arrResult[0][0];
								fm.all('Name').value = arrResult[0][1];
								fm.all('ContNo').value = arrResult[0][2];
								fm.all('ServPlanNo1').value = arrResult[0][3];
								
								var strSql2 = " select distinct c.ServItemCode,(select distinct a.servitemname from LHHealthServPlan a where a.servitemcode=c.ServItemCode),c.ServItemType,b.ExeState,b.ExeDate,b.ServDesc,b.MakeDate,b.MakeTime,b.ServItemNo"
      											 +" from LHServExeTrace b,lhservitem c"
				  									 +" where  "
				  									 +"b.ServPlanNo = '"+ServPlanNo+"'"
				  									 +" and c.ServItemNo =b.ServItemNo "
      											 +" order by b.ServItemNo"
      											 ;
                     // alert(strSql2);
      				  turnPage.queryModal(strSql2,LHServExeTraceGrid); 
								
					  } 
					  else 
					  {
					  	  alert("LHMainCustomerHealth.js->没有此个人服务");
					  }
		    
	}
		
	</script>
</html>
