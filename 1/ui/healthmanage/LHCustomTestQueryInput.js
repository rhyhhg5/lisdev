/** 
 * 程序名称：LHCustomTestInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-01-17 18:32:42
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     

var strSql ="select distinct a.CustomerNo,b.Name, a.InHospitDate,  a.InHospitMode,a.InHospitNo ,"
           +"( select b.Hospitname from ldhospital b where b.HospitCode=a.HospitCode )" 
           +"from LHCustomInHospital a,LDPerson b where a.CustomerNo = b.CustomerNo and a.inhospitmode in ('31','32') "
           +getWherePart("a.InHospitMode","TestModeCode") 
           +getWherePart("a.HospitCode","HospitCode") 
           +getWherePart("a.CustomerNo","CustomerNo") 
           +getWherePart("b.Name","CustomerName")
           +getWherePart("a.InHospitDate","InHospitStartDate",">")
           +getWherePart("a.InHospitDate","InHospitEndDate","<")
	         +" and a.managecom like '"+manageCom+"%%'"
	         +" order by a.CustomerNo, a.InHospitDate desc";
	         //alert(strSql);
	         turnPage.queryModal(strSql, LHCustomTestGrid);
}
function showOne(parm1, parm2) {
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LHCustomTestGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			parent.opener.top.window.focus();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHCustomTestGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LHCustomTestGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function getMedicaItemCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select MedicaItemCode,MedicaItemName from LHCountrMedicaItem ";
    //alert("strsql :" + strsql);
    fm.all("MedicaItemCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}


function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  	//var strSql="select name from ldperson  where 1=1 "+getWherePart("CustomerNo","CustomerNo");
	  
  }
}