<%
//程序名称：LHGrpServItemQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-03-09 15:20:08
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('GrpServPlanNo').value = "";
    fm.all('GrpServItemNo').value = "";
    fm.all('ServItemCode').value = "";
    fm.all('GrpCustomerNo').value = "";
    fm.all('GrpName').value = "";
    fm.all('GrpContNo').value = "";
    fm.all('ComID').value = "";
    fm.all('ServPriceCode').value = "";
    fm.all('OrgType').value = "";
  }
  catch(ex) {
    alert("在LHGrpServItemQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHGrpItemGrid();  
  }
  catch(re) {
    alert("LHGrpServItemQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHGrpItemGrid;
function initLHGrpItemGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="标准服务项目";   
	  iArray[1][1]="120px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=1;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="服务项目号";   
	  iArray[2][1]="90px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=1;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="组织实施方式";   
	  iArray[3][1]="50px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=1;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="个/团属性标识";   
	  iArray[4][1]="50px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=1;
	  
	  iArray[5]=new Array(); 
	  iArray[5][0]="机构属性标识";   
	  iArray[5][1]="50px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=1;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="定价方式";   
	  iArray[6][1]="50px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=1;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="入机日期";   
	  iArray[7][1]="50px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=1;
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="入机时间";   
	  iArray[8][1]="40px";   
	  iArray[8][2]=20;        
	  iArray[8][3]=1;
	  
	  iArray[9]=new Array(); 
	  iArray[9][0]="团体服务计划号码";   
	  iArray[9][1]="0px";   
	  iArray[9][2]=20;        
	  iArray[9][3]=0;
    LHGrpItemGrid = new MulLineEnter( "fm" , "LHGrpItemGrid" ); 
    //这些属性必须在loadMulLine前

    LHGrpItemGrid.mulLineCount = 0;   
    LHGrpItemGrid.displayTitle = 1;
    LHGrpItemGrid.hiddenPlus = 1;
    LHGrpItemGrid.hiddenSubtraction = 1;
    LHGrpItemGrid.canSel = 1;
    LHGrpItemGrid.canChk = 0;


    LHGrpItemGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面

  }
  catch(ex) {
    alert(ex);
  }
}
</script>
