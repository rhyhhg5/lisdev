<%
//程序名称：LHGrpServPlanQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-03-09 11:34:19
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('GrpServPlanNo').value = "";
    fm.all('GrpContNo').value = "";
    fm.all('GrpCustomerNo').value = "";
    fm.all('GrpName').value = "";
    fm.all('ServPlanCode').value = "";
    fm.all('ServPlanName').value = "";
    fm.all('ServPlanLevel').value = "";
    fm.all('ServPlayType').value = "";
    fm.all('ComID').value = "";
    fm.all('StartDate').value = "";
    fm.all('EndDate').value = "";
    fm.all('ServPrem').value = "";
//    fm.all('ManageCom').value = "";
//    fm.all('Operator').value = "";
//    fm.all('MakeDate').value = "";
//    fm.all('MakeTime').value = "";
//    fm.all('ModifyDate').value = "";
//    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LHGrpServPlanQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHGrpServPlanGrid();  
  }
  catch(re) {
    alert("LHGrpServPlanQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHGrpServPlanGrid;
function initLHGrpServPlanGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名                         
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="团单号";         		//列名
    iArray[1][1]="80px";         		//列名
    iArray[1][3]=0;         		//列名    
    
    iArray[2]=new Array();
    iArray[2][0]="团体客户号";         		//列名
    iArray[2][1]="80px";         		//列名
    iArray[2][3]=0;         		//列名    
    
    iArray[3]=new Array();
    iArray[3][0]="团体客户名称";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=0;         		//列名    
    
    iArray[4]=new Array();
    iArray[4][0]="客户分级标识";         		//列名
    iArray[4][1]="30px";         		//列名
    iArray[4][3]=0;         		//列名    
    
    iArray[5]=new Array();
    iArray[5][0]="健管保费（元）";         		//列名
    iArray[5][1]="30px";         		//列名
    iArray[5][3]=0;         		//列名    
    
    iArray[6]=new Array();
    iArray[6][0]="机构属性标识";         		//列名
    iArray[6][1]="30px";         		//列名
    iArray[6][3]=0;         		//列名    
    
    iArray[7]=new Array();
    iArray[7][0]="服务计划起始时间";         		//列名
    iArray[7][1]="50px";         		//列名
    iArray[7][3]=0;         		//列名    
    
    iArray[8]=new Array();
    iArray[8][0]="服务计划终止时间";         		//列名
    iArray[8][1]="50px";         		//列名
    iArray[8][3]=0;         		//列名  
      
    iArray[9]=new Array();
    iArray[9][0]="入机日期";         		//列名
    iArray[9][1]="0px";         		//列名
    iArray[9][3]=3;         		//列名    
    
    iArray[10]=new Array();
    iArray[10][0]="入机时间";         		//列名
    iArray[10][1]="0px";         		//列名
    iArray[10][3]=3;         		//列名    
    
    
    
    
    
    
  
    LHGrpServPlanGrid = new MulLineEnter( "fm" , "LHGrpServPlanGrid" ); 
    //这些属性必须在loadMulLine前

    LHGrpServPlanGrid.mulLineCount = 0;   
    LHGrpServPlanGrid.displayTitle = 1;
    LHGrpServPlanGrid.hiddenPlus = 0;
    LHGrpServPlanGrid.hiddenSubtraction = 0;
    LHGrpServPlanGrid.canSel = 1;
    LHGrpServPlanGrid.canChk = 0;
   // LHGrpServPlanGrid.selBoxEventFuncName = "showOne";

    LHGrpServPlanGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHGrpServPlanGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
