<%
//程序名称：财务接口日志表操作
//程序功能：对财务接口日志表的查询和删除操作
//创建日期：2003-7-22
//创建人  ：guoxiang
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.otof.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  
  CErrors tError = null;  
  String Content = "";
  String FlagStr = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //后面要执行的动作，查询，删除
  //执行动作：查询query ，delete 删除纪录
  String transact  = request.getParameter("transact").trim();
  System.out.println(transact);
  if (transact.equals("Query")) {
     
     //接收信息，并作校验处理。
     //输入参数
     String Batchno   = request.getParameter("Batchno");
     String Voucherid = request.getParameter("Voucherid");
     String Makedate  = request.getParameter("Makedate"); 
     String Bussdate  = request.getParameter("Bussdate"); 
     LITranInfoSchema  tLITranInfoSchema = new LITranInfoSchema(); 
     tLITranInfoSchema.setBatchNo(Batchno); 
     tLITranInfoSchema.setVoucherID(Voucherid); 
     tLITranInfoSchema.setMakeDate(Makedate); 
     tLITranInfoSchema.setBussDate(Bussdate);
  
     // 准备传输数据 VData 
     VData tVData = new VData();
     OtoFLogClearUI tOtoFLogClearUI = new OtoFLogClearUI();
     
     try{
        tVData.addElement(tG);
        tVData.add(tLITranInfoSchema);
        tOtoFLogClearUI.submitData(tVData,transact);
       
     }
     catch(Exception ex) {
       System.out.println(transact+"财务接口日志表时出错！没有相关数据存在");
       Content = transact+"失败，原因是: " + tOtoFLogClearUI.mErrors.getError(0).errorMessage;
       FlagStr = "Fail";
     }   
    //如果在Catch中发现异常，则不从错误类中提取错误信息
    if (FlagStr==""){
       tError = tOtoFLogClearUI.mErrors;
       if (!tError.needDealError()){                       
           Content = transact+"成功";
           FlagStr = "Succ";
       }
       else{
    	     Content = transact+"失败，原因是:" + tError.getFirstError();
    	     FlagStr = "Fail";
        }
     }
	  if (FlagStr == "Succ")
  	{
  		
  		  tVData.clear();
	      int recordCount = 0;
        //财务接口日志表
        LITranInfoSet  tLITranInfoSet = new LITranInfoSet();
	      tVData = tOtoFLogClearUI.getResult();
	      System.out.println("getest1:"+tVData.size());
	      tLITranInfoSet=(LITranInfoSet)tVData.getObjectByObjectName("LITranInfoSet",0);
        System.out.println("getest:"+tLITranInfoSet.size());
       
       
        recordCount=tLITranInfoSet.size();
        if (recordCount>0)
        {
         String strRecord="0|"+recordCount+"^";
         strRecord=strRecord+tLITranInfoSet.encode();       
%>
        <script language="javascript">
        //调用js文件中显示数据的函数
        parent.fraInterface.showRecord("<%=strRecord%>"); 
        </script>                
<%
        }
    }    
 
    System.out.println("QUERY END"); 
  }
  if(transact.equals("Delete")){
 
       String[] strBatchno  =request.getParameterValues("OtoFLogClearQueryGrid1");
       String[] strVoucherid=request.getParameterValues("OtoFLogClearQueryGrid2");
 	     String[] strBussno   =request.getParameterValues("OtoFLogClearQueryGrid3");
	     String[] strPolno    =request.getParameterValues("OtoFLogClearQueryGrid4");
	     String[] strBillid   =request.getParameterValues("OtoFLogClearQueryGrid5");
	     String[] strMakedate =request.getParameterValues("OtoFLogClearQueryGrid6");
	     String[] strMaketime =request.getParameterValues("OtoFLogClearQueryGrid7");
	     String[] strBussdate =request.getParameterValues("OtoFLogClearQueryGrid8");
	     int OtoFLogClearCount = 0;
	     String OtoFLogClearNo[] = request.getParameterValues("OtoFLogClearQueryGridNo");
	     if (OtoFLogClearNo!=null)
		       OtoFLogClearCount = OtoFLogClearNo.length;
	     else
		       OtoFLogClearCount = 0;
		   
		   LITranInfoSet  mLITranInfoSet = new LITranInfoSet();
		   for (int i = 0;i<OtoFLogClearCount;i++){  
	         LITranInfoSchema mLITranInfoSchema=new LITranInfoSchema();
	         mLITranInfoSchema.setBatchNo(strBatchno[i]);
	         mLITranInfoSchema.setBillId(strBillid[i]);
	         mLITranInfoSchema.setBussNo(strBussno[i]);
	         mLITranInfoSchema.setPolNo(strPolno[i]);
	         mLITranInfoSchema.setMakeDate(strMakedate[i]);
           mLITranInfoSchema.setMakeTime(strMaketime[i]);
	         mLITranInfoSchema.setBussDate(strBussdate[i]);
	         mLITranInfoSchema.setVoucherID(strVoucherid[i]);
	         mLITranInfoSet.add(mLITranInfoSchema);      
	     }   
	     
	     OtoFLogClearUI mOtoFLogClearUI = new OtoFLogClearUI();
	     // 准备传输数据 VData 
       VData mVData = new VData();
       try{
          mVData.addElement(mLITranInfoSet);
          mOtoFLogClearUI.submitData(mVData,transact);
       }
       catch(Exception ex) {
          System.out.println(transact+"财务接口日志表时出错！没有相关数据存在");
          Content = transact+"失败，原因是: " + mOtoFLogClearUI.mErrors.getError(0).errorMessage;
          FlagStr = "Fail";
       }   
       //如果在Catch中发现异常，则不从错误类中提取错误信息
       if (FlagStr==""){
           tError = mOtoFLogClearUI.mErrors;
           if (!tError.needDealError()){                       
               Content = transact+"成功";
               FlagStr = "Succ";
           }
           else{
    	         Content = transact+"失败，原因是:" + tError.getFirstError();
    	         FlagStr = "Fail";
           }
       }

	     if (FlagStr == "Succ"){
%>          
            <script language="javascript">
               parent.fraInterface.clearRecord();
               parent.fraInterface.OtoFLogClearQueryGrid.clearData();
            </script>                
<%       
          
       }    
 
       System.out.println("DELETE END"); 
    }
%>                                        
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>




