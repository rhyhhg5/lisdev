<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：calAccInterestDetail.jsp
//程序功能：帐户结息
//创建日期：2004-09-26 11:08:36
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	
  String tPolNo = "";
  tPolNo = request.getParameter("PolNo");
  String tOtherNo = "";
  tOtherNo = request.getParameter("OtherNo");
  String tInsuAccNo = "";
  tInsuAccNo = request.getParameter("InsuAccNo"); 
  System.out.println(tPolNo + " --- " + tOtherNo + " --- " + tInsuAccNo);  
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var PolNo = "<%=tPolNo%>";      //个人单的查询条件.
	var OtherNo = "<%=tOtherNo%>";          //个人单的查询条件.
	var InsuAccNo = "<%=tInsuAccNo%>";   //记录操作员
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="calAccInterestDetail.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="calAccInterestDetailInit.jsp"%>
  <title>累计生息账户领取申请 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./calAccInterestDetailChk.jsp">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      	　<TD  class= title> 保单号码 </TD>
          <TD  class= input> <Input class= common name=PolNo > </TD>
          <TD  class= title> 账户号 </TD>
          <TD  class= input> <Input class= common name=InsuAccNo > </TD>
          <TD  class= title> 其他号码 </TD>
          <TD  class= input> <Input class= common name=OtherNo > </TD>
          <input type=hidden id="fmtransact" name="fmtransact">
         </TR>
      </table>
      <hr/>

     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 账户出/入帐明细
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" >
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
  	   </table>			
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 	
			
  	</div>
  	<p>
 	</p>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol2);">
    		</td>
    		<td class= titleImg>
    			 结息信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCPol2" style= "display: ''">
    <table  class= common>
      	<TR  class= common>
       		<TD  class= title width="25%"> 结息日期 </TD>
       		<TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=BalaDate verify="结息日期|NOTNULL"></TD>
          <input type=hidden id="GetMoneyHide" name="GetMoneyHide">
         </TR>
    </table>
    <p></p>
 		</div>
    <table  class= common align=center>
    <INPUT VALUE="结息确认" class= common TYPE=button onclick="ApplyConfirm();"> 
  	</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
