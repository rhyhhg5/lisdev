
<%@ page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
</head>
<%
 //******************************************************
 // 程序名称：Logout.jsp
 // 程序功能:：
 // 最近更新人：DingZhong
 // 最近更新日期：2002-12-22
 //******************************************************
 %>
 
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.logon.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%
System.out.println("start logout");
System.out.println("start clear data...");
try {
/*
	//解锁
	LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
	tLDSysTraceSchema.setPolNo(mPolNo);
	tLDSysTraceSchema.setCreatePos("人工核保");
	tLDSysTraceSchema.setPolState("1001");
	LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
	inLDSysTraceSet.add(tLDSysTraceSchema);
	
	VData tVData = new VData();
	tVData.add(mGlobalInput);
	tVData.add(inLDSysTraceSet);
	
	LockTableBL LockTableBL1 = new LockTableBL();
	if (!LockTableBL1.submitData(tVData, "DELETE")) {
	System.out.println("解锁失败！");
	}
*/          
    GlobalInput tG1 = new GlobalInput();
    tG1=(GlobalInput)session.getValue("GI");
    System.out.println(tG1.ComCode);
    VData inputData = new VData();
    inputData.addElement(tG1);
    
    logoutUI tlogoutUI = new logoutUI(); 
    
    tlogoutUI.submitData(inputData,"LogOutProcess");
    System.out.println("completed clear data");

    
 } catch (Exception exception) {
 
 }
%>

    <script language=javascript>
        session = null;
    	top.window.location ="../indexlis.jsp";
    </script>  

</html>