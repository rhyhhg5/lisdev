<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<title>暂交费信息查询</title>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>   
  <script src="./OtoFLogClear.js"></script> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="OtoFLogClearInit.jsp"%>
</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name="fm"   target=fraSubmit method=post>
    <table  class= common align=center>
      	<TR  class= common>
          
      <TD  class= title> 批次号: </TD>
          <TD  class= input>
            <Input class= code name=Batchno>
		</TD>
         
      <TD  class= title>凭证号：</TD>
          <TD  class= input>
            <Input class= code name=Voucherid>
          </TD>         
       </TR>                     
      	<TR  class= common>
          <TD  class= title>
          入机日期:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Makedate>
          </TD>       	
          <TD  class= title>
          业务日期:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Bussdate >
          </TD>
       </TR>
       
   </Table>  
      <INPUT CLASS=COMMON TYPE="hidden" NAME="transact">
      <INPUT VALUE="查询" Class=common TYPE=button onclick="submitForm();"> 
      <INPUT VALUE="删除" Class=common TYPE=button onclick="deleteForm();">   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOtoFLogClear);">
    		</TD>
    		<TD class= titleImg>
    			 日志表信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divOtoFLogClear" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanOtoFLogClearQueryGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
 </Div>					

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
</html>
