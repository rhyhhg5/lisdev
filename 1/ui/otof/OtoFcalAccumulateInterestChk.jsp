<%
//程序名称：PEdorConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.otof.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  LCInsureAccSchema mLCInsureAccSchema   = new LCInsureAccSchema();
  CalAccInterestUI tCalAccInterestUI   = new CalAccInterestUI();
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
 System.out.println("-----"+tG.Operator);
  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
    
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String PrtSetStr = "";
  String transact = "INSERT";
  String tBalaDate = request.getParameter("BalaDate");
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    System.out.println("-----"+tBalaDate);
  //个人批改信息
 try
  {
  // 准备传输数据 VData
  
  	 VData tVData = new VData();   
  	 TransferData tTransferData= new TransferData();
  	 tTransferData.setNameAndValue("BalaDate",tBalaDate);
  	 tVData.addElement(tTransferData);    
	 tVData.addElement(tG);
		
	
	//此路径当确认项目为LR时用于打印的路径
      if(!tCalAccInterestUI.submitData(tVData,transact))
      {
        FlagStr="";
      }
      else
      {
	    //即使tApplyGetAccumulateUI失败了
	    if(transact.equals("INSERT"))
        {
        }       
	 }
    }
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCalAccInterestUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 处理成功";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 处理失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=PrtSetStr%>");
</script>
</html>

