//程序名称：calAccInterest.js
//程序功能：帐户结息
//创建日期：2004-09-26 11:08:36
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content);     
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    alert(content);
    easyQueryClick();
   //执行下一步操作
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	k++;
	var strSQL = "";
	strSQL = "select PolNo,RiskCode,AppntName,InsuredNo,InsuAccNo,sumpaym,frozenmoney,AccType,OtherNo from LCInsureAcc where "+k+" = "+k
				 //+ " and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+ComCode+"','Pd')=1 "
				 //+ " and AccType='004'"
				 + getWherePart( 'PolNo' )
				 + getWherePart( 'ManageCom', 'ManageCom')
				 + getWherePart( 'AppntName' )
				 + getWherePart( 'InsuredNo' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'InsuAccNo' )
				 + getWherePart( 'GrpPolNo' )
				 + " and ManageCom like '" + ComCode + "%%'"
				 + " order by PolNo,InsuAccNo,OtherNo";
				 //+ getWherePart( 'RiskVersion' );
				 
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有满足条件的累计生息账户信息！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

function applyGet()
{
  var i = 0;
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		tPolNo = PolGrid.getRowColData(tSel-1,1);
		tInsuAccNo = PolGrid.getRowColData(tSel-1,5);
		tOtherNo = PolGrid.getRowColData(tSel-1,9);	
		
	  if (tPolNo != " ")
	  {
        window.open( "calAccInterestDetailMain.jsp?PolNo=" + tPolNo + "&InsuAccNo=" + tInsuAccNo + "&OtherNo=" + tOtherNo)
    }	
 }	
  //fm.submit(); //提交
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}