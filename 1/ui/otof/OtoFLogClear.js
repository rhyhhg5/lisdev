//程序名称：OtoFLogClearInit.jsp
//程序功能：
//创建日期：2003-07-21 11:10:36
//创建人  ：GUOXIANG创建
//更新记录：  更新人    更新日期     更新原因/内容
//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();                //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm(){  
	 var i = 0;
   var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   OtoFLogClearQueryGrid.clearData("OtoFLogClearQueryGrid");
   fm.all('transact').value='Query';             //FORM 中的transact的值为 QUERY
   fm.action='./OtoFLogClearSave.jsp';           // 提交到OtoFLogClearSave.jsp页面处理
   fm.submit();                                  //提交
}



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
   showInfo.close();
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
   showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
}

//删除对应的处理函数
function deleteForm(){  
	 var strConfirm = confirm("是否删除,删除点击“确定”，否则点“取消”。");
	 if(strConfirm){
	 fm.all('transact').value='Delete';            //FORM 中的transact的值为 QUERY
   fm.action='./OtoFLogClearSave.jsp';           // 提交到OtoFLogClearSave.jsp页面处理
   fm.submit();   
  }                               //提交
}   


//清空显示数据
function clearRecord()
{
  //清空查询字符窜
  turnPage.strQueryResult  = "";
}	

//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord(strRecord)
{

  //保存查询结果字符串
  turnPage.strQueryResult  = strRecord;
  
  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;  
    
  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray = new Array(0,7,2,3,1,4,5,6);

  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = OtoFLogClearQueryGrid;             
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
	
}