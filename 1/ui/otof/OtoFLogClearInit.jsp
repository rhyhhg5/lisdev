<%
//程序名称：OtoFLogClearInit.jsp
//程序功能：
//创建日期：2003-07-19 11:10:36
//创建人  ：GUOXIANG创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
	// 查询条件
	  fm.all('Batchno').value = '';
    fm.all('Voucherid').value = '';
    fm.all('Makedate').value = '';
    fm.all('Bussdate').value = '';
  }
  catch(ex)
  {
    alert("在OtoFlogClearInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm()
{
  try
  {
    initInpBox();  
    initOtoFLogClearQueryGrid();
  }
  catch(re)
  {
    alert("在OtoFLogClearInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var  OtoFLogClearQueryGrid;
 
// 保单信息列表的初始化
function initOtoFLogClearQueryGrid()
  {                               
      
      var iArray = new Array();
      try
      {
      
      iArray[0]=new Array();
      iArray[0][0]="序号";         		    //列名
      iArray[0][1]="35px";            		//列宽
      iArray[0][2]=10;            		    //列最大值
      iArray[0][3]=0;              		    //是否允许输入,1表示允许，0表示不允许

      
      
      
      iArray[1]=new Array();
      iArray[1][0]="批次号";          		//列名
      iArray[1][1]="90px";      	      	//列宽
      iArray[1][2]=100;             			//列最大值
      iArray[1][3]=0;                     //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      
      
      iArray[2]=new Array();
      iArray[2][0]="凭证号";   		        //列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=100;            		    //列最大值
      iArray[2][3]=0;              		    //是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="收据号";     		       //列名
      iArray[3][1]="100px";             		 //列宽
      iArray[3][2]=60;            		     //列最大值
      iArray[3][3]=0;              		     //是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保单号";         		   //列名
      iArray[4][1]="100px";            		 //列宽
      iArray[4][2]=200;            		     //列最大值
      iArray[4][3]=0;              		     //是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="单证号";         		  //列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            	      //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="入机日期";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            	      //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="入机时间";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=200;            	      //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="业务日期";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=200;            	      //列最大值
      iArray[8][3]=0;  
      
      
      OtoFLogClearQueryGrid = new MulLineEnter( "fm" , "OtoFLogClearQueryGrid" ); 
      //这些属性必须在loadMulLine前
      OtoFLogClearQueryGrid.mulLineCount = 12;   
      OtoFLogClearQueryGrid.displayTitle = 1;
      OtoFLogClearQueryGrid.hiddenPlus = 1;
      OtoFLogClearQueryGrid.hiddenSubtraction = 1;
      OtoFLogClearQueryGrid.loadMulLine(iArray);  
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>