//该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass(); 

function easyQueryClick() 
{
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    
    fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, Result)
{	

  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;   
        //fm.all('Speccontent').value = Result;
        
        //保存查询结果字符串
        turnPage.strQueryResult  = Result;

        //使用模拟数据源，必须写在拆分之前
        turnPage.useSimulation   = 1;  
        
        //拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult, 0, 0, turnPage);
        
        //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
        turnPage.pageDisplayGrid = PolGrid;             
        
        //设置查询起始位置
        turnPage.pageIndex       = 0;  
        
        //在查询结果数组中取出符合页面显示大小设置的数组
        var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
        
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid, turnPage);	

  }  	
}
