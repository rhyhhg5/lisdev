<%
//程序名称：testESubmit.jsp
//程序功能：查询测试
//创建日期：2004-8-13 13:59
//创建人  ：JL
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.bl.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
//执行客户端的提交处理。（查询和保存）
//准备提交到后台程序的数据，并调用后台程序进行处理客户端的提交的命令

  //准备输入后台的数据  
  System.out.println("==> TextESubmit.jsp : ------Query Begin-----5");
  
  LCPolSchema tLCPolSchema = new LCPolSchema();
  TestEBL tTestEBL = new TestEBL();
  
  CErrors tError = null;            
  String FlagStr = "";
  String Content = "";
  Vector Result=null;
  
  //设置个人保单号
  tLCPolSchema.setPolNo(request.getParameter("PolNo"));
  	
//  try
//  {
    VData tVData = new VData();  	
    tVData.addElement(tLCPolSchema);

    String strLCPolSet = "";
    if(tTestEBL.QueryLCPol(tVData))
    {
        //Result=tTestEBL.getResult();
        //strLCPolSet = (String) Result.get(0);
    }
    else {
       FlagStr="查询失败！";
    }
    
//  }
//  catch(Exception ex)
//  {
//    Content = transact+"失败，原因是:" + ex.toString();
//    FlagStr = "Fail";
//    Content = " 查询失败！"
//  }			
//  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
//  if (FlagStr=="")
//  {
//    System.out.println("==> PEdorTypeSCSubmit.jsp : ------success");
//    tError = tTestEBL.mErrors;
//    if (!tError.needDealError())
//    {                          
//      Content = " 保存成功";
//      FlagStr = "Success";   
//    }
//    else                                                                           
//    {
//      Content = " 保存失败，原因是:" + tError.getFirstError();
//      FlagStr = "Fail";
//    }
//  }
//  //添加各种预处理

%>                      
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=tTestEBL.getResult()%>");
</script>
</html>