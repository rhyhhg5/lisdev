<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="AllProposalQueryInit.jsp"%>
  <SCRIPT src="AllProposalQuery.js"></SCRIPT>
  
  <title>保单查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./AllProposalQuerySubmit.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入个人保单查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>保单号码 </TD>
          <TD  class= input> <Input class= common name=PolNo > </TD>
          <TD  class= title> 集体保单号码</TD>
          <TD  class= input> <Input class= "code" name=GrpPolNo CodeData="0|^00000000000000000000|非集体单下的个人单" ondblclick="showCodeListEx('GrpPolNo',[this],[0]);" onkeyup="showCodeListKeyEx('GrpPolNo',[this],[0]);"> </TD>
          <TD  class= title> 印刷号码</TD>
          <TD  class= input> <Input class= common name=PrtNo ></TD>
        </TR>
        <TR  class= common>
          <TD  class= title> 险种编码</TD>
          <TD  class= input> <Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);"></TD>
          <TD  class= title> 代理人编码</TD>
          <TD  class= input> <Input class="code" name=AgentCode verify="代理人编码|notnull&code:AgentCode" ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [0, 2]);" onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [0, 2]);"> </TD>
          <TD  class= title> 代理人组别 </TD>
          <TD  class= input> <Input class=common name=AgentGroup verify="代理人组别|notnull&len<=12" > </TD>
        </TR>
        <TR  class= common>
          <TD  class= title> 管理机构 </TD>
          <TD  class= input> <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);"> </TD>
          <TD  class= title> 投保单号码 </TD>
          <TD  class= input> <Input class= common name=ProposalNo > </TD>
          <TD  class= title> 投保人姓名 </TD>
          <TD  class= input> <Input class= common name=AppntName > </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>被保人姓名 </TD>
          <TD  class= input><Input class= common name=InsuredName ></TD>
          <TD  class= title> 被保人性别 </TD>
          <TD  class= input> <Input class= common name=InsuredSex > </TD>
          <TD  class= title> 被保人出生日期 </TD>
          <TD  class= input><Input class= common name=InsuredBirthday ></TD>
        </TR>
    </table>
          <INPUT VALUE="查询" class = common TYPE=button onclick="easyQueryClick();"> 
          <!--INPUT VALUE="保单明细" TYPE=button onclick="getQueryDetail();"--> 					
          <INPUT VALUE="保单明细" class = common TYPE=button onclick="PolClick();">
          <INPUT VALUE="返回" name=Return class = common TYPE=button STYLE="display:none" onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>

      <INPUT VALUE="首页" class = common TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class = common TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class = common TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" class = common TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
