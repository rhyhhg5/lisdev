<%
//程序名称：万能续期应收记录统计
//程序功能：查询系统续期应收报表,通过交至日为查询时间段,查询保单的状态等信息,下载并打印清单。
//创建日期：2010-03-02 18:00
//创建人  ：Pangxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="PrtULIReceivableRecord.js"></SCRIPT>
  <%@include file="PrtULIReceivableRecordInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form method=post name=fm action="./PrtULIReceivableRecordSave.jsp" >
		<Table  class= common>
	    	<tr>
	    		<td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName elementtype=nacessary>
				</td>
				<td class= title>产品类型</td>
				<td class=input>
					<Input class= "codeno" name=ProductType CodeData="0|^0|全部^1|分红险^2|万能险^3|传统险" ondblclick="return showCodeListEx('ProductType',[this,ProductTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('ProductType',[this,ProductTypeName],[0,1]);" ><Input class=codename name=ProductTypeName readonly  >
				</td class=input>
				<td class= title>状态</td>
				<td>
					<Input class= "codeno" name=DealState CodeData="0|^0|待收费^1|收费成功^2|应收作废^3|已撤销^4|待核销^5|正转出^6|转出成功^7|未抽档^8|全部" ondblclick="return showCodeListEx('DealState',[this,DealStateName],[0,1]);" onkeyup="return showCodeListKeyEx('DealState',[this,DealStateName],[0,1]);" ><Input class=codename name=DealStateName readonly >
				</td>
			</tr>
	    	<tr>
	    	    <td class= title>保单号</td>
				<td class=input>
					<input class=common name=PolNo>
				</td>
				<td class= title>应收记录号</td>
				<td class=input>
					<input class=common name=GetNoticeNo>
				</td>
	    	</tr>
	    	<tr>
				<td class= title>应收时间起期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=StartDate elementtype=nacessary verify="应收时间起期|NOTNULL">
				</td>
				<td class= title>应收时间止期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=EndDate elementtype=nacessary verify="应收时间止期|NOTNULL">
				</td>
			</tr>
		</Table>
		<Table>
			<td>
				<input value="查  询" class = cssButton TYPE=button onclick="easyQuery();">
			</td>
			<td>
				<input value="打  印" class = cssButton TYPE=button onclick="easyPrint();">
			</td>
		</Table>
		<Div  id= "divrecord" align=center style= "display: ''" >  
			<Table class= common border=0 width=100% >		
				<tr  class= common>		
					<td><span id ="spanRableRecGrid" ></span></td>					
				</tr>
			</Table>
			<Div  id= "divrecordpage" align=center style= "display: ''" >
				<input value="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
				<input value="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
				<input value="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
				<input value="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();"> 
		    </Div> 
	    </Div> 
    	<input type=hidden id="arrset" name="arrset">
    	<input type=hidden id="strsql" name="strsql">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
