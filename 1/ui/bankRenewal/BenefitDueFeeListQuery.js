//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var turnPage1 = new turnPageClass();
var tInNoticeNo ="";
var mSql="";
var mDateFlag=true;
var SqlPDF = "";
var queryCondition = "";
var SQL1 = "";
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  //	parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
  }
  //resetForm();
   
}


//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function CheckGrpPolNo()
{
    var tSel = ContGrid.getSelNo();	
    if( tSel == 0 || tSel == null )
    {
	alert( "请先选择一条记录，再点击'团单续期个案催收'按钮。" );
	return false;
    }
    var tRow = ContGrid.getSelNo() - 1; 
    //说明应该在这里把窗体相关值填充！杨红于2005-07-19注释

    fm.all('ProposalGrpContNo').value=ContGrid.getRowColData(tRow, 1);
    return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function CheckDate()
{
  if(!isDate(fmMulti.all('StartDate').value)||!isDate(fmMulti.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fmMulti.all('StartDate').value,fmMulti.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

/*********************************************************************
 *  查询应收纪录信息、
 *  参数  ：  pmType ：1 按操作人等批次查询；2 按代理人等批次查询
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick(pmType)
{	
	// 初始化表格
	initJisPayGrid();
	initPolGrid();
	initContGrid();
	
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	var omnipCondition="";	
   
  
	//查询SQL 语句
	if (pmType=="1")
	{
		if(fm.all("StartDate").value!="")
		{
			CurrentTime=fm.all("StartDate").value;
		}
		if(fm.all("EndDate").value!="")
		{
			SubTime=fm.all("EndDate").value;
		}	   
		contCurrentTime=CurrentTime;
		contSubTime=SubTime;
		fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
		fm.all("EndDate").value=contSubTime;	
		var strSQL=""; 
		var strSQL11 = "";
	  if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
	    strSQL11 = " and a.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
	  }			
		
		 strSQL = "select distinct a.SerialNo,a.MakeDate,a.Operator, "
		     + "    (select ShowManageName(ComCode) from LDUser where UserCode = a.Operator), "
		     + "    '',sum(a.SumDuePayMoney) "
		     + "from LJSPayB a where  "
				 + "    a.othernotype='2' " 
				 + " and exists (select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.risktype4 ='2' and lcpol.Contno = a.OtherNo)"
				 + " and exists (select 1 from lccont where contno=a.OtherNo and salechnl in ('04','13')) "
				 + " and a.MakeDate>='"+CurrentTime+"' and a.MakeDate<='"+SubTime+"'"	
				 + " and a.ManageCom like '"+ managecom + "%%'"		
				 	
				 + getWherePart( 'a.ManageCom', 'ManageCom','like' )
				 + getWherePart( 'a.Operator','Operator' )
				+ " group by  a.SerialNo,a.MakeDate,a.Operator "
				+ " order by a.SerialNo desc"
				;
	
  }else
  {   
   
  	if(fm.all("MultStartDate").value!="")
		{
			CurrentTime=fm.all("MultStartDate").value;
		}
		if(fm.all("MultEndDate").value!="")
		{
			SubTime=fm.all("MultEndDate").value;
		}	   
		contCurrentTime=CurrentTime;
		contSubTime=SubTime;
		fm.all("MultStartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！		
	  fm.all("MultEndDate").value=contSubTime;
	  var strState = fm.all('DealState1').value //催收状态 3：全部
		var strConSql = "";
    if (strState=='5'){}else
	  {
	  	 strConSql = " and a.DealState='"+ fm.all('DealState1').value + "'";
	  }	
	  	
		var strSQL = "select distinct a.SerialNo,a.MakeDate,a.Operator, "
		    + "   (select ShowManageName(ComCode) from LDUser where UserCode = a.Operator), "
		    + "   '',sum(a.SumDuePayMoney)  "
		    + "from LJSPayB a "
		    + "where a.othernotype='2' " 
		    +" and exists (select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.risktype4='2' and lcpol.Contno = a.OtherNo)"
		    + " and exists (select 1 from lccont where contno=a.OtherNo and salechnl in ('04','13')) "
				+ " and a.GetNoticeNo in (select GetNoticeNo from ljspaypersonb where "
				+ " LastPayToDate>='"+CurrentTime+"'and  LastPayToDate<='"+SubTime+"' "
				+ " and a.ManageCom like '"+ managecom + "%%'"
				+ " AND getNoticeNo = a.getNoticeNo)"
				+ strConSql
				//+ getWherePart( 'a.AgentCode', 'AgentCode' )
			    + strSQL11
			    + getWherePart( 'a.AgentGroup','AgentGroup','like' )
				+ " group by  a.SerialNo,a.MakeDate,a.Operator "
				+ " order by a.SerialNo desc"				
				;
  }		
	turnPage.queryModal(strSQL, JisPayGrid); 	
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
		if(JisPayGrid.mulLineCount == 0)
	  {
	    alert("没有符合条件的可催收保单信息");
	  }
	
}

/*********************************************************************
 *  查询应收保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function contQueryClick()
{
	// 初始化表格
	initContGrid();
	initPolGrid();
	//initJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var rowNum=JisPayGrid.mulLineCount;
	if(rowNum==0)
	{
		alert("应收记录列表为空，请先查询！");
		return false;
	}
	var tCount = 0;
	
  for(var i=0;i<rowNum;i++)
  {                                                                   
    if(JisPayGrid.getChkNo(i))
		{
			tCount = tCount+1;
			if (tCount == 1)
			{
				tInNoticeNo = "'" + JisPayGrid.getRowColData( i , 1 )+ "'";
			}else
			{
				tInNoticeNo = tInNoticeNo + ",'" + JisPayGrid.getRowColData( i , 1 )+ "'";
			}
		}
  }
  var strSQL = "select  a.ContNo,a.AppntName,CodeName('sex',a.appntsex),"
  	 +" (select mobile from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno ),	"
		 +" (select (case when phone is null then homephone else phone end) from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno) "
  	 + " ,a.CValiDate,nvl((select nvl(AccGetMoney,0) from LCAppAcc where CustomerNo=a.appntno),0),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(c.ManageCom) mana,getUniteCode(a.AgentCode),b.GetNoticeNo,"
		 + " (case when (b.dealstate ='0' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno "
		 + " and riskcode in ('320106','120706')) is not null ) then '等待客户反馈' "
		 + " when (b.dealstate ='1' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno and riskcode in ('320106','120706')) is not null ) then '续保成功' "
		 + " when (b.dealstate ='3' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno  and riskcode in ('320106','120706')	) is not null ) then '满期给付' "
		 +" else (select codename  from ldcode where codetype='dealstate' and code=b.dealstate)  end),"
		 +	" (case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end)" 
		
		// + " (select codename  from ldcode where codetype='dealstate' and code=b.dealstate),(case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end)"
		 +",(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen from LCCont a,ljspayB b ,ljspaypersonB c where  "
		 + " a.ContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo " 
		 + " and b.OtherNoType='2'"
		 + " and  b.SerialNo in (" + tInNoticeNo + ")" 
		 + " and c.ManageCom like '"+ managecom +"%' "
		 + " group by a.ContNo,a.AppntName,a.CValiDate,a.SumPrem,a.appntno,b.SumDuePayMoney,c.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate,b.Cancelreason,a.appntsex"
     + " order by mana,agen,a.AgentCode,b.GetNoticeNo ";
    //添加strSQL2用来打印批单2006-11-03 by huxl.
    var strSQL2 = "";
    strSQL2 += " and a.ContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo " ;
		strSQL2 += " and b.OtherNoType='2'" ;
		strSQL2 += " and  b.SerialNo in (" + tInNoticeNo + ")" ;
		strSQL2 += " and a.salechnl in ('04','13') ";// add by lzy 20160408只查询银保渠道的
		strSQL2 += " and c.ManageCom like '"+ managecom ;
    SqlPDF = strSQL;
    mSql = strSQL2;
	turnPage1.queryModal(strSQL, ContGrid); 

	SQL1=strSQL ;
			if(ContGrid.mulLineCount == 0)
	  {
	    alert("没有符合条件的可催收保单信息");
	  }
	

}

/*********************************************************************
 *  查询个案应收纪录信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function singleQueryClick(pmType)
{
	// 初始化表格
	initJisPayGrid();
	// 初始化表格
	initContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
  initPolGrid();
      
   //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量   
	var omnipCodition="";	
	 omnipCodition=" and exists (select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.risktype4 ='2' and lcpol.Contno = a.ContNo) "
				  +" and a.salechnl in ('04','13') ";
  if (pmType == "1")
  {
	   	if(fm.all("SingleStartDate").value!="")
	   	{
				CurrentTime=fm.all("SingleStartDate").value;
	    }
		if(fm.all("SingleEndDate").value!="")
		{
			SubTime=fm.all("SingleEndDate").value;
		}	   
		contCurrentTime=CurrentTime;
		contSubTime=SubTime;
	//得到所选产品类型
		if(fm.queryType.value == "")
		{
		  alert("请录入产品类型。");
		  return false;
		}
		
  // DealState==5 全部应收记录
    var strState = fm.all('DealState').value //催收状态 5：全部
		var strConSql = "";
    if (strState=='5'){}else
	  {
	  	 strConSql = " and b.DealState='"+ fm.all('DealState').value + "'";
	  }		
	  var strSQL="";
	  var strSQL11 = "";
	  if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
	    strSQL11 = " and a.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
	  }	  
	   strSQL = "select  a.ContNo,a.AppntName,CodeName('sex',a.appntsex),"
				+	" (select mobile from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno ),	"
				+	" (select (case when phone is null then homephone else phone end) from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno) "
				+	" ,a.CValiDate,nvl((select nvl(AccGetMoney,0) from LCAppAcc where CustomerNo=a.appntno),0),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(c.ManageCom) mana,getUniteCode(a.AgentCode),b.GetNoticeNo,"
				+ " (case when (b.dealstate ='0' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno "
				+ " and riskcode in ('320106','120706')) is not null ) then '等待客户反馈' "
				+ " when (b.dealstate ='1' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno and riskcode in ('320106','120706')) is not null ) then '续保成功' "
				+ " when (b.dealstate ='3' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno  and riskcode in ('320106','120706')	) is not null ) then '满期给付' "
				+ " else (select codename  from ldcode where codetype='dealstate' and code=b.dealstate)  end),"
		    +	" (case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end)"
	      +	",(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen"
	      	      +" , ShowManageName(substr(c.managecom,1,4)),substr(c.managecom,1,4),(select bankname from ldbank where bankcode=a.bankcode),a.bankaccno,a.appntno "
	      + " from LCCont a,ljspayB b,ljspaypersonB c where 1=1 "
			  + " and   b.othernotype='2' and a.ContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo " 
			  + omnipCodition
			  + strConSql
			  + queryCondition
			  + getWherePart( 'a.ContNo','ProposalGrpContNo' )
			  + getWherePart( 'b.AppntNo','AppNo' )
			  + getWherePart( ' c.LastPayToDate','SingleStartDate' ,'>=')
			  + getWherePart( ' c.LastPayToDate','SingleEndDate' ,'<=')
			 // + getWherePart( 'b.AgentCode', 'AgentCode' )
			  + strSQL11
			 
			  + " and c.ManageCom like '"+ managecom + "%%'"
			  + " group by a.ContNo,a.AppntName,a.CValiDate,a.SumPrem,a.Dif,b.SumDuePayMoney,c.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate,a.appntno,b.Cancelreason,a.appntsex,a.bankcode,a.bankaccno"
        + " order by mana,agen,a.AgentCode,b.GetNoticeNo";
	 
		
        //添加strSQL2用来打印批单2006-11-03 by huxl.
        var strSQL2 = "";
			  strSQL2 += " and b.othernotype='2' and a.ContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo" ;
			  strSQL2 += strConSql
			  strSQL2 += queryCondition
			  strSQL2 += getWherePart( 'a.ContNo','ProposalGrpContNo' ) ;
			  strSQL2 += getWherePart( 'b.AppntNo','AppNo' ) ;
			  strSQL2 += " and c.LastPayToDate>='"+CurrentTime+"'and  c.LastPayToDate<='"+SubTime+"' ";
			  strSQL2 += " and a.salechnl in ('04','13') ";// add by lzy 20160408只查询银保渠道的
			  strSQL2 += " and c.ManageCom like '"+ managecom;
	}
	else
	{
		if(fm.all("MultStartDate").value!="")
		{
			CurrentTime=fm.all("MultStartDate").value;
		}
		if(fm.all("MultEndDate").value!="")
		{
			SubTime=fm.all("MultEndDate").value;
		}
			//得到所选产品类型
		if(fm.queryType2.value == "")
		{
		  alert("请录入产品类型。");
		  return false;
		}
		else
		{
			if(fm.queryType2.value=='1')
			{
				alert("请录入产品类型。");
			  return false;
			}
			if(fm.queryType2.value=='2')
			{
				queryCondition = " and c.riskcode not in ('320106','120706') " ;
			}
			if(fm.queryType2.value=='3')
			{
				queryCondition = " and c.riskcode  in ('320106','120706') " ;
			}			
		}	   
		contCurrentTime=CurrentTime;
		contSubTime=SubTime;
		fm.all("MultStartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！		
	  fm.all("MultEndDate").value=contSubTime;
	  var strState = fm.all('DealState1').value //催收状态 5：全部
		var strConSql = "";
    if (strState=='5'){}else
	  {
	  	 strConSql = " and b.DealState='"+ fm.all('DealState1').value + "'";
	  }
	  var strSQL="";
	  strSQL = "select  a.ContNo,a.AppntName,CodeName('sex',a.appntsex),"
				+" (select mobile from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno ),	"
				+" (select (case when phone is null then homephone else phone end) from lcaddress lcad, lcappnt lca  where lcad.customerno = lca.appntno and lca.contno=a.contno and lca.addressno=lcad.addressno) "
	  		+" ,CValiDate,nvl((select nvl(AccGetMoney,0) from LCAppAcc where CustomerNo=a.appntno),0),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(c.ManageCom) mana,getUniteCode(a.AgentCode),b.GetNoticeNo,"
	  		+ " (case when (b.dealstate ='0' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno "
				+ " and riskcode in ('320106','120706')) is not null ) then '等待客户反馈' "
				+ " when (b.dealstate ='1' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno and riskcode in ('320106','120706')) is not null ) then '续保成功' "
				+ " when (b.dealstate ='3' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno  and riskcode in ('320106','120706')	) is not null ) then '满期给付' "
				+" else (select codename  from ldcode where codetype='dealstate' and code=b.dealstate)  end),"
		   	+	" (case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end)"
		   // +"(select codename  from ldcode where codetype='dealstate' and code=b.dealstate),(case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end)"
	      +" ,(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen"
	      + " from LCCont a,ljspayB b,ljspaypersonB c,LCAddress d where 1=1 "
          + " and  not exists (select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.risktype4='4' and lcpol.Contno = a.ContNo) "
			  + " and   b.othernotype='2' and a.ContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo " 
			  +" and a.appntno = d.customerno"
			//  + omnipCodition
			  + strConSql
			  + queryCondition
			  + getWherePart( 'a.ContNo','ProposalGrpContNo' )
			  + getWherePart( 'b.AppntNo','AppNo' )
			  //+ getWherePart( 'b.AgentCode', 'AgentCode' )
			  + strSQL11
			  + getWherePart( 'c.LastPayToDate','MultStartDate' ,'>=')
			  + getWherePart( 'c.LastPayToDate','MultEndDate' ,'<=')
			  + " and exists (select 'y' from LABranchGroup where BranchSeries like '"+fm.AgentGroup.value+"%%' and LABranchGroup.AgentGroup=b.AgentGroup)"
			 // + getWherePart( 'a.ManageCom', 'ManageCom2','like' )
			  +"AND c.ManageCom LIKE '"+fm.ManageCom2.value+"%' "
			  + " and( c.LastPayToDate>='"+CurrentTime+"'and  c.LastPayToDate<='"+SubTime+"') "
			  + " and c.ManageCom like '"+ managecom + "%%'"
			  + " group by a.ContNo,a.AppntName,a.CValiDate,a.SumPrem,a.Dif,b.SumDuePayMoney,c.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate,a.appntno,b.Cancelreason,a.appntsex"
        + " order by mana,agen,a.AgentCode,b.GetNoticeNo";		
	 
        
       //添加strSQL2用来打印批单2006-11-03 by huxl.
		var strSQL2 = "";
			  strSQL2 += " and b.othernotype='2' and a.ContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo" ;
			  strSQL2 += strConSql
			  strSQL2 += queryCondition
			  strSQL2 += getWherePart( 'a.ContNo','ProposalGrpContNo' ) ;
			  strSQL2 += getWherePart( 'b.AppntNo','AppNo' ) ;
			  //strSQL2 += getWherePart( 'b.AgentCode', 'AgentCode' ) ;
			  strSQL2 += strSQL11 ;
//			  strSQL2 += getWherePart( 'b.AgentGroup','AgentGroup') ;
				strSQL2 += " and b.AgentGroup in (select AgentGroup from LABranchGroup where BranchSeries like '"+fm.AgentGroup.value+"%25')" ;//%25 代表%号 25是%的acii码表示（16进制）
			  strSQL2 += " and c.LastPayToDate>='"+CurrentTime+"'and  c.LastPayToDate<='"+SubTime+"' ";
			  strSQL2 += " and a.salechnl in ('04','13') ";// add by lzy 20160408只查询银保渠道的
			  strSQL2 += " and c.ManageCom like '"+ fm.all('ManageCom2').value;
	}
	//限制起至时间为3个月
	var startDate = fm.all('SingleStartDate').value;
	var endDate = fm.all('SingleEndDate').value;
	if(startDate==null||startDate=="")
	{
	  alert("应交开始日期不能为空！")
	  return false;
	}
	if(endDate==null||endDate=="")
	{
	  alert("应交终止日期不能为空！")
	  return false;
	}
	var t1 = new Date(startDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(endDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    var contNo=fm.all('ProposalGrpContNo').value;
	if(contNo==null||trim(contNo)==""||contNo=="null")
	{
	    if(tMinus>92 )
	    {
		  mDateFlag=false;
		  alert("查询起止时间不能超过3个月!");
		  return false;
	    }
	    else
	    {
	      mDateFlag=true;
	    }
	}
	mSql = strSQL2;
    turnPage1.queryModal(strSQL, ContGrid); 
	SqlPDF = strSQL;
	fm.all("SingleStartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("SingleEndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
			if(ContGrid.mulLineCount == 0)
	  {
	    alert("没有符合条件的可催收保单信息");
	  }
	
	
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = ContGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		var cContNo = ContGrid.getRowColData( tSel - 1, 1 );
		try
		{
			//window.open("./GrpPolQuery.jsp");
			window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ cContNo+"&ContType=1");
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}
function getpoldetail()//被选中后显示的集体险种应该是符合条件的。++
{
  var arrReturn = new Array();
	var tSel = ContGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}	
	else
	{
		getPolInfo();
	
  }  	                                                                 	               	
}

//被选中后显示的集体险种应该是符合条件的。
function getPolInfo()
{
  //modify by fuxin 2008-5-5 11:21:17 少儿险险种和普通险种分开显示。
	var Condition ='';
	if(fm.queryType.value=='2')
	{
		Condition = " and a.riskcode not in ('320106','120706') " ;
	}
	if(fm.queryType.value=='3')
	{
		Condition = " and a.riskcode  in ('320106','120706') " ;
	}
	//if(fm.queryType2.value=='2')
	//{
	//	Condition = " and a.riskcode not in ('320106','120706') " ;
	//}
	//if(fm.queryType2.value=='3')
	//{
	//	Condition = " and a.riskcode  in ('320106','120706') " ;
	//}
	
	  var tRow = ContGrid.getSelNo() - 1;	        
		var tContNo=ContGrid.getRowColData(tRow,1);  
		fm.all('ProposalGrpContNo').value = tContNo;
		var strSQL = "select a.RiskSeqNo ,a.InsuredName ,(select riskname  from lmrisk where riskcode=a.riskcode) , a.riskcode , "
		+ "	 (select codeName from LDCode where codeType = 'payintv' and code = char(a.PayIntv)), sum(a.prem) , a.CValiDate ,a.PaytoDate,a.PayEndDate, a.EndDate "  
		+ "  from lcpol a "
		+ "  where a.contno='" + tContNo + "' "
		//+ "  and a.PayIntv>0 "
		//+ " and a.PaytoDate<a.PayEndDate "
		//+ "  and a.PolNo not in (select PolNo from LJSPayPerson)"
		+ "  and a.RiskCode  in (select RiskCode from LMRisk where CPayFlag='Y' or RnewFlag != 'N')"
		+ Condition
		+ "  group by a.RiskSeqNo,a.InsuredName,a.riskcode,a.PayIntv,a.CValiDate,a.PaytoDate,a.EndDate,a.PayEndDate " 
		;
    turnPage2.queryModal(strSQL, PolGrid);
		var tGetNoticeNo=ContGrid.getRowColDataByName(tRow,'GetNoticeNo');
		var sql = "select distinct dealstate from ljspaypersonb where GetNoticeNo ='"+tGetNoticeNo+"' and riskcode ='320106'";
		var childInsur = easyExecSql(sql);
		if(childInsur=='0'||childInsur=='1'||childInsur=='3')
		{
			fm.GetNoticeNo.value = tGetNoticeNo;
			divChildInsureOper.style.display="";
			fm.CustomerBackDate.verify = "客户答复日期|date&notnull";
			if(childInsur== '0')
			{
					document.getElementById("dealButton").style.display='';
					document.getElementById("XBDealButton").style.display='none';
					document.getElementById("MJDealButton").style.display='none';
					document.getElementById("divButton1").style.display='';
					document.getElementById("divButton2").style.display='';
			}
			if(childInsur== '1')
			{
					document.getElementById("dealButton").style.display='none';
					document.getElementById("XBDealButton").style.display='';
					document.getElementById("MJDealButton").style.display='none';
					document.getElementById("divButton1").style.display='none';
					document.getElementById("divButton2").style.display='none';
			}
			if(childInsur== '3')
			{
					document.getElementById("dealButton").style.display='none';
					document.getElementById("XBDealButton").style.display='none';
					document.getElementById("MJDealButton").style.display='';
					document.getElementById("divButton1").style.display='none';
					document.getElementById("divButton2").style.display='none';
			}
			
		}else
		{
			divChildInsureOper.style.display="none";
			fm.CustomerBackDate.verify = "";
		}
}


//打印应收记录清单
function printList()
{
  //    if(!chenkDate()){
  //      return true ;
  //  }
	if( ContGrid.mulLineCount == 0)
	{
		alert("没有应收个人保单");
		return false;
	}
	//个案查询应收明细
	window.open("../operfee/IndiDueFeeListPrint.jsp?strsql="+mSql);
}

//催收完成后打印通知书
function printNotice()
{
	var tRow = ContGrid.getSelNo() ;	
	if (tRow == 0 || tRow == null)
	{
		alert("请选择一个保单");
	}else{
		 var tGetNoticeNo=ContGrid.getRowColData(tRow - 1,10);
		 window.open("../operfee/IndiPrintSel.jsp?GetNoticeNo="+ tGetNoticeNo);
	}

}

//打印PDF前往打印管理表插入一条数据
function printInsManage()
{
	if (ContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < ContGrid.mulLineCount; i++)
	{
		if(ContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	var tGetNoticeNo=ContGrid.getRowColData(tChked[0],13);
	/*
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '93' and standbyflag2='"+tGetNoticeNo+"'");

	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
	*/
		fm.all('GetNoticeNo').value = tGetNoticeNo;
		fm.action="./IndiDueFeeInsForPrt_v2.jsp";
		fm.submit();
	/*
	}
	else
	{
		//window.open("../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq );	
		fm.action = "../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
		fm.submit();
	}
	*/
}

function printPDF()
{
	if (ContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}   
	
	var count = 0;
	var tChked = new Array();

	for(var i = 0; i < ContGrid.mulLineCount; i++)
	{
		if(ContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	var tGetNoticeNo=ContGrid.getRowColData(tChked[0],13);  
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '93' and standbyflag2='"+tGetNoticeNo+"'");
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		alert("提取数据失败-->PrtSeq为空");
		return false;
	}
	//window.open("../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq );	
	fm.action="../uw/PrintPDFSave.jsp?Code=093&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
	fm.submit();
}


function printInsManageBat()
{

	if (ContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < ContGrid.mulLineCount; i++)
	{	
		if(ContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}


	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.strsql.value = SqlPDF;
//		fm.action="./IndiDueFeeInsAllBatPrt.jsp?strsql="+SqlPDF;
		fm.action="./IndiDueFeeInsAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.action="./IndiDueFeeInsForBatPrt.jsp";
		fm.submit();
	}
}

function afterInsForBatPrt()
{
	fm.action="./IndiDueFeeBatchPrtSave.jsp";
	fm.submit();
}
//新的接口
function printPDFNotice(aCode,aGetNoticeNo,aPrtSeq)
{
		if(aCode=="093")
		{
			fm.action="../uw/PrintPDFSave.jsp?Code="+aCode+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+aGetNoticeNo+"&PrtSeq="+aPrtSeq;
			fm.submit();
		}else
		{
		    fm.action="../uw/PrintPDFSave.jsp?Code="+aCode+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+aGetNoticeNo+"&PrtSeq="+aPrtSeq;
			fm.submit();
			//window.open("../operfee/PrintChildNotice.jsp?GetNoticeNo="+aGetNoticeNo);
		}
}

//按险种打印应收记录清单
function printPolList()
{
	if( ContGrid.mulLineCount == 0)
	{
		alert("没有应收个人保单");
		return false;
	}
    if(!mDateFlag)
    {
	  alert("查询起止时间不能超过3个月,请修改时间重新查询后再打印！")
	  return false;
    }
	//个案查询应收明细
	window.open("../operfee/IndiDueFeePolListPrint.jsp?strsql="+mSql);
}
// 客户选择操作验证
function afterCodeSelect(cName, Filed)
{
	if(cName == "dealType" && Filed.value == '2')
	{
		 document.getElementById('Paymode').style.display=""; 
	} else
		document.getElementById('Paymode').style.display="none";
}
// 给付方式变更
function ChangePaymode()
{ 
  var chkNum = 0;
  var tContNo = "";
  for (i = 0; i < ContGrid.mulLineCount ; i++)
	{
		if (ContGrid.getChkNo(i)) 
		{
		  chkNum = chkNum + 1;
		  if (chkNum == 1)
			{
				tContNo = ContGrid.getRowColDataByName(i, "ContNo");
			}
		}
	}
	if(chkNum == 0)
	{
	  alert("请选择需要变更的记录");
	  return false;
	}
	if(chkNum != 1)
	{
	  alert("只能选择一条记录进行变更");
	  return false;
	}
	
	fm.all("PayModeChangeID").style.display = "";
	fm.PayModeNew.verify="给付方式|notnull";
  
  var rs = getPayModeInfo(tContNo);
  if(rs)
  {
    fm.PayModeOld.value = rs[0][0];
    fm.PayModeOldName.value = rs[0][1];
    fm.BankCodeOld.value = rs[0][2];
    fm.BankNameOld.value = rs[0][3];
    fm.AccNameOld.value = rs[0][4];
    fm.BankAccNoOld.value = rs[0][5];
  }
}

// 给付信息查询：
function getPayModeInfo(tContNo)
{
  var sql = "select PayMode, CodeName('paymode', a.PayMode), "
          + "   BankCode, (select BankName from LDBank where BankCode = a.BankCode), "
          + "   AccName, BankAccNo "
          + "from LCCont a "
          + "where ContNo = '" + tContNo + "' ";
  var rs = easyExecSql(sql);
  return rs;
}

//变更给付方式确认
function changePayModeSubmit()
{
  if(fm.BankAccNo.value != fm.BankAccNo2.value)
  {
    alert("您两次录入的账号不同，请核对");
    return false;
  }
  
  if(fm.PayModeOld.value == fm.PayModeNew.value
    && fm.BankCodeOld.value == fm.BankCode.value
    && fm.AccNameOld.value == fm.AccName.value
    && fm.BankAccNoOld.value == fm.BankAccNo.value)
  {
    alert("你录入的新给付方式与原给付方式一致，不需要变更");
    return false;
  }
  
  if(fm.PayModeNew.value == "")
  {
    alert("请选择给付方式");
    return false;
  }
  else if(fm.PayModeNew.value == "3" || fm.PayModeNew.value == "4")
  {
    if(fm.BankCode.value == "" || fm.AccName.value == "" || fm.BankAccNo.value == "")
    {
      alert("银行账户信息必须录入完整");
      return false;
    }
  }
  else if(fm.PayModeNew.value == "1" || fm.PayModeNew.value == "2")
  {
    if(fm.BankCode.value != "" || fm.AccName.value != "" || fm.BankAccNo.value != "")
    {
      alert("您所选给付方式不需要录入帐户信息，请清空");
      return false;
    }
  }
 	//获得本次变更的保单号。
  var tContNo = "";
  var chkNum ="" ;
  for (i = 0; i < ContGrid.mulLineCount ; i++)
	{
		if (ContGrid.getChkNo(i)) 
		{
		  chkNum = chkNum + 1;
		  if (chkNum == 1)
			{
				tContNo = ContGrid.getRowColDataByName(i, "ContNo");
			}
		}
	}
  var rs = getPayModeInfo(tContNo);
  if(rs[0][0] == fm.PayModeNew.value && rs[0][2] == fm.BankCode.value
    && rs[0][4] == fm.AccName.value && rs[0][5] == fm.BankAccNo.value)
  {
    alert("您录入的信息已进行了变更");
    return false;
  }
  
  fm.action = "indiDueFeeChangePayModeSave.jsp";
  fm.submit();
  return true;
}
function newprintInsManage()
{   
	if (ContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < ContGrid.mulLineCount; i++)
	{
		if(ContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	var tGetNoticeNo=ContGrid.getRowColData(tChked[0],13);
	var sql = "select distinct 'XB001' from ljspaypersonb where getnoticeno = '"
						+tGetNoticeNo+"' and contno = '"
						+ContGrid.getRowColData(tChked[0],1)+"' and riskcode = '320106'"
						;
	var result = easyExecSql(sql);	
	if(result =='XB001'){
	    fm.action = "../uw/PDFPrintSave.jsp?Code=XB001&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+tGetNoticeNo;
	}else{
	    fm.action = "../uw/PDFPrintSave.jsp?Code=93&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+tGetNoticeNo;
	}
	fm.submit();
		
}
////PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	//showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}
function newprintInsManageBat()
{

	if (ContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < ContGrid.mulLineCount; i++)
	{	
		if(ContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}


	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.strsql.value = SqlPDF;
		//fm.action="./IndiDueFeeInsAllBatPrt.jsp?strsql="+SqlPDF;
		fm.action="../operfee/IndiDueFeeInsAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.action="../operfee/IndiDueFeeInsForBatPrt.jsp";
		fm.submit();
	}
}