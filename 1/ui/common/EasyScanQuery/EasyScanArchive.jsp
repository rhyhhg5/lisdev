<LINK href="../css/Project.css" rel=stylesheet type=text/css>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：EasyScanArchive.jsp
//程序功能：扫描件归档
//创建日期：2007-05-15 15:06:57
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<SCRIPT src="../javascript/Common.js" ></SCRIPT>
<SCRIPT src="../easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<%@include file="./EasyScanQueryKernel.jsp"%>

<SCRIPT src="./ShowPicControl.js"></SCRIPT>
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="./DebugAutoMove.js"></SCRIPT>
</head>
<%

  String FlagStr = "Fail";
	String Content = "";
  //存储图片信息
  String[] arrPic;
  String prtNo = request.getParameter("prtNo");
  String clientUrl = (String)session.getValue("ClientURL");//LQ 2004-04-20
   
  //查询多种单证的标志
  String multi = request.getParameter("MULTI");
  String easyway=request.getParameter("EASYWAY");
  System.out.println("---easyway is "+easyway);
  //只通过main表查询
	if(multi == null || multi.equals(""))
  {
      if(StrTool.cTrim(easyway).equals("1"))
  				{
                 System.out.println("---in main---");
                 String Docid =request.getParameter("DocID");
                 System.out.println("Docid is "+Docid);
                 String DocCode=request.getParameter("DocCode");
                 String BussType = request.getParameter("BussType");
                 String SubType = request.getParameter("SubType");
                 System.out.println("ClientUrl=" + clientUrl);			 //DJB 2005-12-30
                 arrPic = easyScanQueryKernelEasyway(Docid,DocCode,BussType,SubType,clientUrl);
                 System.out.println("---EasyScanQuery easyway End---");
          }
      else{
                System.out.println("---in relation---");
                String BussNoType = request.getParameter("BussNoType");
                String BussType = request.getParameter("BussType");
                String SubType = request.getParameter("SubType");
                System.out.println("ClientUrl=" + clientUrl);			 //LQ 2004-04-20
                arrPic = easyScanQueryKernel(prtNo,BussNoType,BussType,SubType,clientUrl);
                System.out.println("---EasyScanQuery End---");
          }
  }
  //查询多种单证
  else
  {
      String[] BussNoType = (String[]) session.getAttribute("BussNoType");
      String[] BussType = (String[]) session.getAttribute("BussType");
      String[] SubType = (String[]) session.getAttribute("SubType");
      String Order = request.getParameter("Order");
      
      session.removeAttribute("BussNoType");
      session.removeAttribute("BussType");
      session.removeAttribute("SubType");
      
      System.out.println("easyScanQueryKernelMuli start...");
      arrPic = easyScanQueryKernelMuli(prtNo,BussNoType,BussType,SubType,clientUrl,Order);
      System.out.println("easyScanQueryKernelMuli end...");
  }
  if (arrPic != null) {
  	 	TransferData data = new TransferData();
  		data.setNameAndValue("arrPic", arrPic);
  		data.setNameAndValue("prtNo", prtNo);
  		String SubType = request.getParameter("SubType");
  		data.setNameAndValue("SubType", SubType);

		  VData tVData = new VData();
  		tVData.add(data);
  		
  		EasyScanArchiveBL tEasyScanArchiveBL = new EasyScanArchiveBL();
  		if(!tEasyScanArchiveBL.submitData(tVData, ""))
  		{
    		System.out.println(tEasyScanArchiveBL.mErrors.getErrContent());  
    		//Content = tEasyScanArchiveBL.mErrors;
    	}else{
    		FlagStr = "Success";
    		Content = "扫描件归档成功!";
    	}
	}else{
  	Content = "扫描件不存在,请检查是否已经扫描入系统!";
	}
%>
</html>
<script>
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>