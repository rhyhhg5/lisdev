/**
 * 显示/隐藏Html中的控件。
 * 对于Text控件，同时清空Value值。
 * ojbName: 控件名称。
 * isDisplay: true-显示;false-隐藏
 */
function displayInput(ojbName, isDisplay, isClearValue)
{
    if(ojbName == null || ojbName == "undefined")
    {
        alert("ojbName对象不存在。");
        return ;
    }
    
    if(isDisplay == true)
        fm.all(ojbName).style.display = "";
    else
    {
        fm.all(ojbName).style.display = "none";
        if(isClearValue == true && fm.all(ojbName).type == "text")
        {
            fm.all(ojbName).value = "";
        }
    }
}

/**
 * 把传入字符串转换成 Date 类型数据。
 * 如转换失败，返回 null
 */
function toDate(str)
{
    var pattern = /^(\d{4})(-)(0?[1-9]|1[0-2])(-)(0?[1-9]|[12][0-9]|3[01])$/g; 
    var arr = pattern.exec(str);
    if (arr == null)
        return null;
    var date = new Date(arr[1], arr[3]-1, arr[5]);
    return date;
}

