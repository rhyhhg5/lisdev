<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>
<%@page import="java.text.SimpleDateFormat"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>My JSP 'currentTime.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
<%
        String pattern = "HH:mm:ss";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date today = new Date();
        String tString = df.format(today) ;
 %>
   <script>
   var time = "<%=tString%>";
   //alert(time);
   /**
   * 计算两个日期的差,返回差的月数(M)
 */
function dateInterval(dateStart,dateEnd)
{
  if(dateStart==""||dateEnd=="")
  {
  	return false;
  }
  if (typeof(dateStart) == "string") {
    dateStart = getDate(dateStart);
  }

  if (typeof(dateEnd) == "string") {
    dateEnd = getDate(dateEnd);
  }

  var i;
    var endD = dateEnd.getDate();
    var endM = dateEnd.getMonth();
    var endY = dateEnd.getFullYear();
    var startD = dateStart.getDate();
    var startM = dateStart.getMonth();
    var startY = dateStart.getFullYear();

    if(endD>=startD)
    {
      return (endY-startY)*12 + (endM-startM) + 1;
    }
    else
    {
      return (endY-startY)*12 + (endM-startM);
    }
}

/**
 *判断是否是在工作时间打印三个月以上的报表
*/
//function chenkDate(startDate,endDate){
function chenkDate(){
    var startDate = fm.all('StartDate').value ;
    var endDate = fm.all('EndDate').value ;
    if(startDate==""||endDate=="")
    {
  	   return false;
    }
    //alert(startDate);
	var interval = dateInterval(startDate,endDate) ;
	//alert(interval);
	isWorkTime();
	if(interval >= 4){
		if(isWorkTime()){
			alert("对不起，现在是工作时间您不能打印三个月以上的报表");
			return false ;
		}
		return true ;
	}
	return true ;
}
/**
 *判断是否是工作时间
*/
function isWorkTime(){
	var hour = time.substring(0,2);
	//alert(hour);
	if((hour >= 9) && (hour <= 17)){
		return true ;
	}
	return false ;
	
} 
  </script>
  </head>
  <body> 
  </body>
</html>
