/*****************************************************************
 *               Program NAME: 选取代码并显示                       
 *                 programmer: 
 *                Create DATE: 
 *             Create address: Beijing                       
 *                Modify DATE:                               
 *             Modify address:                               
 *****************************************************************
 *                                                
 *         通用代码查询处理页面,包含在隐藏的框架中,输入
 *     过程中要显示代码清单时调用此页面
 * 
 *****************************************************************
 */
//得到存放编码数据的内存引用,要求CVarData.js必须要在一个名称为VD的帧中
//存放的是从数据库的表中取出的编码的数组，即内部数据的存放
mVs=parent.VD.gVCode;                         

//得到存放编码数据的内存引用,要求CVarData.js必须要在一个名称为VD的帧中
//存放的是页面上的表名，位置，从数据库表中取出代码的排序等一些外部操作数据
mCs=parent.VD.gVSwitch;                       

var _Code_FIELDDELIMITER    = "|";            //域之间的分隔符
var _Code_RECORDDELIMITER   = "^";            //记录之间的分隔符

var arrayBmCode;       //存放最近代码的代码

var showFirstIndex=0;
/*************************************************************
 *                      显示编码（该函数为显示编码的入口函数
 *  参数  ：  Field 触发事件的控件;
 *            strCodeName 编码名称;
 *            arrShowCodeObj 将编码显示到对应的控件上
 *            arrShowCodeOrder 编码显示对应控件和编码顺序的对应关系
 *            arrShowCodeFrame 编码显示对应的Frame指针
 *  返回值：  如果没有返回false,否则返回true
 *************************************************************
 */
function showCodeList(strCodeName,arrShowCodeObj,arrShowCodeOrder,objShowCodeFrame)
{
alert(arrShowCodeObj);		
//alert(arrShowCodeObj[0].tagName);
  	var ex,ey;
    var tCode;
//Field  是一个数组集合，表示要显示编码的在页面上的对象    
    var Field;
//其实这个判断是没有用处的，可能理解上会有迷惑作用
//因为arrShowCodeObj参数如果不输入，那么js会自动赋值为undefined而不是null
//因此如果真的想检验,那么加入 	if (arrShowCodeObj != undefined)
//但是，下面这样在这里实际是可行的，至少是没有错误   
	if (arrShowCodeObj != null){
//  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
//  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
    Field=arrShowCodeObj[0];
    setFieldPos(Field);
    
    if (arrShowCodeOrder == null)
    {
      arrShowCodeOrder = [0];
    }
    if (objShowCodeFrame == null)
    {
      objShowCodeFrame = parent.fraInterface;//window.self;
    }
	  //将一些参数放到客户端代码区
	  mCs.updateVar("ShowCodeObj","0",arrShowCodeObj);
	  mCs.updateVar("ShowCodeOrder","0",arrShowCodeOrder);
//    mCs.updateVar("ShowCodeX","0",ex);
//    mCs.updateVar("ShowCodeY","0",ey);
    mCs.updateVar("ShowCodeFrame","0",objShowCodeFrame);  //Frame的指针
	}
  
	tCode=searchCode(strCodeName);                         //从代码区读取代码
//初始页面时，代码区是没有代码的，因此需要向服务器提交请求，
	if (tCode == false && arrShowCodeObj != null){
		requestServer(strCodeName);                          //请求服务器端，读取数据
		return false;
//该js终止，转到向服务器发出请求的页面执行，该页面查询数据后保存到页面上的内存区，
//并再次调用该js,（先是initializeCode，然后showCodeList,请注意initializeCode不可少的作用）
//继续执行js，不过这次代码区已经有了代码，不会进入这个控制区
	}

//将代码区得到的数组和编码名称传入，显示代码
	showCodeList1(tCode,strCodeName);

//如果要显示编码的页面对象存在	
//显示编码表，根据页面对象中的值，使编码表定位到对应Field中的值的那一项
	if(Field!=null)
	{  goToSelect(strCodeName,Field);}
	
	return true;
}

/*************************************************************
 *                      查找代码(键盘按键事件)
 *  参数  ：  Field 需要显示代码的控件;
 *            strCodeName 代码类型(Lx_Code);
 *            intFunctionIndex 需要赋值控件的顺序 1-前一个和本身
 *                                                2-本身和后一个;
 *            stationCode 代码所属区站.
 *  返回值：  string  ：code 或 null
 *************************************************************
 */
function showCodeListKey(strCodeName,arrShowCodeObj,arrShowCodeOrder,objShowCodeFrame)
{
    
  	var ex,ey,i,intElementIndex;
  	var Field;
  	Field=arrShowCodeObj[0];
  	eobj = window.event;
    key  = eobj.keyCode;
    if (  document.all("spanCode").style.display=="" && key == 13)
    {
       document.all("codeselect").focus();
       document.all("codeselect").onclick();
       Field.focus();
    }
    if ( key == 40 && document.all("spanCode").style.display=="")
    {
      Field.onblur=null;
      document.all("codeselect").focus();
      document.all("codeselect").children[showFirstIndex].selected=true;
      //showFirstIndex += showFirstIndex;
    }    
    else
    {
      Field.onblur=closeCodeList;    
    }
    
    if (  document.all("spanCode").style.display=="none" && (key >= 48 || key==8 || key==46 || key==40 ))
    {
      setFieldPos(Field);
      //提交代码名称及信息
      if (arrShowCodeOrder == null)
      {
        arrShowCodeOrder = [0];
      }
      if (objShowCodeFrame == null)
      {
        objShowCodeFrame = parent.fraInterface;//window.self;
      }
      setFieldPos(Field);
  	  mCs.updateVar("ShowCodeObj","0",arrShowCodeObj);
  	  mCs.updateVar("ShowCodeOrder","0",arrShowCodeOrder);
      mCs.updateVar("ShowCodeFrame","0",objShowCodeFrame);  //Frame的指针
      //提交代码名称及信息
      getCode(strCodeName,arrShowCodeObj);
      goToSelect(strCodeName,Field);
    }
    else if ( document.all("spanCode").style.display=="" && (key >= 48 || key==8 || key==46))
    {
      if ( Field.value != null)   
      {
        goToSelect(strCodeName,Field);
      }
    }
}

/*************************************************************
 *                     初始化编码
 *  参数  ：  strCodeName：编码名称
 *  返回值：  boolean   true：找到所需的代码   false：未找到
 *************************************************************
 */
function initializeCode(strCodeName)
{
	var i,i1,j,j1;
  var strValue;                         //存放服务器端返回的代码数据
  var arrField;
  var arrRecord;
  var arrCode = new Array();             //存放初始化变量时用
  var t_Str;

  clearShowCodeError();

  strValue  = getCodeValue(strCodeName);              //得到服务器端读取的数据

  arrRecord = strValue.split(_Code_RECORDDELIMITER);  //拆分字符串，形成返回的数组

  t_Str     = getStr(arrRecord[0],1,_Code_FIELDDELIMITER);

  if (t_Str!="0")                                     //如果不为0表示服务器端执行发生错误
  {
    mCs.updateVar("ShowCodeError",getStr(arrRecord[0],2,_Code_FIELDDELIMITER));   //将错误保存到该变量中
    mCs.updateVar("ShowCodeErrorCode",t_Str);   //将错误保存到该变量中
    return false;   
  }

  i1=arrRecord.length;
  for (i=1;i<i1;i++)
  {
    arrField  = arrRecord[i].split(_Code_FIELDDELIMITER); //拆分字符串,将每个纪录拆分为一个数组
    j1=arrField.length;
    arrCode[i-1] = new Array();
    for (j=0;j<j1;j++)
    {
      arrCode[i-1][j] = arrField[j];
    }
  }
  mVs.addVar(strCodeName,"",arrCode);                 //无论是否有数据从服务器端得到,都设置该变量
  return true; 
}



/*************************************************************
 *                     得到编码值串
 *  参数  ：  strCodeName：编码名称
 *  返回值：  string     ：编码值串
 *************************************************************
 */
function getCodeValue(strCodeName) 
{
  var reStr;
  //try
  //    {
      reStr= parent.EX.fm.all("txtVarData").value;     //从父frame中取得从服务器端取得的值
    //}
    //catch(ex)
    //{}
    return reStr;                                       
}
  
/*************************************************************
 *                     请求服务器
 *  参数  ：  intElementIndex 需要显示编码的控件的索引号;
 *            strCodeName 编码类型(Lx_Code);
 *            intFunctionIndex 需要赋值控件的顺序 1-前一个和本身
 *                                                2-本身和后一个;
 *            stationCode 编码所属区站；
 *            ex,ey 显示的位置.
 *  返回值：  无
 *************************************************************
 */
function requestServer(strCodeName)
{
  var objFrame;
  objFrame=mCs.getVar("ShowCodeFrame");
  	//请求服务器
//  	try                 
//  	{
	  	parent.EX.fm.txtCodeName.value   = strCodeName;          //编码名称
	  	parent.EX.fm.txtFrameName.value  = objFrame.name;        //Frame的名字  
	  	parent.EX.fm.txtVarData.value    = "";                   //返回时需要的空间
	  	parent.EX.fm.txtOther.value      = "";     //提交时的其他数据
	  	parent.EX.fm.submit();
//	}
//	catch(exception){}
}

/*************************************************************
 *                 下拉框的方式显示编码列表
 *  参数  ：  arrCode     包含该类型编码的所有编码信息的数组;
 *            strCodeName 需要显示的代码名称
 *  返回值：  无
 *************************************************************
 */
function showCodeList1(arrCode,strCodeName) 
{
  	var strValue;
  	var flag=false;
  	var strText;
    var arrCount;
	  var fm;
	  
    arrCount=arrCode.length;

	  fm=mCs.getVar("ShowCodeFrame");

	
//  	strText="<select name=codeselect style='width:350px' size=8  onchange=setFieldValue(this,'"+strCodeName+"')>";
  	strText="<select name=codeselect style='width:250px' size=8  onkeyup=\"if (window.event.keyCode==13){setFieldValue(this,'"+strCodeName+"');}\"" +
  	        "onclick=\"setFieldValue(this,'"+strCodeName+"')\""+
  	        "onblur=\"return closeCodeList();\">";

  	for(i=0;i<arrCount;i++)
  	{  
    	flag=true;               
      strText=strText+"<option value="+arrCode[i][0]+">";
      strText=strText+arrCode[i][0]+"-"+arrCode[i][1];
      strText=strText+"</option>";
  	}
  	strText=strText+"</select>"

  	if(flag)
  	{
    	document.all("spanCode").innerHTML =strText;
    	document.all("spanCode").style.left=mCs.getVar("ShowCodeX");    //读取公共变量区的坐标X
    	document.all("spanCode").style.top=mCs.getVar("ShowCodeY");     //读取公共变量区的坐标Y
    	document.all("spanCode").style.display ='';
  	}
  	else
  	{
    	document.all("spanCode").style.display ='none';
  	}
}

/*************************************************************
 *                     为控件赋值
 *  参数  ：  Field 需要赋值的控件
 *  返回值：  无
 *************************************************************
 */ 
function setFieldValue(Field,strCodeName)
{  
  var tFldCode;               //为一个代码表的纪录，如001,总公司,总公司信息
  var tArrDisplayObj;         //需要显示到的对象
  var tArrDisplayOrder;       //显示的顺序
  var i,iMax;
  
  tFldCode = getOneCode(strCodeName,Field.value);   //得到一个代码纪录
  tArrDisplayObj   = mCs.getVar("ShowCodeObj");     //得到需要显示的对象
  tArrDisplayOrder = mCs.getVar("ShowCodeOrder");   //得到显示时对应的顺序
  iMax = tArrDisplayObj.length;
  try
  {
    for (i=0;i<iMax;i++)
    {
      tArrDisplayObj[i].value = tFldCode[tArrDisplayOrder[i]];  //根据显示顺序设置显示对象
    }
  }
  catch(exception){}
	document.all("spanCode").style.display ='none'; 
}
 
/*************************************************************
 *              从Code内存中读取该Code的信息
 *  参数  ：  strCodeName:Code的类型(名称)
 *            strCode    :Code的编码
 *            index 控件的索引号
 *  返回值：  无
 *************************************************************
 */
function getOneCode(strCodeName,strCode)
{
  var tArrCode;
  var i,iMax;
  var tArrReturn;
  tArrCode = mVs.getVar(strCodeName);
  iMax     = tArrCode.length;
  
  for (i=0 ; i<iMax;i++)
  {
    if (tArrCode[i][0]==strCode)
    {
      tArrReturn = tArrCode[i];
      break;
    }
  }
  return tArrReturn;
}


/*************************************************************
 *                      查找编码
 *  参数  ：  strValue：编码名称
 *  返回值：  string  ：code 或 false
 *************************************************************
 */
function searchCode(strValue)
{
  return mVs.getVar(strValue);      //取得编码，如果没有找到，返回-1
}

/*************************************************************
 *                     清空错误信息
 *  参数  ：  没有
 *  返回值：  没有
 *************************************************************
 */
function clearShowCodeError()
{
  mCs.updateVar("ShowCodeError","","");       //清空错误信息
  mCs.updateVar("ShowCodeErrorCode","","");   //清空错误信息
}

/*************************************************************
 *                     增加一个锁
 *  参数  ：  没有
 *  返回值：  没有
 *************************************************************
 */
function addLock(strLockName)
{
  return mCs.addVar(strLockName,"1","Locked");
}

/*************************************************************
 *                     增加一个锁
 *  参数  ：  没有
 *  返回值：  没有
 *************************************************************
 */
function deleteLock(strLockName)
{
  return mCs.deleteVar(strLockName);
}

/*************************************************************
 *                     设置坐标位置
 *  参数  ：  需要参照的对象
 *  返回值：  设置ex,ey的值
 *************************************************************
 */
function setFieldPos(Field)
{
    var ex,ey,i,intElementIndex;
    try
  	{
  	     var posLeft, posTop;
         var oParent;
      
         oParent = Field;
    
         posLeft = 0;
         posTop = oParent.offsetHeight;
  	     do
         { 
          	if ( oParent.tagName.toLowerCase( ) != "tr" && oParent.tagName.toLowerCase( ) != "form" && oParent.tagName.toLowerCase( ) != "span" && oParent.tagName.toLowerCase( ) != "div")
          	{
          		  posLeft += parseInt(oParent.offsetLeft);
          		  posTop  += parseInt(oParent.offsetTop);
          	}          
         	oParent = oParent.parentElement;         	
         } 
         while( oParent.tagName.toLowerCase( ) != "body" );
        
         //alert("document.body.scrollTop" + fraTitle.documnet.body.offsetWidth);
  	    
         ex = posLeft;
         ey = posTop - 5;
      mCs.updateVar("ShowCodeX","0",ex);
      mCs.updateVar("ShowCodeY","0",ey);
    }
    catch(ex)
    {
      alert(ex);
    }
}

function getCode(strCodeName,arrShowCodeObj)
{
  var tCode;
	tCode=searchCode(strCodeName);                         //从代码区读取代码
	if (tCode == false && arrShowCodeObj != null){
		requestServer(strCodeName);                          //请求服务器端，读取数据
		return false;
	}

	showCodeList1(tCode,strCodeName);
}


/*************************************************************
 *                     为控件赋值
 *  参数  ： 无
 *  返回值： 无
 *************************************************************
 */ 
function closeCodeList()
{
  try
  {
    showFirstIndex=0;
    arrayBmCode=null;
    document.all("spanCode").style.display ='none';
  }
  catch(ex)
  {}
}

/*************************************************************
 *                     定位到指定的代码位置
 *  参数  ： 无
 *  返回值： 无
 *  在控件对象上显示编码的时候，根据控件对象中的值定位到编码表的项
 *************************************************************
 */ 
function goToSelect(strCodeName,Field)
{
      	i=0;
        //找到对应代码名称的编码数组     	  
      	arrayBmCode=searchCode(strCodeName);
      	if (arrayBmCode!=null)
      	{
        	for(i=0;i<arrayBmCode.length;i++)
    	    { 
    	      var t_len = trim(Field.value).length;            	
    //如果控件对象中的值等于编码数组中的某一项   		  
    		  if( arrayBmCode[i][0].substring(0,t_len) == trim(Field.value))                         //若是代码值已存在于数组中
    		  {      		
    			showFirstIndex = i;   
    //那么在页面上显示该编码表的时候，定位到那一项 			            
    			document.all("codeselect").children[showFirstIndex].selected=true;
    			return;
    			
    		  }  	
    	    }
  	    }  	    
}