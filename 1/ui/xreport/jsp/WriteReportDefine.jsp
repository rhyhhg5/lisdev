<%@page language="java" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="com.f1j.swing.JBook"%>
<%@page import="com.sinosoft.xreport.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<body>
<%/////////////////////上传报表定义格式文件//////////////////////////
%>

<%!
    void writeFile(String fileName, java.io.InputStream input) throws java.io.IOException
    {
        java.io.FileOutputStream output = new java.io.FileOutputStream(fileName);
        try
        {
            byte buffer[] = new byte[4096];
            int bytesRead;
            while (true)
            {
                bytesRead = input.read(buffer);
                if (bytesRead < 0)
                    break;
                if (bytesRead > 0)
                    output.write(buffer, 0, bytesRead);
            }
        }
        finally
        {
            if (output != null)
                output.close();
        }
    }
%>
<%
    String strFileName;
    Hashtable queryParameters = javax.servlet.http.HttpUtils.parseQueryString(request.getQueryString());
    String fileParameters[] = (String[])queryParameters.get("file");
    StringTokenizer strFile=new StringTokenizer(fileParameters[0],"_");
    String strBranchId=(String)(strFile.nextElement());
    strFileName=SysConfig.FILEPATH+
                "define"+SysConfig.FILESEPARATOR+
                strBranchId+SysConfig.FILESEPARATOR+
                fileParameters[0];
    try
    {
        File file=new File(strFileName);
        if(!file.getParentFile().exists())
        {
            file.getParentFile().mkdirs();
        }
        writeFile(strFileName,request.getInputStream());
    }
    catch (IOException e)
    {
        e.printStackTrace();
    }
    catch(Exception e)
    {
        e.printStackTrace();
    }
%>
</body>
</html>