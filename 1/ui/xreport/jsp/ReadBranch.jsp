<%@page language="java" %><%@
page import="java.util.*" %><%@
page import="java.io.*" %><%@
page import="java.net.*" %><%@
page import="com.f1j.swing.JBook"%><%@
page import="com.sinosoft.xreport.util.*"%><%@
page import="com.sinosoft.xreport.bl.*"%><%@
page import="com.sinosoft.lis.pubfun.*"%><%
/*****************************************************
 * 下载报表结果文件.
 * @todo:自动更名
 * change log:
 * name       date     content
 * -----------------------------------------------------
 * AppleWood  03/05/16 增加错误处理,找不到文件时写出错信息
 *
 *****************************************************/
  try
  {
   
    System.out.println("start down load");
//  String temp=application.getRealPath("xreport/temp/report.xls");
//  File tempFile=new File(temp);
//  File parentFile=tempFile.getParentFile();
//  if(!parentFile.exists())
//  {
//    parentFile.mkdirs();
//  }
//  OutputStream os=new FileOutputStream(tempFile);
//  BufferedOutputStream bos=new BufferedOutputStream(os);

/**读取参数*/
    Hashtable queryParameters = javax.servlet.http.HttpUtils.parseQueryString(request.getQueryString());
    String defineParameters[] = (String[])queryParameters.get("define");
    String define=defineParameters[0];
    String calculateParameters[] = (String[])queryParameters.get("calculate");
    String calculate=calculateParameters[0];
    String fileParameters[] = (String[])queryParameters.get("file");
    String file=fileParameters[0];
    String editionParameters[] = (String[])queryParameters.get("edition");
    String edition=editionParameters[0];

    String monthParameters[] = (String[])queryParameters.get("month");
    String month = monthParameters[0];

    String starttimeParameters[] = (String[])queryParameters.get("starttime");
    String starttime=starttimeParameters[0];

    String endtimeParameters[] = (String[])queryParameters.get("endtime");
    String endtime=endtimeParameters[0];
/*
    String filename1[] = (String[])queryParameters.get("filename");
    String filename1 =filename1[0];
*/
    String paramsParameters[] = (String[])queryParameters.get("params");    
    String params=paramsParameters[0];

    String fileName="";
    String branch="";


    /**参数检验*/
    if(null==define||"".equals(define))
    {
      out.println("err|parameter define should not be null");
      return;
    }
    if(null==calculate||"".equals(calculate))
    {
      out.println("err|parameter calculate should not be null");
      return;
    }
    if(null==file||"".equals(file))
    {
      out.println("err|parameter file should not be null");
      return;
    }
    if(null==edition||"".equals(edition))
    {
      out.println("err|parameter edition should not be null");
      return;
    } 
    System.out.println("month:"+month);
    if (starttime ==null || starttime.trim().equals(""))
    {
	    if (month != null && !month.trim().equals("")) {  //传入的是月份，必须转换为月末和月初日期
	    	month += "-01";  // make the full xxxx-xx-xx time format
	    	String timeArray[] = PubFun.calFLDate(month);
	    	starttime = timeArray[0];
	    	endtime = timeArray[1];
	    	System.out.println("starttime: " + starttime);
	    	System.out.println("endtime: " + endtime); 
	    }
    }

	System.out.println("****************************************######$$$$$$$$$");
	System.out.println(starttime);
	System.out.println(endtime);
    //if(month = null || month.trim().equals("")){
       //starttime=starttime;
       //endtime=endtime;
    //}
    if(null==starttime||"".equals(starttime))
    {
      out.println("err|parameter starttime should not be null");
      return;
    }
    if(null==endtime||"".equals(endtime))
    {
      out.println("err|parameter endtime should not be null");
      return;
    }

    if(null==params)
    {
      params = "";
    }

    DataInfo di = new DataInfo();
    di.setBranch(define);
    di.setCalculateBranch(calculate);
    di.setCode(file);
    di.setEdition(edition);
    di.setStartDate(starttime);
    di.setEndDate(endtime);

    fileName=di.getDataFile()+params+".xls";
    
    System.out.println("fileName: " + fileName);

    BufferedInputStream bin=new BufferedInputStream(new FileInputStream(fileName));
    System.out.println("after bin");
/**************deleted by dingzhong 2003-06-17*************/
    response.setContentType("application/msexcel;charset=gb2312");
/********************************************************/
	
	//拼出要下载的文件名称
//	String strDownFilename = filename1 + "_" + edition + "_" + starttime + "_" + endtime + ".xls";
//	System.out.println("downfilename : " + strDownFilename);
	
//	response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition","attachment; filename=report.xls");

    OutputStream outOS=response.getOutputStream();
    BufferedOutputStream bos=new BufferedOutputStream(outOS);


    byte[] b=new byte[4096];
    int intLength;
    String strFile="";
    while((intLength=bin.read(b))>0)
    {
      bos.write(b,0,intLength);
    }

    bos.flush();
    bos.close();
    bin.close();

//    response.sendRedirect("../temp/report.xls");
  }
  catch(Exception ex)
  {
    response.setContentType("text/html;charset=gb2312");
//        out.println("err|"+ex.getMessage());
//    out.println("<html><head><meta name=http-equiv content=text/html;charset=gb2312 />");
    out.println("<script language=javascript>alert(\"No such file.Have been calculated?\\n\u6587\u4ef6\u4e0d\u5b58\u5728.\u6ca1\u6709\u8ba1\u7b97?\");window.close();</script>");
//    out.println("</head></html>");
  }
%>

