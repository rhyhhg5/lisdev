<%
//程序名称:再保报表打印
//程序功能：
//创建日期：2003-12-22
//创建人  ：guoxiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    System.out.println("开始执行打印操作");
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
    //报表种类
    String Code = request.getParameter("Code");
    System.out.println("报表类型代码:"+Code);
    //时间
    String mDay[]=new String[2];
    mDay[0]=request.getParameter("time");
    mDay[1]=request.getParameter("timeend");
    //再保机构
    String gi="";
    gi=request.getParameter("ReInsureCom");
    //操作
    String strOperation ="PRINTPAY";
    VData tVData = new VData();
    VData mResult = new VData();
    tVData.addElement(mDay);
    tVData.addElement(gi);
    tVData.addElement(Code);
    tVData.addElement(tG);
    CError cError = new CError( );
    CErrors mErrors = new CErrors();
    XmlExport txmlExport = new XmlExport();
    
    RSCheckUI tRSCheckUI = new RSCheckUI();
    if(!tRSCheckUI.submitData(tVData,strOperation)){
      mErrors.copyAllErrors(tRSCheckUI.mErrors);
      cError.moduleName = "RSF1Print";
      cError.functionName = "submitData";
      cError.errorMessage = "tRSCheckUI";
      mErrors.addOneError(cError);
    }
    mResult = tRSCheckUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null){
       System.out.println("null没有正确的打印数据！！！");
    }else{
       session.putValue("PrintStream", txmlExport.getInputStream());
       System.out.println("put session value");
       response.sendRedirect("../../f1print/GetF1Print.jsp");
    }

%> 
<html>
  <script language="javascript">
    top.opener.focus();
    top.close();
   </script>
</html>

