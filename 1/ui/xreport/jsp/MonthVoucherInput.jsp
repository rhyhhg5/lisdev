
<html> 
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@ include file="init.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
    String Code = request.getParameter("Code");
    String CodeName = "";
    System.out.println(Code);
    String CurrentDate = PubFun.getCurrentDate();
    System.out.println(CurrentDate);
    String DefBranch="";
    String CurrentMonth = "";
    FDate fDate = new FDate();
    Date CurDate = fDate.getDate(CurrentDate);
    GregorianCalendar mCalendar = new GregorianCalendar();
    mCalendar.setTime(CurDate);
    int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的
    int Years = mCalendar.get(Calendar.YEAR);
    System.out.println("Months : " + Months);	
    System.out.println("Years :" + Years);	    
    ReportMain reportMain=new ReportMain();
    Vector vRM=reportMain.query();

    Vector vecBranchId=new Vector();
    Vector vecReportName=new Vector();
    Vector vecReportId=new Vector();
    Vector vecReportEdition=new Vector();
    for(int i=0;i<vRM.size();i++) 
    {
        ReportMain report=(ReportMain)vRM.elementAt(i);
        if (report.getReportId().equals(Code)) {
            CodeName = report.getReportName();
            DefBranch= report.getBranchId();
            break;
        }
    }
           
%>


<head>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">

  <SCRIPT src="../../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/MulLine.js"></SCRIPT>
 
  <LINK href="../../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../../common/css/mulLine.css" rel=stylesheet type=text/css>
  <LINK href="Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
      function initForm()
      {

      }
 
    function submitForm()
    {
        //首先得到选定的月的第一天的日期

    	if(document.all.code.value=='month4')
    	{
    		fm.action="./min1calculate.jsp";
    	}
    	else if(document.all.code.value=='month3')
    	{
    		fm.action="./min2calculate.jsp";
    	}
    	var Month = document.all('month').value;

    	if (Month < 10)
    	    Month  = '0' + Month;
    	    
        //组成Date格式	    
    	var fistDayDate = <%=Years%> + '-' + Month + "-01";
    	//如果没有输入，默认取得当前年的当前输入月的1号
    	if (document.all.time.value == "") 
    	{
    		document.all.time.value = fistDayDate;
    	}
    	
    	fm.submit();
    }  
  </script>
  
</head>
<body  onload="initForm();" >

<form action="./calculate.jsp" method=post name=fm >
   <Table class= common>
      <TR class= common>
          <TD  class= title>
            单位编码
          </TD>  
          <TD  class= input>
            <Input class=common readonly name=branch value=<%=DefBranch%>>
          </TD>
      </TR>
      <TR  class= common> 
          <TD  class= title>
            报表代码
          </TD>  
          <TD class= title>
             <input class=common readonly name=CodeName value=<%=CodeName%>>
          </TD>

          <TD  class= input style="display: none">
            <Input class=common readonly name=code value = <%=Code%> >
          </TD>
      </TR>
          
      <TR class= common>    
          <TD class= title>
            报表版别
          </TD>          
          <TD  class= input>
            <Input class=common name=edition  readonly value = 20030401>
          </TD>
      </TR>
       
      <TR class = common>    
         <TD  class= title>
            报表时间(月份)
          </TD>          
          <TD  class= input>    
              <Input class=common  name= "month" value=<%=Months%>>
          </TD>
      </TR>
         <TR class= common>
         <TD  class= title colspan=3>
                当输入以下日期范围时，以日期范围取数，如：输入2003-12-01，则强制提取2003年12月的数据。
          </TD>          
		</tr>      
      
      <TR class = common>    
         <TD  class= title>
            报表日期
          </TD>          
          <TD class= common">
              <Input class="coolDatePicker" dateFormat="short" verify="日期|NOTNULL" name= "time">
          </TD>    
      </TR>

    </Table>   
            
  <Div id= divCmdButton style="display: ''">
     <INPUT VALUE="计算" class= common TYPE=button onclick="submitForm()">  
  </Div>     
</form>

</body>
</html>
