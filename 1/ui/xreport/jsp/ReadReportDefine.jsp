<%@page language="java" %><%@
page import="java.util.*" %><%@
page import="java.io.*" %><%@
page import="java.net.*" %><%@
page import="com.f1j.swing.JBook"%><%@
page import="com.sinosoft.xreport.util.*"%><%@
page contentType="text/plain;charset=GBK" %><%
    /////////////////////////下载报表定义格式文件///////////////////////
    try
    {
        String strFileName=request.getParameter("file");
        String branch="";
        if(null==strFileName||"".equals(strFileName))
        {
            out.println("err|parameter strFileName should not be null");
            return;
        }

        // lixy 20030226 1.4->1.3
        StringTokenizer token=new StringTokenizer(strFileName,SysConfig.REPORTJOINCHAR);
        if(token.hasMoreTokens())
            branch=token.nextElement().toString();

        String fileName=SysConfig.FILEPATH
                       +"define"
                       +SysConfig.FILESEPARATOR
                       +branch
                       +SysConfig.FILESEPARATOR
                       +strFileName;
        BufferedInputStream file=new BufferedInputStream(new FileInputStream(fileName));
        OutputStream os=response.getOutputStream();
        BufferedOutputStream bos=new BufferedOutputStream(os);

        byte[] b=new byte[4096];
        int intLength;
        String strFile="";
        while((intLength=file.read(b))>0)
        {
            bos.write(b,0,intLength);
        }

        bos.flush();
        bos.close();
        }
        catch(Exception ex)
        {
            out.println("err|"+ex.getMessage());
        }
%>