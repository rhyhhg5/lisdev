<%
//程序名称:保全报表打印(初始值）
//程序功能:各模块的清单类型的报表打印  
//创建日期：2003-11-17
//创建人  ：郭翔
//更新记录：  更新人    更新日期     更新原因/内容
//其中：
// * <p>bq1：保全日结(费用类-个险)</p>
// * <p>bq2: 保全月结(综合类-个险)</p>
// * <p>bq3: 保全日结(费用类-法人明细)</p>
// * <p>bq4: 保全月结(综合类-法人明细)</p>
// * <p>bq5: 保全日结(费用类-银代)</p>
// * <p>bq6: 保全月结(综合类-银代)</p>
//add 
// * <p>bq7: 保全日结清单（费用类--团险合计）</p>
// * <p>bq8: 保全月结清单（综合类--团险合计）</p>
%>
<%@include file="../../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.vdb.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
    String strCurTime = PubFun.getCurrentDate();   
    String Code = request.getParameter("Code");
    String CodeName ="";
    System.out.println("bq的参数为:"+Code);
    String CurrentDate = PubFun.getCurrentDate();
    String mDay[]=PubFun.calFLDate(CurrentDate);
    LDMenuDB tLDMenuDB = new LDMenuDB();
    String name="../xreport/jsp/BQcalculate.jsp?Code="+Code;
    tLDMenuDB.setRunScript(name);
    LDMenuSet mLDMenuSet=tLDMenuDB.query();
    for (int i=1;i<=mLDMenuSet.size();i++){
      LDMenuSchema tLDMenuSchema = mLDMenuSet.get(i);
       CodeName=tLDMenuSchema.getNodeName();
    }  
      
%>
<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../../common/javascript/Common.js"></SC RIPT>
  <SCRIPT src="../../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/EasyQuery.js"></SCRIPT>
  <LINK href="Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
   function submitForm(){
      var code="<%=Code%>";
      if (fm.StationCode.value.length ==0){
	 alert("统计区域没选择！！！！");
	 return false;
      }
      if ((fm.StartTime.value.length ==0)||(fm.EndTime.value.length ==0)){
          alert("时间没有选择！！！！");
          return false;
      }
      fm.target = "f1jprint";
      fm.action="./BQF1Print.jsp";
      fm.submit();
   }  
  </script>
</head>
<body>
<form method=post name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>
            报表名称
          </TD>  
          <TD class= input>
             <input readonly class=common name=CodeName value=<%=CodeName%>>
          </TD>
          <TD class= input style="display: none">
            <Input class=common name=Code value = <%=Code%> >
          </TD>
           <TD class=title>
            操 作 员
          </TD>          
          <TD class=input>    
             <input class = common name=opt>
          </TD>  
     
      </TR>
      <TR class = common>    
          <TD class=title>
            统计区域
          </TD>          
          <TD class=input>    
              <input type="text" class="code" name=StationCode onDblClick="showCodeList('station',[this],null,null,codeSql,'1',null,250);"  onKeyUp="return showCodeListKey('station',[this],null,null,codeSql,'1',null,250);">
          </TD>  
          <TD class=title>
            保全项目
          </TD>          
          <TD class=input>  
           <% 
             if(Code.equals("bq1")||Code.equals("bq5")){//个险和银代费用
           %>
              <Input type="text" class="code" name= bqcode ondblclick="return showCodeList('edortypecode', [this],null,null,codeSql1,'1',null,200);" onkeyup="return showCodeListKey('edortypecode', [this],null,null,'1','codename',null,200);" verify="批改类型|NOTNULL">
           <% 
             }else{
                if(Code.equals("bq3")||Code.equals("bq7")){//团险费用
           %>
              <Input type="text" class="code" name= bqcode ondblclick="return showCodeList('edortypecode1', [this],null,null,codeSql1,'1',null,200);" onkeyup="return showCodeListKey('edortypecode', [this],null,null,'1','codename',null,200);" verify="批改类型|NOTNULL">
           <% 
                }else{
                 if(Code.equals("bq2")||Code.equals("bq6")){//个险和银代非费用
           %>
              <Input type="text" class="code" name= bqcode ondblclick="return showCodeList('edortypecode', [this],null,null,null,null,null,200);" onkeyup="return showCodeListKey('edortypecode', [this],null,null,'1','codename',null,200);" verify="批改类型|NOTNULL">
           <%
                 }else{   //团险非费用
           %>
              <Input type="text" class="code" name= bqcode ondblclick="return showCodeList('edortypecode1', [this],null,null,null,null,null,200);" onkeyup="return showCodeListKey('edortypecode', [this],null,null,'0','codename',null,200);" verify="批改类型|NOTNULL">
           <%
                  }
                 }
               }  
           %>
           
           </TD>
        </TR>
        <TR class =common>    
          <TD class= title>
            开始时间
          </TD>          
          <TD class=input> 
            <Input class="coolDatePicker" dateFormat="short" verify="起始时间|NOTNULL" name=StartTime value='<%=strCurTime%>'> 
          </TD>
          <TD class =title> 
            结束时间
          </TD>
          <TD class=input>  
            <Input class="coolDatePicker" dateFormat="short" verify="结束时间|NOTNULL" name=EndTime value='<%=strCurTime%>'>
          </TD>
      </TR>
    </Table>  
    <Div id= divCmdButton style="display: ''">
       <INPUT class=common  VALUE="打印报表" TYPE=button onclick="submitForm()">     
    </Div>
 </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"> </span> 
</body>
</html>
<script>
var codeSql = "1  and code like #"+<%=Branch%>+"%#";
var codeSql1 = "1 and othersign = #1#";
</script>