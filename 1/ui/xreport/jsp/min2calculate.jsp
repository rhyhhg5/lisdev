<%@ page contentType="text/html; charset=GBK" %>
<%@ page import="com.sinosoft.lis.pubfun.*"%>
<%@ page import="com.sinosoft.xreport.bl.*" %>
<%@ page import="com.sinosoft.xreport.util.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="org.apache.log4j.*" %>
<%@ include file="init.jsp"%>
<%--@todo: 计算单位取登录单位,操作员取登录操作员--%>
<%!
  boolean isEmpty(String in)
  {
    return null==in||"".equals(in);
  }

  static final String DATASOURCE="DataSource";
  static final String ENVIRONMENT="Environment";
  static final String TABLERELATIONS="TableRelations";


  org.apache.log4j.Logger log=XTLogger.getLogger(this.getClass());

%>
<%

  log.debug(SysConfig.TRUEHOST+":"+SysConfig.FILEPATH);

//  if(true)
//  	return;

  try
{

  Class.forName("oracle.jdbc.driver.OracleDriver");


  long time=Calendar.getInstance().getTime().getTime();

  String defineBranch=request.getParameter("branch");
  String defineCode=request.getParameter("code");
  String defineEdition=request.getParameter("edition");
  String calculateDate=request.getParameter("time");  //计算时间

  if(isEmpty(defineBranch)||isEmpty(defineCode)||isEmpty(defineEdition))
  {
    out.println("err|参数不全.\n定义单位,代码,版别为必需的参数");
    return;
  }

  //数据层对象
  com.sinosoft.xreport.dl.DataSource dataSource=(com.sinosoft.xreport.dl.DataSource) session.getAttribute(DATASOURCE);
  if(dataSource==null)
  {
    dataSource=new com.sinosoft.xreport.dl.BufferDataSourceImpl();
    session.setAttribute(DATASOURCE,dataSource);
  }

  //环境对象
  Environment environment=(Environment) session.getAttribute(ENVIRONMENT);
  if(environment==null)
  {
    environment=new EnvironmentImpl();
    session.setAttribute(ENVIRONMENT,environment);
  }

  //环境对象
  TableRelations tableRelations=(TableRelations) session.getAttribute(TABLERELATIONS);
  if(tableRelations==null)
  {
    tableRelations=new TableRelations(dataSource);
    session.setAttribute(TABLERELATIONS,tableRelations);
  }

  //todo:登录单位,操作员,权限, add here
  GlobalInput gi=(GlobalInput)session.getAttribute("GI");

  if(null==gi)
  {
    out.println("err|超时.需要重新登录");
    return;
  }

  String operatorID=gi.Operator;  //操作员ID
  String calculateBranch=gi.ManageCom; //登陆单位ID

//  String operatorID="001";  //操作员ID
//  String calculateBranch="86"; //登陆单位ID


  environment.setEnv(Environment.OPERATORID,operatorID);
  environment.setEnv(Environment.CALCBRANCH,calculateBranch);

  //计算时间.
  if(!isEmpty(calculateBranch))
    environment.setEnv(Environment.CALCDATE,calculateDate);
  //没有设定,就是当前时间,见EnvironmentImpl

  //报表对象
  Report r1=new Report(defineBranch,"month31",defineEdition);
  r1.setRunEnv(environment);

  //设定db对象╭∩╮（︶︿︶）╭∩╮
  r1.setDbDataSource(dataSource);
  r1.setTableRelations(tableRelations);

  r1.init();
  r1.calculate(); //it's high cost!
  ReportWriter rw1=new ReportWriter();
  ReportWriter rwExcel1=new ExcelWriter(); //写到excel,当然可以写到xml,html...
  rw1.setEnvironment(environment);
  rwExcel1.setEnvironment(environment);
  r1.write(rw1); //写到xml
  
  //报表对象
  Report r2=new Report(defineBranch,"month32",defineEdition);
  r2.setRunEnv(environment);

  //设定db对象╭∩╮（︶︿︶）╭∩╮
  r2.setDbDataSource(dataSource);
  r2.setTableRelations(tableRelations);

  r2.init();
  r2.calculate(); //it's high cost!
  ReportWriter rw2=new ReportWriter();
  ReportWriter rwExcel2=new ExcelWriter(); //写到excel,当然可以写到xml,html...
  rw2.setEnvironment(environment);
  rwExcel2.setEnvironment(environment);
  r2.write(rw2); //写到xml
  
  
  Report r=new Report(defineBranch,defineCode,defineEdition);
  r.setRunEnv(environment);

  //设定db对象╭∩╮（︶︿︶）╭∩╮
  r.setDbDataSource(dataSource);
  r.setTableRelations(tableRelations);

  r.init();
  r.calculate(); //it's high cost!
  ReportWriter rw=new ReportWriter();
  ReportWriter rwExcel=new ExcelWriter(); //写到excel,当然可以写到xml,html...
  rw.setEnvironment(environment);
  rwExcel.setEnvironment(environment);
  r.write(rw); //写到xml

  try {
    r.write(rwExcel);
  }catch (Exception ex)
  {
    log.error("写入Excel文件时出错,请检查格式文件是否存在.exmsg:"+ex.getMessage());
  }

  time=Calendar.getInstance().getTime().getTime()-time;

  out.println("ok|计算成功.");

  log.info("......calcuate.."+r.getReportData().getDataInfo().getDefineFilePath()+"..total time.."+time);

  }catch(Exception ex)
  {

    out.println("err|计算失败.查看日志文件,获取更多信息.errmsg:");
    ex.printStackTrace(new PrintWriter((Writer)out));
    log.error(ex.getMessage(),ex.fillInStackTrace());
  }

%>