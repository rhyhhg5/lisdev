
<html> 
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@ include file="init.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
    String Code = request.getParameter("Code");
    String CodeName = "";
    System.out.println(Code);
    String CurrentDate = PubFun.getCurrentDate();
    System.out.println(CurrentDate);
    String DefBranch="";
    String CurrentMonth = "";
    FDate fDate = new FDate();
    Date CurDate = fDate.getDate(CurrentDate);
    GregorianCalendar mCalendar = new GregorianCalendar();
    mCalendar.setTime(CurDate);
    int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的
    int Quarter = (Months -1)/3 + 1;
    int Years = mCalendar.get(Calendar.YEAR);
    System.out.println("Months : " + Months);	
    System.out.println("Years :" + Years);	    
    ReportMain reportMain=new ReportMain();
    Vector vRM=reportMain.query();

    Vector vecBranchId=new Vector();
    Vector vecReportName=new Vector();
    Vector vecReportId=new Vector();
    Vector vecReportEdition=new Vector();
    for(int i=0;i<vRM.size();i++) 
    {
        ReportMain report=(ReportMain)vRM.elementAt(i);
        if (report.getReportId().equals(Code)) {
            CodeName = report.getReportName();
            DefBranch= report.getBranchId();
            break;
        }
    }
           

%>


<head>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">

  <SCRIPT src="../../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/MulLine.js"></SCRIPT>
 
  <LINK href="../../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../../common/css/mulLine.css" rel=stylesheet type=text/css>
  <LINK href="Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
      function initForm()
      {

      }
 
    function down()
    {
      var params=document.all.param.value;
      if(params!="")
        params = "_" + params;
      var endmonth = "";
      var firstmonth ="";
      var firstday = "";
      var lastday = "";
      
      if (document.all.time.value==""||document.all.timeend.value==""){
       
        var Quarter = document.all('quarter').value;
       	if (Quarter > 4) {
       	    alert("季度值输入非法！");
       	    return;
       	}
       	
       	
    	//得到季度的首日和尾日
    	endmonth = Quarter * 3;
    	firstmonth = endmonth - 2;
    	firstday =  <%=Years%> + '-' + firstmonth + '-01';
   
    	if (Quarter == 1 || Quarter == 4) {
    	    lastday = <%=Years%> + '-' + endmonth + '-31';
    	} else {
    	    lastday = <%=Years%> + '-' + endmonth + '-30';
    	}    
     }
     else{
        var firstday=document.all.time.value;
        var lastday=document.all.timeend.value;
        
 
     
     }
     window.open("./ReadBranch.jsp?define="+document.all.define.value+
                    "&calculate="+document.all.calculate.value+
                    "&file="+document.all.code.value+
                    "&edition="+document.all.edition.value+
                    "&month= " + 
                    "&starttime= "+ firstday +
    	            "&endtime= "+ lastday +
                    "&params="+params,'newwindow', 'height=1, width=1, top=2000, left=2000, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no');

    }

    function show()
    {
       	var Quarter = document.all('quarter').value;
       	if (Quarter > 4) {
       	    alert("季度值输入非法！");
       	    return;
       	}
       	
    	//得到季度的首日和尾日
    	var endmonth = Quarter * 3;
    	var firstmonth = endmonth - 2;
    	var firstday =  <%=Years%> + '-' + firstmonth + '-01';
    	var lastday = "";
    	if (Quarter == 1 || Quarter == 4) {
    	    lastday = <%=Years%> + '-' + endmonth + '-31';
    	} else {
    	    lastday = <%=Years%> + '-' + endmonth + '-30';
    	}    
          	
    	fm.action="./Show.jsp?define="+document.all.define.value+
    	                        "&calculate="+document.all.calculate.value+
    	                        "&file="+document.all.code.value+
    	                        "&edition="+document.all.edition.value+
    	                        "&month= " + 
			        "&starttime= "+firstday+
    	    			"&endtime= " + lastday;
    	location.href = fm.action;      			            	                       
    }  
     

    
    
    
  </script>
  
</head>
<body  onload="initForm();" >

<form action="./calculate.jsp" method=post name=fm >
   <Table class= common>
      <TR class= common>
          <TD  class= title>
            定义单位
          </TD>  
          <TD  class= input>
            <Input class=common readonly name=define value=<%=DefBranch%> >
          </TD>
      </TR>
      <TR class= common>
          <TD  class= title>
            计算单位
          </TD>  
          <TD  class= input>
            <Input class=common readonly name=calculate value=<%=Branch%> >
          </TD>
      </TR>      
      
      <TR  class= common> 
          <TD  class= title>
            报表代码
          </TD>  
          <TD class= title>
             <input class=common readonly name=file value=<%=CodeName%>>
          </TD>

          <TD  class= input style="display: none">
            <Input class=common readonly name=code value = <%=Code%> >
          </TD>
      </TR>
          
      <TR class= common>    
          <TD class= title>
            报表版别
          </TD>          
          <TD  class= input>
            <Input class=common name=edition  readonly value = 20030401>
          </TD>
      </TR>
      
      <TR class = common>    
         <TD  class= title>
            报表时间(季度)
          </TD>          
          <TD  class= input>    
              <Input class=common  name= "quarter" value=<%=Quarter%>>
          </TD>
          
      </TR>
      <TR class= common>
         <TD  class= title colspan=3>
            当输入该日期范围时，以日期范围取数。
          </TD>          
		</tr>      
      <TR class= common>    
          <TD class= title>
            报表开始日期：
          </TD>          
          <TD class= common>
              <Input class="coolDatePicker" verify="报表时间|DATE" dateFormat="short"   name= "time">
          </TD>    
      </TR>
      <TR class= common>    
          <TD class= title>
            报表结束日期：
          </TD>          
          <TD class= common>
              <Input class="coolDatePicker" verify="报表时间|DATE" dateFormat="short"   name= "timeend">
          </TD>    
      </TR>
     <TR class= common>    
          <TD class= title>
            参数
          </TD>          
          <TD  class= input>
            <Input class=common name=param >
          </TD>
      </TR>
      
    </Table>   
            
  <Div id= divCmdButton style="display: ''">
      <!--<INPUT VALUE="查看" TYPE=button onclick="show()">--> 
    <INPUT VALUE="下载" class= common TYPE=button onclick="down()">  
  </Div>    
   
</form>

</body>
</html>
