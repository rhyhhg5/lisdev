
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-12-17 16:10:00
//创建人  lixy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@include file="init.jsp" %>
<%

/////////////////////////////////////
//AppleWood 报表代码->名称

Map codeMap=(Map)session.getAttribute("ReportMainCodeNameMap");
if(codeMap==null)
{
  ReportMain reportMain=new ReportMain();
  Vector vRM=reportMain.query();
  session.setAttribute("ReportMainCodeNameMap",codeMap);

  codeMap=new HashMap();
  for(int i=0;i<vRM.size();i++)
  {
    ReportMain report=(ReportMain)vRM.elementAt(i);
    codeMap.put(report.getReportId(),report.getReportName());
  }

}

//end AppleWood
////////////////////////////////////

	FileDisplay file=new FileDisplay();
	Vector vecDefine=file.getDefineDir();
	Vector vecCalculate=file.getCalculateDir();
	Vector vecFile=file.getFiles();
	Vector vecEdition=file.getEdition();
	
       String code = request.getParameter("Code");
%>
<head >
  <LINK href="./Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
    function loadBranch()
    {
     var define1=document.all.define;
     <%
     for(int i=0;i<vecDefine.size();i++)
     {
     %>
       var defineoption=document.createElement("option");
       defineoption.innerHTML="<%=(String)vecDefine.elementAt(i)%>";
       defineoption.value="<%=(String)vecDefine.elementAt(i)%>";
       define1.appendChild(defineoption);
     <%}%>

     var calculate1=document.all.calculate;
     <%
     for(int i=0;i<vecCalculate.size();i++)
     {
     %>
       var calculateoption=document.createElement("option");
       calculateoption.innerHTML="<%=(String)vecCalculate.elementAt(i)%>";
       calculateoption.value="<%=(String)vecCalculate.elementAt(i)%>";
       calculate1.appendChild(calculateoption);
     <%
     }
     %>

     var file1=document.all.file;
     <%
     for(int i=0;i<vecFile.size();i++)
     {
     %>
       var fileoption=document.createElement("option");
       fileoption.innerHTML="<%=codeMap.get((String)vecFile.elementAt(i))%>";
       fileoption.value="<%=(String)vecFile.elementAt(i)%>";
       var code1 = "<%=code%>"
  //     alert("code1" + code1);
  //     alert(fileoption.value);
       if (fileoption.value == code1)
           file1.appendChild(fileoption);
     <%}%>

     var edition1=document.all.edition;
     <%
     for(int i=0;i<vecEdition.size();i++)
     {
     %>
       var editionoption=document.createElement("option");
       editionoption.innerHTML="<%=(String)vecEdition.elementAt(i)%>";
       editionoption.value="<%=(String)vecEdition.elementAt(i)%>";
       if (editionoption.value == 20030401)
           edition1.appendChild(editionoption);
     <%}%> 
    }


    function down()
    {
      var params=document.all.param.value;
      if(params!="")
        params = "_" + params;
      //fm.submit();
      /*
      fm.action="./ReadBranch.jsp?define="+document.all.define.value+
      "&calculate="+document.all.calculate.value+
      "&file="+document.all.file.value+
      "&edition="+document.all.edition.value+
      "&starttime="+document.all.starttime.value+
      "&endtime="+document.all.endtime.value+
      "&params="+params;
      */

      //alert(fm.action);
      //location.href=fm.action;
      //fm.submit();
     window.open("./ReadBranch.jsp?define="+document.all.define.value+
                    "&calculate="+document.all.calculate.value+
                    "&file="+document.all.file.value+
                    "&edition="+document.all.edition.value+
                    "&starttime="+document.all.starttime.value+
                    "&endtime="+document.all.endtime.value+
                    "&params="+params,'newwindow', 'height=1, width=1, top=2000, left=2000, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no');

    }

    function show()
    {

    	fm.action="./Show.jsp?define="+document.all.define.value+
    	                        "&calculate="+document.all.calculate.value+
    	                        "&file="+document.all.file.value+
    	                        "&edition="+document.all.edition.value+
    	                        "&starttime="+document.all.starttime.value+
    	                        "&endtime="+document.all.endtime.value;
    	//alert(fm.action);
    	location.href = fm.action;
    	//fm.submit();

    }
  </script>
</head>
  <body onload="loadBranch()">
    <form action="./down.jsp" method=post name="fm">

      <table align="right" class= "common">
      <tr>
      <td height="100"></td>
      </tr>
        <tr class="common">
        <td width="30%"></td>
        	<td height="30"  width="15%">
        		<font size="3">下载数据文件</font>
        	</td>
        	<td>
        	</td>
        </tr>
        <TR class= "common"><td width="30%"></td>

      <TD class= "title" width="10%"> 定义单位 </TD>
          <TD class= "input">
            <select class= "code" name="define">
            </select>
          </TD>
          <td width="10%"></td>
          </tr>
		<tr class= "common"><td width="30%"></td>

      <TD class= "title" width="10%"> 计算单位 </TD>
          <TD class= "input">
            <select class= "code" name="calculate">
            </select>
          </TD>
          <td width="10%"></td>
          </tr>
          <tr class= "common"><td width="30%"></td>

      <TD class= "title" width="10%"> 报表代码 </TD>
          <TD class= "input">
            <select class= "code" name="file">
            </select>
          </TD>
          <td width="10%"></td>
        </tr>
        <tr class= "common"><td width="30%"></td>

      <TD class= "title" width="10%"> 版别 </TD>
          <TD class="input">
        		<select class=common name="edition" value =20030401>
          </TD>
         <td width="10%"></td>
         </tr>
         <tr class= "common"><td width="30%"></td>

      <TD class= "title" width="10%"> 开始时间 </TD>
          <TD class="input">
        		<input class="code" name="starttime" value="<%=Str.getDate()%>">
          </TD>
         <td width="10%"></td>
         </tr>
         <tr class= "common"><td width="30%"></td>

      <TD class= "title" width="10%"> 结束时间 </TD>
          <TD class="input">
        		<input class="code" name="endtime" value="<%=Str.getDate()%>">
          </TD>
         <td width="10%"></td>
         </tr>
         <tr class= "common"><td width="30%"></td>

      <TD class= "title" width="10%"> 参数 </TD>
          <TD class="input">
        		<input class="code" name="param" value="">
          </TD>
         <td width="10%"></td>
         </tr>


         <tr>
         <td width="10%"></td>
         	<td></td>
      <TD class="button" width="10%" align="right"> <img src='../../common/images/butSave.gif' alt="下载"  align="right" width="65" height="23" class="button"
				onclick="down()"
				onmouseover=""
				onmouseout=""></img> <img src='../../common/images/butView.gif' alt="查看"  align="right" width="65" height="23" class="button"
				onclick="show()"
				onmouseover=""
				onmouseout=""></img></TD>

				<td width="35%"></td>
         </tr>
      </table>

  </form>
</body>
</html>
