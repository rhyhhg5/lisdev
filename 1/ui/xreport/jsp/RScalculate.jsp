<%
//程序名称:再保报表统计
//程序功能:再保报表的清单类型的季报打印  
//创建日期：2003-12-22
//创建人  ：guoxiang
//更新记录：  更新人    更新日期     更新原因/内容
//其中
// * <p>rs2:人身险法定分保详细业务批单季度报表</p>
// * <p>rs3:人身险法定分保短期险详细业务新单季度报表</p>
// * <p>rs4:人身险法定分保短期险详细业务理赔季度报表</p>
%>
<%@include file="../../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.vdb.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
<%
    String CurrentDate = PubFun.getCurrentDate();
    System.out.println(CurrentDate);

    String CurrentMonth = "";
    FDate fDate = new FDate();
    Date CurDate = fDate.getDate(CurrentDate);
    GregorianCalendar mCalendar = new GregorianCalendar();
    mCalendar.setTime(CurDate);
    int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的
    int Quarter = (Months - 1) / 3 + 1;  //计算当前季度
    System.out.println("Quarter : " + Quarter);
    int Years = mCalendar.get(Calendar.YEAR);
    System.out.println("Months : " + Months);	
    System.out.println("Years :" + Years);	
    String Code = request.getParameter("Code");
    String CodeName ="";
    System.out.println("参数为:"+Code);

    String mDay[]=PubFun.calFLDate(CurrentDate);
    LDMenuDB tLDMenuDB = new LDMenuDB();
    String name="../xreport/jsp/RScalculate.jsp?Code="+Code;
    tLDMenuDB.setRunScript(name);
    LDMenuSet mLDMenuSet=tLDMenuDB.query();
    for (int i=1;i<=mLDMenuSet.size();i++){
      LDMenuSchema tLDMenuSchema = mLDMenuSet.get(i);
       CodeName=tLDMenuSchema.getNodeName();
    }  
      
%>
<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../../common/javascript/Common.js"></SC RIPT>
  <SCRIPT src="../../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/EasyQuery.js"></SCRIPT>
  <LINK href="Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
    function submitForm(){
      var code="<%=Code%>";
      fm.target = "f1jprint";
      fm.action="./RSF1Print.jsp";   
      fm.submit();
   }  
  </script>
</head>
<body>
<form method=post name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>
            报表名称:
          </TD>  
          <TD class= title>
             <input readonly class=common name=CodeName value=<%=CodeName%>>
          </TD>
          <TD class= input style="display: none">
            <Input class=common name=Code value = <%=Code%> >
          </TD>
      </TR>
      <TR class =common>    
          <TD class= title>
            分保单位：
          </TD>          
          <TD class=input> 
             <input readonly class=common name=ReInsureCom value='1001'> </TD>
      </TR>
     <TR class =common>       
           <TR class =common>    
          <TD class= title>
            开始时间
          </TD>  
          
          <TD class= input>
              <Input class="coolDatePicker" dateFormat="short" verify="起始时间|NOTNULL" name= "time">
          </TD> 
          <TD class =title> 
            结束时间
          </TD>
          <TD class= input>
              <Input class="coolDatePicker" dateFormat="short" verify="结束时间|NOTNULL" name= "timeend">
          </TD> 
     </TR>   
   </Table>  
    <Div id= divCmdButton style="display: ''">
       <INPUT class=common  VALUE="打印报表" TYPE=button onclick="submitForm()">     
    </Div>
 </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"> </span> 
</body>
</html>