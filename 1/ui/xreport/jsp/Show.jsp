<%
//程序名称：Show.jsp
//程序功能：以html的样式展示报表
//创建日期：2003-04-18 13:10:00
//创建人  houzw
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page language="java" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="java.net.*" %>
<%@page import="com.sinosoft.xreport.util.*"%>
<%@page import="com.sinosoft.xreport.bl.*"%>
<%@page import="com.sinosoft.xreport.dl.*"%>
<%@page contentType="text/plain;charset=GBK" %>
<%
/////////////////////////显示报表///////////////////////
try
{
    /**读取参数*/
    System.out.println("start read paramas");
    
    Hashtable queryParameters = javax.servlet.http.HttpUtils.parseQueryString(request.getQueryString());
    String defineParameters[] = (String[])queryParameters.get("define");
    String define=defineParameters[0];
    String calculateParameters[] = (String[])queryParameters.get("calculate");
    String calculate = calculateParameters[0];
    String fileParameters[] = (String[])queryParameters.get("file");
    String file = fileParameters[0];
    String editionParameters[] = (String[])queryParameters.get("edition");
    String edition = editionParameters[0];
    
    System.out.println(define);
    System.out.println(calculate);
    System.out.println(file);
    System.out.println(edition);
    

    
    String monthParameters[] = (String[])queryParameters.get("month");
    String month = monthParameters[0];    
    
    System.out.println(month);

    String starttimeParameters[] = (String[])queryParameters.get("starttime");
    String starttime = starttimeParameters[0];
    String endtimeParameters[] = (String[])queryParameters.get("endtime");
    String endtime = endtimeParameters[0];
    
    System.out.println(starttime);
    System.out.println(endtime);

    String fileName="";
    String branch="";

    /**参数检验*/
    if(null==define||"".equals(define))
    {%>
       <script language="javascript">
       	alert("err|parameter define should not be null");
       	history.back();
       </script>
       
    <% return;
    }else
    if(null==calculate||"".equals(calculate))
    {%>
    	<script language="javascript">alert("err|parameter calculate should not be null");
    	history.back();</script>
    <%	return;
    }else
    if(null==file||"".equals(file))
    {%>
    	<script language="javascript">alert("error|parameter file should not be null");history.back();</script>
    <%	return;
    }else
    if(null==edition||"".equals(edition))
    {%>
    	<script language="javascript">alert("error|parameter edition should not be null");history.back();</script>
    <%	return;
    }else { 
     
     if (month != null && !month.trim().equals("")) {  //传入的是月份，必须转换为月末和月初日期
//    	month = "2003-5-06";
    	month += "-01";  // make the full xxxx-xx-xx time format
    	System.out.println("month: " + month);
    	String[] timeArray = PubFun.calFLDate(month);
    	starttime = timeArray[0];
    	endtime = timeArray[1];
    	System.out.println("starttime: " + starttime);
    	System.out.println("endtime: " + endtime); 
      }  

	    if(null==starttime||"".equals(starttime))
	    {%>
	       <script language="javascript">alert("err|parameter starttime should not be null");history.back();</script>
	     <%   return;
	    }else
	    if(null==endtime||"".equals(endtime))
	    {%>
	        <script language="javascript">alert("err|parameter endtime should not be null");history.back();</script>
	      <%  return;
	    }
      }
		if(file.equals("month4") && edition.equals("20030401"))
		    edition = "20030401_gens";
		DefineInfo dfinfo = new DefineInfo();
		dfinfo.setBranch(define);
		dfinfo.setCode(file);
		dfinfo.setEdition(edition);
		String definefilepath = dfinfo.getDefineFilePath();
	
			//out.println("definefile is :"+definefilepath);
		
		if(edition.equals("20030401_gens"))
			edition = "20030401";
		DataInfo dainfo = new DataInfo();
		dainfo.setCalculateBranch(calculate);
		dainfo.setCode(file);
		dainfo.setEdition(edition);
		dainfo.setBranch(define);
		dainfo.setStartDate(starttime);
		dainfo.setEndDate(endtime);
		String datafilepath = dainfo.getDataFile();
		XTLogger.getLogger(this.getClass()).debug("((((("+datafilepath);
		//out.println("datafile is :"+datafilepath);
		ShowReport sr = new ShowReport();
		sr.setDefineName(definefilepath);
		sr.setStyle(SysConfig.FILEPATH + "style"+SysConfig.FILESEPARATOR+"minsheng.xslt");
		int i1 = datafilepath.lastIndexOf(SysConfig.FILESEPARATOR);
		XTLogger.getLogger(this.getClass()).debug("((((("+SysConfig.FILEPATH + "style"+SysConfig.FILESEPARATOR+"minsheng.xslt");
		
		
		int i2 = datafilepath.indexOf(".xml");
		String temp = datafilepath.substring(i1+1,i2);
		
		
		sr.setResult(SysConfig.FILEPATH+ "result"+SysConfig.FILESEPARATOR+temp+ ".html");
		XTLogger.getLogger(this.getClass()).debug("))))))))))"+SysConfig.FILEPATH+ "result"+SysConfig.FILESEPARATOR+temp+ ".html");
		File f = new File(datafilepath);
		if(!f.exists())
		{%>
			
		<script language="javascript">alert("error|the datafile not exist");history.back();</script>
		<%	return;
		}
		
		sr.setFile(datafilepath);
		
		sr.transToHTML();
		
		
		
		File outfile=new File(SysConfig.FILEPATH+ "result"+SysConfig.FILESEPARATOR+temp+ ".html");
		FileInputStream fi=new FileInputStream(outfile);
		byte[] bt=new byte[2048];
		int i=-1;

		while((i=fi.read(bt))>0)
		{
				out.print(new String(bt,0,i,"GBK"));
		}

		fi.close();
		
		out.close();
		
		
    }
    catch(Exception ex)
    {
        out.println("err|"+ex.getMessage());
    }

    
%>
