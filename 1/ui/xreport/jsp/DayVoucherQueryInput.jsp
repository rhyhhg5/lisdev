
<html> 
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@ include file="init.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
    String Code = request.getParameter("Code");
    String CodeName = "";
    String DefBranch="";
    System.out.println(Code);
    String CurrentDate = PubFun.getCurrentDate();
    
    ReportMain reportMain=new ReportMain();
    Vector vRM=reportMain.query();

    Vector vecBranchId=new Vector();
    Vector vecReportName=new Vector();
    Vector vecReportId=new Vector();
    Vector vecReportEdition=new Vector();
    for(int i=0;i<vRM.size();i++) 
    {
        ReportMain report=(ReportMain)vRM.elementAt(i);
        if (report.getReportId().equals(Code)) {
            CodeName = report.getReportName();
            DefBranch= report.getBranchId();
            break;
        }
    }       

%>


<head>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">

  <SCRIPT src="../../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/MulLine.js"></SCRIPT>
 
  <LINK href="../../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../../common/css/mulLine.css" rel=stylesheet type=text/css>
  <LINK href="Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
  
    function down()
    {
      var params=document.all.param.value;
      if(params!="")
        params = "_" + params;
 
     window.open("./ReadBranch.jsp?define="+document.all.define.value+
                    "&calculate="+document.all.calculate.value+
                    "&file="+document.all.code.value+
                    "&edition="+document.all.edition.value+
                  	"&month=" + " " +
                    "&starttime="+document.all.time.value+
                    "&endtime="+document.all.time.value+           
                    "&params="+params,'newwindow', 'height=1, width=1, top=2000, left=2000, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no');

    }

    function show()
    {
        
    	fm.action="./Show.jsp?define="+document.all.define.value+
    	                        "&calculate="+document.all.calculate.value+
    	                        "&file="+document.all.code.value+
    	                        "&edition="+document.all.edition.value+
    	                        "&month=" + " " +
    	                        "&starttime="+document.all.time.value+
    	                        "&endtime="+document.all.time.value;
          	location.href = fm.action;    	                        
  
    }  
      function initForm()
      {
      
      }
 

  </script>
  
</head>
<body  onload="initForm();" >

<form action="./calculate.jsp" method=post name=fm >
   <Table class= common>
      <TR class= common>
          <TD  class= title>
            定义单位
          </TD>  
          <TD  class= input>
            <Input class=common name=define value=<%=DefBranch%> >
          </TD>
      </TR>
      
      <TR class= common>
          <TD  class= title>
            计算单位
          </TD>  
          <TD  class= input>
            <Input class=common name=calculate value=<%=Branch%> >
          </TD>
      </TR>
            
      <TR  class= common> 
          <TD  class= title>
            报表代码
          </TD>  
          <TD class= title>
             <input class=common name=file value=<%=CodeName%>>
          </TD>

          <TD  class= input style="display: none">
            <Input class=common name=code value = <%=Code%> >
          </TD>
      </TR>
          
      <TR class= common>    
          <TD class= title>
            报表版别
          </TD>          
          <TD  class= input>
            <Input class=common name=edition  value = 20030401>
          </TD>
      </TR>
      
      <TR class = common>    
         <TD  class= title>
            报表时间
          </TD>          
          <TD  class= input>    
              <Input class="coolDatePicker" verify="报表时间|DATE" dateFormat="short" name=time value= <%=CurrentDate%> >
          </TD>
      </TR>
      <TR class= common>    
          <TD class= title>
            参数
          </TD>          
          <TD  class= input>
            <Input class=common name=param>
          </TD>
      </TR>
    </Table>   
            
  <Div id= divCmdButton style="display: ''">
     <!--<INPUT VALUE="查看" TYPE=button onclick="show()">-->
     <INPUT VALUE="下载" class=common TYPE=button onclick="down()">  
  </Div>     
</form>

</body>
</html>
