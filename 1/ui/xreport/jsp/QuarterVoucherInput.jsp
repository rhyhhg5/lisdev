
<html> 
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.xreport.bl.*" %>
<%@page import="com.sinosoft.xreport.util.*" %>
<%@ include file="init.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
    String DefBranch="";
    String Code = request.getParameter("Code");
    String CodeName = "";
    System.out.println(Code);
    String CurrentDate = PubFun.getCurrentDate();
    System.out.println(CurrentDate);

    String CurrentMonth = "";
    FDate fDate = new FDate();
    Date CurDate = fDate.getDate(CurrentDate);
    GregorianCalendar mCalendar = new GregorianCalendar();
    mCalendar.setTime(CurDate);
    int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的
    int Quarter = (Months - 1) / 3 + 1;  //计算当前季度
    System.out.println("Quarter : " + Quarter);
    int Years = mCalendar.get(Calendar.YEAR);
    System.out.println("Months : " + Months);	
    System.out.println("Years :" + Years);	    
    ReportMain reportMain=new ReportMain();
    Vector vRM=reportMain.query();

    Vector vecBranchId=new Vector();
    Vector vecReportName=new Vector();
    Vector vecReportId=new Vector();
    Vector vecReportEdition=new Vector();
    for(int i=0;i<vRM.size();i++) 
    {
        ReportMain report=(ReportMain)vRM.elementAt(i);
        if (report.getReportId().equals(Code)) {
            CodeName = report.getReportName();
            DefBranch= report.getBranchId();
            break;
        }
    }
           

%>


<head>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">

  <SCRIPT src="../../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../../common/javascript/MulLine.js"></SCRIPT>
 
  <LINK href="../../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../../common/css/mulLine.css" rel=stylesheet type=text/css>
  <LINK href="Project.css" rel=stylesheet type=text/css>
  <script language="javascript">
     function initForm()
     {

     }
 
    function submitForm()
    {
      if(document.all.time.value==""){
        var Quarter = document.all('quarter').value;
    	if (Quarter > 4) {
    	    alert("季度值输入非法！");
    	    return;
    	}  
    	
    	//得到季度的首日和尾日
    	var endmonth = Quarter * 3;
    	var firstmonth = endmonth - 2;
    	var firstday =  <%=Years%> + '-' + firstmonth + '-01';
    	var lastday = "";
    	if (Quarter == 1 || Quarter == 4) {
    	    lastday = <%=Years%> + '-' + endmonth + '-31';
    	} else {
    	    lastday = <%=Years%> + '-' + endmonth + '-30';
    	}    
    	document.all.time.value = firstday;
    	document.all.timeend.value = lastday;
      }	
      fm.submit();
    }  
  </script>
  
</head>
<body  onload="initForm();" >

<form action="./calculate.jsp" method=post name=fm >
   <Table class= common>
      <TR class= common>
          <TD  class= title>
            单位编码
          </TD>  
          <TD  class= input>
            <Input class=common name=branch value=<%=DefBranch%> >
          </TD>
      </TR>
      <TR  class= common> 
          <TD  class= title>
            报表代码
          </TD>  
          <TD class= title>
             <input class=common readonly name=CodeName value=<%=CodeName%>>
          </TD>

          <TD  class= input style="display: none">
            <Input class=common readonly name=code value = <%=Code%> >
          </TD>
      </TR>
          
      <TR class= common>    
          <TD class= title>
            报表版别
          </TD>          
          <TD  class= input>
            <Input class=common name=edition  readonly value = 20030401>
          </TD>
      </TR>
      
      <TR class = common>    
         <TD  class= title>
            报表时间(季度)
          </TD>          
          <TD  class= input>    
              <Input class=common  name= "quarter" value=<%=Quarter%>>
          </TD>
      </TR>
      <TR class= common>
          <TD  class= title colspan=3>
            当输入该日期范围时，以日期范围取数,如：输入2003-09-26，则强制提取2003-09-26-2003-12-26这个季度的数据。。
          </TD>          
      </tr>      
      <TR class= common> 
          <TD  class= title>
            报表日期
          </TD>   
       
          <TD class= common>
              <Input class="coolDatePicker" verify="报表时间|DATE" dateFormat="short"  name= "time">
          </TD> 
          <TD class= common style="display: none">
              <Input class=common  name= "timeend">
          </TD> 
      </TR>   
          
    </TR>
  </Table>   
            
  <Div id= divCmdButton style="display: ''">
     <INPUT VALUE="计算" class= common TYPE=button onclick="submitForm()">  
  </Div>     
</form>

</body>
</html>
