<%@page language="java" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="com.f1j.swing.JBook"%>
<%@page import="com.sinosoft.xreport.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<body>
<%/////////////////////上传报表定义格式文件//////////////////////////
%>

<%!
    void writeFile(String fileName, java.io.InputStream input) throws java.io.IOException
    {
        java.io.FileOutputStream output = new java.io.FileOutputStream(fileName);
        try
        {
            byte buffer[] = new byte[4096];
            int bytesRead;
            while (true)
            {
                bytesRead = input.read(buffer);
                if (bytesRead < 0)
                    break;
                if (bytesRead > 0)
                    output.write(buffer, 0, bytesRead);
            }
        }
        finally
        {
            if (output != null)
                output.close();
        }
    }
%>
<%
/**读取参数*/
Hashtable queryParameters = javax.servlet.http.HttpUtils.parseQueryString(request.getQueryString());
String fileParameters[] = (String[])queryParameters.get("file");
String strFileName=fileParameters[0];
String typeParameters[] = (String[])queryParameters.get("type");
String strType=typeParameters[0];

String fileName="";
String branch="";

/**参数检验*/
if(null==strType||"".equals(strType))
{
    out.println("err|parameter type should not be null");
    return;
}
if(null==strFileName||"".equals(strFileName))
{
    out.println("err|parameter file should not be null");
    return;
}

/**读取报表定义*/
if(strType.equals("define"))
{
    StringTokenizer token=new StringTokenizer(strFileName,SysConfig.REPORTJOINCHAR);
    if(token.hasMoreTokens())
        branch=token.nextElement().toString();
    fileName=SysConfig.FILEPATH
            +"define"
            +SysConfig.FILESEPARATOR
            +branch
            +SysConfig.FILESEPARATOR
            +strFileName;
}

/**读取报表数据*/
if(strType.equals("data"))
{
    StringTokenizer token=new StringTokenizer(strFileName,SysConfig.REPORTJOINCHAR);
    if(token.hasMoreTokens())
        branch=token.nextElement().toString();
    fileName=SysConfig.FILEPATH
            +"data"
            +SysConfig.FILESEPARATOR
            +branch
            +SysConfig.FILESEPARATOR
            +strFileName;
}

/**读取配置文件*/
if(strType.equals("conf"))
{
    fileName=SysConfig.FILEPATH
            +"conf"
            +SysConfig.FILESEPARATOR
            +strFileName;
}

/**读取report配置文件*/
if(strType.equals("style"))
{
    fileName=SysConfig.FILEPATH
            +"style"
            +SysConfig.FILESEPARATOR
            +strFileName;
}

try
{
    File file=new File(fileName);
    if(!file.getParentFile().exists())
    {
        file.getParentFile().mkdirs();
    }
    writeFile(fileName,request.getInputStream());
}
catch (IOException e)
{
    e.printStackTrace();
}
catch(Exception e)
{
    e.printStackTrace();
}
%>
</body>
</html>