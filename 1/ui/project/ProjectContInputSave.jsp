<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.projecttb.*"%>

<%
	
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String CurrentDate = PubFun.getCurrentDate();
	String CurrentTime = PubFun.getCurrentTime();
	
	//输入参数
	LCProjectContSet tLCProjectContSet = new LCProjectContSet();
	ProjectContUI mProjectContUI = new ProjectContUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	
	String mProjectNo = request.getParameter("ProjectNo");
	String mProjectYear = request.getParameter("ProjectYear");
	String tChk[] = request.getParameterValues("InpProjectContGridChk");
	String tGrpContNo[] = request.getParameterValues("ProjectContGrid1");
	String tPrtNo[] = request.getParameterValues("ProjectContGrid2");
	String tYear[] = request.getParameterValues("ProjectContGrid7");

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	System.out.println(transact);

	if(mProjectNo == null || "".equals(mProjectNo))
	{
		FlagStr = "Fail";
		Content = "获取项目编码失败！";
	}
	System.out.println("项目编号:" + mProjectNo);
	if("INSERT||MAIN".equals(transact)){
		if(mProjectYear == null || "".equals(mProjectYear))
		{
			FlagStr = "Fail";
			Content = "获取项目年度失败！";
		}
	}
	System.out.println("项目年度:" + mProjectYear);
	if(!"Fail".equals(FlagStr)){
		int grouppolCount = tGrpContNo.length;
		for (int i = 0; i < grouppolCount; i++)
		{
			LCProjectContSchema tLCProjectContSchema = new LCProjectContSchema();
			if( tPrtNo[i] != null && tChk[i].equals( "1" ))
			{
				tLCProjectContSchema.setPrtno(tPrtNo[i]);
				tLCProjectContSchema.setProjectNo(mProjectNo);
				if("DELETE||MAIN".equals(transact)){
					tLCProjectContSchema.setProjectYear(tYear[i]);
				}else{
					tLCProjectContSchema.setProjectYear(mProjectYear);
				}
				tLCProjectContSchema.setManageCom(tG.ManageCom);
				tLCProjectContSchema.setOperator(tG.Operator);
				tLCProjectContSchema.setMakeDate(CurrentDate);
				tLCProjectContSchema.setMakeTime(CurrentTime);
				tLCProjectContSchema.setModifyDate(CurrentDate);
				tLCProjectContSchema.setModifyTime(CurrentTime);
				
				tLCProjectContSet.add(tLCProjectContSchema);
			}
		}
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.addElement(tLCProjectContSet);
		try {
			mProjectContUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = mProjectContUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mProjectNo%>");
</script>
</html>