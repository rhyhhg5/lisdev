
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}

//下载
function downLoad() {
	if(fm.querySql.value != null && fm.querySql.value != "" && ProjectGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "ProjectReportQueryDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
function queryClick() {
	initProjectGrid();
	var strSQL = "select lcpi.managecom,lcpi.projectno,lcpi.projectname,"
			  // + " (select codename from ldcode where codetype = 'projecttype1' and code = lcpi.projecttype), "
			   + " lpc.projectyear,lgc.grpcontno,lgp.riskcode "
			   + " from LCProjectCont lpc "
			   + " inner join LCProjectInfo lcpi on lcpi.projectno = lpc.projectno "
			   + " inner join LCGrpCont lgc on lgc.prtno = lpc.prtno "
			   + " inner join LCGrppol lgp on lgp.grpcontno = lgc.grpcontno "
			   + " where lcpi.state = '05' and lgc.appflag = '1' "
			   + getWherePart('lcpi.ManageCom', 'ManageCom','like')
		       + getWherePart('lcpi.CreateDate', 'StartDate','>=')
		       + getWherePart('lcpi.CreateDate', 'EndDate','<=')
		       + getWherePart('lcpi.ProjectNo', 'ProjectNo')
		      // + getWherePart('lcpi.ProjectType', 'ProjectType')
		       + getWherePart('lcpi.ProjectName', 'ProjectName','like')
		       + " order by lcpi.managecom,lcpi.projectno,lpc.projectyear,lgc.grpcontno ";
	turnPage1.queryModal(strSQL, ProjectGrid);
	showCodeName();	//显示编码代表的汉字
	fm.querySql.value = strSQL;
}

