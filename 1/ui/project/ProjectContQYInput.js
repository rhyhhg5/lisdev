
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function queryClick(){
	initProjectGrid();
	var strSql = "select ProjectNo, ProjectName,  CreateDate, ManageCom "
	           + "from LCProjectInfo lcpi where 1=1 and state = '05' "
		       + getWherePart('ManageCom', 'ManageCom','like')
		       + getWherePart('CreateDate', 'StartDate','>=')
		       + getWherePart('CreateDate', 'EndDate','<=')
		       + getWherePart('ProjectNo', 'ProjectNo')
		       + getWherePart('ProjectType', 'ProjectType')
		       + getWherePart('ProjectName', 'ProjectName','like');
	turnPage.queryModal(strSql, ProjectGrid);
}
function getQueryResult(){
	var mSelNo = ProjectGrid.getSelNo();
	var tProjectNo = ProjectGrid.getRowColData(mSelNo - 1, 1);
	if (tProjectNo == null || tProjectNo == "") {
		alert("获取项目编码失败！");
		return false;
	}
	var tSQL = "select ProjectYear,InsuredPeoples,AveragePrem,case when PeoplesNumber = -1 then '' else char(PeoplesNumber) end,Bond,StopLine,ExpecteRate,ComplexRate,"
    			 + "case when BalanceTermFlag = 'Y' then '是' else '否' end,case when PremCoverTermFlag = 'Y' then '是' else '否' end,CountyCount "
                 + "from LCProjectYearInfo "
	             + "where ProjectNo = '"+tProjectNo+"' order by ProjectYear ";
	turnPage1.queryModal(tSQL, ProjectYearGrid);
}
function addCont(){
	if(!beforeAddAndSub()){
		return false;
	}
  	var tSQL = "select ProjectNo,ProjectYear from LCProjectCont where PrtNo = '"+tPrtNo+"' ";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if(strQueryResult){
		alert("该保单已归属在项目"+strQueryResult[0][0]+",年度"+strQueryResult[0][1]+"下，不能再次归属！");
		return false;
	}
    fm.fmtransact.value = "INSERT||MAIN";
	var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
    fm.submit(); 
}
function subCont(){
	//if(!beforeAddAndSub()){
		//return false;
	//}
	var tSQL = "select ProjectNo,ProjectYear from LCProjectCont where PrtNo = '"+tPrtNo+"' ";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("该保单未归属到项目中！");
		return false;
		
	}
	if(!confirm("取消保单的归属，是否继续？"))
        return false;
    fm.ProjectNo1.value = fm.ProjectNo2.value;
	fm.fmtransact.value = "DELETE||MAIN";
	var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}
function cancel(){
	top.close();
}
function afterSubmit(FlagStr, content, cOperate) {
	showInfo.close();
	if (FlagStr == "Fail"){
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	initForm();
}
function beforeAddAndSub(){
	var checkFlag = 0;
	var tManageCom = "";
	for (i=0; i<ProjectGrid.mulLineCount; i++)
	{
	  if (ProjectGrid.getSelNo(i))
	  {
	    checkFlag = ProjectGrid.getSelNo();
	    break;
	  }
	}
	if(!checkFlag){
		alert("请选择项目！");
	  	return false;
	}
	var	tProjectNo = ProjectGrid.getRowColData(checkFlag - 1, 1);
	tManageCom = ProjectGrid.getRowColData(checkFlag - 1,4);
  	if(tProjectNo == null || tProjectNo == "" || tProjectNo =="null"){
  		alert("获取项目编码失败！");
  		return false;
  	}
	checkFlag = 0;
	for (i=0; i<ProjectYearGrid.mulLineCount; i++)
	{
	  if (ProjectYearGrid.getSelNo(i))
	  {
	    checkFlag = ProjectYearGrid.getSelNo();
	    break;
	  }
	}
	if(!checkFlag){
		alert("请选择保单年度！");
	  	return false;
	}
  	var	tProjectYear = ProjectYearGrid.getRowColData(checkFlag - 1, 1);
  	if(tProjectYear == null || tProjectYear == "" || tProjectYear =="null"){
  		alert("获取项目下年度信息失败！");
  		return false;
  	}
  	var tSQL = "select managecom from lcgrpcont where PrtNo = '"+tPrtNo+"' ";
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("获取保单管理机构失败！");
		return false;
	}
	if(tManageCom.substr(0,4) != strQueryResult[0][0].substr(0,4)){
		alert("保单的管理机构【"+strQueryResult[0][0]+"】与项目的归属机构【"+tManageCom+"】不一致，无法归属！");
		return false;
	}
  	fm.ProjectNo1.value = tProjectNo;
    fm.ProjectYear.value = tProjectYear;
  	return true;
}
function initContState(){
	var tSQL = "select lpc.ProjectNo,lpc.ProjectYear,(select projectname from lcprojectinfo where projectno = lpc.projectno) from LCProjectCont lpc where lpc.PrtNo = '"+tPrtNo+"' ";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if(strQueryResult){
		fm.ContState.value = "已归属";
		fm.ProjectNo2.value = strQueryResult[0][0];
		fm.ProjectYear2.value = strQueryResult[0][1];
		fm.ProjectName2.value = strQueryResult[0][2];
	}else{
		fm.ContState.value = "未归属";
		fm.ProjectNo2.value = "";
		fm.ProjectYear2.value = "";
		fm.ProjectName2.value = "";
	}
}