
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
function cancel() {
	top.close();
}
	
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function initProjectInfo(){
	if(tProjectNo == null || tProjectNo == ""){
		alert("项目审核时，获取项目编码失败！");
		return false;
	}
	var tSQL = "select ProjectNo,ProjectName,Province,City,County,"
	         + "ProjectType,Grade,PersonType,Limit,StartDate,EndDate, "
	         + "(select codename from ldcode1 where codetype = 'province' and code = Province and code1 = '0' ),"
	         + "(select codename from ldcode1 where codetype = 'city' and code = City and code1 = Province ),"
	         + "(select codename from ldcode1 where codetype = 'county' and code = County and code1 = City ), "
	         + "ManageCom "
	         + "from LCProjectInfo "
	         +" where ProjectNo = '"+tProjectNo+"'";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("项目审核时，根据项目编码获取项目信息失败！");
		return false;
	}
	fm.all('ProjectNo').value = strQueryResult[0][0];
    fm.all('ProjectName').value = strQueryResult[0][1];
    fm.all('Province').value = strQueryResult[0][2];
    fm.all('City').value = strQueryResult[0][3];
    fm.all('County').value = strQueryResult[0][4];
    //fm.all('ProjectType').value = strQueryResult[0][5];
    fm.all('Grade').value = strQueryResult[0][6];
    fm.all('PersonType').value = strQueryResult[0][7];
    fm.all('Limit').value = strQueryResult[0][8];
    fm.all('StartDate').value = strQueryResult[0][9];
    fm.all('EndDate').value = strQueryResult[0][10];
    fm.all('ProvinceName').value = strQueryResult[0][11];
    fm.all('CityName').value = strQueryResult[0][12];
    fm.all('CountyName').value = strQueryResult[0][13];
    fm.all('ManageCom').value = strQueryResult[0][14];
    
    
    var tSQLYear = "select ProjectYear,InsuredPeoples,AveragePrem,case when PeoplesNumber = -1 then '' else char(PeoplesNumber) end,Bond,StopLine,ExpecteRate,ComplexRate,"
    			 + "case when BalanceTermFlag = 'Y' then '是' when BalanceTermFlag = 'N' then '否' else '' end,"
			 	 + "case when PremCoverTermFlag = 'Y' then '是' when PremCoverTermFlag = 'N' then '否' else '' end, "
			 	 + "CountyCount, "
			 	 + "case when CoInsuranceFlag = 'Y' then '是' when CoInsuranceFlag = 'N' then '否' else '' end "
                 + "from LCProjectYearInfo "
	             + "where ProjectNo = '"+tProjectNo+"' order by ProjectYear ";
	
	//执行查询并返回结果
	var strYearResult = easyExecSql(tSQLYear);
	if(!strYearResult){
		alert("项目审核时，根据项目编码获取首年度信息失败！");
		return false;
	}
	turnPage1.queryModal(tSQLYear, ProjectYearGrid);	
}
function Medical(){
	if(tProjectNo == null || tProjectNo == "" || tProjectNo == "null"){
		alert("获取项目编码失败，无法查看相关业务资料！");
		return false;
	}
	showInfo = window.open("./ProjectMedicalMain.jsp?ProjectNo="+tProjectNo+"&LookFlag=1");
}
function Allow(){
	if(fm.AuditOpinion.value == null || fm.AuditOpinion.value == ""){
		alert("请填写审核意见!");
		return false;
	}
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "ALLOW";
	fm.submit(); //提交
	return true;
}
function NoAllow(){
	if(fm.AuditOpinion.value == null || fm.AuditOpinion.value == ""){
		alert("请填写审核意见!");
		return false;
	}
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "NOALLOW";
	fm.submit(); //提交
	return true;
}
function afterSubmit(FlagStr,content,aProjectNo) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}
function queryTerm(){
	var checkFlag = 0;
	for (i=0; i<ProjectYearGrid.mulLineCount; i++)
	{
	  if (ProjectYearGrid.getSelNo(i))
	  {
	    checkFlag = ProjectYearGrid.getSelNo();
	    break;
	  }
	}
	if(checkFlag){
		var	tProjectYear = ProjectYearGrid.getRowColData(checkFlag - 1, 1);
		if(tProjectYear == null || tProjectYear == "" || tProjectYear =="null"){
			alert("获取年度失败！");
			return false;
		}
		showInfo = window.open("./ProjectYearTermInput.jsp?ProjectNo=" + tProjectNo+"&ProjectYear="+tProjectYear);
	}else{
		alert("请选择年度！");
		return false;
	}
}