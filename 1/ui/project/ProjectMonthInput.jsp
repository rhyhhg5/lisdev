<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
		String tProjectNo = request.getParameter("ProjectNo");
		String tLookFlag = request.getParameter("LookFlag");
		String tProjectYear = request.getParameter("ProjectYear"); 
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectMonthInput.js"></script>
		<%@include file="ProjectMonthInputInit.jsp"%>

		<script>
	  	var ManageCom = "<%=tGI.ManageCom%>"; 
	  	var tProjectNo = "<%=tProjectNo%>";
	  	var tLookFlag = "<%=tLookFlag%>";
	  	var tProjectYear = "<%=tProjectYear%>";
  		</script>

	</head>
	<body onload="initForm();initElementtype();">
		<form action="./ProjectMonthInputSave.jsp" method=post name=fm
			target="fraSubmit">
			<table>
				<tr>
					<td class=titleImg>
						项目信息
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目编号
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectNo readonly>
					</TD>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectName readonly>
					</TD>
					<TD class=title>
						建项日期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="CreateDate" readonly>
					</TD>
				</TR>
			</table>
			<div id = "divProjectYear1" style="display:'none'">
				<table class=common align='center' >
					<TR class=common>
						<TD class=title>
							项目年度
						</TD>
						<TD class=input>
							<Input class=codeno name=ProjectYear readonly=true><input class=codename name=ProjectYearName readonly=true>
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>
						</TD>
					</TR>
				</table>
			</div>
			<br />
			<table>
				<tr>
					<td class=titleImg>
						费用清单
					</td>
				</tr>
			</table>

			<Div id="divProjectMonthGrid1" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanProjectMonthGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">

				<table>
					<tr align=center>
						<td class=button>
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
						</td>
					</tr>
				</table>
			</Div>
			<br />
			<!-- 
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目年度
					</TD>
					<TD class=input>
						<Input class=codeno name=ProjectYear verify="年度|notnull&code:projectyear"	ondblclick="return showCodeList('projectyear1',[this,ProjectYearName],[0,1],null,fm.ProjectNo.value,'ProjectNo',1);" onkeyup="return showCodeListKey('projectyear1',[this,ProjectYearName],[0,1],null,fm.ProjectNo.value,'ProjectNo',1);"><input class=codename name=ProjectYearName readonly=true elementtype=nacessary>
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
				</TR>

				<TR class=common>
					<TD class=title>
						项目月度
					</TD>
					<TD class=input>
						<Input class=codeno name="ProjectMonth" verify="月度|notnull" readonly ondblclick="return showCodeList('projectmonth',[this,ProjectMonthName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('projectmonth',[this,ProjectMonthName],[0,1],null,null,null,1);"><input class=codename name=ProjectMonthName readonly=true elementtype=nacessary>
					</TD>
					<TD class=title>
						费用类型
					</TD>
					<TD class=input>
						<Input class=codeno name="ProjectType" verify="费用类型|notnull&code:projecttype2" ondblclick="return showCodeList('projecttype2',[this,ProjectTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('projecttype2',[this,ProjectTypeName],[0,1],null,null,null,1);"><input class=codename name=ProjectTypeName readonly=true	elementtype=nacessary>
					</TD>
					<TD class=title>
						费用值
					</TD>
					<TD class=input>
						<Input class='common' name="Cost" verify="费用值|notnull&len<=100" elementtype=nacessary>
					</TD>
				</TR>
			</table>
			 -->
			 <Div id="divProjectMonthGrid2" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanProjectMGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<br />
			<table>
				<input type=hidden id="fmtransact" name="fmtransact">
				<input type=hidden id="MonthFeeNo" name="MonthFeeNo">
				<input type=hidden id="State" name="State">
				<td class=button>
					<input type="button" class=cssButton value=" 添加信息  " id = "AddID" name = "AddID" onclick="submitForm()">
					<input type="button" class=cssButton value=" 维护信息  " id = "UpdateID" name = "UpdateID" onclick="updateForm()">
					<input type="button" class=cssButton value=" 删除信息  " id = "DeleteID" name = "DeleteID" onclick="deleteClick()">
					<input type="button" class=cssButton value="  信息送审  " id = "SendUWID" name = "SendUWID" onclick="sendUW()">
					<input type="button" class=cssButton value=" 查看具体审核意见 " id=QUERY onclick="queryUWYJ()">
					<input type="button" class=cssButton value=" 退  出 " onclick="cancel()">
				</td>
			</table>
			<table>
				<br/>
				<tr class=common>
					<td>
					<font color = red  size = 2>
						录入说明：<br/>
						1、费用值：单位为元，可保留两位小数。<br>
					</td>
				</tr>
			</table>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
