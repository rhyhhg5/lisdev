
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}

function queryClick(){
	initProjectGrid();
	var tWh = "";
	if("1" == fm.QueryType.value){
		tWh = " and lpu.Serialno in (select max(a.Serialno) from LCProjectUW a group by a.projectno) ";
	}
	var strSql = "select lpt.ProjectNo, lpt.ProjectName,lpt.CreateDate,lpt.ManageCom, "
			   //+ " (select codename from ldcode where codetype = 'projecttype1' and code = lpt.ProjectType), "
	           + " (select codename from ldcode where codetype = 'projectauditflag' and code = lpu.AuditFlag),"
	           + " case when lpu.Conclusion is null or lpu.Conclusion ='' then '待审核' else (select codename from ldcode where codetype = 'projectconclusion' and code = lpu.Conclusion) end,"
	           //+ " lpu.AuditOpinion,"
	           + " lpu.UWDate,lpu.serialno "
	           + " from LCProjectUW lpu "
	           + " inner join LCProjectTrace lpt on lpu.ProjectSerialNo = lpt.ProjectSerialNo "
	           + " where 1=1 "
		       + getWherePart('lpt.ManageCom', 'ManageCom','like')
		       + getWherePart('CreateDate', 'StartDate','>=')
		       + getWherePart('CreateDate', 'EndDate','<=')
		       + getWherePart('lpu.ProjectNo', 'ProjectNo')
		      // + getWherePart('ProjectType', 'ProjectType')
		       + getWherePart('ProjectName', 'ProjectName','like')
		       + tWh 
		       + " order by lpt.CreateDate desc,lpt.projectno desc,lpu.uwdate desc,lpu.uwtime desc";
		       				   
	turnPage1.queryModal(strSql, ProjectGrid);
}
function queryUWYJ(){
	var checkFlag = 0;
	for (i=0; i<ProjectGrid.mulLineCount; i++)
	{
	  if (ProjectGrid.getSelNo(i))
	  {
	    checkFlag = ProjectGrid.getSelNo();
	    break;
	  }
	}
	if(checkFlag){
		var	tSerialNo = ProjectGrid.getRowColData(checkFlag - 1, 8);
		var	tProjectNo = ProjectGrid.getRowColData(checkFlag - 1, 1);
		if(tSerialNo == null || tSerialNo == "" || tSerialNo =="null"){
			alert("获取审核信息失败！");
			return false;
		}
		if(tProjectNo == null || tProjectNo == "" || tProjectNo =="null"){
			alert("获取审核信息失败！");
			return false;
		}
		showInfo = window.open("./ProjectQueryUWInput.jsp?ProjectNo="+tProjectNo+"&SerialNo="+tSerialNo+"&Flag=1");
	}else{
		alert("请选择需要查看的项目信息！");
		return false;
	}
}