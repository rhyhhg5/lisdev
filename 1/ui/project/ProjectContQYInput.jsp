<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	String tPrtNo = request.getParameter("PrtNo");
	 %>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectContQYInput.js"></script>
		<%@include file="ProjectContQYInputInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>";
  		var tPrtNo = "<%=tPrtNo%>"; 
 	 	</script>

	</head>
	<body onload="initForm();initElementtype();">
		<form action="./ProjectContQYInputSave.jsp" method=post name=fm target="fraSubmit">
			<br/>
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divProject2);">
					</td>
					<td class=titleImg>
						保单归属信息
					</td>
				</tr>
			</table>
			<div id = "divProject2">
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						保单归属状态
					</TD>
					<TD class=input>
						<Input type="text"  class=readonly  name=ContState  readonly>
					</TD>
					<TD class=title>
						保单归属项目编码
					</TD>
					<TD class=input>
						<Input type="text"  class=readonly name=ProjectNo2  readonly>
					</TD>
					<TD class=title>
						保单归属年度
					</TD>
					<TD class=input>
						<Input type="text"  class=readonly  name=ProjectYear2  readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						保单归属项目名称
					</TD>
					<TD class=input colspan = '5'>
						<Input type="text"  class=readonly4  name=ProjectName2  readonly>
					</TD>
				</TR>
			</table>
			</div>
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divProject1);">
					</td>
					<td class=titleImg>
						查询条件
					</td>
				</tr>
			</table>
			<div id = "divProject1">
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
					</TD>
					<TD class=title>
						建项日期起
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="StartDate" readonly>
					</TD>
					<TD class=title>
						建项日期止
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="EndDate" readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						项目编号
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectNo verify="项目编号|notnull&len<=100">
					</TD>
					<TD class=title>
						项目类型
					</TD>
					<TD class=input>
						<Input class=codeno name=ProjectType verify="项目类型|code:projecttype1"
							ondblclick="return showCodeList('projecttype1',[this,ProjectTypeName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('projecttype1',[this,ProjectTypeName],[0,1],null,null,null,1);"><input class=codename name=ProjectTypeName readonly=true>

					</TD>

					<TD class=title>
						项目名称
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectName 	verify="项目名称|notnull&len<=100">
					</TD>
				</TR>
			</table>
			</div>
			<table>
				<td class=button width="10%" align=left>
					<input type="button" class=cssButton value="查  询" id=QUERY onclick="queryClick()">
				</td>
			</table>
			<table>
				<tr>
					<td class=titleImg>
						项目清单
					</td>
				</tr>
			</table>

			<Div id="divspanProjectGrid" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanProjectGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: '' ">
				<table>
					<tr align=center>
						<td class=button>
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
						</td>
					</tr>
				</table>
			</Div>
			<table>
				<tr>
					<td class=titleImg>
						项目年度
					</td>
				</tr>
			</table>

			<Div id="divProjectYearGrid1" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanProjectYearGrid"></span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPageYear" align=center style="display: '' ">
				<table>
					<tr align=center>
						<td class=button>
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</Div>
			<table>
				<td class=button>
					<input type="button" class=cssButton value=" 加入项目  " onclick="addCont()">
					<input type="button" class=cssButton value=" 移出项目  " onclick="subCont()">
					<input type="button" class=cssButton value=" 返  回 " onclick="cancel()">
				</td>
			</table>
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden id="PrtNo" name="PrtNo">
			<input type=hidden id="ProjectNo1" name="ProjectNo1">
			<input type=hidden id="ProjectYear" name="ProjectYear">
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
