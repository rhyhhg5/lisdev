<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    

<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
  
%>
<script language="JavaScript">
function initInpBox()
{ 
	initProject();
	initProjectMedicalGrid();
	divPage.style.display="";
	showAllCodeName();
	if("1" == tLookFlag){
		fm.all('DeleteID').style.display = "none";
		fm.all('uploadID').style.display = "none";
	}
}

var ProjectMedicalGrid;

function initProjectMedicalGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="文件编号";         	  //列名
    iArray[1][1]="50px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="文件名称";         	
    iArray[2][1]="100px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=1;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="文件说明";         	  //列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="上传日期";      //列名
    iArray[4][1]="50px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许  
    
    
    iArray[5]=new Array();
    iArray[5][0]="上传用户";      //列名
    iArray[5][1]="50px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=1; 
     
    ProjectMedicalGrid = new MulLineEnter("fm", "ProjectMedicalGrid"); 
    //设置Grid属性
    ProjectMedicalGrid.mulLineCount = 10;
    ProjectMedicalGrid.displayTitle = 1;
    ProjectMedicalGrid.locked = 1;
    ProjectMedicalGrid.canSel = 1;
    ProjectMedicalGrid.canChk = 0;
    ProjectMedicalGrid.hiddenSubtraction = 1;
    ProjectMedicalGrid.hiddenPlus = 1;
    ProjectMedicalGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
                                      
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
</script>
