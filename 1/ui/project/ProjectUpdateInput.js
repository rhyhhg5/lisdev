var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		} catch (ex) {
			showInfo = null;
		}
	}
}
function beforeSub(){
	//从mulline中取值
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == "null"){
		alert("管理机构不可以为空！");
		fm.ManageCom.focus();
		return false;
	}
	if(fm.ManageCom.value.length != 8){
		alert("管理机构必须为8位机构！");
		fm.ManageCom.focus();
		return false;
	}
	var tGSSQL = "select 1 from lcprojectcont where projectno = '"+fm.all('ProjectNo').value+"' ";
	var strGSResult = easyExecSql(tGSSQL);
	if(strGSResult && (fm.ManageCom.value != fm.GSManageCom.value)){
		alert("该项目下存在已归属的保单，不允许修改项目归属机构！");
		return false;
	}
	/*
	if(!isInteger(fm.CountyCount.value)){
		alert("覆盖市县数必须为非负整数！");
		fm.CountyCount.focus();
		return false;
	}
	*/
	if(!isNumeric(fm.Limit.value)){
		alert("合作期限必须为数字！");
		fm.Limit.focus();
		return false;
	}
	var tPSQL = "select 1 from ldcode1 where codetype = 'province' and code1 = '0' and code = '"+fm.Province.value+"' ";
	var strPResult = easyExecSql(tPSQL);
	if(!strPResult){
		alert("项目属地（省）编码与配置不符，请核查！");
		return false;
	}
	if(fm.Grade.value == '03' || fm.Grade.value == '04' || fm.Grade.value == '05'){
		if(fm.City.value == null || fm.City.value == '' || fm.City.value == 'null'){
			alert("项目属地（市）不能为空！");
			return false;
		}
		var tCSQL = "select 1 from ldcode1 where codetype = 'city' and code1 = '"+fm.Province.value+"' and code = '"+fm.City.value+"' ";
		var strCResult = easyExecSql(tCSQL);
		if(!strCResult){
			alert("项目属地（市）编码与配置不符，或与对应的省份不符，请核查！");
			return false;
		}
		if(fm.Grade.value == '05'){
			if(fm.County.value == null || fm.County.value == '' || fm.County.value == 'null'){
				alert("项目属地（区县）不能为空！");
				return false;
			}
			var tCCSQL = "select 1 from ldcode1 where codetype = 'county' and code1 = '"+fm.City.value+"' and code = '"+fm.County.value+"' ";
			var strCCResult = easyExecSql(tCCSQL);
			if(!strCCResult){
				alert("项目属地（区县）编码与配置不符，或与对应的市级编码不符，请核查！");
				return false;
			}
		}
	}
	if(!checkProjectName()){
		return false;
	}
	return true;
}
function returnParent() {
	var arrReturn = new Array();
	var tSel = ProjectUW.getSelNo();

	if (tSel == 0 || tSel == null) {
		alert("请先选择一条记录。");
		return false;
	}
	return true;
}

function returnParentone() {
	top.close();
}

function saveInfo(){
	if (verifyInput() == false) {
		return false;
	}
	if(!beforeSub()){
		return false;
	}
	var showStr = "正在保存数据，请不要修改页面上的内容";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./ProjectUpdateInputSave.jsp";
	fm.submit(); //提交
	return true;
}

function queryMedic()
{
	showInfo = window.open("./ProjectMedicalMain.jsp?ProjectNo="+tProjectNo);
}

function afterSubmit(FlagStr,content,aProjectNo) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	fm.all('ProjectNo').value  = aProjectNo;
	tProjectNo = aProjectNo;
}
function initProjectInfo(){
	if(tProjectNo == null || tProjectNo == ""){
		alert("修改操作时，获取项目编码失败！");
		return false;
	}
	var tSQL = "select ProjectNo,ProjectName,Province,City,County,"
	         + "ProjectType,Grade,PersonType,Limit,StartDate,EndDate, "
	         + "(select codename from ldcode1 where codetype = 'province' and code = Province and code1 = '0' ),"
	         + "(select codename from ldcode1 where codetype = 'city' and code = City and code1 = Province ),"
	         + "(select codename from ldcode1 where codetype = 'county' and code = County and code1 = City ), "
	         + "ManageCom "
	         + "from LCProjectInfo "
	         +" where ProjectNo = '"+tProjectNo+"'";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	//alert(strQueryResult);
	if(!strQueryResult){
		alert("修改操作时，根据项目编码获取项目信息失败！");
		return false;
	}
	fm.all('ProjectNo').value = strQueryResult[0][0];
    fm.all('ProjectName').value = strQueryResult[0][1];
    fm.all('Province').value = strQueryResult[0][2];
    fm.all('City').value = strQueryResult[0][3];
    fm.all('County').value = strQueryResult[0][4];
    //fm.all('ProjectType').value = strQueryResult[0][5];
    fm.all('Grade').value = strQueryResult[0][6];
    fm.all('PersonType').value = strQueryResult[0][7];
    fm.all('Limit').value = strQueryResult[0][8];
    fm.all('StartDate').value = strQueryResult[0][9];
    fm.all('EndDate').value = strQueryResult[0][10];
    fm.all('ProvinceName').value = strQueryResult[0][11];
    fm.all('CityName').value = strQueryResult[0][12];
    fm.all('CountyName').value = strQueryResult[0][13];
    fm.all('ManageCom').value = strQueryResult[0][14];
    fm.all('GSManageCom').value = strQueryResult[0][14];
    
    var tSQLYear = "select ProjectYear,InsuredPeoples,AveragePrem,case when PeoplesNumber = -1 then '' else char(PeoplesNumber) end,Bond,StopLine,ExpecteRate,ComplexRate, "
    			 + "case when BalanceTermFlag = 'Y' then '是' when BalanceTermFlag = 'N' then '否' else '' end,"
			 	 + "case when PremCoverTermFlag = 'Y' then '是' when PremCoverTermFlag = 'N' then '否' else '' end, "
			 	 + "CountyCount, "
			 	 + "case when CoInsuranceFlag = 'Y' then '是' when CoInsuranceFlag = 'N' then '否' else '' end "
    			 + "from LCProjectYearInfo "
	         	 + "where ProjectNo = '"+tProjectNo+"' ";    
    turnPage1.queryModal(tSQLYear,ProjectUW);	  
}
function queryTerm(){
	var checkFlag = 0;
	for (i=0; i<ProjectUW.mulLineCount; i++)
	{
	  if (ProjectUW.getSelNo(i))
	  {
	    checkFlag = ProjectUW.getSelNo();
	    break;
	  }
	}
	if(checkFlag){
		var	tProjectYear = ProjectUW.getRowColData(checkFlag - 1, 1);
		if(tProjectYear == null || tProjectYear == "" || tProjectYear =="null"){
			alert("获取年度失败！");
			return false;
		}
		showInfo = window.open("./ProjectYearTermInput.jsp?ProjectNo=" + tProjectNo+"&ProjectYear="+tProjectYear);
	}else{
		alert("请选择年度！");
		return false;
	}
}
function checkProjectName(){
	var tSQL = "select ProjectNo from LCProjectInfo where ProjectNo != '"+fm.ProjectNo.value+"' and  ProjectName = '"+fm.ProjectName.value+"' ";
	var tArrResult = easyExecSql(tSQL);
	if(tArrResult){
		alert("该项目名称已在项目编码为["+tArrResult[0][0]+"]的项目中存在，请核查！");
		return false;
	}
	return true;
}