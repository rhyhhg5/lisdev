
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function beforeSub(){
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == "null"){
		alert("管理机构不可以为空！");
		fm.ManageCom.focus();
		return false;
	}
	if(fm.ManageCom.value.length != 8){
		alert("管理机构必须为8位机构！");
		fm.ManageCom.focus();
		return false;
	}
	if(!isInteger(fm.CountyCount.value)){
		alert("覆盖市县数必须为非负整数！");
		fm.CountyCount.focus();
		return false;
	}
	if(!isNumeric(fm.Limit.value)){
		alert("合作期限必须为数字！");
		fm.Limit.focus();
		return false;
	}
	if(!isInteger(fm.InsuredPeoples.value)){
		alert("被保人人数必须为非负整数！");
		fm.InsuredPeoples.focus();
		return false;
	}
	/*
	if(!isNumeric(fm.AveragePrem.value)){
		alert("人均保费必须为数字！");
		fm.AveragePrem.focus();
		return false;
	}
	var tStrs = new Array();
	var tAveragePrem = parseFloat(fm.AveragePrem.value)+"";  
	tStrs = tAveragePrem.split(".");
	if(tStrs.length>1){
		if(tStrs[1].length>2){
			alert("人均保费最多保留两位小数！");
			fm.AveragePrem.focus();
			return false;
		}
	}
	*/
	if(fm.Bond.value != null && fm.Bond.value != ""){
		if(!isNumeric(fm.Bond.value)){
			alert("保证金必须为数字！");
			fm.Bond.focus();
			return false;
		}
	}
	tStrs = new Array();
	var tBond = parseFloat(fm.Bond.value)+"";  
	tStrs = tBond.split(".");
	if(tStrs.length>1){
		if(tStrs[1].length>2){
			alert("保证金最多保留两位小数！");
			fm.Bond.focus();
			return false;
		}
	}
	if(fm.PeoplesNumber.value != null && fm.PeoplesNumber.value != "" && fm.PeoplesNumber.value != "null"){
		if(!isInteger(fm.PeoplesNumber.value)){
			alert("联合办公人力数必须为非负整数！");
			fm.PeoplesNumber.focus();
			return false;
		}
	}
	if(!isNumeric(fm.ExpecteRate.value)){
		alert("预计赔付率必须为数字！");
		fm.ExpecteRate.focus();
		return false;
	}
	tStrs = new Array();
	var tExpecteRate = parseFloat(fm.ExpecteRate.value)+"";  
	tStrs = tExpecteRate.split(".");
	if(tStrs.length>1){
		if(tStrs[1].length>6){
			alert("预计赔付率最多保留六位小数！");
			fm.ExpecteRate.focus();
			return false;
		}
	}
	var tPSQL = "select 1 from ldcode1 where codetype = 'province' and code1 = '0' and code = '"+fm.Province.value+"' ";
	var strPResult = easyExecSql(tPSQL);
	if(!strPResult){
		alert("项目属地（省）编码与配置不符，请核查！");
		return false;
	}
	if(fm.Grade.value == '03' || fm.Grade.value == '04' || fm.Grade.value == '05'){
		if(fm.City.value == null || fm.City.value == '' || fm.City.value == 'null'){
			alert("项目属地（市）不能为空！");
			return false;
		}
		var tCSQL = "select 1 from ldcode1 where codetype = 'city' and code1 = '"+fm.Province.value+"' and code = '"+fm.City.value+"' ";
		var strCResult = easyExecSql(tCSQL);
		if(!strCResult){
			alert("项目属地（市）编码与配置不符，或与对应的省份不符，请核查！");
			return false;
		}
		if(fm.Grade.value == '05'){
			if(fm.County.value == null || fm.County.value == '' || fm.County.value == 'null'){
				alert("项目属地（区县）不能为空！");
				return false;
			}
			var tCCSQL = "select 1 from ldcode1 where codetype = 'county' and code1 = '"+fm.City.value+"' and code = '"+fm.County.value+"' ";
			var strCCResult = easyExecSql(tCCSQL);
			if(!strCCResult){
				alert("项目属地（区县）编码与配置不符，或与对应的市级编码不符，请核查！");
				return false;
			}
		}
	}
	if(fm.BalanceTermFlag.value == "Y"){
		if(trim(fm.BalanceTerm.value) == ""){
			alert("已选择存在结余返还条款，请录入结余返还条款！");
			return false;
		}
	}else{
		if(trim(fm.BalanceTerm.value) != ""){
			alert("没有选择存在结余返还条款，不能录入结余返还条款！");
			return false;
		}
	}
	if(fm.PremCoverTermFlag.value == "Y"){
		if(trim(fm.PremCoverTerm.value) == ""){
			alert("已选择存在保费回补条款，请录入保费回补条款！");
			return false;
		}
	}else{
		if(trim(fm.PremCoverTerm.value) != ""){
			alert("没有选择存在保费回补条款，不能录入保费回补条款！");
			return false;
		}
	}
	if(!checkProjectName()){
		return false;
	}
	return true;
}
function submitForm(){
	if (verifyInput() == false) {
		return false;
	}
	if(!beforeSub()){
		return false;
	}
	
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	if(transact == "insert"){
		fm.fmtransact.value = "INSERT||MAIN";
	}else if(transact == "update"){
		fm.fmtransact.value = "UPDATE||MAIN";
	}
	fm.action="./ProjectInputSave.jsp";
	fm.submit(); //提交
	fm.action= "";
	return true;
}
function addMedical(){
	if(tProjectNo == null || tProjectNo == "" || tProjectNo == "null"){
		alert("请先保存项目，再增加相关业务资料！");
		return false;
	}
	
	showInfo = window.open("./ProjectMedicalMain.jsp?ProjectNo="+tProjectNo);
}
function addYearInfo() {
	if(tProjectNo == null || tProjectNo == "" || tProjectNo == "null"){
		alert("项目未保存，或获取项目编码失败！");
		return false;
	}
	showInfo = window.open("./ProjectYearMain.jsp?ProjectNo=" + tProjectNo);
}
function sendUW(){
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./ProjectInputSendUWSave.jsp";
	fm.submit(); //提交
	fm.action="";
}

function afterSubmit(FlagStr,content,aProjectNo) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		transact = "update";
		fm.all('ProjectNo').value  = aProjectNo;
		tProjectNo = aProjectNo;
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
	
}
function initProjectInfo(){
	if(transact == "update"){
		if(tProjectNo == null || tProjectNo == ""){
			alert("修改操作时，获取项目编码失败！");
			return false;
		}
		var tSQL = "select ProjectNo,ProjectName,Province,City,County,"
		         + "ProjectType,Grade,PersonType,Limit,StartDate,EndDate, "
		         + "(select codename from ldcode1 where codetype = 'province' and code = Province and code1 = '0' ),"
		         + "(select codename from ldcode1 where codetype = 'city' and code = City and code1 = Province ),"
		         + "(select codename from ldcode1 where codetype = 'county' and code = County and code1 = City ), "
		         + " Managecom "
		         + "from LCProjectInfo "
		         +" where ProjectNo = '"+tProjectNo+"'";
		
		//执行查询并返回结果
		var strQueryResult = easyExecSql(tSQL);
		if(!strQueryResult){
			alert("修改操作时，根据项目编码获取项目信息失败！");
			return false;
		}
		fm.all('ProjectNo').value = strQueryResult[0][0];
	    fm.all('ProjectName').value = strQueryResult[0][1];
	    fm.all('Province').value = strQueryResult[0][2];
	    fm.all('City').value = strQueryResult[0][3];
	    fm.all('County').value = strQueryResult[0][4];
	    //fm.all('ProjectType').value = strQueryResult[0][5];
	    fm.all('Grade').value = strQueryResult[0][6];
	    fm.all('PersonType').value = strQueryResult[0][7];
	    fm.all('Limit').value = strQueryResult[0][8];
	    fm.all('StartDate').value = strQueryResult[0][9];
	    fm.all('EndDate').value = strQueryResult[0][10];
	    fm.all('ProvinceName').value = strQueryResult[0][11];
	    fm.all('CityName').value = strQueryResult[0][12];
	    fm.all('CountyName').value = strQueryResult[0][13];
	    fm.all('Managecom').value = strQueryResult[0][14];
	    
	    var tSQLYear = "select ProjectYear,InsuredPeoples,AveragePrem,Bond,StopLine,ExpecteRate,PeoplesNumber,BalanceTermFlag,PremCoverTermFlag,CountyCount, "
	    		 +" BalanceTerm,PremCoverTerm,CoInsuranceFlag "
	    		 +" from LCProjectYearInfo "
		         +" where ProjectNo = '"+tProjectNo+"' order by ProjectYear fetch first 1 rows only ";
		
		//执行查询并返回结果
		var strYearResult = easyExecSql(tSQLYear);
		if(!strYearResult){
			alert("修改操作时，根据项目编码获取首年度信息失败！");
			return false;
		}
		fm.all('ProjectYear').value = strYearResult[0][0];
	    fm.all('InsuredPeoples').value = strYearResult[0][1];
	    fm.all('AveragePrem').value = strYearResult[0][2];
	    fm.all('Bond').value = strYearResult[0][3];
	    fm.all('StopLine').value = strYearResult[0][4];
	    fm.all('ExpecteRate').value = strYearResult[0][5];
	    if(strYearResult[0][6] == "-1"){
	    	fm.all('PeoplesNumber').value = "";
	    }else{
	    	fm.all('PeoplesNumber').value = strYearResult[0][6];
	    }
	    fm.all('BalanceTermFlag').value = strYearResult[0][7];
	    fm.all('PremCoverTermFlag').value = strYearResult[0][8];
	    fm.all('CountyCount').value = strYearResult[0][9];
	    
	    fm.all('BalanceTerm').value = strYearResult[0][10];
	    fm.all('PremCoverTerm').value = strYearResult[0][11];
	    fm.all('CoInsuranceFlag').value = strYearResult[0][12];
	}
}
function queryUWYJ(){
	if(tProjectNo==null || tProjectNo == "" || tProjectNo == "null"){
		alert("项目信息未保存，无法查看审核意见！");
		return;
	}
	showInfo = window.open("./ProjectQueryUWInput.jsp?ProjectNo="+tProjectNo);
}
function checkProjectName(){
	var tSQL = "select ProjectNo from LCProjectInfo where ProjectNo != '"+fm.ProjectNo.value+"' and  ProjectName = '"+fm.ProjectName.value+"' ";
	var tArrResult = easyExecSql(tSQL);
	if(tArrResult){
		alert("该项目名称已在项目编码为["+tArrResult[0][0]+"]的项目中存在，请核查！");
		return false;
	}
	return true;
}