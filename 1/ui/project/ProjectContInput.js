
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function queryClick(){
	initProjectContGrid();
	var whGSZT = "";
    if(fm.GSZT.value == "1"){
    	whGSZT = " and not exists (select 1 from LCProjectCont where prtno = lgc.prtno)"
    }else if(fm.GSZT.value == "2"){
    	whGSZT = " and exists (select 1 from LCProjectCont where prtno = lgc.prtno)"
    }
	var strSql = "select lgc.GrpContNo,lgc.prtno,lgc.ManageCom,lgc.SignDate,"
			   + "(select codename from ldcode where codetype = 'markettype' and code = lgc.MarketType),lgc.GrpName, "
			   + "case when exists (select 1 from LCProjectCont where prtno = lgc.prtno) then '2' else '1' end, "
			   + "case when exists (select 1 from LCProjectCont where prtno = lgc.prtno) then '已归属' else '未归属' end, "
			   + "(select lcpi.ProjectName from LCProjectInfo lcpi where lcpi.ProjectNo = (select ProjectNo from LCProjectCont where prtno = lgc.prtno)) "
	           + " from LCGrpCont lgc where 1=1 and lgc.appflag = '1' "
		       + getWherePart('ManageCom', 'ManageCom','like')
		       + getWherePart('SignDate', 'StartDate','>=')
		       + getWherePart('SignDate', 'EndDate','<=')
		       + getWherePart('MarketType', 'MarketType')
		       + getWherePart('GrpContNo', 'GrpContNo')
		       + whGSZT;		   
	turnPage1.queryModal(strSql, ProjectContGrid);
}
function submitForm(){
	var count = 0;
	var tErrors = "";
	for(var i = 0; i < ProjectContGrid.mulLineCount; i++ )
	{
		if( ProjectContGrid.getChkNo(i) == true )
		{
			count ++;
			var tGrpContNo = ProjectContGrid.getRowColData(i,1);
			var tManageCom = ProjectContGrid.getRowColData(i,3);
			var tStateFlag = ProjectContGrid.getRowColData(i,7);
			var tProjectName = ProjectContGrid.getRowColData(i,9);
			if(tStateFlag == '2'){
				tErrors = tErrors + "保单【"+tGrpContNo+"】已归属到【"+tProjectName+"】项目中，无法再次归属！\n";
			}
			if(tManageCom.substr(0,4) != fm.all('GSManageCom').value.substr(0,4)){
				tErrors = tErrors + "保单【"+tGrpContNo+"】的管理机构【"+tManageCom+"】与项目的归属机构【"+fm.all('GSManageCom').value+"】不一致，无法归属！\n";
			}
		}
	}
	if(count == 0){
		alert("请选择需要处理的保单！");
		return;
	}
	if(tErrors != ""){
		alert(tErrors);
		return false;
	}
	if(fm.ProjectYear.value == null || fm.ProjectYear.value == ""){
		alert("项目年度不能为空！");
		return false;
	}
	fm.fmtransact.value = "INSERT||MAIN";
	var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}
function cancel() {
	top.close();
}
function afterSubmit(FlagStr, content, cOperate) {
	showInfo.close();
	if (FlagStr == "Fail"){
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	initForm();
}
function initProjectInfo(){
	if(tProjectNo == null || tProjectNo == ""){
		alert("获取项目编码失败！");
		return false;
	}
	var tSQL = "select lpi.ProjectNo,lpi.ProjectName,lpi.CreateDate,(select count(1) from LCProjectCont where ProjectNo = lpi.ProjectNo),ManageCom "
	         + "from LCProjectInfo lpi"
	         +" where ProjectNo = '"+tProjectNo+"'";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("修改操作时，根据项目编码获取项目信息失败！");
		return false;
	}
	fm.all('ProjectNo').value = strQueryResult[0][0];
    fm.all('ProjectName').value = strQueryResult[0][1];
    fm.all('CreateDate').value = strQueryResult[0][2];
    fm.all('ContCount').value = strQueryResult[0][3];
    fm.all('GSManageCom').value = strQueryResult[0][4];
}

