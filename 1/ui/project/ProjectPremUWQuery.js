
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function queryClick(){
	initProjectGrid();
	var strSql = "select ProjectNo, ProjectName,  CreateDate, ManageCom "
	           + "from LCProjectInfo lcpi where 1=1 and state = '05' "
		       + getWherePart('ManageCom', 'ManageCom','like')
		       + getWherePart('CreateDate', 'StartDate','>=')
		       + getWherePart('CreateDate', 'EndDate','<=')
		       + getWherePart('ProjectNo', 'ProjectNo')
		      // + getWherePart('ProjectType', 'ProjectType')
		       + getWherePart('ProjectName', 'ProjectName','like')
		       + " order by lcpi.CreateDate desc,lcpi.projectno desc ";
	turnPage.queryModal(strSql, ProjectGrid);
}
function getQueryResult(){
	var mSelNo = ProjectGrid.getSelNo();
	var tProjectNo = ProjectGrid.getRowColData(mSelNo - 1, 1);
	initProjectPremUW();
	var tWh = "";
	if("1" == fm.QueryType.value){
		tWh = " and lpmt.monthserialno in ( select max(a.monthserialno) from LCProjectMonthTrace a where projectno = '"+tProjectNo+"' group by a.projectyear,a.premyear,a.projectmonth,a.projecttype) ";
	}
	var strSql = "select lpmt.ProjectYear, lpmt.PremYear,lpmt.ProjectMonth, "
			   + " (select codename from ldcode where codetype = 'projecttype2' and code = lpmt.ProjectType), "
	           + " lpmt.Cost,lppu.SendDate,lppu.SendOperator, lppu.Operator, "
	           + " case when lppu.Conclusion is null or lppu.Conclusion ='' then '待审核' else (select codename from ldcode where codetype = 'projectconclusion' and code = lppu.Conclusion) end, "
	           //+ " lppu.AuditOpinion,"
	           + " lppu.UWDate,lppu.serialno,lpmt.ProjectNo "
	           + " from LCProjectPremUW lppu "
	           + " inner join LCProjectMonthTrace lpmt on lppu.MonthSerialNo = lpmt.MonthSerialNo "
	           + " where ProjectNo = '"+tProjectNo+"'"
	           + tWh
		       + " order by int(lpmt.ProjectYear) desc,int(lpmt.PremYear),int(lpmt.ProjectMonth) desc,lppu.uwdate desc,lppu.uwtime desc";
	turnPage1.queryModal(strSql, ProjectPremUW);
	fm.all('divPage').style.display = "";
}
function queryUWYJ(){
	var checkFlag = 0;
	for (i=0; i<ProjectPremUW.mulLineCount; i++)
	{
	  if (ProjectPremUW.getSelNo(i))
	  {
	    checkFlag = ProjectPremUW.getSelNo();
	    break;
	  }
	}
	if(checkFlag){
		var	tSerialNo = ProjectPremUW.getRowColData(checkFlag - 1, 11);
		var	tProjectNo = ProjectPremUW.getRowColData(checkFlag - 1, 12);
		if(tSerialNo == null || tSerialNo == "" || tSerialNo =="null"){
			alert("获取审核信息失败！");
			return false;
		}
		if(tProjectNo == null || tProjectNo == "" || tProjectNo =="null"){
			alert("获取审核信息失败！");
			return false;
		}
		showInfo = window.open("./ProjectQueryPremUWInput.jsp?ProjectNo="+tProjectNo+"&SerialNo="+tSerialNo+"&Flag=1");
	}else{
		alert("请选择需要查看的记录！");
		return false;
	}
}