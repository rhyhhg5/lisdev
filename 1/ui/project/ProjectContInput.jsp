<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	String tProjectNo = request.getParameter("ProjectNo");
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectContInput.js"></script>
		<%@include file="ProjectContInputInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>";
  		var tProjectNo = "<%=tProjectNo%>"; 
  		</script>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./ProjectContInputSave.jsp" method=post name=fm target="fraSubmit">

			<table>
				<tr>
					<td class=titleImg>
						项目信息
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目编号
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectNo readonly>
					</TD>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectName readonly>

					</TD>

					<TD class=title>
						建项日期
					</TD>
					<TD class=input>
						<Input class='common' name=CreateDate readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						已归属保单数
					</TD>
					<TD class=input>
						<Input class='common' name=ContCount readonly>
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>

					</TD>

					<TD class=title>

					</TD>
					<TD class=input>
					</TD>
				</TR>
			</table>


			<table>
				<tr>
					<td class=titleImg>
						查询条件
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
					</TD>
					<TD class=title>
						承保起始日期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="StartDate" readonly>
					</TD>
					<TD class=title>
						承保终止日期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="EndDate" readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						市场类型
					</TD>
					<TD class=input>
						<Input class=codeno name=MarketType verify="项目类型|code:MarketType" ondblclick="return showCodeList('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);"><input class=codename name=MarketTypeName readonly=true>
					</TD>
					
					<TD class=title>
						归属状态
					</TD>
					<TD class=input>
					<Input class=codeno name=GSZT VALUE="0" CodeData="0|^0|全部^1|未归属^2|已归属" ondblclick="return showCodeListEx('GSZT',[this,GSZTName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('GSZT',[this,GSZTName],[0,1],null,null,null,1);"><input class=codename name=GSZTName readonly=true > 		     		
					</TD>
					<TD class=title>
						保单合同号
					</TD>
					<TD class=input>
						<Input class='common' name=GrpContNo verify="保单合同号|len<=20">
					</TD>
				</TR>
			</table>

			<table>
				<td class=button align=left>
					<input type="button" class=cssButton value="查  询" id=QUERY onclick="queryClick()">
				</td>
			</table>

			<table>
				<tr>
					<td class=titleImg>
						保单清单
					</td>
				</tr>
			</table>

			<Div id="divProjectPolicy1" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanProjectContGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">
				<table>
					<tr align=center>
						<td class=button width="10%">
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">

						</td>
					</tr>
				</table>
			</Div>
				<table class=common align='center'>
					<TR class=common>
						<TD class=title>
							项目年度
						</TD>
						<TD class=input>
							<Input class=codeno name=ProjectYear verify="年度|notnull&code:projectyear"	ondblclick="return showCodeList('projectyear1',[this,ProjectYearName],[0,1],null,fm.ProjectNo.value,'ProjectNo',1);" onkeyup="return showCodeListKey('projectyear1',[this,ProjectYearName],[0,1],null,fm.ProjectNo.value,'ProjectNo',1);"><input class=codename name=ProjectYearName readonly=true elementtype=nacessary>
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>
						</TD>
					</TR>
				</table>
			<table>
				<td class=button>
					<input type=hidden id="fmtransact" name="fmtransact">
					<input type=hidden id="GSManageCom" name="GSManageCom">
					<input type="button" class=cssButton value=" 加入项目  " name=insert onclick="submitForm()">
					<input type="button" class=cssButton value=" 退  出 " name=insert onclick="cancel()">
				</td>
			</table>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
