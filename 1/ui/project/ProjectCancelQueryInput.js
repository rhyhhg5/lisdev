var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var  showInfo;
window.onfocus = myonfocus;

function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}

function queryClick(){
	initProjectGrid();
	var strSql = "select ProjectNo, ProjectName,  CreateDate, ManageCom "
	           + "from LCProjectInfo lcpi where 1=1 and state = '05' "
		       + getWherePart('ManageCom', 'ManageCom','like')
		       + getWherePart('CreateDate', 'StartDate','>=')
		       + getWherePart('CreateDate', 'EndDate','<=')
		       + getWherePart('ProjectNo', 'ProjectNo')
		       //+ getWherePart('ProjectType', 'ProjectType')
		       + getWherePart('ProjectName', 'ProjectName','like')
		       + " order by lcpi.CreateDate desc,lcpi.projectno desc ";
		       				   
	turnPage1.queryModal(strSql, ProjectGrid);
}

function ProjectCancel() {
	var checkFlag = 0;
  for (i=0; i<ProjectGrid.mulLineCount; i++)  //遍历表格
  {
    if (ProjectGrid.getSelNo(i)) //
    {
      checkFlag = ProjectGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){  //有项目被选中
  	var	tProjectNo = ProjectGrid.getRowColData(checkFlag - 1, 1);
  	if(tProjectNo == null || tProjectNo == "" || tProjectNo =="null"){
  		alert("请查询项目！");
  		return false;
  	}
  	var  cancelDate = fm.CancelDate.value;
  	if(cancelDate == null || cancelDate == "" ){
  		alert("请选择项目作废日期");
  		return  false;
  	}
  	var  auditOpinion = fm.AuditOpinion.value; 
  	if(auditOpinion == null || auditOpinion == ""){
  		alert("请填写项目作废原因");
  		return  false;
  	}
  	 var  showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	fm.fmtransact.value = "INSERT||MAIN";
	fm.submit();
  }else{
  	alert("请选择需要作废的项目！");
  	return false;
  }
}
function getQueryResult() {
	var mSelNo = ProjectGrid.getSelNo();
	fm.ProjectNo1.value = ProjectGrid.getRowColData(mSelNo - 1, 1);
	fm.ProjectName1.value = ProjectGrid.getRowColData(mSelNo - 1, 2);
}

function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	initForm();
	
	
}




