
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
var tProjectNo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function PPolicy() {
	alert("\u5951\u7ea6\u4fdd\u5355");
	showInfo = window.open("./ProjectCancelPolicyAllMain.jsp");
}
function beforeSubmit() {
	if (!verifyInput2()) {
		return false;
	}
	return true;
}
function RelationCont(){
	if(!beforeSaveAndCancel()){
		return false;
	}
	showInfo = window.open("./ProjectContMain.jsp?ProjectNo="+tProjectNo);
}
function CancelCont() {
	if(!beforeSaveAndCancel()){
		return false;
	}
	showInfo = window.open("./ProjectContCancelMain.jsp?ProjectNo="+tProjectNo);
}
function queryClick() {
	initProjectGrid();
	var strSql = "select ProjectNo, ProjectName,  CreateDate, ManageCom, "
			   //+" (select codename from ldcode where codetype = 'projecttype1' and code = projecttype), "
	           +" (select codename from ldcode where codetype = 'projectstate' and code = state) "
	           + "from LCProjectInfo lcpi where 1=1 and state = '05' "
		       + getWherePart('ManageCom', 'ManageCom','like')
		       + getWherePart('CreateDate', 'StartDate','>=')
		       + getWherePart('CreateDate', 'EndDate','<=')
		       + getWherePart('ProjectNo', 'ProjectNo')
		      // + getWherePart('ProjectType', 'ProjectType')
		       + getWherePart('ProjectName', 'ProjectName','like')
		       + " order by lcpi.CreateDate desc,lcpi.projectno desc ";	
		       				   
	turnPage1.queryModal(strSql, ProjectGrid);
}
function beforeSaveAndCancel(){
	var row = ProjectGrid.getSelNo();
	if (row == null || row == 0) {
		alert("请选择需处理的项目！");
		return false;
	}
	tProjectNo = ProjectGrid.getRowColData(row - 1, 1);
	if (tProjectNo == null || tProjectNo == "") {
		alert("没有获取项目编码，不能进行操作！");
		return false;
	}
	return true;
}
