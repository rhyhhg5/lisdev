
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function UpdateClick() {
	var tSel = ComplexRateGrid.getSelNo();
	if (tSel == 0 || tSel == null) {
		alert("请选择需要维护的项目！");
		return false;
	}
	
	var tProjectNo = ComplexRateGrid.getRowColData(tSel - 1, 1);
	if(tProjectNo == null || tProjectNo == "null" || tProjectNo == ""){
		alert("未查询项目，不能进行维护操作！");
		return false;
	}
	showInfo = window.open("./ProjectComRateMain.jsp?ProjectNo="+tProjectNo);
}
function queryClick() {
	initComplexRateGrid();
	var strSql = "select ProjectNo, ProjectName,  CreateDate, ManageCom, "
			   //+" (select codename from ldcode where codetype = 'projecttype1' and code = projecttype), "
	           +" (select codename from ldcode where codetype = 'projectstate' and code = state) "
	           + "from LCProjectInfo lcpi where 1=1 "
		       + getWherePart('ManageCom', 'ManageCom','like')
		       + getWherePart('CreateDate', 'StartDate','>=')
		       + getWherePart('CreateDate', 'EndDate','<=')
		       + getWherePart('ProjectNo', 'ProjectNo')
		       //+ getWherePart('ProjectType', 'ProjectType')
		       + getWherePart('ProjectName', 'ProjectName','like')
		       + " order by lcpi.CreateDate desc,lcpi.projectno desc ";
	turnPage1.queryModal(strSql, ComplexRateGrid);
}
