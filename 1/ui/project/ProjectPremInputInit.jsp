<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();
    initProjectInfo();  
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectMonthGrid();
	divPage.style.display="";
}
var ProjectMonthGrid;
function initProjectMonthGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         			//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="项目年度";         	  		//列名
    iArray[1][1]="100px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="费用年度";         	
    iArray[2][1]="100px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;        
    
    iArray[3]=new Array();
    iArray[3][0]="费用月度";         	
    iArray[3][1]="100px";            	
    iArray[3][2]=200;            		 
    iArray[3][3]=0;              		 
    
    iArray[4]=new Array();
    iArray[4][0]="费用类型";         	  		//列名
    iArray[4][1]="100px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="费用值";      				//列名
    iArray[5][1]="100px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0; 
    
    iArray[6]=new Array();
    iArray[6][0]="报送日期";         	  		//列名
    iArray[6][1]="100px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="报送人";      				//列名
    iArray[7][1]="100px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=3; 
    
    iArray[8]=new Array();
    iArray[8][0]="月度费用编号";      				//列名
    iArray[8][1]="100px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=3; 
     
    ProjectMonthGrid = new MulLineEnter("fm", "ProjectMonthGrid"); 
    //设置Grid属性
    ProjectMonthGrid.mulLineCount = 0;
    ProjectMonthGrid.displayTitle = 1;
    ProjectMonthGrid.locked = 1;
    ProjectMonthGrid.canSel = 0;
    ProjectMonthGrid.canChk = 0;
    ProjectMonthGrid.hiddenSubtraction = 1;
    ProjectMonthGrid.hiddenPlus = 1;
 	ProjectMonthGrid.selBoxEventFuncName = "getQueryResult";
    ProjectMonthGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}


                                      

</script>
