<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectInfo();
	initProjectContGrid();
	divPage.style.display="";
	fm.all('contDetailID').style.display = "none";
    fm.all('ManageCom').value = ManageCom;
    if("1" == tLookFlag){
		fm.all('deleteID').style.display = "none";
		fm.all('contDetailID').style.display = "";
		fm.ProjectYear.value = tProjectYear;
	}
	showAllCodeName();
	queryClick();
}


var ProjectContGrid;
function initProjectContGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         			//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="合同号";         	  		//列名
    iArray[1][1]="100px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="印刷号";         	
    iArray[2][1]="60px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=3;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="管理机构";         	  		//列名
    iArray[3][1]="60px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="承保日期";      				//列名
    iArray[4][1]="60px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0; 
	
	iArray[5]=new Array();
    iArray[5][0]="保单类型";      				//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;
    
    iArray[6]=new Array();
    iArray[6][0]="投保单位";      			//列名
    iArray[6][1]="160px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="项目年度";      			//列名
    iArray[7][1]="60px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
     
    ProjectContGrid = new MulLineEnter("fm", "ProjectContGrid"); 
    //设置Grid属性
    ProjectContGrid.mulLineCount = 5;
    ProjectContGrid.displayTitle = 1;
    ProjectContGrid.locked = 1;
    ProjectContGrid.canSel = 0;
    ProjectContGrid.canChk = 1;
    ProjectContGrid.hiddenSubtraction = 1;
    ProjectContGrid.hiddenPlus = 1;
    ProjectContGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
