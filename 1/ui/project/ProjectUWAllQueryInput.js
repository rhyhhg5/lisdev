
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function beforeSubmit() {
	if (!verifyInput2()) {
		return false;
	}
	return true;
}
function queryClick(){
	initProjectGrid();
	var strSql = "select ProjectNo, ProjectName,  CreateDate, ManageCom, "
			   + " (select SendDate from LCProjectUW where ProjectNo = lcpi.ProjectNo and AuditFlag = '02' order by SerialNo desc fetch first 1 rows only) SendDate, " 
			   + " (select SendOperator from LCProjectUW where ProjectNo = lcpi.ProjectNo and AuditFlag = '02' order by SerialNo desc fetch first 1 rows only) SendOperator " 
	           + "from LCProjectInfo lcpi where 1=1 and state = '03' "
		       + getWherePart('ManageCom', 'ManageCom','like')
		       + getWherePart('CreateDate', 'StartDate','>=')
		       + getWherePart('CreateDate', 'EndDate','<=')
		       + getWherePart('ProjectNo', 'ProjectNo')
		       //+ getWherePart('ProjectType', 'ProjectType')
		       + getWherePart('ProjectName', 'ProjectName','like')
		       + " order by lcpi.CreateDate desc,lcpi.projectno desc ";	
		       				   
	turnPage1.queryModal(strSql, ProjectGrid);
}
function ProjectUW() {
	var checkFlag = 0;
  for (i=0; i<ProjectGrid.mulLineCount; i++)
  {
    if (ProjectGrid.getSelNo(i))
    {
      checkFlag = ProjectGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	var	tProjectNo = ProjectGrid.getRowColData(checkFlag - 1, 1);
  	if(tProjectNo == null || tProjectNo == "" || tProjectNo =="null"){
  		alert("请查询项目！");
  		return false;
  	}
  	showInfo = window.open("./ProjectUWAllMain.jsp?ProjectNo="+tProjectNo);
  }else{
  	alert("请选择需要审核的项目！");
  	return false;
  }
}
