
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function initProjectInfo(){
	if(tProjectNo == null || tProjectNo == "" || tProjectNo == "null"){
		alert("项目审核时，获取项目编码失败！");
		return false;
	}
	if(tSendDate == null || tSendDate == "" || tSendDate == "null"){
		alert("项目审核时，获取报送时间失败！");
		return false;
	}
	if(tSendOperator == null || tSendOperator == "" || tSendOperator == "null"){
		alert("项目审核时，获取报送人失败！");
		return false;
	}
	var tSQL = " select ProjectNo,ProjectName,CreateDate  from LCProjectInfo where ProjectNo = '"+tProjectNo+"'";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("审核时，根据项目编码获取项目信息失败！");
		return false;
	}
	fm.all('ProjectNo').value = strQueryResult[0][0];
    fm.all('ProjectName').value = strQueryResult[0][1];
    fm.all('CreateDate').value = strQueryResult[0][2];
    fm.all('SendDate').value = tSendDate;
    fm.all('SendOperator').value = tSendOperator;
    
    var tSQLMonth = "select * from "
    +" ("
    +" select lpmi.ProjectYear,lpmi.PremYear,lpmi.ProjectMonth,(select codename from ldcode where codetype = 'projecttype2' and code = lpmi.ProjectType),Cost,"
    +" (select SendDate from LCProjectPremUW where MonthFeeNo = lpmi.MonthFeeNo order by SerialNo desc fetch first 1 rows only) SendDate,"
    +" (select SendOperator from LCProjectPremUW where MonthFeeNo = lpmi.MonthFeeNo order by SerialNo desc fetch first 1 rows only) SendOperator,"
    +" lpmi.MonthFeeNo "
    +" from LCProjectMonthInfo lpmi "
    +" where lpmi.ProjectNo = '"+tProjectNo+"' and lpmi.State = '02' "
    +" order by int(lpmi.ProjectYear),int(lpmi.PremYear),int(lpmi.ProjectMonth),lpmi.ProjectType,Cost "
    +" ) as temp "
    +" where temp.SendDate = '"+tSendDate+"'"
    +" and temp.SendOperator = '"+tSendOperator+"'";
	
	//执行查询并返回结果
	var strMonthResult = easyExecSql(tSQLMonth);
	if(!strMonthResult){
		alert("无待审核信息，或获取待审核月度费用信息失败！");
		return false;
	}
	turnPage1.queryModal(tSQLMonth, ProjectMonthGrid);	
}
function Allow(){
	if(fm.AuditOpinion.value == null || fm.AuditOpinion.value == ""){
		alert("请填写审核意见!");
		return false;
	}
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "ALLOW";
	fm.submit(); //提交
	return true;
}
function NoAllow(){
	if(fm.AuditOpinion.value == null || fm.AuditOpinion.value == ""){
		alert("请填写审核意见!");
		return false;
	}
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "NOALLOW";
	fm.submit(); //提交
	return true;
}
function afterSubmit(FlagStr,content,aProjectNo) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	initForm();
}
function cancel(){
	top.close();
}