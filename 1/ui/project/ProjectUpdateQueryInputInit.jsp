<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    

<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
  
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectGrid();
	divPage.style.display="";                 
    fm.all('ManageCom').value = ManageCom;   
}

var ProjectGrid;
function initProjectGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         	//列名
    iArray[0][1]="30px";         	//列名    
    iArray[0][3]=0;         		//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="项目编号";         	 //列名
    iArray[1][1]="50px";            //列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="项目名称";         	
    iArray[2][1]="120px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="建项日期";         	  //列名
    iArray[3][1]="50px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构";      //列名
    iArray[4][1]="50px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
     
    ProjectGrid = new MulLineEnter("fm", "ProjectGrid"); 
    //设置Grid属性
    ProjectGrid.mulLineCount =0;
    ProjectGrid.displayTitle = 1;
    ProjectGrid.locked = 1;
    ProjectGrid.canSel = 1;
    ProjectGrid.canChk = 0;
    ProjectGrid.hiddenSubtraction = 1;
    ProjectGrid.hiddenPlus = 1;
    ProjectGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
