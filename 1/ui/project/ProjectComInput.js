
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function initProjectInfo(){
	if(tProjectNo == null || tProjectNo == ""){
		alert("修改操作时，获取项目编码失败！");
		return false;
	}
	var tSQL = "select ProjectNo,ProjectName,Province,City,County,"
	         + "ProjectType,Grade,PersonType,Limit,StartDate,EndDate, "
	         + "(select codename from ldcode1 where codetype = 'province' and code = Province and code1 = '0' ),"
	         + "(select codename from ldcode1 where codetype = 'city' and code = City and code1 = Province ),"
	         + "(select codename from ldcode1 where codetype = 'county' and code = County and code1 = City ), "
	         + " ManageCom "
	         + "from LCProjectInfo "
	         +" where ProjectNo = '"+tProjectNo+"'";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("根据项目编码获取项目信息失败！");
		return false;
	}
	fm.all('ProjectNo').value = strQueryResult[0][0];
    fm.all('ProjectName').value = strQueryResult[0][1];
    fm.all('Province').value = strQueryResult[0][2];
    fm.all('City').value = strQueryResult[0][3];
    fm.all('County').value = strQueryResult[0][4];
    //fm.all('ProjectType').value = strQueryResult[0][5];
    fm.all('Grade').value = strQueryResult[0][6];
    fm.all('PersonType').value = strQueryResult[0][7];
    fm.all('Limit').value = strQueryResult[0][8];
    fm.all('StartDate').value = strQueryResult[0][9];
    fm.all('EndDate').value = strQueryResult[0][10];
    fm.all('ProvinceName').value = strQueryResult[0][11];
    fm.all('CityName').value = strQueryResult[0][12];
    fm.all('CountyName').value = strQueryResult[0][13];
    fm.all('ManageCom').value = strQueryResult[0][14];
    
    var tSQLYear = "select ProjectYear,InsuredPeoples,AveragePrem,case when PeoplesNumber = -1 then '' else char(PeoplesNumber) end,Bond,StopLine,ExpecteRate,ComplexRate, "
    		 + "case when BalanceTermFlag = 'Y' then '是' when BalanceTermFlag = 'N' then '否' else '' end,"
			 + "case when PremCoverTermFlag = 'Y' then '是' when PremCoverTermFlag = 'N' then '否' else '' end, "
			 + "CountyCount, "
			 + "case when CoInsuranceFlag = 'Y' then '是' when CoInsuranceFlag = 'N' then '否' else '' end "
    		 + "from LCProjectYearInfo "
	         +" where ProjectNo = '"+tProjectNo+"' ";
	
	//执行查询并返回结果
	var strYearResult = easyExecSql(tSQLYear);
	if(!strYearResult){
		alert("根据项目编码获取首年度信息失败！");
		return false;
	}
	turnPage1.queryModal(tSQLYear, ProjectYearGrid);
}
function cancel() {
	top.close();
}
function Medical(){
	if(tProjectNo == null || tProjectNo == "" || tProjectNo == "null"){
		alert("获取项目编码失败，无法查看相关业务资料！");
		return false;
	}
	showInfo = window.open("./ProjectMedicalMain.jsp?ProjectNo="+tProjectNo+"&LookFlag=1");
}
function queryYearClick(){
	showInfo = window.open("./ProjectYearMain.jsp?ProjectNo=" + tProjectNo+"&LookFlag=1");
}
function queryCostClick(){
	var checkFlag = 0;
	for (i=0; i<ProjectYearGrid.mulLineCount; i++)
	{
	  if (ProjectYearGrid.getSelNo(i))
	  {
	    checkFlag = ProjectYearGrid.getSelNo();
	    break;
	  }
	}
	if(checkFlag){
		var	tProjectYear = ProjectYearGrid.getRowColData(checkFlag - 1, 1);
		if(tProjectYear == null || tProjectYear == "" || tProjectYear =="null"){
			alert("获取年度失败！");
			return false;
		}
		showInfo = window.open("./ProjectMonthMain.jsp?ProjectNo=" + tProjectNo+"&LookFlag=1"+"&ProjectYear="+tProjectYear);
	}else{
		alert("请选择年度！");
		return false;
	}
}
function queryContClick(){
	var checkFlag = 0;
	for (i=0; i<ProjectYearGrid.mulLineCount; i++)
	{
	  if (ProjectYearGrid.getSelNo(i))
	  {
	    checkFlag = ProjectYearGrid.getSelNo();
	    break;
	  }
	}
	if(checkFlag){
		var	tProjectYear = ProjectYearGrid.getRowColData(checkFlag - 1, 1);
		if(tProjectYear == null || tProjectYear == "" || tProjectYear =="null"){
			alert("获取年度失败！");
			return false;
		}
		showInfo = window.open("./ProjectContCancelMain.jsp?ProjectNo=" + tProjectNo+"&LookFlag=1"+"&ProjectYear="+tProjectYear);
	}else{
		alert("请选择年度！");
		return false;
	}
}
function queryTerm(){
	var checkFlag = 0;
	for (i=0; i<ProjectYearGrid.mulLineCount; i++)
	{
	  if (ProjectYearGrid.getSelNo(i))
	  {
	    checkFlag = ProjectYearGrid.getSelNo();
	    break;
	  }
	}
	if(checkFlag){
		var	tProjectYear = ProjectYearGrid.getRowColData(checkFlag - 1, 1);
		if(tProjectYear == null || tProjectYear == "" || tProjectYear =="null"){
			alert("获取年度失败！");
			return false;
		}
		showInfo = window.open("./ProjectYearTermInput.jsp?ProjectNo=" + tProjectNo+"&ProjectYear="+tProjectYear);
	}else{
		alert("请选择年度！");
		return false;
	}
}