<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2013-4-16
//创建人  ：WS
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="ConfigLXCountInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ConfigLXCountInit.jsp"%> 
<script type="text/javascript">
	var str = "1 and comcode != ## and length(trim(comcode))=4	";
</script>
</head>
<body onload="initForm();">
	<form action="ConfigLXCountInsertSave.jsp" method="post" name="fm" target="fraSubmit">
		<table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFFIBaseInfo);">
                </td>
                <td class="titleImg">配置机构应立项目数</td>
                <TD   class=common align="Left"> 
			         <font color="#FF0000" align="Left">
			         		&nbsp;&nbsp;&nbsp;&nbsp;因应立项目数配置到4位机构，如需配置应立项目数，请选择4位或2位机构登录。
			         </font>
   				</TD> 
            </tr>
           </table>
		<table class="common">
			<TR class=common>
				<TD class=title>管理机构</TD>
				<TD class=input><Input name=comCode class='codeno' 
					verify="管理机构|NOTNULL"
					ondblclick="return showCodeList('comcode',[this,comCodeName],[0,1],null,str,1);"
					onkeyup="return showCodeListKey('comcode',[this,comCodeName],[0,1],null,str,1);"><Input
					name=comCodeName class="codename"></TD>
				<TD class=title>应立项目数</TD>
				<TD class=input><Input NAME=LXCount  value=''/>
				</TD>
			</TR>
		</table>
		<br>
    	<INPUT VALUE="保  存" class= cssButton TYPE=button onclick="insertLXCount();">
        <br><br><hr>
        <table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFFIBaseInfo);">
                </td>
                <td class="titleImg">查询机构应立项目数</td>
            </tr>
        </table>
        <div id="divComCode" style= "display:''">
            <table class="common" align='center' >
                <tr class="common">
                    <TD  class= title>
					        管理机构
					</TD>
					<TD>
						<Input name=ManageCom class='codeno'   verify="管理机构|NOTNULL"
				           ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,str,1);"  
				           onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,str,1);"
				          ><Input name=ManageComName class="codename" >
					</TD>
                </tr>
            </table>
        </div>
        <input type="hidden" name="methodName" value="" />
        <input value="查  询" class="cssButton" type="button" onclick="easyQueryClick();">
        <p />

        <div id="divComCodeAndIpGrid" style="display:''">
            <span id="spanComCodeAndIpGrid"></span>
        </div>
    </form>
	<form action="ConfigLXCountSave.jsp" method="post" name="fmUpdate"
		target="fraSubmit">
		<div id="divUpdate" style="display: 'none'">
		<hr>
			<table class="common" align='center'>
			
				<tr class="common">
					<td class="title">管理机构</td>
					<td class="input"><input class="common" name="upComCode"
						readonly=true />
					</td>
					<TD class=title>应立项目数</TD>
					<TD class=input><Input NAME=upLXCount  value='' >
					</TD>
				</tr>
			</table>
			<input type="hidden" name="methodName" value="" /> <input
				value="修  改" class="cssButton" type="button"
				onclick="updateLXCount();"> <input value="删  除"
				class="cssButton" type="button" onclick="deleteLXCount();">
		</div>
	</form>
	<span id="spanCode"  style="display:none; position:absolute; slategray"></span>
</body>