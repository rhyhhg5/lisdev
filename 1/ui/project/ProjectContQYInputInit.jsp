<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();
    initContState();  
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectGrid();
	initProjectYearGrid();
    fm.all('ManageCom').value = ManageCom;
    fm.PrtNo.value = tPrtNo;
    fm.ProjectNo1.value = "";
    fm.ProjectYear.value = "";   
}
var ProjectGrid;
var ProjectYearGrid;
function initProjectGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         			//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="项目编号";         	  		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="项目名称";         	
    iArray[2][1]="120px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="建项日期";         	  		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构";      				//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0; 
    
    ProjectGrid = new MulLineEnter("fm", "ProjectGrid"); 
    //设置Grid属性
    ProjectGrid.mulLineCount = 0;
    ProjectGrid.displayTitle = 1;
    ProjectGrid.locked = 1;
    ProjectGrid.canSel = 1;
    ProjectGrid.canChk = 0;
    ProjectGrid.hiddenSubtraction = 1;
    ProjectGrid.hiddenPlus = 1;
    ProjectGrid.selBoxEventFuncName = "getQueryResult";
    ProjectGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}

function initProjectYearGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         			//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="项目年度";         	  		//列名
    iArray[1][1]="50px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="被保人人数";         	
    iArray[2][1]="50px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="人均保费";         	  		//列名
    iArray[3][1]="50px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="联合办公人力数";      //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[5]=new Array();
    iArray[5][0]="保证金";      //列名
    iArray[5][1]="80px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[6]=new Array();
    iArray[6][0]="止损线";              //列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="预计赔付率";              //列名
    iArray[7][1]="80px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="综合赔付率";              //列名
    iArray[8][1]="80px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="是否有结余返还条款";              //列名
    iArray[9][1]="80px";            	//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="是否有保费回补条款";              //列名
    iArray[10][1]="80px";            	//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="覆盖市县数";      			//列名
    iArray[11][1]="80px";            		//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=0;
     
    ProjectYearGrid = new MulLineEnter("fm", "ProjectYearGrid"); 
    //设置Grid属性
    ProjectYearGrid.mulLineCount = 0;
    ProjectYearGrid.displayTitle = 1;
    ProjectYearGrid.locked = 1;
    ProjectYearGrid.canSel = 1;
    ProjectYearGrid.canChk = 0;
    ProjectYearGrid.hiddenSubtraction = 1;
    ProjectYearGrid.hiddenPlus = 1;
    ProjectYearGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
