<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.projecttb.*"%>

<%
	//输入参数
	ProjectComRateUI ProjectComRateUI = new ProjectComRateUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mProjectNo = request.getParameter("ProjectNo");
	String mProjectYear = request.getParameter("ProjectYear");
	String mComplexRate = request.getParameter("ComplexRate");

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	
	System.out.println("项目编码："+mProjectNo+",年度："+mProjectYear+",综合赔付率："+mComplexRate);
	
	if (mProjectNo == null || "".equals(mProjectNo) || mProjectYear == null || "".equals(mProjectYear) || mComplexRate == null || "".equals(mComplexRate)) {
		Content = "保存失败，原因是:获取保存数据失败！";
		FlagStr = "Fail";
	}else{
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("ProjectNo", mProjectNo);
		transferData.setNameAndValue("ProjectYear", mProjectYear);
		transferData.setNameAndValue("ComplexRate", mComplexRate);		
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			ProjectComRateUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")){
			tError = ProjectComRateUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>