<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2007-05-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GB2312" %>
<%@page import="com.sinosoft.utility.*" %>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.lis.finfee.*" %>
<%@page import="com.sinosoft.lis.projecttb.*" %>
<%@page import="java.util.List" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.tb.*" %>


<%
	 String Content = "";
	 String FlagStr = "";
	 String methodName = (String)request.getParameter("methodName");

	 GlobalInput tGlobalInput = new GlobalInput();
	 tGlobalInput = (GlobalInput) session.getValue("GI");

  	 System.out.println("\n\n---TestSave Start---");
	
  	 String comCode = "";
  	 String LXCount = "";
  	 if(methodName.equals("save")){//操作为添加
  		comCode = request.getParameter("comCode");
  		LXCount = request.getParameter("LXCount");
  	 }else{////操作为修改或删除
  		comCode = request.getParameter("upComCode");
  		LXCount = request.getParameter("upLXCount");
  	 }
  	 
     TransferData transferData = new TransferData();
     transferData.setNameAndValue("comCode", comCode);
     transferData.setNameAndValue("LXCount", LXCount);

     VData tVData = new VData();
     tVData.add(transferData);
     tVData.add(tGlobalInput);
    
     ConfigLXCountUI ConfigLXCountUI = new ConfigLXCountUI();
     if (!ConfigLXCountUI.submitData(tVData, methodName))
     {
    	 CErrors errors = ConfigLXCountUI.mErrors;
         Content = " 处理失败，原因是:" + (String) errors.getFirstError();
         FlagStr = "Fail";
     }
     else
     {
         VData rVData = ConfigLXCountUI.getResult();
         Content = "处理成功!";
         FlagStr = "Succ";
     }

     System.out.println(Content + "\n" + FlagStr + "\n" + methodName + "---TestSave End---\n\n");
	
    System.out.println("end TestSave ----");
%>
<html>
<script language="javascript">
    parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>' , '<%=methodName%>');
</script>
</html>
