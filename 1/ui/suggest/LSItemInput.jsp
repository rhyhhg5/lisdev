<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：LsItemInput.jsp
//程序功能：建议书项目管理
//创建日期：2002-10-12 
//创建人  ：cuiyueli
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String tSugItemCode = "";
	try
	{
		tSugItemCode = request.getParameter("SugItemCode");
		System.out.println("tSugItemCode:"+tSugItemCode);
	}
	catch( Exception e )
	{
		tSugItemCode = "";
	}

%>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="./LSItemInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LSItemInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LSItemSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 项目信息部分 -->
    <table>
      <tr>
     	 <td>
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,LSItem);">
		 </td>
         <td class= titleImg>
      		  项目信息
      	 </td>
      </tr>
    </table>
    <Div  id= "LSItem" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            项目编码
          </TD>
          <TD  class= input>
            <Input class=common  name=SugItemCode >
          </TD>
          <TD  class= title>
            项目名称
          </TD>
          <TD  class= input>
            <Input class= common name=SugItemName>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            项目属性
          </TD>
          <TD  class= input>
            <Input class="code" name=SugPrvtPrpty ondblclick="return showCodeList('SugPrvtPrpty',[this]);" onkeyup="return showCodeListKey('SugPrvtPrpty',[this]);">
          </TD>
          <TD  class= title>
            代理人代码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this]);" onkeyup="return showCodeListKey('AgentCode',[this]);">
          </TD>  
      </table>
    </Div>
    <input type=hidden id="fmAction" name="fmAction">
  </form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
