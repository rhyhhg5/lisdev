<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String tPNo = "";
	try
	{
		tPNo = request.getParameter("PNo");
System.out.println("tPNo:"+tPNo);
	}
	catch( Exception e )
	{
		tPNo = "";
	}

%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="./SugpolInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./SugpolInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./SugpolSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <%@include file="../common/jsp/OperateButton.jsp"%>
    
    <!-- 保单信息部分 -->
    
  <table width="382">
    <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLSPol1);">
      </td>
      <td class= titleImg>
        保单信息
        <INPUT VALUE="查询责任信息" TYPE=button onclick="showDuty();">
        <INPUT id="butChooseDuty" VALUE="选择责任" TYPE=button onclick="chooseDuty();">
        <input type=button value="计算" onclick="return submitForm();">
        <input id="butBack" value="返回" type=button name="button" >
      </td>
    	</tr>
    </table>
    <Div  id= "divLSPol1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          
        <TD  class= title> 投保号码 </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=PolNo >
          </TD>
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            销售渠道
          </TD>
          <TD  class= input>
            <Input class="code" name=SaleChnl ondblclick="return showCodeList('SaleChnl',[this]);" onkeyup="return showCodeListKey('SaleChnl',[this]);">
          </TD>
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCom ondblclick="return showCodeList('AgentCom',[this]);" onkeyup="return showCodeListKey('AgentCom',[this]);">
          </TD>
          <TD  class= title>
            经办人
          </TD>
          <TD  class= input>
            <Input class= common name=Handler >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this]);" onkeyup="return showCodeListKey('AgentCode',[this]);">
          </TD>
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentGroup ondblclick="return showCodeList('AgentGroup',[this]);" onkeyup="return showCodeListKey('AgentGroup',[this]);">
          </TD>
          <TD  class= title>
            联合代理人编码
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCode1 >
          </TD>
        </TR>
      </table>
    </Div>
    <!-- 投保人信息部分 -->
    <table>
    	<tr>
        	<td>
    	  		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLSAppntInd1);">
    		</td>
    		<td class= titleImg>
    			 投保人信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divLSAppntInd1" style= "display: ''">
      <table  class= common>
        <TR class= common>
          <TD  class= title>
            投保人客户号
          </TD>
          <TD  class= input>
            <Input class="code" readonly name=AppntCustomerNo ondblclick="showAppnt();">
          </TD>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntName >
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <input class="readonly" readonly name=AppntSex >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
            <input class="readonly" readonly name="AppntBirthday" >
          </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntIDType >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntIDNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            与被保人关系
          </TD>
          <TD  class= input>
            <Input class="code" name=RelationToInsured ondblclick="return showCodeList('Relation',[this]);" onkeyup="return showCodeListKey('Relation',[this]);">
          </TD>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=AppntPhone >
          </TD>
          <TD  class= title>
            手机
          </TD>
          <TD  class= input>
            <Input class= common name=AppntMobile >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class= common name=AppntEMail >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name=AppntZipCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            通讯地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name=AppntPostalAddress >
          </TD>
        </TR>
      </table>
    </Div>
    <!-- 被保人信息部分 -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLSInsured1);">
    		</td>
    		<td class= titleImg>
    			 被保人信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divLSInsured1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            被保人客户号
          </TD>
          <TD  class= input>
            <Input class="code" readonly name=CustomerNo ondblclick="showInsured();">
          </TD>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Name >
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Sex >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
          <input class="readonly" readonly name="Birthday" >
          </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=IDType >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=IDNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            健康状况
          </TD>
          <TD  class= input>
            <Input class="code" name=Health ondblclick="return showCodeList('Health',[this]);" onkeyup="return showCodeListKey('Health',[this]);">
          </TD>
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input>
            <Input class="code" name="OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class="code" name=Marriage ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);">
          </TD>
        </TR>
      </table>
    </Div>
    <!-- 险种信息部分 -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCKind1);">
    		</td>
    		<td class= titleImg>
    			 险种信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCKind1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);">
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskVersion ondblclick="return showCodeList('RiskVersion',[this]);" onkeyup="return showCodeListKey('RiskVersion',[this]);">
          </TD>
          <TD  class= title>
            保单生效日期
          </TD>
          <TD  class= input>
	        <input class="coolDatePicker" dateFormat="short" name="CValiDate" >
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= title>
            终交期间
          </TD>
          <TD  class= input>
            <Input class= common name=PayEndYear >
          </TD>
          <TD  class= title>
            终交期间单位
          </TD>
          <TD  class= input>
            <Input class="code" name=PayEndYearFlag ondblclick="return showCodeList('PeriodUnit',[this]);" onkeyup="return showCodeListKey('PeriodUnit',[this]);">
          </TD>
          <TD  class= title>
            缴费位置
          </TD>
          <TD  class= input>
            <Input class="code" name=PayLocation ondblclick="return showCodeList('PayLocation',[this]);" onkeyup="return showCodeListKey('PayLocation',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起领期间
          </TD>
          <TD  class= input>
            <Input class= common name=GetYear >
          </TD>
          <TD  class= title>
            起领期间单位
          </TD>
          <TD  class= input>
            <Input class="code" name=GetYearFlag ondblclick="return showCodeList('PeriodUnit',[this]);" onkeyup="return showCodeListKey('PeriodUnit',[this]);">
          </TD>
          <TD  class= title>
            起领日期计算类型
          </TD>
          <TD  class= input>
            <Input class="code" name=GetStartType ondblclick="return showCodeList('CalRefType',[this]);" onkeyup="return showCodeListKey('CalRefType',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保险期间
          </TD>
          <TD  class= input>
            <Input class= common name=InsuYear >
          </TD>
          <TD  class= title>
            保险期间单位
          </TD>
          <TD  class= input>
            <Input class="code" name=InsuYearFlag ondblclick="return showCodeList('PeriodUnit1',[this]);" onkeyup="return showCodeListKey('PeriodUnit1',[this]);">
          </TD>
          <TD  class= title>
            份数
          </TD>
          <TD  class= input>
            <Input class= common name=Mult >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保费
          </TD>
          <TD  class= input>
            <Input class= common name=Prem >
          </TD>
          <TD  class= title>
            保额
          </TD>
          <TD  class= input>
            <Input class= common name=Amnt >
          </TD>
          <TD  class= title>
            自动垫交标志
          </TD>
          <TD  class= input>
            <Input class="code" name=AutoPayFlag ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            红利分配方式
          </TD>
          <TD  class= input>
            <Input class="code" name=BonusMode ondblclick="return showCodeList('BonusMode',[this]);" onkeyup="return showCodeListKey('BonusMode',[this]);">
          </TD>
          <TD  class= title>
            利差返还方式
          </TD>
          <TD  class= input>
            <Input class="code" name=InterestDifFlag ondblclick="return showCodeList('InterestDifFlag',[this]);" onkeyup="return showCodeListKey('InterestDifFlag',[this]);">
          </TD>
          <TD  class= title>
            减额交清标志
          </TD>
          <TD  class= input>
            <Input class="code" name=SubFlag ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            体检标志
          </TD>
          <TD  class= input>
            <Input class="code" name=HealthCheckFlag ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
          </TD>
        </TR>
      </table>
    </Div>
    <!-- 连带被保人信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLSInsured2);">
    		</td>
    		<td class= titleImg>
    			 连带被保人信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLSInsured2" style= "display: 'none'">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanSubInsuredGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
   

  <!--可以选择的责任部分，该部分始终隐藏-->
	<Div  id= "divDutyGrid" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanDutyGrid" >
					</span> 
				</td>
			</tr>
		</table>
		<!--确定是否需要责任信息-->
	</div>
		<input type=hidden id="inpNeedDutyGrid" name="inpNeedDutyGrid" value="0">
		<input type=hidden id="fmAction" name="fmAction">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
