 <%
//程序名称：LDGrpSave.jsp
//程序功能：
//创建日期：2002-08-07 08:49:52
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.suggest.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%

  //接收信息，并作校验处理。
  //输入参数
  LSLatncyGrpSchema tLDGrpSchema   = new LSLatncyGrpSchema();
  LatncyGrpUI tLatncyGrpUI   = new LatncyGrpUI();
  LDGrpUI tLDGrpUI = new LDGrpUI();

  //输出参数
  CErrors tError = null;
  String tBmCert = "";
  //后面要执行的动作：添加，修改，删除
  String transact = "";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
//    if(request.getParameter("GrpNo").length()>0)
    tLDGrpSchema.setGrpNo(request.getParameter("GrpNo"));
//    if(request.getParameter("GrpName").length()>0)
    tLDGrpSchema.setGrpName(request.getParameter("GrpName"));
//    if(request.getParameter("GrpAddressCode").length()>0)
    tLDGrpSchema.setGrpAddressCode(request.getParameter("GrpAddressCode"));
//    if(request.getParameter("GrpAddress").length()>0)
    tLDGrpSchema.setGrpAddress(request.getParameter("GrpAddress"));
//    if(request.getParameter("GrpZipCode").length()>0)
    tLDGrpSchema.setGrpZipCode(request.getParameter("GrpZipCode"));
//    if(request.getParameter("Phone").length()>0)
    tLDGrpSchema.setPhone(request.getParameter("Phone"));
//    if(request.getParameter("Fax").length()>0)
    tLDGrpSchema.setFax(request.getParameter("Fax"));
//    if(request.getParameter("Satrap").length()>0)
    tLDGrpSchema.setSatrap(request.getParameter("Satrap"));
//    if(request.getParameter("Corporation").length()>0)
    tLDGrpSchema.setCorporation(request.getParameter("Corporation"));
//    if(request.getParameter("EMail").length()>0)
    tLDGrpSchema.setEMail(request.getParameter("EMail"));
//    if(request.getParameter("LinkMan").length()>0)
    tLDGrpSchema.setLinkMan(request.getParameter("LinkMan"));
//    if(request.getParameter("Peoples").length()>0)
    tLDGrpSchema.setPeoples(request.getParameter("Peoples"));
//    if(request.getParameter("BankCode").length()>0)
    tLDGrpSchema.setBankCode(request.getParameter("BankCode"));
//    if(request.getParameter("BankAccNo").length()>0)
    tLDGrpSchema.setBankAccNo(request.getParameter("BankAccNo"));
//    if(request.getParameter("BusinessType").length()>0)
    tLDGrpSchema.setBusinessType(request.getParameter("BusinessType"));
//    if(request.getParameter("GrpNature").length()>0)
    tLDGrpSchema.setGrpNature(request.getParameter("GrpNature"));
//    if(request.getParameter("FoundDate").length()>0)
    tLDGrpSchema.setFoundDate(request.getParameter("FoundDate"));
//    if(request.getParameter("GrpGroupNo").length()>0)
    tLDGrpSchema.setGrpGroupNo(request.getParameter("GrpGroupNo"));
//    if(request.getParameter("State").length()>0)
    tLDGrpSchema.setState(request.getParameter("State"));
//    if(request.getParameter("Remark").length()>0)
    tLDGrpSchema.setRemark(request.getParameter("Remark"));
//    if(request.getParameter("BlacklistFlag").length()>0)
    tLDGrpSchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));
//    if(request.getParameter("Operator").length()>0)
    tLDGrpSchema.setOperator(request.getParameter("Operator"));
    transact=request.getParameter("Transact");
    //tracnsact代表了执行的操作类型
System.out.println("Peoples"+tLDGrpSchema.getPeoples());
  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tLDGrpSchema);
    
   //执行动作：INSERT 添加纪录，UPDATE 修改纪录，DELETE 删除纪录
   tLatncyGrpUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDGrpUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = transact+" 成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = transact+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

