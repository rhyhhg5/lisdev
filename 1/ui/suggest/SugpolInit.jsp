<%
//程序名称：SugpolInit.jsp
//程序功能：
//创建日期：2002-10-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单信息部分
    fm.all('PrtNo').value = '';
    fm.all('PolNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('SaleChnl').value = '';
    fm.all('AgentCom').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('Handler').value = '';
    fm.all('AgentCode1').value = '';

  	// 投保人信息部分    
  	fm.all('AppntCustomerNo').value = '';
    fm.all('AppntName').value = '';
    fm.all('AppntSex').value = '';
    fm.all('AppntBirthday').value = '';
    fm.all('AppntIDType').value = '';
    fm.all('AppntIDNo').value = '';
    fm.all('RelationToInsured').value = '';
    fm.all('AppntPhone').value = '';
    fm.all('AppntMobile').value = '';
    fm.all('AppntPostalAddress').value = '';
    fm.all('AppntZipCode').value = '';
    fm.all('AppntEMail').value = '';
	
	// 被保人信息部分
    fm.all('CustomerNo').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('Birthday').value = '';
    fm.all('IDType').value = '';
    fm.all('IDNo').value = '';
    fm.all('Health').value = '';
    fm.all('OccupationType').value = '';
    fm.all('Marriage').value = '';
    
    // 险种信息部分
    fm.all('RiskCode').value = '';
    fm.all('RiskVersion').value = '';
    fm.all('CValiDate').value = '';
    fm.all('PayEndYear').value = '';
    fm.all('PayEndYearFlag').value = '';
    fm.all('GetYear').value = '';
    fm.all('GetYearFlag').value = '';
    fm.all('GetStartType').value = '';
    fm.all('InsuYear').value = '';
    fm.all('InsuYearFlag').value = '';
    fm.all('Mult').value = '';
    fm.all('Prem').value = '';
    fm.all('Amnt').value = '';
    fm.all('AutoPayFlag').value = '';
    fm.all('BonusMode').value = '';
    fm.all('InterestDifFlag').value = '';
    fm.all('SubFlag').value = '';
    fm.all('PayLocation').value = '';
    fm.all('HealthCheckFlag').value = '';
  }
  catch(ex)
  {
    alert("在SugpolInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
//function initSelBox()
//  try                 
//  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
//  }
//  catch(ex)
//  {
//    alert("在ProposalInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!1");
//  }
//}                                        

function initForm()
{
	var loadFlag = "0";
	try
	{
		alert(top.opener.mShowPolDetail);
		if( top.opener.mShowPolDetail == "APPROVE" ) loadFlag = "1";
	}
	catch(ex1){}
	try
	{
		initInpBox();
		//initSelBox();    
		initSubInsuredGrid();
		//initBnfGrid();
		//initImpartGrid();
		//initSpecGrid();
		
		// 初始化时查询保单明细
		if( loadFlag == "1" )
		{
			fm.all( 'PolNo' ).value = <%=tPNo%>;
			queryPolDetail();
		}	
	}
	catch(re)
	{
		alert("SugpolInit.jsp-->InitForm函数中发生异常:初始化界面错误2!");
		alert(re);
	}
}

// 被保人信息列表的初始化
function initSubInsuredGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="连带被保人客户号";    	//列名
      iArray[1][1]="180px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][7]="showSubInsured";
      iArray[1][8]="['SubInsured']";        //是否使用自己编写的函数 

      iArray[2]=new Array();
      iArray[2][0]="姓名";         			//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="性别";         			//列名
      iArray[3][1]="140px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="出生日期";         		//列名
      iArray[4][1]="140px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="与被保人关系";         		//列名
      iArray[5][1]="160px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][4]="Relation";

      SubInsuredGrid = new MulLineEnter( "fm" , "SubInsuredGrid" ); 
      //这些属性必须在loadMulLine前
      SubInsuredGrid.mulLineCount = 2;   
      SubInsuredGrid.displayTitle = 1;
      //SubInsuredGrid.tableWidth = 200;
      SubInsuredGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("SugpolInit.jsp-->InitForm函数中发生异常:初始化界面错误!3");
	alert(ex);
      }
}

</script>