//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var spanObj;
var mDebug = "0";
var mOperate = 0;
var mAction = "";
var arrResult = new Array();
var mShowCustomerDetail = "SUGGEST";
window.onfocus = myonfocus;

/*********************************************************************
 *  保存个人投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  

	if( mAction == "" )
	{
		showSubmitFrame(mDebug);
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		mAction = "INSERT";
		alert("wh:"+mAction);
		fm.all( 'fmAction' ).value = mAction;
		fm.submit(); //提交
	}
}


/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		showDiv(operateButton,"true"); 
		showDiv(inputButton,"false");
	}
	mAction = ""; 
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在SugpolInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
} 

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	showDiv( operateButton, "false" ); 
	showDiv( inputButton, "true" ); 
}           

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick()
{
	if( mOperate == 0 )
	{
		mOperate = 1;
		showInfo = window.open("../Suggest/SugpolQuery.html");
	}
}           

/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
	var tPolNo = "";
	tPolNo = fm.all( 'PolNo' ).value;
	if( tPolNo == null || tPolNo == "" )
		alert( "请先做投保单查询操作，再进行修改!" );
	else
	{
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "UPDATE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}           

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick()
{
	var tPolNo = "";
	tPolNo = fm.all( 'PolNo' ).value;
	if( tPolNo == null || tPolNo == "" )
		alert( "请先做投保单查询操作，再进行删除!" );
	else
	{
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}           

/*********************************************************************
 *  Click事件，当点击“选择责任”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function chooseDuty()
{
	cRiskCode = fm.RiskCode.value;
	cRiskVersion = fm.RiskVersion.value
	
	if( cRiskCode == "" || cRiskVersion == "" )
	{
		alert( "您必须先录入险种和险种版本才能修改该投保单的责任项。" );
		return false
	}

	showInfo = window.open("../suggest/ChooseDutyInput.jsp?RiskCode="+cRiskCode+"&RiskVersion="+cRiskVersion);
	return true
}           

/*********************************************************************
 *  Click事件，当点击“查询责任信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showDuty()
{
	//下面增加相应的代码
	cPolNo = fm.PolNo.value;
	if( cPolNo == "" )
	{
		alert( "您必须先查询投保单才能查看该投保单的责任项。" );
		return false
	}
	
	var showStr = "正在查询数据，请您稍候......";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	showModalDialog( "../Suggest/SugpolDuty.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
	showInfo.close();
}           

/*********************************************************************
 *  Click事件，当点击“关联暂交费信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showFee()
{
	cPolNo = fm.PolNo.value;
	if( cPolNo == "" )
	{
		alert( "您必须先查询投保单才能进入暂交费信息部分。" );
		return false
	}
	
	showInfo = window.open( "../Suggest/SugpolFee.jsp?PolNo=" + cPolNo );
}           

/*********************************************************************
 *  Click事件，当双击“投保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt()
{
	if( mOperate == 0 )
	{
		mOperate = 2;
		showInfo = window.open( "../sys/LSLatncyCustmMain.html" );
	}
}           

/*********************************************************************
 *  Click事件，当双击“被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsured()
{
	if( mOperate == 0 )
	{
		mOperate = 3;
		showInfo = window.open( "../sys/LSLatncyCustmMain.html" );
	}
}           

/*********************************************************************
 *  Click事件，当双击“连带被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubInsured( span, arrPara )
{
	if( mOperate == 0 )
	{
		mOperate = 4;
		spanObj = span;
		showInfo = window.open( "../sys/LSLatncyCustmMain.html" );
	}
}           

/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt()
{
	fm.all( 'AppntCustomerNo' ).value = arrResult[0][0];
	fm.all( 'AppntName' ).value = arrResult[0][1];
	fm.all( 'AppntSex' ).value = arrResult[0][2];
	fm.all( 'AppntBirthday' ).value = arrResult[0][3];
	fm.all( 'AppntIDType' ).value = arrResult[0][4];
	fm.all( 'AppntIDNo' ).value = arrResult[0][5];
}

/*********************************************************************
 *  把查询返回的客户数据显示到被保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayInsured()
{
	fm.all( 'CustomerNo' ).value = arrResult[0][0];
	fm.all( 'Name' ).value = arrResult[0][1];
	fm.all( 'Sex' ).value = arrResult[0][2];
	fm.all( 'Birthday' ).value = arrResult[0][3];
	fm.all( 'IDType' ).value = arrResult[0][4];
	fm.all( 'IDNo' ).value = arrResult[0][5];
}

/*********************************************************************
 *  把查询返回的客户数据显示到连带被保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displaySubInsured()
{
	fm.all( spanObj ).all( 'SubInsuredGrid1' ).value = arrResult[0][0];
	fm.all( spanObj ).all( 'SubInsuredGrid2' ).value = arrResult[0][1];
	fm.all( spanObj ).all( 'SubInsuredGrid3' ).value = arrResult[0][2];
	fm.all( spanObj ).all( 'SubInsuredGrid4' ).value = arrResult[0][3];
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		
		if( mOperate == 1 )			// 查询保单明细
		{
			fm.all( 'PolNo' ).value = arrResult[0][0];
			
			// 查询保单明细
			queryPolDetail();
		}
		if( mOperate == 2 )			// 投保人信息
			displayAppnt();
		if( mOperate == 3 )			// 主被保人信息
			displayInsured();
		if( mOperate == 4 )			// 连带被保人信息
			displaySubInsured();
	}
	mOperate = 0;		// 恢复初态
}

/*********************************************************************
 *  根据查询返回的信息查询投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPolDetail()
{
	var cPolNo;
	cPolNo = fm.all( 'PolNo' ).value;

	//showSubmitFrame(mDebug);
//	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  

//	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	parent.fraSubmit.window.location = "../Suggest/SugpolQueryDetail.jsp?PolNo=" + cPolNo;
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
