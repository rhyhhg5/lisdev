//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mAction = "";

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  	alert("start save!");
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//  showSubmitFrame(mDebug);
  mAction = "INSERT";
  alert("start submit!");
  fm.submit(); //提交 
  alert("end submit");
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
//  alert(FlagStr);
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
//    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
    if( mAction == "INSERT" ) mAction = "INSERT||OK";
    
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LSLatncyCustmInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  //个人信息表中不能为空的字段检验,包括2部分
  //页面显示控件中要输入的字段(CustomerNo,Name,Sex,Birthday,Operator)	
  //隐藏的字段(MakeDate,MakeTime,ModifyDate,ModifyTime),在LSLatncyCustmUI中输入
    if(fm.all('CustomerNo').value == '')
    {
    	alert("客户号码不能为空!");
    	return false;
    	}
    if(fm.all('Name').value == '')
    {
    	alert("客户姓名不能为空!");
    	return false;
    	}
    if(fm.all('Sex').value == '')
    {
    	alert("客户性别不能为空!");
    	return false;
    	}
    if(fm.all('Birthday').value == '')
    {
    	alert("客户出生日期不能为空!");
    	return false;
    	}
    if(fm.all('Operator').value == '')
    {
    	alert("操作员代码不能为空!");
    	return false;
    	}

    if(fm.all('Sex').value !='0'&&fm.all('Sex').value!='1'&&fm.all('Sex').value!='2')
    {
    	alert("客户性别输入有误!");
    	return false;
    	}  
    if(!isDate(fm.all('Birthday').value))
    {
    	alert("客户出生日期输入有误!");
    	return false;
    	}  

   if(!isNumeric(fm.all('Salary').value))
   {
   	alert("客户薪水输入有误!");
    	return false;
   }
   
   if(!isNumeric(fm.all('Stature').value))
   {
   	alert("客户身高输入有误!");
    	return false;
   }
   
   if(!isNumeric(fm.all('Avoirdupois').value))
   {
   	alert("客户体重输入有误!");
    	return false;
   }      
    //个人信息表中可以为空的字段检验!  
/*
    //日期检查，数字检查
    fm.all('Password').value = '1';    
    fm.all('NativePlace').value = '';
    fm.all('Nationality').value = '';
    fm.all('Marriage').value = '';
    fm.all('MarriageDate').value = '';
    fm.all('OccupationType').value = '';
    fm.all('StartWorkDate').value = '';
    fm.all('Salary').value = 0.0;  //float 
    fm.all('Health').value = '';
    fm.all('Stature').value = 0.0;   //float 
    fm.all('Avoirdupois').value = 0.0;//float
    fm.all('CreditGrade').value = '';
    fm.all('IDType').value = '';
    fm.all('Proterty').value = '';
    fm.all('IDNo').value = '';
    fm.all('OthIDType').value = '';
    fm.all('OthIDNo').value = '';
    fm.all('ICNo').value = '';
    fm.all('HomeAddressCode').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('PostalAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('BP').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('BankCode').value = '';
    fm.all('BankAccNo').value = '';
    fm.all('JoinCompanyDate').value = '';
    fm.all('Position').value = '';
    fm.all('GrpNo').value = '';
    fm.all('GrpName').value = '';
    fm.all('GrpPhone').value = '';
    fm.all('GrpAddressCode').value = '';
    fm.all('GrpAddress').value = '';
    fm.all('DeathDate').value = '';
    fm.all('Remark').value = '';
    fm.all('State').value = '';
    fm.all('BlacklistFlag').value = '';
*/
    return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true");
  //表单中的隐藏字段"活动名称"赋为insert 
  fm.all('Transact').value ="insert";
  
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  //请考虑修改客户号码的情况
//  alert("update");
  //表单中的隐藏字段"活动名称"赋为update
  fm.all('Transact').value ="update";
  submitForm();  
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	var loadFlag = "0";

	try
	{
		if( top.opener.mShowCustomerDetail == "PROPOSAL" ) loadFlag = "1";
	}
	catch(ex){}
	
	if( loadFlag == "1" )
		parent.fraInterface.window.location = "./LSLatncyCustmQuery.jsp";
	else
	    window.open("./LSLatncyCustmQuery.html");
  
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
//  alert("delete");
  //尚未考虑全部为空的情况
  if(fm.all('CustomerNo').value == '')
  {
   alert("客户号码不能为空!");
   return false;
  }
  else
  {
  //表单中的隐藏字段"活动名称"赋为insert	
  fm.all('Transact').value ="delete";
  fm.submit();
  }
  
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var loadFlag = "0";
	
	try
	{
		if( top.opener.mShowCustomerDetail == "PROPOSAL" ) loadFlag = "1";
	}
	catch(ex){}
	
	if( loadFlag == "1" )
	{
		if( mAction != "INSERT||OK" )
			alert( "请先保存，再点击返回按钮。" );
		else
		{
			try
			{
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		}
	}
}

function getQueryResult()
{
	var arrSelected = new Array();

	arrSelected[0] = new Array();
	arrSelected[0][0] = fm.all( 'CustomerNo' ).value;
	arrSelected[0][1] = fm.all( 'Name' ).value;
	arrSelected[0][2] = fm.all( 'Sex' ).value;
	arrSelected[0][3] = fm.all( 'Birthday' ).value;
	arrSelected[0][4] = fm.all( 'IDType' ).value;
	arrSelected[0][5] = fm.all( 'IDNo' ).value;

	return arrSelected;
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	if( arrQueryResult != null )
	{
		fm.all( 'CustomerNo' ).value = arrQueryResult[0][0];
		
		easyQueryClick();
	}
}

// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL = "";
	var tCustomerNo = fm.all( 'CustomerNo' ).value;
	strSQL = "select * from LSLatncyCustm where CustomerNo='" + tCustomerNo + "'";
//alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initInpBox();
		 
		// 显示查询结果
		fm.all('CustomerNo').value = arrResult[0][0];
		fm.all('Password').value = arrResult[0][1];
		fm.all('Name').value = arrResult[0][2];
		fm.all('Sex').value = arrResult[0][3];
		fm.all('Birthday').value = arrResult[0][4];
		fm.all('NativePlace').value = arrResult[0][5];
		fm.all('Nationality').value = arrResult[0][6];
		fm.all('Marriage').value = arrResult[0][7];
		fm.all('MarriageDate').value = arrResult[0][8];
		fm.all('OccupationType').value = arrResult[0][9];
		fm.all('StartWorkDate').value = arrResult[0][10];
		fm.all('Salary').value = arrResult[0][11];       //float 
		fm.all('Health').value = arrResult[0][12];
		fm.all('Stature').value = arrResult[0][13];      //float 
		fm.all('Avoirdupois').value = arrResult[0][14];   //float
		fm.all('CreditGrade').value = arrResult[0][15];
		fm.all('IDType').value = arrResult[0][16];
		fm.all('Proterty').value = arrResult[0][17];
		fm.all('IDNo').value = arrResult[0][18];
		fm.all('OthIDType').value = arrResult[0][19];
		fm.all('OthIDNo').value = arrResult[0][20];
		fm.all('ICNo').value = arrResult[0][21];
		fm.all('HomeAddressCode').value = arrResult[0][22];
		fm.all('HomeAddress').value = arrResult[0][23];
		fm.all('PostalAddress').value = arrResult[0][24];
		fm.all('ZipCode').value = arrResult[0][25];
		fm.all('Phone').value = arrResult[0][26];
		fm.all('BP').value = arrResult[0][27];
		fm.all('Mobile').value = arrResult[0][28];
		fm.all('EMail').value = arrResult[0][29];
		fm.all('BankCode').value = arrResult[0][30];
		fm.all('BankAccNo').value = arrResult[0][31];
		fm.all('JoinCompanyDate').value = arrResult[0][32];
		fm.all('Position').value = arrResult[0][33];
		fm.all('GrpNo').value = arrResult[0][34];
		fm.all('GrpName').value = arrResult[0][35];
		fm.all('GrpPhone').value = arrResult[0][36];
		fm.all('GrpAddressCode').value = arrResult[0][37];
		fm.all('GrpAddress').value = arrResult[0][38];
		fm.all('DeathDate').value = arrResult[0][39];
		fm.all('Remark').value = arrResult[0][40];
		fm.all('State').value = arrResult[0][41];
		fm.all('BlacklistFlag').value = arrResult[0][42];
		fm.all('Operator').value = arrResult[0][43];
	} // end of if
}
