<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LSItemSave.jsp
//程序功能：
//创建日期：2002-10-21
//创建人  ：cuiyueli
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.suggest.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LSItemSchema tLSItemSchema   = new LSItemSchema();
  LSItemSet tLSItemSet = new LSItemSet();

  ItemUI tItemUI   = new ItemUI();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "";

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String tAction = "";
  String ContentSave = "";
  String queryFlag = "0";
  String tStr=new String();

  GlobalInput tG = new GlobalInput();

    tG.Operator = "Admin";
    tG.ComCode  = "001";
    session.putValue("GI",tG);
    tG = ( GlobalInput )session.getValue( "GI" );

  tAction = request.getParameter( "fmAction" );
  System.out.println("tAction:"+tAction);
if (!tAction.equals("QUERY"))
 {
    tLSItemSchema.setSugItemCode(request.getParameter("SugItemCode"));
    tLSItemSchema.setSugItemName(request.getParameter("SugItemName"));
    tLSItemSchema.setSugPrvtPrpty(request.getParameter("SugPrvtPrpty"));
    tLSItemSchema.setAgentCode(request.getParameter("AgentCode"));
 }

  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement( tG );

   //此处需要根据实际情况修改
   tVData.addElement(tLSItemSchema);

   if( tAction.equals( "INSERT" )) transact = "INSERT||LSItem";
   if( tAction.equals( "UPDATE" )) transact = "UPDATE||LSItem";
   if( tAction.equals( "DELETE" )) transact = "DELETE||LSItem";
   if( tAction.equals( "QUERY" )) transact = "QUERY||LSItem";
    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    if (tAction.equals("QUERY")) //查询
    {
        String tSugItemCode = request.getParameter("SugItemCode");
        System.out.println("SugItemcode query=="+tSugItemCode);

        LSItemDB tLSItemDB = new LSItemDB();
        tLSItemDB.setSugItemCode( tSugItemCode);
        FlagStr = "query";

        tLSItemSet = (LSItemSet)tLSItemDB.query();
        System.out.println("count=="+tLSItemSet.size());
       if (tLSItemSet.size()<=0)
        {
          System.out.println(" fail !!!!!!!!!");
           Content = " 查询失败,未找到相关数据!";
           FlagStr = "queryFail";
           queryFlag="1";
         }
    }
     else  //insert
    {
        if (!tItemUI.submitData(tVData,transact))
         {
             ContentSave = " 操作失败，原因是: " + tItemUI.mErrors.getError(0).errorMessage;
             FlagStr = "Fail";
         }
    }
   //如果在Catch中发现异常，则不从错误类中提取错误信息
   if (FlagStr=="")
   {
     tError = tItemUI.mErrors;
     if (!tError.needDealError())
     {
         Content = " 保存成功";
         if (tAction.equals("DELETE"))
                  Content = "删除成功";
         FlagStr = "Succ";
     }
   }
   else
     {
          Content = ContentSave;//保存失败后的信息
     }
     //查询判断
    System.out.println("count2==="+tLSItemSet.size()+ContentSave);
    if (FlagStr.equals("query"))
    {
          System.out.println("55555555555");
           %>
            <script language="javascript">
               parent.fraInterface.fm.all("SugItemCode").value="<%=tLSItemSet.get(1).getSugItemCode()%>";
               parent.fraInterface.fm.all("SugItemName").value="<%=tLSItemSet.get(1).getSugItemName()%>";
               parent.fraInterface.fm.all("SugPrvtPrpty").value="<%=tLSItemSet.get(1).getSugPrvtPrpty()%>";
               parent.fraInterface.fm.all("AgentCode").value="<%=tLSItemSet.get(1).getAgentCode()%>";
            </script>
           <%
             Content = " 查询成功!";
    }
    else
    {
       System.out.println("queryfail==="+queryFlag);
       if (queryFlag.equals("1"))   //查询失败
        {
             Content = " 查询失败，未找到相关数据!";
             FlagStr = "queryFail";

         }
   }

%>
<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

