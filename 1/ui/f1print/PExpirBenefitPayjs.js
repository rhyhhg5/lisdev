//该文件中包含客户端需要处理的函数和事件

//程序名称：PExpirBenefitPayjs.js
//程序功能：满期金提取
//创建日期：2015-8-18
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
var showInfo;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
} 

function DataExt()
{	
	if(!beforeSubmit()){
		return false;	
	}
	fm.action="PExpirBenefitPaySave.jsp";
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.submit(); //提交
}

function beforeSubmit(){
	if((fm.all("EndDay").value==null||fm.all("EndDay").value==''||fm.all("StartDay").value==null||fm.all("StartDay").value==''))
	{
		if((fm.all("ContNo").value==null||fm.all("ContNo").value==''))
		{
			alert("请输入提数起止日期或保单号");
			return false;
		}
	}
	
	return true;
	
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  //释放“增加”按钮

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;

    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   
  }
  else
  {
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;

    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

