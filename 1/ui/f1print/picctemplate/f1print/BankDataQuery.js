//
//程序名称：BankDataQuery.js
//程序功能：财务报盘数据查询
//创建日期：2004-10-20
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容

//var arrDataSet;
var turnPage = new turnPageClass();

//简单查询
function easyQuery()
{
	var sDate = fm.all('SendDate').value;
	
	if (sDate == "")
	{
			alert("请输入发盘日期！");
			return;
	}
	
	// 书写SQL语句
	var strSQL = "select polno,AccName,comcode,BankCode,SendDate "
							+ "from lySendToBankB "
							+ " where 1=1 "
							+ getWherePart('polno','PolNo','=','0')
							+ getWherePart('accname','AccName','=','0')
							+ getWherePart('comcode','ManageCom','like','0')
							+ getWherePart('bankcode','BankCode','=','0')
							+ getWherePart('SendDate','SendDate','=','0')
							+ " group by polno,accname,comcode,bankcode,senddate "
							+ "order by 1";
	//alert(strSQL);
	turnPage.queryModal(strSQL, CodeGrid);    
	//arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
}

function easyPrint()
{
	easyQueryPrint(2,'CodeGrid','turnPage');
}