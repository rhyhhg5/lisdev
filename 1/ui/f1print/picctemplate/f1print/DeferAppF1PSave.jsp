<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
	
	GlobalInput tG = (GlobalInput)session.getAttribute("GI");

	LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
	
	tLOPRTManagerSchema.setPrtSeq(request.getParameter("PrtSeq"));

  CError cError = new CError( );

  //后面要执行的动作：添加，修改，删除
  //String strOperation = request.getParameter("fmtransact");
  String PrtSeq=request.getParameter("PrtSeq");
  
	VData tVData = new VData();
	VData mResult = new VData();

  tVData.addElement(tG);
  tVData.addElement(tLOPRTManagerSchema);

	String strErrMsg = "";
	boolean Flag=true;
	
	
  DANF1PUI tDANF1PUI = new DANF1PUI();
	if(!tDANF1PUI.submitData(tVData,"CONFIRM")) 
	{
		if( tDANF1PUI.mErrors.needDealError() ) 
		{
		  Flag=false;
			strErrMsg = tDANF1PUI.mErrors.getFirstError();
		} else {
			strErrMsg = "DANF1PUI发生错误，但是没有提供详细的出错信息";
		}
%>
		<script language="javascript">
			alert('<%= strErrMsg %>');
			window.opener = null;
			window.close();
		</script>
<%
		return;
  }

  mResult = tDANF1PUI.getResult();
  
	XmlExport txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	
	if (txmlExport==null) {
	  System.out.println("null");
	}
	session.setAttribute("PrintNo",PrtSeq );
  session.setAttribute("PrintType","06" );
  session.setAttribute("PrintStream", txmlExport.getInputStream());
	System.out.println("put session value");
	response.sendRedirect("GetF1Print.jsp");
%>