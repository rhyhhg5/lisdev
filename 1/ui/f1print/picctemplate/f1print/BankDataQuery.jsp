<%
//程序名称：BankDataQuery.jsp
//程序功能：财务报盘数据查询
//创建日期：2004-10-20
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
%>

<html>    
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
  
  <SCRIPT src="./BankDataQuery.js"></SCRIPT>   
  <%@include file="./BankDataQueryInit.jsp"%>   
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>提取新契约手机号 </title>
</head>      
 
<body  onload="initForm();" >
  <form method=post name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>管理机构</TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this],null,null,codeSql,'1',null,250);" 
            				onkeyup="return showCodeListKey('station',[this],null,null,codeSql,'1',null,250);">
          </TD>
		      <TD  class= title>银行代码</TD>
		      <TD  class= input>
		        <Input CLASS="code" name=BankCode verify="银行代码|notnull&code:BankCode" ondblclick="return showCodeList('Bank',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('Bank', [this],null,null,null,null,1);">
		      </TD>
       		<TD  class= title width="25%">发盘日期</TD>
       		<TD  class= input width="25%">
       		  <Input class= "coolDatePicker" dateFormat="short" name=SendDate verify="发盘日期|NOTNULL">
       		</TD>
     </TR>
     <TR  class= common>
          <TD  class= title>保单号</TD>
          <TD  class= input><Input class= common name=PolNo></TD>
          <TD  class= title>帐户名</TD>
          <TD  class= input><Input class= common name=AccName></TD>
     </TR>
   	</Table>  
   	<p>
    <input type=hidden id="fmtransact" name="fmtransact">
    <!--数据区-->
    <INPUT VALUE="查询" class= common TYPE=button onclick="easyQuery()"> 	

  	<Div  id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
									<span id="spanCodeGrid" >
									</span> 
		  				</td>
					</tr>
    		</table> 
      	<INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      	<INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      	<INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      	<INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 	
<script>
	<!--选择机构：只能查询本身和下级机构-->
	var codeSql = "1  and code like #"+<%=Branch%>+"%#";
</script>