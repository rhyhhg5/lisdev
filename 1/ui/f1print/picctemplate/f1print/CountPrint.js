//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  // showSubmitFrame(mDebug);
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;
  var flag = 0;
	flag = 0;
        
	//for( i = 0; i < PolGrid.mulLineCount; i++ ) {
		if( PolGrid.getSelNo() ) {			
			flag = 1;
			//break;
		}
	//}
	
	if( flag == 0 ) {
		alert("请先选择一条记录，再打印保单");
		return false;
	}
        
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  // showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  // showSubmitFrame(mDebug);     
	var vOrgTarget = fm.target;
	fm.target="f1print";
	fm.submit();
	fm.target = vOrgTarget;
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");	
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  } else {
  	easyQueryClick();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CountPrint.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();

	var strManageComWhere = " AND ManageCom LIKE '" + manageCom + "%%' ";
	
	if( fm.ManageCom.value != '' ) {
		strManageComWhere += " AND ManageCom LIKE '" + fm.ManageCom.value + "%%' ";
	}
	
	if( fm.BranchGroup.value != '' ) {
		strManageComWhere += " AND AgentGroup IN" + 
												" ( SELECT AgentGroup FROM LABranchGroup WHERE BranchAttr LIKE '" +
												fm.BranchGroup.value + "%%') ";
	}
		
	
	// 书写SQL语句
	var strSQL = "";

	strSQL = "SELECT PolNo, PrtNo, RiskCode, RiskVersion, AppntName, InsuredName FROM LCPol A where GrpPolNo = '00000000000000000000'"
				 + " and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+ComCode+"','Pl')=1 "
				 + " AND AppFlag = '1' "
				 + " AND ( PrintCount < 1 OR PrintCount IS NULL )"
				 + " and riskcode in (select riskcode from LMRiskApp where NotPrintPol = '0')"
				 + getWherePart( 'PolNo' )
				 + getWherePart( 'PrtNo' )
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'SaleChnl' )
				 + strManageComWhere
				 + " and ManageCom like '" + ComCode + "%%'";
				 + "AND NOT EXISTS ( SELECT PolNo FROM LCPol WHERE A.PrtNo = PrtNo AND AppFlag <> '1' ) "
				 + "order by ManageCom,AgentGroup,AgentCode";
			 
	turnPage.strQueryResult  = easyQueryVer3(strSQL);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return alert("未查询到满足条件的数据信息");
	}
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}
