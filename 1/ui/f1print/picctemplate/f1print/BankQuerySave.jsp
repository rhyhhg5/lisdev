<!--用户校验类
查询划款状态的后台处理
-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="java.sql.*"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%
//接收信息，并作校验处理。
  System.out.println("---BankQuery database---");

  //输入参数
  QueryStateUI tQueryStateUI = new QueryStateUI();
  TransferData tTransferData = new TransferData();

  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";

  try
  {
    // 准备传输数据 VData
    String polno = request.getParameter("PolNo2");
    String AppName = request.getParameter("AppName");
    String InsuName = request.getParameter("InsuName");
    String BankCode = request.getParameter("BankCode2");
    
    tTransferData.setNameAndValue("PolNo",polno);
    tTransferData.setNameAndValue("AppntName",AppName);
    tTransferData.setNameAndValue("InsuredName",InsuName);
    tTransferData.setNameAndValue("BankCode",BankCode);
    
    VData tVData = new VData();
    tVData.add(tTransferData);

    System.out.println("--BankQuerySave.jsp--start submitData...");
    if(!tQueryStateUI.submitData(tVData,""))
    {
      Content = " 查询失败，原因是: " + tQueryStateUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
    %>
	   <script language="javascript">
	        parent.fraInterface.initStateGrid();
	   </script>
    <%  
    }
    else
    {
      tVData.clear();
      tVData = tQueryStateUI.getResult();
//      SSRS t_ssrs = new SSRS();
//      t_ssrs = ((SSRS)tVData.getObjectByObjectName("SSRS",0));
      String state[][] = (String [][])tVData.get(0);
//			int n = t_ssrs.getMaxRow() ;
//      String Strtest =   t_ssrs.encode();
      System.out.println("get report "+state.length);
      String Strtest =   tQueryStateUI.encode(state,state.length,8);
      System.out.println("QueryResult: " + Strtest);

   %>
   <script language="javascript">
     try
     {
       parent.fraInterface.displayQueryResult('<%=Strtest%>');
     }
     catch(ex) {}
   </script>
   <%   
    }
  }
  catch(Exception ex)
  {
    Content = "查询失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

	//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tQueryStateUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 查询成功! ";
      FlagStr = "Success";
    }
    else
    {
      Content = " 查询失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

  //添加各种预处理
  System.out.println(Content);
%>
