<html>
<%@page contentType="text/html; charset=GBK" %>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
    String strEdorAcceptNo = request.getParameter("EdorAcceptNo");
    String strErrInfo = "";
    
    String strType = request.getParameter("type");
    System.out.println("prtType: " + strType);
    
    //Get the path of VTS file from LDSysVar table	
    LDSysVarDB tLDSysVarDB = new LDSysVarDB();
 
    String strSql1 = "select * from ldsysvar where Sysvar='VTSFilePath'";
    LDSysVarSet tLDSysVarSet = tLDSysVarDB.executeQuery(strSql1);    
    LDSysVarSchema tLDSysVarSchema = tLDSysVarSet.get(1);
    String strFilePath = tLDSysVarSchema.getV("SysVarValue");
    String strVFFileName = strFilePath + FileQueue.getFileName()+".vts";
    
    String strSql2 = "select * from ldsysvar where Sysvar='VTSRealPath'";
    tLDSysVarSet = tLDSysVarDB.executeQuery(strSql2);    
    tLDSysVarSchema = tLDSysVarSet.get(1);
    String strRealPath = tLDSysVarSchema.getV("SysVarValue");
    String strVFPathName = strRealPath + strVFFileName;
    
    CombineVts tcombineVts=null;
    InputStream ins=null;
  
    if( strEdorAcceptNo != null && !strEdorAcceptNo.equals("") ) {  // 合并VTS模板文件与数据文件存入服务器磁盘中
        // 建立数据库连接
      try{
         Connection conn = DBConnPool.getConnection();
         Statement stmt = null;
         ResultSet rs = null;
    
         if( conn == null ) {
             strErrInfo = "连接数据库失败";
         } else{

               stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
               if (strType!=null && strType.equals("CashValue")) {
                       rs = stmt.executeQuery("SELECT * FROM LPEDORAPPPRINT3 WHERE EdorAcceptNo = '" + strEdorAcceptNo + "' and EdorType='1'");
               }
	       else {
                       rs = stmt.executeQuery("SELECT * FROM LPEDORAPPPRINT WHERE EdorAcceptNo = '" + strEdorAcceptNo + "'");
               }
               
               if( rs.next() )
               {
              
                   //输出数据文件
                   COracleBlob tCOracleBlob = new COracleBlob();
                   Blob tBlob = null;
                   
                   String tSQL = " and EdorAcceptNo = '" + strEdorAcceptNo + "'";
                   if (strType!=null && strType.equals("CashValue")) {
                       System.out.println("==> 现金价值");
      	               tBlob = tCOracleBlob.SelectBlob("LPEDORPRINT3","edorinfo",tSQL,conn);
      	           }
      	           else {
	               System.out.println("==> 个人批单");
                       tBlob = tCOracleBlob.SelectBlob("LPEDORAPPPRINT","edorinfo",tSQL,conn);
                   }
      	           
      	           //BLOB blob = (oracle.sql.BLOB)tBlob;//delete by jianglai	at 2004-7-22 18:08
      	           
                   //BLOB blob = ((OracleResultSet)rs).getBLOB("edorinfo");
                   ins=tBlob.getBinaryStream();
                   System.out.println("get stream object");

                   } else{
                         System.out.println("can't get stream object");
                         }
               rs.close();
               stmt.close();
               conn.close();
                
                               
               //合并VTS文件 
               String strTemplatePath = application.getRealPath("f1print/template/") + "/";
               tcombineVts = new CombineVts(ins,strTemplatePath);
               
               ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
               tcombineVts.output(dataStream);
               
               //把dataStream存储到磁盘文件
               AccessVtsFile.saveToFile(dataStream,strVFPathName);
               System.out.println("==> Write VTS file to disk ");
              
               //session.putValue("FileName", strVFFileName);
               //System.out.println("===strVFFileName : "+strVFFileName);
               session.putValue("RealPath", strVFPathName);
               //System.out.println("===strVFPathName : "+strVFPathName);
               response.sendRedirect("GetF1PrintJ1.jsp");
               }//else
      }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                }
    }    
     else{
          strErrInfo = "没有输入保全号";
          }
%>
<body>
<%= strErrInfo %>
</body>
</html>
