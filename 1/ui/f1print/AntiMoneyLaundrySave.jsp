<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：BatchSendListSave.jsp
//程序功能：邮储代收代付正确清单
//创建人  ：亓莹莹
//创建日期：
//更新记录：  
//更新人
//更新日期
//更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bank.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%
  //接收数据并进行封装
  String strStartDate = request.getParameter("StartDate");
  String strEndDate = request.getParameter("EndDate");
  String strBankType = request.getParameter("BankType");//银行代码
  String strManageCom = request.getParameter("ManageCom");//管理机构
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("StartDate", strStartDate);
  tTransferData.setNameAndValue("EndDate", strEndDate);
  tTransferData.setNameAndValue("BankType", strBankType);
  tTransferData.setNameAndValue("ManageCom", strManageCom);

  //输出参数
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  boolean operFlag=true;
  VData tVData = new VData();
  GlobalInput tG = (GlobalInput) session.getValue("GI");
  tVData.add(tTransferData);
  tVData.addElement(tG);
  AntiMoneyLaundryUI mAntiMoneyLaundryUI = new AntiMoneyLaundryUI();
  //定义一个报表的类
  XmlExport txmlExport = new XmlExport();
  VData mResult = new VData();
  try
  {
    //调用批单打印的类
    if (!mAntiMoneyLaundryUI.submitData(tVData, "PRINT"))
    {
      operFlag = false;
      Content = mAntiMoneyLaundryUI.mErrors.getFirstError().toString();
    }
    else
    {
      mResult = mAntiMoneyLaundryUI.getResult();
      txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);
      if (txmlExport == null)
      {
         operFlag = false;
         Content = "没有得到要显示的数据文件";
      }
    }
  }
  catch (Exception ex)
  {
     Content = "PRINT" + "失败，原因是:" + ex.toString();
     FlagStr = "Fail";
  }
           
//获取临时文件名
  ExeSQL tExeSQL = new ExeSQL();
  String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
  String strFilePath = tExeSQL.getOneValue(strSql);
  String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
//获取存放临时文件的路径
  String strRealPath = application.getRealPath("/").replace('\\','/');
  String strVFPathName = strRealPath + "/" + strVFFileName;
  CombineVts tcombineVts = null;
 if (operFlag==true)
 {
    //合并VTS文件
    String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
    tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    tcombineVts.output(dataStream);
    //把dataStream存储到磁盘文件
    AccessVtsFile.saveToFile(dataStream,strVFPathName);
    System.out.println("===strVFFileName : "+strVFFileName);
    //本来打算采用get方式来传递文件路径
    response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="+strVFPathName);
  }
  else
  {
    FlagStr = "Fail";
%>
<html>
<script language="javascript">
  alert("<%=Content%>");
  top.opener.focus();
  top.close();              
</script>
</html>
<%
}
%>