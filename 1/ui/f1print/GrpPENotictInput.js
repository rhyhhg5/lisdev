//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;
window.onfocus=myonfocus;
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		arrReturn = getQueryResult();
		PrtSeq = PolGrid.getRowColData(tSel-1,1);
		
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		var count = 0;
		var strSql = "select count(1) from loprtmanager where code='03' and standbyflag3=(select grpcontno from lcgrpcont where prtno='"+PrtSeq+"')";
		var arr = easyExecSql(strSql);
		if (arr)
		{
			count = arr[0][0];
		}
		else
			{
				alert("无效的数据");
				return;
			}
		var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");     
		var strSQL = "select * from loprtmanager where code='03' and standbyflag3=(select grpcontno from lcgrpcont where prtno='"+PrtSeq+"')"
		fm.action="../batchprint/NBBPrintSaveWH.jsp?strSql="+strSQL+"&fmtransact=PRINT&intRecordNo="+count;
		fm.submit();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
		if(!verifyInput2())
	return false;
	
	var strManageComWhere = " ";
//if( fm.BranchGroup.value != '' ) {
//	strManageComWhere += " AND LAAgent.AgentGroup IN" + 
//											" ( SELECT AgentGroup FROM LABranchGroup WHERE BranchAttr LIKE '" +
//											fm.BranchGroup.value + "%%') ";
//}
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select distinct b.prtno,b.GrpName,b.agentcode,b.agentgroup,'',b.managecom"
	+" from  loprtmanager a,lcgrpcont b where b.grpcontno=a.standbyflag3  and a.code='03' " 
	//+ "and LAAgent.AgentCode = LOPRTManager.AgentCode "
	//+ "and LABranchGroup.AgentGroup = LAAgent.AgentGroup "
	//+ "and LOPRTManager.OtherNo = LCGrpCont.Proposalgrpcontno "	
	+ getWherePart('b.prtno', 'prtno') 
	+ getWherePart('b.ManageCom', 'ManageCom', 'like')
	+ getWherePart('b.AgentCode','AgentCode')
	+ getWherePart('b.AgentGroup','AgentGroup')
	+ " AND b.ManageCom LIKE '" + comcode + "%%'"
	//+ strManageComWhere
	+" AND a.StateFlag = '0' AND a.PrtType = '0'";
	  
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  //alert(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要打印的团体体检件通知书！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}

//function afterSubmit(FlagStr,Content)
//{
//  showInfo.close();
//  if (FlagStr == "Fail" )
//  {             
//    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
//  }
//  else
//  { 
//    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
//    //showDiv(operateButton,"true"); 
//    //showDiv(inputButton,"false"); 
//    //执行下一步操作
//  }
//}

function afterQuery(arrResult)
{
  
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
}