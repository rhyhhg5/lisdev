//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet1,arrDataSet2; 
var turnPage = new turnPageClass();


var old_AgentGroup="";
var new_AgentGroup="";

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAComQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
     

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function DetailPrint()
{
  fm.action="../agentprint/LAIndexInfoQueryPrt.jsp";
  fm.target="f1print";
  submitForm();
}

//提交，保存按钮对应操作
function submitForm()
{  	
  	var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.target='f1print';
	  fm.submit(); //提交
	  showInfo.close();	
}

function DoSDownload()
{
	if(!verifyInput())
	{
		return false;
	}
	// 薪资计算校验
	if(!checkIndex())
	{
		return false;
	}
    
  	var tEndNo = trim(fm.all('IndexCalNo').value);
	var tEndDate = tEndNo.substr(0,4)+"-"+tEndNo.substr(4,2)+"-01" ;
	var getStartDate = "select date_format(date('"+tEndDate+"') - 2 month,'yyyymm') from dual where 1=1 ";
	var strQueryResult = easyQueryVer3(getStartDate, 1, 1, 1);
	if(!strQueryResult)
     {
        alert("考核期计算有误，烦请核实页面录入所属年月字段是否有误！");
        return false;
     }
    var arr = decodeEasyQueryResult(strQueryResult);
	var tStartNo = arr[0][0];
     
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	
  var tsql = "";
  tsql = "select a.managecom,"
         +"(select name from ldcom where comcode =a.managecom),"
         +"(select branchattr from labranchgroup where agentgroup = a.agentgroup),"
         +"(select name from labranchgroup where agentgroup = a.agentgroup),"
         +"getunitecode(a.agentcode),"
         +"(select name from laagent where agentcode = a.agentcode),"
         +"(select agentgrade from latree where agentcode = a.agentcode),"
         +"(select gbuildstartdate from laagent where agentcode = a.agentcode),"
         +"a.indexcalno,"
         +"(select db2inst1.MONTHS_BETWEEN(char((select startdate from lastatsegment where stattype='1' and yearmonth = int(a.indexcalno))),char((select gbuildstartdate from laagent where agentcode = a.agentcode))) + 1 from dual),"
         +"a.T31,"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'A' then varchar(a.T40) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t33) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then "
         +"( select char(count(1))  from laindexinfo  where branchtype = '1' and branchtype2 = '01' and indextype = '01' and indexcalno <= '"+tEndNo+"' and indexcalno >='"+tStartNo+"' and t67 = 99 and agentgrade like 'A%' " 
         +" and exists( select 1 from laagent where agentcode = laindexinfo.agentcode and branchcode = (select branchcode from laagent where agentcode = a.agentcode)) ) else '--' end ),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t34) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t35) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t42) else '--' end),"
         +"(case (select t67 from laindexinfo where indextype ='01' and indexcalno = a.indexcalno  and agentcode = a.agentcode ) when 99 then 'Y' else 'N' end), "
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'A' then varchar(a.T32) else '--' end) ,"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'A' then varchar(a.T41) else '--' end) ,"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t38) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t38) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.T43) else '--' end),"
         +"( case (select t66 from laindexinfo where indextype ='01' and indexcalno = a.indexcalno  and agentcode = a.agentcode ) when 99 then 'Y' when 98 then 'N' else '未考核' end) "
         +" from laindexinfo a"
         +" where 1=1 "
         +" and a.branchtype = '1'"
         +" and a.branchtype2 = '01'"   
         +" and a.indextype = '00'"   
         +" and exists(select 1 from laindexinfo where indextype = '01' and agentcode = a.agentcode and indexcalno = a.indexcalno and t67 in (99,98)) "   
         // + getWherePart("a.ManageCom","ManageCom")
         + getWherePart("a.IndexCalNo","IndexCalNo")
         //+ getWherePart("a.AgentCode","AgentCode")
         +strAgent
         if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
         {
           tsql+=" and a.agentgroup = (select agentgroup from labranchgroup where branchtype = '1' and branchtype2 = '01' and branchattr = '"+fm.all('BranchAttr').value+"')";
         }
         if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!="")
         {
           tsql+=" and a.managecom like '"+fm.all('ManageCom').value+"%'";
         }
 
        fm.querySql.value = tsql;

       //定义列名
       var strSQLTitle = "select '机构编码','机构名称','团队编码','团队名称','代理人编码','代理人姓名','代理人职级','团建起始日期','薪资年月','考核期','季度个人新单期缴保费','季度个人新单传统期缴保费','季度末架构人力','季度内标准化营销人员','团队月均活动率','营业处季度新单期缴保费','营业处季度新单传统期缴保费','季度团建考核是否达标','个人年度新单期缴保费','个人年度新单传统期缴保费','营业处年度新单期缴保费','营业处年度新单传统期缴保费','年度末架构人力','年度团建考核是否达标'  from dual where 1=1 ";
       fm.querySqlTitle.value = strSQLTitle;
  
       //定义表名
       fm.all("Title").value="select '团建考核指标明细' from dual where 1=1  ";  
  
       fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
       fm.submit();
}

// 查询按钮
function easyQueryClick()
{
	// 简单页面信息校验
	if(!verifyInput())
	{
		return false;
	}
	// 薪资计算校验
	if(!checkIndex())
	{
		return false;
	}
    initIndexInfoGrid1();
    
  	var tEndNo = trim(fm.all('IndexCalNo').value);
	var tEndDate = tEndNo.substr(0,4)+"-"+tEndNo.substr(4,2)+"-01" ;
	var getStartDate = "select date_format(date('"+tEndDate+"') - 2 month,'yyyymm') from dual where 1=1 ";
	var strQueryResult = easyQueryVer3(getStartDate, 1, 1, 1);
	if(!strQueryResult)
     {
        alert("考核期计算有误，烦请核实页面录入所属年月字段是否有误！");
        return false;
     }
	var cstrAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		cstrAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
    var arr = decodeEasyQueryResult(strQueryResult);
	var tStartNo = arr[0][0];    
  var tsql = "";
  tsql = "select a.managecom,"
         +"(select name from ldcom where comcode =a.managecom),"
         +"(select branchattr from labranchgroup where agentgroup = a.agentgroup),"
         +"(select name from labranchgroup where agentgroup = a.agentgroup),"
         +"getunitecode(a.agentcode),"
         +"(select name from laagent where agentcode = a.agentcode),"
         +"(select agentgrade from latree where agentcode = a.agentcode),"
         +"(select gbuildstartdate from laagent where agentcode = a.agentcode),"
         +"a.indexcalno,"
         +"(select db2inst1.MONTHS_BETWEEN(char((select startdate from lastatsegment where stattype='1' and yearmonth = int(a.indexcalno))),char((select gbuildstartdate from laagent where agentcode = a.agentcode))) + 1 from dual),"
         +"a.T31,"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'A' then varchar(a.T40) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t33) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then "
         +"( select char(count(1))  from laindexinfo  where branchtype = '1' and branchtype2 = '01' and indextype = '01' and indexcalno <= '"+tEndNo+"' and indexcalno >='"+tStartNo+"' and t67 = 99 and agentgrade like 'A%' " 
         +" and exists( select 1 from laagent where agentcode = laindexinfo.agentcode and branchcode = (select branchcode from laagent where agentcode = a.agentcode)) ) else '--' end ),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t34) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t35) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t42) else '--' end),"
         +"(case (select t67 from laindexinfo where indextype ='01' and indexcalno = a.indexcalno  and agentcode = a.agentcode ) when 99 then 'Y' else 'N' end), "
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'A' then varchar(a.T32) else '--' end) ,"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'A' then varchar(a.T41) else '--' end) ,"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t38) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.t37) else '--' end),"
         +"(case substr((select agentgrade from latree where agentcode = a.agentcode),1,1) when 'B' then varchar(a.T43) else '--' end),"
         +"( case (select t66 from laindexinfo where indextype ='01' and indexcalno = a.indexcalno  and agentcode = a.agentcode ) when 99 then 'Y' when 98 then 'N' else '未考核' end) "
         +" from laindexinfo a"
         +" where 1=1 "
         +" and a.branchtype = '1'"
         +" and a.branchtype2 = '01'"   
         +" and a.indextype = '00'"   
         +" and exists(select 1 from laindexinfo where indextype = '01' and agentcode = a.agentcode and indexcalno = a.indexcalno and t67 in (99,98)) "   
         //+ getWherePart("a.ManageCom","ManageCom")
         + getWherePart("a.IndexCalNo","IndexCalNo")
//         + getWherePart("a.AgentCode","AgentCode")
         + cstrAgent
         if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
         {
           tsql+=" and a.agentgroup = (select agentgroup from labranchgroup where branchtype = '1' and branchtype2 = '01' and branchattr = '"+fm.all('BranchAttr').value+"')";
         }
         if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!="")
         {
           tsql+=" and a.managecom like '"+fm.all('ManageCom').value+"%' ";
         }
  turnPage.strQueryResult  = easyQueryVer3(tsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有查询到符合条件的考核指标！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage.strQueryResult);
  
turnPage.arrDataCacheSet = arrDataSet1;

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = IndexInfoGrid1;    
          
  //保存SQL语句
  turnPage.strQuerySql     = tsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  
}


function getQueryResult1()
{
  var arrSelected = null;
  tRow = IndexInfoGrid1.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet1 == null )
    return arrSelected;
  arrSelected = new Array();
  tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
  //设置需要返回的数组
  arrSelected[0] = arrDataSet1[tRow-1];
  return arrSelected;
}

function checkIndex()
{
//   var tIndexCalNo = trim(fm.all('IndexCalNo').value);
//   var tManageCom = trim(fm.all('ManageCom').value);
//   
//   var checkSQL = "select distinct 1 from lawagetemp where 1=1 "
//        +" and branchtype = '1' and branchtype2 = '01'"
//   		+" and managecom = '"+tManageCom+"'" 
//   		+" and indexcalno = '"+tIndexCalNo+"'";
//   		
//   var strQueryResult = easyQueryVer3(checkSQL, 1, 1, 1);
//   if(!strQueryResult)
//   {
//      alert("机构"+tManageCom+","+tIndexCalNo+"月尚未进行团建考核计算,请计算后再进行考核指标查询操作！");
//      return false;
//   }

   return true;
}









