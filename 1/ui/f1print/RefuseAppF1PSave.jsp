<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
	GlobalInput tG = (GlobalInput)session.getValue("GI");

	LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
	
	tLOPRTManagerSchema.setPrtSeq(request.getParameter("PrtSeq"));

  CError cError = new CError( );

  //后面要执行的动作：添加，修改，删除
  String strOperation = request.getParameter("fmtransact");

	VData tVData = new VData();
	VData mResult = new VData();

  tVData.addElement(tG);
  tVData.addElement(tLOPRTManagerSchema);

	String strErrMsg = "";
  RefuseAppF1PUI tRefuseAppF1PUI = new RefuseAppF1PUI();

	if(!tRefuseAppF1PUI.submitData(tVData,strOperation)) {
		if( tRefuseAppF1PUI.mErrors.needDealError() ) {
			strErrMsg = tRefuseAppF1PUI.mErrors.getFirstError();
		} else {
			strErrMsg = "RefuseAppF1PUI发生错误，但是没有提供详细的出错信息";
		}
%>
		<script language="javascript">
			alert('<%= strErrMsg %>');
			window.opener = null;
			window.close();
		</script>
<%
		return;
  }

  mResult = tRefuseAppF1PUI.getResult();
  
	XmlExport txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	
	if (txmlExport==null) {
	  System.out.println("null");
	}
	session.putValue("PrintStream", txmlExport.getInputStream());
	System.out.println("put session value");
	response.sendRedirect("GetF1Print.jsp");
%>