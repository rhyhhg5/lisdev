<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：StatHospCircsByGrade.jsp
//程序功能：分合作级别医院设置情况统计
//创建日期：2010-04-01
//创建人  ：石和平
//更新记录：  
//更新人:    
//更新日期:     
//更新原因:
//内容:
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%
	 //接收信息，并作校验处理。
	 CErrors mErrors =new CErrors();
	 StatHospCircsByGradeUI tStatHospCircsByGradeUI = new StatHospCircsByGradeUI();

	 //输出参数
	 CErrors tError = null;
	 String tRela  = "";                
	 String FlagStr = "";
	 String Content = "";
	 String transact = "";
	 boolean operFlag=true;
 
	 GlobalInput tG = new GlobalInput(); 
	 tG=(GlobalInput)session.getValue("GI");
 
	 String tStartDate = request.getParameter("StartDate"); //开始日期
	 String tEndDate = request.getParameter("EndDate"); //结束日期
	 String tManageCom = request.getParameter("ManageCom"); //统计机构编码
	 String tComName = request.getParameter("ComName"); //统计机构名称
 
	 TransferData tTransferData = new TransferData();
 
	 tTransferData.setNameAndValue("StartDate",tStartDate);
	 tTransferData.setNameAndValue("EndDate",tEndDate);
	 tTransferData.setNameAndValue("ManageCom",tManageCom);
	 tTransferData.setNameAndValue("ComName",tComName);
 
  	 VData mResult = new VData();
 	 XmlExport txmlExport = new XmlExport();
 	 
	 try
	 {
	  //准备传输数据VData
	  VData tVData = new VData();  
	     
	  tVData.add(tG);
	  tVData.add(tTransferData);
	  if(!tStatHospCircsByGradeUI.submitData(tVData,transact)){
		  	operFlag = false;
	  } else {
		    mResult = tStatHospCircsByGradeUI.getResult();			
		  	txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
		  	  
		  	TransferData tvtsStyleParme = new TransferData();
			tvtsStyleParme = (TransferData)mResult.getObjectByObjectName("TransferData",0);
			
		  	if (txmlExport == null){
		  		operFlag = false;
		  	    System.out.println("null");
		  	}	  
		 
		  	ExeSQL tExeSQL = new ExeSQL();
			//获取临时文件名
		  	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
		  	String strFilePath = tExeSQL.getOneValue(strSql);
		  	String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
		  	System.out.println("strVFFileName-->>:"+strVFFileName);
	
		  	String strRealPath = application.getRealPath("/").replace('\\','/');
		  	String strVFPathName = strRealPath +"//"+ strVFFileName;
	
		  	CombineVts tcombineVts = null;
		  	if (operFlag==true){
		  		VData vtsStyleParmes = new VData();
		  		vtsStyleParmes.add(tvtsStyleParme); 
		  		  
		  		//合并VTS文件
		  		String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "//";
		  		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
		  		//tcombineVts.setVtsStyleParam(true,vtsStyleParmes);
		  		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		  		tcombineVts.output(dataStream);
	
		  		//把dataStream存储到磁盘文件
		  		System.out.println("存储文件到"+strVFPathName);
		  		AccessVtsFile.saveToFile(dataStream,strVFPathName);
		  		System.out.println("==> Write VTS file to disk ");
		  		session.putValue("RealPath", strVFPathName);
		  		session.putValue("flag", "UIS");	 
		  		session.putValue("strRealPath",strRealPath);
		  		System.out.println("RealPath:"+strVFPathName);
		  		
		  		session.putValue("PrintStream", txmlExport.getInputStream());
		  		  	
		  		request.getRequestDispatcher("../f1print/GetF1PrintJ1.jsp?RealPath="+strVFPathName+"").forward(request,response);		  		
		  		}
			 }
	  }	catch(Exception ex){
		      operFlag=false;
		  	  mErrors=tStatHospCircsByGradeUI.mErrors;
		  	  Content = "打印失败，原因是:"+ mErrors.getFirstError();
%>
<html>
   <script language="javascript">	
		 alert("<%=Content%>");
		 top.close(); 		
   </script>
	 <%
	  }
	 %>		
</html>