<%@page contentType="text/html;charset=GBK" %>
<html>
<%
    GlobalInput tpGI = new GlobalInput();
	tpGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tpGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tpGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="LPPayEndorse.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="LPPayEndorseInit.jsp"%>
  <%@include file = "../bq/ManageComLimit.jsp"%>

  <title>保全交费通知书查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./LCPolQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLCPol1" style= "display: ''">
      <table  class= common>
       <TR class=common>
          <TD  class= title> 保全批单号 </TD>
          <TD  class= input> <Input class= common name=EdorNo > </TD>
          <TD  class= title>  合同号码   </TD>
          <TD  class= input>  <Input class= common name=PolNo > </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
                  </TR> 
         <TR  class= common>
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
          <TD  class= title>  </TD>
          <TD  class= input>  <Input class="code" type=hidden name=AgentGroup ondblclick="return showCodeList('AgentGroup',[this]);" onkeyup="return showCodeListKey('AgentGroup',[this]);">   </TD>
          <TD  class= title>  </TD>
          <td class="input" nowrap="true">
            <Input class="common" name="BranchGroup" type=hidden>
			<input name="btnQueryBranch" class="common" type=hidden type="button" value="?" onclick="queryBranch()" style="width:20">
          </TD>  
        </TR>  
      </table>
    </Div>
          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
          <!--INPUT VALUE="返回" TYPE=button onclick="returnParent();"--> 			
  </form>		
  <form action="./EdorFeeF1PSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol2);">
    		</td>
    		<td class= titleImg>
    			 保全交费信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanLCPolGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
       <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 						
  	</div>
  	<br/>
  	<INPUT VALUE="打印保全交费通知书" class= common TYPE=button onclick="PrintEdor();"> 
  	<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="GetNoticeNo" name="GetNoticeNo">
		<input type=hidden id="OtherNo" name="OtherNo">
		<input type=hidden id="AppntNo" name="AppntNo">
		<input type=hidden id="SumDuePayMoney" name="SumDuePayMoney">
		<input type=hidden id="Agentcode1" name="Agentcode1">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
