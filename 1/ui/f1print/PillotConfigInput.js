var showInfo;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

// 查询机构当前的状态
function easyQuery() {

	initPilotGrid();
	var strSQL = " select code,codename,case othersign when '0' then '非试点' else '试点' end "
			+ "  from ldcode "
			+ " where codetype='pilotconfig' "
			+ getWherePart("Code", "ManageCom")
			+ getWherePart("OtherSign", "ConfigFlag");
	turnPage1.queryModal(strSQL, PilotGrid);
	if (PilotGrid.mulLineCount <= 0) {
		alert("没有查询到相关信息！");
	}
}

// 添加为试点
function addToPilot() {
	var checkFlag = 0;
	 checkFlag = PilotGrid.getSelNo();
	var code = PilotGrid.getRowColData(checkFlag-1, 1);
	if (checkFlag == 0) {
		alert("请选择一条记录,在进行添加试点操作");
		return false;
	}
	var strSQL1 = "select othersign   from ldcode  where  codetype='pilotconfig' and code='"
			+ code + "'";
	ToPilot = easyExecSql(strSQL1);
	if (ToPilot==1) {
		alert("该条记录已经为试点,不能进行添加操作");
		return false;
	}
	  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action = "PillotConfigSave.jsp?Action=addToPilot&ManageCom="+code+"";
	fm.submit();
	// fm.action = oldAction;
}

// 取消试点
function removeFromPilot() {
	var checkFlag = 0;
	 checkFlag = PilotGrid.getSelNo();
	var code = PilotGrid.getRowColData(checkFlag-1, 1);
	if (checkFlag== 0) {
		alert("请选择一条记录,在进行取消试点操作");
		return false;
	}
	var strSQL2 = "select othersign   from ldcode  where  codetype='pilotconfig' and code='"
			+ code + "'";
	ToPilot = easyExecSql(strSQL2);

	if (ToPilot==0) {
		alert("该条记录为非试点,不能进行取消操作");
		return false;

	}
	  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action = "PillotConfigSave.jsp?Action=removeFromPilot&ManageCom="+code+"";
	fm.submit();
	// fm.action = oldAction;

}

function afterSubmit(FlagStr, content) {
	window.focus();
    showInfo.close();	
       
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px"); 
  	top.window.focus();
  	easyQuery();  
}