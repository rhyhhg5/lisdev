<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="java.io.*"%>
<%
	System.out.println("start");
	String tStartDay="";
	String tEndDay="";
	String FYC="";
	String ManageCom = "";
	String Operate = request.getParameter("Operate");
	String tManagecomtype ="";
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	CError cError = new CError( );
	CErrors tError = null;
	XmlExport txmlExport = new XmlExport();
	CombineVts tcombineVts = null;
	//后面要执行的动作：添加，修改，删除	  
	String FlagStr = "";
	String Content = "";
	tStartDay=request.getParameter("StartDate");
	tEndDay=request.getParameter("EndDate");	
	String tManageCom = request.getParameter("ManageCom");	
	String tBranchAttr = request.getParameter("BranchAttr");	
	String tAgentCode = request.getParameter("AgentCode");	
	String tRiskCode = request.getParameter("RiskCode");	
	String tPrintType = request.getParameter("PrintType");	
	
	// 生成报表名称
	Calendar cal = new GregorianCalendar();
	String year = String.valueOf(cal.get(Calendar.YEAR));
	String month=String.valueOf(cal.get(Calendar.MONTH)+1);
	String date=String.valueOf(cal.get(Calendar.DATE));
	String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
	String min=String.valueOf(cal.get(Calendar.MINUTE));
	String sec=String.valueOf(cal.get(Calendar.SECOND));
	String now = year + month + date + hour + min + sec + "_" ;
	  
	String  millis = String.valueOf(System.currentTimeMillis());  
	String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
	String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
 
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	tVData.addElement(tStartDay);
	tVData.addElement(tEndDay);
	tVData.addElement(tManageCom);
	tVData.addElement(tBranchAttr);
	tVData.addElement(tAgentCode);
    tVData.addElement(tRiskCode);
    tVData.addElement(tPrintType);
    tVData.addElement(tOutXmlPath);// 存储路径
  
    try
    {
    	LAAgentPremReportBL tLAAgentPremReportBL = new LAAgentPremReportBL();
        if (!tLAAgentPremReportBL.submitData(tVData, ""))
        {
            Content = "报表下载失败，原因是:" + tLAAgentPremReportBL.mErrors.getFirstError();
            FlagStr = "Fail";
        }
  	  else
  	  {
  	 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
  	 	  File file = new File(tOutXmlPath);
  	 	  
  	        response.reset();
            response.setContentType("application/octet-stream"); 
            response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
            response.setContentLength((int) file.length())	;
        
            byte[] buffer = new byte[10000];
            BufferedOutputStream output = null;
            BufferedInputStream input = null;    
            //写缓冲区
            try 
            {
                output = new BufferedOutputStream(response.getOutputStream());
                input = new BufferedInputStream(new FileInputStream(file));
          
            int len = 0;
            while((len = input.read(buffer)) >0)
            {
                output.write(buffer,0,len);
            }
            input.close();
            output.close();
            }
            catch (Exception e) 
            {
              e.printStackTrace();
             } // maybe user cancelled download
            finally 
            {
                if (input != null) input.close();
                if (output != null) output.close();
                file.delete();
            }
  	   }
  	}
  	catch(Exception ex)
  	{
  	  ex.printStackTrace();
  	}
    
    if (!FlagStr.equals("Fail"))
    {
    	Content = "";
    	FlagStr = "Succ";
    }
%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();	
	//window.opener.afterSubmit("<%=FlagStr%>","<%=Content%>");		
</script>
</html>
