//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	 
   if (verifyInput() == false)
    return false;
      	 
    if( !beforeSubmit())
   {
   	return false;
   	
   	}	

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function beforeSubmit()
{
 
	return true;
	
}


function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function easyQueryClick() 
{

 　if (verifyInput() == false)
     return false;
  //此处书写SQL语句			     
  var tReturn = parseManageComLimitlike();
  	 
  var tManageCom=fm.all('ManageCom').value;
  var tAgentCode=fm.all('AgentCode').value;
   
  var tIndexCalNo=fm.all('AssessYear').value+fm.all('AssessMonth').value;
  
   
  var tSQL = "";

    tSQL  = "select a,b,c,d,e,f,g,value(h,0),value(i,0),case when value(j,0)<0 then 0 else value(j,0) end,value(k,0),value(l,0) j," +
    		"case when value(m,0)<0 then 0 else value(m,0) end,value(n,0),value(o,0),case when value(p,0)<0 then 0 else value(p,0) end," +
    		"value(q,0),case when value(r,0)<0 then 0 else value(r,0) end,value(s,0),value(t,0), case when value(u,0)<0 then 0 else value(u,0) end," +
    		"value(v,0),value(w,0),case when value(x,0)<0 then 0 else value(x,0) end,value(y,0),value(z,0),case when value(z-y,0)<0 then 0 else value(z-y,0) end  "
              + "    from ( "
             + "select getUniteCode(agentcode) a ,"
             +"(select name from laagent b where a.agentcode=b.agentcode) b,"
             + "branchattr c,"
             +"(select name from labranchgroup b where a.agentgroup=b.agentgroup) d,"
             +"agentgrade e,"            
             +"(select b.employdate from laagent b where a.agentcode=b.agentcode) f,"
             +"(select b.startdate from latree b where a.agentcode=b.agentcode) g ,"
             +"value(MngAgentCount+DRTeamMonLabor,0) h,"
             +"5 i,"
             +"value(5-MngAgentCount-DRTeamMonLabor,0) j,"
             +"decimal((DirTeamFYCSum+DRFYCSum+T36),12,2) k,"
             +"decimal(T65/3*T33,12,2) l,"
             +"decimal(T65/3*T33,12,2)-decimal((DirTeamFYCSum+DRFYCSum+T36),12,2) m,"
             +"value(T52*100,0) n,"
             +"75 o,"
             +"75-value(T52*100,0) p,"
             +"75 q,"
             +"(select 75-value(T52*100,0) from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') r,"
             +"(select DInRCount from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') s,"           
             +"5 t,"
             +"(select 5-DInRCount from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') u,"
             +"(select DRTeamCount from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') v,"
             +"3 w,"
             +"(select 3-DRTeamCount from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') x, "
             +"(select decimal((DirTeamFYCSum +DRFYCSum),12,2) from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') y,"           
             +"(select decimal(T65/3*T33,12,2) from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') z "
                   
             +" from laindexinfo a where indextype='02'  and IndexCalno='"+tIndexCalNo+"'"    ;
       
       tSQL += "  and AgentGrade>='B02' and AgentGrade<'B11' and branchtype='1' and  branchtype2='01' " 
       + tReturn
      + getWherePart('ManageCom','ManageCom','like')
//      + getWherePart('AgentCode','AgentCode' )
//      modify lyc 2014-11-27 统一工号
       if(fm.all("AgentCode").value!=""){
    	   tSQL +=" and AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
       }
       tSQL += " order by ManageCom,BranchAttr,AgentCode ) as x ";
	  //  alert(tSQL);
   turnPage.queryModal(tSQL, AgentQueryGrid,1);
  if (!turnPage.strQueryResult) 
  {
    alert("没有符合条件的查询信息！");
    return false;
  }
}

function afterCodeSelect( cCodeName, Field )
{
　	  
  if(cCodeName=="comcode"){
  	 fm.all("BranchAttr").value="";
  	 fm.all("BranchName").value="";
  	if(fm.all("ManageCom").value=='86' )
  	{
  		fm.all("BranchAttr").disabled=true;
  	}
       else
       	{
       		var tManageCom=fm.all("ManageCom").value;
       		fm.all("BranchAttr").disabled=false;
       		msql1=" 1 and   branchtype=#2#  and branchtype2=#01#  and managecom like #"+tManageCom+"%# and endflag=#N#";
       	}
}
 if(cCodeName=="branchattr")
 {
   if(fm.all("ManageCom").value==null || fm.all("ManageCom").value=='')
    {
      alert("请先录入管理机构");
      fm.all("BranchAttr").value='';
       fm.all("BranchName").value='';
       return false;
  
     }
}
}        
                                                                                                                   