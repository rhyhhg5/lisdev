<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PolicyPrintSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  
  tG=(GlobalInput)session.getValue("GI");

//获得用户ip ,得到生成文件目录 
   int terror=0;
   int intIndex      = 0 ;				//一个记录字串结束的位置
   int intPosition   = 0 ;				//个数值后的^位置
   String strRecords = "";
   String servername="";
   String pathname="";
   String printer="";
   String filename="";
   //String Ip = request.getRemoteAddr();
   String Ip = request.getHeader("X-Forwarded-For");
		if(Ip == null || Ip.length() == 0) { 
		   Ip = request.getRemoteAddr(); 
		}
   Ip=Ip+".prt";
   ConfigInfo.SetConfigPath("z:\\AppConfig.properties");
   String pathfull=ConfigInfo.GetValuebyArea(Ip);
   if(!(pathfull.equals("")) && (pathfull.trim().length()>0))
  {
    intIndex = StrTool.getPos(pathfull,SysConst.PACKAGESPILTER,intPosition,1 );
    servername = pathfull.substring(intPosition ,intIndex);
  pathfull = pathfull.substring(intIndex + 1);
  intIndex = StrTool.getPos(pathfull,SysConst.PACKAGESPILTER,intPosition,1 );
  pathname = pathfull.substring(intPosition ,intIndex);

  pathfull = pathfull.substring(intIndex + 1);
  printer=pathfull;
  
  }
  else
  {
  terror=1;
  %>
  <script language="javascript">
	alert("没有指定的目录名或打印机名！");
	
   </script>
  <%
  } 

if (terror==0)
{  
  
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	LCPolSet tLCPolSet = new LCPolSet();

	String tProposalNo[] = request.getParameterValues("PolGrid1");
	String tChk[] = request.getParameterValues("InpPolGridChk");
	boolean flag = false;
	int proposalCount = tProposalNo.length;
	int ProposalCount = 10;	

	for (int j = 0; j < proposalCount; j++)
	{
		if (tProposalNo[j] != null && tChk[j].equals("1"))
		{
                   System.out.println("ProposalNo:"+j+":"+tProposalNo[j]);
	  		LCPolSchema tLCPolSchema = new LCPolSchema();
	
		    tLCPolSchema.setPolNo( tProposalNo[j] );
	    
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add( tLCPolSchema );

			// 数据传输
			ProposalQueryUI tProposalQueryUI   = new ProposalQueryUI();
			if (!tProposalQueryUI.submitData(tVData,"QUERY||DETAIL"))
			{
				Content = " 查询失败，原因是: " + tProposalQueryUI.mErrors.getError(0).errorMessage;
				FlagStr = "Fail";
			}
			else
			{       
			        TextTag texttag=new TextTag();//新建一个TextTag的实例
				XmlExport xmlexport=new XmlExport();//新建一个XmlExport的实例
				xmlexport.createDocument("prt",printer);//最好紧接着就初始化xml文档
				
				tVData.clear();
				tVData = tProposalQueryUI.getResult();
			
				LDCodeSchema tLDCodeSchema = new LDCodeSchema();
				VData tVData1 = new VData();
				CodeQueryUI tCodeQueryUI = new CodeQueryUI();
				
				// 显示
				// 保单信息
				LCPolSchema mLCPolSchema = new LCPolSchema(); 
				mLCPolSchema.setSchema((LCPolSchema)tVData.getObjectByObjectName("LCPolSchema",0));
					// 代理人姓名
					String agentName = "";
					tLDCodeSchema.setCodeType( "AGENTCODE" );
					tLDCodeSchema.setCode( mLCPolSchema.getAgentCode() );
					tVData1.clear();
					tVData1.add( tLDCodeSchema );
					if( tCodeQueryUI.submitData( tVData1, "QUERY||OTHER" ) == true )
					{
						tVData1.clear();
						tVData1 = tCodeQueryUI.getResult();
						tLDCodeSchema.setSchema((LDCodeSchema)tVData.getObjectByObjectName("LDCodeSchema",0));
						agentName = tLDCodeSchema.getCodeName();
					}
					// 操作员姓名
					String userName = "";
					tLDCodeSchema.setCodeType( "USERCODE" );
					tLDCodeSchema.setCode( mLCPolSchema.getOperator() );
					tVData1.clear();
					tVData1.add( tLDCodeSchema );
					if( tCodeQueryUI.submitData( tVData1, "QUERY||OTHER" ) == true )
					{
						tVData1.clear();
						tVData1 = tCodeQueryUI.getResult();
						tLDCodeSchema.setSchema((LDCodeSchema)tVData.getObjectByObjectName("LDCodeSchema",0));
						userName = tLDCodeSchema.getCodeName();
					}
System.out.println("agentName:"+agentName);
System.out.println("userName:"+userName);
					// 大写的金额
					String ChnPrem = "";
					ChnPrem = PubFun.getChnMoney( (double)mLCPolSchema.getPrem() );
					String ChnAmnt = "";
					ChnAmnt = PubFun.getChnMoney( (double)mLCPolSchema.getAmnt() );
System.out.println("Prem:"+ChnPrem);
System.out.println("Amnt:"+ChnAmnt);
						texttag.add("ChnPrem",ChnPrem);
						texttag.add("ChnAmnt",ChnAmnt);
						texttag.add("LCPol.PolNo",mLCPolSchema.getPolNo());
						texttag.add("LCPol.ProposalNo",mLCPolSchema.getProposalNo());
						texttag.add("LCPol.Mult",mLCPolSchema.getMult());
						texttag.add("LCPol.Prem",mLCPolSchema.getPrem());
						texttag.add("LCPol.Amnt",mLCPolSchema.getAmnt());
						texttag.add("LCPol.CValiDate",mLCPolSchema.getCValiDate());
						texttag.add("LCPol.GetStartDate",mLCPolSchema.getGetStartDate());
						texttag.add("LCPol.SignDate",mLCPolSchema.getSignDate());
						texttag.add("LCPol.AgentGroup",mLCPolSchema.getAgentGroup());
						texttag.add("LCPol.AgentCode",mLCPolSchema.getAgentCode());
				
				// 投保人信息
				LCAppntIndSchema mLCAppntIndSchema = new LCAppntIndSchema(); 
				mLCAppntIndSchema.setSchema((LCAppntIndSchema)tVData.getObjectByObjectName("LCAppntIndSchema",0));
						texttag.add("LCAppntInd.Name",mLCAppntIndSchema.getName());
						texttag.add("LCAppntInd.Sex",mLCAppntIndSchema.getSex());
						texttag.add("LCAppntInd.Birthday",mLCAppntIndSchema.getBirthday());
						texttag.add("LCAppntInd.CustomerNo",mLCAppntIndSchema.getCustomerNo());
						texttag.add("LCAppntInd.HomeAddress",mLCAppntIndSchema.getHomeAddress());
						texttag.add("LCAppntInd.RelationToInsured",mLCAppntIndSchema.getRelationToInsured());
				
				// 被保人信息
				LCInsuredSet mLCInsuredSet = new LCInsuredSet(); 
				mLCInsuredSet.set((LCInsuredSet)tVData.getObjectByObjectName("LCInsuredSet",0));
				int insuredCount = mLCInsuredSet.size();
				for (int i = 1; i <= insuredCount; i++)
				{
					LCInsuredSchema mLCInsuredSchema = mLCInsuredSet.get(i);
					if (mLCInsuredSchema.getInsuredGrade().equals("M"))
					{
						texttag.add("LCInsured.Name",mLCInsuredSchema.getName());	// 主被保人
						texttag.add("LCInsured.Sex",mLCInsuredSchema.getSex());
						texttag.add("LCInsured.Birthday",mLCInsuredSchema.getBirthday());
						texttag.add("LCInsured.CustomerNo",mLCInsuredSchema.getCustomerNo());
						texttag.add("LCInsured.GrpName",mLCInsuredSchema.getGrpName());
						texttag.add("LCInsured.HomeAddress",mLCInsuredSchema.getHomeAddress());
					}
				}

				// 责任信息信息
				LCDutySet mLCDutySet = new LCDutySet(); 
				mLCDutySet.set((LCDutySet)tVData.getObjectByObjectName("LCDutySet",0));
				int dutyCount = mLCDutySet.size();
				for (int i = 1; i <= dutyCount; i++)
				{
					LCDutySchema mLCDutySchema = mLCDutySet.get(1);
					String PayYears=String.valueOf(mLCDutySchema.getPayYears());
					texttag.add("LCDuty.PayYears",String.valueOf(mLCDutySchema.getPayYears()));
					String PayIntv=String.valueOf(mLCDutySchema.getPayIntv());
					texttag.add("LCDuty.PayIntv",String.valueOf(mLCDutySchema.getPayIntv()));
				}			

				// 受益人信息
				LCBnfSet mLCBnfSet = new LCBnfSet(); 
				mLCBnfSet.set((LCBnfSet)tVData.getObjectByObjectName("LCBnfSet",0));
				int bnfCount = mLCBnfSet.size();
				for (int i = 1; i <= bnfCount; i++)
				{
					LCBnfSchema mLCBnfSchema = mLCBnfSet.get(i);
					texttag.add("LCBnf.Name"+i,mLCBnfSchema.getName());
					texttag.add("LCBnf.Sex"+i,mLCBnfSchema.getSex());
					texttag.add("LCBnf.Birthday"+i,mLCBnfSchema.getBirthday());
					texttag.add("LCBnf.RelationToInsured"+i,mLCBnfSchema.getRelationToInsured());
					texttag.add("LCBnf.IDNo"+i,mLCBnfSchema.getIDNo());
					texttag.add("LCBnf.BnfLot"+i,mLCBnfSchema.getBnfLot());
					
				}			

				// 告知信息
				LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet(); 
				mLCCustomerImpartSet.set((LCCustomerImpartSet)tVData.getObjectByObjectName("LCCustomerImpartSet",0));
				int impartCount = mLCCustomerImpartSet.size();
				for (int i = 1; i <= impartCount; i++)
				{
					LCCustomerImpartSchema mLCCustomerImpartSchema = mLCCustomerImpartSet.get(i);
				}	

				// 特约信息
				LCSpecSet mLCSpecSet = new LCSpecSet(); 
				mLCSpecSet.set((LCSpecSet)tVData.getObjectByObjectName("LCSpecSet",0));
				int specCount = mLCSpecSet.size();
				for (int i = 1; i <= specCount; i++)
				{
					LCSpecSchema mLCSpecSchema = mLCSpecSet.get(i);
					texttag.add("LCSpec.SpecContent"+i,mLCSpecSchema.getSpecContent());
				}
				
				
				if (texttag.size()>0)
					xmlexport.addTextTag(texttag);//将texttag加到xml文档中
				String systime=StrTool.getHour()+StrTool.getMinute()+StrTool.getSecond();	
			 	filename=systime+"prt";
				xmlexport.outputDocumentToFile(pathname,filename);//输出xml文档到文件
				
			} // end of if

			//如果在Catch中发现异常，则不从错误类中提取错误信息
			if (FlagStr == "Fail")
			{
				tError = tProposalQueryUI.mErrors;
				if (!tError.needDealError())
				{                          
					Content = " 查询成功! ";
					FlagStr = "Succ";
				}
				else                                                                           
				{
					Content = " 查询失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
				}
			}
		} // end of if 
	} // end of for

	
}//end if terror=0
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
