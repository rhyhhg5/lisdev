/*
name : PNoticeBillInput.js
function :Print Notice Bill
Creator :刘岩松
Date :2003-04-16
*/

var arrDataSet 
var showInfo;
var mDebug="0";
var FlagDel;
var turnPage = new turnPageClass();

function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;

    showInfo.close();
    if (FlagStr == "Fail" )
    {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
    else
    {
    	showDiv(inputButton,"false");
    }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}

//根据起始日期进行查询出要该日期范围内的批次号码
function PrintNoticeBill()
{
    if((fm.StartDate.value=="")||(fm.EndDate.value=="")||(fm.StartDate.value=="null")||(fm.EndDate.value=="null"))
    {
    	alert("请输入起始日期和结束日期!!!");
    		return;
    	
    }
    if((fm.ManageCom.value=="")||(fm.ManageCom.value=="null"))
    {
    	alert("请选择管理机构!!1");
    	   	return ;
    }
    else
    {
    	
    	var i = 0;
    	//var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    	//var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    	
    	fm.action = './PNoticeBillSave.jsp';
    	fm.target="f1print";
    	fm.submit(); //提交
    }
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var strManageComWhere = " ";
	
	if((fm.ManageCom.value=="")||(fm.ManageCom.value=="null"))
    {
    	alert("请选择管理机构!!" )
    		return ;
    }
	if((fm.StartDate.value=="")||(fm.EndDate.value=="")||(fm.StartDate.value=="null")||(fm.EndDate.value=="null"))
    {
    	alert("请输入起始日期和结束日期!!!");
    		return;	
    }
  
	if( fm.BranchGroup.value != '' ) {
		strManageComWhere += " AND LAAgent.AgentGroup IN" + 
												" ( SELECT AgentGroup FROM LABranchGroup WHERE BranchAttr LIKE '" +
												fm.BranchGroup.value + "%%') ";
	}	
    strSQL  = "SELECT LOPRTManager.PrtSeq, LOPRTManager.OtherNo,LOPRTManager.AgentCode,LABranchGroup.BranchAttr, LOPRTManager.DoneDate,LOPRTManager.ManageCom " 
	        +" from LOPRTManager, LAAgent,LABranchGroup where (LOPRTManager.PrtSeq in (select max(PrtSeq) from LOPRTManager group by OtherNo,Code))"
            + getWherePart('LOPRTManager.DoneDate >', 'StartDate') 
            + getWherePart('LOPRTManager.DoneDate <', 'EndDate') 
            + getWherePart('LOPRTManager.StateFlag', 'StateFlag') 
            + getWherePart('LOPRTManager.ManageCom', 'ManageCom','like')     
            + getWherePart('LOPRTManager.AgentCode', 'AgentCode') 
            + getWherePart('LAAgent.AgentGroup','AgentGroup')
            + strManageComWhere
            + " and  LOPRTManager.ManageCom LIKE '" + comcode + "%%' "//登陆机构权限控制
            + " and LOPRTManager.AgentCode=LAAgent.AgentCode "
            + " and LABranchGroup.AgentGroup = LAAgent.AgentGroup "
            +" order by  LOPRTManager.AgentCode " ;
	  
	//传到后台的SQL  
    fm.StrSQL.value = "select LOPRTManager.PrtSeq,LOPRTManager.code,LOPRTManager.AgentCode, LAAgent.Name,LABranchGroup.BranchAttr"
            +" from LOPRTManager, LAAgent,LABranchGroup where (LOPRTManager.PrtSeq in (select max(PrtSeq) from LOPRTManager group by OtherNo,Code))"
            + getWherePart('LOPRTManager.DoneDate >', 'StartDate') 
            + getWherePart('LOPRTManager.DoneDate <', 'EndDate') 
            + getWherePart('LOPRTManager.StateFlag', 'StateFlag') 
            + " and LOPRTManager.ManageCom like'" +  fm.ManageCom.value +"%'"//后台处理需要
            + getWherePart('LOPRTManager.AgentCode', 'AgentCode') 
            + getWherePart('LAAgent.AgentGroup','AgentGroup')
            + strManageComWhere
            + " and  LOPRTManager.ManageCom LIKE '" + comcode + "%' "//登陆机构权限控制
            + " and LOPRTManager.AgentCode=LAAgent.AgentCode "
            + " and LABranchGroup.AgentGroup = LAAgent.AgentGroup "
            + " order by  LOPRTManager.AgentCode";

  
	turnPage.strQueryResult  = easyQueryVer3(strSQL);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要打印的通知书清单！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //tArr = chooseArray(arrDataSet,[0,1,3,4]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}
