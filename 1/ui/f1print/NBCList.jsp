<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：NBCList.jsp
//程序功能：
//创建日期：2003-05-13 15:39:06
//创建人  ：zy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="NBCList.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="NBCListInit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form  method=post name=fm target="fraSubmit">
    <table>
    	<tr> 
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFCDay);">
    		</td>
    		<td class= titleImg>
        	输入查询的时间范围
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divFCDay" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title> 起始时间 </TD>
          <TD  class= input> <Input class= "coolDatePicker" dateFormat="short" name=StartDay verify="起始时间|NOTNULL"></TD>
          <TD  class= title> 结束时间 </TD>
          <TD  class= input> <Input class= "coolDatePicker" dateFormat="short" name=EndDay  verify="结束时间|NOTNULL"></TD>  
        </TR>
        <tr>
           <TD  class= title> 销售渠道 </TD>
           <TD  class= input><Input class="codeno" name=SaleChnl ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1]);"  onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName readonly=true ></TD>
           <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   
        </tr>        
        </table>
    </Div>
    <table>
    	<tr> 
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOperator);">
    		</td>
    		<td class= titleImg>
          新契约拜访客户明细表
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divOperator" style= "display: ''">    
    <table  class= common>
        <TR class= common> 
		<TD  class= input width="15%">
			<input class= common type=Button  value="打印" onclick="printList()">
		</TD>
		<TD>			
		</TD>
		</TR>    	 
      </table>
      </Div>
    
       <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>