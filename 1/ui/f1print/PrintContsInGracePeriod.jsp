<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PrintPauseContNotice.jsp 
//程序功能：打印进入宽限期的保单通知书
//创建日期：2008-6-30 
//创建人  ：张 彦梅
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%
    String  contNo = request.getParameter("contNo"); 
    String prem = request.getParameter("prem");
    TransferData tTransferData = new TransferData();
    //将ContNo，Prem添加到TransferData中；
    tTransferData.setNameAndValue("ContNo", contNo);
    tTransferData.setNameAndValue("Prem", prem);
    
    //获取登陆信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    VData data = new VData();
    data.add(tG);
    data.add(tTransferData);
    
    boolean operFlag = true; 
      
    PrintGracePeriodNoticeUI tPrintGracePeriodNoticeUI=new PrintGracePeriodNoticeUI();
    XmlExport txmlExport = tPrintGracePeriodNoticeUI.getXmlExport(data,"");
    if(txmlExport == null)
  {
    operFlag = false;
            
  }
  if (operFlag==true)
	{
	  ExeSQL tExeSQL = new ExeSQL();
    //获取临时文件名
    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
    String strFilePath = tExeSQL.getOneValue(strSql);
    String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
    //获取存放临时文件的路径
    //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
    //String strRealPath = tExeSQL.getOneValue(strSql);
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strVFPathName = strRealPath + "//" +strVFFileName;
    
    CombineVts tcombineVts = null;	
    
    String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
  	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
  
  	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
  	tcombineVts.output(dataStream);
      	
  	//把dataStream存储到磁盘文件
  	//System.out.println("存储文件到"+strVFPathName);
  	AccessVtsFile.saveToFile(dataStream,strVFPathName);
    System.out.println("==> Write VTS file to disk ");
          
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
	}
	else
	{
    	
%>
<html>
<script language="javascript">	
	
	top.close();
</script>
</html>
<%
  	}
    
    
%>