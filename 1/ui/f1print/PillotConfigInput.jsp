<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
    tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="PillotConfigInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PillotConfigInit.jsp"%>
  <title>打印试点配置 </title>
</head>
<body onload="initForm();">
	<form action="" method=post name=fm target="fraSubmit">
		<table class=common border=0 width=100%>
    		<tr>
				<td class=titleImg align= center>请选择：</td>
			</tr>
		</table>
		<table class= common align=center>
			<TR class= common>
          		<TD class=title>配置公司编码</TD>
          		<TD class=input><Input class=code name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('pilotconfig',[this],[0],null,null,null,1);" onkeyup="return showCodeListKey('pilotconfig',[this],[0],null,null,null,1);"></TD>   
          		<TD class=title>试点标记</TD>
          		<TD class=input>
          			<Input class=codeno name=ConfigFlag CodeData="0|^0|非试点机构^1|试点机构" ondblclick="return showCodeListEx('ConfigFlag',[this,ConfigFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ConfigFlag',[this,ConfigFlagName],[0,1],null,null,null,1);"><input class=codename name=ConfigFlagName readonly=true >
          		</TD>
          		<TD class=title></TD>
          		<TD class=input></TD>
          	</TR> 
    	</table>
    	<hr />
    	<Input type=button class=cssButton name=QueryConfig value=" 查  询 " onclick="easyQuery()">
    	<table>
    		<tr>
        		<td class=common>
			    	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divConfig);">
    			</td>
    			<td class= titleImg>清单列表</td>
    		</tr>
    	</table>
  		<Div id="divConfig" style= "display:''">
      		<table class= common>
       			<tr class= common>
      	  			<td text-align: left colSpan=1>
  						<span id="spanPilotGrid"></span> 
  			  		</td>
  				</tr>
    		</table>
		  	<Div id= "divPage" align=center style= "display:''">     
		  		<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  		<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
		  		<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  		<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
		 	</Div>
  		</Div>
    	<hr />
    	<Input type=button class=cssButton name=AddConfig value="添加为试点" onclick="addToPilot()">
    	<Input type=button class=cssButton name=DelConfig value="取消该试点" onclick="removeFromPilot()">
	</form>
	<span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>