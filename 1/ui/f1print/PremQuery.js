//
//程序名称：PremQuery.js
//程序功能：保费查询
//创建日期：2004-10-10
//创建人  ：wentao
//更新记录：更新人    更新日期     更新原因/内容

//var arrDataSet;
var turnPage = new turnPageClass();

//简单查询
function easyQuery()
{
	var MngCom = fm.all('ManageCom').value;
	var EndDate = fm.all('ywEndDate').value;
	var bDate = fm.all('AppntBeginAge').value;
	var eDate = fm.all('AppntEndAge').value;	
	var inSql = "";
	
	if (MngCom == "")
	{
			alert("请选择管理机构！");
			return;
	}
	if (EndDate == "")
	{
			alert("请输入业务终止日期！");
			return;
	}
	if (bDate != "" && eDate == "")
	{
			alert("请输入终止投保年龄！");
			return;
	}
	if (bDate == "" && eDate != "")
	{
			alert("请输入开始投保年龄！");
			return;
	}
	if (bDate == "" && eDate == "")
	{
//			fm.all('AppntBeginAge').value = '0';
//			fm.all('AppntEndAge').value = '1000';
			bDate = '0';
			eDate = '1000';
	}
	if (fm.all('InsureYear').value != "")
	{
			inSql = " and get_age(a.cvalidate,'" + EndDate +"')+1 <= "+fm.all('InsureYear').value;
	}
	
	// 书写SQL语句
	var strSql = "select a.managecom,a.riskcode,a.payendyear,a.payendyearflag,a.INSUYEAR,a.INSUYEARFLAg," + bDate +","+ eDate +",get_age(a.cvalidate,'" + EndDate + "')+1,sum(j.sumactupaymoney),a.payintv "
							+"from v_lcpol_lbpol a, ljapayperson j "
							+"where a.polno=j.polno and j.paytype='ZC' "
//							+ getWherePart('get_age(a.cvalidate,' + EndDate +')+1','InsureYear','<=','0')
							+ inSql
						  + getWherePart('a.payendyearflag','PayEndDateUnit','=','0')
							+ getWherePart('a.payendyear','PayEndDate','=','0')
							+ getWherePart('a.INSUYEARFLAg','InsurePeriodUnit','=','0')
							+ getWherePart('a.INSUYEAR','InsurePeriod','=','0')
							+ getWherePart('a.PayIntv','PayIntv','=','0')
							+ getWherePart('a.riskcode','RiskCode','=','0')
							+ getWherePart('a.ManageCom','ManageCom','like','0')
							+ " and a.InsuredAppAge >= '" + bDate + "' "
							+ " and a.InsuredAppAge <= '" + eDate + "' "
//							+ getWherePart('a.InsuredAppAge','AppntBeginAge','>=','0')
//							+ getWherePart('a.InsuredAppAge','AppntEndAge','<=','0')
							+ getWherePart('a.signdate','ywBeginDate','>=','0')
							+ getWherePart('a.signdate','ywEndDate','<=','0')
							+" group by a.managecom,a.riskcode,a.payendyear,a.payendyearflag,a.INSUYEAR,a.INSUYEARFLAg,get_age(a.cvalidate,'" + EndDate +"'),a.payintv "
							+"union all "
							+ "select a.managecom,a.riskcode,a.payendyear,a.payendyearflag,a.INSUYEAR,a.INSUYEARFLAg," + bDate +","+ eDate +",get_age(a.cvalidate,'" + EndDate + "')+1,j.sum_GetMoney,a.payintv "
							+"from v_lcpol_lbpol a, view2_LJAGetEndorse j "
							+"where a.polno=j.polno "
							+ inSql
//							+ getWherePart('get_age(a.cvalidate,j.GetConfirmDate)+1','InsureYear','<=','0')
							+ getWherePart('a.payendyearflag','PayEndDateUnit','=','0')
							+ getWherePart('a.payendyear','PayEndDate','=','0')
							+ getWherePart('a.INSUYEARFLAg','InsurePeriodUnit','=','0')
							+ getWherePart('a.INSUYEAR','InsurePeriod','=','0')
							+ getWherePart('a.PayIntv','PayIntv','=','0')
							+ getWherePart('a.riskcode','RiskCode','=','0')
							+ getWherePart('a.ManageCom','ManageCom','like','0')
							+ " and a.InsuredAppAge >= '" + bDate + "' "
							+ " and a.InsuredAppAge <= '" + eDate + "' "
							+ getWherePart('a.signdate','ywBeginDate','>=','0')
							+ getWherePart('a.signdate','ywEndDate','<=','0')
//							+" group by a.managecom,a.riskcode,a.payendyear,a.payendyearflag,a.INSUYEAR,a.INSUYEARFLAg,get_age(a.cvalidate,j.GetConfirmDate),a.payintv "
							;

	//alert(strSql);
	turnPage.queryModal(strSql, CodeGrid);    
	//arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
}

//打印mutLine数据
function easyPrint()
{
}