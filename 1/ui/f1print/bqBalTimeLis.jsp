<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：bqBalTimeLis.jsp
//程序功能：
//创建日期：2006-03-20
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
	System.out.println("start");
  CError cError = new CError( );
  boolean operFlag=true;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	
	String StartDate = request.getParameter("StartDate");
	System.out.println(StartDate);

	String EndDate = request.getParameter("EndDate");
	System.out.println(EndDate);

	String PayEndDate = request.getParameter("PayEndDate");
	System.out.println(PayEndDate);
	
	String GrpContNo = request.getParameter("GrpContNo");
	System.out.println(GrpContNo);
	
	String WorkNo = request.getParameter("WorkNo");
	System.out.println(WorkNo);
	
	String PayMode = request.getParameter("PayMode");
	System.out.println(PayMode);
	
	String Bank = request.getParameter("Bank");
	System.out.println(Bank);
	
	String BankAccno = request.getParameter("BankAccno");
	System.out.println(BankAccno);
	
	String AccName = request.getParameter("AccName");
	System.out.println(AccName);
	
	String PayDate = request.getParameter("PayDate");
	System.out.println(PayDate);
	
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("StartDate", StartDate);
  tTransferData.setNameAndValue("EndDate",EndDate);
  tTransferData.setNameAndValue("GrpContNo",GrpContNo);
  tTransferData.setNameAndValue("WorkNo",WorkNo);
  tTransferData.setNameAndValue("PayMode",PayMode);
  tTransferData.setNameAndValue("PayEndDate",PayEndDate);
  tTransferData.setNameAndValue("Bank",Bank);
  tTransferData.setNameAndValue("BankAccno",BankAccno);
  tTransferData.setNameAndValue("AccName",AccName);
  tTransferData.setNameAndValue("PayDate",PayDate);
  
  tVData.addElement(tG);
  tVData.addElement(tTransferData);
  

  bqBalTimeLisBL tbqBalTimeLisBL = new bqBalTimeLisBL();
	XmlExport txmlExport = new XmlExport();    
  if(!tbqBalTimeLisBL.submitData(tVData,"PRINT"))
  {
     operFlag = false;
     Content = tbqBalTimeLisBL.mErrors.getFirstError().toString();                 
  }
  else
  {    
		 mResult = tbqBalTimeLisBL.getResult();			
		 txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
		 if(txmlExport==null)
	   {
	     operFlag=false;
	     Content="没有得到要显示的数据文件";	  
	  	}
	}
	
	LDSysVarDB tLDSysVarDB = new LDSysVarDB();
  tLDSysVarDB.setSysVar("VTSFilePath");
  tLDSysVarDB.getInfo();
  String vtsPath = tLDSysVarDB.getSysVarValue();
  if (vtsPath == null)
  {
    vtsPath = "vtsfile/";
  }
  
  String filePath = application.getRealPath("/").replace('\\', '/') + "/" + vtsPath;
  String fileName = "TASK" + FileQueue.getFileName()+".vts";
  String realPath = filePath + fileName;
	
	if (operFlag==true)
	{
    //合并VTS文件 
    String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);
    
    //把dataStream存储到磁盘文件
    AccessVtsFile.saveToFile(dataStream, realPath);
    response.sendRedirect("GetF1PrintJ1_new.jsp?RealPath=" + realPath);
	}
	else
	{
    	FlagStr = "Fail";

%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();	
</script>
</html>
<%
  	}
%>