 //               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initBonusGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPolBonus()
{
  var i = 0;

  var tSel = BonusGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		fmList.PrtSeq2.value=BonusGrid.getRowColData(tSel-1,1);
		fmList.GrpPolNoForList.value=BonusGrid.getRowColData(tSel-1,2);
		fmList.FiscalYearForList.value=BonusGrid.getRowColData(tSel-1,5);
		PrtSeq = BonusGrid.getRowColData(tSel-1,1);
		fmSave.PrtSeq.value = PrtSeq;
		fmSave.fmtransact.value = "CONFIRM";
		fmSave.submit();
	}
}


//提交，保存按钮对应操作
function printPolBonusForList()
{
  var i = 0;

  if( fmList.GrpPolNoForList.value == null || fmList.GrpPolNoForList.value == "" )
		alert( "请先选择一条记录，打印企业帐户年度报告后再打印清单!" );
	else
	{
		//fmList.fmtransact.value = "CONFIRM";
		fmList.submit();
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initBonusGrid();

	// 书写SQL语句
	var strSQL = "";
	strSQL = "SELECT LOPRTManager.PrtSeq, LOPRTManager.OtherNo,LOPRTManager.ManageCom,LOBonusGrpPolParm.SGetDate,LOBonusGrpPolParm.FiscalYear,LOBonusGrpPolParm.RiskCode FROM LOPRTManager,LOBonusGrpPolParm WHERE LOPRTManager.Code = '34' " 
	+ " and LOPRTManager.OtherNo=LOBonusGrpPolParm.GrpPolNo"
	+ getWherePart('LOPRTManager.OtherNo', 'GrpPolNo') 
	+ getWherePart('LOBonusGrpPolParm.FiscalYear', 'FiscalYear')
	+ getWherePart('LOBonusGrpPolParm.RiskCode', 'RiskCode')
	+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
	+ " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'"
	+ " AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'"
	+ " AND LOPRTManager.OtherNoType = '01' "
	+ "order by LOBonusGrpPolParm.SGetDate ";
	  
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  //alert(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要打印的红利派发通知书！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BonusGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}