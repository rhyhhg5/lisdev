<%
//程序名称：RSReportInit.jsp
//程序功能：添加页面控件的初始化
//创建日期：2002-08-23 17:06:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>                         
 
<script language="JavaScript">

function initForm() {
  try {  
    initGrid();
    initBGGrid();
    initLPGrid();
    initGSXQGrid();
    initGBGGrid();
    initGLPGrid();
    
    
    
  } catch(re) {
    alert("RSReportInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *初始化MULTILINE               
 *功能：初始化MULTILINE Grid
 ************************************************************
 */
//个人再保首期续期 
var VarGrid;          //定义为全局变量，提供给displayMultiline使用 
function initGrid() {   
  var iArray = new Array();
                              
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="100px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="分出险种代码";         //列名
    iArray[1][1]="200px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="分出保单保单号";         //列名
    iArray[2][1]="200px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="分出保单印刷号";         //列名
    iArray[3][1]="200px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="被保险保人名称";         //列名
    iArray[4][1]="200px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="保单年度";         //列名
    iArray[5][1]="200px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="姓别";         //列名
    iArray[6][1]="200px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
       
    iArray[7]=new Array();
    iArray[7][0]="证件类型";         //列名
    iArray[7][1]="200px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[8]=new Array();
    iArray[8][0]="证件号码";         //列名
    iArray[8][1]="200px";         //宽度
    iArray[8][2]=100;         //最大长度
    iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[9]=new Array();
    iArray[9][0]="出生年月日";         //列名
    iArray[9][1]="200px";         //宽度
    iArray[9][2]=100;         //最大长度
    iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="职业代码";         //列名
    iArray[10][1]="200px";         //宽度
    iArray[10][2]=100;         //最大长度
    iArray[10][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[11]=new Array();                                  
    iArray[11][0]="销售方式";         //列名                   
    iArray[11][1]="200px";         //宽度                    
    iArray[11][2]=100;         //最大长度                    
    iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();                                  
    iArray[12][0]="基本保额";         //列名                   
    iArray[12][1]="200px";         //宽度                    
    iArray[12][2]=100;         //最大长度                    
    iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();                                  
    iArray[13][0]="基本保额";         //列名                   
    iArray[13][1]="200px";         //宽度                    
    iArray[13][2]=100;         //最大长度                    
    iArray[13][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[14]=new Array();                                  
    iArray[14][0]="保单份数";         //列名                   
    iArray[14][1]="200px";         //宽度                    
    iArray[14][2]=100;         //最大长度                    
    iArray[14][3]=0;         //是否允许录入，0--不能，1--允许


    iArray[15]=new Array();                                  
    iArray[15][0]="责任倍数";         //列名                   
    iArray[15][1]="200px";         //宽度                    
    iArray[15][2]=100;         //最大长度                    
    iArray[15][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[16]=new Array();                                  
    iArray[16][0]="分保保额";         //列名                   
    iArray[16][1]="200px";         //宽度                    
    iArray[16][2]=100;         //最大长度                    
    iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[17]=new Array();                                  
    iArray[17][0]="年初现金价值";         //列名                   
    iArray[17][1]="200px";         //宽度                    
    iArray[17][2]=100;         //最大长度                    
    iArray[17][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[18]=new Array();                                  
    iArray[18][0]="累计风险保额";         //列名                   
    iArray[18][1]="200px";         //宽度                    
    iArray[18][2]=100;         //最大长度                    
    iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
                                                             
    iArray[19]=new Array();                                  
    iArray[19][0]="自留额";         //列名                   
    iArray[19][1]="200px";         //宽度                    
    iArray[19][2]=100;         //最大长度                    
    iArray[19][3]=0;         //是否允许录入，0--不能，1--允许
                                                             
    iArray[20]=new Array();                                  
    iArray[20][0]="缴费年限";         //列名                   
    iArray[20][1]="200px";         //宽度                    
    iArray[20][2]=100;         //最大长度                    
    iArray[20][3]=0;         //是否允许录入，0--不能，1--允许
 
    
    iArray[21]=new Array();                                   
    iArray[21][0]="保单生效日";         //列名                    
    iArray[21][1]="200px";         //宽度                     
    iArray[21][2]=100;         //最大长度                     
    iArray[21][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[22]=new Array();                                   
    iArray[22][0]="保单责任终止日";         //列名                    
    iArray[22][1]="200px";         //宽度                     
    iArray[22][2]=100;         //最大长度                     
    iArray[22][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[23]=new Array();                                   
    iArray[23][0]="分保比例";         //列名                    
    iArray[23][1]="200px";         //宽度                     
    iArray[23][2]=100;         //最大长度                     
    iArray[23][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[24]=new Array();                                   
    iArray[24][0]="风险分保金额";         //列名                    
    iArray[24][1]="200px";         //宽度                     
    iArray[24][2]=100;         //最大长度                     
    iArray[24][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
                                                              
    iArray[25]=new Array();                                   
    iArray[25][0]="风险保费";         //列名                                         
    iArray[25][1]="200px";         //宽度                                          
    iArray[25][2]=100;         //最大长度                                          
    iArray[25][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[26]=new Array();                                                        
    iArray[26][0]="次标准体加费";         //列名                                         
    iArray[26][1]="200px";         //宽度                                          
    iArray[26][2]=100;         //最大长度                                          
    iArray[26][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[27]=new Array();                                                        
    iArray[27][0]="职业加费";         //列名                                         
    iArray[27][1]="200px";         //宽度                                          
    iArray[27][2]=100;         //最大长度                                          
    iArray[27][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[28]=new Array();                                                        
    iArray[28][0]="分保费合计";         //列名                                         
    iArray[28][1]="200px";         //宽度                                          
    iArray[28][2]=100;         //最大长度                                          
    iArray[28][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[29]=new Array();                                                        
    iArray[29][0]="标准分保费";         //列名                                         
    iArray[29][1]="200px";         //宽度                                          
    iArray[29][2]=100;         //最大长度                                          
    iArray[29][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[30]=new Array();                                                        
    iArray[30][0]="分保职业加费";         //列名                                         
    iArray[30][1]="200px";         //宽度                                          
    iArray[30][2]=100;         //最大长度                                          
    iArray[30][3]=0;         //是否允许录入，0--不能，1--允许                      


    iArray[31]=new Array();                                   
    iArray[31][0]="分保次标准体加费";         //列名                    
    iArray[31][1]="200px";         //宽度                     
    iArray[31][2]=100;         //最大长度                     
    iArray[31][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[32]=new Array();                                   
    iArray[32][0]="分保加费合计";         //列名                    
    iArray[32][1]="200px";         //宽度                     
    iArray[32][2]=100;         //最大长度                     
    iArray[32][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[33]=new Array();                                   
    iArray[33][0]="选择折扣";         //列名                    
    iArray[33][1]="200px";         //宽度                     
    iArray[33][2]=100;         //最大长度                     
    iArray[33][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[34]=new Array();                                   
    iArray[34][0]="手续费";         //列名                    
    iArray[34][1]="200px";         //宽度                     
    iArray[34][2]=100;         //最大长度                     
    iArray[34][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
                                                              
    iArray[35]=new Array();                                   
    iArray[35][0]="意外加点";         //列名                                         
    iArray[35][1]="200px";         //宽度                                          
    iArray[35][2]=100;         //最大长度                                          
    iArray[35][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[36]=new Array();                                                        
    iArray[36][0]="寿险加点";         //列名                                         
    iArray[36][1]="200px";         //宽度                                          
    iArray[36][2]=100;         //最大长度                                          
    iArray[36][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[37]=new Array();                                                        
    iArray[37][0]="重疾加点";         //列名                                         
    iArray[37][1]="200px";         //宽度                                          
    iArray[37][2]=100;         //最大长度                                          
    iArray[37][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[38]=new Array();                                                        
    iArray[38][0]="健康加点";         //列名                                         
    iArray[38][1]="200px";         //宽度                                          
    iArray[38][2]=100;         //最大长度                                          
    iArray[38][3]=0;         //是否允许录入，0--不能，1--允许      
    
    iArray[39]=new Array();                                                        
    iArray[39][0]="风险保额";         //列名                                         
    iArray[39][1]="200px";         //宽度                                          
    iArray[39][2]=100;         //最大长度                                          
    iArray[39][3]=0;         //是否允许录入，0--不能，1--允许                       



    VarGrid = new MulLineEnter( "fm" , "VarGrid" ); 

    //这些属性必须在loadMulLine前
    VarGrid.mulLineCount = 0;   
    VarGrid.displayTitle = 1;

    //VarGrid.canSel = 0;
    VarGrid.hiddenPlus = 1;        //隐藏加号
    VarGrid.hiddenSubtraction = 1; //隐藏减号
    
    VarGrid.loadMulLine(iArray);  
  } catch(ex) {
    alert("初始化Grid时出错："+ ex);
  }  
} 
//个人保全变更报表
var LPGrid;          //定义为全局变量，提供给displayMultiline使用 
function initLPGrid() {   
  var iArray = new Array();
                              
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="100px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="分出保单保单号";         //列名
    iArray[1][1]="200px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="分出保单印刷号";         //列名
    iArray[2][1]="200px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="险种代码";         //列名
    iArray[3][1]="200px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="批单号";         //列名
    iArray[4][1]="200px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="批改原因";         //列名
    iArray[5][1]="200px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="被保人客户代码";         //列名
    iArray[6][1]="200px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
       
    iArray[7]=new Array();
    iArray[7][0]="客户姓名";         //列名
    iArray[7][1]="200px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[8]=new Array();
    iArray[8][0]="证件号码";         //列名
    iArray[8][1]="200px";         //宽度
    iArray[8][2]=100;         //最大长度
    iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[9]=new Array();
    iArray[9][0]="批改后职业类别";         //列名
    iArray[9][1]="200px";         //宽度
    iArray[9][2]=100;         //最大长度
    iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="性别";         //列名
    iArray[10][1]="200px";         //宽度
    iArray[10][2]=100;         //最大长度
    iArray[10][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[11]=new Array();                                  
    iArray[11][0]="出生日期";         //列名                   
    iArray[11][1]="200px";         //宽度                    
    iArray[11][2]=100;         //最大长度                    
    iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();                                  
    iArray[12][0]="保单份数";         //列名                   
    iArray[12][1]="200px";         //宽度                    
    iArray[12][2]=100;         //最大长度                    
    iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();                                  
    iArray[13][0]="缴费期限";         //列名                   
    iArray[13][1]="200px";         //宽度                    
    iArray[13][2]=100;         //最大长度                    
    iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();                                  
    iArray[14][0]="缴费频率";         //列名                   
    iArray[14][1]="200px";         //宽度                    
    iArray[14][2]=100;         //最大长度                    
    iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[15]=new Array();                                  
    iArray[15][0]="保单生效日";         //列名                   
    iArray[15][1]="200px";         //宽度                    
    iArray[15][2]=100;         //最大长度                    
    iArray[15][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[16]=new Array();                                  
    iArray[16][0]="保单终止日";         //列名                   
    iArray[16][1]="200px";         //宽度                    
    iArray[16][2]=100;         //最大长度                    
    iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
                                                             
    iArray[17]=new Array();                                  
    iArray[17][0]="变更日期";         //列名                   
    iArray[17][1]="200px";         //宽度                    
    iArray[17][2]=100;         //最大长度                    
    iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
                                                             
    iArray[18]=new Array();                                  
    iArray[18][0]="承担责任天数";         //列名                   
    iArray[18][1]="200px";         //宽度                    
    iArray[18][2]=100;         //最大长度                    
    iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
 
    
    iArray[19]=new Array();                                   
    iArray[19][0]="分保退保费";         //列名                    
    iArray[19][1]="200px";         //宽度                     
    iArray[19][2]=100;         //最大长度                     
    iArray[19][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[20]=new Array();                                   
    iArray[20][0]="分保退保金";         //列名                    
    iArray[20][1]="200px";         //宽度                     
    iArray[20][2]=100;         //最大长度                     
    iArray[20][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[21]=new Array();                                   
    iArray[21][0]="分保利息";         //列名                    
    iArray[21][1]="200px";         //宽度                     
    iArray[21][2]=100;         //最大长度                     
    iArray[21][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[22]=new Array();                                   
    iArray[22][0]="调整分保保额";         //列名                    
    iArray[22][1]="200px";         //宽度                     
    iArray[22][2]=100;         //最大长度                     
    iArray[22][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
                                                              
    iArray[23]=new Array();                                   
    iArray[23][0]="调整总分保费";         //列名                                         
    iArray[23][1]="200px";         //宽度                                          
    iArray[23][2]=100;         //最大长度                                          
    iArray[23][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[24]=new Array();                                                        
    iArray[24][0]="调整总分保手续费";         //列名                                         
    iArray[24][1]="200px";         //宽度                                          
    iArray[24][2]=100;         //最大长度                                          
    iArray[24][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    BGGrid = new MulLineEnter( "fm" , "BGGrid" ); 

    //这些属性必须在loadMulLine前
    BGGrid.mulLineCount = 0;   
    BGGrid.displayTitle = 1;
    BGGrid.hiddenPlus = 1;        //隐藏加号
    BGGrid.hiddenSubtraction = 1; //隐藏减号
    
    BGGrid.loadMulLine(iArray);  
  } catch(ex) {
    alert("初始化Grid时出错："+ ex);
  }  
} 




//个人理赔变更报表
var BGGrid;          //定义为全局变量，提供给displayMultiline使用 
function initBGGrid() {   
  var iArray = new Array();
                              
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="100px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="分出保单印刷号";         //列名
    iArray[1][1]="200px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="赔案号";         //列名
    iArray[2][1]="200px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="保单年度";         //列名
    iArray[3][1]="200px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="保单号";         //列名
    iArray[4][1]="200px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="险种代码";         //列名
    iArray[5][1]="200px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="保单生效日期";         //列名
    iArray[6][1]="200px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
       
    iArray[7]=new Array();
    iArray[7][0]="被保险人代码";         //列名
    iArray[7][1]="200px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[8]=new Array();
    iArray[8][0]="姓名";         //列名
    iArray[8][1]="200px";         //宽度
    iArray[8][2]=100;         //最大长度
    iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[9]=new Array();
    iArray[9][0]="出生年月日";         //列名
    iArray[9][1]="200px";         //宽度
    iArray[9][2]=100;         //最大长度
    iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="性别";         //列名
    iArray[10][1]="200px";         //宽度
    iArray[10][2]=100;         //最大长度
    iArray[10][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[11]=new Array();                                  
    iArray[11][0]="证件类型";         //列名                   
    iArray[11][1]="200px";         //宽度                    
    iArray[11][2]=100;         //最大长度                    
    iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();                                  
    iArray[12][0]="证件号";         //列名                   
    iArray[12][1]="200px";         //宽度                    
    iArray[12][2]=100;         //最大长度                    
    iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();                                  
    iArray[13][0]="投保份数";         //列名                   
    iArray[13][1]="200px";         //宽度                    
    iArray[13][2]=100;         //最大长度                    
    iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();                                  
    iArray[14][0]="出险日期";         //列名                   
    iArray[14][1]="200px";         //宽度                    
    iArray[14][2]=100;         //最大长度                    
    iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[15]=new Array();                                  
    iArray[15][0]="报案日期";         //列名                   
    iArray[15][1]="200px";         //宽度                    
    iArray[15][2]=100;         //最大长度                    
    iArray[15][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[16]=new Array();                                  
    iArray[16][0]="立案日期";         //列名                   
    iArray[16][1]="200px";         //宽度                    
    iArray[16][2]=100;         //最大长度                    
    iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
                                                             
    iArray[17]=new Array();                                  
    iArray[17][0]="入院日期";         //列名                   
    iArray[17][1]="200px";         //宽度                    
    iArray[17][2]=100;         //最大长度                    
    iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
                                                             
    iArray[18]=new Array();                                  
    iArray[18][0]="赔款日期";         //列名                   
    iArray[18][1]="200px";         //宽度                    
    iArray[18][2]=100;         //最大长度                    
    iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
 
    
    iArray[19]=new Array();                                   
    iArray[19][0]="住院天数";         //列名                    
    iArray[19][1]="200px";         //宽度                     
    iArray[19][2]=100;         //最大长度                     
    iArray[19][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[20]=new Array();                                   
    iArray[20][0]="分保比例";         //列名                    
    iArray[20][1]="200px";         //宽度                     
    iArray[20][2]=100;         //最大长度                     
    iArray[20][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[21]=new Array();                                   
    iArray[21][0]="理赔是否中止保单责任";         //列名                    
    iArray[21][1]="200px";         //宽度                     
    iArray[21][2]=100;         //最大长度                     
    iArray[21][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[22]=new Array();                                   
    iArray[22][0]="实际赔偿金额";         //列名                    
    iArray[22][1]="200px";         //宽度                     
    iArray[22][2]=100;         //最大长度                     
    iArray[22][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
                                                              
    iArray[23]=new Array();                                   
    iArray[23][0]="实际给付金额";         //列名                                         
    iArray[23][1]="200px";         //宽度                                          
    iArray[23][2]=100;         //最大长度                                          
    iArray[23][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[24]=new Array();                                                        
    iArray[24][0]="分保赔款金额";         //列名                                         
    iArray[24][1]="200px";         //宽度                                          
    iArray[24][2]=100;         //最大长度                                          
    iArray[24][3]=0;         //是否允许录入，0--不能，1--允许        
    
    iArray[25]=new Array();                                                        
    iArray[25][0]="分保给付金额";         //列名                                         
    iArray[25][1]="200px";         //宽度                                          
    iArray[25][2]=100;         //最大长度                                          
    iArray[25][3]=0;         //是否允许录入，0--不能，1--允许               
                                                                                   
    LPGrid = new MulLineEnter( "fm" , "LPGrid" ); 

    //这些属性必须在loadMulLine前
    LPGrid.mulLineCount = 0;   
    LPGrid.displayTitle = 1;
    LPGrid.hiddenPlus = 1;        //隐藏加号
    LPGrid.hiddenSubtraction = 1; //隐藏减号
    
    LPGrid.loadMulLine(iArray);  
  } catch(ex) {
    alert("初始化Grid时出错："+ ex);
  }  
} 
//集体再保首期续期 
var GSXQGrid;          //定义为全局变量，提供给displayMultiline使用 
function initGSXQGrid() {   
  var iArray = new Array();
                              
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="100px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="管理机构";         //列名
    iArray[1][1]="200px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="团体保险单号";         //列名
    iArray[2][1]="200px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="分出公司团体保单印刷号";         //列名
    iArray[3][1]="200px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="分出公司个人保单号";         //列名
    iArray[4][1]="200px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="险种代码";         //列名
    iArray[5][1]="200px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="团体名称";         //列名
    iArray[6][1]="200px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[7]=new Array();
    iArray[7][0]="总保额";         //列名
    iArray[7][1]="200px";          //宽度
    iArray[7][2]=100;              //最大长度
    iArray[7][3]=0;               //是否允许录入，0--不能，1--允许

    iArray[8]=new Array();
    iArray[8][0]="平均保额";      //列名
    iArray[8][1]="200px";         //宽度
    iArray[8][2]=100;             //最大长度
    iArray[8][3]=0;              //是否允许录入，0--不能，1--允许

    iArray[9]=new Array();
    iArray[9][0]="投保人数";      //列名
    iArray[9][1]="200px";         //宽度
    iArray[9][2]=100;             //最大长度
    iArray[9][3]=0;               //是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="每份保费";         //列名
    iArray[10][1]="200px";         //宽度
    iArray[10][2]=100;         //最大长度
    iArray[10][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[11]=new Array();                                  
    iArray[11][0]="份数";         //列名                   
    iArray[11][1]="200px";         //宽度                    
    iArray[11][2]=100;         //最大长度                    
    iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();                                  
    iArray[12][0]="毛保费";         //列名                   
    iArray[12][1]="200px";         //宽度                    
    iArray[12][2]=100;         //最大长度                    
    iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();                                  
    iArray[13][0]="参保比例";         //列名                   
    iArray[13][1]="200px";         //宽度                    
    iArray[13][2]=100;         //最大长度                    
    iArray[13][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[14]=new Array();                                  
    iArray[14][0]="分保比例";         //列名                   
    iArray[14][1]="200px";         //宽度                    
    iArray[14][2]=100;         //最大长度                    
    iArray[14][3]=0;         //是否允许录入，0--不能，1--允许


    iArray[15]=new Array();                                  
    iArray[15][0]="手续费";         //列名                   
    iArray[15][1]="200px";         //宽度                    
    iArray[15][2]=100;         //最大长度                    
    iArray[15][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[16]=new Array();                                  
    iArray[16][0]="分保额";         //列名                   
    iArray[16][1]="200px";         //宽度                    
    iArray[16][2]=100;         //最大长度                    
    iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[17]=new Array();                                  
    iArray[17][0]="分保费";         //列名                   
    iArray[17][1]="200px";         //宽度                    
    iArray[17][2]=100;         //最大长度                    
    iArray[17][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[18]=new Array();                                  
    iArray[18][0]="保单生效日期";         //列名                   
    iArray[18][1]="200px";         //宽度                    
    iArray[18][2]=100;         //最大长度                    
    iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
                                                             
    iArray[19]=new Array();                                  
    iArray[19][0]="保单中止日";         //列名                   
    iArray[19][1]="200px";         //宽度                    
    iArray[19][2]=100;         //最大长度                    
    iArray[19][3]=0;         //是否允许录入，0--不能，1--允许
                                                             
    iArray[20]=new Array();                                  
    iArray[20][0]="缴费方式";         //列名                   
    iArray[20][1]="200px";         //宽度                    
    iArray[20][2]=100;         //最大长度                    
    iArray[20][3]=0;         //是否允许录入，0--不能，1--允许
 
    
    iArray[21]=new Array();                                   
    iArray[21][0]="缴费年限";         //列名                    
    iArray[21][1]="200px";         //宽度                     
    iArray[21][2]=100;         //最大长度                     
    iArray[21][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[22]=new Array();                                   
    iArray[22][0]="被保人姓名";         //列名                    
    iArray[22][1]="200px";         //宽度                     
    iArray[22][2]=100;         //最大长度                     
    iArray[22][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[23]=new Array();                                   
    iArray[23][0]="身份证号";         //列名                    
    iArray[23][1]="200px";         //宽度                     
    iArray[23][2]=100;         //最大长度                     
    iArray[23][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[24]=new Array();                                   
    iArray[24][0]="保单年度";         //列名                    
    iArray[24][1]="200px";         //宽度                     
    iArray[24][2]=100;         //最大长度                     
    iArray[24][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
                                                              
    iArray[25]=new Array();                                   
    iArray[25][0]="险种代码";         //列名                                         
    iArray[25][1]="200px";         //宽度                                          
    iArray[25][2]=100;         //最大长度                                          
    iArray[25][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[26]=new Array();                                                        
    iArray[26][0]="保额";         //列名                                         
    iArray[26][1]="200px";         //宽度                                          
    iArray[26][2]=100;         //最大长度                                          
    iArray[26][3]=0;         //是否允许录入，0--不能，1--允许                      
    
    GSXQGrid = new MulLineEnter( "fm" , "GSXQGrid" ); 

    //这些属性必须在loadMulLine前
    GSXQGrid.mulLineCount = 0;   
    GSXQGrid.displayTitle = 1;

    //GSXQGrid.canSel = 0;
    GSXQGrid.hiddenPlus = 1;        //隐藏加号
    GSXQGrid.hiddenSubtraction = 1; //隐藏减号
    
    GSXQGrid.loadMulLine(iArray);  
  } catch(ex) {
    alert("初始化Grid时出错："+ ex);
  }  
} 


//集体再保保全
var GBGGrid;          //定义为全局变量，提供给displayMultiline使用 
function initGBGGrid() {   
  var iArray = new Array();
                              
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="100px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="公司代码";         //列名
    iArray[1][1]="200px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="团体保险单号";         //列名
    iArray[2][1]="200px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="个人投保单号";         //列名
    iArray[3][1]="200px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="批单号";         //列名
    iArray[4][1]="200px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array();
    iArray[5][0]="险种代码";         //列名
    iArray[5][1]="200px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="团体名称";         //列名
    iArray[6][1]="200px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[7]=new Array();
    iArray[7][0]="调整总保费";         //列名
    iArray[7][1]="200px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[8]=new Array();
    iArray[8][0]="调整总保额";         //列名
    iArray[8][1]="200px";          //宽度
    iArray[8][2]=100;              //最大长度
    iArray[8][3]=0;               //是否允许录入，0--不能，1--允许

    iArray[9]=new Array();
    iArray[9][0]="调整手续费";      //列名
    iArray[9][1]="200px";         //宽度
    iArray[9][2]=100;             //最大长度
    iArray[9][3]=0;              //是否允许录入，0--不能，1--允许

    iArray[10]=new Array();
    iArray[10][0]="调整份数";      //列名
    iArray[10][1]="200px";         //宽度
    iArray[10][2]=100;             //最大长度
    iArray[10][3]=0;               //是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="增加投保人";         //列名
    iArray[11][1]="200px";         //宽度
    iArray[11][2]=100;         //最大长度
    iArray[11][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[12]=new Array();                                  
    iArray[12][0]="分保比例";         //列名                   
    iArray[12][1]="200px";         //宽度                    
    iArray[12][2]=100;         //最大长度                    
    iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();                                  
    iArray[13][0]="调整分保额";         //列名                   
    iArray[13][1]="200px";         //宽度                    
    iArray[13][2]=100;         //最大长度                    
    iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();                                  
    iArray[14][0]="调整分保费";         //列名                   
    iArray[14][1]="200px";         //宽度                    
    iArray[14][2]=100;         //最大长度                    
    iArray[14][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[15]=new Array();                                  
    iArray[15][0]="退保费";         //列名                   
    iArray[15][1]="200px";         //宽度                    
    iArray[15][2]=100;         //最大长度                    
    iArray[15][3]=0;         //是否允许录入，0--不能，1--允许


    iArray[16]=new Array();                                  
    iArray[16][0]="退手续费";         //列名                   
    iArray[16][1]="200px";         //宽度                    
    iArray[16][2]=100;         //最大长度                    
    iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[17]=new Array();                                  
    iArray[17][0]="退保金";         //列名                   
    iArray[17][1]="200px";         //宽度                    
    iArray[17][2]=100;         //最大长度                    
    iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[18]=new Array();                                  
    iArray[18][0]="批改类型";         //列名                   
    iArray[18][1]="200px";         //宽度                    
    iArray[18][2]=100;         //最大长度                    
    iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
                    
    
    GBGGrid = new MulLineEnter( "fm" , "GBGGrid" ); 

    //这些属性必须在loadMulLine前
    GBGGrid.mulLineCount = 0;   
    GBGGrid.displayTitle = 1;

    //GBGGrid.canSel = 0;
    GBGGrid.hiddenPlus = 1;        //隐藏加号
    GBGGrid.hiddenSubtraction = 1; //隐藏减号
    
    GBGGrid.loadMulLine(iArray);  
  } catch(ex) {
    alert("初始化Grid时出错："+ ex);
  }  
}

//集体再保理赔
var GLPGrid;          //定义为全局变量，提供给displayMultiline使用 
function initGLPGrid() {   
  var iArray = new Array();
                              
  try {            
    iArray[0]=new Array();                 
    iArray[0][0]="序号";         //列名  
    iArray[0][1]="100px";         //列名   
    iArray[0][2]=100;         //列名       
    iArray[0][3]=0;         //列名         

  
  
  
    iArray[1]=new Array();
    iArray[1][0]="赔案号";         //列名
    iArray[1][1]="100px";         //列名
    iArray[1][2]=100;         //列名
    iArray[1][3]=0;         //列名

    iArray[2]=new Array();
    iArray[2][0]="凭证号码";         //列名
    iArray[2][1]="200px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="管理机构";         //列名
    iArray[3][1]="200px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="团体保险单号";         //列名
    iArray[4][1]="200px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array();
    iArray[5][0]="印刷号";         //列名
    iArray[5][1]="200px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="险种代码";         //列名
    iArray[6][1]="200px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[7]=new Array();
    iArray[7][0]="被保险人姓名";         //列名
    iArray[7][1]="200px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[8]=new Array();
    iArray[8][0]="被保险人身份证号";         //列名
    iArray[8][1]="200px";          //宽度
    iArray[8][2]=100;              //最大长度
    iArray[8][3]=0;               //是否允许录入，0--不能，1--允许

    iArray[9]=new Array();
    iArray[9][0]="保单生效日期";      //列名
    iArray[9][1]="200px";         //宽度
    iArray[9][2]=100;             //最大长度
    iArray[9][3]=0;              //是否允许录入，0--不能，1--允许

    iArray[10]=new Array();
    iArray[10][0]="投保份数";      //列名
    iArray[10][1]="200px";         //宽度
    iArray[10][2]=100;             //最大长度
    iArray[10][3]=0;               //是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="报案日期";         //列名
    iArray[11][1]="200px";         //宽度
    iArray[11][2]=100;         //最大长度
    iArray[11][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[12]=new Array();                                  
    iArray[12][0]="出险日期";         //列名                   
    iArray[12][1]="200px";         //宽度                    
    iArray[12][2]=100;         //最大长度                    
    iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();                                  
    iArray[13][0]="立案日期";         //列名                   
    iArray[13][1]="200px";         //宽度                    
    iArray[13][2]=100;         //最大长度                    
    iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();                                  
    iArray[14][0]="入院日期";         //列名                   
    iArray[14][1]="200px";         //宽度                    
    iArray[14][2]=100;         //最大长度                    
    iArray[14][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[15]=new Array();                                  
    iArray[15][0]="赔案日期";         //列名                   
    iArray[15][1]="200px";         //宽度                    
    iArray[15][2]=100;         //最大长度                    
    iArray[15][3]=0;         //是否允许录入，0--不能，1--允许


    iArray[16]=new Array();                                  
    iArray[16][0]="病因代码";         //列名                   
    iArray[16][1]="200px";         //宽度                    
    iArray[16][2]=100;         //最大长度                    
    iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[17]=new Array();                                  
    iArray[17][0]="住院天数";         //列名                   
    iArray[17][1]="200px";         //宽度                    
    iArray[17][2]=100;         //最大长度                    
    iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[18]=new Array();                                  
    iArray[18][0]="死亡原因代码";         //列名                   
    iArray[18][1]="200px";         //宽度                    
    iArray[18][2]=100;         //最大长度                    
    iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
                    
    iArray[19]=new Array();                                   
    iArray[19][0]="理赔是否中止保单责任";         //列名                    
    iArray[19][1]="200px";         //宽度                     
    iArray[19][2]=100;         //最大长度                     
    iArray[19][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
    iArray[20]=new Array();                                   
    iArray[20][0]="实际赔偿金额";         //列名                    
    iArray[20][1]="200px";         //宽度                     
    iArray[20][2]=100;         //最大长度                     
    iArray[20][3]=0;         //是否允许录入，0--不能，1--允许 
                                                              
                                                              
    iArray[21]=new Array();                                   
    iArray[21][0]="实际给付金额";         //列名                                         
    iArray[21][1]="200px";         //宽度                                          
    iArray[21][2]=100;         //最大长度                                          
    iArray[21][3]=0;         //是否允许录入，0--不能，1--允许                      
                                                                                   
    iArray[22]=new Array();                                                        
    iArray[22][0]="分保赔款金额";         //列名                                         
    iArray[22][1]="200px";         //宽度                                          
    iArray[22][2]=100;         //最大长度                                          
    iArray[22][3]=0;         //是否允许录入，0--不能，1--允许        
    
    iArray[23]=new Array();                                                        
    iArray[23][0]="分保给付金额";         //列名                                         
    iArray[23][1]="200px";         //宽度                                          
    iArray[23][2]=100;         //最大长度                                          
    iArray[23][3]=0;         //是否允许录入，0--不能，1--允许  
    GLPGrid = new MulLineEnter( "fm" , "GLPGrid" ); 

    //这些属性必须在loadMulLine前
    GLPGrid.mulLineCount = 0;   
    GLPGrid.displayTitle = 1;

    //GLPGrid.canSel = 0;
    GLPGrid.hiddenPlus = 1;        //隐藏加号
    GLPGrid.hiddenSubtraction = 1; //隐藏减号
    
    GLPGrid.loadMulLine(iArray);  
  } catch(ex) {
    alert("初始化Grid时出错："+ ex);
  }  
}
</script>