<%
//程序名称：LARakeOffQuery.jsp
//程序功能：报表生成
//创建日期：2005-5-26 9:30
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
  <title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    ReportUI
  </title>
  <head>
  </head>
  <body>
    <%
      String flag = "0";
      String FlagStr = "";
      String Content = "";

      //设置模板名称
      String FileName ="LARakeOffQueryList";
      //取得画面的所有条件
      String stWherePm = request.getParameter("pmWhere");
      System.out.println("提取画面条件"+stWherePm);
      
      //开始进行报表处理
      JRptList t_Rpt = new JRptList();
      //输出报表文件名
      String tOutFileName = "";
      System.out.println("开始进行报表处理！");
      //设置报表属性
      t_Rpt.m_NeedProcess = false;
      t_Rpt.m_Need_Preview = false;
      t_Rpt.mNeedExcel = true;
      
      //设置参数
      t_Rpt.AddVar("pmWhere", stWherePm);
      System.out.println("传参！");
      //设置报表名称
      t_Rpt.Prt_RptList(pageContext,FileName);
      System.out.println("设置报表名称："+FileName);
      //取得生成报表的文件名
      tOutFileName = t_Rpt.mOutWebReportURL;
      System.out.println("生成报表："+tOutFileName);
      System.out.println("报表处理结束！");
    %>
    
    <form name="fm"  method="post"  action="../web/ShowF1Report.jsp" >
      <input name="FileName" type="hidden" value="<%=tOutFileName%>">
    </form>
  </body>
</html>
<script language="javascript">
  var rptError=" ";
  
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
</script >