<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LAAgentOperation.jsp
//程序功能：F1报表生成
//创建日期：2006-02-20
//创建人  ：WL
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");
  	String tManageCom=tG.ManageCom;
  	int len=tManageCom.length();
%>
<%
   String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<script>
  var msql="1 and branchtype =#1# and branchtype2=#01#" 
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LAAgentOperation.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./LAAgentOperationReport.jsp" method=post name=fm >
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName> 
          </TD> 
		   <TD  class= title>
            销售单位
          </TD>
          <TD  class= input>
            <Input class=common name=OperationBranch verify="销售单位" >
          </TD>  
              
        </TR>
        <TR  class= common>
        
        <TD class=title>统计状态</TD>
       		<td class=input>
       		<Input class="codeno" name = AgentState verify="统计状态|NOTNULL"  CodeData = "0|^01|在职|^02|离职|^03|全部"
       		 ondblclick = "return showCodeListEx('AgentState',[this,AgentStateName],[0,1]);"
       		 onkeyup = "return showCodeListKeyEx('AgentState',[this,AgentStateName],[0,1]);"><Input class="codename" name= AgentStateName readonly=true>
       	</td>  
       	 <TD class=title>业务员入司职级</TD>
       		<TD class= input>
 				<Input name=AgentGrade class="codeno"  CodeData="0|^A01|见习客户顾问|^A02|客户顾问|^A04|客户经理|^A06|客户总监|^B01|营业组经理|^B02|营业处主任|^B11|营业区经理"
 				ondblclick="return showCodeListEx('pAgentGrade',[this,AgentGradeName],[0,1]);"
 				onkeyup="return showCodeListKeyEx('pAgentGrade',[this,AgentGradeName],[0,1]);"
 				><Input name=AgentGradeName class="codename" elementtype=nacessary >
 			</TD>
        	
        	<!--
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input name=OperationStartDate class="coolDatePicker" dateFormat="short" verify="结束日期|NOTNULL"> 
          </TD>   
          
          <TD  class= title>
            结束日期
          </TD>
          <TD  class= input>
          	<Input name=OperationEndDate class="coolDatePicker" dateFormat="short" verify="结束日期|NOTNULL"> 
          </TD>  
           -->     
        </TR>
    </table>

    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type="hidden" name=BranchType value='<%=BranchType%>'>
    <input type="hidden" name=BranchType2 value='<%=BranchType2%>'>
		<!--<INPUT VALUE="CSV打印" class="cssButton" TYPE="button" onclick="submitForm()"> -->
		<INPUT VALUE="EXECL打印" class="cssButton" TYPE="button" onclick="download()">	
		 <!-- <p><font color='red'>注：数据量较大时,建议使用CSV下载</font></p> -->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 