<%@page import="java.io.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.f1j.ss.*"%>
<%@page import="java.util.*" %>
<%
	try {
		String strTemplatePath = application.getRealPath("f1print/picctemplate")+"/";
		String templateName = request.getParameter("TemplateName");
		F1PrintParser fp = null;
		InputStream ins = (InputStream) session.getValue("PrintStream");
		if( ins == null ) {
			XmlExport xmlExport = new XmlExport();
			xmlExport.createDocument("nofound.vts", "printer");
  		fp = new F1PrintParser(xmlExport.getInputStream(), strTemplatePath);
		} else {
			fp = new F1PrintParser(ins, strTemplatePath, templateName);				
		}

		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		// Output VTS file to a buffer
    if (!fp.output(dataStream) ) {
      System.out.println("F1PrintKernel.jsp : fail to parse print data");
    }

		byte[] bArr = dataStream.toByteArray();
		dataStream.close();
		
		InputStream insVTS = new ByteArrayInputStream(bArr);
		OutputStream ous = response.getOutputStream();
		ous.write(bArr);
		ous.flush();
		ous.close();
		insVTS.reset();
	  session.putValue("F1PrintData", insVTS);
  }catch(java.net.MalformedURLException urlEx){
    urlEx.printStackTrace();
  }catch(java.io.IOException ioEx){
    ioEx.printStackTrace();
  }catch(Exception ex){
  	ex.printStackTrace();
  }
  session.removeAttribute("PrintStream");
%>