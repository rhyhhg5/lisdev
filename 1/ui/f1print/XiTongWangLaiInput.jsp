<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：XiTongWangLaiInput.jsp
	//程序功能：系统往来校验
	//创建日期：2017-11-11 
	//创建人  ：弓彩霞
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>

<%
	//添加页面控件的初始化。
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="XiTongWangLai.js"></SCRIPT>

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="XiTongWangLaiInit.jsp"%>
</head>

<body onload="initForm();">
<form method=post name=fm target="fraSubmit">
<!-- 流水号码 --> 
<input type=hidden name=serialNo>
<input type=hidden name=ExportExcelSQL>
<input type=hidden name=ClassType>
  <table>
    	<tr> 
    		<td class= titleImg>请输入查询条件</td>   		 
    	</tr>
  </table> 
 
  <p>
				<strong>输入查询的时间范围</strong>
				<Div id="divFCDay" style="display: ''">
					<table class="common">
						<TR class=common>
							<TD class=title>
								起始时间
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="StartDay"
									name="StartDay" verify="起始时间|NOTNULL">
							</TD>
							<TD class=title>
								结束时间
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="EndDay"
									name="EndDay" verify="结束时间|NOTNULL">
							</TD>
    		
						</TR>
					</table>
				</Div>
<p>
<INPUT VALUE="查询" class=cssButton TYPE=button onclick="XiTongWangLaiKeMuQuery();">
<br>
<hr width=98%>
<br>
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,XiTongWangLaiKeMuDataGrid);"></td>
		<td class=titleImg>查询结果</td>
	</tr>
</table>

<Div id="divinitXiTongWangLaiKeMuDataGrid" style="display: ''" align=center>
<table class=common>
	<tr class=common>
		<td text-align: left colSpan=1><!-- 日结试算生成的批次 --> <span id="spanXiTongWangLaiKeMuDataGrid"> </span></td>
	</tr>
</table>
<INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
<INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
</div>


</form>

<span id="spanCode" style="display: none; position:absolute; slategray"></span>

</body>
</html>
