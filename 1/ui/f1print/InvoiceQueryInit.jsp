<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 
<%
//程序名称：FinFeeSureInit.jsp
//程序功能：到帐确认
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
    GlobalInput globalInput = (GlobalInput)session.getValue("GI");
    String strManageCom = globalInput.ComCode;
    String qurOperater = globalInput.Operator;
%>  
                           

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  { 
    var sql = "select current date - 10 days,current date from dual ";
    var rs = easyExecSql(sql);
    fm.StartDate.value = rs[0][0];
    fm.EndDate.value = rs[0][1];
    fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
        fm.all('ManageCom').readOnly=false;
        }
    else{
        fm.all('ManageCom').readOnly=true;
        }
        if(fm.all('ManageCom').value!=null)
    {
        var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
        }                            
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initQueryInvoiceGrid();
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initQueryInvoiceGrid()
{      
var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";                //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";                  //列宽
      iArray[0][2]=10;                  //列最大值
      iArray[0][3]=0;                   //是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="实收号码";        //列名
      iArray[1][1]="100px";                 //列宽
      iArray[1][2]=70;                  //列最大值
      iArray[1][3]=0;
      
      iArray[2]=new Array();
      iArray[2][0]="实收号码类型";        //列名
      iArray[2][1]="60px";                 //列宽
      iArray[2][2]=70;                  //列最大值
      iArray[2][3]=2;                   //是否允许输入,1表示允许，0表示不允许   
      iArray[2][10] = "NoType1";
      iArray[2][11] = "0|^1|集体保单号^2|个人保单号^3|团单批单号^10|个单批单号";
      iArray[2][12] = "3";
      iArray[2][18]=300;
      iArray[2][19] = "0";

      iArray[3]=new Array();
      iArray[3][0]="总实收金额";       //列名
      iArray[3][1]="80px";                 //列宽
      iArray[3][2]=60;                  //列最大值
      iArray[3][3]=0;                   //是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="打印日期";               //列名
      iArray[4][1]="60px";                 //列宽
      iArray[4][2]=60;                      //列最大值
      iArray[4][3]=0;   
      
      iArray[5]=new Array();
      iArray[5][0]="打印金额";               //列名
      iArray[5][1]="80px";                 //列宽
      iArray[5][2]=60;                      //列最大值
      iArray[5][3]=0;                     //是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="付费人";              //列名
      iArray[6][1]="60px";                 //列宽
      iArray[6][2]=60;                      //列最大值
      iArray[6][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="操作员";              //列名
      iArray[7][1]="80px";                 //列宽
      iArray[7][2]=60;                      //列最大值
      iArray[7][3]=0; 
      
      InvoiceGrid = new MulLineEnter( "fm" , "InvoiceGrid" );   
      InvoiceGrid.displayTitle = 1;
      InvoiceGrid.hiddenPlus = 1;
      InvoiceGrid.mulLineCount = 0;  
      InvoiceGrid.hiddenSubtraction = 1;
      InvoiceGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        window.alert(ex);
      }
      }

</script>