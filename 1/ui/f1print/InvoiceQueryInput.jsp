<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//创建日期：2012-2
//创建人  ：ZCX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="InvoiceQueryInput.js"></SCRIPT>
  <%@include file="InvoiceQueryInit.jsp"%>
  <title>拆分打印查询查询</title>
</head>

<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./BatchSendQuerySave.jsp">
    <table class= common border=0 width=100%>
        <tr>
            <td class= titleImg align= center>请输入查询条件：</td>
        </tr>
    </table>
    <table  class= common align=center>  
        <TR  class= common>
           <TD  class= title>  实收号码类型  </TD>
        <TD  class= input>	<Input class=codeno name=IncomeType elementtype=nacessary CodeData="0|^1|集体保单号^2|个人保单号^3|团单批单号^10|个单批单号" ondblClick="showCodeListEx('IncomeType',[this,IncomeTypeName],[0,1]);" onkeyup="showCodeListKeyEx('IncomeType',[this,IncomeTypeName],[0,1]);"><input class=codename name =IncomeTypeName></TD>
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate verify="起始日期|date&NOTNULL" >
            <font color="#FF0000" align="Left">*</font>
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate verify="终止日期|date&NOTNULL" >
            <font color="#FF0000" align="Left">*</font>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title> 实收号码</TD> 
          <TD  class= input> <Input class= common name=IncomeNo > </TD>
          <TD  class= title> 操作员</TD>
          <TD  class= input>  <Input class= common name=Operator ></TD> 
          <TD  class= title>
            管理机构
          </TD>
        <TD  class= input>
          <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
          <font color="#FF0000" align="Left">*</font>
         </TD>
         </TR>
    </table> 
    <table align=right>
      <tr>
        <td> 
          <INPUT VALUE="查 询" class=cssButton TYPE=button onclick="easyQueryClick();">
        </td>
      </tr>
    </table>
    <br>
    <br>
    <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <td text-align: left colSpan=1>
            <span id="spanInvoiceGrid" ></span> 
        </td>
      </TR>
    </Table>                    
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">                  
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div> 
 <INPUT type=hidden name=sql>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
