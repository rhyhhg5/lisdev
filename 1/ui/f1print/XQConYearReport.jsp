<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：XQConYearReport.jsp
//程序功能：F1报表生成
//创建日期：2006-02-20
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%
		System.out.println("start");
		CError cError = new CError( );
		boolean operFlag=true;
		String tRela  = "";
		String FlagStr = "";
		String Content = "";
		String strOperation = "";		

		String ManageCom = request.getParameter("ManageCom");
		String ManageComs = request.getParameter("ManageComs");
		System.out.println(ManageCom);
		System.out.println(ManageComs);
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput)session.getValue("GI");
		LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
		
		tLJSPayBSchema.setManageCom(ManageCom);
		System.out.println("tLJSPayBSchema.getManageCom():" + tLJSPayBSchema.getManageCom());
		LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();			
		VData mResult = new VData();
		CErrors mErrors = new CErrors();
		
		VData tVData = new VData();
		tVData.addElement(tLJSPayBSchema);
		tVData.addElement(tLJAPayPersonSchema);
		tVData.addElement(tG);
		//tVData.addElement(tLJSPayBDB);
		
		XQConManageStatisticBL tXQConManageStatisticBL = new XQConManageStatisticBL();
		XmlExport txmlExport = new XmlExport();
    if(!tXQConManageStatisticBL.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tXQConManageStatisticBL.mErrors.getFirstError().toString();
    }
    else
    {
			mResult = tXQConManageStatisticBL.getResult();
			txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
			if(txmlExport==null)
			{
				operFlag=false;
				Content="没有得到要显示的数据文件";
			}
		}

if (operFlag==true)
		{
			session.putValue("PrintStream", txmlExport.getInputStream());
			response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
		}
		else
		{
    	FlagStr = "Fail";
%><html><script language="javascript">
		alert("<%=Content%>");
		top.close();
	</script></html><%
  }
%>