
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";			//记录登陆机构
	var RePrintFlag = '<%=request.getParameter("RePrintFlag")%>';//记录是否重打
	var Flag = '<%=request.getParameter("Flag")%>';
	var LoadFlag = '<%=request.getParameter("LoadFlag")%>';
	var saleChnl = "";//销售渠道   zhangjianbao   2007-11-16
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./FirstHasten.js"></SCRIPT>
  
  <%@include file="./FirstHastenInit.jsp"%>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <title>首期保费催缴通知</title>
</head>

<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入投保单查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <!--<TD  class= title>  投保单号码   </TD>
          <TD  class= input> --> <Input class= common name=ContNo  type="hidden">
          <TD  class= title>  印 刷 号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo verify="印刷号码|int"> </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
        </TR> 
        <TR  class= common>
         	<TD  class= title> 投保人</TD>
          <TD  class= input><Input class="common" name=AppntName verify="投保人|len<=20"></TD>
          <TD  class= title>  生效日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=CValiDate verify="生效日期|date"> </TD>
          <TD  class= title> 下发日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=MakeDate verify="下发日期|date">   </TD>
        </TR> 
        <TR class= common>
         	<TD  class= title> 申请日期 </TD>
          <TD  class= input><Input class="coolDatePicker" name=PolApplyDate verify="申请日期|date"></TD>
         <TD  class= title>
           缴费方式
          </TD>
          <TD  class= input>
            <Input NAME=PayMode VALUE=""  CLASS=codeNo CodeData="0|^1|现金^4|银行转账" MAXLENGTH=20  ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" verify="缴费方式|notnull&code:PayMode"><input class=codename name=PayModeName readonly=true elementtype=nacessary> 
          </TD>                   
        <TD  class= title> 是否已打印</TD>
          <TD  class= input><Input class="codeNo" name=StateFlag CodeData="0|4^0|未打印^1|已打印^9|所有" ondblclick="showCodeListEx('StateFlag',[this,StateFlagName],[0,1]);" onkeyup="return showCodeListKey('StateFlag',[this,StateFlagName],[0,1]);"><input class=codename name=StateFlagName readonly=true ><input class=readonly name=StateFlagName2 value="未打印" readonly=true >  </TD>
          <TD  class= title> </TD>
          <TD  class= input></TD>
        </TR>
       <tr>
         	<TD  class= title> 销售渠道 </TD>
          <TD  class= input><Input class="codeNo" name=saleChannel ondblclick="return showCodeList('statuskind',[this,saleChannelName],[0,1]);" onkeyup="return showCodeListKey('statuskind',[this,saleChannelName],[0,1]);"><input class=codename name=saleChannelName readonly=true ></TD>
          <TD  class= title> 营销机构 </TD>
          <TD  class= input><Input class="codeNo" name=branchAttr readonly ondblclick="beforeCodeSelect();return showCodeList('branchattrandagentgroup',[this,branchAttrName,agentGroup],[0,1,2],null,saleChnl,'saleChannel',1,200);"><input class=codename name=branchAttrName readonly=true ></TD>
      </tr>
    </table>
    <input type=hidden name="agentGroup">
		<INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 投保单信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  						<span id="spanPolGrid" ></span> 
  			  	</td>
  				</tr>
    		</table>
    	
      <INPUT CLASS=CssButton VALUE="首  页" class= CssButton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=CssButton VALUE="上一页" class= CssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=CssButton VALUE="下一页" class= CssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=CssButton VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage.lastPage();"> 				
  </Div>
  <br>
    	 <Div  id= "divLCPol4" style= "display: 'none'">
    <TR  class= common> 
      <TD width="100%" height="15%"  class= title>转帐失败原因</TD>
    </TR>
    <TR  class= common>
      <TD height="85%"  class= title><textarea name="Content" cols="125" rows="5" class="common" ></textarea></TD>
    </TR>
	</div>  
  
  
  <Div  id= "divLCPol3" style= "display: ''">
      <INPUT VALUE="打印首期保费催缴通知书" class= CssButton Name=Firstpay2 TYPE=button onclick="printHasten();">
			<!--INPUT VALUE="打印首期保费催缴通知书(pdf)" class= CssButton Name=Firstpay1 TYPE=button onclick="printPolpdf();"-->
			<!--<INPUT VALUE="打印首期保费催缴通知书(pdf)" class= CssButton Name=Firstpay1 TYPE=button onclick="printPolpdfNew();">-->
			<!--<INPUT VALUE="批打首期保费催缴通知书(pdf)" class= CssButton Name=Firstpaypdfbat TYPE=button onclick="printPolpdfbat();">-->
	</div>
  <INPUT Name="CustPrtNo" 		TYPE="hidden" >                       	
  <INPUT Name="AppName" 			TYPE="hidden" >
  <INPUT Name="AppAddress" 		TYPE="hidden" >
  <INPUT Name="AppZipCode" 		TYPE="hidden" >
  <INPUT Name="AppCValiDate"	TYPE="hidden" >
  
  <INPUT Name="Prem"					TYPE="hidden" >  
  <INPUT Name="ComLetter"			TYPE="hidden" >
  <INPUT Name="ComAddres" 		TYPE="hidden" >
  <INPUT Name="ComZipCode" 		TYPE="hidden" >
  <INPUT Name="ComFax"				TYPE="hidden" >
  
  <INPUT Name="ComPhone"			TYPE="hidden" >   
  <INPUT Name="CuSex"					TYPE="hidden" >
  <INPUT Name="AgentNo"				TYPE="hidden" >
  <INPUT Name="ApplyDate" 		TYPE="hidden" >
  <INPUT Name="UWDate" 				TYPE="hidden" >
  
  <INPUT Name="AgentName"	 		TYPE="hidden" > 
	<INPUT Name="AgentPhone"		TYPE="hidden" >  
	<INPUT Name="GetNoticeNo"		TYPE="hidden" >   
	<INPUT Name="FirstSendDate"	TYPE="hidden" >        
  <INPUT Name="EndHastenDate"	TYPE="hidden" > 
  
  <INPUT Name="FailReason"		TYPE="hidden" > 
  <INPUT Name="BankName"	    TYPE="hidden" >        
  <INPUT Name="BankAccNo"	    TYPE="hidden" > 
  <INPUT Name="AccName"		    TYPE="hidden" > 
  <INPUT Name="PrtSeq"		    TYPE="hidden" >
  <input type=hidden id="fmtransact" name="fmtransact">
  </form>
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form id=printform target="printfrm" method = post action="">
      		<input type=hidden name=filename value=""/>
  		</form>
</body>
</html> 