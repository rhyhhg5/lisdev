<%@page contentType="text/html;charset=GBK" %>
<%
//name : PNoticeBillSave.jsp
//functon : Receive data and transfer data
//Creator  ：刘岩松
//Date ：2003-02-14
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.f1print.*"%>
<%
  System.out.println("开始执行打印操作");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";

  	String strMngCom    = request.getParameter("ManageCom");
  	//String strAgentCode = request.getParameter("AgentCode");
  	String strStartDate = request.getParameter("StartDate");
  	String strEndDate   = request.getParameter("EndDate");
  	//String strFlag      = request.getParameter("Flag");
    //String strStateFlag = request.getParameter("StateFlag");
    String strSQL = request.getParameter("StrSQL");

  	PNoticeBillUI tPNoticeBillUI = new PNoticeBillUI();
  	//System.out.println("管理机构是"+strMngCom);
  	//System.out.println("代理人编码是"+strAgentCode);
  	System.out.println("开始日期是"+strStartDate);
  	System.out.println("结束日期是"+strEndDate);
  	//System.out.println("标志是"+strFlag);
    //System.out.println("打印状态是"+strStateFlag);
    System.out.println("查询语句为:"+strSQL); 
  VData tVData = new VData();
  VData mResult = new VData();
  try
  {
    tVData.addElement(strMngCom);
    //tVData.addElement(strAgentCode);
    tVData.addElement(strStartDate);
    tVData.addElement(strEndDate);
    //tVData.addElement(strFlag);
    // tVData.addElement(strStateFlag);
    tVData.addElement(strSQL);
    tPNoticeBillUI.submitData(tVData,"PRINT");
  }
  catch(Exception ex)
  {
    Content = "PRINT"+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  mResult = tPNoticeBillUI.getResult();
  XmlExport txmlExport = new XmlExport();
  txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
  if (txmlExport==null)
  {
    System.out.println("null");
     tError = tPNoticeBillUI.mErrors;
    Content = "打印失败,原因是＝＝"+tError.getFirstError();
    FlagStr = "Fail";
  }
  else
  {
  	session.putValue("PrintStream", txmlExport.getInputStream());
  	System.out.println("put session value");
  	response.sendRedirect("../f1print/GetF1Print.jsp");
  }
  %>
  <html>
  <script language="javascript">
	alert("<%=Content%>");
	top.opener.focus();
	top.close();
</script>
</html>