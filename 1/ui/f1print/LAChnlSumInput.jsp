<%
//程序名称：LAChnlSumInput.jsp
//程序功能：渠道综合报表
//创建日期：2008-04-16
//创建人  ：xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  	GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
		String Managecom2 = tG.ManageCom;
		System.out.println("tManageCom"+Managecom2);
%>
<script>
 var tsql =" 1 and  comcode Like #"+'<%=Managecom2%>'+"%#  and char(length(trim(comcode))) in (#4#,#2#) " ;
 var msql =" 1 and  comcode Like #"+'<%=Managecom2%>'+"%#  and char(length(trim(comcode))) =#8# " ;

</script> 

<html>    
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAChnlSumInput.js"></SCRIPT>  

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAChnlSumInit.jsp"%>     
  <title>渠道综合报表</title>
</head>      
<body  onload="initForm();initElementtype();" >    
  <form action= "./LAChnlSumReport.jsp" method=post name=fm target="fraSubmit">
  <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		渠道保费明细统计条件
       		 </td>   		 
    	</tr>
    </table>


    <Div  id= "divAgent1" style= "display: ''">
   <Table class= common>
     <TR class= common> 
          <TD  class= title>管理机构</TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom     verify="管理机构"  
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,1);"><input class=codename name=ManageComName readonly=true   >
          </TD> 
          <!--
          <TD  class= title>管理机构</TD>
          <TD  class= input>
            <Input class="codeno" name=ManageComPart     verify="管理机构|NOTNULL"
            ondblclick="return showCodeList('comcode',[this,ManageComNamePart],[0,1],null,8,'char(length(trim(comcode)))',1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComNamePart],[0,1],null,8,'char(length(trim(comcode)))',1);"><input class=codename name=ManageComNamePart readonly=true   >
          </TD> 
          -->
           <TD  class= title>三级管理机构</TD>
           <TD  class= input>
           <input class=codeno name=ManageCom3 
            ondblclick="return getmanage(this,ManageCom3Name);"
            onkeyup="return getmanage(this,ManageCom3Name);"><input class=codename readonly  name=ManageCom3Name >
           </TD> 
           </TR>     
     
       <TR class=common>
		      <TD  class= title>统计起期</TD>
		      <TD  class= input>
		       <Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="统计起期"  elementtype=nacessary >
		      </TD>
       		<TD  class= title>统计止期</TD>
       		<TD  class= input>
       		  <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="统计止期" elementtype=nacessary>
       		</TD>
       </TR>    
   	 </Table>  
   	 </Div>
   	    <input type=hidden name=Managecom2  value ='<%=Managecom2%>'>

    <INPUT VALUE="打  印" class= cssbutton TYPE=button onclick="submitForm();"> 	
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html> 	
