<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<% 
//程序名称：
//程序功能：
//创建日期：2002-12-20
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="GetCredenceInput.js"></SCRIPT> 
  <%@include file="GetCredenceInit.jsp"%>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
	<title>给付凭证打印</title>
</head>

<body  onload="initForm();" >    
  <form  method=post name=fm target="fraSubmit">
    <table class= common border=0 width=100%>
    	<tr> 
    		<td class=common  width=2% >
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGetCre);">
    		</td>
    		<td class= titleImg align=left>
        		输入查询条件
       	</td>   		      
    	</tr>
    </table>
    <Div  id= "divGetCre" style= "display: ''">
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>  实付号码  </TD>
          <TD  class= input>  <Input class= common name=ActuGetNo >  </TD>
          <TD  class= title>  其它号码 </TD>
          <TD  class= input> <Input class= common name=OtherNo >  </TD>
          <TD  class= title>  其它号码类型 </TD>
          <TD  class= input>	<Input class=codeno name=OtherNoType verify="其它号码类型" CodeData="0|^0|生存领取合同号^1|生存领取集体保单号^2|生存领取个人保单号^3|批改号^4|暂交费退费给付通知书号^5|赔付应收给付通知书号^6|(个单)其他退费给付通知书号^7|红利个人保单号^8|(团单)其他退费给付通知书号" ondblClick="showCodeListEx('OtherNoType',[this,OtherNoTypeName],[0,1]);" onkeyup="showCodeListKeyEx('OtherNoType',[this,OtherNoTypeName],[0,1]);"><input class=codename name=OtherNoTypeName readonly=true >  </TD>                     
          </TR>
        <TR  class= common>
          <TD  class= title> 应付日期  </TD>
          <TD  class= input><Input class= "coolDatePicker" dateFormat="short" name=ShouldDate > </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   
         <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
        </TR>
    </table>
    <table align=right>
      <tr>
        <td>
    	<INPUT VALUE="查  询" class= cssButton TYPE=button onclick="easyQueryClick();"> 
    	</td>
      </tr>
    </table>
    </Div>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJAGet1);">
    		</td>
    		<td class= titleImg>
    			 给付信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLJAGet1" style= "display: ''">
      <table class= common>
        <TR  class= common>
					<td text-align: left colSpan=1>
		  			<span id="spanPolGrid" >
		  			</span> 
		  		</td>	
				</TR>    	 
      </table>
      <div align=center >
		      <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="turnPage.firstPage();"> 
		      <INPUT VALUE="上一页" class= cssButton  TYPE=button onclick="turnPage.previousPage();"> 					
		      <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="turnPage.nextPage();"> 
		      <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="turnPage.lastPage();">				
		
	  </div>  
	  <br/>	
    <table align=right>
      <tr>
        <td>	 
	  <input type=Button class= cssButton name="GetPrint" value="给付凭证打印" onclick="GPrint()"> 
	</td>
      </tr>
    </table>		
    </Div>
        <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 