//
//程序名称：BaoBankDataQuery.js
//程序功能：财务报盘数据查询
//创建日期：2011-6-30
//创建人  ：wanghongliang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mSql="";

//日期校验
function checkShiJian()
{
    //限制起至时间为3个月
	var startDate = fm.all('SendDate').value;
	var endDate = fm.all('SendEndDate').value;
	if(startDate==null||startDate==""||startDate=="null")
	{
	  alert("报盘起期不能为空！")
	  return false;
	}
	if(endDate==null||endDate==""||endDate=="null")
	{
	  alert("报盘止期不能为空！")
	  return false;
	}
	var t1 = new Date(startDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(endDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    var contNo=fm.all('ContNo').value;
	if(contNo==null||trim(contNo)==""||contNo=="null")
	{
	    if(tMinus>92 )
	    {
		  alert("查询起止时间不能超过3个月!");
		  return false;
	    }
	}
	return true;
}

//报盘及回盘限制
function baoPanJiHuiPan()
{
   var BankSuccFlag=fm.all('BankSuccFlag').value;
   var BankState=fm.all('BankState').value;
   
   if(BankSuccFlag=="1")
   {
      if(BankState=="1"||BankState=="2"||BankState=="3"||BankState=="4")
      {
         alert("回盘结果选【成功】，报盘状态只能选【全部】或【已回盘处理】，请重新选择！");
         return false;
      }
   }
   return true;
}

//简单查询
function easyQuery()
{
    if(!checkShiJian())
    {
        return false;
    }
    if(!baoPanJiHuiPan())
    {
        return false;
    }
    var ManageCom=fm.all('ManageCom').value;
    var BankCode=fm.all('BankCode').value;
    var SendDate=fm.all('SendDate').value;
    var SendEndDate=fm.all('SendEndDate').value;
    var ContNo=fm.all('ContNo').value;
    var BankAccNo=fm.all('BankAccNo').value;
    var BankState=fm.all('BankState').value;
    var BankSuccFlag=fm.all('BankSuccFlag').value;
    var SaleChnl=fm.all('SaleChnl').value;
    var sqlWherePar="";
    
    var sqlFeiWherePar="";
    var sqlFeiYouChuOnHou="";
    var sqlFeiYouChuWherePar="";
    if(ManageCom!=null&&ManageCom!=""&&ManageCom!="null")
    {
       sqlWherePar+=" and b.ManageCom like '"+ManageCom+"%' "
    }
    if(trim(BankCode)!=null&&trim(BankCode)!=""&&trim(BankCode)!="null")
    {
       sqlWherePar+=" and b.BankCode = '"+BankCode+"' ";
    }
    if(SendDate!=null&&SendDate!=""&&SendDate!="null"&&SendEndDate!=null&&SendEndDate!=""&&SendEndDate!="null")
    {
       //sqlWherePar+=" and b.StartPayDate between '"+SendDate+"' and '"+SendEndDate+"' ";
       sqlWherePar+=" and exists (select 1 from ljspaypersonb where getnoticeno=b.getnoticeno and lastpaytodate between '"+SendDate+"' and '"+SendEndDate+"' )";
    }
    if(BankAccNo!=null&&BankAccNo!=""&&BankAccNo!="null")
    {
       sqlWherePar+=" and b.BankAccNo = '"+BankAccNo+"' ";
    }
    if(ContNo!=null&&ContNo!=""&&ContNo!="null")
    {
       sqlWherePar+=" and b.OtherNo = '"+ContNo+"' ";
    }
    if(SaleChnl!=null&&SaleChnl!=""&&SaleChnl!="null")
    {
       if(SaleChnl=="0")
       {
          
       }
       if(SaleChnl=="1")
       {
           sqlWherePar+=" and exists (select 1 from lccont where contno=b.otherno and salechnl not in ('04','13')) "; 
       }
       if(SaleChnl=="2")
       {
           sqlWherePar+=" and exists (select 1 from lccont where contno=b.otherno and salechnl  in ('04','13')) "; 
       }
    }
    
    
    

    
    if(BankState!=null&&BankState!=""&&BankState!="null")
    {
       if(BankState=="0")//全部
       {
           sqlFeiYouChuOnHou+=""; 
       }
       if(BankState=="1")//已抽档未抽盘
       {
           sqlFeiYouChuOnHou+=" and  c.serialno is null and exists (select 1 from ljspay where getnoticeno=a.getnoticeno)"; 
       }
       if(BankState=="2")//已抽盘未发盘
       {
           sqlFeiYouChuOnHou+=" and  exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is null  ) ";  
       }
       if(BankState=="3")//已发盘未回盘
       {
           sqlFeiYouChuOnHou+=" and exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is not null and y.infile  is null) "; 
       }
       if(BankState=="4")//已回盘未处理
       {
           sqlFeiYouChuOnHou+=" and  exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is not null and y.infile  is not null and not exists(select 1 from lyreturnfrombankb z where z.serialno=y.serialno))"; 
       }
       if(BankState=="5")//已回盘处理
       {
           sqlFeiYouChuOnHou+=" and exists (select 1 from lyreturnfrombankb x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno  and not exists (select 1 from lysendtobank where paycode=a.getnoticeno ))"; 
       }  
    }
    if(BankSuccFlag!=null&&BankSuccFlag!=""&&BankSuccFlag!="null")
    {
        if(BankSuccFlag=="0")//全部
        {
            
        }
        if(BankSuccFlag=="1")//成功
        {
        	sqlFeiYouChuWherePar+=" and  exists (select 1 from lyreturnfrombankb x,lybanklog y where x.serialno=y.serialno and  x.paycode=b.getnoticeno and not exists (select 1 from lysendtobank where paycode=b.getnoticeno) and  exists (select 1 from ljtempfee where tempfeeno=b.getnoticeno ) )";
        }
        if(BankSuccFlag=="2")//未成功
        {
            sqlFeiYouChuWherePar+=" and not exists (select 1 from ljtempfee where tempfeeno=b.getnoticeno) ";
        }
    }
    sqlFeiWherePar=sqlWherePar+sqlFeiYouChuWherePar;
    //非邮储的查询SQL
	var feiYouChuSQL ="select int(ROW_NUMBER() OVER()) 序号列, "
	           +"(select name from ldcom where comcode =(select managecom from ljspayb where getnoticeno=a.getnoticeno)) 管理机构,  "
	           +"(select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12) fetch first 1 row only) from lccont x,laagent y,labranchgroup z where x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.otherno  union select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12) fetch first 1 row only) from lbcont x,laagent y,labranchgroup z where  x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.otherno fetch first 1 row only) 营销部门, "
	           +"a.otherno 保单号, "
	           +"(select lastpaytodate from ljspaypersonb where getnoticeno=a.getnoticeno fetch first 1 row only) 应缴日期, "
	           +"(select accname from ljspayb where getnoticeno=a.getnoticeno) 账户名, "
	           +"(select bankname from ldbank where bankcode = (select BankCode from ljspayb where getnoticeno=a.getnoticeno)) 银行名称, "
	           +"(select xx.tt from (select y.startdate tt, x.serialno serialno ,x.paycode  paycode from lysendtobank x, lybanklog y where x.serialno = y.serialno  union select y.startdate tt, x.serialno serialno , x.paycode  paycode  from lyreturnfrombank x, lybanklog y  where x.serialno = y.serialno   ) as xx where  xx.paycode = a.getnoticeno  order by serialno desc fetch first 1 rows only) 抽盘日期, "
	           +"(select xx.tt from (select y.senddate tt,  x.serialno serialno ,x.paycode  paycode from lysendtobank x, lybanklog y where x.serialno = y.serialno  union select y.senddate tt,  x.serialno serialno ,x.paycode  paycode  from lyreturnfrombank x, lybanklog y  where x.serialno = y.serialno   ) as xx where  xx.paycode = a.getnoticeno  order by  serialno desc fetch first 1 rows only) 发盘日期, "
	           +"(select sumduepaymoney from ljspayb where getnoticeno=a.getnoticeno ) 发盘金额 , "
	           +"   (case when c.serialno is null and exists (select 1 from ljspay where getnoticeno=a.getnoticeno) then '已抽档未抽盘'"
	           +"         when exists (select 1 from ljspayb where getnoticeno = a.getnoticeno and dealstate in ('2','3') )  then '应收已作废'"
	           +"         when exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is null  )  then '已抽盘未发盘'"
	           +"         when exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is not null and y.infile  is null )  then '已发盘未回盘'"
	           +"         when exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is not null and y.infile  is not null and not exists(select 1 from lyreturnfrombankb z where z.serialno=y.serialno)) then '已回盘未处理'"
	           +"         when exists (select 1 from lyreturnfrombank a Where Not Exists (Select 1 From Lysendtobank Where Paycode = a.Paycode And Serialno = a.Serialno)) then '已回盘处理'"
	           +"         when exists (select 1 from lyreturnfrombankb x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno  and not exists (select 1 from lysendtobank where paycode=a.getnoticeno )) then '已回盘处理'"
	           +"         when exists (select 1 from ljtempfeeclass where tempfeeno = a.getnoticeno and paymode!='4' )  then '非银行转账收费'"
	           +"         else '未知状态'"
	           +"     end )"
	           +" 状态, "
	           +"(select returndate from lybanklog where  serialno=c.serialno fetch first 1 rows only )  回盘时间,"
	           +"(select modifydate from lyreturnfrombankb where serialno=c.serialno fetch first 1 rows only ) 回盘处理时间,"
	           +"(case when ((exists " +
	           		"(select 1 " +
	           		"  from lyreturnfrombankb x, lybanklog y  " +
	           		" where x.serialno = y.serialno  " +
	           		" and x.paycode = a.getnoticeno   and not exists (select 1  " +
	           		"  from lysendtobank  where paycode = a.getnoticeno)   and exists  " +
	           		"(select 1 from ljtempfee where tempfeeno = a.getnoticeno)  " +
	           		"  ))or exists (select 1     from lyreturnfrombank x, lybanklog y " +
	           		" where x.serialno = y.serialno   " +
	           		"  and x.paycode = a.getnoticeno) " +
	           		"  and not exists (select 1    from lysendtobank   where paycode = a.getnoticeno) " +
	           		" and exists   (select 1 from ljtempfee where tempfeeno = a.getnoticeno))" +
	           		" then '成功' else '未成功' end)回盘结果, "
	           +"nvl((((select paymoney   from lyreturnfrombankb x, lybanklog y  where x.serialno = y.serialno    and x.paycode = a.getnoticeno    and not exists  (select 1 from lysendtobank where paycode = x.paycode)    and exists  (select 1 from ljtempfee where tempfeeno = x.paycode) )  order by x.serialno desc fetch first 1 rows only) " +
	           		"union   " +
	           		" (   select paymoney   from lyreturnfrombank x, lybanklog y  " +
	           		" where x.serialno = y.serialno    and x.paycode = a.getnoticeno  " +
	           		"  and not exists  (select 1 from lysendtobank where paycode = x.paycode)  " +
	           		"  and exists  (select 1 from ljtempfee where tempfeeno = x.paycode)  " +
	           		"order by x.serialno desc fetch first 1 rows only)), 0) 转账成功金额, "
//	           +"(select  bankunsuccreason  from lyreturnfrombankb x,lybanklog y  where  x.serialno=y.serialno and x.paycode=a.getnoticeno and not exists (select 1 from lysendtobank  where paycode=x.paycode) and  not exists (select 1 from ljtempfee where tempfeeno=x.paycode  ) order by x.serialno desc fetch first 1 rows only  ) 不成功原因, "
	           +"(select db2inst1.BANK_REASON(a.getnoticeno) from dual) 不成功原因, "		
	           +"(select z.name from lccont x,laagent y,labranchgroup z where x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.otherno union select z.name from lbcont x,laagent y,labranchgroup z where x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.otherno fetch first 1 row only) 营销机构, "
	           +"(select agentcode from lccont where contno=a.otherno union select agentcode from lbcont where contno=a.otherno fetch first 1 row only) 业务员代码, "
	           +"(select y.name from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.otherno union select y.name from lbcont x,laagent y where x.agentcode=y.agentcode and  x.contno = a.otherno fetch first 1 row only) 业务员姓名, "
	           +"(select startpaydate from ljspayb where getnoticeno=a.getnoticeno) 允许发盘开始时间, "
	           +"(select paydate from ljspayb where getnoticeno=a.getnoticeno) 允许发盘截止时间, "
	           +"(select xx.serialno from (select x.serialno serialno,x.paycode paycode from lysendtobank x where 1=1 union select x.serialno serialno,x.paycode paycode from lyreturnfrombankb x where 1=1) as xx where xx.paycode=a.getnoticeno order by xx.serialno desc fetch first 1 rows only) 发盘批次号, "
	           +"(select bankaccno from ljspayb where getnoticeno=a.getnoticeno) 银行账号,"
	           +"( case when exists (select 1 from lccont where contno=a.otherno and salechnl in ('04','13')) then '银保' else '个险' end  )        销售渠道,"
	           +" a.getnoticeno 应收号 "
	           +" from  "
	           +"(select      max(b.getnoticeno) getnoticeno, b.otherno otherno   "
	           +" from ljspayb b "
	           +" where  1=1 "
	           +" and b.accname is not null "
	           +" and b.bankaccno is not null "
	           +" and b.bankcode is not null "
	           +" and b.sumduepaymoney!=0"
	           +" and b.othernotype='2' "
	           +" and not exists (select 1 from ljspayb where otherno=b.otherno   and  getnoticeno>b.getnoticeno)"
	           +" and b.bankcode not like '16%' "
	           + sqlFeiWherePar
	           +" group by otherno "
	           +"  ) as a "
	           +" left join (select max(serialno) serialno , paycode from lysendtobank  t	 where 1=1 and not exists (select 1 from lyreturnfrombankb where paycode=t.paycode and serialno>t.serialno ) group by paycode "
	           +" union  select max(serialno) serialno , paycode from lyreturnfrombankb t where 1=1 and not exists (select 1 from lysendtobank where paycode=t.paycode and serialno>t.serialno ) group by paycode " +
//	           		" union select max(serialno) serialno, paycode" +
//	           		" from lyreturnfrombank t  where 1 = 1" +
//	           		" and not exists (select 1 from lysendtobank" +
//	           		" where paycode = t.paycode and serialno > t.serialno)" +
//	           		"  group by paycode" +
	           	") as  C "
	           +" on a.getnoticeno = C.paycode "
	           +"where 1=1 "
	           + sqlFeiYouChuOnHou
               ;
               
               
               
    
    //邮储的查询SQL
    var sqlYouWherePar="";
    var sqlYouChuOnHou="";
    var sqlYouChuWherePar="";
    if(BankState!=null&&BankState!=""&&BankState!="null")
    {
       if(BankState=="0")//全部
       {
           sqlYouChuOnHou+=""; 
       }
       if(BankState=="1")//已抽档未抽盘
       {
           sqlYouChuOnHou+=" and  c.serialno is null and exists (select 1 from ljspay where getnoticeno=a.getnoticeno)"; 
       }
       if(BankState=="2")//已抽盘未发盘
       {
           sqlYouChuOnHou+=" and  exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is null  ) ";  
       }
       if(BankState=="3")//已发盘未回盘
       {
           sqlYouChuOnHou+=" and exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is not null and y.infile  is null) "; 
       }
       if(BankState=="4")//已回盘未处理
       {
           sqlYouChuOnHou+=" and  1=2 "; 
       }
       if(BankState=="5")//已回盘处理
       {
           sqlYouChuOnHou+=" and exists (select 1 from lyreturnfrombank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno  and not exists (select 1 from lysendtobank where paycode=a.getnoticeno ))"; 
       }  
    }
    if(BankSuccFlag!=null&&BankSuccFlag!=""&&BankSuccFlag!="null")
    {
        if(BankSuccFlag=="0")//全部
        {
            
        }
        if(BankSuccFlag=="1")//成功
        {
            sqlYouChuWherePar+=" and  (exists (select 1 from lyreturnfrombank x,lybanklog y where x.serialno=y.serialno and  x.paycode=b.getnoticeno and not exists (select 1 from lysendtobank where paycode=b.getnoticeno) and  exists (select 1 from ljtempfee where tempfeeno=b.getnoticeno ) )";
            sqlYouChuWherePar+=" or  exists (select 1 from lyreturnfrombankb x,lybanklog y where x.serialno=y.serialno and  x.paycode=b.getnoticeno and not exists (select 1 from lysendtobank where paycode=b.getnoticeno) and  exists (select 1 from ljtempfee where tempfeeno=b.getnoticeno ) ))";
        }
        if(BankSuccFlag=="2")//未成功
        {
            sqlYouChuWherePar+=" and not exists (select 1 from ljtempfee where tempfeeno=b.getnoticeno) ";
        }
    }
    sqlYouWherePar=sqlWherePar+sqlYouChuWherePar;
	var youChuSQL ="select int(ROW_NUMBER() OVER()) 序号列, "
	           +"(select name from ldcom where comcode =(select managecom from ljspayb where getnoticeno=a.getnoticeno)) 管理机构,  "
	           +"(select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12) fetch first 1 row only) from lccont x,laagent y,labranchgroup z where x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.otherno  union select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12) fetch first 1 row only) from lbcont x,laagent y,labranchgroup z where  x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.otherno fetch first 1 row only) 营销部门, "
	           +"a.otherno 保单号, "
	           +"(select lastpaytodate from ljspaypersonb where getnoticeno=a.getnoticeno fetch first 1 row only) 应缴日期, "
	           +"(select accname from ljspayb where getnoticeno=a.getnoticeno) 账户名, "
	           +"(select bankname from ldbank where bankcode = (select BankCode from ljspayb where getnoticeno=a.getnoticeno)) 银行名称, "
	           +"(select xx.tt from (select y.startdate tt, x.serialno serialno ,x.paycode  paycode from lysendtobank x, lybanklog y where x.serialno = y.serialno  union select y.startdate tt, x.serialno serialno , x.paycode  paycode  from lyreturnfrombank x, lybanklog y  where x.serialno = y.serialno union select y.startdate tt, x.serialno serialno , x.paycode  paycode  from lyreturnfrombankb x, lybanklog y  where x.serialno = y.serialno  ) as xx where  xx.paycode = a.getnoticeno  order by serialno desc fetch first 1 rows only) 抽盘日期, "
	           +"(select xx.tt from (select y.senddate tt,  x.serialno serialno ,x.paycode  paycode from lysendtobank x, lybanklog y where x.serialno = y.serialno  union select y.senddate tt,  x.serialno serialno ,x.paycode  paycode  from lyreturnfrombank x, lybanklog y  where x.serialno = y.serialno union select y.senddate tt,  x.serialno serialno ,x.paycode  paycode  from lyreturnfrombankb x, lybanklog y  where x.serialno = y.serialno  ) as xx where  xx.paycode = a.getnoticeno  order by  serialno desc fetch first 1 rows only) 发盘日期, "
	           +"(select sumduepaymoney from ljspayb where getnoticeno=a.getnoticeno ) 发盘金额 , "
	           +"   (case when c.serialno is null and exists (select 1 from ljspay where getnoticeno=a.getnoticeno) then '已抽档未抽盘'"
	           +"         when exists (select 1 from ljspayb where getnoticeno = a.getnoticeno and dealstate in ('2','3') )  then '应收已作废'"
	           +"         when exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is null  )  then '已抽盘未发盘'"
	           +"         when exists (select 1 from lysendtobank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno and y.outfile is not null and y.infile  is null )  then '已发盘未回盘'"
	           +"         when exists (select 1 from lyreturnfrombank x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno  and not exists (select 1 from lysendtobank where paycode=a.getnoticeno )) then '已回盘处理'"
	           +"         when exists (select 1 from lyreturnfrombankb x,lybanklog y where x.serialno=y.serialno and x.serialno=c.serialno and  x.paycode=a.getnoticeno  and not exists (select 1 from lysendtobank where paycode=a.getnoticeno )) then '已回盘处理'"
	           +"         when exists (select 1 from ljtempfeeclass where tempfeeno = a.getnoticeno and paymode!='4' )  then '非银行转账收费'"
	           +"         else '已回盘处理'"
	           +"     end )"
	           +" 状态, "
	           +"(select returndate from lybanklog where  serialno=c.serialno fetch first 1 rows only )  回盘时间,"
	           +"(select modifydate from lyreturnfrombankb where serialno=c.serialno union select modifydate from lyreturnfrombank where serialno=c.serialno fetch first 1 rows only ) 回盘处理时间,"
	           +"(case when ((exists " +
          		"(select 1 " +
          		"  from lyreturnfrombankb x, lybanklog y  " +
          		" where x.serialno = y.serialno  " +
          		" and x.paycode = a.getnoticeno   and not exists (select 1  " +
          		"  from lysendtobank  where paycode = a.getnoticeno)   and exists  " +
          		"(select 1 from ljtempfee where tempfeeno = a.getnoticeno)  " +
          		"  ))or exists (select 1     from lyreturnfrombank x, lybanklog y " +
          		" where x.serialno = y.serialno   " +
          		"  and x.paycode = a.getnoticeno) " +
          		"  and not exists (select 1    from lysendtobank   where paycode = a.getnoticeno) " +
          		" and exists   (select 1 from ljtempfee where tempfeeno = a.getnoticeno))" +
          		" then '成功' else '未成功' end)回盘结果," 
	           //+"nvl((select  paymoney from lyreturnfrombank x,lybanklog y  where  x.serialno=y.serialno and x.paycode=a.getnoticeno and not exists (select 1 from lysendtobank  where paycode=x.paycode) and  exists (select 1 from ljtempfee where tempfeeno=x.paycode  ) order by x.serialno desc fetch first 1 rows only   ), 0) 转账成功金额, "
	           +"  nvl((select sum(paymoney) from ljtempfee where tempfeeno=a.getnoticeno ),0) 转账成功金额,"
//	           +"'' 不成功原因,"
	           +"(select db2inst1.BANK_REASON(a.getnoticeno) from dual) 不成功原因, "
	           +"(select z.name from lccont x,laagent y,labranchgroup z where x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.otherno union select z.name from lbcont x,laagent y,labranchgroup z where x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.otherno fetch first 1 row only) 营销机构, "
	           +"(select agentcode from lccont where contno=a.otherno union select agentcode from lbcont where contno=a.otherno fetch first 1 row only) 业务员代码, "
	           +"(select y.name from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.otherno union select y.name from lbcont x,laagent y where x.agentcode=y.agentcode and  x.contno = a.otherno fetch first 1 row only) 业务员姓名, "
	           +"(select startpaydate from ljspayb where getnoticeno=a.getnoticeno) 允许发盘开始时间, "
	           +"(select paydate from ljspayb where getnoticeno=a.getnoticeno) 允许发盘截止时间, "
	           +"(select max(xx.serialno) from (select x.serialno serialno,x.paycode paycode from lysendtobank x where 1=1 union select x.serialno serialno,x.paycode paycode from lyreturnfrombank x where 1=1 union select x.serialno serialno,x.paycode paycode from lyreturnfrombankb x where 1=1) as xx where xx.paycode=a.getnoticeno group by xx.paycode ) 发盘批次号, "
	           +"(select bankaccno from ljspayb where getnoticeno=a.getnoticeno) 银行账号,"
	           +"( case when exists (select 1 from lccont where contno=a.otherno and salechnl in ('04','13')) then '银保' else '个险' end  )        销售渠道,"
	           +" a.getnoticeno 应收号 "
	           +" from  "
	           +"(select      max(b.getnoticeno) getnoticeno, b.otherno otherno   "
	           +" from ljspayb b "
	           +" where  1=1 "
	           +" and b.accname is not null "
	           +" and b.bankaccno is not null "
	           +" and b.bankcode is not null "
	           +" and b.sumduepaymoney!=0"
	           +" and exists (select 1 from lccont where contno=b.otherno and paymode='4') "
	           +" and b.othernotype='2' "
	           +" and not exists (select 1 from ljspayb where otherno=b.otherno  and  getnoticeno>b.getnoticeno)"
	           +" and b.StartPayDate>'2010-11-3' "
	           +" and b.bankcode like '16%' "
	           + sqlYouWherePar
	           +" group by otherno "
	           +"  ) as a "
	           +" left join ( select max(ccc.serialno) serialno,ccc.paycode  paycode from "
	           +" (select max(serialno) serialno , paycode from lysendtobank 	 where 1=1 group by paycode "
	           +" union 	select max(serialno) serialno , paycode from lyreturnfrombankb  where 1=1 group by paycode "
	           +" union 	select max(serialno) serialno , paycode from lyreturnfrombank  where 1=1 group by paycode) as ccc group by ccc.paycode ) as  C "
	           +" on a.getnoticeno = C.paycode "
	           +"where 1=1 "
	           + sqlYouChuOnHou
               ;
	          
	var strSQL=feiYouChuSQL+" union all "  + youChuSQL  + " with ur " ;       
	mSql=strSQL;
	
	turnPage1.queryModal(strSQL, CodeGrid);   
	if(CodeGrid.mulLineCount == 0)
	{
	    alert("没有符合条件的报盘数据");
	}
	//showCodeName(); 
}

function easyPrint()
{	
	if( CodeGrid.mulLineCount == 0)
	{
		alert("没有回盘信息");
		return false;
	}
	fm.all('strsql').value=mSql;
	//清单信息打印打印
	fm.submit();
}
