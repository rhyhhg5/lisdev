<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="SendCustomerNoticeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="SendCustomerNoticeInputInit.jsp"%>
  <title>打印客户通知书 </title>
</head>
<body  onload="initForm();" >
  <form action="./FirstPayQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入投保单查询条件：</td>
		</tr>
	</table>
    <!--table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>印刷号   </TD>
          <TD  class= input><Input class= common name=prtno > </TD>
          <TD  class= title>管理机构  </TD>
          <TD  class= input>  <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">  </TD>   
         </TR> 
         <TR  class= common>
         <TD  class= title> 代理人编码 </TD>
          <TD  class= input>  <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this]);" onkeyup="return showCodeListKey('AgentCode',[this]);">   </TD> 
         <TD  class= title> 代理人组别 </TD>
          <TD  class= input>  <Input class="code" name=AgentGroup ondblclick="return showCodeList('AgentGroup',[this]);" onkeyup="return showCodeListKey('AgentGroup',[this]);">   </TD>
          <TD  class= title> 展业机构 </TD>
          <td class="input" nowrap="true">
            <Input class="common" name="BranchGroup">
			<input name="btnQueryBranch" class="common" type="button" value="?" onclick="queryBranch()" style="width:20">
          </TD>  
        </TR> 
    </table-->
    <table class=common>
    	<tr class=common>
    		<TD  class= title>印刷号</TD>
          <TD  class= input><Input class= common name=PrtNo verify="印刷号码|int&len=11"> </TD>
        <TD  class= title>投保人</TD>
          <TD  class= input><Input class= common name=AppntName verify="投保人|len<=20"> </TD>
        <TD  class= title>被保险人</TD>
          <TD  class= input><Input class= common name=InsuredName verify="被保险人|len<=20"> </TD>
    		</tr>
    	<tr class=common>
    		<TD  class= title>申请时间</TD>
          <TD  class= input><Input class= coolDatePicker name=ApplyDate verify="申请时间|date"> </TD>
        <TD  class= title>生效时间</TD>
          <TD  class= input><Input class= coolDatePicker name=CValiDate verify="生效日期|date"> </TD>
        <TD  class= title>管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   
    		</tr>
    </table>
          <INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();"> 
  </form>
  <form action="./GrpFirstPayF1PSave.jsp" method=post name=fmSave target="f1print">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 投保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
	    	<p align="center">
	    		<DIV id="div1" style="display: ''">
			      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
			      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
			      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
			      <INPUT VALUE="尾  页" class= CssButton  TYPE=button onclick="getLastPage();"> 	
			  	</div>
	  		</p>
    	</div>
    
    	<Div  id= "divLCPol2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPrintTypeGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    <p>
      <INPUT VALUE="打 印" class= CssButton TYPE=button onclick="printPol();">
  	</p>
  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="PolNo" name="PolNo">
  	<input type=hidden id="PrtSeq" name="PrtSeq">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>