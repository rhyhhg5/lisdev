<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
    boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	XmlExport txmlExport = null;   
	GlobalInput tG = (GlobalInput)session.getValue("GI");

	String tStartDate = request.getParameter("StartDate"); //起始日
	String tEndDate = request.getParameter("EndDate");  //截止日
	String tDealState = request.getParameter("DealState");//催收状态

    System.out.println(tStartDate);
	System.out.println(tEndDate);
    System.out.println(tDealState);

    TransferData tTransferData= new TransferData();
	tTransferData.setNameAndValue("StartDate",tStartDate);
	tTransferData.setNameAndValue("EndDate",tEndDate);
	tTransferData.setNameAndValue("DealState",tDealState);

	VData tVData = new VData();
    tVData.addElement(tG);
	tVData.addElement(tTransferData);
          
    GrpPremDueFeeListPrintUI tGrpPremDueFeeListPrintUI = new GrpPremDueFeeListPrintUI(); 
    if(!tGrpPremDueFeeListPrintUI.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tGrpPremDueFeeListPrintUI.mErrors.getErrContent();                
    }
    else
    {    
		VData mResult = tGrpPremDueFeeListPrintUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	//System.out.println(operFlag);
	//if (operFlag==true)
	//{
	//	session.putValue("PrintStream", txmlExport.getInputStream());
	//	response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
	//}
	
  LDSysVarDB tLDSysVarDB = new LDSysVarDB();
  tLDSysVarDB.setSysVar("VTSFilePath");
  tLDSysVarDB.getInfo();
  String vtsPath = tLDSysVarDB.getSysVarValue();
  if (vtsPath == null)
  {
    vtsPath = "vtsfile/";
  }
  
  String filePath = application.getRealPath("/").replace('\\', '/') + "/" + vtsPath;
  String fileName = "TASK" + FileQueue.getFileName()+".vts";
  String realPath = filePath + fileName;
	
	if (operFlag==true)
	{
    //合并VTS文件 
    String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);
    
    //把dataStream存储到磁盘文件
    AccessVtsFile.saveToFile(dataStream, realPath);
    response.sendRedirect("GetF1PrintJ1_new.jsp?RealPath=" + realPath);
	}else
	{
    	FlagStr = "Fail";

%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();	
</script>
</html>
<%
  	}
%>