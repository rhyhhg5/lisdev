//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	 
   if (verifyInput() == false)
    return false;
      	 
    if( !beforeSubmit())
   {
   	return false;
   	
   	}	

  var i =0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function beforeSubmit()
{
 
	return true;
	
}


function download()
{
	if (verifyInput() == false)
    return false;
  var i =0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function easyQueryClick() 
{

 　if (verifyInput() == false)
     return false;
  //此处书写SQL语句			     
  var tReturn = parseManageComLimitlike();
  	 
  var tManageCom=fm.all('ManageCom').value;
  var tAgentCode=fm.all('AgentCode').value;
   
  var tIndexCalNo=fm.all('AssessYear').value+fm.all('AssessMonth').value;
  
   
  var tSQL = "";

  //  tSQL  = "select a,b,c,d,e,f,g,h,ee,i" +
//    		"case  when value(i,0) <0 then 0 else value(i,0) end,value(k,0),value(l,0),"
//       +" case when value(m,0) <0 then 0 else value(m,0) end,value(n,0),value(p,0), case when value(q,0) <0 then 0 else value(q,0) end,"
//       +" value(s,0),r,case when value(t,0)<0 then 0 else value(t,0) end " +
    tSQL  =  " select getUniteCode(agentcode) a,"
       +"(select name from laagent b where a.agentcode=b.agentcode) b,"
       +"branchattr c,"
       +"(select name from labranchgroup b where a.agentgroup=b.agentgroup) d,"
       +"agentgrade e,"            
       +"(select b.employdate from laagent b where a.agentcode=b.agentcode) f,"
             +"(select b.startdate from latree b where a.agentcode=b.agentcode) g,"
             +"value(DirMngCount+DRDepMonLabor,0) h,"
             +"20 ee,"
             +"case when 20-value(DirMngCount+DRDepMonLabor,0)<0 then 0 else 20-value(DirMngCount+DRDepMonLabor,0) end  i,"
             +"value(TeamCount,0) k,"    
             +"4 l,"           
             +"case when 4-value(TeamCount,0)<0 then 0 else 4-value(TeamCount,0) end  m,"
             +"decimal((DepFYCSum+DirDepMonAvgFYC+T50),12,2) n,"            
             +"decimal(T65/3*T33,12,2) p,"            
             +"case when decimal(T65/3*T33,12,2)-decimal((DepFYCSum+DirDepMonAvgFYC+T50),12,2) <0 then 0 else decimal(T65/3*T33,12,2)-decimal((DepFYCSum+DirDepMonAvgFYC+T50),12,2) end q,"
             +"value(T53*100,0) s," 
             +"75 r,"
             +"case when 75-value(T53*100,0)<0 then 0 else 75-value(T53*100,0) end  t"
            
             +" from laindexinfo a where indextype='02' and IndexCalno='"+tIndexCalNo+"'"    ;
       
       tSQL += "  and AgentGrade>='B11' and AgentGrade<'B21' and branchtype='1' and  branchtype2='01' " 
       + tReturn
      + getWherePart('ManageCom','ManageCom','like')
//      alert(tSQL);
//      + getWherePart('AgentCode','AgentCode' )
//    modify lyc 2014-11-27 统一工号
      if(fm.all("AgentCode").value!=""){
   	   tSQL +=" and AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
      }       
//       tSQL += " order by ManageCom,BranchAttr,AgentCode ) as x ";
//	 alert(tSQL);
   turnPage.queryModal(tSQL, AgentQueryGrid,1);
  if (!turnPage.strQueryResult) 
  {
    alert("没有符合条件的查询信息！");
    return false;
  }
}

function afterCodeSelect( cCodeName, Field )
{
　	  
  if(cCodeName=="comcode"){
  	 fm.all("BranchAttr").value="";
  	 fm.all("BranchName").value="";
  	if(fm.all("ManageCom").value=='86' )
  	{
  		fm.all("BranchAttr").disabled=true;
  	}
       else
       	{
       		var tManageCom=fm.all("ManageCom").value;
       		fm.all("BranchAttr").disabled=false;
       		msql1=" 1 and   branchtype=#2#  and branchtype2=#01#  and managecom like #"+tManageCom+"%# and endflag=#N#";
       	}
}
 if(cCodeName=="branchattr")
 {
   if(fm.all("ManageCom").value==null || fm.all("ManageCom").value=='')
    {
      alert("请先录入管理机构");
      fm.all("BranchAttr").value='';
       fm.all("BranchName").value='';
       return false;
  
     }
}
}        
                                                                                                                   