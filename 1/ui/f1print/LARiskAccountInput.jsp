<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LAClientNumInput.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<script>
	msql = " 1 and exists (select 1 from lrcontinfo a,lrcalfactorvalue b where a.recomcode = #002# and a.recontcode = b.recontcode and a.DiskKind = #1# and b.riskcode = LMRisk.RiskCode) ";
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LARiskAccountInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initForm();initElementtype();">    
  <form action="./LARiskAccountReport.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>险种编码</TD>
          <TD  class= input> <Input class="code" name=RiskCode verify = "险种编码|notnull"
          ondblclick = "return showCodeList('RiskCode',[this,RiskName],[0,1],null,msql,1);"
          onkeyup = "return showCodeListKey('RiskCode',[this,RiskName],[0,1],null,msql,1);"> </TD>
          <TD class= title>
          险种名称
          </TD>
          <TD class= input>
          <Input class="readOnly" name=RiskName  readOnly elementtype=nacessary></TD>         
          </tr>
          <TR class=common> 
          <TD  class= title> 统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计日期|notnull&Date" elementtype=nacessary> </TD> 
             <TD  class= title>统计止期</TD>
          <td class=input><input name=EndDate class='coolDatePicker' verify="统计日期|notnull&Date" elementtype=nacessary>
        </TR>
    </table>

    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 