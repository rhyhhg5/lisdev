//
//程序名称：BankDataQuery.js
//程序功能：财务报盘数据查询
//创建日期：2004-10-20
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容

//var arrDataSet;
var turnPage = new turnPageClass();

//简单查询
function easyQuery()
{
	var sDate = fm.all('SendDate').value;
	var sEndDate = fm.all('SendEndDate').value;
	var sBankSucc = fm.all('BankSuccFlag').value;
	var strSqlCon = "";
	
	if (sDate == null || sDate == "")
	{
			alert("请输入发盘起期！");
			return;
	}
	if (sEndDate == null || sEndDate == "")
	{
			alert("请输入发盘止期！");
			return;
	}
	//by gzh 为避免查询数据集过大，限制只查询3个月内的数据。
	if(dateDiff(sDate,sEndDate,"M")>3)
	{
		alert("查询时间最多为三个月！");
		return false;
	}
	strSqlCon = " and notype='9' and Dealtype='S'"
	// 书写SQL语句
	if (sBankSucc=='1')
	{
		var strSQL = " select polno,AccName,lyrb.comcode,lyrb.BankCode,lyrb.SendDate,PayMoney,'成功','', "
                + " (select BranchAttr from LABranchGroup where AgentGroup = laa.AgentGroup), "
                + " laa.groupAgentCode "
				+ " from LYReturnFromBankB lyrb "
				+ " inner join LAAgent laa on laa.AgentCode = lyrb.AgentCode "
				+ " inner join Lybanklog lyl on Lyrb.serialno = lyl.serialno "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('lyrb.comcode','ManageCom','=','0')
				+ getWherePart('lyrb.bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and BankSuccFlag = (select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode ) "
				+ " and lyl.startdate >='" + sDate + "'"
				+ " and lyl.startdate <='" + sEndDate + "'"	
				+ strSqlCon			
				+ " union all "
				+ " select polno,AccName,lyrb.comcode,lyrb.BankCode,lyrb.SendDate,PayMoney,'成功','', "
                + " (select BranchAttr from LABranchGroup where AgentGroup = laa.AgentGroup), "
                + " laa.groupAgentCode "
				+ " from LYReturnFromBank lyrb "
				+ " inner join LAAgent laa on laa.AgentCode = lyrb.AgentCode "
				+ " inner join Lybanklog lyl on Lyrb.serialno = lyl.serialno "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('lyrb.comcode','ManageCom','=','0')
				+ getWherePart('lyrb.bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and BankSuccFlag = (select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode ) "
				+ " and Not Exists (Select 1 From Lysendtobank Where Paycode = lyrb.Paycode And Serialno = lyrb.Serialno)"
				+ " and lyl.startdate >='" + sDate + "'"
				+ " and lyl.startdate <='" + sEndDate + "'"	
				+ strSqlCon			
				+ " order by polno";
	}else if (sBankSucc=='')
	{
		var strSQL = " select polno,AccName,lyrb.comcode,lyrb.BankCode,lyrb.SendDate,PayMoney,'成功','', "
                + " (select BranchAttr from LABranchGroup where AgentGroup = laa.AgentGroup), "
                + " laa.groupAgentCode "
				+ " from LYReturnFromBankB lyrb "
				+ " inner join LAAgent laa on laa.AgentCode = lyrb.AgentCode "
				+ " inner join Lybanklog lyl on Lyrb.serialno = lyl.serialno "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('lyrb.comcode','ManageCom','=','0')
				+ getWherePart('lyrb.bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and BankSuccFlag =(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode ) "
				+ " and lyl.startdate >='" + sDate + "'"
				+ " and lyl.startdate <='" + sEndDate + "'"
				+ strSqlCon	
				+ " union all "
				+ " select polno,AccName,lyrb.comcode,lyrb.BankCode,lyrb.SendDate,PayMoney,'成功','', "
                + " (select BranchAttr from LABranchGroup where AgentGroup = laa.AgentGroup), "
                + " laa.groupAgentCode "
				+ " from LYReturnFromBank lyrb "
				+ " inner join LAAgent laa on laa.AgentCode = lyrb.AgentCode "
				+ " inner join Lybanklog lyl on Lyrb.serialno = lyl.serialno "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('lyrb.comcode','ManageCom','=','0')
				+ getWherePart('lyrb.bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and BankSuccFlag =(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode ) "
				+ " and Not Exists (Select 1 From Lysendtobank Where Paycode = lyrb.Paycode And Serialno = lyrb.Serialno)"
				+ " and lyl.startdate >='" + sDate + "'"
				+ " and lyl.startdate <='" + sEndDate + "'"
				+ strSqlCon	
				+ " union all "
				+  " select polno,AccName,lyrb.comcode,lyrb.BankCode,lyrb.SendDate,PayMoney,'未成功',"
				+ " (select CodeName from ldcode1 where codetype='bankerror' and code=lyl.BankCode and code1=lyrb.BankSuccFlag), "
				+ " (select BranchAttr from LABranchGroup where AgentGroup = laa.AgentGroup), "
                + " laa.groupAgentCode "
				+ " from LYReturnFromBankB lyrb "
				+ " inner join LAAgent laa on laa.AgentCode = lyrb.AgentCode "
				+ " inner join Lybanklog lyl on Lyrb.serialno = lyl.serialno "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('lyrb.comcode','ManageCom','=','0')
				+ getWherePart('lyrb.bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and (BankSuccFlag <>(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode )  or BankSuccFlag is null )"
				+ " and lyl.startdate >='" + sDate + "'"
				+ " and lyl.startdate <='" + sEndDate + "'"
				+ strSqlCon
				+ " union all "
				+ " select polno,AccName,lyrb.comcode,lyrb.BankCode,lyrb.SendDate,PayMoney,'未成功',"
				+ " (select CodeName from ldcode1 where codetype='bankerror' and code=lyl.BankCode and code1=lyrb.BankSuccFlag), "
				+ " (select BranchAttr from LABranchGroup where AgentGroup = laa.AgentGroup), "
                + " laa.groupAgentCode "
				+ " from LYReturnFromBank lyrb "
				+ " inner join LAAgent laa on laa.AgentCode = lyrb.AgentCode "
				+ " inner join Lybanklog lyl on Lyrb.serialno = lyl.serialno "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('lyrb.comcode','ManageCom','=','0')
				+ getWherePart('lyrb.bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and (BankSuccFlag <>(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode )  or BankSuccFlag is null )"
				+ " and Not Exists (Select 1 From Lysendtobank Where Paycode = lyrb.Paycode And Serialno = lyrb.Serialno)"
				+ " and lyl.startdate >='" + sDate + "'"
				+ " and lyl.startdate <='" + sEndDate + "'"
				+ strSqlCon
				;
	}else
	{
		var strSQL = " select polno,AccName,lyrb.comcode,lyrb.BankCode,lyrb.SendDate,PayMoney,'未成功', "
		        + " (select CodeName from ldcode1 where codetype='bankerror' and code=lyl.BankCode and code1=BankSuccFlag),"
		        + " (select BranchAttr from LABranchGroup where AgentGroup = laa.AgentGroup), "
                + " laa.groupAgentCode "
		        + " from LYReturnFromBankB lyrb "
		        + " inner join LAAgent laa on laa.AgentCode = lyrb.AgentCode "
		        + " inner join Lybanklog lyl on Lyrb.serialno = lyl.serialno "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('lyrb.comcode','ManageCom','=','0')
				+ getWherePart('lyrb.bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and (BankSuccFlag <>(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode )  or BankSuccFlag is null )"
				+ " and lyl.startdate >='" + sDate + "'"
				+ " and lyl.startdate <='" + sEndDate + "'"
				+ strSqlCon	
				+ " union all "
				+ " select polno,AccName,lyrb.comcode,lyrb.BankCode,lyrb.SendDate,PayMoney,'未成功', "
		        + " (select CodeName from ldcode1 where codetype='bankerror' and code=lyl.BankCode and code1=BankSuccFlag),"
		        + " (select BranchAttr from LABranchGroup where AgentGroup = laa.AgentGroup), "
                + " laa.groupAgentCode "
		        + " from LYReturnFromBank lyrb "
		        + " inner join LAAgent laa on laa.AgentCode = lyrb.AgentCode "
		        + " inner join Lybanklog lyl on Lyrb.serialno = lyl.serialno "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('lyrb.comcode','ManageCom','=','0')
				+ getWherePart('lyrb.bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and (BankSuccFlag <>(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode )  or BankSuccFlag is null )"
				+ " and Not Exists (Select 1 From Lysendtobank Where Paycode = lyrb.Paycode And Serialno = lyrb.Serialno)"
				+ " and lyl.startdate >='" + sDate + "'"
				+ " and lyl.startdate <='" + sEndDate + "'"
				+ strSqlCon	
				;
	}
	turnPage.queryModal(strSQL, CodeGrid);  
	showCodeName(); 

}

function easyPrint()
{
	easyQueryPrint(2,'CodeGrid','turnPage');
}

function download()
{
  if(CodeGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }
  
  fm.Sql.value = turnPage.strQuerySql;
  fm.action="QYBankDataQueryDownLoad.jsp";
  fm.submit();
}