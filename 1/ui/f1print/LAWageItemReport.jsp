<%
//程序名称：LAWageTaxReport.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%
	//String FileName = "GroupProduct_Income";
//String StartDate = "2005-01-01";
//String EndDate = "2005-01-11";
//String ManageCom = "86";

  String FlagStr = "";
  String Content = "";
  System.out.println("ssssss");

//得到excel文件的保存路径
  String sql = "select SysvarValue "
              + "from ldsysvar "
              + "where sysvar='SaleXmlPath'";
  ExeSQL tExeSQL = new ExeSQL();
  String subPath = tExeSQL.getOneValue(sql);
  
Calendar cal = new GregorianCalendar();
String year = String.valueOf(cal.get(Calendar.YEAR));
String month=String.valueOf(cal.get(Calendar.MONTH)+1);
String date=String.valueOf(cal.get(Calendar.DATE));
String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
String min=String.valueOf(cal.get(Calendar.MINUTE));
String sec=String.valueOf(cal.get(Calendar.SECOND));
String now = year + month + date + hour + min + sec + "_" ;

String  millis = String.valueOf(System.currentTimeMillis());  
String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
subPath+=fileName;
String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
String realPath=application.getRealPath("/")+"/"+subPath;
System.out.println("..........download jsp getrealpath here :"+application.getRealPath("vtsfile"));
//String tType = "3";
System.out.println("OutXmlPath:" + tOutXmlPath);

//接收信息，并作校验处理。
//输入参数
LAAgentSalaryTotalBL tLAAgentSalaryTotalBL   = new LAAgentSalaryTotalBL();
//输出参数
CErrors tError = null;
//String tRela  = "";                
boolean operFlag=true;
GlobalInput tG = new GlobalInput(); 
tG=(GlobalInput)session.getValue("GI");

try{
	  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	   tG=(GlobalInput)session.getValue("GI");
	   String tManageCom=tG.ManageCom;
	   String ManageCom = request.getParameter("ManageCom");
	   String sql1="select name from ldcom where comcode='"+ManageCom+"'";
	   ExeSQL tExeSQL1 = new ExeSQL();
	   String tName = tExeSQL1.getOneValue(sql1);
	   System.out.println(ManageCom+"名字："+tName);

	  //接受参数

		String tBranchType = request.getParameter("BranchType");
		String tBranchType2 = request.getParameter("BranchType2");
		String tWageYear = request.getParameter("WageYear");
		String tWageMonth = request.getParameter("WageMonth");

		
	   XmlExport txmlExport = new XmlExport();
	   VData tVData = new VData();
	   VData mResult = new VData();
	   // 准备传输数据 VData
	    tVData.add(ManageCom);
		tVData.add(tBranchType);
		tVData.add(tBranchType2);
		tVData.add(tWageYear);
		tVData.add(tWageMonth);
		tVData.add(tName);
		tVData.add(tG);
	    tVData.add(realPath);
	  	 //调用打印的类
	  	 System.out.println(".............begin submit here");
	    if (!tLAAgentSalaryTotalBL.submitData(tVData,"PRINT")){
		      FlagStr="Fail";
		      Content=tLAAgentSalaryTotalBL.mErrors.getFirstError().toString();    
	    }
		 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
		 	  //File file = new File(tOutXmlPath);
		 	  File file = new File(realPath);
		 	  System.out.println("..................report jsp here ");
		      response.reset();
	          response.setContentType("application/octet-stream"); 
	          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
	          response.setContentLength((int) file.length());
	      
	          byte[] buffer = new byte[4096];
	          BufferedOutputStream output = null;
	          BufferedInputStream input = null;    
	          //写缓冲区
	          try 
	          {
	              output = new BufferedOutputStream(response.getOutputStream());
	              input = new BufferedInputStream(new FileInputStream(file));
	        
	          int len = 0;
	          while((len = input.read(buffer)) >0)
	          {
	              output.write(buffer,0,len);
	          }
	          input.close();
	          output.close();
	          }
	          catch (Exception e) 
	          {
	            e.printStackTrace();
	           } // maybe user cancelled download
	          finally 
	          {      
	              if (input != null) input.close();
	              if (output != null) output.close();
	              //file.delete();
	          }
	  }catch(Exception ex){
	  	ex.printStackTrace();
	  }
	  if(FlagStr.equals("Fail")){
		  
		  
		  
		  %>
		  <html>
		  <script language="javascript">
		  	alert(<%=Content%>);
		  	//top.close();
		  </script>
		  </html>
		  <%}%>
 


