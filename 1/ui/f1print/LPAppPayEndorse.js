//               该文件中包含客户端需要处理的函数和事件
var arrDataSet;
var showInfo;
var mDebug="0";
//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initLCPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	 initForm();
  }
  catch(re)
  {
  	alert("在LCPolQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
  

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

// 查询按钮
function easyQueryClick()
{
	var EdorState;
	
	// 初始化表格
	initLCPolGrid();
	var tEdorAccetpNo = fm.all('EdorAcceptNo').value;
	//var tEdorNo = fm.all('EdorNo').value;
	//var tReturn = parseManageComLimit();
    //alert(tReturn);
	// 书写SQL语句
	var strSQL = "";

	  strSQL = "select ljsPay.GetNoticeNo,ljsPay.OtherNo,ljsPay.SumDuePayMoney,ljsPay.AgentCode,'' ,'' ,ljsPay.ManageCom, ljsPay.AppntNo from ljsPay,LPEdorApp where ljsPay.othernotype = '10'"//+" and"+tReturn
	       + " and  LPEdorApp.EdorAcceptNo = ljsPay.OtherNo " 
	       + getWherePart('ljsPay.OtherNo', 'EdorAcceptNo') 
	       + getWherePart('ljsPay.ManageCom', 'ManageCom', 'like')
	       + getWherePart('ljsPay.AgentCode','AgentCode')
	       + getWherePart('ljsPay.AgentGroup','AgentGroup')
	       + " AND ljsPay.ManageCom LIKE '" + comcode + "%%' "; //登陆机构权限控制
	

        
	//alert(strSQL);				 
	//查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	  
	  //判断是否查询成功
	  if (!turnPage.strQueryResult) 
	  {
	    alert("查询失败！");
	  }
	  else
	  {
	  	//查询成功则拆分字符串，返回二维数组
	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  
	  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	  turnPage.pageDisplayGrid = LCPolGrid;    
	           
	  //保存SQL语句
	  turnPage.strQuerySql     = strSQL; 
	  
	  //设置查询起始位置
	  turnPage.pageIndex       = 0;  
	  
	  //在查询结果数组中取出符合页面显示大小设置的数组
	  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	  
	  //调用MULTILINE对象显示查询结果
	  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
		}
}

function returnParent()
{
    tSel=LCPolGrid.getSelNo();
    if( tSel == 0 || tSel == null )
		top.close();
    else
    {
    	tCol=1;
   	tPolNo = LCPolGrid.getRowColData(tSel-1,tCol);
   	//alert(tPolNo);
   	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    	top.location.href="./LCPolQueryDetail.jsp?PolNo="+tPolNo;
     }
}


//提交，打印按钮对应操作
function PrintEdor()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = LCPolGrid.getSelNo();	  
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		arrReturn = getQueryResult();
		//showInfo.close();
		fmSave.GetNoticeNo.value = arrReturn[0][0];
		fmSave.OtherNo.value = arrReturn[0][1];  
		fmSave.AppntNo.value = arrReturn[0][7];
		fmSave.SumDuePayMoney.value = arrReturn[0][2];
		fmSave.Agentcode1.value = arrReturn[0][3];
		fmSave.fmtransact.value = "PRINT";
		fmSave.target = "f1print";
		fmSave.submit();
		showInfo.close();
		
		
	}
}


function getQueryResult()
{
	var arrSelected = null;
	tRow = LCPolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	return arrSelected;
	arrSelected = new Array();
	tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
	return arrSelected;
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}
