//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";
var turnPage1 = new turnPageClass(); 

//提交，保存按钮对应操作
function submitForm()
{
 if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
	showInfo.close();
}
function beforeSubmit()
{
	return true;	
}
function checkAgent()
{
  var sql = "select * from laagent where groupagentcode ='"+fm.all('AgentCode').value+"' and agentstate<='02' and branchtype='1' and branchtype2='01'";
  var strQueryResult  = easyQueryVer3(sql, 1, 1, 1);
  if(!strQueryResult)
  {
    alert("该代理人不存在！");
    fm.all('AgentCode').value ='';
    return false;
  }
}

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  showSubmitFrame(mDebug); 
  fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	//fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function easyQueryClick() 
{
   if (verifyInput() == false)
   return false;
   //时间的转换 //利用SQL查询
   //拼接日期 
  var currdate = fm.all('currDate').value ;
   var StartMonth = fm.all('StartMonth').value;
   var EndMonth = fm.all('EndMonth').value;
   var startDate = StartMonth.substring(0,4)+'-'+StartMonth.substring(4,6)+'-01';//统计起期月份的1号 
   var sql = "select date('"+startDate+"')-14 month from dual";
   var strQueryResult  = easyQueryVer3(sql, 1, 1, 1);
   var arr = decodeEasyQueryResult(strQueryResult);
   var laststartdate = arr[0][0];
   //追溯14个月后月份加1个月减1天
   var enddate = EndMonth.substring(0,4)+'-'+EndMonth.substring(4,6)+'-01';
   var tSQL = "select date('"+enddate+"')-13 month from dual";
   var strQueryResult  = easyQueryVer3(tSQL, 1, 1, 1);
   var arr = decodeEasyQueryResult(strQueryResult);
   var ttSQL = "select date('"+arr[0][0]+"')-1 day from dual";
     //获得追溯14个月后的当月最后一天
    var strQueryResult  = easyQueryVer3(ttSQL, 1, 1, 1);
    var arr = decodeEasyQueryResult(strQueryResult);
    var lastenddate = arr[0][0];
    //当前时间的最后止期
    var currenlastdate = "";
    var curr = currdate.substring(0,7).replace('-','');
    if(parseInt(EndMonth)>=parseInt(curr))
    {
     currenlastdate = currdate;
    }
    else
    {
     var currendateSQL = "select date('"+enddate+"')+1 month from dual";
     var strQueryResult  = easyQueryVer3(currendateSQL, 1, 1, 1);
     var arr1 = decodeEasyQueryResult(strQueryResult);
     var tttSQL = "select date('"+arr1[0][0]+"')-1 day from dual";
     var strQueryResult  = easyQueryVer3(tttSQL, 1, 1, 1);
     var arr2 = decodeEasyQueryResult(strQueryResult);
     currenlastdate = arr2[0][0];
    }
  //查询满足条件的值
  var strSql = "select a.contno,a.p14,a.p11,"
  + "(select e.postaladdress from lcaddress e where e.customerno = b.appntno and e.addressno=(select addressno from lcappnt where b.contno=lcappnt.contno)),"
  + " (select e.mobile from lcaddress e where e.customerno = b.appntno and e.addressno=(select addressno from lcappnt where b.contno=lcappnt.contno)),"
  + " a.riskcode,a.transmoney,b.agentcode,"
  + "(select name from laagent where laagent.agentcode=a.agentcode),"
  +" (select agentgrade from latree where latree.agentcode=a.agentcode),"
  + "(select branchattr from labranchgroup  where labranchgroup.agentgroup=a.branchcode) h,"
  + "(select name from labranchgroup  where labranchgroup.agentgroup=a.branchcode),"
  + "a.CValidate,case (select nvl(sum(transmoney),0) from lacommision f where f.renewcount=1 and f.polno=a.polno and f.tmakedate>='"+startDate+"' and f.tmakedate<='"+currenlastdate+"') when 0 then '否' else '是' end "  //保单在统计区间续期判断 
  + "from lacommision a,lccont b  "
  + "where a.contno=b.contno and a.branchtype='1' and a.branchtype2='01' and "
  + " a.CValidate>='"+laststartdate+"' and  a.CValidate<='"+lastenddate+"' and a.riskcode in (select riskcode from lmriskapp where riskperiod='M')"
  + getWherePart('a.ManageCom','ManageCom','like') 
  + getWherePart('a.BranchAttr','BranchAttr','like')
  + getWherePart('a.RiskCode','RiskCode')
  + getWherePart('b.AgentCode','AgentCode')
  + "order by h,b.agentcode";
  turnPage.queryModal(strSql, LARenewCountGrid);
  if (!turnPage.strQueryResult) 
  {
    alert("没有符合条件的查询信息！");
    return false;
  }
  
}
    
                                                                                                                   