//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;
var SqlPDF = "";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;  
  var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	else
	{
		PrtSeq = PolGrid.getRowColData(tChked[0],1);
		PrtNo = PolGrid.getRowColData(tChked[0],2)
		
		fmSave.PrtSeq.value = PrtSeq;
		fmSave.PrtNo.value = PrtNo;
		fmSave.fmtransact.value = "PRINT";
		fmSave.action = "FirstPayF1PSave.jsp?LoadFlag="+LoadFlag;
		fmSave.submit();
		
	}
}
function printPolpdf()	
{
  var i = 0;  
  var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	else
	{
		//arrReturn = getQueryResult();
		PrtSeq = PolGrid.getRowColData(tChked[0],1)
		PrtNo = PolGrid.getRowColData(tChked[0],2)		
		fmSave.PrtSeq.value = PrtSeq;
		fmSave.PrtNo.value = PrtNo;
		//fmSave.PolNo.value = arrReturn[0][1];
		fmSave.fmtransact.value = "PRINT";
		fmSave.action = "../uw/PrintPDFSave.jsp?Code=007&RePrintFlag="+RePrintFlag;
		fmSave.submit();
		
		
	}
}
//zhoushujing于2007.06.14增加下面的函数
function printPolpdfbat()
{
	if (PolGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}


	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	if(tChked.length == 0)
	{
	  if(!confirm("由于您没有选择要打印的记录，将打印前150条现金缴费的数据。确认吗？"))
	  {
	    return false;
	  }
	  //sqlpdf用于存放没有选择时的批打脚本   2008-2-15
	  fmSave.strsql.value = SqlPDF;
		fmSave.action="./FirstPayAllBatPrt.jsp";
		fmSave.submit();
	}
	else
	{
		fmSave.action="./FirstPayForBatPrt.jsp";
		fmSave.submit();
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
		if(!verifyInput2())
	return false;
	
	var tSeltype="";
	if (fm.all('SelType').value=="0")
			{
				tSeltype=" and j<i ";
			}
	else if(fm.all('SelType').value=="1")
			{
				tSeltype=" and j>=i ";
			}
	else
			{
				tSeltype="";
			}
	var tStateFlag="";
	if(fm.all('StateFlag').value=="null"||fm.all('StateFlag').value==null||fm.all('StateFlag').value=="")
 	 		{
    			tStateFlag="";
 	 		}
  else{
			if (fm.all('StateFlag').value=="0")
	  			{
		  				tStateFlag=" and LOPRTManager.stateflag='0' ";
	  			}
	  	else if (fm.all('StateFlag').value=="9")
	  			{
		 	 				tStateFlag="";
	  			}
	  	else
	  			{
	  			tStateFlag=" and LOPRTManager.stateflag<>'0' ";
	  			}
			}
	 var tCardFlag="";
		if(Flag == "card")
		  {
		      tCardFlag += " and LCCont.cardflag in ('1','2','5','6') ";
	    }
		var tLoadFlag=" (case LOPRTManager.stateflag when '0' then '未打印' else '已打印' end) k ";
		if(LoadFlag=="Back")
			{
					tLoadFlag=" '' k ";
			}
	// 书写SQL语句
    var tStrCondUWFlag = "";
    if(tIsBackFee == "1")
    {
        tStrCondUWFlag = " and LCCont.uwflag in ('1','8','a') "
            + " and lcpol.uwflag in ('1','8','a') "
            + " and LCCont.PayLocation = '0' ";
    }
    else
    {
        tStrCondUWFlag = " and LCCont.uwflag not in ('1','8','a') "
            + " and lcpol.uwflag not in ('1','8','a') ";
    }
    
	var strSQL = "";
	var cstrSQL="";
	cstrSQL = "SELECT LOPRTManager.PrtSeq a,LCCont.PrtNo b,LCCont.PolApplyDate c,LCCont.CValiDate d,"
	+ "LCCont.AppntName e,(select Groupagentcode from LAAgent aa where aa.AgentCode = LOPRTManager.AgentCode) f,LOPRTManager.ManageCom g,LOPRTManager.makedate h,"
	+ "sum(lcpol.prem) i,CodeName('paymode',LCCont.PayMode) n,"
	+ "(select substr(branchattr, 1, 10) from LABranchGroup bg where bg.AgentGroup = LCCont.AgentGroup) m, "
	+ "(select Name from LABranchGroup bg where BranchLevel = '03' and bg.BranchAttr = (select substr(branchattr, 1, 10) from LABranchGroup bg where bg.AgentGroup = LCCont.AgentGroup)) o, "
	+ "(select Name from LAAgent aa where aa.AgentCode = LOPRTManager.AgentCode) p "
	//+ " sum(lcpol.prem) j, "
	//+ tLoadFlag
	+ "FROM LOPRTManager,LAAgent,LCCont,lcpol WHERE LCCont.Conttype='1' and LCPol.ContType='1' and LOPRTManager.Code = '07' " 
	+ "and LAAgent.AgentCode = LOPRTManager.AgentCode "
	+ "and LOPRTManager.OtherNo = LCCont.proposalContNo "	
	+ "and LCCont.PrtNo=LCPol.PrtNo "
	+ "and LCCont.Appflag not in ('4','1')  "
	//+ "and LCCont.uwflag not in ('1','8','a')"
	+ "and lcpol.Appflag not in ('4','1')  "
	//+ "and lcpol.uwflag not in ('1','8','a')"
    + tStrCondUWFlag
	+ " and LOPRTManager.ManageCom like '" + fm.ManageCom.value + "%' "
	+ getWherePart('LCCont.agentGroup', 'agentGroup')
	+ getWherePart('LCCont.PayMode', 'PayMode')
	+ getWherePart('LCCont.CvaliDate', 'CvaliDate') 
	+ getWherePart('LCCont.PrtNo', 'PrtNo')	
	+ getWherePart('LCCont.AppntName', 'AppntName')	
	//+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
	+ getWherePart('LAAgent.Groupagentcode','AgentCode')
	//+ getWherePart('LOPRTManager.AgentCode','AgentCode')
	+ getWherePart('LOPRTManager.MakeDate','MakeDate')
	+ getWherePart('LCCont.PolApplyDate','PolApplyDate')
	+"and LCCont.PayMode <> '8' "
	+" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'"
	+ tCardFlag
	//+ " and (LCCont.CardFlag is null or LCCont.CardFlag != '8') "
	+ " group by LOPRTManager.PrtSeq,LCCont.PrtNo,LCCont.PolApplyDate,"
	+ "LCCont.CValiDate,LCCont.AppntName,LOPRTManager.AgentCode,LOPRTManager.ManageCom,"
	+ "LOPRTManager.makedate,LOPRTManager.stateflag,LCCont.AgentGroup,LCCont.PayMode"
	;
	if(fm.ManageCom.value.length <= 2){
		alert("请用四位以及四位以上的管理机构进行查询!");
		return false;
	}
	strSQL="select a,b,c,d,e,f,p,m,o,g,h,i,'',n,'' from ("
	       + cstrSQL
	       +") as x where 1=1 order by n, m "
	       ;
  var groupIndex = cstrSQL.indexOf(" group by");
  //现金交费方式的前150条记录的打印脚本   2008-2-15
  SqlPDF = "select a from (" + cstrSQL.substring(0, groupIndex) 
	      + " and LCCont.PayMode = '1'" + cstrSQL.substring(groupIndex)
	      + ") as x order by n, m fetch first 150 rows only";
	

//	strSQL = "SELECT LOPRTManager.PrtSeq,LCCont.PrtNo,LCCont.AppntName,LOPRTManager.AgentCode, LCCont.prem,LCCOnt.CvaliDate,LOPRTManager.makedate,LOPRTManager.ManageCom FROM LOPRTManager,LAAgent,LABranchGroup,LCCont WHERE LOPRTManager.Code = '07' " 
//	+ "and LAAgent.AgentCode = LOPRTManager.AgentCode "
//	+ "and LABranchGroup.AgentGroup = LAAgent.AgentGroup "
//	+ "and LOPRTManager.OtherNo = LCCont.ContNO "	
//	+ getWherePart('LCCont.CvaliDate', 'CvaliDate') 
//	+ getWherePart('LCCont.PrtNo', 'PrtNo')	
//	+ getWherePart('LCCont.AppntName', 'AppntName')	
//	+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
//	+ getWherePart('LOPRTManager.AgentCode','AgentCode')
//	+ getWherePart('LOPRTManager.MakeDate','MakeDate')
//	+ getWherePart('LCCont.PolApplyDate','PolApplyDate')
//	+" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'"
//	;
	if(RePrintFlag == "1"){
		var mstrSQL
	mstrSQL = "SELECT LOPRTManager.PrtSeq a,LCCont.PrtNo b,LCCont.PolApplyDate c,LCCont.CValiDate d,LCCont.AppntName e,(select Groupagentcode from LAAgent aa where aa.AgentCode = LOPRTManager.AgentCode) f,LOPRTManager.ManageCom g,"
	+ "LOPRTManager.makedate h, "
	+ " sum(lcpol.prem) i, "
	+ "(to_zero((select sum(paymoney) from ljtempfee where otherno=LCCont.PrtNo and enteraccdate is not null and confflag = '0'))) j, "
	+ tLoadFlag
	+ ",CodeName('paymode',LCCont.PayMode) n,"
	+ "(select substr(branchattr, 1, 10) from LABranchGroup bg where bg.AgentGroup = LCCont.AgentGroup) m, "
	+ "(select Name from LABranchGroup bg where BranchLevel = '03' and bg.BranchAttr = (select substr(branchattr, 1, 10) from LABranchGroup bg where bg.AgentGroup = LCCont.AgentGroup)) o, "
	+ "(select Name from LAAgent aa where aa.AgentCode = LOPRTManager.AgentCode) p "
	+ "FROM LOPRTManager,LAAgent,LCCont,lcpol WHERE LCCont.Conttype='1' and LCPol.ContType='1' and LOPRTManager.Code = '07' " 
	+ "and LAAgent.AgentCode = LOPRTManager.AgentCode "
	+ "and LOPRTManager.OtherNo = LCCont.proposalContNo "	
	+ "and LCCont.PrtNo=LCPol.PrtNo "
	+ "and LCCont.Appflag not in ('4','1')  "
	+ "and lcpol.Appflag not in ('4','1')  "
	//+ "and lcpol.uwflag not in ('1','8','a')"
    + tStrCondUWFlag
	+ " and LOPRTManager.ManageCom like '" + fm.ManageCom.value + "%' "
	+ getWherePart('LCCont.agentGroup', 'agentGroup')
	+ getWherePart('LCCont.PayMode', 'PayMode')
	+ getWherePart('LCCont.CvaliDate', 'CvaliDate') 
	+ getWherePart('LCCont.PrtNo', 'PrtNo')	
	+ getWherePart('LCCont.AppntName', 'AppntName')	
	+ "and LCCont.PayMode <> '8' "
	//+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
	+ getWherePart('LAAgent.Groupagentcode','AgentCode')
	//+ getWherePart('LOPRTManager.AgentCode','AgentCode')
	+ getWherePart('LOPRTManager.MakeDate','MakeDate')
	+ getWherePart('LCCont.PolApplyDate','PolApplyDate')
	+ tStateFlag
	//+ " and (LCCont.CardFlag is null or LCCont.CardFlag != '8') "
	+ " group by LOPRTManager.PrtSeq,LCCont.PrtNo,LCCont.PolApplyDate,"
	+ "LCCont.CValiDate,LCCont.AppntName,LOPRTManager.AgentCode,LOPRTManager.ManageCom,"
	+ "LOPRTManager.makedate,LOPRTManager.stateflag,LCCont.AgentGroup,LCCont.PayMode"
	;
	strSQL="select a,b,c,d,e,f,p,m,o,g,h,i,j,n,k from ("
	       + mstrSQL
	       +") as x where 1=1 "
	       + tSeltype
	       + " order by n, m "
	       ;
	  groupIndex = mstrSQL.indexOf(" group by");
    //现金交费方式的前150条记录的打印脚本   2008-2-15
	  SqlPDF = "select a from (" + mstrSQL.substring(0, groupIndex) 
	        + " and LCCont.PayMode = '1'" + mstrSQL.substring(groupIndex)
	        + ") as x where 1 = 1 " + tSeltype + " order by n, m fetch first 150 rows only";
	}
	
	fm.querySql.value = strSQL;//用于报表的打印    zhangjianbao    2007-10-31
	  
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  //alert(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合查询条件的记录");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //SqlPDF = strSQL;  
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}

function caseView()
{
//	fmSave.action = "../archive";	
//	fmSave.submit();
//	alert(PolGrid.getSelNo());
	if(PolGrid.getSelNo()<1)
	{
		alert("请先选择一条信息");
		return false	
	}
	
	var prtNo = PolGrid.getRowColData(PolGrid.getSelNo()-1,2);
	
	if(prtNo=="")
	{
		alert("请先选择一条非空信息");
		return false	
	}
	
	window.open("../archive?action=display&account="+prtNo);
	//window.open("../archive?action=display&account=00000000001");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  	fm.agentGroup.value = arrResult[0][0];
  }
}

//下载投保单信息清单	2007-10-31
function printList()
{
	if(fm.querySql.value != null && fm.querySql.value != "" && PolGrid.mulLineCount > 0)
	{
		fm.action = "FirstPayPrint.jsp";
		fm.target = "_blank";
		fm.submit();
	}
	else
	{
		alert("请先执行查询操作，再打印！");
		return ;
	}
}

//下拉框选择后执行   zhangjianbao   2007-11-16
function afterCodeSelect(cName, Filed)
{	
  if(cName=='statuskind')
  {
    saleChnl = fm.saleChannel.value;
	}
}

//选择营销机构前执行   zhangjianbao   2007-11-16
function beforeCodeSelect()
{
	if(saleChnl == "") alert("请先选择销售渠道");
}

function newprintPolpdf()	
{
  var i = 0;  
  var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	else
	{
		//arrReturn = getQueryResult();
		PrtSeq = PolGrid.getRowColData(tChked[0],1)
		PrtNo = PolGrid.getRowColData(tChked[0],2)		
		fmSave.PrtSeq.value = PrtSeq;
		fmSave.PrtNo.value = PrtNo;
		var contno = easyExecSql(" select distinct contno from (select contno,modifydate from lccont where PrtNo='"+PrtNo+"' order by modifydate fetch first rows only) as x");
		//fmSave.PolNo.value = arrReturn[0][1];
		fmSave.fmtransact.value = "PRINT";
		fmSave.target = "fraSubmit";
		fmSave.action = "../uw/PDFPrintSave.jsp?Code=07&OtherNo="+contno;
		fmSave.submit();
		
		
	}
}
function afterSubmit2(FlagStr,Content)
{
	//showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}
function newprintPolpdfbat()
{
	if (PolGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}


	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	if(tChked.length == 0)
	{
	  if(!confirm("由于您没有选择要打印的记录，将打印前150条现金缴费的数据。确认吗？"))
	  {
	    return false;
	  }
	  //sqlpdf用于存放没有选择时的批打脚本   2008-2-15
	  fmSave.strsql.value = SqlPDF;
		fmSave.action="./FirstPayAllBatPrt.jsp";
		fmSave.submit();
	}
	else
	{
		fmSave.action="./FirstPayForBatPrt.jsp";
		fmSave.submit();
	}
}
