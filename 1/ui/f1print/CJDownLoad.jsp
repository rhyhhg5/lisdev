<%@page import="java.io.PrintStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.BufferedOutputStream"%>
<%@ page contentType="application/x-xls;charset=GBK"%>
<%@ page import="java.io.File,java.io.FileInputStream"%>
<%@ page import="javax.servlet.ServletOutputStream"%>
<%@ page import="javax.servlet.http.*"%>
<%
//若使用字节流进行文件下载，应该首先清空一下out对象，默认是字符流
	request.setCharacterEncoding("GBK");
	response.setCharacterEncoding("GBK");
	String filename=request.getParameter("filename");
	String filenamepath=request.getParameter("filenamepath");
	//filename=new String(filename.getBytes("UTF-8"), "GBK");
	filename=java.net.URLDecoder.decode(filename, "UTF-8");
	filenamepath=java.net.URLDecoder.decode(filenamepath, "UTF-8");
	//filenamepath=new String(filenamepath.getBytes("UTF-8"), "GBK");
	System.out.println("filename="+filename);
	System.out.println("filenamepath="+filenamepath);
	out.clear(); 
	out = pageContext.pushBody();
	//通过设置头标题来显示文件传送到前端浏览器的文件信息
	response.setHeader("Content-disposition","attachment; filename="+java.net.URLEncoder.encode(filename, "UTF-8")); 
	File file = new File(filenamepath);
	response.setHeader("Content_Length",String.valueOf(file.length()));
//	BufferedOutputStream bout = null;
	BufferedInputStream bin = null;
	PrintStream ps = null;
	byte[] buffer = new byte[1024];
	try{
	   bin = new BufferedInputStream(new FileInputStream(file));
//	   bout = new BufferedOutputStream(response.getOutputStream());
	   ps = new PrintStream(response.getOutputStream(),true);
	   
	   int length = 0;
	   while((length=bin.read(buffer))!=-1){
//	    bout.write(buffer,0,length);
		ps.write(buffer,0,length);
	   }
	   bin.close();
//	   bout.close();
		ps.close();
	}catch(Exception e){
	   System.out.print(e.getMessage());
	}finally{
	   try{
	   	if(bin !=null) bin.close();
//	   	if(bout !=null) bout.close();
	   }catch(Exception ex){
		   System.out.print(ex.getMessage());
	   }
	}

%>