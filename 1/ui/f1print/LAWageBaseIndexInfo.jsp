<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAWageBaseIndexInfo.jsp
//程序功能：
//创建日期：2008-06-27
//创建人  ：苗祥征
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>    
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAWageBaseIndexInfo.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="./LAWageBaseIndexInfoInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title></title>
  <script>
</script>
</head>

<body  onload="initForm(); initElementtype();" >
  <form action="./LAWageBaseIndexInfoReport.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAIndexInfo1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </tr>
    </table>
    <Div  id= "divLAIndexInfo1" style= "display: ''">
    
  <table  class= common>
    <tr  class= common>        
       <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>
         <TD class=title>
          销售单位代码
        </TD>
        <TD class=title>
          <Input class=common name=BranchAttr>
        </TD>
          
     </TR>
      <TR class=common>
        <TD  class= title>
        代理人编码
       </TD>
       <TD  class= input>
            <Input class=common name=AgentCode >
       </TD>
                  <TD  class= title>
            起始年月
          </TD>
          <TD  class= input>
            <Input class=common name=BIndexCalNo verify="起始年月|notnull&len<=6&int" elementtype=nacessary >
            <font color="red"> 'YYYYMM'</font>
          </TD>
        
      </TR>
      <TR  class= common>
          <TD  class= title>
            终止年月
          </TD>
          <TD  class= input>
            <Input class=common name=EIndexCalNo verify="终止年月|notnull&len<=6&int" elementtype=nacessary >
            <font color="red"> 'YYYYMM'</font>
          </TD>
              
    </tr>
  </table>
   
   <INPUT class=cssbutton VALUE="查  询" TYPE=button onclick="easyQueryClick();" >
   <INPUT VALUE="打  印" TYPE=button onclick="DoSDownload();" class=cssbutton>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divIndexInfoGrid1);">
    		</td>
    		<td class= titleImg>
    			 考核指标信息
    		</td>
    </tr>
    </table>
  	<Div  id= "divIndexInfoGrid1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanIndexInfoGrid1" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
  	  <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton > 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
  	</div>

	      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	      <input type=hidden name=HiddenStrMoreResult value="">
	      <input type=hidden name=BranchType value="">
	      <input type=hidden class=Common name=querySql > 
    	  <input type=hidden class=Common name=querySqlTitle > 
          <input type=hidden class=Common name=Title >
  </form>
</body>
</html>
