<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">

function initInpBox(){ 

  	try{                                   
     	fm.all('Order1').value='';
	 	fm.all('Order2').value='';
	 	fm.all('Order3').value='';
  	}catch(ex){
    	alert("在PillotConfigInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  	}      
}                                      

function initForm(){
  	try{
    	//initInpBox();
		initPilotGrid();
  	}catch(re){
    	alert("PillotConfigInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  	}
}

// 报案信息列表的初始化
function initPilotGrid(){
	
    var iArray = new Array();
      
    try{
    	
      	iArray[0]=new Array();
      	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      	iArray[0][1]="30px";            		//列宽
      	iArray[0][2]=10;            			//列最大值
      	iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      	iArray[1]=new Array();
      	iArray[1][0]="机构编码";         		//列名
      	iArray[1][1]="100px";            		//列宽
      	iArray[1][2]=100;            			//列最大值
      	iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      	iArray[2]=new Array();
      	iArray[2][0]="机构名称";         		//列名
      	iArray[2][1]="100px";            		//列宽
      	iArray[2][2]=100;            			//列最大值
      	iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      	iArray[3]=new Array();
      	iArray[3][0]="是否为试点机构";         		//列名
      	iArray[3][1]="80px";            		//列宽
      	iArray[3][2]=100;            			//列最大值
      	iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      	PilotGrid = new MulLineEnter( "fm" , "PilotGrid" ); 
      	//这些属性必须在loadMulLine前
      	PilotGrid.mulLineCount = 0;   
      	PilotGrid.displayTitle = 1;
      	PilotGrid.canSel=1;
      	PilotGrid.hiddenPlus=1;
      	PilotGrid.hiddenSubtraction=1
      	PilotGrid.loadMulLine(iArray);
    }catch(ex){
        alert(ex);
    }
}

</script>