//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
 if (verifyInput() == false)
    return false;
  if(!beforeSubmit())
  {
    return false;
  }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
	showInfo.close();
}
function checkAgent()
{
  var sql = "select * from laagent where groupagentcode ='"+fm.all('GroupAgentCode').value+"' and branchtype='2' and branchtype2='01'";
  var strQueryResult  = easyQueryVer3(sql, 1, 1, 1);
  if(!strQueryResult)
  {
    alert("该代理人不存在！");
    fm.all('GroupAgentCode').value ='';
    return false;
  }
  
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
//    alert(fm.all('AgentCode').value);
}
function beforeSubmit()
{
	//校验开始年月必须为某年的一月
	var month = fm.all('StartMonth').value;
	var endmonth = fm.all('EndMonth').value;
   if(parseInt(endmonth)-parseInt(month)<0)
	{
	  alert("结束日期应该大于开始日期！");
	  return false;
	} 
	if(trim(month.substring(4,6))!='01')
	{
	   alert("开始年月必须为某年的一月！");
	   return false;
	}	
	//统计日期必须在同一年月
	if(trim(month.substring(0,4))!=trim(endmonth.substring(0,4)))
	{
	  alert("统计起期与统计止期必须在同一年！");  
	  return false;
	}
	//录入机构不能为中介，中介不在此统计范围内
	if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
	{
	  var sSQL = "select branchlevel,state from labranchgroup where branchattr='"+fm.all('BranchAttr').value+"' and branchtype='2'";
	   var strQueryResult  = easyQueryVer3(sSQL, 1, 1, 1);
	   
       var arr = decodeEasyQueryResult(strQueryResult);
       if(arr[0][0]==null||arr[0][0]=='')
      {
        alert("系统不存在该机构！");
        return false;
      } 
      else
      {
       if(arr[0][0]=='31')
       {
         alert("中介机构不在统计范围内，请重新输入！");
         return false;
       }
       if(arr[0][1]=='1')
       {
         alert("公司业务不在统计范围内!");
         return false;
       }
       }
	}
	if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!="")
	{
	   
	    var sSQL = "select branchtype2 from laagent where groupagentcode='"+fm.all('GroupAgentCode').value+"' and branchtype='2'";
	   var strQueryResult  = easyQueryVer3(sSQL, 1, 1, 1);
       var arr1 = decodeEasyQueryResult(strQueryResult);
       if(arr1[0][0]==null||arr1[0][0]=='')
       {
         alert("系统不存在该代理人！");
         return false;
       }
       else
       {
       if(arr1[0][0]=='02')
       {
         alert("中介人员不在统计范围内，请重新输入！");
         return false;
       }
       }
	}
	return true;	
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
  }
}

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  showSubmitFrame(mDebug); 
  fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

                                                                                                                   