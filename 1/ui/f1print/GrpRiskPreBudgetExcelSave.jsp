
<jsp:directive.page
	import="com.sinosoft.lis.certify.SysOperatorNoticeBL" />
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：集团风险保费收支报表
	//程序功能：
	//创建日期：2013-10-14
	//创建人  ：yan
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<%
	System.out.println("start GrpRiskPreBudgetExcelSave --------");
	String mDay[] = new String[2];
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	//输出参数
	CError cError = new CError();
	//后面要执行的动作：添加，修改，删除
	String FlagStr = "";
	String Content = "";
	String tOutXmlPath="";  //文件路径
	String fileName = "";	//文件名称
	String strOperation = "";//用来判断是否是打印标记--PRINT
	String strOpt = "";//打印类型--保费应收（PreReceive）--保费收支（PreBudget）
	String sManageCom = ""; //选择的公司代码
	strOperation = request.getParameter("fmtransact");
	strOpt = request.getParameter("Opt");
	mDay[0] = request.getParameter("StartDate");   //打印起期
	mDay[1] = request.getParameter("EndDate");     //打印止期
	sManageCom = request.getParameter("ManageCom");		//打印公司代码
	
	//输出打印信息：
	System.out.println("打印类型是:" + strOpt+";打印时间：" + mDay[0]+"至"+mDay[1]);
	System.out.println("tG.Operator："+tG.Operator+";sManageCom："+sManageCom);
	System.out.println("serverletPath:"+request.getServletPath());

	VData tVData = new VData();	
	tVData.addElement(mDay);
	tVData.addElement(tG);
	tVData.addElement(sManageCom);
	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
	String tDate=dateFormat.format(new Date());
	String tTime=timeFormat.format(new Date());
	fileName=tDate.replace("-","")+tTime.replace(":","")+"-PB-"+tG.Operator+".xls";
	System.out.println("文件名称-fileName："+fileName);
	tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
//	tOutXmlPath = "F:/PBFiles/"+fileName;
	System.out.println("文件路径-tOutXmlPath:" + tOutXmlPath);
	tVData.addElement(tOutXmlPath);
	tVData.addElement(fileName);
	
	RptMetaDataRecorder rpt = new RptMetaDataRecorder(request);
	
	//   集团风险--应收保费报表打印-PreBudget
	if (strOpt.equals("PreBudget")) {
		System.out.println("strOpt的标志是：" + strOpt);
		GrpRiskPreBudgetExcelUI tGrpRiskPreBudgetExcelUI = new GrpRiskPreBudgetExcelUI();
		if (!tGrpRiskPreBudgetExcelUI.submitData(tVData, strOperation)) {
		    System.out.println("报错信息：");
            Content = "报表下载失败，原因是:" + tGrpRiskPreBudgetExcelUI.mErrors.getFirstError();
            FlagStr = "Fail";
		}
	}
		
          String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
          File file = new File(tOutXmlPath);
          rpt.updateReportMetaData(FileName);
          response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[10000];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
//              file.delete();
          }
  if (!FlagStr.equals("Fail"))
  {
    Content = "打印报表成功！";
    FlagStr = "Succ";
  }
%>

<html>
   <script language="javascript">  
    alert('<%=Content %>');
    top.close();
   </script>
</html>