<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<% 
//程序名称：
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<script>
	var RePrintFlag = "<%=request.getParameter("RePrintFlag")%>";//记录是否重打
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="GetFeeBatchInvoiceInput.js"></SCRIPT>    
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="GetFeeBatchInvoiceInit.jsp"%>
<%
   String PayName = request.getParameter("IncType");
    
%>   
</head>
<body  onload="initForm();" >    
  <form  method=post name=fm target="fraSubmit">
    <table class= common border=0 width=100%>
    	<tr>     		
    		 <td class= titleImg>
        		输入查询条件
       		 </td>   		      
    	</tr>
    </table>   
    <Div  id= "divFeeOth" style= "display: ''"> 
    <table class= common border=0 width=100%>
      <TR  class= common> 
        <TD  class= title>  业务号码类型  </TD>
        <TD  class= input>	<Input class=codeno name=IncomeType elementtype=nacessary verify="业务号码类型|NOTNULL" CodeData="0|^1|理赔案件号^2|个人批单号^3|团体批单号^4|满期给付号" ondblClick="showCodeListEx('IncomeType',[this,IncomeTypeName],[0,1]);" onkeyup="showCodeListKeyEx('IncomeType',[this,IncomeTypeName],[0,1]);"><input class=codename name =IncomeTypeName></TD> 
        <TD  class= title>  实付号码</TD>
        <TD  class= input>  <Input class= common name=PrtNo ></TD> 
        </TR>
     </table>
    </Div>
    <Div  id= "divFeeNo" style= "display: ''"> 
     <table class= common border=0 width=100%>         
      <TR  class= common>         
        <TD  class= title> 业务号码</TD> 
        <TD  class= input> <Input class= common name=IncomeNo > </TD>
        </div>
        <TD  class= title>  管理机构   </TD>
        <TD  class= input>
        <Input class="codeno" name=MngCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,MngComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,MngComName],[0,1],null,null,null,1);"><input class=codename name =MngComName></TD> 
        </TD> 
       
      </TR>     
    </table>
   </DIV> 
   <Div  id= "SaleChnlDiv" style= "display: ''"> 
     <table class= common border=0 width=100%>         
      <TR  class= common> 
      	<TD  class= title>  起始到账日期</TD>
        <TD  class= input>	<Input class= "coolDatePicker" dateFormat="short" name=EnterAccDate >  </TD>        
        <TD  class= title> 截至到账日期</TD> 
        <TD  class= input> <Input class= "coolDatePicker" dateFormat="short" name=EnterAccDateEnd > </TD>
       </TR>  
       <TR  class= common>  
        <TD  class= title> 发票状态  </TD> 
        <TD  class= input> 
        <Input class=codeNo name=InvoiceState value='1'  CodeData="0|^1|未打印^2|已打印^3|全部^4|不打印" ondblclick="return showCodeListEx('InvoiceState',[this,InvoiceStateName],[0,1]);" onkeyup="return showCodeListKeyEx('InvoiceState',[this,InvoiceStateName],[0,1]);"><input class=codename name=InvoiceStateName value='未打印' readonly=true>
        </TD>
        
      </TR>     
    </table>
   </DIV> 
   
 
    <Div  id= "divFeeButton" style= "display: ''">   
		<INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="find();"> 
  	</Div>          
   
  
  	<input TYPE=hidden  name=PayNo> 
  	<input TYPE=hidden  name=IncNo> 
  	<input TYPE=hidden name= SQLstr>  
  	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJAPay1);">
    		</td>
    		<td class= titleImg>
    			 发票信息
    		</td>
    		<td>
    		  <font color="#FF0000" size="2">
    		     在打印前请您将手中的发票实物同系统提示信息项核对，如发现实物同系统提示不符，请及时到单证管理模块进行调整。系统打印过程中将自动从最小的发票号码进行核销。
    		  </font>
    		</td>
    	</tr>
    </table>
  	<Div  id= "divStart" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanStartGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="turnPage1.lastPage();">				
  	</div>  
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJAPay1);">
    		</td>
    		<td class= titleImg>
    			 费用信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLJAPay1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  	
    </div>
        <input type=hidden id="fmtransact" name="fmtransact">
        <input type=hidden id="EnterAccDate1" name="EnterAccDate1">
        

  	<Div  id= "divRemark" style= "display: ''">
      	<table  class= common>
    	  <tr>     		
    		 <td class= titleImg>
        		额外信息录入
       		</td>   		      
       		<td><input TYPE="checkBox" name ="CertifyFlag" value="01" onclick = "showCertify();" > 手工录入
       		</td>
       		<td id="CertifyInfo" style= "display: 'none'"  colspan=4>
    		  <font color="#FF0000" size="2">
    		     所录入的起始号和终止号必须属于一个号段范围
    		  </font>
    		  </td>
    	  </tr> 
        <TR  class= common id="CertifyStart" style="display:''" >
          <TD class= title>
          	发票起始号
          </TD>
          <TD class= title>
          	<input class="readonly" name="StartNo" ReadOnly >
          </TD>
        </TR>              	     		
        <TR  class= common id="CertifyInput" style="display:'none'" >
          <TD class= title>
          	发票起始号
          </TD>
          <TD class= title>
          	<input class="common" name="StaNo" >
          </TD>
          <TD class= title>
          	发票终止号
          </TD>
          <TD class= title>
          	<input class="common" name="EndNo" >
          </TD>
        </TR>
       		<TR  class= common>
          <TD class= title>
            经手人
          </TD>  
          <TD class= input>
            <Input name="HPerson" class= common >
          </TD>  
          <TD class= title>
            复核人
          </TD>  
          <TD class= input>
            <Input name="CPerson" class= common >
          </TD> 
          <TD class= title>
            附注
          </TD>
          <TD class= input>
            <Input name="Remark" class= common >
          </TD>           
  			</TR>
  			<TR  class= common>
          <TD class= title>
            发票代码
          </TD>  
          <TD class= input>
            <Input name="FPDM" class= common >
          </TD>  
  			</TR>                                  		
    	</table>
		<INPUT VALUE="发票打印" class= cssButton TYPE=button onclick="PPrint();"> 
	    <INPUT VALUE="不打印发票" class= cssButton TYPE=hidden onclick="NPPrint();"> 
		<INPUT TYPE="hidden" name="CertifyCode"> 
		<INPUT TYPE="hidden" name="Operator" value="<%=tG.Operator%>"> 
		<INPUT TYPE="hidden" name="intRecordNum"> 
		
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 