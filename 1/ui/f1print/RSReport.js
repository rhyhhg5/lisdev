//RSReport.js
//该文件中包含客户端需要处理的函数和事件
//f1打印处理

//需求变更，下面的语句都要加上控制
//1.报表只需要合同分保的数据ProtItem=T
//2.报表不体现无名单和公共账户的保单

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
/**
 * 后台数据库查询
 */
function easyPrint() {
  // 拼SQL语句，从页面采集信息
  var strSql;
  if(fm.Code.value =='SQ'||fm.Code.value =='XQ'){
   strSql = "SELECT a.RISKCODE,a.polno,b.prtno,a.InsuredName,a.InsuredYear,a.InsuredSex,"
                //     分出险种代码，分出保单保单号，分出保单印刷号，被保人姓名，保单年度，被保人姓别                
               +"c.IDType,c.IDNo,a.InsuredBirthday,c.OccupationType,b.salechnl,a.payIntv,a.amnt,"
                //       证件类型，证件号码，出生年月日，职业类型，销售方式，缴费方式，基本保额
               +"b.Mult,'','',a.EnterCA,a.SumRiskAmount,a.riskamnt-a.CessionAmount,a.payYears,a.cvalidate,a.enddate,"
               //保单份数，责任倍数，分保保额，年初现金价值，累计风险保额，自留额,缴费年限，保单生效日，保单责任终止日
               +"a.cessionRate,a.CessionAmount,a.prem,"
               //分保比例，风险分保金额，风险保费
               +"LR_AddPremScore(1,a.polno,1), LR_AddPremScore(1,a.polno,2),"                                              
                //次标准体加费    职业加费
               +" a.cessprem,a.cessprem,'','','',a.cesscommRate,a.cesscomm,"
               +" '','',"
               //分保费合计，标准分保费，分保职业加费，分保次标准体加费，分保加费合计，选择折扣，手续费，意外加点，寿险加点，重疾加点，健康加点,风险保额
                +"LR_AddPremScore(2,a.polno,2),LR_AddPremScore(2,a.polno,1), "  
               //职业评点 //健康评点
               +" a.riskamnt"
               //风险保额
               +" FROM LRPOL a,lcpol b,ldperson c"
               +" where a.PolNo=b.polno  "  
               +" and a.InsuredNo=c.customerno and a.GrpPolNo='00000000000000000000'" 
               +" and a.ReinsurItem='C'  and a.ProtItem='T' and b.PolTypeFlag not in('1','2') "
	           + getWherePart('a.cvalidate','time','>=','0')
	           + getWherePart('a.cvalidate','timeend','<=','0')
	           ;
	var SXSql=""; 
	if(fm.Code.value =='SQ')    SXSql=" and a.InsuredYear=1";
	if(fm.Code.value =='XQ')    SXSql=" and a.InsuredYear>1";
    strSql+=SXSql;
    strSql="("+strSql+") union all ("+replace(strSql,"lcpol","lbpol")+")";    
    //alert(strSql);
    turnPage.queryModal(strSql, VarGrid);        
	easyQueryPrint(2,'VarGrid','turnPage'); 
  } 
  if(fm.Code.value =='BG'){ 

    strSql="select  a.polno,b.prtno,a.RISKCODE,c.edorno,c.edortype,c.insuredno,c.insuredname,"
      //分出保单保单号,分出保单印刷号，分出险种代码,批单号，批改原因，被保人客户代码，客户姓名
      +"d.IDNo,b.occupationtype,a.insuredsex,a.InsuredBirthday,b.mult,a.payYears,a.payintv,"
      //  证件号码，批改后职业类别，性别，出生日期，保单份数,缴费期限,缴费频率
      +"a.cvalidate,a.enddate,c.confdate,c.cessEnd-c.cessStart,'','','',c.ChgCessAmt,c.shRePrem,c.shRecomm"
      //  保单生效日,保单终止日,变更日期,承担责任天数,分保退保费,分保退保金,分保利息,调整分保保额,调整总分保费,调整总分保手续费
      +" from LRPOL a,lcpol b,lredormain c,ldperson d where a.polno=b.polno "
      +" and a.insuredno=d.customerno  and a.ReinsurItem='C' and a.ProtItem='T' and b.PolTypeFlag not in('1','2') "
      +" and a.GrpPolNo='00000000000000000000' and a.polno=c.polno  and a.ReinsureCom=c.ReinsureCom"
      +" and a.InsuredYear=c.InsuredYear and a.ReinsurItem=c.ReinsurItem"
      + getWherePart('c.confdate','time','>=','0')
	  + getWherePart('c.confdate','timeend','<=','0')
	  ;
     strSql="("+strSql+") union all ("+replace(strSql,"lcpol","lbpol")+")";       
     //alert(strSql);   
     turnPage.queryModal(strSql, BGGrid);        
	 easyQueryPrint(2,'BGGrid','turnPage');  
   }
   if(fm.Code.value =='LP'){ 
    
     strSql="select a.prtno,b.clmno,b.insuredyear,b.polno,b.riskcode,b.cvalidate,b.insuredno,b.insuredname, "
       //分出保单印刷号，赔案号，保单年度，保单号，险种代码，保单生效日期，被保险人代码，姓名
       +" c.birthday,c.sex,c.idtype,c.idno,a.mult,d.accidentdate ,d.rptdate,e.rgtdate,'',f.endcasedate,"
       //出生年月日，性别，证件类型，证件号，投保份数，出险日期，报案日期，立案日期，入院日期，赔款日期
       +" '',b.cessionrate,LR_claim(b.clmno,b.polno),LR_Pay('0',b.clmno,b.polno,'0'),LR_Pay('0',b.clmno,b.polno,'1'),"
       //住院天数 ，分保比例，理赔是否中止保单责任，实际赔偿金额，实际给付金额，
       +" LR_Pay('1',b.clmno,b.polno,'0'),LR_Pay('1',b.clmno,b.polno,'1')"
       //分保赔款金额，分保给付金额
       +" from lcpol a,LRClaimPolicy b,ldperson c,llreport d,LLRegister e,llclaim f "
       +" where a.polno=b.polno and b.insuredno=c.customerno and b.RgtNo=e.RgtNo"
       +" and b.clmno=f.clmno and e.RptNo=d.RptNo and b.ReinsureCom='1001'"
       +" and b.ReinsurItem='C'  and b.GrpPolNo='00000000000000000000' "
       + getWherePart('f.endcasedate','time','>=','0')
	   + getWherePart('f.endcasedate','timeend','<=','0')
       ;
       //strSql="("+strSql+") union all ("+replace(strSql,"lcpol","lbpol")+")";       
       //alert(strSql);   
       turnPage.queryModal(strSql, LPGrid);        
	   easyQueryPrint(2,'LPGrid','turnPage');  
 
   } 
   if(fm.Code.value =='GSQ'||fm.Code.value =='GXQ'){ 
       strSql="select c.managecom,a.grppolno,c.prtno,a.polno,a.riskcode,c.GrpName,c.amnt,c.amnt/NVL(c.peoples2,1),c.peoples2,"
             // 管理机构,团体保险单号,分出公司团体保单印刷号,分出公司个人保单号,险种代码,团体名称,总保额,平均保额,投保人数
             +" '','',a.prem,'',a.CessionRate,a.CessComm,a.CessionAmount,a.CessPrem,a.CValiDate,a.EndDate,"
             //每份保费,份数,毛保费,参保比例,分保比例,手续费 分保额 保单生效日期 保单中止日 
             +" a.payintv,a.payyears,a.insuredname,a.InsuredYear,d.idno,a.riskcode,a.amnt"
             //缴费方式 ,缴费年限, 被保人姓名 ,保单年度，身份证号, 险种代码 ,保额
             +" from lrpol a,lcgrppol c,ldperson d"
             +" where  a.grppolno=c.GrpPolNo and a.insuredno=d.customerno and a.ReinsureCom='1001'"
             +" and a.ReinsurItem='C' and a.ProtItem='T' and a.GrpPolNo!='00000000000000000000' "//客户表保证没有无名单和公共账户数据
             + getWherePart('a.cvalidate','time','>=','0')
	         + getWherePart('a.cvalidate','timeend','<=','0')
       ;
       var GSXSql=""; 
	   if(fm.Code.value =='GSQ')    GSXSql=" and a.InsuredYear=1";
	   if(fm.Code.value =='GXQ')    GSXSql=" and a.InsuredYear>1";
	   strSql+=GSXSql;
       strSql="("+strSql+") union all ("+replace(strSql,"lcgrppol","lbgrppol")+")";    
       //alert(strSql);
       turnPage.queryModal(strSql, GSXQGrid);        
	   easyQueryPrint(2,'GSXQGrid','turnPage'); 
    }   
    if(fm.Code.value =='GBG'){ 
       strSql="select  c.managecom,c.grppolno,b.polno,a.edorno,c.riskcode,c.grpname, "
             //管理机构,团体保险单号,个人保单号,个人批单号,险种代码,团体名称
             +" a.ChgAmnt,a.ChgPrem,a.ShReComm,'','',b.CessionRate,"
             //调整的总保额，调整的总保费，调整的分保手续费，调整的分数，调整的投保人数，分保比例
             +" a.ChgCessAmt,a.ShRePrem,'','','',a.edortype " 
             //调整的分保额，调整的分保费，退保费，退手续费，退保金，批改类型
             +" from LREdorMain a,LRPol b,lcgrppol c "
             +" where a.GrpPolNo=c.GrpPolNo "
             +" and a.polno=b.polno and a.InsuredYear=b.InsuredYear "
             +" and a.ReinsurItem=b.ReinsurItem and a.ReinsureCom=b.ReinsureCom"
             +" and a.ReinsureCom='1001' "
             +" and a.ReinsurItem='C' and b.ProtItem='T' and a.GrpPolNo!='00000000000000000000' "
             + getWherePart('a.confdate','time','>=','0')
	         + getWherePart('a.confdate','timeend','<=','0')
       ;
       strSql="("+strSql+") union all ("+replace(strSql,"lcgrppol","lbgrppol")+")";    
       turnPage.queryModal(strSql, GBGGrid);        
	   easyQueryPrint(2,'GBGGrid','turnPage'); 
    }   
    if(fm.Code.value =='GLP'){ 
       strSql="select b.clmno,b.polno,a.managecom,b.grppolno,a.prtno,b.riskcode,b.insuredname,c.idno,b.cvalidate,  "
              //
              +" a.mult,d.rptdate,d.accidentdate ,e.rgtdate,'',f.endcasedate,"
              +" LR_claim_GB(e.RgtNo),'',"
              +" (select GetDutyKind from llclaim where getdutykind in('102','202') and llclaim.clmno=b.clmno),"
              +" LR_claim(b.clmno,b.polno),LR_PAY_Grp('0',b.clmno,b.polno,'0'),LR_PAY_Grp('0',b.clmno,b.polno,'1'),LR_PAY_Grp('1',b.clmno,b.polno,'0'),"
              +" LR_PAY_Grp('1',b.clmno,b.polno,'1')"
              +" from lcpol a,LRClaimPolicy b,ldperson c,llreport d,LLRegister e,llclaim f"
              +" where a.polno=b.polno"
              +" and b.insuredno=c.customerno"
              +" and b.RgtNo=e.RgtNo"
              +" and b.clmno=f.clmno"
              +" and e.RptNo=d.RptNo"
              +" and b.ReinsureCom='1001'"
              +" and b.ReinsurItem='C'"
              +" and b.GrpPolNo!='00000000000000000000'"
              + getWherePart('f.endcasedate','time','>=','0')
	          + getWherePart('f.endcasedate','timeend','<=','0')
       ;
       strSql="("+strSql+") union all ("+replace(strSql,"lcpol","lbpol")+")";    
       turnPage.queryModal(strSql, GLPGrid);        
	   easyQueryPrint(2,'GLPGrid','turnPage'); 
    }  
} 


