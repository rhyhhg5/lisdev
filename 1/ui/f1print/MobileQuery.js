//
//程序名称：MobileQuery.js
//程序功能：提取新契约手机号
//创建日期：2004-08-23
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容

//var arrDataSet;
var turnPage = new turnPageClass();

//简单查询
function easyQuery()
{
	var MngCom = fm.all('ManageCom').value;
	var sDate = fm.all('StartDate').value;
	var eDate = fm.all('EndDate').value;
	
	if (MngCom == "")
	{
			alert("请选择管理机构！");
			return;
	}
	if (sDate == "")
	{
			alert("请输入开始日期！");
			return;
	}
	if (eDate == "")
	{
			alert("请输入结束日期！");
			return;
	}
	
	// 书写SQL语句
	var strSQL = "select ldperson.name,max(ldperson.mobile),lcpol.ManageCom "
							+ "from ldperson,lcpol "
							+ "where ldperson.customerno = lcpol.appntno "
							+ "and lcpol.renewcount = '0' "
							+ "and lcpol.salechnl in ('02','03') "
//							+ " and lcpol.signdate >= " + sDate + " and lcpol.signdate <= " + eDate
//							+ " and lcpol.ManageCom like '" + MngCom + "' "
							+ getWherePart('lcpol.ManageCom','ManageCom','like','0')
							+ getWherePart('lcpol.signdate','StartDate','>=','0')
							+ getWherePart('lcpol.signdate','EndDate','<=','0')
							+ " group by ldperson.customerno,ldperson.name,lcpol.managecom";
	strSQL += " union all (" + replace(strSQL,"lcpol","lbpol") + ")	order by 2,3";
	//alert(strSQL);
	turnPage.queryModal(strSQL, CodeGrid);    
	//arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
}

function easyPrint()
{
	easyQueryPrint(2,'CodeGrid','turnPage');
}