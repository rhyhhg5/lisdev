//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet1,arrDataSet2; 
var turnPage = new turnPageClass();


var old_AgentGroup="";
var new_AgentGroup="";

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAComQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
     

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function DetailPrint()
{
  fm.action="../agentprint/LAIndexInfoQueryPrt.jsp";
  fm.target="f1print";
  submitForm();
}

//提交，保存按钮对应操作
function submitForm()
{  	
  	var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.target='f1print';
	  fm.submit(); //提交
	  showInfo.close();	
}

function DoSDownload()
{
	if(!verifyInput())
	{
		return false;
	}
//	// 薪资计算校验
	if(!checkIndex())
	{
		return false;
	}
    
  	var tEndNo = trim(fm.all('IndexCalNo').value);
	var tEndDate = tEndNo.substr(0,4)+"-"+tEndNo.substr(4,2)+"-01" ;
	var getStartDate = "select date_format(date('"+tEndDate+"') - 2 month,'yyyymm') from dual where 1=1 ";
	var strQueryResult = easyQueryVer3(getStartDate, 1, 1, 1);
	if(!strQueryResult)
     {
        alert("考核期计算有误，烦请核实页面录入所属年月字段是否有误！");
        return false;
     }
    var arr = decodeEasyQueryResult(strQueryResult);
	var tStartNo = arr[0][0];
     
  var tsql = "";
  tsql ="select aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,"
       +" (select BRANCHSTANDEXAM(hh, ee, ii, KK, jj, ll) from dual)," 
       +" (select BRANCHSTANDEXAM_T(hh, ee, ii,kk, jj,ll) from dual)"
       +" from "
       +" (select a.managecom  aa,"
       +" (select name from ldcom where comcode =a.managecom) bb,"
       +"a.branchattr cc,"
       +"a.name dd,"
       +"a.gbuildflag ee,"
       +"a.gbuildstartdate ff,"
       +"'"+tEndNo+"' gg,"
       +"(select db2inst1.MONTHS_BETWEEN(char((select startdate from lastatsegment where stattype='1' and yearmonth = int('"+tEndNo+"'))),char(a.gbuildstartdate)) + 1 from dual) hh,"
       +"(select GETBRANCHAGENT(a.agentgroup,a.gbuildflag,a.gbuildstartdate,'"+tEndNo+"') from dual) ii,"
       +"(select count(distinct agentcode) from laindexinfo  where branchtype = '1' and branchtype2 = '01' and indextype = '01' and indexcalno <= '"+tEndNo+"' and indexcalno >='"+tStartNo+"' and t67 = 99 and agentgrade like 'B%' " 
       +" and exists(select 1 from laagent where agentcode =laindexinfo.agentcode and agentcode in (select agentcode from laagent where branchtype ='1' and branchtype2 ='01' and agentgroup in (select agentgroup from labranchgroup where 1=1 and branchtype = '1' and branchtype2 = '01'and endflag = 'N' and substr(branchseries,1,12)=a.agentgroup)) )) jj,"
       +"(select count(distinct agentcode) from laindexinfo  where branchtype = '1' and branchtype2 = '01' and indextype = '01' and indexcalno <= '"+tEndNo+"' and indexcalno >='"+tStartNo+"' and t67 = 99 and agentgrade like 'A%' "
       +" and exists(select 1 from laagent where agentcode =laindexinfo.agentcode and agentcode in (select agentcode from laagent where branchtype ='1' and branchtype2 ='01' and agentgroup in (select agentgroup from labranchgroup where 1=1 and branchtype = '1' and branchtype2 = '01'and endflag = 'N' and substr(branchseries,1,12)=a.agentgroup)) )) kk,"
       +"(select GETBRANCHMONEY(a.agentgroup,a.gbuildflag,char(a.gbuildstartdate),'"+tEndNo+"') from dual) ll"
       +" from labranchgroup a" 
       +" where 1=1 "
       +"and a.branchtype = '1' and a.branchtype2 = '01' and a.gbuildflag in ('A','B') and branchlevel = '03' "
       //+ getWherePart("a.ManageCom","ManageCom")
        + getWherePart("a.branchlevel","BranchLevel");
        if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
         {
           tsql+=" and a.agentgroup = (select agentgroup from labranchgroup where branchtype = '1' and branchtype2 = '01' and branchattr = '"+fm.all('BranchAttr').value+"')"
         }
        if(fm.all('ManageCom').value !=null&&fm.all('ManageCom').value!="")
        {
        	tsql += " and a.managecom like '"+fm.all('ManageCom').value+"%'";
        }
     
       tsql+="  ) as temp where 1=1 ";
         
       fm.querySql.value = tsql;

       //定义列名
       var strSQLTitle = "select '机构编码','机构名称','团队编码','团队名称','团建类型','团建起期','薪资月','考核期','季度末架构人力','标准期营业处','标准营销人员','季度季度期保费','季度团建是否达标','团建通算是否达标'  from dual where 1=1 ";
       fm.querySqlTitle.value = strSQLTitle;
  
       //定义表名
       fm.all("Title").value="select '营业部团建考核指标明细' from dual where 1=1  ";  
  
       fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
       fm.submit();
}

// 查询按钮
function easyQueryClick()
{
	// 简单页面信息校验
	if(!verifyInput())
	{
		return false;
	}
	// 薪资计算校验
	if(!checkIndex())
	{
		return false;
	}
    initIndexInfoGrid1();
    
  	var tEndNo = trim(fm.all('IndexCalNo').value);
	var tEndDate = tEndNo.substr(0,4)+"-"+tEndNo.substr(4,2)+"-01" ;
	var getStartDate = "select date_format(date('"+tEndDate+"') - 2 month,'yyyymm') from dual where 1=1 ";
	var strQueryResult = easyQueryVer3(getStartDate, 1, 1, 1);
	if(!strQueryResult)
     {
        alert("考核期计算有误，烦请核实页面录入所属年月字段是否有误！");
        return false;
     }
    var arr = decodeEasyQueryResult(strQueryResult);
	var tStartNo = arr[0][0];    
     
  var tsql = "";
  tsql ="select aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,"
       +" (select BRANCHSTANDEXAM(hh, ee, ii, KK, jj, ll) from dual)," 
       +" (select BRANCHSTANDEXAM_T(hh, ee, ii,kk, jj,ll) from dual)"
       +" from "
       +" (select a.managecom  aa,"
       +" (select name from ldcom where comcode =a.managecom) bb,"
       +"a.branchattr cc,"
       +"a.name dd,"
       +"a.gbuildflag ee,"
       +"a.gbuildstartdate ff,"
       +"'"+tEndNo+"' gg,"
       +"(select db2inst1.MONTHS_BETWEEN(char((select startdate from lastatsegment where stattype='1' and yearmonth = int('"+tEndNo+"'))),char(a.gbuildstartdate)) + 1 from dual) hh,"
       +"(select GETBRANCHAGENT(a.agentgroup,a.gbuildflag,a.gbuildstartdate,'"+tEndNo+"') from dual) ii,"
       +"(select count(distinct agentcode) from laindexinfo  where branchtype = '1' and branchtype2 = '01' and indextype = '01' and indexcalno <= '"+tEndNo+"' and indexcalno >='"+tStartNo+"' and t67 = 99 and agentgrade like 'B%' " 
       +" and exists(select 1 from laagent where agentcode =laindexinfo.agentcode and agentcode in (select agentcode from laagent where branchtype ='1' and branchtype2 ='01' and agentgroup in (select agentgroup from labranchgroup where 1=1 and branchtype = '1' and branchtype2 = '01'and endflag = 'N' and substr(branchseries,1,12)=a.agentgroup)) )) jj,"
       +"(select count(distinct agentcode) from laindexinfo  where branchtype = '1' and branchtype2 = '01' and indextype = '01' and indexcalno <= '"+tEndNo+"' and indexcalno >='"+tStartNo+"' and t67 = 99 and agentgrade like 'A%' "
       +" and exists(select 1 from laagent where agentcode =laindexinfo.agentcode and agentcode in (select agentcode from laagent where branchtype ='1' and branchtype2 ='01' and agentgroup in (select agentgroup from labranchgroup where 1=1 and branchtype = '1' and branchtype2 = '01'and endflag = 'N' and substr(branchseries,1,12)=a.agentgroup)) )) kk,"
       +"(select GETBRANCHMONEY(a.agentgroup,a.gbuildflag,char(a.gbuildstartdate),'"+tEndNo+"') from dual) ll"
       +" from labranchgroup a" 
       +" where 1=1 "
       +"and a.branchtype = '1' and a.branchtype2 = '01' and a.gbuildflag in ('A','B') and branchlevel = '03' "
       //+ getWherePart("a.ManageCom","ManageCom")
       + getWherePart("a.branchlevel","BranchLevel");
        if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
         {
           tsql+=" and a.agentgroup = (select agentgroup from labranchgroup where branchtype = '1' and branchtype2 = '01' and branchattr = '"+fm.all('BranchAttr').value+"')"
         }
        if(fm.all('ManageCom').value !=null&&fm.all('ManageCom').value!="")
        {
           tsql += " and a.managecom like '"+fm.all('ManageCom').value+"%'";
        }
     
       tsql+=" ) as temp where 1=1 ";
      
  
  turnPage.strQueryResult  = easyQueryVer3(tsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有查询到符合条件的考核指标！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage.strQueryResult);
  
  turnPage.arrDataCacheSet = arrDataSet1;

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = IndexInfoGrid1;    
          
  //保存SQL语句
  turnPage.strQuerySql     = tsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  
}


function getQueryResult1()
{
  var arrSelected = null;
  tRow = IndexInfoGrid1.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet1 == null )
    return arrSelected;
  arrSelected = new Array();
  tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
  //设置需要返回的数组
  arrSelected[0] = arrDataSet1[tRow-1];
  return arrSelected;
}

function checkIndex()
{
   var tIndexCalNo = trim(fm.all('IndexCalNo').value);
   var tManageCom = trim(fm.all('ManageCom').value);
   
   var checkSQL = "select distinct 1 from lawagetemp where 1=1 "
        +" and branchtype = '1' and branchtype2 = '01'"
   		+" and managecom like '"+tManageCom+"%'" 
   		+" and indexcalno = '"+tIndexCalNo+"'";
   		
   var strQueryResult = easyQueryVer3(checkSQL, 1, 1, 1);
   if(!strQueryResult)
   {
      alert("机构"+tManageCom+","+tIndexCalNo+"月尚未进行团建考核计算,请计算后再进行考核指标查询操作！");
      return false;
   }

   return true;
}









