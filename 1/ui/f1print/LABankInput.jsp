<%
//程序名称：LAChannelInput.jsp
//程序功能：渠道报表
//创建日期：2007-11-20
//创建人  ：sgh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LABankInput.js"></SCRIPT>   
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LABankInit.jsp"%>     
  <title>渠道查询报表</title>
</head>      
<body  onload="initForm();initElementtype();" >    
  <form action= "./LABankReport.jsp" method=post name=fm target="fraSubmit">
  <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		渠道查询统计条件
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
   <Table class= common>
     <TR class= common> 
          <TD  class= title>管理机构</TD>
          <TD  class= input>
            <Input class="codeno" name=Level "  
            ondblclick="return showCodeList('comcode',[this,LevelName],[0,1],null,4,'char(length(trim(comcode)))',null,null);" 
            onkeyup="return showCodeListKey('comcode',[this,LevelName],[0,1],null,4,'char(length(trim(comcode)))',null,null);"><input class=codename name=LevelName readonly=true >
          </TD>
          <td class= title>险种</td>
          <TD class=input>
          <input class="codeno" name = RiskCode 
          ondblclick="return showCodeList('riskcode',[this,RiskName],[0,1],null,null,null,null,300);"
          onkeyup="return showCodeListKey('riskcode',[this,RiskName],[0,1]);"><Input name=RiskName readOnly=true class=codename></TD>
          </TR>
          <tr class=common>
		      <TD  class= title>统计起期</TD>
		      <TD  class= input>
		       <Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="统计起期|NOTNULL" elementtype=nacessary>
		      </TD>
       		<TD  class= title>统计止期</TD>
       		<TD  class= input>
       		  <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="统计止期|NOTNULL" elementtype=nacessary>
       		</TD>
       		</TR>
       		<tr class=common>
			<TD  class= title>交费方式</TD>
       		<TD  class= input>
       		 <Input  class="codeno" name=PayIntv  CodeData = "0|^0|趸交|^1|期交"
       		 ondblclick = "return showCodeListEx('PayIntv',[this,PayIntvName],[0,1]);"
       		 onkeyup = "return showCodeListKeyEx('PayIntv',[this,PayIntvName],[0,1]);"><Input class="codename" name= PayIntvName readonly=true>
       		</TD>
       		<TD class=title>交费年限</TD>
       		<td class=input>
       		<Input class="codeno" name = PayYears  CodeData = "0|^0|3年|^1|5年|^2|8年|^3|10年|^4|10年以上"
       		 ondblclick = "return showCodeListEx('PayYears',[this,PayYearsName],[0,1]);"
       		 onkeyup = "return showCodeListKeyEx('PayYears',[this,PayYearsName],[0,1]);"><Input class="codename" name= PayYearsName readonly=true>
       		 </td>       		
     </TR>    
   	</Table>  
   	</Div>
    <INPUT VALUE="打  印" class= cssbutton TYPE=button onclick="submitForm();"> 	
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html> 	
