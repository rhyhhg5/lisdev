<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>                            

<script language="JavaScript">

function initInpBox()
{ 
  try
  {
  	fm.reset();                                   
  }
  catch(ex)
  {
    alert("在FirstLifePlunkRQInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
  	
    initInpBox();
	  initPolGrid();
  }
  catch(re)
  {
    alert("FirstLifePlunkRQInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initPolGrid()
{                               
  var iArray = new Array();
      
  try {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            	//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="批单号";         		//列名
	  iArray[1][1]="140px";            	//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[2]=new Array();
	  iArray[2][0]="保险单号";       		//列名
	  iArray[2][1]="140px";            	//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="分保险单号";         	//列名
	  iArray[3][1]="140px";            	//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0; 
    
	
	  iArray[4]=new Array();
	  iArray[4][0]="金额";              //列名
	  iArray[4][1]="100px";            	//列宽
	  iArray[4][2]=200;            			//列最大值
	  iArray[4][3]=0; 									//是否允许输入,1表示允许，0表示不允许
      
      
    iArray[5]=new Array();
	  iArray[5][0]="日期";              //列名
	  iArray[5][1]="100px";            	//列宽
	  iArray[5][2]=200;            			//列最大值
	  iArray[5][3]=0; 									//是否允许输入,1表示允许，0表示不允许
	  
	  
	  iArray[6]=new Array();
	  iArray[6][0]="人数";              //列名
	  iArray[6][1]="100px";            	//列宽
	  iArray[6][2]=200;            			//列最大值
	  iArray[6][3]=0; 									//是否允许输入,1表示允许，0表示不允许
      
	  iArray[7]=new Array();
	  iArray[7][0]="投保人客户代码";    //列名
	  iArray[7][1]="100px";            	//列宽
	  iArray[7][2]=200;            			//列最大值
	  iArray[7][3]=3; 									//是否允许输入,1表示允许，0表示不允许
	  
	  
	  PolGrid = new MulLineEnter( "fmSave" , "PolGrid" ); 
	  //这些属性必须在loadMulLine前
	  PolGrid.mulLineCount = 5;   
	  PolGrid.displayTitle = 1;
	  PolGrid.canSel = 1;
    PolGrid.locked = 1;
        PolGrid.hiddenPlus = 1;
    PolGrid.hiddenSubtraction = 1;
	  PolGrid.loadMulLine(iArray);  
	
	} catch(ex) {
		alert(ex);
	}
}

</script>