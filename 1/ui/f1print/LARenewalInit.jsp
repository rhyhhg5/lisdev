<%
//程序名称：GetCredenceInit.jsp
//程序功能：
//创建日期：2002-12-20 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//添加页面控件的初始化。
	GlobalInput tG = (GlobalInput)session.getValue("GI");

	if(tG == null) {
		out.println("session has expired");
		return;
	}
	
	String strOperator = tG.Operator;
	String currentDate = PubFun.getCurrentDate();//当天日期
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
	  var managecomvalue = '<%=tG.ManageCom%>';
function initInpBox()
{ 
  try
  {     
    fm.all('BranchAttr').value = '';
    fm.all('RiskCode').value = '';
    fm.all('RiskName').value = '';
    fm.all('StartMonth').value = '';
    fm.all('ManageCom').value = '';
    fm.all('EndMonth').value = '';
    fm.all('ManageComName').value = '';
    fm.all('AgentCode').value = '';
    fm.all('currDate').value = '<%=currentDate%>';
 //   alert(fm.all('currentDate').value);
   	}
  catch(ex)
  {
    alert("在GetCredenceInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
{
  }
  catch(ex)
  {
    alert("在GetCredenceInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();  
  // initLACrossGrid();
  }
  catch(re)
  {
    alert("GetCredenceInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var LACrossGrid;
function initLACrossGrid() {                               
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="合同号码";         		//列名
    iArray[1][1]="100px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="印刷号码";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="投保人姓名";         		//列名
    iArray[3][1]="100px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="地址";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;    
    
    iArray[5]=new Array();
    iArray[5][0]="联系方式";         		//列名
    iArray[5][1]="80px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;    
    
    iArray[6]=new Array();
    iArray[6][0]="险种编码";         		//列名
    iArray[6][1]="80px";         		//宽度
    iArray[6][3]=80;         		//最大长度
    iArray[6][4]=0;    
    

    iArray[7]=new Array();
    iArray[7][0]="缴费年期";         		//列名
    iArray[7][1]="80px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;    
    
    iArray[8]=new Array();
    iArray[8][0]="缴费保费";         		//列名
    iArray[8][1]="80px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0; 
    
    iArray[9]=new Array();
    iArray[9][0]="代理人编码";         		//列名
    iArray[9][1]="60px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0; 
    
    iArray[10]=new Array();
    iArray[10][0]="代理人姓名";         		//列名
    iArray[10][1]="60px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0; 
      
    iArray[11]=new Array();
    iArray[11][0]="代理人职级";         		//列名
    iArray[11][1]="60px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0; 
    
    iArray[12]=new Array();
    iArray[12][0]="销售机构代码";         		//列名
    iArray[12][1]="80px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0; 
     
    iArray[13]=new Array();
    iArray[13][0]="销售机构名称";         		//列名
    iArray[13][1]="120px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;    
    
    iArray[14]=new Array();
    iArray[14][0]="承保日期";         		//列名
    iArray[14][1]="80px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=0; 
    
    iArray[15]=new Array();
    iArray[15][0]="是否已经续期";         		//列名
    iArray[15][1]="80px";         		//宽度
    iArray[15][3]=100;         		//最大长度
    iArray[15][4]=0; 
    
    LACrossGrid = new MulLineEnter( "fm" , "LACrossGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACrossGrid.mulLineCount = 0;   
    LACrossGrid.displayTitle = 1;
    LACrossGrid.hiddenPlus = 1;
    LACrossGrid.hiddenSubtraction = 1;
    LACrossGrid.canSel = 0;
    LACrossGrid.canChk = 0;
  // LACrossGrid.selBoxEventFuncName = "showOne";
  
    LACrossGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACrossGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>