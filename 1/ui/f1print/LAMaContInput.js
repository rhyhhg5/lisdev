//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，打印按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
   //校验时间间隔不超过6个月 
   var mStartDate= fm.all('StartDate').value;
   var mEndDate= fm.all('EndDate').value;
    
   var months=dateDiff(mStartDate,mEndDate,'M');
   if(months>6)
   {
      alert("查询的时间间隔大于6个月，请重新输入日期！");  
      fm.StartDate.value="";
      fm.EndDate.value="";
      return  false;
    }	
//   if( !getname())
//   {
// 	}
   var strSql="select o,p,a,b,decimal(decimal(D+D1,12,2)/10000,14,6) c,C+C1 d, "
            +"case when C+C1=0  then 0 else decimal(round(decimal(D+D1,14,2)/(C+C1),2),14,2) end e,decimal(decimal(G1+G2)/2,12,1) gg, "
            +" case when G1+G2=0  then 0 else decimal(round(decimal(D+D1,14,2)*2/(G1+G2),2),14,2) end H, "
            +" E+E1 dd,F+F1 ff "
            +" from  "
            +" ( select y.agentgroup o,y.branchattr p,y.name a,y.branchmanagername b,  y.branchseries  ord, y.managecom  mmm, "
            +" (select count(distinct c.contno) from lccont c,labranchgroup aa where  c.salechnl='01' and c.managecom like '"+fm.ManageCom.value+"%' and c.ContType='1' and c.agentgroup  "
            +" in (select a.agentgroup from  labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) or aa.agentgroup =substr(a.branchseries,14,12)or aa.agentgroup =substr(a.branchseries,27,12))) "
            +" and aa.agentgroup=y.agentgroup "
            +" and c.makedate<='"+fm.EndDate.value+"' and c.makedate>='"+fm.StartDate.value+"' )  C, "
            +" (select count(distinct c.contno) from lbcont c  ,labranchgroup aa where  c.salechnl='01' and c.managecom like '"+fm.ManageCom.value+"%' and c.ContType='1' and c.agentgroup " 
            +"  in (select a.agentgroup from  labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) or aa.agentgroup =substr(a.branchseries,14,12)or aa.agentgroup =substr(a.branchseries,27,12))) " 
            +"  and aa.agentgroup=y.agentgroup "                      
            +"  and c.makedate<='"+fm.EndDate.value+"' and c.makedate>='"+fm.StartDate.value+"')  C1, "
            +" (select value(sum(c.prem),0)   from lccont c  ,labranchgroup aa where  c.salechnl='01' and c.managecom like '"+fm.ManageCom.value+"%' and c.ContType='1' and c.agentgroup " 
            +"  in (select a.agentgroup from  labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) or aa.agentgroup =substr(a.branchseries,14,12)or aa.agentgroup =substr(a.branchseries,27,12)) ) " 
            +"  and aa.agentgroup=y.agentgroup "
            +"  and c.makedate<='"+fm.EndDate.value+"' and c.makedate>='"+fm.StartDate.value+"' ) D, "     
            +"  (select value(sum(c.prem),0)  from lbcont c ,labranchgroup aa where  c.salechnl='01' and c.managecom like '"+fm.ManageCom.value+"%' and c.ContType='1' and c.agentgroup " 
            +"  in (select a.agentgroup from  labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) "
            +"  or aa.agentgroup =substr(a.branchseries,14,12)or aa.agentgroup =substr(a.branchseries,27,12)) )  "
            +"  and aa.agentgroup=y.agentgroup "
            +"  and c.makedate<='"+fm.EndDate.value+"' and c.makedate>='"+fm.StartDate.value+"' ) D1,  " 
            +"  (select count(distinct b.agentcode) from laagent b, labranchgroup aa where b.branchtype='1' and b.agentgroup " 
            +"   in (select a.agentgroup from "  
            +"   labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) or aa.agentgroup =substr(a.branchseries,14,12) or " 
            +"   aa.agentgroup =substr(a.branchseries,27,12)) ) "
            +"   and aa.agentgroup=y.agentgroup "
            +"   and b.employdate<='"+fm.StartDate.value+"' "
            +"   and (b.outworkdate>'"+fm.StartDate.value+"' or b.outworkdate is null )) G1, "
            +"  (select count(distinct agentcode) from laagent b , labranchgroup aa where b.branchtype='1'and b.agentgroup " 
            +"   in (select a.agentgroup from  labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) or aa.agentgroup =substr(a.branchseries,14,12)or aa.agentgroup =substr(a.branchseries,27,12)) ) "
            +"   and aa.agentgroup=y.agentgroup "
            +"   and b.employdate<='"+fm.EndDate.value+"' "
            +"   and (b.outworkdate>'"+fm.EndDate.value+"' or b.outworkdate is null)) G2, "
            +"   (select count(c.InsuredNo) from lccont c  ,labranchgroup aa where  c.salechnl='01' and c.managecom like '"+fm.ManageCom.value+"%' and c.ContType='1' and c.agentgroup " 
            +"   in (select a.agentgroup from  labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) or aa.agentgroup =substr(a.branchseries,14,12)or aa.agentgroup =substr(a.branchseries,27,12)) ) " 
            +"   and aa.agentgroup=y.agentgroup "
            +"   and c.makedate<='"+fm.EndDate.value+"' and c.makedate>='"+fm.StartDate.value+"'  )  E, "
            +"  (select count(c.InsuredNo) from lbcont c  ,labranchgroup aa where  c.salechnl='01' and c.managecom like '"+fm.ManageCom.value+"%' and c.ContType='1' and c.agentgroup " 
            +"  in (select a.agentgroup from  labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) or aa.agentgroup =substr(a.branchseries,14,12)or aa.agentgroup =substr(a.branchseries,27,12)) ) " 
            +"  and aa.agentgroup=y.agentgroup "                               
            +"  and c.makedate<='"+fm.EndDate.value+"' and c.makedate>='"+fm.StartDate.value+"'      )  E1, "
            +"  (select count(distinct c.InsuredNo) from lccont c  ,labranchgroup aa where  c.salechnl='01' and c.managecom like '"+fm.ManageCom.value+"%' and c.ContType='1' and c.agentgroup " 
            +"  in (select a.agentgroup from  labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) or aa.agentgroup =substr(a.branchseries,14,12)or aa.agentgroup =substr(a.branchseries,27,12)) ) " 
            +"  and aa.agentgroup=y.agentgroup "
            +"  and c.makedate<='"+fm.EndDate.value+"' and c.makedate>='"+fm.StartDate.value+"'  )  F, "
            +"  (select count(distinct c.InsuredNo) from lbcont c  ,labranchgroup aa where  c.salechnl='01' and c.managecom like '"+fm.ManageCom.value+"%' and c.ContType='1' and c.agentgroup " 
            +"  in (select a.agentgroup from  labranchgroup a where (aa.agentgroup =substr(a.branchseries,1,12) or aa.agentgroup =substr(a.branchseries,14,12)or aa.agentgroup =substr(a.branchseries,27,12)) ) " 
            +"  and aa.agentgroup=y.agentgroup "                               
            +"  and c.makedate<='"+fm.EndDate.value+"' and c.makedate>='"+fm.StartDate.value+"'      )  F1 "
            +"  from  labranchgroup y where  y.managecom like '"+fm.ManageCom.value+"%' and founddate<='"+fm.EndDate.value+"' and (enddate>'"+fm.StartDate.value+"' or enddate is null) and y.branchtype='1' "
            +"  and y.branchtype2='01' ) as x order by mmm,ord  with ur ";
            fm.querySql.value = strSql;
            //alert(strSql);
//  var i = 0;
//  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	        fm.action = "LAMaCont.jsp";
	        fm.submit();
}
function getname()
{
return;
	
	}


function download()
{
	if (verifyInput() == false)
    return false;
    Sql();
//  var i = 0;
//  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	
	fm.submit();
	//showInfo.close();
}

