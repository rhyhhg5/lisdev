
<jsp:directive.page
	import="com.sinosoft.lis.certify.SysOperatorNoticeBL" />
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：按险种打印操作员日结
	//程序功能：
	//创建日期：2013-11-19
	//创建人  ：y
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
	System.out.println("start NewFinDayCheckNExcelSave......");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	//输出参数
	CError cError = new CError();
	//后面要执行的动作：添加，修改，删除

	boolean operFlag = true;
	String Content = "";
	String tOutXmlPath = "";
	String strOperation = "";//用来判断日结单类型
	String mDay[] = new String[2];     //日结单打印日期
	strOperation = request.getParameter("fmtransact");
	mDay[0] = request.getParameter("StartDay");
	mDay[1] = request.getParameter("EndDay");

    System.out.println("打印类型：" + strOperation+";打印起始日期：" + mDay[0]+";打印终止日期：" + mDay[1]);
	System.out.println("操作员："+tG.Operator+";登录机构："+tG.ManageCom);

	VData tVData = new VData();	
    tVData.addElement(mDay);
	tVData.addElement(tG);
	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
	String tDate=dateFormat.format(new Date());
	String tTime=timeFormat.format(new Date());
	String fileName=tDate.replace("-","")+tTime.replace(":","")+"-"+strOperation+"-"+tG.Operator+".xls";
	System.out.println("文件名称："+fileName);
	tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
	System.out.println("文件路径:" + tOutXmlPath);
	tVData.addElement(tOutXmlPath);
	
	//strOperation='SS'-->X1-实收日结单/strOperation='SF'-->X2-实付日结单/strOperation='SXF'-->X9-手续费日结单
	NewFinDayCheckNExcelUI tNewFinDayCheckNExcelUI = new NewFinDayCheckNExcelUI();
	if (!tNewFinDayCheckNExcelUI.submitData(tVData, strOperation)) {
		operFlag = false;
		Content = "报表下载失败，原因是:" + tNewFinDayCheckNExcelUI.mErrors.getFirstError();
	}
	
	System.out.println("返回NewFinDayCheckNExcelSave页面，开始打印"+strOperation+"文件");
	String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
    File file = new File(tOutXmlPath);
    
    response.reset();
    response.setContentType("application/octet-stream"); 
    response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
    response.setContentLength((int) file.length());

    byte[] buffer = new byte[10000];
    BufferedOutputStream output = null;
    BufferedInputStream input = null;    
    //写缓冲区
    try 
    {
        output = new BufferedOutputStream(response.getOutputStream());
        input = new BufferedInputStream(new FileInputStream(file));
  
    int len = 0;
    while((len = input.read(buffer)) >0)
    {
        output.write(buffer,0,len);
    }
    input.close();
    output.close();
    }
    catch (Exception e) 
    {
      e.printStackTrace();
     } // maybe user cancelled download
    finally 
    {
        if (input != null) input.close();
        if (output != null) output.close();
        file.delete();
    }
	if (operFlag)
	{
	Content = "打印报表成功！";
	}
%>
<html>
   <script language="javascript">  
    alert('<%=Content %>');
    top.close();
   </script>
</html>