/*
name : PrintDrawNoticeInput.js
function :打印生存领取通知书清单
Creator :刘岩松
Date :2003-11-12
*/

var PringFlag;
var showInfo;
var mDebug="0";
var FlagDel;
var turnPage = new turnPageClass();

function displayQueryResult(strResult)
{
  strResult = Conversion(strResult);
  var filterArray = new Array(0,1,9,10,23,6,29);
  turnPage.strQueryResult  = strResult;
  var tArr                 = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  turnPage.useSimulation   = 1;
  turnPage.pageDisplayGrid = PolGrid;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr=="Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}
function test()
{
  alert("test");
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//根据起始日期打印出生存领取的通知书清单。
function PrintNoticeBill()
{
  initPolGrid();
  if((fm.StartDate.value=="")||(fm.EndDate.value=="")||(fm.StartDate.value=="null")||(fm.EndDate.value=="null"))
  {
    alert("请输入起始日期和结束日期!!!");
    return;
  }
  if((fm.ManageCom.value=="")||(fm.ManageCom.value=="null"))
  {
    alert("请选择管理机构!!");
    return ;
  }
  else
  {
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./PNoticeBillSave.jsp";
    fm.target="f1print";
    fm.target = "_blank";
    fm.submit(); //提交
  }
}

// 查询按钮
function DrawNoticeQuery()
{
	initPolGrid();
  //判断起始日期是否为空
  if((fm.StartDate.value=="null")||(fm.StartDate.value==""))
  {
    alert("请您录入开始日期");
    return;
  }
  if((fm.EndDate.value=="null")||(fm.EndDate.value==""))
  {
    alert("请您录入结束日期");
    return;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  showSubmitFrame(mDebug);
  fm.action = "./QueryDrawNoticeSave.jsp";
  fm.submit(); //提交
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

function SinglePrint()
{
  fm.PrintFlag.value = "0";
  var rowNum=PolGrid. mulLineCount ;
  var checkNum = 0;
  var t_count;
  for(var i=0;i<10;i++)
  {
    if(PolGrid.getChkNo(i))
    {
      checkNum ++;
      t_count = i;
    }
  }
  if(checkNum==0)
  {
    alert("请您选择一条要打印的记录");
    return;
  }
  if(checkNum>1)
  {
    alert("选择的记录大于一条，请您从新选择，且选择的记录只能为一条");
    return;
  }
  var i = 0;
//  var showStr="正在打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  var PolNo = PolGrid. getRowColData(t_count,2);
  fm.PolNo.value = PolNo;
  showSubmitFrame(mDebug);
  fm.action = "./PrintDrawNoticeSave.jsp";
  fm.target="f1print";
  fm.submit(); //提交
}

function BatchPrint()
{
  fm.PrintFlag.value = "1";
  var rowNum=PolGrid. mulLineCount ;
  var checkNum = 0;
  for(var i=0;i<10;i++)
  {
    if(PolGrid.getChkNo(i))
    {
      checkNum ++;
    }
  }
  if(checkNum==0)
  {
    alert("请您选择要进行批量打印的记录");
    return;
  }
  if(checkNum==1)
  {
    alert("请您选择多于一条的记录进行打印");
    return;
  }
  var i = 0;
  var showStr="正在打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  showSubmitFrame(mDebug);
  fm.action = "./PrintDrawNoticeSave.jsp";
  fm.target = "fraSubmit";
  fm.submit(); //提交
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}
