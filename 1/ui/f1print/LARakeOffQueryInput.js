//该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

/***************************************
 *提交，打印按钮相应事件
 ***************************************
 */
function submitForm()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  //取得画面提供的检索条件
  var stWhere = getQueryParam();
  fm.all('pmWhere').value = stWhere;
  
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

/***************************************
 *提交，下载按钮相应事件
 ***************************************
 */
function getQueryParam()
{
  var rtParam = " ";
  
  //取得画面上的管理机构号，去掉两端空格
  var tManageCom = trim(document.fm.ManageCom.value);
  if(tManageCom.length > 0)
  {
    rtParam += " AND A.ManageCom like '" + tManageCom +"%'";
  }

  //取得画面上的销售机构号，去掉两端空格
  var tBranchAttr = trim(document.fm.BranchAttr.value);
  if(tBranchAttr.length > 0)
  {
    rtParam += " AND B.BranchAttr like '" + tBranchAttr + "%'";
  }

  //取得画面上的营销员编码，去掉两端空格
  var tAgentCode = trim(document.fm.AgentCode.value);
  if(tAgentCode.length > 0)
  {
    rtParam += " AND A.AgentCode like '" + tAgentCode + "%'";
  }
  
  //取得画面上佣金年月，去掉两端空格
  var tRakeOffYM = trim(document.fm.RakeOffDate.value);
  if(tRakeOffYM.length > 0)
  {
    rtParam += " AND D.IndexCalNo like '" + tRakeOffYM + "%'";
  }

  return rtParam;
}

/***************************************
 *提交，下载按钮相应事件
 ***************************************
 */
function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

//测试用
function aa()
{
  var a = document.fm.AgentCode.value;
  alert("营业员编码：" + a + "  长度为：" + a.length);
  var b = Trim(a);
  alert("去掉空格后 营业员编码：" + b + "  长度为：" + b.length);
}

/***************************************
 *去掉字符串两端半角空格，返回两端无空格的字符串
 ***************************************
 */
function Trim(str)
{
  alert("'"+str+"'");
  return str.replace(/^\s*|\s*$/g,"");
}