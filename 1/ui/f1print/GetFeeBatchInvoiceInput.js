//               该文件中包含客户端需要处理的函数和事件
var arrDataSet;
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var mIncomeNo = "";
var mPayNo = "";
var multLineFlag = true;

window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try 
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function find(){
  if(fm.IncomeType.value==''){
  	alert('请选择业务号码类型！');
  	return false;
  }
	if(fm.all("IncomeType").value =="1"  ){
		if(find1()==false){
			return false;
		}
	}else if(fm.all("IncomeType").value =="2"){
		if(find2()==false){
			return false;
		}
	}else if(fm.all("IncomeType").value =="3"){
		if(find3()==false){
			return false;
		}
	}else if(fm.all("IncomeType").value =="4"){
		if(find4()==false){
			return false;
		}
	}else{
		
	}
}
//理赔案件
function find1(){
  if(!verifyInput()) 
  {
  	return false;
  }
  if(fm.IncomeType.value==''){
  	alert('请选择业务号码类型！');
  	return false;
  }
    if(fm.EnterAccDate.value==''&& fm.EnterAccDateEnd.value==''&&fm.PrtNo.value==''&&fm.IncomeNo.value==''){
  	alert('实付号，业务号和日期不能同时为空！');
  	return false;
  }
  if(fm.InvoiceState.value=='1' || fm.InvoiceState.value=='2' || fm.InvoiceState.value=='3'|| fm.InvoiceState.value=='4'){
  }else{
  	alert('请选择发票状态');
  	return false;
  }
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
			
	strSQL = "select (case grpcontno when '00000000000000000000' then contno else grpcontno end ), "
  			 +" a.drawer,a.actugetno,a.otherno,'','1',sum(b.pay),a.enteraccdate,a.confdate,a.operator,a.managecom,a.agentcode,a.agentgroup,b.salechnl"
			 +" from ljaget a,ljagetclaim b where a.actugetno=b.actugetno and b.pay<>0 and a.ConfDate is not null "	
	strSQL = strSQL + " and exists (select '1' from lccont where contno=b.contno  or grpcontno=b.grpcontno  )";
		
	strSQL = strSQL + getWherePart( 'a.actugetno', 'PrtNo');
	strSQL = strSQL	 + getWherePart( 'a.otherno','IncomeNo' )
		if(fm.InvoiceState.value=='1'){
		strSQL = strSQL	 +" and (select count(1) from LOPRTManager2 b where otherno=a.actugetno and code in('37','38') and stateflag='1') = 0";
	}if(fm.InvoiceState.value=='2'){
		strSQL = strSQL	 +" and exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='37' and stateflag='1' )";
	}else if(fm.InvoiceState.value=='4'){
		strSQL = strSQL	 +" and exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='38' and stateflag='1' ) and not exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='37' and stateflag='1' )";
	}
    if(fm.EnterAccDate.value!=""){
			strSQL+=" and a.ConfDate>='"+fm.EnterAccDate.value+"' ";
		}
		if(fm.EnterAccDateEnd.value!=""){
			strSQL+=" and a.ConfDate<='"+fm.EnterAccDateEnd.value+"' ";
		}
	if (fm.MngCom.value == null || fm.MngCom.value == "" )
	{	
	}	
	else
	{
		strSQL = strSQL + getWherePart( 'a.ManageCom','MngCom','like' );
	}
	strSQL = strSQL + "group by (case grpcontno when '00000000000000000000' then contno else grpcontno end ), "
  			 +" a.drawer,a.actugetno,a.otherno,'','1',a.enteraccdate,a.confdate,a.operator,a.managecom,a.agentcode,a.agentgroup,b.salechnl order by  a.actugetno";
	
	fm.SQLstr.value = strSQL;
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL); 
    
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
    //alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  turnPage.pageLineNum=20;
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function find2(){

  if(!verifyInput()) 
  {
  	return false;
  }
  if(fm.IncomeType.value==''){
  	alert('请选择业务号码类型！');
  	return false;
  }
    if(fm.EnterAccDate.value==''&& fm.EnterAccDateEnd.value==''&&fm.PrtNo.value==''&&fm.IncomeNo.value==''){
  	alert('实付号，业务号和日期不能同时为空！');
  	return false;
  }
  if(fm.InvoiceState.value=='1' || fm.InvoiceState.value=='2' || fm.InvoiceState.value=='3'|| fm.InvoiceState.value=='4'){
  }else{
  	alert('请选择发票状态');
  	return false;
  }
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
			
			
	strSQL = "select  b.contno,a.drawer,a.actugetno,'',a.otherno,'2',sum(-b.getmoney),a.enteraccdate, "
  			 +" a.confdate,a.operator,a.managecom,a.agentcode,a.agentgroup,''"
			 +" from ljaget a,ljagetendorse b where a.actugetno=b.actugetno and a.othernotype='10' and b.getmoney<>0 and a.ConfDate is not null "
					
	strSQL = strSQL + getWherePart( 'a.actugetno', 'PrtNo');
	strSQL = strSQL	 + getWherePart( 'a.otherno','IncomeNo' )
		if(fm.InvoiceState.value=='1'){
		strSQL = strSQL	 +" and (select count(1) from LOPRTManager2 b where otherno=a.actugetno and code in('37','38') and stateflag='1') = 0";
	}if(fm.InvoiceState.value=='2'){
		strSQL = strSQL	 +" and exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='37' and stateflag='1' )";
	}else if(fm.InvoiceState.value=='4'){
		strSQL = strSQL	 +" and exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='38' and stateflag='1' ) and not exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='37' and stateflag='1' )";
	}
    if(fm.EnterAccDate.value!=""){
			strSQL+=" and a.ConfDate>='"+fm.EnterAccDate.value+"' ";
		}
		if(fm.EnterAccDateEnd.value!=""){
			strSQL+=" and a.ConfDate<='"+fm.EnterAccDateEnd.value+"' ";
		}
	if (fm.MngCom.value == null || fm.MngCom.value == "" )
	{	
	}	
	else
	{
		strSQL = strSQL + getWherePart( 'a.ManageCom','MngCom','like' );
	}
	strSQL = strSQL + "group by b.contno,a.drawer,a.actugetno,'',a.otherno,'2',a.enteraccdate, "
  			 +" a.confdate,a.operator,a.managecom,a.agentcode,a.agentgroup order by  a.actugetno ";
	
	fm.SQLstr.value = strSQL;
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL); 
    
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
    alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }
  

  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  turnPage.pageLineNum=20;
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

//团单
function find3(){
if(!verifyInput()) 
  {
  	return false;
  }
  if(fm.IncomeType.value==''){
  	alert('请选择业务号码类型！');
  	return false;
  }
    if(fm.EnterAccDate.value==''&& fm.EnterAccDateEnd.value==''&&fm.PrtNo.value==''&&fm.IncomeNo.value==''){
  	alert('实付号，业务号和日期不能同时为空！');
  	return false;
  }
  if(fm.InvoiceState.value=='1' || fm.InvoiceState.value=='2' || fm.InvoiceState.value=='3'|| fm.InvoiceState.value=='4'){
  }else{
  	alert('请选择发票状态');
  	return false;
  }
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
			
			
	strSQL = "select  b.grpcontno,a.drawer,a.actugetno,'',a.otherno,'2',sum(-b.getmoney),a.enteraccdate, "
  			 +" a.confdate,a.operator,a.managecom,a.agentcode,a.agentgroup,''"
			 +" from ljaget a,ljagetendorse b where a.actugetno=b.actugetno and a.othernotype='3' and b.getmoney<>0 and a.ConfDate is not null  "
					
	strSQL = strSQL + getWherePart( 'a.actugetno', 'PrtNo');
	strSQL = strSQL	 + getWherePart( 'a.otherno','IncomeNo' )
		if(fm.InvoiceState.value=='1'){
		strSQL = strSQL	 +" and (select count(1) from LOPRTManager2 b where otherno=a.actugetno and code in('37','38') and stateflag='1') = 0";
	}if(fm.InvoiceState.value=='2'){
		strSQL = strSQL	 +" and exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='37' and stateflag='1' )";
	}else if(fm.InvoiceState.value=='4'){
		strSQL = strSQL	 +" and exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='38' and stateflag='1' ) and not exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='37' and stateflag='1' )";
	}
    if(fm.EnterAccDate.value!=""){
			strSQL+=" and a.ConfDate>='"+fm.EnterAccDate.value+"' ";
		}
		if(fm.EnterAccDateEnd.value!=""){
			strSQL+=" and a.ConfDate<='"+fm.EnterAccDateEnd.value+"' ";
		}
	if (fm.MngCom.value == null || fm.MngCom.value == "" )
	{	
	}	
	else
	{
		strSQL = strSQL + getWherePart( 'a.ManageCom','MngCom','like' );
	}
	strSQL = strSQL + "group by select  b.grpcontno,a.drawer,a.actugetno,'',a.otherno,'2',a.enteraccdate, "
  			 +" a.confdate,a.operator,a.managecom,a.agentcode,a.agentgroup  order by  a.actugetno ";
	
	fm.SQLstr.value = strSQL;
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL); 
    
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
    alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }
  

  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  turnPage.pageLineNum=20;
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
 
}

function find4(){

  if(!verifyInput()) 
  {
  	return false;
  }
  if(fm.IncomeType.value==''){
  	alert('请选择业务号码类型！');
  	return false;
  }
    if(fm.EnterAccDate.value==''&& fm.EnterAccDateEnd.value==''&&fm.PrtNo.value==''&&fm.IncomeNo.value==''){
  	alert('实付号，业务号和日期不能同时为空！');
  	return false;
  }
  if(fm.InvoiceState.value=='1' || fm.InvoiceState.value=='2' || fm.InvoiceState.value=='3'|| fm.InvoiceState.value=='4'){
  }else{
  	alert('请选择发票状态');
  	return false;
  }
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
			
			
	strSQL = "select  (case a.othernotype when '20' then b.contno else b.grpcontno end ),a.drawer,a.actugetno,'',a.otherno,'4',sum(b.getmoney),a.enteraccdate, "
  			 +" a.confdate,a.operator,a.managecom,a.agentcode,a.agentgroup,''"
			 +" from ljaget a,ljsgetdraw b where a.actugetno=b.getnoticeno and a.othernotype in('20','21') and b.getmoney<>0 and a.ConfDate is not null "
					
	strSQL = strSQL + getWherePart( 'a.actugetno', 'PrtNo');
	if(fm.IncomeNo.value!=""){
		strSQL = strSQL	 +" and (b.contno='"+fm.IncomeNo.value+"' or b.grpcontno='"+fm.IncomeNo.value+"')";
	}if(fm.InvoiceState.value=='1'){
		strSQL = strSQL	 +" and (select count(1) from LOPRTManager2 b where otherno=a.actugetno and code in('37','38') and stateflag='1') = 0";
	}if(fm.InvoiceState.value=='2'){
		strSQL = strSQL	 +" and exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='37' and stateflag='1' )";
	}else if(fm.InvoiceState.value=='4'){
		strSQL = strSQL	 +" and exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='38' and stateflag='1' ) and not exists (select '1' from LOPRTManager2 b where otherno=a.actugetno and code='37' and stateflag='1' )";
	}
    if(fm.EnterAccDate.value!=""){
			strSQL+=" and a.ConfDate>='"+fm.EnterAccDate.value+"' ";
		}
		if(fm.EnterAccDateEnd.value!=""){
			strSQL+=" and a.ConfDate<='"+fm.EnterAccDateEnd.value+"' ";
		}
	if (fm.MngCom.value == null || fm.MngCom.value == "" )
	{	
	}	
	else
	{
		strSQL = strSQL + getWherePart( 'a.ManageCom','MngCom','like' );
	}
	strSQL = strSQL + "group by (case a.othernotype when '20' then b.contno else b.grpcontno end ),a.drawer,a.actugetno,'',a.otherno,'4',a.enteraccdate, "
  			 +" a.confdate,a.operator,a.managecom,a.agentcode,a.agentgroup order by  a.actugetno ";
	
	fm.SQLstr.value = strSQL;
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL); 
    
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
    alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }
  

  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  turnPage.pageLineNum=20;
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

//发票打印
function PPrint()
{
	var checkFlag=0;
	for(var i=0;i<PolGrid.mulLineCount;i++){
		if(PolGrid.getChkNo(i)==true){
			checkFlag++;
		}
	}
	if(checkFlag==0){
		alert('至少选择一项');
		return false;
	}

	var tIncomeNo = "";
	var tempIncomeNo = "";	
	var arrReturn = new Array();

  if(!checkFPDM())
  {
    return;
  }
  	
  fm.action="GetFeeBatchInvoiceF1PSave.jsp";
	fm.target="fraSubmit";
	fm.fmtransact.value="batch";
	submitForm();
}


function submitForm()
{
  if(verifyInput()) 
  {
  	if(fm.StartNo.value==""){
  		alert("未在单证管理系统中对业管部发票打印用户发放发票号段动作,不能进行发票打印!");
  		return false;
  	}else{
  	  if (!fm.CertifyFlag.checked) {
  	  	fm.StaNo.value = fm.StartNo.value;
  	 	fm.EndNo.value = fm.StartNo.value;
  	  }
  		fm.submit();
  	} //提交
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, Content)
{	
	 if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else{
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
  fm.StaNo.value = "";
  fm.EndNo.value = "";
	find();  
	StartList();
}

/***************************************************************
*		程序功能:打印后更新主页面单证序号
*
****************************************************************/
function freshNum() 
{	
		var strSQL = "";
		if (fm.MngCom.value.length<=4)
		{
			strSQL = "select min(StartNo) from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='A"+fm.MngCom.value+"' and StateFlag in ('0','7')";
		}
		else
		{
			strSQL = "select min(StartNo) from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='B"+fm.Operator.value+"' and StateFlag in ('0','7')";
		}
	
		var arrResult = easyExecSql(strSQL);
		//alert(arrResult[0]);
		if (arrResult==null||arrResult=="")
		{
			alert("没有可用发票！"); 
		} 
		else 
		{
			fm.StartNo.value=arrResult; 
		}
}


//不打印发票
function NPPrint()
{
	var checkFlag=0;
	for(var i=0;i<PolGrid.mulLineCount;i++){
		if(PolGrid.getChkNo(i)==true){
			checkFlag++;
		}
	}
	if(checkFlag==0){
		alert('至少选择一项');
		return false;
	}
	//if(checkStartNo(checkFlag)==false){
		//return false;
	//}
	var tsql="";
	var tIncomeNo = "";
	var tempIncomeNo = "";	
	var arrReturn = new Array();
  if (PolGrid.mulLineCount==0){
	 	   alert("不存在满足条件的发票信息！");
	 	   return;
	 }
	     for(var i=0;i< PolGrid.mulLineCount;i++ ){
     	if(PolGrid.getChkNo(i,PolGrid)){
     	    tsql="select * from loprtmanager2 where otherno='"+PolGrid.getRowColData(i ,3)+"' and code='36' with ur";
     	     var arrResult = easyExecSql(tsql);
             if(arrResult!= null){ 
               alert("该条记录已经设置了不打印标记，请不要重复设置");
             return ;
             }
     	}
 	 }
		fm.intRecordNum.value = "";
		fm.intRecordNum.value	= PolGrid.mulLineCount;	   	
		if (fm.IncomeType.value == "15")
		{
		 for(var i=0;i< PolGrid.mulLineCount;i++ ){			 		 	 	
		 	 if(PolGrid.getChkNo(i,PolGrid)){  		
		  		if (tIncomeNo == ""){
		  		 	 tempIncomeNo = PolGrid.getRowColData(i ,2);
		  		 	
		  		 }
		  		 tIncomeNo = PolGrid.getRowColData(i ,2);		  		 		   			
		  		 if (tempIncomeNo != tIncomeNo)
		  		 {  	  	
				 	  	 alert("请选择同一个结算单！");
				 	  	 return ;
				 	 }		 	
  	   }	  
		 }
		}
		
  if(!checkFPDM())
  {
    return;
  }
	
  fm.action="FeeBatchInvoiceF1PSave.jsp";
	fm.target="fraSubmit";
	fm.fmtransact.value="notbatch";
	fm.submit();
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组 
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function showAppntInfo() {
  var strSql = "";
  if (fm.IncomeType.value == "1" )
  {
  	strSql = "select distinct GrpName,RiskName,a.riskcode from lcgrppol a,lmriskapp b where GrpContNo = '" 
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2) 
             + "' and a.riskcode=b.riskcode order by a.riskcode";
  }
  if (fm.IncomeType.value == "2" )
  {
    strSql = "select distinct c.AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
             + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
             + "when '530301' then (select riskwrapplanname from ldcode1 "
             + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode from lcpol a,lmriskapp b,lccont c where c.ContNo = a.ContNo " 
             + "and a.ContNo = '"+PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2) 
             + "' and a.riskcode=b.riskcode order by a.riskcode";     
  }        
  if (fm.IncomeType.value == "3" )
  {
  	if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3) == "3")
  	{
    strSql = "select distinct b.GrpName,RiskName,b.riskcode from LJAGetEndorse a, lcgrppol b, lmriskapp c where ActuGetNo = '" 
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1) 
             + "' and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode order by b.riskcode";     
    }
    if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3) == "13")
    {       
    	strSql = "Select distinct b.GrpName,RiskName,b.riskcode from ljagetendorse a,lcgrppol b,lmriskapp c "
             +"where actugetno in ( Select Btactuno from ljaedorbaldetail "
             +"where actuno in ( Select getnoticeno from LJAPay where payno='"
             +PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1)+"' )) "
             +"and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode order by b.riskcode "
    }           
  }             
  if (fm.IncomeType.value == "10" )
  {             
    strSql = "select distinct AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
             + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
             + "when '530301' then (select riskwrapplanname from ldcode1 "
             + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode from LJAGetEndorse a, lcpol b, lmriskapp c where ActuGetNo = '"
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1) 
             + "' and a.contno=b.contno and b.riskcode=c.riskcode order by b.riskcode";          
  }                
 //alert(strSql);      
  var arrResult = easyExecSql(strSql);
  if(arrResult== null){ return ;}
  fm.all("AppntName").value = arrResult[0][0];
  if (arrResult.length>1)
  {
      fm.all("RiskName").value = arrResult[0][1]+"等";
  }
  else
  	{
  		fm.all("RiskName").value = arrResult[0][1];
  	}
  
  
}
//显示定额单证的信息
function showDAppntInfo() {
	
  
  var strPayNo = "";
  var strIncomeNo = ""; 
  mIncomeNo = "";
  var  ContStr = "";
  for (var i=0;i< PolGrid.mulLineCount;i++){
  	if(PolGrid.getChkNo(i,PolGrid)){
  		
  		if (mIncomeNo == ""){
  		 	 strIncomeNo = PolGrid.getRowColData(i ,2);		
  		 }
  		 mIncomeNo = PolGrid.getRowColData(i ,2); 		
   		 strPayNo = PolGrid.getRowColData(i ,1);
  		 if (mIncomeNo != strIncomeNo)
  		 {  
  		 	   multLineFlag = false;	  	
		 	  	 alert("请选择同一个结算单进行打印！");
		 	  	 return ;
		 	 }		 	
  	}
  } 
  nameGet();
  fm.PayNo.value = strPayNo;
  fm.IncNo.value = mIncomeNo;
}

/*********************************************************************
 *  取得险种名称
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function nameGet() {
	var strSql = "select distinct '',b.RiskName,b.riskcode from lmcardrisk a,lmriskapp b,LZCardPay c,LMCertifyDes d where c.PayNo = '" 
             + mIncomeNo
             + "' and a.riskcode=b.riskcode and d.CertifyCode=a.CertifyCode and d.subcode=c.cardtype "
             + " order by  b.riskcode";
  var arrResult = easyExecSql(strSql);
  
  if (arrResult == null){return;}
  fm.all("AppntName").value = arrResult[0][0];
  
  if (arrResult.length>1)
  {
     fm.all("RiskName").value = arrResult[0][1]+"等";
  }
  else
	{
		 fm.all("RiskName").value = arrResult[0][1];
	}  
}
/*********************************************************************
 *  选择暂交费类型后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 
function afterCodeSelect(cCodeName, Field) {	 		
	 	fm.all('PayNo').value = '';
    fm.all('IncomeNo').value = '';    
    fm.all('EnterAccDate').value = '';   
    fm.all('AgentCode').value = '';
    fm.all('HPerson').value = '';
    fm.all('CPerson').value = '';
    fm.all('Remark').value = '';	  
   
	 initPolGrid();	 

	 if (fm.all("IncomeType").value =='')
	 {
	 	  fm.all("IncomeType").value = fm.all("IncomeType1").value ;
	 }
	else{
		 	
		
		  divFeeNo.style.display="";
		  divFeeOth.style.display="";
		  divFeeButton.style.display="";			 
	}	
	if(fm.all("IncomeType").value =="1" || fm.all("IncomeType").value =="2"){
		fm.all("SaleChnl1").disabled=false;
		fm.all("SaleChnl").disabled=false;
		fm.all("BankContFlag").disabled=false;
		fm.all("BankContFlagName").disabled=false;
	}else{
		fm.all("SaleChnl1").disabled=true;
		fm.all("SaleChnl").disabled=true;
		fm.all("SaleChnl1").value="";
		fm.all("SaleChnl").value="";
		fm.all("SaleChnlName").value="";
		fm.all("SaleChnlName1").value="";
		fm.all("BankContFlag").value="3";
		fm.all("BankContFlagName").value="全部";
		fm.all("BankContFlag").disabled=true;
		fm.all("BankContFlagName").disabled=true;
	}
	if(fm.all("SaleChnl1").value =="04" || fm.all("SaleChnl").value =="04") {
		fm.all("AgentCom").disabled=false;
		fm.all("AgentComAgentCode").disabled=false;
	}else{
		fm.all("AgentCom").disabled=true;
		fm.all("AgentComAgentCode").disabled=true;
		fm.all("AgentCom").value='';
		fm.all("AgentComAgentCode").value='';
	}
	
}






function StartList(){
		strSQL = "";
		if (fm.MngCom.value.length<=4)
		{
			strSQL = "select '"+fm.all('CertifyCode').value+"', startno,endno,sumcount from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='A"+fm.MngCom.value+"' and StateFlag in ('0','7') order by startno";
		}
		else
		{
			strSQL = "select '"+fm.all('CertifyCode').value+"',startno,endno,sumcount from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='B"+fm.Operator.value+"' and StateFlag in ('0','7') order by startno";
		}
		turnPage1.queryModal(strSQL,StartGrid);
		if(StartGrid.mulLineCount>0){
			fm.StartNo.value=StartGrid.getRowColData(0,2);
		}
}

function showCertify() {
	if (fm.CertifyFlag.checked) {
		showDiv(CertifyInput,"true");
		showDiv(CertifyInfo,"true");		
		showDiv(CertifyStart,"false");
	} else {
		showDiv(CertifyStart,"true");
		showDiv(CertifyInput,"false");
		showDiv(CertifyInfo,"false");
	}
}

function checkFPDM()
{
  var fpdm = fm.all("FPDM").value;
  var mngcom = fm.all("MngCom").value ;
  return true ;
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}
//发票下载
function download()
{
//	if(!checkData())
//	{
//		return false;
//	}
    var mngcom1 = fm.all("MngCom").value ;
    if(mngcom1.substr(0,4) == "8612"||mngcom1.substr(0,4) =="8644"){
    
	fm.action="./FeeInvoiceListPrint.jsp";
	fm.submit(); //提交
	}
	else{
	
	alert("您的管理机构不是天津或者广州，因此您不能下载发票，请您谅解。");
      return false;
	}
}
