  <%
//程序名称：AgentWageGatherInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
        
  	fm.all('ManageCom').value = ''; 
  	fm.all('Operate').value = 'PRINT';    
  	fm.all('AssessYear').value = '';
    fm.all('AssessMonth').value = '';
    fm.all('AgentCode').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';     
    fm.all('BranchType2').value = '<%=BranchType2%>';
    
  }
  catch(ex)
  {
    alert("在LAAssessBFInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
 var AgentQueryGrid ;
 
// 保单信息列表的初始化
function initAgentQueryGrid()
  {                        
    
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
 
      iArray[1]=new Array();
      iArray[1][0]="业务代码";         	//列名
      iArray[1][1]="80px";              	//列宽
      iArray[1][2]=200;            	        //列最大值
      iArray[1][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="姓名";         	//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=120;            	        //列最大值
      iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="销售单位代码";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            	        //列最大值
      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="销售单位名称";         		//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=200;            	        //列最大值
      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
   
      iArray[5]=new Array();
      iArray[5][0]="主管代码";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            	        //列最大值
      iArray[5][3]=0;      
     
       
      iArray[6]=new Array();
      iArray[6][0]="主管名称";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;        
      
      iArray[7]=new Array();
      iArray[7][0]="入司时间";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="前三月FYC";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=200;            	        //列最大值
      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//      
      iArray[9]=new Array();
      iArray[9][0]="维持标准";         		//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=200;            	        //列最大值
      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
//      iArray[10]=new Array();
//      iArray[10][0]="差额";         		//列名
//      iArray[10][1]="60px";            		//列宽
//      iArray[10][2]=200;            	        //列最大值
//      iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
//      
//      iArray[8]=new Array();
//      iArray[8][0]="前六月FYC";         		//列名
//      iArray[8][1]="100px";            		//列宽
//      iArray[8][2]=200;            	        //列最大值
//      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      
//      iArray[9]=new Array();
//      iArray[9][0]="前六月交叉FYC";         		//列名
//      iArray[9][1]="100px";            		//列宽
//      iArray[9][2]=200;            	        //列最大值
//      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[10]=new Array();
      iArray[10][0]="维持差额";         		//列名
      iArray[10][1]="100px";            		//列宽
      iArray[10][2]=200;            	        //列最大值
      iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="晋升标准";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=200;            	        //列最大值
      iArray[11][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[12]=new Array();
      iArray[12][0]="晋升差额";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=200;            	        //列最大值
      iArray[12][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
     
      
      AgentQueryGrid = new MulLineEnter( "fm" , "AgentQueryGrid" ); 
      //这些属性必须在loadMulLine前

      AgentQueryGrid.mulLineCount = 0;   
        AgentQueryGrid.displayTitle = 1;
      AgentQueryGrid.hiddenPlus = 1;
      AgentQueryGrid.hiddenSubtraction = 1;
        AgentQueryGrid.locked=1;
        AgentQueryGrid.canSel=0;
        AgentQueryGrid.canChk=0;
        AgentQueryGrid.loadMulLine(iArray);  
      
      }
      catch(ex)
      {
        alert(ex);
      }
 }
function initForm()
{
  try
  {
    initInpBox();
    
    initAgentQueryGrid();
  }
  catch(re)
  {
    alert("LAAssessBFInit.jsp-->InitForm函数中发生异常:初始化界面错误2!");
  }
}

</script>
