
<jsp:directive.page
	import="com.sinosoft.lis.certify.SysOperatorNoticeBL" />
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：按险种打印操作员日结
	//程序功能：
	//创建日期：2002-12-12
	//创建人  ：lh
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
	System.out.println("start");
	String mDay[] = new String[2];
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	//输出参数
	CError cError = new CError();
	//后面要执行的动作：添加，修改，删除
	String FlagStr = "";
	String Content = "";
	String tOutXmlPath="";  //文件路径
	String strOperation = "";//用来判断是收费还是付费
	String strOpt = "";//LYS 用来记录是暂收还是预收
	strOperation = request.getParameter("fmtransact");
	strOpt = request.getParameter("Opt");
	mDay[0] = request.getParameter("StartDay");   //打印首期
	mDay[1] = request.getParameter("EndDay");     //打印止期
	
	
	//输出打印信息：
	System.out.println("打印类型是:" + strOpt);
	System.out.println("start" + mDay[0]);
	System.out.println("end" + mDay[1]);
	System.out.println(tG.Operator);
	System.out.println(tG.ManageCom);

	VData tVData = new VData();	
	tVData.addElement(mDay);
	tVData.addElement(tG);

	//-----------------------------------------------------日结单
	//   X3首期续期收入日结单
	if (strOpt.equals("FenHeBao")) {
		  System.out.println("strOpt的标志是" + strOpt);
		  SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		  SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
		  String tDate=dateFormat.format(new Date());
		  String tTime=timeFormat.format(new Date());
		  String fileName=tDate.replace("-","")+tTime.replace(":","")+"-X3-"+tG.Operator+".xls";
		  System.out.println("文件名称："+fileName);
		  tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
		  System.out.println("文件路径:" + tOutXmlPath);
		  
		  tVData.addElement(tOutXmlPath);
		  
		FenFinDayCheckPremiumExcelUI tFenFinDayCheckPremiumExcelUI = new FenFinDayCheckPremiumExcelUI();
		if (!tFenFinDayCheckPremiumExcelUI.submitData(tVData, strOperation)) {
		    System.out.println("报错信息：");
            Content = "报表下载失败，原因是:" + tFenFinDayCheckPremiumExcelUI.mErrors.getFirstError();
            FlagStr = "Fail";
		}
	//   X5理赔应付日结单
	}else if(strOpt.equals("FenClaim")){
		System.out.println("strOpt的标志是" + strOpt);
		  SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		  SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
		  String tDate=dateFormat.format(new Date());
		  String tTime=timeFormat.format(new Date());
		  String fileName=tDate.replace("-","")+tTime.replace(":","")+"-X5-"+tG.Operator+".xls";
		  System.out.println("文件名称："+fileName);
		  tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
		  System.out.println("文件路径:" + tOutXmlPath);
		  
		  tVData.addElement(tOutXmlPath);
		  System.out.println("start  FenClaimDayBalanceExcelUI......." );
		  FenClaimDayBalanceExcelUI tFenClaimDayBalanceExcelUI = new FenClaimDayBalanceExcelUI();
		 
		if (!tFenClaimDayBalanceExcelUI.submitData(tVData, strOperation)) {
		    System.out.println("报错信息：");
          Content = "报表下载失败，原因是:" + tFenClaimDayBalanceExcelUI.mErrors.getFirstError();
          System.out.println("报错信息："+Content);
          FlagStr = "Fail";
		}
	}
          String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
          File file = new File(tOutXmlPath);
          
          response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[10000];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
              file.delete();
          }
  if (!FlagStr.equals("Fail"))
  {
    Content = "打印报表成功！";
    FlagStr = "Succ";
  }
%>
<html>
   <script language="javascript">  
    alert('<%=Content %>');
    top.close();
   </script>
</html>