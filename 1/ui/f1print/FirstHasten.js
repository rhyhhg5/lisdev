var turnPage = new turnPageClass();
var SqlPDF = "";
	
function easyQueryClick()
{
	divLCPol4.style.display="none";	
	var tStateFlag="";
	if(fm.all('StateFlag').value=="null"||fm.all('StateFlag').value==null||fm.all('StateFlag').value=="")
 	 		{
    			tStateFlag="";
 	 		}
  else{
			if (fm.all('StateFlag').value=="0")
	  			{
		  				tStateFlag=" and c.stateflag='0' ";
	  			}
	  	else if (fm.all('StateFlag').value=="9")
	  			{
		 	 				tStateFlag="";
	  			}
	  	else
	  			{
	  			tStateFlag=" and c.stateflag<>'0' ";
	  			}
			}
	if(fm.all('PayMode').value==""){
		alert("请选择缴费方式!");
		return false;
	}
	var strSQL 	= "";
	var tLoadFlag=",'', (case c.stateflag when '0' then '未打印' else '已打印' end),  ";
	if(fm.all('PayMode').value=="4"){
		strSQL = " select c.prtSeq ,"  
				 + " a.OtherNo,AppntName,cvalidate,prem,'银行转账','转账失败','',"
	       + " case cansendbank when '0' then '未锁定' else '锁定' end,(select getUniteCode( b.agentcode) from dual),b.managecom"
	       + tLoadFlag                                                                          
	       + "( select d.codename from lyreturnfrombankb c, ldcode1 d where c.paycode=a.getnoticeno and "
	       + " d.codetype='bankerror' and d.code=c.bankcode and d.code1=c.banksuccflag"
	       + " and c.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.getnoticeno)), "
	       + "( select c.bankunsuccreason from lyreturnfrombankb c where c.paycode=a.getnoticeno  "
	       + " and c.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.getnoticeno)),c.StandbyFlag1 "
	       + " from LJSPay a, LCCont b,LOPrtManager c,lyreturnfrombankb d "
	       + " where conttype='1' and appflag<>'1' and uwflag not in ('5','0','a')  and a.OtherNo=PrtNo"
	       + " and a.OtherNoType='9' and BankOnTheWayFlag='0' and SendBankCount>=1 and a.BankSuccFlag='0' "		 
	       + " and c.code='65' and c.otherno=b.prtno and b.paymode='4' and d.paycode=a.getnoticeno and d.polno=b.prtno and d.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.getnoticeno) ";//2007-10-22
		if(fm.agentGroup.value != null && fm.agentGroup.value != "")//2007-10-22
			strSQL += " and b.AgentGroup in (select agentgroup from LABranchGroup bg where bg.agentGroup = '" + fm.agentGroup.value + "') ";
		//2014-11-4  杨阳
		//把新的业务员编码转成旧的
		if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
			strSQL+=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
		}
		strSQL += getWherePart( 'b.prtno','PrtNo' )
				 + getWherePart( 'b.AppntName','AppntName' )
				 + getWherePart( 'CValiDate' )
				 + getWherePart( 'b.PolApplyDate','PolApplyDate' )
				 + getWherePart( 'c.makedate','MakeDate') 
				 ;
	}else{
		strSQL=" select c.prtSeq,b.prtno,AppntName,cvalidate,prem,'现金','未缴费','','未锁定',(select getUniteCode( b.agentcode) from dual),b.managecom"
	       + tLoadFlag
	       +" '','',c.StandbyFlag1  "
	       +" from LCCont b,LOPrtManager c "
	       +" where conttype='1' and appflag<>'1' and uwflag not in ('5','0','a')  and otherno=b.prtno and c.code='75' and b.paymode='1'  ";//2007-10-22
		if(fm.agentGroup.value != null && fm.agentGroup.value != "")//2007-10-22
			strSQL += " and b.AgentGroup in (select agentgroup from LABranchGroup bg where bg.agentGroup = '" + fm.agentGroup.value + "') ";
		//2014-11-4  杨阳
		//把新的业务员编码转成旧的
		if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
			strSQL+=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
		}
		strSQL += getWherePart( 'b.prtno','PrtNo' )
				 + getWherePart( 'b.AppntName','AppntName' )
				 + getWherePart( 'CValiDate' )
				 + getWherePart( 'b.PolApplyDate','PolApplyDate' )
				 + getWherePart( 'c.makedate','MakeDate')
				 ; 
	}		 
	if (fm.ManageCom.value != "" )
	 {
		strSQL = strSQL + getWherePart( 'b.ManageCom','ManageCom','like' );
   }
   if (RePrintFlag=="1")
	 {
	 	strSQL=strSQL+tStateFlag+" with ur ";
	 }
	 else
	 {
	 	strSQL=strSQL+" and c.stateflag='0' with ur";
	 }
 
   //alert(strSQL);
   turnPage.queryModal(strSQL, PolGrid);   
   SqlPDF = strSQL;   
}

function printHasten()
{
	var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		prtNo = PolGrid.getRowColData(tSel-1,2);
		comCode = PolGrid.getRowColData(tSel-1,11);

		if(prtNo==''||prtNo==null) {
			alert("无效的数据");
			return;
		}
		

			fm.all("CustPrtNo").value		= PolGrid.getRowColData(tSel-1,2);
			fm.all("AgentNo").value			=	PolGrid.getRowColData(tSel-1,10);
			fm.all("AppCValiDate").value= PolGrid.getRowColData(tSel-1,4);
			fm.all("Prem").value 				= PolGrid.getRowColData(tSel-1,5);
			fm.PrtSeq.value=PolGrid.getRowColData(tSel-1,1);			
			getPrintData(prtNo,comCode);
			
			fm.fmtransact.value = "CONFIRM";
			fm.action = "HastenSave.jsp?RePrintFlag="+RePrintFlag;
			fm.submit();

	}
}

function getPrintData(prtNo,comCode)
{
	strSQL = "select b.codename,a.bankunsuccreason from lyreturnfrombankb a,ldcode1 b, LJSpay c"
		+ " where a.paycode=c.getnoticeno and b.codetype='bankerror' and b.code=a.bankcode and b.code1=a.banksuccflag"
		+ " and a.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=c.getnoticeno)"
		+ " and c.otherno='"+prtNo+"'" ;
	var failReason = easyExecSql(strSQL);
	if (failReason)
	{
		if(failReason[0][1]!=""){
		fm.all("FailReason").value = failReason[0][1];
		}else{
		fm.all("FailReason").value = failReason[0][0];
		}	
	}
}

function showPrintInfo(parm1,parm2)
{
	divLCPol4.style.display="none";	
	var tSel=PolGrid.getSelNo();	
	var prtNo=PolGrid.getRowColData(tSel-1,2);
	
	strSQL="select stateflag from loprtmanager where code= '75' and otherno='" + PolGrid.getRowColData(tSel-1,2)+"'";
	var isPlayF = easyExecSql(strSQL);
	if (isPlayF)
	{
		if (isPlayF[0][0]=="0")
		{
			fm.all("StateFlagName2").value = "未打印"; 
		}
		if (isPlayF[0][0]=="1")
		{
			fm.all("StateFlagName2").value = "已打印"; 
		}/* */
	}
	if(fm.PayMode.value!="1"){
        var tStrSql="select 1 from ljspay a,lyreturnfrombankb b where b.paycode=a.getnoticeno  and OtherNoType='9' and BankOnTheWayFlag='0' and SendBankCount>=1 and a.BankSuccFlag in('0','2') and a.otherno='"+prtNo+"'";
	 			var tarrResult = easyExecSql(tStrSql);
	 			if(tarrResult){
        var tStrSql1="select bankunsuccreason from ljspay a,lyreturnfrombankb b where b.paycode=a.getnoticeno  and OtherNoType='9' and BankOnTheWayFlag='0' and SendBankCount>=1 and a.BankSuccFlag in('0','2') and a.otherno='"+prtNo+"' and b.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.getnoticeno)";
	 			var tarrResult1 = easyExecSql(tStrSql1);
        	if(tarrResult1!=""){
        		//alert(tarrResult1);
        		divLCPol4.style.display="";
        		fm.Content.value=tarrResult1[0][0];
        	} 			
	 			}else{
	 			divLCPol4.style.display="none";	 
	 			return false;
	 			}

	}	
}
function printPolpdf()	
{
  var i = 0;  
  var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	else
	{
		PrtSeq = PolGrid.getRowColData(tChked[0],1);
		PrtNo = PolGrid.getRowColData(tChked[0],2);
		//alert(PrtSeq);
		//alert(PrtNo);
		fm.PrtSeq.value = PrtSeq;
		fm.PrtNo.value = PrtNo;
		//fm.PolNo.value = arrReturn[0][1];
		fm.fmtransact.value = "PRINT";
		if(fm.PayMode.value=="1"){
		fm.action = "../uw/PrintPDFSave.jsp?Code=075&RePrintFlag="+RePrintFlag;
		}else{
		fm.action = "../uw/PrintPDFSave.jsp?Code=065&RePrintFlag="+RePrintFlag;
		}
		fm.submit();		
	}
}

function printPolpdfbat()
{
	//alert("打印列表没有数据");
	if (PolGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
  var tCode="";
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	//alert(fm.all('PayMode').value);
	if(fm.PayMode.value=="1")
	{
	  tCode='75';
	} else
		{
			tCode='65';
			}
	if(tChked.length == 0)
	{
		fm.action="./FirstHastenInsAllBatPrt.jsp?strsql="+SqlPDF+"&Code="+tCode;
		fm.submit();
	}
	else
	{
		fm.action="./FirstHastenInsForBatPrt.jsp?Code="+tCode;
		fm.submit();
	}
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  	fm.agentGroup.value = arrResult[0][0];
  }
}

//下拉框选择后执行   zhangjianbao   2007-11-16
function afterCodeSelect(cName, Filed)
{	
  if(cName=='statuskind')
  {
    saleChnl = fm.saleChannel.value;
	}
}

//选择营销机构前执行   zhangjianbao   2007-11-16
function beforeCodeSelect()
{
	if(saleChnl == "") alert("请先选择销售渠道");
}

function printPolpdfNew()	
{
  var i = 0;  
  var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	else
	{
		PrtSeq = PolGrid.getRowColData(tChked[0],1);
		PrtNo = PolGrid.getRowColData(tChked[0],2);
		//alert(PrtSeq);
		//alert(PrtNo);
		fm.PrtSeq.value = PrtSeq;
		fm.PrtNo.value = PrtNo;
		//fm.PolNo.value = arrReturn[0][1];
		fm.fmtransact.value = "PRINT";
		var showStr="正在准备打印数据，请稍后...";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.target = "fraSubmit";

		if(fm.PayMode.value=="1"){
		  fm.action="../uw/PDFPrintSave.jsp?Code=75&OtherNo="+PrtNo+"&OldPrtSeq="+PrtSeq;
		}else{
		  fm.action="../uw/PDFPrintSave.jsp?Code=65&OtherNo="+PrtNo+"&OldPrtSeq="+PrtSeq;
		}
		fm.submit();		
	}
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

function afterSubmit(FlagStr, content)
{
  try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}