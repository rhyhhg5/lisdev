<%
//程序名称：PremQuery.jsp
//程序功能：保费查询
//创建日期：2004-10-10
//创建人  ：wentao
//更新记录：更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
%>

<html>    
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
  
  <SCRIPT src="./PremQuery.js"></SCRIPT>   
  <%@include file="./PremQueryInit.jsp"%>   
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>保费查询 </title>
</head>      
 
<body  onload="initForm();" >
  <form method=post name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>管理机构</TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom ondblclick="return showCodeList('station',[this,ManageComName],[0,1],null,codeSql,'1',null,250);" 
            				onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1],null,codeSql,'1',null,250);"><input class = codename name = ManageComName ></TD>          
       		<TD  class= title width="25%">业务开始日期</TD>
       		<TD  class= input width="25%">
       		  <Input class= "coolDatePicker" dateFormat="short" name=ywBeginDate verify="起始日期|NOTNULL">
       		</TD>
       		<TD  class= title width="25%">业务终止日期</TD>
       		<TD  class= input width="25%">
       		  <Input class= "coolDatePicker" dateFormat="short" name=ywEndDate verify="结束日期|NOTNULL">
       		</TD>  
     </TR>
     <TR  class= common>
       		<TD  class= title>交费终止期间</TD>
       		<TD  class= input>
       		  <Input class= "common" dateFormat="short" name=PayEndDate>
       		</TD>
       		<TD  class= title>交费终止期间单位</TD>
       		<TD  class= input>
       		  <Input class= "codeno" dateFormat="short" name=PayEndDateUnit CodeData = "0|^A|岁|^Y|年^M|月^D|天"
       		  ondblClick="showCodeListEx('PayEndDateUnit',[this,PayEndDateUnitName],[0,1]);" onkeyup="showCodeListKeyEx('PayEndDateUnit',[this,PayEndDateUnitName],[0,1]);"><input class= codename name = PayEndDateUnitName>
       		</TD>  
       		<TD  class= title>保险期间</TD>
       		<TD  class= input>
       		  <Input class= "common" dateFormat="short" name=InsurePeriod>
       		</TD>  
     </TR>
     <TR  class= common>
       		<TD  class= title>保险期间单位</TD>
       		<TD  class= input>
       		  <Input class= "codeno" dateFormat="short" name=InsurePeriodUnit CodeData = "0|^A|岁|^Y|年^M|月^D|天"
       		  ondblClick="showCodeListEx('InsurePeriodUnit',[this,InsurePeriodUnitName],[0,1]);" onkeyup="showCodeListKeyEx('InsurePeriodUnit',[this,InsurePeriodUnitName],[0,1]);"><input class= codename name = InsurePeriodUnitName>
       		</TD>
       		<TD  class= title>投保年龄(开始)</TD>
       		<TD  class= input>
       		  <Input class= "common" dateFormat="short" name=AppntBeginAge>
       		</TD>  
       		<TD  class= title>投保年龄(终止)</TD>
       		<TD  class= input>
       		  <Input class= "common" dateFormat="short" name=AppntEndAge>
       		</TD>  
     </TR>
     <TR  class= common>
       		<TD  class= title>保单年度</TD>
       		<TD  class= input">
       		  <Input class= "common" dateFormat="short" name=InsureYear>
       		</TD>
       		<TD  class= title>险种编码</TD>
          <TD  class= input>
          	<Input class="codeno" name=RiskCode ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('RiskCode',[this,RiskCodeName],[0,1]);"><input class= codename name = RiskCodeName>
          </TD>
          <TD  class= title>交费方式</TD>
          <TD  class= input>
            <Input class="codeno" name=PayIntv ondblclick="return showCodeList('PayIntv',[this,PayIntvName],[0,1]);"><input class= codename name = PayIntvName>
          </TD>
     </TR>
   	</Table> 
    <input type=hidden id="fmtransact" name="fmtransact">
		<br>
    <!--数据区-->
    <INPUT VALUE="查询" class= common TYPE=button onclick="easyQuery()"> 	
    <INPUT VALUE="打印" class= common TYPE=button onclick="easyQueryPrint(2,'CodeGrid','turnPage')"> 	

  	<Div  id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
									<span id="spanCodeGrid" >
									</span> 
		  				</td>
					</tr>
    		</table> 
      	<INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      	<INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      	<INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      	<INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 	
<script>
	<!--选择机构：只能查询本身和下级机构-->
	var codeSql = "1  and code like #"+<%=Branch%>+"%#";
</script>