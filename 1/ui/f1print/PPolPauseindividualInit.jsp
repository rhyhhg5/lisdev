<%
//程序名称：PPolPauseindividualInit.jsp
//程序功能：
//创建日期：2006-10-03
//创建人  ：韦力
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
  String mCurDate = PubFun.getCurrentDate();
  String sql = "select date('" + mCurDate + "') - 3 months from dual ";
  String mStartDate = new ExeSQL().getOneValue(sql);
  if(mStartDate.equals("") || mStartDate.equals("null"))
  {
    mStartDate = "";
  }
  
  String mLoadFlag = request.getParameter("LoadFlag");
%>                            

<script language="JavaScript">
//单击时查询
//function RegisterDetailClick(cObj)
//{
//  	var ex,ey;
//  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
//  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
//  	divDetailInfo.style.left=ex;
//  	divDetailInfo.style.top =ey;
//    divDetailInfo.style.display ='';
//}

var mLoadFlag = "<%= mLoadFlag%>";

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
    fm.StartDate.value = "<%=mStartDate%>";
    fm.EndDate.value = "<%=mCurDate%>";
    fm.ManageCom.value = ComCode;
    if(mLoadFlag != null && mLoadFlag == "G")
    {
      fm.all("CodeID").style.display = "none";
    }
    else
    {
      fm.all("GrpCodeID").style.display = "none";
    }
    
   	fm.all('Order1').value='';
 		fm.all('Order2').value='';
 		fm.all('Order3').value='';
  }
  catch(ex)
  {
    alert("PPolPauseindividualInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!" + ex.message);
  }      
}                                      

function initForm()
{
  try
  {
    initInpBox();
		initBillGrid();
  }
  catch(re)
  {
    alert("PPolPauseindividualInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 清单信息列表的初始化
function initBillGrid()
{
  var iArray = new Array();
    
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="抽档日期";         		//列名
    iArray[1][1]="70px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="PayMakeDate";

    iArray[2]=new Array();
    iArray[2][0]="管理机构";         		//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="ManageCom";

    iArray[3]=new Array();
    iArray[3][0]="营销部门";         		//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="AgentGroup";

    iArray[4]=new Array();
    iArray[4][0]="保单号";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="ContNo";

  	iArray[5]=new Array();
    iArray[5][0]="投保人";         		//列名
    iArray[5][1]="50px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="AppntName";

  	iArray[6]=new Array();
    iArray[6][0]="投保人电话";         		//列名
    iArray[6][1]="70px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="Mobile";

    iArray[7]=new Array();
    iArray[7][0]="投保人联系地址";         		//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="PostalAddress";

  	iArray[8]=new Array();
    iArray[8][0]="应收记录号";         		//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="GetNoticeNo";

  	iArray[9]=new Array();
    iArray[9][0]="待收保费";         		//列名
    iArray[9][1]="70px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[9][21]="SumDuePayMoney"; 
    
    iArray[10]=new Array();
    iArray[10][0]="待收时间";         		//列名
    iArray[10][1]="70px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[10][21]="PayToDate";
    
    iArray[11]=new Array();
    iArray[11][0]="缴费截止日期";         		//列名
    iArray[11][1]="70px";            		//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[11][21]="PayDate";
    
    iArray[12]=new Array();
    iArray[12][0]="缴费方式";         		//列名
    iArray[12][1]="50px";            		//列宽
    iArray[12][2]=200;            			//列最大值
    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[12][21]="PayMode";
    
    iArray[13]=new Array();
    iArray[13][0]="失效/暂停通知书号";         		//列名
    iArray[13][1]="100px";            		//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[13][21]="PrtSeq"; 
    
    iArray[14]=new Array();
    iArray[14][0]="保单失效/暂停日";         		//列名
    iArray[14][1]="90px";            		//列宽
    iArray[14][2]=200;            			//列最大值
    iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[14][21]="StartDate"; 
    
    iArray[15]=new Array();
    iArray[15][0]="类型";         		//列名
    iArray[15][1]="50px";            		//列宽
    iArray[15][2]=200;            			//列最大值
    iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[15][21]="CodeType";
    
    iArray[16]=new Array();
    iArray[16][0]="代理人编码";         		//列名
    iArray[16][1]="70px";            		//列宽
    iArray[16][2]=200;            			//列最大值
    iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[16][21]="AgentCode";
    
    iArray[17]=new Array();
    iArray[17][0]="代理人电话";         		//列名
    iArray[17][1]="80px";            		//列宽
    iArray[17][2]=200;            			//列最大值
    iArray[17][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[17][21]="AgentPhone";
    
    iArray[18]=new Array();
    iArray[18][0]="代理人姓名";         		//列名
    iArray[18][1]="60px";            		//列宽
    iArray[18][2]=200;            			//列最大值
    iArray[18][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[18][21]="AgentName";
    
    iArray[19]=new Array();
    iArray[19][0]="失效时间";         		//列名
    iArray[19][1]="60px";            		//列宽
    iArray[19][2]=200;            			//列最大值
    iArray[19][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[19][21]="MakeDate";
    
    iArray[20]=new Array();
    iArray[20][0]="保单归属状态";         		//列名
    iArray[20][1]="60px";            		//列宽
    iArray[20][2]=200;            			//列最大值
    iArray[20][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[20][21]="ContAgentState";
    
    BillGrid = new MulLineEnter( "fm" , "BillGrid" ); 
    //这些属性必须在loadMulLine前
    BillGrid.mulLineCount = 0;   
    BillGrid.displayTitle = 1;
    BillGrid.canChk=1;
    BillGrid.locked = 1;
    BillGrid.loadMulLine(iArray);  
    BillGrid.detailInfo="单击显示详细信息";
    //BillGrid.detailClick=RegisterDetailClick;
  }
  catch(ex)
  {
    alert(ex.messate);
  }
}

</script>