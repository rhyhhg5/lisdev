<%
//程序名称：RSReport.jsp
//程序功能：
//创建日期：2004-7-2 11:30
//创建人  ：GUOXIANG
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String Code = request.getParameter("Code");
    String CodeName ="";
    System.out.println("参数为:"+Code);
    LDMenuDB tLDMenuDB = new LDMenuDB();
    String name="../f1print/RSReport.jsp?Code="+Code;
    tLDMenuDB.setRunScript(name);
    LDMenuSet mLDMenuSet=tLDMenuDB.query();
    for (int i=1;i<=mLDMenuSet.size();i++){
      LDMenuSchema tLDMenuSchema = mLDMenuSet.get(i);
      CodeName=tLDMenuSchema.getNodeName();
    }  
      
%>
<html>    
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312">

  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
  
  <SCRIPT src="./RSReport.js"></SCRIPT>   
  <%@include file="./RSReportInit.jsp"%>   
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <title>再报对外报表 </title>
</head>      
 
<body  onload="initForm();" >
  <form method=post name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>
            报表名称:
          </TD>  
          <TD class= title>
             <input readonly class=common name=CodeName value=<%=CodeName%>>
          </TD>
          <TD class= input style="display: none">
            <Input class=common name=Code value = <%=Code%> >
          </TD>
       
          <TD class= title>
            分保单位：
          </TD>          
          <TD class=input> 
             <input readonly class=common name=ReInsureCom value='1001'> </TD>
      </TR>
     <TR class =common>       
           <TR class =common>    
          <TD class= title>
            开始时间
          </TD>  
          
          <TD class= input>
              <Input class="coolDatePicker" dateFormat="short" verify="起始时间|NOTNULL" name= "time">
          </TD> 
          <TD class =title> 
            结束时间
          </TD>
          <TD class= input>
              <Input class="coolDatePicker" dateFormat="short" verify="结束时间|NOTNULL" name= "timeend">
          </TD> 
     </TR>   
   </Table>  
    <Div id= divCmdButton style="display: ''">
       <INPUT class=common  VALUE="打印报表" TYPE=button onclick="easyPrint()">     
    </Div>
    <Div  id= "divVarGrid" style= "display: 'none'">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanVarGrid" >
					</span> 
			  	</td>
			</tr>
  	   </table> 
  	</Div>
  	<Div  id= "divBGGrid" style= "display: 'none'">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanBGGrid" >
					</span> 
			  	</td>
			</tr>
  	    </table> 
  	</Div>
  	<Div  id= "divLPGrid" style= "display: 'none'">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanLPGrid" >
					</span> 
			  	</td>
			</tr>
  	    </table> 
  	</Div>
  	<Div  id= "divGSXQGrid" style= "display: 'none'">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanGSXQGrid" >
					</span> 
			  	</td>
			</tr>
  	    </table> 
  	</Div>
  	<Div  id= "divGBGGrid" style= "display: 'none'">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanGBGGrid" >
					</span> 
			  	</td>
			</tr>
  	    </table> 
  	</Div>
  	<Div  id= "divGLPGrid" style= "display: 'none'">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanGLPGrid" >
					</span> 
			  	</td>
			</tr>
  	    </table> 
  	</Div>
</body>
</html> 	
	