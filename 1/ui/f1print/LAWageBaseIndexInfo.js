//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet1,arrDataSet2; 
var turnPage = new turnPageClass();


//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAComQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
     

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function DetailPrint()
{
  fm.action="../agentprint/LAIndexInfoQueryPrt.jsp";
  fm.target="f1print";
  submitForm();
}

//提交，保存按钮对应操作
function submitForm()
{  	
  	var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.target='f1print';
	  fm.submit(); //提交
	  showInfo.close();	
}

function DoSDownload()
{
 // 书写SQL语句
	var strSQL = "";
	if(!verifyInput())
	{
		return false;
	}
    strSQL = "select a.indexcalno,getUniteCode(a.agentcode),c.Name,b.AgentGrade,a.ManageCom,a.BranchAttr,d.name,"
            +"a.t67,a.t50,a.t49,"
            +"case when a.t50>0 then decimal(a.t49/a.t50,12,4) else  0.8 end,"
            +"a.t45,a.t65,a.t61,"
            +"(case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where  indexcalno=a.indexcalno  and  agentCode  in(select agentcode from latree where  " 
            +" agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode )))  else nvl(a.t60,0) end ),"
            +"(case substr(a.indexcalno,5,2) when '03' then (case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where indexcalno>='"+fm.BIndexCalNo.value+"'"
            +" and indexcalno<='"+fm.EIndexCalNo.value+"' and agentCode  in(select agentcode from latree where  agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode ))) "
            +" else nvl(a.t62,0) end)  when '06' then (case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where indexcalno>='"+fm.BIndexCalNo.value+"'"
			+" and indexcalno<='"+fm.EIndexCalNo.value+"' and agentCode  in(select agentcode from latree where  agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode ))) "
   			+" else nvl(a.t62,0) end) when '09' then (case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where indexcalno>='"+fm.BIndexCalNo.value+"'"
			+" and indexcalno<='"+fm.EIndexCalNo.value+"' and agentCode  in(select agentcode from latree where  agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode ))) "
			+" else nvl(a.t62,0) end) when '12' then (case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where indexcalno>='"+fm.BIndexCalNo.value+"'"
			+" and indexcalno<='"+fm.EIndexCalNo.value+"' and agentCode  in(select agentcode from latree where  agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode ))) "
 			+"else nvl(a.t62,0) end) else nvl(a.t62,0) end ),"
            +"a.t46,a.t55,a.t56,a.t66,a.t63,a.t64"
            + " from laindexinfo a,LAtree b,LAAgent c,LABranchGroup d "
	        +"where a.AgentCode = b.AgentCode "
	        +" and a.agentcode=c.agentcode and a.branchattr=d.branchattr "
	        +" and a.IndexType ='00' and a.branchtype='"+fm.BranchType.value+"' and d.branchtype='"+fm.BranchType.value+"'"
	        +" and a.indexcalno >='"+fm.BIndexCalNo.value+"' and a.indexcalno<='"+fm.EIndexCalNo.value+"'"
	 	    +" and a.ManageCom like '"+fm.ManageCom.value+"%'"
	 	    +getWherePart('c.groupAgentCode','AgentCode')
	 	    +getWherePart('a.BranchAttr','BranchAttr','like','0','%')
		    strSQL+=" ORDER BY a.branchattr,a.IndexCalNo, a.agentcode asc with ur";       
     
	//查询SQL，返回结果字符串
//            fm.querySql.value = strSQL;
//            fm.submit();
    fm.querySql.value = strSQL;
	// 定义列名
	var strSQLTitle = "select '薪资年月','代理人编码','代理人姓名','代理人职级','管理机构','销售机构代码','销售机构名称','个人当考核月保费','13月新单保费(分母)','13月有效保费(分子)','13月继续率','个人月FYC','个人当考核月FYC','个人当季度FYC','团队当月FYC','团队当季度FYC','个人月续年续佣','个人月续保续佣','个人当月新标准客户','个人当考核月客户','当月新增人数','当季新增人数'  from dual where 1=1 ";
	fm.querySqlTitle.value = strSQLTitle;
	// 定义表名
	fm.all("Title").value = "select '薪资计算基础指标明细表 ：' from dual where 1=1 ";

	fm.action = " ../agentquery/LAPrintTemplateSave.jsp";

	fm.submit(); //提交 	
}

// 查询按钮
function easyQueryClick()
{
	if(!verifyInput())
	{
		return false;
	}
	initIndexInfoGrid1();
	var tReturn = getManageComLimitlike("a.ManageCom");
  var tCondition = "" ;
  var tsql = "";
  var colArray=new Array();
  colArray[0]=
  tsql = "select a.indexcalno,getUniteCode(a.agentcode),c.Name,b.AgentGrade,a.ManageCom,a.BranchAttr,d.name,"
            +"nvl(a.t67,0),nvl(a.t50,0),nvl(a.t49,0),"
            +"case when a.t50>0 then decimal(a.t49/a.t50,12,4) else  0.8 end,"
            +"nvl(a.t45,0),nvl(a.t65,0),nvl(a.t61,0),"
            +"(case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where indexcalno=a.indexcalno and agentCode  in(select agentcode from latree where " 
            +" agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode )))  else nvl(a.t60,0) end ),"
            +"(case substr(a.indexcalno,5,2) when '03' then (case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where indexcalno>='"+fm.BIndexCalNo.value+"'"
            +" and indexcalno<='"+fm.EIndexCalNo.value+"' and agentCode  in(select agentcode from latree where  agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode ))) "
            +" else nvl(a.t62,0) end)  when '06' then (case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where indexcalno>='"+fm.BIndexCalNo.value+"'"
			+" and indexcalno<='"+fm.EIndexCalNo.value+"' and agentCode  in(select agentcode from latree where  agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode ))) "
   			+" else nvl(a.t62,0) end) when '09' then (case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where indexcalno>='"+fm.BIndexCalNo.value+"'"
			+" and indexcalno<='"+fm.EIndexCalNo.value+"' and agentCode  in(select agentcode from latree where  agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode ))) "
			+" else nvl(a.t62,0) end) when '12' then (case b.AgentGrade when 'B01' then (select sum(t65+t55) from laindexinfo where indexcalno>='"+fm.BIndexCalNo.value+"'"
			+" and indexcalno<='"+fm.EIndexCalNo.value+"' and agentCode  in(select agentcode from latree where  agentgroup2 in (select agentgroup2 from latree where agentcode =b.agentcode ))) "
 			+"else nvl(a.t62,0) end) else nvl(a.t62,0) end ),"
            +"nvl(a.t46,0),nvl(a.t55,0),nvl(a.t56,0),nvl(a.t66,0),nvl(a.t63,0),nvl(a.t64,0)"
            + " from laindexinfo a,LAtree b,LAAgent c,LABranchGroup d "
	    +"where a.AgentCode = b.AgentCode "
	    +" and a.agentcode=c.agentcode and a.branchattr=d.branchattr "
	    +" and a.IndexType ='00' and a.branchtype='"+fm.BranchType.value+"' and d.branchtype='"+fm.BranchType.value+"'"
	    +" and a.indexcalno >='"+fm.BIndexCalNo.value+"' and a.indexcalno<='"+fm.EIndexCalNo.value+"'"
	    +getWherePart('a.ManageCom','ManageCom','like')
	    +getWherePart('a.BranchAttr','BranchAttr','like')
	    +getWherePart('c.groupAgentCode','AgentCode')     
      tsql += " ORDER BY a.branchattr,a.IndexCalNo, a.agentcode asc"; 
 

  turnPage.strQueryResult  = easyQueryVer3(tsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有查询到符合条件的考核指标！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage.strQueryResult);
  
turnPage.arrDataCacheSet = arrDataSet1;

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = IndexInfoGrid1;    
          
  //保存SQL语句
  turnPage.strQuerySql     = tsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  
}


function getQueryResult1()
{
  var arrSelected = null;
  tRow = IndexInfoGrid1.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet1 == null )
    return arrSelected;
  arrSelected = new Array();
  tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
  //设置需要返回的数组
  arrSelected[0] = arrDataSet1[tRow-1];
  return arrSelected;
}




/*function selectItem(spanID,parm2)
{
  var arrSelect = new Array();
  arrSelect = getQueryResult1();
  var strIndexCalNo=arrSelect[0][0];
  var strIndexType=arrSelect[0][1];
  var strAgentCode=arrSelect[0][2];
  if (strIndexCalNo!="")
    strIndexCalNo=" and IndexCalNo='"+strIndexCalNo+"'";
  if (strIndexType!="")
    strIndexType=" and IndexType='"+strIndexType+"'";
  if (strAgentCode!="")
    strAgentCode=" and AgentCode='"+strAgentCode+"'";

  var strSQLMoreResult="select * from LAIndexInfo where 1=1 "
                 + strIndexCalNo
	         + strIndexType
	         + strAgentCode;
	        
  var strMoreResult=easyQueryVer3(strSQLMoreResult, 1, 1, 1);
  fm.all('HiddenStrMoreResult').value=strMoreResult; 

  if (strMoreResult)
  {
   showInfo=window.open("./LAIndexInfoQueryMoreResult.jsp");
  }
  else
    alert("查询失败");

}*/








