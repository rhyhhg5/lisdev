
<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>

<html>
<%
//程序名称：
//程序功能：续期保费银行转账成功清单
//创建日期：2004-5-24
//创建人  ：刘岩松程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="PPolPauseInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="PPolPauseInit.jsp"%>
	</head>

	<body  onload="initForm();" >
  	<form action="./PPolPausePrint.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
    		
    	<Div  id= "divLLReport1" style= "display: ''">    	
      	<table  class= common>
		  <TR  class= common>
		    <TD  class= title>
            	失效日期范围：  开始日期
          	</TD>    	
          	<TD  class= input>
            	<input class="coolDatePicker" dateFormat="short" name="StartDate" >
          	</TD>
          	<TD  class= title>
            	结束日期
          	</TD>
          	<TD  class= input>
            	<input class="coolDatePicker" dateFormat="short" name="EndDate" >
          	</TD>
		</TR>
		<TR class= common>
			<TD class= title>
				机构
			</TD>
			<TD class= input>
				<input type=text name=ManageCom class=common verify="机构|len<10">
			</TD>
			<TD class= title>
				业务员代码
			</TD>
			<TD class= input>
				<input type=text name=AgentCode class=common verify="业务员代码|len<10">
			</TD>
			<TD class= title>
				失效类型
			</TD>
			<TD class= input>
				<Input class= code name="GrpCode" CodeData="0|^42|未交费暂停^58|未结算暂停^21|保单满期^0|全部<默认值>"ondblclick="return showCodeListEx('GrpCode',[this]);" verify="失效类型|Num&len<10">
			</TD>
		</TR>
      <table  class= common>
	    <table  class= common>
		  <TR class= common>
		     <TD class= title> 排序1 </td>	 
			  <TD  class= input>	
			  <Input class= code name=Order1 CodeData="0|^0|投保人^1|失效日期^2|业务员^3|业务部门"ondblclick="return showCodeListEx('Order1',[this]);" >
			  </TD>						
			  <TD class= title>
				排序2
			  </td>
			  <TD  class= input>
				<Input class= code name=Order2 CodeData="0|^0|投保人^1|失效日期^2|业务员^3|业务部门"ondblclick="return showCodeListEx('Order2',[this]);">
			  </TD> 
			   <TD class= title>
				排序3
			  </td>
			  <TD  class= input>
				<Input class= code name=Order3 CodeData="0|^0|投保人^1|失效日期^2|业务员^3|业务部门"ondblclick="return showCodeListEx('Order3',[this]);">
			  </TD>  
		    <TD></TD>
		  </tr>
          <TR  class= common>
            <TD>
          	<input class=cssButton type=button value="查  询" onclick="showSerialNo()">
            </TD>                      
        </TR>        	
 	</table>

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanBillGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	
		  <Div  id= "divPage" align=center style= "display: '' ">     
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		 </Div>
  	</Div>
	</BR>
		<input class=cssButton type=button value="打印清单" onclick="PrintBill();">
		<input type=hidden id="sql" name="sql" value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
