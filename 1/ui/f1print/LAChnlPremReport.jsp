<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：RateCommisionModule.jsp
//程序功能：佣金率导入模版下载
//创建日期：2008-10-17 11:07
//创建人  ：苗祥征
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%
  String FlagStr = "";
  String Content = "";
  System.out.println("ssssss");
  
  //得到excel文件的保存路径
    String sql = "select SysvarValue "
                + "from ldsysvar "
                + "where sysvar='SaleXmlPath'";
    ExeSQL tExeSQL = new ExeSQL();
    String subPath = tExeSQL.getOneValue(sql);
    
  Calendar cal = new GregorianCalendar();
  String year = String.valueOf(cal.get(Calendar.YEAR));
  String month=String.valueOf(cal.get(Calendar.MONTH)+1);
  String date=String.valueOf(cal.get(Calendar.DATE));
  String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String now = year + month + date + hour + min + sec + "_" ;
  
  String  millis = String.valueOf(System.currentTimeMillis());  
  String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".csv";
  subPath+=fileName;
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
  String realPath=application.getRealPath("/")+"/"+subPath;
  System.out.println("..........download jsp getrealpath here :"+application.getRealPath("vtsfile"));
  //String tType = "3";
  System.out.println("OutXmlPath:" + tOutXmlPath);
  
    //接收信息，并作校验处理。
  //输入参数
  LAChnlPremReportBL tLAChnlPremReportBL   = new LAChnlPremReportBL();
  //输出参数
  CErrors tError = null;
  //String tRela  = "";                
  boolean operFlag=true;
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	try{
  
   String tManageCom = request.getParameter("ManageCom");
   String tBankType2 = request.getParameter("BankType2");
   String tAgentCom = request.getParameter("AgentCom");
   String tRiskCode = request.getParameter("RiskCode");
   String tAgentCode= request.getParameter("AgentCode");
   String tState = request.getParameter("State");
   String tGetPolState=request.getParameter("GetPolState");
   String tStartDate = request.getParameter("StartDate");
   String tEndDate = request.getParameter("EndDate");
   String tPayYears = request.getParameter("PayYears"); 
   String tPayYear=request.getParameter("PayYear");
   String tGetState=request.getParameter("GetState");
   String tsaleFlag=request.getParameter("saleFlag");     //新增查询条件。 tsaleFlag
   String tphoneNumber=request.getParameter("phoneNumber");
   String tage=request.getParameter("age");
   String toutFlag=request.getParameter("outFlag");
   XmlExport txmlExport = new XmlExport();
   VData tVData = new VData();
   VData mResult = new VData();
   // 准备传输数据 VData
	tVData.add(tManageCom);
	tVData.add(tBankType2);
	tVData.add(tAgentCom);
	tVData.add(tRiskCode);
	tVData.add(tAgentCode);
	tVData.add(tState);
	tVData.add(tStartDate);
	tVData.add(tEndDate);
	tVData.add(tPayYears);
    tVData.add(tG);
    tVData.add(realPath);
    tVData.add(tGetPolState);
    tVData.add(tPayYear);
    tVData.add(tGetState);
    tVData.add(tsaleFlag);
    tVData.add(tphoneNumber);
    tVData.add(tage);
    tVData.add(toutFlag);
  	 //调用打印的类
    if (!tLAChnlPremReportBL.submitData(tVData,"PRINT")){
	      FlagStr="Fail";
	      Content=tLAChnlPremReportBL.mErrors.getFirstError().toString();    
        }
	 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
	 	  
	 	  File file = new File(realPath);
	      response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[4096];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
              //file.delete();
          }
  }catch(Exception ex){
  	ex.printStackTrace();
  }
  if(FlagStr.equals("Fail")){

%>
<html>
<script language="javascript">
	alert(<%=Content%>);
</script>
</html>
<%}%>