<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%
	System.out.println("start NewFinDayCheckBQExcelSave ...X4");
	String mDay[]=new String[2];
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
    //输出参数
	CError cError = new CError( );
    //后面要执行的动作：添加，修改，删除

	String FlagStr = "";
	String Content = "";
	String tOutXmlPath="";  //文件路径
	String strOperation = "";//用来判断是收费还是付费
	String strOpt = "";//LYS 用来记录是暂收还是预收
	strOperation = request.getParameter("fmtransact");
	strOpt = request.getParameter("Opt");
	mDay[0] = request.getParameter("StartDay");   //打印首期
	mDay[1] = request.getParameter("EndDay");     //打印止期
	 
	//输出打印信息：
	System.out.println("X4打印类型是：" + strOpt+";打印日期：" + mDay[0]+"至"+mDay[1]);
	System.out.println("Operator-->"+tG.Operator+";ManageCom-->"+tG.ManageCom);	  
		 
	VData tVData = new VData();
	tVData.addElement(mDay);
	tVData.addElement(tG);
	
	//X4-保全保费日结单
	if (strOpt.equals("FenBQPrem")){
		System.out.println("X4-strOpt的标志是：" + strOpt);
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
		String tDate=dateFormat.format(new Date());
		String tTime=timeFormat.format(new Date());
		String fileName=tDate.replace("-","")+tTime.replace(":","")+"-X4-"+tG.Operator+".xls";
		System.out.println("文件名称："+fileName);
		tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
		System.out.println("文件路径:" + tOutXmlPath);
		tVData.addElement(tOutXmlPath);
	
		FenFinDayBQPremExcelUI tFenFinDayBQPremExcelUI = new FenFinDayBQPremExcelUI();
		if(!tFenFinDayBQPremExcelUI.submitData(tVData,strOperation))
		{
		    System.out.println("报错信息：");
            Content = "报表下载失败，原因是:" + tFenFinDayBQPremExcelUI.mErrors.getFirstError();
            FlagStr = "Fail";
		 }
	}
	System.out.println("返回Save页面开始---");     
	String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
	File file = new File(tOutXmlPath);
	
	response.reset();
	response.setContentType("application/octet-stream"); 
	response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
	response.setContentLength((int) file.length());
	
	byte[] buffer = new byte[10000];
	BufferedOutputStream output = null;
	BufferedInputStream input = null;    
	//写缓冲区
	try 
	{
	    output = new BufferedOutputStream(response.getOutputStream());
	    input = new BufferedInputStream(new FileInputStream(file));
	
	int len = 0;
	System.out.println("-**-下载X4文件开始-**-");
	while((len = input.read(buffer)) >0)
	{
	    output.write(buffer,0,len);
	}
	System.out.println("-**-下载X4文件完成-**-");
	input.close();
	output.close();
	}
	catch (Exception e) 
	{
	  e.printStackTrace();
	 } // maybe user cancelled download
	finally 
	{
	    if (input != null) input.close();
	    if (output != null) output.close();
	    file.delete();
	}
	if (!FlagStr.equals("Fail"))
	{
	  Content = "打印报表成功！";
	  FlagStr = "Succ";
	}
%>
<html>
   <script language="javascript">  
    alert('<%=Content %>');
    top.close();
   </script>
</html>