<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%
    boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	XmlExport txmlExport = null;   
	GlobalInput tG = (GlobalInput)session.getValue("GI");

	String tStartDate = request.getParameter("StartDate"); //起始日
	String tEndDate = request.getParameter("EndDate");  //截止日
	String tOrderSql = request.getParameter("OrderSql");//；排序语句
    System.out.println(tStartDate);
	System.out.println(tEndDate);
    System.out.println(tOrderSql);

    TransferData tTransferData= new TransferData();
	tTransferData.setNameAndValue("StartDate",tStartDate);
	tTransferData.setNameAndValue("EndDate",tEndDate);
	tTransferData.setNameAndValue("OrderSql",tOrderSql);

	VData tVData = new VData();
    tVData.addElement(tG);
	tVData.addElement(tTransferData);
          
    GrpPPolPauseListPrintUI tGrpPPolPauseListPrintUI = new GrpPPolPauseListPrintUI(); 
    if(!tGrpPPolPauseListPrintUI.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tGrpPPolPauseListPrintUI.mErrors.getErrContent();                
    }
    else
    {    
		VData mResult = tGrpPPolPauseListPrintUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	System.out.println(operFlag);
	if (operFlag==true)
	{
		session.putValue("PrintStream", txmlExport.getInputStream());
		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
	}
	else
	{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>