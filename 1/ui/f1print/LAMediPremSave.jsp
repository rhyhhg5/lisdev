
<%
	//程序名称：LAMediPremSave.jsp
	//程序功能：F1报表生成
	//创建日期：2007-11-21
	//创建人  ：shaoax
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="java.io.*"%>

<body>
	<%
		System.out.println("=======LAMediaPremSave===========");
		//标志变量及错误类
		boolean operFlag = true;
		String flag = "0";
		String FlagStr = "";
		String Content = "";
		CError cError = new CError();
		CErrors tError = null;

		XmlExport txmlExport = new XmlExport();
		CombineVts tcombineVts = null;
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
		
		//获得输入参数
		String tManageCom = tG.ManageCom;
		String ManageCom = request.getParameter("ManageCom");
		String tStartDate = request.getParameter("StartDate");
		String tEndDate = request.getParameter("EndDate");
		String tRiskCode = request.getParameter("RiskCode");
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^"+tRiskCode);
		System.out.println("=======LAMediaPremSave111111===========");
		//为向后台传输做准备
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("tManageCom", ManageCom);
		tTransferData.setNameAndValue("tStartDate", tStartDate);
		tTransferData.setNameAndValue("tEndDate", tEndDate);
		tTransferData.setNameAndValue("tRiskCode", tRiskCode);

		VData tVData = new VData();
		tVData.addElement(tG);
		tVData.addElement(tTransferData);

		LAMediPremUI tLAMediPremUI = new LAMediPremUI();
		System.out.println("Start 后台处理...");

		try {
			//提交数据
			if (!tLAMediPremUI.submitData(tVData, "PRINT")) {
				operFlag = false;
				Content = tLAMediPremUI.mErrors.getFirstError();
				System.out.println(Content);
			}
			VData mResult = tLAMediPremUI.getResult();
			System.out.println("=======LAMediaPremSave2222222===========");
			txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);
			if (txmlExport == null) {
				System.out.println("txmlExport == null");
			}
			System.out.println("开始打开报表!");
			ExeSQL tExeSQL = new ExeSQL();
			
			//获取临时文件名
			String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
			String strFilePath = tExeSQL.getOneValue(strSql);
			String strVFFileName = strFilePath + tG.Operator + "_"
			+ FileQueue.getFileName() + ".vts";
			
			//获取存放临时文件的路径
			String strRealPath = application.getRealPath("/").replace('\\','/');
			String strVFPathName = strRealPath + "//" + strVFFileName;

			//合并VTS文件
			String strTemplatePath = application.getRealPath("f1print/picctemplate/")+ "/";
			tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
			System.out.println("开始1!");
			ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
			tcombineVts.output(dataStream);
			System.out.println("开始2!");
			
			//把dataStream存储到磁盘文件
			System.out.println("存储文件到" + strVFPathName);
			AccessVtsFile.saveToFile(dataStream, strVFPathName);
			System.out.println("==> Write VTS file to disk ");
			System.out.println("===strVFFileName : " + strVFFileName);
			
			//传递文件路径
			response.sendRedirect("../uw/GetF1PrintJ1.jsp?Code=07&RealPath="+ strVFPathName);
		} catch (Exception ex) {
		
			Content = "失败，原因是:" + ex.toString();
			FlagStr = "Fail";
			tError = tLAMediPremUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 处理失败! ";
				FlagStr = "Succ";
			} else {
				Content = " 操作失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
	%>

<script language="javascript">	
	alert("<%=Content%>");
	top.close();	
	//window.opener.afterSubmit("<%=FlagStr%>","<%=Content%>");		
</script>
</html>
<%
}
%>


