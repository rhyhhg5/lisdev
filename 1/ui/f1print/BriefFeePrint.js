//               该文件中包含客户端需要处理的函数和事件
var arrDataSet;
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try 
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  //var i = 0;
  //var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //fm.hideOperate.value=mOperate;
  //if (fm.hideOperate.value=="")
  //{
  //  alert("操作控制数据丢失！");
  //}
  //showSubmitFrame(mDebug);
  //alert (verifyInput());
  if(verifyInput()) 
  {
  	if(fm.StartNo.value==""){
  		alert("未在单证管理系统中对业管部发票打印用户发放发票号段动作,不能进行发票打印!");
  		return false;
  	}
  	fm.submit(); //提交
  }
  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, tCode )
{
	if (tCode!=null||tCode!="")
	{
		freshNum();
	}
	else
	{
		showInfo.close();
	  if (FlagStr == "Fail" )
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }
	  else
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
	  	//parent.fraInterface.initForm();
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	
	    showDiv(operateButton,"true");
	    showDiv(inputButton,"false");
	    //执行下一步操作
	  }
	}
}

/***************************************************************
*		程序功能:打印后更新主页面单证序号
*
****************************************************************/
function freshNum() 
{	
		
		var strSQL = "";
		if (fm.MngCom.value.length<=4)
		{
			strSQL = "select min(StartNo) from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='A"+fm.MngCom.value+"' and StateFlag='0'";
		}
		else
		{
			strSQL = "select min(StartNo) from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='B"+fm.Operator.value+"' and StateFlag='0'";
		}
		
		var arrResult = easyExecSql(strSQL);
		
		if (arrResult==null||arrResult=="")
		{
			
			alert("没有可用发票！"); 
		} 
		else 
		{
			fm.StartNo.value=arrResult; 
		}
}

// 查询按钮
function easyQueryClick()
{	

	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
			
//	strSQL = "select contno,prtno,prem,Operator,ManageCom,AgentCode from lccont where 1=1 "			 
//	        +"and appflag in ('1','9')"
//				  + getWherePart('ManageCom','MngCom','like')
//				  + getWherePart('contno','ContNo')
//          + getWherePart('prtno','PrtNo')
//          +" and cardflag = '6' and conttype='1' with ur";
    strSQL = "select distinct lcc.contno,lcc.prtno,lcc.prem,lcc.Operator,lcc.ManageCom, getUniteCode(lcc.agentcode)"         
    	//+ " (select groupagentcode from laagent where agentcode = lcc.AgentCode) "
        + " from lccont lcc "
        + " left join loprtmanager2 lpm on lpm.otherno = lcc.contno "
    	+ " and lpm.othernotype = '05' and lpm.code = '35' "
        + " where cardflag = '6' and appflag = '1' and conttype='1' "
        + getWherePart('lcc.ManageCom','MngCom','like')
        + getWherePart('lcc.contno','ContNo')
        + getWherePart('lcc.prtno','PrtNo')
        + " group by lcc.contno,lcc.prtno,lcc.prem,lcc.Operator,lcc.ManageCom,lcc.AgentCode ";
    if(PrtStateFlag == "0")
    {
        strSQL += " having max(lpm.stateflag) = '0' or max(lpm.stateflag) is null ";
    }
    else if(PrtStateFlag == "1")
    {
        strSQL += " having max(lpm.stateflag) = '1' "
    }
    strSQL += " order by lcc.contno desc "
        + " with ur ";
//	if(RePrintFlag == "1") //RePrintFlag记录是否重打
//	{
//		strSQL = "select PayNo,IncomeNo,IncomeType,SumActuPayMoney,PayDate,ConfDate,Operator,a.ManageCom,a.AgentCode from LJAPay a where 1=1 ";			 
//				 //+ getWherePart( 'PayNo' )
//		if (fm.PrtNo.value!=null && fm.PrtNo.value != "")
//		{
//			if (fm.IncomeType.value == "1")
//			{
//				strSQL = strSQL + " and incomeno in (select grpcontno from lcgrpcont where prtno = '"+fm.PrtNo.value+"')";
//			}
//			else if (fm.IncomeType.value == "2") 
//			{ 
//			  strSQL = strSQL + " and incomeno in (select contno from lccont where prtno = '"+fm.PrtNo.value+"')";
//		  }
//		}
//	if(fm.IncomeType.value == "3")
//	{			
//		strSQL = strSQL + " and IncomeType in('3','13') ";
//	}
//	else
//	{	
//		strSQL = strSQL + getWherePart( 'IncomeType' );
//	}	 
//		strSQL = strSQL + getWherePart( 'IncomeNo' )
//					 + getWherePart( 'EnterAccDate' )				 
//					 + getWherePart( 'AgentCode' )
//					 +" and payno in (select otherno from LOPRTManager2 b where code='35' and stateflag='1')";
//					 	
//		if (fm.MngCom.value == null || fm.MngCom.value == "" )
//		{	
//		}	
//		else
//		{
//			strSQL = strSQL + getWherePart( 'a.ManageCom','MngCom','like' );
//		}		
//		 strSQL = strSQL + "order by IncomeNo,PayDate,ConfDate";
//	}
	fm.SQLstr.value = strSQL;
	//查询SQL，返回结果字符串
  // turnPage.strQueryResult  = easyQueryVer3(strSQL);
  turnPage.queryModal(strSQL, PolGrid);
    

  //判断是否查询成功
  if (turnPage.strQueryResult == null || !turnPage.strQueryResult) {
  	PolGrid.clearData();
    //alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  //turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  //turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  //turnPage.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);   
}

//发票打印
function PPrint()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击发票打印按钮。" );
	else
	{
		//arrReturn = getQueryResult();
	  var cConotNo = PolGrid.getRowColData(tSel - 1,1);		
		if (cConotNo == "")
		    return;
		}  
	fm.ContNo.value = cConotNo
	
	//fm.IncomeType.value = arrReturn[0][2];
  	if(RePrintFlag == "1")
  {
  	fm.action="BriefFeePrintSave.jsp?RePrintFlag=1";
 	  fm.target="f1print";
	  fm.fmtransact.value="CONFIRM";
	  submitForm();
  }
  	else
  {
	  fm.action="BriefFeePrintSave.jsp";
	  fm.target="f1print";
	  fm.fmtransact.value="CONFIRM";
	  submitForm();
	}

}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function showAppntInfo() {
  var strSql = "";
  if (fm.IncomeType.value == "1" )
  {
  	strSql = "select distinct GrpName,RiskName from lcgrppol a,lmriskapp b where GrpContNo = '" 
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2) 
             + "' and a.riskcode=b.riskcode";
  }
  if (fm.IncomeType.value == "2" )
  {
    strSql = "select distinct c.AppntName,RiskName from lcpol a,lmriskapp b,lccont c where c.ContNo = a.ContNo " 
             + "and a.ContNo = '"+PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2) 
             + "' and a.riskcode=b.riskcode";     
  }        
  if (fm.IncomeType.value == "3" )
  {
  	if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3) == "3")
  	{
    strSql = "select distinct b.GrpName,RiskName from LJAGetEndorse a, lcgrppol b, lmriskapp c where ActuGetNo = '" 
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1) 
             + "' and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode";     
    }
    if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3) == "13")
    {       
    	strSql = "Select distinct b.GrpName,RiskName from ljagetendorse a,lcgrppol b,lmriskapp c "
             +"where actugetno in ( Select Btactuno from ljaedorbaldetail "
             +"where actuno in ( Select getnoticeno from LJAPay where payno='"
             +PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1)+"' )) "
             +"and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode"
    }           
  }             
  if (fm.IncomeType.value == "10" )
  {             
    strSql = "select distinct AppntName,RiskName from LJAGetEndorse a, lcpol b, lmriskapp c where ActuGetNo = '"
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1) 
             + "' and a.contno=b.contno and b.riskcode=c.riskcode";          
  }                
 //alert(strSql);      
  var arrResult = easyExecSql(strSql);
  
  fm.all("AppntName").value = arrResult[0][0];
  if (arrResult.length>1)
  {
  fm.all("RiskName").value = arrResult[0][1]+"等";
  }
  else
  	{
  		fm.all("RiskName").value = arrResult[0][1];
  	}
  
}
