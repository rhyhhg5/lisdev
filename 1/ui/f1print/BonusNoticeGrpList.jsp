   <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%

  GlobalInput tG = (GlobalInput)session.getAttribute("GI");
  String PrtSeq  = request.getParameter("PrtSeq2");	
  String GrpPolNo  = request.getParameter("GrpPolNoForList");
  String FiscalYear= request.getParameter("FiscalYearForList");
  LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema=new LOBonusGrpPolParmSchema();
  tLOBonusGrpPolParmSchema.setGrpPolNo(GrpPolNo);
  tLOBonusGrpPolParmSchema.setFiscalYear(FiscalYear);
  
  CError cError = new CError( );

  VData tVData = new VData();
  VData mResult = new VData();

  tVData.addElement(tG);
  tVData.addElement(tLOBonusGrpPolParmSchema);

  String strErrMsg = "";
  boolean Flag=true;

  BonusNoticeGrpPolListUI tBonusNoticeGrpPolListUI = new BonusNoticeGrpPolListUI();
  if(!tBonusNoticeGrpPolListUI.submitData(tVData,"CONFIRM"))
  {
    if( tBonusNoticeGrpPolListUI.mErrors.needDealError() )
    {
      Flag=false;
      strErrMsg = tBonusNoticeGrpPolListUI.mErrors.getFirstError();
    }
     else 
    {
      strErrMsg = "tBonusNoticeGrpPolListUI发生错误，但是没有提供详细的出错信息";
    }

%>
  <script language="javascript">
   alert('<%= strErrMsg %>');
     window.opener = null;
     window.close();
  </script>
<%
  return;
  }

  mResult = tBonusNoticeGrpPolListUI.getResult();

  XmlExport txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);

  if (txmlExport==null) {
    System.out.println("null");
  }  
  session.setAttribute("PrintNo",PrtSeq );
  session.setAttribute("PrintType","34" );
  session.setAttribute("PrintStream", txmlExport.getInputStream());
  System.out.println("put session value");
  response.sendRedirect("GetF1Print.jsp");
%>