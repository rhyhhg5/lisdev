//
//程序名称：BankDataQuery.js
//程序功能：财务报盘数据查询
//创建日期：2004-10-20
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容

//var arrDataSet;
var turnPage = new turnPageClass();

//简单查询
function easyQuery()
{
	var sDate = fm.all('SendDate').value;
	var sEndDate = fm.all('SendEndDate').value;
	var sBankSucc = fm.all('BankSuccFlag').value;
	var strSqlCon = "";
	
	if (sDate == "")
	{
			alert("请输入发盘起期！");
			return;
	}
	if (sEndDate == "")
	{
			alert("请输入发盘止期！");
			return;
	}
	strSqlCon = " and notype='1' and Dealtype='S'"
	// 书写SQL语句
	if (sBankSucc=='1')
	{
		var strSQL = " select polno,AccName,comcode,BankCode,SendDate,PayMoney,'成功','' "
				+ " from LYReturnFromBankB"
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('comcode','ManageCom','=','0')
				+ getWherePart('bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and BankSuccFlag =(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = LYReturnFromBankB.bankcode ) "
				+ " and SendDate >='" + sDate + "'"
				+ " and SendDate <='" + sEndDate + "'"	
				+ strSqlCon			
				+ " order by polno";
	}else if (sBankSucc=='')
	{
		var strSQL = " (select polno,AccName,comcode,BankCode,SendDate,PayMoney,'成功','' "
				+ " from LYReturnFromBankB "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('comcode','ManageCom','=','0')
				+ getWherePart('bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and BankSuccFlag =(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = LYReturnFromBankB.bankcode ) "
				+ " and SendDate >='" + sDate + "'"
				+ " and SendDate <='" + sEndDate + "'"
				+ strSqlCon	
				+ " union all "
				+  " select polno,AccName,comcode,BankCode,SendDate,PayMoney,'未成功',"
				+ " (select CodeName from ldcode1 where codetype='bankerror' and code=BankCode and code1=BankSuccFlag)"
				+ " from LYReturnFromBankB "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('comcode','ManageCom','=','0')
				+ getWherePart('bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and (BankSuccFlag <>(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = LYReturnFromBankB.bankcode )  or BankSuccFlag is null )"
				+ " and SendDate >='" + sDate + "'"
				+ strSqlCon	
				+ " and SendDate <='" + sEndDate + "')"
				
				;
	}else
	{
		var strSQL = " select polno,AccName,comcode,BankCode,SendDate,PayMoney,'未成功', "
		        + " (select CodeName from ldcode1 where codetype='bankerror' and code=BankCode and code1=BankSuccFlag)"
		        + " from LYReturnFromBankB "
				+ " where 1=1 "
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('comcode','ManageCom','=','0')
				+ getWherePart('bankcode','BankCode','=','0')
				+ getWherePart('SerialNo','SerialNo')				
				+ " and (BankSuccFlag <>(select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) from ldbank where  cansendflag = '1' and bankcode = LYReturnFromBankB.bankcode )  or BankSuccFlag is null )"
				+ " and SendDate >='" + sDate + "'"
				+ " and SendDate <='" + sEndDate + "'"
				+ strSqlCon	
				;
	}
	turnPage.queryModal(strSQL, CodeGrid);  
	showCodeName(); 

}

function easyPrint()
{
	easyQueryPrint(2,'CodeGrid','turnPage');
}