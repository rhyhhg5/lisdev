//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
 if (verifyInput() == false)
    return false;
  if(!beforeSubmit())
  {
    return false;
  }
  var i = 0;
  //alert("a");
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
	showInfo.close();
}
function checkAgentCode(){
var sql=" select agentcode  from laagent   where  groupagentcode='"+fm.GroupAgentCode.value+"' "
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
}
function beforeSubmit()
{
	//录入机构不能为中介，中介不在此统计范围内
	
	if(fm.all('BranchAttr').value!=null&&fm.all('BranchAttr').value!="")
	{
	  var sSQL = "select branchlevel,state from labranchgroup where branchattr='"+fm.all('BranchAttr').value+"' and branchtype='2'";
	   var strQueryResult  = easyQueryVer3(sSQL, 1, 1, 1);
	 //  alert(sSQL);	 
      
       if(!strQueryResult)
      {
        alert("系统不存在该机构！");
        return false;
      } 
      else
      {
       var arr = decodeEasyQueryResult(strQueryResult);
       if(arr[0][0]=='31')
       {
         alert("中介机构不在统计范围内，请重新输入！");
         return false;
       }
       if(arr[0][1]=='1')
       {
         alert("公司业务不在统计范围内!");
         return false;
       }
       }
	}
	if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!="")
	{
	 //  alert("aa");
	    var sSQL = "select branchtype2 from laagent where agentcode='"+fm.all('AgentCode').value+"' and branchtype='2'";
	   var strQueryResult  = easyQueryVer3(sSQL, 1, 1, 1);
	   if(!strQueryResult)
	   {	  
         alert("系统不存在该代理人！");
         return false;
       }
       else
       {
       var arr1 = decodeEasyQueryResult(strQueryResult);
       if(arr1[0][0]=='02')
       {
         alert("中介人员不在统计范围内，请重新输入！");
         return false;
       }
       }
	}
	return true;	
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
  }
}

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  showSubmitFrame(mDebug); 
  fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

                                                                                                                   