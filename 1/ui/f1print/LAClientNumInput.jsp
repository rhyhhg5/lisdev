<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LAClientNumInput.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LAClientNumInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();">    
  <form action="./LAClientNum.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify="管理机构|NOTNULL" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName readOnly elementtype=nacessary> 
          </TD>
          </TR>         
          <TR  class=common>              
          <TD  class= title>
            销售单位代码 
          </TD>
          <TD  class= input>
            <Input  class=common  name=BranchAttr onchange="return checkBranchAttr()">
          </TD> 
          <TD  class= title>
            业务员代码  
          </TD>
          <TD  class= input>
            <Input  class=common  name=GroupAgentCode onchange="return checkAgentCode()">
          </TD> 
           
        </TR>
        <TR  class=common>              
          <TD  class= title>
            业务员状态 
          </TD>
          <TD  class= input>
           <Input class='codeno' 　 name=AgentState    verify="状态|code:AgentState" 
           CodeData="0|^01|在职|^02|离职|^03|全体" ;
            
             ondblClick="   showCodeListEx('AgentState',[this,StateName],[0,1]) ; "
             onkeyup="      showCodeListKeyEx('AgentState',[this,StateName],[0,1]) ; "
          ><Input class=codename name=StateName >  
          </TD>
          <TD  class= title>
            业务员属性  
          </TD>
          <TD  class= input>
           <Input class='codeno' 　 name=AgentKind    verify="属性|code:BranchOrigin2" 
           CodeData="0|^01|正常业务员|^02|公司业务代码" ;
            
             ondblClick="   showCodeListEx('BranchOrigin2',[this,AgentKindName],[0,1]) ; "
             onkeyup="      showCodeListKeyEx('BranchOrigin2',[this,AgentKindName],[0,1]) ; "
          ><Input class=codename name=AgentKindName >  
          </TD> 
           
        </TR>
          <TR  class= common>
          <TD  class= title>
             查询起期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=StartDate  verify="起期|NOTNULL" elementtype=nacessary  >  
          </TD>
          <TD  class= title>
             查询止期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="止期|NOTNULL" elementtype=nacessary >  
          </TD>  
  </TR>
    </table>
	<input type=hidden class=Common name=querySql >
    <input type="hidden" name=op value="">
    <input type=hidden name=AgentCode value=''>
		<INPUT VALUE="查  询" class="cssButton" TYPE="button" onclick="submitForm()">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 