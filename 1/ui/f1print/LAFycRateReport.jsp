<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PLPsqs.jsp--打印续期保单
//程序功能：
//创建人  ：songgh
//创建日期：2003-02-14
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
  <%@page import="com.sinosoft.lis.brieftb.*"%>
  <%@page import="java.io.*"%>
<%
	  System.out.println("开始执行打印操作");
	  String Content = "";
	  CErrors tError = null;
	  String FlagStr = "Fail";
	  boolean operFlag=true;

  	String mManageCom = request.getParameter("ManageCom");
		String mBranchAttr  = request.getParameter("BranchAttr");//传入外部编码 
		String mAgentCode 	 = request.getParameter("AgentCode");
		String mStartMonth = request.getParameter("StartMonth");
		String mEndMonth = request.getParameter("EndMonth");
		String mState = request.getParameter("State");
  	GlobalInput tG = new GlobalInput();	
	  tG=(GlobalInput)session.getValue("GI");
  	CErrors mErrors = new CErrors();
  	
  	LAFycRateReportBL tLAFycRateReportBL = new LAFycRateReportBL();
  	XmlExport txmlExport = new XmlExport();

    VData tVData = new VData();
  	VData mResult = new VData();
  	try{		    
		    tVData.addElement(mManageCom);
		    tVData.addElement(mBranchAttr);
		    tVData.addElement(mAgentCode);
		    tVData.addElement(mStartMonth);
		    tVData.addElement(mEndMonth);
		    tVData.addElement(mState);
		    tVData.addElement(tG);
		    
    
		    //调用批单打印的类
		    if (!tLAFycRateReportBL.submitData(tVData,"PRINT")){
			      operFlag=false;
			      Content=tLAFycRateReportBL.mErrors.getFirstError().toString();    
		    }else{
		    		mResult = tLAFycRateReportBL.getResult();
			      txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
			      if(txmlExport==null){
			      		operFlag=false;
			      		Content="没有得到要显示的数据文件";
			      }
		    }
 	  }catch(Exception ex){
    		Content = "打印失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
  	}
  	
  	
		ExeSQL tExeSQL = new ExeSQL();
		//获取临时文件名
		String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
		String strFilePath = tExeSQL.getOneValue(strSql);
		String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
		//获取存放临时文件的路径
		String strRealPath = application.getRealPath("/").replace('\\','/');
		String strVFPathName = strRealPath +"/"+ strVFFileName;
		CombineVts tcombineVts = null;
		
		if (operFlag==true){
			//合并VTS文件
			String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
			tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
			ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
			tcombineVts.output(dataStream);
			//把dataStream存储到磁盘文件
			AccessVtsFile.saveToFile(dataStream,strVFPathName);
			System.out.println("==> Write VTS file to disk ");		
			System.out.println("===strVFFileName : "+strVFFileName);
			//本来打算采用get方式来传递文件路径
			response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="+strVFPathName);
		}else{
			FlagStr = "Fail";
%>
		<html>
		<script language="javascript">
			alert("<%=Content%>");
			top.opener.focus();
			top.close();				
		</script>
		</html>
<%
		}
%>