//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	 
   if (verifyInput() == false)
    return false;
      	 
    if( !beforeSubmit())
   {
   	return false;
   	
   	}	

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function beforeSubmit()
{
 
	return true;
	
}


function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function easyQueryClick() 
{

 　if (verifyInput() == false)
     return false;
  //此处书写SQL语句			     
  var tReturn = parseManageComLimitlike();
  	 
  var tManageCom=fm.all('ManageCom').value;
  var tAgentCode=fm.all('AgentCode').value;
   
  var tIndexCalNo=fm.all('AssessYear').value+fm.all('AssessMonth').value;
  
   
  var tSQL = "";

    tSQL  = "select a,b,c,d,e,f,g,h,i,value(j,0),value(m,0), case when value(n,0)< 0 then 0 else value(n,0) end ,"
    +"value(k,0),case when value(l,0)< 0 then 0 else value(l,0) end,value(o,0),value(p,0),"
    +"case when value(t,0)< 0 then 0 else value(t,0) end,value(u,0),value(w,0),case when value(v,0)< 0 then 0 else value(v,0) end "
    + "  from ( "
    + "select getUniteCode(agentcode) a ,"
    +"(select name from laagent b where a.agentcode=b.agentcode) b,"
    + "branchattr c,"
    +"(select name from labranchgroup b where a.agentgroup=b.agentgroup) d,"
    +"(select getUniteCode(branchmanager) from labranchgroup b where a.agentgroup=b.agentgroup) e,"
    +"(select branchmanagername from labranchgroup b where a.agentgroup=b.agentgroup) f,"
    +" agentgrade g,"
    +"(select a.startdate from latree b where a.agentcode=b.agentcode) h,"
    +"(select b.employdate from laagent b where a.agentcode=b.agentcode) i,"
    +"IndFYCSum j,"  // 个人累计FYC
    +"decimal(getfycstand(a.AgentGrade,'D',a.ManageCom)/3*T33,12,2) m," //维持标准
    +"decimal(getfycstand(a.AgentGrade,'D',a.ManageCom)/3*T33,12,2)-value(IndFYCSum,0)  n,"//维持差额
    +"decimal(getfycstand(a.AgentGrade,'U',a.ManageCom)/3*T33,12,2) k,"        // 晋升标准
    +"decimal(getfycstand(a.AgentGrade,'U',a.ManageCom)/3*T33,12,2)-(select b.IndFYCSum from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') l,"//晋升差额
    +"(select value(b.DInRCount,0) from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') o,"//任职月数
    +"2 p,"//任职月数标准
    +"(select 2-value(b.DInRCount,0) from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') t," //晋升差额
    //+"(select value(b.T51*100,0)-value(b.T21,0) from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') u ," 
    +"(select value(b.T51*100,0)  from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') u," //季度继续率 
    +" 75 w,"
    +"(select 75-value(b.T51*100,0)  from laindexinfo b where a.agentcode=b.agentcode and a.indexcalno=b.indexcalno and b.indextype='03') v "
    +" from laindexinfo a where indextype='02'  and IndexCalno='"+tIndexCalNo+"'"    ;
        
    tSQL += "  and AgentGrade>='A02' and AgentGrade<'B01' and branchtype='1' and  branchtype2='01' " 
     + tReturn
     + getWherePart('ManageCom','ManageCom','like')
//     + getWherePart('AgentCode','AgentCode' )
//   modify lyc 2014-11-27 统一工号
     if(fm.all("AgentCode").value!=""){
  	   tSQL +=" and AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
     }     
    tSQL += " order by ManageCom,BranchAttr,AgentCode ) as x ";
   // alert(tSQL);
   turnPage.queryModal(tSQL, AgentQueryGrid,1);
  if (!turnPage.strQueryResult) 
  {
    alert("没有符合条件的查询信息！");
    return false;
  }
}

function afterCodeSelect( cCodeName, Field )
{
　	  
  if(cCodeName=="comcode"){
  	 fm.all("BranchAttr").value="";
  	 fm.all("BranchName").value="";
  	if(fm.all("ManageCom").value=='86' )
  	{
  		fm.all("BranchAttr").disabled=true;
  	}
       else
       	{
       		var tManageCom=fm.all("ManageCom").value;
       		fm.all("BranchAttr").disabled=false;
       		msql1=" 1 and   branchtype=#2#  and branchtype2=#01#  and managecom like #"+tManageCom+"%# and endflag=#N#";
       	}
}
 if(cCodeName=="branchattr")
 {
   if(fm.all("ManageCom").value==null || fm.all("ManageCom").value=='')
    {
      alert("请先录入管理机构");
      fm.all("BranchAttr").value='';
       fm.all("BranchName").value='';
       return false;
  
     }
}
}        
                                                                                                                   