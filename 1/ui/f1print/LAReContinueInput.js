//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();    
var showInfo;
var mDebug="0";


function checkBranchAttr()
{
 if(fm.all("ManageCom").value==null || fm.all("ManageCom").value=='')
 {
   alert("请先录入管理机构");
   fm.all("BranchAttr").value='';
   return false;
 }
 var sql=" select agentgroup  from labranchgroup  where branchattr='"+fm.BranchAttr.value+"'  and  branchtype='1'	"
           +" and branchtype2='01'  and endflag='N'  "
           + getWherePart("ManageCom","ManageCom",'like')	;
     var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
 // alert(sql);
   if (!strQueryResult) 
    {
      alert("此管理机构没有此销售单位！");  
      fm.BranchAttr.value="";
      return;
    }	
    var arr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentGroup').value=arr[0][0];
    
    return true;
}	


//提交，保存按钮对应操作按保单层
function submitForm() 
{
　if (verifyInput() == false)
    return false;
//  var tEmployStartDate = fm.all('EmployeeStartDate').value;
//  var tEmployEndDate= fm.all('EmployeeEndDate').value;
//  var tDissStartDate = fm.all('DissStartDate').value;
//  var tDissEndDate = fm.all('DissEndDate').value;
//  var appendStrSql =""; 
//  if(fm.all('AgentState').value=='0')
//  {
//	  if((tEmployStartDate==null || tEmployStartDate=='')||(tEmployEndDate==null || tEmployEndDate==''))
//	  {
//		  alert("请输入业务员的入职的起止时间段！");
//		  return false;
//	  }
//	       var tMonth = dateDiff(tEmployStartDate,tEmployEndDate,"M");
////		  alert(tMonth);
//			if(3<tMonth)
//			{
//				alert("因为数据量较大，故请将查询间隔控制在3个月内，谢谢！");  
//				return false;
//			}  
//			appendStrSql=" and a.agentstate in ('01','02','03','04')  and a.employdate between '"+tEmployStartDate+"' and '"+tEmployEndDate+"' "
//  }
//  if(fm.all('AgentState').value=='1')
//  {
//	  if((tDissStartDate==null || tDissStartDate=='')||(tDissEndDate==null || tDissEndDate==''))
//	  {
//		  alert("请输入业务员的离职的起止时间段！");
//		  return false;
//	  }
//		  var tMonth = dateDiff(tDissStartDate,tDissEndDate,"M");
//			if(3<tMonth)
//			{
//				alert("因为数据量较大，故请将查询间隔控制在3个月内，谢谢！");  
//				return false;
//			}
//			appendStrSql=" and a.agentstate in ('05','06') and a.outworkdate between '"+tDissStartDate+"' and '"+tDissEndDate+"' "
//  }
  //此处书写SQL语句	
  //个单正常承保
  strSql = "select b.contno,b.appntname,b.cvalidate, a.groupagentcode,"
+"a.name,a.outworkdate,a.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup),"
+"(select f.mobile from lcaddress f where f.customerno = b.appntno  "
+"and  f.addressno= (select addressno from lcappnt where contno = b.contno )), "
+"(select g.postaladdress from lcaddress g where g.customerno = b.appntno "
+" and g.addressno=(select addressno from lcappnt where contno =b.contno)), "
+" (select f.phone from lcaddress f where f.customerno = b.appntno  "
+"and  f.addressno=(select addressno from lcappnt where contno =b.contno)), "
+" (case b.appntsex  when '0' then '男'  else '女' end), "
+"  b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' "
+"  then '失效中止' else '终止' end) ,'个单',(select char(payyears) from lcpol where contno = b.contno fetch first 1 rows only ) "
+"  from laagent a,lccont b  "
+"  where a.agentcode=b.agentcode "
+"  and a.branchtype='1' and a.branchtype2='01'  "
+"  and b.stateflag <> '0' and b.grpcontno ='00000000000000000000' "
   + getWherePart('b.ManageCom', 'ManageCom','like')
  	    + getWherePart('b.ContNo', 'ContNo')
  	    + getWherePart('a.GroupAgentCode', 'AgentCode')
  	    + getWherePart('a.Name', 'Name')
  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
  {
	  strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
  }
//	  strSql+=appendStrSql;

	  //个单CT,WT,XT保单
	  strSql=strSql+" union "
+"select b.contno,b.appntname,b.cvalidate, a.groupagentcode,"
+"a.name,a.outworkdate,a.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup),"
+"(select f.mobile from lcaddress f where f.customerno = b.appntno  "
+"and  f.addressno=(select addressno from lcappnt where contno = b.contno)), "
+"(select g.postaladdress from lcaddress g where g.customerno = b.appntno "
+" and g.addressno=(select addressno from lcappnt where contno = b.contno)), "
+" (select f.phone from lcaddress f where f.customerno = b.appntno  "
+"and  f.addressno=(select addressno from lcappnt where contno = b.contno)) , "
+" (case b.appntsex  when '0' then '男'  else '女' end), "
+"  b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' "
+"  then '失效中止' else '终止' end) ,'个单',(select char(payyears) from lbpol where contno = b.contno fetch first 1 rows only ) "
+"  from laagent a,lbcont b  "
+"  where a.agentcode=b.agentcode "
+"  and a.branchtype='1' and a.branchtype2='01'  "
+" and b.stateflag<>'0'  "
+" and exists (select 1  from lpedoritem lpe, lpedorapp lpa where lpe.edorno = lpa.EdorAcceptNo "
+"  and lpe.contno = b.contno and lpe.edortype in ('XT', 'CT', 'WT') and lpa.edorstate = '0' and lpe.edorno = b.edorno)  "
+ getWherePart('b.ManageCom', 'ManageCom','like')
  + getWherePart('b.ContNo', 'ContNo')
  + getWherePart('a.GroupAgentCode', 'AgentCode')
  + getWherePart('a.Name', 'Name')
if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
{
strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
}
//strSql+=appendStrSql;
//
//  //正常团单
//  strSql=strSql+" union "
//  +" select "
//  +" b.grpcontno,b.grpname,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,a.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup),"
//  +" '-',(select g.grpaddress from lcgrpaddress g where g.addressno = b.addressno fetch first 1 rows only),(select g.phone1 from lcgrpaddress g where g.addressno = b.addressno fetch first 1 rows only),"
//  +" '-',b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),"
//  +" '团单','-'"
//  +" from laagent a,lcgrpcont b  "
//  +" where a.agentcode = b.agentcode and a.branchtype ='1' and a.branchtype2='01' "
//  +" and b.stateflag <> '0' "
//  + getWherePart('b.ManageCom', 'ManageCom','like')
//  + getWherePart('b.GrpContNo', 'ContNo')
//  + getWherePart('a.GroupAgentCode', 'AgentCode')
//  + getWherePart('a.Name', 'Name')
//if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//{
//strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//}
//strSql+=appendStrSql;
//
////XT,CT,WT团单
//strSql=strSql+" union "
//+" select "
//+" b.grpcontno,b.grpname,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,a.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup),"
//+" '-',(select g.grpaddress from lcgrpaddress g where g.addressno = b.addressno fetch first 1 rows only),(select g.phone1 from lcgrpaddress g where g.addressno = b.addressno fetch first 1 rows only),"
//+" '-',b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),"
//+" '团单','-'"
//+" from laagent a,lbgrpcont b  "
//+" where a.agentcode = b.agentcode and a.branchtype ='1' and a.branchtype2='01' "
//+" and b.stateflag <> '0' and  exists (select 1 from lpgrpedoritem lpe, lpedorapp lpa where lpe.edorno = lpa.EdorAcceptNo "
//+"  and lpe.grpcontno = b.grpcontno and lpe.edortype in ('XT', 'CT', 'WT') and lpa.edorstate = '0' and lpe.edorno = b.edorno) "
//+ getWherePart('b.ManageCom', 'ManageCom','like')
//+ getWherePart('b.GrpContNo', 'ContNo')
//+ getWherePart('a.GroupAgentCode', 'AgentCode')
//+ getWherePart('a.Name', 'Name')
//if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//{
//strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//}
//strSql+=appendStrSql;
//
////归属个单
//strSql=strSql+" union "
//+" select b.contno,b.appntname,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup),"
//+"(select f.mobile from lcaddress f where f.customerno = b.appntno  "
//+"and  f.addressno=(select addressno from lcappnt where contno = b.contno)), "
//+"(select g.postaladdress from lcaddress g where g.customerno = b.appntno and g.addressno=(select addressno from lcappnt where contno = b.contno)), "
//+"(select f.phone from lcaddress f where f.customerno = b.appntno and  f.addressno=(select addressno from lcappnt where contno = b.contno)),  "
//+"(case b.appntsex  when '0' then '男'  else '女' end),b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' "
//+"  then '失效中止' else '终止' end),'个单',(select char(payyears) from lcpol where contno = b.contno fetch first 1 rows only) "
//+" from laagent a,lccont b,laascription p "
//+" where a.agentcode = b.agentcode and b.agentcode=p.agentnew and a.branchtype='1' and a.branchtype2='01' "
//+" and  b.contno = p.contno and b.stateflag<>'0' and p.ascriptioncount=1 "
//+ getWherePart('b.ManageCom', 'ManageCom','like')
//+ getWherePart('b.ContNo', 'ContNo')
//+ getWherePart('a.GroupAgentCode', 'AgentCode')
//+ getWherePart('a.Name', 'Name')
//if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//{
//strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//}
//strSql+=appendStrSql;
//
////归属退保个单
//strSql=strSql+" union "
//+" select b.contno,b.appntname,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup),"
//+"(select f.mobile from lcaddress f where f.customerno = b.appntno  "
//+"and  f.addressno=(select addressno from lcappnt where contno = b.contno)), "
//+"(select g.postaladdress from lcaddress g where g.customerno = b.appntno and g.addressno=(select addressno from lcappnt where contno = b.contno)), "
//+"(select f.phone from lcaddress f where f.customerno = b.appntno and  f.addressno=(select addressno from lcappnt where contno = b.contno)),  "
//+"(case b.appntsex  when '0' then '男'  else '女' end),b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' "
//+"  then '失效中止' else '终止' end),'个单',(select char(payyears) from lbpol where contno = b.contno fetch first 1 rows only) "
//+" from laagent a,lbcont b,laascription p "
//+" where a.agentcode = b.agentcode and b.agentcode=p.agentnew and a.branchtype='1' and a.branchtype2='01' "
//+" and  b.contno = p.contno and b.stateflag<>'0' and p.ascriptioncount=1 "
//+"and exists (select 1 from lpedoritem lpe, lpedorapp lpa where lpe.edorno = lpa.EdorAcceptNo "
//+" and lpe.contno = b.contno and lpe.edortype in ('XT', 'CT', 'WT') and lpa.edorstate = '0' and lpe.edorno = b.edorno) "
//+ getWherePart('b.ManageCom', 'ManageCom','like')
//+ getWherePart('b.ContNo', 'ContNo')
//+ getWherePart('a.GroupAgentCode', 'AgentCode')
//+ getWherePart('a.Name', 'Name')
//if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//{
//strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//}
//strSql+=appendStrSql;
//
////归属团单
//strSql=strSql+" union "
//+"  select b.grpcontno,b.grpname,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,a.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup),"
//+" '-',(select g.grpaddress from lcgrpaddress g where g.addressno = b.addressno fetch first 1 rows only),(select g.phone1 from lcgrpaddress g where g.addressno = b.addressno fetch first 1 rows only),"
//+" '-',b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),"
//+" '团单','-'"
//+" from laagent a,lcgrpcont b,laascription p "
//+" where a.agentcode =b.agentcode and a.agentcode =p.agentnew and b.grpcontno=p.grpcontno "
//+" and a.branchtype='1' and a.branchtype2='01' and b.stateflag<>'0' and p.ascriptioncount =1  "
//+ getWherePart('b.ManageCom', 'ManageCom','like')
//+ getWherePart('b.GrpContNo', 'ContNo')
//+ getWherePart('a.GroupAgentCode', 'AgentCode')
//+ getWherePart('a.Name', 'Name')
//if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//{
//strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//}
//strSql+=appendStrSql;
//
////归属团单退保 
//strSql=strSql+" union "
//+"  select b.grpcontno,b.grpname,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,a.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup),"
//+" '-',(select g.grpaddress from lcgrpaddress g where g.addressno = b.addressno fetch first 1 rows only),(select g.phone1 from lcgrpaddress g where g.addressno = b.addressno fetch first 1 rows only),"
//+" '-',b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),"
//+" '团单','-'"
//+" from laagent a,lbgrpcont b,laascription p "
//+" where a.agentcode =b.agentcode and a.agentcode =p.agentnew and b.grpcontno=p.grpcontno "
//+" and a.branchtype='1' and a.branchtype2='01' and b.stateflag<>'0' and p.ascriptioncount=1  and exists (select 1 from lpedoritem lpe, lpedorapp lpa where lpe.edorno = lpa.EdorAcceptNo "
//+" and lpe.grpcontno = b.grpcontno and lpe.edortype in ('XT', 'CT', 'WT') and lpa.edorstate = '0' and lpe.edorno = b.edorno)  "
//+ getWherePart('b.ManageCom', 'ManageCom','like')
//+ getWherePart('b.GrpContNo', 'ContNo')
//+ getWherePart('a.GroupAgentCode', 'AgentCode')
//+ getWherePart('a.Name', 'Name')
//if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//{
//strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//}
//strSql+=appendStrSql;

fm.querySql.value = strSql;

var i = 0;
var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//showSubmitFrame(mDebug); 
//fm.fmtransact.value = "PRINT";

	fm.all('op').value = '';
	fm.all('Flag').value = 'Cont';
	fm.submit();
	showInfo.close();
}

//提交，保存按钮对应操作按险种层
function submitFormR() 
{
	　if (verifyInput() == false)
	    return false;
//	  var tEmployStartDate = fm.all('EmployeeStartDate').value;
//	  var tEmployEndDate= fm.all('EmployeeEndDate').value;
//	  var tDissStartDate = fm.all('DissStartDate').value;
//	  var tDissEndDate = fm.all('DissEndDate').value;
//	  var appendStrSql =""; 
//	  if(fm.all('AgentState').value=='0')
//	  {
//		  if((tEmployStartDate==null || tEmployStartDate=='')||(tEmployEndDate==null || tEmployEndDate==''))
//		  {
//			  alert("请输入业务员的入职的起止时间段！");
//			  return false;
//		  }
//		       var tMonth = dateDiff(tEmployStartDate,tEmployEndDate,"M");
////			  alert(tMonth);
//				if(3<tMonth)
//				{
//					alert("因为数据量较大，故请将查询间隔控制在3个月内，谢谢！");  
//					return false;
//				}  
//				appendStrSql=" and a.agentstate in ('01','02','03','04')  and a.employdate between '"+tEmployStartDate+"' and '"+tEmployEndDate+"' "
//	  }
//	  if(fm.all('AgentState').value=='1')
//	  {
//		  if((tDissStartDate==null || tDissStartDate=='')||(tDissEndDate==null || tDissEndDate==''))
//		  {
//			  alert("请输入业务员的离职的起止时间段！");
//			  return false;
//		  }
//			  var tMonth = dateDiff(tDissStartDate,tDissEndDate,"M");
//				if(3<tMonth)
//				{
//					alert("因为数据量较大，故请将查询间隔控制在3个月内，谢谢！");  
//					return false;
//				}
//				appendStrSql=" and a.agentstate in ('05','06') and a.outworkdate between '"+tDissStartDate+"' and '"+tDissEndDate+"' "
//	  }
	
  //正常個單	  
  var strSql = "select b.contno,b.cvalidate ,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup  where agentgroup = b.agentgroup),b.appntname , "
  +" (select f.mobile from lcaddress f where f.customerno = b.appntno  and f.addressno = (select addressno from lcappnt where contno = b.contno))"
  + " ,(select g.postaladdress from lcaddress g where g.customerno = b.appntno and g.addressno=(select addressno from lcappnt where contno = b.contno)) "
  + " ,(select f.phone from lcaddress f where f.customerno = b.appntno  and  f.addressno=(select addressno from lcappnt where contno = b.contno))"
  +", b.insuredname"
  +", b.riskcode"
  +", (select riskname from lmriskapp where riskcode=b.riskcode)"
  +", b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),'个单',char(b.payyears)  "
  + " from laagent a,lcpol b "
   + " where a.agentcode=b.agentcode  and a.branchtype='1' and a.branchtype2='01' and"　　　　　　
  + " b.stateflag <> '0' and b.grpcontno ='00000000000000000000'  " 
  + getWherePart('b.ManageCom', 'ManageCom','like')
  + getWherePart('b.ContNo', 'ContNo')
  + getWherePart('a.GroupAgentCode', 'AgentCode')
  + getWherePart('a.Name', 'Name')
  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
  {
	  strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
  }
//	  strSql+=appendStrSql;
  //個單WT,CT,XT保單
	  strSql=strSql+" union "	  
 +" select b.contno,b.cvalidate ,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup where agentgroup =b.agentgroup),b.appntname,"
  +" (select f.mobile from lcaddress f where f.customerno = b.appntno  and f.addressno = (select addressno from lcappnt where contno = b.contno)),"
  +"(select g.postaladdress from lcaddress g where g.customerno = b.appntno and g.addressno=(select addressno from lcappnt where contno = b.contno)),"
  + "(select f.phone from lcaddress f where f.customerno = b.appntno  and  f.addressno=(select addressno from lcappnt where contno = b.contno)),"
  +" b.insuredname,b.riskcode,(select riskname from lmriskapp where riskcode = b.riskcode),b.prem,"
  +"(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),"
  +"'个单',char(b.payyears)"
  + " from laagent a,lbpol b"
   + " where a.agentcode=b.agentcode and a.branchtype='1' and a.branchtype2='01' and "
  + " b.stateflag <>'0'and exists (select 1 from lpedoritem lpe, lpedorapp lpa where lpe.edorno = lpa.EdorAcceptNo " 
  +" and lpe.contno = b.contno and lpe.edortype in ('XT', 'CT', 'WT') and lpa.edorstate = '0' and lpe.edorno = b.edorno) "
  + getWherePart('b.ManageCom', 'ManageCom','like')
  + getWherePart('b.ContNo', 'ContNo')
  + getWherePart('a.GroupAgentCode', 'AgentCode')
  + getWherePart('a.Name', 'Name')
  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
  {
	  strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
  }
//	  strSql+=appendStrSql;
////	  alert(strSql);
//  //正常团单
//	  strSql=strSql+" union "
//	  +" select b.grpcontno,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup where agentgroup =b.agentgroup),"
//	  +" b.grpname,'-',(select g.grpaddress from lcgrpaddress g where g.customerno = b.customerno and g.addressno = b.addressno), "
//	  +" (select f.phone1 from lcgrpaddress f where f.customerno = b.customerno and f.addressno =b.addressno),'-',b.riskcode,(select riskname from lmriskapp where riskcode =b.riskcode), "
//	  +" b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),'团单','-' "
//	  +" from laagent a,lcgrppol b "
//	  +" where a.agentcode = b.agentcode and a.branchtype='1' and a.branchtype2='01' and b.stateflag <>'0'  "
//	  + getWherePart('b.ManageCom', 'ManageCom','like')
//	  + getWherePart('b.GrpContNo', 'ContNo')
//	  + getWherePart('a.GroupAgentCode', 'AgentCode')
//	  + getWherePart('a.Name', 'Name')
//	  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//	  {
//		  strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//	  }
//		  strSql+=appendStrSql;
//		  
//	//XT,CT,WT团单
//    strSql=strSql+" union "	
//    +" select b.grpcontno,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup), "
//    +" b.grpname,'-',(select g.grpaddress from lcgrpaddress g where g.customerno = b.customerno and g.addressno = b.addressno),"
//    +" (select f.phone1 from lcgrpaddress f where f.customerno = b.customerno and f.addressno =b.addressno),'-',b.riskcode,(select riskname from lmriskapp where riskcode = b.riskcode), "
//    +" b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),'团单','-' "
//    +" from laagent a,lbgrppol b  "
//    +" where a.agentcode =b.agentcode and a.branchtype='1' and a.branchtype2='01' and b.stateflag <> '0'  "
//    +" and exists (select 1 from lpgrpedoritem lpe, lpedorapp lpa where lpe.edorno = lpa.EdorAcceptNo "
//    +" and lpe.grpcontno = b.grpcontno and lpe.edortype in ('XT', 'CT', 'WT') and lpa.edorstate = '0' and lpe.edorno = b.edorno) "
//    + getWherePart('b.ManageCom', 'ManageCom','like')
//	  + getWherePart('b.GrpContNo', 'ContNo')
//	  + getWherePart('a.GroupAgentCode', 'AgentCode')
//	  + getWherePart('a.Name', 'Name')
//	  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//	  {
//		  strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//	  }
//		  strSql+=appendStrSql;
//      
//	  //归属个单	  
//	  strSql=strSql+" union "
//	  +" select b.contno,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup), "
//	  +" b.appntname,(select f.mobile from lcaddress f where  f.customerno = b.appntno and f.addressno = (select addressno from lcappnt where contno = b.contno)), "
//	  +" (select g.postaladdress from lcaddress g where g.customerno = b.appntno and g.addressno = (select addressno from lcappnt where contno =b.contno)), "
//	  +" (select f.phone from lcaddress f where f.customerno = b.appntno and f.addressno = (select addressno from lcappnt where contno = b.contno)),b.insuredname, "
//	  +" b.riskcode,(select riskname from lmriskapp where riskcode =b.riskcode),b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),'个单',char(b.payyears) "
//	  +" from laagent a,lcpol b,laascription d "
//	  +" where a.agentcode = b.agentcode and a.branchtype='1' and a.branchtype2='01' and b.agentcode = d.agentnew "
//	  +" and b.contno = d.contno and b.stateflag<>'0'  "
//	  + getWherePart('b.ManageCom', 'ManageCom','like')
//	  + getWherePart('b.ContNo', 'ContNo')
//	  + getWherePart('a.GroupAgentCode', 'AgentCode')
//	  + getWherePart('a.Name', 'Name')
//	  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//	  {
//		  strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//	  }
//		  strSql+=appendStrSql;
//	  	  
//	  //归属个单退保
//	  strSql=strSql+" union "
//	  +" select b.contno,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup), "
//	  +" b.appntname,(select f.mobile from lcaddress f where  f.customerno = b.appntno and f.addressno = (select addressno from lcappnt where contno = b.contno)), "
//	  +" (select g.postaladdress from lcaddress g where g.customerno = b.appntno and g.addressno = (select addressno from lcappnt where contno = b.contno)), "
//	  +" (select f.phone from lcaddress f where f.customerno = b.appntno and f.addressno = (select addressno from lcappnt where contno = b.contno)),b.insuredname, "
//	  +" b.riskcode,(select riskname from lmriskapp where riskcode =b.riskcode),b.prem,(case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),'个单',char(b.payyears) "
//	  +" from laagent a,lbpol b,laascription d  "
//	  +" where a.branchtype='1' and a.branchtype2='01' and a.agentcode = b.agentcode and b.contno = d.contno and b.agentcode =d.agentnew "
//	  +" and d.ascriptioncount=1 and exists (select 1 from lpedoritem lpe, lpedorapp lpa where lpe.edorno = lpa.EdorAcceptNo and lpe.contno = b.contno and lpe.edortype in ('XT', 'CT', 'WT') and lpa.edorstate = '0' and lpe.edorno = b.edorno) "
//	  
//	  + getWherePart('b.ManageCom', 'ManageCom','like')
//	  + getWherePart('b.ContNo', 'ContNo')
//	  + getWherePart('a.GroupAgentCode', 'AgentCode')
//	  + getWherePart('a.Name', 'Name')
//	  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//	  {
//		  strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//	  }
//		  strSql+=appendStrSql;
//	 
//	  //归属团单
//	  strSql=strSql+" union "
//	  +" select b.grpcontno,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup where agentgroup = b.agentgroup), "
//	  +" b.grpname,'-',(select g.grpaddress from lcgrpaddress g where g.customerno = b.customerno and g.addressno = (select addressno from lcgrpappnt where lcgrpappnt.grpcontno = b.grpcontno)),"
//	  +"(select f.phone1 from lcgrpaddress f where f.customerno = b.customerno and f.addressno = (select addressno from lcgrpappnt where lcgrpappnt.grpcontno = b.grpcontno)),"
//	  +"'-',b.riskcode,(select riskname from lmriskapp where riskcode=b.riskcode),b.prem, (case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),"
//	  +" '团单','-' "
//	  +" from laagent a, lcgrppol b,laascription c "
//	  +" where a.agentcode =b.agentcode and a.branchtype ='1' and a.branchtype2='01' and b.grpcontno = c.grpcontno "
//	  +" and a.agentcode = c.agentnew and b.stateflag<>'0' and c.ascriptioncount=1  "
//	  + getWherePart('b.ManageCom', 'ManageCom','like')
//	  + getWherePart('b.GrpContNo', 'ContNo')
//	  + getWherePart('a.GroupAgentCode', 'AgentCode')
//	  + getWherePart('a.Name', 'Name')
//	  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//	  {
//		  strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//	  }
//		  strSql+=appendStrSql;
//	  
//	  //归属退保团单
//	  strSql=strSql+" union "
//	  +" select b.grpcontno,b.cvalidate,a.groupagentcode,a.name,a.outworkdate,b.managecom,(select branchattr from labranchgroup where agentgroup =b.agentgroup),"
//	  +"b.grpname,'-',(select g.grpaddress from lcgrpaddress g where g.customerno = b.customerno and g.addressno = (select addressno from lcgrpappnt where lcgrpappnt.grpcontno = b.grpcontno)), "
//	  +"(select f.phone1 from lcgrpaddress f where f.customerno = b.customerno and f.addressno = (select addressno from lcgrpappnt where lcgrpappnt.grpcontno = b.grpcontno)),"
//	  +"'-',b.riskcode,(select riskname from lmriskapp where riskcode=b.riskcode),b.prem, (case b.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end),"
//	  +" '团单','-' "
//	  +" from laagent a,lbgrppol b,laascription c "
//	  +" where a.branchtype ='1' and a.branchtype2='01' and b.grpcontno=c.grpcontno and a.agentcode = b.agentcode  "
//	  +" and a.agentcode = c.agentnew and b.stateflag<>'0' and c.ascriptioncount=1   "
//	  +" and  exists (select 1 from lpgrpedoritem lpe, lpedorapp lpa where lpe.edorno = lpa.EdorAcceptNo "
//	  +" and lpe.grpcontno = b.grpcontno and lpe.edortype in ('XT', 'CT', 'WT') and lpa.edorstate = '0' and lpe.edorno = b.edorno) " 	  
//      
//	  + getWherePart('b.ManageCom', 'ManageCom','like')
//	  + getWherePart('b.GrpContNo', 'ContNo')
//	  + getWherePart('a.GroupAgentCode', 'AgentCode')
//	  + getWherePart('a.Name', 'Name')
//	  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
//	  {
//		  strSql+=" and b.agentgroup = (select agentgroup from labranchgroup where branchtype ='1' and branchtype2='01' and branchattr ='"+fm.all('BranchAttr').value+"'  ) "
//	  }
//		  strSql+=appendStrSql;
	  
		  fm.querySql.value = strSql;

	      var i = 0;
	      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	      //showSubmitFrame(mDebug); 
		  //fm.fmtransact.value = "PRINT";

		fm.all('op').value = '';
		fm.all('Flag').value = 'Risk';
		fm.submit();
		showInfo.close();
}

function afterCodeSelect( cCodeName, Field )
{
　	  
  if(cCodeName=="AgentState")
  {
	  if(Field.value=='0')
	  {
		  try{
				fm.all("EmployeeType").style.display = '';
				fm.all("DissmissionType").style.display = 'none';
				 
		  }catch(ex){}
	  }
  }
  if(cCodeName=="AgentState")
  {
	  if(Field.value=='1')
	  {
		  try{
				fm.all("EmployeeType").style.display = 'none';
				fm.all("DissmissionType").style.display = '';
				 
		  }catch(ex){}
	  }
  }

}        
                                                                                                                   