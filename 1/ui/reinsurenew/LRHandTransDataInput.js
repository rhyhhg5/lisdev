var showInfo;

var turnPage = new turnPageClass(); 




//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}




function check(){
	
	if( verifyInput() == true) 
	{
		return true;
	}
	return false;
}

function queryDetailData(){
	
	var strSQL = " select ManageCom,RiskCode,ReContCode,CostCenter," +
			"ReComCode,(select recomname from LRReComInfo where ReComCode = LRHandTransData.ReComCode)," +
			"CessPrem,ReProcFee,ContType,'手工插入' ,GetDataDate" +
//			"MakeDate,MakeTime,ModifyDate,ModifyTime,Oprater"
				" from LRHandTransData where" 
		 +" 1=1 "
		+ getWherePart( 'RecontCode','RecontCode' )
//		 + getWherePart( 'ContType', 'TempCessFlag')
		 + getWherePart( 'RiskCode','RiskCode' )
		 + " with ur ";
		 turnPage.queryModal(strSQL, ContGrid);
	
}

/**
 * 导入清单。
 */
function importList()
{
	fm.btnImport.disabled = true;
    
 
    
    var showStr = "正在导入清单数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.action = "./LRHandTransDataImportSave.jsp";
    fm.submit();

    
}

function deleteData()
{
	var chlno = ContGrid.getSelNo();
	try 
	{
 	 	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面。";
 	 	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 	 	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 	 	fm.action = "./LRHandTransDataSave.jsp?chlno="+ chlno;
 	    fm.submit();
	} catch(ex) 
	{
		showInfo.close( );
		alert(ex);
	}
}
/**
 * 导入清单提交后动作。
 */
function afterImportCertifyList(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("导入批次失败，请尝试重新进行申请。");
       
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
       
    }
    
    fm.btnImport.disabled = false;
}

function downLoad(){
 	 	fm.OperateType.value = "DownLoad";
 	 	fm.action = "./LRHandTrandDataDownloadSave.jsp?OperateType="+ fm.all("OperateType").value+"";
 	    fm.submit();
	
}

        

