<% 
	//程序名称：LRGetDataSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%
  GlobalInput tGI = new GlobalInput( );
   tGI = (GlobalInput)session.getValue("GI") ;
  LRAccountSet tLRAccountSet = new LRAccountSet();
  LRAccountDB tLRAccountDB = new LRAccountDB();
  LRDataTransferConfirmUI  tLRDataTransferConfirmUI= new LRDataTransferConfirmUI();
  TransferData tTransferData = new TransferData();
  VData tVData = new VData();
  CErrors tError = null;
  String Content = "";
  String FlagStr = "";
  String feetype = request.getParameter("feetype");
  String aBelongYear = request.getParameter("year");
  String aBelongMonth = request.getParameter("month");
  String aReContCode = "";
  String aRiskCode = "";
  String aImportCount = "";
  String aGrpContNo = "";
  String aContNo = "";
  String aDataType = "";
  
  //	String tDataTransferConfirmGrid2[] = request.getParameterValues("DataTransferConfirmGrid2"); 
//  String tDataTransferConfirmGrid3[] = request.getParameterValues("DataTransferConfirmGrid3");
//  String tDataTransferConfirmGrid4[] = request.getParameterValues("DataTransferConfirmGrid4");
//  String tDataTransferConfirmGrid5[] = request.getParameterValues("DataTransferConfirmGrid5");
//  String tDataTransferConfirmGrid14[] = request.getParameterValues("DataTransferConfirmGrid14");
//  String tDataTransferConfirmGrid15[] = request.getParameterValues("DataTransferConfirmGrid15");
//  String tDataTransferConfirmGrid16[] = request.getParameterValues("DataTransferConfirmGrid16");
//  String tDataTransferConfirmGrid17[] = request.getParameterValues("DataTransferConfirmGrid17");
//  String tChk[] = request.getParameterValues("InpDataTransferConfirmGridChk");
//  System.out.println(tChk.length);
//  for(int index=0;index<tChk.length;index++)
//  {
//    if(tChk[index].equals("1")){           
//       aBelongYear = tDataTransferConfirmGrid2[index];
//       aBelongMonth = tDataTransferConfirmGrid3[index];
//       aReContCode = tDataTransferConfirmGrid4[index];
//       aRiskCode = tDataTransferConfirmGrid5[index];
//       aImportCount = tDataTransferConfirmGrid14[index];
//       aGrpContNo = tDataTransferConfirmGrid15[index];
//       aContNo = tDataTransferConfirmGrid16[index];
//       aDataType = tDataTransferConfirmGrid17[index];
//       
//       //根据主键查出一条记录
//       tLRAccountDB.setBelongYear(aBelongYear);
//       tLRAccountDB.setBelongMonth(aBelongMonth);
//       tLRAccountDB.setReContCode(aReContCode);
//       tLRAccountDB.setRiskCode(aRiskCode);
//       tLRAccountDB.setImportCount(aImportCount);
//       tLRAccountDB.setGrpContNo(aGrpContNo);
//       tLRAccountDB.setContNo(aContNo);
//       tLRAccountDB.setDataType(aDataType);
//       LRAccountSet mLRAccountSet = tLRAccountDB.query();
//       tLRAccountSet.add(mLRAccountSet.get(1));
//    }
//  }
  try
  {
  // 准备传输数据 VData
    tTransferData.setNameAndValue("feetype",feetype);
    tTransferData.setNameAndValue("BelongYear",aBelongYear);
    tTransferData.setNameAndValue("BelongMonth",aBelongMonth);
  	tVData.add(tTransferData);
  	tVData.add(tGI);
  //	tVData.add(tLRAccountSet);
  	tLRDataTransferConfirmUI.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr=="")
  {
    tError = tLRDataTransferConfirmUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
