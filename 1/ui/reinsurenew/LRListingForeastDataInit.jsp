
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    
<%
   	GlobalInput tGI = (GlobalInput) session.getValue("GI");	
	
%> 

<script language="JavaScript">
var turnPage = new turnPageClass();
var ManageCom=<%=tGI.ManageCom%>
function initInpBox()
{    	
//	 fm.all("CessPrem').value = "";
//	 fm.all("ReProcFee').value = "";
 //	 fm.all("ClaimBackFee').value = "";
//	 fm.all("CRCsum').value = "";	 
}       
                            
function initForm() 
{	  	
  try 
  {  
    initInpBox();
    initListForeastDataGrid();  
  }
  catch(re) 
  {
    alert("操作错误！请关掉上次登陆打开的页面");
  }
}

//领取项信息列表的初始化
var ListForeastDataGrid;
function initListForeastDataGrid() 
{                   
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构代码";         	  //列名
    iArray[1][1]="100px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="险种编码";         	
    iArray[2][1]="60px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="再保合同号";         	  //列名
    iArray[3][1]="80px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="成本中心";      //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[5]=new Array();
    iArray[5][0]="保单号";              //列名
    iArray[5][1]="100px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="再保公司";              //列名
    iArray[6][1]="120px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="分出保费";              //列名
    iArray[7][1]="80px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="再保手续费";              //列名
    iArray[8][1]="80px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0; 

    iArray[9]=new Array();
    iArray[9][0]="理赔摊回金额";              //列名
    iArray[9][1]="80px";            	//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0; 

    iArray[10]=new Array();
    iArray[10][0]="合同类型";              //列名
    iArray[10][1]="80px";            	//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0; 

    iArray[11]=new Array();
    iArray[11][0]="团单号";              //列名
    iArray[11][1]="80px";            	//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=3;

    iArray[12]=new Array();
    iArray[12][0]="个单号";              //列名
    iArray[12][1]="80px";            	//列宽
    iArray[12][2]=200;            			//列最大值
    iArray[12][3]=3;
   
    iArray[13]=new Array();
    iArray[13][0]="市场类型";              //列名
    iArray[13][1]="80px";            	//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=0;
    
    ListForeastDataGrid = new MulLineEnter("fm", "ListForeastDataGrid"); 
    //设置Grid属性
    ListForeastDataGrid.mulLineCount = 10;
    ListForeastDataGrid.displayTitle = 1;
    ListForeastDataGrid.locked = 1;
<!--    ListForeastDataGrid.canSel = 1;-->
    ListForeastDataGrid.canChk = 0;
    ListForeastDataGrid.hiddenSubtraction = 1;
    ListForeastDataGrid.hiddenPlus = 1;
    ListForeastDataGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
} 
</script>
