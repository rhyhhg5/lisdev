<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LRGetDataInput.jsp
//程序功能：
//创建日期：2006-10-31
//创建人  ：JavaBean
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LRFillDataInput.js"></SCRIPT> 
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
	<%@include file="LRFillDataInit.jsp" %>
</head>

<body onload="initElementtype();initForm();">    
  <form action="" method=post name=fm target="fraSubmit" >
    <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">再保提数</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divCessGetData" style= "display: ''" style="float: right">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	         
	        </TR>
	        <TR class= common>
	         <TD  class= title>再保合同号</TD>
	          <TD class= input>
   				<Input class= common name="ReContCode" id="ReContCode" > 
   			</TD>
   			<TD  class= title>险种号</TD>
	          <TD class= input>
   				<Input class= common name="RiskCode" id="RiskCode" > 
   			</TD>
   			</TR>
   			 <TR class= common>
	         <TD  class= title>团单号</TD>
	          <TD class= input>
   				<Input class= common name="GrpContNo" id="GrpContNo" > 
   			</TD>
   			<TD  class= title>个单号</TD>
	          <TD class= input>
   				<Input class= common name="ContNo" id="ContNo" > 
   			</TD>
   			</TR>
   			<TR class= common>
	         <TD  class= title>PolNo</TD>
	          <TD class= input>
   				<Input class= common name="PolNo" id="PolNo" > 
   			</TD>
   			<TD  class= title>操作者</TD>
	          <TD class= input>
   				<Input class= common name="Operator" id="Operator"  > 
   			</TD>
   			</TR>
	    </table>
	    <br>
		<INPUT class=cssButton  VALUE="分保数据提取" TYPE=button onClick="fillData('getcessdata');">	
		<INPUT class=cssButton  VALUE="保全数据提取" TYPE=button onClick="fillData('getedordata');">	
		<INPUT class=cssButton  VALUE="理赔数据提取" TYPE=button onClick="fillData('getclaimdata');">	
		<br><br>
		<INPUT class=cssButton  VALUE="分保数据计算" TYPE=button onClick="fillData('calcessdata');">	
		<INPUT class=cssButton  VALUE="保全数据计算" TYPE=button onClick="fillData('caledordata');">	
		<INPUT class=cssButton  VALUE="理赔数据计算" TYPE=button onClick="fillData('calclaimdata');">	
		<br><br>
		<INPUT class=cssButton  VALUE="分保清单表汇总" TYPE=button onClick="fillData('getcesslist');">	
		<INPUT class=cssButton  VALUE="理赔清单表汇总" TYPE=button onClick="fillData('getclaimlist');">	
		<INPUT class=cssButton  VALUE="批处理维护" TYPE=button onClick="fillData('filltask');"	>
		</Div>
    <input type="hidden" name=OperateType value="">
  </form><br><br>
  <font color='red'>
  注：如果以上功能对于数据提取不到，可以通过下面手工补数功能来进行补数。下面的功能，只能对于“成数合同”进行维护
  </font>
  <hr>
  <form action="" method=post name=fm2 target="fraSubmit" >
   <div style="width:200">
  	 <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">手工补数</td>
        </tr>
      </table>
      </div>
    <br>
    <table class= common border=0 width=100%>
      	<TR  class= common>
		  <TD  class= title>提数日期</TD>
          <TD  class= input> 
          	<Input name=getDataDate class='coolDatePicker' dateFormat='short' > 
          </TD> 
         <TD  class= title>再保合同号</TD>
          <TD class= input>
  				<Input class= common name="ReContCode" id="ReContCode" > 
  			</TD>
  			<TD  class= title>险种号</TD>
          <TD class= input>
  				<Input class= common name="RiskCode" id="RiskCode" > 
  			</TD>
  			</TR>
  			<TR class= common>
  			<TD  class= title>团单号</TD>
          <TD class= input>
  				<Input class= common name="GrpContNo" id="GrpContNo" > 
  			</TD>
  			<TD  class= title>个单号</TD>
          <TD class= input>
  				<Input class= common name="ContNo" id="ContNo" > 
  			</TD>
         <TD  class= title>PolNo</TD>
          <TD class= input>
  				<Input class= common name="PolNo" id="PolNo" > 
  			</TD>
  			</TR>
  			<TR class= common>
         <TD  class= title>reinsureitem</TD>
          <TD class= input>
  				<Input class= common name="ReinsureItem" id="ReinsureItem" > 
  			</TD>
  			<TD  class= title>PayNo</TD>
          <TD class= input>
  				<Input class= common name="PayNo" id="PayNo" > 
  			</TD>
  			<TD  class= title>操作者</TD>
          <TD class= input>
  				<Input class= common name="Operator" id="Operator"  > 
  			</TD>
  			</TR>
  			<TR class=input>     
         <TD class=common>
          <input type =button class=cssbutton value="查 询" onclick="FillDataQuery();">    
          <input type =button class=cssbutton value="插 入" onclick="FillDataUpdate();">
          </td>
       </TR>       
    </table>
    <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">信息列表 </td>
        </tr>
      </table>
  	</div>
  	<div>
  		<span id ="spanFillDataGrid"></span>
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 