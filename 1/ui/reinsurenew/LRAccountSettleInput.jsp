<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate(); 
%>
<!--<script language="javascript">
    
   function initDate(){
     fm.InputDate.value="<%=CurrentDate%>";
        }
</script> --> 
        
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html;charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LRAccountSettle.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRAccountSettleInit.jsp"%>
 
  <title>清单预估数据生成</title>   
</head>
<body  onload="initForm();initElementtype();" >
	<!--initDate();-->
  
 <form action="./LRAccountSettleSave.jsp" method=post name=fm target="fraSubmit"> 
    <table>
     <tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssess1);">
      </td>
      <td class=titleImg>
          请输入查询条件
      </td>
     </tr>
    </table>
    <Div  id="divLAAssess1" style= "display:''">
    <table class=common align=center>
     <TR  class=common>
      <TD  class=title>
          年份
      </TD>
      <TD  class=input>
       <Input class=common name=year value='' elementtype=nacessary verify="年份|num&len=4" 
       onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>       
      <TD  class=title>
          季度
      </TD>
      <TD  class=input>
       <Input class="codeno"  readOnly name= "quarter" CodeData="0|^01|第一季度^02|第二季度^03|第三季度^04|第四季度" 
          ondblClick="showCodeListEx('quartertype',[this,quarterName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('quartertype',[this,quarterName],[0,1],null,null,null,1);"><Input 
          class= codename name= 'quarterName'  value=""> 
      </TD>
      <TD  class=title>
          月份
      </TD>
      <TD  class=input>
       <Input class="codeno"  readOnly name= "month" CodeData="0|^01|第1月份^02|第2月份^03|第3月份^04|第4月份^05|第5月份^06|第6月份^07|第7月份^08|第8月份^09|第9月份^10|第10月份^11|第11月份^12|第12月份" 
          ondblClick="showCodeListEx('monthtype',[this,monthName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('monthtype',[this,monthName],[0,1],null,null,null,1);"><Input 
          class= codename name= 'monthName'  value=""> 
      </TD>       
      </TR>  
      <TR class="common">
      <TD  class=title>
          再保公司
      </TD>
      <TD  class=input>
      <Input class="codeno"  readOnly name= "ReComCode"  
          ondblClick="showCodeList('reinsurecomcode',[this,ReComCodeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKey('reinsurecomcode',[this,ReComCodeName],[0,1],null,null,null,1);"><Input 
          class= codename name= 'ReComCodeName' value=""> 
      </TD>
     
      <TD  class=title>
          合同类型
      </TD>
      <TD  class=input>
      <Input class="codeno"  readOnly name= "RecontType" CodeData="0|^C|合同分保^T|临时分保" 
          ondblClick="showCodeListEx('reconttype',[this,RecontTypeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('reconttype',[this,RecontTypeName],[0,1],null,null,null,1);"><Input 
          class= codename name= 'RecontTypeName'  value=""> 
      </TD>       
      <TD  class=title>
         再保合同号
      </TD>
      <TD  class=input>
      	<Input class="codeno"   name= "RecontCode"   
          ondblClick="showCodeList('accountRecontCode',[this,RecontCodeName],[0,1],null,[fm.ReComCode.value,fm.RecontType.value],1,1);"
          onkeyup="showCodeListKey('accountRecontCode',[this,RecontCodeName],[0,1],null,[fm.ReComCode.value,fm.RecontType.value ],1,1);"><Input 
          class= codename name= 'RecontCodeName'  value=""> 
      </TD>
     </TR>  
    </table>
  <INPUT class=cssButton VALUE="查   询" TYPE=button onClick="queryData();">
   <INPUT class=cssButton VALUE="账单结算" TYPE=button onClick="accountSettle();">
   <INPUT class=cssButton VALUE="账单下载" TYPE=button onClick="accountDownload();">  
   
   <table  class=common>
     <tr  class=common>
      <td text-align: left colSpan=1>
      <span id="spanLRAccountSettleGrid" >
      </span> 
      </td>
     </tr>
    </table>
    <div  align="center">
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton >                  
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
      </div>
    </div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden name=Type value='1'>
    <input type=hidden name=MakeContNo  value=''>
    <input type=hidden id="fmAction" name="fmAction">  
    <input type=hidden class=Common name=querySql > 
  </form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
