<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRNewAccountCheckInput.jsp
//程序功能：账单审核
//创建日期：2008-11-11
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LRHandTransDataInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRHandTransDataInit.jsp"%>
  <title>手工社保数据导入</title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "./LRHandTransDataSave.jsp" enctype="multipart/form-data">
 <Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFileImport);"></td>
          <td class="titleImg">数据批量导入</td>
        </tr>
      </table>
  </Div>
 <div id="divFileImport" name="divFileImport" style="display:''">
           
            <table class="common">
                <tr class="common">
                    <td class="title">批量导入文件</td>
                    <td class="common">
                        <input class="cssfile" name="FileName" type="file" />
                    </td>
                    
                </tr>
            </table>
            <font color="red" >批量导入模板说明：</font><br>
            <font color="red" >1.模板中sheet页的现有名称(LRHandTransData)和第一行的抬头名称不能修改； </font>  
 		<br>
 		<font color="red" >2.模板中标记黄色的字段为必填项,都保持现有的文本格式。 </font>   
 		<br>
 		<font color="red" >3.险种编码要填写再保系统中的险种编码,如1601要写成161101。 </font> 
 		<br/>
 		<font color="red" >4.分出保费和手续费的单位都为：元,日期格式为：YYYY-MM-DD。 </font> 
            <br><input class="cssButton" type="button"  value="导入模板下载" onclick="downLoad();" />
                    
            <input class="cssButton" type="button" id="btnImport" name="btnImport" value="批量数据导入" onclick="importList();" />
       		
        </div>
         
        <hr />
         
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInfo);"></td>
          <td class="titleImg">请输查询信息</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divInfo" style= "display: ''">
    <table class=common>
    	<tr class=common>
    		 <td class="title">再保合同号</td>
				<td class="input">
					<Input class="code" name="RecontCode" 
						ondblClick="return showCodeList('RecontCode',[this,null],[0,1],null,null,null,1);" 
						onkeyup="return showCodeListKey('RecontCode',[this,null],[0,1],null,null,null,1);">
				</td>	
<!--   			<td class="title">分保类型</td>-->
<!--	       		<td class="input">-->
<!--	   				<input class="codeno" readOnly name= "TempCessFlag" CodeData="0|^H|手工导入" -->
<!--	  					ondblClick="showCodeListEx('ReinsureType',[this,TempCessFlagName],[0,1],null,null,null,1);	"-->
<!--	  					onkeyup="showCodeListKeyEx('ReinsureType',[this,TempCessFlagName],[0,1],null,null,null,1);"><input class="codename" name= "TempCessFlagName" readonly>-->
<!--	   			</td> -->
   			<TD  class= title>险种编码</TD>
        <TD  class= input> <Input class="codeno" name=RiskCode 
          ondblclick = "return showCodeList('RiskCode',[this,RiskName],[0,1],null,null,null,1,300);"
          onkeyup = "return showCodeListKey('RiskCode',[this,RiskName],[0,1],null,null,null,1,300);"><Input class="codename" name= 'RiskName' readonly> 
        </TD>  			
        </tr>
    	
     	<TR  class= common>
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	         
	        </TR>
  	</table>    
  </Div>


  <INPUT class=cssButton VALUE="查   询" TYPE=button onClick="queryDetailData()">
<!--  <INPUT class=cssButton VALUE="单一数据导入" TYPE=button onClick="SaveData()">-->
  <br/>
  <input type="hidden" name="OperateType" >
 <Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divList);"></td>
          <td class="titleImg">数据详细信息</td>
        </tr>
      </table>
  </Div>
  <div  id= "divList" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
	<center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
	</center>  	
  	</div>
  	<INPUT class=cssButton VALUE="删除数据" TYPE=button onClick="deleteData()">
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>