var turnPage = new turnPageClass();  
var tSQL="";
var year="";
var month="";
var showInfo;
var FlagStr = "";
var content = "";


function queryData()
{
	if( verifyInput2() == false )
		{	
	  	 	return false; 
		}	
	if( checkValid() == false)
		{	
			return false;
		}
	
	year=fm.all("year").value;
	month=fm.all("month").value;
	tSQL = "select recomname,belongyear,belongmonth,recontcode,riskcode," 
		  +"cessprem,(case a.cesspremstate when '00' then '未流转' when '01' then '可流转' else '已流转' end),"
		  +	"reprocfee,(case a.reprocfeestate when '00' then '未流转' when '01' then '可流转' else '已流转' end)," 
		  +	"claimbackfee,(case a.claimbackfeestate when '00' then '未流转' when '01' then '可流转' else '已流转' end)," 
		  +	"accountmoney,accountdate,importcount,grpcontno,contno,(case  datatype when '00' then '系统上传' when '02' then '其它调整'  when '01' then '每月调整' end),accountoprater "
		  +	"from (select (select recomname from lrrecominfo where recomcode in (select recomcode from lrcontinfo where recontcode = c.recontcode union select comcode from lrtempcesscont where tempcontcode = c.recontcode)) as recomname,"
		  +"c.belongyear,c.belongmonth,c.recontcode,c.riskcode," 
		  +	"c.cessprem,c.cesspremstate," 
		  +	"c.reprocfee,c.reprocfeestate," 
		  +	"c.claimbackfee,c.claimbackfeestate," 
		  +	"c.accountmoney,c.accountdate,c.importcount,c.grpcontno,c.contno,c.datatype,c.accountoprater from LRAccount c"
		  +" where c.belongyear = '"+year+"'"
		  +" and c.belongmonth = '"+month+"'" 
//		  +" and c.datatype in('00','01')"
		  +	") a"
		  +" where a.cesspremstate = 00"
		  +" or a.reprocfeestate = 00"
		  +" or a.claimbackfeestate = 00";

      
    var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		turnPage.queryModal(tSQL, DataTransferConfirmGrid); 
		return false;
	}
    else{ 
    	turnPage.queryModal(tSQL, DataTransferConfirmGrid);
    }
	fm.all("cesspremButton").disabled ="";
	fm.all("claimbackfeeButton").disabled="";
    tSQL = "";
    year = "";
    month = "";
}

function dataConfirm1(){
	var value = "";
	var cessStateValue = "";	
//	var rowNum = DataTransferConfirmGrid.mulLineCount;
//	if(cheMul() == false){
//		return false;
//	}
	if( checkBelongMonth() == false)
	{	
		return false;
	}
	if (chkCessPremState()==false){
		return false;
	}
	if(checkFinMonth()==false)
	{
		return false;
	}

	var belongyear = fm.year.value;
	var belongmonth = fm.month.value;
	var tCurrCessprem = 0;
	var tFinCessPrem = 0;
	var tSQL2 = "select cesspremstate,sum(cessprem) from lraccount  c where belongyear = '"+belongyear+"'  and belongmonth = '"+belongmonth+"' group by cesspremstate order by cesspremstate with ur";
	var strQueryResult2 = easyQueryVer3(tSQL2, 1, 0, 1);
	if(strQueryResult2)
	{
		var tArr = new Array();
		tArr = decodeEasyQueryResult(strQueryResult2);
		if(tArr!=null&&tArr!=''&&tArr.length>0)
		{
			for(var i=0;i<tArr.length;i++)
			{
				if("00"==tArr[i][0])
				{
					tCurrCessprem += tArr[i][1];
				}
				if("02"==tArr[i][0])
				{
					tFinCessPrem += tArr[i][1];
				}
			}
		}
	}
	if(tCurrCessprem==0)
	{
		alert("本次需要流转的分出保费为0");
		return false;
	}
	if(confirm("本月已经流转的分出保费为："+tFinCessPrem+";本次需要流转的分出保费为："+tCurrCessprem))
	{
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
		fm.all("feetype").value = "cess";	
		fm.submit();
		fm.all("cesspremButton").disabled ="true";
		fm.all("claimbackfeeButton").disabled="true";
	}
	
}	
function dataConfirm2(){
	var value = "";
	var procStateValue = "";
	var claimStateValue = "";
	if( checkBelongMonth1() == false)
	{	
		return false;
	}
	if (chkClaimState()==false){
		return false;
	}
	if(checkFinMonth()==false)
	{
		return false;
	}

//	for (var row = 0;row<rowNum1;row++){
//		if(DataTransferConfirmGrid.getChkNo(row)){
//			value = DataTransferConfirmGrid.getRowColDataByName(row,"datatype");
//			procStateValue = DataTransferConfirmGrid.getRowColDataByName(row,"procState");
//			claimStateValue = DataTransferConfirmGrid.getRowColDataByName(row,"claimState")
//			if(value=="01"){
//				alert("你所选中的有长期计提数据不能流转请重新选择");
//				return false;
//			}
//			if(procStateValue == "已流转"||claimStateValue == "已流转"){
//				alert("你所选中的有已流转数据请重新选择");
//				return false;
//			}
//		}
//	}
	var belongyear = fm.year.value;
	var belongmonth = fm.month.value;
	var tCurrReprocFee = 0;
	var tFinReprocFee = 0;
	var tCurrClaimBackFee = 0;
	var tFinClaimBackFee = 0;
	var tSQL2 = "select reprocfeestate,claimbackfeestate,sum(reprocfee),sum(claimbackfee) from lraccount  c where belongyear = '"+belongyear+"'  and belongmonth = '"+belongmonth+"' group by reprocfeestate,claimbackfeestate order by reprocfeestate,claimbackfeestate with ur";
	var strQueryResult2 = easyQueryVer3(tSQL2, 1, 0, 1);
	if(strQueryResult2)
	{
		var tArr = new Array();
		tArr = decodeEasyQueryResult(strQueryResult2);
		if(tArr!=null&&tArr!=''&&tArr.length>0)
		{
			for(var i=0;i<tArr.length;i++)
			{
				if("00"==tArr[i][0])
				{
					tCurrReprocFee += parseFloat(tArr[i][2],"10");
				}
				if("02"==tArr[i][0])
				{
					tFinReprocFee += parseFloat(tArr[i][2],"10");
				}
				if("00"==tArr[i][1])
				{
					tCurrClaimBackFee += parseFloat(tArr[i][3],"10");
				}
				if("02"==tArr[i][1])
				{
					tFinClaimBackFee += parseFloat(tArr[i][3],"10");
				}
			}
		}
	}
	if(tCurrReprocFee==0&&tCurrClaimBackFee==0)
	{
		alert("本次需要流转的手续费和理赔摊回都为0");
		return false;
	}
	if(confirm("本月已经流转的手续费为："+tFinReprocFee+"，摊回赔款为："+tFinClaimBackFee+";本次需要流转的手续费为："+tCurrReprocFee+"，摊回赔款为："+tCurrClaimBackFee))
	{
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
		fm.all("feetype").value = "claim";	
		fm.submit();
		fm.all("cesspremButton").disabled ="true";
		fm.all("claimbackfeeButton").disabled="true";
	}
}
function afterSubmit(FlagStr,content){
	 showInfo.close(); 
	  if (FlagStr == "Fail" ) {             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	    
	  } else { 
		  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;		  
		  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }
	  initForm();
	  
}
function checkValid(){
	if (fm.year.value == "")
	{
		alert("请输入年份!");
		fm.month.value="";
		fm.all('year').focus();
		return false;
	}
	if(fm.month.value == "")
	{
		alert("请输入月份!");
		fm.month.value="";
		fm.all('month').focus();
		return false;
	}
	return true;
}

function checkBelongMonth(){
	var tSQL1 = "";
	var belongyear = fm.year.value;
	var belongmonth = fm.month.value;
	
	tSQL1 = "select max(belongyear||belongmonth) from lraccount  c where   c.cesspremstate = '02' with ur";
	var strQueryResult1 = easyQueryVer3(tSQL1, 1, 0, 1);
	if(strQueryResult1){
		var tArr = new Array();
		tArr = decodeEasyQueryResult(strQueryResult1);
		if(tArr!=null&&tArr!=''&&tArr.length>0)
		{
			var calYear = tArr[0][0].substring(0,4);
			var calMonth = tArr[0][0].substring(4,6);
			if((parseInt(calMonth,"10")+1+parseInt(calYear,"10")*12)==(parseInt(belongmonth,"10")+parseInt(belongyear,"10")*12)||(parseInt(calMonth,"10")+parseInt(calYear,"10")*12)==(parseInt(belongmonth,"10")+parseInt(belongyear,"10")*12))
			{
				return true;
			}
			else
			{
				alert("目前（分出保费）已流转到"+tArr[0][0]+"月，请按顺序进行流转确认");
				return false;
			}
		}
	}else{
		tSQL1 = "";
		strQueryResult1 = "";
		return true;
	}		
}
function checkBelongMonth1(){
	var tSQL1 = "";
	var belongyear = fm.year.value;
	var belongmonth = fm.month.value;
	
	tSQL1 = "select max(belongyear||belongmonth) from lraccount  c where   c.reprocfeestate ='02' and  c.claimbackfeestate = '02' with ur";
	var strQueryResult1 = easyQueryVer3(tSQL1, 1, 0, 1);
	if(strQueryResult1){
		var tArr = new Array();
		tArr = decodeEasyQueryResult(strQueryResult1);
		if(tArr!=null&&tArr!=''&&tArr.length>0)
		{
			var calYear = tArr[0][0].substring(0,4);
			var calMonth = tArr[0][0].substring(4,6);
			if((parseInt(calMonth,"10")+1+parseInt(calYear,"10")*12)==(parseInt(belongmonth,"10")+parseInt(belongyear,"10")*12)||(parseInt(calMonth,"10")+parseInt(calYear,"10")*12)==(parseInt(belongmonth,"10")+parseInt(belongyear,"10")*12))
			{
				return true;
			}
			else
			{
				alert("目前（手续费、理赔摊回）已流转到"+tArr[0][0]+"月，请按顺序进行流转确认");
				return false;
			}
		}
	}else{
		tSQL1 = "";
		strQueryResult1 = "";
		return true;
	}		
}
function cheMul(){
	var iCount = 0;
	var rowNum1 = DataTransferConfirmGrid.mulLineCount;
	for(var i=0;i<rowNum1;i++){
		if(DataTransferConfirmGrid.getChkNo(i)){
			iCount++;
		}
	}
	if(iCount==0){
		alert("请先选择行");
		return false;
	}
	return true;
}

function chkCessPremState(){
	year=fm.all("year").value;
	month=fm.all("month").value;
	var mSQL = "select * from lraccount where belongyear = '"+year+"' and belongmonth = '"+month+"' and cesspremstate = '01'" ;
	var strQueryResult2 = easyQueryVer3(mSQL, 1, 0, 1);
	if(!strQueryResult2){
		return true;
	}else {
		alert("本月数据（分出保费）有数据正在流转财务，请稍后再点击【流转确认】！");
		return false;
	}
}

function chkClaimState(){
	year=fm.all("year").value;
	month=fm.all("month").value;
	var mSQL = "select * from lraccount where belongyear = '"+year+"' and belongmonth = '"+month+"' and (reprocfeestate = '01' or claimbackfeestate = '01') with ur " ;
	var strQueryResult2 = easyQueryVer3(mSQL, 1, 0, 1);
	if(!strQueryResult2){
		return true;
	}else {
		alert("本月数据（手续费和摊回赔款）有数据正在流转财务，请稍后再点击【流转确认】！");
		return false;
	}
}
function checkFinMonth()
{
	year=fm.all("year").value;
	month=fm.all("month").value;
	var sql ="select 'Y' from FIPeriodManagement  where state ='1'  and accyear ='"+year+"' and accmonth ='"+month+"' with ur";
	var strQueryResult2 = easyQueryVer3(sql, 1, 0, 1);
	if(!strQueryResult2){
		alert("本次流转的数据与财务账期不符！");
		return false;
	}else {
		return true;
	}
}
