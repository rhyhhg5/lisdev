<%
//程序名称：LRGetDataSave.jsp
//程序功能：
//创建日期：2006-10-24
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>

<%
  System.out.println("开始执行Save页面");
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  LRCessListSet tLRCessListSet = new LRCessListSet(); 
  int lineCount = 0;
  String tChk[] = request.getParameterValues("InpFillDataGridChk");
  String tGrpContNo[] = request.getParameterValues("FillDataGrid1");
  String tContNo[] = request.getParameterValues("FillDataGrid2");
  String tPolNo[] = request.getParameterValues("FillDataGrid3");
  String tReContCode[] = request.getParameterValues("FillDataGrid5");
  String tReinsureitem[] = request.getParameterValues("FillDataGrid8");
  String tPayNo[] = request.getParameterValues("FillDataGrid9");
  String tRenewCount[] = request.getParameterValues("FillDataGrid10");
  String tGetDataDate[] = request.getParameterValues("FillDataGrid11");
  String tPrem[] = request.getParameterValues("FillDataGrid12");
  String tNewGetDataDate[] = request.getParameterValues("FillDataGrid13");
  String tNewReinsureitem[] = request.getParameterValues("FillDataGrid14");
  String tNewPayNo[] = request.getParameterValues("FillDataGrid15");
  int number = 0; 
  System.out.println(tChk.length);
  for(int index=0;index<tChk.length;index++)
  {
      if(tChk[index].equals("1")) 
      {
    	  LRCessListSchema tLRCessListSchema = new LRCessListSchema();
    	  tLRCessListSchema.setGrpContNo(tGrpContNo[index]);
    	  tLRCessListSchema.setContNo(tContNo[index]);
    	  tLRCessListSchema.setPolNo(tPolNo[index]);
    	  tLRCessListSchema.setReContCode(tReContCode[index]);
    	  tLRCessListSchema.setReinsureItem(tReinsureitem[index]);
    	  tLRCessListSchema.setPayNo(tPayNo[index]);
    	  tLRCessListSchema.setReNewCount(tRenewCount[index]);
    	  tLRCessListSchema.setGetDataDate(tGetDataDate[index]);
    	  tLRCessListSchema.setPrem(tPrem[index]);
    	  tLRCessListSchema.setMakeDate(tNewGetDataDate[index]);//用makedate存储新的提数日期getdatadate
    	  tLRCessListSchema.setAppntName(tNewReinsureitem[index]);//用投保人字段存储新修改的reinsureitem
    	  tLRCessListSchema.setInsuredName(tNewPayNo[index]);//用被保人字段存储新修改的payNo
    	  
    	  tLRCessListSet.add(tLRCessListSchema);
      }
  }
  System.out.println("开始执行BL页面");
VData tVData = new VData();
tVData.addElement(tGI);
tVData.addElement(tLRCessListSet);
  //将团单的公共信息通过TransferData传到UI
	LRFillDataBL tLRFillDataBL = new LRFillDataBL();
  try
  {
	tLRFillDataBL.submitHMData(tVData,"HM");
  }
  catch(Exception ex)
  {
    Content = "失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  tError = tLRFillDataBL.mErrors;
	if (FlagStr=="")
	{
	  if (!tError.needDealError())
	  {
	    Content = "完成";
	  	FlagStr = "Succ";
	  }
	  else
	  {
	  	Content = " 失败，原因是:" + tError.getFirstError();
	  	FlagStr = "Fail";
	  }
	}
	
	 
%>

<html>
	<script language="javascript">
			parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>