
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    
<%
   	GlobalInput tGI = (GlobalInput) session.getValue("GI");	
	
%> 

<script language="JavaScript">
var turnPage = new turnPageClass();
var ManageCom=<%=tGI.ManageCom%>
var operator =<%=tGI.Operator%>;
function initInpBox()
{    	
	 
}       
                            
function initForm() 
{	  	
  try 
  {  
    initInpBox();
    initLRAccountSettleGrid();  
  }
  catch(re) 
  {
    alert("操作错误！请关掉上次登陆打开的页面");
  }
}

//领取项信息列表的初始化
var LRAccountSettleGrid;
function initLRAccountSettleGrid() 
{                   
  var iArray = new Array();
  try 
  {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         		//列名
	    iArray[0][4]="station";         		//列名
	    
	    iArray[1]=new Array();
	    iArray[1][0]="再保公司";         	  //列名
	    iArray[1][1]="150px";            	//列宽
	    iArray[1][2]=200;            			//列最大值
	    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	    
	    iArray[2]=new Array();
	    iArray[2][0]="年份";         	
	    iArray[2][1]="50px";            	
	    iArray[2][2]=200;            		 
	    iArray[2][3]=0; 
	    iArray[2][21]="belongyear"             		 
	    
	    iArray[3]=new Array();
	    iArray[3][0]="月份";         	  //列名
	    iArray[3][1]="50px";            	//列宽
	    iArray[3][2]=200;            			//列最大值
	    iArray[3][3]=0;
	    iArray[3][21]="belongmonth"              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[4]=new Array();
	    iArray[4][0]="再保合同编号";      //列名
	    iArray[4][1]="80px";            	//列宽
	    iArray[4][2]=200;            			//列最大值
	    iArray[4][3]=0;
	    iArray[4][21]="recontcode"              			//是否允许输入,1表示允许，0表示不允许  
	    
	    iArray[5]=new Array();
	    iArray[5][0]="险种编码";              //列名
	    iArray[5][1]="80px";            	//列宽
	    iArray[5][2]=200;            			//列最大值
	    iArray[5][3]=0; 
	    iArray[5][21]="riskcode"             			//是否允许输入,1表示允许，0表示不允许

	    iArray[6]=new Array();
	    iArray[6][0]="分出保费";              //列名
	    iArray[6][1]="80px";            	//列宽
	    iArray[6][2]=200;            			//列最大值
	    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[7]=new Array();
	    iArray[7][0]="分出保费状态";              //列名
	    iArray[7][1]="80px";            	//列宽
	    iArray[7][2]=200;            			//列最大值
	    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[8]=new Array();
	    iArray[8][0]="分保手续费";              //列名
	    iArray[8][1]="80px";            	//列宽
	    iArray[8][2]=200;            			//列最大值
	    iArray[8][3]=0; 

	    iArray[9]=new Array();
	    iArray[9][0]="分保手续费状态";              //列名
	    iArray[9][1]="100px";            	//列宽
	    iArray[9][2]=200;            			//列最大值
	    iArray[9][3]=0; 

	    iArray[10]=new Array();
	    iArray[10][0]="理赔摊回金额";              //列名
	    iArray[10][1]="80px";            	//列宽
	    iArray[10][2]=200;            			//列最大值
	    iArray[10][3]=0; 

	    iArray[11]=new Array();
	    iArray[11][0]="理赔摊回状态";              //列名
	    iArray[11][1]="80px";            	//列宽
	    iArray[11][2]=200;            			//列最大值
	    iArray[11][3]=0;

	    iArray[12]=new Array();
	    iArray[12][0]="合同类型";              //列名
	    iArray[12][1]="80px";            	//列宽
	    iArray[12][2]=200;            			//列最大值
	    iArray[12][3]=0;

	    iArray[13]=new Array();
	    iArray[13][0]="数据类型代码";              //列名
	    iArray[13][1]="80px";            	//列宽
	    iArray[13][2]=200;            			//列最大值
	    iArray[13][3]=3;

	    iArray[14]=new Array();
	    iArray[14][0]="数据类型";              //列名
	    iArray[14][1]="80px";            	//列宽
	    iArray[14][2]=200;            			//列最大值
	    iArray[14][3]=0;

	    iArray[15]=new Array();
	    iArray[15][0]="结算金额";              //列名
	    iArray[15][1]="80px";            	//列宽
	    iArray[15][2]=200;            			//列最大值
	    iArray[15][3]=0;

	    iArray[16]=new Array();
	    iArray[16][0]="结算日期";              //列名
	    iArray[16][1]="80px";            	//列宽
	    iArray[16][2]=200;            			//列最大值
	    iArray[16][3]=0;
	    iArray[16][21]="datatype";
		 
		iArray[17]=new Array();
	    iArray[17][0]="结算人";              //列名
	    iArray[17][1]="80px";            	//列宽
	    iArray[17][2]=200;            			//列最大值
	    iArray[17][3]=0;
	    iArray[17][21]="datatype";   
    
    LRAccountSettleGrid = new MulLineEnter("fm", "LRAccountSettleGrid"); 
    //设置Grid属性
    LRAccountSettleGrid.mulLineCount = 10;
    LRAccountSettleGrid.displayTitle = 1;
    LRAccountSettleGrid.locked = 1;
<!--    LRAccountSettleGrid.canSel = 1;-->
    LRAccountSettleGrid.canChk = 1;
    LRAccountSettleGrid.hiddenSubtraction = 1;
    LRAccountSettleGrid.hiddenPlus = 1;
    LRAccountSettleGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
} 
</script>
