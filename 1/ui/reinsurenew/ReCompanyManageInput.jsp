<html>
<%
	//name :ReCompanyManageInput.jsp
	//function :ReCompanyManage
	//Creator :liuli
	//date :2008-10-16
%>
<%@page contentType="text/html;charset=GBK"%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.reinsure.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="./ReCompanyManageInput.js"></SCRIPT>
<%@include file="./ReCompanyManageInit.jsp"%>
</head>
<body onload="initElementtype();initForm()">
	<form action="./ReCompanyManageSave.jsp" method=post name=fm
		target="fraSubmit">
		<Div id="divLLReport1" style="display: ''">
			<br>
			<table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;" OnClick="showPage(this,divCertifyType);"></td>
					<td class=titleImg>再保公司</td>
				</tr>
			</table>

			<Div id="divCertifyType" style="display: ''">
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1><span id="spanCompanyGrid"></span>
						</td>
					</tr>
				</table>
				<Div id="div1" align="left" style="display: ''">
					<INPUT VALUE="首  页" class=cssButton TYPE=button
						onclick="turnPage.firstPage();"> <INPUT VALUE="上一页"
						class=cssButton TYPE=button onclick="turnPage.previousPage();">
					<INPUT VALUE="下一页" class=cssButton TYPE=button
						onclick="turnPage.nextPage();"> <INPUT VALUE="尾  页"
						class=cssButton TYPE=button onclick="turnPage.lastPage();">
				</div>
			</div>
			<br>
			<table>
				<table>
					<tr>
						<td class=common><IMG src="../common/images/butExpand.gif"
							style="cursor: hand;" OnClick="showPage(this,divCertifyType);"></td>
						<td class=titleImg>详细信息</td>
					</tr>
				</table>
				<Table class=common>
					<TR class=common>
						<TD class=title>公司编码</TD>
						<TD class=input><Input class=code name=ReComCode
							id="ReComCodeId" Readonly></TD>
						<TD class=title>公司名称</TD>
						<TD class=input colspan=2><Input class=common name=ReComName
							id="ReComNameId" style="width: 100%"></TD>
						<TD class=title>SAP客户号</TD>
						<TD class=input><Input class=common name=SAPAppntNo
							id="SapId"> <font color='red'>*</font></TD>
					</TR>

					<TR>
						<TD class=title>公司简称</TD>
						<TD class=input><Input class=common name=SimpleName
							id="SimpleNameId"></TD>
						<td class="title">邮政编码</td>

						<td class="input" colspan=2><Input class=common
							name=PostalCode id="PosttalCodeId"></td>
						<TD class=title>传&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;真</TD>
						<TD class=input><Input class=common name=FaxNo id="FaxId">
						</TD>

					</TR>

					<TR>
						<TD class=title>通讯地址</TD>
						<TD class=input colspan=5><Input class=common name=Address
							id="AddressId" style="width: 98%"></TD>
					</TR>
					<TR>
						<TD class=title>保险机构代码</TD>
						<TD class=input><Input class=common name=CompanyCode
							id="CompanyCodeId"> <font color='red'>*</font></TD>
					</TR>
				</Table>
				<br>
			</table>
			<table>
				<tr>
					<td class=title><INPUT class=cssButton VALUE="保 存" TYPE=button
						onClick="submitForm();"></td>
					<td class=title><INPUT class=cssButton VALUE="修 改" TYPE=button
						onClick="updateForm();"></td>
					<td class=title><INPUT class=cssButton VALUE="删 除" TYPE=button
						onClick="deleteForm();"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			<input class=common name=OperateType type=hidden>
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>