<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate(); 
%>
<!--<script language="javascript">
    
   function initDate(){
     fm.InputDate.value="<%=CurrentDate%>";
        }
</script> --> 
        
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html;charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LRDataTransferConfirm.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRDataTransferConfirmInit.jsp"%>
 
  <title>清单预估数据生成</title>   
</head>
<body  onload="initForm();initElementtype();" >
	<!--initDate();-->
  
 <form action="./LRDataTransferConfirmSave.jsp" method=post name=fm target="fraSubmit"> 
    <table>
     <tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssess1);">
      </td>
      <td class=titleImg>
          请输入查询条件
      </td>
     </tr>
    </table>
    <Div  id="divLAAssess1" style= "display:''">
    <table class=common align=center>
     <TR  class=common>
      <TD  class=title>
          年份
      </TD>
      <TD  class=input>
       <Input class=common name=year elementtype=nacessary verify="年份|num&len=4" 
       onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>       
      <TD  class=title>
          月份
      </TD>
      <TD  class=input>
       <Input class=common name=month elementtype=nacessary verify="月份|num&len=2&value>=01&value<=12"
        onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>
     </TR>  
    </table>
  <INPUT class=cssButton VALUE="查   询" TYPE=button onclick="queryData();">
     
   
   <table  class=common>
     <tr  class=common>
      <td text-align: left colSpan=1>
      <span id="spanDataTransferConfirmGrid" >
      </span> 
      </td>
     </tr>
    </table>
    <div  align="center">
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton >                  
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
      </div>
      <br/>
      <INPUT class=cssButton VALUE="流转确认(分出保费)" name ="cesspremButton" TYPE=button onClick="dataConfirm1();">
      <INPUT class=cssButton VALUE="流转确认(手续费、理赔摊回)"  name ="claimbackfeeButton" TYPE=button onClick="dataConfirm2();">
    </div>
    <br/>
    <font color="red" >注：每个月的数据流转一次大约需要20分钟左右，如果页面没有反应，请刷新页面后点击【查询】，查看对应的数据状态</font>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden name=Type value='1'>
    <input type=hidden name=MakeContNo  value=''>
    <input type=hidden id="fmAction" name="fmAction">  
    <input type=hidden class=Common name=querySql > 
    <input type=hidden name=feetype value='' > 
  </form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
