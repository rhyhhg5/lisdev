var showInfo;

var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var strSQL ="";


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}


function checkNum(control){
   var a = control.value.match(/^(-?\d+)(\.\d+)?$/); 
   if (a == null) {
      alert('数值不符合');
			control.select();
			control.focus();
			return false;
   } 
}


function check(){
	if(vCheck()==false){
		return false;
	}
	if(confirm('是否确定对选中的信息做审核通过操作')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="CONFIRM";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}

function vCheck(){
	var rowCount=0;
	for(var i=0;i<NewAccountCheckGrid.mulLineCount;i++){
		if(NewAccountCheckGrid.getChkNo(i)==true){
			if(rowCount>1){
				break;
			}
			rowCount++;
		}
	}
	if(rowCount==0){
		alert('请选择需要审核的信息');
		return false;
	}
}

function onloadCesslist(){

	try 
	{
 		var strWhere="";
 		if(fm.StartDate.value!=''){
 	 		strWhere+=" and getdatadate>='"+fm.StartDate.value+"' ";
 	 		
 	 	}
 		if(fm.EndDate.value!=''){
 	 		strWhere+=" and getdatadate<='"+fm.EndDate.value+"' ";
 	 		
 	 	}	 		
 	 	if(fm.RecontCode.value!=''){
 	 		strWhere+=" and RecontCode='"+fm.RecontCode.value+"' ";
 	 		
 	 	}
 	 	if(fm.RiskCode.value != ''){
 	 		strWhere+=" and RiskCode='"+fm.RiskCode.value+"' ";
 	 		
 	 	}
 	 	if(fm.TempCessFlagName.value != ''){
 	 		strWhere+=" and TempCessFlag='"+fm.TempCessFlagName.value+"' ";
 	 		
 	 	}
 	 	

 	 	//2014-10-27   杨阳
 	 	//增加保单号原则：社保业务增加保单号，非社保业务为空；
 	    // 社保业务定义： 成本中心第五、六位为93的是社保业务；

 	 	strSQL ="select managecom,riskcode,recontcode,costcenter,grpcontno,"+
			" (select b.RecomName from lrrecominfo b,lrcontinfo c where b.Recomcode = c.Recomcode " +
			" and c.RecontCode = aa.recontCode union all select b.RecomName from lrrecominfo b,lrtempcesscont c " +
 			" where b.Recomcode = c.comcode " +
 			" and c.tempcontcode = aa.recontCode),sum(cessprem),sum(reprocfee),sum(claimbackfee) "+
			" ,tempcessflag from  " +
			" ((select managecom,riskcode,recontcode,costcenter,(case grpcontno when '00000000000000000000' then contno else grpcontno end) as grpcontno,cessprem as cessprem,reprocfee as  " +
			" reprocfee,0 as claimbackfee,tempcessflag from lrcesslist  where  1=1" +strWhere+
	 		"  and SUBSTR(costcenter,5,2)='93' )"  +
			" union all (select  managecom,riskcode,recontcode,costcenter,(case grpcontno when '00000000000000000000' then contno else grpcontno end) as grpcontno,0 as cessprem," +
			" 0 as reprocfee,claimbackfee" +
			" as claimbackfee,tempcessflag from lrclaimlist where 1=1 "+strWhere+
			" and SUBSTR(costcenter,5,2)='93'  ))"+
			" as aa  group by   recontcode,riskcode,costcenter,grpcontno,managecom,tempcessflag " +
 	 		" union all"+
 	 		" select managecom,riskcode,recontcode,costcenter, '',"+
			" (select b.RecomName from lrrecominfo b,lrcontinfo c where b.Recomcode = c.Recomcode " +
			" and c.RecontCode = aa.recontCode union all select b.RecomName from lrrecominfo b,lrtempcesscont c " +
 			" where b.Recomcode = c.comcode " +
 			" and c.tempcontcode = aa.recontCode),sum(cessprem),sum(reprocfee),sum(claimbackfee) "+
			" ,tempcessflag from  " +
			" ((select managecom,riskcode,recontcode,costcenter,cessprem as cessprem,reprocfee as  " +
			" reprocfee,0 as claimbackfee,tempcessflag from lrcesslist  where 1=1 "+strWhere+
	 		" and SUBSTR(costcenter,5,2)<>'93'  ) "+
			" union all (select  managecom,riskcode,recontcode,costcenter,0 as cessprem," +
			" 0 as reprocfee,claimbackfee as claimbackfee,tempcessflag from lrclaimlist where 1=1 "+strWhere+
			" and SUBSTR(costcenter,5,2)<>'93' )) "+
			" as aa  group by   recontcode,riskcode,costcenter,managecom,tempcessflag with ur" ;
 	 	
		var i = 0;
//		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.OperateType.value="ONLOADCess";
		 fm.querySql.value = strSQL;
		 fm.action = "LREstimateWSave.jsp";
		 fm.submit();
  } catch(ex) 
  {
  	//showInfo.close( );
  	alert(ex);
  }
}





function downAfterSubmit(cfilePath) {
	showInfo.close();
	fileUrl.href = cfilePath; 
  fileUrl.click();
}

function showMonthLastDay()
{
     var tmpDate=new Date(fm.Year.value,fm.Month.value,1); 
     var MonthLastDay=new Date(tmpDate-86400000);
     return MonthLastDay.getDate();      
}

        

