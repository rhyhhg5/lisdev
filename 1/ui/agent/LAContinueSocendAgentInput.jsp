<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
	//程序名称：LAAgentInput.jsp
	//程序功能：个人代理增员管理
	//创建日期：2002-08-16 15:39:06
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String BranchType=request.getParameter("BranchType");
	String BranchType2=request.getParameter("BranchType2");
	System.out.println("BranchType:"+BranchType);
	System.out.println("BranchType2:"+BranchType2);
	String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='05' ";
	%>
	<script>
		var manageCom = <%=tG.ManageCom%>;
		var mBranchType =  <%=BranchType%>;
		var mBranchType2 =  '<%=BranchType2%>';
	    var msql='1 and branchtype=#'+<%=BranchType%>+'#  and branchtype2=#05# ';
	</script>
	<%@page contentType="text/html;charset=GBK" %>
	<head >
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="LAContinueSocendAgentInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LAContinueAgentInit.jsp"%>
		<%@include file="../agent/SetBranchType.jsp"%>
	</head>
	<body  onload="initForm();initElementtype();" >
		<form action="./LAContinueSocendAgentSave.jsp" method=post name=fm target="fraSubmit">
			<%//@include file="./AgentOp1.jsp"%>
			<%//@include file="./AgentOp4.jsp"%>
	       <table class="common" align=center>
				<tr align=right>
					<td class=button >
						&nbsp;&nbsp;
					</td>
					<td class=button width="10%">
						<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return agentConfirm();">
					</td>
					<td class=button width="10%">
						<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="return queryClick();">
					</td>
				</tr>
	       </table>
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
						<td class=titleImg>
							服务专员信息
						</td>
				</tr>
			</table>
			<Div  id= "divLAAgent1" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD class= title>
							服务专员代码
						</TD>
						<TD  class= input>
							<Input class= 'readonly' readonly name=AgentCode >
							<input type=hidden name=HiddenAgentCode value=''>
						</TD>
						<TD  class= title>
							姓名
						</TD>
						<TD  class= input>
							<Input name=Name class= common verify="姓名|NotNull&len<=15" elementtype=nacessary >
						</TD>
						<TD  class= title>
							性别
						</TD>
						<TD  class= input>
							<Input name=Sex class="codeno" verify="性别|NotNull" 
							ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,null,'100');" 
							onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" debug='10'
							><Input name=SexName class="codename"  readonly elementtype=nacessary >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							出生日期
						</TD>
						<TD  class= input>
							<Input name=Birthday class=common  verify="出生日期|NotNull&Date" elementtype=nacessary>
						</TD>
						<TD  class= title>
							证件类型
						</TD>
						<TD  class= input>
							<Input name=IDNoType class= "codeno" verify="证件类型|notnull&code:idtype"
							ondblclick="return showCodeList('idtype',[this,IDNoTypeName],[0,1],null,null,null,null,'100');"
							onkeyup="return showCodeListKey('idtype',[this,IDNoTypeName],[0,1]);"
							><Input name=IDNoTypeName class="codename" readonly elementtype=nacessary>
						</TD>
						<TD  class= title>
							证件号码
						</TD>
						<TD  class= input>
							<Input name=IDNo class= common verify="证件号码|notnull&len<=20" elementtype=nacessary onchange="return changeIDNo();">
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							籍贯
						</TD>
						<TD  class= input>
							<Input name=NativePlace class="codeno" verify="籍贯|code:NativePlaceBak" id="NativePlaceBak" 
							ondblclick="return showCodeList('NativePlaceBak',[this,NativePlaceName],[0,1]);" 
							onkeyup="return showCodeListKey('NativePlaceBak',[this],[0,1]);"
							><Input name=NativePlaceName class="codename"  >
						</TD>
						<TD  class= title>
							政治面貌
						</TD>
						<TD  class= input>
							<Input name=PolityVisage class="codeno" verify="政治面貌|code:polityvisage" id="polityvisage" 
							ondblclick="return showCodeList('polityvisage',[this,PolityVisageName],[0,1]);" 
							onkeyup="return showCodeListKey('polityvisage',[this,PolityVisageName],[0,1]);"
							><Input name=PolityVisageName class="codename"  >
						</TD>
						<TD  class= title>
							户口所在地
						</TD>
						<TD  class= input>
							<Input name=RgtAddress class="codeno" 
							ondblclick="return showCodeList('NativePlaceBak',[this,RgtAddressName],[0,1],null,null,null,null,'160');" 
							onkeyup="return showCodeListKey('NativePlaceBak',[this,RgtAddressName],[0,1]);"
							><Input name=RgtAddressName class="codename"  >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							学历
						</TD>
						<TD  class= input>
							<Input name=Degree class="codeno" verify="学历|code:Degree" id="Degree" 
							ondblclick="return showCodeList('Degree',[this,DegreeName],[0,1]);" 
							onkeyup="return showCodeListKey('Degree',[this,DegreeName],[0,1]);"
							><Input name=DegreeName class="codename"  >
						</TD>
						<TD  class= title>
							毕业院校
						</TD>
						<TD  class= input>
							<Input name=GraduateSchool class= common verify="毕业院校|len<=13"  >
						</TD>
						<TD  class= title>
							专业
						</TD>
						<TD  class= input>
							<Input name=Speciality class= common  verify="毕业院校|len<=13"  >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							民族
						</TD>
						<TD  class= input>
							<Input name=Nationality class="codeno" id="Nationality" 
							ondblclick="return showCodeList('Nationality',[this,NationalityName],[0,1],null,null,null,null,'100');" 
							onkeyup="return showCodeListKey('Nationality',[this,NationalityName],[0,1]);"
							><Input name=NationalityName class="codename"  >
						</TD>
						<TD  class= title>
							家庭地址
						</TD>
						<TD  class= input>
							<Input name=HomeAddress class= common  verify="家庭地址|len<=26">
						</TD>
						<TD  class= title>
							邮政编码
						</TD>
						<TD  class= input>
							<Input name=ZipCode class= common verify="邮政编码|len=6">
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							联系电话
						</TD>
						<TD  class= input>
							<Input name=Phone class= common verify="联系电话|len<=18&notnull" elementtype=nacessary onchange="return checkPhone();">
						</TD>
						<TD  class= title>
							传呼
						</TD>
						<TD  class= input>
							<Input name=BP class= common verify="传呼|len<=20" >
						</TD>
						<TD  class= title>
							手机
						</TD>
						<TD  class= input>
							<Input name=Mobile class= common verify="手机|len<=15"  onchange="return checkMobile();">
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							职称
						</TD>
						<TD  class= input>
							<Input name=ContinuePostTitle class='codeno' verify="职称|code:continueposttitle" 
							ondblclick="return showCodeList('ContinuePostTitle',[this,ContinuePostTitleName],[0,1]);" 
							onkeyup="return showCodeListKey('ContinuePostTitle',[this,ContinuePostTitleName],[0,1]);"
							><Input name=ContinuePostTitleName class="codename"  >
						</TD>
						<TD  class= title>
							E_mail
						</TD>
						<TD  class= input>
							<Input name=EMail class= common verify="E_mail|len<=30" >
						</TD>
						<TD  class= title>
							原工作单位
						</TD>
						<TD  class= input>
							<Input name=OldCom class= common  verify="原工作单位|len<=20" >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							原职业
						</TD>
						<TD  class= input>
							<Input name=OldOccupation class='common' verify="原职业|len<=10" >
						</TD>
						<TD  class= title>
							原工作职务
						</TD>
						<TD  class= input>
							<Input name=HeadShip class= common  verify="原工作职务|len<=10" >
						</TD>
						<TD  class= title>
							培训期数
						</TD>
						<TD  class= input>
							<Input name=TrainPeriods class=common verify="培训期数|INT" >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							入司日期
						</TD>
						<TD  class= input>
							<Input name=EmployDate class='coolDatePicker' dateFormat='short' verify="入司时间|NotNull&Date" elementtype=nacessary  onchange="return checkdate();">
						</TD>
						<TD  class= title>
							操作员代码
						</TD>
						<TD  class= input>
							<Input name=Operator class= 'readonly' readonly >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							备注
						</TD>
						<TD  class=input colSpan= 3>
							<Input name=Remark class= common3 >
						</TD>
	          <TD  class= title>
             人员类别
            </TD>
            <TD  class= input>
              <Input name=InsideFlag class='codeno' verify="人员类别" 
               ondblclick="return showCodeList('insideflag',[this,InsideFlagName],[0,1]);" 
               onkeyup="return showCodeListKey('insideflag',[this,InsideFlagName],[0,1]);"
              ><Input class=codename name=InsideFlagName readOnly>
            </TD>	
					</TR>
					<TR  class= common>
						<!-- <TD class= title>
							筹备标记
						</TD>
					  <TD class= input>
						  <Input class=codeno readOnly name=NoWorkFlag verify="筹备标记" CodeData="0|^Y|是|^N|否"
						   ondblClick="showCodeListEx('noworkflag',[this,NoWorkFlagName],[0,1],null,null,null,1);  "
               onkeyup="showCodeListKeyEx('noworkflag',[this,NoWorkFlagName],[0,1],null,null,null,1);"
              ><Input class=codename name=NoWorkFlagName  readOnly >
					  </TD>
						<TD  class= title>
							筹备开始日期
						</TD>
						<TD  class= input>
							<Input name=TrainDate class='coolDatePicker' dateFormat='short' verify="筹备开始日期|Date" >
						</TD> -->
		<TD class=title>服务专员类型</TD>
		<TD class=input><Input name=AgentType class='codeno'
			verify="服务专员类型|notnull"><Input
			class=codename name=AgentTypeName elementtype=nacessary readOnly></TD>
	</TR>
					<TR  class= common>
						<TD  class=input colSpan= 3>
							<font color='red'>提示：输入项“联系电话”会打印到客户的相关单证上</font>
						</TD>
					</TR>
				</table>
			</Div>
				
 <!--行政信息-->
 <table>
 	<tr>
 		<td class=common>
 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
 			<td class= titleImg>
 				行政信息
 			</td>
 	</tr>
 </table>
 <Div id= "divLAAgent3" style= "display: ''">
 	<table class=common>
 		<tr class=common>
 			<TD class= title>
 				服务专员职级
 			</TD>
 			<TD class= input>
 				<Input name=AgentGrade class="codeno"  verify="营销员职级|notnull&code:AgentGrade"
 				ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
 				onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
 				><Input name=AgentGradeName class="codename" elementtype=nacessary >
 			</TD>
 			<TD class= title>
 				收费服务组代码
 			</TD>
 			<TD class= input>
 				<Input class=common name=BranchCode verify="收费服务组代码|notnull" onchange="return changeGroup();" elementtype=nacessary>
 			</TD>
 			<TD class= title>
 				管理机构
 			</TD>
 			<TD class= input>
 				<!--Input name=ManageCom class='code' verify="管理机构|notnull" ondblclick="return showCodeList('station',[this]);" -->
 				<Input name=ManageCom class='readonly'readonly >
 			</TD>
 		</tr>
 		<tr class=common>
 			<TD class= title>
 				收费服务组主管
 			</TD>
 			<TD class= input>
 				<Input name=GroupManagerName class='readonly'readonly >
 			</TD>
 			<TD class= title>
 				部经理
 			</TD>
 			<TD class= input>
 				<Input name=DepManagerName class='readonly'readonly >
 			</TD>
 		<!-- 	<TD class= title>
 				收费服务部经理
 			</TD>
 			<TD class= input>
 				<Input name=Minister class='readonly'readonly >
 			</TD> -->
 		</tr>
 			<!--TR  class= common>
 		<TD  class= title>
 		是否有待业证标志
 		</TD>
 		<TD  class= input>
 		<Input name=NoWorkFlag class='code' verify="是否有待业证标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
 		</TD>
 		<TD  class= title>密码</TD>
 		<TD  class= input><Input name=Password class=common ></TD>
 		<TD  class= title>推荐报名编号</TD>
 		<TD  class= input><Input name=EntryNo class= common ></TD>
 		<TD  class= title>来源地</TD>
 		<TD  class= input><Input name=Source class= common ></TD>
 		<TD  class= title>血型</TD>
 		<TD  class= input>
 		<Input name=BloodType class="code" verify="血型|code:BloodType" id="BloodType" ondblclick="return showCodeList('BloodType',[this]);" onkeyup="return showCodeListKey('BloodType',[this]);">
 		</TD>
 		<TD  class= title>婚姻状况</TD>
 		<TD  class= input>
 		<Input name=Marriage class="code" verify="婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);" >
 		</TD>
 		<TD  class= title>结婚日期</TD>
 		<TD  class= input>
 		<Input name=MarriageDate class="coolDatePicker" dateFormat="short" >
 		</TD>
 		<TD  class= title>外语水平</TD>
 		<TD  class= input>
 		<Input name=ForeignLevel class="code" verify="外语水平|code:EngLevel" id="EngLevel" ondblclick="return showCodeList('EngLevel',[this]);" onkeyup="return showCodeListKey('EngLevel',[this]);">
 		</TD>
 		<TD  class= title>家庭地址编码</TD>
 		<TD  class= input><Input name=HomeAddressCode class= common ></TD>
 		<TD  class= title>通讯地址</TD>
 		<TD  class= input><Input name=PostalAddress class= common ></TD>
 		<TD  class= title>是否吸烟标志</TD>
 		<TD  class= input>
 		<Input name=SmokeFlag class='code' verify="是否吸烟标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);">
 		</TD>
 		<TD  class= title>银行编码</TD>
 		<TD  class= input>
 		<Input name=BankCode class='codeno' verify="银行编码|code:bank" ondblclick="return showCodeList('bank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('bank',[this,BankCodeName],[0,1]);"><Input name=BankCodeName class="codename"  >
 		</TD>
 		<TD  class= title>从业年限</TD>
 		<TD  class= input><Input name=WorkAge class=common></TD>
 		<TD  class= title>推荐代理人</TD>
 		<TD  class= input><Input name=RecommendAgent class= common></TD>
 		<TD  class= title>工种/行业</TD>
 		<TD  class= input><Input name=Business class=common ></TD>
 		<TD  class= title>信用等级</TD>
 		<TD  class= input><Input name=CreditGrade class=common ></TD>
 		<TD  class= title>销售资格</TD>
 		<TD  class= input>
 		<Input name=SaleQuaf class='code' verify="销售资格|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
 		</TD>
 		<TD  class= title>证书开始日期</TD>
 		<TD  class= input><Input name=QuafStartDate class="coolDatePicker" dateFormat="short" ></TD>
 		<TD  class= title>展业证号码2</TD>
 		<TD  class= input><Input name=DevNo2 class= common ></TD>
 		<TD  class= title>聘用合同号码</TD>
 		<TD  class= input><Input name=RetainContNo class= common ></TD>
 		<TD  class= title>代理人类别</TD>
 		<TD  class= input>
 		<Input name=AgentKind class='code' verify="代理人类别|code:AgentKind" ondblclick="return showCodeList('AgentKind',[this]);" onkeyup="return showCodeListKey('AgentKind',[this]);"  >
 		</TD>
 		<TD  class= title>业务拓展级别</TD>
 		<TD  class= input><Input name=DevGrade class=common ></TD>
 		<TD  class= title>内勤标志</TD>
 		<TD  class= input>
 		<Input name=InsideFlag class='code' verify="内勤标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
 		</TD>
 		<TD  class= title>是否专职标志</TD>
 		<TD  class= input>
 		<Input name=FullTimeFlag class='code' verify="是否专职标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
 		</TD>
 		<TD  class= title>转正日期</TD>
 		<TD  class= input><Input name=InDueFormDate class='coolDatePicker' dateFormat='short'></TD>
 		<TD  class= title>代理人状态</TD>
 		<TD  class= input>
 		<Input name=AgentState class='code' verify="代理人状态|code:agentstate" ondblclick="return showCodeList('agentstate',[this]);" onkeyup="return showCodeListKey('agentstate',[this]);" >
 		</TD>
 		<TD  class= title>复核员</TD>
 		<TD  class= input><Input name=Approver class= common ></TD>
 		<TD  class= title>复核日期</TD>
 		<TD  class= input><Input name=ApproveDate class= common ></TD>
 		<TD  class= title>标志位</TD>
 		<TD  class= input><Input name=QualiPassFlag class= common MAXLENGTH=1></TD>
 		<TD class= title>组育成人</TD>
 		<TD class= input><Input name=RearAgent class=common verify="组育成人|len<=10"></TD>
 		<TD class= title>部育成人</TD>
 		<TD class= input><Input name=RearDepartAgent class=common verify="部育成人|len<=10"></TD>
 		<TD class= title>督导长育成人</TD>
 		<TD class= input><Input class=common name=RearSuperintAgent verify="督导长育成人|len<=10"></TD>
 		<TD class= title>区督导长育成人</TD>
 		<TD class= input>
 		<Input class=common name=RearAreaSuperintAgent verify="区域督导长育成人|len<=10">
 		</TD>
 		</tr>
 		-->
 	</table>
 	<!--代理人系列--> <Input name=AgentSeries type=hidden>
 </Div>
 <!--担保人信息（列表） -->
 <table>
 	<tr>
 		<td class=common>
 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
 			<td class= titleImg>
 				担保人信息
 			</td>
 	</tr>
 </table>
 <Div  id= "divLAAgent2" style= "display: ''">
 	<table  class= common>
 		<tr  class= common>
 
 			<td text-align: left colSpan=1>
 				<span id="spanWarrantorGrid" >
 				</span>
 			</td>
 		</tr>
 	</table>
 </div>
 <input type=hidden name=hideOperate value=''>
<!-- <input type=hidden name=initOperate value='INSERT'>-->
 <input type=hidden name=hideIsManager value='false'>
 <Input type=hidden name=AgentState value='02' >       <!--代理人状态-->
 <Input type=hidden name=initAgentState value='02' >   <!--代理人状态-->
 <!--input type=hidden name=hideManageCom value=''-->
 <input type=hidden name=BranchType value=''>
 <input type=hidden name=BranchType2 value=''>
 <input type=hidden name=hideBranchCode value=''> <!--所属组的隐式代码-->
 <input type=hidden name=UpAgent value=''>
 <input type=hidden name=ManagerCode value=''> <!--机构管理人员代码-->
 <input type=hidden name=upBranchAttr value=''>
 <input type=hidden name=SpeciFlag value=''>
 <input type=hidden name=BankCodeName value=''>
 <input type=hidden name=BankCode value=''>
 <input type=hidden name=OrphanCode value=''>
 <input type=hidden name=PrepareEndDate value=''><!--筹备结束日期-->
 <input type=hidden name=PreparaGrade value=''><!--筹备职级-->
 <input type=hidden name=WageVersion value=''><!--薪资版本-->
 <input type=hidden name=GBuildFlag value=''><!--团建标识-->
 <input type=hidden name=GBuildStartDate value=''><!--团建开始时间-->
 <input type=hidden name=GBuildEndDate value=''><!--团建结束时间-->
 <Input type=hidden name=hiddenAgentGrade value=''  >  
 <input type=hidden name=diskimporttype value='LAQualification'>
  <Input type=hidden name=hiddenEmployDate value='' > 
   <Input name=OutWorkDate class=common type=hidden >
 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>


