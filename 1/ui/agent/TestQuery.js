var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

// 页面查询功能
function rqueryClick()
{
   if (verifyInput()==false) return false; // 页面简单校验
   initBranchGroupGrid();
   var strSQL = "";
	strSQL = "select ManageCom,(select Name from LDCom where ComCode=laassessmain.ManageCom),indexcalno,AgentGrade,state,BranchType,(select CodeName from ldcode where codetype = 'branchtype' and Code=laassessmain.BranchType),BranchType2,(select CodeName from ldcode where codetype = 'branchtype2' and Code=laassessmain.BranchType2) from laassessmain where 1=1 "    
			+ getWherePart('ManageCom','ManageCom')//管理机构
			+ getWherePart('BranchType','BranchType')//展业类型
			+ getWherePart('BranchType2','BranchType2')//销售渠道
			+ getWherePart('indexcalNo','WageNo')//薪资月
			+ getWherePart('State','State')//考核状态
			+ getWherePart('AgentGrade','AgentGrade');//职级
	
	var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	if(!strQueryResult)
	{  
		alert("没有符合条件的信息！"); 
		turnPage.queryModal(strSQL, BranchGroupGrid);
		return false;
	}
	else turnPage.queryModal(strSQL, BranchGroupGrid);   	
}

//返回操作
function returnClick()
{
    var arrReturn = new Array();
	var tSel = BranchGroupGrid.getSelNo();
		
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
			try
			{		
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
// 查询返回数据信息
function getQueryResult()
{
   	var arrSelected = null;
	tRow = BranchGroupGrid.getSelNo();
	if( tRow == 0 || tRow == null) return arrSelected;
	arrSelected = new Array();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select ManageCom,(select Name from LDCom where ComCode=laassessmain.ManageCom),indexcalno,AgentGrade,state,BranchType,(select CodeName from ldcode where codetype = 'branchtype' and Code=laassessmain.BranchType),BranchType2,(select CodeName from ldcode where codetype = 'branchtype2' and Code=laassessmain.BranchType2)";
	strSQL +="  from laassessmain where ManageCom='"+BranchGroupGrid.getRowColData(tRow-1,1)+"' and indexcalno='"+BranchGroupGrid.getRowColData(tRow-1,3)+"' and AgentGrade='"+BranchGroupGrid.getRowColData(tRow-1,4)+"' and BranchType='"+BranchGroupGrid.getRowColData(tRow-1,6)+"' and BranchType2='"+BranchGroupGrid.getRowColData(tRow-1,8)+"' ";     
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
	
	
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
	
}