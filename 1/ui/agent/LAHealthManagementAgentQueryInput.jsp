<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAHealthManagementAgentQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAHealthManagementAgentQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>健管专员查询 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            <td class= titleImg>
                健管专员查询条件
            </td>
    	</tr>
     </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
       <TR  class= common>   
        <TD class= title>
          管理机构
        </TD>
        <TD  class= input>
            <Input class="codeno" name=ManageCom  verify="管理机构|notnull" 
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
             ><Input name=ManageComName class="codename" elementtype=nacessary> 
        </TD> 
        <TD class= title>
          销售团队代码 
        </TD>
        <TD class= input>
          <Input class=common name=AgentGroup > 
        </TD>
      </TR>
      <TR  class= common> 
        <TR  class= common> 
        <TD class= title> 
          健管专员代码 
        </TD>
        <TD  class= input> 
          <Input class=common  name=AgentCode >
        </TD>
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD>        
      </TR>
      <TR  class= common> 
        <TD  class= title>
          证件类型 
        </TD>
        
        <TD  class= input> 
          <Input name=IDNoType class= "codeno" 
                 ondblclick="return showCodeList('idtype',[this,IDNoTypeName],[0,1],null,null,null,null,'100');"
                 onkeyup="return showCodeListKey('idtype',[this,IDNoTypeName],[0,1]);" 
                 ><Input name=IDNoTypeName class="codename" readonly  > 
        </TD>            
        <TD  class= title>
           证件号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common > 
        </TD>
      </TR>
      <TR>
		<TD class=title>销售人员类型</TD>
		<TD class=input><Input name=AgentType class='codeno'
			verify="销售人员类型"
			ondblclick="return showCodeList('agenthealthtypecode',[this,AgentTypeName],[0,1]);"
			onkeyup="return showCodeListKey('agenthealthtypecode',[this,AgentTypeName],[0,1]);"><Input
			class=codename name=AgentTypeName readOnly></TD>
	</TR>      
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          
          <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();">
          <INPUT VALUE="返  回" class="cssbutton" TYPE=button onclick="returnParent();"> 	
    </Div>      
          				
    <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 健管专员查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage.lastPage();"> 						
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
