<%
//程序名称：LAZJDimissionQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  System.out.println("^^^^^^^^^^^^:"+BranchType);
%>
<script>
   var msqlBranch=" 1 and CODEALIAS =#"+'2'+"#";
</script>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="./LAZJDimissionQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LAZJDimissionQueryInit.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>离职管理 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAZJDimissionQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAZJDimission1);">
    </IMG>
      <td class=titleImg> 查询条件 </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAZJDimission1" style= "display: ''">
     <table  class= common>
       <tr  class= common> 
       	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL" 
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
          ><Input name=ManageComName class="codename" elementtype=nacessary > 
          </TD> 
         <TD class = title>
             渠道类型
          </TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1],null,msqlBranch,1);" verify="渠道类型|notnull&code:BranchType2" 
             ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
          </TD>
        
		 </tr>
      <tr  class= common> 
      <td  class= title> 
		  专员编码 
		</td>
        <td  class= input> 
		  <input class= common name=AgentCode > 
		</td>   
        <td  class= title> 
		  专员姓名 
		</td>
        <td  class= input> 
		  <input class=common  name=AgentName > 
		</td>
     
       
       </tr>
      <tr  class= common> 
       <td  class= title> 
		  离职原因 
		</td>
        <td  class= input> 
		  <input name=DepartRsn class='codeno' 
		   ondblclick="return showCodeList('DepartRsn',[this,DepartRsnName],[0,1]);" 
		   onkeyup="return showCodeListKey('DepartRsn',[this,DepartRsnName],[0,1]);" 
		  ><Input name=DepartRsnName class="codename"> 
		</td>
        <td  class= title> 
		  离职日期 
		</td>
        <td  class= input> 
		  <Input class='coolDatePicker' name=DepartDate dateFormat='short' >
		</td>
     
 
      </tr>
     <tr>
            <td  class= title> 
		  离职申请日期
		</td>
        <td  class= input> 
		  <Input class='coolDatePicker' name=ApplyDate dateFormat='short'>
		</td>
     </tr>
    </table> 
          <input type=hidden name=BranchType value=<%=BranchType%>>
          <input type=hidden name=tManageCom value= '<%=tG.ManageCom%>'>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton"> 
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton"> 						
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divDimissionGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanDimissionGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton"> 				
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
