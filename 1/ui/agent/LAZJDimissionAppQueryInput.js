 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //initPolGrid();
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

function initEdorType(cObj)
{
	mEdorType = " 1 and codealias=#2# ";
	showCodeList('agentkind',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{
	mEdorType = " 1 and codealias=#2#";
	showCodeListKey('agentkind',[cObj], null, null, mEdorType, "1");
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
  var arrReturn = new Array();

   var tSel = AgentGridB.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{

			try
			{
				arrReturn = getQueryResult();	
			  top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				top.close();
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
    arrSelected = new Array();

	var strSQL = "";


	  tRow = AgentGridB.getSelNo();
	  var AgentCode = AgentGridB.getRowColData(tRow-1,1);

	  if( tRow == 0 || tRow == null || arrDataSet == null )
	  return arrSelected; 
	 strSQL = "select getUniteCode(a.AgentCode),a.Name,a.Sex,a.Birthday,a.NativePlace,a.Nationality,a.RgtAddress,a.HomeAddress,a.ZipCode,a.Phone,a.BP,a.Mobile,a.EMail,a.IDNo,a.PolityVisage,a.Degree,a.GraduateSchool,a.Speciality,a.PostTitle,a.OldCom,a.OldOccupation,a.HeadShip,a.EmployDate,a.AgentState,a.Remark,b.AgentKind,a.Operator,a.ManageCom,c.branchmanager,b.AgentGrade,c.BranchManager,c.AgentGroup,c.BranchLevel,a.InsideFlag,b.agentseries,c.branchattr,b.TutorShip,b.InDueFormFlag,a.FullTimeFlag,b.isConnMan,b.SpeciFlag,b.agentgrade1,b.agentseries1,a.quafno,a.quafstartdate,a.salequaf "
	        +" from LAAgent a,LATree b,LABranchGroup c where 1=1 "
	        +"and a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup "
	        +" and a.groupAgentCode='"+AgentCode+"' and (c.state<>'1' or c.state is null) and a.agentstate<'03' ";

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("无此业务员或此业务员已经离职登记！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);

	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
	if (verifyInput() == false)
  return false;
	var tReturn = getManageComLimitlike("a.managecom");
	// 初始化表格
	initAgentGridB();
	
		// 书写SQL语句
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	} 

		var strSQL = "";
			strSQL = "select getunitecode(a.agentcode),a.name,c.branchattr,d.agentgrade,a.idno,case a.agentstate when '01' then '在职' when '03' then '离职登记' else '离职确认' end"
		     +"  from LAAgent a,LABranchGroup c,latree d where 1=1 "
		         + "and a.agentgroup = c.AgentGroup  and (c.state<>'1' or c.state is null) and a.agentcode=d.agentcode  "
		         + tReturn
		        // + getWherePart('a.AgentCode','AgentCode','like')
		         +strAgent
		         + getWherePart('a.BranchType','BranchType')
		         + getWherePart('c.BranchAttr','AgentGroup','like')
		         + getWherePart('a.ManageCom','ManageCom','like')
		         + getWherePart('a.Name','Name','like')
		         + getWherePart('a.Sex','Sex')
		         + getWherePart('a.IDNo','IDNo')
		         + getWherePart('a.EmployDate','EmployDate')
		         + getWherePart('a.BranchType2','BranchType2');		        
		         //与前面查询条件保持一致
		       if(fm.AgentState.value=='01')
	        {
		        strSQL += " and a.agentstate<='02'  ";
	        }
	        if(fm.AgentState.value=='02')
		      {
			        strSQL += " and a.agentstate = '03' ";
		      }
		      if(fm.AgentState.value=='03')
		      {
			        strSQL += " and a.agentstate > '03' ";
		      }

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
   turnPage.pageDisplayGrid = AgentGridB;
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果

  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}