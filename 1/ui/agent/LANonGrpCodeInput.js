//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAAgentQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	if(!chkMulLine()){
		return false;
	}
	fm.all('fmAction').value='SELECTPAY';
	submitForm();
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
function DoReset()
{
	initInpBox();
	initAgentGrid();
}

function chkMulLine(){
	//alert("enter chkmulline");
	var i;
	var iCount = 0;
	var rowNum = AgentGrid.mulLineCount;
	for(i=0;i<rowNum;i++){
		if(AgentGrid.getChkNo(i)){
			iCount++;
		} 
	}
	if(iCount == 0){
		alert("请选择要删除的数据!");
		return false
	}
	return true;
}

function DownClick()
{
	if(!verifyInput())
	  {
	  	return false;
	  }
		var tReturn = getManageComLimitlike("a.managecom");
		// 书写SQL语句
		var strSQL = "";
		strSQL = "select b.BranchAttr,a.managecom,a.name,c.agentgrade,case when a.idnotype='0' then '身份证' when a.idnotype='1' then '护照' when a.idnotype='2' then '军官证' when a.idnotype='3' then '工作证' when a.idnotype='4' then '其它' when a.idnotype='5' then '户口本' "
			     +" when a.idnotype='6' then '出生证' when a.idnotype='7' then '回乡证/台胞' else  '外国人永久居留身份证' end,a.idno,case when a.sex='0' then '男' else '女' end,a.birthday,"
		         + "case when a.agentstate='01' then '在职'  when a.agentstate='02' then '二次增员' "
		         + " when a.agentstate='03' then '离职登记' when a.agentstate='04' then '二次离职登记' "
		         + " when a.agentstate='06' then '离职确认' when a.agentstate='07' then '二次离职确认' end,"
		         + " case c.Speciflag when '01' then '是' else '否' end,a.agentcode "
		         + " from LAAgent a,LABranchGroup b,LATree c where 1=1 "
		         + " and a.Agentgroup = b.agentGroup and c.agentcode=a.agentcode and (b.state<>'1' or b.state is null) and (a.groupagentcode is null or a.groupagentcode ='') ";
		         if(fm.all("AgentGroup").value!=null && fm.all("AgentGroup").value!='')
	     	        {
		                  strSQL+= " and b.branchattr like '"+fm.all('AgentGroup').value+"%'";
		            }
		         strSQL+=" and 1= 1 "
		         + tReturn
		         + getWherePart('a.ManageCom','ManageCom','like')
		         + getWherePart('a.Name','Name')
		         + getWherePart('a.Sex','Sex')
		         + getWherePart('a.Birthday','Birthday')
		         + getWherePart('a.idnotype','IDNoType')
		         + getWherePart('a.IDNo','IDNo')
		         + getWherePart('a.EmployDate','EmployDate')
		         + getWherePart('a.BranchType','BranchType')
		         + getWherePart('a.BranchType2','BranchType2')
		         
		         fm.querysql.value =strSQL;
		         fm.action ="LANonGrpCodeDownLoad.jsp"
		         fm.submit();
		         fm.action=oldAction;
	
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
if(!verifyInput())
  {
  	return false;
  }
	initAgentGrid();
	var tReturn = getManageComLimitlike("a.managecom");
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.groupAgentCode,b.BranchAttr,a.managecom,a.name,c.agentgrade,case when a.idnotype='0' then '身份证' when a.idnotype='1' then '护照' when a.idnotype='2' then '军官证' when a.idnotype='3' then '工作证' when a.idnotype='4' then '其它' when a.idnotype='5' then '户口本' "
		     +" when a.idnotype='6' then '出生证' when a.idnotype='7' then '回乡证/台胞' else  '外国人永久居留身份证' end,a.idno,case when a.sex='0' then '男' else '女' end,a.birthday,a.agentstate,"
	         + "case when a.agentstate='01' then '在职'  when a.agentstate='02' then '二次增员' "
	         + " when a.agentstate='03' then '离职登记' when a.agentstate='04' then '二次离职登记' "
	         + " when a.agentstate='06' then '离职确认' when a.agentstate='07' then '二次离职确认' end,"
	         + " case c.Speciflag when '01' then '是' else '否' end,a.agentcode "
	         + " from LAAgent a,LABranchGroup b,LATree c where 1=1 "
	         + " and a.Agentgroup = b.agentGroup and c.agentcode=a.agentcode and (b.state<>'1' or b.state is null) and (a.groupagentcode is null or a.groupagentcode ='') ";
	         if(fm.all("AgentGroup").value!=null && fm.all("AgentGroup").value!='')
     	        {
	                  strSQL+= " and b.branchattr like '"+fm.all('AgentGroup').value+"%'";
	            }
	         strSQL+=" and 1= 1 "
	         + tReturn
	         + getWherePart('a.ManageCom','ManageCom','like')
	         + getWherePart('a.Name','Name')
	         + getWherePart('a.Sex','Sex')
	         + getWherePart('a.Birthday','Birthday')
	         + getWherePart('a.idnotype','IDNoType')
	         + getWherePart('a.IDNo','IDNo')
	         + getWherePart('a.EmployDate','EmployDate')
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2')
	       
	    
     	turnPage.queryModal(strSQL, AgentGrid); 
}


