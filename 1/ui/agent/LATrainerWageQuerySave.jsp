<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LATrainerWageQuerySave.jsp
//程序功能： 
//创建日期：2018-5-29
//创建人  ：wangQingMin
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
 boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String downLoadFileName = "组训薪资查询报表 _"+tG.Operator+"_.xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    String querySql = request.getParameter("querySql");
    System.out.println(querySql);
    querySql = querySql.replaceAll("%25","%");
    		 //设置表头
		    String[][] tTitle = {{"组训人员工号","组训人员姓名","组训人员职级","管理机构代码","管理机构名称","营业部代码","营业部名称","基本工资","岗位津贴","月绩效工资","奖励加款","奖励扣款","其他1","其他2","本月应发工资"}};
		    //表头的显示属性
		    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		    
		    //数据的显示属性
		    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		    //生成文件
		    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
		    createexcellist.createExcelFile();
		    String[] sheetName ={"组训薪资报表"};
		    createexcellist.addSheet(sheetName);
		    int row = createexcellist.setData(tTitle,displayTitle);
		    if(row ==-1) errorFlag = true;
		        createexcellist.setRowColOffset(row,0);//设置偏移
		        System.out.println(querySql);
		    if(createexcellist.setData(querySql,displayData)==-1)
		        errorFlag = true;
		    if(!errorFlag)
		        //写文件到磁盘
		        try{
		            createexcellist.write(tOutXmlPath);
		        }catch(Exception e)
		        {
		            errorFlag = true;
		            System.out.println(e);
		        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>