<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：离职回退功能
//程序功能：
//创建日期：2008-06-05 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<head >
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LADimissionBackInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LADimissionBackInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
 <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LADimissionBackSave.jsp" method=post name=fm target="fraSubmit">

    <table class="common" align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="回  退"  TYPE=button onclick="return submitForm();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="重  置"  TYPE=button onclick="return resetForm();">
			</td>
			
		</tr>
	</table>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLADimission1);">
    
    <td class=titleImg>
      员工离职信息
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLADimission1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  <%=tTitleAgent%>代码  
		</td>
        <td  class= input> 
		  <input class= common name=AgentCode onchange="return checkValid();" elementtype=nacessary> 
		</td>
    <td  class= title> 
		  <%=tTitleAgent%>姓名 
		</td>
    <td  class= input> 
		  <input class="readonly" readonly name=AgentName > 
		</td>
    </tr>
    
    <tr  class= common> 
    <td  class= title> 
		  离职原因 
		</td>
    <td  class= input> 
		 <Input  class="readonly"  name=DepartRsnName readOnly >  
		</td>
         
    <td  class= title> 
		  申请日期 
		</td>
    <TD  class= input>
        <Input class="readonly" readOnly  name=AppDate  verify="申请日期|notnull&date">
    </TD>   
    
   </tr>
    <tr  class= common> 
    <td  class= title>
		  备注
		</td>
        <td  class= input>
		  <input name=Noti  readOnly  class="readonly" >
		</td>
      <td  class= title> 
		  离职日期 
		</td>
    <TD  class= input>
        <Input class="readonly" readOnly  name=OutWorkDate  >
    </TD>      
      </tr>  
     
    </table>
    
  </Div>
    <input type=hidden name=DepartRsn value=''>
    <input type=hidden name=Operator value=''>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=DepartTimes value=''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
