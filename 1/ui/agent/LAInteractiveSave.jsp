<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
 
  
  LAInteractiveUI tLAInteractiveUI = new LAInteractiveUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");//接受传入的操作符
      LAAgentSet tSetU = new LAAgentSet();	//用于更新
	  // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      tVData.add(tGI);
      //创建数据集
      if(tAction.equals("DoSave")||tAction.equals("DoDelete")){
      	String tChk[] = request.getParameterValues("InpAgentGridChk"); 
      	String tManageCom[] = request.getParameterValues("AgentGrid1");
      	String tChannel[] = request.getParameterValues("AgentGrid3");
      	String tAgentCode[]= request.getParameterValues("AgentGrid5");
      	String tInteractiveFlag[] = request.getParameterValues("AgentGrid9");
      	for(int i=0;i<tChk.length;i++){
      		if(tChk[i].equals("1")){
          		//创建一个新的Schema
      	  		LAAgentSchema tSch = new LAAgentSchema();
      	  	    tSch.setManageCom(tManageCom[i].trim());
      	  	    tSch.setAgentCode(tAgentCode[i].trim());
      	  	    tSch.setTogaeFlag(tInteractiveFlag[i]);
      	  		tSetU.add(tSch);
       		}
      	}
      	tVData.add(tSetU);
      	System.out.println("tSetU.size()"+tSetU.size());
        System.out.println("Start tLAInteractiveUI Submit...SELECTPAY");
        tLAInteractiveUI.submitData(tVData,tAction);
      }else{
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAInteractiveUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
