<%
//程序名称：LAAgentBlackListSave.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>   
<%  //接收信息，并作校验处理。
  //输入参数
  LAAgentBlacklistSchema tLAAgentBlackListSchema = new LAAgentBlacklistSchema();
  LAAgentBlacklistUI tLAAgentBlacklistUI = new LAAgentBlacklistUI();
  String mmResult = "";
  
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");              //获得用户信息
  CErrors tError = null;                                 //错误类的获得
  String tOperate=request.getParameter("fmtransact");   //获得操作符号
  tOperate=tOperate.trim();
                                                         
  VData tVData = new VData();                            // 准备传输数据 VData
  String FlagStr="";
  String Content="";
  							//取得加入黑名单代理人信息
    tLAAgentBlackListSchema.setName(request.getParameter("Name"));
    tLAAgentBlackListSchema.setBlackListCode(request.getParameter("BlackListCode"));
    tLAAgentBlackListSchema.setSex(request.getParameter("Sex"));
    tLAAgentBlackListSchema.setBirthday(request.getParameter("Birthday"));
    tLAAgentBlackListSchema.setIDNo(request.getParameter("IDNo"));
    tLAAgentBlackListSchema.setPostalAddress(request.getParameter("PostalAddress"));
    tLAAgentBlackListSchema.setZipCode(request.getParameter("ZipCode"));
    tLAAgentBlackListSchema.setSource(request.getParameter("Source"));
    tLAAgentBlackListSchema.setWorkAge(request.getParameter("WorkAge"));    
    tLAAgentBlackListSchema.setInsurerCompany(request.getParameter("InsureCom"));
    tLAAgentBlackListSchema.setHeadShip(request.getParameter("HeadShip"));
    tLAAgentBlackListSchema.setBusiness(request.getParameter("Business"));    
    tLAAgentBlackListSchema.setBlacklistReason(request.getParameter("Reason"));
    tLAAgentBlackListSchema.setPhone(request.getParameter("Phone")); 

    tVData.add(tG);					//准备向后方传输的数据	
    tVData.addElement(tLAAgentBlackListSchema);
    
    try{
    	tLAAgentBlacklistUI.submitData(tVData,tOperate); 
    }catch(Exception ex)
  	{
    		Content = "保存失败，原因是:" + ex.toString();
    		System.out.println(Content);
    		FlagStr = "Fail";
  	}
    if (!FlagStr.equals("Fail"))
  	{
   	   tError = tLAAgentBlacklistUI.mErrors;
    	   if (!tError.needDealError())
    		{if (tOperate.equals("INSERT||MAIN"))
                       { mmResult = tLAAgentBlacklistUI.getResult();
                        }
                  if (tOperate.equals("UPDATE||MAIN"))
                       { mmResult = tLAAgentBlackListSchema.getBlackListCode();
                        }      
    			Content = "保存成功!";
    			FlagStr = "Succ";
    		}
   	  else
    		{
    			Content = " 保存失败，原因是:" + tError.getFirstError();
    			FlagStr = "Fail";
    		}
  	}
  	
%>
<html>
<script language="javascript">
        parent.fraInterface.fm.all('BlackListCode').value = '<%=mmResult%>';
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>  	