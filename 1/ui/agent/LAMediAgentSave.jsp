<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
  LATreeSchema tLATreeSchema = new LATreeSchema();
  LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
  LARearRelationSet tLARearRelationSet = new LARearRelationSet();
  //ALABankAgentUI tLABankAgent   = new ALABankAgentUI();
  LAQualificationSchema tLAQualificationSchema   = new LAQualificationSchema();
  LAMediAgentUI tLABankAgent = new LAMediAgentUI();
  String AgentCode = "";
  String GroupAgentCode = "";

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  System.out.println("操作符：[ " + tOperate + " ]");
  String tIsManager = request.getParameter("hideIsManager");
  System.out.println(request.getParameter("ManageCom"));
  System.out.println(tIsManager+"*********");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    LisIDEA tLisIdea = new LisIDEA();
    tLAAgentSchema.setPassword(tLisIdea.encryptString("1111"));//设置销售人员默认密码，方便以后查询分析系统使用
    tLAAgentSchema.setAgentCode(request.getParameter("AgentCode"));
    tLAAgentSchema.setGroupAgentCode(request.getParameter("GroupAgentCode"));
    tLAAgentSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
    tLAAgentSchema.setBranchType(request.getParameter("BranchType"));
    tLAAgentSchema.setBranchType2(request.getParameter("BranchType2"));
    tLAAgentSchema.setName(request.getParameter("Name"));
    tLAAgentSchema.setSex(request.getParameter("Sex"));
    tLAAgentSchema.setBirthday(request.getParameter("Birthday"));
    tLAAgentSchema.setIDNo(request.getParameter("IDNo"));
    tLAAgentSchema.setIDNoType("0");
    if(tOperate.equals("UPDATE||MAIN"))
    {//by gzh 20110407 当修改操作时，若销售人员未提交到集团，此时状态不变
    	String sql = "select crs_check_status from laagent where agentcode = '"+request.getParameter("AgentCode")+"'";
	    String crs_check_status = new ExeSQL().getOneValue(sql);
	    if("01".equals(crs_check_status)){
	    	tLAAgentSchema.setCrs_Check_Status("99"); //更新标志
	    }else{
	    	tLAAgentSchema.setCrs_Check_Status("00"); //更新标志
	    } 
    }
    if(tOperate.equals("INSERT||MAIN"))
    {
    	tLAAgentSchema.setCrs_Check_Status("00");	
    }
    
    tLAAgentSchema.setNativePlace(request.getParameter("NativePlace"));
    tLAAgentSchema.setNationality(request.getParameter("Nationality"));
    tLAAgentSchema.setPolityVisage(request.getParameter("PolityVisage"));
    tLAAgentSchema.setRgtAddress(request.getParameter("RgtAddress"));
    tLAAgentSchema.setDegree(request.getParameter("Degree"));
    tLAAgentSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
    tLAAgentSchema.setSpeciality(request.getParameter("Speciality"));
    tLAAgentSchema.setPostTitle(request.getParameter("PostTitle"));
    tLAAgentSchema.setHomeAddress(request.getParameter("HomeAddress"));
    tLAAgentSchema.setZipCode(request.getParameter("ZipCode"));
    tLAAgentSchema.setPhone(request.getParameter("Phone"));
    tLAAgentSchema.setBP(request.getParameter("BP"));
    tLAAgentSchema.setMobile(request.getParameter("Mobile"));
    tLAAgentSchema.setEMail(request.getParameter("EMail"));
    tLAAgentSchema.setOldCom(request.getParameter("OldCom"));
    tLAAgentSchema.setOldOccupation(request.getParameter("OldOccupation"));
    tLAAgentSchema.setHeadShip(request.getParameter("HeadShip"));
    //tLAAgentSchema.setQuafNo(request.getParameter("QuafNo"));
   // tLAAgentSchema.setQuafEndDate(request.getParameter("QuafEndDate"));
    tLAAgentSchema.setDevGrade(request.getParameter("DevGrade"));
    tLAAgentSchema.setTrainDate(request.getParameter("TrainDate"));
    tLAAgentSchema.setTrainPeriods(request.getParameter("TrainPeriods"));
    tLAAgentSchema.setEmployDate(request.getParameter("EmployDate"));
    tLAAgentSchema.setAgentState(request.getParameter("AgentState"));
   // tLAAgentSchema.setBankCode(request.getParameter("BankCode"));
    //tLAAgentSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLAAgentSchema.setRemark(request.getParameter("Remark"));
    //tLAAgentSchema.setAgentKind(request.getParameter("AgentKind"));
    tLAAgentSchema.setOperator(request.getParameter("Operator"));
    tLAAgentSchema.setChannelName(request.getParameter("ChannelName"));
    tLAAgentSchema.setInsideFlag(request.getParameter("AgentInsideFlag"));
    //add by zhuxt 20140903
    tLAAgentSchema.setAgentType(request.getParameter("AgentType"));
    tLAAgentSchema.setRetainContNo(request.getParameter("RetainContno"));
    //取得行政信息--在bl中设置职级及系列
    tLATreeSchema.setAgentCode(request.getParameter("AgentCode"));
    tLATreeSchema.setManageCom(request.getParameter("ManageCom"));
    tLATreeSchema.setAgentGroup(request.getParameter("hideAgentGroup"));
    //tLATreeSchema.setUpAgent(request.getParameter("UpAgent"));
    tLATreeSchema.setAgentGrade(request.getParameter("AgentGrade"));
    tLATreeSchema.setAgentLine("A");
    String tAgentGrade = request.getParameter("AgentGrade");
    //tLATreeSchema.setAgentSeries(request.getParameter("AgentKind"));
    if(tAgentGrade.substring(0,1).equals("H")){
    	   tLATreeSchema.setAgentKind("D");//员工
    }
    if(tAgentGrade.substring(0,1).equals("I")){
    	 tLATreeSchema.setAgentKind("E");//经理
    }
    tLATreeSchema.setBranchType(request.getParameter("BranchType"));
    tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));

    tLAQualificationSchema.setQualifNo(request.getParameter("QualifNo"));
    tLAQualificationSchema.setGrantUnit(request.getParameter("GrantUnit"));
    tLAQualificationSchema.setGrantDate(request.getParameter("GrantDate"));
    tLAQualificationSchema.setValidStart(request.getParameter("ValidStart"));
    tLAQualificationSchema.setValidEnd(request.getParameter("ValidEnd"));
    tLAQualificationSchema.setState(request.getParameter("QualifState"));
System.out.println(tLAQualificationSchema.getQualifNo()+"\\\\"+tLAQualificationSchema.getGrantUnit());

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
System.out.println("IsManager:"+tIsManager);
  tVData.add(tG);
  tVData.add(tIsManager);
  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLATreeSchema);
  tVData.addElement(tLAWarrantorSet);
  tVData.addElement(tLARearRelationSet);
  tVData.addElement(tLAQualificationSchema);
  try
  {
    System.out.println("this will save the data!!!");
    tLABankAgent.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
    ex.printStackTrace();
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABankAgent.mErrors;
    if (!tError.needDealError())
    {
      AgentCode = tLABankAgent.getAgentCode();
      	String tGroupAgentCodeSQL = "select GroupAgentCode from laagent where agentcode = '"+AgentCode+"' ";
    	GroupAgentCode = new ExeSQL().getOneValue(tGroupAgentCodeSQL);
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println(Content);
System.out.println(FlagStr);
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.fm.all('AgentCode').value = '<%=AgentCode%>';
	parent.fraInterface.fm.all('GroupAgentCode').value = '<%=GroupAgentCode%>';
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

