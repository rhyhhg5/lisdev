 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mFlag="0"; 
var mFlag1="0"; 
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
	if(showInfo!=null){
	  try{
	    showInfo.focus();
	  }
	  catch(ex){
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;
  if (beforeSubmit() == false)
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    AgentGrid.clearData("AgentGrid");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
	
}

/**
 *  页面查询操作
 */
function easyQueryClick()
{
  if(!verifyInput()) return false;
  var tAgentState = fm.all('AgentState').value;
  var tChannel = fm.all('Channel').value;
  var tChannelName = fm.all('ChannelName').value;
  var tStartDate = fm.all('StartDate').value;
  var tEndDate = fm.all('EndDate').value;
  var tSpend = "";
  switch(tChannel)
  {
     case '1':
    tSpend =" and branchtype = '1' and branchtype2 ='01'"
    break
    case '2':
    tSpend =" and branchtype = '1' and branchtype2 = '02'"
    break
    case '3':
    tSpend =" and branchtype = '2' and branchtype2 ='01'"
    break
    case '4':
    tSpend =" and branchtype = '2' and branchtype2 = '02'"
    break
    case '5':
    tSpend=" and branchtype = '3' and branchtype2 = '01'"
    break
    case '6':
    tSpend =" and branchtype = '4' and branchtype2 = '01'"
    case '7':
    tSpend =" and 1=1"
    break
    default:
    alert("业务渠道输入有误！")
  }
  var query_sql = "select substr(a.managecom,1,4),(select name from ldcom where comcode=substr(a.managecom,1,4)),a.managecom,"
               +"(select name from ldcom where comcode = a.managecom),"
               +"(select branchattr from labranchgroup where agentgroup = a.agentgroup),"
               +"(select name from labranchgroup where agentgroup = a.agentgroup),"
               +"a.agentcode,"
               +"a.name,"
               +"(case when a.branchtype = '1' and a.branchtype2 = '01' then '个险直销'" 
               +" when a.branchtype = '1' and a.branchtype2 = '02' then '个险中介' "
               +"when a.branchtype = '2' and a.branchtype2 = '01' then '团险直销'"
               +"when a.branchtype ='2'  and a.branchtype2 = '02' then '团险中介'"
               +"when a.branchtype = '3' and a.branchtype2 = '01' then '银保'"
               +"when a.branchtype = '4' and a.branchtype2 = '01' then '电销' end ),"
               +"(case when agentstate<'06' then '在职' else '离职' end),"
               +"(select nvl(sum(transmoney),0) from lacommision where agentcode = a.agentcode and tmakedate between '"+tStartDate+"' and '"+tEndDate+"')"
                +" from laagent a where 1 = 1 and managecom like '"+fm.all('ManageCom').value+"%' and a.togaeflag='Y'"
                +tSpend
                +getWherePart('AgentCode','AgentCode')
                +getWherePart('Name','Name')
        if(fm.all('AgentState').value!=null && fm.all('AgentState').value!='')
          {
               if(tAgentState=="1")
               {
                 Down_sql+=" and agentstate <'06'";
               }
               if(tAgentState=="2")
               {
                 Down_sql+=" and agentstate >='06'";
               }    
           }
      turnPage.queryModal(query_sql, AgentGrid); 
}

/**
 * 页面处理下载操作
 */
 function DoDownload()
 {
  if(!verifyInput()) return false;
  var tAgentState = fm.all('AgentState').value;
  var tChannel = fm.all('Channel').value;
  var tChannelName = fm.all('ChannelName').value;
  var tStartDate = fm.all('StartDate').value;
  var tEndDate = fm.all('EndDate').value;
  var tSpend = "";
  switch(tChannel)
  {
    case '1':
    tSpend =" and branchtype = '1' and branchtype2 ='01'"
    break
    case '2':
    tSpend =" and branchtype = '1' and branchtype2 = '02'"
    break
    case '3':
    tSpend =" and branchtype = '2' and branchtype2 ='01'"
    break
    case '4':
    tSpend =" and branchtype = '2' and branchtype2 = '02'"
    break
    case '5':
    tSpend=" and branchtype = '3' and branchtype2 = '01'"
    break
    case '6':
    tSpend =" and branchtype = '4' and branchtype2 = '01'"
    break
    case '7':
    tSpend =" and 1=1"
    break
    default:
    alert("业务渠道输入有误！")
  }
 
  var Down_sql = "select substr(a.managecom,1,4),(select name from ldcom where comcode=substr(a.managecom,1,4)),a.managecom,"
               +"(select name from ldcom where comcode = a.managecom),"
               +"(select branchattr from labranchgroup where agentgroup = a.agentgroup),"
               +"(select name from labranchgroup where agentgroup = a.agentgroup),"
               +"a.agentcode,"
               +"a.name,"
                +"(case when a.branchtype = '1' and a.branchtype2 = '01' then '个险直销'" 
               +" when a.branchtype = '1' and a.branchtype2 = '02' then '个险中介' "
               +"when a.branchtype = '2' and a.branchtype2 = '01' then '团险直销'"
               +"when a.branchtype ='2'  and a.branchtype2 = '02' then '团险中介'"
               +"when a.branchtype = '3' and a.branchtype2 = '01' then '银保'"
               +"when a.branchtype = '4' and a.branchtype2 = '01' then '电销' end ),"
               +"(case when agentstate<'06' then '在职' else '离职' end),"
               +"(select nvl(sum(transmoney),0) from lacommision where agentcode = a.agentcode and tmakedate between '"+tStartDate+"' and '"+tEndDate+"')"
                +" from laagent a where 1 = 1 and managecom like '"+fm.all('ManageCom').value+"%' and a.togaeflag='Y'"
                +tSpend
                +getWherePart('AgentCode','AgentCode')
                +getWherePart('Name','Name')
        if(fm.all('AgentState').value!=null && fm.all('AgentState').value!='')
          {
               if(tAgentState=="1")
               {
                 Down_sql+=" and agentstate <'06'";
               }
               if(tAgentState=="2")
               {
                 Down_sql+=" and agentstate >='06'";
               }    
           }
         
     fm.querySql.value = Down_sql;
     
     var DownSQLTitle = "select '分公司机构代码','分公司机构名称','支公司机构代码','支公司机构名称','团队外部编码','团队名称','业务员编码','业务员姓名','业务渠道','代理人状态','保费收入' from dual where 1=1 ";
     fm.querySqlTitle.value = DownSQLTitle;  
     
     fm.all("Title").value="select '互动部业务统计报表下载' from dual where 1=1  "; 
     
     fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
     
     fm.submit();
 }
 


function reset()
{
   initInpBox();
}