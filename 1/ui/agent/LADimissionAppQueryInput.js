//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LADimissionQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//***************************************************
//* 验证对应业务员代码的名字和销售机构
//***************************************************
function getAgentMs()
{
 if (fm.all('AgentCode').value==null || fm.all('AgentCode').value=="")
 {
 	alert("请先录入人员代码");
 	fm.all('AgentCode').value='';
 	return;
}
 var strSQL = "";
 var tAgentCode=fm.all('AgentCode').value;

   if (tAgentCode!='' )
  {
     strSQL = "select a.agentcode,a.name  from laagent a where  1=1  "
     + getWherePart('a.BranchType','BranchType')
	   + getWherePart('a.BranchType2','BranchType2')
     + getWherePart('a.groupAgentCode','AgentCode') ;
     var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
     if (!strQueryResult)
    {
      alert("不存在此业务员");
      fm.all('AgentCode').value="";
      fm.all('AgentName').value = "";
      return;
    }
  }

else
  {
     fm.all('AgentCode').value="";
     fm.all('AgentName').value = "";
     return;
  }

  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentName').value  =tArr[0][1];
}



//提交，打印按钮对应操作
function submitForm()
{
	if (fm.all('AgentCode').value==null || fm.all('AgentCode').value=="")
 {
 	alert("请先录入人员代码");
 	fm.all('AgentCode').value="";
 	return;
}
	if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "f1print";
	fm.all('Operation').value = 'PRINT';
	fm.submit();
	showInfo.close();
}

function returnParent()
{
	top.close();
}

//unction getQueryResult()
//{
//	var arrSelected = null;
//	tRow = DimissionGrid.getSelNo();
//	if( tRow == 0 || tRow == null || arrDataSet == null )
//	  return arrSelected;
//	arrSelected = new Array();
//	var strSQL = "";
//	strSQL = strSQL = "select a.AgentCode,b.name,a.DepartTimes,a.DepartRsn,a.DepartDate,a.ApplyDate,a.QualityDestFlag,a.PbcFlag,a.ReturnFlag,a.AnnuityFlag,a.VisitFlag,a.BlackFlag,a.BlackListRsn,case when (a.DepartState='03' or a.DepartState='04' or a.DepartState='05') then '离职未确认' when (a.DepartState='08' or a.DepartState='07' or a.DepartState='06') then '离职已确认' else '' end,a.DestoryFlag,a.Operator,a.ModifyDate from ladimission a,laagent b where a.agentcode=b.agentcode and a.agentcode='"+DimissionGrid.getRowColData(tRow-1,1)+"' and a.departtimes="+DimissionGrid.getRowColData(tRow-1,3);
//
//	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
//  //判断是否查询成功
//  if (!turnPage.strQueryResult) {
//    alert("没有满足条件的记录！");
//    return false;
//    }
//查询成功则拆分字符串，返回二维数组
//  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
//
//	return arrSelected;
//}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initDimissionGrid();
	//var tReturn = getManageComLimitlike("c.managecom");
	// 书写SQL语句
	if (fm.all('AgentCode').value==null || fm.all('AgentCode').value=="")
 {
 	alert("请先录入人员代码");
 	fm.all('AgentCode').value="";
 	return;
}
	var strSQL = "";
	var cAgentCode=fm.all('AgentCode').value;
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
//  add lyc 统一工号 2014-11-25
  var cSql = "select agentcode from laagent where  groupagentcode = '"+cAgentCode+"'";
  var tAgentCode = easyExecSql(cSql);
  //alert(tAgentCode);
 if (tBranchType=='1' && tBranchType2=='01'){
    strSQL="select a.prtno,a.appntname,case  when a.appntsex='0' then '男'  else '女' end,"
    +"(select b.phone from lcaddress b,lcappnt c where b.customerno=a.appntno  and b.addressno=c.addressno "
    +"  and b.customerno=appntno and c.contno=a.contno),a.prem,a.makedate,a.signdate,a.customgetpoldate,"
    +"case a.stateflag when '2' then '是' else '否' end "
    +" from  lccont  a where  a.agentcode='"+tAgentCode
    +"' and (a.appflag<>'1' or a.appflag is null ) and a.stateflag<>'3' and a.grpcontno='00000000000000000000' "
    +"and ((a.uwflag<>'a' and a.uwflag<>'8' and a.uwflag<>'1' and uwflag<>'2') or a.uwflag is null) "
    +"and not exists (select 'X' from lcrnewstatelog where newcontno = a.contno)	"
    +" union "
    +"select a.prtno,a.appntname,case  when a.appntsex='0' then '男' else '女' end,"
    +"(select b.phone from lcaddress b,lcappnt c where b.customerno=a.appntno  and b.addressno=c.addressno "
    +"  and b.customerno=appntno and c.contno=a.contno),a.prem,a.makedate,a.signdate,"
    +"case when a.customgetpoldate is null then a.customgetpoldate else a.getpoldate end, "
    +"case a.stateflag when '2' then '是' else '否' end "
    +"from  lccont  a where  a.agentcode='"+tAgentCode
    +"'  and  (a.customgetpoldate is null or a.getpoldate is null)  and a.stateflag<>'3' and a.grpcontno='00000000000000000000' "
    +" and  ((a.uwflag<>'a' and a.uwflag<>'8' and a.uwflag<>'1' and uwflag<>'2') or a.uwflag is null) "
    +"and not exists (select 'X' from lcrnewstatelog where newcontno = a.contno) "
    +" union "
    +"select a.prtno,a.grpname,'',"
    +"(select b.phone from lcaddress b,lcappnt c where b.customerno=a.appntno  and b.addressno=c.addressno "
    +"  and b.customerno=appntno and c.grpcontno=a.grpcontno),a.prem,a.makedate,a.signdate,a.customgetpoldate,"
    +"case a.stateflag when '2' then '是' else '否' end "
    +" from  lcgrpcont  a where  a.agentcode='"+tAgentCode
    +"' and (a.appflag<>'1' or a.appflag is null ) and a.stateflag<>'3' "
    +"and ((a.uwflag<>'a' and a.uwflag<>'8' and a.uwflag<>'1' and uwflag<>'2') or a.uwflag is null) "
    +"and not exists (select 'X' from lcrnewstatelog where newcontno = a.grpcontno)	"
    +" union "
    +"select a.prtno,a.grpname,'',"
    +"(select b.phone from lcaddress b,lcappnt c where b.customerno=a.appntno  and b.addressno=c.addressno "
    +" and b.customerno=appntno and c.grpcontno=a.grpcontno),a.prem,a.makedate,a.signdate,"
    +"case when a.customgetpoldate is null then a.customgetpoldate else a.getpoldate end, "
    +"case a.stateflag when '2' then '是' else '否' end "
    +"from  lcgrpcont  a where  a.agentcode='"+tAgentCode
    +"' and  (a.customgetpoldate is null or a.getpoldate is null)  and a.stateflag<>'3' "
    +" and ((a.uwflag<>'a' and a.uwflag<>'8' and a.uwflag<>'1' and uwflag<>'2') or a.uwflag is null) "
    +"and not exists (select 'X' from lcrnewstatelog where newcontno = a.grpcontno)	"
    ;
	
  }

/**	 else if (tBranchType=='2' && tBranchType2=='01')
	 	{
	 		strSQL="select a.prtno,a.grpname,a.prem,a.makedate,a.signdate,a.customgetpoldate "
    +" from  lccont  a where  a.agentcode=' "+tAgentCode
    +"' and   (a.appflag<>'1' or a.appflag is null ) "
    +"and ((a.uwflag<>'a' and a.uwflag<>'8' and a.uwflag<>'1') or a.uwflag is null) "
    +"and not exists (select 'X' from lcrnewstatelog where newcontno = a.contno)	"
    +"union "
    +"select  a.prtno,a.grpname,a.prem,a.makedate,a.signdate,a.customgetpoldate "
    +"from  lccont  a where  a.agentcode='"+tAgentCode
    +"'  and  a.customgetpoldate is null "
    +" and  a.uwflag<>'a' and a.uwflag<>'8' and uwflag<>'1'";
    }
    */
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有满足条件的记录！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  turnPage.pageDisplayGrid = DimissionGrid;
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}
