//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug = "0";
var cName = "";
var strSql = "";
try {
	var turnPage = new turnPageClass(); // 使用翻页功能，必须建立为全局变量
} catch (ex) {
}
// 提数操作
function getManagecom(Managecom, ManagecomName) {
	var pManagecom = fm.all('PManageCom').value;
	if (pManagecom == null || pManagecom == '') {
		alert("请先录入省公司代码");
		return false;
	}
	var strsql = "1 and length(trim(comcode))=8 and comcode like #"
			+ pManagecom + "%#";
	showCodeList('comcode', [ Managecom, ManagecomName ], [ 0, 1 ], null,
			strsql, 1, 1);
}
function getBranchAttr(BranchAttr, BranchAttrName) {
	var cManagecom = fm.all('CManageCom').value;

	if (cManagecom == null || cManagecom == '') {
		alert("请先录入中心支公司代码");
		return false;
	}
	var strsql1 = "1 and EndFlag=#N# and length(trim(branchattr))=10 and branchtype = #"
			+ fm.all('BranchType').value
			+ "# and branchtype2 = #"
			+ fm.all('BranchType2').value
			+ "# and managecom like #"
			+ cManagecom + "%#";
	showCodeList('BranchAttr', [ BranchAttr, BranchAttrName ], [ 0, 1 ], null,
			strsql1, 1, 1);

}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {

	showInfo.close();

	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		// parent.fraInterface.initForm();
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

// 重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
	try {
		initForm();
	} catch (re) {
		alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}

// 执行查询状态
function AgentWageStateQuery() {
	if (!verifyInput()) {
		return false;
	}

	// 校验是否该月佣金已算过
	var strSQ = "select state from latrainerWagehistory  " + "where 1=1 "
			+ getWherePart('WageNo', 'WageNo')
			+ getWherePart('ManageCom', 'PManageCom', 'like')
			+ getWherePart('ManageCom', 'CManageCom')
			+ getWherePart('BranchType', 'BranchType')
			+ getWherePart('BranchType2', 'BranchType2')
	        + " order by state desc ";
	// 判断是否查询成功
	var strQueryResult = easyQueryVer3(strSQ, 1, 1, 1);
	if (!strQueryResult) {
		alert("该月佣金提数计算还未进行!");
		return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	var tState = arr[0][0];

	if (tState == "00") {
		alert("佣金还没有计算！");
		return false;
	} else if (tState == "11") {
		alert("佣金计算正在进行中");
		return false;
	}
     return true;
}

// 执行查询
function LATrainerWageQueryQuery() {
	if (!verifyInput())
		return false;
	if(!AgentWageStateQuery())return false;
	divAgentQuery.style.display = '';
	initAgentQueryGrid();
	showRecord();
}

// 显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord() {
	// 拼SQL语句，从页面采集信息
	var tbranchAttr = fm.all('BranchAttr').value;
    var strSql= "select a.trainerCode,(select distinct(trainername) from latrainer where trainercode=a.trainerCode),(select distinct(codename) from ldcode where code=a.trainerGrade and codetype='trainergrade'),a.managecom,(select  Name from ldcom where char(length(trim(comcode))) = '8' and Sign='1' and comcode = a.managecom  ), "
			+ " a.branchattr,(select name from labranchgroup where branchattr=a.branchattr and branchtype='1' and branchtype2='01'), "
			+ " a.BasicWage,a.JobSubsidies,a.TPMoney,a.W03,a.W04,a.W05,a.W06,a.ShouldMoney "
			+ " from latrainerWage a where 1=1"
	        + getWherePart('WageNo', 'WageNo')
			+ getWherePart('ManageCom', 'PManageCom', 'like')
			+ getWherePart('ManageCom', 'CManageCom')
			+ getWherePart('BranchType', 'BranchType')
			+ getWherePart('BranchType2', 'BranchType2');
	    if(tbranchAttr!=null&&tbranchAttr!=""){
		  var  strSql1=" and '"+tbranchAttr+"' in (select branchattr from labranchgroup where agentgroup in (select agentgroup from latrainer where trainercode=a.trainercode)) "
	      strSql+=strSql1;
	  }
	// 查询SQL，返回结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSql, 1, 1, 1);

	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		// 清空MULTILINE，使用方法见MULTILINE使用说明
		AgentQueryGrid.clearData('AgentQueryGrid');
		alert("查询失败！");
		return false;
	}

	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	// 设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = AgentQueryGrid;

	// 保存SQL语句
	turnPage.strQuerySql = strSql;

	// 设置查询起始位置
	turnPage.pageIndex = 0;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,
			turnPage.pageIndex, turnPage.pageLineNum);

	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

	// 控制是否显示翻页按钮
	if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
		try {
			window.divPage.style.display = "";
		} catch (ex) {
		}
	} else {
		try {
			window.divPage.style.display = "none";
		} catch (ex) {
		}
	}
}

function AgentWageDownLoad() {
	if (!verifyInput())
		return false;

	if (AgentQueryGrid.mulLineCount == 0) {
		alert("列表中没有数据可下载");
		return false;
	} else {
		fm.querySql.value = turnPage.strQuerySql;
		fm.submit();
	}
}
