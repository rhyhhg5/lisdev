<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAAgentSecondInput.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAAgentSecondInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAAgentInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LAAgentSecondSave.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
          <td class=titleImg>
            代理人信息
          </td>
        </td>
      </tr> 
    </table>
     <Div  id= "divLAAgent1" style= "display: ''">      
    <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          代理人编码 
        </TD>
        <TD  class= input> 
          <Input class= common name=AgentCode verify="代理人编码|notnull" onchange="return agentConfirm();">
        </TD>        
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common verify="姓名|NotNull&len<=20">
        </TD>
        <TD  class= title>
          性别 
        </TD>
        <TD  class= input>
          <Input name=Sex class="code" verify="性别|code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);" >
        </TD> 
        <!--TD  class= title> 
          密码 
        </TD> 
        <TD  class= input>
          <Input name=Password class=common > 
        </TD--> 
      </TR> 
      <TR  class= common> 
        <!--TD  class= title> 
          推荐报名编号 
        </TD>
        <TD  class= input> 
          <Input name=EntryNo class= common > 
        </TD-->
        <TD  class= title>
          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class=common  verify="出生日期|NotNull&Date"> 
        </TD>
        <TD  class= title>
          身份证号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common verify="身份证号码|notnull&len<=20" > 
        </TD>
        <TD  class= title>
          民族
        </TD>
        <TD  class= input>
          <Input name=Nationality class="code" id="Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);" > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title> 
          籍贯
        </TD>
        <TD  class= input>
          <Input name=NativePlace class="code" verify="籍贯|code:NativePlaceBak" id="NativePlaceBak" ondblclick="return showCodeList('NativePlaceBak',[this]);" onkeyup="return showCodeListKey('NativePlaceBak',[this]);">
        </TD>   
        <TD  class= title>
          政治面貌 
        </TD>
        <TD  class= input> 
          <Input name=PolityVisage class="code" verify="政治面貌|code:polityvisage" id="polityvisage" ondblclick="return showCodeList('polityvisage',[this]);" onkeyup="return showCodeListKey('polityvisage',[this]);" > 
        </TD> 
        <TD  class= title>
          户口所在地
        </TD>
        <TD  class= input> 
          <Input name=RgtAddress class="code" ondblclick="return showCodeList('NativePlaceBak',[this]);" onkeyup="return showCodeListKey('NativePlaceBak',[this]);"> 
        </TD>
      </TR>
      <!--TR  class= common> 
        <TD  class= title>
          来源地 
        </TD>
        <TD  class= input> 
          <Input name=Source class= common > 
        </TD>        
        <TD  class= title> 
          血型
        </TD>
        <TD  class= input> 
          <Input name=BloodType class="code" verify="血型|code:BloodType" id="BloodType" ondblclick="return showCodeList('BloodType',[this]);" onkeyup="return showCodeListKey('BloodType',[this]);"> 
        </TD>        
        <TD  class= title>
          婚姻状况 
        </TD>
        <TD  class= input>
          <Input name=Marriage class="code" verify="婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);" >
        </TD>   
      </TR>
      <tr class=common>           
        <TD  class= title> 
          结婚日期
        </TD>
        <TD  class= input> 
          <Input name=MarriageDate class="coolDatePicker" dateFormat="short" > 
        </TD>     
      </tr-->
      <TR  class= common> 
        <TD  class= title>
          学历 
        </TD>
        <TD  class= input> 
          <Input name=Degree class="code" verify="学历|code:Degree" id="Degree" ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);"> 
        </TD>
       <TD  class= title> 
          毕业院校
        </TD>
        <TD  class= input> 
          <Input name=GraduateSchool class= common > 
        </TD>
        <TD  class= title>
          专业 
        </TD>
        <TD  class= input> 
          <Input name=Speciality class= common > 
        </TD>
      </TR>
      <TR  class= common>
        <!--TD  class= title> 
          外语水平 
        </TD>
        <TD  class= input> 
          <Input name=ForeignLevel class="code" verify="外语水平|code:EngLevel" id="EngLevel" ondblclick="return showCodeList('EngLevel',[this]);" onkeyup="return showCodeListKey('EngLevel',[this]);"> 
        </TD-->
        <TD  class= title>
          职称 
        </TD>
        <TD  class= input> 
          <Input name=PostTitle class='code' verify="职称|code:posttitle" ondblclick="return showCodeList('posttitle',[this]);" onkeyup="return showCodeListKey('posttitle',[this]);" > 
        </TD>
        <!--TD  class= title>
          家庭地址编码
        </TD>
        <TD  class= input>
          <Input name=HomeAddressCode class= common >
        </TD-->
        <TD  class= title>
          家庭地址 
        </TD>
        <TD  class= input> 
          <Input name=HomeAddress class= common > 
        </TD>
        <!--TD  class= title> 
          通讯地址 
        </TD>
        <TD  class= input> 
          <Input name=PostalAddress class= common > 
        </TD-->
        <TD  class= title>
          邮政编码 
        </TD>
        <TD  class= input> 
          <Input name=ZipCode class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          电话 
        </TD>
        <TD  class= input> 
          <Input name=Phone class= common > 
        </TD>
        <TD  class= title> 
          传呼
        </TD>
        <TD  class= input>
          <Input name=BP class= common > 
        </TD>
        <TD  class= title>
          手机 
        </TD>
        <TD  class= input> 
          <Input name=Mobile class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          E_mail 
        </TD>
        <TD  class= input> 
          <Input name=EMail class= common > 
        </TD>
        <!--TD  class= title>
          是否吸烟标志 
        </TD>
        <TD  class= input> 
          <Input name=SmokeFlag class='code' verify="是否吸烟标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);"> 
        </TD-->
        <TD  class= title>
          银行编码
        </TD>
        <TD  class= input> 
          <Input name=BankCode class=common > 
        </TD>
        <TD  class= title>
          工资存折 
        </TD>
        <TD  class= input> 
          <Input name=BankAccNo class= common > 
        </TD>
      </TR>
      <TR  class= common>
        <!--TD  class= title>
          从业年限 
        </TD>
        <TD  class= input> 
          <Input name=WorkAge class= common > 
        </TD-->
        <TD  class= title>
          原工作单位 
        </TD>
        <TD  class= input> 
          <Input name=OldCom class= common > 
        </TD>
        <TD  class= title> 
          原职业 
        </TD>
        <TD  class= input>
          <Input name=OldOccupation class='code' verify="原职业|code:occupation" ondblclick="return showCodeList('occupation',[this]);" onkeyup="return showCodeListKey('occupation',[this]);"> 
        </TD>
        <TD  class= title>
          工作职务 
        </TD>
        <TD  class= input> 
          <Input name=HeadShip class= common  > 
        </TD>
      </TR>
      <!--TR  class= common> 
        <TD  class= title>
          推荐代理人 
        </TD>
        <TD  class= input> 
          <Input name=RecommendAgent class= common  > 
        </TD>
        <TD  class= title> 
          工种/行业
        </TD>
        <TD  class= input> 
          <Input name=Business class=common > 
        </TD>
      </TR-->
      <TR  class= common>         
        <!--TD  class= title>
          信用等级 
        </TD>
        <TD  class= input> 
          <Input name=CreditGrade class=common > 
        </TD>
        <TD  class= title>
          销售资格 
        </TD>
        <TD  class= input> 
          <Input name=SaleQuaf class='code' verify="销售资格|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD-->
        <TD  class= title>
          代理人资格证号码 
        </TD>
        <TD  class= input> 
          <Input name=QuafNo class= common > 
        </TD>
        <TD  class= title>
          证书结束日期 
        </TD>
        <TD  class= input> 
          <Input name=QuafEndDate class="coolDatePicker" dateFormat="short" > 
        </TD>
        <TD  class= title>
          展业证号码1 
        </TD>
        <TD  class= input> 
          <Input name=DevNo1 class= common > 
        </TD>
      </TR>
      <!--TR  class= common>         
        <TD  class= title> 
          证书开始日期 
        </TD>
        <TD  class= input> 
          <Input name=QuafStartDate class="coolDatePicker" dateFormat="short" > 
        </TD>
      </TR>
      <TR  class= common>        
        <TD  class= title> 
          展业证号码2
        </TD>
        <TD  class= input> 
          <Input name=DevNo2 class= common > 
        </TD>
        <TD  class= title>
          聘用合同号码 
        </TD>
        <TD  class= input> 
          <Input name=RetainContNo class= common > 
        </TD> 
        <TD  class= title>
          代理人类别 
        </TD>
        <TD  class= input> 
          <Input name=AgentKind class='code' verify="代理人类别|code:AgentKind" ondblclick="return showCodeList('AgentKind',[this]);" onkeyup="return showCodeListKey('AgentKind',[this]);"  > 
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          业务拓展级别
        </TD>
        <TD  class= input> 
          <Input name=DevGrade class=common > 
        </TD>
        <TD  class= title>
          内勤标志 
        </TD>
        <TD  class= input> 
          <Input name=InsideFlag class='code' verify="内勤标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
        <TD  class= title>
          是否专职标志 
        </TD>
        <TD  class= input> 
          <Input name=FullTimeFlag class='code' verify="是否专职标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
      </TR-->
      <TR  class= common> 
        <!--TD  class= title>
          是否有待业证标志 
        </TD>
        <TD  class= input> 
          <Input name=NoWorkFlag class='code' verify="是否有待业证标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD-->
        <TD  class= title>
          培训期数 
        </TD>
        <TD  class= input> 
          <Input name=TrainPeriods class=common verify="培训期数|INT" > 
        </TD>
        <TD  class= title>
          入司时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class='readonly'readonly > 
        </TD>
        <TD  class= title>
          保证金 
        </TD>
        <TD  class= input> 
          <Input name=AssuMoney class= common verify="保证金|notnull&value>0"> 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          备注
        </TD>
        <TD  class=input colSpan= 3> 
          <Input name=Remark class= common3 > 
        </TD>
        <!--TD  class= title>
          转正日期 
        </TD>
        <TD  class= input> 
          <Input name=InDueFormDate class='coolDatePicker' dateFormat='short' > 
        </TD-->
        <!--TD  class= title>
          代理人状态 
        </TD>
        <TD  class= input> 
          <Input name=AgentState class='code' verify="代理人状态|code:agentstate" ondblclick="return showCodeList('agentstate',[this]);" onkeyup="return showCodeListKey('agentstate',[this]);" > 
        </TD-->
        <TD  class= title>
          操作员代码 
        </TD>
        <TD  class= input> 
          <Input name=Operator class= 'readonly' readonly > 
        </TD>
      </TR>
      <!--TR  class= common> 
        <TD  class= title>
          复核员
        </TD>
        <TD  class= input> 
          <Input name=Approver class= common > 
        </TD>
        <TD  class= title>
          复核日期
        </TD>
        <TD  class= input> 
          <Input name=ApproveDate class= common > 
        </TD>
      </TR>
      <TR class= common> 
        <TD  class= title>
          标志位
        </TD>
        <TD  class= input> 
          <Input name=QualiPassFlag class= common MAXLENGTH=1> 
        </TD>
      </TR-->
    </table>
       <!--代理人状态--> <Input name=AgentState type=hidden value ="02" > 
       <!--代理人状态--> <Input name=initAgentState type=hidden value ='02' > 
    </Div>	
    <!--行政信息-->    
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
            <td class= titleImg>
                行政信息
            </td>
            </td>
    	</tr>
     </table>
     <Div id= "divLAAgent3" style= "display: ''">
       <table class=common>
        <tr class=common>     
        <TD class= title>
          代理人职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="code"  verify="代理人职级|notnull&code:AgentGrade" ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" > 
        </TD>
        <TD class= title>
          管理机构
        </TD>
        <TD class= input>
          <!--Input name=ManageCom class='code' verify="管理机构|notnull" ondblclick="return showCodeList('station',[this]);" --> 
          <Input name=ManageCom class='readonly'readonly >
        </TD>
        <TD class= title>
          推荐人 
        </TD>
        <TD class= input>
          <Input class=common name=IntroAgency onchange="return changeIntroAgency();"> 
        </TD>
        
        </tr>
        <tr class=common>
        <TD class= title>
          销售机构 
        </TD>
        <TD class= input>
          <Input class=common name=BranchCode verify="销售机构|notnull" onchange="changeGroup();"> 
        </TD>
        <TD class= title>
          组经理
        </TD>
        <TD class= input>
          <Input name=GroupManagerName class='readonly'readonly > 
        </TD>
        <TD class= title>
          部经理
        </TD>
        <TD class= input>
          <Input name=DepManagerName class='readonly'readonly > 
        </TD>
        </tr>
        <tr class=common>
        <TD class= title>
          组育成人
        </TD>
        <TD class= input>
          <Input name=RearAgent class=common verify="组育成人|len<=10"> 
        </TD>
        <TD class= title>
          部育成人
        </TD>
        <TD class= input>
          <Input name=RearDepartAgent class=common verify="部育成人|len<=10"> 
        </TD>
        <TD class= title>
          督导长育成人 
        </TD>
        <TD class= input>
          <Input class=common name=RearSuperintAgent verify="督导长育成人|len<=10"> 
        </TD>
        </tr>
        <tr class=common>
        <TD class= title>
          区督导长育成人 
        </TD>
        <TD class= input>
          <Input class=common name=RearAreaSuperintAgent verify="区域督导长育成人|len<=10"> 
        </TD>
        </tr>
       </table>   
         <!--代理人系列--> <Input name=AgentSeries type=hidden> 
    </Div>
    <!--担保人信息（列表） -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
            <td class= titleImg>
                担保人信息
            </td>
            </td>
    	</tr>
     </table>
    <Div  id= "divLAAgent2" style= "display: ''">
    <table  class= common>
            <tr  class= common>
                    
        <td text-align: left colSpan=1> 
		  <span id="spanWarrantorGrid" >
		  </span> 
        </td>
                    </tr>
            </table>
    </div>
    <input type=hidden name=hideOperate value='INSERT||MAIN'>
    <input type=hidden name=initOperate value='INSERT'>
    <input type=hidden name=hideAgentGroup value=''>
    <!--input type=hidden name=hideManageCom value=''-->
    <input type=hidden name=BranchType value=''>    
    <input type=hidden name=hideIsManager value='false'>    
    <input type=hidden name=hideBranchCode value=''> <!--所属组的隐式代码-->
    <input type=hidden name=UpAgent value=''>
    <input type=hidden name=ManagerCode value=''>
    <input type=hidden name=upBranchAttr value=''>
    
    <table>
    <TR>
    <TD class=common >   
        <Input class=common type=button value="保存" onclick="saveForm();">
    </TD>
    </TR>
    </Table>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

        