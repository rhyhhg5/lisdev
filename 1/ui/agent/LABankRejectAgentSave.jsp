<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgenttempSchema tLAAgenttempSchema   = new LAAgenttempSchema();
  LABankRejectAgentUI tLABankRejectAgentUI = new LABankRejectAgentUI();
  String AgentCode = "";
  String GroupAgentCode = "";

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  System.out.println("操作符：[ " + tOperate + " ]");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String cAgentCode = "";

	GlobalInput tG = new GlobalInput();
	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);
  tG=(GlobalInput)session.getValue("GI");
  //add by lyc 统一工号 2014-11-27
//  if(""!= request.getParameter("AgentCode")){
//  String cSql = "select agentcode from laagent where groupagentcode = '"+request.getParameter("AgentCode")+"' fetch first row only";
//   cAgentCode = new ExeSQL().getOneValue(cSql);
//  }
  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    tLAAgenttempSchema.setAgentCode(request.getParameter("AgentCode"));
    tLAAgenttempSchema.setSource(request.getParameter("RejectReason"));
    tLAAgenttempSchema.setAgentKind(request.getParameter("agenttype"));
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(tLAAgenttempSchema);
  try
  {
    System.out.println("this will save the data!!!");
    tLABankRejectAgentUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
    ex.printStackTrace();
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABankRejectAgentUI.mErrors;
    if (!tError.needDealError())
    {
      AgentCode = tLABankRejectAgentUI.getAgentCode();
      String cSql1 = "select groupagentcode from laagent where agentcode = '"+AgentCode+"' fetch first row only";
	  GroupAgentCode = new ExeSQL().getOneValue(cSql1);
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println(Content);
System.out.println(FlagStr);
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.fm.all('AgentCode').value = '<%=AgentCode%>';
	parent.fraInterface.fm.all('GroupAgentCode').value = '<%=GroupAgentCode%>';
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
