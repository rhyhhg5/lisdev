<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaTrainercommisionSave.jsp
//程序功能：
//创建日期：2018-06-7
//创建人  ：   WangQingMin
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  LATrainerCommisionUI tLATrainerCommisionUI = new LATrainerCommisionUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LATrainerIndexSet tSetU = new LATrainerIndexSet();	//用于更新 LATrainerIndex
      LATrainerIndexSet tSetD = new LATrainerIndexSet();	//用于删除
      LATrainerIndexSet tSetI = new LATrainerIndexSet();    //用于插入
            
      String tChk[] = request.getParameterValues("InpSetGridChk"); 
      
      String tManageCom[] = request.getParameterValues("SetGrid1");
      String tRewardMoney[] = request.getParameterValues("SetGrid3");
      String tIdx[] = request.getParameterValues("SetGrid4");
      //创建数据集
      for(int i=0;i<tChk.length;i++)
      {
      	if(tChk[i].equals("1"))
      	{      		
      		//创建一个新的Schema LATrainerIndex
      		LATrainerIndexSchema tSch = new LATrainerIndexSchema();
			tSch.setPManageCom(tManageCom[i]);
			tSch.setTransMoney(tRewardMoney[i]);
			tSch.setWageCode("GX0001");
			tSch.setWageName("基本工资标准");
      		if((tIdx[i] == null)||(tIdx[i].equals("")))
      		{
      			//需要插入记录
      			tSetI.add(tSch);
       		}
      		else
      		{
            //需要删除
            if(tAction.trim().equals("DELETE"))      			      	
      			{
      			tSch.setIdx(tIdx[i]);      			
      			tSetD.add(tSch);
      			}
            //需要更新
            else if(tAction.trim().equals("UPDATE"))      			      	
      			{
      			tSch.setIdx(tIdx[i]);      			
      			tSetU.add(tSch);
      			}
      		 
       		}
      	}      	
      }
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      
      tVData.add(tGI);
      //没有更新或删除或插入的数据
      if ((tSetD.size() == 0)&&(tSetU.size() == 0)&&(tSetI.size() == 0))
      {
        	FlagStr = "Fail";
        	Content = FlagStr + "未选中要处理的数据！";      	
      }
    else if (tSetI.size() != 0)
    	{
        	//只有插入数据
        	tVData.add(tSetI);
        	System.out.println("Start tLATrainerCommisionUI Submit...INSERT");
        	tLATrainerCommisionUI.submitData(tVData,"INSERT");        	    	
    	}
    else if (tSetD.size() != 0)
    	{
        	//只有删除数据
        	tVData.add(tSetD);
        	System.out.println("Start tLATrainerCommisionUI Submit...DELETE");
        	tLATrainerCommisionUI.submitData(tVData,"DELETE");        	    	
    	}
    else if (tSetU.size() != 0)
    	{
        	//只有更新数据
        	tVData.add(tSetU);
        	System.out.println("Start tLATrainerCommisionUI Submit...UPDATE");
        	tLATrainerCommisionUI.submitData(tVData,"UPDATE");        	    	
    	}
    	

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLATrainerCommisionUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
