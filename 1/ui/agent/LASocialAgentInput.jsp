<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：
	//程序功能：公司业务录入
	//创建日期：
	//创建人   
	//更新记录：  更新人    更新日期     更新原因/内容
	//
%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
    var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LASocialAgentInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LASocialAgentInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body onload="initForm();initElementtype();">
<form action="./LASocialAgentSave.jsp" method=post name=fm target="fraSubmit">
<%@include file="../common/jsp/OperateAgentButton.jsp"%> 
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLAAgent1);">
		<td class=titleImg>公司业务信息</td>
	</tr>
</table>
<Div id="divLAAgent1" style="display: ''">
<table class=common>
	<TR class=common>
		<TD class=title>公司业务代码</TD>
		<TD class=input><Input class='readonly' readonly name=AgentCode>
		</TD>
		<TD class=title>公司业务姓名</TD>
		<TD class=input><Input name=Name class=common
			verify="公司业务姓名|NotNull&len<=20" elementtype=nacessary></TD>
	</TR>

	<tr class=common>

		<TD class=title>管理机构</TD>
		<TD class=input><Input class="codeno" name=ManageCom
			verify="管理机构|code:comcode&NOTNULL&len>=8"
			ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"><Input
			class=codename name=ManageComName readOnly elementtype=nacessary>
		</TD>
		<TD class=title>建立日期</TD>
		<TD class=input><Input name=EmployDate class='coolDatePicker'
			dateFormat='short' verify="建立日期|notnull&Date" elementtype=nacessary>
		</TD>
	</tr>
	<tr class=common>
		<td class=title>销售团队</td>
		<td class=input><input class='codeno' name=BranchAttr
			verify="销售团队|notnull"
			ondblclick="return getChangeComName(this,BranchAttrName);"
			onkeyup="return getChangeComName1(this,BranchAttrName);"><input
			class='codeName' readonly name=BranchAttrName elementtype=nacessary>
		</td>
	</tr>
</table>
</Div>

<Input type=hidden name=InsideFlag> 
<input type=hidden name=AgentState value=''> 
<Input type=hidden name=Operator value=''> 
<input type=hidden name=Operate value=''> 
<input type=hidden name=GrpFlag value='2'> 
<input type=hidden name=AgentGroup value=''> 
<input type=hidden name=BranchType value=''> 
<input type=hidden name=BranchType2 value=''>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>

