<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LATrainerCommisionRateInput.jsp
	//程序功能：组训月绩效比例录入
	//创建时间：2018-06-7
	//创建人  ：wangQingMin
%>
<%@page contentType="text/html;charset=GBK"%>
<script language="JavaScript">
	var cSql = " 1 and length(trim(comcode)) <= #4# ";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LATrainerCommisionRateInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LATrainerCommisionRateInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="./LATrainerCommisionRateSave.jsp" method=post name=fm
		target="fraSubmit">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divQryModify);"></td>
				<td class=titleImg>查询条件</td>
			</tr>
		</table>
		<div id="divQryModify" style="display: ''">
			<table class=common>
				<tr class=common>
					<TD class=title>省分公司</TD>
					<TD class=input><Input class="codeno" name=PManageCom
						verify="省分公司|code:comcodeallsign&NOTNULL&len<=4"
						ondblclick="return showCodeList('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1);"
						onkeyup="return showCodeListKey('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1); "><Input
						class=codename name=PManageComName readOnly elementtype=nacessary></TD>
					<TD class=title>中心支公司</TD>
					<TD class=input><Input class="codeno" name=CManageCom
						verify="中心支公司|code:comcode&len>7"
						ondblclick="return getManagecom(CManageCom,CManageComName);"
						onkeyup="return getManagecom(CManageCom,CManageComName);"><Input
						class=codename name=CManageComName readOnly></TD>

				</tr>

				<TR class=common>
					<TD class=title>营业部</TD>
					<TD class=input><Input name=BranchAttr id=BranchAttr
						class="codeno"
						ondblclick="return getBranchAttr(BranchAttr,BranchAttrName);"
						onkeyup="return getBranchAttr(BranchAttr,BranchAttrName);"><Input
						maxlength=12 class="codename" name=BranchAttrName></TD>
					<TD class=title>薪资年月</TD>
					<TD class=input><Input class=common name=WageNo
						verify="薪资年月|NUM&NOTNULL&len=6" elementtype=nacessary></TD>
				</TR>
				<TR class=common>
					<td class='title'>组训人员工号</td>
					<TD class=input><Input name=TrainerCode class=common
						verify="组训人员工号|len=10"></TD>
				</TR>

			</table>
		</div>
		<table>
			<tr>
				<td><input type=button value="查  询" class=cssButton
					onclick="easyQueryClick();"> <input type=button
					value="修  改" class=cssButton onclick="return DoSave();"> <input
					type=button value="新  增" class=cssButton
					onclick="return DoInsert();"> <input type=button
					value="删  除" class=cssButton onclick="return DoDel();"> <input
					type=button value="重  置" class=cssButton
					onclick="return DoReset();"></td>
			</tr>
		</table>
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divSetGrid);"></td>
				<td class=titleImg>基本工资信息结果</td>
			</tr>
		</table>

		<div id="divSetGrid" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1><span id="spanSetGrid"></span>
					</td>
				</tr>
			</table>

			<INPUT VALUE=" 首页 " TYPE="button" class=cssButton
				onclick="turnPage.firstPage();"> <INPUT VALUE="上一页"
				TYPE="button" class=cssButton onclick="turnPage.previousPage();">
			<INPUT VALUE="下一页" TYPE="button" class=cssButton
				onclick="turnPage.nextPage();"> <INPUT VALUE=" 尾页 "
				TYPE="button" class=cssButton onclick="turnPage.lastPage();">
		</div>
		<br>
		<hr>
		<input type=hidden class=Common name=AgentGroup> <input
			type=hidden id="sql_where" name="sql_where"> <input
			type=hidden id="fmAction" name="fmAction"> <input type=hidden
			class=Common name=querySql> <input type=hidden
			id="BranchType" name="BranchType" value="1"> <input
			type=hidden id="BranchType2" name="BranchType2" value="01">
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>




