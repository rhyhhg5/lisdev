//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}



//提交，保存按钮对应操作
function submitForm(){
	if (mOperate!='DELETE||MAIN'){
		if (!beforeSubmit())
			return false;
		if (mOperate!="UPDATE||MAIN")
			mOperate="INSERT||MAIN";
	}
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//xjh Add For PICC
	//fm.all('AgentKind').disabled = true;
	//fm.all('AgentGrade').disabled = true;
	//fm.all('AgentGroup').disabled = true;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		initInpBox();
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAMedComAgentInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit(){
	
		//xjh Add For PICC
	//zff  身份证尾号不能为小x的校验
	if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}
	// 验证邮件地址输入是否正确
	var tEMail = document.fm.EMail.value;
	if(tEMail != "" && tEMail != null)
	{
		var tArrValue = tEMail.split("@");
		if(tArrValue.length != 2)
		{
			alert("邮件地址输入有误！");
			return false;
		}
		var tEMailLen = tEMail.length;
		if(tEMail.indexOf("@") <= 0 || tEMail.indexOf("@") == tEMailLen-1)
		{
			alert("邮件地址输入有误！");
			return false;
		}
	}
	if (mOperate!="UPDATE||MAIN")
	{
		if(document.fm.AgentCode.value != "" && document.fm.AgentCode.value != null)
		{
			alert("查询出来的数据只允许进行修改操作！");
			return false;
		}
	}
	
	//添加操作
	if( verifyInput() == false ) return false;
	//新增校验联系电话
	var Phone = fm.all('Phone').value.trim();
	if(Phone!=null&&Phone!=""){
    	var result=CheckFixPhoneNew(Phone);
    	var result2=CheckPhoneNew(Phone);
        if(result!=""&&result2!=""){
    	  alert("联系电话不符合规则，请录入正确的固话或手机号！");
    	  fm.all('Phone').value='';
    	  return false;
    	}
	}
    //新增校验手机号
    var Mobile = fm.all('Mobile').value.trim();
    if(Mobile!=null&&Mobile!="")
     { 
	  var result=CheckPhoneNew(Mobile);
      if(result!=""){
	  alert(result);
	  fm.all('Mobile').value='';
	  return false;
	  }
    }
	var strReturn = checkIdNo(trim(fm.all('IDNoType').value),trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value))
	if (strReturn != ''){
		alert(strReturn);
		return false;
	}
	
	return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,82,*";
	}
}



//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的人员记录！');
  }else
  {
  	//zff  身份证尾号不能为小x的校验
	if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码

   var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
    showInfo=window.open("./LAMedComAgentQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要删除的代理人记录！');
  }else
  {
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function afterQuery(arrQueryResult)
{

  var arrResult = new Array();
  
	if( arrQueryResult != null )
	{

		arrResult = arrQueryResult;
		fm.all('AgentCode').value = arrResult[0][0];
		fm.all('Name').value = arrResult[0][1];
		fm.all('Sex').value = arrResult[0][2];
		fm.all('Birthday').value = arrResult[0][3];
		fm.all('NativePlace').value = arrResult[0][4];
		fm.all('Nationality').value = arrResult[0][5];
		fm.all('RgtAddress').value = arrResult[0][6];
		fm.all('HomeAddress').value = arrResult[0][7];
        fm.all('ZipCode').value = arrResult[0][8];
		fm.all('Phone').value = arrResult[0][9];
		fm.all('BP').value = arrResult[0][10];
		fm.all('Mobile').value = arrResult[0][11];
		fm.all('EMail').value = arrResult[0][12];
		fm.all('IDNo').value = arrResult[0][13];
		fm.all('PolityVisage').value = arrResult[0][14];
		fm.all('Degree').value = arrResult[0][15];
		fm.all('GraduateSchool').value = arrResult[0][16];	
		fm.all('Speciality').value = arrResult[0][17];
		fm.all('PostTitle').value = arrResult[0][18];
		fm.all('OldCom').value = arrResult[0][19];
		fm.all('OldOccupation').value = arrResult[0][20];
		fm.all('HeadShip').value = arrResult[0][21];
		fm.all('EmployDate').value = arrResult[0][22];	
		fm.all('AgentState').value = arrResult[0][23];
		fm.all('Operator').value = arrResult[0][25];
		fm.all('ManageCom').value = arrResult[0][26];
		fm.all('AgentCom').value = arrResult[0][29];
		fm.all('IDNoType').value = arrResult[0][30];	
		fm.all('AgentGroup').value = arrResult[0][31];
		
		
		
		//xjh Add For PICC ,禁用行政信息修改,因为在这里不能修改
		//fm.all('AgentKind').disabled = true;
		//fm.all('AgentGrade').disabled = true;
		//fm.all('AgentGroup').disabled = true;
		//document.fm.AgentKind.readOnly=true ;
		//document.fm.AgentGrade.readOnly=true ;
		//document.fm.AgentGroup.readOnly=true ;	
	
		// 查询并显示编码所代表的中文意思
		showCodeName();
	}
}

function showCodeName()
{
	var tSQL = "";
	var tCode = "";
	// 性别
	tCode = trim(document.fm.Sex.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'sex' and code = '"+tCode+"'";
		document.fm.SexName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 民族
	tCode = trim(document.fm.Nationality.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'nationality' and code = '"+tCode+"'";
		document.fm.NationalityName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 户口所在地
	tCode = trim(document.fm.RgtAddress.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'nativeplacebak' and code = '"+tCode+"'";
		document.fm.RgtAddressName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 籍贯
	tCode = trim(document.fm.NativePlace.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'nativeplacebak' and code = '"+tCode+"'";
		document.fm.NativePlaceName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 政治面貌
	tCode = trim(document.fm.PolityVisage.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'polityvisage' and code = '"+tCode+"'";
		document.fm.PolityVisageName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 学历
	tCode = trim(document.fm.Degree.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'degree' and code = '"+tCode+"'";
		document.fm.DegreeName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 职称
	tCode = trim(document.fm.PostTitle.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'posttitle' and code = '"+tCode+"'";
		document.fm.PostTitleName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 原职业
	tCode = trim(document.fm.OldOccupation.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select trim(OccupationName)||'-'||workname from LDOccupation where OccupationCode = '"+tCode+"'";
		document.fm.OldOccupationName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
   // 证件类型
	tCode = trim(document.fm.IDNoType.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'idtype' and code = '"+tCode+"'";
		document.fm.IDNoTypeName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";

    // 资格证信息
	tCode = trim(document.fm.AgentCode.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select QualifNo,GrantUnit,GrantDate,ValidStart,ValidEnd,"
		+"State,Case State WHEN '0' THEN '有效' else '无效' END "
		+" from laqualification where agentcode =  '"+tCode+"'";
		var strQueryResult  = easyQueryVer3(tSQL, 1, 1, 1);
		if (strQueryResult)
		{
					var arr = decodeEasyQueryResult(strQueryResult);
					fm.all('QualifNo').value= trim(arr[0][0]) ;
					fm.all('GrantUnit').value= trim(arr[0][1]) ;
					fm.all('GrantDate').value= trim(arr[0][2]) ;
					fm.all('ValidStart').value= trim(arr[0][3]) ;
					fm.all('ValidEnd').value= trim(arr[0][4]) ;
					fm.all('QualifState').value= trim(arr[0][5]) ;
					fm.all('QualifStateName').value= trim(arr[0][6]) ;
				}
	}




	tSQL = "";
	tCode = "";
	// 机构主管
	tCode = trim(document.fm.ManageCom.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select name from ldcom where comcode='"+tCode+"'";
   		document.fm.ManageComName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	//
	tCode = trim(document.fm.AgentCom.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select a.name from lacom a  where a.agentcom='"+tCode+"'";
   		document.fm.AgentComName.value = getArrValueBySQL(tSQL);
	}
}

//function changeIDNo()
//{
//   if (getWherePart('IDNo')=='')
//     return false;
//   var strSQL = "";
//   strSQL = "select * from LAAgent where 1=1  and agentstate<'06'"
//           + getWherePart('IDNo') ;
//     	 //alert(strSQL);
//   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
//   if (strQueryResult)
//   {
//   	alert('该证件号已存在并未离职!');
//   	fm.all('IDNo').value = '';
//   	return false;
//   }
//   return true;
//}


/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	try	{
		if( cCodeName == "AgentGrade" )
		{
			checkvalid();//loadFlag在页面出始化的时候声明
		}
	}
	catch( ex ) {
	}
}


function checkvalid()
{
  //	fm.InsideFlag.value="";
}

/***************************************
 * 根据参数查询结果
 *     参数：pmSQL  查询用的SQL文
 *     返回：查询结果（要求SQL文的结果是唯一值）
 ***************************************/
function getArrValueBySQL(pmSQL)
{
	//alert(pmSQL);
	var strQueryResult  = easyQueryVer3(pmSQL, 1, 0, 1);
	
	if (!strQueryResult)
		return "";
		
	var arr = decodeEasyQueryResult(strQueryResult);
	
	return arr[0][0];
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#" + fm.all('BranchType').value + "# and (BranchType2=#" + '02' + "# or BranchType2 =#" + '04' + "#) and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}

function AgentClick()
{
	var AgentCode=fm.all('AgentCode').value;
	if(!AgentCode)
	{
		alert('销售人员代码不能为空！');
		return false;
	}
	showInfo=window.open("../agentdaily/LAMedQualifInput.jsp?AgentCode="+fm.all('AgentCode').value+"");
}