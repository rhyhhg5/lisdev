<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<html>
	<%
//程序名称：ChangeBranchInput.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String tOperator = tG.Operator;
	System.out.println("tOperator"+tOperator);
	String BranchType=request.getParameter("BranchType");
	String BranchType2=request.getParameter("BranchType2");
%>

<head >
 <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
 <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
 <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
 <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
 <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
 <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
 <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LANewToFormInput1.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<title>
	维护
</title>
</head>
<body  onload="initForm();initDate();initElementtype();" >
	<form action="./LANewToFormSave1.jsp" method=post name=fm target="fraSubmit">
		<table class=common >
		  <tr class=common>
       <td class=common>
         <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAimChangeBranch);" >
         <td class=titleImg>新入司人员信息</td>
       </tr>
		</table>
		<Div  id= "divAimChangeBranch" style= "display: ''">
			<table  class= common>
				<TR  class= common>
					<TD class= title>业务员代码</TD>
					<TD  class= input>
						<Input class=common name=AgentCode
						 onchange="return getAgentMs();"
						  verify="业务员代码|notnull"   elementtype=nacessary>
					</TD>
					<TD class= title>业务员名称</TD>
					<TD  class= input>
						<Input class=readonly readonly name=AgentName>
					</TD>
				</tr>
				<TR  class= common>
					<TD class= title>业务员团队代码</TD>
					<TD  class= input>
						<Input class=readonly readonly name=BranchAttr>
					</TD>
					<TD class= title>业务员团队名称</TD>
					<TD  class= input>
						<Input class=readonly readonly name=BranchAttrName>
					</TD>
				</tr>
					<TR  class= common>
					<TD  class= title>业务员管理机构</TD>
					<TD  class= input>
						<Input class=readonly readonly name=ManageCom >
					</TD>
						<TD  class= title>业务员入司时间</TD>
					<TD  class= input >
					<Input class=readonly readonly name=IndueFormDate >
					</TD>
				</TR>
				<TR  class= common>
					<TD  class= title>推荐人</TD>
					<TD  class= input>
						<Input class=readonly readonly name=IntroOldAgency >
					</TD>
						<TD  class= title>推荐人名称</TD>
					<TD  class= input >
					<Input class=readonly readonly name=IntroOldAgencyName >
					</TD>
				</TR>

				<TR  class= common>
					<TD  class= title>新入司时间</TD>
					<TD  class= input >
						<Input class='coolDatePicker' dateformat= 'short' name=AdjustDate verify="新入司时间|Date" elementtype=nacessary >
           </TD>

         <TD  class= title>操作员代码</TD>
					<TD  class= input>
						<Input class="readonly" readonly name=Operator value = <%=tOperator%>>
					</TD>
			</TR>
			<TR  class= common>
			 <TD  class= title>新推荐人</TD>
				<TD  class= input >
					 <Input class=common name=IntroNewAgency  onchange="return changeIntroAgency();">
         </TD>
			<TD class= title>
         推荐人姓名
       </TD>
       <TD class= input>
       	<Input class=readonly readonly name=IntroNewAgencyName >
       </TD>
        </TR>
			<TR  class= common>
			<TD  class= title>人员筹备标记</TD>
				<TD  class= input >
					 <Input class=readonly name=NoWorkFlag readonly>
         </TD>
			 <TD  class= title>人员筹备时间</TD>
				<TD  class= input >
					 <Input class=readonly name=TrainDate readonly>
         </TD>
        </TR>
        <TR  class= common>
        <TD  class= title>新人员筹备时间</TD>
				<TD  class= input >
					 <Input  class='coolDatePicker' dateformat= 'short' name=NewTrainDate verify="新人员筹备时间|Date" >
         </TD>
        </TR>
			</table>
			<br>
      <input type=button class=cssButton name=saveb value='保存' onclick="return submitSave()">
      <p> <font color="#ff0000">注：新入司时间、新推荐人或者筹备时间至少输入一个</font></p>
		</div>
		
    <input type=hidden name=BranchType value=<%=BranchType%>>
	<input type=hidden name=BranchType2 value=<%=BranchType2%>>
	<input type=hidden name=AgentGrade value=''>
	<input type=hidden name=AgentGroup value=''>
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
