<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AdjustAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentassess.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//接收信息，并作校验处理。
//输入参数
	LAAssessAccessorySchema mLAAssessAccessorySchema = new LAAssessAccessorySchema();
	LAActiveAssessInputUI mLAActiveAssessInputUI = new LAActiveAssessInputUI();

//输出参数
	CErrors tError = null;
	String tOperate="";
	String tRela  = "";
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");

	mLAAssessAccessorySchema.setAgentCode(request.getParameter("AgentCode"));
	mLAAssessAccessorySchema.setAgentGrade(request.getParameter("OldAgentGrade"));//存的是原来的职级
	mLAAssessAccessorySchema.setAgentGrade1(request.getParameter("NewAgentGrade"));//存的是将要调整至的职级
	mLAAssessAccessorySchema.setAgentGroup(request.getParameter("AgentGroup"));
//  mLAAssessAccessorySchema.setOperator(request.getParameter("Operator"));
	System.out.println("AgentGroup:"+request.getParameter("AgentGroup"));
//	mLAAssessAccessorySchema.setAgentGroupNew(request.getParameter("NewAgentGroup"));
	mLAAssessAccessorySchema.setIndexCalNo(request.getParameter("IndexCalNo"));
	mLAAssessAccessorySchema.setBranchType(request.getParameter("BranchType"));
	mLAAssessAccessorySchema.setBranchType2(request.getParameter("BranchType2"));
	mLAAssessAccessorySchema.setMakeTime(request.getParameter("MakeTime"));
	mLAAssessAccessorySchema.setManageCom(request.getParameter("ManageCom"));
	mLAAssessAccessorySchema.setModifyFlag(request.getParameter("TransferType"));
	mLAAssessAccessorySchema.setAgentSeries(request.getParameter("OldSeries"));//存的是laaegentgrade 中gradeproperty2字段
	mLAAssessAccessorySchema.setAgentSeries1(request.getParameter("NewSeries"));//存的是laaegentgrade 中gradeproperty2字段
	mLAAssessAccessorySchema.setStandAssessFlag("0");//0  手工考核
	mLAAssessAccessorySchema.setAssessType("00"); //默认为职级考核维度

// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";
	tVData.add(tG);
	tVData.addElement(mLAAssessAccessorySchema);
	try{
		mLAActiveAssessInputUI.submitData(tVData,"INSERT||MAIN");
	}
	catch(Exception ex){
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail"))
	{
		tError = mLAActiveAssessInputUI.mErrors;
		if (!tError.needDealError())
		{
			Content = " 保存成功! ";
			FlagStr = "Succ";
		}
		else
		{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

//添加各种预处理

%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

