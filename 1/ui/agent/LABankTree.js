//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function initEdorType(cObj)
{
	mEdorType = " 1 and codealias=#3# ";
	showCodeList('agentkind',[cObj,AgentKindName],[0,1], null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " 1 and codealias=#3#";
	showCodeListKey('agentkind',[cObj,AgentKindName],[0,1], null, mEdorType, "1");
}

//提交，保存按钮对应操作
function submitForm()
{  
  if (!beforeSubmit())
    return false;
   if(fm.all('BranchCode').value!=null && fm.all('BranchCode').value!='' && fm.all('NewAgentGrade').value!=null && fm.all('NewAgentGrade').value!=''){
 		if(!checkBranchManager()){
			return false;
		}
   }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.submit(); //提交 
}
//核查团队有无主管
function checkBranchManager(){
	var tBranchAttr;
	var tAgentGrade=fm.all('NewAgentGrade').value;
	var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
	if(fm.all('BranchCode').value==null || fm.all('BranchCode').value==''){
		alert('销售团队代码不能为空！');
		return false;
	}else{
		tBranchAttr=fm.all('BranchCode').value;
		var tSql="select gradeproperty2 from laagentgrade where gradecode='"+tAgentGrade+"'";
		var arr=easyExecSql(tSql);
		var tGradeProperty2=arr[0][0];
		tSql="select * from latree where agentgroup=(select agentgroup from labranchgroup "
			+"where branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"' and branchattr='"+tBranchAttr+"')"
			+"and agentgrade in (select gradecode from laagentgrade where branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"' and gradeproperty2='"+tGradeProperty2+"')"
			+"and branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"' and agentcode in (select agentcode from laagent where agentgroup=latree.agentgroup and agentstate<'06')";
		var strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);
		if(strQueryResult && tGradeProperty2==1){
			alert("该团队已存在渠道经理级别的主管！");
			return false;
		}else if(strQueryResult && tGradeProperty2==2){
			alert("该团队已存在营业部负责人级别的主管！");
			return false;
		}
		return true;	
	}
	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LABankTree.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  if( verifyInput() == false )   
  	return false;  
  //这里不在校验，营业部经理降级为业务员，允许团队有主管，所以校验放在后台处理
  //if(fm.UpAgent.value != null && fm.UpAgent.value != ""){
  //	alert("所选销售团队必须无主管！");
  //	return false;
	//}
	//业务职级与行政职级的校验
	if(fm.all('AgentGrade').value==null || fm.all('AgentGrade').value==''){
		if(fm.all('NewAgentGrade').value==null || fm.all('NewAgentGrade').value==''){
			if(fm.BranchCode.value != null && fm.BranchCode.value != ""){
				alert("行政职级没有跨序列修改，不能录入销售团队代码！");
				return false;
			}
		}else {
			if(fm.BranchCode.value == null || fm.BranchCode.value == ""){
				alert("跨序列，必须录入新的销售团队代码！");
				return false;
			}
		}
	}else if(fm.all('AgentGrade').value<='G52'){
		if(fm.all('NewAgentGrade').value==null || fm.all('NewAgentGrade').value==''){
			if(fm.BranchCode.value != null && fm.BranchCode.value != ""){
				alert("由渠道经理变为业务人员，不能录入销售团队代码！");
				return false;
			}
		}else if(fm.all('NewAgentGrade').value<='G52'){
			if(fm.BranchCode.value != null && fm.BranchCode.value != ""){
				alert("行政职级没有跨序列修改，不能录入销售团队代码！");
				return false;
			}
		}else if(fm.all('NewAgentGrade').value>'G52'){
			if(fm.BranchCode.value == null || fm.BranchCode.value == ""){
				alert("跨序列，必须录入新的销售团队代码！");
				return false;
			}
		}
	}else if(fm.all('AgentGrade').value>'G52'){
		if(fm.all('NewAgentGrade').value==null || fm.all('NewAgentGrade').value==''){
			if(fm.BranchCode.value == null || fm.BranchCode.value == ""){
				alert("跨序列，必须录入新的销售团队代码！");
				return false;
			}
		}else if(fm.all('NewAgentGrade').value<='G52'){
			if(fm.BranchCode.value == null || fm.BranchCode.value == ""){
				alert("跨序列，必须录入新的销售团队代码！");
				return false;
			}
		}else if(fm.all('NewAgentGrade').value>'G52'){
			if(fm.BranchCode.value != null && fm.BranchCode.value != ""){
				alert("行政职级没有跨序列修改，不能录入销售团队代码！");
				return false;
			}
		}
	}
	if(fm.NewAgentGrade1.value == null ||fm.NewAgentGrade1.value ==""){
		if(fm.NewAgentGrade.value == null ||fm.NewAgentGrade.value ==""){
			alert("拟改业务职级，拟改行政职级不能同时为空！");
			return false;
		}
		if(fm.NewAgentGrade.value <= 'G52'){
			alert("渠道经理必须拥有业务职级");
			return false;
		}
	}else{
		if(fm.NewAgentGrade.value != null&&fm.NewAgentGrade.value !=""&&fm.NewAgentGrade.value > 'G52'){
			alert("营业部经理不能拥有业务职级");
			return false;
		}
	}
	return true;  
}           

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 else 
 {
  	parent.fraMain.rows = "0,0,0,82,*";
 }
}
       
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的业务员信息！');    
  }else
  {
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {  	
      mOperate="UPDATE||MAIN";      
      submitForm();   
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./LABankTreeQuery.html");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function afterQuery(arrQueryResult)
{
	initForm();	
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;		                
		fm.all('AgentCode').value = arrResult[0][0];
    fm.all('Name').value = arrResult[0][1];
    if(arrResult[0][7]=='0'){
      fm.all('AgentGrade1').value = arrResult[0][2];
      fm.all('AgentGradeComName1').value = arrResult[0][9];
    }else if(arrResult[0][7]=='1'){
      fm.all('AgentGrade1').value = arrResult[0][2];
      fm.all('AgentGradeComName1').value = arrResult[0][9];
      fm.all('AgentGrade').value = arrResult[0][3];
      fm.all('AgentGradeComName').value = arrResult[0][10];
    }else if(arrResult[0][7]>'1'){
      fm.all('AgentGrade').value = arrResult[0][3];
      fm.all('AgentGradeComName').value = arrResult[0][10];
    }
    fm.all('BranchAttr').value = arrResult[0][4];
    fm.all('ChannelName').value=arrResult[0][5];
    fm.all('OldUpAgent').value = arrResult[0][6];   
   	//fm.all('Operator').value = arrResult[0][7];    
   }

}
function afterCodeSelect( cCodeName, Field )
{
  if(cCodeName == "branchattr"){
  	tsql="select name,agentgroup,branchmanager,managecom,branchlevel "
  	+" from labranchgroup a where branchtype='3' and branchtype2='01' and branchattr='"
  	+fm.BranchCode.value+"'   and (EndFlag <> 'Y' or EndFlag is null) ";
  	var crr = easyExecSql(tsql);
  	if(crr){
  		fm.BranchName.value = crr[0][0];
  		fm.AgentGroup.value = crr[0][1]; //团队内部编码
  		fm.UpAgent.value = crr[0][2]; //上级主管编码，用来该团队是否已经存在主管
  		fm.ManageCom.value = crr[0][3];
  		fm.BranchLevel.value = crr[0][4];
  	}
  	else{
  		alert("没有此团队!");
  		fm.BranchCode.value='';
  		fm.BranchName.value='';
  		return false ;
  	}
  }
}
