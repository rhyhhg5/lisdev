<%
//程序名称：LABranchGroupBuildQueryInit.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript"><!--
function initForm()
{
	initBranchGroupGrid();
	
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化BranchGroupGrid
 ************************************************************
 */
function initBranchGroupGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="管理机构";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
		
        iArray[2]=new Array();
        iArray[2][0]="管理机构名称";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="薪资月";         //列名
        iArray[3][1]="70px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;        
        
        iArray[4]=new Array();
        iArray[4][0]="职级";         //列名
        iArray[4][1]="60px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="考核状态";         //列名
        iArray[5][1]="70px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
      	iArray[6]=new Array();
        iArray[6][0]="展业类型";         //列名
        iArray[6][1]="70px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许 */
        
        iArray[7]=new Array();
        iArray[7][0]="展业类型名称";         //列名
        iArray[7][1]="80px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="销售渠道";         //列名
        iArray[8][1]="70px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许 */
        
         iArray[9]=new Array();
        iArray[9][0]="销售渠道名称";         //列名
        iArray[9][1]="80px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
/*      iArray[9]=new Array();
        iArray[9][0]="计算类别";         //列名
        iArray[9][1]="70px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许 
        
        iArray[10]=new Array();
        iArray[10][0]="入机日期";         //列名
        iArray[10][1]="70px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许 
        
        iArray[11]=new Array();
        iArray[11][0]="入机时间";         //列名
        iArray[11][1]="70px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许 

   */
        BranchGroupGrid = new MulLineEnter( "fm" , "BranchGroupGrid" ); 

        //这些属性必须在loadMulLine前
        BranchGroupGrid.mulLineCount = 0;   
        BranchGroupGrid.displayTitle = 1;
        BranchGroupGrid.hiddenPlus = 1;
        BranchGroupGrid.hiddenSubtraction = 1;
        BranchGroupGrid.locked=0;
        BranchGroupGrid.canSel=1;
        BranchGroupGrid.canChk=0;
        BranchGroupGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化BranchGroupGrid时出错："+ ex);
      }
    }


--></script>