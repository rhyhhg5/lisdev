//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
 

// 获取销售团队信息 ondblclick方法
function getChangeComName(cObj,cName)
{
  if (fm.all('ManageCom').value ==null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
 if(fm.all('ManageCom').value.substring(4,8)=='0000')
 {
 	var strsql =" 1 and BranchType=#"+fm.all('BranchType').value+"# and BranchType2=#"+fm.all('BranchType2').value+"# and ManageCom like #" + fm.all('ManageCom').value + "%#  and substr(ManageCom,5)=#0000# and EndFlag<>#Y#  and state=#1# ";
 }
 else
	var strsql =" 1 and BranchType=#"+fm.all('BranchType').value+"# and BranchType2=#"+fm.all('BranchType2').value+"# and ManageCom like #" + fm.all('ManageCom').value + "%# and EndFlag<>#Y#  and state=#1# ";
	showCodeList('branchattr',[cObj,cName],[0,1],null,strsql,'1',1);
}

// 获取销售团队信息 onkeyup方法
 function getChangeComName1(cObj,cName)
{
  if (fm.all('ManageCom').value ==null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
 if(fm.all('ManageCom').value.substring(4,8)=='0000')
 {
 	var strsql =" 1 and BranchType=#"+fm.all('BranchType').value+"# and BranchType2=#"+fm.all('BranchType2').value+"# and ManageCom like #" + fm.all('ManageCom').value + "%#  and substr(ManageCom,5)=#0000# and EndFlag<>#Y#  and state=#1# ";
 }
 else
	var strsql =" 1 and BranchType=#"+fm.all('BranchType').value+"# and BranchType2=#"+fm.all('BranchType2').value+"# and ManageCom like #" + fm.all('ManageCom').value + "%# and EndFlag<>#Y#  and state=#1# ";
	showCodeListKey('branchattr',[cObj,cName],[0,1],null,strsql,'1',1);
}
 


//提交，保存按钮对应操作
function submitForm()
{
  if (mOperate!='DELETE||MAIN')
  {
	  if (mOperate!="UPDATE||MAIN") mOperate="INSERT||MAIN";
	  if (!beforeSubmit()) return false;	
  }
  if (mOperate=='INSERT||MAIN')
  {
	if (fm.all('AgentCode').value != "")
	{
	  if (confirm("您确实想增加该记录吗?"))
      {
	  }else{				
		alert("您取消了操作");	 
		return false;
	  }
	 }
  }
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.all('Operate').value=  mOperate;
  fm.submit(); //提交
  fm.GrpFlag.value="2";
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
 
  showInfo.close();   
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    //执行下一步操作
  }
  mOperate = "";
}

//提交前的校验、计算
function beforeSubmit()
{
	//添加操作
	if( verifyInput() == false ) return false; 
    if(!changeGroup())
    {
       return false;
    }
	return true;
}

// 校验有效团队
function changeGroup()
{
   var strSQL = "";
   strSQL = "select agentgroup "
           +"  from LABranchGroup a "
           +"where 1=1 and   a.EndFlag <> 'Y' and  a.state = '1'  "
           + getWherePart('a.ManageCom','ManageCom')
           + getWherePart('a.BranchType','BranchType')
           + getWherePart('a.BranchType2','BranchType2')
           + getWherePart('a.BranchAttr','BranchAttr');
    
   var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
   if (!strQueryResult)
   {
   	alert('不存在该销售团队！');
   	fm.all('AgentGroup').value = '';
   	fm.all('BranchAttr').value = '';
   	return false;
   }
   var arr = decodeEasyQueryResult(strQueryResult);
   
   fm.all('AgentGroup').value = trim(arr[0][0]);
   if(fm.all('AgentGroup').value==null || fm.all('AgentGroup').value=='')
   {
        alert('不存在该销售团队！');
   	fm.all('AgentGroup').value = '';
   	fm.all('BranchAttr').value = '';
   	return false;  
   }
   return true;
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,82,*";
	}
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的代理人记录！');
  }else
  {
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
   var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
   showInfo=window.open("./LASocialAgentQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
 


function afterQuery(arrQueryResult)
{
  var arrResult = new Array(); 
  if( arrQueryResult != null )
  {
  	
  	arrResult = arrQueryResult;
  	fm.all('AgentCode').value = arrResult[0][0];
  	fm.all('Name').value = arrResult[0][1];
  	fm.all('AgentGroup').value = arrResult[0][2];
  	 
  	fm.all('ManageCom').value = arrResult[0][3];
  	showOneCodeNametoAfter('comcode','ManageCom'); 
  	fm.all('BranchAttr').value = arrResult[0][4];
  	
  	var strsql ="  select BranchAttr, Name from LABranchGroup where 1 =  1 and BranchType='"+fm.all('BranchType').value+"' and BranchType2='"+fm.all('BranchType2').value+"' "
  	+" and   EndFlag<>'Y'  and state='1'  "
  	+ getWherePart('ManageCom','ManageCom','like') 
  	+ getWherePart('AgentGroup','AgentGroup');  	
  	var strQueryResult  = easyQueryVer3(strsql, 1, 0, 1); 
  	 
  	if (!strQueryResult)
    {
   	  alert('不存在该销售团队！');
   	  fm.all('AgentCode').value = '';
      fm.all('Name').value = '';
      fm.all('EmployDate').value = '';
      fm.all('Operator').value = '';
      fm.all('ManageCom').value = '';
      fm.all('BranchAttr').value = '';
      return false;
     }else
     {
         var arr = decodeEasyQueryResult(strQueryResult); 
         fm.all('BranchAttrName').value=arr[0][1] ;
     }        
  	fm.all('EmployDate').value = arrResult[0][5];
  	fm.all('AgentState').value = arrResult[0][6];
  	fm.all('BranchType').value = arrResult[0][7];
  	fm.all('BranchType2').value = arrResult[0][8];
  }
  
}