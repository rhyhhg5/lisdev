<%@include file="../common/jsp/UsrCheck.jsp"%>
 <html>  
<%
//程序名称：LATrainerWageConfirmInput.jsp
//程序功能：
//创建日期：2018-05-29 15:39:06
//创建人  ：WangQingMin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="LATrainerWageConfirmInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LATrainerWageConfirmInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >    
  <form action="./LATrainerWageConfirmSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		组训员工月终薪资确认
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom  verify="管理机构|NOTNULL"
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            ><Input class="codename" name=ManageComName readOnly  elementtype=nacessary> 
          </TD> 
           <TD  class= title>
            薪资年月
          </TD>
          <TD  class= input>
            <Input class=common  name=WageNo verify="薪资年月|NOTNULL&num&len=6"  elementtype=nacessary >
          </TD>        
        </TR>
      </table>
        <input type =button name =Calculate class=cssbutton value="确认" onclick="LATrainerWageConfirmSave();">    
    </Div>        
   <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
      <input type=hidden id="fmAction" name="fmAction">  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>