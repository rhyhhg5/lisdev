<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
  LATreeSchema tLATreeSchema = new LATreeSchema();
  LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
  LARearRelationSet tLARearRelationSet = new LARearRelationSet();
  LAAgentEditUI tLAAgentEditUI = new LAAgentEditUI();
  String Agentcode = "";

	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tIsManager = request.getParameter("hideIsManager");
  String tOrphanCode = request.getParameter("OrphanCode");
  String tRearStr = "";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String tIntroAgency="";
  String AgentName=request.getParameter("AgentName");
  System.out.println(".....................save jsp:agentname"+AgentName);
 
  System.out.println("begin agent schema...");
  //add by lyc 统一工号 2014-11-27
  String cSql = "select agentcode from laagent where groupagentcode = '"+request.getParameter("AgentCode")+"' fetch first row only";
  String cAgentCode = new ExeSQL().getOneValue(cSql);
  //取得代理人信息(增加加密信息1111)
  LisIDEA tLisIdea = new LisIDEA();
  tLAAgentSchema.setPassword(tLisIdea.encryptString("1111"));
  tLAAgentSchema.setAgentCode(cAgentCode);
  tLAAgentSchema.setAgentGroup(request.getParameter("hideBranchCode")); //暂存显式代码
  tLAAgentSchema.setBranchCode(request.getParameter("hideBranchCode"));
  tLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
  //by gzh 20110407  修改时，当状态为“00” 未提交集团，则仍为‘00’，若为‘01’，则修改为99
  String sql = "select crs_check_status from laagent where groupagentcode = '"+request.getParameter("AgentCode")+"'";
  String crs_check_status = new ExeSQL().getOneValue(sql);
  if("01".equals(crs_check_status)){
  	tLAAgentSchema.setCrs_Check_Status("99"); //更新标志
  }else{
  	tLAAgentSchema.setCrs_Check_Status("00"); //更新标志
  } 
  //by gzh end
  tLAAgentSchema.setBranchType(request.getParameter("BranchType"));
  tLAAgentSchema.setBranchType2(request.getParameter("BranchType2"));
  tLAAgentSchema.setName(request.getParameter("Name"));
  System.out.println(request.getParameter("BranchCode"));
  System.out.println(request.getParameter("hideBranchCode"));
  //tLAAgentSchema.setSex(request.getParameter("Sex"));
  tLAAgentSchema.setSex(request.getParameter("hideSex"));
  System.out.println(request.getParameter("hideSex"));
  tLAAgentSchema.setBirthday(request.getParameter("Birthday"));
  tLAAgentSchema.setIDNo(request.getParameter("IDNo"));
 
  
  //tLAAgentSchema.setIDNoType(request.getParameter("IDNoType"));
  tLAAgentSchema.setIDNoType(request.getParameter("hideIDNoType"));
  System.out.println(request.getParameter("hideIDNoType"));
  tLAAgentSchema.setNativePlace(request.getParameter("NativePlace"));
  tLAAgentSchema.setNationality(request.getParameter("Nationality"));
  tLAAgentSchema.setPolityVisage(request.getParameter("PolityVisage"));
  tLAAgentSchema.setRgtAddress(request.getParameter("RgtAddress"));
  tLAAgentSchema.setDegree(request.getParameter("Degree"));
  tLAAgentSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
  tLAAgentSchema.setSpeciality(request.getParameter("Speciality"));
  tLAAgentSchema.setPostTitle(request.getParameter("PostTitle"));
  tLAAgentSchema.setHomeAddress(request.getParameter("HomeAddress"));
  tLAAgentSchema.setZipCode(request.getParameter("ZipCode"));
  tLAAgentSchema.setPhone(request.getParameter("Phone"));
  tLAAgentSchema.setBP(request.getParameter("BP"));
  tLAAgentSchema.setMobile(request.getParameter("Mobile"));
  tLAAgentSchema.setEMail(request.getParameter("EMail"));
  tLAAgentSchema.setOldCom(request.getParameter("OldCom"));
  tLAAgentSchema.setOldOccupation(request.getParameter("OldOccupation"));
  tLAAgentSchema.setHeadShip(request.getParameter("HeadShip"));
  tLAAgentSchema.setQuafNo(request.getParameter("QuafNo"));
  tLAAgentSchema.setQuafEndDate(request.getParameter("QuafEndDate"));
  tLAAgentSchema.setDevNo1(request.getParameter("DevNo1"));
  tLAAgentSchema.setDevGrade(request.getParameter("DevGrade"));
  tLAAgentSchema.setInsideFlag("1");  //外勤
  //筹备标记
  tLAAgentSchema.setNoWorkFlag(request.getParameter("NoWorkFlag"));
  tLAAgentSchema.setTrainDate(request.getParameter("TrainDate"));
  tLAAgentSchema.setPrepareEndDate(request.getParameter("PrepareEndDate"));
  tLAAgentSchema.setPreparaGrade(request.getParameter("PreparaGrade"));
  tLAAgentSchema.setPreparaType(request.getParameter("PreparaType"));
  tLAAgentSchema.setWageVersion(request.getParameter("WageVersion"));  
  
  tLAAgentSchema.setTrainPeriods(request.getParameter("TrainPeriods"));
  tLAAgentSchema.setEmployDate(request.getParameter("EmployDate"));
  tLAAgentSchema.setAssuMoney(request.getParameter("AssuMoney"));
  tLAAgentSchema.setAgentState(request.getParameter("AgentState"));
  tLAAgentSchema.setQualiPassFlag(request.getParameter("QualiPassFlag"));
  tLAAgentSchema.setBankCode(request.getParameter("BankCode"));
  tLAAgentSchema.setBankAccNo(request.getParameter("BankAccNo"));
  tLAAgentSchema.setRemark(request.getParameter("Remark"));
  tLAAgentSchema.setOperator(request.getParameter("Operator"));
  //add by zhuxt 20140902
  tLAAgentSchema.setAgentType(request.getParameter("AgentType"));
  //add by lyc 2014-11-27 统一工号
   tLAAgentSchema.setGroupAgentCode(request.getParameter("AgentCode"));
  //取得行政信息--在bl中设置职级及系列
  tLATreeSchema.setAgentCode(cAgentCode);
  tLATreeSchema.setManageCom(request.getParameter("ManageCom"));
  //modify by  fuxin 2014/12/25 10:37:52
  System.out.println("我是科比:"+request.getParameter("IntroAgency"));
  //zyy 增加推荐人集团工号校验
  if(!"".equals(request.getParameter("IntroAgency"))){
   tIntroAgency=new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("IntroAgency")+"'");   
  System.out.println("我是乔丹："+tIntroAgency);
  }
  tLATreeSchema.setIntroAgency(tIntroAgency);
  tLATreeSchema.setUpAgent(request.getParameter("UpAgent"));
  tLATreeSchema.setAgentSeries(request.getParameter("AgentSeries"));
  tLATreeSchema.setAgentGrade(request.getParameter("AgentGrade"));
  tLATreeSchema.setAgentLine("A");
  //tLATreeSchema.setAgentGrade(request.getParameter("AgentGrade"));
  tLATreeSchema.setBranchType(request.getParameter("BranchType"));
  tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));
  tLATreeSchema.setSpeciFlag(request.getParameter("SpeciFlag"));
  tLATreeSchema.setAgentLastGrade(request.getParameter("AgentLastGrade"));
  tLATreeSchema.setAgentLastSeries(request.getParameter("AgentLastSeries"));
  tLATreeSchema.setInitGrade(request.getParameter("InitGrade"));
  tLATreeSchema.setWageVersion(request.getParameter("WageVersion"));
  //添加营业组团队编码
  String tAgentGroup2Hidden =  request.getParameter("HiddenAgentGroup2");
  tLATreeSchema.setAgentGroup2(tAgentGroup2Hidden);
  
  if(tLAAgentSchema.getNoWorkFlag()!=null&&tLAAgentSchema.getTrainDate()!=null)
  {
   String tTraindate = AgentPubFun.formatDate(tLAAgentSchema.getTrainDate(),"yyyy-MM-dd");
    if((("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim()))&&tTraindate.compareTo("2012-04-01")>=0&&tTraindate.compareTo("2012-06-30")<=0)||
    		(("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim())&&tTraindate.compareTo("2013-01-01")>=0)))
     {
	  tLAAgentSchema.setWageVersion("2012A");
	  tLATreeSchema.setWageVersion("2012A");
     }
    if((tTraindate.compareTo("2013-04-01")>=0)&&("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim()))) 
    {
      tLAAgentSchema.setWageVersion("2013A");
   	  tLATreeSchema.setWageVersion("2013A");
    }
    if((tTraindate.compareTo("2013-03-16")>=0)&&("A".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim()))) 
    {
      tLAAgentSchema.setWageVersion("2013A");
   	  tLATreeSchema.setWageVersion("2013A");
    }
  }
  System.out.println("薪资版本---------》》"+tLAAgentSchema.getWageVersion());
//团队建设
  ExeSQL tExe = new ExeSQL();
  String tSql = "select gbuildflag,gbuildstartdate,gbuildenddate from labranchgroup where branchtype='1' and branchtype2='01' and branchattr='"+tLAAgentSchema.getAgentGroup()+"'";
  SSRS tSSRS = tExe.execSQL(tSql);
  if(tSSRS!=null&&(tSSRS.GetText(1,1).equals("A")||tSSRS.GetText(1,1).equals("B"))&&!tLAAgentSchema.getWageVersion().equals("2013A")&&tLAAgentSchema.getEmployDate().compareTo("2014-04-01")<0)
  {
	  tLAAgentSchema.setWageVersion("2013B");
   	  tLATreeSchema.setWageVersion("2013B");  
   	  tLAAgentSchema.setGBuildFlag(tSSRS.GetText(1,1));
   	  ExeSQL ttExe = new ExeSQL();
      System.out.println("人员入司日期"+tLAAgentSchema.getEmployDate());
      String tEmp = AgentPubFun.formatDate(tLAAgentSchema.getEmployDate(),"yyyy-MM-dd");
      String tGbulid = AgentPubFun.formatDate(tSSRS.GetText(1,2),"yyyy-MM-dd");
     if(tEmp.compareTo(tGbulid)>0)
     {
    	 tGbulid = tEmp;
     }
      String ttSql = "select enddate from LASTATSEGMENT where char(yearmonth) = db2inst1.DATE_FORMAT(((date('"+tGbulid+"') + 1 year) - 1 month),'yyyymm') and  stattype = '1'";
      SSRS ttSSRS = ttExe.execSQL(ttSql);
      tLAAgentSchema.setGBuildStartDate(tGbulid);
  	  tLAAgentSchema.setGBuildEndDate(ttSSRS.GetText(1,1));  
   	  String AgentSql = "select gbuildflag,gbuildstartdate,gbuildenddate,wageversion from laagent where agentcode = '"+tLAAgentSchema.getAgentCode()+"'";
      SSRS tSSRSTemp = tExe.execSQL(AgentSql);
      if(tSSRSTemp!=null&&(tSSRSTemp.GetText(1,1).equals("A")||tSSRSTemp.GetText(1,1).equals("B"))&&tSSRSTemp.GetText(1,4).equals("2013B"))
      {
    	  tLAAgentSchema.setGBuildFlag(tSSRSTemp.GetText(1,1));
    	  tLAAgentSchema.setGBuildStartDate(tSSRSTemp.GetText(1,2));
    	  tLAAgentSchema.setGBuildEndDate(tSSRSTemp.GetText(1,3));
    	  //tLATreeSchema.setWageVersion(tSSRSTemp.GetText(1,4));    	  
      }	
  }
  
  //取得担保人信息
  int lineCount = 0;
  String arrCount[] = request.getParameterValues("WarrantorGridNo");
  String tCautionerName[] = request.getParameterValues("WarrantorGrid1");
  String tCautionerSex[] = request.getParameterValues("WarrantorGrid2");
  String tCautionerID[] = request.getParameterValues("WarrantorGrid4");
  String tCautionerCom[] = request.getParameterValues("WarrantorGrid5");
  String tHomeAddress[] = request.getParameterValues("WarrantorGrid6");
  //String tCautionerMobile[] = request.getParameterValues("WarrantorGrid7");
  String tZipCode[] = request.getParameterValues("WarrantorGrid8");
  String tPhone[] = request.getParameterValues("WarrantorGrid9");
  String tRelation[] = request.getParameterValues("WarrantorGrid10");
  lineCount = arrCount.length; //行数
  LAWarrantorSchema tLAWarrantorSchema;
  for(int i=0;i<lineCount;i++)
  {
    tLAWarrantorSchema = new LAWarrantorSchema();
    tLAWarrantorSchema.setAgentCode(cAgentCode);
    //tLAWarrantorSchema.setSerialNo(i+1);
    tLAWarrantorSchema.setCautionerName(tCautionerName[i]);
    tLAWarrantorSchema.setCautionerSex(tCautionerSex[i]);
    tLAWarrantorSchema.setCautionerID(tCautionerID[i]);
    tLAWarrantorSchema.setCautionerCom(tCautionerCom[i]);
    tLAWarrantorSchema.setHomeAddress(tHomeAddress[i]);
    //tLAWarrantorSchema.setMobile(tCautionerMobile[i]);
    tLAWarrantorSchema.setZipCode(tZipCode[i]);
    tLAWarrantorSchema.setPhone(tPhone[i]);
    tLAWarrantorSchema.setRelation(tRelation[i]);
    tLAWarrantorSet.add(tLAWarrantorSchema);
    System.out.println("for:"+tCautionerName[i]);
  }
  System.out.println("end 担保人信息...");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.add(tIsManager);
  tVData.add(AgentName);
  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLATreeSchema);
  tVData.addElement(tLAWarrantorSet);
  tVData.addElement(tLARearRelationSet);
  tVData.addElement(tOrphanCode);
  
System.out.println("tOperate:"+tOperate);
  try
  {
    System.out.println("this will save the data!!!");
    tLAAgentEditUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAgentEditUI.mErrors;
    if (!tError.needDealError())
    {
    Agentcode = tLAAgentEditUI.getAgentCode();
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
        parent.fraInterface.fm.all('AgentCode').value = '<%=request.getParameter("AgentCode")%>';
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
