<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAContinueAgentQueryInput.jsp
//程序功能：
//创建日期：2016-04-26 15:31:08
//创建人  ：wyd程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAContinueAgentQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAContinueAgentQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>营销员查询 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAContinueAgentQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            <td class= titleImg>
                营销员查询条件
            </td>
            </td>
    	</tr>
     </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          营销员编码 
        </TD>
        <TD  class= input> 
          <Input class=common  name=AgentCode >
        </TD>
        <TD class= title>
          收费服务组代码 
        </TD>
        <TD class= input>
          <Input class=common name=AgentGroup > 
        </TD>
        <TD class= title>
          管理机构
        </TD>
        <TD  class= input>
            <Input class="codeno" name=ManageCom  verify="管理机构|notnull" 
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
             ><Input name=ManageComName class="codename" elementtype=nacessary> 
        </TD> 
      </TR>
      <TR  class= common> 
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD>        
        <TD  class= title>
          性别 
        </TD>
        <TD  class= input>
          <Input name=Sex class="codeno" MAXLENGTH=1 
           ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" 
           onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" 
           ><Input name=SexName class="codename"> 
        </TD>
        <TD  class= title>
          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class=common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          证件类型 
        </TD>
        
        <TD  class= input> 
          <Input name=IDNoType class= "codeno"  
                 ondblclick="return showCodeList('idtype',[this,IDNoTypeName],[0,1],null,null,null,null,'100');"
                 onkeyup="return showCodeListKey('idtype',[this,IDNoTypeName],[0,1]);" 
                 ><Input name=IDNoTypeName class="codename" readonly  > 
        </TD>            
        <TD  class= title>
           证件号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common > 
        </TD>

        </TD>
        <TD  class= title> 
          籍贯
        </TD>
        <TD  class= input>
          <Input name=NativePlace class="codeno" verify="籍贯|code:NativePlaceBak" id="NativePlaceBak" ondblclick="return showCodeList('NativePlaceBak',[this,NativePlaceName],[0,1]);" onkeyup="return showCodeListKey('NativePlaceBak',[this],[0,1]);"
          ><Input name=NativePlaceName class="codename"  >
          <!--Input name=NativePlace class="codeno" MAXLENGTH=2 id="NativePlaceBak" 
           ondblclick="return showCodeList('NativePlaceBak',[this,NativePlaceName],[0,1]);" 
           onkeyup="return showCodeListKey('NativePlaceBak',[this，NativePlaceName],[0,1]);"
           ><Input name=NativePlaceName class="codename"-->
        </TD>
      </TR>      
      <TR  class= common>        
        <TD  class= title>
          手机 
        </TD>
         <TD  class= input> 
          <Input name=Mobile class= common > 
        </TD>
        <TD  class= title>
          电话 
        </TD>
        <TD  class= input> 
          <Input name=Phone class= common > 
        </TD>
        <TD class= title>
          职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="codeno"  verify="营销员职级|code:AgentGrade" 
         ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
          onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
          ><Input name=AgentGradeName class="codename" > 
        </TD>
        <!--TD  class= title> 
          职级
        </TD>
        <TD  class= input>
          <Input name=AgentGrade class= common > 
        </TD-->
      </TR>
      <TR  class= common> 
        <TD  class= title>
          民族 
        </TD>
        <TD  class= input>
          <Input name=Nationality class="codeno" id="Nationality" 
           ondblclick="return showCodeList('Nationality',[this,NationalityName],[0,1]);" 
           onkeyup="return showCodeListKey('Nationality',[this,NationalityName],[0,1]);" 
          ><Input name=NationalityName class="codename"></TD>
        <TD  class= title>
          e_mail 
        </TD>
        <TD  class= input> 
          <Input name=EMail class= common > 
        </TD>
        <TD  class= title>
          培训期数 
        </TD>
        <TD  class= input> 
          <Input name=TrainPeriods class= common  > 
        </TD>
      </TR>
      <!--TR  class= common>
        <TD  class= title>
          是否吸烟标志 
        </TD>
        <TD  class= input> 
          <Input name=SmokeFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);"> 
        </TD>
        <TD  class= title>
          银行编码
        </TD>
        <TD  class= input> 
          <Input name=BankCode class=common > 
        </TD>
        <TD  class= title>
          银行账号 
        </TD>
        <TD  class= input> 
          <Input name=BankAccNo class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          从业年限 
        </TD>
        <TD  class= input> 
          <Input name=WorkAge class= common  > 
        </TD>
        <TD  class= title>
          原工作单位 
        </TD>
        <TD  class= input> 
          <Input name=OldCom class= common > 
        </TD>
        <TD  class= title> 
          原职业 
        </TD>
        <TD  class= input> 
          <Input name=OldOccupation class= common > 
        </TD>
      </TR-->
      <!--TR  class= common> 
        <TD  class= title>
          推荐代理人 
        </TD>
        <TD  class= input> 
          <Input name=RecommendAgent class= common  > 
        </TD>
        <TD  class= title> 
          工种/行业
        </TD>
        <TD  class= input> 
          <Input name=Business class=common > 
        </TD>
      </TR-->
      <TR  class= common> 
        <!--TD  class= title>
          信用等级 
        </TD>
        <TD  class= input> 
          <Input name=CreditGrade class=common > 
        </TD>
        <TD  class= title>
          销售资格 
        </TD>
        <TD  class= input> 
          <Input name=SaleQuaf class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD-->
        <TD class=title>服务专员类型</TD>
		<TD class=input><Input name=AgentType class='codeno'
			verify="服务专员类型"><Input
			class=codename name=AgentTypeName readOnly></TD>         
        <TD  class= title>
          入司时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' > 
        </TD>
        <TD  class= title>
          代理人状态 
        </TD>
        <TD  class= input> 
          <Input name=AgentState class='codeno'  maxlength=2 ondblclick="return  showCodeList('AgentState',[this,AgentStateName],[0,1]);" 
          onkeyup="return showCodeListKey('AgentState',[thisAgentStateName],[0,1]);" 
          ><Input name=AgentStateName class="codename" >
        </TD>
      </TR>
    	
      <!--TR  class= common> 
        <TD  class= title> 
          证书开始日期 
        </TD>
        <TD  class= input> 
          <Input name=QuafStartDate class="coolDatePicker" dateFormat="short" > 
        </TD>
        <TD  class= title>
          证书结束日期 
        </TD>
        <TD  class= input> 
          <Input name=QuafEndDate class="coolDatePicker" dateFormat="short" > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title> 
          展业证号码2
        </TD>
        <TD  class= input> 
          <Input name=DevNo2 class= common > 
        </TD>
        <TD  class= title>
          聘用合同号码 
        </TD>
        <TD  class= input> 
          <Input name=RetainContNo class= common > 
        </TD>
        <TD  class= title>
          代理人类别 
        </TD>
        <TD  class= input> 
          <Input name=AgentKind class='code' ondblclick="return showCodeList('AgentKind',[this]);" onkeyup="return showCodeListKey('AgentKind',[this]);"  > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          业务拓展级别
        </TD>
        <TD  class= input> 
          <Input name=DevGrade class=common > 
        </TD>
        <TD  class= title>
          内勤标志 
        </TD>
        <TD  class= input> 
          <Input name=InsideFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
        <TD  class= title>
          是否专职标志 
        </TD>
        <TD  class= input> 
          <Input name=FullTimeFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          是否有待业证标志 
        </TD>
        <TD  class= input> 
          <Input name=NoWorkFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
        </TD>
        <TD  class= title>
          培训日期 
        </TD>
        <TD  class= input> 
          <Input name=TrainDate class='coolDatePicker' dateFormat='short' > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          转正日期 
        </TD>
        <TD  class= input> 
          <Input name=InDueFormDate class='coolDatePicker' dateFormat='short' > 
        </TD>
        <TD  class= title>
          代理人状态 
        </TD>
        <TD  class= input> 
          <Input name=AgentState class='code'  maxlength=2 ondblclick="return showCodeList('AgentState',[this]);" onkeyup="return showCodeListKey('AgentState',[this]);" > 
        </TD>
        <TD  class= title>
          离司日期 
        </TD>
        <TD  class= input> 
          <Input name=OutWorkDate class='coolDatePicker' dateFormat='short' > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          保证金 
        </TD>
        <TD  class= input> 
          <Input name=AssuMoney class= common > 
        </TD>
        <TD  class= title>
          复核员
        </TD>
        <TD  class= input> 
          <Input name=Approver class= common > 
        </TD>
        <TD  class= title>
          复核日期
        </TD>
        <TD  class= input> 
          <Input name=ApproveDate class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          标志位
        </TD>
        <TD  class= input> 
          <Input name=QualiPassFlag class= common MAXLENGTH=1 > 
        </TD>
        <TD  class= title>
          备注
        </TD>
        <TD class=input> 
          <Input name=Remark class= common > 
        </TD>
        <TD  class= title>
          操作员代码 
        </TD>
        <TD  class= input> 
          <Input name=Operator class= common > 
        </TD>
      </TR-->
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <input type=hidden class=Common name=querySql > 
          <input type=hidden class=Common name=querySqlTitle > 
          <input type=hidden class=Common name=Title >
          
          <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();">
          <INPUT VALUE="下  载" class="cssbutton" TYPE=button onclick="ListExecl();"> 
          <INPUT VALUE="返  回" class="cssbutton" TYPE=button onclick="returnParent();"> 	
    </Div>      
          				
    <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 营销员结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage.lastPage();"> 						
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
