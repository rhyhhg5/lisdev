//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var mOperate="";
var tOrphanCode="N";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作

function submitForm()
{ 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.OrphanCode.value=tOrphanCode;
  fm.hideOperate.value=mOperate;
 
  fm.submit(); //提交
}

function AscripText()
{  
	var AgentCode=fm.all('AgentCode').value;
	var tSql="select '1' from laascription  where agentold=getAgentCode('"+AgentCode+"') ";
	tSql+="and validflag='N' and ascripstate<'3' ";
	var strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);
	if (!strQueryResult)
	{
	  var tSql1="select * from laorphanpolicy where  flag='1' and agentcode=getAgentCode('"+AgentCode+"') ";
	  strQueryResult  = easyQueryVer3(tSql1, 1, 0, 1);		
	}
	if (strQueryResult)
	{
		if (confirm("是否将孤儿单重新回归给此代理人？"))
    {
     tOrphanCode="Y";             
     return true; 
    }
    else
    {
     tOrphanCode= "N";
     return true;
    }
		}
	 return true;
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{   
	//下面增加相应的代码
	//mOperate="QUERY||MAIN";
	var tBranchType = fm.all('BranchType').value;
	var tBranchType2 = fm.all('BranchType2').value;
//	var tAgentState = fm.all('initAgentState').value;
	
   showInfo=window.open("./LAAgentModifyQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//alert(FlagStr);
	//alert(content);
	showInfo.close();

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    clearGrid();
  }
  mOperate = "";
}

function afterQuery(arrQueryResult)
{
		var arrResult = new Array();
//		resetForm();
		if( arrQueryResult != null )
		{   
			arrResult = arrQueryResult;
			fm.all('AgentCode').value = arrResult[0][0];
			fm.all('Name').value = arrResult[0][3];
			fm.all('AgentName').value = arrResult[0][3];
			fm.all('Sex').value = arrResult[0][4];
			fm.all('Birthday').value = arrResult[0][5];
			fm.all('IDNo').value = arrResult[0][6];
			fm.all('hideIdNo').value = arrResult[0][6];//身份证号校验时用
//			fm.all('Operator').value = arrResult[0][67];
			fm.all('IDNoType').value = arrResult[0][9];
			fm.all('OldManageCom').value = arrResult[0][2];
			fm.all('OldAgentGroup').value= arrResult[0][7];
			fm.all('hideAgentGroup').value = arrResult[0][7];
			fm.all('OldAgentGroupName').value= arrResult[0][8];
			fm.all('OldManageComName').value=arrResult[0][10];
			fm.all('hideAgentGrade').value=arrResult[0][12];
			fm.all('RecomAgentCode').value= arrResult[0][13];
			
			//显示业务员推荐人姓名
			var Sql_AgentCodeName="select name from laagent where groupagentcode ='"+fm.all('RecomAgentCode').value+"' ";
			var strQueryResult_AgentCodeName  = easyQueryVer3(Sql_AgentCodeName, 1, 1, 1);
			if (strQueryResult_AgentCodeName)
			{
				var arrName = decodeEasyQueryResult(strQueryResult_AgentCodeName);
				fm.all('RecomAgentCodeName').value= trim(arrName[0][0]) ;
			}
			
			showOneCodeNametoAfter('idtype','IDNoType','IDNoTypeName');
			fm.all('OldManageCom').value = arrResult[0][2];
//			fm.all('hideBranchCode').value = arrResult[0][83];
			
				//显式机构代码 行政信息
			var Sql_SexName="select codename from ldcode where codeType='sex' and code='"+fm.all('Sex').value+"' ";
			var strQueryResult_SexName  = easyQueryVer3(Sql_SexName, 1, 1, 1);
			if (strQueryResult_SexName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_SexName);
				fm.all('SexName').value= trim(arr[0][0]) ;
			}


				
		}//结果不为空
}





//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




function submitSave()
{   
	if(fm.all('AgentCode').value == null || fm.all('AgentCode').value=='')
	{
		alert("请先点击查询数据返回后进行操作！");
		return false;
	}
//	AscripText();
	if (!verifyInput())
    return false;
	if(!beforeSubmit())
		return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}
	var tDate = new Date();
    var tBirthDay = fm.all('Birthday').value;
	if(tBirthDay > tDate.getYear()+"-"+tDate.getMonth()+"-"+tDate.getDay())
	{
		alert("[出生日期]小于当前日期，无法录入！");
		return false;
	}
    submitForm();
}




function clearGrid()
{
   fm.all('AgentCode').value = '';
   fm.all('AgentCodeName').value = '';
   fm.all('RecomAgentCode').value = '';
   fm.all('RecomAgentCodeName').value = '';
   fm.all('ManageCom').value = '';
   fm.all('ManageComName').value = '';
   
}


	
	function changeRecomAgentCode()
	{   
		
		
		var newIntro=fm.all('RecomAgentCode').value;
		if(newIntro==''||newIntro==null)
		return true;
		
		var oldIntro=fm.all('AgentCode').value;
		if (oldIntro==newIntro)
		{
			alert('推荐人不能与被推荐人编码相同!');
			fm.all('RecomAgentCode').value = '';
			fm.all('RecomAgentCodeName').value = '';
			return false;
		}	
		var mangeCom=fm.all('OldManageCom').value;
		var strSQL1 = "";
		strSQL1 = "select name from laagent where 1=1 and groupagentcode='"+newIntro+"'"
		+" and branchtype='1' and branchtype2='01' and agentstate<'05'"
		+" and managecom='"+mangeCom+"'";
		var strQueryResult = easyQueryVer3(strSQL1, 1, 1, 1);
		if (!strQueryResult)
		{
			alert('推荐人不存在或不在该管理机构或已经离职！');
			fm.all('RecomAgentCode').value = '';
			fm.all('RecomAgentCodeName').value = '';
			return false;
	  }
	 	var arr = decodeEasyQueryResult(strQueryResult);
		fm.all('RecomAgentCodeName').value = arr[0][0];
		return true;
	}

	//提交前的校验、计算
	function beforeSubmit()
	{
		 
		if( verifyInput() == false ) return false;
		var strReturn = checkIdNo(trim(fm.all('IDNoType').value),trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value))
		if (strReturn != '')
			{
				alert('营销员'+strReturn);
				return false;
			}
		if (fm.all('IDNoType').value=='0')
		{
			var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value))
			if (strChkIdNo != "")
			{
				alert(strChkIdNo);
				return false;
			}
		}
			mOperate=fm.all('initOperate').value;
		  
			return true;
	}



