<%
//程序名称：LAAgentQueryInit.js
//程序功能：
//创建日期：2003-01-13 15:31:09
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {    
    /*
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value = '';
    fm.all('Password').value = '';
    fm.all('EntryNo').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('Birthday').value = '';
    fm.all('NativePlace').value = '';
    fm.all('Nationality').value = '';
    fm.all('Marriage').value = '';
    fm.all('CreditGrade').value = '';
    fm.all('HomeAddressCode').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('PostalAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('BP').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('MarriageDate').value = '';
    fm.all('IDNo').value = '';
    fm.all('Source').value = '';
    fm.all('BloodType').value = '';
    fm.all('PolityVisage').value = '';
    fm.all('Degree').value = '';
    fm.all('GraduateSchool').value = '';
    fm.all('Speciality').value = '';
    fm.all('PostTitle').value = '';
    fm.all('ForeignLevel').value = '';
    fm.all('WorkAge').value = '';
    fm.all('OldCom').value = '';
    fm.all('OldOccupation').value = '';
    fm.all('HeadShip').value = '';
    fm.all('RecommendAgent').value = '';
    fm.all('Business').value = '';
    fm.all('SaleQuaf').value = '';
    //fm.all('QuafNo').value = '';
    fm.all('QuafStartDate').value = '';
    fm.all('QuafEndDate').value = '';
    //fm.all('DevNo1').value = '';
    fm.all('DevNo2').value = '';
    fm.all('RetainContNo').value = '';
    fm.all('AgentKind').value = '';
    fm.all('DevGrade').value = '';
    fm.all('InsideFlag').value = '';
    fm.all('FullTimeFlag').value = '';
    fm.all('NoWorkFlag').value = '';
    fm.all('TrainDate').value = '';
    fm.all('EmployDate').value = '';
    fm.all('InDueFormDate').value = '';
    fm.all('OutWorkDate').value = '';
    fm.all('Approver').value = '';
    fm.all('ApproveDate').value = '';
    fm.all('AssuMoney').value = '';
    fm.all('AgentState').value = '';
    fm.all('QualiPassFlag').value = '';
    fm.all('SmokeFlag').value = '';
    fm.all('RgtAddress').value = '';
    fm.all('BankCode').value = '';
    fm.all('BankAccNo').value = '';
    fm.all('Remark').value = '';
    fm.all('Operator').value = '';*/
     fm.all('BranchType').value = '<%=BranchType%>';  
    fm.all('BranchType2').value = '<%=BranchType2%>';
  }
  catch(ex)
  {
    alert("在LAAgentQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAAgentQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initAgentGrid();
  }
  catch(re)
  {
    alert("LAAgentQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentGrid
 ************************************************************
 */
function initAgentGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="营销员代码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="销售机构";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="管理机构";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="姓名";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="职级";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="证件号";         //列名
        iArray[6][1]="120px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="营销员状态代码";         //列名
        iArray[7][1]="0px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[8]=new Array();
        iArray[8][0]="营销员状态";         //列名
        iArray[8][1]="80px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许 
        
        iArray[9]=new Array();
        iArray[9][0]="是否特殊人员";         //列名
        iArray[9][1]="80px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许  
        
        iArray[10]=new Array();
        iArray[10][0]="员工内部核心编码";         //列名
        iArray[10][1]="80px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=3;         //是否允许录入，0--不能，1--允许 
         
        AgentGrid = new MulLineEnter( "fm" , "AgentGrid" ); 
        
        

        //这些属性必须在loadMulLine前
        AgentGrid.mulLineCount = 0;   
        AgentGrid.displayTitle = 1;
        AgentGrid.canSel=1;
        AgentGrid.canChk=0;
        AgentGrid.locked=1;
        AgentGrid.hiddenPlus = 1;
      	AgentGrid.hiddenSubtraction = 1;

        AgentGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化AgentGrid时出错："+ ex);
      }
    }


</script>