<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.agent.*"%>

<%
  String FlagStr = "";
  String Content = "";
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");

  Calendar cal = new GregorianCalendar();
  String year = String.valueOf(cal.get(Calendar.YEAR));
  String month=String.valueOf(cal.get(Calendar.MONTH)+1);
  String date=String.valueOf(cal.get(Calendar.DATE));
  String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY)); 
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String now = year + month + date + hour + min + sec + "_" ;
  
  String  millis = String.valueOf(System.currentTimeMillis());  
  String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
  tTransferData.setNameAndValue("querySql",request.getParameter("querySql"));
  tTransferData.setNameAndValue("querySqlTitle",request.getParameter("querySqlTitle"));
  tTransferData.setNameAndValue("Title",request.getParameter("Title"));
  try
  {
      VData vData = new VData();
  	  vData.add(tG);
  	  vData.add(tTransferData);
  	  LAComBusinessPrintUI tLAComBusinessPrintUI = new LAComBusinessPrintUI();
      if (!tLAComBusinessPrintUI.submitData(vData, ""))
      {
          Content = "报表下载失败，原因是:" + tLAComBusinessPrintUI.mErrors.getFirstError();
          FlagStr = "Fail";
      }
	  else
	  {
	 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
	 	  File file = new File(tOutXmlPath);
	 	  
	        response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[10000];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } 
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
              file.delete();
          }
	   }
	}
	catch(Exception ex)
	{
	  ex.printStackTrace();
	}
  
  if (!FlagStr.equals("Fail"))
  {
  	Content = "";
  	FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>