//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var mOperate="";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作

function submitForm()
{
  if (verifyInput() == false)
  return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//alert(FlagStr);
	//alert(content);
	showInfo.close();

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    clearGrid();
  }
  mOperate = "";
}

//***************************************************
//* 验证对应业务员代码的名字和销售机构
//***************************************************
function getAgentMs()
{
 if (fm.all('AgentCode').value==null || fm.all('AgentCode').value=="")
 { 
 	alert("请先录入业务员代码");
 	fm.all('AgentCode').value="";
 	return;
}
 var strSQL = "";
 var tAgentCode=fm.all('AgentCode').value;
 
 if (tAgentCode!='' )
 {	
     strSQL = "select a.name,b.branchattr,b.name,a.managecom,a.employdate,c.introagency,(select name from laagent where agentcode=c.introagency) ,c.agentgrade,c.agentgroup,a.noworkflag,a.traindate from laagent a,labranchgroup b,latree c  where a.agentstate<='02' and a.agentgroup=b.agentgroup  and a.agentcode=c.agentcode and a.branchtype='1'  and a.branchtype2='01' "
     + getWherePart('a.AgentCode','AgentCode') ;   
     var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
     
  //判断是否查询成功
   if (!strQueryResult) 
   {
   alert("此业务员状态非在职状态或该人员不存在");
   fm.all('AgentCode').value = '';
   fm.all('AgentName').value = '';
   fm.all('BranchAttr').value = '';
   fm.all('BranchAttrName').value = '';
   fm.all('ManageCom').value = '';
   fm.all('IndueFormDate').value = '';
   fm.all('IntroOldAgency').value = '';
   fm.all('IntroOldAgencyName').value = '';
   fm.all('AgentGrade').value = '';
   fm.all('AgentGroup').value = '';
   fm.all('NoWorkFlag').value = '';
   fm.all('TrainDate').value = '';
   
   return;
    }
  }
 
else
  {
   fm.all('AgentCode').value = '';
   fm.all('AgentName').value = '';
   fm.all('BranchAttr').value = '';
   fm.all('BranchAttrName').value = '';
   fm.all('ManageCom').value = '';
   fm.all('IndueFormDate').value = '';
   fm.all('IntroOldAgency').value = '';
   fm.all('IntroOldAgencyName').value = '';
   fm.all('AgentGrade').value = '';
   fm.all('AgentGroup').value = '';
   fm.all('NoWorkFlag').value = '';
   fm.all('TrainDate').value = '';
     return;
  }
  
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
   fm.all('AgentName').value = tArr[0][0];
   fm.all('BranchAttr').value = tArr[0][1];	
   fm.all('BranchAttrName').value =tArr[0][2];
   fm.all('ManageCom').value = tArr[0][3];
   fm.all('IndueFormDate').value = tArr[0][4];
   fm.all('IntroOldAgency').value =tArr[0][5];
   fm.all('IntroOldAgencyName').value = tArr[0][6];
   fm.all('AgentGrade').value = tArr[0][7];
   fm.all('AgentGroup').value = tArr[0][8];
   fm.all('NoWorkFlag').value = tArr[0][9];
   fm.all('TrainDate').value = tArr[0][10];
}


//提交前的校验、计算  有待修改
function beforeSubmit()
{
	return true ;	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




function submitSave()
{   
	
  if((trim(fm.all('AgentCode').value)=="")||(fm.all('AgentCode').value==null)) 
   {
      alert("请输入修改新入司人员代码！");	
      return false;
   }  
  

    if (!verifyInput())
    return false;
      
      
     
	if(beforeSubmit()==false)  
	return false;
   submitForm();
     
}
	function changeIntroAgency()
	{
		var newIntro=fm.all('IntroNewAgency').value;
		if(newIntro==''||newIntro==null)
		return true;
		
		var oldIntro=fm.all('IntroOldAgency').value;
		if (oldIntro==newIntro)
		{
			alert('新推荐人不能与原推荐人编码相同!')
			fm.all('IntroNewAgency').value = '';
			fm.all('IntroNewAgencyName').value = '';
			return false;
		}	
		var mangeCom=fm.all('ManageCom').value;	
		var strSQL1 = "";
		strSQL1 = "select name from laagent where 1=1 and agentcode='"+newIntro+"'"
		+" and branchtype='1' and branchtype2='01' and agentstate<'03'"
		+" and managecom='"+mangeCom+"'";
		var strQueryResult = easyQueryVer3(strSQL1, 1, 1, 1);
		if (!strQueryResult)
		{
			alert('不存在该业务员！');
			fm.all('IntroAgency').value = '';
			return false;
	  }
	 	var arr = decodeEasyQueryResult(strQueryResult);
		fm.all('IntroNewAgencyName').value = arr[0][0];
		return true;
	}
	
	





