<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LATrainerWageConfirmSave.jsp
//程序功能：
//创建日期：2018-06-7
//创建人  ： WangQingMin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>  
 <%@page import="com.sinosoft.lis.agentwages.*"%>  
<%
// 输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆"; 
  }
  else //页面有效
  {
  System.out.print("进来了");
  String tWageNo = request.getParameter("WageNo");
  String tBranchType = request.getParameter("BranchType");
  String tBranchType2=request.getParameter("BranchType2");
  LATrainerWageConfirmUI tLATrainerWageConfirmUI =new LATrainerWageConfirmUI();
  VData tVData =new VData();
  tVData.add(request.getParameter("ManageCom"));
  tVData.add(tBranchType);
  tVData.add(tBranchType2);
  tVData.add(tWageNo);
  tVData.add(tGI);
  tLATrainerWageConfirmUI.submitData(tVData);
  tError = tLATrainerWageConfirmUI.mErrors; 
   

   //如果在Catch中发现异常，则不从错误类中提取错误信息
   try
   {
     if (!tError.needDealError())
     {                          
       Content = " 保存成功! ";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content = " 失败，原因:" + tError.getFirstError();
       FlagStr = "Fail";
     }
   } 
   catch(Exception ex)
   {
     Content = " 失败，原因:" + ex.toString();
     FlagStr = "Fail";    
   }    
   System.out.println(Content);                  
}//页面有效区

%>                      
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

