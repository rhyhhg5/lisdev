<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  
  String tTitleAgent="";
  String tTitleWage = "";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
    tTitleWage  = "佣金";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
    tTitleWage  = "薪资";
  }
%>
<head >
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LADimissionInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LADimissionInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LADimissionSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="./AgentOp1.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLADimission1);">
    
    <td class=titleImg>
      <%=tTitleAgent%>离职信息
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLADimission1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  <%=tTitleAgent%>代码 
		</td>
        <td  class= input> 
		  <input class= common name=AgentCode verify="<%=tTitleAgent%>代码 |notnull" onchange="return checkValid();" type=hidden>
		  <input class= common name=GroupAgentCode verify="<%=tTitleAgent%>代码 |notnull" onchange="return checkValid();" elementtype=nacessary>  
		</td>
        <td  class= title> 
		   <%=tTitleAgent%>姓名 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=AgentName > 
		</td>
      </tr>
      <tr  class= common> 
         <td  class= title> 
		  离职次数 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=DepartTimes > 
		</td>
        <td  class= title> 
		  离职原因 
		</td>
        <td  class= input> 
		  <input name=DepartRsn class='codeno' verify="离职原因|notnull&code:DepartRsn"
		   ondblclick="return showCodeList('DepartRsn',[this,DepartRsnName],[0,1]);" 
		   onkeyup="return showCodeListKey('DepartRsn',[this,DepartRsnName],[0,1]);" 
		   ><Input class=codename name=DepartRsnName readOnly elementtype=nacessary> 
		</td>
      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  离职申请日期 
		</td>
        <td  class= input> 
		 <Input class='coolDatePicker' name=ApplyDate dateFormat='short' verify="预离职日期|DATE">
		</td>
        <td  class= title> 
		  离职日期
		</td>
        <td  class= input> 
		  <Input class='coolDatePicker' name=DepartDate dateFormat='short' verify="离职日期|notnull&DATE" elementtype=nacessary> 
		</td>
      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  资格证返还
		</td>
        <td  class= input> 
		  <input name=QualityDestFlag class='codeno' verify="资格证销毁标记|code:yesno" 
		   ondblclick="return showCodeList('yesno',[this,QualityDestFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,QualityDestFlagName],[0,1]);" 
		   ><Input class=codename name=QualityDestFlagName readOnly> 
		</td>
        <td  class= title> 
		  展业证回收标志
		</td>
        <td  class= input> 
		  <input name=PbcFlag class='codeno' verify="展业证回收标志|code:yesno" 
		   ondblclick="return showCodeList('yesno',[this,PbcFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,PbcFlagName],[0,1]);" 
		   ><Input class=codename name=PbcFlagName readOnly> 
		</td>
      
      </tr>
      <tr  class= common> 
        <!--td  class= title> 
		  保险费暂收据回收标志
		</td>
        <td  class= input> 
		  <input name=ReceiptFlag class='code' verify="保险费暂收据回收标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
		</td-->
        <!--td  class= title> 
		  丢失标志
		</td>
        <td  class= input> 
		  <input name=LostFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" > 
		</td-->
	 <td  class= title> 
		 单证回收标记
		</td>
        <td  class= input> 
		  <input name=DestoryFlag class='codeno' verify="保证金领取标记|code:yesno" 
		   ondblclick="return showCodeList('yesno',[this,DestoryFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,DestoryFlagName],[0,1]);"
		   ><Input class=codename name=DestoryFlagName readOnly> 
		</td>	
		
        <td  class= title> 
		  保证金领取标记
		</td>
        <td  class= input> 
		  <input name=ReturnFlag class='codeno' verify="保证金领取标记|code:yesno" 
		   ondblclick="return showCodeList('yesno',[this,ReturnFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,ReturnFlagName],[0,1]);"
		   ><Input class=codename name=ReturnFlagName readOnly> 
		</td>
		
      </tr>
      <tr  class= common>
      
        <td  class= title>
		  养老金领取标记
		</td>
        <td  class= input>
		  <input name=AnnuityFlag class='codeno' verify="养老金领取标记|code:yesno" 
		   ondblclick="return showCodeList('yesno',[this,AnnuityFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,AnnuityFlagName],[0,1]);"
		   ><Input class=codename name=AnnuityFlagName readOnly>
		</td>
        <td  class= title>
		   离司回访通过标记
		</td>
        <td  class= input>
		  <input name=VisitFlag class='codeno' verify="离司回访通过标记|code:yesno" 
		   ondblclick="return showCodeList('yesno',[this,VisitFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,VisitFlagName],[0,1]);"
		   ><Input class=codename name=VisitFlagName readOnly>
		</td>
		
      </tr>
      <tr  class= common>
         <td  class= title>
		  黑名单标记
		</td>
        <td  class= input>
		  <input name=BlackFlag class='codeno' verify="黑名单标记|code:yesno" 
		   ondblclick="return showCodeList('yesno',[this,BlackFlagName],[0,1]);" 
		   onkeyup="return showCodeListKey('yesno',[this,BlackFlagName],[0,1]);"
		   ><Input class=codename name=BlackFlagName readOnly>
		</td>
        <td  class= title>
		   黑名单原因
		</td>
        <td  class= input>
		  <input name=BlackListRsn class="common">
      </tr>  
      <tr>
        <td  class= title>
		      是否欠费
  		  </td>
        <td  class= input>
		      <input name=OweMoney readonly class="readonly">
		    </td>
        <td  class= title>
		      离职状态
  		  </td>
        <td  class= input>
		      <input name=DepartState readonly class="readonly">
		    </td>
      </tr>
      
       <TR>
    <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
     <Input class= 'readonly' readonly   name=Operator >
    </TD>
    
    <TD  class= title>
      操作时间
    </TD>
    <TD  class= input>
     <Input class= 'readonly' readonly   name=ModifyDate >
    </TD>
   </TR>
    </table>
  </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=ReceiptFlag>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
   <input type=hidden name=CurrDate value=''>
    <input type=hidden class=Common name=querySql > 
    <!--INPUT VALUE="查询佣金信息" TYPE=button onclick="easyQueryClick();" class="cssButton"-->
    <!--INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton"-->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDimissionGrid);">
    		</td>
    		<td class= titleImg>
    			 <%=tTitleWage%>信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divDimissionGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentWageGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton"> 				
  	</div>
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			业务员未签单或未回执回销保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();" class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();" class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();" class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();" class="cssButton">
      <INPUT VALUE="下  载" TYPE=button onclick="doDownload();" class="cssButton"> 				 				
  	</div>
  	<%
  	if(BranchType!=null&&BranchType.equals("1")){
  	%>
    <p><font color="red">注：对于失效中止可复效保单不在列表里显示，但会在下载中显示。<br/>&nbsp;&nbsp;&nbsp;&nbsp;如果要对保单作签单或回执回销的操作，请与当地业管部门联系.</font></p>
    <%
  	}else if(BranchType!=null&&BranchType.equals("2")){
  	%>
  	<p><font color="red">注：有未签单或未回执回销的保单，允许离职登记，不允许离职确认。<br/>&nbsp;&nbsp;&nbsp;&nbsp;如果要对保单作签单或回执回销的操作，请与当地业管部门联系.</font></p>
  	<%}%>
  	<div id ="contnoyincang">
  	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			业务员销售的所有保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanSaleContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();" class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();" class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();" class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();" class="cssButton">
      <INPUT VALUE="下  载" TYPE=button onclick="doDownload();" class="cssButton"> 				 				
  	</div>
  	</div>
  	<%
  	if(BranchType!=null&&BranchType.equals("1")){
  	%>
    <p><font color="red">注：自2015年4月1号之后,离职的销售员，其下的保单均标记为孤儿单。</font></p>
    <%
  	}
  	%>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
