//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var tGrantDate="";
var tStartValidDate="";
var tInvalidDate="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦 
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if(!checkdate()){
  	return false;
  }
  
 if (!beforeSubmit())
  return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "INSERT||MAIN";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	 document.all.AgentCode.readOnly=false;
	 document.all.CertifNo.readOnly=false;
	initInpBox();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LACertification.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value==null))
  {
  	alert("请输入营销员代码！");
  	fm.all('AgentCode').focus();
  	return false;
  }	  
  if ((fm.all('CertifNo').value == '')||(fm.all('CertifNo').value==null))
  {
  	alert("请输入展业证书号！");
  	fm.all('CertifNo').focus();
  	return false;
  }  
  if ((fm.all('AuthorUnit').value == '')||(fm.all('AuthorUnit').value==null))
  {
  	alert("请输入批准单位！");
  	fm.all('AuthorUnit').focus();
  	return false;
  }
  if (!verifyInput()) return false;
/*   if ((fm.all('GrantDate').value == '')||(fm.all('GrantDate').value==null))
  {
  	alert("请输入发放日期！");
  	fm.all('GrantDate').focus();
  	return false;
  }
  if ((fm.all('StartValidDate').value == '')||(fm.all('StartValidDate').value==null))
  {
  	alert("请输入起始有效期！");
  	fm.all('StartValidDate').focus();
  	return false;
  }
  if ((fm.all('EndValidDate').value == '')||(fm.all('EndValidDate').value==null))
  {
  	alert("请输入截至有效日期！");
  	fm.all('EndValidDate').focus();
  	return false;
  }
*/  
  if ((fm.all('CertiState').value == '')||(fm.all('CertiState').value==null))
  {
  	alert("请输入展业证书状态！");
  	fm.all('CertiState').focus();
  	return false;
  }

  if (fm.all('CertiState').value=='1'&& fm.all('CertiState1').value=='0'&& trim(fm.all('InvalidRsn').value)=='')
  {
  	alert("请录入失效原因!")  ;
  	fm.all('InvalidRsn').focus();
  	return false;
	}
  if (trim(fm.all('InvalidDate').value)!='' &&  trim(fm.all('InvalidDate').value)<trim(fm.all('StartValidDate').value))
  {
  	alert("失效日期不能小于起始有效起期!")  ;
  	fm.all('InvalidDate').focus();
  	return false;
	}			
	return true;
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  fm.fmtransact.value = "UPDATE||MAIN";
   if(fm.all('CertiState').value =='0')
   {
     if(fm.all('InvalidDate').value!="")
     {
       alert("展业证状态为有效时，页面不能录入相关失效日期");
       fm.all('InvalidDate').value="";
       fm.all('InvalidDate').focus();
     	return false;
     }
     if(fm.all('InvalidRsn').value!="")
     {
       alert("展业证状态为有效时，页面不能录入相关失效原因");
       fm.all('InvalidRsn').value="";
       fm.all('InvalidRsn').focus();
     	return false;
     }
   }
  if (!beforeSubmit())
  {
  	return false;
  }
  
    var i = 0;                                                                                                        
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";                                         
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;                                         
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fdisable();
    fm.submit(); //提交
  }
    else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  showInfo=window.open("./LAContinueCertificationQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{	
		arrResult = arrQueryResult;
        fm.all('GroupAgentCode').value= arrResult[0][0];
        fm.all('CertifNo').value= arrResult[0][1];
        fm.all('CertifNoQuery').value= arrResult[0][1];
        fm.all('AuthorUnit').value= arrResult[0][2];
        tGrantDate=fm.all('GrantDate').value= arrResult[0][3];
        tInvalidDate=fm.all('InvalidDate').value= arrResult[0][7];
        fm.all('InvalidRsn').value= arrResult[0][8];
        tStartValidDate=fm.all('StartValidDate').value= arrResult[0][4];
        fm.all('EndValidDate').value= arrResult[0][5];       
        var StringName=fm.all('CertiStateName').value= arrResult[0][6];
        fm.all('CertiState1').value=arrResult[0][6];
        fm.all('Operator').value= arrResult[0][9];
        fm.all('ModifyDate').value=arrResult[0][10];
        fm.all('CertiState').value=arrResult[0][11];
        document.all.AgentCode.readOnly="true";
        checkvalid();
        tdisable();
	}
}               
        
function checkvalid()
{
	var tSql = "";
	
	//判断有无业务员代码
	if (getWherePart('GroupAgentCode')=='')
  {
    document.fm.AgentName.value  = "";
    document.fm.GroupAgentCode.value = "";
    document.fm.AgentCode.value = "";
   	return false;
  }
  
  tSql  = "SELECT";
  tSql += "    A.Name,";
  tSql += "    A.ManageCom,";
  tSql += "    B.BranchAttr,";
  tSql += "    A.AgentCode";
  tSql += "  FROM";
  tSql += "    LAAgent A,";
  tSql += "    LABranchGroup B";
  tSql += " WHERE 1=1 AND";
  tSql += "    A.AgentGroup = B.AgentGroup";
  tSql += getWherePart('A.GroupAgentCode','GroupAgentCode');
  
  var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
  
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    document.fm.AgentName.value  = "";
    document.fm.GroupAgentCode.value = "";
    document.fm.AgentCode.value = "";
    return;
  }
  
  //查询成功则拆分字符串，返回二维数组
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  document.fm.AgentName.value = tArr[0][0];
  document.fm.AgentCode.value = tArr[0][3];
}

//  不能变更
function tdisable(){
	    fm.all('AgentCode').disabled=true;
		fm.all('CertifNo').disabled=true;
}

//  可以变更
function fdisable(){
	    fm.all('AgentCode').disabled=false;
		fm.all('CertifNo').disabled=false;
		fm.all('AuthorUnit').disabled=false;
		fm.all('GrantDate').disabled=false;
		fm.all('StartValidDate').disabled=false;
		fm.all('InvalidDate').disabled=false;
		fm.all('InvalidRsn').disabled=false;
}
//校验是否 录入的有效区间与原有记录重复
function checkdate(){
	 
//  var tSql =   "select * from LACertification"
//  tSql +=   " WHERE 1=1 and";
//  tSql +=  "(('"+fm.all('StartValidDate').value+"'<=Validend and '"+fm.all('StartValidDate').value+"'>=ValidStart) or('"+fm.all('EndValidDate').value+"'<=Validend and '"+fm.all('EndValidDate').value+"'>=ValidStart))";
//  tSql +=  getWherePart('AgentCode');
//	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
//	if (!strQueryResult)
//	{
//     return true;
//    }else{
//     	alert('录入的有效区间与原有记录重复');
//  	    return false;
//  }
	return true;
}

////去掉字符的前后空格和字符的全角变成半角
//function trim1(str){
//	
//    var DBCStr = "";    
//    for(var i = 0; i < str.length; i++){     
//            var c = str.charCodeAt(i);     
//            if(c == 12288){     
//                    DBCStr += String.fromCharCode(32);     
//                    continue;     
//            }     
//            if(c > 65280 && c < 65375){     
//                    DBCStr += String.fromCharCode(c - 65248);     
//                    continue;     
//            }     
//            DBCStr += String.fromCharCode(c);     
//    }     
//    DBCStr = DBCStr.replace(/(^\s*)|(\s*$)/g, "");
//    fm.all('CertifNo').value =DBCStr;
//}

function checkInput(str)
{	
	fm.all('CertifNo').value = str.replace(/(^[\s\u3000]*)|([\s\u3000]*$)/g, "");
}















