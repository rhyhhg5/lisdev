//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if(mOperate=="")
	{
		addClick();
	}
	//alert(mOperate);
  if (!beforeSubmit())
    return false;
    
  //by gzh 20110329
  if(!checkMac()){
  	return false;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	

	mOperate="";
	//alert(mOperate);
 //initForm();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    //alert(content);
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

 //   showDiv(operateButton,"true"); 
 //   showDiv(inputButton,"false"); 
    //执行下一步操作
  }  
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
	  
	  initForm();
	  document.getElementById('LoginName').readOnly=false;
      document.getElementById('LoginName').className='common';
  }
  catch(re)
  {
  	alert("LAComPersonal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

function nameIsExsist()
{
	var strSQL = "";
	strSQL = "select a.loginname from lacompersonal a where a.loginname='"+ trim(fm.all('LoginName').value)+"'";
	var strQueryResult = easyQueryVer3(strSQL,1,1,1);
	if(strQueryResult)
	{
	 	alert('该登录名称已经存在！')
	 	return false;
	}
	return true;
}

function beforeSubmit()
{
  if (!verifyInput()) return false;
 // alert(12);
  var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value))
  // alert(13);
//  if (strChkIdNo != "")
//  {
//    alert(strChkIdNo);
//    return false;           xianchun  2005-12-19 不需要录入个人信息
//  }
   // alert(14);xianchun
 //   alert(fm.all('PasswordConfirm').value);
 
 if(fm.all('UserType').value == "01" && fm.all('AgentCom').value == "")
	{
		alert("请选择代理机构信息！"); 
        return ;
	}
	
	if(fm.all('UserType').value == "02" && fm.all('AgentId').value == "")
	{
		alert("请选择业务员代码！"); 
        return ;
	}
	
  if(trim(fm.all('PasswordConfirm').value)!=trim(fm.all('Password').value))
  {
  	alert("航意险口令与确认口令不相同!");
  	return false ;
  }
  //if (!nameIsExsist()) {
	//return false;
      //}
  return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  //by gzh 20110329
  if(!checkMac()){
  	return false;
  }
  
  if ((fm.all("AgentCode").value==null)||(trim(fm.all("AgentCode").value)==''))
    alert("请确定要修改的人员！");
  else
  { 
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
   //   alert(1);
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LAComPersonalQueryHtml.jsp");
} 

//Click事件，当点击“查询”图片时触发该函数
function queryComClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LAComPersonalQueryComHtml.jsp");
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCode").value==null)||(trim(fm.all("AgentCode").value)==''))
    alert("请确定要删除的记录！");
  else
  {
    if (confirm("您确实想删除该记录吗?"))
    {
      mOperate="DELETE||MAIN";  
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

  function changeIDNo()
  {
     if (getWherePart('IDNo')=='')
     {
       return false;
}
     var strSQL = "";
     strSQL = "select * from lacompersonal where 1=1 "
 + getWherePart('IDNo');
       //alert(strSQL);
     var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
     if (strQueryResult)
     {
      alert('该身份证号已存在!');
      fm.all('IDNo').value = '';
      return false;
     }
     return true;
  }
 function checkcom()
 {
     var strSQL = "";
     strSQL = "select name from lacom where 1=1 "
 + getWherePart('AgentCom');
       //alert(strSQL);
     var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
     if (!strQueryResult)
     {
      alert('不存在该中介机构!');
      fm.all('AgentCom').value = '';
      fm.all('AgentComName').value = '';
      
      return false;
     }
     var arr = decodeEasyQueryResult(strQueryResult);
     fm.all('AgentComName').value=arr[0][0];
     return true;	
 	
  }
function afterQuery(arrQueryResult)
{	
	//alert(1);
      var arrResult = new Array();

      resetForm();
      if( arrQueryResult != null )
      {

      arrResult = arrQueryResult;
      fm.all('AgentCode').value = arrResult[0][0];
      fm.all('Name').value = arrResult[0][1];
      fm.all('AgentCom').value = arrResult[0][2];
      fm.all('AgentComName').value = arrResult[0][3];
      fm.all('IDNo').value = arrResult[0][4];
    
      
      //fm.all('Password').value = arrResult[0][5];
      fm.all('LoginName').value = arrResult[0][6];
      fm.all('Sex').value = arrResult[0][7];
      fm.all('HeadShip').value = arrResult[0][8];  
      fm.all('Birthday').value = arrResult[0][9];
      fm.all('HomeAddress').value = arrResult[0][10];
      fm.all('Phone').value = arrResult[0][11];
      
      fm.all('EmployDate').value = arrResult[0][12];
      fm.all('OutWorkDate').value = arrResult[0][13];
      fm.all('Operator').value = arrResult[0][14];
      fm.all('Noti').value = arrResult[0][15];
      fm.all('Mobile').value = arrResult[0][16];
      fm.all('ManageCom').value = arrResult[0][17];
      
      fm.all('UserType').value = arrResult[0][19];
      fm.all('RuleState').value = arrResult[0][20];
      //AgentComTitleID
      fm.all('AgentId').value = arrResult[0][21];
      fm.all('Mac_id').value = arrResult[0][22];
      fm.all('Mac_flag').value = arrResult[0][23];
      if(fm.all('Mac_flag').value == "1"){
      	fm.all('backFlagName').value = "是";
      }else if(fm.all('Mac_flag').value == "0"){
      	fm.all('backFlagName').value = "否";
      }
      //alert(arrResult[0][21]);
      
      document.getElementById('LoginName').readOnly=true;
      document.getElementById('LoginName').className='readonly';
      
      //判断用户类型
      afterCodeSelect1(arrResult[0][19]);
      //alert(arrResult[0][19].value=='01');
      //showAllCodeName();
      
      showOneCodeNametoAfter('sex','Sex','SexName');
      showOneCodeNametoAfter('usertype','UserType','UserTypeName');
      showOneCodeNametoAfter('agentcom','AgentCom','AgentComName');

      //alert(arrResult[0][18].value);
      showOneCodeNametoAfter('rulestate','RuleState','RuleStateName');
       
      	    //personalcode=0
	        //name=1
	        //agentcom=2
	        //b.name=3//''=3
	        //idno=4
	        //password=5
	        //loginname=6
	        //sex=7
	        //headship=8
	        //birthday=9
	        //homeaddress=10
	        //phone=11
	        //employdate=12
	        //outworkdate=13
	        //operator=14
	        //noti=15
	        //mobile=16
	        //managecom=17
      	    //usertype=20
	        //rulestate=21
   	}
}
      
  function afterQueryCom(arrQueryResult)
  {	
  	//alert(1);
        var arrResult = new Array();

       
        if( arrQueryResult != null )
        {
        arrResult = arrQueryResult;
        fm.all('AgentCom').value = arrResult[0][0];
        fm.all('AgentComName').value = arrResult[0][2];
     	}
  }
  
function afterCodeSelect( cCodeName, Field )
{	 	
    if(cCodeName=="UserType")
	{
	//alert('here');
	  if(Field.value == "01" )
	  {
	  //alert('here');
	    fm.all("AgentComTitleID").style.display = "";
	    fm.all("AgentComInputID").style.display = "";
	    fm.all("AgentTitleID").style.display = "none";
	    fm.all("AgentInputID").style.display = "none";
	  }
	  else
	  {
	    fm.all("AgentComTitleID").style.display = "none";
	    fm.all("AgentComInputID").style.display = "none";
	    fm.all("AgentTitleID").style.display = "";
	    fm.all("AgentInputID").style.display = "";
	  }
	  
	  fm.AgentCom.value = "";
	  fm.AgentComName.value = "";
	  fm.AgentId.value = "";
	}
}  

//用于判断传回的用户类型

function afterCodeSelect1(  Field )
{	 	

	  if(Field == "01" )
	  {
	  //alert('here');
	    fm.all("AgentComTitleID").style.display = "";
	    fm.all("AgentComInputID").style.display = "";
	    fm.all("AgentTitleID").style.display = "none";
	    fm.all("AgentInputID").style.display = "none";
	  }
	  else
	  {
	    fm.all("AgentComTitleID").style.display = "none";
	    fm.all("AgentComInputID").style.display = "none";
	    fm.all("AgentTitleID").style.display = "";
	    fm.all("AgentInputID").style.display = "";
	  }
	 
}  

function queryAgent()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！"); 
        return ;
    }

    if(fm.all('AgentId').value == "")
    {  

        //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=1","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        
        //var tSaleChnl = (fm.SaleChnl != null && fm.SaleChnl != "undefined") ? fm.SaleChnl.value : "";
        //var tAgentCom = (fm.AgentCom != null && fm.AgentCom != "undefined") ? fm.AgentCom.value : "";
        var tBranchType = 2;
        // 暂定为团险直销人员
        //------------------------------------
        //在查询页面限制渠道 modify by zhuxt 20140911
        //功能 #2171   modify by yangyang 20141203  增加特殊标记
        var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
//            + "&SaleChnl=" + "02"
//            + "&branchtype=" + tBranchType;
        + "&specFlag=sale2171";
        var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('AgentId').value != "")
    {
        var cAgentId = fm.AgentId.value;  //保单号码	
        var strSql = "select GroupAgentCode,Name from LAAgent where GroupAgentCode='" + cAgentId +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            alert("代码为:[" + fm.all('AgentId').value + "]的业务员不存在，请确认!");
        }
    }
}

function queryAgent2()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	if(fm.all('AgentId').value != "" && fm.all('AgentId').value.length==10 )	 {
	var cAgentId = fm.AgentId.value;  //保单号码	
	var strSql = "select GroupAgentCode,Name from LAAgent where GroupAgentCode='" + cAgentId +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    }
    else{
     alert("代码为:["+fm.all('AgentId').value+"]的业务员不存在，请确认!");
     }
	}	
} 
  
  
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  //	alert("arrResult[0][0]_____"+arrResult[0][0]);
  	var strSql = "select groupagentcode from LAAgent where AgentCode='" + arrResult[0][0] +"'";
    var codearrResult = easyExecSql(strSql);
  //  alert("codearrResult[0][0]_____"+codearrResult[0][0]);
  	fm.AgentId.value = codearrResult[0][0];//fm.all('AgentCodeName').value = easyExecSql("select name from laagent where agentcode='"+arrSelected[0][1]+"'");
  }
}

//by gzh 20110329 当选中“使用物理地址时，物理地址不能为空”
function checkMac(){
	if(fm.Mac_flag.value == "1" ){
		if(fm.Mac_id.value == "" || fm.Mac_id.value == null){
			alert("选择使用物理地址校验时，请录入物理地址！");
			fm.Mac_id.focus();
			return  false;
		}
	}
	return true;
}