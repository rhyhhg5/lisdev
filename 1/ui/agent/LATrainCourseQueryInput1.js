//               该文件中包含客户端需要处理的函数和事件

var arrDataSet = new Array(); 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//修改人：解青青  时间：2014-11-23
function checkAgentCode()
{
//   var sql = "select * from laagent where agentstate<='02' and branchtype='1' and branchtype2='01'";
   var sql=" select agentcode  from laagent   where  1=1" 
             + getWherePart("groupagentcode","GroupAgentCode")
//   		"groupagentcode='"+fm.GroupAgentCode.value+"' "
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
    
}





/*****************************
  * 注: 提交前的校验、计算  
  ****************************/
function beforeSubmit()
{
  //添加操作	
}           
/*****************************
  * 注:显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示 
  ****************************/
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

/******************************************
 * 注: 查询得到返回值
 ******************************************/

/******************************************
 * 注: 返回主界面
 ******************************************/


/******************************************
 * 注: 查询操作
 ******************************************/
function easyQueryClick()
{
     // 提前校验
	 if (!verifyInput()) return false ;
	 
     // 初始化表格
     initSetGrid();

  // 书写SQL语句
     var strSQL ="select pmanagecom,name,codename,managecount,traincount,cast(round(cast(traincount as float)/managecount,2) as decimal(10,2)) from ("
    	 +"select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
         +" (select codename from ldcode where codetype = 'course' and code = a.coursename),"    
         + "(select count(1) from labranchgroup  where length(trim(branchattr)) = 10  and endflag ='N' and managecom like a.pmanagecom || '%'  and branchtype = '1' and branchtype2 = '01' and  not exists (select 1 from ldcom where trim(InteractFlag)='Y'  and comcode =labranchgroup.managecom)) as managecount,"
         +" count(1) as traincount"
         +"  from latraincourse a  where 1=1 and a.pmanagecom in ('8611','8612','8631','8650','8691','8692','8694','8695') and  a.cmanagecom in(select comcode from ldcom where trim(InteractFlag)<>'Y' or InteractFlag is null) "
         +getWherePart('a.pmanagecom','PManageCom','like')
         if(fm.all('StartDate').value!=null&&fm.all('StartDate').value!=""){
        	strSQL+=" and a.enddate>='"+fm.all('StartDate').value+"'";
         }
         if(fm.all('EndDate').value!=null&&fm.all('EndDate').value!=""){
     	    strSQL+=" and a.enddate<='"+fm.all('EndDate').value+"'";
         }
         strSQL+=" group by a.pmanagecom,a.coursename ";
      strSQL+="union all "    
      strSQL +=" select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
         +" (select codename from ldcode where codetype = 'course' and code = a.coursename),"    
         + "(select count(distinct substr(comcode,1,6)) from ldcom where length(trim(comcode))>=6 and comcode like a.pmanagecom||'%' and  trim(InteractFlag)<>'Y' or InteractFlag is null ) as managecount,"
         +" count(1) as traincount"
         +"  from latraincourse a  where 1=1   and a.pmanagecom not in ('8611','8612','8631','8650','8691','8692','8694','8695') and a.cmanagecom in(select comcode from ldcom where trim(InteractFlag)<>'Y' or InteractFlag is null) "
         +getWherePart('a.pmanagecom','PManageCom','like')
         if(fm.all('StartDate').value!=null&&fm.all('StartDate').value!=""){
        	strSQL+=" and a.enddate>='"+fm.all('StartDate').value+"'";
         }
         if(fm.all('EndDate').value!=null&&fm.all('EndDate').value!=""){
     	    strSQL+=" and a.enddate<='"+fm.all('EndDate').value+"'";
         }
         strSQL+=" group by a.pmanagecom,a.coursename) b order by b.pmanagecom ";
         turnPage.queryModal(strSQL, SetGrid);  
       	
}
function downLoad()
{
	 if (!verifyInput()) return false ;
     var strSql ="select pmanagecom,name,codename,managecount,traincount,cast(round(cast(traincount as float)/managecount,2) as decimal(10,2)) from ("
    	 +"select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
         +" (select codename from ldcode where codetype = 'course' and code = a.coursename),"    
         + "(select count(1) from labranchgroup  where length(trim(branchattr)) = 10  and endflag ='N' and managecom like a.pmanagecom || '%' and branchtype = '1' and branchtype2 = '01'  and not exists (select 1 from ldcom where trim(InteractFlag)='Y'  and comcode =labranchgroup.managecom)) as managecount,"
         +" count(1) as traincount"
         +"  from latraincourse a  where 1=1 and a.pmanagecom in ('8611','8612','8631','8650','8691','8692','8694','8695') and a.cmanagecom in(select comcode from ldcom where trim(InteractFlag)<>'Y' or InteractFlag is null) "
         +getWherePart('a.pmanagecom','PManageCom','like')
         if(fm.all('StartDate').value!=null&&fm.all('StartDate').value!=""){
        	 strSql+=" and a.enddate>='"+fm.all('StartDate').value+"'";
         }
         if(fm.all('EndDate').value!=null&&fm.all('EndDate').value!=""){
        	 strSql+=" and a.enddate<='"+fm.all('EndDate').value+"'";
         }
         strSql+=" group by a.pmanagecom,a.coursename  ";
         strSql+="union all "    
         strSql +=" select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
         +" (select codename from ldcode where codetype = 'course' and code = a.coursename),"    
         + "(select count(distinct substr(comcode,1,6)) from ldcom where length(trim(comcode))>=6 and comcode like a.pmanagecom||'%' and trim(InteractFlag)<>'Y' or InteractFlag is null ) as managecount,"
         +" count(1) as traincount"
         +"  from latraincourse a  where 1=1   and a.pmanagecom not in ('8611','8612','8631','8650','8691','8692','8694','8695')  and a.cmanagecom in(select comcode from ldcom where trim(InteractFlag)<>'Y' or InteractFlag is null) "
         +getWherePart('a.pmanagecom','PManageCom','like')
         if(fm.all('StartDate').value!=null&&fm.all('StartDate').value!=""){
        	 strSql+=" and a.enddate>='"+fm.all('StartDate').value+"'";
         }
         if(fm.all('EndDate').value!=null&&fm.all('EndDate').value!=""){
        	 strSql+=" and a.enddate<='"+fm.all('EndDate').value+"'";
         }
         strSql+=" group by a.pmanagecom,a.coursename) b order by b.pmanagecom "; 
     
	fm.all('querySQL').value = strSql;
	fm.submit();
}
