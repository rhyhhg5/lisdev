<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//	朱向峰	2004-11-15 9:47	
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType+" BranchType:"+BranchType2);

%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql ="";
   var msqlBranch=" 1 and CODEALIAS =#"+'2'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LAAMAgentInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LAAMAgentInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="./LAAMAgentSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
	<table>
		<tr class=common>
			<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
			<td class=titleImg>
				人员信息
			</td>
		</tr>
	</table>
	<Div id= "divLAAgent1" style= "display: ''">
    <table  class= common>
      <TR  class= common>
        <TD class= title>
          人员编码
        </TD>
        <TD  class= input>
          <Input class= 'readonly' readonly name=AgentCode type = hidden >
          <Input class= 'readonly' readonly name=groupAgentCode >
        </TD>
        <TD  class= title>
          人员姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common verify="人员姓名|NotNull&len<=20" elementtype=nacessary>
        </TD>
        <TD  class= title>
          性别
        </TD>
        <TD  class= input>
          <Input name=Sex class="codeno" verify="性别|code:Sex" 
           ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" 
           onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"
           ><Input class=codename name=SexName readOnly elementtype=nacessary>
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          出生日期
        </TD>
        <TD  class= input>
          <Input name=Birthday class=common  verify="出生日期|NotNull&Date" elementtype=nacessary>
        </TD>
        <TD  class= title>
          身份证号码
        </TD>
        <TD  class= input>
          <Input name=IDNo class= common verify="身份证号码|notnull&len<=20" onchange="return changeIDNo();" elementtype=nacessary>
        </TD>
         <TD  class= title>
          民族
        </TD>
        <TD  class= input>
          <Input name=Nationality class="codeno" id="Nationality" 
           ondblclick="return showCodeList('Nationality',[this,NationalityName],[0,1]);" 
           onkeyup="return showCodeListKey('Nationality',[this,NationalityName],[0,1]);" 
           ><Input class=codename name=NationalityName readOnly >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          户口所在地
        </TD>
        <TD  class= input>
          <Input name=RgtAddress class="codeno" 
           ondblclick="return showCodeList('NativePlaceBak',[this,RgtAddressName],[0,1]);" 
           onkeyup="return showCodeListKey('NativePlaceBak',[this,RgtAddressName],[0,1]);"
           ><Input class=codename name=RgtAddressName readOnly >
        </TD>
        <TD  class= title>
          籍贯
        </TD>
        <TD  class= input>
          <Input name=NativePlace class="codeno" verify="籍贯|code:NativePlaceBak" id="NativePlaceBak" 
           ondblclick="return showCodeList('NativePlaceBak',[this,NativePlaceName],[0,1]);" 
           onkeyup="return showCodeListKey('NativePlaceBak',[this,NativePlaceName],[0,1]);"
           ><Input class=codename name=NativePlaceName readOnly >
        </TD>
        <TD  class= title>
          政治面貌
        </TD>
       <TD  class= input>
          <Input name=PolityVisage class="codeno" verify="政治面貌|code:polityvisage" id="polityvisage" 
           ondblclick="return showCodeList('polityvisage',[this,PolityVisageName],[0,1]);" 
           onkeyup="return showCodeListKey('polityvisage',[this,PolityVisageName],[0,1]);" 
           ><Input class=codename name=PolityVisageName readOnly >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          学历
        </TD>
        <TD  class= input>
          <Input name=Degree class="codeno" verify="学历|code:Degree" id="Degree" 
           ondblclick="return showCodeList('Degree',[this,DegreeName],[0,1]);" 
           onkeyup="return showCodeListKey('Degree',[this,DegreeName],[0,1]);"
           ><Input class=codename name=DegreeName readOnly >
        </TD>
       <TD  class= title>
          毕业院校
        </TD>
        <TD  class= input>
          <Input name=GraduateSchool class= common >
        </TD>
        <TD  class= title>
          专业
        </TD>
        <TD  class= input>
          <Input name=Speciality class= common >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          职称
        </TD>
        <TD  class= input>
          <Input name=PostTitle class='codeno' verify="职称|code:posttitle" 
           ondblclick="return showCodeList('posttitle',[this,PostTitleName],[0,1]);" 
           onkeyup="return showCodeListKey('posttitle',[this,PostTitleName],[0,1]);" 
           ><Input class=codename name=PostTitleName readOnly >
        </TD>
        <TD  class= title>
          家庭地址
        </TD>
        <TD  class= input>
          <Input name=HomeAddress class= common >
        </TD>
        <TD  class= title>
          邮政编码
        </TD>
        <TD  class= input>
          <Input name=ZipCode class= common >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          联系电话
        </TD>
        <TD  class= input>
          <Input name=Phone class= common verify="联系电话|len<=18&notnull" elementtype=nacessary>
        </TD>
        <TD  class= title>
          传呼
        </TD>
        <TD  class= input>
          <Input name=BP class= common >
        </TD>
        <TD  class= title>
          手机
        </TD>
        <TD  class= input>
          <Input name=Mobile class= common >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          E_mail
        </TD>
        <TD  class= input>
          <Input name=EMail class= common >
        </TD>
         <TD  class= title>
          原工作单位
        </TD>
        <TD  class= input>
          <Input name=OldCom class= common >
        </TD>
        <TD  class= title>
          原职业
        </TD>
        <TD  class= input>
          <Input name=OldOccupation class='codeno' verify="原职业|code:occupationcode" 
           ondblclick="return showCodeList('occupationcode',[this,OldOccupationName],[0,1]);" 
           onkeyup="return showCodeListKey('occupationcode',[this,OldOccupationName],[0,1]);"
           ><Input class=codename name=OldOccupationName readOnly >
        </TD>

      </TR>
      <TR  class= common>

        <TD  class= title>
          原工作职务
        </TD>
        <TD  class= input>
          <Input name=HeadShip class= common  >
        </TD>
         <TD  class= title>
          入司时间
        </TD>
        <TD  class= input>
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' verify="入司时间|notnull&Date" elementtype=nacessary>
        </TD>
        <TD  class= title>
          备注
        </TD>
        <TD  class=input>
          <Input name=Remark class= common >
        </TD>
      </TR>
    <TR>
      <TD class = title>
             渠道类型
          </TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1],null,msqlBranch,1);" 
             verify="渠道类型|notnull&code:BranchType2" 
             ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
          </TD>
       </TR>
    </table>
        <!--离司日期-->
        <Input name=OutWorkDate class=common type=hidden >
    </Div>
    
       <table>
	 <tr>
		<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent7);">
		<td class= titleImg>
		资格证信息
		</td>
	</tr>
</table>
    
    
    <Div id= "divLAAgent7" style= "display: ''">
	<table  class= common  >
        <TR  class= common>
        <TD  class= title>
      资格证书号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=QualifNo verify="资格证书号" >
    </TD>
    <TD  class= title>
      批准单位
    </TD>
    <TD  class= input>
       <Input class= 'common' name=GrantUnit verify="批准单位" >
    </TD>

          <TD  class= title>
            发放日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=GrantDate verify="发放日期" > 
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            有效起期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=ValidStart verify="有效起期"  > 
          </TD>
           <TD  class= title>
            有效止期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=ValidEnd verify="有效止期"  > 
          </TD>
          <TD  class= title>
            资格证书状态
          </TD>
          <TD class=input><input name=QualifState class="codeno" name="QualifState"  verify="资格证书状态"
             CodeData="0|^0|有效|^1|失效"
             ondblClick="showCodeListEx('QualifStateList',[this,QualifStateName],[0,1]);"
             onkeyup="showCodeListKeyEx('QualifStateList',[this,QualifStateName],[0,1]);"
             ><Input class=codename name=QualifStateName readOnly >
          </TD>
        </TR>  
  </table>
 </Div >
    <!--行政信息-->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
            <td class= titleImg>
                团队信息
            </td>
    	</tr>
     </table>
		<Div id= "divLAAgent3" style= "display: ''">
			<table class=common>
			  <tr>
					<TD class= title>所属团队代码</TD>
					<TD class= input>
					<Input maxlength=10 class=common name=AgentGroup verify="所属机构|notnull" onChange="changeGroupItem();" elementtype=nacessary></TD>
					<TD class= title>所属团队名称</TD>
					<TD class= input>
					<Input maxlength=10 class=readonly name=AgentGroupName readonly></TD>
			 </tr>			
			</table>
   <tr><td><p> <font color="#ff0000">注：北分公司、江苏机构在中介人员录入时,需录入相关资格证信息</font></p></td></tr>
   <tr><td><p> <font color="#ff0000">注：北分公司、江苏机构在中介人员录入时,资格证书号、批准单位、发放日期、有效起期、有效止期、资格证状态 为必录项！</font></p></td></tr>
		</Div>

		<Input type=hidden name=InsideFlag >
		<Input type=hidden name=AgentGrade >
		<Input type=hidden name=UpAgent >
		<Input type=hidden name=AgentGradeName >
		<Input type=hidden name=AgentKind >
		<input type=hidden name=AgentState value=''>
		<Input type=hidden name=Operator >
		<Input type=hidden name=ManageCom >
		<input type=hidden name=hideOperate value=''>
		<input type=hidden name=hideAgentGroup value=''>
		<input type=hidden name=BranchLevel value=''>
		<!--input type=hidden name=hideManageCom value=''-->
		<input type=hidden name=BranchType value=''>
		<input type=hidden name=BranchManager value=''>
		<input type=hidden name=hideIsManager value='false'>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</form>
</body>
</html>

