<%
//程序名称：LABankTreeQueryInit.jsp
//程序功能：
//创建日期：2008-03-13 15:31:09
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {      
    fm.all('AgentCode').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('BranchType').value = top.opener.fm.all('BranchType').value;
    fm.all('BranchType2').value = top.opener.fm.all('BranchType2').value;
  }
  catch(ex)
  {
    alert("在LABankNewTreeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm()
{
  try
  {
    initInpBox();    
    initAgentGrid();
  }
  catch(re)
  {
    alert("在LABankNewTreeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentGrid
 ************************************************************
 */
function initAgentGrid()
{                               
 	var iArray = new Array();  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="业务员代码";         //列名
    iArray[1][1]="80px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="业务姓名";         //列名
    iArray[2][1]="100px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="管理机构";         //列名
    iArray[3][1]="80px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="职级";         //列名
    iArray[4][1]="80px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
 
    iArray[5]=new Array();
    iArray[5][0]="职级名称";         //列名
    iArray[5][1]="100px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许 
    
    iArray[6]=new Array();
    iArray[6][0]="销售团队代码";         //列名
    iArray[6][1]="100px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="销售团队名称";         //列名
    iArray[7][1]="100px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
    AgentGrid = new MulLineEnter( "fm" , "AgentGrid" ); 

    //这些属性必须在loadMulLine前
    AgentGrid.mulLineCount = 0;   
    AgentGrid.displayTitle = 1;
    AgentGrid.canSel=1;
    AgentGrid.canChk=0;
    AgentGrid.locked=1;
    AgentGrid.loadMulLine(iArray);  
   }
   catch(ex)
   {
     alert("初始化AgentGrid时出错："+ ex);
   }
}
</script>