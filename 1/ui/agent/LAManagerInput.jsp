<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LABranchGroupInput.jsp
	//程序功能：
	//创建日期：
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String LoginManagecom = tG.ManageCom;
	String CurDate = PubFun.getCurrentDate();
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
%>
<script>
	var cSql = " 1 and length(trim(comcode)) = #4# ";
</script>

<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LAManagerInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LAManagerInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
<form action="./LAManagerSave.jsp" method=post name=fm
	target="fraSubmit">
 <%@include file="../common/jsp/OperateAgentButton.jsp"%>
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divLABranchGroup1);">
		<td class=titleImg>基础信息录入</td>
	</tr>
</table>
<Div id="divLABranchGroup1" style="display: ''">
<table class=common>
	<tr class=common>
		<TD class=title>省分公司</TD>
		<TD class=input><Input class="codeno" name=PManageCom
			verify="省分公司|code:comcodeallsign&NOTNULL&len=4"
			ondblclick="return showCodeList('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1);"
			onkeyup="return showCodeListKey('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1);"><Input
			class=codename name=PManageComName readOnly elementtype=nacessary></TD>
		<TD class=title>中心支公司</TD>
		<TD class=input><Input class="codeno" name=CManageCom
			verify="中心支公司|code:comcode&NOTNULL&len=8"
			ondblclick="return getManagecom(CManageCom,CManageComName);"
			onkeyup="return getManagecom(CManageCom,CManageComName);"><Input
			class=codename name=CManageComName readOnly elementtype=nacessary></TD>
		<TD class=title>营业部</TD>
		<TD class=input><Input name=BranchAttr id=BranchAttr
			class="codeno" verify="营业部|notnull&len=10"
			ondblclick="return getBranchAttr(BranchAttr,BranchAttrName);"
			onkeyup="return getBranchAttr(BranchAttr,BranchAttrName);"><Input
			maxlength=12 class="codename" name=BranchAttrName
			verify="营业部|notnull" elementtype=nacessary></TD>
	</tr>
	<tr class=common>
	 <TD class= title>
		营业部经理工号
	 </TD>
	 <TD  class= input>
		<Input class= 'readonly' readonly name=ManagerCode >
	 </TD>
		<TD class=title>姓名</TD>
		<TD class=input><Input name=ManagerName class=common
			verify="姓名|NotNull&len<=20" elementtype=nacessary></TD>
		<TD class=title>性别</TD>
		<TD class=input><Input name=Sex class="codeno"
			verify="性别|code:Sex&notnull" 
			ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,null,'100');"
			onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"
			debug='10'><Input name=SexName class="codename" elementtype=nacessary
			></TD>
		
	</tr>
	<tr class=common>
	<TD class=title>岗位职级</TD>
		<TD class=input><Input name=ManagerGrade class="codeno"
			verify="岗位职级|code:managergrade&notnull"  ondblclick="return showCodeList('managergrade',[this,GradeName],[0,1]);" onkeyup="return showCodeListKey('managergrade',[this,GradeName],[0,1]);" 
 ><Input name=GradeName class="codename" elementtype=nacessary readonly
			></TD>
			 <td class=title>
	 任本职级日期
	 </td>
	 <TD  class= input>
		  <Input name=TakeOfficeDate class='coolDatePicker' dateFormat='short' verify=" 任本职级日期|Date&notnull&len=10" ><span style="color:Red">*</span>
	 </TD>
	    <td>
                          政治面貌 
        </TD>
        <TD  class= input> 
          <Input name=PolityVisage class="codeno" verify="政治面貌|code:polityvisage" id="polityvisage" ondblclick="return showCodeList('polityvisage',[this,PolityVisageName],[0,1]);" onkeyup="return showCodeListKey('polityvisage',[this,PolityVisageName],[0,1]);" 
          ><Input name=PolityVisageName class="codename"  > 
        </TD> 
      
	</tr>
	<tr class=common>
	    <TD  class= title>
		     入司时间
		</TD>
		<TD  class= input>
		  <Input name=InsureDate class='coolDatePicker' dateFormat='short' verify=" 入司时间|Date&len=10" >
		</TD>
		<TD  class= title> 
                        毕业院校
        </TD>
        <TD  class= input> 
          <Input name=GraduateSchool class= common > 
        </TD>
        <TD  class= title>
                       所学专业 
        </TD>
        <TD  class= input> 
          <Input name=Speciality class= common > 
        </TD>
	</tr>

	<tr class=common>
	  <TD  class= title>
		     毕业时间
		</TD>
		<TD  class= input>
		  <Input name=GraduateDate class='coolDatePicker' dateFormat='short' verify=" 入司时间|Date&len=10" >
		</TD>
	     <TD  class= title>
                       最高学历
        </TD>
        <TD  class= input> 
          <Input name=Education class="codeno" verify="最高学历|code:Degree" id="Education" ondblclick="return showCodeList('Degree',[this,EducationName],[0,1]);" onkeyup="return showCodeListKey('Degree',[this,EducationName],[0,1]);"
          ><Input name=EducationName class="codename"  >  
        </TD>
        <TD  class= title>
                       最高学位
        </TD>
        <TD  class= input> 
          <Input name=Degree class="codeno" verify="最高学位|code:edu" id="Degree" ondblclick="return showCodeList('edu',[this,DegreeName],[0,1]);" onkeyup="return showCodeListKey('Degree',[this,DegreeName],[0,1]);"
          ><Input name=DegreeName class="codename"  >  
        </TD>
	    
	</tr>
	<tr class=common>
	  <TD  class= title>
                年度考核
      </TD>
      <TD  class= input>
        <Input name=Assess class="codeno" verify="年度考核|code:Assess" ondblclick="return showCodeList('Assess',[this,AssessName],[0,1],null,null,null,null,'100');" onkeyup="return showCodeListKey('Assess',[this,AssessName],[0,1]);" debug='10'
        ><Input name=AssessName class="codename" >
      </TD> 
      	  <TD  class= title>
                兼职身份
      </TD>
      <TD class=input><Input name=MulitId class="codeno"
			verify="兼职身份|code:mulitid&num"  ondblclick="return showCodeList('mulitid',[this,MulitIdName],[0,1]);" onkeyup="return showCodeListKey('mulitid',[this,MulitIdName],[0,1]);" 
 ><Input name=MulitIdName class="codename"  
			></TD> 
   <TD  class= title>
                     在职状态
        </TD>
        <TD  class= input>
        <Input name=ManagerState class="codeno" verify="在职状态|num&notnull" CodeData="0|^0|在职|^1|离职"  ondblclick="return showCodeListEx('ManagerState',[this,StateName],[0,1]);" onkeyup="return showCodeListKeyEx('ManagerState',[this,StateName],[0,1]);" debug='10'
        ><Input name=StateName class="codename" elementtype=nacessary >
      </TD>
	</tr >
	<tr class=common>
	 <td class= title>
	  从事保险工作时间
	 </td>
	 <td class= input>
          <Input name=WorkYear class= common verify="从事保险工作时间|num&len<3" ><span>（年）</span>
	 </td>
	   <TD  class= title>
		     参加工作时间
		</TD>
		<TD  class= input>
		  <Input name=WorkDate class='coolDatePicker' dateFormat='short' verify="参加工作时间|Date&len=10" >
		</TD>
		<TD  class= title>
                          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class='coolDatePicker' dateFormat='short' verify="出生日期|NotNull&Date&len=10" ><span style="color:Red">*</span> 
        </TD>
	</tr>
	<tr class=common>
	<TD  class= title>
							证件类型
						</TD>
						<TD  class= input>
							<Input name=IDNoType class= "codeno" verify="证件类型|notnull&code:idtype"
							ondblclick="return showCodeList('idtype',[this,IDNoTypeName],[0,1],null,null,null,null,'100');"
							onkeyup="return showCodeListKey('idtype',[this,IDNoTypeName],[0,1]);"
							><Input name=IDNoTypeName class="codename" readonly elementtype=nacessary>
						</TD>
	<TD  class= title>
          证件号码 
        </TD>        
        <TD  class= input> 
          <Input name=IDNo class= common verify="证件号码|notnull&len<=20"  elementtype=nacessary> 
        </TD>
       <TD  class= title>
                      手机号码
        </TD>
        <TD  class= input> 
          <Input name=Mobile class= common verify="手机号码|num&len<=18&notnull" elementtype=nacessary>
        </TD>
        </tr>
</table>
</Div>
<input type=hidden name=LaborContract value=''> 
<input type=hidden name=hideOperate value=''> 
<input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
<input type=hidden name=BranchType value=<%=BranchType%>> 
<input type=hidden name=BranchType2 value=<%=BranchType2%>> 
<input type=hidden name=LoginManagecom value=<%=LoginManagecom%>> 
<input type=hidden name=ReturnFlag value=''> 
<input type=hidden name=AgentGroup value=''> <!--后台操作的隐式机构编码，不随机构的调整而改变 --> 
<span id="spanCode" style="display: none; position: absolute;"></span>
</form>
<form action="" method=post name=fm2 target="fraSubmit">
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divLABranchGroup2);">
		<td class=titleImg>培训文件</td>
	</tr>
</table>
<Div id="divLABranchGroup2" style="display: ''">
 <table class=common>
	<TR class=common>
		<TD class=title style="width: 100px">劳动合同影印件</TD>
		<TD class=input><Input type="file" class="common"
			style="width: 300px" name="PictureFile" id="PictureFile" /></TD>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="上  传" id="goDiskImport"
			class=cssButton onclick="upLoad('PictureFile')">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="下  载" id="goDiskImport"
			class=cssButton onclick="download('PictureFile')"></td>
	</TR>
 </table>
</div>
<input type=hidden name=LaborContract value=''> 
<input type=hidden name=dataName value=''> 
<input type=hidden name=downName value=''> 
<input type=hidden name=filePath value=''>
<input type=hidden name=ManagerCode2 value=''>
<input type=hidden name=tBranchAttrName value=''> 
</form>
</body>
</html>
