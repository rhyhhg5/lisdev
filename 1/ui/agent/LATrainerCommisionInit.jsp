<%
//程序名称：LaTrainerCommisionInit.jsp
//程序功能：
//创建时间：2018-06-07
//创建人  ：WangQingMin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
 var  mSQL=" 1 and  char(length(trim(comcode)))  = #4# and sign=#1#" ;
 var triskcode="1 and code not in (select riskcode from lmriskapp where risktype4=#4#) ";
  </SCRIPT>       


<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.all('ManageCom').value='';
  	fm.all('ManageComName').value='';

          
  }
  catch(ex)
  {
    alert("在LATraineCommisionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
        
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[1][4]="ComCode";
    iArray[1][5]="1|2";
    iArray[1][6]="0|1";
    iArray[1][9]="管理机构|code:comcode&NotNull&NUM&len=4";  
    iArray[1][15]="1";
    iArray[1][16]=mSQL; 
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许        
    
    iArray[3]=new Array();
    iArray[3][0]="基本工资标准"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;          //列最大值
    iArray[3][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[3][9]="基本工资标准|NotNull&NUM&value>=0>";
    

    iArray[4]=new Array();
    iArray[4][0]="idx";            //列名 idx
    iArray[4][1]="0px";          //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0; 
   
  
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
    SetGrid.displayTitle = 1;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LATraineCommisionInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();    
    initSetGrid();
  }
  catch(re)
  {
    alert("在LATraineCommisionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>