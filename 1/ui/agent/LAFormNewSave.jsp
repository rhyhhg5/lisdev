<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LANewToFormSave1.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema mLAAgentSchema = new LAAgentSchema();
  LATreeSchema  mLATreeSchema = new LATreeSchema();
  LAFormNewUI mLAFormNewUI  = new LAFormNewUI();

  //输出参数
  CErrors tError = null;
  String tOperate="UPDATE||INSERT";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String newAgentGrade = request.getParameter("AgentGradeNew");
  String newAgentGroup = request.getParameter("AgentGroupNew");


  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //取得被调整信息
  System.out.println("begin LAFormNewUI schema...");

  mLAAgentSchema.setAgentCode(request.getParameter("AgentCode"));
  mLAAgentSchema.setEmployDate(request.getParameter("AdjustDate"));
  mLAAgentSchema.setAgentGroup(request.getParameter("AgentGroup"));
  mLATreeSchema.setIntroAgency(request.getParameter("IntroNewAgency"));
  mLATreeSchema.setAgentCode(request.getParameter("AgentCode"));
  mLATreeSchema.setInitGrade(request.getParameter("InitGrade"));
  if((newAgentGroup.equals(""))||(newAgentGroup== null))
  {
  mLATreeSchema.setAgentGroup(request.getParameter("AgentGroup"));
  }
  else
  mLATreeSchema.setAgentGroup(newAgentGroup);	
  if((newAgentGrade.equals(""))||(newAgentGrade== null))
  {
  mLATreeSchema.setAgentGrade(request.getParameter("AgentGradeOld"));
  }
  else
  mLATreeSchema.setAgentGrade(newAgentGrade);	



  System.out.println("IntroAgency:"+request.getParameter("IntroNewAgency"));
  System.out.println("AgentCode:"+request.getParameter("AgentCode"));
  System.out.println("AdjustDate:"+request.getParameter("AdjustDate"));
  System.out.println("AgentGroup:"+mLATreeSchema.getAgentGroup());
  System.out.println("AgentGrade:"+mLATreeSchema.getAgentGrade());

  //传递数据
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(mLAAgentSchema);
  System.out.println(mLAAgentSchema.getAgentCode());
  tVData.addElement(mLATreeSchema);
  System.out.println(mLATreeSchema.getIntroAgency());
  System.out.println("add over");
   try
  {
    System.out.println("this will save the data!!!");
    mLAFormNewUI.submitData(tVData,tOperate);
     System.out.println(FlagStr);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLAFormNewUI.mErrors;
    if (!tError.needDealError())
    {
    //Agentcode = tLAAgent.getAgentCode();
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript" type="">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

