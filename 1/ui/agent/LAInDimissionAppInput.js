//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var mDepartDate="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
//判断是否存在没签单的保单  2006-02-17 xiangchun    
   if (!checkcontno())
    return false;   

  if (mOperate==""){
  	mOperate="INSERT||MAIN";
  }

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.hideOperate.value=mOperate;

  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";
var tBranchType = fm.all('BranchType').value;
var tBranchType2 = fm.all('BranchType2').value;
  showInfo=window.open("./LADimissionAppQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}  


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
		initForm();
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  //  showDiv(operateButton,"true"); 
  //  showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LADimissionInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	  
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value==null))
  {
  	alert("请输入员工编码！");
  	fm.all('AgentCode').focus();
  	return false;
  }	  
  if ((fm.all('DepartRsn').value == '')||(fm.all('DepartRsn').value==null))
  {
  	alert("请输入离职原因！");
  	fm.all('DepartRsn').focus();
  	return false;
  }  
  if ((fm.all('AppDate').value == '')||(fm.all('AppDate').value==null))
  {
  	alert("请输入申请日期！");
  	
  	return false;
  }
  if(!isDate(document.fm.AppDate.value))
  {
  	alert("请输入一个合法日期！");
  	return false;
  }
 
  return true;
}           
//查寻是否存在没签单的保单
function checkcontno()
{
  var tAgentCode=fm.all('AgentCode').value;	
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  var sql="";
  if (tBranchType=='1' && tBranchType2=='01')
  {
    sql="select contno  from  lccont where  agentcode=getAgentCode('"+tAgentCode+"') and grpcontno='00000000000000000000' and (appflag<>'1' or appflag is null ) and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno) and stateflag<>'3' and stateflag<>'2'  ";	
  	sql +="union select grpcontno  from  lcgrpcont where  agentcode=getAgentCode('"+tAgentCode+"') and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and stateflag<>'2'  and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;
  }
  else if (tBranchType=='2' && tBranchType2=='01')
  { 
    sql="select grpcontno  from  lcgrpcont where  agentcode=getAgentCode('"+tAgentCode+"') and  (appflag<>'1' or appflag is null ) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) ";	  	
  }
  strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if(strQueryResult)
  {//如果有没签单的
  	alert("此业务员还有没签单的保单，不能离职！");
  	return false ;
  }
  else
   {//如果都签单了
   var sql="";
    if (tBranchType=='1' && tBranchType2=='01')
    {
     sql="select contno  from  lccont where agentcode=getAgentCode('"+tAgentCode+"') and grpcontno='00000000000000000000' and  (customgetpoldate is null or getpoldate is null)  and  ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and stateflag<>'3' and stateflag<>'2'";		
     sql+=" union select grpcontno  from  lcgrpcont where  agentcode=getAgentCode('"+tAgentCode+"')   and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and stateflag<>'2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)  and (cardflag<>'2' or cardflag is null)" ;		
     strQueryResult = easyQueryVer3(sql, 1, 1, 1); 

       if(strQueryResult)
       {
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);	    
     	  alert("此业务员还有未回执回销的保单，不能离职！");
     	  return false;
      }      
    }
    else if (tBranchType=='2' && tBranchType2=='01')
    {
       sql="select grpcontno  from  lcgrpcont where  agentcode=getAgentCode('"+tAgentCode+"')   and  customgetpoldate is null  and  uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2' and stateflag<>'3' and (cardflag<>'2' or cardflag is null)";			
       strQueryResult = easyQueryVer3(sql, 1, 1, 1); 
       if(strQueryResult)
       {
           var tArr = new Array();
           tArr = decodeEasyQueryResult(strQueryResult);	    
           alert("此业务员还有未回执回销的保单，不能离职！");
     	   return false;	   
      }                
    }
    sql="select contno  from  lccont where agentcode=getAgentCode('"+tAgentCode+"') and grpcontno='00000000000000000000' and  (customgetpoldate is null or getpoldate is null or signdate is null) and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and stateflag='2' and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno) ";			
    sql+=" union select grpcontno  from  lcgrpcont where  agentcode=getAgentCode('"+tAgentCode+"') and stateflag='2' and  (customgetpoldate is null or getpoldate is null or signdate is null) and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;
     	strQueryResult = easyQueryVer3(sql, 1, 1, 1);
     	if(strQueryResult){
     		if(!confirm("有保单失效但没有交回回执，若复效后会影响后续代理人的离职!仍要继续进行离职登记吗？")){
  				return false;
  			}
     	}
  		return true;
   }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
 initForm();
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert("请确定业务员编码！");
  else if ((fm.all('DepartTimes').value==null)||(fm.all('DepartTimes').value==''))
    alert("请查询出要修改的记录！");
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           


//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert("请确定业务员编码！");
  else if ((fm.all('DepartTimes').value==null)||(fm.all('DepartTimes').value==''))
    alert("请查询出要删除的记录！");
  else
  {
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证业务员编码的合理性
function checkValid()
{ 

  var strSQL = "";
  var strQueryResult = null;
  var tBranchType=fm.all('BranchType').value;
   var tBranchType2=fm.all('BranchType2').value;
   var tManageCom =getManageComLimit(); 
  if (getWherePart('AgentCode')!='')
  {
//     	modify lyc 统一工号 2014-11-25
     strSQL = "select getUniteCode(AgentCode),name,branchtype,branchtype2,managecom from LAAgent where 1=1 "+getWherePart('groupAgentCode','AgentCode')
             
             +" and (Agentstate in ('01','02') or (AgentState is null))"
             +" and branchtype='"+tBranchType+"'";
     strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
    // alert(strSQL);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    fm.all('AgentCode').value  = "";
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  else
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult); 
  	fm.all('AgentName').value = arrDataSet[0][1]; 	
  	fm.all('BranchType').value = arrDataSet[0][2];
  	fm.all('BranchType2').value = arrDataSet[0][3];   
  	mManageCom= arrDataSet[0][4]; 
  	cAgentCode = arrDataSet[0][0];
  //	alert(mManageCom.indexOf(tManageCom)); 
  	if(mManageCom.indexOf(tManageCom)<0)
  	{
  	alert("您没有该业务员的操作权限!");
  	fm.all('AgentCode').value  = "";
        fm.all('DepartTimes').value = "";
        initInpBox();
        return false;
  	}
  }
  //查询成功则拆分字符串，返回二维数组
  var tBranchType=fm.all('BranchType').value;
   var tBranchType2=fm.all('BranchType2').value;
  strQueryResult = null;
  strSQL = "select getUniteCode(AgentCode),departtimes from LADimission where 1=1 and AgentCode = getAgentCode('"+cAgentCode+"')"
	   +getWherePart('BranchType')
	   +getWherePart('BranchType2')
	   + " order by AgentCode,departtimes";
  strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult)
    fm.all('DepartTimes').value = '1';
  else
  {
    var arrDataSet = decodeEasyQueryResult(strQueryResult);  
    fm.all('DepartTimes').value = eval(arrDataSet[arrDataSet.length-1][1])+1;
  }

}
//用来显示返回的选项
function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.all('AgentCode').value = arrResult[0][0]; 
    
    fm.all('AgentName').value = getAgentName(arrResult[0][0]); 
    fm.all('DepartTimes').value = arrResult[0][1];
    fm.all('DepartDate').value = arrResult[0][2];  
    mDepartDate=arrResult[0][2];
                                                
    fm.all('DepartRsn').value = arrResult[0][3];                         
    fm.all('ContractFlag').value = arrResult[0][4];   
    fm.all('WorkFlag').value = arrResult[0][5];                                             
    fm.all('PbcFlag').value = arrResult[0][6];                                           
    fm.all('ReceiptFlag').value = arrResult[0][7];                                           
    //fm.all('LostFlag').value = arrResult[0][8];                                           
    fm.all('CheckFlag').value = arrResult[0][9];                                               
    fm.all('WageFlag').value = arrResult[0][10];                                               
    fm.all('TurnFlag').value = arrResult[0][11];                                               
    fm.all('Noti').value = arrResult[0][12];                                               
    fm.all('Operator').value = arrResult[0][13];                                                                                                                                                                                                                           	
  }   
}
function getAgentName(tAgentCode)
{
	var tAgentName='';
	var strSQL = "select name from LAAgent where 1=1 and AgentCode=getAgentCode('"+tAgentCode
             +"')";
             //alert(strSQL);
  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  if (strQueryResult)
  {
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
  tAgentName = arrDataSet[0][0]; 
	}
  return tAgentName;
}
/*
function handler(key_event)
{
	if (document.all)
	{
		if (key_event.keyCode==13)
		{
		    parent.fraInterface.fm.action = "DimAgentQuery.jsp";	
		    if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
                    {
  	              alert("请输入业务员编码！");
  	              fm.all('AgentCode').focus();
  	              return false;
                    }
		    fm.submit();
		    parent.fraInterface.fm.action = "LADimissionSave.jsp";	
		    return true;
		}
	}  	
}*/

function checkDate(){
  var   date   =   new   Date(); 
 var   curYear=date.getYear(); 
 var   curMonth=date.getMonth()+1; 
 var   curDate=date.getDate(); 
 var   strDate=curYear+ "-"+curMonth+ "-"+curDate;
  var tAgentCode = fm.all('AgentCode').value;
  var strSQL = "select employdate from LAAgent where 1=1 and AgentCode=getAgentCode('"+tAgentCode
             +"')";
   var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  if (strQueryResult)
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
  temploydate = arrDataSet[0][0]; 
  var arr2 = temploydate.split("-");
  var employdateNew = new Date(arr2[0],arr2[1],arr2[2]);
	var tCurDate=new Date();
	var tEmpDate=fm.all('AppDate').value;
	var arr1=tEmpDate.split("-");
	var tEmpDate=new Date(arr1[0],arr1[1],arr1[2]);
	if(tEmpDate.getYear()<tCurDate.getYear()
		||(tEmpDate.getYear()==tCurDate.getYear() && tEmpDate.getMonth()<(tCurDate.getMonth()+1))
		||(tEmpDate.getYear()==tCurDate.getYear() && tEmpDate.getMonth()==(tCurDate.getMonth()+1) &&　tEmpDate.getDate()<tCurDate.getDate())){
		alert("离职申请时间不应早于当前日期！操作失败。");
		fm.all('AppDate').value= strDate;
	}
	if(tEmpDate.getYear()<employdateNew.getYear()
		||(tEmpDate.getYear()==employdateNew.getYear() && tEmpDate.getMonth()<(employdateNew.getMonth()+1))
		||(tEmpDate.employdateNew()==employdateNew.getYear() && tEmpDate.getMonth()==employdateNew.getMonth() &&　tEmpDate.getDate()<employdateNew.getDate())){
		alert("离职申请时间不应早于入司日期！操作失败。");
		fm.all('AppDate').value= strDate;
	}
	return true;
	
}