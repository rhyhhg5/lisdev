<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LATrainerWageRateQuerySave.jsp
//程序功能： 
//创建日期：2018-5-29
//创建人  ：wangQingMin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
 boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    //薪资月，如果 为12月，则显示 年终奖，否则不显示 
    String downLoadFileName = "组训薪资明细查询报表 _"+tG.Operator+"_ .xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    String querySql = request.getParameter("querySql");
    System.out.println(querySql);
    querySql = querySql.replaceAll("%25","%");
    		 //设置表头
		    String[][] tTitle = {{"组训人员工号","组训人员姓名","组训人员职级","管理机构代码","管理机构名称","营业部代码","营业部名称","基本工资标准","月绩效比例","月新单期缴标准保费","继续率","绩优人力","月初人力","月末人力","举绩率"}};
		    //表头的显示属性
		    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		    
		    //数据的显示属性
		    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		    //生成文件
		    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
		    createexcellist.createExcelFile();
		    String[] sheetName ={"组训薪资明细报表"};
		    createexcellist.addSheet(sheetName);
		    int row = createexcellist.setData(tTitle,displayTitle);
		    if(row ==-1) errorFlag = true;
		        createexcellist.setRowColOffset(row,0);//设置偏移
		        System.out.println(querySql);
		    if(createexcellist.setData(querySql,displayData)==-1)
		        errorFlag = true;
		    if(!errorFlag)
		        //写文件到磁盘
		        try{
		            createexcellist.write(tOutXmlPath);
		        }catch(Exception e)
		        {
		            errorFlag = true;
		            System.out.println(e);
		        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>