//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug = "0";
var cName = "";
var strSql = "";
// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		// parent.fraInterface.initForm();
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

// 执行查询状态
function AgentWageStateQuery() {
	if (!verifyInput()) {
		return false;
	}
	// 校验是否该月佣金已算过
	var strSQ = "select state from latrainerWagehistory  " + "where 1=1 "
			+ getWherePart('WageNo', 'WageNo')
			+ getWherePart('ManageCom', 'ManageCom')
			+ getWherePart('BranchType', 'BranchType')
			+ getWherePart('BranchType2', 'BranchType2');
	// 判断是否查询成功
	var strQueryResult = easyQueryVer3(strSQ, 1, 1, 1);
	if (!strQueryResult) {
		alert("该月佣金提数计算还未进行!");
		return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	var tState = arr[0][0];

	if (tState == "00") {
		alert("佣金还没有计算！");
		return false;
	} else if (tState == "11") {
		alert("佣金计算正在进行中");
		return false;
	} else if (tState == "13") {
		alert("佣金计算已经确认！");
		return false;
	}
	return true;
}
// 执行薪资确认
function LATrainerWageConfirmSave() {
	if (!verifyInput())
		return false;
	if (!AgentWageStateQuery())
		return false;
	fm.submit();
}
