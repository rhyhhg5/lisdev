<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：
	//程序功能：
	//创建日期：2002-08-16 16:25:40
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
%>
<head>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LASocialDimissionInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LASocialDimissionInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body onload="initForm();initElementtype();">
<form action="./LASocialDimissionSave.jsp" method=post name=fm target="fraSubmit">
<table class="common" align=center>
	<tr align=right>
		<td class=button>&nbsp;&nbsp;</td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="保  存" TYPE=button onclick="return submitForm();"></td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="重  置" TYPE=button onclick="return resetForm();"></td>
	</tr>
</table>
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLADimission1);">
		<td class=titleImg>员工离职信息</td>
	</tr>
</table>
<Div id="divLADimission1" style="display: ''">
<table class=common>
	<tr class=common>
		<td class=title>业务代码</td>
		<td class=input><input class=common name=GroupAgentCode
			onchange="return checkValid();" elementtype=nacessary></td>
		<td class=title>业务姓名</td>
		<td class=input><input class="readonly" readonly name=AgentName>
		</td>
	</tr>

	<tr class=common>
		<td class=title>离职原因</td>
		<td class=input><input name=DepartRsn class='codeno'
			ondblclick="return showCodeList('DepartRsn',[this,DepartRsnName],[0,1]);"
			onkeyup="return showCodeListKey('DepartRsn',[this,DepartRsnName],[0,1]);"><Input
			class=codename name=DepartRsnName readOnly elementtype=nacessary>
		</td>

		<td class=title>申请日期</td>
		<TD class=input><Input class='coolDatePicker' name=AppDate
			dateFormat='short' verify="申请日期|notnull&date"></TD>
	</tr>
	<tr class=common>
		<td class=title>备注</td>
		<td class=input><input name=Noti class=common></td>

		<td class=title>操作员代码</td>
		<td class=input><input name=Operator class="readonly" readonly>
		</td>
	</tr>
</table>
</Div>
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divContGrid);"></td>
		<td class=titleImg>业务未签单或未回执回销保单信息</td>
	</tr>
</table>
<Div id="divContGrid" style="display: ''">
<table class=common>
	<tr class=common>
		<td text-align: left colSpan=1><span id="spanContGrid"> </span>
		</td>
	</tr>
</table>
<INPUT VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();" class="cssButton"> 
<INPUT VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();" class="cssButton"> 
<INPUT VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();" class="cssButton"> 
<INPUT VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();" class="cssButton"></div>
<p><font color="red">注：含有未签单或未回执回销保单的业务可以做离职登记，但不能做离职确认。</font></p>
<input type=hidden name=hideOperate value=''> 
<input type=hidden name=BranchType value=''> 
<input type=hidden name=BranchType2 value=''> 
<input type=hidden name=DepartTimes value=''>
<input type=hidden name=CurrDate value=''> 
<input type=hidden name=AgentCode value=''>
<span id="spanCode" style="display: none; position:absolute; slategray"></span></form>
</body>
</html>
