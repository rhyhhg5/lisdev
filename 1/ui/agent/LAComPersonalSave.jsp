<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAComPersonalSave.jsp
//程序功能：
//创建日期：2005-11-01 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.encrypt.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAComPersonalSchema tLAComPersonalSchema   = new LAComPersonalSchema();
  LAComPersonalUI tLAComPersonalUI = new LAComPersonalUI();
  String Agentcode = "";

	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRearStr = "";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";



  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    LisIDEA tLisIdea = new LisIDEA();
  tLAComPersonalSchema.setPassword(tLisIdea.encryptString(request.getParameter("Password")));
  if(request.getParameter("UserType").endsWith("02")){
	  tLAComPersonalSchema.setPersonalCode(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("AgentCode")+"'"));
 }else{
	  tLAComPersonalSchema.setPersonalCode(request.getParameter("AgentCode"));
  }
  System.out.println("AgentCode---"+request.getParameter("AgentCode"));
//  tLAComPersonalSchema.setPersonalCode(request.getParameter("AgentCode"));
  tLAComPersonalSchema.setName(request.getParameter("Name")); //暂存显式代码
  tLAComPersonalSchema.setsex(request.getParameter("Sex"));
  tLAComPersonalSchema.setHeadShip(request.getParameter("HeadShip"));
  System.out.println("UserType"+request.getParameter("UserType"));
  tLAComPersonalSchema.setUserType(request.getParameter("UserType"));
  System.out.println("AgentId"+request.getParameter("AgentId"));
 // tLAComPersonalSchema.setAgentCode(request.getParameter("AgentId"));
   tLAComPersonalSchema.setAgentCode(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("AgentId")+"'"));
  tLAComPersonalSchema.setAgentCom(request.getParameter("AgentCom"));
  tLAComPersonalSchema.setBirthday(request.getParameter("Birthday"));
   tLAComPersonalSchema.setHomeAddress(request.getParameter("HomeAddress"));
   
  tLAComPersonalSchema.setIDNo(request.getParameter("IDNo"));
  tLAComPersonalSchema.setPhone(request.getParameter("Phone"));
  tLAComPersonalSchema.setMobile(request.getParameter("Mobile"));
  tLAComPersonalSchema.setEmployDate(request.getParameter("EmployDate"));
  tLAComPersonalSchema.setOutWorkDate(request.getParameter("OutWorkDate"));
  
  tLAComPersonalSchema.setNoti(request.getParameter("Noti"));
  tLAComPersonalSchema.setLoginName(request.getParameter("LoginName"));
 // tLAComPersonalSchema.setPassword(request.getParameter("Password"));
  tLAComPersonalSchema.setOperator(request.getParameter("Operator"));
  tLAComPersonalSchema.setManageCom(request.getParameter("ManageCom"));
  tLAComPersonalSchema.setRuleState(request.getParameter("RuleState"));
  //System.out.print(request.getParameter("RuleState"));
 // tLAComPersonalSchema.setIsForAirUse("Y");  //是航意险
 tLAComPersonalSchema.setMac_id(request.getParameter("Mac_id"));
 tLAComPersonalSchema.setMac_flag(request.getParameter("Mac_flag"));
 
 System.out.println("什么情况："+request.getParameter("Mac_flag"));
  

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.addElement(tLAComPersonalSchema);

System.out.println("tOperate:"+tOperate+tVData);
  try
  {
    System.out.println("this will save the data!!!");
    tLAComPersonalUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAComPersonalUI.mErrors;
    if (!tError.needDealError())
    {
    Agentcode = tLAComPersonalUI.getPersonalCode();
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
        parent.fraInterface.fm.all('AgentCode').value = '<%=Agentcode%>';
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
