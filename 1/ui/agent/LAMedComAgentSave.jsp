<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgenttempSchema tLAAgentSchema   = new LAAgenttempSchema();
  LAQualificationSchema tLAQualificationSchema   = new LAQualificationSchema();
  
  LAMedComAgentUI tLAMedComAgentUI = new LAMedComAgentUI();
  
  String AgentCode = "";
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  System.out.println(request.getParameter("ManageCom"));
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    LisIDEA tLisIdea = new LisIDEA();
    tLAAgentSchema.setPassword(tLisIdea.encryptString("1111"));//设置销售人员默认密码，方便以后查询分析系统使用
    tLAAgentSchema.setAgentCode(request.getParameter("AgentCode"));
    tLAAgentSchema.setAgentGroup("000000000000");
    tLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
    tLAAgentSchema.setEntryNo(request.getParameter("AgentCom"));
    tLAAgentSchema.setBranchType(request.getParameter("BranchType"));
    tLAAgentSchema.setBranchType2(request.getParameter("BranchType2"));
    tLAAgentSchema.setName(request.getParameter("Name"));
    tLAAgentSchema.setSex(request.getParameter("Sex"));
    tLAAgentSchema.setBirthday(request.getParameter("Birthday"));
    tLAAgentSchema.setIDNo(request.getParameter("IDNoType"));
    tLAAgentSchema.setIDNo(request.getParameter("IDNo"));
    tLAAgentSchema.setIDNoType("0");
    tLAAgentSchema.setNativePlace(request.getParameter("NativePlace"));
    tLAAgentSchema.setNationality(request.getParameter("Nationality"));
    tLAAgentSchema.setPolityVisage(request.getParameter("PolityVisage"));
    tLAAgentSchema.setRgtAddress(request.getParameter("RgtAddress"));
    tLAAgentSchema.setDegree(request.getParameter("Degree"));
    tLAAgentSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
    tLAAgentSchema.setSpeciality(request.getParameter("Speciality"));
    tLAAgentSchema.setPostTitle(request.getParameter("PostTitle"));
    tLAAgentSchema.setHomeAddress(request.getParameter("HomeAddress"));
    tLAAgentSchema.setZipCode(request.getParameter("ZipCode"));
    tLAAgentSchema.setPhone(request.getParameter("Phone"));
    tLAAgentSchema.setBP(request.getParameter("BP"));
    tLAAgentSchema.setMobile(request.getParameter("Mobile"));
    tLAAgentSchema.setEMail(request.getParameter("EMail"));
    tLAAgentSchema.setOldCom(request.getParameter("OldCom"));
    tLAAgentSchema.setOldOccupation(request.getParameter("OldOccupation"));
    tLAAgentSchema.setHeadShip(request.getParameter("HeadShip"));
    tLAAgentSchema.setDevGrade(request.getParameter("DevGrade"));
    tLAAgentSchema.setTrainDate(request.getParameter("TrainDate"));
    tLAAgentSchema.setTrainPeriods(request.getParameter("TrainPeriods"));
    tLAAgentSchema.setEmployDate(request.getParameter("EmployDate"));
    tLAAgentSchema.setAgentState(request.getParameter("AgentState"));
    tLAAgentSchema.setOperator(request.getParameter("Operator"));
    tLAAgentSchema.setInsideFlag(request.getParameter("InsideFlag"));   


    tLAQualificationSchema.setQualifNo(request.getParameter("QualifNo"));
    tLAQualificationSchema.setGrantUnit(request.getParameter("GrantUnit"));
    tLAQualificationSchema.setGrantDate(request.getParameter("GrantDate"));
    tLAQualificationSchema.setValidStart(request.getParameter("ValidStart"));
    tLAQualificationSchema.setValidEnd(request.getParameter("ValidEnd"));
    tLAQualificationSchema.setState(request.getParameter("QualifState"));
System.out.println(tLAQualificationSchema.getQualifNo()+"\\\\"+tLAQualificationSchema.getGrantUnit());
    

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  
  tVData.add(tG);
  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLAQualificationSchema);
  try
  {
    System.out.println("this will save the data!!!");
    tLAMedComAgentUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
    ex.printStackTrace();
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAMedComAgentUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println(Content);
System.out.println(FlagStr);
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
