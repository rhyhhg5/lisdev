<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="LAGroupCodeQueryInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LAGroupCodeQueryInit.jsp"%>
</head>
<body onload="initForm();">
<form action="LAGroupCodeQuerySave.jsp" method=post name=fm
	target="fraSubmit">
<title></title>

<table class=common border=0 width=100%>
	<tr>
		<td class=titleImg align=center>请输入查询条件：</td>
	</tr>
</table>
<table class=common align='center'>
	<TR class=common>
		<TD class=title>管理机构</TD>
		<TD class=input><Input class="codeno" name="ManageCom"
			verify="管理机构|notnull"
			ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><Input
			name="ManageComName" class="codename" verify="姓名|NotNull&len<=15" elementtype=nacessary>
		</TD>
		<TD class=title>业务员编码</TD>
		<TD class=input><Input class=common name="AgentCode"></TD>
	</TR>
	<TR class=common>
		<TD class=title>业务员姓名</TD>
		<TD class=input><Input class=common name="Name"></TD>
		<TD class=title>人员状态</TD>
		<TD class=input><Input name="AgentState" class='codeno'
			maxlength=2
			ondblclick="return  showCodeList('AgentWorkState',[this,AgentStateName],[0,1],null,null,null,1);"
			onkeyup="return showCodeListKey('AgentWorkState',[this,AgentStateName],[0,1],null,null,null,1);"><Input
			name="AgentStateName" class="codename"></TD>
	</TR>
	<TR class=common>
		<TD class=title>业务渠道</TD>
		<TD class=input><Input name="BranchTypeCode" class='codeno'
			ondblclick="return  showCodeList('BranchTypeCode',[this,BranchTypeCodeName],[0,1],null,null,null,1);"
			onkeyup="return showCodeListKey('BranchTypeCode',[this,BranchTypeCodeName],[0,1],null,null,null,1);"><Input
			name="BranchTypeCodeName" class="codename"></TD>
		<TD class=title>集团工号</TD>
		<TD class=input><Input class=common name="GroupAgentCode"></TD>
	</TR>

	<INPUT VALUE="查  询" class=cssButton TYPE=button
		onclick="return easyQueryClick();">
	<INPUT VALUE="清  空" class=cssButton TYPE=button
		onclick="return clearData();">
	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden name=BranchType value=''>
	<input type=hidden name=BranchType2 value=''>
</table>
	<input type =button class=cssButton value="打 印" onclick="submitForm();">
	<input type =button class=cssButton name="but" value="获取集团工号" onclick="getGroupAgentCode();">
	<input type=hidden class=Common name=querySql > 
    <input type=hidden class=Common name=querySqlTitle > 
    <input type=hidden class=Common name=Title >
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLAGroupCodeQueryBox);">
		</td>
		<td class=titleImg>信息</td>
	</tr>
</table>
<!-- 信息（列表） -->
<Div id="divLAGroupCodeQueryBox" style="display:''" align=center>
<table class=common>
	<tr class=common>
		<td text-align:left colSpan=1><span
			id="spanLAGroupCodeQueryBoxGrid"> </span></td>
	</tr>
</table>
</div>

<Div id="divPage" align=center style="display: 'none' "><INPUT
	CLASS=cssButton VALUE="首  页" TYPE=button
	onclick="turnPage.firstPage(); showCodeName();"> <INPUT
	CLASS=cssButton VALUE="上一页" TYPE=button
	onclick="turnPage.previousPage(); showCodeName();"> <INPUT
	CLASS=cssButton VALUE="下一页" TYPE=button
	onclick="turnPage.nextPage(); showCodeName();"> <INPUT
	CLASS=cssButton VALUE="尾  页" TYPE=button
	onclick="turnPage.lastPage(); showCodeName();"></Div>
</form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
