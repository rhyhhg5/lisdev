   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var cName = "";
var strSql = "";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
 
function submitForm()
{ 
 fm.all('Calculate').disabled = true ;	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
	
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    fm.Calculate.disabled = false ;
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
    fm.Calculate.disabled = false ; 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行提数         
function LATrainerWageGatherSave()
{
 	if(!verifyInput()) 
	  { 
	  	return false; 
	  }	
 	
 	 //校验是否该月个险薪资计算是否已经审核发放
 	  var tWageNo = fm.WageNo.value;
 	  var strSQ = "select 1 from lawagehistory  " 
 	               +"where  state='14' and wageno= '"+tWageNo+"'"
 	               +getWherePart('ManageCom','ManageCom')
 	               +getWherePart('BranchType','BranchType')
 	               +getWherePart('BranchType2','BranchType2');
 	  //判断是否查询成功
 	var strQueryResult  = easyQueryVer3(strSQ, 1, 1, 1);
 	if(!strQueryResult)
 	{   alert("个险薪资还未审核发放，不能计算组训薪资!");
 	    return false;
 	} 	
 	
 // 校验是否该月佣金已算过
	var strSQ = "select state from latrainerWagehistory where 1=1 "
			+ getWherePart('WageNo', 'WageNo')
			+ getWherePart('ManageCom', 'ManageCom')
			+ getWherePart('BranchType', 'BranchType')
			+ getWherePart('BranchType2', 'BranchType2');
	// 判断是否查询成功
	var strQueryResult = easyQueryVer3(strSQ, 1, 1, 1);
	if (!strQueryResult) {
		alert("该月组训新单标准保费提数计算还未进行!");
		return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	var tState = arr[0][0];

	 if (tState == "11") {
		alert("佣金计算正在进行中");
		return false;
	}else if (tState == "13") {
		alert("佣金计算已经确认！");
		return false;
	}
 //校验是否该月组训佣金计算并发量
  var tWageNo = fm.all('WageNo').value;
  var sql =" select count(*) from laTrainerWagehistory  " 
          +" where wageno= '"+tWageNo+"' and BranchType='1' and BranchType2='01' and state='11' " ;
  var str  = easyQueryVer3(sql, 1, 1, 1);
  var arr = decodeEasyQueryResult(str); 
  var tCount=arr[0][0] ;
  if (tCount>=10) 
  { 
    alert("现已有10个支公司在进行薪资试算，为不影响系统效率，请稍后再进行试算，谢谢合作！");
    fm.Calculate.disabled = true ;
    return false;
  }	
  submitForm();	
}  

//执行查询状态
function AgentWageStateQuery()
{
	if (!verifyInput()) {
		return false;
	}

	// 校验是否该月佣金已算过
	var strSQ = "select state from latrainerWagehistory  " + "where 1=1 "
			+ getWherePart('WageNo', 'WageNo')
			+ getWherePart('ManageCom','ManageCom')
			+ getWherePart('BranchType', 'BranchType')
			+ getWherePart('BranchType2', 'BranchType2');
	// 判断是否查询成功
	var strQueryResult = easyQueryVer3(strSQ, 1, 1, 1);
	if (!strQueryResult) {
		alert("该月组训新单标准保费提数计算还未进行!");
		return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	var tState = arr[0][0];

	if (tState == "00") {
		alert("佣金还没有计算,可以进行计算！");
		return false;
	} else if (tState == "11") {
		alert("佣金计算正在进行中");
		return false;
	}else if (tState == "12") {
		alert("佣金已经计算，可以重新计算！");
		return false;
	}else if (tState == "13") {
		alert("佣金计算已经确认！");
		return false;
	}
}               

