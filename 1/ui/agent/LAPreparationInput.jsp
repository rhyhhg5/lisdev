<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAPreparationInput.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    String MakeType= request.getParameter("MakeType");
    if (BranchType==null || BranchType.equals(""))
    {
       BranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
%>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAPreparationInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAPreparationInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAPreparationSave.jsp" method=post name=fm target="fraSubmit">  
   <table>
    <tr class=common with = 100%>
     <td class=common>
     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription);">
     <td class=titleImg>
      查询条件
     </td>
     </td> 
    </tr>
   </table>
  <Div  id= "divLAAscription" style= "display: ''">      
    <table  class= common>
     <TR  class= common>      
      <TD  class= title>
       管理机构
      </TD>
      <TD  class= input>
       <Input class="codeno" name=ManageCom verify="管理机构|notnull&code:comcode&len>7"
        ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
        onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
       ><Input class="codename" name=ManageComName   readOnly elementtype=nacessary> 
      </TD> 
      <TD class=title>
      	团队编码 
      </TD>
      <TD class=input>
       <input class=common name=BranchAttr >
      </TD>
     </TR>
     <TR  class= common> 
      <TD  class= title>
        人员编码
      </TD>          
      <TD  class= input>
       <Input class=common name=AgentCode >
      </TD>  
      <TD  class= title>
		   人员姓名
		  </TD>
      <TD  class= input>
		   <input name=Name class= common >
		  </TD> 
		 </TR> 
		 <TR  class= common> 
      <TD class= title>
     	 筹备标记
      </TD>
		  <TD class= input>
				<Input class=codeno readOnly name=NoWorkFlag verify="筹备标记" CodeData="0|^Y|是"
				 ondblClick="showCodeListEx('noworkflag',[this,NoWorkFlagName],[0,1],null,null,null,1);  "
         onkeyup="showCodeListKeyEx('noworkflag',[this,NoWorkFlagName],[0,1],null,null,null,1);"
         ><Input class=codename name=NoWorkFlagName  readOnly >
			</TD>      
		 </TR> 
      <TD>
       <input type=button class=cssbutton value='查  询' onclick="return agentConfirm()">
      </TD>       
    </table>  
    <p> <font color="#ff0000">注：计算薪资时,按当时筹备标记及筹备开始日期为准 。(2009A代表2009版筹备基本法 ，2010A代表2010版筹备基本法 )</font></p> 
    <p> <font color="#ff0000">注：转为正式人员时，当前年月>筹备期满年月 且 当前最大薪资月>=筹备期满年月 </font></p> 
    <table>
    <TR class=common>
    <TD class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription2);">
    <TD class=titleImg>
      查询结果
    </TD>
    </TD>
    </TR>
    </table>
  <Div  id= "divLAAscription2" style= "display: ''">
    <table  class= common>
     <tr  class= common>
      <td text-align: left colSpan=1> 
		  <span id="spanAscriptionGrid" >
		  </span> 
      </td>
  	 </tr>
    </table>
    <Div id="divPage" style= "display: ''">
      <INPUT class=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">	
    </Div>
    <p>   
    <input type=button class=cssbutton name=saveb value='选择确认' onclick="submitSaveb();"> 
    <!--input type=button class=cssbutton name=saveall value='全部确认' onclick="submitSaveall();"-->            
    </p>
    </Div>   
   </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=Flag value=''>
    <input type=hidden name=MakeType value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
