 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{ 
	if(!verifyInput())	 return false;
  if(!checkValue())    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	fm.reset;
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    if (fm.action == './LAAscriptionSave.jsp')     
      saveClick=true;
  }
}


          


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function agentConfirm()
{	
 if(!verifyInput())
 {
 	return false;
 }		

  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  var tAgentCode = fm.all('AgentCode').value;
  
  initAscriptionGrid(); 
  var tReturn = parseManageComLimitlike();
  strSQL="select '',a.managecom,b.branchattr,a.agentcode,a.name,a.employdate,"
  +"a.noworkflag,a.traindate,c.agentgrade,a.wageversion "
  +",(select max(indexcalno) from lawage  where agentcode=a.agentcode and state='1' ),"
  +"case when c.agentgrade<='A07' then "
  +"case  when day(a.traindate)<16 then DATE_FORMAT((date(a.traindate)+ 5 month),'yyyymm') "
  +"else  DATE_FORMAT((date(a.traindate)+ 6 month),'yyyymm')  end  "  
  +"else "
  +"case  when day(a.traindate)<16 then  DATE_FORMAT((date(a.traindate)+ 2 month),'yyyymm') " 
  +"else  DATE_FORMAT((date(a.traindate)+ 3 month),'yyyymm')  end  end " 
  +",DATE_FORMAT(current date,'yyyymm')"
  +" from laagent a,labranchgroup b,latree c "
  +" where a.agentgroup=b.agentgroup  and a.agentcode=c.agentcode "
  +" and a.branchtype='1' and a.branchtype2='01' "
  +" and b.branchtype='1' and b.branchtype2='01' "
  +" and a.wageversion='2010A' "
  +getWherePart('a.ManageCom','ManageCom')
  +getWherePart('b.BranchAttr','BranchAttr')
  +getWherePart('a.AgentCode','AgentCode')
  +getWherePart('a.Name','Name')
  +getWherePart('a.NoWorkFlag','NoWorkFlag')
  +" union select '',a.managecom,b.branchattr,a.agentcode,a.name,a.employdate,"
  +"a.noworkflag,a.traindate,c.agentgrade,a.wageversion "
  +",(select max(indexcalno) from lawage  where agentcode=a.agentcode and state='1' ),"
  +"case  when c.agentgrade<='A07' then "
  +"case  when day(a.traindate)<16 then DATE_FORMAT((date(a.traindate)+ 2 month),'yyyymm') "
  +"else  DATE_FORMAT((date(a.traindate)+ 3 month),'yyyymm')  end  "  
  +" when c.agentgrade<='B03' then case when day(a.traindate)<16 then DATE_FORMAT((date(a.traindate)+ 5 month),'yyyymm') "
  +"else  DATE_FORMAT((date(a.traindate)+ 6 month),'yyyymm')  end  "  
  +"else "
  +"case  when day(a.traindate)<16 then  DATE_FORMAT((date(a.traindate)+ 8 month),'yyyymm') " 
  +"else  DATE_FORMAT((date(a.traindate)+ 9 month),'yyyymm')  end  end " 
  +",DATE_FORMAT(current date,'yyyymm')"
  +" from laagent a,labranchgroup b,latree c "
  +" where a.agentgroup=b.agentgroup  and a.agentcode=c.agentcode "
  +" and a.branchtype='1' and a.branchtype2='01' "
  +" and b.branchtype='1' and b.branchtype2='01' "
  +" and a.wageversion='2009A' "
  +getWherePart('a.ManageCom','ManageCom')
  +getWherePart('b.BranchAttr','BranchAttr')
  +getWherePart('a.AgentCode','AgentCode')
  +getWherePart('a.Name','Name')
  +getWherePart('a.NoWorkFlag','NoWorkFlag');  
    
  
  turnPage.queryModal(strSQL, AscriptionGrid); 
  //alert(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据或者查询出错");
    return ;
    }

}   
function submitSave()
{  
   var lineCount = AscriptionGrid.mulLineCount;
   var str='';

   submitForm();
   return ;
}
function submitSaveb()
{
	fm.all("Flag").value="SELECT";
  tSel=false;
	var lineCount = AscriptionGrid.mulLineCount;
	for( i=0;i<lineCount;i++)
	{
	 tSel = AscriptionGrid.getChkNo(i);
	 if(tSel==true)
	 i=lineCount;
  }
	if( tSel == false || tSel == null )
	alert( "请先选择一条记录!" );
	else
	{			
	submitSave();
	}
}
function submitSaveall()
{
	fm.all("Flag").value="ALL";
	submitSave();
}

function clearMulLine()
{  
   AscriptionGrid.clearData("AscriptionGrid");
   saveClick=false;
}

function checkValue()
{
	var countNum=0;
	var selFlag=true;
	if(AscriptionGrid.mulLineCount==0)
	{
		alert('没有选择要操作的记录！');
		return false;
	}
	for(var i=0;i<AscriptionGrid.mulLineCount;i++)
	{　
		if(AscriptionGrid.getChkNo(i))
		{
			countNum++;
			var tagentcode=AscriptionGrid.getRowColData(i,3).trim();
			var tmaxcalmonth=AscriptionGrid.getRowColData(i,11).trim();
			var ttrainendmonth=AscriptionGrid.getRowColData(i,12).trim();
			var tcurrentmonth=AscriptionGrid.getRowColData(i,13).trim();
			  /*当前年月>筹备期满年月 且 当前最大薪资月>=筹备期满年月*/
			if(tcurrentmonth>ttrainendmonth &&tmaxcalmonth>=ttrainendmonth)
      {
        continue;
      }
			else
			{
				alert("第"+(i+1)+"条纪录的人员没有达到筹备期满，或者薪资没有计算完毕，无法进行处理！");
        AscriptionGrid.checkBoxAllNot();
        AscriptionGrid.setFocus(i,2,AscriptionGrid);
        selFlag = false;
        break;
			}
		}
	}
	if(!selFlag)
	{
   	  return selFlag;
   	}
	if(countNum==0)
	{
		alert("请选择进行操作的记录。");
		return false;
	}
	return true 
}