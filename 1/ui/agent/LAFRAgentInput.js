//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//查询人员
function EdorType(cObj,dobj)
{
	var tManageCom=fm.all('ManageCom').value;	
	var tBranchType = fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# ";  
	showCodeList('Agentcode',[cObj,dobj], [0,1], null, mEdorType, "1");
}
//查询人员
function KeyUp(cObj,dobj)
{
	var tManageCom=fm.all('ManageCom').value;	
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;  
	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# ";     
	showCodeListKey('Agentcode',[cObj,dobj], [0,1], null, mEdorType, "1");        
}

function EdorType1(cObj,dobj)
{
	var tManageCom=fm.all('ManageCom').value;	
	var tBranchType = fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# ";  
	showCodeList('AgentName',[cObj,dobj], [0,1], null, mEdorType, "1");
}

function KeyUp1(cObj,dobj)
{
	var tManageCom=fm.all('ManageCom').value;	
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;  
	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# ";     
	showCodeListKey('AgentName',[cObj,dobj], [0,1], null, mEdorType, "1");        
}
//查询类型
function initEdorType(cObj){
	mEdorType = " 1 and codealias=#2# ";
	showCodeList('agentkind',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{
	mEdorType = " 1 and codealias=#2#";
	showCodeListKey('agentkind',[cObj], null, null, mEdorType, "1");
}



//提交，保存按钮对应操作
function submitForm(){
  if (mOperate!='DELETE||MAIN')
  {
		if (mOperate!="UPDATE||MAIN")
			mOperate="INSERT||MAIN";
		if (!beforeSubmit())
			return false;	
  }
  if (mOperate=='INSERT||MAIN'){
		if (fm.all('AgentCode').value == ""||fm.all('AgentCode').value==null )
		{
		    if (confirm("您确实想增加该记录吗?"))
    		{
			}
			else
			{
			 alert("您取消了操作");
	         if(fm.all('AgentCode').value!=null && fm.all('AgentGrade').value!="")
	         {
		       fm.all('AgentGrade').disabled = true;
		       fm.all('AgentGroup').disabled = true;
		       fm.all('TutorShip').disabled = true;
		       fm.all('TutorShipName').disabled = true;
	         }
		     return false;
			 }
		}
		else
		{
		   alert("人员编码已经存在，不能进行保存操作!");
	       return false;	
	    }
  }
  
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
 //fm.all('AgentKind').disabled = true;
  fm.all('AgentGrade').disabled = true;
  fm.all('AgentGroup').disabled = true;
  fm.all('TutorShip').disabled = true;
  fm.all('TutorShipName').disabled = true;
  fm.reset();
  initInpBox();
  showInfo.close();
  fm.all('hideIsManager').value = false;
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
	  initForm();
  }
  catch(re)
  {
  	alert("在LAFRAgentInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit(){
	//添加操作
	//zff  身份证尾号不能为小x的校验
	if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}
	if( verifyInput() == false ) return false;
	//新增校验联系电话
	var Phone = fm.all('Phone').value.trim();
	var result=CheckFixPhoneNew(Phone);
	var result2=CheckPhoneNew(Phone);
    if(result!=""&&result2!=""){
	  alert("联系电话不符合规则，请录入正确的固话或手机号！");
	  fm.all('Phone').value='';
	  return false;
	}
    //新增校验手机号
	  var Mobile = fm.all('Mobile').value.trim();
	  if(Mobile!=null&&Mobile!="")
	    { 
		  var result=CheckPhoneNew(Mobile);
	      if(result!=""){
		  alert(result);
		  fm.all('Mobile').value='';
		  return false
		  }
	   }
	if ( mOperate=='INSERT||MAIN')
	{
	//校验身份证号是否存在
	if(!changeIDNo() )  return false;	
    }
	var strReturn = checkIdNo('0',trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
	if (strReturn != ''){
		alert(strReturn);
		return false;
	}
	if(fm.all("TutorShipName").value!=null && fm.all("TutorShipName").value!='')
	{
	 if(fm.all("TutorShip").value==null || fm.all("TutorShip").value=='')
	 {
	  alert("辅导人姓名存在，必须录入辅导人代码,请查询并双击输入框选择!");
	  return false;
	  }
	}
	if(fm.all("InsideFlag").value==null || fm.all("InsideFlag").value=='')
	{
	fm.all("InsideFlag").value='1';
	}
	if (fm.all('BranchType').value=='2' &&　fm.all('BranchType2').value=='01' && mOperate=="INSERT||MAIN" ){
		//alert("****");
	var strsql="select managecom from labranchgroup where branchattr='"+fm.all('AgentGroup').value+"'";
	    var strQueryResult1=easyQueryVer3(strsql, 1, 1, 1);
	    var arr1= decodeEasyQueryResult(strQueryResult1);
	    // alert("ppppppp:"+arr1[0][0]);	
	var strSQL2 = "select agentstate  from LAAgent where 1=1 "
	   +"and branchtype='2' and branchtype2='01' and idno='"+fm.all('IDNo').value+"' and managecom='"+arr1[0][0]+"'";
	//alert("zzzzzzzzzzzzz");	
	var strQueryResult2  = easyQueryVer3(strSQL2, 1, 1, 1);
	//alert("wwwwwwwwww");	
	if (strQueryResult2)
	{  // alert("TTTTTTT");
	var arr= decodeEasyQueryResult(strQueryResult2);
	if(arr[0][0]>='06'){
		//alert("QQQQQQQQQ");
	  	alert("此人在该机构做过离职，请在二次增员界面进行增员操作！");
	  	fm.all('IDNo').value = '';
	 	return false;
	  }
	}
}
	//add by zhuxt 20140903
//	if(fm.all('AgentType').value == '1') 
//	{
//		if(fm.all('RetainContno').value == null || fm.all('RetainContno').value == '') 
//		{
//			alert("销售人员类型为代理制，必须录入委托代理合同号！");
//			fm.RetainContno.focus();
//			return false;
//		}
//	}
	fm.GrpFlag.value="2";
//	fm.all('AgentKind').disabled = false;
	fm.all('AgentGrade').disabled = false;
	fm.all('AgentGroup').disabled = false;	
	fm.all('TutorShip').disabled = false;
    fm.all('TutorShipName').disabled = false;
	return true;
}
  

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,82,*";
	}
}


//Click事件，当点击增加图片时触发该函数
function addClick(){
	//下面增加相应的代码
	mOperate="INSERT||MAIN";
	showDiv(operateButton,"false");
	showDiv(inputButton,"true");
	fm.all('AgentCode').value = '';
	fm.all('IDNo').value = '';
	fm.all('AgentGrade').value = '';
	fm.all('AgentGroup').value = '';
	fm.all('ManageCom').value = '';
	fm.all('UpAgent').value = '';	
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的代理人记录！');
  }
  else
  {
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";
    var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
    showInfo=window.open("./LABankAgentQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要删除的代理人记录！');
  }
  else
  {
  //下面增加相应的删除代码
   if (confirm("您确实想删除该记录吗?"))
   {
     mOperate="DELETE||MAIN";
     submitForm();
   }
   else
   {
     mOperate="";
     alert("您取消了删除操作！");
   }
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function checkTutorShip()
{
   var strSQL = "";
   strSQL = "select getUniteCode(agentcode) from laagent where  (AgentState is null or AgentState < '03') " 
          + getWherePart('groupAgentCode','TutorShip')
          + getWherePart('BranchType','BranchType')
           + getWherePart('BranchType2','BranchType2');
           
   var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
   if (!strQueryResult)
   {
    alert('没有此代理人');
    fm.all('TutorShip').value = '';	
    return false;
   }	
   return true;	
}

function changeGroup()
{
   if (getWherePart('AgentGroup')=='')     return false;
   var tAgentGrade = trim(fm.all('AgentGrade').value);
   if (tAgentGrade==null ||tAgentGrade=='')
   {
     alert('请先录入职级！');
     fm.all('AgentGroup').value = '';
     return false;
   }
   var strSQL = "";
   strSQL = "select a.BranchAttr,a.ManageCom,getUniteCode(a.BranchManager),a.AgentGroup,a.BranchLevel,a.BranchJobType,"
           +" (select getUniteCode(b.BranchManager) from labranchGroup b "
           +" where b.AgentGroup = a.UpBranch and (b.state<>'1' or b.state is null)) upAgent "
           +" from LABranchGroup a "
           +" where 1=1 and a.BranchType = '"+fm.all('BranchType').value+"' and a.BranchType2='"+fm.all('BranchType2').value+"'"
           +" and a.EndFlag <> 'Y' and (a.state<>'1' or a.state is null)"
           + getWherePart('a.BranchAttr','AgentGroup');
   //alert(strSQL);
   var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
   if (!strQueryResult)
   {
   	alert('不存在该销售机构！');
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('ManageCom').value = '';
   	fm.all('BranchManager').value = '';
   	//fm.all('hideManageCom').value = '';
   	fm.all('hideAgentGroup').value = '';
   	fm.all('BranchLevel').value = '';
   	return false;
   }
   var arr = decodeEasyQueryResult(strQueryResult);
   
   //外勤AgentGrade为D,E系列
   if(trim(arr[0][4]) == '21')
   {	
	 if(tAgentGrade.substr(0,1) != 'D' && tAgentGrade.substr(0,1) != 'E')
	 {
   	  alert('销售机构级别与职级不对应！');
   	  fm.all('AgentGroup').value = '';
   	  fm.all('UpAgent').value = '';
   	  fm.all('ManageCom').value = '';
   	  fm.all('BranchManager').value = '';
   	  fm.all('hideAgentGroup').value = '';
   	  fm.all('BranchLevel').value = '';
   	  return false;
	 }
	 if(tAgentGrade >= 'E01')
	 {
		if(trim(arr[0][2]) != '' && trim(arr[0][2]) != null)
		{
	   	alert('销售机构已经有经理！');
	   	fm.all('AgentGroup').value = '';
	   	fm.all('UpAgent').value = '';
	   	fm.all('ManageCom').value = '';
	   	fm.all('BranchManager').value = '';
	   	//fm.all('hideManageCom').value = '';
	   	fm.all('hideAgentGroup').value = '';
	   	fm.all('BranchLevel').value = '';
	   	return false;
		}
		else
		{
			fm.hideIsManager.value = 'true';
		}
	 }
	 if(tAgentGrade <= 'D10')
	 {
     }
   }
  //内勤AgentGrade为F系列
   if(trim(arr[0][4]) == '22')
   {
	if(tAgentGrade.substr(0,1) != 'F')
	{
   	alert('销售机构级别与职级不对应！');
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('ManageCom').value = '';
   	fm.all('BranchManager').value = '';
   	//fm.all('hideManageCom').value = '';
   	fm.all('hideAgentGroup').value = '';
   	fm.all('BranchLevel').value = '';
   	return false;
	}
   }

//   var tAgentKind = trim(fm.all('AgentKind').value);
   fm.all('AgentGroup').value = trim(arr[0][0]);   
   
   fm.all('UpAgent').value = trim(arr[0][6]);
     //如果经理
   if (fm.hideIsManager.value == 'true')
     fm.all('UpAgent').value = "";
   else//组员
     fm.all('UpAgent').value = trim(arr[0][2]);

   fm.all('ManageCom').value = trim(arr[0][1]);
   fm.all('BranchManager').value = trim(arr[0][2]);
   //fm.all('hideManageCom').value = arr[0][1];
   fm.all('hideAgentGroup').value = trim(arr[0][3]);
   fm.all('BranchLevel').value = trim(arr[0][4]);
   return true;
}

function afterQuery(arrQueryResult){
	var arrResult = new Array();

	if( arrQueryResult != null ){
		arrResult = arrQueryResult;
		fm.all('AgentCode').value = arrResult[0][0];
		fm.all('Name').value = arrResult[0][1];
		fm.all('Sex').value = arrResult[0][2];
	        var Sql_SexName="select codename from ldcode where codeType='sex' and code='"+fm.all('Sex').value+"' ";
                var strQueryResult_SexName  = easyQueryVer3(Sql_SexName, 1, 1, 1);
		if (strQueryResult_SexName)
	        {
		  var arr = decodeEasyQueryResult(strQueryResult_SexName);
		  fm.all('SexName').value= trim(arr[0][0]) ;
		 }
		fm.all('Birthday').value = arrResult[0][3];
		fm.all('NativePlace').value = arrResult[0][4];
		fm.all('Nationality').value = arrResult[0][5];
		if(fm.all('Nationality').value!="" && fm.all('Nationality').value!=null)
	        {
		  var Sql_NationalityName="select codename from ldcode where codeType='nationality' and code='"+fm.all('Nationality').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
		  {
		    var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
		    fm.all('NationalityName').value= trim(arr[0][0]) ;
		  }
		 }
		
		fm.all('HomeAddress').value = arrResult[0][7];
		fm.all('ZipCode').value = arrResult[0][8];
		fm.all('Phone').value = arrResult[0][9];
		fm.all('BP').value = arrResult[0][10];
		fm.all('Mobile').value = arrResult[0][11];
		fm.all('EMail').value = arrResult[0][12];
		fm.all('IDNo').value = arrResult[0][13];
		fm.all('hideIdNo').value = arrResult[0][13];//身份证号校验
		fm.all('PolityVisage').value = arrResult[0][14];
		fm.all('Degree').value = arrResult[0][15];
		fm.all('GraduateSchool').value = arrResult[0][16];
		fm.all('Speciality').value = arrResult[0][17];
		fm.all('PostTitle').value = arrResult[0][18];
		fm.all('OldCom').value = arrResult[0][19];
		fm.all('OldOccupation').value = arrResult[0][20];
		fm.all('HeadShip').value = arrResult[0][21];
		// fm.all('QuafNo').value = arrResult[0][37];
		//fm.all('QuafEndDate').value = arrResult[0][39];
		// fm.all('TrainPeriods').value = arrResult[0][73];
		fm.all('EmployDate').value = arrResult[0][22];
		fm.all('hideEmployDate').value = arrResult[0][22];
		fm.all('AgentState').value = arrResult[0][23];
		fm.all('RgtAddress').value = arrResult[0][6];
		if(fm.all('RgtAddress').value!="" && fm.all('RgtAddress').value!=null)
		{
		  var Sql_NationalityName="select codename from ldcode where codeType='nativeplacebak' and code='"+fm.all('RgtAddress').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
		  {
		     var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
		     fm.all('RgtAddressName').value= trim(arr[0][0]) ;
		  }
		 }
	//fm.all('BankCode').value = arrResult[0][65];
		//fm.all('BankAccNo').value = arrResult[0][66];
		fm.all('Remark').value = arrResult[0][24];		
		fm.all('Operator').value = arrResult[0][67];
		fm.all('ManageCom').value = arrResult[0][27];
//79c.BranchManager,80b.AgentGrade,81c.BranchAttr,82c.AgentGroup,83b.upAgent,
//84c.BranchLevel,85c.name,86b.Agentkind,87b.AgentSeries,88c.branchmanagername
		//行政信息
		fm.all('UpAgent').value = arrResult[0][28];
		fm.all('AgentGrade').value = arrResult[0][29];
		var grade = trim(arrResult[0][34]);
		if(grade == '0')
		{
			//fm.all('AgentKind').value = 'D';
		}
		else
		{
			//fm.all('AgentKind').value = 'E';
		}		
		
		fm.all('InDueFormDate').value = arrResult[0][44];
		fm.all('InitGrade').value = arrResult[0][43];
		fm.all('OldStartDate').value = arrResult[0][45];
		fm.all('OldEndDate').value = arrResult[0][46];
		fm.all('AgentLastGrade').value = arrResult[0][47];
		fm.all('AgentLastSeries').value = arrResult[0][48];
		fm.all('InsideFlag').value = arrResult[0][49];
		if(fm.all('InsideFlag').value=="" || fm.all('InsideFlag').value==null)
		  {
		  fm.all('InsideFlag').value='1';	
		  }
		//add by zhuxt 20140903
		fm.all('AgentType').value = arrResult[0][50];
		if(fm.all('AgentType').value != null && fm.all('AgentType').value != '') 
		{
			var typeSQL = "select codename from ldcode where codetype = 'agenttypecode' and code = '" + fm.all('AgentType').value + "'";
			var typeQueryResult= easyQueryVer3(typeSQL, 1, 1, 1);
			if(typeQueryResult){
				var typeArr = decodeEasyQueryResult(typeQueryResult);
				fm.all('AgentTypeName').value = trim(typeArr[0][0]);
			}
		}
		fm.all('RetainContno').value = arrResult[0][51];
		//end add
		//显式机构代码	
		fm.all('AgentGroup').value = arrResult[0][35];
		fm.all('TutorShip').value = arrResult[0][36];
		//alert(fm.all('TutorShip').value=="");
		if(fm.all('TutorShip').value==""||fm.all('TutorShip').value==null){
			fm.all('TutorShipName').value ="";	
			//alert(fm.all('TutorShipName').value);
		}else{
			fm.all('TutorShipName').value = arrResult[0][41]
		}
		fm.all('InDueFormFlag').value = arrResult[0][37];
		if(fm.all('InDueFormFlag').value=='N'){
			fm.all('InDueFormName').value='未转正';		
		}	else
		{
			 fm.all('InDueFormFlag').value = 'Y';
			 fm.all('InDueFormName').value='已转正';	
			 var Sql="select indueformdate,modifydate,operator from laagent where  groupagentcode='"+fm.all('AgentCode').value+"' ";
             var strQueryResult= easyQueryVer3(Sql, 1, 1, 1);
		     if (strQueryResult)
			 {
				var arr = decodeEasyQueryResult(strQueryResult);
			    fm.all('IndueFormDate1').value= trim(arr[0][0]) ;
                var sql1="select modifydate2,operator2 from latreeb "
                +"where agentcode=getAgentCode('"+fm.all('AgentCode').value+"')"
                +" and indueformdate='"+fm.all('IndueFormDate1').value+"'"
                +" order by modifydate2 asc fetch first 1 rows only ";
			    var strResult=easyQueryVer3(sql1, 1, 1, 1);
			    if (strResult)
			    {
			     var arr1=decodeEasyQueryResult(strResult);
			     fm.all('FormDateModi').value= trim(arr1[0][0]) ;
			     fm.all('FormDateOper').value= trim(arr1[0][1]) ;
			    }
			    else {
			    fm.all('FormDateModi').value= trim(arr[0][1]) ;
			    fm.all('FormDateOper').value= trim(arr[0][2]) ;
			    }
		     }	 
		}	
		fm.all('GrpFlag').value = arrResult[0][38];
		if(fm.all('GrpFlag').value=='2'){
		//fm.all('GrpFlagName').value='职团开拓';		
		}
		else
		{
			 fm.all('GrpFlag').value='1';
			 //fm.all('GrpFlagName').value='常规业务员';
		}	
		//fm.UpAgent.value = arrResult[0][87];
		//xjh Add For PICC ,禁用行政信息修改,因为在这里不能修改
		//fm.all('AgentKind').disabled = true;
		fm.all('AgentGrade').disabled = true;
		fm.all('AgentGroup').disabled = true;
		fm.all('TutorShip').disabled = true;
		fm.all('EmployDate').disabled = true;
		//showOneCodeNametoAfter('agentcode','TutorShip');
		fm.all('TutorShipName').disabled = true;
		
		if(fm.all('NativePlace').value!="" && fm.all('NativePlace').value!=null)
		  {
		  var Sql_NationalityName="select codename from ldcode where codeType='nativeplacebak' and code='"+fm.all('NativePlace').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
			  fm.all('NativePlaceName').value= trim(arr[0][0]) ;
		  }
		 }
		if(fm.all('PolityVisage').value!="" && fm.all('PolityVisage').value!=null)
		  {
		  var Sql_NationalityName="select codename from ldcode where codeType='polityvisage' and code='"+fm.all('NativePlace').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
			  fm.all('PolityVisageName').value= trim(arr[0][0]) ;
		  }
		 }
		if(fm.all('Degree').value!="" && fm.all('Degree').value!=null)
		  {
		  var Sql_NationalityName="select codename from ldcode where codeType='degree' and code='"+fm.all('Degree').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
			  fm.all('DegreeName').value= trim(arr[0][0]) ;
		  }
		 }
                if(fm.all('PostTitle').value!="" && fm.all('PostTitle').value!=null)
		  {
		  var Sql_NationalityName="select codename from ldcode where codeType='posttitle' and code='"+fm.all('PostTitle').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
			  fm.all('PostTitleName').value= trim(arr[0][0]) ;
		  }
		 }
		if(fm.all('AgentGrade').value!="" && fm.all('AgentGrade').value!=null)
		  {
		  var Sql_NationalityName="select gradename from laagentgrade where  gradecode='"+fm.all('AgentGrade').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
    
		  if (strQueryResult_NationalityName)
			{
				
				var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
			  fm.all('AgentGradeName').value= trim(arr[0][0]) ;
		  }
		 
		 }
		 if(fm.all('InsideFlag').value!="" && fm.all('InsideFlag').value!=null)
		  {
		  var Sql_InsideFlagName="select codename from ldcode where codeType='insideflag' and code='"+fm.all('InsideFlag').value+"' ";
     
                  var strQueryResult_InsideFlagName  = easyQueryVer3(Sql_InsideFlagName, 1, 1, 1);
		  if (strQueryResult_InsideFlagName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_InsideFlagName);
			  fm.all('InsideFlagName').value= trim(arr[0][0]) ;
		  }
		 }	   
	}
	showCodeName();
}
// 身份证信息校验
function changeIDNo()
{
	if(fm.IDNo.value==''||fm.IDNo.value==null){
		alert('新增人员身份证号不能为空！');
		return false;
	}else{
	  // 获得身份证号
      var IDNo = fm.all('IDNo').value;
      // 数据校验 校验系统身份证信息
      strSQL1 = "select agentstate  from LAAgent where 1=1 and idno = '"+IDNo+"'";
      var strQueryResult1  = easyQueryVer3(strSQL1, 1, 1, 1);
      if(strQueryResult1)
        {
          	var arr= decodeEasyQueryResult(strQueryResult1);
          	if(arr[0][0]=='01'||arr[0][0]=='02'||arr[0][0]=='03'||arr[0][0]=='04'||arr[0][0]=='05')
          	{
          	   alert('该代理人已在职或为准离职状态!不能再次做增员处理');
		       fm.all('IDNo').value = '';
		       return false;
          	}
        }     
		return true;
	}
	
}


/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	
	try	{
		if( cCodeName == "AgentGrade" )
		{
			
			//checkvalid();//loadFlag在页面出始化的时候声明
			var tAgentGrade =  fm.all('AgentGrade').value;
			if(tAgentGrade.substr(0,1)== 'D')
			{				
				 fm.all('InDueFormFlag').value = 'N';
				 fm.all('InDueFormName').value = '未转正';
			}else{
				 fm.all('InDueFormFlag').value = 'Y';
				 fm.all('InDueFormName').value = '已转正';
				
			}
		}
	}
	catch( ex ) {
	}
}
function showCodeName()
{
	var tSQL = "";
	var tCode = "";
	 // 资格证信息
	tCode = trim(document.fm.AgentCode.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select QualifNo,GrantUnit,GrantDate,ValidStart,ValidEnd,"
		+"State,Case State WHEN '0' THEN '有效' else '无效' END "
		+" from laqualification where agentcode =  getAgentCode('"+tCode+"')";
		var strQueryResult  = easyQueryVer3(tSQL, 1, 1, 1);
		if (strQueryResult)
		{
			var arr = decodeEasyQueryResult(strQueryResult);
			fm.all('QualifNo').value= trim(arr[0][0]) ;
			fm.all('GrantUnit').value= trim(arr[0][1]) ;
			fm.all('GrantDate').value= trim(arr[0][2]) ;
			fm.all('ValidStart').value= trim(arr[0][3]) ;
			fm.all('ValidEnd').value= trim(arr[0][4]) ;
			fm.all('QualifState').value= trim(arr[0][5]) ;
			fm.all('QualifStateName').value= trim(arr[0][6]) ;
		}
	}
}

function checkvalid()
{
  	fm.InsideFlag.value="";
}
