 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mFlag="0"; 
var mFlag1="0"; 
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
	if(showInfo!=null){
	  try{
	    showInfo.focus();
	  }
	  catch(ex){
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;
  if (beforeSubmit() == false)
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    AgentGrid.clearData("AgentGrid");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
	
}

/**
 *  页面查询操作
 */
function easyQueryClick()
{
  if(!verifyInput()) return false;
  var tAgentState = fm.all('AgentState').value;
  var tChannel = fm.all('Channel').value;
  var tChannelName = fm.all('ChannelName').value;
  var tSpend = "";
  switch(tChannel)
  {
    case '1':
    tSpend =" and branchtype = '1' and branchtype2 = '01'"
    break
    case '2':
    tSpend =" and branchtype = '1' and branchtype2 = '02'"
    break
    case '3':
    tSpend =" and branchtype = '2' and branchtype2 = '01'"
    break
    case '4':
    tSpend =" and branchtype = '2' and branchtype2 = '02'"
    break
    case '5':
    tSpend=" and branchtype = '3' and branchtype2 = '01'"
    break
    case '6':
    tSpend =" and branchtype = '4' and branchtype2 = '01'"
    break
    default:
    alert("业务渠道输入有误！")
  }
  var query_sql = "select managecom ,(select name from ldcom where comcode = laagent.managecom),'"+tChannelName+"',(select branchattr from labranchgroup where agentgroup = laagent.agentgroup),agentcode,name,"
                +" case when agentstate<'06' then '在职' else '离职' end,case when togaeflag = 'Y' then '是' when togaeflag ='N' then '否' else '' end,togaeflag "
                +" from laagent where 1 = 1 and managecom like '"+fm.all('ManageCom').value+"%'"
                +tSpend
                +getWherePart('AgentCode','AgentCode')
                +getWherePart('Name','Name')
                +getWherePart('togaeflag','InteractiveFlag');
        if(fm.all('AgentState').value!=null && fm.all('AgentState').value!='')
          {
               if(tAgentState=="1")
               {
                 query_sql+=" and agentstate <'06'";
               }
               if(tAgentState=="2")
               {
                 query_sql+=" and agentstate >='06'";
               }    
           }
         if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
          {
             query_sql+=" and exists (select 'Y' from labranchgroup where agentgroup = laagent.agentgroup and branchattr like '"+fm.all('BranchAttr').value+"%' "
                        +tSpend +")";
         }
      turnPage.queryModal(query_sql, AgentGrid); 
}

/**
 * 页面处理下载操作
 */
 function DoDownload()
 {
  if(!verifyInput()) return false;
  var tAgentState = fm.all('AgentState').value;
  var tChannel = fm.all('Channel').value;
  var tChannelName = fm.all('ChannelName').value;
  var tSpend = "";
  switch(tChannel)
  {
    case '1':
    tSpend =" and branchtype = '1' and branchtype2 = '01'"
    break
    case '2':
    tSpend =" and branchtype = '1' and branchtype2 = '02'"
    break
    case '3':
    tSpend =" and branchtype = '2' and branchtype2 = '01'"
    break
    case '4':
    tSpend =" and branchtype = '2' and branchtype2 = '02'"
    break
    case '5':
    tSpend=" and branchtype = '3' and branchtype2 = '01'"
    break
    case '6':
    tSpend =" and branchtype = '4' and branchtype2 = '01'"
    break
    default:
    alert("业务渠道输入有误！")
  }
 
  var Down_sql = "select managecom ,(select name from ldcom where comcode = laagent.managecom),'"+tChannelName+"',(select branchattr from labranchgroup where agentgroup = laagent.agentgroup),agentcode,name,"
                +" case when agentstate<'06' then '在职' else '离职' end,case when togaeflag = 'Y' then '是' when togaeflag ='N' then '否' else '' end "
                +" from laagent where 1 = 1 and managecom like '"+fm.all('ManageCom').value+"%'"
                +tSpend
                +getWherePart('AgentCode','AgentCode')
                +getWherePart('Name','Name')
                +getWherePart('togaeflag','InteractiveFlag');
        if(fm.all('AgentState').value!=null && fm.all('AgentState').value!='')
          {
               if(tAgentState=="1")
               {
                 Down_sql+=" and agentstate <'06'";
               }
               if(tAgentState=="2")
               {
                 Down_sql+=" and agentstate >='06'";
               }    
           }
         if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!='')
          {
             Down_sql+=" and exists (select 'Y' from labranchgroup where agentgroup = laagent.agentgroup and branchattr like '"+fm.all('BranchAttr').value+"%' "
                        +tSpend +")";
         }
         
     fm.querySql.value = Down_sql;
     
     var DownSQLTitle = "select '管理机构代码','管理机构名称','业务渠道','团队代码','业务员编码','业务员姓名','代理人状态','互动部标志' from dual where 1=1 ";
     fm.querySqlTitle.value = DownSQLTitle;  
     
     fm.all("Title").value="select '人员互动部标志 报表下载' from dual where 1=1  "; 
     
     fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
     
     fm.submit();
 }
 
function DoSave(){
	if(!chkMulLine()){return false;}
	fm.all('fmAction').value='DoSave';
	submitForm();
}
function DoDelete(){
	if(!chkMulLine()){return false;}
	fm.all('fmAction').value='DoDelete';
	submitForm();
}

function chkMulLine(){
	var i;
	var iCount = 0;
	var rowNum = AgentGrid.mulLineCount;
	for(i=0;i<rowNum;i++){
		if(AgentGrid.getChkNo(i)){
			iCount++;
		} 
	}
	if(iCount == 0){
		alert("请选择要标志的记录!");
		return false
	}
	return true;
}

function reset()
{
   initInpBox();
}