//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAAgentQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


function returnParent()
{
        var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
			try
			{	
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	tRow = AgentGrid.getSelNo();
	if( tRow == 0 || tRow == null)
	  return arrSelected;
	var arrSelected = new Array();
	var strSQL = "";
	strSQL = "select "
    +" groupAgentCode"
    +",name"
    +",managecom"
    +",(select name from ldcom where comcode = laagent.managecom)"
    +",(select branchattr from labranchgroup where agentgroup = laagent.agentgroup)"
    +",(select name from labranchgroup where agentgroup = laagent.agentgroup)"
    +",(select applygbflag from labranchgroup where agentgroup = laagent.agentgroup)"
    +",(select applygbstartdate from labranchgroup where agentgroup = laagent.agentgroup)"
    +",(select gbuildflag from labranchgroup where agentgroup = laagent.agentgroup)"
    +",(select gbuildstartdate from labranchgroup where agentgroup = laagent.agentgroup)"
    +",noworkflag"
    +",traindate"
    +",agentstate"
    +",employdate"
    +",(select agentgrade from latree where agentcode = laagent.agentcode)"
	+" from LAAgent  where 1=1 and agentstate <'06' and  "
	+ " groupAgentCode='"+AgentGrid.getRowColData(tRow-1,5)+"'"; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
        //判断是否查询成功
       if (!turnPage.strQueryResult) {
         alert("查询失败！");
         return false;
       }
       //查询成功则拆分字符串，返回二维数组
       arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
if(!verifyInput())
  {
  	return false;
  }
	initAgentGrid();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select managecom"
	       +",(select name from ldcom where comcode = laagent.managecom)"
	       +",(select branchattr from labranchgroup where agentgroup= laagent.agentgroup)"
	       +",(select name from labranchgroup where agentgroup= laagent.agentgroup)"
	       +",groupAgentCode"
	       +",name"
	       +",(select agentgrade from latree where agentcode= laagent.agentcode)"
	       +",noworkflag"
	       +",traindate"
	       +" from laagent where 1=1 and branchtype ='1' and branchtype2 ='01' and agentstate <'06' "
	       + getWherePart('groupAgentCode','AgentCode')
	    //   + getWherePart('ManageCom','ManageCom')
	       + getWherePart('Name','Name');
	       if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!='')
	       {
	       	strSQL =strSQL+ " and managecom like '"+fm.all('ManageCom').value+"%'";
	       }
	       if(fm.all('BranchAttr').value!=null&&fm.all("BranchAttr").value!='')
	       {
	       	strSQL+=" and agentgroup = (select agentgroup from labranchgroup where branchtype = '1' and branchtype2 = '01' and branchattr = '"+fm.all('BranchAttr').value+"')";
	       }
     	turnPage.queryModal(strSQL, AgentGrid); 

}


function ListExecl()
{
	
if(!verifyInput())
  {
  	return false;
  }  
//定义查询的数据
	var strSQL = "";
	strSQL = "select managecom"
	       +",(select name from ldcom where comcode = laagent.managecom)"
	       +",(select branchattr from labranchgroup where agentgroup= laagent.agentgroup)"
	       +",(select name from labranchgroup where agentgroup= laagent.agentgroup)"
	       +",groupAgentCode"
	       +",name"
	       +",(select agentgrade from latree where agentcode= laagent.agentcode)"
	       +",noworkflag"
	       +",traindate"
	       +" from laagent where 1=1 and branchtype ='1' and branchtype2 ='01' and agentstate <'06' "
	       + getWherePart('groupAgentCode','AgentCode')
	    //   + getWherePart('ManageCom','ManageCom')
	       + getWherePart('Name','Name');
	       if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!='')
	       {
	       	strSQL =strSQL+ " and managecom like '"+fm.all('ManageCom').value+"%'";
	       }
	       if(fm.all('BranchAttr').value!=null&&fm.all("BranchAttr").value!='')
	       {
	       	strSQL+=" and agentgroup = (select agentgroup from labranchgroup where branchtype = '1' and branchtype2 = '01' and branchattr = '"+fm.all('BranchAttr').value+"')";
	       }
     
fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '管理机构','机构名称','团队编码','团队名称','营销员编码','姓名','职级','筹备标记','筹备开始日期'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '筹备标记管理 查询下载' from dual where 1=1  ";  
  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}
