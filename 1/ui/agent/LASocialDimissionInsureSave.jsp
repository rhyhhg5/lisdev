<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LADimissionSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="java.util.Date"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.agent.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK"%>
<%
  //接收信息，并作校验处理 输入参数
  LADimissionSchema tLADimissionSchema   = new LADimissionSchema();

  LASocialDimissionInsureBL tLADimission   = new LASocialDimissionInsureBL();
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");


  tLADimissionSchema.setAgentCode(request.getParameter("AgentCode"));
  tLADimissionSchema.setBranchType(request.getParameter("BranchType"));
  tLADimissionSchema.setBranchType2(request.getParameter("BranchType2"));
  tLADimissionSchema.setDepartTimes(request.getParameter("DepartTimes"));
  tLADimissionSchema.setDepartDate(request.getParameter("DepartDate"));
  tLADimissionSchema.setApplyDate(request.getParameter("ApplyDate"));
  tLADimissionSchema.setDepartRsn(request.getParameter("DepartRsn"));
  tLADimissionSchema.setQualityDestFlag(request.getParameter("QualityDestFlag"));
  tLADimissionSchema.setReturnFlag(request.getParameter("ReturnFlag"));
  tLADimissionSchema.setPbcFlag(request.getParameter("PbcFlag"));
  tLADimissionSchema.setDestoryFlag(request.getParameter("DestoryFlag"));
  tLADimissionSchema.setAnnuityFlag(request.getParameter("AnnuityFlag"));
  tLADimissionSchema.setVisitFlag(request.getParameter("VisitFlag"));
  tLADimissionSchema.setBlackFlag(request.getParameter("BlackFlag"));
  tLADimissionSchema.setBlackListRsn(request.getParameter("BlackListRsn"));
    
  System.out.println("Schema:over");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tLADimissionSchema);
  tVData.add(tG);
  try
  {
    tLADimission.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (!FlagStr.equals("Fail"))
  {
    tError = tLADimission.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
        System.out.println("no reord");
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
   System.out.println("Schema:over12");

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

