var showInfo;



function getManagecom(Managecom,ManagecomName)
{
	    var pManagecom = fm.all('PManageCom').value;
	    if(pManagecom==null||pManagecom=='')
	    {
	       alert("请先录入省公司代码");
	       return false;
	    }
  var strsql ="1  and length(trim(comcode))=8 and comcode like #"+pManagecom+"%#";
  showCodeList('comcode',[Managecom,ManagecomName],[0,1],null,strsql,1,1);
}  

// 页面触发获取营业部基础信息
function getBranchAttr(BranchAttr,BranchAttrName)
{
	var cManagecom = fm.all('CManageCom').value;
	
    if(cManagecom==null||cManagecom=='')
    {
       alert("请先录入中心支公司代码");
       return false;
    }
    var strsql1 ="1 and EndFlag=#N# and length(trim(branchattr))=10 and branchtype = #"+fm.all('BranchType').value+"# and branchtype2 = #"+fm.all('BranchType2').value+"# and managecom like #"+cManagecom+"%#";
    showCodeList('branchattr',[BranchAttr,BranchAttrName],[0,1],null,strsql1,1,1);
    
}

// 保存操作
function submitForm()
{
	
  if(fm.all('ReturnFlag').value=='Y')
  {
     alert("查询返回的信息如想进行调整，烦请通过修改按钮进行操作！");
     return false;
  }
  //前台录入信息校验 
  if( verifyInput() == false ) return false;
  if(changeIDNo() == false ) return false;
  if(!beforeSubmit()) return false;
  if(fm.all('PManageCom').value)
  fm.all("hideOperate").value="INSERT||MAIN";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();// 提交
	initForm();

}
// 修改操作
function updateClick()
{
  
  if(fm.all("ManagerCode").value==null||fm.all("ManagerCode").value==""){
		alert('请查询返回后再进行修改操作');
		return false;
  }
  if( verifyInput() == false ) return false;
  if(!beforeSubmit()) return false;
  if(changeIDNo2() == false ) return false;

  var i = 0;
  fm.all("hideOperate").value="UPDATE||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();// 提交
  initForm();

}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
    var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
  showInfo=window.open("./LAManagerQueryHtml.jsp?BranchType="+tBranchType+"&BranchType2="+tBranchType2);
}    

// 查询页面，查询返回操作
function afterQuery(arrQueryResult)
{
	var arr = new Array();
	if( arrQueryResult != null )
	{
		arr = arrQueryResult;
        fm.all('ManagerCode').value=arr[0][0];
		fm.all('PManageCom').value= arr[0][1];
		fm.all('PManageComName').value= arr[0][2];	                                   
		fm.all('CManageCom').value= arr[0][3];
        fm.all('CManageComName').value= arr[0][4];
        fm.all('BranchAttr').value= arr[0][5];
        fm.all('BranchAttrName').value= arr[0][6];
        fm.all('ManagerName').value= arr[0][7];
        fm.all('Sex').value= arr[0][8];
        fm.all('SexName').value= arr[0][9];  
        fm.all('ManagerGrade').value= arr[0][10]; 
        fm.all('GradeName').value= arr[0][11];
        fm.all('TakeOfficeDate').value= arr[0][12];
        fm.all('PolityVisage').value= arr[0][13];
        fm.all('PolityVisageName').value= arr[0][14];
        fm.all('InsureDate').value= arr[0][15];
        fm.all('GraduateSchool').value= arr[0][16];
        fm.all('Speciality').value= arr[0][17];
        fm.all('GraduateDate').value= arr[0][18];
        fm.all('Education').value= arr[0][19];
        fm.all('EducationName').value= arr[0][20];
        fm.all('Degree').value= arr[0][21];
        fm.all('DegreeName').value= arr[0][22];
        fm.all('Assess').value= arr[0][23];
        fm.all('AssessName').value= arr[0][24];
        fm.all('MulitId').value= arr[0][25]
        fm.all('MulitIdName').value= arr[0][26];
        fm.all('ManagerState').value= arr[0][27];
        fm.all('StateName').value= arr[0][28];
        fm.all('WorkYear').value= arr[0][29];
        fm.all('WorkDate').value= arr[0][30];
        fm.all('Birthday').value=arr[0][31];
        fm.all('IDNoType').value=arr[0][32];
        fm.all('IDNoTypeName').value=arr[0][33];
        fm.all('IDNo').value=arr[0][34];
        fm.all('Mobile').value=arr[0][35];
        fm.all('LaborContract').value=arr[0][36];
        fm.all('ReturnFlag').value='Y';

	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content="+content  ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}
function changeIDNo()
{
   if (getWherePart('IDNo')=='')
     return false;
   var strSQL = "";
   strSQL = "select 1 from laManager a where 1=1 "
           + getWherePart('a.IDNo','IDNo');
   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
   if (strQueryResult)
   {
   	alert('该证件号已存在,请进行查询修改操作');
   	return false;
   }
    return true;
}

function changeIDNo2()
{
	   if (getWherePart('IDNo')=='')
		     return false;
		   var strSQL = "";
		   strSQL = "select 1 from laManager a where 1=1 and ManagerCode <>'"+fm.all('ManagerCode').value+"'"
		           + getWherePart('a.IDNo','IDNo');
		   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
		   if (strQueryResult)
		   {
		   	alert('该证件号已存在,请进行查询修改操作');
		   	return false;
		   }
		    return true;
}
// 简单逻辑校验
function beforeSubmit()
{
	var WorkYear=fm.all('WorkYear').value;
     	if(WorkYear!=null&&WorkYear!=''){
     		if(!isNumeric(WorkYear)){
     			errorMessage('请输入从事保险工作时间合法的数字');
     			return false;
     		}
     	}
     	if(fm.all('ManagerState').value!=0&&fm.all('ManagerState').value!=1){
     		alert('请录入正确的在职状态！');
     		return false;
     	}
        var strReturn = checkIdNo( trim(fm.all('IDNoType').value),trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
    	if (strReturn != ''){
    		alert(strReturn);
    		return false;
    	}
    
	return true;
}


function afterCodeSelect(codeName,Field){
	if(Field.value.length==4&codeName=='comcodeallsign'){
		fm.all('CManageCom').value=''
		fm.all('CManageComName').value=''
		fm.all('BranchAttr').value=''
		fm.all('BranchAttrName').value=''
	}
	if(Field.value.length==8&codeName=='comcode'){
		fm.all('BranchAttr').value=''
		fm.all('BranchAttrName').value=''
	}
		
}


//上传
function upLoad(dataname)
{
	var ManagerCode = fm.all('ManagerCode').value;
	if(ManagerCode==null||ManagerCode=="")
	{
	alert("请先查询返回后再进行上传操作");
	return false;
    }
	fm2.all("dataName").value=dataname;

	if(fm2.all('PictureFile').value=="")
	{
	alert("请选择要上载的文件.");
	return false;
    }
   
    fm2.all("filePath").value="test";
    fm2.all('tBranchAttrName').value=fm.all('BranchAttrName').value
    fm2.all('ManagerCode2').value = fm.all('ManagerCode').value;
    if (verifyInput() == false)
    return false;

	var showStr="正在上载数据……";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm2.action="./LAManagerUpLoad.jsp";
	fm2.encoding ="multipart/form-data";
	fm2.submit();
	initForm();
}
//下载影印文件
function download(downName)
{
	
	fm2.all('downName').value=downName;

    var ManagerCode = fm.all('ManagerCode').value;
	if(ManagerCode==""||fm2.all('downName').value==""||ManagerCode==null||fm2.all('downName').value==null)
	{
	alert("请先查询返回后再进行下载操作");
	return false;
    }
	fm2.action="./LAManagerDownLoad.jsp?ManagerCode="+ManagerCode+"&downName="+downName;
    fm2.submit();
}
function afterdownload(content){
	  window.focus();
   if(content=="1"){
  	 alert('下载成功');
   }
   if(content=="2"){
  	 alert('下载失败，请确认文件是否存在');
   }
   if(content=="3"){
  	 alert('下载失败,未找到相关文件');
   }
   
}
