<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LATreeEditSave.jsp
//程序功能：
//创建日期：2003-07-22 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LATreeSchema tLATreeSchema = new LATreeSchema();
  LATreeEditUI tLATreeEditUI   = new LATreeEditUI();

  //输出参数
  CErrors tError = null;
  String tOperate = "UPDATE||MAIN";
  String tRearStr = "";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin LATREE schema...");
  
  //取得行政信息--在bl中设置职级及系列
  tLATreeSchema.setAgentCode(request.getParameter("AgentCode"));
  //tLATreeSchema.setManageCom(request.getParameter("ManageCom"));
  tLATreeSchema.setIntroAgency(request.getParameter("IntroAgency"));
  tLATreeSchema.setAgentGrade(request.getParameter("AgentGrade"));
  tLATreeSchema.setAstartDate(request.getParameter("AdjustDate"));
  tLATreeSchema.setBranchCode(request.getParameter("BranchCode"));
  if (tLATreeSchema.getAgentGrade().compareTo("A03")>0)
  {  
    if (!request.getParameter("RearAgent").equals(""))
    {
      tRearStr = request.getParameter("RearAgent");
      tLATreeSchema.setEduManager(request.getParameter("RearAgent"));  
    }
    if (tLATreeSchema.getAgentGrade().compareTo("A05")>0)
    {
      tRearStr += ":";
      tLATreeSchema.setEduManager("");
      if (!request.getParameter("RearDepartAgent").equals(""))
      {
        tRearStr += request.getParameter("RearDepartAgent"); 
        tLATreeSchema.setEduManager(request.getParameter("RearDepartAgent"));  
      }
      if (tLATreeSchema.getAgentGrade().compareTo("A07")>0)
      { 
         tRearStr += ":";
         tLATreeSchema.setEduManager("");
         if (!request.getParameter("RearSuperintAgent").equals(""))
         {
            tRearStr += request.getParameter("RearSuperintAgent"); 
            tLATreeSchema.setEduManager(request.getParameter("RearSuperintAgent"));  
         }
         if (tLATreeSchema.getAgentGrade().compareTo("A09")==0)
         {
           tRearStr += ":";
           tLATreeSchema.setEduManager("");
           if (!request.getParameter("RearAreaSuperintAgent").equals(""))
           {
             tRearStr += request.getParameter("RearAreaSuperintAgent"); 
             tLATreeSchema.setEduManager(request.getParameter("RearAreaSuperintAgent"));  
           }
         }
      }
    }
    System.out.println("育成链："+tRearStr);
  }
  tLATreeSchema.setAscriptSeries(tRearStr);  
  
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  
  tVData.add(tG);
  tVData.addElement(tLATreeSchema);
  
  try
  {
    System.out.println("this will save the data!!!");
    tLATreeEditUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLATreeEditUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
 
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

