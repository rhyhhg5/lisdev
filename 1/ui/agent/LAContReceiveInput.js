
var showInfo;

var turnPage = new turnPageClass();


//***************************************************
//* 验证必填项
//***************************************************
function checkValid()
{
	if (trim(document.fm.ManageCom.value)=="")
	{
		alert("请填写管理机构!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		return false;
	}
	
	if (document.fm.ManageCom.value.length != 8)
	{
		alert("管理机构输入错误!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		fm.all('ManageCom').focus();
		return false;
	}
	

	return true;
}
function checkValid1()
{
	if (trim(document.fm.ManageCom.value)=="")
	{
		alert("请填写管理机构!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		return false;
	}
	
	if (document.fm.ManageCom.value.length != 8)
	{
		alert("管理机构输入错误!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		fm.all('ManageCom').focus();
		return false;
	}
	
	if(trim(document.fm.GroupAgentCode.value)=="")
	{
		alert("请填写业务员代码!");
		document.fm.GroupAgentCode.value="";
		fm.all('GroupAgentCode').focus();
		return false;
	}
	
 if(trim(document.fm.ProposalContNo.value)=="")
	{
		alert("请填写投保书号码!");
		document.fm.ProposalContNo.value="";
		fm.all('ProposalContNo').focus();
		return false;
	}
   
	if(trim(document.fm.AppntName.value)=="")
	{
		alert("请填写投保人姓名!");
		document.fm.AppntName.value="";
		fm.all('AppntName').focus();
		return false;
	}
	
	
	if(trim(document.fm.Prem.value)=="")
	{
		alert("请填写投保费用!");
		document.fm.Prem.value="";
		fm.all('Prem').focus();
		return false;
	}
	if(trim(document.fm.InputDate.value)=="")
	{
		alert("请填写投保时间!");
		document.fm.InputDate.value="";
		fm.all('InputDate').focus();
		return false;
	}
	return true;
}

//***************************************************
//* 验证对应业务员代码的名字和销售机构
//***************************************************
function getAgent()
{
 if (fm.all('ManageCom').value==null || fm.all('ManageCom').value=='')
 { 
 	alert("请先录入管理机构");
 	fm.all('AgentCode').value="";
 	return;
}
 var strSQL = "";
 var tAgentCode=fm.all('GroupAgentCode').value;
 
   if (tAgentCode!='' )
  {	
     strSQL = "select a.name,b.branchattr,a.agentgroup,b.branchseries  from laagent a,labranchgroup b where  a.agentgroup=b.agentgroup and a.agentstate<='02' "
     + getWherePart('a.GroupAgentCode','GroupAgentCode') 
     + getWherePart('a.ManageCom','ManageCom');     
     var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
     
  //判断是否查询成功
     if (!strQueryResult) 
    {
      alert("该机构没有此业务员");
      fm.all('GroupAgentCode').value="";
      fm.all('AgentName').value = "";
      fm.all('BranchAttr').value = "";
      fm.all('AgentGroup').value = "";
      fm.all('BranchSeries').value = "";
      return;
    }
  }
 
else
  {
    fm.all('GroupAgentCode').value="";
    fm.all('AgentName').value = "";
    fm.all('BranchAttr').value = "";
    fm.all('AgentGroup').value = "";
    fm.all('BranchCode').value = "";
     return;
  }
 //alert("4");
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  fm.all('BranchAttr').value  =tArr[0][1];
  fm.all('AgentName').value  =tArr[0][0];	
 fm.all('AgentGroup').value =tArr[0][2];
 fm.all('BranchSeries').value = tArr[0][3];
	
	
	
}

//***************************************************
//* 点击‘保存’进行的操作
//***************************************************
function submitForm()
{
	 if(verifyInput() == false)
 {
 	return false;
 }
    if (checkValid1() == false)
    return false;
    
  //alert("chenggong 1");
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   
  fm.fmtransact.value = "INSERT||MAIN";
  
  fm.submit(); //提交
  //alert("chenggong 3");
  return true;
}


//***************************************************
//* 单击“查询”进行的操作
//***************************************************

function queryClick()
{
  //下面增加相应的代码
  showInfo=window.open("./LAContReceiveQueryHtml.jsp");
}           

function queryClick1()
{  
	
	//验证必填项
	if(!checkValid())
	{
		return false;
		
	}
	//alert("zhengque");
	var tSQL = "";

	
	
    
	tSQL = "select a.MakeContNo,a.ProposalContNo ,  getUniteCode(a.AgentCode),(select name from LAAgent where agentcode =a.agentcode), a.ManageCom, "
	+ " a.BranchAttr,c.Name,a.RiskCode,a.Prem,a.AppntName"
    + " FROM "
    + " LAMakeCont a ,LAAgent b,LABranchGroup c" 
    + " where "
    + " a.agentcode = b.agentcode"
    + " and a.Agentgroup =c.Agentgroup ";
   //业务系列

  tSQL += getWherePart("a.ManageCom","ManageCom","like")
  + getWherePart("a.RiskCode","RiskCode")
  + getWherePart("a.BranchAttr","BranchAttr")
  + getWherePart("a.AgentCode","AgentCode")
  + getWherePart("a.AppntName","AppntName")
  + getWherePart("a.ProposalContNo","ProposalContNo")
  + getWherePart("a.Prem","Prem")
  + getWherePart("a.InputDate","InputDate")
  + "ORDER BY a.ProposalContNo" ;
	
	
	//alert(tSQL);
	//执行查询并返回结果
	turnPage.queryModal(tSQL, EvaluateGrid);
	//alert("33");
}

//***************************************************
//* 单击“修改”进行的操作
//***************************************************

function updateClick()
{
  //下面增加相应的代码
  
  if (fm.all('MakeContNo').value==null || fm.all('MakeContNo').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('MakeContNo').value="";
 	return;
}
  
  if (fm.all('ManageCom').value==null || fm.all('ManageCom').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('ManageCom').value="";
 	return;
}
  if (fm.all('GroupAgentCode').value==null || fm.all('GroupAgentCode').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('GroupAgentCode').value="";
 	return;
}
 if (fm.all('ProposalContNo').value==null || fm.all('ProposalContNo').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('ProposalContNo').value="";
 	return;
}
 if (fm.all('AppntName').value==null || fm.all('AppntName').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('AppntName').value="";
 	return;
}


if (fm.all('Prem').value==null || fm.all('Prem').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('Prem').value="";
 	return;
}
  if (confirm("您确实想修改该条记录?"))
  {
 
  //showSubmitFrame(mDebug);
  
   //alert("xiu1");
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
   //alert("xiu2");

  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//***************************************************
//* 点击“删除”进行的操作
//***************************************************
function deleteClick()
{
   if (fm.all('ManageCom').value==null || fm.all('ManageCom').value=='')
 { 
 	alert("请先查询出要删除的数据！");
 	fm.all('ManageCom').value="";
 	return;
}
 
  
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }

}           
//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    //EvaluateGrid.clearData("EvaluateGrid");
  }
}

function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
        var agentSQL = "SELECT AGENTCODE from laagent where  groupagentcode='"+arrQueryResult[0][1]+"' ";
        var agentCodeResult=easyExecSql(agentSQL);
		
		arrResult = arrQueryResult;
        fm.all('GroupAgentCode').value = arrResult[0][1];
        fm.all('AgentCode').value = agentCodeResult[0][0];
        document.all.AgentCode.readOnly="true";
        fm.all('BranchAttr').value = arrResult[0][2];
        fm.all('ManageCom').value = arrResult[0][0];
        fm.all('Agentname').value = arrResult[0][3];
        fm.all('ProposalContNo').value = arrResult[0][4];
        fm.all('AppntName').value = arrResult[0][5];
        fm.all('InputDate').value = arrResult[0][6];
        fm.all('Prem').value = arrResult[0][7];
        fm.all('MakeContNo').value = arrResult[0][8];
        fm.all('ManageComName').value = arrResult[0][9];
        fm.all('ManageCom').disabled = true;
        fm.all('AgentCode').disabled = true;
        document.all.AgentCode.readOnly="true";
        
        }     

  // return false;
}
