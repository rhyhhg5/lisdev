<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LABranchGroupInput.jsp
	//程序功能：
	//创建日期：
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.ExeSQL"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String LoginManagecom = tG.ManageCom;
	String CurDate = PubFun.getCurrentDate();
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
	
%>
<script>
	var cSql = " 1 and length(trim(comcode)) = #4# ";
</script>

<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LATrainCourseInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LATrainCourseInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
<form action="./LATrainCourseSave.jsp" method=post name=fm
	target="fraSubmit"><%@include
	file="../common/jsp/OperateButton.jsp"%>
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divLABranchGroup1);">
		<td class=titleImg>基础信息录入</td>
	</tr>
</table>
<Div id="divLABranchGroup1" style="display: ''">
<table class=common>
	<tr>
		<TD class=title>省分公司</TD>
		<TD class=input><Input class="codeno" name=PManageCom
			verify="省分公司|code:comcodeallsign&NOTNULL&len=4"
			ondblclick="return showCodeList('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1);"
			onkeyup="return showCodeListKey('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1); "><Input
			class=codename name=PManageComName readOnly elementtype=nacessary></TD>
		<TD class=title>中心支公司</TD>
		<TD class=input><Input class="codeno" name=CManageCom
			verify="中心支公司|code:comcode&len>7"
			ondblclick="return getManagecom(CManageCom,CManageComName);"
			onkeyup="return getManagecom(CManageCom,CManageComName);" ><Input
			class=codename name=CManageComName readOnly ></TD>
		<TD class=title>营业部</TD>
		<TD class=input><Input name=BranchAttr id=BranchAttr
			class="codeno" "
			ondblclick="return getBranchAttr(BranchAttr,BranchAttrName);"
			onkeyup="return getBranchAttr(BranchAttr,BranchAttrName);"><Input
			maxlength=12 class="codename" name=BranchAttrName
			 ></TD>
	</tr>
	<TR class=common>
		<TD class=title>培训开始时间</TD>
		<TD class=input><Input class='coolDatePicker' name=StartDate
			verify="培训开始时间|notnull&DATE&len=10" format='short'
			onchange="return getEndDate();" elementtype=nacessary></TD>
		<!--	</TR>-->
		<!--	<TR class=common>-->
		<TD class=title>培训结束时间</TD>
		<TD class=input><Input class='coolDatePicker' name=EndDate
			verify="培训结束时间|notnull&DATE&len=10" format='short'
			onchange="return getEndDate();" elementtype=nacessary></TD>
		<TD class=title>培训地点</TD>
		<TD class=input><Input class=common name=TrainPlace verify="培训地点|notnull" elementtype=nacessary></TD>
	</TR>
	<TR class=common>
		<TD class=title>培训班名称</TD>
		<TD class=input><Input name=Course id=Course class="codeno"
			verify="培训班名称|notnull&code:Course"
			ondblclick="return showCodeList('course',[this,CourseName],[0,1]);"
			onkeyup="return showCodeListKey('course',[this,CourseName],[0,1]);"><Input
			class=codename name=CourseName readOnly elementtype=nacessary>
		</TD>
	</TR>
</table>
</Div>

<input type=hidden name=hideOperate value=''> 
<input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
<input type=hidden name=BranchType value=<%=BranchType%>> 
<input type=hidden name=BranchType2 value=<%=BranchType2%>> 
<input type=hidden name=LoginManagecom value=<%=LoginManagecom%>> 
<input type=hidden name=CourseNo value=''> 
<input type=hidden name=ReturnFlag value=''> 
<input type=hidden name=AgentGroup value=''> <!--后台操作的隐式机构编码，不随机构的调整而改变 --> 
<span id="spanCode" style="display: none; position: absolute;"></span>
</form>
	
<form action="" method=post name=fm2 target="fraSubmit">
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divLABranchGroup2);">
		<td class=titleImg>培训文件</td>
	</tr>
</table>
<Div id="divLABranchGroup2" style="display: ''">
<table class=common>
	<TR class=common>
		<TD class=title style="width: 100px">培训课程表</TD>
		<TD class=input><Input type="file" class="common"
			style="width: 300px" name="CourseList" id="CourseListFile" /></TD>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="上  传" id="goDiskImport"
			class=cssButton onclick="upLoad('CourseList')">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="下  载" id="goDiskImport"
			class=cssButton onclick="download('CourseList')"></td>
	</TR>
	<TR class=common>
		<TD class=title style="width: 100px">学员名单</TD>
		<TD class=input><Input type="file" class="common"
			style="width: 300px" name="TraineeList" id="TraineeListFile" /></TD>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="上  传" id="goDiskImport"
			class=cssButton onclick="upLoad('TraineeList')">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="下  载" id="goDiskImport"
			class=cssButton onclick="download('TraineeList')"></td>
	</TR>
	<TR class=common>
		<TD class=title style="width: 100px">培训照片</TD>
		<TD class=input><Input type="file" class="common"
			style="width: 300px" name="PictureFile" id="PictureFile" /></TD>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="上  传" id="goDiskImport"
			class=cssButton onclick="upLoad('PictureFile')">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="下  载" id="goDiskImport"
			class=cssButton onclick="download('PictureFile')"></td>
	</TR>
	<TR class=common>
		<TD class=title style="width: 100px">培训评估总结</TD>
		<TD class=input><Input type="file" class="common"
			style="width: 300px" name="ConclusionFile" id="ConclusionFile" /></TD>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="上  传" id="goDiskImport"
			class=cssButton onclick="upLoad('ConclusionFile')">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button
			class=cssButton name="goDiskImport" value="下  载" id="goDiskImport"
			class=cssButton onclick="download('ConclusionFile')"></td>
	</TR>
</table>
</Div>
<input type=hidden name=dataName value=''> 
<input type=hidden name=downName value=''> 
<input type=hidden name=filePath value=''>
<input type=hidden name=TCourseNo value=''>
<input type=hidden name=tBranchAttrName value=''> 
</form>
</body>
</html>
