<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

  if(globalInput == null) {
	out.println("session has expired");
	return;
}

String strOperator = globalInput.Operator;
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">

function initInpBox()
{
  try
  {
  	//alert("11");
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';
    fm.all('AgentCode').value = '';
    fm.all('ProposalContNo').value = '';
    fm.all('AppntName').value = '';
    fm.all('InputDate').value = '';
    fm.all('Prem').value = '';
    //alert("22");
    //fm.all('AgentGroup').value='';
    //fm.all('BranchAttr').value=''; 
    //fm.all('MakeContNo').value = '';
    //fm.all('RiskCode').value = '';
    
}
  catch(ex)
  {
  	
    alert("111111在Init.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initEvaluateGrid();
  }
  catch(re)
  {
    alert("22Init.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 var EvaluateGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initEvaluateGrid()
{

    var iArray = new Array();
  try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px";            		//列宽
      iArray[0][2]=4;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="投保书号";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=40;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
       iArray[2]=new Array();
      iArray[2][0]="投保人姓名";      		//列名
      iArray[2][1]="75px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="业务员代码";         		//列名
      iArray[3][1]="75px";            		//列宽
      iArray[3][2]=40;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="业务员姓名";         		//列名
      iArray[4][1]="75px";            		//列宽
      iArray[4][2]=40;            			//列最大值
     iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
     iArray[5]=new Array();
     iArray[5][0]="管理机构";         		//列名
      iArray[5][1]="75px";            		//列宽
      iArray[5][2]=40;            			//列最大值
     iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     iArray[6]=new Array();
      iArray[6][0]="销售团队代码";         		//列名
      iArray[6][1]="75px";            		//列宽
      iArray[6][2]=200;            			//列最大值
     iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     iArray[7]=new Array();
      iArray[7][0]="销售团队名称";         		//列名
     iArray[7][1]="150px";            		//列宽
     iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="保费";      		//列名
      iArray[8][1]="75px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="流水号";         		//列名
      iArray[9][1]="0px";            		//列宽
      iArray[9][2]=40;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      EvaluateGrid = new MulLineEnter( "fm" , "EvaluateGrid" );
      //EvaluateGrid.selBoxEventFuncName = "getPara";
      //这些属性必须在loadMulLine前
      EvaluateGrid.displayTitle = 1;
      EvaluateGrid.mulLineCount = 0;
      EvaluateGrid.hiddenPlus=1;
      EvaluateGrid.hiddenSubtraction=1;
      EvaluateGrid.canSel = 1;
      EvaluateGrid.canChk =0;  // 1为显示CheckBox列，0为不显示 (缺省值)
      EvaluateGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //EvaluateGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
