<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAAgetstateInput.jsp
//程序功能：
//创建日期：2016-09-02
//创建人  ：zyy程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>`
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAAgetstateInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAAgetstateInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAgetstateSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="./LAAgetstateOp6.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
          <td class=titleImg>
            营销员信息
          </td>
      </tr> 
    </table>
    <Div  id= "divLAAgent1" style= "display: ''">      
    <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          营销员代码 
        </TD>
        <TD  class= input> 
          <Input class= 'readonly' readonly name=AgentCode >
        </TD>        
       
      </TR> 
      <TR  class= common> 
       <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common verify="姓名|NotNull&len<=15" elementtype=nacessary>
        </TD>
        <TD  class= title>
          证件号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common verify="身份证号码|notnull&len<=20 "  elementtype=nacessary> 
        </TD>
      </TR>
      <TR  class= common> 
       <TD  class= title>
          离职日期 
        </TD>
        <TD  class= input> 
          <Input name=Outworkdate class='common'  verify="离职时间|notnull&Date" elementtype=nacessary>
        </TD>
        <TD  class= title>
          代理人状态 
        </TD>
        <TD  class= input> 
          <Input name=AgentState class='common' verify="代理人状态|notnull&agentstate"  > 
        </TD>
	  </TR>
      <TR  class= common>
	     <TD  class=input colSpan= 3>
		 </TD>
	    </TR>
    </table>
    </Div>	
    <!--行政信息-->    
    <table>
    	<tr>
    		<td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
        <td class= titleImg>
         行政信息
        </td>
    	</tr>
     </table>
     <Div id= "divLAAgent3" style= "display: ''">
       <table class=common>
        <tr class=common>     
        <TD class= title>
          营销员职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="common"  verify="营销员职级|notnull&AgentGrade"  class='readonly'readonly 
          >
        </TD>
        <TD class= title>
          销售机构 
        </TD>
        <TD class= input>
          <Input class=common name=BranchCode verify="销售机构|notnull" onchange="return changeGroup();" elementtype=nacessary> 
        </TD>
        <TD class= title>
          管理机构
        </TD>
        <TD class= input>
          <Input name=ManageCom class='readonly'readonly >
        </TD>
        </tr>
        <tr class=common>
        <TD class= title>
          处经理
        </TD>
        <TD class= input>
          <Input name=GroupManagerName class='readonly'readonly > 
        </TD>
        <TD class= title>
          区经理
        </TD>
        <TD class= input>
          <Input name=DepManagerName class='readonly'readonly > 
        </TD>
        <TD class= title>
          营销部经理
        </TD>
        <TD class= input>
          <Input name=Minister class='readonly'readonly > 
        </TD>
        </tr>
        <tr class=common>
 			<TD class= title>
 				营业组团队编码 
 			</TD>
 			<TD class= input>
 				<Input name=AgentGroup2 class="code"    class='readonly'readonly 
 				>
 			</TD>
 			<TD class= title>
 				营业组经理
 			</TD>
 			<TD class= input>
 				<Input name=Group2ManagerName class='readonly'readonly >
 			</TD>
 		</tr>
         <tr>
         <TD class= title>
          推荐人 
        </TD>
        <TD class= input>
          <Input class=readonly name=IntroAgency readonly onchange="return changeIntroAgency();"> 
        </TD>
        <TD class= title>
          推荐人姓名 
        </TD>
        <TD class= input>
          <Input class=readonly name=IntroAgencyName readonly> 
        </TD>
        </tr>
       </table>   
    </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=hideagentcode value=''>
    <input type=hidden name=hideagentgroup value=''>
     <input type=hidden name=hidebranchcode value=''>
    <input type=hidden name=initOperate value='DELETE'>
    <input type=hidden name=hideIsManager value='false'>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden name=hideagentstate value=''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    <input type=hidden id="fmAction" name="fmAction">    
  </form>
</body>
</html>

        
