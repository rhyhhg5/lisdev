// 保存操作
function submitForm()
{
	
  if(fm.all('ReturnFlag').value=='Y')
  {
     alert("查询返回的信息如想进行调整，烦请通过修改按钮进行操作！");
     return false;
  }
  //前台录入信息校验 
  if( verifyInput() == false ) return false;
  if(!beforeSubmit()) return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();// 提交
}
// 修改操作
function updateClick()
{
  if(fm.all('AgentCode').value==null||fm.all('AgentCode').value=="")
  {
     alert("请先通过查询功能进行代理人信息查询！");
     return false;
  }
  if( verifyInput() == false ) return false;
  if(!beforeSubmit()) return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();// 提交
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
    var tBranchType = "1";
    var tBranchType2 = "01";
  showInfo=window.open("./LAAgentTrainDateQueryHtml.jsp?BranchType="+tBranchType+"&BranchType2="+tBranchType2);
}    

// 查询页面，查询返回操作
function afterQuery(arrQueryResult)
{
	var arr = new Array();
	if( arrQueryResult != null )
	{
		arr = arrQueryResult;
		fm.all('AgentCode').value= arr[0][0];
        fm.all('Name').value= arr[0][1];
        fm.all('ManageCom').value= arr[0][2];
        fm.all('ManageComName').value= arr[0][3];
        fm.all('BranchAttr').value= arr[0][4];
        fm.all('BranchName').value= arr[0][5];
        fm.all('ApplyGBFlag').value= arr[0][6];
        fm.all('ApplyGBStartDate').value= arr[0][7];
        fm.all('GbuildFlag').value= arr[0][8];
        fm.all('GbuildStartDate').value= arr[0][9];
        fm.all('NoWorkFlag').value= arr[0][10];
        fm.all('oldNoWorkFlag').value= arr[0][10];
        if(fm.all('NoWorkFlag').value!=null&&fm.all('NoWorkFlag').value!="")
        {
           if(fm.all('NoWorkFlag').value=='Y')
           {
              fm.all('NoWorkFlagName').value='是';
           }
           if(fm.all('NoWorkFlag').value=='N')
           {
              fm.all('NoWorkFlagName').value='否';
           }
        }
        fm.all('TrainDate').value= arr[0][11];
        fm.all('AgentState').value= arr[0][12];
        fm.all('Employdate').value= arr[0][13];
        fm.all('AgentGrade').value= arr[0][14];
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}
// 简单逻辑校验
function beforeSubmit()
{
    if( verifyInput() == false ) return false;
	//筹备校验
    var tAgentGrade=fm.all('AgentGrade').value; 
    if(tAgentGrade<'B01')
     {
       if(fm.all('oldNoWorkFlag').value!=null&&fm.all('oldNoWorkFlag').value!="")
       {
       alert("人员已有筹备记录，无法通过此功能进行调整");
       return false;
      }
     }
    if(fm.all('NoWorkFlag').value!=null&&fm.all('NoWorkFlag').value=='N')
    {
       alert("去除筹备标记，烦请通过代理人维护功能");
       return false;
    }
    if(!checkTrainDate()){  return false;  }	        	   

	return true;
}

// 筹备日期校验
function checkTrainDate()
{	 
	  
	// 筹备条件校验
   var sS = "select '1'  "
		+" from LABranchGroup where 1=1 "
		+" and BranchType = '1' and BranchType2 = '01'  and EndFlag <> 'Y' "
		+" and (state<>'1' or state is null)"
		+" and ApplyGBFlag='Y' "
		+" and branchattr='"+fm.all('BranchAttr').value+"'";
		
   var strQueryResult  = easyQueryVer3(sS, 1, 1, 1);
   if (!strQueryResult)
   {
		   alert("所属团队不为申请“申报标记”团队，不能添加筹备标记！");
		   fm.NoWorkFlag.value='';
		   fm.NoWorkFlagName.value='';
		   fm.TrainDate.value='';
		   return false;
	}          
   
	if ((fm.all("TrainDate").value == null)||(fm.all("TrainDate").value == ''))
	{
		alert('筹备人员必须录入筹备开始日期');
		return false	;
	}
	else 
	{
	   var tEmpDate=fm.all('EmployDate').value;
	   var tAgentState=fm.all('AgentState').value;	
       var tTrainDate=fm.all('TrainDate').value;
       var tAgentGrade=fm.all('AgentGrade').value; 
       if(tAgentGrade<'B01')
       {
         if(tAgentState!='01'&&tAgentState!='03')
	       {
		     alert("二次入司业务序列人员及离职人员不能转为筹备人员！");
		     return false;		
	       }
       }
    
 	  if ('2009-10-01' > tTrainDate)
	  {
           alert('筹备开始日期必须大于等于2009-10-01！');
           fm.all('TrainDate').value='';
	       return false	;
      }     
       if(tTrainDate<tEmpDate)
       {
           alert('筹备开始日期必须大于等于入司日期！');
           fm.all('TrainDate').value='';
	       return false	;
       }   
      if ( tTrainDate>='2013-03-16')
	   {
	    if(tAgentGrade<'B01'&&tEmpDate<'2013-03-16')
	    {
         alert('2013年3月16号之后入司业务序列人员才能参与2013版的新筹基本法！');
         fm.all('TrainDate').value='';
	     return false	;
	    }
     }      
      
       if(tTrainDate>='2013-03-16'){
       //业务职级必须为入司当日
        if(tAgentGrade<'B01')
        {
           if (tEmpDate > tTrainDate||tEmpDate < tTrainDate)
	       {
              alert('业务职级的筹备开始日期必须等于入司日期！');
              fm.all('TrainDate').value='';
	          return false	;
           }
        }
       }
      if(tTrainDate>='2013-07-01')
      {
         alert("7月份及之后筹备人员，烦请联系IT部门后台进行处理");
         return false;
      }
      }
    return true;
}