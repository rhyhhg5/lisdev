
<%@page contentType="text/html;charset=GBK" %>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.health.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="com.sinosoft.lis.agent.*"%>
           
<SCRIPT src="UserAdd.js"></SCRIPT>
<html>
<head> 
 <%@include file="../common/jsp/UsrCheck.jsp"%>
</head>
  
<body>
<% 
   // 输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
    
  String tManageCom = "";
  String tBranchType= "";
  String tBranchType2= "";
  String tState= "";
  String tAgentSeries= "";
  String tCountDate="";
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    Content = "页面失效,请重新登陆";
    FlagStr = Content;
  }
  else //页面有效
  {
    tManageCom = request.getParameter("ManageCom");
    tBranchType= request.getParameter("BranchType");
    tBranchType2= request.getParameter("BranchType2");
    tState= request.getParameter("state");
    tAgentSeries= request.getParameter("agentseries");
    tCountDate=request.getParameter("CountDate");
    
  }
		  
    try
  	{
  		TransferData tTransferData= new TransferData();
			tTransferData.setNameAndValue("ManageCom",tManageCom);
			tTransferData.setNameAndValue("BranchType",tBranchType); 
			tTransferData.setNameAndValue("BranchType2",tBranchType2);
			tTransferData.setNameAndValue("State",tState); 
			tTransferData.setNameAndValue("AgentSeries",tAgentSeries); 
			tTransferData.setNameAndValue("CountDate",tCountDate ); 
	  	// 准备传输数据 VData
	    VData tVData = new VData();
	    tVData.addElement(tGI);
	    tVData.addElement(tTransferData);
	    XmlExport txmlExport = new XmlExport();
	    // 数据传输
	    AgentBaseInfoReportUI tAgentBaseInfoReportUI=new AgentBaseInfoReportUI();
	    VData mResult = new VData();
	    if(tAgentBaseInfoReportUI.submitData(tVData, "Query")){
	    	mResult = tAgentBaseInfoReportUI.getResult();
	    	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
		    if (txmlExport==null)
		    {
		      return;
		    }
		    ExeSQL tExeSQL = new ExeSQL();
	
				//获取临时文件名
				String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
				String strFilePath = tExeSQL.getOneValue(strSql);
				String strVFFileName = strFilePath + tGI.Operator + "_" + FileQueue.getFileName()+".vts";
				//获取存放临时文件的路径
				String strRealPath = application.getRealPath("/").replace('\\','/');
				String strVFPathName = strRealPath +"/"+ strVFFileName;
				
				CombineVts tcombineVts = null;
	
				//合并VTS文件
				String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
				tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
				
				ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
				tcombineVts.output(dataStream);
				
				//把dataStream存储到磁盘文件
				System.out.println("存储文件到"+strVFPathName);
				AccessVtsFile.saveToFile(dataStream,strVFPathName);
				System.out.println("==> Write VTS file to disk ");
				
				System.out.println("===strVFFileName : "+strVFFileName);
				//本来打算采用get方式来传递文件路径
				//缺点是文件路径被暴露
				System.out.println("---passing params by get method");
				System.out.println("RealPath="+strVFPathName);
				response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="+strVFPathName);
				
	    }else{
	    	tError = tAgentBaseInfoReportUI.mErrors;
	    }	    
  	}
	  catch(Exception ex)
	  {
	    Content = "操作失败，原因是:" + ex.toString();
	    FlagStr = Content;
	  }

  	
  	
%>  

	
</body>

</html>