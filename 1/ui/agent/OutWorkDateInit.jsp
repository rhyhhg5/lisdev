<%
//程序名称：OutWorkDateInit.jsp
//程序功能：Init.jsp
//创建日期：2017-10-24
//创建人  ：yangjian
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

function initInpBox()
{ 
  try
  {                
    fm.all('ManageCom').value = '';
    fm.all('BranchType').value = '';
    fm.all('BranchType2').value = '';
    fm.all('GroupAgentCode').value = ''; 
    fm.all('AgentCode').value = ''; 
    fm.all('Name').value = ''; 
    fm.all('OutWorkDate').value = ''; 
    fm.all('AgentState').value = ''; 
  }
  catch(ex)
  {
    alert("在OutWorkDateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
	try
	  {
	    initInpBox();
	    initOutWorkDateGrid();
	  }
	  catch(re)
	  {
	    alert("OutWorkDateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	  }
}

function initOutWorkDateGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="管理机构";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="业务员代码	";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="业务员姓名";         //列名
        iArray[3][1]="70px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;        
        
        iArray[4]=new Array();
        iArray[4][0]="业务员核心编码";         //列名
        iArray[4][1]="60px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="离职日期（原）";         //列名
        iArray[5][1]="60px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="离职日期（改）";         //列名
        iArray[6][1]="70px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="销售渠道";         //列名
        iArray[7][1]="70px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
      	iArray[8]=new Array();
        iArray[8][0]="展业类型";         //列名
        iArray[8][1]="70px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许 */
        
     
        OutWorkDateGrid = new MulLineEnter( "fm" , "OutWorkDateGrid" ); 
        //这些属性必须在loadMulLine前
        OutWorkDateGrid.mulLineCount = 10;   
        OutWorkDateGrid.displayTitle = 1;
        OutWorkDateGrid.hiddenPlus = 1;
        OutWorkDateGrid.hiddenSubtraction = 1;
        OutWorkDateGrid.locked=0;
        OutWorkDateGrid.canSel=0;
        OutWorkDateGrid.canChk=1;
        OutWorkDateGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化OutWorkDateGrid时出错："+ ex);
      }
    }
</script>