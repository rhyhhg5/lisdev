//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;

var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function initEdorType(cObj){
	//程序会自动将#转换为'，所以需要特别注意
	mEdorType = " 1 and codealias=#2# ";
	showCodeList('agentkind',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{
	mEdorType = " 1 and codealias=#2#";
	showCodeListKey('agentkind',[cObj], null, null, mEdorType, "1");
}


//******************************************************
//提交，保存按钮对应操作
//********************************************************

function submitForm(){
	if(!getAgentSeries())
	{
	  return false;
	}
	if (mOperate!='DELETE||MAIN'){
		if (!beforeSubmit())
			return false;
		if (mOperate!="UPDATE||MAIN"){
			mOperate="INSERT||MAIN";
		}
	}
	if (!changeGroupItem())
		return false;
		
  if(fm.all('AgentGrade').value!=null && fm.all('AgentGrade').value!=''&& mOperate=="INSERT||MAIN"){
  	if(fm.all('AgentGrade').value>='G51'){
		if(!checkBranchManager()){
			return false;
		}
		}
	} 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交
}
//核查团队有无主管
function checkBranchManager(){

	var tBranchAttr;
	var tAgentGrade=fm.all('AgentGrade').value;
	var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
	if(fm.all('BranchAttr').value==null || fm.all('BranchAttr').value==''){
		alert('所属团队不能为空！');
		return false;
	}else{
		tBranchAttr=fm.all('BranchAttr').value;
		var tSql="select gradeproperty2 from laagentgrade where gradecode='"+tAgentGrade+"'";
		var arr=easyExecSql(tSql);
		var tGradeProperty2=arr[0][0];
		tSql="select * from latree where agentgroup=(select agentgroup from labranchgroup "
			+"where branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"' and branchattr='"+tBranchAttr+"')"
			+"and agentgrade in (select gradecode from laagentgrade where branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"' and gradeproperty2='"+tGradeProperty2+"')"
			+"and branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2
			+"' and agentcode in (select agentcode from laagent where agentgroup in (select agentgroup from labranchgroup "+
			"where branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"' and branchattr='"+tBranchAttr+"') and agentstate<'06') ";
		var strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);
		if(strQueryResult && tGradeProperty2==1){
			alert("该团队已存在经理级别的主管！");
			return false;
		}
		//else if(strQueryResult && tGradeProperty2==2){
		//	alert("该团队已存在营业部负责人级别的主管！");
		//	return false;
		//}
		return true;	
	}
	
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//xjh Add For PICC
	//fm.all('AgentKind').disabled = true;
	//fm.all('AgentGrade').disabled = true;
	//fm.all('AgentGroup').disabled = true;
  showInfo.close();
  fm.all('hideIsManager').value = false;
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

	//initInpBox();
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
	  initForm();
  }
  catch(re)
  {
  	alert("在LAFRAgentInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}
//***************************************
//提交前的校验、计算
//***************************************
function beforeSubmit(){
	//先校验必录项
	if( verifyInput() == false ) return false;
	//新增校验联系电话
	var Phone = fm.all('Phone').value.trim();
	var result=CheckFixPhoneNew(Phone);
	var result2=CheckPhoneNew(Phone);
    if(result!=""&&result2!=""){
  	  alert("联系电话不符合规则，请录入正确的固话或手机号！");
	  fm.all('Phone').value='';
	  return false;
	}
    //新增校验手机号
    var Mobile = fm.all('Mobile').value.trim();
    if(Mobile!=null&&Mobile!="")
     { 
	  var result=CheckPhoneNew(Mobile);
      if(result!=""){
	  alert(result);
	  fm.all('Mobile').value='';
	  return false
	  }
    }
 	//zff  身份证尾号不能为小x的校验
	if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}
	// 验证邮件地址输入是否正确
	var tEMail = document.fm.EMail.value;
	if(tEMail != "" && tEMail != null)
	{
		var tArrValue = tEMail.split("@");
		if(tArrValue.length != 2)
		{
			alert("邮件地址输入有误！");
			return false;
		}
		var tEMailLen = tEMail.length;
		if(tEMail.indexOf("@") <= 0 || tEMail.indexOf("@") == tEMailLen-1)
		{
			alert("邮件地址输入有误！");
			return false;
		}
	}
	//性别不能进行修改
	if (mOperate=="UPDATE||MAIN")
	{
		if(fm.all('hideSex').value!=fm.all('Sex').value)
		{
		   alert("性别不能修改!");
		   return false;  
		}
		if(fm.all('hideBirthday').value!=fm.all('Birthday').value)
		{
		   alert("生日不能修改!");
		   return false;  
		}
		
     tAgentCode=fm.all('AgentCode').value;
     var sql1="";
     sql1="select agentcode from laagent where agentcode='"+tAgentCode+"' with ur"
     var arrResult1  = easyExecSql(sql1);
     if(arrResult1)
     {
		if(fm.all('hideNationality').value!=fm.all('Nationality').value)
		{
		   alert("民族不能修改!");
		   return false;  
		}
		if(fm.all('hideRgtAddress').value!=fm.all('RgtAddress').value)
		{
		   alert("户口所在地不能修改!");
		   return false;  
		}
		if(fm.all('hideNativePlace').value!=fm.all('NativePlace').value)
		{
		   alert("籍贯不能修改!");
		   return false;  
		}
		if(fm.all('hidePolityVisage').value!=fm.all('PolityVisage').value)
		{
		   alert("政治面貌不能修改!");
		   return false;  
		}
		if(fm.all('hideDegree').value!=fm.all('Degree').value)
		{
		   alert("学历不能修改!");
		   return false;  
		}
		if(fm.all('hidePostTitle').value!=fm.all('PostTitle').value)
		{
		   alert("职称不能修改!");
		   return false;  
		}
		if(fm.all('hideOldOccupation').value!=fm.all('OldOccupation').value)
		{
		   alert("原职业不能修改!");
		   return false;  
		}
		if(fm.all('hidehasExp').value!=fm.all('hasExp').value)
		{
		   alert("是否有同业经验不能修改!");
		   return false;  
		}
		if(fm.all('hideAgentGrade').value!=fm.all('AgentGrade').value)
		{
		   alert("职级不能修改!");
		   return false;  
		}
		if(fm.all('hideBranchattr').value!=fm.all('Branchattr').value)
		{
		   alert("所属团队不能修改!");
		   return false;  
		}
      }
	}
	if (mOperate!="UPDATE||MAIN")
	{
		if(document.fm.AgentCode.value != "" && document.fm.AgentCode.value != null)
		{
			alert("查询出来的数据只允许进行修改操作！");
			return false;
		}
		if(fm.all('hiddenSaleQuaf').value=='N'){
			alert('没有得到正确的资格证信息．');
			return false;
		}
//		alert(fm.all('hiddenSaleQuaf').value);
//		alert(fm.all('salequaf').value);
//		if(fm.all('hiddenSaleQuaf').value!=null||fm.all('hiddenSaleQuaf')!=""){
		if(fm.all('salequaf').value=='Y'){
		if(fm.all('QuafNo').value==null||fm.all('QuafNo').value==''){
			alert('请录入资格证书号，位数为20位．');
			fm.QuafNo.focus();
			return false;
		}
		var tQuafNo=fm.all('QuafNo').value;
		if(tQuafNo.length!=20){
			alert('资格证书号录入错误，位数应为20位．');
			fm.QuafNo.focus();
			return false;
		}
		if(fm.all('Quafstartdate').value==null||fm.all('Quafstartdate').value==''){
			alert('资格证取证日期为必录！');
			fm.Quafstartdate.focus();
			return false;
		}
		if(fm.all('QuafEndDate').value==null||fm.all('QuafEndDate').value==''){
			alert('资格证到期日期为必录！');
			fm.QuafEndDate.focus();
			return false;
		}
		if(fm.all('Quafstartdate').value>fm.all('QuafEndDate').value){
			alert('到期日期必须大于取证日期！');
			fm.QuafEndDate.focus();
			return false;
		}
		}
	}
	else if(mOperate=="UPDATE||MAIN"){
		if(fm.all('hiddenSaleQuaf').value==null||fm.all('hiddenSaleQuaf').value==''){
//			if(fm.all('salequaf').value=='N'){
//				alert('不能将代理人资格证改为否，系统不允许新增无资格证的代理人信息．');
//				return false;
//			}
		}
		else if(fm.all('hiddenSaleQuaf').value=='Y'){
			if(fm.all('salequaf').value=='N'){
//				alert('不能将代理人资格证改为否，系统不允许新增无资格证的代理人信息．');
//				return false;
			}else if(fm.all('salequaf').value=='Y'){
				if(fm.all('QuafNo').value==null||fm.all('QuafNo').value==''){
					alert('资格证书号录入错误，请录入新的２０位的资格证书号或原资格证书号．');
					fm.QuafNo.focus();
					return false;
				}
				
				if(fm.all('QuafNo').value!=fm.all('hiddenQuafNo').value){
					var tQuafNo=fm.all('QuafNo').value;
					if(tQuafNo.length!=20){
						alert('资格证书号录入错误，请录入新的２０位的资格证书号或原资格证书号．');
						fm.QuafNo.focus();
						return false;
					}
				}
				
				if(fm.all('Quafstartdate').value==null||fm.all('Quafstartdate').value==''){
					alert('资格证取证日期为必录！');
					fm.Quafstartdate.focus();
					return false;
				}
				if(fm.all('QuafEndDate').value==null||fm.all('QuafEndDate').value==''){
					alert('资格证到期日期为必录！');
					fm.QuafEndDate.focus();
					return false;
				}
				if(fm.all('Quafstartdate').value>fm.all('QuafEndDate').value){
			        alert('到期日期必须大于取证日期！');
			          fm.QuafEndDate.focus();
			           return false;
		          }
			}
		}
		else if(fm.all('hiddenSaleQuaf').value=='N'){
			if(fm.all('salequaf').value=='Y'){
				if(fm.all('QuafNo').value==null||fm.all('QuafNo').value==''){
					alert('请录入资格证书号，位数为２０位．');
					fm.QuafNo.focus();
					return false;
				}
				var tQuafNo=fm.all('QuafNo').value;
				if(tQuafNo.length!=20){
					alert('资格证书号录入错误，位数应为２０位．');
					fm.QuafNo.focus();
					return false;
				}
				if(fm.all('Quafstartdate').value==null||fm.all('Quafstartdate').value==''){
					alert('资格证取证日期为必录！');
					fm.Quafstartdate.focus();
					return false;
				}
				if(fm.all('QuafEndDate').value==null||fm.all('QuafEndDate').value==''){
					alert('资格证到期日期为必录！');
					fm.QuafEndDate.focus();
					return false;
				}
				if(fm.all('Quafstartdate').value>fm.all('QuafEndDate').value){
			alert('到期日期必须大于取证日期！');
			fm.QuafEndDate.focus();
			return false;
		}
			}else if(fm.all('salequaf').value=='N'){
//				if(fm.all('QuafNo').value!=null&&fm.all('QuafNo').value!=''){
//					alert('没有代理人资格证，不能录入资格证书号！');
//					fm.QuafNo.focus();
//					return false;
//				}
//				if(fm.all('Quafstartdate').value!=null&&fm.all('Quafstartdate').value!=''){
//					alert('没有代理人资格证，不能录入取证日期！');
//					fm.Quafstartdate.focus();
//					return false;
//				}
//				if(fm.all('QuafEndDate').value!=null&&fm.all('QuafEndDate').value!=''){
//					alert('没有代理人资格证，不能录入到期日期！');
//					fm.QuafEndDate.focus();
//					return false;
//				}
			}
		}
	}
	if(fm.all('hasExp').value=='Y'&&(fm.all('ExpYear').value==null||fm.all('ExpYear').value=='')){
		alert('有同业经验，同业年限为必录！');
		fm.ExpYear.focus();
		return false;
	}
	else if(fm.all('hasExp').value=='N'&&fm.all('ExpYear').value!=null&&fm.all('ExpYear').value!=''){
		alert('没有同业经验，不能录入同业年限！');
		fm.ExpYear.focus();
		return false;
	}
			
	
	var tAgentSeries = document.fm.AgentSeries.value ;
    var branchattr=fm.all('BranchAttr').value;
    var sql="select branchlevel from labranchgroup where branchtype='3' and branchtype2='01' and branchattr='"+branchattr+"'";
    var branchlevel=easyExecSql(sql);
    if(branchlevel[0][0]==null||branchlevel[0][0]==''){
    alert("所属团队不存在！");
    document.fm.BranchAttr.value = "";
    return false;
    }
    else{
    	if(fm.all('AgentGrade').value!=null && fm.all('AgentGrade').value!=""){
    		if(fm.all('AgentGrade').value>='G51')
    		{
    		var codegrade=fm.all('AgentGrade').value;
    		var sql="select gradeproperty2 from laagentgrade where branchtype='3' and branchtype2='01' and gradecode='"+codegrade+"'";
    		var gradeproperty2=easyExecSql(sql);
    		if(gradeproperty2[0][0]==null||gradeproperty2[0][0]==''){
    			alert("职级不存在！");
    			return false;
    		}
    		else{
    			if(gradeproperty2[0][0]<=1 && branchlevel[0][0] != '43'){
    				alert("职级与所属团队级别不一致。");
    				return false;
    			}
    			//else if(gradeproperty2[0][0]==2 && branchlevel[0][0] == '41'){
    			//	alert("职级与所属团队级别不一致。职级为营业部负责人,所属团队应为营业部!");
    			//	return false;
    			//}
    	    }
    	  }
    	  else 
    	  {
    	  if(branchlevel[0][0]!='43'){
    		alert("业务职级人员所属团队应为营业部！");
    		return false;
    	  }
        }
  	 	}
   }
    
	//添加操作
    var strReturn = checkIdNo('0',trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
	if (strReturn != ""){
		alert(strReturn);
		return false;
	}
	return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,82,*";
	}
}


//Click事件，当点击增加图片时触发该函数
function addClick(){
	//下面增加相应的代码
	mOperate="INSERT||MAIN";
	showDiv(operateButton,"false");
	showDiv(inputButton,"true");
	fm.all('AgentCode').value = '';
	fm.all('IDNo').value = '';
	fm.all('AgentGrade').value = '';
	fm.all('AgentGroup').value = '';
	fm.all('ManageCom').value = '';
	fm.all('UpAgent').value = '';
	//fm.all('AgentKind').disabled = false;
	//fm.all('AgentGrade').disabled = false;
	//fm.all('AgentGroup').disabled = false;
}

//********************************************
//Click事件，当点击“修改”图片时触发该函数
//********************************************
function updateClick()
{
  
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的代理人记录！');
    return false;
  }else
  {
//  var tAgentState=fm.all('AgentState').value;
//  if(tAgentState=='2' || tAgentState.length==2){
//  	alert('修改失败，此业务员已经入司，不能对其信息进行修改。如要修改，请联系总公司银保部解决。');
//  	return false;
//  }
    //下面增加相应的代码
    if(fm.all('AgentState').value>5){
  	alert('修改失败，此代理人处于离职状态，不能对其进行修改。');
  	return false;
  	}
//zff  身份证尾号不能为小x的校验
	if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}
    if (confirm("您确实想修改该记录吗?"))
    {
      	mOperate="UPDATE||MAIN";
        submitForm();
      
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
      return false;
    }
  }
}
//*********************************************
//**
//Click事件，当点击“查询”图片时触发该函数
//**
//***********************************************
function queryClick()
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";

   var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
    
    showInfo=window.open("./LABankAgentNACQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要删除的代理人记录！');
  }else
  {
  var tAgentState=fm.all('AgentState').value;
  if(tAgentState=='2' || tAgentState.length==2){
  	alert('修改失败，此业务员已经入司，不能对其信息进行删除。');
  	return false;
  }
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function changeGroupItem()
{
	if (trim(document.fm.BranchAttr.value) == "")
	{
	  alert("请录入销售机构代码!");		
	  return false;
	}
	//查询当前机构的管理员
  var tSQL = "select branchmanager,branchmanagername, ManageCom, AgentGroup,BranchLevel from labranchgroup  ";
  tSQL += " where branchattr='"+document.fm.BranchAttr.value+"' and endflag='N'";
  tSQL += " and branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"'";
  var arr = easyExecSql(tSQL);
  if( arr == null||arr==''){
        alert("该销售机构不存在或者已经停业!");
		document.fm.BranchAttr.value = "";
		document.fm.UpAgent.value = "";
		document.fm.BranchManager.value = "";
		document.fm.BranchLevel.value = "";
		document.fm.AgentGroup.value = "";
		document.fm.ManageCom.value = "";
		return false;
        }
   else{     
		document.fm.UpAgent.value=arr[0][1];
		document.fm.BranchManager.value=arr[0][0];
		fm.all('ManageCom').value = trim(arr[0][2]);
        fm.all('AgentGroup').value = trim(arr[0][3]);	
        fm.all('BranchLevel').value = trim(arr[0][4]);	 	
	}
  var tBranchLevel = '';
  tBranchLevel = trim(arr[0][4]);
//进行验证 如果录入的人员是经理 验证该机构是否已存在经理
	if (trim(document.fm.AgentSeries.value) != null&&trim(document.fm.AgentSeries.value)!='')
	{	
	//if ("2" == trim(document.fm.AgentSeries.value))
	//{
	//	if("41"==tBranchLevel){
	//		alert("营业部负责人不能在营业组中!");
	//		return false;
	//	}
	//	//验证当前机构是否存在经理
	//	//alert("进行机构中是否存在经理的验证");
	//}
	//else 
	if("1" == trim(document.fm.AgentSeries.value)){
		if("43" !=tBranchLevel){
			alert("渠道经理不能在营业部中!");
			return false;
		}
	}
  }
  else{
  //当只有业务职级时,需要与销售机构进行校验
   if("43"!=tBranchLevel){
      alert("业务序列人员只能在营业部中!");
      return false;
   }
  }
	return true;
}
function changeGroup()
{
   var strSQL = "";
   strSQL = "select a.BranchAttr,a.ManageCom,a.BranchManager,a.AgentGroup,a.BranchLevel,a.BranchJobType,"
           +" (select b.BranchManager from labranchGroup b where b.AgentGroup = a.UpBranch and (b.state<>'1' or b.state is null)) upAgent from LABranchGroup a "
           +"where 1=1 and a.BranchType = '"+fm.all('BranchType').value+"' and a.BranchType2='"+fm.all('BranchType2').value+"' and a.EndFlag <> 'Y' and (a.state<>'1' or a.state is null)"
           + getWherePart('a.BranchAttr','AgentGroup');
   var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
   if (!strQueryResult)
   {
   	alert('不存在该销售机构！');
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('ManageCom').value = '';
   	fm.all('BranchManager').value = '';
   	fm.all('AgentGroup').value = '';
   	fm.all('BranchLevel').value = '';
   	return false;
   }
   var arr = decodeEasyQueryResult(strQueryResult);

  // fm.all('BranchAttr').value = trim(arr[0][0]);
   fm.all('ManageCom').value = trim(arr[0][1]);
   fm.all('BranchManager').value = trim(arr[0][2]);
   fm.all('AgentGroup').value = trim(arr[0][3]);
   fm.all('BranchLevel').value = trim(arr[0][4]);
 //  alert(fm.all('BranchAttr').value);
   return true;
}

function afterQuery(arrQueryResult)
{
  var arrResult = new Array();
	if( arrQueryResult != null )
	{
	    initInpBox()	
		arrResult = arrQueryResult;
		var sqlAgent="select getUniteCode('"+arrResult[0][0]+"') from dual with ur";
     	var arrResultAgent = easyExecSql(sqlAgent);
     	if(arrResultAgent)
     	{
     		fm.all('GroupAgentCode').value = arrResultAgent[0][0];
     	}else{
     		fm.all('GroupAgentCode').value = '';
     	}
		fm.all('AgentCode').value = arrResult[0][0];
		fm.all('Name').value = arrResult[0][1];
		fm.all('Sex').value = arrResult[0][2];
		fm.all('hideSex').value = arrResult[0][2];		
		//fm.all('SexName').value = getArrValueBySQL("")
		fm.all('Birthday').value = arrResult[0][3];
		fm.all('hideBirthday').value = arrResult[0][3];
		fm.all('NativePlace').value = arrResult[0][4];
		fm.all('hideNativePlace').value = arrResult[0][4];
		fm.all('Nationality').value = arrResult[0][5];
		fm.all('hideNationality').value = arrResult[0][5];
		fm.all('RgtAddress').value = arrResult[0][6];
		fm.all('hideRgtAddress').value = arrResult[0][6];
		fm.all('HomeAddress').value = arrResult[0][7];
		fm.all('ZipCode').value = arrResult[0][8];
		fm.all('Phone').value = arrResult[0][9];
		fm.all('Mobile').value = arrResult[0][11];
		fm.all('EMail').value = arrResult[0][12];
		fm.all('IDNo').value = arrResult[0][13];
		fm.all('hideIdNo').value = arrResult[0][13];//身份证校验
		fm.all('PolityVisage').value = arrResult[0][14];
		fm.all('hidePolityVisage').value = arrResult[0][14];
		fm.all('Degree').value = arrResult[0][15];
		fm.all('hideDegree').value = arrResult[0][15];
		fm.all('GraduateSchool').value = arrResult[0][16];
		fm.all('Speciality').value = arrResult[0][17];
		fm.all('PostTitle').value = arrResult[0][18];
		fm.all('hidePostTitle').value = arrResult[0][18];
		fm.all('OldCom').value = arrResult[0][19];
		fm.all('OldOccupation').value = arrResult[0][20];
		fm.all('hideOldOccupation').value = arrResult[0][20];
		fm.all('HeadShip').value = arrResult[0][21];
		fm.all('EmployDate').value = arrResult[0][22];
		fm.all('hideEmployDate').value = arrResult[0][22];
		fm.all('AgentState').value = arrResult[0][23];
		if(arrResult[0][23]=='01'||arrResult[0][23]=='02')
		{
		   fm.all('AgentStateName').value ='在职';
		}
		else if(arrResult[0][23]=='03'||arrResult[0][23]=='04')
		{
		  fm.all('AgentStateName').value ='离职登记';
		}else if(arrResult[0][23]=='0'||arrResult[0][23]=='0'){
			fm.all('AgentStateName').value ='待审核';
		}else if(arrResult[0][23]=='1'||arrResult[0][23]=='1'){
			fm.all('AgentStateName').value ='审核失败';
		}else if(arrResult[0][23]=='2'||arrResult[0][23]=='2'){
			fm.all('AgentStateName').value ='审核通过';
		}else
		{
		  fm.all('AgentStateName').value ='离职确认';
		}
		fm.all('Remark').value = arrResult[0][24];	
		fm.all('QuafNo').value=arrResult[0][43];
		fm.all('hiddenQuafNo').value=arrResult[0][43];
		fm.all('Quafstartdate').value = arrResult[0][44];
		fm.all('QuafEndDate').value = arrResult[0][48];
		//modify by zhuxt 增加业务员类型和委托代理合同号字段
		fm.all('AgentKind').value = arrResult[0][49];
		fm.all('RetainContno').value = arrResult[0][50];
		//end modify
		fm.all('hiddenQuafDate').value = arrResult[0][44];
		fm.all('hiddenSaleQuaf').value = arrResult[0][45];
		fm.all('salequaf').value = arrResult[0][45];
		//fm.all('salequaf').disabled=false;
		fm.all('BranchAttrName').value=arrResult[0][46];
		fm.all('RejectRemark').value=arrResult[0][47];
		//fm.all('Quafstartdate').value = arrResult[0][44];	
		fm.all('Operator').value = arrResult[0][26];
		fm.all('ManageCom').value = arrResult[0][27];
		if(arrResult[0][45]=='Y'){
//			fm.all('salequaf').value = 'Y';
			fm.all('hasQuafName').value = '是';
		}else if(arrResult[0][45]=='N'){
	//		fm.all('salequaf').value = 'N';
			fm.all('hasQuafName').value = '否';
		}
		if(arrResult[0][43]!=""){
			fm.all('salequaf').value = 'Y';
			fm.all('hasQuafName').value = '是';
		}
//		if(arrResult[0][34]=='2')
//		{
//           fm.all('AgentGrade').value = arrResult[0][29];
//	       fm.all('AgentSeries').value=arrResult[0][34];
//	       fm.all('hideAgentGrade').value = arrResult[0][29];
//	       showOneCodeNametoAfter('AgentGrade','AgentGrade','AgentGradeName');
//		}
//		else if(arrResult[0][34]=='0')
//		{
//		 fm.all('AgentGrade1').value = arrResult[0][41];
//	     fm.all('AgentSeries1').value=arrResult[0][42];
//	     fm.all('hideAgentGrade1').value = arrResult[0][41];
//	     showOneCodeNametoAfter('AgentGrade','AgentGrade1','AgentGradeName1');
//		}
//		else
//		{
		  fm.all('AgentGrade').value = arrResult[0][29];
		  fm.all('AgentSeries').value=arrResult[0][34];
//		  fm.all('AgentGrade1').value = arrResult[0][41];
//		  fm.all('AgentSeries1').value=arrResult[0][42];
		  fm.all('hideAgentGrade').value = arrResult[0][29];
//		  fm.all('hideAgentGrade1').value = arrResult[0][41];
		  showOneCodeNametoAfter('AgentGrade','AgentGrade','AgentGradeName');
//		  showOneCodeNametoAfter('AgentGrade','AgentGrade1','AgentGradeName1');
//		}

	  fm.all('hasExp').value=arrResult[0][39];
	  fm.all('hidehasExp').value=arrResult[0][39];
	  fm.all('ExpYear').value=arrResult[0][40];
		//显式机构代码	
		fm.all('BranchAttr').value = arrResult[0][35];
		fm.all('hideBranchattr').value=	arrResult[0][35];
		//fm.all('GroupAgentCode').value=arrResult[0][51];
		fm.all('AgentGrade').readOnly=true ;
		fm.all('BranchAttr').readOnly=true ;	
		//fm.all('AgentGrade1').readOnly=true ;
		fm.all('IDNo').readOnly=true ;
		fm.all('Birthday').readOnly=true ;
		fm.all('Sex').readOnly = true;	
//		document.fm.AgentGrade1.readOnly=true ;
		// 查询并显示编码所代表的中文意思
		showOneCodeNametoAfter('Sex','Sex','SexName');
		showOneCodeNametoAfter('Nationality','Nationality','NationalityName');
		showOneCodeNametoAfter('NativePlaceBak','RgtAddress','RgtAddressName');
		showOneCodeNametoAfter('NativePlaceBak','NativePlace','NativePlaceName');
		showOneCodeNametoAfter('polityvisage','PolityVisage','PolityVisageName');
		showOneCodeNametoAfter('Degree','Degree','DegreeName');
		showOneCodeNametoAfter('posttitle','PostTitle','PostTitleName');
		showOneCodeNametoAfter('occupationcode','OldOccupation','OldOccupationName');
		showOneCodeNametoAfter('yesno','hasExp','OldOccupationName');
		//modify by zhuxt 20141105 查询并显示业务员类型编码所代表的中文意思
		showOneCodeNametoAfter('agenttypecode','AgentKind','AgentTypeName');
		//进行主管查询
		 var tSQL = "select branchmanager,branchmanagername from labranchgroup  ";
		  tSQL += " where branchattr='"+document.fm.BranchAttr.value+"' and endflag='N'";
		  tSQL += " and branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"'";
		  var arr = easyExecSql(tSQL);   
		  document.fm.UpAgent.value=arr[0][1];
		  document.fm.BranchManager.value=arr[0][0];
		//showCodeName();
		  fm.all('AgentCode').value = arrResult[0][0];
		  tAgentCode=fm.all('AgentCode').value;
          var sql="";
          sql="select agentcode from laagent where agentcode='"+tAgentCode+"' with ur"
          var arrResult1  = easyExecSql(sql);
          if(arrResult1)
          {
          tdisable();
          }
	}
}

function showCodeName()
{
	var tSQL = "";
	var tCode = "";
	// 性别
	tCode = trim(document.fm.Sex.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'sex' and code = '"+tCode+"'";
		document.fm.SexName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 民族
	tCode = trim(document.fm.Nationality.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'nationality' and code = '"+tCode+"'";
		document.fm.NationalityName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 户口所在地
	tCode = trim(document.fm.RgtAddress.value);
	if(tCode != "" || tCode != null)
	{    
		tSQL = "select CodeName from ldcode where codetype = 'nativeplacebak' and code = '"+tCode+"'";
		document.fm.RgtAddressName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 籍贯
	tCode = trim(document.fm.NativePlace.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'nativeplacebak' and code = '"+tCode+"'";
		document.fm.NativePlaceName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 政治面貌
	tCode = trim(document.fm.PolityVisage.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'polityvisage' and code = '"+tCode+"'";
		document.fm.PolityVisageName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 学历
	tCode = trim(document.fm.Degree.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'degree' and code = '"+tCode+"'";
		document.fm.DegreeName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 职称
	tCode = trim(document.fm.PostTitle.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'posttitle' and code = '"+tCode+"'";
		document.fm.PostTitleName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 原职业
	tCode = trim(document.fm.OldOccupation.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select trim(OccupationName)||'-'||workname from LDOccupation where OccupationCode = '"+tCode+"'";
		document.fm.OldOccupationName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 机构主管
	tCode = trim(document.fm.BranchAttr.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select branchmanagername,branchmanager from labranchgroup  ";
    tSQL += " where branchattr='" + tCode + "' ";
    tSQL += " and branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"'";
    var strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
    var arr = decodeEasyQueryResult(strQueryResult);   
    document.fm.UpAgent.value = arr[0][0];
    document.fm.BranchManager.value = arr[0][1];
	}

	tSQL = "";
	tCode = "";
}

//检查身份证号是否重复
function changeIDNo(){
	if(fm.IDNo.value==''||fm.IDNo.value==null){
		alert('新增人员身份证号不能为空！');
		return false;
	}else{
		if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == '')){
			var tSql="select agentstate from laagent where agentstate<='05' and idno='"+fm.IDNo.value+"' and idnotype='0' ";
			var strQueryResult=easyQueryVer3(tSql,1,1,1);
			if(strQueryResult){
					alert('该代理人已在职或为准离职状态!不能再次做增员处理');
					fm.all('IDNo').value = '';
					return false;
			}
			return true;
		}else{
			var tIdNo=fm.all('IDNo').value;
			var thideIdNo=fm.all('hideIdNo').value;
			if(tIdNo==thideIdNo){
				return true;
			}
			var tSql="select agentstate from laagent where agentstate<='05' and idno='"+fm.IDNo.value+"' and idnotype='0' ";
			var strQueryResult=easyQueryVer3(tSql,1,1,1);
			if(strQueryResult){
					alert('该代理人已在职或为准离职状态!不能再次做增员处理');
					fm.all('IDNo').value = '';
					return false;
			}
			return true;
		}
		
	}
}

/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 
function afterCodeSelect( cCodeName, Field )
{
	try	{
		if( cCodeName == "AgentGrade" )
		{
			getAgentSeries();
		}
	}
	catch( ex ) {
	}
	
}
*/
function getAgentSeries()
{
	var tAgentGrade = fm.all("AgentGrade").value ;
	if(tAgentGrade != null && tAgentGrade != '')
	{
	  var tsql="select gradeproperty2 from laagentgrade where  gradecode='"+tAgentGrade+"'";
	  var strQueryResult  = easyQueryVer3(tsql, 1, 0, 1);
    
	  if (!strQueryResult)
	  {
	  	alert("查询主管职级出错！");
	  	return false ;
	  }	
	  var arr = decodeEasyQueryResult(strQueryResult); 
	  fm.all("AgentSeries").value = arr[0][0];
	}

	return  true;
}

/***************************************
 * 根据参数查询结果
 *     参数：pmSQL  查询用的SQL文
 *     返回：查询结果（要求SQL文的结果是唯一值）

function getArrValueBySQL(pmSQL)
{
	alert(pmSQL);
	var strQueryResult  = easyQueryVer3(pmSQL, 1, 0, 1);
	
	if (!strQueryResult)
		return "";
		
	var arr = decodeEasyQueryResult(strQueryResult);
	
	return arr[0][0];
}


//次函数暂没用到
function salequafChange(){
	alert('mnmn');
	if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!=''){
		if(fm.all('hiddenSaleQuaf').value==''||fm.all('hiddenSaleQuaf').value==null){
			if(fm.all('salequaf').value=='N'){
				alert('资格证不能选择否，请选择是或不作变动．');
				return false;
			}
		}else if(fm.all('hiddenSaleQuaf').value=='Y'){
			if(fm.all('salequaf').value=='N'||fm.all('salequaf').value==''){
				alert('资格证不能选择否，请选择是或不作变动．');
				return false;
			}
		}else{
			return true;
		}
	}
}




 ***************************************/
function tdisable()
{
        document.getElementById('AgentCode').readOnly=true;
        document.getElementById('Name').readOnly=true;
        document.getElementById('Sex').readOnly=true;
        document.getElementById('Birthday').readOnly=true;
        document.getElementById('IDNo').readOnly=true;
        document.getElementById('Nationality').readOnly=true;
        document.getElementById('RgtAddress').readOnly=true;
        document.getElementById('NativePlace').readOnly=true;
        document.getElementById('PolityVisage').readOnly=true;
        document.getElementById('Degree').readOnly=true;
        document.getElementById('GraduateSchool').readOnly=true;
        document.getElementById('Speciality').readOnly=true;
        document.getElementById('PostTitle').readOnly=true;
        document.getElementById('HomeAddress').readOnly=true;
        document.getElementById('ZipCode').readOnly=true;
        //document.getElementById('Mobile').readOnly=true;
        document.getElementById('HeadShip').readOnly=true;
        document.getElementById('OldCom').readOnly=true;
        document.getElementById('OldOccupation').readOnly=true;
        document.getElementById('EmployDate').readOnly=true;
        document.getElementById('Remark').readOnly=true;
        document.getElementById('AgentStateName').readOnly=true;
        document.getElementById('AgentStateName').readOnly=true;
        document.getElementById('hasExp').readOnly=true;
        document.getElementById('ExpYear').readOnly=true;
        document.getElementById('RejectRemark').readOnly=true;
        document.getElementById('AgentGrade').readOnly=true;
        document.getElementById('BranchAttr').readOnly=true;
        document.getElementById('salequaf').readOnly=true;
}