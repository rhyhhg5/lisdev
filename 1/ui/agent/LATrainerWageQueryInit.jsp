  <%
//程序名称：LATrainerWageQueryInit.jsp
//程序功能：
//创建日期：2018-6-7
//创建人  ：wangQingMin
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
        
  	fm.all('PManageCom').value = '';
  	fm.all('PManageComName').value = ''; 
  	fm.all('CManageCom').value = '';
  	fm.all('CManageComName').value = ''; 
    fm.all('WageNo').value = '';   
    fm.all('BranchType').value = '<%=BranchType%>';     
    fm.all('BranchType2').value = '<%=BranchType2%>';
    fm.all('BranchAttr').value='';
  }
  catch(ex)
  {
    alert("在LATrainerWageQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
 var AgentQueryGrid ;
 
// 保单信息列表的初始化
function initAgentQueryGrid()
  {                               
    var iArray = new Array();
    try
    {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            		//列最大值
    iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="组训人员工号";         	//列名
    iArray[1][1]="80px";              	//列宽
    iArray[1][2]=200;            	        //列最大值
    iArray[1][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="组训人员姓名";         	//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=120;            	        //列最大值
    iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="组训人员职级";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            	        //列最大值
    iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构代码";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            	        //列最大值
    iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
 
    iArray[5]=new Array();
    iArray[5][0]="管理机构名称";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=200;            	        //列最大值
    iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
   
    
    iArray[6]=new Array();                                                              
    iArray[6][0]="营业部代码";         		//列名                                             
    iArray[6][1]="80px";            		//列宽                                         
    iArray[6][2]=200;            	        //列最大值                                       
    iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                        
   
    iArray[7]=new Array();                                                              
    iArray[7][0]="营业部名称";         		//列名                                             
    iArray[7][1]="80px";            		//列宽                                         
    iArray[7][2]=200;            	        //列最大值                                       
    iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                        
   
    
    iArray[8]=new Array();                                                                             
    iArray[8][0]="基本工资";         		//列名                                                         
    iArray[8][1]="60px";            		//列宽                                                         
    iArray[8][2]=200;            	        //列最大值                                                       
    iArray[8][3]=0;                                                                                    
                                                                                                       
    iArray[9]=new Array();                                                                             
    iArray[9][0]="岗位津贴";         		//列名                                                             
    iArray[9][1]="60px";            		//列宽                                                         
    iArray[9][2]=200;            	        //列最大值                                                       
    iArray[9][3]=0;                                                                                    
                                                                                                                     
    iArray[10]=new Array();                                                                             
    iArray[10][0]="月绩效工资";         		//列名                                                         
    iArray[10][1]="80px";            		//列宽                                                         
    iArray[10][2]=200;            	        //列最大值                                                       
    iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许                                        
           
    iArray[11]=new Array();                                          
    iArray[11][0]="奖励加款";                                                            
    iArray[11][1]="60px";                                               
    iArray[11][2]=200;            	                                               
    iArray[11][3]=0;                                                       
                                              
    iArray[12]=new Array();             
    iArray[12][0]="奖励扣款";         
    iArray[12][1]="60px";            
    iArray[12][2]=200;            	    
    iArray[12][3]=0;                   	
                                        
    iArray[13]=new Array();             
    iArray[13][0]="其它1";         		
    iArray[13][1]="50px";            
    iArray[13][2]=200;            	    
    iArray[13][3]=0;                   	
                                        
    iArray[14]=new Array();             
    iArray[14][0]="其它2";         		
    iArray[14][1]="50px";            
    iArray[14][2]=200;            	    
    iArray[14][3]=0;                   	
                                        
    iArray[15]=new Array();             
    iArray[15][0]="本月应发薪资";       
    iArray[15][1]="80px";            
    iArray[15][2]=200;            	    
    iArray[15][3]=0;                    

    AgentQueryGrid = new MulLineEnter( "fm" , "AgentQueryGrid" ); 
    //这些属性必须在loadMulLine前
    AgentQueryGrid.displayTitle = 1;
    AgentQueryGrid.hiddenPlus = 1;
    AgentQueryGrid.hiddenSubtraction = 1;
    AgentQueryGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
 }
function initForm()
{
  try
  {
    initInpBox();
    
    initAgentQueryGrid();
  }
  catch(re)
  {
    alert("LATrainerWageQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误2!");
  }
}

</script>
