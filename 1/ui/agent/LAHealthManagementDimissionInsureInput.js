//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var mDepartDate="";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
  if(!checkCont()){
  	return false;
  }  
  if (mOperate=="")
  {
    if(fm.all('BranchType').value=='7' && fm.all('BranchType2').value=='01'){
    	if(!checkDepartDate()){
    		return false;
    	}
    }
  	mOperate="INSERT||MAIN";
  }
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交
}
function checkDepartDate(){
	var tCurDate=new Date();
	var tEmpDate=fm.all('DepartDate').value;
	
	var strSQL = "select '1' from dual where 1=1 and current date='"+tEmpDate+"'";
    var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
    if (!strQueryResult)
    {
        alert("离职日期不是当前日期！操作失败。");
        fm.all('DepartDate').value='';
		return false;
	}
	return true;
}
function checkCont(){
	var pmAgentCode=fm.all('AgentCode').value;
	var tSQL = "";
	var tBranchtype=fm.all('Branchtype').value;
	if(tBranchtype=='1'){
		tSQL ="select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"') and grpcontno='00000000000000000000' and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and stateflag<>'2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	 
		tSQL+=" union select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"')  and grpcontno='00000000000000000000'  and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and stateflag<>'2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;		
	
		tSQL +="union select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  aagentcode=getAgentCode('"+pmAgentCode+"') and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and stateflag<>'2'  and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;
	 
		tSQL+=" union select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"')   and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and stateflag<>'2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;		
		strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
		if(strQueryResult)
  		{//如果有未签单的或未回执回销的
  			alert("此业务员还有未签单的或未回执回销的保单，不能做离职确认！");
  			return false ;
  		}else{
  			tSQL ="select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"') and grpcontno='00000000000000000000' and   (appflag<>'1' or appflag is null ) and stateflag='2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	 
			tSQL+=" union select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"')  and grpcontno='00000000000000000000'  and  (customgetpoldate is null or getpoldate is null) and stateflag='2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;		
	
			tSQL +="union select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"') and   (appflag<>'1' or appflag is null ) and stateflag='2'  and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;
	 
			tSQL+=" union select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"')   and  (customgetpoldate is null or getpoldate is null) and stateflag='2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;		
			strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
			if(strQueryResult)
			{//如果有未签单的或未回执回销的
  				if(!confirm("有保单失效但没有交回回执，若复效后会影响后续代理人的离职!仍要继续进行离职确认吗？"))
  				{
  					return false;
  				}
  			}
  			return true;
  		}
	}else{
		tSQL ="select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"') and grpcontno='00000000000000000000' and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	 
		tSQL+=" union select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"')  and grpcontno='00000000000000000000'  and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;		
	
		tSQL +="union select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"') and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;
	 
		tSQL+=" union select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"')   and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;		
		strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
		if(strQueryResult)
  		{//如果有未签单的或未回执回销的
  			alert("此业务员还有未签单的或未回执回销的保单，不能做离职确认！");
  			return false ;
  		}
  		return true;
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    //执行下一步操作
  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LADimissionInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	  
  if(!verifyInput())
  {
      return false;	
  }
  if (mDepartDate!=null&&mDepartDate!='')
  {
  	if (fm.all('DepartDate').value !=mDepartDate)
  	{
	   alert("离职日期不可修改！");
	   fm.all('DepartDate').focus();
	   return false;
  	}
  }	

 if ((fm.all('ApplyDate').value != '')||(fm.all('ApplyDate').value!=null))
 { 	
   if(fm.all("DepartDate").value<fm.all('ApplyDate').value)
   {
  		alert("离职申请日期不能大于离职日期");
  		return false;
   }
 }  	   
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证代理人编码的合理性
function checkValid()
{ 
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  var strSQL = "";
  var strQueryResult = null;
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  var tManageCom =getManageComLimit(); 
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select getunitecode(AgentCode),name,managecom from LAAgent where 1=1 "+strAgent//getWherePart('AgentCode')           
             +" and branchtype='"+tBranchType+"'"
             +" and branchtype2='"+tBranchType2+"'";
     strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
 
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员或此业务员没有离职登记！");
    fm.all('AgentCode').value  = "";
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  else
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult); 
  	fm.all('AgentName').value = arrDataSet[0][1]; 
  	mManageCom=arrDataSet[0][2]; 	  
  	if(mManageCom.indexOf(tManageCom)<0)
  	{
  	  alert("您没有该业务员的操作权限!");
  	  fm.all('AgentCode').value  = "";
      fm.all('DepartTimes').value = "";
      initInpBox();
      return false;
  	}  	  
  }
  //查询成功则拆分字符串，返回二维数组
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  strQueryResult = null;
  var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  strSQL = "select getunitecode(a.AgentCode),b.name,a.DepartTimes,a.DepartRsn,a.DepartDate,a.ApplyDate,"
	       + "a.QualityDestFlag,a.PbcFlag,a.ReturnFlag,a.AnnuityFlag,a.VisitFlag,a.BlackFlag,"
	       + "a.BlackListRsn,case when (a.DepartState='03' or a.DepartState='04' or a.DepartState='05') then '离职未确认'"
	       + " when (a.DepartState='08' or a.DepartState='07' or a.DepartState='06') then '离职已确认' else '' end,"
	       + "a.DestoryFlag,a.operator,a.makedate from ladimission a,laagent b"
	       + " where a.agentcode=b.agentcode " 
	       //+getWherePart('a.AgentCode','AgentCode')
	       + strAgent
	       +" order by a.applydate desc";
	       
  strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult)
    fm.all('DepartTimes').value = '1';
  else
  {
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    afterQuery(arrDataSet);
  }

	document.fm.OweMoney.value = getDebtMark(document.fm.AgentCode.value);
	queryAgentWage(document.fm.AgentCode.value);
	queryAgentCont(fm.all('AgentCode').value);
}
//用来显示返回的选项
function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.all('AgentCode').value = arrResult[0][0];
    fm.all('AgentName').value = arrResult[0][1];
    fm.all('DepartTimes').value = arrResult[0][2];    
    fm.all('DepartRsn').value = arrResult[0][3];
    showOneCodeNametoAfter('DepartRsn','DepartRsn');
    fm.all('DepartDate').value = arrResult[0][4];
    mDepartDate=arrResult[0][4];
    fm.all('ApplyDate').value=arrResult[0][5];
    fm.all('QualityDestFlag').value = arrResult[0][6];
    showOneCodeNametoAfter('YesNo','QualityDestFlag');
    fm.all('PbcFlag').value = arrResult[0][7];
    showOneCodeNametoAfter('YesNo','PbcFlag');
    fm.all('ReturnFlag').value = arrResult[0][8];
    showOneCodeNametoAfter('YesNo','ReturnFlag');
    fm.all('AnnuityFlag').value = arrResult[0][9];
    showOneCodeNametoAfter('YesNo','AnnuityFlag');
    fm.all('VisitFlag').value = arrResult[0][10];
    showOneCodeNametoAfter('YesNo','VisitFlag');
    fm.all('BlackFlag').value = arrResult[0][11];
    showOneCodeNametoAfter('YesNo','BlackFlag');
    fm.all('BlackListRsn').value = arrResult[0][12];
    fm.all('DepartState').value = arrResult[0][13];
    fm.all('DestoryFlag').value = arrResult[0][14];
    fm.all('Operator').value=arrResult[0][15];
    fm.all('ModifyDate').value=arrResult[0][16];
    showOneCodeNametoAfter('YesNo','DestoryFlag');
    // 设置离职欠费标记
	document.fm.OweMoney.value = getDebtMark(arrResult[0][0]);
	// 表示佣金列表
	queryAgentWage(arrResult[0][0]);
	queryAgentCont(arrResult[0][0]);
  }   
}

function getAgentName(tAgentCode)
{
  var tAgentName='';
  var strSQL = "select name from LAAgent where 1=1 and AgentCode='"+tAgentCode+"'";
  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  if (strQueryResult)
  {
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    tAgentName = arrDataSet[0][0]; 
  }
  return tAgentName;
}



/**************************************************************
 * 功能描述：查询是否有欠费
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function getDebtMark(pmAgentCode)
{
  var tSQL = "";
  var tDebtMark = "";
 	
  tSQL  = "select";
  tSQL += " case when value(summoney,0) < 0 then 'Y'";
  tSQL += " else 'N'";
  tSQL += "  end as aaa";
  tSQL += " from lawage";
  tSQL += " where branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"'";
  tSQL += "  and agentcode='"+pmAgentCode+"'";
  tSQL += " order by indexcalno desc";
	
  var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
  if (strQueryResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult);
  	tDebtMark = arrDataSet[0][0]; 
  }
	
  if("" == tDebtMark)
  {
	tDebtMark = "N";
  }
	
  return tDebtMark;
}

/**************************************************************
 * 功能描述：查询制定业务员佣金信息
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function queryAgentWage(pmAgentCode)
{
	var tSQL = "";
	
	tSQL  = "select getunitecode(a.AgentCode),";
	tSQL += "       b.Name,";
	tSQL += "       a.ManageCom,";
	tSQL += "       a.branchattr,";
	tSQL += "       c.Name,";
	tSQL += "       a.indexcalno,";
	tSQL += "       a.summoney";
	tSQL += "  from LAWage a,LAAgent b,LABranchGroup c";
	tSQL += " where a.AgentCode = b.AgentCode";
	tSQL += "   and b.AgentGroup = c.AgentGroup";
	tSQL += "   and a.branchtype='"+fm.all('BranchType').value+"' and a.branchtype2='"+fm.all('BranchType2').value+"'";
	tSQL += "   and a.agentcode = getagentcode('"+pmAgentCode+"')";
	tSQL += " order by a.indexcalno DESC";
	
	//执行查询并返回结果
	turnPage.queryModal(tSQL, AgentWageGrid);
}
/**************************************************************
 * 功能描述：查询制定业务员网点信息
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function queryAgentCont(pmAgentCode)
{
	var tSQL = "";
	var tBranchtype=fm.all('Branchtype').value;
	if(tBranchtype=='1')
	{
		tSQL ="select contno,prtno,appntname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"') and grpcontno='00000000000000000000' and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and stateflag<>'2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	 
		tSQL+=" union select contno,prtno,appntname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"')  and grpcontno='00000000000000000000'  and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and stateflag<>'2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;		
	
		tSQL +="union select grpcontno,prtno,grpname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"') and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and stateflag<>'2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;
	 
		tSQL+=" union select grpcontno,prtno,grpname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"')   and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and stateflag<>'2' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;		
		
	}else{
		tSQL ="select contno,prtno,appntname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"') and grpcontno='00000000000000000000' and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	 
		tSQL+=" union select contno,prtno,appntname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"')  and grpcontno='00000000000000000000'  and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;		
	
		tSQL +="union select grpcontno,prtno,grpname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"') and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;
	 
		tSQL+=" union select grpcontno,prtno,grpname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"')   and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;		
		
	}
	
	//执行查询并返回结果
	turnPage2.queryModal(tSQL, ContGrid);
}

function doDownload()
{	
	var pmAgentCode  = fm.all('AgentCode').value;
	if ( pmAgentCode == null || pmAgentCode == "" )
	{
		alert("请输入管理机构！");
		return;
	}
 
    // 书写SQL语句
	var tSQL = "";
	
	tSQL ="select contno,prtno,appntname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"') and grpcontno='00000000000000000000' and   (appflag<>'1' or appflag is null ) and stateflag<>'3'  and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	 
	tSQL+=" union select contno,prtno,appntname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lccont where  agentcode=getAgentCode('"+pmAgentCode+"')  and grpcontno='00000000000000000000'  and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;		
	
	tSQL +="union select grpcontno,prtno,grpname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"') and   (appflag<>'1' or appflag is null ) and stateflag<>'3'  and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;
	 
	tSQL+=" union select grpcontno,prtno,grpname,prem,signdate,case when customgetpoldate is null then customgetpoldate else getpoldate end  from  lcgrpcont where  agentcode=getAgentCode('"+pmAgentCode+"')   and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3'  and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;		
	
    fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "DimissionReport.jsp";
    fm.submit();
    fm.action = oldAction;
}