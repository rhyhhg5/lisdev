<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAassessmianInput.jsp
//程序功能：
//创建日期：
//创建人  zyy程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//	xx	2017-04-01 09:32	
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
    String CurDate = PubFun.getCurrentDate();    
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<script>
		var manageCom = <%=tG.ManageCom%>;
		var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
	</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LAassessmainInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LAassessmainInit.jsp"%>
</head> 
<body  onload="initForm();initElementtype();" >
  <form action="./LAassessmainSave.jsp" method=post name=fm target="fraSubmit">
  <input type=hidden name=querySQL value=""> 
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		考核主表信息查询条件
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:comcode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>        
            <td class=title>
           考核年月
        </td>
        <td class=input>
        <input  name=Indexcalno verify="考核年月|NOTNULL" elementtype=nacessary>
        </td>
        </TR>
        <tr class=common>
        <TD class = title>
             营销员职级
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentGrade verify="营销员职级|notnull&code:AgentGrade"
 				ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
 				onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
 				><Input name=AgentGradeName class="codename" elementtype=nacessary >
          </TD>
        <td class=title>
           考核状态
        </td>
        <td class=input>
        <input  name=State verify="考核状态|NOTNULL">
        </td>
        </tr>
      </table>       
         <Table class="common" align=center>
  			<Tr>
    			<Td class=button>
      				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="return easyQueryClick()">
      				<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateclick()">
    			</Td>
  			</Tr>
		</Table>
          <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">
          <input type="hidden" class=input name=BranchType2 value="<%=BranchType2%>">           
    </Div>
    <table class='common'>
	<p><font color="red">注:考核主表状态有两种，分别是0（考核未确认）、1（考核确认未归属）、2（组织归属完毕），此功能是针对考核主表职级考核状态的维护功能</font></p>
 </table>
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
	  <Div  id= "LAAgent1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid"></span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT CLASS=cssButton VALUE="首页"   TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾页"   TYPE=button onclick="turnPage.lastPage();">				
  	</div>   
  	<input type=hidden id="fmAction" name="fmAction">     
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>