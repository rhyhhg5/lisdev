<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
  //程序名称：LRCGroupUW.jsp
  //程序功能：总公司核保审核
  //创建日期：2011-11-15
  //创建人  ：Gaoyx
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
  String tContNo = "";
  String tPrtNo = "";
  String tGrpContNo = "";
  String tMissionID = "";
  String tSubMissionID = "";
  String tLoadFlag="";
  String tResource="";
  tPrtNo = request.getParameter("PrtNo");
  tGrpContNo = request.getParameter("GrpContNo");
  tMissionID = request.getParameter("MissionID");
  tSubMissionID = request.getParameter("SubMissionID");
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput) session.getValue("GI");
  tLoadFlag = request.getParameter("LoadFlag");
  tResource =request.getParameter("Resource");
  System.out.println(tGrpContNo + "asdf" + tPrtNo);
%>
<script>
	var PrtNo = "<%=tPrtNo%>";
	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
	var MissionID = "<%=tMissionID%>";
	var SubMissionID = "<%=tSubMissionID%>";	
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var LoadFlag= "<%=tLoadFlag%>";
	var Resource= "<%=tResource%>";
	//alert(LoadFlag);
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="LRCGroupUW.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LRCGroupUWInit.jsp"%>
</head>
<body onload="initForm();">
<form action="" method=post name=fmQuery target="fraSubmit">
<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGrpCont);">
    </td>
    <td class=titleImg>团体保单信息    </td>
  </tr>
</table>
<Div id="divGrpCont" style="display: ''">
  <table class=common>
    <TR class=common>
      <!--  团体投保单号码-->
      <Input class="readonly" readonly name=GrpContNo type="hidden">
      <TD class=title8>印刷号码      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=PrtNo>
      </TD>
      <TD class=title8>管理机构      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=ManageCom_ch>
        <input type=hidden name=ManageCom>
      </TD>
      <TD class=title8>销售渠道      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=SaleChnl_ch>
        <input type=hidden name=SaleChnl>
      </TD>
    </TR>
    <TR class=common8>
      <TD class=title8>中介机构      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=AgentCom_ch>
        <input type=hidden name=AgentCom>
      </TD>
      <TD class=title8>业务员代码      </TD>
      <TD class=input8>
        <input class="readonly" readonly name=AgentCode>
      </TD>
      <TD class=title8>业务员姓名      </TD>
      <TD class=input8>
        <Input class="readonly" readonly NAME=AgentCode_ch>
      </TD>
    </TR>
    <TR class=common>
      <TD class=title8>业务员组别      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=AgentGroup_ch>
        <input type=hidden name=AgentGroup>
      </TD>
      <TD class=title8>申请日期</TD>
      <TD class=input8>
        <Input class="readonly" readonly name=PolApplyDate>
      </TD>
      <TD class=title8>生效日期</TD>
      <TD class=input8>
        <Input class="readonly" readonly name=CValiDate>
      </TD>
    </tr>
    <tr class=common>
      <TD class=title8>特别约定</TD>
      <TD class=input8 COLSPAN="6">
        <Input class="readonly4" readonly name=Remark>
      </TD>
    </TR>
  </table>
  <div id=DivLCSendTrance STYLE="display:'none'">
    <table class=common>
      <tr CLASS="common">
        <td CLASS="title">上报标志</td>
        <td CLASS="input" COLSPAN="1">
          <input NAME="SendFlag" MAXLENGTH="10" CLASS="readonly" readonly>
        </td>
        <td CLASS="title">核保结论</td>
        <td CLASS="input" COLSPAN="1">
          <input NAME="SendUWFlag" CLASS="readonly" readonly>
        </td>
        <td CLASS="title">核保意见</td>
        <td CLASS="input" COLSPAN="1">
          <input NAME="SendUWIdea" CLASS="readonly" readonly>
        </td>
      </tr>
    </table>
  </div>
</Div>
  <!-- 团体查询条件 -->
  <!--
    TR  class= common>
    <TD  class= titles>
    团体投保单号码
    </TD>
    <TD  class= input>
    <Input class= common name=GrpProposalNo >
    <Input type= "hidden" class= common name=GrpMainProposalNo >
    <INPUT type= "hidden" name= "Operator" value= "">
    </TD>
    </TR
  -->
  <!-- 查询未过团体保单（列表） -->
<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);">
    </td>
    <td class=titleImg>团体投保资料    </td>
  </tr>
</table>
<Div id="divLCPol1" style="display: ''">
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanGrpAppntGrid">  </span>
      </td>
    </tr>
  </table>
</div>
<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol2);">
    </td>
    <td class=titleImg>团体投保单查询结果    </td>
  </tr>
</table>
<Div id="divLCPol2" style="display: ''">
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanGrpGrid">  </span>
      </td>
    </tr>
  </table>
</div>
  <!--
    table>
    <tr>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPolFee);">
    </td>
    <td class= titleImg>
    险种保费信息
    </td>
    </tr>
    </table
  -->
<Div id="divLCPolFee" style="display: 'none'">
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanGrpPolFeeGrid">  </span>
      </td>
    </tr>
  </table>
</div>
<br>
<hr>
</hr><INPUT VALUE="团体扫描件查询" Class="cssButton" TYPE=button onclick="ScanQuery2();">
<INPUT VALUE="团体自动核保信息" Class="cssButton" TYPE=button onclick="showGNewUWSub();">
<INPUT VALUE="团体保单明细" Class="cssButton" TYPE=button onclick="showGrpCont();">
<INPUT VALUE="团体既往信息" Class="cssButton" TYPE=button onclick="showHistoryContI();">
<INPUT VALUE="个人自动核保信息" Class="cssButton" TYPE=button onclick="GrpContQuery('<%=tGrpContNo%>');">
<INPUT VALUE="团体保单问题件处理" Class="cssButton" TYPE=button onclick="GrpQuestInput();">
<INPUT VALUE="团单契调信息录入" Class="cssButton" TYPE=button onclick="showRReport();">
<span  id= "GrpQuest13" style= "display: ''">
<INPUT VALUE="记事本" class="cssButton" TYPE=button onclick="showNotePade();">
<INPUT VALUE="再保审核信息" Class="cssButton" TYPE=button onclick="showRIExamInfo();">
<INPUT VALUE="总公司核保审核信息" Class="cssButton" TYPE=button onclick="showLRCContMsg();">
</span>
<hr>
</hr>
<span  id= "GrpQuest11" style= "display: ''">
<INPUT VALUE="发团体体检通知书" Class="cssButton" TYPE=button onclick="checkBody();">
<INPUT VALUE="发团单契调信息通知书" Class="cssButton" TYPE=button onclick="SendRReport();">
<INPUT VALUE="发团体核保调知书" Class="cssButton" TYPE=hidden onclick="SendNotice();">
<INPUT VALUE="发团体问题件通知书" Class="cssButton" TYPE=hidden onclick="SendIssueNotice();">
<INPUT VALUE="承保过程记录" Class="cssButton" TYPE=button onclick="showMarkPrice();">

 <!--input value="发核保通知书" class=cssButton type=button onclick="SendNotice();"-->
<hr>
</hr>
</span>
<span  id= "GrpQuest111" style= "display: ''">
<input value="承保计划变更" class=cssButton type=button onclick="showChangePlan();">
<input value="承保计划变更结论录入" class=cssButton type=button onclick="showChangeResultView();">
<input value="中介手续费调整" class=cssButton type=button onclick="showAgentDiv();">
</span>

<!--input value="费用率调整" class=cssButton type=button onclick="showFeeRate();"-->
<div id="divFeeRate" style="display: ''">
	  <table class=common align=center>
	    <table class=common>
	    <tr class=common>
	      <td text-align: left colSpan=1>
	        <span id="spanFeeRateGrid">  </span>
	      </td>
	    </tr>
	  </table>

  </table>
</div>
<div id="divAgent" style="display: 'none'">
  <table class=common align=center>
    <TD class=title8>中介手续费率（该费率值不能大过1）    </TD>
    <TD class=input8>
      <Input class="common" name=AgentcyRate>
    </TD>
    <input class="common" type=hidden name=PrtNoInput>
    <td class=common>
      <INPUT VALUE="确  定" class=cssButton TYPE=button onclick="AgentcySave();">
      <!-- <INPUT VALUE="取  消" class=cssButton TYPE=button onclick="showAgentDiv();"> -->
    </td>
  </table>
</div>

<table class=common border=0 width=100%>
  <tr>
    <td class=titleImg align=center>总公司核保审核结论：</td>
  </tr>
</table>
<table class=common border=0 width=100%>
  <TR class=common>
    <TD height="29" class=title>      总公司核保审核结论：
      <Input class=codeno name=RICUWState CodeData="0|^0|同意^1|不同意" ondblclick="showCodeListEx('RICUWState',[this,RICUWStateName],[0,1]);" onkeyup="showCodeListKeyEx('RICUWState',[this,RICUWStateName],[0,1]);">
      <input class=codename name=RICUWStateName readonly=true>
    </TD>
  </TR>
  <tr>  </tr>
  <TD class=title>总公司核保审核意见  </TD>
  <tr>  </tr>
  <TD class=input>
    <textarea name="RICUWIdea" cols="100%" rows="5" witdh=100% class="common">    </textarea>
  </TD>
</table>
<div id=divUWSave style="display:''">
  <INPUT VALUE="核保审核确认" Class="cssButton" TYPE=button onclick="gmanuchk();">
  <INPUT VALUE="返  回" Class="cssButton" TYPE=button onclick="GoBack();">
</div>
<input type="hidden" name="WorkFlowFlag" value="">
<input type="hidden" name="MissionID" value="">
<input type="hidden" name="SubMissionID" value="">
<input type="hidden" name="PrtNoHide" value="">
<input type="hidden" name="GrpProposalContNo" value="">
<INPUT type="hidden" class=Common name="YesOrNo" value="">
<INPUT type="hidden" class=Common name="GrpSendUpFlag" value="">
</div><div id="divChangeResult" style="display: 'none'">
  <table class=common align=center>
    <TD height="24" class=title>承保计划变更结论录入:    </TD>
    <tr>    </tr>
    <TD class=input>
      <textarea name="ChangeIdea" cols="100%" rows="5" witdh=100% class="common">      </textarea>
    </TD>
  </table>
  
  <INPUT VALUE="确  定" class=cssButton TYPE=button onclick="showChangeResult();">
  <INPUT VALUE="取  消" class=cssButton TYPE=button onclick="HideChangeResult();">
</div>
<div id=hidden style="display : 'none'">
  <!--
    <TD  class= title8>
    投保单客户号码
    </TD>
    <TD  class= input8>
  -->
  <Input class="readonly" readonly name=AppntNo type="hidden">
</TD>  <TD class=title8>VIP标记  </TD>
  <TD class=input8>
    <Input class="readonly" readonly name=VIPValue>
    <INPUT type="hidden" name="Operator" value="">
  </TD>
  <TD class=title8>黑名单标记  </TD>
  <TD class=input8>
    <Input class="readonly" readonly name=BlacklistFlag>
  </TD>
</div>
<span id="LongSqlDiv"  style= "display: ''">
</span>
    <INPUT type=hidden name="LoadFlag" value="">
		<INPUT type=hidden name="Resource" value="">
</form>
  <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
<body name="LongSqlDiv">
</body>
</html>
