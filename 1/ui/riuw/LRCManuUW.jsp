<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<%
//程序名称：LRCManuUWAll.jsp.
//程序功能：总公司核保审核
//创建日期：2011-11-14 
//创建人  ：Gaoyx
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<html>
<script>
	var tPrtNo = "<%=request.getParameter("PrtNo")%>";
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LRCManuUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRCManuUWInit.jsp"%>
  <title>总公司核保审核信息</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action=""> 
 
   <DIV id=DivLCContInfo STYLE="display:''"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 总公司核保审核意见：
    		</td>
    	</tr>  	
    </table>
    </Div>
    <Div  id= "divLCPol11" style= "display: ''" align = left>
     <table>
       <tr class=common>
      	  <td text-align: left colSpan=1 >
  					<span id="spanGrpHisUWGrid" >
  					</span> 
  			  </td>
  			</tr>  
    	</table>    
    </div>
    <Div  id= "divGrpHisUWPage" style= "display: ''" align = center>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">   
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">    
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">    
    </div>
        <table>
    	<tr>
    		 <td class=common>    		
    		 	<INPUT class=cssButton id="riskbutton" VALUE="返  回" TYPE=button onClick="window.close();">
    		</td>
    	</tr>  	
    </table>

    <table>
       	<span id="spanCode"  style="display: none; position:absolute; slategray" onclick="return ;	if(spanCode) showCodeList('bank',[ManageCom],null,null,null,null,1); spanCode=false;" onkeyup="return showCodeListKey('bank',[this],null,null,null,null,1);"></span>	
    	 <Input type=hidden name="ApplyType" >
    </table>
</body>
</html>
