<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolInput.jsp
//程序功能：
//创建日期：2002-08-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr="";
	String Content = "";
	String tAction = "";
	String tOperate = "";

	//输入参数
	VData tVData = new VData();
	LCGrpPolSchema tLCGrpPolSchema   = new LCGrpPolSchema();

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");


	tAction = request.getParameter( "fmAction" );
System.out.println("tAction:"+tAction);
	if( tAction.equals( "DELETE" ))
	{
	    tLCGrpPolSchema.setGrpPolNo(request.getParameter("GrpProposalNo"));
	}
	else
	{
            //集体信息
	    tLCGrpPolSchema.setContNo(request.getParameter("ContNo"));
	    tLCGrpPolSchema.setGrpProposalNo(request.getParameter("GrpProposalNo"));
	    tLCGrpPolSchema.setPrtNo(request.getParameter("PrtNo"));
	    tLCGrpPolSchema.setManageCom(request.getParameter("ManageCom"));
	    tLCGrpPolSchema.setSaleChnl(request.getParameter("SaleChnl"));
	    tLCGrpPolSchema.setAgentCom(request.getParameter("AgentCom"));
	    tLCGrpPolSchema.setAgentCode(request.getParameter("AgentCode"));
	    tLCGrpPolSchema.setAgentCode1(request.getParameter("AgentCode1"));

	    //投保单位资料
	    tLCGrpPolSchema.setGrpNo(request.getParameter("GrpNo"));
	    tLCGrpPolSchema.setGrpName(request.getParameter("GrpName"));	    
	    tLCGrpPolSchema.setGrpAddress(request.getParameter("GrpAddress"));
	    tLCGrpPolSchema.setGrpZipCode(request.getParameter("GrpZipCode"));
	    tLCGrpPolSchema.setGrpNature(request.getParameter("GrpNature"));
	    tLCGrpPolSchema.setBusinessType(request.getParameter("BusinessType"));
	    tLCGrpPolSchema.setPeoples(request.getParameter("Peoples"));
	    tLCGrpPolSchema.setRgtMoney(request.getParameter("RgtMoney"));
	    tLCGrpPolSchema.setAsset(request.getParameter("Asset"));
	    tLCGrpPolSchema.setNetProfitRate(request.getParameter("NetProfitRate"));
	    tLCGrpPolSchema.setMainBussiness(request.getParameter("MainBussiness"));
	    tLCGrpPolSchema.setCorporation(request.getParameter("Corporation"));
	    tLCGrpPolSchema.setComAera(request.getParameter("ComAera"));
	    //保险联系人一
	    tLCGrpPolSchema.setLinkMan1(request.getParameter("LinkMan1"));
	    tLCGrpPolSchema.setDepartment1(request.getParameter("Department1"));
	    tLCGrpPolSchema.setHeadShip1(request.getParameter("HeadShip1"));
	    tLCGrpPolSchema.setPhone1(request.getParameter("Phone1"));
	    tLCGrpPolSchema.setE_Mail1(request.getParameter("E_Mail1"));
	    tLCGrpPolSchema.setFax1(request.getParameter("Fax1"));
	    //保险联系人二
	    tLCGrpPolSchema.setLinkMan2(request.getParameter("LinkMan2"));
	    tLCGrpPolSchema.setDepartment2(request.getParameter("Department2"));
	    tLCGrpPolSchema.setHeadShip2(request.getParameter("HeadShip2"));
	    tLCGrpPolSchema.setPhone2(request.getParameter("Phone2"));
	    tLCGrpPolSchema.setE_Mail2(request.getParameter("E_Mail2"));
	    tLCGrpPolSchema.setFax2(request.getParameter("Fax2"));
	    tLCGrpPolSchema.setGetFlag(request.getParameter("GetFlag"));
	    tLCGrpPolSchema.setBankCode(request.getParameter("BankCode"));
	    tLCGrpPolSchema.setBankAccNo(request.getParameter("BankAccNo"));
	    tLCGrpPolSchema.setCurrency(request.getParameter("Currency"));
	    tLCGrpPolSchema.setEmployeeRate(request.getParameter("EmployeeRate"));
	    tLCGrpPolSchema.setFamilyRate(request.getParameter("FamilyRate"));
	    tLCGrpPolSchema.setPhone(request.getParameter("Phone"));
	    tLCGrpPolSchema.setFax(request.getParameter("Fax"));
	    tLCGrpPolSchema.setEMail(request.getParameter("EMail"));
	    //险种信息	    	    	
	    tLCGrpPolSchema.setRiskCode(request.getParameter("RiskCode"));
	    tLCGrpPolSchema.setCValiDate(request.getParameter("CValiDate"));
	    tLCGrpPolSchema.setPayIntv(request.getParameter("PayIntv"));
	    tLCGrpPolSchema.setPeakLine(request.getParameter("PeakLine"));
	    tLCGrpPolSchema.setGetLimit(request.getParameter("GetLimit"));
	    tLCGrpPolSchema.setGetRate(request.getParameter("GetRate"));
	    tLCGrpPolSchema.setMaxMedFee(request.getParameter("MaxMedFee"));
	    tLCGrpPolSchema.setBonusRate(request.getParameter("BonusRate"));
	} // end of if
System.out.println("end setSchema:");
	// 准备传输数据 VData
	tVData.add( tLCGrpPolSchema );
	tVData.add( tG );

	if( tAction.equals( "INSERT" )) tOperate = "INSERT||GROUPPOL";
	if( tAction.equals( "UPDATE" )) tOperate = "UPDATE||GROUPPOL";
	if( tAction.equals( "DELETE" )) tOperate = "DELETE||GROUPPOL";

	GroupPolUI tGroupPolUI = new GroupPolUI();
	if( tGroupPolUI.submitData( tVData, tOperate ) == false )
	{
		Content = " 保存失败，原因是: " + tGroupPolUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";

		tVData.clear();
		tVData = tGroupPolUI.getResult();

		// 显示
		// 保单信息
		LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema(); 
		mLCGrpPolSchema.setSchema(( LCGrpPolSchema )tVData.getObjectByObjectName( "LCGrpPolSchema", 0 ));
		%>
    	<script language="javascript">
    	 	parent.fraInterface.fm.all("ContNo").value = "<%=mLCGrpPolSchema.getContNo()%>";
    	 	parent.fraInterface.fm.all("GrpProposalNo").value = "<%=mLCGrpPolSchema.getGrpProposalNo()%>";
                parent.fraInterface.fm.all("GrpNo").value = "<%=mLCGrpPolSchema.getGrpNo()%>";     	
    	</script>
		<%		
	}
System.out.println("Content:"+Content);	

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

