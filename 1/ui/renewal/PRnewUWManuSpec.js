//程序名称：UWManuSpec.js
//程序功能：人工核保条件承保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var str = "";
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保

//提交，保存按钮对应操作
function submitForm(tPolNo,tPrtNo,tMissionID)
{
	if(fm.Remark.value == "" && fm.SpecRemark.value == "")
    {
  	alert("请录入续保特别约定或续保备注,再进行提交!");
  	return ;
    }
	if(fm.SubMissionID.value == "")
   {
  	alert("已录入核保通知书信息,但未打印,不容许录入新的核保通知书特约信息!");
  	initForm(tPolNo,tMissionID);
  	return;
  	}

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      
    showInfo.close();
    alert(content);
    parent.close();
  }
  else
  { 
	var showStr="操作成功";  	
  	showInfo.close();
  	alert(showStr);
  	parent.close();
  	
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
         

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function manuchkspecmain()
{
	fm.submit();
}

//查询已经录入特约内容
function QuerySpec(tPolNo)
{	
	// 书写SQL语句
	var strSQL = "";
	var i, j, m, n;	  
	strSQL = "select speccontent from LCSpec where 1=1 "
			 + " and polno = '"+tPolNo+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
  //判断是否查询成功
  if (turnPage.strQueryResult) {
  	//查询成功则拆分字符串，返回二维数组
   turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  
   fm.SpecRemark.value = turnPage.arrDataCacheSet[0][0];	  
  }

  //获取工作流子任务号
	var tMissionID = fm.MissionID.value;	
	var strsql = "select LWMission.SubMissionID from LWMission where 1=1"
				 + " and LWMission.MissionProp2 = '" + tPolNo + "'"
				 + " and LWMission.ActivityID = '0000000103'"
				 + " and LWMission.ProcessID = '0000000001'"
				 + " and LWMission.MissionID = '" + tMissionID + "'";				 
	turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);    
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    fm.SubMissionID.value = "";
    return ;
  }  
   turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
   fm.SubMissionID.value = turnPage.arrDataCacheSet[0][0];
  
  return ;	
}


//查询已经录入特约原因
function QuerySpecReason(tPolNo)
{	
	// 书写SQL语句
	var strSQL = "";
	var i, j, m, n;	
	strSQL = "select specreason from LCUWMaster where 1=1 "
			 + " and polno = '"+tPolNo+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return "";
  }  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  
  fm.SpecReason.value = turnPage.arrDataCacheSet[0][0];	  
  return true;	
}

//查询已经录入特约原因
function QueryRemark(tPolNo)
{	
	// 书写SQL语句
	var strSQL = "";
	var i, j, m, n;	
	strSQL = "select RemarkCont from LCRemark where 1=1 "
			 + " and polno = '"+tPolNo+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return "";
  }  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  
  fm.Remark.value = turnPage.arrDataCacheSet[0][0];	  
  return true;	
}

