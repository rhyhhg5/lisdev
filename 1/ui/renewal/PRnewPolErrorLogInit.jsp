<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>                 

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
    fm.all('RiskCode').value = '';
    fm.all('PrtNo').value = '';
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initPolGrid();
	initTraceGrid();
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="险种编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符
      iArray[2][9]="险种编码|code:RiskCode&NOTNULL";
      iArray[2][18]=250;
      iArray[2][19]= 0 ;
      
      
      iArray[3]=new Array();
      iArray[3][0]="日志日期";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0; 

      iArray[4]=new Array();
      iArray[4][0]="错误信息";         		//列名
      iArray[4][1]="300px";            		//列宽
      iArray[4][2]=400;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 5;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.canChk = 0;
      PolGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initTraceGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="类型";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="时间";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许    
      
      iArray[3]=new Array();
      iArray[3][0]="动作描述";         		//列名
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0; 

      iArray[4]=new Array();
      iArray[4][0]="提示信息";         		//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=400;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="管理机构";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=400;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="操作员";         		//列名
      iArray[6][1]="50px";            		//列宽
      iArray[6][2]=400;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      TraceGrid = new MulLineEnter( "fm2" , "TraceGrid" ); 
      //这些属性必须在loadMulLine前
      TraceGrid.mulLineCount = 5;   
      TraceGrid.displayTitle = 1;
      TraceGrid.locked = 1;
      TraceGrid.canSel = 1;
      TraceGrid.canChk = 0;
      TraceGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>