<%
//程序名称：GrpDueFeeBackOper.jsp
//程序功能：团单实收保费转出
//创建日期：2006-09-19
//创建人  ：QuLiqiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  String FlagStr = "";
  String Content = "";
  
  VData tVData =new VData();
  String number = request.getParameter("SubmitPayNo");
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  LJSPayBSchema  temp= new LJSPayBSchema();
  temp.setGetNoticeNo(number);
  tVData.add(0,temp);
  tVData.add(1,tG);    
  GrpRnewDueFeeBackUI tGrpDueFeeBackUI = new GrpRnewDueFeeBackUI();
  if(!tGrpDueFeeBackUI.submitData(tVData,""))
  {
		FlagStr = "Fail";
   	Content = tGrpDueFeeBackUI.mCErrors.getFirstError().toString();
  }
	else
	{
		FlagStr = "Succ";
   	Content = "实收保费转出成功。";
	}
%>
<html>
<script language="javascript">	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>