<%
//程序名称：PEdorAppConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  
  LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
  
  PEdorAppConfirmUI tPEdorAppConfirmUI   = new PEdorAppConfirmUI();
  
  GlobalInput tG = new GlobalInput();
   
  tG = (GlobalInput)session.getValue("GI");
  
 System.out.println("-----"+tG.Operator);
  //输出参数
  CErrors tError = null;
  VData tResult = new VData();
  //后面要执行的动作：添加，修改，删除
    
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String prtParams = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("transact : " + transact);
  
 
  //个人批改信息
 
 boolean tag = false;
 try
  {
        //打印保全交费项
        if (transact.equals("PRINT")) {
            VData tVData = new VData();   
            VData mResult = new VData();
            String EdorNo = request.getParameter("EdorNo");
            System.out.println("Edorno : " + EdorNo);
            tLPEdorMainSchema.setEdorNo(EdorNo);
            String PrintParams = request.getParameter("prtParams");
            System.out.println("prtParams :" + PrintParams);
            StrTool strTool = new StrTool();
            String GetNoticeNo = strTool.decodeStr(PrintParams,"|",1);
            String AppntNo = strTool.decodeStr(PrintParams,"|",4);
            String SumduepayMoney = strTool.decodeStr(PrintParams,"|",5);
            System.out.println("GetNoticeNo : " + GetNoticeNo);
            System.out.println("AppntNo :" + AppntNo);
            System.out.println("SumduepayMoney :" + SumduepayMoney);
        %>    
<html>
<script language="javascript">
	parent.fraInterface.PrtPay("<%=GetNoticeNo%>","<%=AppntNo%>","<%=SumduepayMoney%>");
</script>
</html>	 
        <%  
	    return;
/*
	    tVData.addElement(tLPEdorMainSchema);
            tVData.addElement(GetNoticeNo);
            tVData.addElement(AppntNo);
            tVData.addElement(SumduepayMoney);
            tVData.addElement(tG);
       	    EdorFeeF1PUI tEdorFeeF1PUI = new EdorFeeF1PUI();
                        
    	    if (!tEdorFeeF1PUI.submitData(tVData,"PRINT"))
    	        return;

	    mResult = tEdorFeeF1PUI.getResult();
	    XmlExport txmlExport = new XmlExport();
	    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	    if (txmlExport==null)
	    {
	        System.out.println("null");
	    }
	    session.putValue("PrintStream", txmlExport.getInputStream());
	    System.out.println("put session value");
	    response.sendRedirect("../f1print/GetF1Print.jsp");
  	    
            return;
*/            
        } else {
        
        
        //保全确认
        tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));       
  // 准备传输数据 VData
  
  	 VData tVData = new VData();   
	 //保存个人保单信息(保全)	
	 
  	tVData.addElement(tLPEdorMainSchema);
       
	tVData.addElement(tG);
	tag = tPEdorAppConfirmUI.submitData(tVData,transact);
	prtParams = tPEdorAppConfirmUI.getPrtParams();
	System.out.println("prtParams " + prtParams);
	
	System.out.println("--------transact"+transact);
      }
    } catch(Exception ex) {
		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
    }			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tResult = tPEdorAppConfirmUI.getResult();
    tError = tPEdorAppConfirmUI.mErrors;
    if (tag )
    {                          
      Content = " 保存成功";
      String tUWFlag = (String)tResult.get(0);
      String tEdorNO = (String)tResult.get(1);
      if(tUWFlag != null && tUWFlag.equals("9"))
        FlagStr = "Success";
      if(tUWFlag != null && tUWFlag.equals("5"))
        FlagStr = "Success2";
    }
    else                                                                           
    {
        if (  tError.needDealError())
    	    Content = " 保存失败，原因是:" + tError.getFirstError();
    	else 
    	    Content = " 保存失败 ";
    	
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
//        alert("hehe");
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=prtParams%>");
</script>
</html>

