  <%
//程序名称：GRnewFinFeeSubmitAll.jsp
//程序功能：
//创建日期：2018-08-09
//创建人  ：lzj
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.bl.*"%>  
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.xb.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>  
  
<%@page contentType="text/html;charset=GBK" %> 
<%
  
//集体保单表-放置集体保单号，交费日期，操作员，管理机构      
	LCGrpContSet    tLCGrpContSet  = new LCGrpContSet();  ;
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	TransferData tTransferData = new TransferData();	
          
         
  
// 输出参数
	CErrors tError = null;          
	String FlagStr = "";
	String Content = "";
 
	GlobalInput tGI = new GlobalInput(); //repair:
	tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
	System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxx"+request.getParameter("GetNoticeNo"));
	if(tGI==null){
		System.out.println("页面失效,请重新登陆");   
		FlagStr = "Fail";        
		Content = "页面失效,请重新登陆";  
	}else{
		String Operator  = tGI.Operator ;  //保存登陆管理员账号
		String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
	  
	  	//表单中的隐藏字段
		String GrpContNo=request.getParameter("SubmitGrpContNo"); //保单号码 
		String tGetNoticeNo = request.getParameter("GetNoticeNo") ; // 应收号 	   
		tTransferData.setNameAndValue("GetNoticeNo",tGetNoticeNo);
	    
		// 目前这个这个变量后台未做任何处理
		System.out.println(GrpContNo);  
		tLCGrpContSchema.setGrpContNo(GrpContNo);
		GRnewDueVerifyUI tGRnewDueVerifyUI = new GRnewDueVerifyUI();   
	  	try{
			VData tVData = new VData();
			tVData.add(tGI);
			tVData.addElement(tLCGrpContSchema);
			tVData.add(tTransferData);
			tGRnewDueVerifyUI.submitData(tVData,"VERIFY"); //将页面数据封装后提交到后台
			tError = tGRnewDueVerifyUI.mErrors;
	   
		   	if (tError.needDealError()){
				 Content = " 核销失败，原因是: " + tGRnewDueVerifyUI.mErrors.getFirstError();
				 FlagStr = "Fail";      	  
			}else{
				 Content = " 数据处理完毕";
				 FlagStr = "Succ";   	 
			} 
	  	}catch(Exception ex){
			Content = "核销失败，原因是:" + ex.toString();
			FlagStr = "Fail";
	  	}
	}//页面有效区  
	System.out.println("Content:"+Content);
%>                                              
<html>
<body>
<script language="javascript">
    if("<%=FlagStr%>"=="Succ")
      {
       parent.fraInterface.NormPayCollGrid.clearData("NormPayCollGrid");   
      }
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</body>
</html>

