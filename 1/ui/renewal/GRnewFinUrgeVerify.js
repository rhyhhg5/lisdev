  //               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var showInfo;

// 查询保单信息
//pmType 1:单张核销;2:批次核销

function easyQueryClick(pmType) {

  var strSql="";
  initNormPayCollGrid(); 

  if (pmType == "1")
  {
	  strSql = "select a.GrpContNo,b.GrpName,a.GetNoticeNo,sum(a.SumDuePayMoney), '',value((select sum(sumDuePayMoney) from LJSPayGrp where getNoticeNo = a.getNoticeNo and payType <> 'YEL'), 0),"
		  + " value((select sum(sumDuePayMoney) from LJSPayGrp where getNoticeNo = a.getNoticeNo and payType = 'YEL'), 0), "
		  +"(select max(EnterAccDate) from LJTempFee where TempFeeNo= a.GetNoticeNo and ConfFlag='0'),(select codename from ldcode where codetype='paymode' and code=b.PayMode),min(a.CurPayToDate),getUniteCode(a.AgentCode),c.Operator,'',min(a.LastPayToDate) " 
		  + " from LJSPayGrp a ,LCGrpCont  b,LJSPay c "
		  + " where a.GrpContNo='" + fm.all('GrpContNo').value + "' "
		  + " and b.PayIntv = 0 and b.AppFlag='1' and (b.StateFlag is null or b.StateFlag = '1') and c.othernotype='1' and c.OtherNo=a.GrpContNo"
		  + " and exists(select RiskCode from LMRiskPay where  RiskCode = '162801')"
		  + " and b.GrpContNo=a.GrpContNo and b.managecom like '"+manageCom+"%%' "
		  + " and ((select count(*) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
		  + " group by a.GrpContNo,b.GrpName,a.GetNoticeNo,a.AgentCode,b.GrpContNo,b.PayMode,c.Operator ";
	
  }else{
	  strSql = "select a.GrpContNo,b.GrpName,a.GetNoticeNo,c.SumDuePayMoney, '' ,value((select sum(sumDuePayMoney) from LJSPayGrp where getNoticeNo = a.getNoticeNo and payType <> 'YEL'), 0),"
		  + " value((select sum(sumDuePayMoney) from LJSPayGrp where getNoticeNo = a.getNoticeNo and payType = 'YEL'), 0), "
		  +"(select max(EnterAccDate) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0'),(select codename from ldcode where codetype='paymode' and code=b.PayMode),min(a.CurPayToDate),getUniteCode(a.AgentCode),c.Operator,'',min(a.LastPayToDate) " 
		  + " from LJSPayGrp a ,LCGrpCont  b,LJSPay c "
		  + " where a.LastPayToDate <='" + fm.all('EndDate').value  + "' "
		  + " and b.PayIntv = 0 and b.AppFlag='1' and (b.StateFlag is null or b.StateFlag = '1')"
		  + " and c.othernotype='1' and c.OtherNo=b.GrpContNo"
		  + " and exists(select RiskCode from LMRiskPay where RiskCode = '162801')"
		  + " and a.GrpContNo=b.GrpContNo and b.managecom like '"+manageCom+"%%' "
		  + " and ((select count(*) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
		  + " group by a.GrpContNo,b.GrpName,a.GetNoticeNo,a.AgentCode,b.GrpContNo,b.PayMode,c.SumDuePayMoney,c.Operator "; 
  }
  
   turnPage.queryModal(strSql, NormPayCollGrid);  
   setPeoples2Input();
}

//设置交费人数
function setPeoples2Input()
{
  for(var i = 0; i < NormPayCollGrid.mulLineCount; i++)
  {
	  sql = " select peoples2 from LCGrpCont where grpContNo = '" 
		  + NormPayCollGrid.getRowColDataByName(i, "grpContNo") + "' ";
	  rs = easyExecSql(sql);
	  if(rs)
	  {
		  NormPayCollGrid.setRowColDataByName(i, "peoples2Input", rs[0][0]);
	  }
  }
}
function afterQuery() {
	
	//pmType=1 个案核销；否则为批次核销
	pmType = fm.all('NormType').value ;
	initNormPayCollGrid(); 
	
	var strSql = ""; 
	if (pmType == "1"){
		strSql = "select  b.GrpContNo,b.GrpName,c.GetNoticeNo,c.SumActuPayMoney,'',value((select sum(sumDuePayMoney) from LJAPayGrp where PayNo=c.PayNo and payType <> 'YEL'), 0)," 
			+ "value((select sum(sumDuePayMoney) from LJAPayGrp where getNoticeNo = c.getNoticeNo and payType = 'YEL'), 0),(select max(EnterAccDate) from LJTempFee where TempFeeNo= c.GetNoticeNo and ConfFlag='1'),(select codename from ldcode where codetype='paymode' and code=b.PayMode),"
			+ "(select  min(CurPayToDate) from LJAPayGrp where PayNo=c.PayNo),getUniteCode(b.AgentCode),c.Operator,c.PayNo,(select  min(LastPayToDate) from LJAPayPerson where PayNo=c.PayNo)"
			+ " from LCGrpCont  b,LJAPay c  "
			+ " where b.GrpContNo='" + fm.all('GrpContNo').value + "' "
			+ " and b.PayIntv=0 and b.AppFlag='1' and (b.StateFlag is null or b.StateFlag = '1') and c.IncomeType='1' and c.IncomeNo=b.GrpContNo "
			+ " and exists (select GetNoticeNo from LJSPayB WHERE OtherNoType='1' and DealState='1' and getnoticeno = c.getnoticeno)"   
			+ " and c.PayNo in (select max(payno) from LJAPay where IncomeNo=b.GrpContNo)";	
	}else{
		strSql = "select  b.GrpContNo,b.GrpName,a.GetNoticeNo,sum(a.SumActuPayMoney),'',value((select sum(sumDuePayMoney) from LJAPayGrp where PayNo=a.PayNo and payType <> 'YEL'), 0)," 
			+ "value((select sum(sumDuePayMoney) from LJAPayGrp where getNoticeNo = a.getNoticeNo and payType = 'YEL'), 0),(select max(EnterAccDate) from LJTempFee where TempFeeNo= a.GetNoticeNo and ConfFlag='1'),(select codename from ldcode where codetype='paymode' and code=b.PayMode),"
			+ "(select  min(CurPayToDate) from LJAPayGrp where PayNo=a.PayNo),getUniteCode(b.AgentCode),a.Operator,a.PayNo,(select  min(LastPayToDate) from LJAPayPerson where PayNo=a.PayNo)"
			+ " from LJAPayGrp a ,LCGrpCont  b "
			+ " where a.LastPayToDate <='" + fm.all('EndDate').value  + "' "
			+ " and a.RiskCode = '162801'"
			+ " and b.PayIntv=0 and b.AppFlag='1' and (StateFlag is null or StateFlag = '1') and a.paycount>1"	       
			+ " and a.GrpContNo=b.GrpContNo "
			+ " and a.PayNo in (select max(payno) from LJAPay where IncomeNo=b.GrpContNo)"   
			+ " and exists (select GetNoticeNo from LJSPayB WHERE OtherNoType='1' and DealState='1' and getnoticeno = a.getnoticeno)"   
			+ " and a.makedate='" + CurrentTime+ "'"   
			+ " group by b.GrpContNo,b.GrpName,a.GetNoticeNo,b.PayMode,b.Dif,b.AgentCode,a.Operator,a.PayNo";  
	}
	
	turnPage.queryModal(strSql, NormPayCollGrid);  
	setPeoples2Input();
}

function fmSubmit()
{
    if(checkValue())
    {   
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
    fm.submit();    
    }
    	
}

function checkValue()
{
   if(!verifyInput())
     return false;
  
   return true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    afterQuery();
  }
  document.all.normConfirm.disabled=false; 
}
 

//提交所有选中数据，后台事务处理
function verifyChooseRecord()
{
    if(checkValue())
    {
      document.all.normConfirm.disabled=true; 
       if (fm.all('GrpContNo').value =='')
       {
		   if (fm.all('EndDate').value =='')
		   {
			   alert("请输入查询条件");
				return false;
		   }else
		   {
			    var showStr="本程序运行时间可能较长，请点击‘确认’按钮，关闭本窗口。本次作业运行情况，请到本窗口的‘当前线程查询’查询。作业已完成的，请到‘续收对帐单打印’菜单中查询。";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
				fmSaveAll.all('SubmitPayDate').value=fm.all('EndDate').value;
				fmSaveAll.submit();	
		   }
       }else
	   {
    	   var  selNo=  NormPayCollGrid.getSelNo();
    	
    		 if (selNo==null || selNo==0)
    		 {
    			 alert("请选择一条记录");
    		return false;
    		 }else{
    			
			var i = 0;
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
			fmSubmitAll.all('SubmitGrpContNo').value=fm.all('GrpContNo').value;		  
			fmSubmitAll.all('GetNoticeNo').value = NormPayCollGrid.getRowColDataByName(0,"getNoticeNo");
			fmSubmitAll.submit();
    		 }
		}		
    }
}

//直接提交所有数据，后台事务处理
function submitCurDataAll()
{
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
    fmSaveAll.all('SubmitGrpContNo').value=fm.all('GrpContNo').value;
    fmSaveAll.submit();	
 	
}

//批次核销查询数据
function queryMultRecord()
{
   if(checkValue())
   { 
	   if (fm.all('EndDate').value == '')
	   {
		   alert("请选择日期");
	   }else
	   {
		   fm.all('NormType').value = '2';
		   easyQueryClick("2");
	   }
   }
}

//单张核销查询数据
function querySingleRecord()
{
   if(checkValue())
   { 
	   if (fm.all('GrpContNo').value == '')
	   {
		   alert("请输入保单号");
		   return false;
	   }else
	   {
		   fm.all('NormType').value = '1'
           easyQueryClick("1");
	   }
   }
}

//打印清单
function printList()
{
	if (NormPayCollGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
    if(checkValue())
    { 
        if (fm.all('GrpContNo').value =='')
       {
		   if (fm.all('EndDate').value =='')
		   {
			   alert("请输入查询条件");
		   }else
		   {
				fmPrintAll.all('PrintPayDate').value=fm.all('EndDate').value;
				window.open("../operfee/GrpNormPayListPrint.jsp?PrintPayDate="+ fm.all('EndDate').value + "&PrintVerifyType=1");
		   }
       }else
	   {
			fmPrintAll.all('PrintGrpContNo').value=fm.all('GrpContNo').value;
			fmPrintAll.all('PrintVerifyType').value="2"; //个案核销
			window.open("../operfee/GrpNormPayListPrint.jsp?PrintGrpContNo="+ fm.all('GrpContNo').value + "&PrintVerifyType=2");
		}		
    }		
}

//打印对账单
function printPayComp(){
	if (NormPayCollGrid.mulLineCount == 0){
		alert("打印列表没有数据");
		return false;
	}
	var selNo = NormPayCollGrid.getSelNo();
	if(selNo == null || selNo == 0){
  	  alert("请选择一条记录");
  	  return false;
	}
	var cPayNo = NormPayCollGrid.getRowColDataByName( selNo - 1, "payNo"); 
//	window.open("../operfee/GRnewPayCompPrint.jsp?PayNo="+ cPayNo );
//	window.open("../uw/PDFPrintSave.jsp?Code=90&StandbyFlag2="+ cPayNo );
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=90&StandbyFlag2="+cPayNo;
	fm.submit();
}
//查询当前线程
function queryProject(){
	divProject.style.display = '';
	var tempCurrDate = CurrentTime;
	var strSql = "select a.SerialNo,a.Operator,a.MakeDate,a.MakeTime,case when a.DealState='1' then '正在处理中' when a.DealState='2' then '处理错误' when  a.DealState='3' then '处理已完成' end"
	 + " from LCUrgeVerifyLog a"
	 +" where a.MakeDate='"+tempCurrDate+"' and RiskFlag='1' and OperateType='2'"
	 + " order by a.ModifyDate,a.ModifyTime  desc  fetch first row only with ur";
	
  var arrReturn = new Array();
	arrReturn = easyExecSql(strSql);
	if (!(arrReturn == null ) ){
	    fm.all("Projecta").value = "批次号：" + arrReturn[0][0]+"；操作员："+ arrReturn[0][1]+"；操作时间："+arrReturn[0][2]+" " + arrReturn[0][3] +";操作状态："
	    + arrReturn[0][4];
	} 	
}

