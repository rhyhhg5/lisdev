var showInfo;
var turnPage = new turnPageClass();

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;     
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  }
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
  fm.all('check').disabled = false;
  easyQueryClick();
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	var strSQL = "select PrtNo,GrpProposalNo,GrpPolNo,RiskCode,GrpName,Peoples2,MakeDate,PaytoDate from LCGrpRnewLog where "
	       + " state ='4' "
		   + getWherePart( 'GrpPolNo','GrpPolNo')
		   + getWherePart( 'GrpProposalNo','GrpProposalNo')
		   + getWherePart( 'ManageCom', 'ManageCom','like')
		   + getWherePart( 'RiskCode' )
		   + getWherePart( 'PrtNo' )
		   + " order by prtno";

  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有待续保确认的团单！");
    return;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function autochk()
{
  var i = 0;
  var tSelNo = PolGrid.getSelNo();
  if( tSelNo　<= 0)
  {
  	alert("请先选择一条记录信息，再提交续保确认！");
  	return ;
  }
  fm.all('transact').value = "INSERT";
  fm.all('PrtNo').value = PolGrid.getRowColData(tSelNo-1,1);	
  if(fm.all('PrtNo').value == "")
  {
  	alert("请先选择一条记录信息，再提交续保确认！");
  	return ;
  }
  var showStr="正在传送数据,如果团单下被保人数很多，请您耐心等待,不要链接到其他页面!";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
  fm.all('check').disabled = true; 
  fm.submit(); //提交
}
