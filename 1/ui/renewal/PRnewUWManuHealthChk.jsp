<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWManuHealthChk.jsp
//程序功能：续保人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.xb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tGlobalInput = new GlobalInput();
  tGlobalInput=(GlobalInput)session.getValue("GI");	  
  if(tGlobalInput == null) {
	out.println("session has expired");
	return;
  }

  
  	// 投保单列表
	LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
	LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
	
	String tPolNo = request.getParameter("PolNo");
	String tInsureNo = request.getParameter("InsureNo");
	String tHospital = request.getParameter("Hospital");
	String tIfEmpty = request.getParameter("IfEmpty");
	String tEDate = request.getParameter("EDate");
	String tNote = request.getParameter("Note");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
	
	String tSerialNo[] = request.getParameterValues("HealthGridNo");
	String thealthcode[] = request.getParameterValues("HealthGrid1");
	String thealthname[] = request.getParameterValues("HealthGrid2");
	String tIfCheck[] = request.getParameterValues("HealthGrid3");
	//String tChk[] = request.getParameterValues("InpHealthGridChk");
	
	System.out.println("polno:"+tPolNo);
	System.out.println("hospital:"+tHospital);
	System.out.println("note:"+tNote);
	System.out.println("ifempty:"+tIfEmpty);
	System.out.println("insureno:"+tInsureNo);
	System.out.println("EDATE:"+tEDate);
	
	boolean flag = true;
	int ChkCount = 0;
	if(tSerialNo != null)
	{		
		ChkCount = tSerialNo.length;
	}
	System.out.println("count:"+ChkCount);
	if (ChkCount == 0 || tHospital.equals("") || tIfEmpty.equals(""))
	{
		Content = "体检资料信息录入不完整!";
		FlagStr = "Fail";
		flag = false;
		System.out.println("111");
	}
	else
	{
		System.out.println("222");
	    //体检资料一
		    tLCPENoticeSchema.setProposalNo(tPolNo);	    		
	    	tLCPENoticeSchema.setPEAddress(tHospital);
	    	tLCPENoticeSchema.setPEDate(tEDate);
	    	tLCPENoticeSchema.setPEBeforeCond(tIfEmpty);
	    	tLCPENoticeSchema.setRemark(tNote);
	    	tLCPENoticeSchema.setInsuredNo(tInsureNo);

	    	System.out.println("chkcount="+ChkCount);
	    	if (ChkCount > 0)
	    	{
	    		for (int i = 0; i < ChkCount; i++)
			{
				if (!thealthcode[i].equals("")&&!tIfCheck[i].equals(""))
				{
		  			LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();
		  			tLCPENoticeItemSchema.setProposalNo(tPolNo);
					tLCPENoticeItemSchema.setPEItemCode( thealthcode[i]);
					tLCPENoticeItemSchema.setPEItemName( thealthname[i]);
					tLCPENoticeItemSchema.setFreePE( tIfCheck[i]);	
					tLCPENoticeItemSchema.setInsuredNo(tInsureNo);    		    	    
			    	tLCPENoticeItemSet.add( tLCPENoticeItemSchema );
			        System.out.println("i:"+i);
	    		    System.out.println("healthcode:"+thealthcode[i]);	    	
			   		flag = true;
				}
			}
			    
		}
		else
		{
			Content = "传输数据失败!";
			flag = false;
		}
	}
	
	System.out.println("flag:"+flag);
  	if (flag == true)
  	{
		// 准备传输数据 VData
	   VData tVData = new VData();
	   TransferData tTransferData = new TransferData();
	   tTransferData.setNameAndValue("PolNo",tPolNo);
	   tTransferData.setNameAndValue("InsuredNo",tInsureNo );
	   tTransferData.setNameAndValue("MissionID",tMissionID );
	   tTransferData.setNameAndValue("SubMissionID",tSubMissionID );
       tTransferData.setNameAndValue("LCPENoticeSchema",tLCPENoticeSchema);
       tTransferData.setNameAndValue("LCPENoticeItemSet",tLCPENoticeItemSet);
       
	   tVData.add(tGlobalInput);
	   tVData.add(tTransferData);
		// 数据传输
		PRnewManuUWWorkFlowUI tPRnewManuUWWorkFlowUI   = new PRnewManuUWWorkFlowUI();
		if (tPRnewManuUWWorkFlowUI.submitData(tVData,"0000000101") == false)
		{
			int n = tPRnewManuUWWorkFlowUI.mErrors.getErrorCount();
			Content = " 续保体检资料录入失败，原因是:";
			Content = Content + tPRnewManuUWWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPRnewManuUWWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 续保体检资料录入成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 续保体检资料录入失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
		}
	} 
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
