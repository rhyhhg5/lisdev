<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	String tPolNo = "";
	tPolNo = request.getParameter("PolNo");
	session.putValue("PolNo",tPolNo);
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="PRnewUWManu.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../uw/UnderwriteInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./uw/UnderwriteCho.jsp">
    <!-- 保单查询条件 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            投保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=QProposalNo >
          </TD>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="code" name=QRiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);">
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=QManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            核保级别
          </TD>
          <TD  class= input>
            <Input class="code" name=UWGrade value= "1" CodeData= "0|^1|本级保单^2|下级保单" ondblClick="showCodeListEx('Grade',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('Grade',[this,''],[0,1]);">
          </TD>
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class="readonly" name=QAgentGroup>
          </TD>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class="code" name=QAgentCode ondblclick="return showCodeList('AgentCode',[this,QAgentGroup],[0,2]);" onkeyup="return showCodeListKey('AgentCode',[this,QAgentGroup],[0,2]);">
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT type= "hidden" name= "Operator" value= ""> 
    <!-- 保单查询结果部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" onclick="getPolGridCho();">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
    <!-- 核保 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>核保信息：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            投保单号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ProposalNo >
          </TD>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskVersion >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ManageCom >
          </TD>
          <TD  class= title>
            投保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntNo >
          </TD>
          <TD  class= title>
            投保人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            被保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredNo >
          </TD>
          <TD  class= title>
            被保人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredName >
          </TD>
          <TD  class= title>
            被保人性别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredSex >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            份数
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Mult >
          </TD>
          <TD  class= title>
            保费
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Prem >
          </TD>
          <TD  class= title>
            保额
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Amnt >
          </TD>
        </TR>
	</table>
	<table class= common border=0 width=100% >
          <INPUT VALUE="保单明细信息" TYPE=button onclick="showPolDetail();"> 
          <INPUT VALUE="既往投保信息" TYPE=button onclick="showApp();"> 
          <INPUT VALUE="以往核保记录" TYPE=button onclick="showOldUWSub();">
          <INPUT VALUE="最终核保信息" TYPE=button onclick="showNewUWSub();">
          <input value="体检资料" type=button onclick="showHealth();">
          <INPUT VALUE="问题件查询" TYPE=button onclick="QuestQuery();">
    </table>
    <!-- 核保结论 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>核保结论：</td>
		</tr>
	</table>
    
  <table  class= common align=center>
    <TR  class= common>
          <TD height="29"  class= title>
            核保结论

            <Input class="code" name=UWState ondblclick="return showCodeList('UWState',[this]);">
          </TD>
		  <tr></tr>
          <TD height="24"  class= title>
            核保意见
          </TD>
		  <tr></tr>
          
      <TD  class= input> <textarea name="UWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
	</table>
          
  <p>
    <INPUT VALUE="确定" TYPE=button onclick="manuchk();">
    <INPUT VALUE="取消" TYPE=button onclick="canceluw();">
  </p>
  <table  class= common align=center>
      	
	</table>          
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
