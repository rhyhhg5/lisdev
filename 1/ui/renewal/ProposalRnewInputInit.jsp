<%
//程序名称：ProposalInput.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox() { 
  try {  
                                   
	  // 保单信息部分
    fm.all('ProposalNo').value = '';
    fm.all('PrtNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('SaleChnl').value = '';
    fm.all('AgentCom').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('Handler').value = '';
    fm.all('AgentCode1').value = '';
 

  //  fm.all('ContNo').value = '';
    fm.all('GrpPolNo').value = '';
    fm.all('MainPolNo').value = '';
    fm.all('FirstPayDate').value = '';
    fm.all('Lang').value = '';
    fm.all('Currency').value = '';
    fm.all('DisputedFlag').value = '';
    fm.all('AgentPayFlag').value = '';
    fm.all('AgentGetFlag').value = '';
    fm.all('Remark').value = '';
      

  	// 个人投保人信息部分   
    fm.all( 'AppntCustomerNo' ).value          = "";
  	fm.all( 'AppntName' ).value                = "";
  	fm.all( 'AppntSex' ).value                 = "";
  	fm.all( 'AppntBirthday' ).value            = "";
  	fm.all( 'AppntAge' ).value                 = "";
  	fm.all( 'AppntIDType' ).value              = "";
  	fm.all( 'AppntIDNo' ).value                = "";
  	fm.all( 'AppntNativePlace' ).value         = "";
  	fm.all( 'AppntRgtAddress' ).value          = "";
  	fm.all( 'AppntMarriage' ).value            = "";
  	fm.all( 'AppntNationality' ).value         = "";
  	fm.all( 'AppntDegree' ).value              = "";
  	fm.all( 'AppntRelationToInsured' ).value   = "";
  	fm.all( 'AppntPostalAddress' ).value       = "";
  	fm.all( 'AppntZipCode' ).value             = "";
  	fm.all( 'AppntPhone' ).value               = "";
  	fm.all( 'AppntMobile' ).value              = "";
  	fm.all( 'AppntEMail' ).value               = "";
  	fm.all( 'AppntGrpName' ).value             = "";
  	fm.all( 'AppntGrpPhone' ).value            = "";
  	fm.all( 'AppntGrpAddress' ).value          = "";
  	fm.all( 'AppntGrpZipCode' ).value          = "";
  	fm.all( 'AppntWorkType' ).value            = "";
  	fm.all( 'AppntPluralityType' ).value       = "";
  	fm.all( 'AppntOccupationType' ).value      = "";
  	fm.all( 'AppntOccupationCode' ).value      = "";
  	fm.all( 'SmokeFlag' ).value                = "";

  	// 集体投保人信息部分    
  	fm.all('GrpNo').value = '';
    fm.all('GrpName').value = '';
    fm.all('LinkMan').value = '';
    fm.all('GrpRelation').value = '';
    fm.all('GrpPhone').value = '';
    fm.all('GrpFax').value = '';
    fm.all('GrpEMail').value = '';
    fm.all('GrpZipCode').value = '';
    fm.all('GrpAddress').value = '';

	  // 被保人信息部分  
  	fm.all( 'CustomerNo' ).value          = "";
  	fm.all( 'Name' ).value                = "";
  	fm.all( 'Sex' ).value                 = "";
  	fm.all( 'Birthday' ).value            = "";
  	fm.all( 'Age' ).value                 = "";
  	fm.all( 'IDType' ).value              = "";
  	fm.all( 'IDNo' ).value                = "";
  	fm.all( 'NativePlace' ).value         = "";
  	fm.all( 'RgtAddress' ).value          = "";
  	fm.all( 'Marriage' ).value            = "";
  	fm.all( 'Nationality' ).value         = "";
  	fm.all( 'Degree' ).value              = "";
  	fm.all( 'SmokeFlag' ).value           = "";
  	fm.all( 'PostalAddress' ).value       = "";
  	fm.all( 'ZipCode' ).value             = "";
  	fm.all( 'Phone' ).value               = "";
  	fm.all( 'Mobile' ).value              = "";
  	fm.all( 'EMail' ).value               = "";
  	fm.all( 'GrpName' ).value             = "";
  	fm.all( 'GrpPhone' ).value            = "";
  	fm.all( 'GrpAddress' ).value          = "";
  	fm.all( 'GrpZipCode' ).value          = "";	
  	fm.all( 'WorkType' ).value            = "";
  	fm.all( 'PluralityType' ).value       = "";
  	fm.all( 'OccupationType' ).value      = "";
  	fm.all( 'OccupationCode' ).value      = "";
  
    // 险种信息部分
    fm.all('RiskCode').value              = '';
    fm.all('RiskVersion').value           = '';
    fm.all('CValiDate').value             = '';
    fm.all('HealthCheckFlag').value       = '';
    fm.all('Years').value                 = '';
    fm.all('PayYears').value              = '';
    fm.all('PayIntv').value               = '';
    fm.all('OutPayFlag').value            = '';
    fm.all('PayLocation').value           = '';
    fm.all('BankCode').value              = '';
    fm.all('countName').value             = '';
    fm.all('BankAccNo').value             = '';
    fm.all('LiveGetMode').value           = '';
    fm.all('getTerm').value               = '';
    fm.all('getIntv').value               = '';
    fm.all('BonusGetMode').value          = '';
    fm.all('Mult').value                  = '';
    fm.all('Prem').value                  = '';
    fm.all('Amnt').value                  = '';
   
    fm.all('PayEndYear').value = '';
    fm.all('PayEndYearFlag').value = '';
    fm.all('GetYear').value = '';
    fm.all('GetYearFlag').value = '';
    fm.all('GetStartType').value = '';
    fm.all('InsuYear').value = '';
    fm.all('InsuYearFlag').value = '';
    fm.all('AutoPayFlag').value = '';
    fm.all('InterestDifFlag').value = '';
    fm.all('SubFlag').value = '';
  } catch(ex) {
    //alert("在ProposalInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ProposalInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

// 被保人信息列表的初始化
function initSubInsuredGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="连带被保人客户号";    	//列名
      iArray[1][1]="180px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][7]="showSubInsured";
      iArray[1][8]="['SubInsured']";        //是否使用自己编写的函数 

      iArray[2]=new Array();
      iArray[2][0]="姓名";         			//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="性别";         			//列名
      iArray[3][1]="140px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="出生日期";         		//列名
      iArray[4][1]="140px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="与被保人关系";         		//列名
      iArray[5][1]="160px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][4]="Relation";

      SubInsuredGrid = new MulLineEnter( "fm" , "SubInsuredGrid" ); 
      //这些属性必须在loadMulLine前
      SubInsuredGrid.mulLineCount = 1;   
      SubInsuredGrid.displayTitle = 1;
      //SubInsuredGrid.tableWidth = 200;
      SubInsuredGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 受益人信息列表的初始化
function initBnfGrid() {                               
  var iArray = new Array();

  try {
    iArray[0]=new Array();
    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";		//列宽
    iArray[0][2]=10;			//列最大值
    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许
  
    iArray[1]=new Array();
    iArray[1][0]="受益人类别"; 		//列名
    iArray[1][1]="80px";		//列宽
    iArray[1][2]=40;			//列最大值
    iArray[1][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="BnfType";
    iArray[1][9]="受益人类别|notnull&code:BnfType";
   
    iArray[2]=new Array();
    iArray[2][0]="受益人姓名"; 	//列名
    iArray[2][1]="80px";		//列宽
    iArray[2][2]=40;			//列最大值
    iArray[2][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[2][9]="受益人姓名|len<=20";//校验
   
    iArray[3]=new Array();
    iArray[3][0]="性别"; 	//列名
    iArray[3][1]="30px";		//列宽
    iArray[3][2]=100;			//列最大值
    iArray[3][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="sex";
    iArray[3][9]="性别|code:sex";//校验
  
    iArray[4]=new Array();
    iArray[4][0]="证件类型"; 		//列名
    iArray[4][1]="60px";		//列宽
    iArray[4][2]=80;			//列最大值
    iArray[4][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[4][4]="IDType";
    iArray[4][9]="证件类型|code:IDType";
  
    iArray[5]=new Array();
    iArray[5][0]="证件号码"; 		//列名
    iArray[5][1]="150px";		//列宽
    iArray[5][2]=80;			//列最大值
    iArray[5][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[5][9]="证件号码|len<=20";
  
    iArray[6]=new Array();
    iArray[6][0]="出生日期"; 		//列名
    iArray[6][1]="80px";		//列宽
    iArray[6][2]=80;			//列最大值
    iArray[6][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[6][9]="出生日期|date";
  
  
    iArray[7]=new Array();
    iArray[7][0]="与被保人关系"; 	//列名
    iArray[7][1]="90px";		//列宽
    iArray[7][2]=100;			//列最大值
    iArray[7][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[7][4]="Relation";
    iArray[7][9]="与被保人关系|code:Relation";
  
    iArray[8]=new Array();
    iArray[8][0]="受益比例"; 		//列名
    iArray[8][1]="60px";		//列宽
    iArray[8][2]=80;			//列最大值
    iArray[8][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[8][9]="受益比例|num&len<=10";
  
    iArray[9]=new Array();
    iArray[9][0]="受益顺序"; 		//列名
    iArray[9][1]="60px";		//列宽
    iArray[9][2]=80;			//列最大值
    iArray[9][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[9][4]="OccupationType";
    iArray[9][9]="受益顺序|code:OccupationType";
  
    iArray[10]=new Array();
    iArray[10][0]="联系地址"; 		//列名
    iArray[10][1]="400px";		//列宽
    iArray[10][2]=240;			//列最大值
    iArray[10][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[10][9]="通讯地址|len<=80";
  
    iArray[11]=new Array();
    iArray[11][0]="邮编"; 		//列名
    iArray[11][1]="60px";		//列宽
    iArray[11][2]=80;			//列最大值
    iArray[11][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[11][9]="邮编|zipcode";
  
    iArray[12]=new Array();
    iArray[12][0]="电话"; 		//列名
    iArray[12][1]="100px";		//列宽
    iArray[12][2]=80;			//列最大值
    iArray[12][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[12][9]="电话|len<=18";
  
    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" ); 
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 1; 
    BnfGrid.displayTitle = 1;
    BnfGrid.loadMulLine(iArray); 
  
    //这些操作必须在loadMulLine后面
    //BnfGrid.setRowColData(0,8,"1");
    //BnfGrid.setRowColData(0,9,"1");
  } catch(ex) {
    alert(ex);
  }
}

// 告知信息列表的初始化
function initImpartGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="告知版别";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="ImpartVer";
      iArray[1][9]="告知版别|len<=5";

      iArray[2]=new Array();
      iArray[2][0]="告知编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ImpartCode";
      iArray[2][5]="2|3";
      iArray[2][6]="0|1";
      iArray[2][9]="告知编码|len<=4";
      iArray[2][15]="ImpartVer";
      iArray[2][17]="1";

      iArray[3]=new Array();
      iArray[3][0]="告知内容";         		//列名
      iArray[3][1]="360px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="填写内容";         		//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][9]="填写内容|len<=200";

      iArray[5]=new Array();
      iArray[5][0]="告知客户类型";         		//列名
      iArray[5][1]="90px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][4]="CustomerType";
      iArray[5][9]="告知客户类型|len<=18";

      ImpartGrid = new MulLineEnter( "fm" , "ImpartGrid" ); 
      //这些属性必须在loadMulLine前
      ImpartGrid.mulLineCount = 1;   
      ImpartGrid.displayTitle = 1;
      //ImpartGrid.tableWidth   ="500px";
      ImpartGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ImpartGrid.setRowColData(1,1,"asdf");
    }
    catch(ex) {
      alert(ex);
    }
}

// 特别约定信息列表的初始化
function initSpecGrid() {                               
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="特约类型";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="特约编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="特约内容";         		//列名
      iArray[3][1]="540px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="特约内容|len<=255";
      
      SpecGrid = new MulLineEnter( "fm" , "SpecGrid" );
      //这些属性必须在loadMulLine前
      SpecGrid.mulLineCount = 1;   
      SpecGrid.displayTitle = 1;
      SpecGrid.loadMulLine(iArray);           
      //这些操作必须在loadMulLine后面
      //SpecGrid.setRowColData(1,1,"asdf");
      SpecGrid.setRowColData(0,1,"1");
      SpecGrid.setRowColData(0,2,"1");
      }
      catch(ex)
      {
        alert(ex);
      }
}

//责任列表
function initDutyGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="责任编码";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="责任名称";         			//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="保费";         			//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保额";         			//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="交费年期";         			//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="领取年期";         			//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
 
      DutyGrid = new MulLineEnter( "fm" , "DutyGrid" ); 
      //这些属性必须在loadMulLine前
      DutyGrid.mulLineCount = 0;   
      DutyGrid.displayTitle = 1;
      DutyGrid.canChk = 1;
      DutyGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //DutyGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


function emptyForm() {
	emptyFormElements();     //清空页面所有输入框，在COMMON。JS中实现
	
	//initInpBox();
	//initSelBox();    
	initSubInsuredGrid();
	initBnfGrid();
	initImpartGrid();
	initSpecGrid();
	initDutyGrid();
	
}

function initForm() {
	try	{  
		if (loadFlag == "2") {	//集体下个人投保单录入
			var tRiskCode = parent.VD.gVSwitch.getVar( "RiskCode" );
			getRiskInput( tRiskCode, loadFlag );
		}
		
		if (loadFlag == "3") {	//个人投保单明细查询
			var tPolNo = top.opener.parent.VD.gVSwitch.getVar( "PolNo" );
			queryPolDetail( tPolNo );
		}

		if (loadFlag == "4") {	//集体下个人投保单明细查询
			var tPolNo = top.opener.parent.VD.gVSwitch.getVar( "PolNo" );
			queryPolDetail( tPolNo );
		}

		if (loadFlag == "5") {	//个人投保单复核查询
			var tPolNo = top.opener.parent.VD.gVSwitch.getVar( "PolNo" );
			queryPolDetail( tPolNo );
		}
		
		if (loadFlag == "6") {	//个人投保单查询
			var tPolNo = top.opener.parent.VD.gVSwitch.getVar( "PolNo" );
			queryPolDetail( tPolNo );
		}
		
		//方便录入，MINIM增加
		fm.all('RiskCode').focus();
		
	} catch(ex) {
	}			
}

//取得集体投保单的信息
function getGrpPolInfo()
{

	try { fm.all( 'ContNo' ).value = parent.VD.gVSwitch.getVar( "ContNo" ); } catch(ex) { };
	try { fm.all( 'RiskCode' ).value = parent.VD.gVSwitch.getVar( "RiskCode" ); } catch(ex) { };
	try { fm.all( 'RiskVersion' ).value = parent.VD.gVSwitch.getVar( "RiskVersion" ); } catch(ex) { };
	try { fm.all( 'CValiDate' ).value = parent.VD.gVSwitch.getVar( "CValiDate" ); } catch(ex) { };

	try { fm.all( 'PrtNo' ).value = parent.VD.gVSwitch.getVar( "PrtNo" ); } catch(ex) { };
	try { fm.all( 'GrpPolNo' ).value = parent.VD.gVSwitch.getVar( "GrpProposalNo" ); } catch(ex) { };

	try { fm.all( 'ManageCom' ).value = parent.VD.gVSwitch.getVar( "ManageCom" ); } catch(ex) { };
	try { fm.all( 'SaleChnl' ).value = parent.VD.gVSwitch.getVar( "SaleChnl" ); } catch(ex) { };
	try { fm.all( 'AgentCom' ).value = parent.VD.gVSwitch.getVar( "AgentCom" ); } catch(ex) { };
	try { fm.all( 'AgentCode' ).value = parent.VD.gVSwitch.getVar( "AgentCode" ); } catch(ex) { };
	try { fm.all( 'AgentGroup' ).value = parent.VD.gVSwitch.getVar( "AgentGroup" ); } catch(ex) { };
	try { fm.all( 'AgentCode1' ).value = parent.VD.gVSwitch.getVar( "AgentCode1" ); } catch(ex) { };

	try { fm.all('AppGrpNo').value = parent.VD.gVSwitch.getVar('GrpNo'); } catch(ex) { };
	try { fm.all('Password').value = parent.VD.gVSwitch.getVar('Password'); } catch(ex) { };
	try { fm.all('AppGrpName').value = parent.VD.gVSwitch.getVar('GrpName'); } catch(ex) { };
	try { fm.all('AppGrpAddressCode').value = parent.VD.gVSwitch.getVar('GrpAddressCode'); } catch(ex) { };
	try { fm.all('AppGrpAddress').value = parent.VD.gVSwitch.getVar('GrpAddress'); } catch(ex) { };
	try { fm.all('AppGrpZipCode').value = parent.VD.gVSwitch.getVar('GrpZipCode'); } catch(ex) { };
	try { fm.all('BusinessType').value = parent.VD.gVSwitch.getVar('BusinessType'); } catch(ex) { };
	try { fm.all('GrpNature').value = parent.VD.gVSwitch.getVar('GrpNature'); } catch(ex) { };
	try { fm.all('Peoples').value = parent.VD.gVSwitch.getVar('Peoples'); } catch(ex) { };
	try { fm.all('RgtMoney').value = parent.VD.gVSwitch.getVar('RgtMoney'); } catch(ex) { };
	try { fm.all('Asset').value = parent.VD.gVSwitch.getVar('Asset'); } catch(ex) { };
	try { fm.all('NetProfitRate').value = parent.VD.gVSwitch.getVar('NetProfitRate'); } catch(ex) { };
	try { fm.all('MainBussiness').value = parent.VD.gVSwitch.getVar('MainBussiness'); } catch(ex) { };
	try { fm.all('Corporation').value = parent.VD.gVSwitch.getVar('Corporation'); } catch(ex) { };
	try { fm.all('ComAera').value = parent.VD.gVSwitch.getVar('ComAera'); } catch(ex) { };
	try { fm.all('LinkMan1').value = parent.VD.gVSwitch.getVar('LinkMan1'); } catch(ex) { };
	try { fm.all('Department1').value = parent.VD.gVSwitch.getVar('Department1'); } catch(ex) { };
	try { fm.all('HeadShip1').value = parent.VD.gVSwitch.getVar('HeadShip1'); } catch(ex) { };
	try { fm.all('Phone1').value = parent.VD.gVSwitch.getVar('Phone1'); } catch(ex) { };
	try { fm.all('E_Mail1').value = parent.VD.gVSwitch.getVar('E_Mail1'); } catch(ex) { };
	try { fm.all('Fax1').value = parent.VD.gVSwitch.getVar('Fax1'); } catch(ex) { };
	try { fm.all('LinkMan2').value = parent.VD.gVSwitch.getVar('LinkMan2'); } catch(ex) { };
	try { fm.all('Department2').value = parent.VD.gVSwitch.getVar('Department2'); } catch(ex) { };
	try { fm.all('HeadShip2').value = parent.VD.gVSwitch.getVar('HeadShip2'); } catch(ex) { };
	try { fm.all('Phone2').value = parent.VD.gVSwitch.getVar('Phone2'); } catch(ex) { };
	try { fm.all('E_Mail2').value = parent.VD.gVSwitch.getVar('E_Mail2'); } catch(ex) { };
	try { fm.all('Fax2').value = parent.VD.gVSwitch.getVar('Fax2'); } catch(ex) { };
	try { fm.all('Fax').value = parent.VD.gVSwitch.getVar('Fax'); } catch(ex) { };
	try { fm.all('Phone').value = parent.VD.gVSwitch.getVar('Phone'); } catch(ex) { };
	try { fm.all('GetFlag').value = parent.VD.gVSwitch.getVar('GetFlag'); } catch(ex) { };
	try { fm.all('Satrap').value = parent.VD.gVSwitch.getVar('Satrap'); } catch(ex) { };
	try { fm.all('EMail').value = parent.VD.gVSwitch.getVar('EMail'); } catch(ex) { };
	try { fm.all('FoundDate').value = parent.VD.gVSwitch.getVar('FoundDate'); } catch(ex) { };
	try { fm.all('BankAccNo').value = parent.VD.gVSwitch.getVar('BankAccNo'); } catch(ex) { };
	try { fm.all('BankCode').value = parent.VD.gVSwitch.getVar('BankCode'); } catch(ex) { };
	try { fm.all('GrpGroupNo').value = parent.VD.gVSwitch.getVar('GrpGroupNo'); } catch(ex) { };
	try { fm.all('State').value = parent.VD.gVSwitch.getVar('State'); } catch(ex) { };
	try { fm.all('BlacklistFlag').value = parent.VD.gVSwitch.getVar('BlacklistFlag'); } catch(ex) { };

}

</script>