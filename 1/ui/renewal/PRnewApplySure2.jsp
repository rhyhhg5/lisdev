<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("1"+tGI.Operator);
	System.out.println("2"+tGI.ManageCom);
	
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="PRnewApplySure.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PRnewApplySureInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm action="./PRnewApplySure.jsp">
    <!-- 保单查询条件 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            原个人保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=QProposalNo>
          </TD>
          <TD  class= title>
            续保通知书号
          </TD>
          <TD  class= input>
            <Input class=common name=RenewalNo>
          </TD>
        </TR>
    </table>

    <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
    <INPUT type= "hidden" name= "Operator" value= ""> 
        <!-- 保单查询结果部分（列表） -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>核保信息：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            原保单号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ProposalNo >
          </TD>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskVersion >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ManageCom >
          </TD>
          <TD  class= title>
            投保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntNo >
          </TD>
          <TD  class= title>
            投保人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            被保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredNo >
          </TD>
          <TD  class= title>
            被保人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredName >
          </TD>
          <TD  class= title>
            被保人性别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredSex >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            份数
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Mult >
          </TD>
          <TD  class= title>
            保费
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Prem >
          </TD>
          <TD  class= title>
            保额
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Amnt >
          </TD>
        </TR>
    </table>
    <INPUT VALUE="保单明细" TYPE=button onclick="queryDetailClick();">
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            是否用原保单号码
          </TD>
          <TD  class= input>
            <Input class="code" name=IfNew CodeData = "0|^0|用原保单号^ 1|用新保单号" ondblclick= "showCodeListEx('IfNew',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('IfNew',[this,''],[0,1]);">
          </TD>
        </TR>
    </table>
    
    <INPUT VALUE="确认申请" TYPE=button onclick="sure();"> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
