<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWAutoChk.jsp
//程序功能：个人续保自动核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.xbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	LCPolSet tLCPolSet = new LCPolSet();

	String tProposalNo[] = request.getParameterValues("PolGrid1");
	String tChk[] = request.getParameterValues("InpPolGridChk");
	boolean flag = false;
	int proposalCount = tProposalNo.length;
	int ProposalCount = 10;	

	for (int i = 0; i < proposalCount; i++)
	{
		if (!tProposalNo.equals("") && tChk[i].equals("1"))
		{
			System.out.println("ProposalNo:"+i+":"+tProposalNo[i]);
	  		LCPolSchema tLCPolSchema = new LCPolSchema();
	
		    tLCPolSchema.setPolNo( tProposalNo[i] );
		    tLCPolSet.add( tLCPolSchema );
		    flag = true;   
		}
	}

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCPolSet );
		tVData.add( tG );
		
		// 数据传输
		PRnewUWAutoChkUI tPRnewUWAutoChkUI   = new PRnewUWAutoChkUI();
		if (tPRnewUWAutoChkUI.submitData(tVData,"INSERT") == false)
		{
			int n = tPRnewUWAutoChkUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tPRnewUWAutoChkUI.mErrors.getError(i).errorMessage);
			Content = " 自动核保失败，原因是: " + tPRnewUWAutoChkUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPRnewUWAutoChkUI.mErrors;
		    //tErrors = tPRnewUWAutoChkUI.mErrors;
		    Content = " 自动核保成功! ";
		    if (!tError.needDealError())
		    {                          
		    	//Content = "自动核保成功!";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{    			      
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
			}

		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	//Content = " 自动核保成功! ";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
			}
		    	FlagStr = "Fail";
		    }
		}
	}
	else
	{
		Content = "请选择保单！";
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	<%
	//parent.fraInterface.fm.submit();
	%>
</script>
</html>
