//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var cflag = "1";  //问题件操作位置 1.核保

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  //alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
           
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function applyBat()
{
  fm.all('Operator').value="AUTO";
  submitForm();
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	var strSQL = "";
	strSQL = "select PrtNo,RiskCode,makedate,ErrInfo from lcrnewerrlog where 1=1 "
				 + getWherePart( 'makedate','StartDate' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'PrtNo' )
				 + getWherePart( 'ProType' )
				 + " order by makedate desc";
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //alert(turnPage.strQueryResult);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有续保错误日志信息！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function TraceQuery()
{
	var strSQL = "select SubType,MakeDate,OperDesc,Msg,ManageCom,Operator from LCOperTrace where 1=1 ";
	if(trim(fm2.all('BeginDate').value) != "")
	{
		strSQL = strSQL + "and MakeDate >= '" + trim(fm2.all('BeginDate').value)+"' ";
	}
	if(trim(fm2.all('EndDate').value) != "")
	{
		strSQL = strSQL + "and MakeDate <= '"+trim(fm2.all('EndDate').value)+"' ";
	}
	
	if(trim(fm2.all('ManageCom').value) != "")
	{
		strSQL = strSQL + "and ManageCom like '"+ trim(fm2.all('ManageCom').value) +"%%' ";
	}
	
	if(trim(fm2.all('operate').value) != "")
	{
		strSQL = strSQL + "and  Operator='"+trim(fm2.all('operate').value)+"' ";
	}
	
	
	strSQL = strSQL + " order by MakeDate desc";
	//alert(strSQL);
	  turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //alert(turnPage.strQueryResult);
  //判断是否查询成功
  if (!turnPage2.strQueryResult) {
    alert("没有错误日志信息！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage2.pageDisplayGrid = TraceGrid;    
          
  //保存SQL语句
  turnPage2.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage2.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  displayMultiline(arrDataSet, turnPage2.pageDisplayGrid);
  return true;
}