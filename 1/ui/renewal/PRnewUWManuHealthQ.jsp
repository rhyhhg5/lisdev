<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWManuHealthQ.jsp
//程序功能：保全人工核保体检资料查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./PRnewUWManuHealthQ.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>续保体检资料录入</title>
  <%@include file="PRnewUWManuhealthQInit.jsp"%>
</head>
<body  onload="initForm('<%=tPolNo%>');" >
  <form method=post name=fm target="fraSubmit" action= "./PRnewUWManuHealthQChk.jsp">
    <!-- 非列表 -->
    <table>
    	<TR  class= common>
          <TD  class= title>投保单号码 </TD>
          <TD  class= input>  <Input class="readonly" name=PolNo > </TD>
           <INPUT  type= "hidden" class= Common name= EdorNo value= "">
          <TD  class= title>   是否打印 </TD>
          <TD  class= input> <Input class="readonly" name=PrintFlag > </TD>
        </TR>
        <TR  class= common>
          <TD  class= title> 被保险人 </TD>
          <TD  class= input>  <Input class=code name=InsureNo ondblClick="showCodeListEx('InsureNo',[this,InsuredNoHide],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('InsureNo',[this,InsuredNoHide],[0,1],null,null,null,1);" onFocus= "easyQueryClickSingle();easyQueryClick();"></TD>
          <TD  class= title> 体检日期  </TD>
          <TD  class= input>  <Input class="readonly" name=EDate>  </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>  体检医院 </TD>
          <TD  class= input>  <Input class="readonly" name=Hospital> </TD>
          <TD  class= title>  是否空腹  </TD>
          <TD  class= input> <Input class="readonly" name=IfEmpty></TD>
        </TR>
    </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);">
    		</td>
    		<td class= titleImg>
    			 体检项目录入
    		</td>
    	</tr>	
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanHealthGrid">
  				</span> 
  		  	</td>
  		</tr>
    	   </table>
      </div>
      <table  class= common>
  		<tr>
    		<td class= title>其他体检信息	</td>
    		<td class = input>	<input class="readonly" name= Note>	</td>
    	</tr>
    	</table>      
        <table>
    		<TR  class= common>
          		<TD  class= title>体检结论录入:</TD>
           		<tr></tr>          
		        <TD  class= input> <textarea name="Content" cols="100%" rows="20" witdh=100% class="common"></textarea></TD>
         	</TR>
        </table>
        <INPUT type= "button" class= common name= "sure" value="体检结论录入确认" onclick="submitForm()">	
        <INPUT type= "hidden" name= "InsuredNoHide" value= "">	
    <!--读取信息-->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>