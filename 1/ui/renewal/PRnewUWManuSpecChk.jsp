<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWManuSpecChk.jsp
//程序功能：续保人工核保特约承保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.xb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean flag = true;
  GlobalInput tG = new GlobalInput();  
  tG=(GlobalInput)session.getValue("GI");  
  if(tG == null) {
		out.println("session has expired");
		return;
   } 
    //接收信息
  	LCSpecSchema tLCSpecSchema = new LCSpecSchema();
  	TransferData tTransferData = new TransferData();
    String tPolNo = request.getParameter("PolNo");
	String tSpecRemark = request.getParameter("SpecRemark");
	String tRemark = request.getParameter("Remark");
	String tSpecReason = request.getParameter("SpecReason");
	String tPrtNo = request.getParameter("PrtNo");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
	
	System.out.println("PrtNo:"+tPrtNo);
	System.out.println("polno:"+tPolNo);
	System.out.println("remark:"+tRemark);
	if (tPolNo == "" || (tRemark == "" && tSpecRemark== "" ) )
	{
		Content = "请录入续保特别约定信息或续保备注信息!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{     
	      if(tPolNo != null && tPrtNo != null && tMissionID != null )
	      {
		   //准备特约信息
		   	tLCSpecSchema.setPolNo(tPolNo); 
		   	tLCSpecSchema.setPolType("1");
		   	tLCSpecSchema.setSpecContent(tSpecRemark);
		   	tLCSpecSchema.setSpecType("1");
		   	tLCSpecSchema.setSpecCode("1");
		   	
            tTransferData.setNameAndValue("PolNo",tPolNo);
	        tTransferData.setNameAndValue("PrtNo",tPrtNo) ;
	        tTransferData.setNameAndValue("MissionID",tMissionID);
	        tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
	        tTransferData.setNameAndValue("Remark",tRemark);	
	        tTransferData.setNameAndValue("SpecReason",tSpecReason);
	        tTransferData.setNameAndValue("LCSpecSchema",tLCSpecSchema);	    	
		   }// End of if
		  else
		  {
			Content = "传输数据失败!";
			flag = false;
		  }
	}

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
		PRnewManuUWWorkFlowUI tPRnewManuUWWorkFlowUI   = new PRnewManuUWWorkFlowUI();
		if (!tPRnewManuUWWorkFlowUI.submitData(tVData,"0000000103"))//执行保全核保特约工作流节点0000000003
		  {
			int n = tPRnewManuUWWorkFlowUI.mErrors.getErrorCount();
			Content = " 核保特约失败，原因是: " + tPRnewManuUWWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPRnewManuUWWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 续保核保特约成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
