<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UnderwriteCho.jsp
//程序功能：
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.xb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%

  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
   
  LCGrpRnewPersonChangeParmSet tLCGrpRnewPersonChangeParmSet=new LCGrpRnewPersonChangeParmSet();   
  LCGrpRnewPersonChangeParmSchema tLCGrpRnewPersonChangeParmSchema=new LCGrpRnewPersonChangeParmSchema();

  String tGrpProposalNo= request.getParameter("GrpProposalNo");
  String tOperate= request.getParameter("Operate");
  String tGrpPolNo= request.getParameter("GrpPolNo2");
  String tPremFlag= request.getParameter("PremFlag");
  String tPremNum= request.getParameter("PremNum");
  String tAmntFlag= request.getParameter("AmntFlag");
  String tAmntNum= request.getParameter("AmntNum");
  String tManageFeeFlag= request.getParameter("ManageFeeFlag");
  String tManageFeeNum= request.getParameter("ManageFeeNum");      
  String tPolNo= request.getParameter("PolNo");

  String tPolNoChk[] = request.getParameterValues("InpPolGrid1Chk");   
  String tPolNoArr[] = request.getParameterValues("PolGrid11"); 

  //则缺省的个人统一变动
  if(tPolNo.equals("00000000000000000000"))
  {
	    	tLCGrpRnewPersonChangeParmSchema=new LCGrpRnewPersonChangeParmSchema();
    		tLCGrpRnewPersonChangeParmSchema.setGrpProposalNo(tGrpProposalNo);
  			tLCGrpRnewPersonChangeParmSchema.setGrpPolNo(tGrpPolNo);
  			tLCGrpRnewPersonChangeParmSchema.setPolNo(tPolNo);  
  			tLCGrpRnewPersonChangeParmSchema.setPremFlag(tPremFlag);  
  			tLCGrpRnewPersonChangeParmSchema.setPremNum(tPremNum);  
  			tLCGrpRnewPersonChangeParmSchema.setAmntFlag(tAmntFlag);  
  			tLCGrpRnewPersonChangeParmSchema.setAmntNum(tAmntNum);  
  			tLCGrpRnewPersonChangeParmSchema.setManageFeeFlag(tManageFeeFlag);  
  			tLCGrpRnewPersonChangeParmSchema.setManageFeeNum(tManageFeeNum);  
  			tLCGrpRnewPersonChangeParmSchema.setModifyDate(PubFun.getCurrentDate()); 
   			tLCGrpRnewPersonChangeParmSet.add(tLCGrpRnewPersonChangeParmSchema);
  }
  else
  {
      for(int index=0;index<tPolNoChk.length;index++)
      {
   		if(tPolNoChk[index].equals("1"))
   		{
    		tLCGrpRnewPersonChangeParmSchema=new LCGrpRnewPersonChangeParmSchema();
    		tLCGrpRnewPersonChangeParmSchema.setGrpProposalNo(tGrpProposalNo);
  			tLCGrpRnewPersonChangeParmSchema.setGrpPolNo(tGrpPolNo);
  			tLCGrpRnewPersonChangeParmSchema.setPolNo(tPolNoArr[index]);  
  			tLCGrpRnewPersonChangeParmSchema.setPremFlag(tPremFlag);  
  			tLCGrpRnewPersonChangeParmSchema.setPremNum(tPremNum);  
  			tLCGrpRnewPersonChangeParmSchema.setAmntFlag(tAmntFlag);  
  			tLCGrpRnewPersonChangeParmSchema.setAmntNum(tAmntNum);  
  			tLCGrpRnewPersonChangeParmSchema.setManageFeeFlag(tManageFeeFlag);  
  			tLCGrpRnewPersonChangeParmSchema.setManageFeeNum(tManageFeeNum);  
  			tLCGrpRnewPersonChangeParmSchema.setModifyDate(PubFun.getCurrentDate()); 
   			tLCGrpRnewPersonChangeParmSet.add(tLCGrpRnewPersonChangeParmSchema);
   		}         
      }
   }
        
			VData tVData = new VData();
			tVData.add(tLCGrpRnewPersonChangeParmSet);
			tVData.add(tGI);			

			// 数据传输
  			GRnewApplyBL tGRnewApplyBL   = new GRnewApplyBL();
			if (!tGRnewApplyBL.submitDataParm(tVData,tOperate))
			{
      				Content = " 提交失败，原因是: " + tGRnewApplyBL.mErrors.getError(0).errorMessage;
      				FlagStr = "Fail";
			}
			else
			{
				    Content = " 提交成功!";
      				FlagStr = "Succ";
			}  
			 
%>  
<HTML>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</HTML> 