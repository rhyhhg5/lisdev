<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('GrpProposalNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('RiskCode').value = '';
    fm.all('PrtNo').value = '';
  }
  catch(ex)
  {
    alert("GRnewAppCancelInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    

function initForm()
{
  try
  {
    initInpBox();
	initPolGrid();
  }
  catch(re)
  {
    alert("GRnewAppCancelInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单印刷号";         		//列名
      iArray[1][1]="92px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="新投保单号";         		//列名
      iArray[2][1]="134px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="原投保单号";         		//列名
      iArray[3][1]="134px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="险种编码";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=2; 
      iArray[4][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      iArray[4][9]="险种编码|code:RiskCode&NOTNULL";
      iArray[4][18]=100;
      iArray[4][19]= 0 ;

      iArray[5]=new Array();
      iArray[5][0]="投保人名称";         		//列名
      iArray[5][1]="180px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="总人数";         		//列名
      iArray[6][1]="40px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="团单状态";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][10]="state";              	        //是否引用代码:null||""为不引用
      iArray[7][11]="0|^0|续保申请|^1|待自核|^2|待人工核保|^3|待催收|^4|待续保确认|^5|续保确认成功|^6|原保单到期并已转移备份|";
      iArray[7][18]=60;
      iArray[7][19]= 0 ;

      iArray[8]=new Array();
      iArray[8][0]="该状态的个单数";         		//列名
      iArray[8][1]="85px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 10;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.canChk = 0;
      PolGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>