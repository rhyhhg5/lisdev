<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
	String PrtSeq=request.getParameter("PrtSeq");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
	String tPrtNo	 = request.getParameter("PrtNo");
	String tPolNo	 = request.getParameter("PolNo");
		
	GlobalInput tG = (GlobalInput)session.getAttribute("GI");
    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
    tLOPRTManagerSchema.setPrtSeq(request.getParameter("PrtSeq"));
    CError cError = new CError( );

  //后面要执行的动作：添加，修改，删除
  VData tVData = new VData();
  VData mResult = new VData();
    
  tVData.addElement(tG);
  tVData.addElement(tLOPRTManagerSchema);
	String strErrMsg = "";
	boolean Flag=true;
		
  PRnewMeetF1PUI tPRnewMeetF1PUI = new PRnewMeetF1PUI();
	if(!tPRnewMeetF1PUI.submitData(tVData,"CONFIRM")) {
		if( tPRnewMeetF1PUI.mErrors.needDealError() ) {
		Flag=false;
			strErrMsg = tPRnewMeetF1PUI.mErrors.getFirstError();
		} else {
			strErrMsg = "tPRnewMeetF1PUI发生错误，但是没有提供详细的出错信息";
		}
%>
		<script language="javascript">
			alert('<%= strErrMsg %>');
			window.opener = null;
			window.close();
		</script>
<%
		return;
  }

  mResult = tPRnewMeetF1PUI.getResult();
  
	XmlExport txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	
	if (txmlExport==null) {
	  System.out.println("null");
	}
	
	session.putValue("PrintNo",PrtSeq );
	session.putValue("MissionID",tMissionID );
	session.putValue("SubMissionID",tSubMissionID );
	session.putValue("Code","44" );	//面见通知书类别
	session.putValue("PrtNo",tPrtNo );
	session.putValue("PolNo",tPolNo );		
	session.setAttribute("PrintStream", txmlExport.getInputStream());
	System.out.println("put session value");
	response.sendRedirect("./PRnewGetF1Print.jsp");
%>