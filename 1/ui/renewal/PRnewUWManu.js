//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  //alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//既往投保信息
function showApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  cInsureNo = fm.InsuredNo.value;
  
  if (cProposalNo != "")
  {
  	//showModalDialog("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  	window.open("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  	
  }
}           

//以往核保记录
function showOldUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  //showModalDialog("./UWSubMain.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  if (cProposalNo != "")
  {
  	window.open("./UWSubMain.jsp?ProposalNo1="+cProposalNo,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  	
  }
}

//当前核保记录
function showNewUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if (cProposalNo != "")
  {
  	window.open("./UWErrMain.jsp?ProposalNo1="+cProposalNo,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  
  }
}                      

//保单明细信息
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if (cProposalNo != "")
  {
  	//window.open("../app/ProposalDisplay.jsp?PolNo="+cProposalNo,"window1");
  	mSwitch.deleteVar( "PolNo" );
  	mSwitch.addVar( "PolNo", "", cProposalNo );
  	mSwitch.updateVar("PolNo", "", cProposalNo);
		
  	//window.open("../app/ProposalMain.jsp?LoadFlag=3");]
  	var prtNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
  	window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+prtNo);
  	//showInfo.close();
  }
  else
  {  	
  	alert("请先选择保单!");	
  }

}           

//体检资料
function showHealth()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if (cProposalNo != "")
  {
  	window.open("./UWManuHealthMain.jsp?ProposalNo1="+cProposalNo,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	k++;
	var uwgrade = fm.UWGrade.value;
	var mOperate = fm.Operator.value;
	var strSQL = "";
	//alert(uwgrade);
	if (uwgrade == "1")
	{
		strSQL = "select LCPol.ProposalNo,LCPol.PrtNo,LCPol.RiskCode,LCPol.RiskVersion,LCPol.AppntName,LCPol.InsuredName from LCPol,LCUWMaster where "+k+"="+k				 	
				 + " and LCPol.AppFlag='2' "
				 + " and LCPol.ApproveFlag = '9' "
				 + " and LCPol.UWFlag in ('2','5','6') "         //自动核保待人工核保
				 + " and LCPol.grppolno = '00000000000000000000'"
				 + " and LCPol.contno = '00000000000000000000'"
				 + getWherePart( 'LCPol.ProposalNo','QProposalNo' )
				 + getWherePart( 'LCPol.ManageCom','QManageCom' )
				 + getWherePart( 'LCPol.AgentCode','QAgentCode' )
				 + getWherePart( 'LCPol.AgentGroup','QAgentGroup' )
				 + getWherePart( 'LCPol.RiskCode','QRiskCode' )
				 //+ getWherePart( 'LCPol.RiskVersion','QRiskVersion' )
				 + " and LCPol.ProposalNo = LCUWMaster.ProposalNo "
				 + " and LCUWMaster.appgrade = (select UWPopedom from LDUser where usercode = '"+mOperate+"')"
				 + " and LCPol.ManageCom like '" + manageCom + "%%'";
	}
	else
	{
		strSQL = "select LCPol.ProposalNo,LCPol.PrtNo,LCPol.RiskCode,LCPol.RiskVersion,LCPol.AppntName,LCPol.InsuredName from LCPol,LCUWMaster where "+k+"="+k
				 + " and LCPol.AppFlag='2' "
				 + " and LCPol.ApproveFlag = '9' "
				 + " and LCPol.UWFlag in ('2','5','6') "         //自动核保待人工核保
				 + " and LCPol.grppolno = '00000000000000000000'"
				 + " and LCPol.contno = '00000000000000000000'"
				 + getWherePart( 'LCPol.ProposalNo','QProposalNo' )
				 + getWherePart( 'LCPol.ManageCom','QManageCom' )
				 + getWherePart( 'LCPol.AgentCode','QAgentCode' )
				 + getWherePart( 'LCPol.AgentGroup','QAgentGroup' )
				 + getWherePart( 'LCPol.RiskCode','QRiskCode' )
				 //+ getWherePart( 'LCPol.RiskVersion','QRiskVersion' )
				 + " and LCPol.ProposalNo = LCUWMaster.ProposalNo "
				 + " and LCUWMaster.appgrade < (select UWPopedom from LDUser where usercode = '"+mOperate+"')"
				 + " and LCPol.ManageCom like '" + manageCom + "%%'";
	}
	
	//alert(strSQL);
	//execEasyQuery( strSQL );
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有没通过核保个人单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}


//选择要人工核保保单
function getPolGridCho()
{
	//setproposalno();
	fm.submit();
}

function manuchk()
{
	cProposalNo=fm.ProposalNo.value;
	flag = fm.all('UWState').value;
	tUWIdea = fm.all('UWIdea').value;
	//alert("flag:"+flag);
	
	if (flag == "0"||flag == "1"||flag == "4"||flag == "6"||flag == "9")
	{
		showModalDialog("./UWManuNormMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	}
	
	if (flag == "2") //延期
	{
		//showModalDialog("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
		window.open("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
	}
	
	if (flag == "3") //条件承保
	{
		//showModalDialog("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
		window.open("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
	}
	if (flag == "7")
	{
		QuestInput();
	}
	
	initInpBox();
    	initPolBox();
	initPolGrid();
}

function QuestInput()
{
	cProposalNo = fm.ProposalNo.value;  //保单号码
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	
	//initInpBox();
    	//initPolBox();
	//initPolGrid();
	
}

function QuestReply()
{
	cProposalNo = fm.ProposalNo.value;  //保单号码
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./QuestReplyMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	
	initInpBox();
    	initPolBox();
	initPolGrid();
	
}

function QuestQuery()
{
  cProposalNo = fm.ProposalNo.value;  //保单号码

  
  if (cProposalNo != "")
  {		
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }
	//initInpBox();
    	//initPolBox();
	//initPolGrid();
	
}

function cancelchk()
{
	fm.all('UWState').value = "";
	fm.all('UWIdea').value = "";	
}

function setproposalno()
{
	var count = PolGrid.getSelNo();
	fm.all('ProposalNo').value = PolGrid.getRowColData(count - 1,1,PolGrid);
}