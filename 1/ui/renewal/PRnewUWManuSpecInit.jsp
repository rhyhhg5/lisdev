<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWManuSpecInit.jsp
//程序功能：保全人工核保特约承保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
<%
  String tPolNo = "";
  String tPrtNo = "";
  String tMissionID = "";
  String tSubMissionID = "";
  tPolNo = request.getParameter("PolNo");  
  tPrtNo = request.getParameter("PrtNo");  
  tMissionID = request.getParameter("MissionID");  
  tSubMissionID = request.getParameter("SubMissionID"); 
%>                            

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
try
  {                                   
	// 延长日期天数
    fm.all('Remark').value = '';
    //fm.all('SpecReason').value = '';
  }
  catch(ex)
  {
    alert("在PEdorUWManuSpecInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }   
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在PEdorUWManuSpecInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm(tPolNo,tPrtNo,tMissionID)
{
  try
  {
     initInpBox();
	initHide(tPolNo,tPrtNo,tMissionID);
	QuerySpec(tPolNo);
	QuerySpecReason(tPolNo);
	QueryRemark(tPolNo);
	
  }
  catch(re)
  {
    alert("在PEdorUWManuSpecInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initHide(tPolNo,tPrtNo,tMissionID)
{
	fm.all('PolNo').value = tPolNo;
	fm.all('PrtNo').value = tPrtNo;
	fm.all('MissionID').value = tMissionID;

}

</script>


