<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ProposalSignSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.xb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";

	try
	{
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");	  
		RnewSign tRnewSign   = new RnewSign();
	    VData tVData=new VData();
	    tVData.add(tG);
		if(tRnewSign.submitData(tVData,"")==false)
		{
		   FlagStr = "Fail";
		   Content="确认过程中部分数据失败，请察看错误日志表";
		}
		else
		{
		   FlagStr = "Succ";
		   Content="续保确认成功";
		}
		
	} // end of try
	catch ( Exception e1 )
	{
		Content = "续保确认失败，原因是: " + e1.toString();
		FlagStr = "Fail";
	}
	//System.out.println(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.initPolGrid();
	parent.fraInterface.easyQueryClick();
</script>
</html>
