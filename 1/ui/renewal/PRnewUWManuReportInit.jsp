<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorUWManuReportInit.jsp
//程序功能：保全人工核保核保报告录入
//创建日期：
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String tProposalNo = "";
  String tFlag = "";
  String tUWIdea = "";
  String str = "";
  String tflag = "";
  tProposalNo = request.getParameter("ProposalNo2");
  tflag = request.getParameter("flag");
%>                            

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	
	
	String strOperator = globalInput.Operator;
	//System.out.println("1:"+strOperator);
%>

<script language="JavaScript">

var str = "";

// 输入框的初始化（单记录部分）
function initInpBox(tProposalNo)
{ 
try
  {                                   
	// 延长日期天数
    //fm.all('Prem').value = '';
    //fm.all('SumPrem').value = '';
    //fm.all('Mult').value = '';
    //fm.all('RiskAmnt').value = '';
    fm.all('ProposalNo').value = tProposalNo;
    fm.all('Operator').value = '<%= strOperator %>';
    fm.all('Content').value = '';
  }
  catch(ex)
  {
    alert("程序名称：PEdorUWManuReportInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }   
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在程序名称：PEdorUWManuReportInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm(tProposalNo)
{
  var str = "";
  try
  {
	initInpBox(tProposalNo);
	initHide(tProposalNo);
	initContent();
  }
  catch(re)
  {
    alert("程序名称：PEdorUWManuReportInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initHide(tProposalNo)
{
	var flag = '<%=tflag%>';
	
	if (flag == "1")
	{
		divButton.style.display = 'none';
	}
	
	fm.all('ProposalNoHide').value=tProposalNo;
	//fm.all('Flag').value=tFlag;
	//fm.all('UWIdea').value= tUWIdea;
	//alert("pol:"+tProposalNo);
}

</script>


