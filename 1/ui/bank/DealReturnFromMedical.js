//ReadFromFileInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var filePath;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量


//提交，保存按钮对应操作
function submitForm() {
  if(verifyInput() == false) return false;
  fm.action = "./DealReturnFromMedicalSave.jsp";
  var accsql = "select 1 from ldfinbank where bankcode ='" + fm.getbankcode.value + "' and bankaccno='" + fm.getbankaccno.value +"'";
      var arrBankResult = easyExecSql(accsql);
      if(arrBankResult==null){
        alert("归集账户不存在，请确认录入的归集账户");
      }
      
  if (BankGrid.getSelNo()) { 
    fm.all("serialNo").value = BankGrid.getRowColData(BankGrid.getSelNo()-1 , 2);
    fm.all("bankCode").value = BankGrid.getRowColData(BankGrid.getSelNo()-1 , 1);
    
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

//获取文件上传保存路径
function getUploadPath() {
  var strSql = "select SysVarValue from LDSysVar where SysVar = 'ReturnFromBankPath'";
  
  filePath = easyExecSql(strSql);
}

//查询出本机构未返回处理的银行批次
function initQuery() {
  var strSql = "select bankcode,serialno from lybanklog a where logtype='D' "
             + "and not exists (select 1 from lysendtosettle where serialno=a.serialno) "
             + "and returndate is not null and comcode like '" + comCode + "%'"
             + "and bankcode not in (select code from ldcode where codetype='automedicalcom')" ;
  turnPage.queryModal(strSql, BankGrid);
}

//初始化银行代码
function initMedicalCode() {
  var strSql = "select MedicalComCode, MedicalComName from LDMedicalCom where  ComCode like '" + comCode + "%'"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("MedicalCode").CodeData = strResult;
  }
}   

// 查询按钮
function easyQueryClick() {
	// 书写SQL语句
	var strSql = "select distinct log.bankcode, log.serialno, log.makedate, log.returndate"
		+"  from lybanklog log"
		+"  left join lysendtosettle set"
		+"    on log.serialno = set.serialno"
		+" where set.dealtype = 'D'"
		+"  and set.dealstate = '1'"
		+ getWherePart('log.BankCode','MedicalCode')
		+ getWherePart('log.returndate','dealdate')
				+" order by log.returndate desc with ur";
				     

				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
}
//显示银行批次数据的统计信息，金额、件数
function showStatistics(parm1, parm2) {
//  var strSql = "select Totalmoney, Totalnum from lybanklog where SerialNo = '" 
//             + BankGrid.getRowColData(BankGrid.getSelNo() - 1, 2) 
//             + "'";
  fm.serialNo.value = BankGrid.getRowColData(BankGrid.getSelNo() - 1, 2);
  tSelNo = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
}
