<html>
<%
  //程序名称：金联佣金类报表
  //程序功能：
  //创建日期：2016-08-10
  //创建人  ：姜丽
  //更新记录：  更新人    更新日期     更新原因/内容
 %>
  <%@page contentType="text/html;charset=GBK"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="GoldenUnionSendListInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GoldenUnionSendListInit.jsp"%>
</head>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>

<SCRIPT>
  var managecom = <%=tGI.ManageCom%>;
</SCRIPT> 

<body onload="initForm();">
  <form action="./GoldenUnionSendListSave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/InputButton.jsp"%>
  <!-- 显示或隐藏LLReport1的信息 -->
  <Div id="divLLReport1" style="display: ''">
    <table class=common>
      <tr class=titleImg>
		 <td class=titleImg>金联佣金类代收付清单</td>
	  </tr>
	</table>
	<table class=common>
	  <tr class=common>
		 <td class=title>发盘开始日期</td>
		 <td class=input><input class="coolDatePicker" dateFormat="short" name="FStartDate"></td>
		 <td class=title>发盘结束日期</td>
		 <td class=input><input class="coolDatePicker" dateFormat="short" name="FEndDate"></td>
	  </tr>
	  <tr class=common>
		 <td class=title>回盘开始日期</td>
		 <td class=input><input class="coolDatePicker" dateFormat="short" name="HStartDate"></td>
		 <td class=title>回盘结束日期</td>
		 <td class=input><input class="coolDatePicker" dateFormat="short" name="HEndDate"></td>
	  </tr>
	  <tr class=common>
		 <td class=title>处理状态</td>
		 <td class=input><Input name=States class=codeNo  CodeData="0|^1|成功^2|失败^3|处理中" ondblclick="return showCodeListEx('States',[this,StatesName],[0,1]);" onkeyup="return showCodeListKeyEx('States',[this,StatesName],[0,1]);" ><input class=codename name=StatesName readonly=true></td>
          <td class=title>管理机构</td>
		 <td class=input colspan='3'><Input class=codeNo name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true></td>     
      </tr>
      <tr class=common>
		 <td><input class=cssButton type=button value="查询" onclick="Showquery()"></td>
      </tr>
      <tr></tr>
      </table>
    <table>
	  <tr>
		 <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" onClick="showPage(this,divLCPol1);"></td>
		 <td class=titleImg>清单列表</td>
	 </tr>
	</table>
	<Div id="divLLReport2" style="display: ''">
		<table class=common>
		  <tr class=common>
			 <td text-align: left colSpan=1><span id="spanBatchSendListGrid"></span></td>
		  </tr>
		</table>
	<Div id="divPage" align=center style="display: 'none' ">
		<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage2.firstPage();">
		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</Div>
	<br><br>
	<table>
      <tr class=common>
            <td><input class=cssButton type=button value="下载清单" onclick="download();"></td>
      </tr>
	</table>
</Div>
		 <INPUT type=hidden name=sql>
 </form>
 <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
