<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//创建日期：2012-2
//创建人  ：ZCX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="BatchSendQueryInput.js"></SCRIPT>
  <%@include file="BatchSendQueryInit.jsp"%>
  <title>集中代收付批次状态查询</title>
</head>

<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./BatchSendQuerySave.jsp">
    <table class= common border=0 width=100%>
        <tr>
            <td class= titleImg align= center>请输入查询条件：</td>
        </tr>
    </table>
    <table  class= common align=center>  
        <TR  class= common>
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            回盘起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=ReturnStartDate >
          </TD>
          <TD  class= title>
            回盘止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=ReturnEndDate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
             银联编码
           </TD>
           <TD  class= input>
                <Input NAME=UniteBankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank4ytj',[this,UniteBankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank4ytj',[this,UniteBankCodeName],[0,1]);"><input class=codename name=UniteBankCodeName readonly=true  elementtype="nacessary">
           </TD>
        <TD  class= title>
            管理机构
        </TD>
        <TD  class= input>
          <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
          <font color="#FF0000" align="Left">*</font>
         </TD>
         </TR>
         <TR  class= common>
          <TD  class= title>
             收付费类型
           </TD>
           <td class=input><Input class=codeno name=Flag CodeData="0|^S|代收^F|代付^R|实时代收" ondblClick="showCodeListEx('SXFlag1',[this,SXFlagName],[0,1]);" onkeyup="showCodeListKeyEx('SXFlag1',[this,SXFlagName],[0,1]);"><input class=codename name=SXFlagName readonly=true></td>
        <TD  class= title>
            
        </TD>
        <TD  class= input>
         
         </TD>
         </TR>
    </table> 
    <table align=right>
      <tr>
        <td> 
          <INPUT VALUE="查 询" class=cssButton TYPE=button onclick="easyQueryClick();">
        </td>
        <td>
          <INPUT VALUE="下 载" class=cssButton TYPE=button onclick="download();"> 
        </td>
      </tr>
    </table>
    <br>
    <font color="#FF0000" align="Left">起始日期、终止日期：为批次生成日期区间段。</font><br>
    <font color="#FF0000" align="Left">回盘起期、回盘止期：为批次回盘日期区间段，若批次尚未回盘，选择回盘起/止期，批次不会显示。</font><br>
    <font color="#FF0000" align="Left">起始日期/终止日期、回盘起期/回盘止期，两组日期至少录入一组。</font><br>
    <br>
    <br>
    <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <td text-align: left colSpan=1>
            <span id="spanBankLogGrid" ></span> 
        </td>
      </TR>
    </Table>                    
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">                  
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div> 
 <INPUT type=hidden name=sql>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
