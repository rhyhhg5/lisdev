<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：ReadFromFileInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head > 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="ReadFromFileInput.js"></SCRIPT>
  <%@include file="ReadFromFileInit.jsp"%>
  
  <title>生成送银行文件 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  comCode = "<%=tGlobalInput.ComCode%>";
  dealType = "<%=request.getParameter("DealType")%>";
</script>

<body  onload="initForm();" >
  <form action="./ReadFromFileSave.jsp" ENCTYPE="multipart/form-data" method=post name=fm target="fraTitle" >
    <%@include file="../common/jsp/InputButton.jsp"%>
    
    <table class= common border=0 width=100%>
      <tr>
  			<td class= titleImg align= center>请输入查询条件：</td>
  		</tr>
  	</table>
	
    <table  class= common align=center>
      	<TR  class= common>
 <TD  class= title>
        银行代码
      </TD>
      <TD  class= input>
        <Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank" ><input class=codename name=BankCodeName readonly=true >
      </TD>
          <TD  class= title>
            文件生成日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=SendDate >
          </TD>
					<TD  class= input>
            <INPUT VALUE="查  询" TYPE=button class=CssButton onclick="easyQueryClick();">
          </TD>
        </TR>
    </table>
    
    <hr> 
    
    <!-- 生成送银行文件 fraSubmit-->
    
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请选择批次号：</td>
  		</tr>
  	</table>   
        
    <!-- 批次号信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 批次号信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </Div>
    
    <br>
  
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        请输入要读取的文件名称：
      </TD>
      <TD  class= input>
        <Input class= common5 TYPE=file name=FileName verify="文件名称|notnull" > 
      </TD>           
    </TR>
    </table>
    
    <hr>
    
    <table  class= common>
    <TR  class= common>
    	<TD  class= title>
        非银联直接读取	
      </TD>
      <TD  class= input>
        <INPUT VALUE=" 读  取  文  件 " class= cssbutton TYPE=button onclick="submitForm()">
      </TD> 
      <TD  class= input>
         
      </TD>
      <TD  class= input>
         
      </TD>        
    </TR>
    </table>
    
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        银联子银行代码	
      </TD>
      <TD  class= input>
        <Input NAME=BankInUniteCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('bank',[this,BankInUniteCodeName],[0,1]);" onkeyup="return showCodeListKey('bank',[this,BankInUniteCodeName],[0,1]);" verify="银行代码|notnull&code:bank" ><input class=codename name=BankInUniteCodeName readonly=true >
      </TD>
      <TD  class= input>
        <INPUT VALUE="读取银联总汇文件" class= cssbutton TYPE=button onclick="submitFormHZ()">  
      </TD>
      <TD  class= input>
        <INPUT VALUE="读取银联明细文件" class= cssbutton TYPE=button onclick="submitFormMX()">   
      </TD>           
    </TR>
        <INPUT VALUE=""  TYPE=hidden  name=DealType>
    </table>
    
  </form>    
     <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

