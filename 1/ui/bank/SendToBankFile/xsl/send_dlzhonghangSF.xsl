<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
					  <!-- 业务类型 -->
						<xsl:choose>
					    <xsl:when test="DealType='S'">
					      <xsl:text>40502</xsl:text>
					    </xsl:when>
					   	<xsl:otherwise>
					     	<xsl:text>40501</xsl:text>
					   	</xsl:otherwise>
					  </xsl:choose>
						<!-- 企业编码 -->
						<xsl:value-of select="'222000100'"/>
						<xsl:call-template name="supplement">
              <xsl:with-param name="valueLen" select="string-length('222000100')"/>
              <xsl:with-param name="length" select="12"/>
              <xsl:with-param name="key" select="' '"/>
            </xsl:call-template>
						<!-- 批次号 -->
						<xsl:variable name="serialno"  select ="SerialNo"/> 
					  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
						<xsl:value-of select="'222000100'"/>
						<xsl:value-of select="substring($serialno,string-length(string($serialno))-4,string-length(string($serialno)))"/> 
						<xsl:choose>
					    <xsl:when test="DealType='S'">
					      <!-- 回执期限 -->
						    <xsl:text>01</xsl:text>
					    </xsl:when>
					   	<xsl:otherwise>
					     	<!-- 合同号 -->
			              <xsl:value-of select="'2220001000000000'"/>
						  <xsl:call-template name="supplement">
                            <xsl:with-param name="valueLen" select="string-length('2220001000000000')"/>
                            <xsl:with-param name="length" select="60"/>
                            <xsl:with-param name="key" select="' '"/>
                          </xsl:call-template>
					    </xsl:otherwise>
					  </xsl:choose>
						<!-- 开户银行 -->
						<xsl:choose>
					    <xsl:when test="DealType='S'">
					      <xsl:text>313222080230</xsl:text>
					    </xsl:when>
					   	<xsl:otherwise>
					     	<xsl:text>102222020070</xsl:text>
					   	</xsl:otherwise>
					  </xsl:choose>
						<!-- 收款人账户 -->
						<xsl:choose>
					    <xsl:when test="DealType='S'">
					      <xsl:text>802301209007501                 </xsl:text>
					    </xsl:when>
					   	<xsl:otherwise>
					      <xsl:text>3400200729023102412             </xsl:text>
					   	</xsl:otherwise>
					  </xsl:choose>
						<!-- 业务种类 -->
                        <xsl:choose>
                        <xsl:when test="DealType='S'">
                          <xsl:text>00600</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>10600</xsl:text>
                        </xsl:otherwise>
                      </xsl:choose>
						<!-- 总金额  -->
						<xsl:call-template name="supplement">
              <xsl:with-param name="valueLen" select="string-length(string($totalpay*100))"/>
              <xsl:with-param name="length" select="15"/>
              <xsl:with-param name="key" select="' '"/>
            </xsl:call-template>
            <xsl:value-of select="string($totalpay*100)"/>
						<!-- 总笔数 -->
						<xsl:call-template name="supplement">
              <xsl:with-param name="valueLen" select="string-length(string($totalpic))"/>
              <xsl:with-param name="length" select="8"/>
              <xsl:with-param name="key" select="' '"/>
            </xsl:call-template>
						<xsl:value-of select="$totalpic"/>
						<!-- 定制信息 -->
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	<xsl:for-each select="BANKDATA/ROW">
	    <!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 以下的代码块根据不同的银行而不同 -->
			<!-- 序号 -->
        <xsl:value-of select="substring(PolNo,string-length(PolNo)-7,string-length(PolNo))"/>
        <xsl:call-template name="supplement">
              <xsl:with-param name="valueLen" select="string-length(substring(PolNo,string-length(PolNo)-7,string-length(PolNo)))"/>
              <xsl:with-param name="length" select="8"/>
              <xsl:with-param name="key" select="'0'"/>
            </xsl:call-template>
			<!-- 开户银行 -->
			<xsl:value-of select="'            '"/>
      <!-- 收款人卡号 -->
  		<xsl:value-of select="AccNo"/>
        <xsl:call-template name="supplement">
              <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
              <xsl:with-param name="length" select="32"/>
              <xsl:with-param name="key" select="' '"/>
            </xsl:call-template>
  		<!-- 账户名称，不够60位右补空格 -->
        <xsl:variable name="accNameValue" select="string(AccName)"/>
       <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.GetAgreementNo.getChinaLenOth($accNameValue)"/>
        <xsl:choose>
        <xsl:when test="30 > $accNameLen">
          <xsl:value-of select="substring(AccName, 1, 60-$accNameLen)"/>
        </xsl:when>
        <!-- 多于五个汉字的处理 -->
        <xsl:otherwise>
          <xsl:value-of select="substring(AccName, 1, 30)"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
        <xsl:with-param name="length" select="60"/>
        <xsl:with-param name="key" select="' '"/>
      </xsl:call-template>
  		<!-- 联系方式 -->
			<xsl:value-of select="'                                                            '"/>
  		<!-- 交易金额，15位，左补空格 -->
      <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length(string(PayMoney*100))"/>
        <xsl:with-param name="length" select="15"/>
        <xsl:with-param name="key" select="' '"/>
      </xsl:call-template>
      <xsl:value-of select="PayMoney*100"/>
			<xsl:choose>
					    <xsl:when test="DealType='S'">
					      <!-- 合同号 -->
                          <xsl:variable name="paycode"  select ="PayCode"/> 
                       <xsl:variable name="agreementno" select="string(java:com.sinosoft.lis.pubfun.GetAgreementNo.getAgreementNo($paycode))"/>
                        <xsl:value-of select="$agreementno"/>
					      <xsl:call-template name="supplement">
                   <xsl:with-param name="valueLen" select="string-length($agreementno)"/>
                   <xsl:with-param name="length" select="60"/>
                   <xsl:with-param name="key" select="' '"/>
               </xsl:call-template>
					    </xsl:when>
					   	<xsl:otherwise>
					   	</xsl:otherwise>
					  </xsl:choose>
			<!-- 附录 -->
			<xsl:value-of select="'                                                            '"/>
			<!-- 定制信息 -->
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>
