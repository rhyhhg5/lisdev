<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
              <xsl:variable name="serialno"  select ="SerialNo"/>
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				      <!-- 总笔数 6位-->
						  <xsl:value-of select="format-number($totalpic, '000000')"/>
						  <!-- 总金额 15位 -->
						  <xsl:value-of select="format-number($totalpay, '000000000000.00')"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose>
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
	<!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 账号，不超过18位-->
  <!-- 账号，不够18位左补空格 -->
  <xsl:variable name="accNoValue" select="substring(AccNo, 1, 18)"/>
  <xsl:variable name="accNo" select="java:com.sinosoft.lis.pubfun.PubFun.LFillCh($accNoValue,' ',18)"/>
  <xsl:value-of select="$accNo"/>
	
  <!-- 账户名称，不够10位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  <xsl:choose>
    <xsl:when test="20" > 
      <xsl:value-of select="substring(AccName, 1, 20-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 20)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
	<!-- 录入日期 8位-->
	<xsl:value-of select="concat(substring(SendDate,1,4),substring(SendDate,6,2),substring(SendDate,9,2))"/>
	<!-- 标志 2位-->
	<xsl:value-of select="'2K'"/>
	<!-- 入帐金额，12位 -->
  <xsl:value-of select="format-number(PayMoney, '000000000.00')"/>
  <!-- 记录顺序号 12位 -->
 	<xsl:variable name="detailnumber" select="position()"/>
	<xsl:value-of select="format-number($detailnumber, '000000000000')"/>
	<!-- 入账日期 8位-->
	<xsl:value-of select="'        '"/>
	<!-- 回车换行 -->
	<xsl:text>&#13;&#10;</xsl:text>

	</xsl:for-each>
	
</xsl:template>

</xsl:stylesheet>