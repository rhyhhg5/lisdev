<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
  <xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>

	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
					    <xsl:value-of select="'#总计信息'"/> 
					     <xsl:text>&#13;&#10;</xsl:text>
					     <xsl:value-of select="'#金额精确到分'"/> 
					     <xsl:text>&#13;&#10;</xsl:text>
					     <xsl:value-of select="'#币种|日期|总计标志|总金额|总笔数|'"/> 
					     <xsl:text>&#13;&#10;</xsl:text>
					     <xsl:value-of select="'RMB'"/>
					     <xsl:value-of select="'|'"/> 
					     <xsl:variable name="tDate"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/> 
               <xsl:value-of select="concat(substring($tDate,1,4),substring($tDate,6,2),substring($tDate,9,2))"/> 
					     <xsl:value-of select="'|'"/> 
					     <xsl:value-of select="'1'"/> 
					     <xsl:value-of select="'|'"/> 
					     <xsl:variable name="serialno"  select ="SerialNo"/> 
						  <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
					    <xsl:value-of select="format-number($totalpay*100, '####')"/> 
						  <xsl:value-of select="'|'"/>
              <xsl:value-of select="number($totalpic)"/>
              <xsl:text>&#13;&#10;</xsl:text> 
				      <xsl:value-of select="'#明细信息如下'"/> 
							<xsl:text>&#13;&#10;</xsl:text> 
						  <xsl:value-of select="'#币种|提交日期|明细标志|顺序号|付方开户行名|付方卡号|缴费编号|付方户名|收款单位开户行名|省份|收款方地区名|收款方地区代码|收款帐号|协议编号|收款单位名称|付款金额|用途|备注|'"/> 
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
<xsl:for-each select="BANKDATA/ROW">
	<xsl:value-of select="'RMB'"/>
  <xsl:value-of select="'|'"/> 
  <xsl:variable name="tDate"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/> 
  <xsl:value-of select="concat(substring($tDate,1,4),substring($tDate,6,2),substring($tDate,9,2))"/> 
  <xsl:value-of select="'|'"/>  
  <xsl:value-of select="'2'"/> 
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="position()"/>
  <xsl:value-of select="'|'"/>
   <xsl:value-of select="'工行'"/>
   <xsl:value-of select="'|'"/>
    <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="PayCode"/> 
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'工行'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'广东'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'中山'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'2011'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'2011002519200078351'"/>
  <xsl:value-of select="'|'"/>
   <xsl:value-of select="'BDP6904'"/>
  <xsl:value-of select="'|'"/>
   <xsl:value-of select="'中国人民健康保险股份有限公司中山中心支公司'"/>
  <xsl:value-of select="'|'"/>
   <xsl:value-of select="format-number(PayMoney*100, '####')"/> 
  <xsl:value-of select="'|'"/>
     <xsl:value-of select="'保费'"/>
      <xsl:value-of select="'|'"/>
      <xsl:value-of select="'|'"/>
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>