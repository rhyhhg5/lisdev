<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                xmlns:medical="com.sinosoft.lis.bank"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 交易编码 -->
	<xsl:value-of select="'2511'"/> 
	<xsl:value-of select="'^'"/> 
	<!-- 医保定点机构编码 -->
	<xsl:value-of select="'9001'"/> 
	<xsl:value-of select="'^'"/>   
	<!-- 操作员编码 -->
	<xsl:value-of select="'999001'"/> 
	<xsl:value-of select="'^'"/>
	<!-- 业务周期号 -->
	<xsl:value-of select="medical:GetParamToM.getBusiCode()" />
	<xsl:value-of select="'^'"/>
	<!-- 医院交易流水号 -->
	<xsl:value-of  select ="SerialNo"/>
	<xsl:value-of select="'^'"/> 
	<!-- 中心编码 -->
	<xsl:value-of select="'0000'"/> 
	<xsl:value-of select="'^'"/> 
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
						<!-- 缴费通知书号--> 
						<xsl:value-of select="PolNo"/>  
						<xsl:value-of select="'|'"/>  
						<!-- 个人编号 -->
						<xsl:value-of select ="AccNo"/> 
						<xsl:value-of select="'|'"/> 
					    <!-- 险种编码 -->
					    <xsl:value-of select="RiskCode"/>
					    <xsl:value-of select="'|'"/> 	
						<!-- 业务类型 -->
					    <xsl:value-of select="PayType"/>
					    <xsl:value-of select="'|'"/> 			    
					    <!-- 经办人 -->
					    <xsl:value-of select="AgentCode"/>
						<!-- 隔行 -->
						<xsl:value-of select="'$'"/> 
	</xsl:for-each>
	<!-- 联机标志 -->
	<xsl:value-of select="'^'"/> 
	<xsl:value-of select="'1'"/>  
	<xsl:value-of select="'^'"/> 
</xsl:template>

</xsl:stylesheet>