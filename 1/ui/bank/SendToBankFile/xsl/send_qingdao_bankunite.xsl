<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
				      <xsl:value-of select="''"/>
							<xsl:value-of select="'|'"/> 
				      <xsl:variable name="date"  select ="substring(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2(),3,6)"/> 
			        <xsl:variable name="time"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentTime())"/>
			        <xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,$time),2))"/>
			        <xsl:value-of select="$no"  /> 
			         <xsl:value-of select="'|'"/>
			        <xsl:choose>
    					   <xsl:when test="DealType='S'">
      			       	<xsl:text>0</xsl:text>
    				    	</xsl:when>
   					     <xsl:otherwise>
     				       <xsl:text>1</xsl:text>
   					     </xsl:otherwise>
  			    	</xsl:choose>
			         <xsl:value-of select="'|'"/>
			          <xsl:value-of select="''"/>
              <xsl:value-of select="'|'"/>
              <xsl:variable name="date1"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/> 
              <xsl:value-of select="concat(substring($date1,1,4),substring($date1,6,2),substring($date1,9,2))"/> 
              <xsl:value-of select="'|'"/>
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
						  <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						   <!-- 总金额 18位 -->
              <xsl:value-of select="format-number($totalpay, '0.00')"/> 
						  <xsl:value-of select="'|'"/>
						  <!-- 总笔数 5位-->
						  <xsl:value-of select="number($totalpic)"/>
              <xsl:value-of select="'|'"/>
              <xsl:value-of select="'|'"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
	 <xsl:variable name="position"  select ="position()"/>
	 <xsl:value-of select="'00000000'"/>
	  <xsl:call-template name="supplement">
   				 <xsl:with-param name="valueLen" select="string-length($position)"/>
    			 <xsl:with-param name="length" select="8"/>
    			<xsl:with-param name="key" select="'0'"/>
  				</xsl:call-template>		
	 <xsl:value-of select="$position"/>	
	 <xsl:value-of select="'|'"/>
	 	<xsl:value-of select="'156'"/>
	 	<xsl:value-of select="'|'"/>
	 	<xsl:value-of select=" string(PayMoney)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(BankCode)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(BankName)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(PayCode)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
	
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>