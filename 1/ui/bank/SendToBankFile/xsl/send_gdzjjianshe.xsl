<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">


	<!-- 20位序号 -->
	<xsl:value-of select="substring(PayCode, 1, 20)"/>
	
	<xsl:value-of select="'|'"/>
	
	<!-- 账号，不超过19位 -->
	<xsl:variable name="accNoLen"  select ="string-length(AccNo)"/> 
	<xsl:choose>
    <xsl:when test="20 > $accNoLen">
      <xsl:value-of select="AccNo"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="substring(AccNo, 1, 19)"/>
    </xsl:otherwise>
  </xsl:choose>
	
	<xsl:value-of select="'|'"/>
	
	<!-- 账号姓名 -->
	<xsl:value-of select="AccName"/>
	
	<xsl:value-of select="'|'"/>
	
	<!-- 交易金额 -->
	<xsl:value-of select="PayMoney"/>
	
	<xsl:value-of select="'|'"/>
	
	<!-- 摘要 -->
	<xsl:value-of select="'无'"/>
	
	

  <!-- 以上的代码块根据不同的银行而不同 -->

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>