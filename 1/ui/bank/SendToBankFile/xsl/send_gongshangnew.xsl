<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gb2312"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
  <!-- 序号 -->
  <xsl:value-of select="'序号*'"/>
  <xsl:value-of select="'&#9;'"/>
  <!-- 银行代码 -->
  <xsl:value-of select="'银行代码*'"/>
  <xsl:value-of select="'&#9;'"/>
  <!-- 账号 -->
  <xsl:value-of select="'账号*'"/>
  <xsl:value-of select="'&#9;'"/>
  <!-- 户名 -->
  <xsl:value-of select="'户名*'"/>
  <xsl:value-of select="'&#9;'"/>
  <!-- 金额 -->
  <xsl:value-of select="'金额*'"/>
  <xsl:text>&#13;&#10;</xsl:text>

<xsl:for-each select="BANKDATA/ROW">
   
  <!-- 序列号 -->
  <xsl:variable name="detailnumber" select="position()"/>
	<xsl:value-of select="format-number($detailnumber, '000000')"/>	
	<xsl:value-of select="'&#9;'"/>
 
  <!--  银行代码  固定103-->
  <xsl:value-of select="'103'"/>
  <xsl:value-of select="'&#9;'"/>
  <!--  银行账号  -->
  <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'&#9;'"/>
  <!--  银行姓名  -->
  <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'&#9;'"/>
  <!--  金额  -->
  <xsl:value-of select=" string(PayMoney)"/>
  
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>