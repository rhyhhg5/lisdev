<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			
			<!-- 收款人卡号，21位 -->
  		<xsl:value-of select="substring(AccNo, 1, 21)"/>	
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 收款人姓名，20位 -->
  		<xsl:value-of select="substring(AccName, 1, 20)"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 金额，8位 -->
  		<xsl:value-of select="substring(PayMoney, 1, 8)"/>	
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		<!-- 收款人证件号码，20位 -->
  		<xsl:value-of select="substring(IDNo, 1, 18)"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 收款人证件类型，10位 -->
  		<xsl:value-of select="substring(IDType, 1, 10)"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 合同号，20位 -->
  		<xsl:value-of select="substring(PolNo, 1, 20)"/>
			<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>