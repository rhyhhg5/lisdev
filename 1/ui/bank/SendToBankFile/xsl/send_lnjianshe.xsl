<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->			
			<!-- 收款人卡号，20位 -->
  		<xsl:value-of select="substring(AccNo,1,20)"/>
  		<xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
		    <xsl:with-param name="length" select="20"/>
		    <xsl:with-param name="key" select="' '"/>
		  </xsl:call-template>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>
				<!-- 金额，16位-->
			<xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
		    <xsl:with-param name="length" select="16"/>
		    <xsl:with-param name="key" select="' '"/>
		  </xsl:call-template>
  		<xsl:value-of select="substring(PayMoney,1,16)"/>
  		<xsl:value-of select="'|'"/>	
  		<!-- 保单号，16位-->
  		<xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
		    <xsl:with-param name="length" select="16"/>
		    <xsl:with-param name="key" select="' '"/>
		  </xsl:call-template>
  		<xsl:value-of select="substring(PayCode,1,16)"/>
  		<xsl:value-of select="'|'"/>	
  	<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>