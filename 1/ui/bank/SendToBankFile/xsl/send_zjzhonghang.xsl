<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			<!-- 明细标志--> 
			<!-- 收款人卡号，30位 -->
			<!-- 保单号，30位 -->
			
  		<xsl:value-of select="substring(PayCode, 1,30)"/>
	  		<xsl:call-template name="supplement">
			    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
			    <xsl:with-param name="length" select="30"/>
			    <xsl:with-param name="key" select="' '"/>
			  </xsl:call-template>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>
				
			<xsl:variable name="accNameValue" select="string(AccName)"/>
  		<xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
			<xsl:choose>
		    <xsl:when test="30 > $accNameLen">
		      <xsl:value-of select="substring(AccName, 1, 60-$accNameLen)"/>
		    </xsl:when>
		    <!-- 多于五个汉字的处理 -->
		    <xsl:otherwise>
		      <xsl:value-of select="substring(AccName, 1, 30)"/>
		    </xsl:otherwise>
		  </xsl:choose>
		  <xsl:call-template name="supplement">
				 <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
				 <xsl:with-param name="length" select="60"/>
				 <xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
  
	
				<xsl:value-of select="'|'"/>
					
  		<xsl:value-of select="substring(AccNo, 1,30)"/>
  		<xsl:call-template name="supplement">
				 <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
				 <xsl:with-param name="length" select="30"/>
				 <xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>		
  		<!-- 金额，12位 -->
  		<xsl:value-of select="substring(PayMoney, 1,13)"/>
  		<xsl:call-template name="supplement">
				 <xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
				 <xsl:with-param name="length" select="13"/>
				 <xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>
  		<!-- 日期，8位 -->
  		<xsl:variable name="date"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2())"/> 
  		<xsl:value-of select="$date"/>
  	<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>