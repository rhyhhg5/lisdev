<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<!-- 汇总记录 -->
	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="DealType"/>
						<xsl:value-of select="','"/>
						<xsl:value-of select="'1345200007'"/>
						<xsl:value-of select="','"/>
						<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
						<xsl:value-of select="','"/>
						<xsl:variable name="serialno" select="SerialNo"/> 
			      <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						<xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>         
					  <xsl:value-of select="$totalpic"/>
						<xsl:value-of select="','"/>
						<xsl:value-of select="format-number(substring($totalpay*100,1,12), '000')"/> 
						<xsl:value-of select="','"/>
						<xsl:value-of select="'00200'"/>					        						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
<xsl:for-each select="BANKDATA/ROW">
  <!-- 以下的代码块根据不同的银行而不同 -->
  <xsl:variable name="tposition" select="position()"/> 
  <xsl:value-of select="format-number($tposition, '000000')"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="BankCode"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="AccNo"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="AccName"/> 
  <xsl:value-of select="','"/> 
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:variable name="tPayMoney" select="PayMoney"/> 
  <xsl:value-of select="format-number($tPayMoney*100, '00')"/>
  <xsl:value-of select="','"/> 
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
  <xsl:value-of select="PayCode"/> 
  <xsl:value-of select="','"/>
  <xsl:value-of select="','"/>
 <xsl:text>&#13;&#10;</xsl:text>
</xsl:for-each>

</xsl:template>

</xsl:stylesheet>