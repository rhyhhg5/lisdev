<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

  <!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 姓名，20位 -->
  <!-- 账户名称，不够20位右补空格 -->
  <xsl:variable name="accNameValue" select="string(Name)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>

  <xsl:choose>
    <xsl:when test="20 > $accNameLen">
      <xsl:value-of select="substring(Name, 1, 20-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(Name, 1, 20)"/>
    </xsl:otherwise>
  </xsl:choose>
  
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(Name) + $accNameLen"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="'|'"/>

  <!-- 交易金额，15位 -->
  
    <xsl:value-of select="PayMoney"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(string(PayMoney))"/>
    <xsl:with-param name="length" select="15"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="'|'"/>  
  

  <!-- 账号，不超过30位-->
  <xsl:value-of select="substring(AccNo, 1, 30)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="'|'"/>
  <!-- 证件号，不超过20位  -->
  <xsl:value-of select="substring(IDNo, 1, 20)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(IDNo)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="'|'"/>
  
  <!-- 收据号，不够21位后面补空格 -->
  <xsl:value-of select="PayCode"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="21"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="'|'"/>      
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>