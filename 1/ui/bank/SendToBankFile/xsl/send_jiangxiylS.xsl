<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
	<xsl:choose>
		<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
			</xsl:when>
				<xsl:otherwise>
					<!-- 收款标志 -->
  				<xsl:value-of select="'S'"/>
  				<xsl:value-of select="','"/> 
  				
  				<!-- 商户ID -->
  				<xsl:value-of select="'1342000006'"/>
  				<xsl:value-of select="','"/> 
  				
  				<!-- 提交日期 -->
  				<xsl:variable name="tDate"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/> 
					<xsl:value-of select="concat(substring($tDate,1,4),substring($tDate,6,2),substring($tDate,9,2))"/> 		
					<xsl:value-of select="','"/> 
					
  				<!-- 总记录数 -->
  				<xsl:variable name="serialno"  select ="SerialNo"/> 
					<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
					<xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
					<xsl:value-of select="number($totalpic)"/>
  				<xsl:value-of select="','"/> 
  				
  				<!-- 总金额 -->
  				<xsl:value-of select="$totalpay*100"/> 
  				<xsl:value-of select="','"/> 
  				
  				<!-- 业务类型 -->
  				<xsl:value-of select="'10600'"/> 
					
					<!-- 回车换行 -->
					<xsl:text>&#13;&#10;</xsl:text>
				</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	
	<xsl:for-each select="BANKDATA/ROW">
		<!-- 1 处理顺序号，6位，是 --> 
		<xsl:value-of select="format-number(position(), '000000')"/>
		<xsl:value-of select="','"/> 
		
		<!-- 2 通联支付用户编号，20位,否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 3 银行代码，3位，是--> 
		<xsl:value-of select="substring(UniteBankCode, 1,3)"/>
		<xsl:value-of select="','"/> 
		
		<!-- 4 帐号类型，2位，否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 5 账号，32位，是 --> 
		<xsl:value-of select="substring(AccNo, 1,32)"/>
		<xsl:value-of select="','"/> 
		
		<!-- 6 账户名，60，是 --> 
		<xsl:value-of select="substring(AccName, 1,60)"/>
		<xsl:value-of select="','"/> 
		
		<!-- 7 开户行所在省	CHAR(20)	否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 8 开户行所在市	CHAR(20)	否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 9 开户行名称	CHAR(60)	否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 10 账户类型	CHAR(1)	否 -->
		<xsl:value-of select="','"/> 
		
		<!--11 金额	CHAR(12)	是 -->
		<xsl:value-of select="PayMoney*100"/> 
		<xsl:value-of select="','"/> 
		
		<!--12 货币类型	CHAR(3)	否 -->
		<xsl:value-of select="','"/> 
		
		<!--13 协议号	CHAR(60)	否 -->
		<xsl:value-of select="','"/> 
		
		<!--14 协议用户编号	CHAR(30) 否 -->
		<xsl:value-of select="','"/> 
		
		<!--15 开户证件类型	CHAR(1)	Null-->
		<xsl:value-of select="','"/> 
		<!--16 证件号	CHAR(22)	否      -->
		
		<xsl:value-of select="','"/> 
		<!--17 手机号小灵通	CHAR(13)	否-->
		<xsl:value-of select="','"/> 
		<!--18 自定义用户号	CHAR(20)	否-->
		<xsl:value-of select="','"/> 
		<!--19 备注	CHAR(50)	否        -->
		<xsl:value-of select="','"/> 
		
		<!-- 20 反馈码	CHAR(4)	否        -->
		<xsl:value-of select="','"/> 
		
		<!--21原因	CHAR(100)	否        -->
		
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>
	
</xsl:template>

</xsl:stylesheet>