<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">


  <xsl:value-of select="substring(AccNo,1,22)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="22"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="' '"/>
	<xsl:value-of select="PayMoney*100"/>	
	  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayMoney*100)"/>
    <xsl:with-param name="length" select="15"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
   <xsl:value-of select="' '"/>
  
  <!-- 账户名称，10	右对齐，不足补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="6 > $accNameLen">
    <xsl:value-of select="substring(AccName, 1, 12-$accNameLen)"/>
      <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length($accNameValue)+$accNameLen"/>
        <xsl:with-param name="length" select="12"/>
        <xsl:with-param name="key" select="' '"/>
      </xsl:call-template>     
    </xsl:when>
    <!-- 多于5个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 6)"/>
    </xsl:otherwise>
  </xsl:choose>
  
	<xsl:value-of select="substring(PayCode, 1,16)"/>
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="16"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>