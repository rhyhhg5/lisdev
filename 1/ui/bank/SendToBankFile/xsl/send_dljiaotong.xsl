<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
					    <xsl:when test="DealType='S'">
					      <xsl:text>2120000714</xsl:text>
					    </xsl:when>
					   	<xsl:otherwise>
					     	<xsl:text>2120000715</xsl:text>
					   	</xsl:otherwise>
					  </xsl:choose>
						<!-- 分隔符--> 
						<xsl:text>|</xsl:text>
						<xsl:variable name="serialno"  select ="SerialNo"/> 
					  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						<!-- 总笔数 -->
						<xsl:value-of select="$totalpic"/>
						<!-- 分隔符--> 
						<xsl:text>|</xsl:text>
				    <!-- 总金额  -->
						<xsl:value-of select="format-number($totalpay, '0.00')"/>										        						                    
						<!-- 回车换行 -->
						<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->			
			<!-- 收款人卡号，30位 -->
  		<xsl:value-of select="AccNo"/>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>
			<xsl:value-of select="AccName"/>
				<xsl:value-of select="'|'"/>	
  		<!-- 金额，12位� -->
  		<xsl:value-of select="PayMoney"/>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>			
  		<!-- 保单号，30位� -->
  		<xsl:value-of select="PayCode"/>
  	<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
	<xsl:text>&#13;&#10;</xsl:text>
	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			<xsl:choose>
					    <xsl:when test="DealType='S'">
					      <xsl:text>2120000714</xsl:text>
					    </xsl:when>
					   	<xsl:otherwise>
					     	<xsl:text>2120000715</xsl:text>
					   	</xsl:otherwise>
			</xsl:choose>
			<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>
			<!-- 收款人卡号 -->
  		<xsl:value-of select="AccNo"/>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>
  		<!-- 保单号 -->
  		<xsl:value-of select="PayCode"/>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>
			<xsl:value-of select="AccName"/>
				<xsl:value-of select="'|'"/>
  		
  		<xsl:variable name="tIDType"  select ="IDType"/> 
  		<xsl:choose>
					    <xsl:when test="$tIDType='0'">
					      <xsl:text>15</xsl:text>
					    </xsl:when>
					   	<xsl:otherwise>
					     	<xsl:value-of select="$tIDType"/>
					   	</xsl:otherwise>
			</xsl:choose>
  		<xsl:value-of select="'|'"/>
  		<xsl:value-of select="IDNo"/>
  	<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>