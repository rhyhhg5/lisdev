<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java" version="1.0">

	<xsl:output method="text" encoding="gbk" />

	<xsl:include href="send_function.xsl" />

	<xsl:template match="/">
		<xsl:for-each select="BANKDATA/ROW[position()=1]">
			<xsl:variable name="serialno" select="SerialNo" />
			<xsl:variable name="totalpay"
				select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))" />
			<xsl:variable name="totalpic"
				select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))" />
			<!-- 加密标志（1位）0-明文，1-密文 -->
			<xsl:value-of select="'JIAMBZ:0'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 单位编号，未提供 -->
			<xsl:value-of select="'DANWBH:31110001'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 业务种类（4位）（代扣客户帐0005，付款给客房0118）-->
			<xsl:value-of select="'YWZLBH:0005'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 外围业务帐户类型 -->
			<xsl:value-of select="'KHZHLX:1'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 客户帐号 -->
			<xsl:value-of select="'KEHUZH:31110188000046934'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 币种 -->
			<xsl:value-of select="'HUOBDH:01'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 钞汇标志 -->
			<xsl:value-of select="'CHUIBZ:0'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 帐户性质 -->
			<xsl:value-of select="'ZHHUXZ:0001'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 收付标志，不可为空，1-批量代发，2-批量代扣 -->
			<xsl:value-of select="'SHOFBZ:2'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 请求总金额 （为明细的汇总金额） -->
			<xsl:value-of select="concat('ZONGJE:',$totalpay)" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 请求总笔数 （为明细的汇总笔数） -->
			<xsl:value-of select="concat('BISHUU:',$totalpic)" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!-- 可空(入帐日期) （为当天日期） -->
			<xsl:value-of select="'RUZHRQ:'" />
			<xsl:text>&#13;&#10;</xsl:text>
			<!--  -->
			<xsl:value-of select="'---------------------------------'" />
		</xsl:for-each>

		<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
		<xsl:for-each select="BANKDATA/ROW">
			<!-- 客户账号 -->
			<xsl:value-of select="AccNo" />
			<xsl:value-of select="'|'" />
			<!-- 上传金额 -->
			<xsl:value-of select="PayMoney" />
			<xsl:value-of select="'|'" />
			<!-- 客户姓名 -->
			<xsl:value-of select="AccName" />
			<xsl:value-of select="'|'" />
			<!-- 收费号 -->
			<xsl:value-of select="PayCode" />
			<xsl:value-of select="'|'" />
			<!-- 摘要 -->
			<xsl:value-of select="''" />
			<xsl:value-of select="'|'" />
			<!-- 回车换行 -->

			<xsl:text>&#13;&#10;</xsl:text>
		</xsl:for-each>

	</xsl:template>

</xsl:stylesheet>
