﻿<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
							<xsl:value-of select="'F'"/>
							<xsl:text>&#9;</xsl:text>	
							<xsl:value-of select="'0090'"/>
							<xsl:text>&#9;</xsl:text>
							<xsl:value-of select="'090'"/>
							<xsl:value-of select="'3701010'"/>		
							<xsl:variable name="date"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2())"/> 
						  <xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,'lcyz'),2))"/>
							<xsl:value-of select="$date"/>
							<xsl:value-of select="$no"/> 
							<xsl:text>&#9;</xsl:text>
							<xsl:value-of select="'0'"/>
							<xsl:text>&#9;</xsl:text>
							<xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						  <xsl:value-of select="format-number($totalpic, '0')"/>						   				      
							<xsl:text>&#9;</xsl:text>
						  <xsl:value-of select="format-number($totalpay, '0.00')"/> 
							<xsl:text>&#9;</xsl:text>
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<xsl:value-of select="'1'"/>
			<xsl:text>&#9;</xsl:text>
  		<xsl:value-of select="substring(AccNo, 1, 20)"/>
			<xsl:text>&#9;</xsl:text>
  		<xsl:value-of select="format-number(PayMoney, '0.00')"/>	
			<xsl:text>&#9;</xsl:text>
  		<xsl:value-of select="substring(PolNo, 1, 30)"/>
			<xsl:text>&#9;</xsl:text>
  		<xsl:choose>
    			<xsl:when test="AccName='null'">
      				<xsl:text>#</xsl:text>
    			</xsl:when>
   				<xsl:otherwise>
     				 <xsl:value-of select="substring(AccName, 1, 30)"/>
   				</xsl:otherwise>
  		</xsl:choose>
			<xsl:text>&#9;</xsl:text>
			<xsl:value-of select="'01'"/>
			<xsl:text>&#9;</xsl:text>
			<xsl:value-of select="'#'"/>
			<xsl:text>&#9;</xsl:text>
			<xsl:value-of select="'#'"/>
			<xsl:text>&#9;</xsl:text>
			<xsl:value-of select="'#'"/>
			<xsl:text>&#9;</xsl:text>
			<xsl:value-of select="'#'"/>		
			<xsl:text>&#9;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>