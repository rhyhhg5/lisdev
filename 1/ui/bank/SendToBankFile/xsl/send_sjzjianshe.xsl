<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gb2312"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
    	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
            <xsl:value-of select="'数量'"/>
             <xsl:value-of select="'&#9;'"/>
             <xsl:value-of select="'保单号'"/>
             <xsl:value-of select="'&#9;'"/> 
             <xsl:value-of select="'银行帐号'"/>
             <xsl:value-of select="'&#9;'"/>
             <xsl:value-of select="'户名'"/>
             <xsl:value-of select="'&#9;'"/>
             <xsl:value-of select="'金额'"/>
				   
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

   
<xsl:for-each select="BANKDATA/ROW">

  <xsl:value-of select="position()"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select="PayCode"/>
  <xsl:value-of select="'&#9;'"/> 
 <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select=" string(PayMoney)"/>

  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>