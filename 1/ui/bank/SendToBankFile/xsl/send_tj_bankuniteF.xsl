<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>模板ID号</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>业务类型</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>协议号</xsl:text>
						<xsl:text>&#13;&#10;</xsl:text>
						<xsl:text>CACP-CM01</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>40501</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>00000000</xsl:text>
						<xsl:text>&#13;&#10;</xsl:text>
						<xsl:text>企业编码</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>业务种类</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>付款人开户行行号</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>付款人银行账号</xsl:text>
						<xsl:text>&#13;&#10;</xsl:text>
						<xsl:text>11001066</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>09900</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>102110001462</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>0302009629100015683</xsl:text>
						<xsl:text>&#13;&#10;</xsl:text>
						<xsl:text>日期</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>序号</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>明细数目</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>金额(单位:分)</xsl:text>
						<xsl:text>&#13;&#10;</xsl:text>
						<xsl:variable name="serialno"  select ="SerialNo"/> 
					  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						 <xsl:variable name="curDate" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
             <xsl:value-of select="concat(substring($curDate,1,4), substring($curDate,6,2), substring($curDate,9,2))"/>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>000001</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:value-of select="$totalpic"/>
						<xsl:text>&#9;</xsl:text>
						<xsl:value-of select="string(format-number($totalpay,'0.00'))"/>							        						                    
						<!-- 回车换行 -->
						<xsl:text>&#13;&#10;</xsl:text>
						<xsl:text>业务号码</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>明细序号</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>收款人开户行行号</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>收款人开户行名称</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>收款人银行账号</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>户名</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>金额(单位:分)</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>企业流水号</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>备注</xsl:text>
						<xsl:text>&#9;</xsl:text>
						<xsl:text>手机号</xsl:text>
						<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	<xsl:for-each select="BANKDATA/ROW">
	    <xsl:value-of select="PayCode"/>
			<xsl:value-of select="'&#9;'"/>
			<xsl:variable name="date"  select ="substring(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2(),3,6)"/> 
			<xsl:variable name="time"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentTime())"/> 
			<xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,$time),3))"/> 
			<xsl:value-of select="$no"/>
			<xsl:value-of select="'&#9;'"/>
			<xsl:value-of select="BankCode"/>
			<xsl:value-of select="'&#9;'"/>	
			<xsl:value-of select="''"/>
			<xsl:value-of select="'&#9;'"/>
			<xsl:value-of select="AccNo"/>
			<xsl:value-of select="'&#9;'"/>	
			<xsl:value-of select="AccName"/>
			<xsl:value-of select="'&#9;'"/>	
  		<xsl:value-of select="string(format-number(PayMoney,'0.00'))"/>	
			<xsl:value-of select="'&#9;'"/>	
			<xsl:value-of select="''"/>
			<xsl:value-of select="'&#9;'"/>
			<xsl:value-of select="''"/>
			<xsl:value-of select="'&#9;'"/>		
  		<xsl:value-of select="''"/>
			<xsl:value-of select="'&#9;'"/>
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>