<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java" version="1.0">

	<xsl:output method="text" encoding="gb2312" />

	<xsl:include href="send_function.xsl" />

	<xsl:template match="/">

		<xsl:variable name="setFirstLine"
			select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()" />

		<xsl:for-each select="BANKDATA/ROW">
			<!-- 机构代码 -->
			<xsl:value-of select="ComCode" />
			<xsl:value-of select="'|'" />
			
			<!-- 标明理赔、满期、退保 -->
			<xsl:value-of select="PolNo" />
			<xsl:value-of select="'|'" />

			<!-- 收款账号 -->
			<xsl:value-of select="AccNo" />
			<xsl:value-of select="'|'" />

			<!-- 收款人姓名 -->
			<xsl:value-of select="AccName" />
			<xsl:value-of select="'|'" />

			<!-- 金额 -->
			<xsl:value-of select="PayMoney" />
			<xsl:value-of select="'|'" />

			<!-- 开户银行及支行 -->
			<xsl:value-of select="'中国建设银行湘江支行营业部'" />
			<xsl:value-of select="'|'" />

			<!-- 给付号码 -->
			<xsl:value-of select="PayCode" />
			
			<xsl:text>&#13;&#10;</xsl:text>
		</xsl:for-each>

	</xsl:template>

</xsl:stylesheet>