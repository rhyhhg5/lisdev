<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

  <!-- 账号，22位-->
  <xsl:value-of select="substring(AccNo, 1, 22)"/>
  <xsl:value-of select="'|'"/>

  <!-- 收据号，15位 -->
  <xsl:value-of select="PayCode"/>
  <xsl:value-of select="'||'"/> 

  <!-- 证件号，18位  -->
  <xsl:value-of select="substring(IDNo, 1, 18)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="AccName"/>
  <xsl:value-of select="'|'"/>
  <!-- 交易金额 -->  
  <xsl:value-of select="PayMoney"/>
  <xsl:value-of select="'|'"/>
   
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>