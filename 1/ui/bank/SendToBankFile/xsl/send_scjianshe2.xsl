<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			<!-- 明细标志--> 
			<xsl:value-of select="position()"/>
      <xsl:value-of select="'|'"/> 
			<!-- 收款人卡号，30位 -->
  		<xsl:value-of select="AccNo"/>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>
			<xsl:value-of select="AccName"/>
				<xsl:value-of select="'|'"/>
	
  		<!-- 金额，12位 -->
  		<xsl:value-of select="PayMoney"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
				
  		<!-- 保单号，30位 -->
  		<xsl:value-of select="PayCode"/>
  		<!-- 分隔符--> 
				<xsl:value-of select="'|'"/>
  	<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>