<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
 <xsl:value-of select="'#总计信息'"/> 
 <xsl:text>&#13;&#10;</xsl:text>
 <xsl:value-of select="'#币种|日期|总计标志|总金额|总笔数|'"/> 
 <xsl:text>&#13;&#10;</xsl:text>
 <xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>RMB</xsl:text>
						<xsl:text>|</xsl:text>
						<xsl:variable name="date"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2())"/> 
						<xsl:value-of select="$date"/>  
						<xsl:text>|</xsl:text>
						<xsl:text>1</xsl:text>
						<xsl:variable name="serialno"  select ="SerialNo"/> 
					  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						<!-- 分隔符--> 
						<xsl:text>|</xsl:text>
				    <!-- 总金额  -->
						<xsl:value-of select="format-number($totalpay*100, '0')"/>		
						<xsl:text>|</xsl:text>							
						<!-- 总笔数 -->
						<xsl:value-of select="$totalpic"/>	  
						<xsl:text>|</xsl:text>	      						                    
						<!-- 回车换行 -->
						<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
 <xsl:value-of select="'#明细信息如下'"/> 
 <xsl:text>&#13;&#10;</xsl:text>
 <xsl:value-of select="'#币种|提交日期|明细标志|顺序号|付方开户行名|付方卡号|缴费编号|付方户名|收款单位开户行名|省份|收款方地区名|收款方地区代码|收款帐号|协议编号|收款单位名称|付款金额|用途|备注|'"/> 
 <xsl:text>&#13;&#10;</xsl:text>
 
<xsl:for-each select="BANKDATA/ROW">
	<xsl:value-of select="'RMB'"/> 
	<xsl:value-of select="'|'"/> 
	<xsl:variable name="date1"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2())"/> 
	<xsl:value-of select="$date1"/> 
	<xsl:value-of select="'|'"/>
	<xsl:value-of select="'2'"/>
	<xsl:value-of select="'|'"/> 
	<xsl:variable name="date"  select ="substring(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2(),3,6)"/> 
	<xsl:variable name="time"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentTime())"/> 
	<xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,$time),1))"/> 
  <!-- 1位序列号 -->
  <xsl:value-of select="$no"/>  
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'南平工行'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'9558801406101152229'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="PayCode"/>
  <xsl:value-of select="'|'"/>
  <!-- 账户名称（姓名） -->
  <xsl:value-of select="AccName"/> 
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'南平分行营业部'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'福建'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'南平'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'1406'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="AccNo"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'BDP7036'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'中国人民健康保险股份有限公司南平中心支公司'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="PayMoney*100"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'保费'"/>
  <xsl:value-of select="'|'"/>

  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>
