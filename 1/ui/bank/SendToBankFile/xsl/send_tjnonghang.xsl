<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
              <xsl:variable name="serialno"  select ="SerialNo"/>
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				      <!-- 总笔数 6位-->
						  <xsl:value-of select="format-number($totalpic, '000000')"/>
						  <!-- 总金额 15位 -->
						  <xsl:value-of select="format-number($totalpay, '000000000000.00')"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose>
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
	<!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 账号，不超过19位-->
  <!-- 账号，不够19位右补空格 -->
  <xsl:value-of select="substring(AccNo, 1, 19)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="19"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
	
  <!-- 账户名称，不够10位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  <xsl:choose>
    <xsl:when test="10 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 10-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 10)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
 	<!-- 代理种类 4位 -->	
 	<xsl:value-of select="'    '"/>
 	<!-- 代理单位客户端号，不够20位后面补空格 -->
  <xsl:value-of select="PayCode"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>	
 	<!-- 处理顺序号 8位 -->
 	<xsl:variable name="detailnumber" select="position()"/>
	<xsl:value-of select="format-number($detailnumber, '00000000')"/>
	<!-- 交易日期 8位-->
	<xsl:value-of select="concat(substring(SendDate,1,4),substring(SendDate,6,2),substring(SendDate,9,2))"/>
	<!-- 证件类型 6位-->
	<xsl:variable name="IDType" select="string(IDType)"/>
  <xsl:choose>
    <xsl:when test="$IDType='0'">
      <xsl:value-of select="'110001'"/>
    </xsl:when>
    <xsl:when test="$IDType='1'">
      <xsl:value-of select="'110023'"/>
    </xsl:when>
    <xsl:when test="$IDType='2'">
      <xsl:value-of select="'110007'"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="'      '"/>
    </xsl:otherwise>
  </xsl:choose>
	<!-- 身份证号码 30字符 -->
  <xsl:value-of select="IDNo"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(IDNo)"/>
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>	
  <!-- 交易金额，12位 -->
  <xsl:value-of select="format-number(PayMoney, '000000000.00')"/>
	<!-- 备注 12位 -->		
	<xsl:value-of select="'            '"/> 
  <!-- 银行返回码 4位 发盘时为空 -->			
	<xsl:value-of select="'    '"/> 				
	<!-- 备注 40位 -->		
	<xsl:value-of select="'                                        '"/> 
	<!-- 回车换行 -->
	<xsl:text>&#13;&#10;</xsl:text>

	</xsl:for-each>
	
</xsl:template>

</xsl:stylesheet>