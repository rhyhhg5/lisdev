<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">
                
<!-- 补充空格（或其它字符）函数，用递归实现 -->
<xsl:template name="supplement">
  <xsl:param name="valueLen"/>
  <xsl:param name="length"/>
  <xsl:param name="key"/>
  
  <xsl:if test="$length > $valueLen">
    <xsl:value-of select="$key"/>
    <xsl:call-template name="supplement">
      <xsl:with-param name="valueLen" select="$valueLen + 1"/>
      <xsl:with-param name="length" select="$length"/>
      <xsl:with-param name="key" select="$key"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>