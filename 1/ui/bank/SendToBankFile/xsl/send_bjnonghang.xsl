<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

	<!-- 账号，不够30位右补空格 -->
  <xsl:value-of select="substring(AccNo, 1,30)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
	
	<!-- 交易金额，18位 -->
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(string(PayMoney))"/>
    <xsl:with-param name="length" select="18"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  
  <xsl:value-of select="PayMoney"/>
	
	<!-- 证件类型-->
	<xsl:value-of select="'      '"/>
	
	<!-- 证件号码-->
	<xsl:value-of select="'                                        '"/>
	
	<!-- 账户名称，不够60位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>

  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="60"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:choose>
    <xsl:when test="30 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 60-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 30)"/>
    </xsl:otherwise>
  </xsl:choose>
	
	<!-- 校验标志-->
	<xsl:value-of select="'2'"/>
	
	<!-- 第三方摘要-->
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(string(position()))"/>
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  
	<xsl:value-of select="substring(position(), 1, 30)"/>
	
  
  <!-- 以上的代码块根据不同的银行而不同 -->
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>