<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="'RMB'"/>
						<xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2()"/>
						<xsl:value-of select="'1'"/>
						<xsl:variable name="serialno"  select ="SerialNo"/> 
					  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 

						<xsl:value-of select="format-number($totalpay*100, '000000000000000000')"/>					
						<xsl:value-of select="format-number($totalpic, '00000')"/>		        						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			<xsl:value-of select="'RMB'"/>
			<xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2()"/>
			<xsl:value-of select="'2'"/>
			<xsl:value-of select="format-number(position(), '00000')"/>	
			<xsl:value-of select="'                                        '"/>
			<xsl:value-of select="'        '"/>
			<xsl:value-of select="'                                                            '"/>
			<xsl:value-of select="'1001184229023102788           '"/>
			<xsl:value-of select="'中国人民健康保险股份有限公司上海分公司                      '"/>
			<xsl:value-of select="substring(BankName,1,60)"/>
	   	<xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length(BankName)"/>
		    <xsl:with-param name="length" select="60"/>
		    <xsl:with-param name="key" select="' '"/>
	   	</xsl:call-template> 
			<!-- 省份名 -->
			<xsl:value-of select="'                    '"/>
			<xsl:value-of select="'                    '"/>
			<xsl:value-of select="'     '"/>
			<xsl:value-of select="substring(AccNo,1,30)"/>
	   	<xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
		    <xsl:with-param name="length" select="30"/>
		    <xsl:with-param name="key" select="' '"/>
	   	</xsl:call-template> 
	   	<xsl:value-of select="substring(AccName,1,60)"/>
	   	<xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length(AccName)"/>
		    <xsl:with-param name="length" select="60"/>
		    <xsl:with-param name="key" select="' '"/>
	   	</xsl:call-template>
			<xsl:value-of select="format-number(PayMoney*100, '00000000000000000')"/>
			<xsl:value-of select="'保费退还'"/>
			<xsl:value-of select="'                                                                      '"/>

	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>