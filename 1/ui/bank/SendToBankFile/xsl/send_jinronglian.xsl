<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
						<!-- 代办银行代号 -->
						  <xsl:value-of select="'01000'"/>
  					<!-- 收付单位代码 -->
  						<xsl:value-of select="'  C128'"/>
  					<!-- 币种 -->
  						<xsl:value-of select="'0'"/>
  					<!-- 收付标志&收付代码-->
  					  <xsl:choose>
    						<xsl:when test="DealType='S'">
      						<xsl:text>1701</xsl:text>
    						</xsl:when>
    						<xsl:otherwise>
      						<xsl:text>0170</xsl:text>
    						</xsl:otherwise>
  						</xsl:choose>
  					<!-- 交易文件名 -->
  						<xsl:value-of select="'00000001'"/>
  					<!-- -->
						<xsl:variable name="serialno"  select ="SerialNo"/> 
					  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						<!-- 总笔数 -->
						<xsl:call-template name="supplement">
    				<xsl:with-param name="valueLen" select="string-length($totalpic)"/>
    				<xsl:with-param name="length" select="8"/>
    				<xsl:with-param name="key" select="'0'"/>
  					</xsl:call-template>
						<xsl:value-of select="$totalpic"/>						
						<!-- 总金额  -->
						<xsl:call-template name="supplement">
    				<xsl:with-param name="valueLen" select="string-length(format-number($totalpay, '0.00'))"/>
    				<xsl:with-param name="length" select="14"/>
    				<xsl:with-param name="key" select="'0'"/>
    				</xsl:call-template>
						<xsl:value-of select="format-number($totalpay, '0.00')"/>
							
							
						<!-- 已划拨户数 -->
						<xsl:value-of select="'        '"/>
						<!-- 已划拨金额 -->
						<xsl:value-of select="'              '"/>
						<!-- 送银行日期 -->
						<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
						<!-- 应划款日期 -->
						<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
						<!-- 划款日期 -->
						<xsl:value-of select="'        '"/>
						<!-- 回车换行 -->
						<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	<xsl:for-each select="BANKDATA/ROW">
			
			<xsl:value-of select="substring(PolNo,1,10)"/>
			<xsl:call-template name="supplement">
    	<xsl:with-param name="valueLen" select="string-length(substring(PolNo,1,10))"/>
    	<xsl:with-param name="length" select="10"/>
    	<xsl:with-param name="key" select="' '"/>
    	</xsl:call-template>
    	<!-- 分行代码 -->
			<xsl:variable name="bankid1" select="BankCode"/>
			<xsl:choose>
				<xsl:when test="$bankid1='0195'"><xsl:text>02</xsl:text></xsl:when>
				<xsl:when test="$bankid1='0495'"><xsl:text>03</xsl:text></xsl:when>
				<xsl:when test="$bankid1='0295'"><xsl:text>04</xsl:text></xsl:when>
				<xsl:when test="$bankid1='0395'"><xsl:text>05</xsl:text></xsl:when>
				<xsl:when test="$bankid1='1095'"><xsl:text>06</xsl:text></xsl:when>
				<xsl:when test="$bankid1='2095'"><xsl:text>07</xsl:text></xsl:when>
				<xsl:when test="$bankid1='1295'"><xsl:text>08</xsl:text></xsl:when>
				<xsl:when test="$bankid1='0695'"><xsl:text>09</xsl:text></xsl:when>
				<xsl:when test="$bankid1='1495'"><xsl:text>10</xsl:text></xsl:when>
				<xsl:when test="$bankid1='1195'"><xsl:text>11</xsl:text></xsl:when>
				<xsl:when test="$bankid1='0595'"><xsl:text>12</xsl:text></xsl:when>
				<xsl:when test="$bankid1='0795'"><xsl:text>15</xsl:text></xsl:when>
				<xsl:when test="$bankid1='2195'"><xsl:text>16</xsl:text></xsl:when>
				<xsl:when test="$bankid1='1395'"><xsl:text>17</xsl:text></xsl:when>
				<xsl:when test="$bankid1='0895'"><xsl:text>18</xsl:text></xsl:when>
				<xsl:when test="$bankid1='0995'"><xsl:text>21</xsl:text></xsl:when>
				<xsl:when test="$bankid1='01'"><xsl:text>31</xsl:text></xsl:when>
				<xsl:when test="$bankid1='0201'"><xsl:text>32</xsl:text></xsl:when>
				<xsl:when test="$bankid1='bb'"><xsl:text>33</xsl:text></xsl:when>
				<xsl:when test="$bankid1='cc'"><xsl:text>47</xsl:text></xsl:when>
				<xsl:when test="$bankid1='2395'"><xsl:text>51</xsl:text></xsl:when>
				<xsl:when test="$bankid1='dd'"><xsl:text>53</xsl:text></xsl:when>
				<xsl:when test="$bankid1='1695'"><xsl:text>91</xsl:text></xsl:when>
			</xsl:choose>			
			
			
			
			<xsl:value-of select="AccNo"/>
			<xsl:call-template name="supplement">
    	<xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    	<xsl:with-param name="length" select="21"/>
    	<xsl:with-param name="key" select="' '"/>
    	</xsl:call-template>
    	
    	<xsl:call-template name="supplement">
    	<xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
    	<xsl:with-param name="length" select="12"/>
    	<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			<xsl:value-of select="PayMoney"/>
			
  		<xsl:value-of select="'X'"/>	
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>