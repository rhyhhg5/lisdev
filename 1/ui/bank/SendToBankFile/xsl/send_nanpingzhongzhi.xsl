<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gb2312"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">
  
  <xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
  <xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
						  <!-- 单位编号 Max:10位-->
							<xsl:value-of select="'健康保险'"/>
  						<xsl:value-of select="'|'"/>
								<xsl:value-of select="'农业银行'"/>
  						<xsl:value-of select="'|'"/>
						  
				  		<xsl:variable name="serialno"  select ="SerialNo"/> 
					                <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				                  <!-- 总笔数 Max:5位-->
						<xsl:value-of select="substring($totalpic,1,5)"/>						  
				 			<xsl:value-of select="'|'"/>
						  <!-- 总金额 16位 -->
				                <xsl:value-of select="substring(string(format-number($totalpay,'0.00')),1,16)"/> 
						
  						<xsl:value-of select="'|'"/>
						
						
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
  </xsl:choose> 
  </xsl:for-each>
  

  <xsl:for-each select="BANKDATA/ROW">
  
  <xsl:value-of select="position()"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(PayCode)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'|'"/>	
  <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(PayMoney)"/>
  <xsl:value-of select="'|'"/>
  <!-- 交易日期，转换成yyyymmdd -->
  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'保险费'"/>
  <xsl:value-of select="'|'"/>


  <!-- 以上的代码块根据不同的银行而不同 -->
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>