<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
	
	<xsl:for-each select="BANKDATA/ROW">
	<!-- 以下的代码块根据不同的银行而不同 -->
			
	<!-- 明细标志，1位--> 			
	<xsl:value-of select="'2'"/>	  
	
	<!-- 代理业务编号(收付标志) -->
  <xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:value-of select="'160750237'"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="'160272405'"/>
    </xsl:otherwise>
  </xsl:choose>
  
  <!-- 代理种类，3位--> 			
	<xsl:value-of select="'502'"/>	  
  
  <!--帐户，19位-->
  <xsl:value-of select="AccNo"/>
  <xsl:call-template name="supplement">
		<xsl:with-param name="valueLen" select="string-length(AccNo)"/>
	  <xsl:with-param name="length" select="19"/>
	  <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  
   
	<!-- 标志位，1位--> 			
	<xsl:value-of select="'0'"/>
	
	<!-- 处理顺序号 不足6位补空格 -->
  <xsl:value-of select="'000000'"/>
  
	<!-- 交易金额，17位， -->
	<xsl:value-of select="format-number(PayMoney*100, '00000000000000000')"/> 
	
	<!-- 应处理笔数，17位， -->
	<xsl:value-of select="'00000000000000001'"/> 
			
  <!-- 借贷标志 -->  
	<xsl:choose>
    <xsl:when test="DealType='S'">
    	<!--代收：借-->
      <xsl:value-of select="'1'"/>
    </xsl:when>
    <xsl:otherwise>
    	<!--代付：贷-->
      <xsl:value-of select="'2'"/>
    </xsl:otherwise>
  </xsl:choose>
	  
  <!-- 摘要，70位 -->
  <xsl:value-of select="'                                                                      '"/> 
  
  <!-- 备用，12位 -->
  <xsl:value-of select="'            '"/> 
  
	<!-- 回车换行 -->
	<xsl:text>&#13;&#10;</xsl:text>
	
	</xsl:for-each>
		
	<!-- 汇总 -->	
		
	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
			<xsl:otherwise>
	
		<!-- 汇总标志，1位--> 			
		<xsl:value-of select="'1'"/>	  
		
		<!-- 代理业务编号(收付标志) -->
	  <xsl:choose>
	    <xsl:when test="DealType='S'">
	      <xsl:value-of select="'160750237'"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="'160272405'"/>
	    </xsl:otherwise>
	  </xsl:choose>
	  
	  <!-- 代理种类，3位--> 			
		<xsl:value-of select="'502'"/>	  
	  
	  <!--帐户，19位-->
	  <xsl:choose>
		  <xsl:when test="DealType='S'">
		      <xsl:value-of select="'1607001529224006074'"/>
		    </xsl:when>
		    <xsl:otherwise>
		      <xsl:value-of select="'1602023319200038465'"/>
		    </xsl:otherwise>
	  </xsl:choose>	  
	   
		<!-- 标志位，1位--> 			
		<xsl:value-of select="'0'"/>
		
		<!-- 处理顺序号 不足6位补空格 -->
	  <xsl:value-of select="'000000'"/>
	  
	  <xsl:variable name="serialno"  select ="SerialNo"/> 
	  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
		<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
	  
	  <!-- 总金额 12位 -->
		<xsl:value-of select="format-number($totalpay*100, '00000000000000000')"/> 
		<!-- 总笔数 6位-->
		<xsl:value-of select="format-number($totalpic, '00000000000000000')"/>
	  
	  <!-- 借贷标志 --> 
		<xsl:choose>
	    <xsl:when test="DealType='S'">
	    	<!--代收：借-->
	      <xsl:value-of select="'2'"/>
	    </xsl:when>
	    <xsl:otherwise>
	    	<!--代付：贷-->
	      <xsl:value-of select="'1'"/>
	    </xsl:otherwise>
	  </xsl:choose>
		
	  <!-- 摘要，70位 -->
	  <xsl:value-of select="'                                                                      '"/> 
	  
	  <!-- 备用，12位 -->
	  <xsl:value-of select="'            '"/> 
		
	  <!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
		
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>