<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
				
							<xsl:value-of select="'3770000654|'"/>  
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
						  <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						  <!-- 总笔数 5位-->
						  <xsl:value-of select="number($totalpic)"/>
						  <xsl:value-of select="'|'"/>
						  <!-- 总金额 18位 -->
              <xsl:value-of select="format-number($totalpay, '0.00')"/> 
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			
  <!-- 账号，不超过30位-->
  <xsl:value-of select="substring(AccNo, 1, 30)"/>
  <xsl:value-of select="'|'"/>
  <!--收费户名，60位-->
  <xsl:value-of select="substring(AccName, 1, 60)"/> 
  <xsl:value-of select="'|'"/>
  <!-- 交易金额，不超过18位， -->
		 <xsl:value-of select="format-number(PayMoney, '0.00')"/> 
		<xsl:value-of select="'|'"/>
		<!--缴费编号，不超过15位-->
  <xsl:value-of select="substring(PolNo, 1, 15)"/>
  <xsl:value-of select="'|'"/> 
   <!--收费户名，60位-->
  <xsl:value-of select="substring(AccName, 1, 60)"/> 
	
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>