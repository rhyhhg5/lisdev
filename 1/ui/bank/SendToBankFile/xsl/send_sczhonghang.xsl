<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

	<!-- 账号，22位 不足后补空格 --> 
	<xsl:variable name="tAccNo" select="string(AccNo)"/>
	<xsl:variable name="ttAccNo" select="substring(AccNo, 1, 22)"/>
	<xsl:choose>
    <xsl:when test="substring($tAccNo,1,6)='601382'">
    	<xsl:variable name="ttAccNo" select="concat('OD',substring($tAccNo,7,16))"/>
      <xsl:value-of select="$ttAccNo"/>
      <xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length($ttAccNo)"/>
		    <xsl:with-param name="length" select="22"/>
		    <xsl:with-param name="key" select="' '"/>
		  </xsl:call-template>
    </xsl:when>
    <xsl:when test="substring($tAccNo,1,6)='456351'">
      <xsl:variable name="ttAccNo" select="concat('OV',substring($tAccNo,7,16))"/>
      <xsl:value-of select="$ttAccNo"/>
      <xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length($ttAccNo)"/>
		    <xsl:with-param name="length" select="22"/>
		    <xsl:with-param name="key" select="' '"/>
		  </xsl:call-template>
    </xsl:when>
    <!-- 多于四个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccNo, 1, 22)"/>
      <xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
		    <xsl:with-param name="length" select="22"/>
		    <xsl:with-param name="key" select="' '"/>
		  </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
  

  <xsl:value-of select="' '"/>
  <!-- 金额，14位 不足后补空格 --> 
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayMoney*100)"/>
    <xsl:with-param name="length" select="15"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
	<xsl:value-of select="substring(PayMoney*100,1,15)"/>  
	<xsl:value-of select="' '"/>

	<!-- 账户名称（姓名），不够19位右补空格 -->
	<xsl:value-of select="' '"/>
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="9 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 19-$accNameLen)"/>
    </xsl:when>
    <!-- 多于四个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 10)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="supplement">
			<xsl:with-param name="valueLen" select="string-length(AccName)+$accNameLen"/>
			<xsl:with-param name="length" select="19"/>
			<xsl:with-param name="key" select="' '"/>
	</xsl:call-template>
  
  <xsl:value-of select="' '"/>
  <xsl:value-of select="'健康'"/>
  <!-- 保单号，不够11位右补空格 -->
 	<xsl:value-of select="substring(PolNo,1,12)"/>
 	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PolNo)"/>
    <xsl:with-param name="length" select="12"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  
   
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>