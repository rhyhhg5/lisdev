<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
				
							<xsl:value-of select="'44^'"/>  
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
						  <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
					    <xsl:variable name="tCurrentDate" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
					    <xsl:value-of select="substring($serialno, 13, 8)"/>
					    <xsl:value-of select="'^'"/>  
						  <!-- 总笔数 5位-->
						  <xsl:value-of select="number($totalpic)"/>
						  <xsl:value-of select="'^'"/> 
						  <!-- 总金额 18位 -->
              <xsl:value-of select="format-number($totalpay, '0.00')"/> 
              <xsl:value-of select="'^'"/> 
              <xsl:value-of select="concat(substring($tCurrentDate,1,4),substring($tCurrentDate,6,2),substring($tCurrentDate,9,2))"/>
              <xsl:value-of select="'^'"/> 
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			<xsl:variable name="serialno"  select ="SerialNo"/> 
	<xsl:value-of select="substring($serialno, 13, 8)"/>
	<xsl:value-of select="'^'"/>
	<xsl:value-of select="position()"/>	
	<xsl:value-of select="'^'"/>
	 <xsl:value-of select="substring(PayCode, 1, 30)"/>	
	 <xsl:value-of select="'^'"/>
	 <xsl:value-of select="substring(PayCode, 1, 8)"/>	
	 <xsl:value-of select="'^'"/>
	 <xsl:value-of select="AccName"/>
	 <xsl:value-of select="'^'"/>
  <!-- 账号，不超过30位-->
  <xsl:value-of select="substring(AccNo, 1, 20)"/>
  <xsl:value-of select="'^'"/>
  <xsl:value-of select="substring(PolNo, 1, 16)"/>	
  <xsl:value-of select="'^'"/>
  <!-- 交易金额，不超过18位， -->
		<xsl:value-of select="format-number(PayMoney, '0.00')"/> 
  <xsl:value-of select="'^'"/> 
  <xsl:value-of select="'^'"/> 
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>