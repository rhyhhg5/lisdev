﻿<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">


<xsl:for-each select="BANKDATA/ROW">
	<xsl:value-of select="PayCode"/>
  <xsl:value-of select="'&#9;'"/> 
  <xsl:value-of select="AccName"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:choose>
    <xsl:when test="string-length(AccNo)=19">
      <xsl:text>1</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>2</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select="AccNo"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select="PayMoney"/>
  

  <!-- 以上的代码块根据不同的银行而不同 -->
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>