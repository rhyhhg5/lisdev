<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java" version="1.0">

 <xsl:output method="text" encoding="gbk"/>

	<xsl:include href="send_function.xsl" />

	<xsl:template match="/">
		<xsl:for-each select="BANKDATA/ROW[position()=1]">

  <xsl:value-of select="'F'"/>
  <xsl:value-of select="','"/> 
  <!-- 单位代码，前2位为空格，后3位由结算中心统一编号：C18 -->
  <xsl:value-of select="'000191400206958'"/>
  <xsl:value-of select="','"/> 
  <!-- 送银行日期，转换成yyyymmdd -->
  <xsl:variable name="curDate" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
  <xsl:value-of select="concat(substring($curDate,1,4), substring($curDate,6,2), substring($curDate,9,2))"/>
  <xsl:value-of select="','"/>
  <!-- 总笔数 6位-->
  <xsl:variable name="serialno"  select ="SerialNo"/> 
	<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
  <xsl:value-of select="format-number($totalpic, '')"/>
  <xsl:value-of select="','"/>
  <!-- 总金额 12位 -->
  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
  <xsl:value-of select="string(format-number($totalpay*100,'#####'))"/>
  <xsl:value-of select="','"/>
  <!-- 业务类型 -->
  <xsl:value-of select="'00600'"/>   
</xsl:for-each>
		
		<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
   <xsl:for-each select="BANKDATA/ROW">
			<!-- 流水号，6位 -->
			<xsl:variable name="date"  select ="substring(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2(),3,6)"/> 
			<xsl:variable name="time"  select ="substring(java:com.sinosoft.lis.pubfun.PubFun.getCurrentTime2(),0,4)"/>
			<xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,$time),6))"/>
			<xsl:value-of select="$no"  />
			<xsl:value-of select="','"/>
			<xsl:value-of select="','"/>
			<!-- 银行代码，8位  -->
			<xsl:value-of select="UniteBankCode"  />
      <xsl:value-of select="','"/>
      <xsl:value-of select="'00'"/>
      <xsl:value-of select="','"/>
			<!-- 账号，不够30位右补空格 -->
      <xsl:value-of select="substring(AccNo, 1,32)"/>
      <xsl:value-of select="','"/>
			
			<!-- 用户姓名10位-->
     <xsl:value-of select="string(AccName)"/> 
     <xsl:value-of select="','"/>
     <xsl:value-of select="','"/>
     <xsl:value-of select="','"/>
     <xsl:value-of select="','"/>
     <xsl:value-of select="'0'"/>
     <xsl:value-of select="','"/>
       <!--扣款金额-->				
      <xsl:value-of select="string(format-number(PayMoney*100,'#####'))"/>
      <xsl:value-of select="','"/>
      <xsl:value-of select="'CNY'"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="string(PayCode)"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="','"/>
		
			<!-- 回车换行 -->
			
			<xsl:text>&#13;&#10;</xsl:text>
		</xsl:for-each>

	</xsl:template>

</xsl:stylesheet>
