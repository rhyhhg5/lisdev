<?xml version="1.0" encoding="gb2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
							  xmlns:java="http://xml.apache.org/xslt/java"
							  exclude-result-prefixes="java">
<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>
	<xsl:template match="/">
	<xsl:for-each select="BANKDATA/ROW">
	<!-- 序列号 -->
	<xsl:value-of select="string(XuLie)"/>
	<xsl:value-of select="'|_|'"/>
	<!-- 账户名 -->
	<xsl:value-of select="AccName"/>
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName)"/>
    <xsl:with-param name="length" select="70"/>
    <xsl:with-param name="key" select="' '"/>
  	</xsl:call-template> 
  	<xsl:value-of select="'|_|'"/>
  	<!-- 账号 -->
  	<xsl:value-of select="AccNo"/>
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  	</xsl:call-template> 
  	<xsl:value-of select="'|_|'"/>
  	<!-- 币种 -->
  	<xsl:value-of select="'01'"/>
  	<xsl:value-of select="'|_|'"/>
  	<xsl:value-of select="'|_|'"/>
  	<!-- 缴费退费金额 -->
  	<xsl:value-of select="format-number(PayMoney, '0.00')"/>
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
    <xsl:with-param name="length" select="18"/>
    <xsl:with-param name="key" select="' '"/>
  	</xsl:call-template> 
  	<xsl:value-of select="'|_|'"/>
  	<!-- 交退费编码 -->
  	<xsl:value-of select="PayCode"/>
  	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  	</xsl:call-template> 
  	<xsl:value-of select="'|_|'"/>
  	<!-- 合约校验标志 -->
  	<xsl:value-of select="'02'"/>
  	<xsl:value-of select="'|_|'"/>
  	<!-- 户名校验标志 -->
  	<xsl:value-of select="'1'"/>
  	<xsl:value-of select="'|_|'"/>
  	<!-- 客户移动电话校验标识 -->
  	<xsl:value-of select="'0'"/>
  	<xsl:value-of select="'|_|'"/>
  	<xsl:value-of select="'|_|'"/>
  	<xsl:value-of select="'|_|'"/>
	<xsl:value-of select="'|_|'"/>  	
  	  <!-- 回车换行 -->
  	<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>