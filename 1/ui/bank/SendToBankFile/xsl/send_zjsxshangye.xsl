<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

	<xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:text>0</xsl:text>
    </xsl:when>
   	<xsl:otherwise>
     	<xsl:text>1</xsl:text>
   	</xsl:otherwise>
  </xsl:choose>

	<xsl:value-of select="'0'"/>

	<xsl:value-of select="'000000000'"/>


	<!-- 账户名称（姓名），不够8位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  <xsl:choose>
    <xsl:when test="4 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 8-$accNameLen)"/>
    </xsl:when>
    <!-- 多于四个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 4)"/>
    </xsl:otherwise>
  </xsl:choose>
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="8"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  
  <!-- 账号，20位 不足后补空格 --> 
  <xsl:value-of select="substring(AccNo,1,20)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <xsl:variable name="tPayMoney" select="substring(string(PayMoney*100),1,9)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length($tPayMoney)"/>
    <xsl:with-param name="length" select="9"/>
    <xsl:with-param name="key" select="'0'"/>
  </xsl:call-template> 
  <xsl:value-of select="$tPayMoney"/>
  
  <xsl:variable name="Today" select="substring(string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2()),3,6)"/>
  <xsl:value-of select="$Today"/>
  
  <xsl:value-of select="substring(PayCode,1,17)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="17"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <xsl:value-of select="'0000000000'"/>
   
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>