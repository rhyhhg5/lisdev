<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
	<!-- 明细行开始 -->
	<xsl:text>&#13;&#10;</xsl:text>
	<xsl:for-each select="BANKDATA/ROW">		
				<!-- 金额 -->		
				<xsl:value-of select="substring(PayMoney,1,13)"/>
				<xsl:value-of select="'|'"/>
				<!-- 付款方帐号 -->
				<xsl:value-of select="'05520313263111139'"/>
				<xsl:value-of select="'|'"/>
				<!-- 收款方帐号 -->
				<xsl:value-of select="substring(AccNo,1,19)"/>
				<xsl:value-of select="'|'"/>
				<!-- 帐户名 -->
				<xsl:value-of select="substring(AccName,1,20)"/>
				<xsl:value-of select="'|'"/>
				<!-- 收款分行名称 -->
				<xsl:value-of select="'上海分行'"/>
				<xsl:value-of select="'|'"/>
				<!-- GRHH收款方城市代码 -->
				<xsl:value-of select="'GRHH00310'"/>
				<xsl:value-of select="'|'"/>
				<!-- 人保公司相关的交易流水号 -->
				<xsl:variable name="date"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2())"/> 
				<xsl:value-of select="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,'shjs'),7))"/>
	 			<!-- 回车换行 -->
				<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>