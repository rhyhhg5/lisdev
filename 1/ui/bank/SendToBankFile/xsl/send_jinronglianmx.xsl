<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">
			
			<xsl:value-of select="substring(PolNo,1,10)"/>
			<xsl:call-template name="supplement">
    	<xsl:with-param name="valueLen" select="string-length(substring(PolNo,1,10))"/>
    	<xsl:with-param name="length" select="10"/>
    	<xsl:with-param name="key" select="' '"/>
    	</xsl:call-template>
    	<!-- 分行代码 行别 -->
			<xsl:value-of select="UniteBankCode"/>
			<!-- 开户行行号 -->
			<xsl:value-of select="'000000000000'"/>
			<!-- 帐号 -->
			<xsl:value-of select="AccNo"/>
			<xsl:call-template name="supplement">
    	<xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    	<xsl:with-param name="length" select="32"/>
    	<xsl:with-param name="key" select="' '"/>
    	</xsl:call-template>
			<!-- 金额 -->
			 <xsl:variable name="totalpay"  select ="PayMoney"/> 
    	<xsl:call-template name="supplement">
    	<xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
    	<xsl:with-param name="length" select="12"/>
    	<xsl:with-param name="key" select="'0'"/>
			</xsl:call-template>
			<xsl:value-of select="format-number($totalpay, '0.00')"/>
			<!-- 扣款标识 -->
			<xsl:value-of select="'X'"/>
			
			<!-- 账户名称，不够60位右补空格 -->
			<xsl:variable name="accNameValue" select="string(AccName)"/>
			
  		<xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
			<xsl:choose>
    		<xsl:when test="30 > $accNameLen">
    		  <xsl:value-of select="substring(AccName, 1, 60-$accNameLen)"/>
    		</xsl:when>
    		<!-- 多于30个汉字的处理 -->
    		<xsl:otherwise>
    		  <xsl:value-of select="substring(AccName, 1, 30)"/>
    		</xsl:otherwise>
  		</xsl:choose>
  		<xsl:call-template name="supplement">
  		  <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
  		  <xsl:with-param name="length" select="60"/>
  		  <xsl:with-param name="key" select="' '"/>
  		</xsl:call-template>
  		
  		<!-- 附言 -->
      <xsl:value-of select="'                                                            '"/>
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>