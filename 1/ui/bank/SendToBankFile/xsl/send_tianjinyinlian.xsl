<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
							<!-- 业务类型号--> 
							<xsl:value-of select="'40502'"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>	
							<!-- 企业编码--> 
							<xsl:value-of select="'11001117'"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!--费项代码--> 
							<xsl:value-of select="'00600'"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>	
							<!-- 批次日期--> 
						    <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 批次号，当天唯一 左补0--> 
							<xsl:value-of select="substring(SerialNo,14,6)"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<xsl:variable name="serialno"  select ="SerialNo"/> 
					        <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				            <!-- 总笔数 6位-->
						    <xsl:value-of select="$totalpic"/>
						    <!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
				            <!-- 总金额 15位? -->
				            <xsl:value-of select="$totalpay*100"/>	
						    <!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 有效天数-->
							<xsl:value-of select="'2'"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 行号-->
							<xsl:value-of select="102110000277"/>	
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 账号-->
							<xsl:value-of select="'0302040629300293225'"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 协议号-->
							<xsl:value-of select="'00000000'"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 附言--> 
							<xsl:value-of select="' '"/>
							<xsl:value-of select="'|'"/>								  																		        						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			
			<!-- 明细序号，16位当天唯一--> 
			<xsl:value-of select="position()"/>
			<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 行号 -->
  		    <xsl:value-of select="' '"/>
			<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<xsl:variable name="paycode"  select ="PayCode"/> 
			<xsl:variable name="Bankname" select="string(java:com.sinosoft.lis.pubfun.BankFileInfo.getBankName($paycode))"/>
			<!-- 行名 -->
  		    <xsl:value-of select="$Bankname"/>
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>			
			<!-- 账号 -->
  		    <xsl:value-of select="AccNo"/>	
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>			
			<!-- 户名 -->
  		    <xsl:value-of select="AccName"/>	
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>	
  		    <!-- 金额-->
  		    <xsl:value-of select="PayMoney*100"/>			
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>	
  		    <!-- 地址 -->
  		    <xsl:value-of select="' '"/>	
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		    <!-- 协议号或服务号 -->
  		    <xsl:value-of select="' '"/>	
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		    <!-- 备注 -->
  		    <xsl:value-of select="' '"/>
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		    <!-- 证件号码-->
  		    <xsl:value-of select="' '"/>
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		    <!--手机号，15位 -->
  		    <xsl:value-of select="' '"/>
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		    <!-- 企业流水号 -->
  		    <xsl:value-of select="PayCode"/>
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		    <!-- 预留备用关键字1，40位 -->
  		    <xsl:value-of select="' '"/>
  		    <!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 预留备用关键字2，40位 -->
  		    <xsl:value-of select="' '"/>
  		    <xsl:value-of select="'|'"/>	
			<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
