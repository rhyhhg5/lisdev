<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gb2312"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">
  
 
  <xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
						  <!-- 单位编号 Max:10位-->
							<xsl:value-of select="'0090'"/>
  						<xsl:value-of select="'|'"/>
								<xsl:value-of select="substring($serialno,16,5)"/>
  						<xsl:value-of select="'|'"/>
								<xsl:value-of select="'1'"/>
  						<xsl:value-of select="'|'"/>
						  
				  		<xsl:variable name="serialno"  select ="SerialNo"/> 
					                <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				                  <!-- 总笔数 Max:5位-->
						<xsl:value-of select="$totalpic"/>						  
				 			<xsl:value-of select="'|'"/>
						  <!-- 总金额 16位 -->
				                <xsl:value-of select="substring(string(format-number($totalpay,'0.00')),1,16)"/> 
							

		
  						<xsl:value-of select="'|'"/>
				
  						<xsl:value-of select="'|'"/>

  						<xsl:value-of select="'|'"/>

  						<xsl:value-of select="'|'"/>

  						<xsl:value-of select="'|'"/>

  						<xsl:value-of select="'|'"/>

  						<xsl:value-of select="'|'"/>
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
  </xsl:choose> 
  </xsl:for-each>
  

  <xsl:for-each select="BANKDATA/ROW">
  
  <xsl:value-of select="position()"/>
  <xsl:value-of select="'|'"/>	
  <xsl:value-of select=" string(AccNo)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select=" string(PayMoney)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'string(PayNo)'"/>
  <xsl:value-of select="'|'"/>	
  <xsl:value-of select=" string(AccName)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'05'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'1'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'N'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  
 
  


  <!-- 以上的代码块根据不同的银行而不同 -->
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>