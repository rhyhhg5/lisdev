﻿<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>

							<xsl:value-of select="'3040'"/>
							<xsl:text>|</xsl:text>
							<xsl:value-of select="'04'"/>
							<xsl:text>|</xsl:text>
							<xsl:text>|</xsl:text> 
							<xsl:text>|</xsl:text> 
							<xsl:text>|</xsl:text> 							
							<xsl:variable name="date"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2())"/> 
						  <xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,'lcyz'),2))"/>
							<xsl:value-of select="$date"/>
							<xsl:text>|</xsl:text>	
					    <xsl:value-of select="substring(DealType, 1, 1)"/>
   				    <xsl:text>|</xsl:text> 
							<xsl:variable name="serialno" select="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						  <xsl:value-of select="format-number(substring($totalpay,1,15), '0.00')"/> 						   				      
							<xsl:text>|</xsl:text> 
						  <xsl:value-of select="format-number($totalpic, '0')"/>
							<xsl:text>|</xsl:text> 
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<xsl:value-of select="substring(SerialNo, 1, 20)"/>
			<xsl:text>|</xsl:text> 
			<xsl:value-of select="substring(PayCode, 1, 20)"/>
			<xsl:text>|</xsl:text> 
  		<xsl:value-of select="substring(AccNo, 1, 30)"/>		
			<xsl:text>|</xsl:text> 
  		<xsl:value-of select="format-number(substring(PayMoney,1,15), '0.00')"/>	
			<xsl:text>|</xsl:text> 
      <xsl:value-of select="substring(PolNo, 1, 30)"/>
			<xsl:text>|</xsl:text> 
			<xsl:value-of select="'110001'"/>			
			<xsl:text>|</xsl:text> 
			<xsl:value-of select="substring(IdNo, 1, 30)"/>
			<xsl:text>|</xsl:text> 
			<xsl:text>|</xsl:text> 
			<xsl:text>|</xsl:text> 
			<xsl:value-of select="substring(PolNo, 1, 30)"/>
			<xsl:text>|</xsl:text> 
				<xsl:value-of select="substring(AccName, 1, 30)"/>
			<xsl:text>|</xsl:text> 
			<xsl:value-of select="'1'"/>		
			<xsl:text>|</xsl:text> 
			<xsl:text>|</xsl:text> 
			<xsl:text>|</xsl:text> 
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>