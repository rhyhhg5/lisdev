<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gb2312"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:value-of select="PayCode"/>
			<xsl:value-of select="','"/>
  		<xsl:value-of select="AccNo"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="PayMoney"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2),PayCode)"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="'RBJK'"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="IDNo"/> 
			<xsl:value-of select="','"/>
			<xsl:value-of select="string(AccName)"/>
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>