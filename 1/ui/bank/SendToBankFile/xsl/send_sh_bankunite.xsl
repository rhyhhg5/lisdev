<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
							<!-- 商户代码，15位--> 
							<xsl:value-of select="'700000000000001'"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>	
							<!-- 批次号，2位当天唯一--> 
							<xsl:value-of select="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2),'shHZ'),2))"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 代收代付标志--> 
							<xsl:value-of select="'0'"/>
							<xsl:value-of select="'|'"/>	
							<!-- 商户处理卡号，32位--> 
							<xsl:value-of select="''"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 委托日期，8位--> 
							<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				      <!-- 总金额 15位 -->
						  <xsl:value-of select="format-number($totalpay, '000000000000.00')"/> 
						  <!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
				      <!-- 总笔数 6位-->
						  <xsl:value-of select="format-number($totalpic, '000000')"/>
						  <!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 预留字段1-->
							<xsl:value-of select="''"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 预留字段2-->
							<xsl:value-of select="''"/>
							<!-- 分隔符--> 
							<xsl:value-of select="'|'"/>		  																		        						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			
			<!-- 明细序号，16位当天唯一--> 
			<xsl:value-of select="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2),'shMX'),16))"/>
			<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 币种，3位 -->
  		<xsl:value-of select="'人民币'"/>	
			<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 金额，15位 -->
  		<xsl:value-of select="format-number(PayMoney, '000000000000.00')"/>	
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>			
			<!-- 客户开户行号 -->
  		<xsl:value-of select="BankCode"/>	
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>			
			<!-- 开户行名称，80位 -->
  		<xsl:value-of select="substring(UniteBankName, 1, 80)"/>	
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>	
  		<!-- 客户开户行卡号 -->
  		<xsl:value-of select="AccNo"/>		
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>	
  		<!-- 客户开户名称 -->
  		<xsl:value-of select="AccName"/>	
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		<!-- 备注，60位 -->
  		<xsl:value-of select="' '"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		<!-- 收款人证件类型，2位 -->
  		<xsl:value-of select="substring(IDType, 1, 2)"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		<!-- 收款人证件号码，20位 -->
  		<xsl:value-of select="substring(IDNo, 1, 20)"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		<!-- 收款人手机号，15位 -->
  		<xsl:value-of select="''"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		<!-- 交易手续费，15位 -->
  		<xsl:value-of select="''"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
  		<!-- 预留备用1，40位 -->
  		<xsl:value-of select="''"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 预留备用2，40位 -->
  		<xsl:value-of select="''"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>
			<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>