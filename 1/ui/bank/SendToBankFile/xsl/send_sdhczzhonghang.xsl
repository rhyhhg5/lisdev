<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
	<!-- 单据号 -->
	<xsl:value-of select="substring(PayCode, 1, 20)"/>		
	<xsl:value-of select="'&#9;'"/>
	<!-- 银行帐号 -->
	<xsl:value-of select="substring(AccNo, 1, 20)"/> 	 
	<xsl:value-of select="'&#9;'"/>
 <!-- 客户名 -->
	<xsl:value-of select="substring(AccName, 1, 8)"/>
	<xsl:value-of select="'&#9;'"/>
	<!-- 交易金额 -->
	<xsl:value-of select="substring(PayMoney, 1, 18)"/> 
	<xsl:value-of select="'&#9;'"/>
  <!-- 成功标记 -->
  <xsl:value-of select="'9999'"/> 
	<xsl:value-of select="'&#9;'"/>
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>