<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
				      <xsl:value-of select="'报销号'"/>
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'单据张数'"/> 
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'付款帐号开户行'"/> 
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'付款帐号'"/> 
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'付款方户名'"/>
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'收款帐号开户行'"/> 
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'收款帐号省份'"/> 
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'收款帐号地市'"/> 
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'收款帐号地区码'"/> 
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'收款帐号'"/>
			        <xsl:value-of select="'&#9;'"/>
			        <xsl:value-of select="'收款方户名'"/>
			        <xsl:value-of select="'&#9;'"/>
			        <xsl:value-of select="'金额'"/>
			        <xsl:value-of select="'&#9;'"/>	
			        <xsl:value-of select="'汇款用途'"/> 
              <xsl:value-of select="'&#9;'"/> 
              <xsl:value-of select="'备注信息'"/>
			        <xsl:value-of select="'&#9;'"/>
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	
	<xsl:for-each select="BANKDATA/ROW">
      <xsl:value-of select="position()"/>
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="'1'"/> 
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="'工行赣支'"/> 
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="'1502211009300087186'"/> 
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="'中国人民健康保险股份有限公司江西分公司'"/>
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="'工行'"/> 
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="'江西省'"/> 
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="'抚州'"/> 
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="'1511'"/> 
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="AccNo"/>
			<xsl:value-of select="'&#9;'"/>
			<xsl:value-of select="AccName"/>
			<xsl:value-of select="'&#9;'"/>
			<xsl:value-of select="PayMoney"/>
			<xsl:value-of select="'&#9;'"/>	
			<xsl:value-of select="'赔款'"/> 
      <xsl:value-of select="'&#9;'"/> 
      <xsl:value-of select="PayCode"/>
			<xsl:value-of select="'&#9;'"/>
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>