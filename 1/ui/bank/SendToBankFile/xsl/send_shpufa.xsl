<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:value-of select="'帐号'"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select="'金额'"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select="'姓名'"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select="'编号'"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select="'摘要'"/>
  <!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->			 
  <!-- 账号，22位-->
  <xsl:value-of select="AccNo"/>
  <xsl:value-of select="'&#9;'"/>

  <!-- 交易金额 -->  
  <xsl:value-of select="PayMoney"/>
  <xsl:value-of select="'&#9;'"/>

  <!-- 姓名 -->  
  <xsl:value-of select="AccName"/> 
  <xsl:value-of select="'&#9;'"/>

  <!--顺序号，5位-->
  <xsl:value-of select="position()"/>
  <xsl:value-of select="'&#9;'"/>

  <!-- 收据号，15位 -->
  <xsl:value-of select="PayCode"/>
  <xsl:value-of select="'&#9;'"/> 

	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>