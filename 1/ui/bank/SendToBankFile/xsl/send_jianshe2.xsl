<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">
  
  <!-- 分卡格式与折格式两种 BankAccNo以110开头的均为折格式 -->
  <xsl:variable name="AccNoType" select="substring(AccNo, 1, 3)"/>  
  <!-- 库标（首续标志），6表示个人投保单号（首期），2、3表示个人保单号（续期），其它归为特殊划帐-->
	<xsl:choose>
		 <xsl:when test="NoType='6'">
		   <xsl:text>0</xsl:text>
		 </xsl:when>
		 <xsl:when test="NoType='2'">
		   <xsl:text>1</xsl:text>
		 </xsl:when>
		 <xsl:when test="NoType='3'">
		   <xsl:text>1</xsl:text>
		 </xsl:when>
		 <xsl:otherwise>
		   <xsl:text>0</xsl:text>
		 </xsl:otherwise>
  </xsl:choose>
 
  <!-- 收付标志 -->
  <xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:text>0</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>1</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  
  <!-- 合同书号，数据库没有，待定 -->
	<xsl:value-of select="'0000000000'"/>
	
	<!-- 账户名称（姓名），不够8位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  <xsl:choose>
    <xsl:when test="4 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 8-$accNameLen)"/>
    </xsl:when>
    <!-- 多于四个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 4)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="8"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <xsl:choose>
  	<!-- 折格式定义部分 -->
  	<xsl:when test="$AccNoType='110'">
   	  <!-- 账号（储蓄存折号），不够19位前面补0 -->
		  <xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
		    <xsl:with-param name="length" select="19"/>
		    <xsl:with-param name="key" select="'0'"/>
		  </xsl:call-template>
		  <xsl:value-of select="substring(AccNo,1,19)"/>
	 	</xsl:when>
  	<!-- 卡格式定义部分 -->
  	<xsl:otherwise>	  		  
		  <!-- 账号（储蓄存折号），不够20位前面补0 -->
		  <xsl:call-template name="supplement">
		    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
		    <xsl:with-param name="length" select="20"/>
		    <xsl:with-param name="key" select="'0'"/>
		  </xsl:call-template>
		  <xsl:value-of select="substring(AccNo,1,20)"/>
  	</xsl:otherwise>
  </xsl:choose>
		
  <!-- 保单号（单据号），不够20位后面补0 -->
  <xsl:value-of select="substring(PolNo, 1, 20)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PolNo)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="'0'"/>
  </xsl:call-template>

  <!-- 经办所所号 -->
  <xsl:value-of select="'18700'"/>
    
  <!-- 交易金额，10位，乘100用来去掉小数点 -->
  <xsl:variable name="tPayMoney" select="string(PayMoney*100)"/>
  <xsl:value-of select="format-number($tPayMoney, '0000000000')"/>
  
  <!-- 交易日期，转换成yyyymmdd -->
  <xsl:value-of select="'00000000'"/>
  
  <!-- 备用项，3位 -->
  <xsl:value-of select="'000'"/>
  
  <!-- 交易状态，发送时全为代交00 -->  
	<xsl:value-of select="'00'"/>
  <!-- 以上的代码块根据不同的银行而不同 -->
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>
  
</xsl:for-each>

</xsl:template>

</xsl:stylesheet>