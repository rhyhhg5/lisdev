<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
<!-- 汇总记录 -->
	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="'F'"/>
						<!-- 日期-->
						<xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2()"/>

						<xsl:variable name="serialno" select="SerialNo"/> 
			      <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						<xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						<!-- 总人数（比数）-->
						  <xsl:call-template name="supplement">
   							<xsl:with-param name="valueLen" select="string-length($totalpic)"/>
    						<xsl:with-param name="length" select="6"/>
    						<xsl:with-param name="key" select="'0'"/>
  						</xsl:call-template>         
					  <xsl:value-of select="$totalpic"/>
						<!-- 总金额-->
						<xsl:call-template name="supplement">
   							<xsl:with-param name="valueLen" select="string-length($totalpay*100)"/>
    						<xsl:with-param name="length" select="12"/>
    						<xsl:with-param name="key" select="'0'"/>
  						</xsl:call-template>     
						<xsl:value-of select="format-number(substring($totalpay*100,1,12), '000')"/> 		        						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	
	
<xsl:for-each select="BANKDATA/ROW">
	<!-- 银行帐号，26位 不足后补空格 左对齐  --> 
  <xsl:value-of select="substring(AccNo,1,20)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="26"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
            
	<!-- 交易金额，12位  含两位小数 左对齐 不够前面补空格 -->
	<xsl:variable name="PayMoney1" select="PayMoney"/>
	 <xsl:call-template name="supplement">
    	<xsl:with-param name="valueLen" select="string-length(string($PayMoney1*100))"/>
    	<xsl:with-param name="length" select="12"/>
    	<xsl:with-param name="key" select="'0'"/>
    </xsl:call-template>  
	 <xsl:value-of select="$PayMoney1*100"/>
	<!-- 收付标志 -->
   <xsl:value-of select="'S'"/>
   <!-- 保单号 22位 右补空格-->
   <xsl:value-of select="PayCode"/>
	 <xsl:call-template name="supplement">
    	<xsl:with-param name="valueLen" select="string-length(string(PayCode))"/>
    	<xsl:with-param name="length" select="22"/>
    	<xsl:with-param name="key" select="' '"/>
    </xsl:call-template>  
    <!-- 账户名-->

   <xsl:variable name="accname" select="AccName"/>
   <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accname)"/>    
   <xsl:value-of select="$accname"/>
    <xsl:call-template name="supplement">
    	<xsl:with-param name="valueLen" select="string-length($accname)+$chinaLen"/>
    	<xsl:with-param name="length" select="20"/>
    	<xsl:with-param name="key" select="' '"/>
    </xsl:call-template> 
    
     
  <!-- 以上的代码块根据不同的银行而不同 -->
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>