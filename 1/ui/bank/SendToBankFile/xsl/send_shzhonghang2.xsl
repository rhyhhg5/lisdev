<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gb2312"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

  <!-- 以下的代码块根据不同的银行而不同 -->
  
  <!-- 账号，不够19位右补空格 -->
  <xsl:value-of select="substring(AccNo, 1, 19)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="19"/>
    <xsl:with-param name="key" select="'0'"/>
  </xsl:call-template>
  
  
  <!-- 交易金额，10位，乘100用来去掉小数点 -->
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(string(PayMoney))"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="'0'"/>
  </xsl:call-template>
  <xsl:value-of select="format-number(PayMoney,'0.00')"/>
   <!-- 货币代码   （人民币01） -->
  <xsl:text>01</xsl:text>
   <!-- 帐户种类   （人民币00） -->
  <xsl:text>00</xsl:text>
  <!-- 保单号，不够12位右补空格 -->
 	<xsl:value-of select="substring(PayCode,1,15)"/>
 	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>