/*
  //创建人：
  //修改人：
  //修改内容：
  //该文件中包含客户端需要处理的函数和事件
*/
var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//根据起始日期进行查询出要该日期范围内的批次号码
function Showquery()
{
  if((fm.FStartDate.value=="")||(fm.FEndDate.value=="")||(fm.HStartDate.value==null)||(fm.HEndDate.value==null))
  {
    alert("请您输入发盘回盘的开始日期和发盘回盘的结束日期！");
    return false;
  }
  
  if(fm.ManageCom.value=="")
  {
    alert("请您输入管理机构！");
    return false;
  }
  var Querysql ="";
  
  if(fm.States.value=="1")
  {
	  Querysql ="select lyr.serialno,"
		  +" lyr.paycode,"
		  +" lyr.comcode,"
		  +" lyb.senddate,"
		  +" lyr.accname,"
		  +" lyr.accno,"
		  +" lyr.paymoney,"
		  +" '成功',"
		  +" (select count(*) from lyreturnfrombank where paycode =lyr.paycode) times,"
		  +" lyb.returndate,"
		  +" ' ' err"
		  +" from lybanklog lyb ,lyreturnfrombank lyr  "
		  +" where lyb.serialno=lyr.serialno and lyr.banksuccflag='00' and lyb.bankcode='7707' and lyr.paycode in (select actugetno from ljaget  where othernotype='24' and paymode='4') and lyb.senddate >='" + fm.FStartDate.value + "' and lyb.senddate<='"+ fm.FEndDate.value + "' and lyb.returndate >='" +  fm.HStartDate.value + "' and lyb.returndate<='" + fm.HEndDate.value + "' and lyb.comcode like '" + fm.ManageCom.value + "%'  order by comcode ,serialno";
	  
  } else if (fm.States.value=="2"){
	  Querysql ="select lyr.serialno,"
		  +" lyr.paycode,"
		  +" lyr.comcode,"
		  +" lyb.senddate,"
		  +" lyr.accname,"
		  +" lyr.accno,"
		  +" lyr.paymoney,"
		  +" '失败',"
		  +" (select count(*) from lyreturnfrombank where paycode =lyr.paycode) times,"
		  +" lyb.returndate,"
		  +" (select codename from ldcode1 where codetype='bankerror' and code='7707' and code1 = lyr.banksuccflag) err"
		  +" from lybanklog lyb ,lyreturnfrombank lyr  "
		  +" where lyb.serialno=lyr.serialno and lyr.banksuccflag<>'00' and  not exists (select 1 from lyreturnfrombank where banksuccflag='00' and paycode=lyr.paycode) and lyb.bankcode='7707' and lyr.paycode in (select actugetno from ljaget  where othernotype='24' and paymode='4') and lyb.senddate >='" + fm.FStartDate.value + "' and lyb.senddate<='"+ fm.FEndDate.value + "' and lyb.returndate >='" +  fm.HStartDate.value + "' and lyb.returndate<='" + fm.HEndDate.value + "' and lyb.comcode like '" + fm.ManageCom.value + "%'  order by comcode,serialno";
  } else if (fm.States.value=="3"){
	  Querysql ="select lys.serialno,"
		  +" lys.paycode,"
		  +" lys.comcode,"
		  +" lyb.senddate,"
		  +" lys.accname,"
		  +" lys.accno,"
		  +" lys.paymoney,"
		  +" '处理中',"
		  +" (select count(*) from lyreturnfrombank where paycode=lys.paycode) times,"
		  +" lyb.returndate ,"
		  +" '' err"
		  +" from lybanklog lyb ,lysendtobank lys  "
		  +" where lyb.serialno=lys.serialno and lyb.bankcode='7707' and lys.paycode in (select actugetno from ljaget  where othernotype='24' and paymode='4' and bankonthewayflag='1' and cansendbank='9') and lyb.senddate >='" + fm.FStartDate.value + "' and lyb.senddate<='"+ fm.FEndDate.value + "' and lys.comcode like '" + fm.ManageCom.value + "%'  order by comcode ,serialno";
	  
  } else {
	  
	  Querysql ="select lyr.serialno,"
		  +" lyr.paycode,"
		  +" lyr.comcode,"
		  +" lyb.senddate,"
		  +" lyr.accname,"
		  +" lyr.accno,"
		  +" lyr.paymoney,"
		  +" '成功',"
		  +" (select count(*) from lyreturnfrombank where paycode =lyr.paycode) times,"
		  +" lyb.returndate,"
		  +" ' ' err"
		  +" from lybanklog lyb ,lyreturnfrombank lyr  "
		  +" where lyb.serialno=lyr.serialno and lyr.banksuccflag='00' and lyb.bankcode='7707' and lyr.paycode in (select actugetno from ljaget  where othernotype='24' and paymode='4') and lyb.senddate >='" + fm.FStartDate.value + "' and lyb.senddate<='"+ fm.FEndDate.value + "' and lyb.returndate >='" +  fm.HStartDate.value + "' and lyb.returndate<='" + fm.HEndDate.value + "' and lyb.comcode like '" + fm.ManageCom.value + "%'"
	      +" union all "
	      +" select lyr.serialno,"
		  +" lyr.paycode,"
		  +" lyr.comcode,"
		  +" lyb.senddate,"
		  +" lyr.accname,"
		  +" lyr.accno,"
		  +" lyr.paymoney,"
		  +" '失败',"
		  +" (select count(*) from lyreturnfrombank where paycode =lyr.paycode) times,"
		  +" lyb.returndate,"
		  +" (select codename from ldcode1 where codetype='bankerror' and code='7707' and code1 = lyr.banksuccflag) err"
		  +" from lybanklog lyb ,lyreturnfrombank lyr  "
		  +" where lyb.serialno=lyr.serialno and lyr.banksuccflag<>'00' and  not exists (select 1 from lyreturnfrombank where banksuccflag='00' and paycode=lyr.paycode) and lyb.bankcode='7707' and lyr.paycode in (select actugetno from ljaget  where othernotype='24' and paymode='4') and lyb.senddate >='" + fm.FStartDate.value + "' and lyb.senddate<='"+ fm.FEndDate.value + "' and lyb.returndate >='" +  fm.HStartDate.value + "' and lyb.returndate<='" + fm.HEndDate.value + "' and lyb.comcode like '" + fm.ManageCom.value + "%'"
	      +" union all "
	      +" select lys.serialno,"
		  +" lys.paycode,"
		  +" lys.comcode,"
		  +" lyb.senddate,"
		  +" lys.accname,"
		  +" lys.accno,"
		  +" lys.paymoney,"
		  +" '处理中',"
		  +" (select count(*) from lyreturnfrombank where paycode=lys.paycode) times,"
		  +" lyb.returndate ,"
		  +" '' err"
		  +" from lybanklog lyb ,lysendtobank lys  "
		  +" where lyb.serialno=lys.serialno and lyb.bankcode='7707' and lys.paycode in (select actugetno from ljaget  where othernotype='24' and paymode='4' and bankonthewayflag='1' and cansendbank='9') and lyb.senddate >='" + fm.FStartDate.value + "' and lyb.senddate<='"+ fm.FEndDate.value + "' and lys.comcode like '" + fm.ManageCom.value + "%' order by comcode ,serialno,paycode ";
	      
  }
  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  var tempQuery = turnPage2.queryModal(Querysql,BatchSendListGrid);
  if(tempQuery==false){
    showInfo.close();
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "没有符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    return false;
  }else{
	  fm.sql.value = Querysql;
  }
  showInfo.close();
}

function download(){
    if(BatchSendListGrid.mulLineCount == 0 || BatchSendListGrid.mulLineCount == null)
  {
    alert("没有需要下载的数据，请先查询！");
    return false;
  }
	  submitForm();
}

function submitForm() {
	  //检验录入框
	    if (!verifyInput()){
	        return false;
	    }
	    fm.submit(); //提交
	}

