//PayReturnFromBankInput.js该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;
var mDebug="0";
var tFees;
//提交，保存按钮对应操作
function submitForm() {
  if(verifyInput() == false) return false;
  
  var accsql = "select 1 from ldfinbank where bankcode ='" + fm.getbankcode.value + "' and bankaccno='" + fm.getbankaccno.value +"'";
      var arrBankResult = easyExecSql(accsql);
      if(arrBankResult==null){
        alert("归集账户不存在，请确认录入的归集账户");
        return false ;
      }
      
  if (BankGrid.getSelNo()) { 
    fm.all("serialNo").value = BankGrid.getRowColData(BankGrid.getSelNo()-1 , 1);
    
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}

function initQuery() {
  var strSql = "select SerialNo from LYBankLog where DealState is null and ComCode like '" + comCode + "%%' and LogType='F'";
  
  turnPage.queryModal(strSql, BankGrid);
} 
function afterCodeSelect(cCodeName, Field) {
	if(cCodeName == "getbankcode") {
	  fm.getbankaccno.value='';
	}
	
}


//退回文件返回 按钮
function backForm() {
	
	if (BankGrid.getSelNo()) { 
		functionback();
		var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	    fm.action = "./GetReturnFromBankBackSave.jsp";
		fm.submit();
	} else {
		alert("请先选择一条批次号信息！");  
	}
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		window.focus();
		window.location.reload();
	}
}

//选中MulLine行
function functionback () //参数名可以是任意的，不限于parm1和parm2 
{ 
	var i=BankGrid.getSelNo();
	var value=BankGrid.getRowColData(i-1,1);
	fm.serialNo.value=value;
}