<%
//程序名称：GetAdvanceBatchSave.jsp
//程序功能：
//创建日期：2009-06-19 09:25:18
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%
    String FlagStr = "";
    String Content = "";
    String type = request.getParameter("type");
    
    try{
    if(type.equals("1")){
        System.out.println("-----生 成 数 据-----");
        BatchSendDataBaoRong tBatchSendDataBaoRong = new BatchSendDataBaoRong();
        FlagStr = "";
        Content = "批处理程序触发成功";
        tBatchSendDataBaoRong.run();
    } else if(type.equals("2")){
        System.out.println("-----代 收 发 盘-----");
        BatchSendBaoRongTrans tBatchSendBaoRongTrans = new BatchSendBaoRongTrans();
        tBatchSendBaoRongTrans.run();
        FlagStr = "";
        Content = "批处理程序触发成功";
    } else if(type.equals("3")){
        System.out.println("-----代付发盘-----");
        BatchSendBaoRongTransF tBatchSendBaoRongTransF = new BatchSendBaoRongTransF();
        tBatchSendBaoRongTransF.run();
        FlagStr = "";
        Content = "批处理程序触发成功";
    } else if(type.equals("4")){
        System.out.println("-----集中代收付回盘-----");
        BatchReturnBaoRongTrans tBatchReturnBaoRongTrans = new BatchReturnBaoRongTrans();
        tBatchReturnBaoRongTrans.run();
        FlagStr = "";
        Content = "批处理程序触发成功";
    }
    } catch (Exception ex){
        ex.printStackTrace();
        FlagStr = "Fail";
        Content = "批处理程序运行失败";
    }
%>                      
<html>
<script language="javascript">

parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>

