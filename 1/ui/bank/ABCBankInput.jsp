<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<html> 
<%
//程序名称：PaySendToBankInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>   
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="ABCBankInput.js"></SCRIPT>
  
  <title>银行代付 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  var comCode = "<%=tGlobalInput.ComCode%>";
  var cDate = "<%=PubFun.getCurrentDate()%>";
</script>

<body  onload="" >
  <form action="./ABCBankSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 银行代付 fraSubmit-->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center >请选择银行</td>
  		</tr>
  	</table>
  	
    <table  class= common>
    <TR  class= common>
 <TD  class= title>
        银行代码
      </TD>
<td class=input><Input class=codeno name=BankCode CodeData="0|^7708|农行^7709|建行" ondblClick="showCodeListEx('SXFlag1',[this,SXFlagName],[0,1]);" onkeyup="showCodeListKeyEx('SXFlag1',[this,SXFlagName],[0,1]);" verify="银行代码|notnull"><input class=codename name=SXFlagName readonly=true></td>

    </TR>
    </table>
    
    <br>
    
    <INPUT VALUE="代收发送银行" class= cssButton TYPE=button name=sub onclick="submitFormS()">
    <INPUT VALUE="代付发送银行" class= cssButton TYPE=button name=sub1 onclick="submitForm1()">
    <INPUT class= input  name=flag style="display: none;">
    <INPUT VALUE="代收付回盘查询" class= cssButton TYPE=button name=sub2 onclick="submitForm2()">
<!--     <INPUT VALUE="代付回盘查询" class= cssButton TYPE=button name=sub3 onclick="submitForm3()">
 -->    
    <br><br><br><br><hr>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
