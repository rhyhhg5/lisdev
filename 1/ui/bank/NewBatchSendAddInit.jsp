<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>
function initForm()
{
  try
  {
    fm.all('ManageCom').value = <%=tGlobalInput.ManageCom%>;
    initBankGrid();
  }
  catch(re)
  {
    alter("在BatchSendAddInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initBankGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="银联编码";   		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            		//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="银联名称";		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=200;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="银行编码";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            	        //列最大值
      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="银行名称";         		//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=200;            	        //列最大值
      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="所属银行名称";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="所属银行";              //列名
      iArray[6][1]="0px";                 //列宽
      iArray[6][2]=200;                     //列最大值
      iArray[6][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="银行代码";         //列名
      iArray[7][1]="0px";                 //列宽
      iArray[7][2]=200;                   //列最大值
      iArray[7][3]=0;                     //是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="代收付支持方式";         //列名
      iArray[8][1]="0px";                 //列宽
      iArray[8][2]=200;                   //列最大值
      iArray[8][3]=0;                     //是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="代收付支持";         //列名
      iArray[9][1]="100px";                 //列宽
      iArray[9][2]=200;                   //列最大值
      iArray[9][3]=0;                     //是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="代收付支持";         //列名
      iArray[10][1]="0px";                 //列宽
      iArray[10][2]=200;                   //列最大值
      iArray[10][3]=0;                     //是否允许输入,1表示允许，0表示不允许

      BankGrid = new MulLineEnter( "fm" , "BankGrid" ); 
      //这些属性必须在loadMulLine前
      BankGrid.mulLineCount = 0;   
      BankGrid.displayTitle = 1;
      BankGrid.hiddenPlus = 1;
      BankGrid.hiddenSubtraction = 1;
      BankGrid.canSel = 1;
      BankGrid.loadMulLine(iArray);  
      BankGrid.selBoxEventFuncName = "UpdateBank";
      }
      catch(ex)
      {
        alert(ex);
      }
}

 </script>