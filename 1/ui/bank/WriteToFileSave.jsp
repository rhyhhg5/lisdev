<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：WriteToFileSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%> 
  <%@page import="com.sinosoft.lis.easyscan.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="java.io.*"%>
  <%@page import="java.util.*"%>


<%
  System.out.println("\n\n---WriteToFileSave Start---");
  
  //String Allfilename = request.getParameter("downfilesum");
  String Bankcode1= "";
  String Allfilename = "";
  String serverUrl = request.getParameter("Url");
  String DownloadPath = application.getRealPath("bank/SendToBankFile/");
  //  String DownloadPath = application.getRealPath("bank/SendToBankFile/")+"/";
  System.out.println("第一次的下载路径"+DownloadPath);
  
  String DownloadSum  = "";
  String DownloadList = "";
  String zipFilename ="";
  String DealType=request.getParameter("DealType");
  System.out.println("DealType : "+ DealType);
  WriteToFileUI writeToFileUI1 = new WriteToFileUI();
  
  LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
  tLYSendToBankSchema.setSerialNo(request.getParameter("serialNo"));
  System.out.println("serialNo : "+ request.getParameter("serialNo"));
  String fileNameSql = "select Outfile from LYBankLog where serialNo ='"+request.getParameter("serialNo")+"'";
  
  //允许前台录入文件名，现在暂时取消
  TransferData transferData1 = new TransferData();
  //transferData1.setNameAndValue("fileName", request.getParameter("FileName"));
  transferData1.setNameAndValue("DealType", DealType);
  //System.out.println("FileName : "+ request.getParameter("FileName"));
  
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
	VData inVData = new VData();
  
  String Content = "";
  String FlagStr = "";
  int flag = -1;
	
	String action = request.getParameter("fmtransact");/*下载或生成文件标记*/
  System.out.println(action);
	
	if (action.equals("download"))
	{
		flag = 0;
	}
	else
  {
  	flag = 1;
	  
	  inVData.add(tLYSendToBankSchema);
	  inVData.add(transferData1);
	  inVData.add(tGlobalInput);
	  //生成文件
	  if (!writeToFileUI1.submitData(inVData, "WRITE")) {
	    VData rVData = writeToFileUI1.getResult();
	    Content = " 处理失败，原因是:" + (String)rVData.get(0);
	  	FlagStr = "Fail";
	  }
	  else {
	    Content = " 处理成功! ";
	  	FlagStr = "Success";
	  }
	
		System.out.println(Content + "\n" + FlagStr + "\n---WriteToFileSave End---\n\n");
	}
/*银联zip包下载页面*/	
/*取生成的输出文件名*/	
  ExeSQL tExeSQL = new ExeSQL();
  Allfilename = tExeSQL.getOneValue(fileNameSql);
  System.out.println("下载的文件名"+Allfilename);
  
  List mChecksValuesPath = new ArrayList();
  List mChecksValues = new ArrayList();  
  
  
  
  String bankcodeSql = "select bankcode from LYBankLog where serialNo ='"+request.getParameter("serialNo")+"'";
  ExeSQL tExeSQL2 = new ExeSQL();
  Bankcode1 = tExeSQL2.getOneValue(bankcodeSql);
 if("9995".equals(Bankcode1) || "9941".equals(Bankcode1)){//河南银联和深圳二次开发下载文件
	    System.out.println("开始下载9995或者9941");
		DownloadList = Allfilename;
		System.out.println("DownloadList : " + DownloadList);
		if("9995".equals(Bankcode1)){
			zipFilename = DownloadList.substring(0,DownloadList.lastIndexOf("["));
		}else {
			zipFilename = DownloadList.substring(0,DownloadList.lastIndexOf("."));
		}
		
		String tChecksValues[] = {DownloadList};	
		
		if (!(DownloadList.equals("")||DownloadList == null))
		  {
		      for(int i = 0; i < 1; i++ )
		      {
		        System.out.println(DownloadPath+tChecksValues[i]);
		      	mChecksValuesPath.add(DownloadPath+tChecksValues[i]);
		      	mChecksValues.add(tChecksValues[i]);
		      }
		      String[] cChecksValuesPath=(String[]) mChecksValuesPath.toArray(new String[0]);
		      String[] cChecksValues=(String[]) mChecksValues.toArray(new String[0]);
		      System.out.println("cChecksValuesPath.length is "+cChecksValuesPath.length);
		      /*压缩文件，并下载*/
		      if(cChecksValuesPath.length>0&&cChecksValues.length>0)
		      {
		          ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
		          String zipfilepath = DownloadPath+zipFilename+".zip";
		          if(!tProposalDownloadBL.CreatZipFile(cChecksValuesPath,cChecksValues,zipfilepath))
		          {
		          System.out.println("生成压缩文件失败!");
		          }
		          else
		  	      {
			          FlagStr="Succ";
		            Content="生成文件成功";
		            response.sendRedirect(serverUrl+zipFilename+".zip");
			        }
			
		       }
		       else
		       {
			        System.out.println("没有选中的文件");
			        FlagStr="Fail";
		          Content="没有选中的文件";
			     }
			
		  }
	 
 } else {
	/*如果是银联，将Outfile拆分，赋值*/
  if (Allfilename.indexOf(",")!= -1)
  {
		DownloadSum = Allfilename.substring(0,Allfilename.lastIndexOf(","));
		DownloadList = Allfilename.substring(Allfilename.lastIndexOf(",")+1);
		System.out.println("DownloadSum : " + DownloadSum);
		System.out.println("DownloadList : " + DownloadList);
		zipFilename = DownloadSum.substring(0,DownloadSum.lastIndexOf("["));
  }
  String tChecksValues[] = {DownloadSum,DownloadList};	
 /*判断是否是银联数据，如果是打包成zip包*/
  if (!(DownloadList.equals("")||DownloadList == null))
  {
      for(int i = 0; i < 2; i++ )
      {
        System.out.println(DownloadPath+tChecksValues[i]);
      	mChecksValuesPath.add(DownloadPath+tChecksValues[i]);
      	mChecksValues.add(tChecksValues[i]);
      }
      String[] cChecksValuesPath=(String[]) mChecksValuesPath.toArray(new String[0]);
      String[] cChecksValues=(String[]) mChecksValues.toArray(new String[0]);
      System.out.println("cChecksValuesPath.length is "+cChecksValuesPath.length);
      /*压缩文件，并下载*/
      if(cChecksValuesPath.length>0&&cChecksValues.length>0)
      {
          ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
          String zipfilepath = DownloadPath+zipFilename+".zip";
          if(!tProposalDownloadBL.CreatZipFile(cChecksValuesPath,cChecksValues,zipfilepath))
          {
          System.out.println("生成压缩文件失败!");
          }
          else
  	      {
	          FlagStr="Succ";
            Content="生成文件成功";
            response.sendRedirect(serverUrl+zipFilename+".zip");
	        }
	
       }
       else
       {
	        System.out.println("没有选中的文件");
	        FlagStr="Fail";
          Content="没有选中的文件";
	     }
	
  }
 
  else
	{ 
	   
/*普通下载txt文件*/ 
    %>                      
    <html>
    <script language="javascript">
    	if ("<%=FlagStr%>" != "Fail")
    	{
    		parent.fraInterface.downAfterSubmit('<%=serverUrl%>',<%=flag%>);
    	}
    	if (<%=flag%> == 1)
    	{
    		parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
    	}
    	
    </script>
    </html>
<%
   }	}
%>