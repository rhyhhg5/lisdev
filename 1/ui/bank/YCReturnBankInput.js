
var showInfo;
var turnPage = new turnPageClass();

/*  @wdd    
 * type为: 
 * 	1：邮储代收 		 DS	
 *  2: 邮储代付  		 DF
 *  3：邮储代收合计        DSC
 *  4：邮储代付合计	 DFC
*/
function Print(type)
{
    if(!verifyInput()){
        return false;
    }
    if(!verifyTime()){
    	return false;
    }
    if(type == 1){
    	fm.type.value="DS";
        fm.action ="YCReturnBankList.jsp";
        fm.submit();
    } else if (type == 2){
    	fm.type.value="DF";
        fm.action ="YCReturnBankList.jsp";
        fm.submit();
    } else if (type == 3){
    	fm.type.value="DSC";
        fm.action ="YCReturnBankList.jsp";
        fm.submit();
    }else if (type == 4){
    	
    	fm.type.value="DFC";
        fm.action ="YCReturnBankList.jsp";
        fm.submit();
    }
}
function verifyTime(){
	var date = changeDate(getNowDate());
    var flag = true;
	var errorMessage = "";
	var startDate = changeDate(fm.all("StartDate").value);
	var endDate = changeDate(fm.all("endDate").value);
	if(startDate>endDate||endDate>date){
		errorMessage = errorMessage + "时间输入不合理！\n";
		alert(errorMessage);
		flag =  false;
	}
	return flag;
}

function changeDate(str){
	var month = str.substring(5,str.lastIndexOf ("-"));
	var day = str.substring(str.length,str.lastIndexOf ("-")+1);
	var year = str.substring(0,str.indexOf ("-"));
	return Date.parse(month+"/"+day+"/"+year);


}
function beforeSubmit()
{

}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


