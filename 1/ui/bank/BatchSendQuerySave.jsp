<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BatchSendQueryInput.jsp
//程序功能：清单下载
//创建日期：2012-2
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "批次状态查询清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  String querySql = request.getParameter("sql");
  
  ExeSQL tExeSQL = new ExeSQL();
  SSRS tSSRS = tExeSQL.execSQL(querySql.replaceAll("%25","%"));
  System.out.println("-----sql------" + querySql.replaceAll("%25","%"));
  System.out.println("----" + tSSRS.getMaxRow());
  
  String manageCom = request.getParameter("ManageCom");
  String sql = "select name from ldcom where comcode='" + manageCom + "' ";
  SSRS mSSRS = tExeSQL.execSQL(sql);
  
  
  String[][] mToExcel = new String[4000][30];
        mToExcel[0][0] = "集中代收付批次状态查询";
        mToExcel[1][0] = "机构:" + mSSRS.GetText(1,1);
        mToExcel[2][0] = "起始日期：" + request.getParameter("StartDate");
        mToExcel[2][3] = "终止日期：" + request.getParameter("EndDate");
        mToExcel[3][0] = "批次号";
        mToExcel[3][1] = "第三方银行";
        mToExcel[3][2] = "收费类型";
        mToExcel[3][3] = "发盘状态";
        mToExcel[3][4] = "发盘时间";
        mToExcel[3][5] = "回盘状态";
        mToExcel[3][6] = "回盘时间";
        mToExcel[3][7] = "发盘金额";
        mToExcel[3][8] = "发盘笔数";
        mToExcel[3][9] = "成功金额";
        mToExcel[3][10] = "成功笔数";
        mToExcel[3][11] = "失败金额";
        mToExcel[3][12] = "失败笔数";
        
        
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol() - 2; col++)
            {
                mToExcel[row + 3][col - 1] = tSSRS.GetText(row, col);
            }
        }        
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }
  //返回客户端
  if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
  out.clear();
    out = pageContext.pushBody();
%>