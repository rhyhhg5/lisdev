<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>
  <xsl:for-each select="BANKDATA/ROW">
  <ROW>
    <xsl:variable name="accNameValue" select="substring(COLUMN, 104, 120)"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '120')"/>
    <!-- 以下的代码块根据不同的银行而不同 --> 
    <!-- 行别，银行编码 -->   
    <BankCode type="text"> 
      <xsl:value-of select="substring(COLUMN, 57, 2)"/>
    </BankCode>  
    <!-- 账号，32位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 73, 32)"/>
    </AccNo>
    <!-- 交易金额，10位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="number(substring(COLUMN, 41, 16))"/>
    </PayMoney>
    <!-- 交易状态 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 345-$chinaLen, 8)"/>
    </BankSuccFlag>
     <!-- Paycode -->
    <PayCode type="text">
      <xsl:value-of select="substring(COLUMN, 353-$chinaLen, 100)"/>
    </PayCode>
    <!-- 以上的代码块根据不同的银行而不同 -->
  </ROW>
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

