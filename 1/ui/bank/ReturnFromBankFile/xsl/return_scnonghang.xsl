<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">
<BANKDATA>
    
<xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 --> 
  <ROW>
  
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring-after(COLUMN,substring(COLUMN,1,20))"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue,'32')"/>
    <!-- 账户名称（姓名）32位 -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN,21,32-$chinaLen)"/>
    </AccName>
    <!-- 账号，最30位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN,53-$chinaLen,30)"/>
    </AccNo>
    <!-- 交易金额，18位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN,83-$chinaLen,18)"/>
    </PayMoney> 
   <!-- 业务类型摘要 -->
   	<xsl:variable name="a" select="substring-after(COLUMN,substring(COLUMN,1,100-$chinaLen))"/>
    <xsl:variable name="b" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($a,'30')"/>
    
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="substring(COLUMN,(131-$chinaLen)-$b,8)"/>
    </BankDealDate>  
    <!-- 成功标记  -->
    
    <BankSuccFlag type="text">
			<xsl:value-of select="substring(COLUMN,(139-$chinaLen)-$b,4)"/>
		</BankSuccFlag>   
    
    
    </ROW>
    <!-- 以上的代码块根据不同的银行而不同 -->
 </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

