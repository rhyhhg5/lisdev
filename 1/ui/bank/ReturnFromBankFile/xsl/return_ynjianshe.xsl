<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 备注，最大16位，这里为15位收据号 -->          
    <!--PayCode type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/> 
    </PayCode-->
    <!-- 账号，最大16位 -->        
    <AccNo type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[8]"/>
    </AccNo>
    <!-- 账户名称，最大20位 -->    
    <AccName type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[9]"/> 
    </AccName>
    <!--交易时间-->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>
    <!-- 交易状态 -->
    <xsl:variable name="flagStr"  select="xalan:tokenize(COLUMN, '|')[12]"/>      <BankSuccFlag type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[12]"/>
    </BankSuccFlag>
    <!-- 交易金额，整数部分最多13位，小数部分为2位 -->
    <xsl:variable name="payMoneyStr" select="xalan:tokenize(COLUMN, '|')[10]"/>
    <PayMoney type="number">
      <xsl:value-of select="number($payMoneyStr) div 100"/>
    </PayMoney> 
    <!--PayMoney type="text"> 
      <xsl:value-of select="$payMoneyStrTwo"/> 
    </PayMoney-->
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

