<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">
<BANKDATA>
    
<xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 --> 
  <ROW>
  
  	<!-- 收据号 -->   
    <PayCode type="text"> 
      <xsl:value-of select="substring(COLUMN, 1, 15)"/>
    </PayCode>
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring-after(COLUMN,substring(COLUMN,1,55))"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue,'10')"/>
    <!-- 账户名称（姓名）10位 -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN,56,10-$chinaLen)"/>
    </AccName>
    <!-- 交易金额，10位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN,67-$chinaLen,10)"/>
    </PayMoney> 
    
    <!-- 账号，最19位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN,78-$chinaLen,19)"/>
    </AccNo>
    
    <!-- 成功标记  -->    
    <BankSuccFlag type="text">
			<xsl:value-of select="substring(COLUMN,97-$chinaLen,2)"/>
		</BankSuccFlag>   
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>
    
    </ROW>
    <!-- 以上的代码块根据不同的银行而不同 -->
 </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

