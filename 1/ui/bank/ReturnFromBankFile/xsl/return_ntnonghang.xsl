<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">
  
  <row>    
    <!-- 以下的代码块根据不同的银行而不同 帐号，帐户名，金额，paycode -->
    <!-- 帐号/卡号	char（30）	左对齐 右空格-->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN,26,30)"/>
    </AccNo>

    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN,56,15) div 100"/>
    </PayMoney> 
    
    <!-- 银行回写标志		--> 
    <BankSuccFlag type="text">
			<xsl:value-of select="substring(COLUMN,71,6)"/>
		</BankSuccFlag>  
    <!-- 账户名称（姓名）20位 左对齐 右空格 -->
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring(COLUMN, 97,20)"/>
		<xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '20')"/> 

    <!-- 账户名称（姓名） -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 97, 20-$chinaLen)"/>
    </AccName>

    <!-- 交易日期	Char8	YYYYMMDD-->
    <BankDealDate type="text">
      <xsl:value-of select="substring(COLUMN,117-$chinaLen,8)"/>
    </BankDealDate>    

    <!--paycode-->
    <PayCode type="text">
      <xsl:value-of select="substring(COLUMN, 127-$chinaLen, 20)"/>
    </PayCode>
       
  <!-- 以上的代码块根据不同的银行而不同 -->
     </row>
  
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>




