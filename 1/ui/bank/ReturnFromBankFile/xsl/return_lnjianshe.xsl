<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW"> 
  <!-- 以下的代码块根据不同的银行而不同 -->
  <ROW>  
		<!-- 账号 -->        
		<AccNo type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
		</AccNo>
		<!-- 交易金额 -->
		<PayMoney type="number">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
		</PayMoney>
		<!-- 保单号，30位 -->
		<PayCode>
			<xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/>
		</PayCode>
		
		<BankSuccFlag type="text">
			<xsl:variable name="flag" select="xalan:tokenize(COLUMN, '|')[4]"/>
			<xsl:choose>
		    <xsl:when test="string-length($flag)>0">
		      <xsl:value-of select="$flag"/>
		    </xsl:when>
		    <!-- 多于4个汉字的处理 -->
		    <xsl:otherwise>
		      <xsl:value-of select="'N'"/>
		    </xsl:otherwise>
	  </xsl:choose>
		</BankSuccFlag> 
		<!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>  
     
   
	</ROW>  
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

