<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW[position()>5]">

   <ROW>
    
    <!--  先收费银行  以下的代码块根据不同的银行而不同 -->
    <!--  缴费编号 -->
 		<PayCode type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
    </PayCode>
    <!--  帐号 -->
 		<AccNo type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[8]"/>
    </AccNo>
    <!--  帐户名 -->
 		<AccName type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[9]"/>
    </AccName>
    <!--  金额 -->
    <xsl:variable name="PayMoneyStr" select="xalan:tokenize(COLUMN, '|')[10]"/>    
    <xsl:variable name="payMoney" select="substring($PayMoneyStr,1,string-length($PayMoneyStr)-1)"/>
 		<PayMoney type="number">
      <xsl:value-of select="number($payMoney)"/>
    </PayMoney>
    <!--  印刷号 -->
 		<PolNo type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[14]"/>
    </PolNo>
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(xalan:tokenize(COLUMN, '|')[17],1,4), '-', substring(xalan:tokenize(COLUMN, '|')[17],5,2), '-', substring(xalan:tokenize(COLUMN, '|')[17],7,2))"/>    
    </BankDealDate>
    
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

