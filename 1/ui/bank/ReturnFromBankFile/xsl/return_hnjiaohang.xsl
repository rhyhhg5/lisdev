<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <row>
    	<!-- 中文字符处理 -->
			    <xsl:variable name="accNameValue" select="substring(COLUMN,37,60)"/>
			    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
			     <!-- 账户名称（姓名） -->
			    <AccName type="text">
			      <xsl:value-of select="substring(COLUMN,37,60-$chinaLen)"/>
			    </AccName>   
			    <!-- 帐号 -->			    
			    <AccNo type="text">
			      <xsl:value-of select="substring(COLUMN,106-$chinaLen,30)"/>
			    </AccNo> 
			    <!-- 交易金额，12位 -->
			    <PayMoney type="text">
			      <xsl:value-of select="substring(COLUMN,142-$chinaLen,12)"/>
			    </PayMoney>
			    
			    <!-- 成功标记  -->    
			    <BankSuccFlag type="text">
						<xsl:value-of select="substring(COLUMN,168-$chinaLen,2)"/>
					</BankSuccFlag>     
			    <!-- 交易日期 -->
			    <BankDealDate type="text">
			      <xsl:value-of select="substring(COLUMN,170-$chinaLen,8)"/>
			    </BankDealDate>
    </row>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

