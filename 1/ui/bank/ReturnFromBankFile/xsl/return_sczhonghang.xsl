<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">
  <ROW>  
   <!-- 以下的代码块根据不同的银行而不同 -->   
    <!-- 账号，不够22位后面补空格 --> 
    
    <AccNo type="text">
      <xsl:variable name="tFlag" select="substring(COLUMN, 1, 2)"/>
	    <xsl:choose>
		    <xsl:when test="$tFlag='OD'">
		      <xsl:value-of select="concat('601382',substring(COLUMN, 3, 22))"/>
		    </xsl:when>
		    <xsl:when test="$tFlag='OV'">
		      <xsl:value-of select="concat('456351',substring(COLUMN, 3, 22))"/>
		    </xsl:when>
		    <!-- 多于四个汉字的处理 -->
		    <xsl:otherwise>
		      <xsl:value-of select="substring(COLUMN, 1, 22)"/>
		    </xsl:otherwise>
		  </xsl:choose>
    </AccNo>   
  <!-- 交易金额，15位， -->
   <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 24, 15)"/>
   </PayMoney>
   
   <!-- 中文字符处理 -->
   <xsl:variable name="accNameValue" select="substring-after(COLUMN, substring(COLUMN, 1, 39))"/>
   <xsl:variable name="chinaLen" select="number(java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '26')-2)"/>

    <!-- 账户名称（姓名） -->
   <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 40, 20-$chinaLen)"/>
   </AccName>
   <xsl:variable name="charValue" select="substring-after(COLUMN, substring(COLUMN, 1, 60-$chinaLen))"/>
   <xsl:variable name="char1Len" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($charValue, '2')"/>
   
   <xsl:variable name="charLen" select="number($char1Len+$chinaLen)"/>
  
 
 	<BankDealDate type="text">
			<xsl:value-of select="substring(COLUMN, 78-$charLen, 10)"/>
	</BankDealDate>
	<xsl:variable name="charValue1" select="substring-after(COLUMN, substring(COLUMN, 1, 88-$charLen))"/>
  <xsl:variable name="char1Len1" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($charValue1, '5')"/>
  <xsl:variable name="charLen1" select="number($charLen+$char1Len1)"/>
	<xsl:variable name="sucFlag" select="substring(COLUMN, 94-$charLen1, 1)"/>
  <xsl:choose>
    <xsl:when test="$sucFlag='1'">
    	<BankSuccFlag type="text">
					<xsl:value-of select="substring(COLUMN, 94-$charLen1, 1)"/>
			</BankSuccFlag>
    </xsl:when>
    <xsl:otherwise>
      <BankSuccFlag type="text">
					<xsl:value-of select="substring(COLUMN, 97-$charLen1, 4)"/>
			</BankSuccFlag>
    </xsl:otherwise>
  </xsl:choose>
  <!-- 以上的代码块根据不同的银行而不同 -->
     
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>




