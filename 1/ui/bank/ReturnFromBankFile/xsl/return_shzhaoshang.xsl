<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/"> 

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW[position()>1]">

  <ROW>
  
  	<PayCode type="text">
      <xsl:value-of select="substring(COLUMN, 9, 30)"/>
    </PayCode>
    
    <xsl:variable name="accNameValue" select="substring(COLUMN, 39,60)"/>
		<xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '60')"/> 
		<AccName type="text">
		    <xsl:value-of select="substring(COLUMN, 39,60-$chinaLen)" />
		</AccName>  
		
		<PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 99-$chinaLen, 15) div 100"/>
    </PayMoney>

    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 314-$chinaLen, 50)"/>
    </AccNo>
    
    <BankSuccFlag type="text">
		  <xsl:value-of select="substring(COLUMN, 514-$chinaLen,10)"/>
		</BankSuccFlag>	
    
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>  
    

  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>
