<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 账号，最19位 -->        
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 12, 22)"/>
    </AccNo>

    <!-- 交易金额，12位，包括小数点后两位 -->
      <xsl:variable name="PayMoney1" select="concat(substring(COLUMN, 34, 10),'.',substring(COLUMN, 44, 2))"/>     
    <PayMoney type="number">
      <xsl:value-of select="$PayMoney1"/>
    </PayMoney>  
    <xsl:variable  name="SuccFlag" select="substring(COLUMN, 72, 3)"/>        
    <!-- 交易状态 --> 
    <xsl:choose> 
    	<xsl:when test="$SuccFlag='101'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>
    	<xsl:when test="$SuccFlag='102'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>    	
    	<xsl:when test="$SuccFlag='103'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>    	
    	<xsl:when test="$SuccFlag='104'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>    	
    	<xsl:when test="$SuccFlag='105'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>
    	<xsl:when test="$SuccFlag='106'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>
    	    	<xsl:when test="$SuccFlag='107'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>
    	    	<xsl:when test="$SuccFlag='108'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>
    	    	<xsl:when test="$SuccFlag='109'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>
    	    	<xsl:when test="$SuccFlag='999'">      
    	  <BankSuccFlag type="text">
		  		<xsl:value-of select="$SuccFlag"/>
		  	</BankSuccFlag>
    	</xsl:when>
    	<xsl:otherwise>
    		<BankSuccFlag type="text">
		      <xsl:value-of select="'000'"/>
		    </BankSuccFlag>
    	</xsl:otherwise>
    </xsl:choose>   	 
    	    	    	
    <!-- 收据号 -->   
    <PayCode type="text"> 
      <xsl:value-of select="substring(COLUMN, 55, 17)"/>
    </PayCode>
        <!--交易时间-->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>  
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

