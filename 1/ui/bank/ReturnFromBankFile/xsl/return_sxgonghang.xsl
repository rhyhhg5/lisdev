<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW[position()>5]"> 

  <ROW>  		
		<!-- 账号 -->        
		<AccNo type="text">
			 <xsl:value-of select="xalan:tokenize(COLUMN, '|')[8]"/>
		</AccNo>				
		<AccName type="text"> 
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[9]"/> 
		</AccName>
		<!-- 交易金额 -->
		 
		 <xsl:variable name="payMoneyStr" select="translate(xalan:tokenize(COLUMN, '|')[10],',','')"/>
		 <xsl:variable name="payMoneyStrTwo" select="substring($payMoneyStr,1,string-length($payMoneyStr)-1)"/>
    <PayMoney type="number">
      <xsl:value-of select="number($payMoneyStrTwo)"/>
    </PayMoney> 
		
    <!-- 成功标记  -->  
    <xsl:variable name="flagStr" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK(substring(xalan:tokenize(COLUMN, '|')[11],1,4)))"/> 
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('处理成功'))"/> 
    <xsl:if test="$flagStr=$flagStr1"> 
    <BankSuccFlag type="text">
      <xsl:value-of select="'Y'"/>
    </BankSuccFlag>
    </xsl:if>  
   
	 <!-- 扣款日期 -->
    <BankDealDate type="text">
       <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate> 
	</ROW>  
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

