<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">
  <ROW>  
   <!-- 以下的代码块根据不同的银行而不同 -->   
    <!-- 账号，不够26位后面补空格 --> 
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 1, 26)"/>
    </AccNo>   
 
  <!-- 交易金额，12位，乘100用来去掉小数点 -->
   <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 27, 12) div 100"/>
   </PayMoney>
  
  <!-- 收付标志（借贷标志） -->
  <DealType type="text">
     <xsl:value-of select="substring(COLUMN, 39, 1)"/>
  </DealType> 
  
   <!-- 顺序号 不足16位补空格 -->
 
  
  <!-- 保单号，填充位，使用收据号代替，不够20位后面补空格 --> 
  <PayCode type="text">
     <xsl:value-of select="substring(COLUMN, 56, 22)"/>
  </PayCode> 
  
    <!-- 中文字符处理 -->
   <xsl:variable name="accNameValue" select="substring-after(COLUMN, substring(COLUMN, 1, 77))"/>
   <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '8')"/>
    <!-- 账户名称（姓名） -->
   <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 78, 8-$chinaLen)"/>
   </AccName>
  
  <!-- 管理机构 -->
    <ComCode type="text">
      <xsl:value-of select="substring(COLUMN, 98, 6)"/>
   </ComCode>
  
  <!-- 交易状态（成功标志） -->
   <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 104-$chinaLen, 1)"/>
   </BankSuccFlag>
   
  <!-- 交易日期 -->
     <BankDealDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN, 105-$chinaLen,8),1,4), '-', substring(substring(COLUMN, 105-$chinaLen,8),5,2), '-', substring(substring(COLUMN, 105-$chinaLen,8),7,2))"/>
    </BankDealDate>
    
  <!-- 以上的代码块根据不同的银行而不同 -->
     
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>




