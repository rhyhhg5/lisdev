<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 --> 
        <!-- 账号，19位 -->
    <xsl:variable name="FlagStr"  select="substring(COLUMN, 1, 11)"/> 
    <xsl:variable name="FlagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('45635108000'))"/> 
    <xsl:variable name="FlagStr2" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('60138208000'))"/> 
    <xsl:if test="$FlagStr=$FlagStr1" >
    	<AccNo type="text">
     	 <xsl:value-of select="substring(COLUMN, 1, 19)"/>
    	</AccNo>    
    </xsl:if>
    <xsl:if test="$FlagStr=$FlagStr2" >
    	<AccNo type="text">
     	 <xsl:value-of select="substring(COLUMN, 1, 19)"/>
    	</AccNo>    
    </xsl:if>     
    <!-- 账号，18位 --> 
    <xsl:variable name="flagStr"  select="substring(COLUMN, 9, 4)"/>  
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('0188'))"/> 
    <xsl:if test="$flagStr=$flagStr1" >
    	<AccNo type="text">
     	 <xsl:value-of select="substring(COLUMN, 1, 18)"/>
    	</AccNo>    
    </xsl:if>        
    <!-- 交易日期 -->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>
    
    <!-- 交易金额，10位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 20, 10)"/>
    </PayMoney>
    <!-- 交易状态 -->    
    <xsl:variable name="LongStr"  select="string-length(COLUMN)"/>
    <BankSucc type="text">
      <xsl:value-of select="$LongStr"/>
    </BankSucc>     
    <xsl:variable name="LongStr1" select="29"/> 
    <xsl:variable name="LongStr2" select="30"/> 
    <xsl:if test="$LongStr=$LongStr1" >
    <BankSuccFlag type="text">
      <xsl:value-of select="'Y'"/>
    </BankSuccFlag>    
    </xsl:if>
    <xsl:if test="$LongStr=$LongStr2" >
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN,30, 1)"/>
    </BankSuccFlag>   
    </xsl:if>      
    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

