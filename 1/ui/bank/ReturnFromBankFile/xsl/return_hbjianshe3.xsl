<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW[position()>0]"> 
<xsl:choose>
<xsl:when test="position()=last()">
</xsl:when>
<xsl:otherwise>	
  <!-- 以下的代码块根据不同的银行而不同 -->
  <ROW>  
  	<!-- 客户编号 -->        
		<PayCode type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/>
		</PayCode>
  	<!-- 持卡人卡号 -->        
		<AccNo type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[9]"/>
		</AccNo>
		 <!-- 交易状态 -->
    <BankSuccFlag type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[12]"/>
    </BankSuccFlag>
		 <!-- 金额 -->
    <PayMoney type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[14]"/>
    </PayMoney>  
   
	</ROW>  
</xsl:otherwise>
</xsl:choose>	
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

