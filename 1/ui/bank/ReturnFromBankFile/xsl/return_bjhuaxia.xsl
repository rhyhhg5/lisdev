<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
  
  <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 业务号 -->    
     <PayCode type="text">
      <xsl:value-of select="substring(COLUMN,1,12)"/>
    </PayCode>    
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring-after(COLUMN, substring(COLUMN,1,32))"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '10')"/>
    <!-- 账户名称（姓名） -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN,33,10-$chinaLen)"/>
    </AccName>    
    <!-- 交易金额，10位，包括小数点后两位，注意，工行返回数据中有小数点 -->
    <PayMoney type="text">
      <xsl:value-of select="substring(COLUMN,43-$chinaLen,10)"/>
    </PayMoney>    
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN,73-$chinaLen,8),1,4),'-',substring(substring(COLUMN,73-$chinaLen,8),5,2),'-',substring(substring(COLUMN,73-$chinaLen,8),7,2))"/>
    </BankDealDate>
      
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

