<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">
	
	<xsl:variable name="flagStr"  select="substring(COLUMN, 4, 1)"/> 
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('2'))"/> 
    
    <xsl:if test="$flagStr=$flagStr1" >
  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 --> 
	    <!-- 交易状态 -->
	    <BankSuccFlag type="text">
	      <xsl:value-of select="substring(COLUMN, 1, 3)"/>
	    </BankSuccFlag>
	    
	    <!-- 账号，19位 -->
	    <AccNo type="text">
	      <xsl:value-of select="substring(COLUMN, 17, 19)"/>
	    </AccNo>
	    
	    <!-- 交易日期 -->
	    <BankDealDate type="text">
	      <xsl:value-of select="concat(substring(substring(COLUMN, 112, 8),1,4), '-', substring(substring(COLUMN, 112, 8),5,2), '-', substring(substring(COLUMN, 112, 8),7,2))"/>
	    </BankDealDate> 
	    
	    <!-- 交易金额，12位，包括小数点后两位 -->
	    <PayMoney type="number">
	      <xsl:value-of select="substring(COLUMN, 43, 17) div 100"/>
	    </PayMoney>
    
        
    <!-- 以上的代码块根据不同的银行而不同 -->
    
  </ROW>
  </xsl:if>
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

