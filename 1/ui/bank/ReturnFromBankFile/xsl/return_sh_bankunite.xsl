<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 备注，最大16位，这里为15位收据号 -->        
    <!--交易时间-->
    <BankDealDate type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[16]"/>
    </BankDealDate>
    <!-- 交易金额，整数部分最多15位 -->
    <PayMoney type="number">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[9]"/>
    </PayMoney>
    <!-- 银行编码 -->        
    <BankCode type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[10]"/>
    </BankCode>
    <!-- 账号，最大32位 -->        
    <AccNo type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[12]"/>
    </AccNo>
    <!-- 账户名称，最大60位 -->    
    <AccName type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[13]"/> 
    </AccName>
    <BankSuccFlag type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[18]"/>
    </BankSuccFlag>

    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

