﻿<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">
<BANKDATA>    
<xsl:for-each select="BANKDATA/ROW">
<xsl:choose>
    <xsl:when test="substring(COLUMN,4,1)='1'">
		</xsl:when> 
		<xsl:otherwise>
  <row> 
    <BankSuccFlag type="text">
			<xsl:value-of select="substring(COLUMN,1,3)"/>
		</BankSuccFlag>   
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN,17,19)"/>
    </AccNo>
    <xsl:variable name="payMoneyStr" select="substring(COLUMN,60,17)"/>
    <PayMoney type="number">
        <xsl:value-of select="format-number($payMoneyStr*0.01, '0.00')"/>
    </PayMoney> 
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(COLUMN,112,4),'-',substring(COLUMN,116,2),'-',substring(COLUMN,118,2))"/>
    </BankDealDate>    
    </row>
    </xsl:otherwise>
     </xsl:choose> 
 </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

