<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">
  <ROW>  
   <!-- 以下的代码块根据不同的银行而不同 -->   
    <!-- 账号，不够26位后面补空格 --> 
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 1, 25)"/>
    </AccNo>   
  
  <!-- 交易日期 -->
   <BankDealDate type="text">
      <xsl:value-of select="substring(COLUMN, 26, 8)"/>
   </BankDealDate>
   
  <!-- 交易状态（成功标志） -->
   <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 34, 1)"/>
   </BankSuccFlag>
   
  
   
  <!-- 保单号，填充位，使用收据号代替，不够20位后面补空格 --> 
  <PayCode type="text">
     <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.LRemoveString(substring(COLUMN, 35, 16),'0')"/>
  </PayCode> 
  
    
  <!-- 以上的代码块根据不同的银行而不同 -->
     
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>




