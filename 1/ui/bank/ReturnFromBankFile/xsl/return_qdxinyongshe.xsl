<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">
<BANKDATA>
    
<xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 --> 
  <row>
  	<!-- 账号，最26位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN,1,26)"/>
    </AccNo>
    <!-- 交易金额，12位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="format-number(substring(COLUMN,27,12)*0.01,'0.00')"/> 
    </PayMoney> 
  
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring(COLUMN,62,20)"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
    <!-- 账户名称（姓名）20位 -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN,62,20-$chinaLen)"/>
    </AccName>
     
    <!-- 成功标记  -->    
    <BankSuccFlag type="text">
			<xsl:value-of select="substring(COLUMN,83-$chinaLen,1)"/>
		</BankSuccFlag>   
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>
    
  </row>
    <!-- 以上的代码块根据不同的银行而不同 -->
 </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

