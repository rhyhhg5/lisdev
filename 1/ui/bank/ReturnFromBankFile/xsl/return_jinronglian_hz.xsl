<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 --> 
    <!-- 银行代码 -->   
    <BankCode type="text"> 
      <xsl:value-of select="substring(COLUMN, 1, 2)"/>
    </BankCode>  
    
    <!-- 总户数，8位 -->
    <TotalNum type="number">
      <xsl:value-of select="number(substring(COLUMN, 31, 8))"/>
    </TotalNum>
    
    <!-- 总金额，14位，包括小数点后两位 -->
    <TotalMoney type="number">
      <xsl:value-of select="number(substring(COLUMN, 39, 14))"/>
    </TotalMoney>
    
    <!-- 银行成功总数量，8位 -->
    <BankSuccNum type="number">
      <xsl:value-of select="number(substring(COLUMN, 53, 8))"/>
    </BankSuccNum>
    
    <!-- 银行成功总金额，14位，包括小数点后两位 -->
    <BankSuccMoney type="number">
      <xsl:value-of select="number(substring(COLUMN, 61, 14))"/>
    </BankSuccMoney>
    
    <!-- 财务确认总金额，14位，包括小数点后两位 -->
    <AccTotalMoney type="number">
      <xsl:value-of select="number(substring(COLUMN, 61, 14))"/>
    </AccTotalMoney>
    
    <!-- 划款日期 -->
    <TransDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN, 91, 8),1,4), '-', substring(substring(COLUMN, 91, 8),5,2), '-', substring(substring(COLUMN, 91, 8),7,2))"/>
    </TransDate>
    <!-- 以上的代码块根据不同的银行而不同 -->
    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

