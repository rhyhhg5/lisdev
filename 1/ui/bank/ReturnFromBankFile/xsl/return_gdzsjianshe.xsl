<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW[position()>0]"> 
  <xsl:choose>         
      <xsl:when test="substring-after(format-number(position()*0.5, '0.00'),'.')!='00'"> 
  <ROW>  		
		<AccNo type="text">
			 <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
		</AccNo>				
		<AccName type="text"> 
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/> 
		</AccName>
     
    <xsl:variable name="flagStr" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK(substring(xalan:tokenize(COLUMN, '|')[3],1,4)))"/> 
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('交易成功'))"/> 
    <xsl:if test="$flagStr!=$flagStr1"> 
    	<BankSuccFlag type="text">
    		<xsl:value-of select="substring(xalan:tokenize(COLUMN, '|')[3],1,2)"/>
    	</BankSuccFlag>
    </xsl:if>
    <xsl:if test="$flagStr=$flagStr1"> 
      <BankSuccFlag type="text">
        <xsl:value-of select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('成功'))"/>
      </BankSuccFlag> 
    </xsl:if>
	
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate> 

 </ROW>  
	   </xsl:when>   
  </xsl:choose>  
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

