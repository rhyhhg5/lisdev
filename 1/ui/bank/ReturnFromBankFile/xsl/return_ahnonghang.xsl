<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    			<PayCode type="text">
			      <xsl:value-of select="substring(COLUMN,1,15)"/>
			    </PayCode>  
  				<AccNo type="text">
			      <xsl:value-of select="substring(COLUMN,16,25)"/>
			    </AccNo> 
			    <!-- 中文字符处理 -->
			    <xsl:variable name="accNameValue" select="substring-after(COLUMN, substring(COLUMN,1,40))"/>
			    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '20')"/>
			    
			     <!-- 账户名称（姓名） -->
			    <AccName type="text">
			      <xsl:value-of select="substring(COLUMN,41,20-$chinaLen)"/>
			    </AccName>   
			    <!-- 交易金额，12位 -->
			    <PayMoney type="text">
			      <xsl:value-of select="substring(COLUMN,61-$chinaLen,18) div 100"/>
			    </PayMoney>
			    <!-- 成功标记  -->    
			    <BankSuccFlag type="text">
						<xsl:value-of select="substring(COLUMN,79-$chinaLen,4)"/>
					</BankSuccFlag>     
			    <!-- 交易日期 -->
			    <BankDealDate type="text">
			      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
			    </BankDealDate>
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

