﻿<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
<xsl:for-each select="BANKDATA/ROW[position()>1]"> 
  <ROW>  
	 <xsl:variable name="flagStr" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK(substring(xalan:tokenize(COLUMN, '&#9;')[9],1,4)))"/> 
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('处理成功'))"/> 
    <xsl:if test="$flagStr=$flagStr1"> 
    <BankSuccFlag type="text">
      <xsl:value-of select="00"/>
    </BankSuccFlag>
    </xsl:if>  
		<!-- 账号 -->        
    <AccNo type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[3]"/>
    </AccNo>
    <xsl:variable name="payMoneyStr" select="xalan:tokenize(COLUMN, '&#9;')[7]"/>
    <xsl:variable name="payMoneyStrTwo" select="substring($payMoneyStr,1,string-length($payMoneyStr)-1)"/>
    <xsl:variable name="payMoneyStrThree" select= "translate($payMoneyStrTwo,',','')"/>
    <PayMoney type="number">
      <xsl:value-of select="number($payMoneyStrThree)"/>
    </PayMoney> 
    <PolNo>
    	<xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[10]"/>
    </PolNo>   
    <AccName type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[4]"/> 
    </AccName>
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>  
	</ROW>  
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

