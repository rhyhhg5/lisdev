<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>
  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
  <ROW>
    <!-- 以下的代码块根据不同的银行而不同 -->   
    <!--序号20-->
    
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring(COLUMN, 21, 64)"/>
    
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '32')"/>    
    <!-- 账户名称 32（姓名） -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 21-$chinaLen, 32)"/>
    </AccName>
    
    <!-- 账号30位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 53-$chinaLen, 30)"/>
    </AccNo>  
   
    <!-- 交易金额18位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 83-$chinaLen , 18)"/>
    </PayMoney>
    <!--摘要 30 -->

    <!-- 扣款日期  8 -->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>
    
    <xsl:variable name="accNameValue1" select="substring(COLUMN, 1)"/>
    <xsl:variable name="chinaLen1" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue1,135)"/>    
    <!-- 交易状态 返回码 4位	 0000表示成功 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN,139-$chinaLen1 , 4)"/>
    </BankSuccFlag>
    <!--冗余码 1 -->
    
    <!-- 以上的代码块根据不同的银行而不同 -->
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

