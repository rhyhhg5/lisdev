<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">
<BANKDATA>
    
<xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 --> 
  <ROW>

    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring-after(COLUMN,substring(COLUMN,1,5))"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue,'20')"/>
    <!-- 账户名称（姓名）10位 -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN,6,10-$chinaLen)"/>
    </AccName>
    
    <!-- 账号，最20位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN,16-$chinaLen,20)"/>
    </AccNo>
    
    <!-- 交易金额，10位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN,36-$chinaLen,10)"/>
    </PayMoney> 
    
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN, 46-$chinaLen, 8),1,4), '-', substring(substring(COLUMN, 46-$chinaLen, 8),5,2), '-', substring(substring(COLUMN, 46-$chinaLen, 8),7,2))"/>
    </BankDealDate>
    
    <!-- 成功标记  -->    
    <BankSuccFlag type="text">
			<xsl:value-of select="substring(COLUMN,54-$chinaLen,4)"/>
		</BankSuccFlag>   
    
    </ROW>
    <!-- 以上的代码块根据不同的银行而不同 -->
 </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

