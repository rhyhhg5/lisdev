<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 -->
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring(COLUMN, 13, 40)"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '40')"/>
    <!-- 账户名称（姓名） -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 13, 40-$chinaLen)"/>
    </AccName>
    <!-- 由于折共87位，卡88位，故以后部分因不同类型而不同 -->
    <xsl:variable name="rowLength" select="string-length(COLUMN)+$chinaLen"/>
    <AccName type="text">
      <xsl:value-of select="$rowLength"/>
    </AccName>
		<xsl:choose>
    	<!-- 折格式开始 -->
    	<xsl:when test="$rowLength='119'">
					<!-- 账号，折：19位 卡：20位 -->
		    	<AccNo type="text">      
		      	<xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.LRemoveString(substring(COLUMN, 53-$chinaLen, 19),'0')"/>
			    </AccNo>	    
			    <!-- 保单号 -->
			    <PolNo type="text">
			      <xsl:value-of select="substring(COLUMN, 72-$chinaLen, 20)"/>
			    </PolNo>			    
			    <!-- 交易金额，10位，包括小数点后两位 -->
			    <PayMoney type="number">
			      <xsl:value-of select="substring(COLUMN, 97-$chinaLen, 10) div 100"/>
			    </PayMoney>			    
			    <!-- 交易日期 -->
			    <BankDealDate type="text">
			      <xsl:value-of select="concat(substring(substring(COLUMN, 107-$chinaLen, 8),1,4), '-', substring(substring(COLUMN, 107-$chinaLen, 8),5,2), '-', substring(substring(COLUMN, 107-$chinaLen, 8),7,2))"/>
			    </BankDealDate>			    
			    <!-- 交易状态 -->
			    <BankSuccFlag type="text">
			      <xsl:value-of select="substring(COLUMN, 118-$chinaLen, 2)"/>
			    </BankSuccFlag>
    	</xsl:when>
    	<!-- 卡格式开始 -->
    	<xsl:otherwise>
    			<!-- 账号，卡：20位 -->
		    	<AccNo type="text">      
		      	<xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.LRemoveString(substring(COLUMN, 53-$chinaLen, 20),'0')"/>
			    </AccNo>	    
			    <!-- 保单号 -->
			    <PolNo type="text">
			      <xsl:value-of select="substring(COLUMN, 73-$chinaLen, 20)"/>
			    </PolNo>			    
			    <!-- 交易金额，10位，包括小数点后两位 -->
			    <PayMoney type="number">
			      <xsl:value-of select="substring(COLUMN, 98-$chinaLen, 10) div 100"/>
			    </PayMoney>			    
			    <!-- 交易日期 -->
			    <BankDealDate type="text">
			      <xsl:value-of select="concat(substring(substring(COLUMN,108-$chinaLen,8),1,4), '-', substring(substring(COLUMN,108-$chinaLen,8),5,2), '-', substring(substring(COLUMN,108-$chinaLen,8),7,2))"/>
			    </BankDealDate>			    
			    <!-- 交易状态 -->
			    <BankSuccFlag type="text">
			      <xsl:value-of select="substring(COLUMN,119-$chinaLen, 2)"/>
			    </BankSuccFlag>
    	</xsl:otherwise>
    </xsl:choose>
    
    <!-- 以上的代码块根据不同的银行而不同 -->
    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

