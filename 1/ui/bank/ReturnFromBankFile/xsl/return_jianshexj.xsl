<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>

    <AccNo type="number">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
    </AccNo>
    
    <AccName type="number">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/>
    </AccName>
    
    <PayMoney type="number">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[4]"/>
    </PayMoney>
    
    <PayCode type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[5]"/>
    </PayCode>
    
    <BankSuccFlag type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[6]"/>
    </BankSuccFlag>

    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

