<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">
<BANKDATA>
    
<xsl:for-each select="BANKDATA/ROW"> 
	<!--这个是代付的回盘文件，投保人姓名在卡号前面-->
    <!-- 以下的代码块根据不同的银行而不同 --> 
  <ROW>
  	 <!-- 保单号 -->
  	<PayCode type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
		</PayCode>
		<!-- 业务员工号 -->
		<AgentCode type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
		</AgentCode>
			<!-- 账户名 -->
		<AccName type="text"> 
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/> 
		</AccName> 
		<!-- 卡号-->
		<AccNo type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[4]"/>
		</AccNo>
		<!-- 证件号码 -->
		<IDNo type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[5]"/>
		</IDNo> 
    <!-- 金额 -->
    <PayMoney type="number">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[6]"/>
		</PayMoney>
		<!--成功标志返回码-->
		<BankSuccFlag type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[7]"/>
		</BankSuccFlag>
		<!-- 交易日期 -->
		<BankDealDate type="text">
  		<xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()" /> 
  	</BankDealDate> 
    </ROW>
    <!-- 以上的代码块根据不同的银行而不同 -->
 </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

