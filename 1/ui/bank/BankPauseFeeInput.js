
//程序名称：BankPauseFeeInput.js
//程序功能：暂停收费输入
//创建日期：2005-08-01
//创建人  ：张君
//更新记录：  更新人    更新日期     更新原因/内容
var turnPage = new turnPageClass();
var strSql;
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
function queryAgent() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
  }
  if(fm.all('AgentCode').value == "") {
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  }
  //alert("AgentCode"+fm.all('AgentCode').value);

  if(fm.all('AgentCode').value != "") {
    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
  //  alert(arrResult);
    if (arrResult != null) {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
  }

}
function displayQueryResult(strResult) {
  //判断是否查询成功
  if (!strResult) {
    alert("未查询到满足条件的数据！");
    initBillGrid();
    return false;
  }

  //保存查询结果字符串
  turnPage.strQueryResult  = strResult;
  //设置查询起始位置
  turnPage.pageIndex = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  turnPage.pageLineNum = 10 ;
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(strResult);

  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = BillGrid;
  //保存SQL语句
  turnPage.strQuerySql = strSql ;

  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try {
      window.divPage.style.display = "";
    } catch(ex) { }
  }
  else {
    try {
      window.divPage.style.display = "none";
    } catch(ex) { }
  }

  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;

}
//根据起始日期进行查询出要该日期范围内的批次号码
function PauseDataQuery() {
	
  var strResult;
  //alert(fm.PauseFeeType.value);
  var tOtherNoType = fm.PauseFeeType.value;
  //alert(tOtherNoType);

  if(fm.PauseFeeType.value ==null||fm.PauseFeeType.value=="" ) 
  {
    alert("请您输入交费类型！");
  } else if((fm.BankCode.value=="")&&(fm.PauseFeeType.value==4)||(fm.BankCode.value ==null)&&(fm.PauseFeeType.value==4)) {
    alert("请您输入银行代码！");
  }
  else {
    //alert("2");
    strSql = "select GetNoticeNo,BankCode,BankAccNo,SumDuePayMoney,PayDate,BankOnTheWayFlag from LJSPay ";
    switch (parseInt(tOtherNoType)) {
    case 1:
      strSql += "where OtherNoType in ('4','5','6','7','8','9') ";
      break;
    case 2:
      strSql += "where OtherNoType in ('1','2') ";
      break;
    case 3:
      strSql += "where OtherNoType in ('3','10') ";
      break;
    case 4:
      strSql += "where BankCode <> ''";
      break;
    default:
      break;
    }

    strSql += getWherePart('LJSPay.BankOnTheWayFlag','PauseFlag')
              + getWherePart('LJSPay.PayData' , 'PayDate')
              + getWherePart('LJSPay.ManageCom' , 'ManageCom')
              + getWherePart('LJSPay.AgentCode' , 'AgentCode')
              + getWherePart('LJSPay.BankCode' , 'BankCode')
              + getWherePart('LJSPay.PayDate' , 'PayDate');
   // alert(strSql);
    strResult =  easyQueryVer3(strSql, 1, 1, 1);
    displayQueryResult(strResult);

  }

}

function PausePayFee() {
  //alert("pause");
  fm.fmtransact.value="Pause";
  //if (verifyInput() == false) return false;
  fm.submit();
}

function CancelPauseFee() {
	//alert("cancel")
  fm.fmtransact.value="Cancel";
  //if (verifyInput() == false) return false;
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];
  }
}
