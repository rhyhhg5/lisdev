<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PaySendToBankSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  
<%
  System.out.println("\n\n---PaySendToBankSave Start---");
  
  PaySendToBankUI petSendToBankUI1 = new PaySendToBankUI();

  TransferData transferData1 = new TransferData();
 
String bankcode =request.getParameter("BankCode");
String flag= request.getParameter("flag");
System.out.println("收付费标识："+flag);
String FlagStr = null;
String Content = null;

try{
    if(flag.equals("S") && bankcode.equals("7708") ){
        System.out.println("-----农行代收-----");
        ABCPayTask tABCPayTask = new ABCPayTask();
        FlagStr = "";
        Content = "批处理程序触发成功";
        tABCPayTask.run();
    } else if(flag.equals("F") && bankcode.equals("7708")){
        System.out.println("-----农行代付-----");
        ABCGetTask tABCGetTask = new ABCGetTask();
        //BatchSendBankTrans tBatchSendBankTrans = new BatchSendBankTrans();
        tABCGetTask.run();
        FlagStr = "";
        Content = "批处理程序触发成功";
    }else if(flag.equals("SF") && bankcode.equals("7708") ){
        System.out.println("-----农行代收代付-----");
        ABCBankPayGetQueryTask tABCBankPayGetQueryTask = new ABCBankPayGetQueryTask();
        FlagStr = "";
        Content = "批处理程序触发成功";
        tABCBankPayGetQueryTask.run();
    	} 
    } catch (Exception ex){
        ex.printStackTrace();
        FlagStr = "Fail";
        Content = "批处理程序运行失败";
    }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.fm.all("sub").disabled=false;
	parent.fraInterface.fm.all("sub1").disabled=false;
	parent.fraInterface.fm.all("sub2").disabled=false;
</script>
</html>
