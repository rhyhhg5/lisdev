<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：LDCodeInterfaceInput.jsp
//程序功能：代码对应关系定义
//创建日期：2006-10-17 15:21
//创建人  ：Wulg
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
//全局变量获取	
String tFlag = request.getParameter("type");
String SysName = request.getParameter("SysName");
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="./LDCodeInterfaceInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LDCodeInterfaceInputInit.jsp"%>
		<title>代码对应关系定义
		</title>
	</head>
	<body onload="initForm();">
		<form name=fm action="LDCodeInterfaceSave.jsp" method=post target="fraSubmit">
		<%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
		<br>
		<table class=common align=center>		
			<tr class=common>
				<td class=title>
					代码类型
				</td>
				<td class=input>
					<input class=common type="text" name="CodeType" verify="代码类型|notnull&len<20"/>
				</td>
				<td class=title>
					代码类型名称
				</td>
				<td class=input>
					<input class=common type="text" name="CodeTypeNmae" verify="代码类型名称|len<300"/>
					<input type=hidden name="fmtransact"/>
					<input type=hidden name="SysName" value="<%=SysName%>"/>
					<input type=hidden name="MakeDate"/>
					<input type=hidden name="MakeTime"/>
				</td>
			</tr>
		</table>
		<br>
		<Div  id= "divCodeInterfaceInput" style= "display: ''">
    <Table  class=common>
      <tr>
    	  <td text-align: left colSpan=1>
	        <span id="spanCodeGrid" >
	        </span> 
	      </td>
      </tr>
      <tr>
      	<td>
      		<Div id= "divPage" align=center style= "display: 'none' ">
          <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
          <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </Div>
      	</td>
      </tr>
    </Table>      
    </Div>
  </form>
	</body>
</html>