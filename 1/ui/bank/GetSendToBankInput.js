//GetSendToBankInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//提交，保存按钮对应操作
function submitForm() {
  
  if(checkBank() == false) return false;
  if(verifyInput() == false) return false;
  if (fm.all("EndDate").value > cDate )
  {
	 	alert("终止日期大于当前日期！");
	 	return false;
  }
  fm.all("typeFlag").value = "";
  fm.all("sub").disabled=true;
  fm.submit();
}

//提交，保存按钮对应操作
function submitFormForXQ() {
  if(verifyInput() == false) return false;
  
  if (fm.all("EndDate").value > cDate )
  {
	 	alert("终止日期大于当前日期！");
	 	return false;
  }
  
  fm.all("typeFlag").value = "ALLXQ";
  
  if (confirm("提取所有续期续保的催收数据，确定吗？")) {
    fm.submit();
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Manage ,SerialNo ) {
  try { showInfo.close(); } catch(e) {}
  
  if(Manage != ""){
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Manage ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");  
  }
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px"); 
  
  //显示提取的清单
  if (SerialNo != ""){
	  showList(SerialNo);
  } else {
	  BankGrid.clearData("BankGrid");
  }
} 

//初始化银行代码
function initBankCode() {
  var strSql = "select BankCode, BankName from LDBank where ComCode = '" + comCode + "'"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("bankCode").CodeData = strResult;
  }
}   


//显示提取的清单
function showList(serialno) {
	// 书写SQL语句
	var strSql = "select paycode, polno, bankcode, paymoney,accno,accname,comcode,serialno from lysendtobank where serialno='" + serialno + "'";
				     
	turnPage.queryModal(strSql, BankGrid);
}

function checkBank(){
 var BankCode = fm.all("BankCode").value;
  var strSql ="select BankCode,Bankname from LDBank where (AgentPaySendF is  null or AgentPayReceiveF is null or AgentPaySuccFlag is  null) and bankcode = '"+ BankCode + "'"; 
 var strResult = easyQueryVer3(strSql);
  if (strResult) {
    alert("您所选择的银行不支持收款！");
    return false;
  }
}