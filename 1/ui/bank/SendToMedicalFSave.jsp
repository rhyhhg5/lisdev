<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PaySendToBankSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  
<%
  System.out.println("\n\n---SendToMedicalSave Start---");
  
  SendToMedicalFUI SendToMedicalFUI = new SendToMedicalFUI();

  TransferData transferData1 = new TransferData();
  transferData1.setNameAndValue("startDate", request.getParameter("StartDate"));
  transferData1.setNameAndValue("endDate", request.getParameter("EndDate"));
  transferData1.setNameAndValue("MedicalCode", request.getParameter("MedicalCode"));

  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");

  VData tVData = new VData();
  tVData.add(transferData1);
  tVData.add(tGlobalInput);
  
  String Content = "";
  String FlagStr = "";
  String Manage = "";
  String SerialNo = "";

  if (!SendToMedicalFUI.submitData(tVData, "PAYMONEY")) {
    VData rVData = SendToMedicalFUI.getResult();
    Content = " 处理失败，原因是:" + (String)rVData.get(0);
  	FlagStr = "Fail";
  }
  else {
  	VData rVData = SendToMedicalFUI.getResult();
    Content = " 处理成功! "+ (String)rVData.get(0);
  	FlagStr = "Succ";
  	Manage = SendToMedicalFUI.getManage();
  	SerialNo = SendToMedicalFUI.getSerialNo();
  }

	System.out.println(Content + "\n" + FlagStr + "\n---SendToMedicalSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>','<%=Manage%>','<%=SerialNo%>');
	parent.fraInterface.fm.all("sub").disabled=false;
</script>
</html>
