//GetSendToBankInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//提交，保存按钮对应操作
function submitForm() {
  
  if(checkMedicalCode() == false) return false;
  if(verifyInput() == false) return false;
  fm.all("typeFlag").value = "";
  fm.all("sub").disabled=true;
  fm.submit();
}

function MXButton(){
	
	var serialno = fm.serialNo.value;
//	serialno = '00000000000000195696';
	var querySQL = "select lss.agentcode,"
				    +"   laa.name,"
				    +"   lss.medicalcode,"
				    +"  lss.accno,"
				    +" lss.accname,"
				    +" lss.paycode,"
				    +" lss.polno,"
				    +" case lss.paytype"
				    +"   when '1' then"
				    +"    '首期'"
				    +"   else"
				    +"    '续期'"
				    +" end,"
				    +" lss.paymoney,"
				    +" lss.comcode"
				    +" from lysendtosettle lss"
				    +" left join laagent laa"
				    +" on lss.agentcode = laa.agentcode"
				    +" where serialno = '"+serialno+"'"
				    +" and dealstate is not null with ur";
	fm.querySql.value = querySQL;
	fm.filename.value = 'SettledMX_';
	//提交，下载
	DownLoad(serialno);
}

//下载
function DownLoad(serialno) {
	
	if(serialno != null && serialno != "" )
    {
        var formAction = fm.action;
        fm.action = "MedicalListOfDSDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先选择批次，再生成清单！");
        return ;
    }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Manage ,SerialNo ) {
  try { showInfo.close(); } catch(e) {}
  
  if(Manage != ""){
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Manage ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");  
  }
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px"); 
  
  //显示提取的清单
  if (SerialNo != ""){
	  showList(SerialNo);
  } else {
	  BankGrid.clearData("BankGrid");
  }
  fm.serialNo.value = SerialNo;
} 

//初始化银行代码
function initMedicalCode() {
  var strSql = "select MedicalComCode, MedicalComName from LDMedicalCom where ComCode = '" + comCode + "'"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("MedicalCode").CodeData = strResult;
  }
}   


//显示提取的清单
function showList(serialno) {
	// 书写SQL语句
	var strSql = "select paycode, polno,(select customgetpoldate from lccont where prtno = lss.polno),medicalcode, paymoney,accno,accname,comcode,serialno from lysendtoSettle lss where lss.serialno='" + serialno + "'";
				     
	turnPage.queryModal(strSql, BankGrid);
	showStatistics(serialno);
}
function QueryGridData(){
	var strSQL = " select distinct ltf.tempfeeno,"
				 +"               lcc.prtno,"
				 +"               lcc.customgetpoldate,"
				 +"               lcc.bankcode,"
				 +"               ltf.paymoney,"
				 +"               (select bankaccno"
				 +"                  from ljtempfeeclass"
				 +"                 where tempfeeno = ltf.tempfeeno),"
				 +"               (select accname"
				 +"                  from ljtempfeeclass"
				 +"                 where tempfeeno = ltf.tempfeeno),"
				 +"               lcc.managecom,"
				 +" case  when lcc.customgetpoldate is null then '未回执回销' "
				 +" else  '未确认' end"
				 +" from lccont lcc"
				 +" left join ljtempfee ltf"
				 +"   on lcc.contno = ltf.otherno"
				 +" where (lcc.customgetpoldate + 20 day <= current date or lcc.customgetpoldate is null  )"
				 +"	and lcc.managecom like '" + comCode + "%' "
				 +"	and lcc.paymode = '8'"
				 +"   and not exists (select *"
				 +"         from lysendtoconfirm stc"
				 +"        where stc.paycode = lcc.prtno"
				 +"          and stc.confirmstate in ('1', '2')) "
				 +"	and ltf.tempfeeno is not null"
				 +" with ur";
	
	turnPage1.queryModal(strSQL, QueryGrid);
}

function checkMedicalCode(){
 var MedicalCode = fm.all("MedicalCode").value;
  var strSql ="select MedicalComCode,MedicalComName from LDMedicalCom where (AgentPaySendF is  null or AgentPayReceiveF is null or AgentPaySuccFlag is  null) and MedicalComCode = '"+ MedicalCode + "'"; 
 var strResult = easyQueryVer3(strSql);
  if (strResult) {
    alert("您所选择的医保机构不支持收款！");
    return false;
  }
}
//显示银行批次数据的统计信息，金额、件数
function showStatistics(serialno) {
  var strSql = "select Totalmoney, Totalnum from lybanklog where SerialNo = '" 
             + serialno
             + "' and LogType='D'";
  var arrResult = easyExecSql(strSql);
  fm.all("TotalMoney").value = arrResult[0][0];
  fm.all("TotalNum").value = arrResult[0][1];
 
}