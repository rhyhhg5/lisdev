//DeleteSendToBankInput.js该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;

//提交，保存按钮对应操作
function submitForm() {
  if (BankGrid.getSelNo() == 0) {
    alert("请先选择一条要取消的银行数据，银行在途数据不能取消！");
  }
  else {     
    fm.TempFeeNo.value = BankGrid.getRowColData(BankGrid.getSelNo()-1, 1);
    fm.Action.value = "DELETE";   
    
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
  }
}

//提交，保存按钮对应操作
function submitForm2() {
  if (BankGrid.getSelNo() == 0) {
    alert("请先选择一条要取消的银行数据！");
  }
  else {     
    fm.TempFeeNo.value = BankGrid.getRowColData(BankGrid.getSelNo()-1, 1);   
    fm.Action.value = "UPDATE";  
    
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

// 查询按钮
function easyQueryClick() {
  if (fm.PrtNo.value == "") {
    alert("请先输入印刷号");
    return; 
  }
  
	// 书写SQL语句			     
	var strSql = "select a.tempfeeno, a.paymoney, a.bankcode, a.accname, a.bankaccno, '否',0"
             + " from ljtempfeeclass a where a.paymode='4' and a.enteraccdate is null and a.tempfeeno in "
          	 + " (select tempfeeno from ljtempfee where trim(otherno) = '" + fm.PrtNo.value 
          	 + "') and a.tempfeeno not in (select GetNoticeNo from ljspay)"
          	 + " union select a.tempfeeno, a.paymoney, a.bankcode, a.accname, a.bankaccno,(case b.bankonthewayflag when '1' then '是' else '否' end),b.sendbankcount"
          	 + " from ljtempfeeclass a,ljspay b where a.paymode='4' and a.enteraccdate is null and a.tempfeeno in "
          	 + " (select tempfeeno from ljtempfee where trim(otherno) = '" + fm.PrtNo.value 
          	 + "') and a.tempfeeno=b.GetNoticeNo"          	 
				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
}

