//PaySendToBankInput.js该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug = "0";
var tFees;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm() {
	if (verifyInput() == false) {
		return false;
	}
	if (fm.all("EndDate").value > cDate) {
		alert("\u7ec8\u6b62\u65e5\u671f\u5927\u4e8e\u5f53\u524d\u65e5\u671f\uff01");
		return false;
	}
	fm.all("sub").disabled = true;
	fm.submit();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Manage, SerialNo) {
	try {
		showInfo.close();
	}
	catch (e) {
	}
	if (Manage != "") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + Manage;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");
	}
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
	showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");  
  
  // 显示提取的清单
	if (SerialNo != "") {
		showList(SerialNo);
	} else {
		BankGrid.clearData("BankGrid");
	}
}

//初始化银行代码
function initMedicalCode() {
  var strSql = "select MedicalComCode, MedicalComName from LDMedicalCom where ComCode like '" + comCode + "%' and medicalcomcode not in (select code from ldcode where codetype='automedicalcom')"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("MedicalCode").CodeData = strResult;
  }
}       

// 显示提取的清单
function showList(serialno) {
	// 书写SQL语句
	var strSql = "select paycode, polno, bankcode, paymoney,accno,accname,comcode,serialno from lysendtobank where serialno='" + serialno + "'";
	turnPage.queryModal(strSql, BankGrid);
}
