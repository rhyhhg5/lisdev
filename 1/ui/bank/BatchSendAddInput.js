var showInfo;
var turnPage = new turnPageClass();

function bankHelp(){
    window.open("./BankHelp.jsp");
}

function quBank() {
  if(fm.UniteBankCode2.value == null || fm.UniteBankCode2.value == ""){
      alert("请填写银联编码！");
      return false;
  }

  var strSql = "select bankunitecode,bankunitename,bankcode,unitebankname,(select codename from ldcode1 where code='" + fm.UniteBankCode2.value + "' and codealias=unitebankcode),(select code1 from ldcode1 where code='" + fm.UniteBankCode2.value + "' and codealias=unitebankcode),unitebankcode,case unitegroupcode when '1' then '支持代收' when '2' then '支持代付' when '3' then '支持代收及代付' end from ldbankunite where bankunitecode='" + fm.UniteBankCode2.value + "'";
  if(fm.BankType2.value != null && fm.BankType2.value != ""){
      strSql = strSql + " and unitebankcode=(select codealias from ldcode1 where code1='" + fm.BankType2.value + "' and code='" + fm.UniteBankCode2.value + "' and codetype='BatchBank')";
  }
  strSql = strSql + getWherePart('ComCode','ManageCom','like');
  var strSqlTemp=easyQueryVer3(strSql, 1, 0, 1); 
     turnPage.strQueryResult=strSqlTemp;
     if(!turnPage.strQueryResult)
     {
        window.alert("没有查询记录!");
        BankGrid.clearData();
        return false;
     }
     else
     {
          turnPage.queryModal(strSql, BankGrid);
     }
}

function UpdateBank(){
     var tSel = 0;
     tSel = BankGrid.getSelNo();
        try
        {
            if (tSel !=0 ){
               Updatediv.style.display = "";
            } else{
               Updatediv.style.display = "none";
            }
            fm.UniteBankCode3.value = BankGrid.getRowColData(tSel-1,1);
            fm.upBankType.value = BankGrid.getRowColData(tSel-1,6);
            fm.BankTypeName3.value = BankGrid.getRowColData(tSel-1,5);
            fm.upBankCode.value = BankGrid.getRowColData(tSel-1,3);
        }
        catch(ex)
        {
            alert( "没有发现父窗口的接口" + ex );
        }
}

function addBank(){
    if (!verifyInput()) {
        return false;
    }
    
    if(fm.BankCode.value == null || fm.BankCode.value == ""){
      alert("请填写银行编码！");
      return false;
    }
  
    var strBankSql = "select 1 from ldbank where bankcode = '" + fm.BankCode.value + "' and comcode like '" + fm.ManageCom.value + "%'";
    var strBankSqlTemp = easyQueryVer3(strBankSql, 1, 0, 1); 
    if(!strBankSqlTemp){
        alert("银行编码不存在或不在本机构下！");
        return false;
    }
  
    fm.submitType.value="INSERT";
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}

function upBank(){
    if(fm.upBankCode.value == null || fm.upBankCode.value == ""){
        alert("请选中一条记录！");
        return false;
    }
    if(fm.upBankType.value == null || fm.upBankType.value == ""){
        alert("请选择所属银行！");
        return false;
    }
    if(fm.UniteBankCode3.value == null || fm.UniteBankCode3.value == ""){
        alert("请选中一条记录！");
        return false;
    }
    fm.submitType.value="UPDATE";
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}

function afterSubmit( FlagStr, content ,submitType) {
  try { 
      showInfo.close(); 
  } catch(e) {
  
  }
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
  
  if(submitType == "UPDATE"){
      Updatediv.style.display = "none";
      fm.upBankCode.value="";
      fm.upBankType.value="";
      fm.UniteBankCode3.value="";
      fm.BankTypeName3.value="";
      quBank();
  }
} 

function querybank()
{
    bankflag="0";
    showInfo = window.open("../bq/LDBankQueryMain.jsp");
}

function afterQuery( arrReturn )
{
    fm.all('BankCode').value = arrReturn[0];
    fm.all('BankCodeName').value = arrReturn[1];
}