<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <%@include file="BatchSendAddInit.jsp"%>
  <SCRIPT src="BatchSendAddInput.js"></SCRIPT>

  
  <title>添加已有银行分行或支行 </title>
</head>

<body onload="initForm();initElementtype();">
  <form action="./BatchSendAddSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 银行 -->
    <table class= common border=0 width=100%>
        <tr>
            <td class= titleImg align= center>配置已有银行与银联进行关联</td>
        </tr>
    </table>
    
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        银联编码
      </TD>
      <TD  class= input>
        <Input NAME=UniteBankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank4ytj',[this,UniteBankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank4ytj',[this,UniteBankCodeName],[0,1]);" verify="银联编码|code:sendbank4ytj&notnull" ><input class=codename name=UniteBankCodeName readonly=true  elementtype="nacessary">
      </TD>
      <TD  class= title>
        银行编码
      </TD>
      <TD  class= input>
        <Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="querybank();" onkeyup="querybank();"><input class=codename name=BankCodeName readonly=true  elementtype="nacessary">
      </TD>
      <TD  class= title>
        所属银行
      </TD>
      <TD  class= input>
        <Input NAME=BankType CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank6',[this,BankTypeName],[0,1],null,fm.UniteBankCode.value,'code',1,150);" onkeyup="return showCodeListKey('sendbank6',[this,BankTypeName],[0,1],null,fm.UniteBankCode.value,'code',1,100);" verify="所属银行|code:sendbank6&notnull" ><input class=codename name=BankTypeName readonly=true  elementtype="nacessary">
      </TD>
    </TR>
    </table>
    <br>
    <INPUT VALUE="确  定" class= cssButton TYPE=button onclick="addBank();">
    <INPUT VALUE="支持银行清单" class= cssButton TYPE=button onclick="bankHelp();">  
    <br><br><hr>
    <table class= common border=0 width=100%>
        <tr>
            <td class= titleImg align= center>已配置银行查询</td>
        </tr>
    </table>
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        银联编码
      </TD>
      <TD  class= input>
        <Input NAME=UniteBankCode2 CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank4ytj',[this,UniteBankCodeName2],[0,1]);" onkeyup="return showCodeListKey('sendbank4ytj',[this,UniteBankCodeName2],[0,1]);" ><input class=codename name=UniteBankCodeName2 readonly=true  elementtype="nacessary">
      </TD>
      <TD  class= title>
        所属银行
      </TD>
      <TD  class= input>
        <Input NAME=BankType2 CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank6',[this,BankTypeName2],[0,1],null,fm.UniteBankCode2.value,'code',1,150);" onkeyup="return showCodeListKey('sendbank6',[this,BankTypeName2],[0,1],null,fm.UniteBankCode2.value,'code',1,100);" ><input class=codename name=BankTypeName2 readonly=true >
      </TD>
    </TR>
    </table>
    <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="quBank();">
        <br><br>
            <table>
      <tr>
         <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" onClick="showPage(this,divLCPol1);"></td>
         <td class=titleImg>清单列表</td>
     </tr>
    </table>
        <Table  class= common>
            <TR  class= common>
             <TD text-align: left colSpan=1>
                 <span id="spanBankGrid" ></span> 
       	    </TD>
           </TR>
         </Table>	
     <Div  id= "bankdiv" align=center style= "display: ''">				
           <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
           <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
           <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
           <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div>
  <hr>
    <table class= common border=0 width=100%>
        <tr>
            <td class= titleImg align= center>所属银行调整</td>
        </tr>
    </table>                   
  <Div  id= "Updatediv" style= "display: 'none'">    
  <table class= common>
      <TR class= common>
      <TD class=title>银行编码</TD>
      <TD class= input><input class="common"  name=upBankCode readonly=true></TD>
      <TD class=title>所属银行</TD>
      <TD  class= input>
      <Input NAME=upBankType CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank6',[this,BankTypeName3],[0,1],null,fm.UniteBankCode3.value,'code',1,150);" onkeyup="return showCodeListKey('sendbank6',[this,BankTypeName3],[0,1],null,fm.UniteBankCode3.value,'code',1,100);" ><input class=codename name=BankTypeName3 readonly=true  elementtype="nacessary">
      </TD>
      </TR>
      <TR class= common style= "display: 'none'">
          <TD class=title>银联编码</TD>
          <TD class= input><input class="common"  name=UniteBankCode3></TD>
      </TR>
  </table>
  <INPUT VALUE="修  改" class= cssButton TYPE=button onclick="upBank();">
  </Div>
  <input type=hidden name=submitType>
  <input type=hidden name=ManageCom>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
