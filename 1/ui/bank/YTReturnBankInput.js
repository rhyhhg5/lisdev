
var showInfo;
var turnPage = new turnPageClass();

function downloadList()
{   
    if(!verifyInput())
    {
    	return false;
    }
    if(fm.Flag.value!= "S" && fm.Flag.value!= "F"&& fm.Flag.value!= "R"){
    	alert("收付费标记填写有误，请重新填写！");
    	return false;
    }
    if(
     fm.BankCode.value!= "7705" && fm.BankCode.value!= "7706" && fm.BankCode.value!= "7707" && fm.BankCode.value!= "7709")
    {
    	alert("银行代码填写有误，请重新填写！");
    	return false;
    }	
    if(!verifyTime())
    {
    	return false;
    }
    fm.action ="YTReturnBankList.jsp";
    fm.submit();
}
function verifyTime(){
	var date = changeDate(getNowDate());
    var flag = true;
	var errorMessage = "";
	var startDate = changeDate(fm.all("StartDate").value);
	var endDate = changeDate(fm.all("endDate").value);
	if(startDate>endDate||endDate>date){
		errorMessage = errorMessage + "时间输入不合理！\n";
		alert(errorMessage);
		flag =  false;
	}
	return flag;
}

function changeDate(str){
	var month = str.substring(5,str.lastIndexOf ("-"));
	var day = str.substring(str.length,str.lastIndexOf ("-")+1);
	var year = str.substring(0,str.indexOf ("-"));
	return Date.parse(month+"/"+day+"/"+year);


}
function beforeSubmit()
{

}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

