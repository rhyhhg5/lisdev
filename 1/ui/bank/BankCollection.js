var showInfo;
var mDebug = "0";

//提交，保存按钮对应操作
function submitForm() {
	if (verifyInput() == false)
		return false;

	var tStartDate = fm.StartDate.value;
	var tEndDate = fm.EndDate.value;
	var tMngCom = fm.ManageCom.value;
	if (dateDiff(tStartDate, tEndDate, "M") > 3) {
		alert("统计期最多为三个月！");
		return false;
	}

	if (dateDiff(tStartDate, tEndDate, "M") < 1) {
		alert("统计止期比统计统计起期早");
		return false;
	}

	var tStrSQL ="select utab.ucomcode , "+
	"db2inst1.DATE_FORMAT(utab.udate,'yyyymmdd'), (select "+                    
	"nvl(sum(f3yrfb.paymoney),0) f7703 from ljfiget f3jfg left join lyreturnfrombank f3yrfb "+
	"on f3jfg.actugetno = f3yrfb.paycode left join Lybanklog f3ybl on f3ybl.Serialno "+
	"= f3yrfb.Serialno where Exists (Select 1 From Licodetrans Where Codetype = 'ManageCom' And Codealias = Utab.Ucomcode And Code = F3jfg.Policycom) and f3jfg.confdate = "+
	"utab.udate and f3ybl.Bankcode = '7703' and f3yrfb.Banksuccflag = '0000' "+        
	"and f3ybl.Logtype = 'F'), (select nvl(sum(f5yrfb.paymoney),0) f7705 from ljfiget f5jfg "+
	"left join lyreturnfrombank f5yrfb on f5jfg.actugetno = f5yrfb.paycode left join "+
	"Lybanklog f5ybl on f5ybl.Serialno = f5yrfb.Serialno where Exists (Select 1 From Licodetrans Where Codetype = 'ManageCom' And Codealias = Utab.Ucomcode And Code = F5jfg.Policycom)  "+   
	"and f5jfg.confdate = utab.udate and f5ybl.Bankcode = '7705' and "+  
	"f5yrfb.Banksuccflag = '0000'   and f5ybl.Logtype = 'F'), (select "+               
	"nvl(sum(f6yrfb.paymoney),0) f7706 from ljfiget f6jfg left join lyreturnfrombank f6yrfb "+
	"on f6jfg.actugetno = f6yrfb.paycode left join Lybanklog f6ybl on f6ybl.Serialno "+
	"= f6yrfb.Serialno where Exists (Select 1 From Licodetrans Where Codetype = 'ManageCom' And Codealias = Utab.Ucomcode And Code = F6jfg.Policycom) and f6jfg.confdate =  "+  
	"utab.udate and f6ybl.Bankcode = '7706' and f6yrfb.Banksuccflag = '0000'  and "+  
	"f6ybl.Logtype = 'F'), " +
	"(select nvl(sum(f7yrfb.paymoney),0) f7707 from ljfiget f7jfg left join lyreturnfrombank f7yrfb "+
	"on f7jfg.actugetno = f7yrfb.paycode left join Lybanklog f7ybl on f7ybl.Serialno "+
	"= f7yrfb.Serialno where Exists (Select 1 From Licodetrans Where Codetype = 'ManageCom' And Codealias = Utab.Ucomcode And Code = F7jfg.Policycom) and f7jfg.confdate =  "+  
	"utab.udate and f7ybl.Bankcode = '7707' and f7yrfb.Banksuccflag = '00'  and "+  
	"f7ybl.Logtype = 'F'), " +
	"(select nvl(sum(s3yrfb.paymoney),0) S7703 from ljtempfeeclass  "+  
	"s3jtfc left join lyreturnfrombank s3yrfb on s3jtfc.tempfeeno = s3yrfb.paycode  "+ 
	"left join Lybanklog s3ybl on s3ybl.Serialno = s3yrfb.Serialno where "+            
	"Exists (Select 1 From Licodetrans Where Codetype = 'ManageCom' And Codealias = Utab.Ucomcode And Code = S3jtfc.Policycom) and s3jtfc.enteraccdate = utab.udate and "+      
	"s3ybl.bankcode = '7703' and s3yrfb.Banksuccflag = '0000'   and s3ybl.logtype =  "+
	"'S'), (select nvl(sum(s5yrfb.paymoney),0) S7705 from ljtempfeeclass s5jtfc left join "+  
	"lyreturnfrombank s5yrfb on s5jtfc.tempfeeno = s5yrfb.paycode left join Lybanklog "+
	"s5ybl on s5ybl.Serialno = s5yrfb.Serialno where Exists (Select 1 From Licodetrans Where Codetype = 'ManageCom' And Codealias = Utab.Ucomcode And Code = S5jtfc.Policycom) "+
	"and s5jtfc.enteraccdate = utab.udate and s5ybl.bankcode = '7705' and "+           
	"s5yrfb.Banksuccflag = '0000'   and s5ybl.logtype = 'S'), (select "+               
	"nvl(sum(s6yrfb.paymoney),0) S7706 from ljtempfeeclass s6jtfc left join lyreturnfrombank "+
	"s6yrfb on s6jtfc.tempfeeno = s6yrfb.paycode left join Lybanklog s6ybl on "+       
	"s6ybl.Serialno = s6yrfb.Serialno where Exists (Select 1 From Licodetrans Where Codetype = 'ManageCom' And Codealias = Utab.Ucomcode And Code = S6jtfc.Policycom) and "+    
	"s6jtfc.enteraccdate = utab.udate and s6ybl.bankcode = '7706' and "+               
	"s6yrfb.Banksuccflag = '0000'   and s6ybl.logtype = 'S')," +
	" (select nvl(sum(s7yrfb.paymoney),0) S7707 from ljtempfeeclass s7jtfc left join lyreturnfrombank "+
	"s7yrfb on s7jtfc.tempfeeno = s7yrfb.paycode left join Lybanklog s7ybl on "+       
	"s7ybl.Serialno = s7yrfb.Serialno where Exists (Select 1 From Licodetrans Where Codetype = 'ManageCom' And Codealias = Utab.Ucomcode And Code = S7jtfc.Policycom) and "+    
	"s7jtfc.enteraccdate = utab.udate and s7ybl.bankcode = '7707' and "+               
	"s7yrfb.Banksuccflag = '00'   and s7ybl.logtype = 'S'),"+
	" (select nvl(sum(a.prem),0) ybt "+
	"from lccont a, lktransstatus b where b.funcflag = '01' and b.polno = a.contno "+  
	"and a.appflag = '1' and a.PolApplyDate = utab.udate and Exists (Select 1 From Licodetrans Where Codetype = 'ManageCom' And Codealias = Utab.Ucomcode And Code = a.Managecom) "+          
	"and b.bankCode = '16' and b.RBankVSMP = '00' and b.Resultbalance = "+
	"'0') from (select (Select Codealias Sapcode From Licodetrans Where Codetype = 'ManageCom' And Code = Jfg.Policycom) Ucomcode, jfg.confdate udate from ljfiget jfg "+  
	"left join lyreturnfrombank yrfb on jfg.actugetno = yrfb.paycode left join "+      
	"Lybanklog ybl on ybl.Serialno = yrfb.Serialno where jfg.policycom like '"+tMngCom+"%' and "+
	"jfg.confdate between '"+tStartDate+"' and '"+tEndDate+"' and ybl.Bankcode in ('7703', "+   
	"'7705', '7706','7707') and ybl.Logtype = 'F' and ( Yrfb.Banksuccflag = '0000' or Yrfb.Banksuccflag = '00') union "+     
	"select (Select Codealias Sapcode From Licodetrans Where Codetype = 'ManageCom' And Code = Jtfc.Policycom) Ucomcode, jtfc.enteraccdate udate from ljtempfeeclass jtfc "+
	"left join lyreturnfrombank yrfb on jtfc.tempfeeno = yrfb.paycode left join "+     
	"Lybanklog ybl on ybl.Serialno = yrfb.Serialno where jtfc.policycom like '"+tMngCom+"%' "+  
	"and jtfc.enteraccdate between '"+tStartDate+"' and '"+tEndDate+"' and ( Yrfb.Banksuccflag = '0000' or Yrfb.Banksuccflag = '00' ) and ybl.Bankcode in ('7703', '7705', '7706','7707') and ybl.Logtype = 'S' union "+
	"select (Select Codealias Sapcode From Licodetrans Where Codetype = 'ManageCom' And Code = a.Managecom) Ucomcode, a.PolApplyDate udate from lccont a, lktransstatus b "+
	"where b.funcflag = '01' and b.polno = a.contno and a.appflag = '1' and "+         
	"a.PolApplyDate between '"+tStartDate+"' and '"+tEndDate+"' and a.managecom like '"+tMngCom+"%' and "+
	"b.bankCode = '16' and b.RBankVSMP = '00' and b.Resultbalance = '0') utab order by ucomcode, udate with ur";

	fm.querySql.value = tStrSQL;

	var oldAction = fm.action;
	fm.action = "BankCollectionSubDown.jsp";
	fm.submit();
	fm.action = oldAction;

	//  var i = 0;
	//  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	//  //showSubmitFrame(mDebug); 
	//	//fm.fmtransact.value = "PRINT";
	//	fm.target = "f1print";
	//	fm.all('op').value = '';
	//	fm.submit();
	//	showInfo.close();

}
