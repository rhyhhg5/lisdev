<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：WriteToFileSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%> 
  <%@page import="com.sinosoft.lis.easyscan.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="java.io.*"%>
  <%@page import="java.util.*"%>


<%
  System.out.println("\n\n---WriteToFileSave Start---");
  
  String DealType=request.getParameter("DealType");
  System.out.println("DealType : "+ DealType);
  MedicalDealSUI MedicalDealSUI = new MedicalDealSUI();
  
  LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
  tLYSendToBankSchema.setSerialNo(request.getParameter("serialNo"));
  System.out.println("serialNo : "+ request.getParameter("serialNo"));
  
  //允许前台录入文件名，现在暂时取消
  TransferData transferData1 = new TransferData();
  transferData1.setNameAndValue("DealType", DealType);
  
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  VData inVData = new VData();
  
  String Content = "";
  String FlagStr = "";
	
	  
	  inVData.add(tLYSendToBankSchema);
	  inVData.add(transferData1);
	  inVData.add(tGlobalInput);
	  //生成文件
	  if (!MedicalDealSUI.submitData(inVData, "WRITE")) {
	    VData rVData = MedicalDealSUI.getResult();
	    Content = " 处理失败，原因是:" + (String)rVData.get(0);
	  	FlagStr = "Fail";
	  }
	  else {
	    Content = " 处理成功! ";
	  	FlagStr = "Success";
	  }
	
	 System.out.println(Content + "\n" + FlagStr + "\n---WriteToFileSave End---\n\n");

/*普通下载txt文件*/ 
  
    %>                      
    <html>
    <script language="javascript">
          parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
    </script>
    </html>
