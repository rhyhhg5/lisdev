<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：AddUniteBankSave.jsp
//程序功能：
//创建日期：2010-08-05
//创建人  ：yuml
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
  <%
    String Content = "";
    String FlagStr = "";
    String strBankCode="";
    
  	System.out.println("Welcome AddUniteBankSave!!");
    //接受页面参数
    String mOperate =request.getParameter("mOperate");
  	String mUniteBankCode = request.getParameter("UniteBankCode");
    String mUniteBankCodeName = request.getParameter("UniteBankCodeName");
    String mBankCode = request.getParameter("BankCode");
    String mBankCodeName = request.getParameter("BankCodeName");
    String mManageCom = request.getParameter("ManageCom");
    
    System.out.println("得到页面传入的参数!!");
    System.out.println("mUniteBankCode :"+mUniteBankCode);
    System.out.println("mUniteBankCodeName :"+mUniteBankCodeName);
    System.out.println("mBankCode :"+mBankCode);
    System.out.println("mBankCodeName :"+mBankCodeName);
    System.out.println("mManageCom :"+mManageCom);
    
    GlobalInput tGlobalInput = new GlobalInput(); 
    tGlobalInput = (GlobalInput)session.getValue("GI");
    
    TransferData inTransferData = new TransferData();
    inTransferData.setNameAndValue("UniteBankCode",mUniteBankCode);
    inTransferData.setNameAndValue("BankCode",mBankCode);
    inTransferData.setNameAndValue("ManageCom",mManageCom);
    inTransferData.setNameAndValue("BankCodeName",mBankCodeName);
    inTransferData.setNameAndValue("UniteBankCodeName",mUniteBankCodeName);
    
    VData aVData = new VData();
    aVData.add(inTransferData);
    aVData.add(tGlobalInput);
    
    AddUniteBankUI tAddUniteBankUI = new AddUniteBankUI();
    System.out.println(" come in the AddUniteBankUI!! ");
    
    if(!tAddUniteBankUI.submitData(aVData,request.getParameter("mOperate")))
    {
    	System.out.println("失败了！！！！！");
    	VData tVData = tAddUniteBankUI.getResult();
    	Content = "银行添加失败"+(String)tVData.get(0);
    	FlagStr = "Fail";
    }
    else
    {
    	Content = "处理成功！";
    	FlagStr = "Succ"; 
    }
    
    
  %>
  
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
//parent.fraInterface.fm.all("sub").disabled=false;
</script>
</html>