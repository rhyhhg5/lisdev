<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：LDCodeInterfaceInput.jsp
//程序功能：查询代码对应关系定义
//创建日期：2006-10-17 20:21
//创建人  ：Wulg
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
//全局变量获取	
String tFlag = request.getParameter("type");
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
String SysName = request.getParameter("SysName");
%>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="LDCodeInterfaceQueryInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LDCodeInterfaceQueryInit.jsp"%>
		<title>代码定义查询</title>
	</head>
	<body onload="initForm();">
		<form name=fm method=post action="">
		<table>
			<tr>
				<td>
    		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divResultGrid);">
    		</td>
				<td class= titleImg>
					查询条件
				</td>
			</tr>
		</table>
		<Div  id= "divResultGrid" style= "display: ''">   
		<table class=common align=center>
			<tr class=common>
				<td class=title>
					代码类型
				</td>
				<td class=input>
					<input class=common type="text" name="CodeType" verify="代码类型|len<20"/>
				</td>
				<td class=title>
					LAS中代码
				</td>
				<td class=input>
					<input class=common type="text" name="InterfaceCode" verify="LAS中代码|len<20"/>
				</td>
				<td class=title>
					代码名称
				</td>
				<td class=input>
					<input class=common type="text" name="CodeName" verify="代码名称|len<120"/>
				</td>
			</tr>
			<tr class=common>
				<td class=title>
					对应代码
				</td>
				<td class=input>
					<input class=common type="text" name="Code" verify="对应代码|len<20"/>
				</td>
			</tr>
		</table>
	  </div>
		<table class=common align=center>
			<tr>
				<td align=right>
					<input class=cssButton type=button value="查询" onclick="return easyQueryClick()"/>
					<input type="hidden" name="SysName" value="<%=SysName%>"/>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td>
    		  <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCodeSearchGrid);">
    		</td>
				<td class= titleImg>
					查询结果
				</td>
			</tr>
		</table>
		<Div  id= "divCodeSearchGrid" style= "display: ''">
		<table class=common align=center>			
			<tr>
				<td>
					<span id="spanSearchGrid" >
	        </span> 
				</td>
			</tr>
		</table>
		<table class=common align=center>
		  <tr>
		    <td>
				  <Div id= "divPage" align=center style= "display: 'none' ">
            <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
            <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
            <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
            <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </Div>
        </td>
      </tr>
    </table>
		</Div>
		<table class=common align=center>
			<tr align=right>
				<td>
					<input class=cssButton type=button value="返回所选结果" onclick="returnParent()"/>
					<input class=cssButton type=button value="返回所选类型" onclick="returnType()"/>
				</td>
			</tr>
		</table>
	</form>
	</body>
</html>