<%
//程序名称：LDCodeInterfaceInputInit.jsp
//程序功能：代码定义初始页面
//创建日期：2006-10-17 15:58
//创建人  ：Wulg
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>  
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
//初始化代码列表
function initCodeGrid()
{                               
    var iArray = new Array();      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           	//列宽
      iArray[0][2]=1;            			  //列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="LAS中代码";         //列名
      iArray[1][1]="40px";      	      //列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=1;                   //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="代码名称";         	//列名
      iArray[2][1]="40px";            	//列宽
      iArray[2][2]=120;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="对应代码";      	  //列名
      iArray[3][1]="40px";              //列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="创建日期";      	  //列名
      iArray[4][1]="40px";              //列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="创建时间";      	  //列名
      iArray[5][1]="40px";              //列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      CodeGrid = new MulLineEnter( "fm" , "CodeGrid" ); 
      //这些属性必须在loadMulLine前
      CodeGrid.mulLineCount = 0;   
      CodeGrid.displayTitle = 1;   
      CodeGrid.loadMulLine(iArray);  
    }
    catch(ex)
    {
      alert(ex);
    }
}

//初始化Form
function initForm()
{
  try
  {
    initCodeGrid();
  }
  catch(re)
  {
    alert("TempFeeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>                            