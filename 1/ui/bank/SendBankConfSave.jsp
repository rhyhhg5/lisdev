<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：WriteToFileSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->


<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.bank.*"%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%
	System.out.print("---SendBankConfSave Start---");

	//提示变量
	String Content = "";
	String FlagStr = "";
	String flag = request.getParameter("flag");
	String buttonflag = request.getParameter("buttonflag");
    String bankcode = request.getParameter("BankCode");
    String banktype = request.getParameter("BankType");
	
	GlobalInput tG = new GlobalInput(); 
    tG = (GlobalInput)session.getValue("GI");
	
    //flag 为 1时 为代收
    if("1".equals(flag))
    {
    	System.out.println("------------   代收执行啦！！！   ------------");
    	LJSPaySchema tLJSPaySchema;
        LJSPaySet tLJSPaySet =new LJSPaySet();
         
        String tLJSNum[] = request.getParameterValues("BankGridNo");
    	String tOtherNo[] = request.getParameterValues("BankGrid1");
    	String tBankCode[] = request.getParameterValues("BankGrid2");
        String tSumDuePayMoney[] = request.getParameterValues("BankGrid3");
        String tBankAccNo[] = request.getParameterValues("BankGrid4");
        String tAccName[] = request.getParameterValues("BankGrid5");
        
        String tManageCom[] = request.getParameterValues("BankGrid6");
        String tChk[] = request.getParameterValues("InpBankGridChk"); 
        for (int index=0; index<tChk.length; index++) {
             if (tChk[index].equals("1")) {     
              tLJSPaySchema=new LJSPaySchema();
    	      tLJSPaySchema.setGetNoticeNo(tOtherNo[index].trim());
    	      tLJSPaySchema.setBankCode(tBankCode[index]);
    	      tLJSPaySchema.setSumDuePayMoney(tSumDuePayMoney[index]);
    	      tLJSPaySchema.setBankAccNo(tBankAccNo[index]);
    	      tLJSPaySchema.setAccName(tAccName[index]);
    	      tLJSPaySchema.setManageCom(tManageCom[index]);
    	      
    	      tLJSPaySet.add(tLJSPaySchema);
    	      
    	      System.out.println("tLJSPaySchema.getOtherNo():::"+tLJSPaySchema.getOtherNo());
        }
       }  
       
        VData tVData = new VData();
    	tVData.add(tLJSPaySet);
        tVData.add(tG);
        tVData.add(buttonflag);
        tVData.add(bankcode);
        tVData.add(banktype);
        
    	
        SendBankConfUI tSendBankConfUI = new SendBankConfUI();
        if (!tSendBankConfUI.submitData(tVData, "WRITE")) {
    			VData rData = tSendBankConfUI.getResult();
    			Content = "处理失败，原因是:" + (String) rData.get(0);
    			FlagStr = "Fail";
    		} else {
    			Content = "处理成功";
    			FlagStr = "Success";
    		}
    }
    else if("2".equals(flag))
    {
    	System.out.println("------------   代付执行啦！！！   ------------");
    	LJAGetSchema tLJAGetSchema;
        LJAGetSet tLJAGetSet =new LJAGetSet();
         
        String tLJSNum[] = request.getParameterValues("BankGridNo");
    	String tOtherNo[] = request.getParameterValues("BankGrid1");
    	String tBankCode[] = request.getParameterValues("BankGrid2");
    	System.out.println("-=-=-=-=-=-=-=--"+tBankCode.length);
        String tSumGetMoney[] = request.getParameterValues("BankGrid3");
        String tBankAccNo[] = request.getParameterValues("BankGrid4");
        String tAccName[] = request.getParameterValues("BankGrid5");
        
        String tManageCom[] = request.getParameterValues("BankGrid6");
        String tChk[] = request.getParameterValues("InpBankGridChk"); 
        for (int index=0; index<tChk.length; index++) {
             if (tChk[index].equals("1")) {     
              tLJAGetSchema=new LJAGetSchema();
              tLJAGetSchema.setOtherNo(tOtherNo[index].trim());
              tLJAGetSchema.setBankCode(tBankCode[index]);
              tLJAGetSchema.setSumGetMoney(tSumGetMoney[index]);
              tLJAGetSchema.setBankAccNo(tBankAccNo[index]);
              tLJAGetSchema.setAccName(tAccName[index]);
              tLJAGetSchema.setManageCom(tManageCom[index]);
              tLJAGetSet.add(tLJAGetSchema);
    	      System.out.println("tLJSPaySchema.getOtherNo():::"+tLJAGetSchema.getBankCode());
        }
       }  
       
        VData tVData = new VData();
    	tVData.add(tLJAGetSet);
        tVData.add(tG);
        tVData.add(buttonflag);
        tVData.add(bankcode);
        tVData.add(banktype);
    	
        SendBankConfFUI tSendBankConfFUI = new SendBankConfFUI();
        if (!tSendBankConfFUI.submitData(tVData, "WRITE")) {
    			VData rData = tSendBankConfFUI.getResult();
    			Content = "处理失败，原因是:" + (String) rData.get(0);
    			FlagStr = "Fail";
    		} else {
    			Content = "处理成功";
    			FlagStr = "Success";
    		}
    }
	
	
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	//parent.fraInterface.fm.all("sub").disabled=false;
</script>
</html>
