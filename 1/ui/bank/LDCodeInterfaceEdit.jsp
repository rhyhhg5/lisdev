<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDCodeInterfaceEdit.jsp
//程序功能：修改代码定义
//创建日期：2006-10-17 19:56
//创建人  ：Wulg
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDCodeInterfaceSchema tLDCodeInterfaceSchema = new LDCodeInterfaceSchema();
  LDCodeInterfaceSet tLDCodeInterfaceSet = new LDCodeInterface();
  XXXXUI tXXXXUI = new XXXXUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  
  String MakeDate = (String)pubFun.getCurrentDate();
  String MakeTime = (String)pubFUn.getCurrentTime();
  String[] chk = request.getParameterValues("CodeGridNo");
  String[] interfaceCode = request.getParameterValues("CodeGrid1");
  String[] codeName = request.getParameterValues("CodeGrid2");
  String[] code = request.getParameterValues("CodeGrid3");
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("开始设置Schema:" + transact);
  for (int i = 0; i < chk.length; i++) {
    LDCodeInterfaceSchema tLDCodeInterfaceSchema = new LDCodeInterfaceSchema();
    tLDCodeInterfaceSchema.setSysName(request.getParameter("SysName"));
    tLDCodeInterfaceSchema.setCodeType(request.getParameter("CodeType"));
    tLDCodeInterfaceSchema.setCodeTypeNmae(request.getParameter("CodeTypeName"));
    tLDCodeInterfaceSchema.setInterfaceCode(InterfaceCode[i]);
    tLDCodeInterfaceSchema.setCode(code[i]);
    tLDCodeInterfaceSchema.setCodeName(codeName[i]);
    tLDCodeInterfaceSchema.setMakeDate(MakeDate);   
    tLDCodeInterfaceSchema.setMakeTime(MakeTime);
    tLDCodeInterfaceSchema.setModifyDate(MakeDate);
    tLDCodeInterfaceSchema.setModifyTime(MakeTime);
    tLDCodeInterfaceSet.add(tLDCodeInterfaceSchema);
  }
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
    System.out.println(tLDCodeInterfaceSchema.encode());
	  tVData.add(tLDCodeInterfaceSet);
  	tVData.add(tG);
    tXXXXUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tXXXXUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
