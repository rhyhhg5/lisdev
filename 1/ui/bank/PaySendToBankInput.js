//PaySendToBankInput.js该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug = "0";
var tFees;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm() {
	if (checkBank() == false) {
		return false;
	}
	if (checkTime() == false) {
		return false;
	}
	if (verifyInput() == false) {
		return false;
	}
	if (fm.all("EndDate").value > cDate) {
		alert("\u7ec8\u6b62\u65e5\u671f\u5927\u4e8e\u5f53\u524d\u65e5\u671f\uff01");
		return false;
	}
	fm.all("sub").disabled = true;
	fm.submit();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Manage, SerialNo) {
	try {
		showInfo.close();
	}
	catch (e) {
	}
	if (Manage != "") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + Manage;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");
	}
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
	showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");  
  
  // 显示提取的清单
	if (SerialNo != "") {
		showList(SerialNo);
	} else {
		BankGrid.clearData("BankGrid");
	}
}    

//初始化银行代码
function initBankCode() {
	var strSql = "select BankCode, BankName from LDBank where ComCode = '" + comCode + "'";
	var strResult = easyQueryVer3(strSql);
  //alert(strResult);
	if (strResult) {
		fm.all("bankCode").CodeData = strResult;
	}
}

// 显示提取的清单
function showList(serialno) {
	// 书写SQL语句
	var strSql = "select paycode, polno, bankcode, paymoney,accno,accname,comcode,serialno from lysendtobank where serialno='" + serialno + "'";
	turnPage.queryModal(strSql, BankGrid);
}
function checkBank() {
	var BankCode = fm.all("BankCode").value;
	var strSql = "select BankCode,Bankname from LDBank where (AgentGetSendF is  null or AgentGetReceiveF is  null or AgentGetSuccFlag is  null) and bankcode = '" + BankCode + "'";
	var strResult = easyQueryVer3(strSql);
	if (strResult) {
		alert("\u60a8\u6240\u9009\u62e9\u7684\u94f6\u884c\u4e0d\u652f\u6301\u4ed8\u6b3e\uff01");
		return false;
	}
}
  // 集中代收付，付费并发控制。在特定的时间段之内无法付款。
function checkTime() {
	var BankCode = fm.all("BankCode").value;
	var sql ="select code,codename from ldcode where codetype = 'sendtime'";
	var result = easyExecSql(sql);
	for(var i=0;i<result.length; i++) {
		if (result[i][0]=='starttime') {
		  var starttime =result[i][1];
		} 
		if (result[i][0]=='endtime') {
		  var endtime =result[i][1];
		}
	}
	
	var strSql = "select 1 from dual where current Time between '" + starttime + "' and '" + endtime + "'"
		 + "and exists (select 1 from ldbankunite where bankunitecode in ('7705', '7706')and  bankcode = '" + BankCode + "')";
    var strResult = easyExecSql(strSql);
    if (strResult) {
		alert("该银行在" + starttime + " 至 " + endtime + " 之间不支持付款！请稍后再试！");
		return false;
	}
}
