<html>
<%
//程序名称：银行代收对帐清单
//程序功能：
//创建日期：2003-3-25
//创建人  ：刘岩松程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="BankYSBillRightInputYBT.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="BankYSBillRightInitYBT.jsp"%>
	</head>

	<body  onload="initForm();" >
  	<form action="./PrintBillYBT.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
    		
    	<Div  id= "divLLReport1" style= "display: ''">
    	<table class=common>
          <tr class= titleImg>
            <TD class= titleImg > 银保通模式代收成功清单 </TD>
          </tr>
        </table>
      	<table  class= common>
        	<TR  class= common>
          	<TD  class= title> *开始日期 </TD>
          	<TD  class= input> <input class="coolDatePicker" dateFormat="short" name="StartDate" > </TD>
          	<TD  class= title> *结束日期 </TD>
          	<TD  class= input> <input class="coolDatePicker" dateFormat="short" name="EndDate" > </TD>
		      </TR>
		     <TR class= common>
		   	  <TD  class= title> *收付费标记  </TD>
					
            <TD  class= code> <Input class=codeno name=Flag verify="收付费标志|NOTNULL"CodeData="0|^YS|代收^YF|代付" ondblClick="showCodeListEx('SXFlag1',[this,SXFlagName],[0,1]);"onkeyup="showCodeListKeyEx('SXFlag1',[this,SXFlagName],[0,1]);"><input class=codename name=SXFlagName readonly=true > </TD>
                   
						<TD> <Input Type=hidden name=SXFlag value="ybt"> </TD>     <!--代收代付标志-->
						<!--<TD> <Input Type=hidden name=Flag> </TD> -->    <!--代收代付标志-->
						<TD> <Input Type= hidden name=TFFlag > </TD> <!--正确清单还是错误清单的标志-->
					</tr>

 					<TR  class= common>
<TD  class= title>
        银行代码
      </TD>
      <TD  class= input>
        <Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank2',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank2',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank" ><input class=codename name=BankCodeName readonly=true >
      </TD>    				
      <TD class= title>  </TD>
    				<TD class=input > <Input class="readonly" readonly name=BankName verify="银行名称|notnull&len<=12" > </TD>
        	</TR>
        	
					
					 <TR  class= common>
            <TD> <input class=common type=button value="列出打印批号" onclick="showSerialNo()"> </TD>
            <TD class=input width="23%"> <input class=common type=button value="打印批单" onclick="billDown()"> </TD>
            <TD > <input type=hidden  name="BillNo" > </TD>
        	</TR>
        	
 				</table>

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanBillGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>
  	</div>
  	
  	   <Div  id= "divLL" style= "display: 'none'">
    	<table class=common> 
					<TR class= common>
						<TD  class= title> 管理机构 </TD>
            <TD  class= input> <Input class= code name=Station ondblclick="return showCodeList('Station',[this]);" onkeyup="return showCodeListKey('Station',[this]);"> </TD>
					</tr>
     </table>		
      </Div>
     
    <input type="hidden" name=serialno value="">
    <input type="hidden" name=bankcode value="">
    <input type="hidden" name=toltalmoney value="">
    <input type="hidden" name=toltalnum value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
