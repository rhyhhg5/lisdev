<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PLPsqs.jsp--打印理赔申请书程序
//程序功能：
//创建人  ：刘岩松
//创建日期：2003-02-14
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
  <%@page import="java.io.*"%>
<%
  System.out.println("开始执行打印操作");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  boolean operFlag=true;

  	String strBillNo = request.getParameter("BillNo");
  	String strBankCode = request.getParameter("BankCode");
  	String strFlag = "YS";
  	//String strMngCom = request.getParameter("Station");
  	String strTFFlag = request.getParameter("TFFlag");
  	String strSXFlag = request.getParameter("SXFlag");
  	
  	GlobalInput tG = new GlobalInput();	
	  tG=(GlobalInput)session.getValue("GI");
  	CErrors mErrors = new CErrors();
  	PrintBillUI tPrintBillUI = new PrintBillUI();
  	XmlExport txmlExport = new XmlExport();
  	System.out.println("要打印的批单号码是＝＝＝＝"+strBillNo);
  	System.out.println("要打印的银行代码是＝＝＝＝"+strBankCode);
  	System.out.println("代收代付标志是＝＝＝＝"+strFlag);
  	//System.out.println("要打印的管理机构是＝＝＝＝"+strMngCom);
		System.out.println("正确错误的标志是＝＝＝＝"+strTFFlag);
		System.out.println("首期续期的标志是＝＝＝＝"+strSXFlag);

  	
  	//定义一个全局变量存储赔案号

  VData tVData = new VData();
  VData mResult = new VData();
  try
  {
    tVData.addElement(strBillNo);
    tVData.addElement(strBankCode);
    tVData.addElement(strFlag);
   // tVData.addElement(strMngCom);
    tVData.addElement(strTFFlag);
    tVData.addElement(strSXFlag);
    tVData.addElement(tG);
    
    //调用批单打印的类
    if (!tPrintBillUI.submitData(tVData,"PRINT")){
	      operFlag=false;
	      Content=tPrintBillUI.mErrors.getFirstError().toString();    
      }
    else{
    		mResult = tPrintBillUI.getResult();
	      txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	      if(txmlExport==null)
	      {
	      	operFlag=false;
	      	Content="没有得到要显示的数据文件";
	      }
    	}
  }
  catch(Exception ex)
  {
    Content = "PRINT"+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
ExeSQL tExeSQL = new ExeSQL();
//获取临时文件名
String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
String strFilePath = tExeSQL.getOneValue(strSql);
String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
//获取存放临时文件的路径
//strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
//String strRealPath = tExeSQL.getOneValue(strSql);
String strRealPath = application.getRealPath("/").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName;
CombineVts tcombineVts = null;
System.out.println("============================");
if (operFlag==true)
{
	//合并VTS文件
	String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
	System.out.println("============================1");
	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
  System.out.println("============================2");
	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
	System.out.println("================3 :"+dataStream);
	tcombineVts.output(dataStream);
System.out.println("============================4");
	//把dataStream存储到磁盘文件
	AccessVtsFile.saveToFile(dataStream,strVFPathName);
	System.out.println("==> Write VTS file to disk ");

	System.out.println("===strVFFileName : "+strVFFileName);
//本来打算采用get方式来传递文件路径
	response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="+strVFPathName);
}
else
{
	FlagStr = "Fail";
 %>
  <html>
  <script language="javascript">
	alert("<%=Content%>");
	top.opener.focus();
	top.close();				
	
</script>
</html>
<%
}
%>