<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：
//程序功能：集中代收付清单直接下载
//创建人  ：
//创建日期：
//更新记录：  
//更新人
//更新日期
//更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bank.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%
  //接收数据并进行封装
  String strStartDate = request.getParameter("StartDate");
  String strEndDate = request.getParameter("EndDate");
  String strBankCode = request.getParameter("BankCode1");//银行代码
  String strManageCom = request.getParameter("ManageCom");//管理机构
  String strFlag = request.getParameter("Flag");//收付费
  String strTFFlag = request.getParameter("TFFlag");//是否正确
  System.out.println("---直接下载---");
  System.out.println("--strBankCode--"+strBankCode);
  System.out.println("tfflag--"+strTFFlag);
  String strSumDuePayMoney = request.getParameter("SumDuePayMoney");//总金额
  String strSumCount = request.getParameter("SumCount");//总笔数
  String strSerialNo = request.getParameter("BillNo");//批次号
  
  //获取文件名称
  Calendar cal = new GregorianCalendar();
  String year = String.valueOf(cal.get(Calendar.YEAR));
  String month=String.valueOf(cal.get(Calendar.MONTH)+1);
  String date=String.valueOf(cal.get(Calendar.DATE));
  String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String now = year + month + date + hour + min + sec + "_" ;
  String  millis = String.valueOf(System.currentTimeMillis());  
  String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  
  
  
  
  //封装到transferData中
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
  tTransferData.setNameAndValue("StartDate", strStartDate);
  tTransferData.setNameAndValue("EndDate", strEndDate);
  tTransferData.setNameAndValue("BankCode", strBankCode);
  tTransferData.setNameAndValue("ManageCom", strManageCom);
  tTransferData.setNameAndValue("Flag", strFlag);
  tTransferData.setNameAndValue("TFFlag", strTFFlag);
  tTransferData.setNameAndValue("SumDuePayMoney", strSumDuePayMoney);
  tTransferData.setNameAndValue("SumCount", strSumCount);
  tTransferData.setNameAndValue("SerialNo", strSerialNo);
  //输出参数
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  boolean operFlag=true;
  VData tVData = new VData();
  GlobalInput tG = (GlobalInput) session.getValue("GI");
  tVData.add(tTransferData);
  tVData.addElement(tG);
  BatchSendDownloadListUI mBatchSendDownloadListUI = new BatchSendDownloadListUI();
  //定义一个报表的类
  XmlExport txmlExport = new XmlExport();
  VData mResult = new VData();
  try
  {
    //调用批单打印的类
    if (!mBatchSendDownloadListUI.submitData(tVData, "PRINT"))
    {
      Content = "报表下载失败，原因是:" + mBatchSendDownloadListUI.mErrors.getFirstError();
      FlagStr = "Fail";
    }
    else
    {
      String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
          File file = new File(tOutXmlPath);
          
          response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[10000];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
              file.delete();
          }
       }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  
  if (!FlagStr.equals("Fail"))
  {
    Content = "";
    FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">  
    alert("<%=Content%>");
    top.close();
</script>
</html>