<html>
<%
//程序名称：续期保费转账不成功清单
//程序功能：
//创建日期：2003-3-25
//创建人  ：刘岩松程序创建
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="XQBankInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="XQBankInit.jsp"%>
	</head>

	<body   >
  	<form action="./XQBankPrint.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>

    	<Div  id= "divLLReport1" style= "display: ''">
        <table class=common>
          <tr class= common>
            <TD  >
              续期保费转账不成功清单
            </TD>
          </tr>
        </table>

      	<table  class=common>
        	<TR  class= common>
          	<TD  class= title>
            	开始日期
          	</TD>
          	<TD  class= input>
            	<input class="coolDatePicker" dateFormat="short" name="StartDate" >
          	</TD>
          	<TD  class= title>
            	结束日期
          	</TD>
          	<TD  class= input>
            	<input class="coolDatePicker" dateFormat="short" name="EndDate" >
          	</TD>
		      </TR>
<!--
 					<TR  class= common>
         		<TD  class= title>
            	银行代码
          	</TD>
          	<TD  class= input>
            	<Input class="code" name=BankCode verify="银行代码|notnull&code:BankCode" ondblclick="return showCodeList('Bank',[this, BankName], [0, 1]);" onkeyup="return showCodeListKey('Bank', [this, BankName], [0, 1]);">
          	</TD>
    				<TD class= title>
    				银行名称
    				</TD>
    				<TD class=input >
          		<Input class="readonly" readonly name=BankName verify="银行名称|notnull&len<=12" >
						</TD>
        	</TR>
-->
					<TR class= common>
					  <TD  class= title>
              管理机构
            </TD>
            <TD  class= input>
	            <Input class= code name=Station ondblclick="return showCodeList('Station',[this]);" onkeyup="return showCodeListKey('Station',[this]);">
						</TD>

						<TD class= title>
					    清单类型
					  </td>
					  <TD  class= code>
  					  <Input class=code name=AgentState verify="清单类型|NOTNULL"CodeData="0|^0|孤儿单^1|在职单" ondblClick="showCodeListEx('AgentState1',[this],[0,1]);"onkeyup="showCodeListKeyEx('AgentState1',[this],[0,1]);">
  				  </TD>
					</tr>

					 <TR  class= common>
            <TD>
          	<input class=common type=button value="打印续期保费转账失败清单" onclick="PXQPremErr()">
          </TD>
            
          <TD>
            	<input type=hidden  name="PremType" >
              <input type=hidden  name="Flag" >
          	</TD>
        	</TR>
 				</table>

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
