<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDClassInfoSave.jsp
//程序功能：
//创建日期：2005-05-30 18:29:02
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  String strLogs=""; 
  LJSPaySchema tLJSPaySchema;
  PausePayFeeUI tPausePayFeeUI   = new PausePayFeeUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	tLJSPaySchema   = new LJSPaySchema();
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    String tChk[] = request.getParameterValues("InpBillGridChk");
    String GetNoticeNo[] = request.getParameterValues("BillGrid1");
    String GetPauseflag[] = request.getParameterValues("BillGrid6");
    tLJSPaySchema.setManageCom(request.getParameter("ManageCom"));
    tLJSPaySchema.setPayDate(request.getParameter("PayDate"));
    tLJSPaySchema.setAgentCode(request.getParameter("AgentCode"));
    tLJSPaySchema.setBankCode(request.getParameter("BankCode"));
    tLJSPaySchema.setOtherNoType(request.getParameter("PauseFeeType"));
    tLJSPaySchema.setBankOnTheWayFlag(request.getParameter("PauseFlag"));
    System.out.println("ManageCom:  "+request.getParameter("ManageCom")); 
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLJSPaySchema);
  	tVData.add(tG);
  	if (transact.equals("Query"))
  	 {
       tPausePayFeeUI.submitData(tVData,transact);
     }    
    else
    {	 
       System.out.println("--------------come in the Pause-------------------"); 
       System.out.println(tChk.length); 
       if (tChk.length == 0)
	         FlagStr="Fail";
	     for(int i= 0;i<tChk.length;i++)
	    {
	     System.out.println("--------------I come in the For---------------");
		   if(tChk[i].equals("0")) continue; 
		   tVData = new VData();
		   tLJSPaySchema   = new LJSPaySchema();
       tPausePayFeeUI  = new PausePayFeeUI();
		   System.out.println("i:  "+i);
       System.out.println("setGetNoticeNo:   "+ GetNoticeNo[i]);
		   System.out.println("setBankOnTheWayFlag:   "+ GetPauseflag[i]);
		   tLJSPaySchema.setGetNoticeNo(GetNoticeNo[i]);
		   System.out.println("^^^^^^^^^^^^^^^^^^^^^^");
	     tLJSPaySchema.setBankOnTheWayFlag(GetPauseflag[i]);
	     System.out.println("======================");
		   System.out.println(transact);
		   tVData.addElement(tLJSPaySchema);
		   tLJSPaySchema = null;
		   tVData.addElement(tG);
		   if(!tPausePayFeeUI.submitData(tVData,transact))
		    {
		     System.out.println("--------------I come in the if-One---------------");
		     System.out.println("if_transact  :"+transact);
			   FlagStr = "Fail";
			   Content=" 通知书号码"+GetNoticeNo[i]+"："+tPausePayFeeUI.mErrors.getFirstError().toString();
			   System.out.println("Content-->"+Content);
			   strLogs=strLogs+Content;
			   continue;
		    }
	    }
	  }
  }
  catch(Exception ex)
  {
  	ex.printStackTrace();
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPausePayFeeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	strLogs = " 操作成功! ";
    	FlagStr = "Success";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=strLogs%>");
</script>
</html>
