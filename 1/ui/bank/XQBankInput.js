/*
  //创建人：刘岩松
  //修改人：刘岩松
  //修改内容：

  //该文件中包含客户端需要处理的函数和事件
*/
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功

function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;

    showInfo.close();
    if (FlagStr == "Fail" )
    {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
}
    else
    {
    	showDiv(inputButton,"false");
    }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}
//打印续期保费转账不成功清单的内容
function PXQPremErr()
{
  if((fm.StartDate.value=="")||(fm.EndDate.value=="")||(fm.StartDate.value=="null")||(fm.EndDate.value=="null"))
  {
    alert("请您输入起始日期和结束日期！！！");
    return;
  }
  if((fm.Station.value=="")||(fm.AgentState.value=="")||(fm.Station.value=="null")||(fm.AgentState.value=="null"))
  {
  	alert("请您录入管理机构和清单类型！");
  	return;
  }
  
  else
  {
    fm.Flag.value = "S";
    fm.PremType.value="X";
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = './XQBankPrint.jsp';
    fm.target="f1print";
    showInfo.close();
    fm.submit();
  }
}
function PXQPremSucc()
{
  if((fm.StartDate.value=="")||(fm.EndDate.value=="")||(fm.StartDate.value=="null")||(fm.EndDate.value=="null"))
  {
    alert("请您输入起始日期和结束日期！！！");
    return;
  }
   if((fm.Station.value=="")||(fm.AgentState.value=="")||(fm.Station.value=="null")||(fm.AgentState.value=="null"))
  {
  	alert("请您录入管理机构和清单类型！");
  	return;
  }
  else
  {
    fm.Flag.value = "S";
    fm.PremType.value="X";
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = './XQBankSuccPrint.jsp';
    fm.target="f1print";
    showInfo.close();
    fm.submit();
  }
}

