<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK"%>
    <%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<title>Insert title here</title>
</head>

<%
  boolean errorFlag = false;
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "金联佣金类代收付状态查询清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  String querySql = request.getParameter("sql");
  ExeSQL tExeSQL = new ExeSQL();
  SSRS tSSRS = tExeSQL.execSQL(querySql.replaceAll("%25","%"));
  System.out.println("-----sql------" + querySql.replaceAll("%25","%"));
  System.out.println("----" + tSSRS.getMaxRow());
  
  String[][] mToExcel = new String[4000][12];
        mToExcel[0][0] = "金联佣金类代收付状态查询";
        mToExcel[1][0] = "批次号";
        mToExcel[1][1] = "付费号";
        mToExcel[1][2] = "机构";
        mToExcel[1][3] = "发盘时间";
        mToExcel[1][4] = "户名";
        mToExcel[1][5] = "账号";
        mToExcel[1][6] = "金额";
        mToExcel[1][7] = "处理状态";
        mToExcel[1][8] = "回盘次数";
        mToExcel[1][9] = "回盘时间";
        mToExcel[1][10] = "失败原因";
        
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row + 1 ][col - 1] = tSSRS.GetText(row, col);
            }
        }        
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }
  //返回客户端
  if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
  out.clear();
    out = pageContext.pushBody();
%>
</html>