<%@page contentType="text/html;charset=GBK"%>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bank.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.text.*"%>
<%@page import="java.util.*"%>

<%
            String Content = "";
            String FlagStr = "";
            String submitType = request.getParameter("submitType");
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput = (GlobalInput) session.getValue("GI");
            
            //生成银行数据
            System.out.println("\n\n---BatchSendAddSave Start---");

            TransferData transferData = new TransferData();
            transferData.setNameAndValue("UniteBankCode", request.getParameter("UniteBankCode"));
            transferData.setNameAndValue("BankCode", request.getParameter("BankCode"));
            transferData.setNameAndValue("BankType", request.getParameter("BankType"));
            transferData.setNameAndValue("upBankCode", request.getParameter("upBankCode"));
            transferData.setNameAndValue("upBankType", request.getParameter("upBankType"));
            transferData.setNameAndValue("UniteBankCode3", request.getParameter("UniteBankCode3"));

            VData tVData = new VData();
            tVData.add(transferData);
            tVData.add(tGlobalInput);
            
            BatchSendAddUI tBatchSendAddUI = new BatchSendAddUI();
            if (!tBatchSendAddUI.submitData(tVData, submitType))
            {
                VData rVData = tBatchSendAddUI.getResult();
                Content = " 处理失败，原因是:" + (String) rVData.get(0);
                FlagStr = "Fail";
            }
            else
            {
                VData rVData = tBatchSendAddUI.getResult();
                Content = "处理成功!";
                FlagStr = "Succ";
            }

            System.out.println(Content + "\n" + FlagStr + "\n" + submitType + "---BatchSendAddSave End---\n\n");
%>
<html>
    <script language="javascript">
    parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>' , '<%=submitType%>');
</script>
</html>
