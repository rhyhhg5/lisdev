//GetSendToBankInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;

//提交，保存按钮对应操作
function submitForm() {
	fm.flag.value = "1";

  if(verifyInput() == false) return false;
  if (fm.all("EndDate").value > cDate )
  {
	 	alert("终止日期大于当前日期！");
	 	return false;
  }
  fm.all("typeFlag").value = "";
  fm.all("sub").disabled=true;
  fm.submit();
}
function submitForm1() {

fm.flag.value = "2";

  if(verifyInput() == false) return false;
  if (fm.all("EndDate").value > cDate )
  {
	 	alert("终止日期大于当前日期！");
	 	return false;
  }
  //fm.all("typeFlag").value = "";
  fm.all("sub").disabled=true;
  fm.submit();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
} 

//初始化银行代码
function initBankCode() {
  var strSql = "select BankCode, BankName from LDBank where operator = 'sys'"; 
  
  var strResult = easyQueryVer3(strSql);

  if (strResult) {
    fm.all("bankCode").CodeData = strResult;
  }
}   
