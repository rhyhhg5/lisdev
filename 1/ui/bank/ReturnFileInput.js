//ReturnFileInput.js该文件中包含客户端需要处理的函数和事件

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;
var mDebug = "0";
var tSelNo = "";
var filePath = "";
var strBankCode ="";
//提交，保存按钮对应操作
function submitForm() {
 fm.buttonflag.value = "Yes";
 for (i=0; i<BankGrid.mulLineCount; i++) {
 if (BankGrid.getChkNo(i)) { 
    
 }
 }
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    fm.submit(); //提交
}

function submitForm1() {
 fm.buttonflag.value = "No";
 for (i=0; i<BankGrid.mulLineCount; i++) {
 if (BankGrid.getChkNo(i)) { 
    
 }
 }
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
//function afterSubmit( FlagStr, content ) {
  //try { showInfo.close(); } catch(e) {}
  
  //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
//} 


//初始化银行代码
function initBankCode() {
  var strSql = "select BankCode, BankName from LDBank where operator = 'ybt' "; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("bankCode").CodeData = strResult;
  }
  
  easyQueryClick();
  easyQueryClick1();
  
}   

//代收查询按钮
function easyQueryClick() {
	// 书写SQL语句
	fm.flag.value = "1";
	//var tSel = BankGrid.getSelNo();
	//var arrReturn = new Array();
	
		//arrReturn = getQueryResult();
		var strSql = "select getnoticeno,BankCode,SumDuePayMoney,BankAccNo,AccName,ManageCom,''  from ljspay a where ManageCom like '" + comCode + "%%' "
	                 + " and cansendbank = '6' "
	                 +"  and exists ( select 1 from ldbankunite where bankunitecode='"
	                 + fm.BankCode.value
	                 +"' and bankcode=a.bankcode)"
				     + " order by SerialNo desc";	     
				    
	//turnPage.queryModal(strSql, BankGrid);
	
	//修改为每页显示30行
	//查询SQL，返回结果字符串
   turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
	//判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    BankGrid.clearData('BankGrid');  
    alert("没有查询到数据！");
    return false;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BankGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  turnPage.pageLineNum=30;
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //turnPage.queryModal(turnPage.strQuerySql, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
	
}
//代付查询按钮
function easyQueryClick1()
{
	//书写SQL语句
	fm.flag.value = "2";
	//var arrReturn = new Array();
	//var tSel = BankGrid.getSelNo();
	
	//arrReturn = getQueryResult();
	var strSql1 = "select OtherNo,BankCode,SumGetMoney,BankAccNo,AccName,ManageCom,actugetno from ljaget a where ManageCom like '"+comCode+"%%' "
				+ " and cansendbank = '6' "
				+	"  and exists ( select 1 from ldbankunite where bankunitecode='"
	      + fm.BankCode.value
	      +	"' and bankcode=a.bankcode)"
				+ " order by SerialNo desc";
	//turnPage.queryModal(strSql1, BankGrid);
	      
	//修改为每页显示30行
	//查询SQL，返回结果字符串
   turnPage.strQueryResult  = easyQueryVer3(strSql1, 1, 1, 1);  
	//判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    BankGrid.clearData('BankGrid');  
    alert("没有查询到数据！");
    return false;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BankGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql1; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  turnPage.pageLineNum=30;
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //turnPage.queryModal(turnPage.strQuerySql, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
}
function afterSubmit( FlagStr, content  )
{    
	  showInfo.close();
      if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,
                    "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    //var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
    easyQueryClick();
  }
 
}
//得到选择数据
function getQueryResult()
{
	var arrSelected = null;
	tRow = BankGrid.getSelNo();
	
	if(tRow == 0 || tRow == null)
	{
		return arrSelected;
	}
	arrSelected = new Array();
	
	arrSelected[0] = new Array();
	arrSelected[0] = BankGrid.getRowColData(tRow - 1);
	
	return arrSelected;
}
