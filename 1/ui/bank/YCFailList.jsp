<html>
<%
  //程序名称：邮储代收代付成功清单
  //程序功能：
  //创建日期：2011-02-28
  //创建人  ：亓莹莹
  //更新记录：  更新人    更新日期     更新原因/内容
 %>
  <%@page contentType="text/html;charset=GBK"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="YCFailList.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="YCSuccessListInit.jsp"%>
</head>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>

<SCRIPT>
  var managecom = <%=tGI.ManageCom%>;
</SCRIPT> 
<body onload="initForm();">
  <form action="./YCSuccessListSave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/InputButton.jsp"%>
  <!-- 显示或隐藏LLReport1的信息 -->
  <Div id="divLLReport1" style="display: ''">
    <table class=common>
      <tr class=titleImg>
		 <td class=titleImg>邮储代收代付失败清单</td>
	  </tr>
	</table>
	<table class=common>
	  <tr class=common>
		 <td class=title>*开始日期</td>
		 <td class=input><input class="coolDatePicker" dateFormat="short" name="StartDate"></td>
		 <td class=title>*结束日期</td>
		 <td class=input><input class="coolDatePicker" dateFormat="short" name="EndDate"></td>
	  </tr>
	  <tr class=common>
		 <td class=title>*收付费标记</td>
 		 <td class=input><Input class=codeno name=Flag verify="收付费标志|NOTNULL" CodeData="0|^YS|代收^YF|代付"ondblClick="showCodeListEx('SXFlag1',[this,SXFlagName],[0,1]);" onkeyup="showCodeListKeyEx('SXFlag1',[this,SXFlagName],[0,1]);"><input class=codename name=SXFlagName readonly=true></td>
		 <td class=title>银行代码</td>
		 <td class=input><Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank2',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank2',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank"><input class=codename name=BankCodeName readonly=true></td>
      </tr>
      <tr class=common>
		 <td class=title>管理机构</td>
		 <td class=input colspan='3'><Input class=codeNo name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true></td>
	  </tr>
      <tr class=common>
		 <td><input class=common type=button value="列出打印批号" onclick="showSerialNo()"></td>
		 <td class=input width="23%"><input class=common type=button value="打印批单"onclick="printBill();"></td>
		 <td><input type=hidden name=BillNo><input type=hidden name=TFFlag><input type=hidden name=SumDuePayMoney><input type=hidden name=SumCount></td>
	  </tr>
	</table>
    <table>
	  <tr>
		 <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" onClick="showPage(this,divLCPol1);"></td>
		 <td class=titleImg>清单列表</td>
	 </tr>
	</table>
	<Div id="divLLReport2" style="display: ''">
		<table class=common>
		  <tr class=common>
			 <td text-align: left colSpan=1><span id="spanYCSuccessListGrid"></span></td>
		  </tr>
		</table>
	<Div id="divPage" align=center style="display: 'none' ">
		<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage2.firstPage();">
		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</Div>
</Div>
		 <input type="hidden" name=SerialNo value="">
		 <input type="hidden" name=BankCode1 value="">
		 <input type="hidden" name=ToltalMoney value="">
		 <input type="hidden" name=ToltalNum value="">
 </form>
 <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
