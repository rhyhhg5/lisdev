<%
//程序名称：ContQueryInit.jsp
//程序功能：
//创建日期：2007-8-23 17:29
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
%>

<script language="JavaScript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var tCurrentDate = "<%=tCurrentDate%>";
  

function initForm()
{
	try
	{
		initInpBox();
		initContGrid();
		
    initElementtype();
	}
	catch(re)
	{
		alert("ProposalPrintInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

//定义为全局变量，提供给displayMultiline使用
var ContGrid;

// 保单信息列表的初始化
function initContGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="印刷号";
		iArray[1][1]="80px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		iArray[1][21]="PrtNo";

		iArray[2]=new Array();
		iArray[2][0]="保险合同号";
		iArray[2][1]="100px";           
		iArray[2][2]=100;            	
		iArray[2][3]=0;         	
		iArray[2][21]="ContNo";

		iArray[3]=new Array();
		iArray[3][0]="管理机构";
		iArray[3][1]="100px";
		iArray[3][2]=200;
		iArray[3][3]=0;
		iArray[3][21]="ManageCom";

		iArray[4]=new Array();
		iArray[4][0]="销售渠道";
		iArray[4][1]="60px";
		iArray[4][2]=200;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="业务员";
		iArray[5][1]="80px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="投保人";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="生效日期";
		iArray[7][1]="70px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="保险止期";
		iArray[8][1]="70px";
		iArray[8][2]=100;
		iArray[8][3]=0;

		iArray[9]=new Array();
		iArray[9][0]="保险费";
		iArray[9][1]="50px";
		iArray[9][2]=100;
		iArray[9][3]=0;

		iArray[10]=new Array();
		iArray[10][0]="录单员";
		iArray[10][1]="50px";
		iArray[10][2]=100;
		iArray[10][3]=0;

		ContGrid = new MulLineEnter( "fm" , "ContGrid" );
		//这些属性必须在loadMulLine前
		ContGrid.mulLineCount = 0;
		ContGrid.displayTitle = 1;
		ContGrid.hiddenPlus = 1;
		ContGrid.hiddenSubtraction = 1;
		ContGrid.canSel = 0;
		ContGrid.canChk = 0;
		ContGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>