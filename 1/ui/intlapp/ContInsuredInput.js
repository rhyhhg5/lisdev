//               该文件中包含客户端需要处理的函数和事件
var mOperate = "";
var showInfo;
var showInfo1;
var mDebug="0";
var cflag = "0";  //问题件操作位置
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();  //套餐信息
var turnPage3 = new turnPageClass();  //套餐明细信息
var turnPage4 = new turnPageClass();  //告知整理
var turnPage5 = new turnPageClass();  //受益人
var turnPage6 = new turnPageClass();

var arrResult;
var Resource;
var approvefalg;
//存放添加动作执行的次数
var addAction = 0;
//暂交费总金额
var sumTempFee = 0.0;
//暂交费信息中交费金额累计
var tempFee = 0.0;
//暂交费分类信息中交费金额累计
var tempClassFee = 0.0;
//单击确定后，该变量置为真，单击添加一笔时，检验该值是否为真，为真继续，然后再将该变量置假
var confirmFlag=false;
//
var arrCardRisk;
//window.onfocus=focuswrap;
var mSwitch = parent.VD.gVSwitch;
//工作流flag
var mWFlag = 0;
//使得从该窗口弹出的窗口能够聚焦

var insuredNo = "";

function focuswrap() {
  myonfocus(showInfo1);
}

//提交，保存按钮对应操作
function submitForm() {
	ChangeDecodeStr();
	}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
	UnChangeDecodeStr();
  try 
  {
    showInfo.close();
  } 
  catch(e) 
  {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  
  if (FlagStr=="Succ" && mWFlag == 0 && LoadFlag!=24) 
  {
    getInsuredInfo();
    
    if(tCardFlag != "8" && "INSERT||CONTINSURED" == fm.fmAction.value && confirm("是否继续录入其他客户？")) 
    {
      fm.SamePersonFlag.checked = false;
      isSamePerson();
      emptyInsured();
      if (fm.ContType.value==2) 
      {
        fm.ContNo.value="";
        fm.ProposalContNo.value="";
      }

      if(fm.InsuredSequencename.value=="第一被保险人资料") 
      {
        //emptyFormElements();
        param="122";
        fm.pagename.value="122";
        fm.SequenceNo.value="2";
        fm.InsuredSequencename.value="第二被保险人资料";
        
        if (scantype== "scan") 
        {
          setFocus();
        }
        //noneedhome();
        return false;
      }
      if(fm.InsuredSequencename.value=="第二被保险人资料") 
      {
        //emptyFormElements();
        param="123";
        fm.pagename.value="123";
        fm.SequenceNo.value="3";
        fm.InsuredSequencename.value="第三被保险人资料";
        if (scantype== "scan") {
          setFocus();
        }
        //noneedhome();
        return false;
      }
      if(fm.InsuredSequencename.value=="第三被保险人资料") 
      {
        //emptyFormElements();
        param="123";
        fm.pagename.value="123";
        fm.SequenceNo.value="4";
        fm.InsuredSequencename.value="第四被保险人资料";
        if (scantype== "scan") 
        {
          setFocus();
        }
        
        return false;
      }
    }
    else if("DELETE||CONTINSURED" == fm.fmAction.value)
    {
      window.location.reload();
    }
    
		if("Confirm" == fm.all('fmAction').value)
		{
		  top.opener.focus();
		  top.opener.initForm();
		  top.close();
		}
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterQuery( FlagStr, content ) {
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
}

//取消按钮对应操作
function cancelForm() {
}

//提交前的校验、计算
function beforeSubmit() {
  //添加操作

}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
  if(cDebug=="1") {
    parent.fraMain.rows = "0,0,50,82,*";
  } else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

//问题件录入
function QuestInput() {

 cContNo = fm.ContNo.value;  //保单号码
//  if(LoadFlag=="2"||LoadFlag=="4"||LoadFlag=="13") {
//    if(mSwitch.getVar( "ProposalGrpContNo" )=="") {
//      alert("尚无集体合同投保单号，请先保存!");
//    } else {
//      window.open("./GrpQuestInputMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&ProposalNo="+cGrpProposalContNo+"&Flag="+cflag,"window2");
      //window.open("./GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+cflag+"&ProposalNo="+cGrpProposalContNo,"window2");
//    }
//  } else {
    if(cContNo == "") {
      alert("尚无该投保单，请先保存!");
    } else {
      window.open("../uw/QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+ LoadFlag+"&AppFlag=2","window1");
    }
//  }
  var cGrpProposalContNo = fm.GrpContNo.value;  //团体保单号码
	//if(cGrpProposalContNo==""||cGrpProposalContNo==null)
	//{
  //		alert("请先选择一个团体主险投保单!");
  //		return ;
  //  }
  //
	//window.open("../uw/GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+LoadFlag,"window2");

}
//问题件查询
function QuestQuery() {
  cContNo = fm.all("ContNo").value;  //保单号码
  if(LoadFlag=="2"||LoadFlag=="4"||LoadFlag=="13") {
    if(mSwitch.getVar( "ProposalGrpContNo" )==""||mSwitch.getVar( "ProposalGrpContNo" )==null) {
      alert("请先选择一个团体主险投保单!");
      return ;
    } else {
      window.open("./GrpQuestQueryMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+LoadFlag);
    }
  } else {
    if(cContNo == "") {
      alert("尚无该投保单，请先保存!");
    } else {
      window.open("../uw/QuestQueryMain.jsp?ContNo="+cContNo+"&Flag="+LoadFlag,"window1");
    }
  }
}
//Click事件，当点击增加图片时触发该函数
function addClick() {
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick() {
	ChangeDecodeStr();
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick() {
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick() {
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow) {
}

/*********************************************************************
 *  删除缓存中被保险人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delInsuredVar() {
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('InsuredNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('PrtNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('GrpContNo');
  } catch(ex) {}
  ;
  //   try{mSwitch.deleteVar('AppntNo');}catch(ex){};
  //   try{mSwitch.deleteVar('ManageCom');}catch(ex){};
  try {
    mSwitch.deleteVar('ExecuteCom');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('FamilyType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('RelationToMainInsure');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('RelationToAppnt');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('AddressNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('SequenceNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Name');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Sex');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Birthday');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('IDType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('IDNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('RgtAddress');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Marriage');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('NativePlace');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('MarriageDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Health');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Stature');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Avoirdupois');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Degree');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('CreditGrade');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('BankCode');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('BankAccNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('AccName');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('JoinCompanyDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('StartWorkDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Position');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Salary');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('OccupationType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('OccupationCode');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('WorkType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('PluralityType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('SmokeFlag');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ContPlanCode');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Operator');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('MakeDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('MakeTime');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ModifyDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ModifyTime');
  } catch(ex) {}
  ;
}

/*********************************************************************
 *  将被保险人信息加入到缓存中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addInsuredVar() {
  try {
    mSwitch.addVar('ContNo','',fm.ContNo.value);
  } catch(ex) {}
  ;
  //alert("ContNo:"+fm.ContNo.value);
  try {
    mSwitch.addVar('InsuredNo','',fm.InsuredNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('PrtNo','',fm.PrtNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);
  } catch(ex) {}
  ;
  //   try{mSwitch.addVar('AppntNo','',fm.AppntNo.value);}catch(ex){};
  //   try{mSwitch.addVar('ManageCom','',fm.ManageCom.value);}catch(ex){};
  try {
    mSwitch.addVar('ExecuteCom','',fm.ExecuteCom.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('FamilyType','',fm.FamilyType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('RelationToMainInsure','',fm.RelationToMainInsure.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('RelationToAppnt','',fm.RelationToAppnt.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('AddressNo','',fm.AddressNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('SequenceNo','',fm.SequenceNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Name','',fm.Name.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Sex','',fm.Sex.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Birthday','',fm.Birthday.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('IDType','',fm.IDType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('IDNo','',fm.IDNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('RgtAddress','',fm.RgtAddress.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Marriage','',fm.Marriage.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('MarriageDate','',fm.MarriageDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Health','',fm.Health.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Stature','',fm.Stature.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Avoirdupois','',fm.Avoirdupois.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Degree','',fm.Degree.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('CreditGrade','',fm.CreditGrade.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('BankCode','',fm.BankCode.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('AccName','',fm.AccName.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('JoinCompanyDate','',fm.JoinCompanyDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('StartWorkDate','',fm.StartWorkDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Position','',fm.Position.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Salary','',fm.Salary.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('NativePlace','',fm.NativePlace.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('OccupationType','',fm.OccupationType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('OccupationCode','',fm.OccupationCode.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('WorkType','',fm.WorkType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('PluralityType','',fm.PluralityType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('SmokeFlag','',fm.SmokeFlag.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ContPlanCode','',fm.ContPlanCode.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Operator','',fm.Operator.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('MakeDate','',fm.MakeDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('MakeTime','',fm.MakeTime.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ModifyDate','',fm.ModifyDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ModifyTime','',fm.ModifyTime.value);
  } catch(ex) {}
  ;
}

/*********************************************************************
 *  添加被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addRecord() {
  if(LoadFlag==1||LoadFlag==3||LoadFlag==5||LoadFlag==6) {
    var tPrtNo=fm.all("PrtNo").value;
    var sqlstr="select SequenceNo from LCInsured where PrtNo='"+tPrtNo+"'";
    arrResult=easyExecSql(sqlstr,1,0);
    if(arrResult!=null) {
      for(var sequencenocout=0; sequencenocout<arrResult.length;sequencenocout++ ) {
        if(fm.SequenceNo.value==arrResult[sequencenocout][0]) {
          alert("已经存在该客户内部号码");
          fm.SequenceNo.focus();
          return false;
        }
      }
    }
  }

  var selectCount = 0;
  for(var i = 0; i < RiskWrapGrid.mulLineCount; i++)
  {
    if(RiskWrapGrid.getChkNo(i))
    {
      selectCount = selectCount + 1;
    }
  }
  if(selectCount == 0)
  {
    alert("套餐责任信息");
    return false;
  }

  if(LoadFlag==1) 
  {
    if(fm.SequenceNo.value == "")
    {
      fm.SequenceNo.focus();
      alert("请选择客户内部号码");
      return false;
    }
    
    if(fm.SequenceNo.value>4) 
    {
      alert("只能添加三个被保险人！");
      fm.SequenceNo.focus();
      return false;
    }
    
    if(fm.RelationToMainInsured.value=="") 
    {
      fm.RelationToMainInsured.focus();
      alert("请填写与第一被保险人关系！");
      return false;
    }
    if(fm.RelationToAppnt.value==""&&fm.SequenceNo.value=="1") {
      fm.RelationToAppnt.focus();
      alert("请填写与投保人关系！！");
      return false;
    }
    if ( fm.SamePersonFlag.checked == true && fm.RelationToAppnt.value != "00")
    {
    	fm.RelationToAppnt.focus();
    	alert("投保人为被保险人本人时，与投保人关系矛盾，请检查！");
      return false;
    }
    if ( fm.SequenceNo.value=="1"&&fm.RelationToMainInsured.value!="00")
    {
    	fm.RelationToMainInsured.focus();
    	alert("客户内部号码与第一被保险人关系矛盾，请检查！");
      return false;
    }

  }
  
  if(!checkNative()){
      return false;
  }
  
//增加：第一被保险人资料中证件信息校验   ranguo 20170818
  var tIDType=fm.IDType.value;
  var tIDNo=fm.IDNo.value;
  var tBirthday=fm.Birthday.value;
  var tSex=fm.Sex.value;
  var checkResult=checkIdNo(tIDType,tIDNo,tBirthday,tSex);
  if(checkResult!=null && checkResult!=""){
	  alert(checkResult);
	 return false;
 }
  
  if(!checksex())
  {
    return false;
  }
  if (fm.all('PolTypeFlag').value==0) 
  {
    if( verifyInput2() == false )
      return false;
  }
  
  if(fm.all('IDType').value=="0") 
  {
    var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
    if (strChkIdNo != "") 
    {
      alert(strChkIdNo);
      return false;
    }
  }
  //if(!checkself()) return false;
  if(!checkrelation())
  {
    return false;
  }
  
  if(LoadFlag==9) 
  {
    if(!checkifnoname())
    {
      return false;
    }
    if(!checkenough())
    {
      return false;
    }
  }
  
    // 保障状况告知;
    if (InputMuline1() == false)
    {
        return false;
    }

    if(!ImpartCheck119())
    {
        return false;
    }
  
  if(!checkBnf())
  {
    return false;
  }
  
  if(!checkOccupationType())
  {
    return false;
  }
  
  if(!checkAllInsu()){
    return false;
  }
  
  fm.all('ContType').value=ContType;
  fm.all( 'BQFlag' ).value = BQFlag;
  fm.all('fmAction').value="INSERT||CONTINSURED";
  ChangeDecodeStr();
  
  showSubmiInfo();
  fm.action="./ContInsuredSave.jsp?oldContNo="+oldContNo;
  fm.submit();
  
}

/*********************************************************************
 *  修改被选中的被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function modifyRecord() 
{
  if(!checkAllSelectBox())
  {
    return false;
  }
//增加校验：第一被保险人资料中证件信息校验   ranguo 20170818 
  var tIDType=fm.IDType.value;
  var tIDNo=fm.IDNo.value;
  var tBirthday=fm.Birthday.value;
  var tSex=fm.Sex.value;
  var checkResult=checkIdNo(tIDType,tIDNo,tBirthday,tSex);
  if(checkResult!=null && checkResult!=""){
	alert(checkResult);
	return false;
  }
  if (fm.all('PolTypeFlag').value==0 && LoadFlag!=24) {
    if( verifyInput2() == false )
      return false;
  }
  
  if(!checkNative()){
      return false;
  }

  // if(!checkself()) return false;
  if(!checksex())
    return false;
  if (fm.Name.value=='') {
    alert("请选中需要修改的客户！")
    return false;
  }
  
  var strSql = "select ContType from lccont where contno ='"+fm.ProposalContNo.value+"'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
  	if(arr[0][0]==1)
  	{
  		if(fm.SequenceNo.value == null || fm.SequenceNo.value == "")
  		{
  			alert("请选择是第几个被保险人!");
  			return false;
  		}
  		var insuredCount = InsuredGrid.mulLineCount;
  		var checkCount = fm.SequenceNo.value;
  		//认为如果mulline中的客户数大于等于SequenceNo 才可以修改，否则应该是保存
  		if(insuredCount<checkCount)
  		{
  			alert("请先保存客户信息才能修改！");
  			return false;
  		}
  	}
  }

    // 保障状况告知;
    if (InputMuline1() == false)
    {
        return false;
    }
    
	if(!ImpartCheck119())
	{
	   return false;
	}

  
  if(!checkBnf())
  {
    return false;
  }
  
  if(!checkOccupationType())
  {
    return false;
  }
  
  if(!checkAllInsu()){
    return false;
  }
  
  showSubmiInfo();
  
  fm.all('ContType').value=ContType;
  fm.all('fmAction').value="UPDATE||CONTINSURED";
  if(LoadFlag=="24"){
		var strSQL = "select 1 from lobgrpcont where PrtNo='"+fm.PrtNo.value+"'";
		var arr = easyExecSql(strSQL);
	  	if(!arr){
		  	if(checkCanModify()){
		  		fm.action="./ContInsuredSave.jsp?LoadFlag=2";
		  		ChangeDecodeStr();
		  		fm.submit();
				}
			}else{
				ChangeDecodeStr();
				fm.submit();
			}
	}else{
		ChangeDecodeStr();
	  fm.submit();
	}

}

//校验受益人是否符合业务要求
function checkBnf()
{
  if(!BnfGrid.checkValue())
  {
    return false;
  }
  
  //对同一受益类别同一受益顺序的受益人的受益份额求和
  var tArray = new Array();
  for(var i = 0; i < BnfGrid.mulLineCount; i++)
  {
    var tBnfType = BnfGrid.getRowColDataByName(i, "BnfType");  //受益类别
    var tBnfGrade = BnfGrid.getRowColDataByName(i, "BnfGrade");  //受益顺序
    var tIndex = tBnfType + tBnfGrade;  //注意，tIndex的第一位为“受益类别”，第二位为“受益顺序”，不能先转换为int再相加
    var tIndexInt = parseInt(tIndex);
    if(tArray[tIndexInt] == undefined)
    {
      tArray[tIndexInt] = BnfGrid.getRowColDataByName(i, "BnfLot");
    }
    else
    {
      var old = parseFloat(tArray[tIndexInt]);
      var nw = parseFloat(BnfGrid.getRowColDataByName(i, "BnfLot"));
      tArray[tIndexInt] = old + nw;
    }
    
  //增加  受益人身份证号校验  ranguo 20170824
    var IDType=BnfGrid.getRowColData(i,3);
    var IDNo=BnfGrid.getRowColData(i,4);
    var Sex=BnfGrid.getRowColData(i,5);
    var checkResult=checkIdNo(IDType,IDNo,"",Sex);
    if(checkResult!=null && checkResult!=""){
  	  var j = i+1;
  	  alert("第"+j+"行 "+BnfGrid.getRowColData(i,2)+" "+checkResult);
  	  return false;
    }
    
    //添加受益人关于证件号为身份证号的校验        by zhangyang 2011-04-20
    var tBnfInfo = BnfGrid.getRowData(i);
    var tIDType = tBnfInfo[2];
    var tIDNo = tBnfInfo[3];
    var tBnfSex = tBnfInfo[4];
    if(tIDType == "0")
    {
    	var tBirthday = getBirthdatByIdNo(tIDNo);
    	var strChkIdNo = chkIdNo(trim(tIDNo),trim(tBirthday),trim(tBnfSex));
    	if (strChkIdNo != "") 
    	{
      		alert("受益人" + strChkIdNo);
      		return false;
    	}
    }
    
    //------------------------------------------------
  }
  
  //同一受益类别同一受益顺序的受益人的受益份额之和应该等于1
  for(var tIndex = 0; tIndex < tArray.length; tIndex++)
  {
    if(tArray[tIndex] == undefined)
    {
      continue;
    }
    
    if(tArray[tIndex] != 1)
    {
      var tIndexStr = "" + tIndex;
      if(tIndex < 10)
      {
        tIndexStr = "0" + tIndex;
      }
      var tCue = "受益类别为" + tIndexStr.charAt(0) + "，受益顺序为" 
        + tIndexStr.charAt(1) + " 的受益人受益份额之和应该等于1";
      alert(tCue);
      return false;
    }
  }
  
  return true;
}

function checkOccupationType()
{
    if(fm.OccupationCode.value != "")
    {
        var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+fm.OccupationCode.value+"'";
	    var arr = easyExecSql(strSql);
	    if( arr ) {
	        fm.OccupationType.value = arr[0][0];
	    }
    }else
    {
        fm.OccupationType.value = '';
    }
    return true;    
}

//校验选择框是否选中，包括被保人，套餐，责任
//必须全选才返回true
function checkAllSelectBox()
{
  if(InsuredGrid.getSelNo() == 0)
  {
    alert("请选择被保人!");
    return false;
  }
  
  var selectedCount = 0;
  for(var i = 0; i < RiskWrapGrid.mulLineCount; i++)
  {
    if(RiskWrapGrid.getChkNo(i))
    {
      selectedCount++;
    }
  }
  if(selectedCount == 0)
  {
    alert("请选择责任信息");
    return false;
  }
  
  return true;
}

//显示提交操作时的提示框
function showSubmiInfo()
{
  var showStr="正在提交操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
}

/*********************************************************************
 *  删除被选中的被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteRecord() {
  if (fm.InsuredNo.value=='') {
    alert("请选中需要删除的客户！")
    return false;
  }
  if (InsuredGrid.mulLineCount==0) {
    alert("该被保险人还没有保存，无法进行删除！");
    return false;
  }
//  if (fm.InsuredNo.value==''&&fm.AddressNo.value!='') {
//    alert("被保险人客户号为空，不能有地址号码");
//    return false;
//  }
  if(!confirm("是否要删除此被保险人？删除请点击“确定”，否则点“取消”。"))
  {
  	return;
  }
  fm.all('ContType').value=ContType;
  fm.all('fmAction').value="DELETE||CONTINSURED";
  
  showSubmiInfo();
  fm.submit();
}
/*********************************************************************
 *  返回上一页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function returnparent() {
  var backstr=fm.all("ContNo").value;

  var sql = "select PolApplyDate from LCCont where ContNo = '" + backstr + "' ";
  var rs = easyExecSql(sql);

  mSwitch.addVar("PolNo", "", backstr);
  mSwitch.updateVar("PolNo", "", backstr);
  var cVideoFlag = fm.VideoFlag.value;
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) {}
  ;
  
  //if(LoadFlag=="1"||LoadFlag=="3") {
    //alert(fm.all("PrtNo").value);
    var tUrl = "ContInput.jsp?LoadFlag="+ LoadFlag + "&prtNo=" + fm.all("PrtNo").value+"&scantype="+scantype+"&ScanFlag="+ScanFlag+"&MissionID=" + tMissionID+ "&SubMissionID=" + tSubMissionID+"&ContNo="+ContNo+"&ActivityID="+mSwitch.getVar("ActivityID") + "&PolApplyDate=" + rs + "&CardFlag=" + tCardFlag + "&IntlFlag=" + tIntlFlag+"&VideoFlag="+cVideoFlag+"&XSFlag=Y";
    location.href=tUrl;
}
/*********************************************************************
 *  进入险种计划界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function grpRiskPlanInfo() {
  var newWindow = window.open("../app/GroupRiskPlan.jsp");
}
/*********************************************************************
 *  代码选择后触发时间
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ,InputObj) {

  if(cCodeName=="AddressNo") {
    getdetailaddress()
  }

  try {
    if(cCodeName == "Relation") {
      if(fm.all('RelationToAppnt').value=="00") {
        fm.all('SamePersonFlag').checked=true;
        displayissameperson();
        isSamePerson();
      }
      if(fm.SamePersonFlag.checked == true && fm.all('RelationToAppnt').value != "00")
      {
        alert("您已勾选投保人作为被保人，与投保人关系只能为'本人'");
        fm.all('RelationToAppnt').value = "00"
        return false;
      }
    }
    //如果是无名单
    if( cCodeName == "PolTypeFlag") {
      DivGrpNoname.style.display='';
      if (Field.value!='0') {
        fm.all('InsuredPeoples').readOnly=false;
        fm.all('InsuredAppAge').readOnly=false;
      } else {
        fm.all('InsuredPeoples').readOnly=true;
        fm.all('InsuredAppAge').readOnly=true;
      }
      if(Field.value=='1') {
        FillNoName();
      }
    }
    if( cCodeName == "SequenceNo") 
    {
      emptyInsured();
      
      //if(fm.SamePersonFlag.checked==false) 
      //{
        var mulLineCount = InsuredGrid.mulLineCount;
        for(i = 1; i < Field.value; i++)
        {
          if(InsuredGrid.mulLineCount == (i - 1))
          {
            alert("请先添加第" + i + "被保人");
            fm.SequenceNo.value=i;
            return false;
          }
        }
        setInsuredSequencename(i);
        //noneedhome();
        param="123";
        fm.pagename.value="123";
      //}
       
      if (scantype== "scan") {
        setFocus();
      }
    }
    if( cCodeName == "CheckPostalAddress") {
    var PostalAddressSQL="select PostalAddress from lcaddress where customerno in(select appntno from lccont where prtno ='"+fm.all( 'PrtNo' ).value+"')  and AddressNo =(select char(max(int(AddressNo))) from LCAddress where customerno in(select appntno from lccont where prtno='"+fm.all( 'PrtNo' ).value+"'))";
    var sqlstr=easyExecSql(PostalAddressSQL,1,0);
    if(sqlstr){
			//alert();
    	if(fm.CheckPostalAddress.value=="1") {
    		fm.all('PostalAddress').value=sqlstr;
    		fm.all('ZipCode').value="";
    		}else if(fm.CheckPostalAddress.value=="2"){
    		fm.all('PostalAddress').value="";
    		fm.all('ZipCode').value="";    		
    		}
  		}else{
    	fm.all('PostalAddress').value="";
    	fm.all('ZipCode').value="";    
    	}  
    }
    if(cCodeName=="OccupationCode") {
      //alert();
      var t = Field.value;
      var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+t+"'";
      //alert(strSql);
      var arr = easyExecSql(strSql);
      if( arr ) {
        fm.OccupationType.value = arr[0][0];
      }
    }
    if(cCodeName=="AccountNo") {
    	if(Field.value=="1") {
		    if(mSwitch.getVar("AppntBankAccNo"))
		      fm.all('BankAccNo').value=mSwitch.getVar("AppntBankAccNo");
		    if(mSwitch.getVar("AppntBankCode"))
		      fm.all('BankCode').value=mSwitch.getVar("AppntBankCode");
		    if(mSwitch.getVar("AppntAccName"))
		      fm.all('AccName').value=mSwitch.getVar("AppntAccName");
		   }
	  }
	  else if (cCodeName == "customertype") {
      if (Field.value == "A") {
        if(ContType!="2") {
          var sql = " select lca.AppntName, lca.IDType, lca.IDNo, lca.AppntSex , "
                  + " lca.nativeplace ,lca.OccupationCode, case when lcd.mobile is not null then lcd.mobile else lcd.phone end ,"
                  + " lcd.Postaladdress , lca.idstartdate , lca.idenddate "
                  + " from LCAppnt lca,lcaddress lcd where lca.ContNo = '" + fm.all("ContNo").value + "' "
                  + " and lca.appntno=lcd.customerno and lca.addressno=lcd.addressno ";
          var rs = easyExecSql(sql);
          if(rs == null)
          {
            alert("没有查询到投保人信息");
            return false;
          }
          
          var index = BnfGrid.mulLineCount;
          if(BnfGrid.getRowColData(index-1,1)=="1"&&fm.RelationToAppnt.value=="00"){
              alert("身故受益人不能选择被保险人本人");
              BnfGrid.setRowColData(index-1, 2, "");
              BnfGrid.setRowColData(index-1, 3, "");
              BnfGrid.setRowColData(index-1, 4, "");
              BnfGrid.setRowColData(index-1, 5, "");
              BnfGrid.setRowColData(index-1, 6, "");
              BnfGrid.setRowColData(index-1, 7, "");
              BnfGrid.setRowColData(index-1, 8, "");
              BnfGrid.setRowColData(index-1, 9, "");
              BnfGrid.setRowColData(index-1, 10, "");
              BnfGrid.setRowColData(index-1, 11, "");
              BnfGrid.setRowColData(index-1, 12, "");
              BnfGrid.setRowColData(index-1, 13, "");
              BnfGrid.setRowColData(index-1, 14, "");
          }else{
              BnfGrid.setRowColData(index-1, 2, rs[0][0]);
              BnfGrid.setRowColData(index-1, 3, rs[0][1]);
              BnfGrid.setRowColData(index-1, 4, rs[0][2]);
              BnfGrid.setRowColData(index-1, 5, rs[0][3]);
              BnfGrid.setRowColData(index-1, 9, rs[0][4]);
              BnfGrid.setRowColData(index-1, 10, rs[0][5]);
              BnfGrid.setRowColData(index-1, 11, rs[0][6]);
              BnfGrid.setRowColData(index-1, 12, rs[0][7]);
              BnfGrid.setRowColData(index-1, 13, rs[0][8]);
              BnfGrid.setRowColData(index-1, 14, rs[0][9]);
              //BnfGrid.setRowColData(index-1, 6, fm.all("AppntRelationToInsured").value);
              BnfGrid.setRowColData(index-1, 15, "A");
          }
          
        } else {
          alert("投保人为团体，不能作为受益人！")
          var index = BnfGrid.mulLineCount;
          BnfGrid.setRowColData(index-1, 2, "");
          BnfGrid.setRowColData(index-1, 3, "");
          BnfGrid.setRowColData(index-1, 4, "");
          BnfGrid.setRowColData(index-1, 5, "");
          BnfGrid.setRowColData(index-1, 8, "");
          BnfGrid.setRowColData(index-1, 9, "");
          BnfGrid.setRowColData(index-1, 10, "");
          BnfGrid.setRowColData(index-1, 11, "");
          BnfGrid.setRowColData(index-1, 12, "");
          BnfGrid.setRowColData(index-1, 13, "");
          BnfGrid.setRowColData(index-1, 14, "");
        }
      } else if (Field.value == "I") {
        var index = BnfGrid.mulLineCount;
        if(BnfGrid.getRowColData(index-1,1)=="1")
        {
          alert("身故受益人不能选择被保险人本人");
          BnfGrid.setRowColData(index-1, 2, "");
          BnfGrid.setRowColData(index-1, 3, "");
          BnfGrid.setRowColData(index-1, 4, "");
          BnfGrid.setRowColData(index-1, 5, "");
          BnfGrid.setRowColData(index-1, 6, "");
          BnfGrid.setRowColData(index-1, 7, "");
          BnfGrid.setRowColData(index-1, 8, "");
          BnfGrid.setRowColData(index-1, 9, "");
          BnfGrid.setRowColData(index-1, 10, "");
          BnfGrid.setRowColData(index-1, 11, "");
          BnfGrid.setRowColData(index-1, 12, "");
          BnfGrid.setRowColData(index-1, 13, "");
          BnfGrid.setRowColData(index-1, 14, "");
        }
        else
        {
          BnfGrid.setRowColData(index-1, 2, fm.all("Name").value);
          BnfGrid.setRowColData(index-1, 3, fm.all("IDType").value);
          BnfGrid.setRowColData(index-1, 4, fm.all("IDNo").value);
          BnfGrid.setRowColData(index-1, 5, fm.all("Sex").value);
          BnfGrid.setRowColData(index-1, 6, "00");
          BnfGrid.setRowColData(index-1, 9, fm.all("NativePlace").value);
          BnfGrid.setRowColData(index-1, 10, fm.all("OccupationCode").value);
          var mobile = fm.all("Mobile").value;
          if(mobile==""){
             mobile=fm.all("Phone").value;
          }
          BnfGrid.setRowColData(index-1, 11, mobile);
          BnfGrid.setRowColData(index-1, 12, fm.all("PostalAddress").value);
          BnfGrid.setRowColData(index-1, 13, fm.all("IDStartDate").value);
          BnfGrid.setRowColData(index-1, 14, fm.all("IDEndDate").value);
          BnfGrid.setRowColData(index-1, 15, "I");
        }
      }
    }
    if(cCodeName=="NativePlace"&&InputObj.name=="NativePlace"){
	   if(Field.value == "OS"){
	      fm.all("NativePlace1").style.display = "";
	      fm.all("NativeCity1").style.display = "";
	      fm.all("NativeCityTitle").innerHTML="国家";
	   }else if(Field.value == "HK"){
	      fm.all("NativePlace1").style.display = "";
	      fm.all("NativeCity1").style.display = "";
	      fm.all("NativeCityTitle").innerHTML="地区";
	   }else{
	      fm.all("NativePlace1").style.display = "none";
	      fm.all("NativeCity1").style.display = "none";
	   }
	   fm.NativeCity.value="";
	   fm.NativeCityName.value="";
	}
    
  } catch(ex) {}

}

//根据传入的被保人序号代码，显示被保人序号汉字
function setInsuredSequencename(seqNo)
{
  var seqName = "一二三四五";
  if(seqName.indexOf(seqNo) > -1)
  {
    fm.InsuredSequencename.value="第" + seqNo + "被保险人资料";
  }
  else
  {
    fm.InsuredSequencename.value="第" + seqName.substring(seqNo - 1, seqNo) + "被保险人资料";
  }
}

/*********************************************************************
 *  显示家庭单下被保险人的信息
 *  返回值：  无
 *********************************************************************
 */
function getInsuredInfo() 
{
  var ContNo=fm.all("ContNo").value;

  if(ContNo!=null&&ContNo!="") 
  {
    var tSelNo = InsuredGrid.getSelNo();  //当前已选择中的行
    
    var strSQL ="select InsuredNo,Name,Sex,Birthday,RelationToMainInsured,SequenceNo "
                + "from LCInsured "
                + "where ContNo='"+ContNo+"' "
                + "order by SequenceNo ";
    turnPage.queryModal(strSQL,InsuredGrid);
    
    if(tCardFlag == "8" && InsuredGrid.mulLineCount > 0)				
    {
      InsuredGrid.radioBoxSel(1);
      queryInsrued();
    }
    
    //默认选中第一个被保人
    if(InsuredGrid.mulLineCount > 0)
    {
      if(tSelNo == 0)
      {
        ++tSelNo;
      }
      InsuredGrid.radioBoxSel(tSelNo);
      queryInsrued();
    }
  }
}

/*********************************************************************
 *  获得个单合同的被保人信息
 *  返回值：  无
 *********************************************************************
 */
function queryInsrued() 
{
  emptyInsured();
  
  var row = InsuredGrid.getSelNo();
  
  if(row == 0)
  {
    return;
  }
  
  setInsuredSequencename(row);
  
  insuredNo = InsuredGrid.getRowColDataByName(row -1, "InsuredNo");

  var tContNo=fm.all("ContNo").value;
  var StrSQL="select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate,NativeCity,'',(select Authorization from ldperson where customerno=LcInsured.insuredno) "
						+"from LcInsured "
						+ "where ContNo='"+tContNo+"' "
						+ "   and InsuredNo = '" + insuredNo + "' ";		
  arrResult=easyExecSql(StrSQL,1,0);
  

  if(arrResult==null) 
  {
    //alert("未得到被投保人信息");
    return;
  } 
  else 
  {
    DisplayInsured();//该合同下的被投保人信息
    var tCustomerNo = arrResult[0][2];		// 得到投保人客户号
    var tAddressNo = arrResult[0][10]; 		// 得到投保人地址号
    arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo='"+tCustomerNo+"'",1,0);
    if(arrResult==null) 
    {
      //alert("未得到用户信息");
      //return;
    } 
    else 
    {
      //displayAppnt();       //显示投保人详细内容
      emptyUndefined();
      fm.AddressNo.value=tAddressNo;
      getdetailaddress();//显示投保人地址详细内容
    }
  }
  //getInsuredPolInfo();
  
  checkWrapInfo(insuredNo);
  
  queryICDDisDesb(insuredNo);
}

//查询告知信息
function queryICDDisDesb(insuredNo)
{
  var sql = "select distinct a.customerno, c.name, a.disdesb, a.disresult, a.icdcode, "
          + "   a.SerialNo, b.ICDName, a.DiseaseCode, b.DisText, a.Remark "
					+ "from LCDiseaseResult a, LDUWAddFee b, LCInsured c "
					+ "where a.CustomerNo = c.InsuredNo "
					+ "    and a.DiseaseCode = b.DiseaseCode "
					+ "    and c.ContNo = '" + fm.ContNo.value + "' "
					+ "    and a.CustomerNo = '" + insuredNo + "' "
					+ " order by SerialNo " ;	
	turnPage4.pageDivName = "divPage4";
	turnPage4.queryModal(sql, DisDesbGrid);
}

//选中已录入套餐
function checkWrapInfo(insuredNo)
{
  //为清空已选复选框，进行查询操作
  queryWrapGrip();
  
  var sql = "select distinct RiskWrapCode, RiskCode, DutyCode "
            + "from LCRiskDutyWrap "
            + "where PrtNo = '" + fm.PrtNo.value + "' "
            + "   and InsuredNo = '" + insuredNo + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    //被保人的套餐
    for(var i = 0; i < rs.length; i++)
    {
      //系统所有套餐
      for(var j = 0; j < WrapGrid.mulLineCount; j++)
      {
        if(rs[i][0] == WrapGrid.getRowColDataByName(j, "RiskWrapCode"))
        {
          WrapGrid.checkBoxSel(j+1);
        }
      }
    }
    
    queryWrapDetail();
  }
}

/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayCustomer() {
  try {
    fm.all('Nationality').value= arrResult[0][8];
  } catch(ex) {}
  ;

}
/*********************************************************************
 *  把查询返回的客户地址数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayAddress() {
  try {
    fm.all('PostalAddress').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= arrResult[0][14];
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= arrResult[0][16];
  } catch(ex) {}
  ;
  try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){};
  try {
    fm.all('GrpPhone').value= arrResult[0][12];
  } catch(ex) {}
  ;
  try {
    fm.all('CompanyAddress').value= arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][11];
  } catch(ex) {}
  ;
}
/*********************************************************************
 *  显示被保人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayInsured() {
  try {
    fm.all('GrpContNo').value=arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('ContNo').value=arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('InsuredNo').value=arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('PrtNo').value=arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('AppntNo').value=arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('ManageCom').value=arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('ExecuteCom').value=arrResult[0][6];
  } catch(ex) {}
  ;
  try {
    fm.all('FamilyID').value=arrResult[0][7];
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToMainInsured').value=arrResult[0][8];
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToAppnt').value=arrResult[0][9];
  } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value=arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    if(arrResult[0][11] == null || arrResult[0][11] == "")
    {
      arrResult[0][11] = "1";
    }
    fm.all('SequenceNo').value=arrResult[0][11];
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value=arrResult[0][12];
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value=arrResult[0][13];fm.all('SexName').value=easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'sex' and Code='"+arrResult[0][13]+"'");
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value=arrResult[0][14];
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value=arrResult[0][15];
    var sql = "select CodeName('idtype', '" + fm.all('IDType').value + "') from dual ";
    var rs = easyExecSql(sql);
    {
      if(rs)
      {
        fm.all('IDTypeName').value = rs[0][0];
      }
    }
  } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value=arrResult[0][16];
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value=arrResult[0][17];
    var arr = easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'nativeplace' and Code='"+arrResult[0][17]+"'");
    if(arr)
    fm.all('NativePlaceName').value=arr;
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value=arrResult[0][18];
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value=arrResult[0][19];
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value=arrResult[0][20];fm.all('MarriageName').value=easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'marriage' and Code='"+arrResult[0][20]+"'");
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value=arrResult[0][21];
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value=arrResult[0][22];
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value=arrResult[0][23];
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value=arrResult[0][24];
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value=arrResult[0][25];
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value=arrResult[0][26];
  } catch(ex) {}
  ;
  try {
    fm.all('BankCode').value=arrResult[0][27];
  } catch(ex) {}
  ;
  try {
    fm.all('BankAccNo').value=arrResult[0][28];
  } catch(ex) {}
  ;
  try {
    fm.all('AccName').value=arrResult[0][29];
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value=arrResult[0][30];
  } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value=arrResult[0][31];
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value=arrResult[0][32];
  } catch(ex) {}
  ;
  try {
  if(arrResult[0][33]=='-1'){
	try{fm.all('Salary').value=""; }catch(ex){};               
	}else{
	try{fm.all('Salary').value= arrResult[0][33]; }catch(ex){};
	}
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value=arrResult[0][34];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value=arrResult[0][35];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][35]);
  } catch(ex) { }
  ;
  try {
    fm.all('WorkType').value=arrResult[0][36];
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value=arrResult[0][37];
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value=arrResult[0][38];
  } catch(ex) {}
  ;
  try {
    fm.all('ContPlanCode').value=arrResult[0][39];
  } catch(ex) {}
  ;
  try {
    fm.all('Operator').value=arrResult[0][40];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value=arrResult[0][41];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value=arrResult[0][42];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value=arrResult[0][43];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value=arrResult[0][44];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value=arrResult[0][44];
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDNo').value=arrResult[0][55];
  } catch(ex) {}
  ;
  try {
    fm.all('EnglishName').value=arrResult[0][56];
  } catch(ex) {}
  ;
    try {
    fm.all('IDStartDate').value=arrResult[0][58];
  } catch(ex) {}
  ;
  try {
    fm.all('IDEndDate').value=arrResult[0][59];
  } catch(ex) {}
  ;
  try {
    fm.all('NativeCity').value=arrResult[0][60];
    setCodeName("NativeCityName", "nativecity", arrResult[0][60]);
  } catch(ex) {}
  ;
  //modify by zxs 
  try {
	    fm.all('InsuredAuth').value=arrResult[0][62];
	    //alert(arrResult[0][62]);
	  } catch(ex) {}
	  ;	
	var s =  easyExecSql("select 1 from lwmission where missionprop1 = '"+fm.prtn2.value+"' and activityid='0000009001'");
		if(s =='1'){
				fm.all('InsuredAuth').value = '1';
			}
  showAllCodeName();
  controlNativeCity("");
  
}
function displayissameperson() 
{
  try 
  {
    fm.all('InsuredNo').value= mSwitch.getVar( "AppntNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value= mSwitch.getVar( "AppntName" );
  } catch(ex) {}
  ;
  try {
    fm.all('EnglishName').value= mSwitch.getVar( "EnglishName" ); 
  } catch(ex) {}
  ;
  try 
  {
    fm.all('Sex').value= mSwitch.getVar( "AppntSex" );
  } catch(ex) {}
  ;
  try 
  {
    var sql = "select CodeName from LDCode where CodeType = 'sex' and Code = '" + fm.all('Sex').value + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      fm.all('SexName').value= rs[0][0];
    }
  } catch(ex) {}
  ;
  try 
  {
    fm.all('Birthday').value= mSwitch.getVar( "AppntBirthday" );
  } catch(ex) {}
  ;
  try 
  {
    fm.all('IDStartDate').value= mSwitch.getVar( "IDStartDate" );
  } catch(ex) {}
  ;
  try 
  {
    fm.all('IDEndDate').value= mSwitch.getVar( "IDEndDate" );
  } catch(ex) {}
  ;
  try 
  {
    fm.all('IDType').value= mSwitch.getVar( "AppntIDType" );
  } catch(ex) {}
  ;
  try 
  {
    var sql = "select CodeName from LDCode where CodeType = 'idtype' and Code = '" + fm.all('IDType').value + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      fm.all('IDTypeName').value= rs[0][0];
    }
  } catch(ex) {}
  ;
  try 
  {
    fm.all('IDNo').value= mSwitch.getVar( "AppntIDNo" );
  } catch(ex) {}
  ;
  try 
  {
    fm.all('Password').value= mSwitch.getVar( "AppntPassword" );
  } catch(ex) {}
  ;
  try 
  {
    fm.all('NativePlace').value= mSwitch.getVar( "AppntNativePlace" );
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value= mSwitch.getVar( "AppntNationality" );
  } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value= mSwitch.getVar( "AddressNo" );
  } catch(ex) {}
  ;
  try 
  {
    fm.all('RgtAddress').value= mSwitch.getVar( "AppntRgtAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value= mSwitch.getVar( "AppntMarriage" );
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value= mSwitch.getVar( "AppntMarriageDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value= mSwitch.getVar( "AppntHealth" );
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value= mSwitch.getVar( "AppntStature" );
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value= mSwitch.getVar( "AppntAvoirdupois" );
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value= mSwitch.getVar( "AppntDegree" );
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value= mSwitch.getVar( "AppntDegreeCreditGrade" );
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDType').value= mSwitch.getVar( "AppntOthIDType" );
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDNo').value= mSwitch.getVar( "AppntOthIDNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('ICNo').value= mSwitch.getVar( "AppntICNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpNo').value= mSwitch.getVar( "AppntGrpNo" );
  } catch(ex) {}
  ;
  try {
    fm.all( 'JoinCompanyDate' ).value = mSwitch.getVar( "JoinCompanyDate" );
    if(fm.all( 'JoinCompanyDate' ).value=="false") {
      fm.all( 'JoinCompanyDate' ).value="";
    }
  } catch(ex) { }
  ;
  try {
    fm.all('StartWorkDate').value= mSwitch.getVar( "AppntStartWorkDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= mSwitch.getVar( "Position" );
  } catch(ex) {}
  ;
  try {
    fm.all('Salary').value= mSwitch.getVar( "AppntSalary" );
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= mSwitch.getVar( "AppntOccupationType" );
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value= mSwitch.getVar( "AppntOccupationCode" );
  } catch(ex) {}
  ;
  ;
  try {
    var sql = "select OccupationName from LDOccupation where OccupationCode = '" + fm.all('OccupationCode').value + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      fm.all('OccupationName').value = rs[0][0];
    }
  } catch(ex) {}
  ;
  try {
    fm.all('WorkType').value= mSwitch.getVar( "AppntWorkType" );
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value= mSwitch.getVar( "AppntPluralityType" );
  } catch(ex) {}
  ;
  try {
    fm.all('DeathDate').value= mSwitch.getVar( "AppntDeathDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value= mSwitch.getVar( "AppntSmokeFlag" );
  } catch(ex) {}
  ;
  try {
    fm.all('BlacklistFlag').value= mSwitch.getVar( "AppntBlacklistFlag" );
  } catch(ex) {}
  ;
  try {
    fm.all('Proterty').value= mSwitch.getVar( "AppntProterty" );
  } catch(ex) {}
  ;
  try {
    fm.all('Remark').value= mSwitch.getVar( "AppntRemark" );
  } catch(ex) {}
  ;
  try {
    fm.all('State').value= mSwitch.getVar( "AppntState" );
  } catch(ex) {}
  ;
  try {
    fm.all('Operator').value= mSwitch.getVar( "AppntOperator" );
  } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value= mSwitch.getVar( "AppntMakeDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value= mSwitch.getVar( "AppntMakeTime" );
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value= mSwitch.getVar( "AppntModifyDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value= mSwitch.getVar( "AppntModifyTime" );
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value= mSwitch.getVar( "PostalAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= mSwitch.getVar( "ZipCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= mSwitch.getVar( "Phone" );
  } catch(ex) {}
  ;
  try {
    fm.all('CompanyPhone').value= mSwitch.getVar( "CompanyPhone" );
  } catch(ex) {}
  ;
  try {
    fm.all('Fax').value= mSwitch.getVar( "Fax" );
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= mSwitch.getVar( "Mobile" );
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= mSwitch.getVar( "EMail" );
  } catch(ex) {}
  ;
  try {
  fm.all('GrpName').value= mSwitch.getVar( "GrpName" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpPhone').value= mSwitch.getVar( "GrpPhone" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value= mSwitch.getVar( "CompanyAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= mSwitch.getVar( "AppntGrpZipCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpFax').value= mSwitch.getVar( "AppntGrpFax" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= mSwitch.getVar( "AppntHomeAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= mSwitch.getVar( "Phone" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= mSwitch.getVar( "HomeZipCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeFax').value= mSwitch.getVar( "HomeFax" );
  } catch(ex) {}
  ;
  
  try {fm.all('PostalProvince').value= mSwitch.getVar( "PostalProvince" );} catch(ex) {};
  try {fm.all('Province').value= mSwitch.getVar( "Province" );} catch(ex) {};
  try {fm.all('PostalCity').value= mSwitch.getVar( "PostalCity" );} catch(ex) {};
  try {fm.all('City').value= mSwitch.getVar( "City" );} catch(ex) {};
  try {fm.all('PostalCounty').value= mSwitch.getVar( "PostalCounty" );} catch(ex) {};
  try {fm.all('County').value= mSwitch.getVar( "County" );} catch(ex) {};
  try {fm.all('PostalStreet').value= mSwitch.getVar( "PostalStreet" );} catch(ex) {};
  try {fm.all('PostalCommunity').value= mSwitch.getVar( "PostalCommunity" );} catch(ex) {};  
  
  try {
    fm.all('NativeCity').value= mSwitch.getVar( "NativeCity" );
    setCodeName("NativeCityName", "nativecity", fm.all('NativeCity').value);
  } catch(ex) {}
  ;
  //modify by zxs 
  try {
	  //	alert(mSwitch.getVar( "AppntAuth" ));
	    fm.all('InsuredAuth').value= mSwitch.getVar( "AppntAuth" );
	    
	  } catch(ex) {}
	  ;
  showAllCodeName();
  controlNativeCity("");
}

/*********************************************************************
 *  根据家庭单类型，隐藏界面控件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function choosetype() {
  if(fm.FamilyType.value=="1")
    divTempFeeInput.style.display="";
  if(fm.FamilyType.value=="0")
    divTempFeeInput.style.display="none";
}
/*********************************************************************
 *  校验被保险人与主被保险人关系
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function checkself() {
  if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value=="") {
    fm.RelationToMainInsured.value="00";
    return true;
  } else if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value!="00") {
    alert("个人单中'与第一被保险人关系'只能是'本人'");
    fm.RelationToMainInsured.value="00";
    return false;
  } else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value==""&&InsuredGrid.mulLineCount==0) {
    fm.RelationToMainInsured.value="00";
    return true;
  } else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value!="00"&&InsuredGrid.mulLineCount==0) {
    alert("家庭单中第一位被保险人的'与第一被保险人关系'只能是'本人'");
    fm.RelationToMainInsured.value="00";
    return false;
  } else
    return true;
}
/*********************************************************************
 *  校验保险人
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function checkrelation() {
  if(LoadFlag==2||LoadFlag==7) {
    if (fm.all('ContNo').value != "") {
      alert("团单的个单不能有多被保险人");
      return false;
    } else
      return true;
  } else {
      if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="1"&&fm.RelationToAppnt.value=="00") {
      var strSql="select * from LCInsured where contno='"+fm.all('ContNo').value +"' and RelationToAppnt='00' ";
      turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);
      if(turnPage.strQueryResult) {
        alert("投保人已经是该保单下的被保险人");
        return false;
      } else
        return true;
    } else
      return true;
  }
  //select count(*) from ldinsured

}
/*********************************************************************
 *  投保人与被保人相同选择框事件
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function isSamePerson() 
{
  //对应未选同一人，又打钩的情况
  if ( fm.SamePersonFlag.checked==true&&fm.RelationToAppnt.value!="00") 
  {
    fm.all('DivLCInsured').style.display = "none";
    //divLCInsuredPerson.style.display = "none";
    //divSalary.style.display = "none";
    fm.SamePersonFlag.checked = true;
    fm.RelationToAppnt.value="00"
    displayissameperson();
  }
  //对应是同一人，又打钩的情况
  else if (fm.SamePersonFlag.checked == true) 
  {
    fm.all('DivLCInsured').style.display = "none";
    //divLCInsuredPerson.style.display = "none";
    //divSalary.style.display = "none";
    displayissameperson();
  }
  //对应不选同一人的情况
  else if (fm.SamePersonFlag.checked == false) 
  {
    fm.all('DivLCInsured').style.display = "";
    emptyInsured();
  }
  
}
/*********************************************************************
 *  投保人客户号查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryInsuredNo() 
{
  
  if (fm.all("InsuredNo").value == "") 
  {
    showAppnt1();
  } 
  else 
  {
    arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + fm.all("InsuredNo").value + "'", 1, 0);
    if (arrResult == null) 
    {
      alert("未查到投保人信息");
      displayAppnt(new Array());
      emptyUndefined();
    } 
    else 
    {
      displayAppnt(arrResult[0]);
      getaddresscodedata();
    }
  }
}
/*********************************************************************
 *  投保人查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt1() 
{
  if( mOperate == 0 ) 
  {
    mOperate = 2;
    showInfo = window.open( "../sys/LDPersonQueryNew.html" );
  }
}
/*********************************************************************
 *  显示投保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt() {
  try {
    fm.all('InsuredNo').value= arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value= arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('IDTypeName').value= getNameFromLDCode("IDType",arrResult[0][4]);
  } catch(ex) { }
  ;
  try {
    fm.all('IDNo').value= arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('Password').value= arrResult[0][6];
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value= arrResult[0][7];
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value= arrResult[0][8];
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value= arrResult[0][9];
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value= arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value= arrResult[0][11];
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value= arrResult[0][12];
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value= arrResult[0][13];
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value= arrResult[0][14];
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value= arrResult[0][15];
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value= arrResult[0][16];
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDType').value= arrResult[0][17];
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDNo').value= arrResult[0][18];
  } catch(ex) {}
  ;
  try {
    fm.all('ICNo').value= arrResult[0][19];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpNo').value= arrResult[0][20];
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value= arrResult[0][21];
  } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value= arrResult[0][22];
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= arrResult[0][23];
  } catch(ex) {}
  ;
  try {
	if(arrResult[0][24]=='-1'){
	try{fm.all('Salary').value=""; }catch(ex){};               
	}else{
	try{fm.all('Salary').value= arrResult[0][24]; }catch(ex){};
	}
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= arrResult[0][25];
  } catch(ex) {}
  ;
  //alert( arrResult[0][25]);
  try {
    fm.all('OccupationCode').value= arrResult[0][26];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][26]);
  } catch(ex) { }
  ;
  try {
    fm.all('WorkType').value= arrResult[0][27];
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value= arrResult[0][28];
  } catch(ex) {}
  ;
  try {
    fm.all('DeathDate').value= arrResult[0][29];
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value= arrResult[0][30];
  } catch(ex) {}
  ;
  try {
    fm.all('BlacklistFlag').value= arrResult[0][31];
  } catch(ex) {}
  ;
  try {
    fm.all('Proterty').value= arrResult[0][32];
  } catch(ex) {}
  ;
  try {
    fm.all('Remark').value= arrResult[0][33];
  } catch(ex) {}
  ;
  try {
    fm.all('State').value= arrResult[0][34];
  } catch(ex) {}
  ;
  try {
    fm.all('Operator').value= arrResult[0][35];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value= arrResult[0][36];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value= arrResult[0][37];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value= arrResult[0][38];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value= arrResult[0][39];
  } catch(ex) {}
  ;
  try {
 fm.all('GrpName').value= arrResult[0][41];
  } catch(ex) {}
  ;
  //地址显示部分的变动
  try {
    fm.all('AddressNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value=  "";
  } catch(ex) {}
  ;
 //try{fm.all('GrpName').value= arrResult[0][46];}catch(ex){};
  try {
    fm.all('GrpPhone').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativeCity').value= arrResult[0][47] ;
  } catch(ex) {}
  ;
  showAllCodeName();
  controlNativeCity("");
}
/*********************************************************************
 *  查询返回后触发
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {

  //alert("here:" + arrQueryResult + "\n" + mOperate);
  if( arrQueryResult != null ) {
    arrResult = arrQueryResult;

    if( mOperate == 1 ) {		// 查询投保单
      fm.all( 'ContNo' ).value = arrQueryResult[0][0];

      arrResult = easyExecSql("select ProposalContNo,PrtNo,ManageCom,SaleChnl,AgentCode,AgentGroup,AgentCode1,AgentCom,AgentType,Remark from LCCont where LCCont = '" + arrQueryResult[0][0] + "'", 1, 0);

      if (arrResult == null) {
        alert("未查到投保单信息");
      } else {
        displayLCContPol(arrResult[0]);
      }
    }

    if( mOperate == 2 ) {		// 投保人信息
      //arrResult = easyExecSql("select a.*,b.AddressNo,b.PostalAddress,b.ZipCode,b.HomePhone,b.Mobile,b.EMail,a.GrpNo,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode from LDPerson a,LCAddress b where 1=1 and a.CustomerNo=b.CustomerNo and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      if (arrResult == null) {
        alert("未查到投保人信息");
      } else {
        displayAppnt(arrResult[0]);
        getaddresscodedata();
      }
    }
  }
  mOperate = 0;		// 恢复初态
}

/*********************************************************************
 *  查询被保险人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getdetailaddress() {
  var strSQL="select b.AddressNo,b.PostalAddress,b.ZipCode,b.Phone,b.Mobile,b.EMail,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode,b.HomeAddress,b.HomeZipCode,b.HomePhone,b.GrpName,b.PostalProvince,b.PostalCity,b.PostalCounty,b.PostalStreet,b.PostalCommunity from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.InsuredNo.value+"'";
  arrResult=easyExecSql(strSQL);
  try {
    fm.all('AddressNo').value= arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value= arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpPhone').value= arrResult[0][6];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value= arrResult[0][7];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][8];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= arrResult[0][9];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('HomePhone').value= arrResult[0][11];
  } catch(ex) {}
  ;
  try {

 fm.all('GrpName').value= arrResult[0][12];
  } catch(ex) {}
  ; //已经在 LDPerson 里里面查询出来了
  var rs1 = easyExecSql("select codename from ldcode1 where codetype='province1' and code='"+arrResult[0][13]+"' and code1='0' ");
  var rs2 = easyExecSql("select codename from ldcode1 where codetype='city1' and code='"+arrResult[0][14]+"' and code1='"+arrResult[0][13]+"' ");
  var rs3 = easyExecSql("select codename from ldcode1 where codetype='county1' and code='"+arrResult[0][15]+"' and code1='"+arrResult[0][14]+"' ");
  if(rs1 != null ){
	  try {fm.all('Province').value= arrResult[0][13];} catch(ex) {};
	  try {fm.all('City').value= arrResult[0][14];} catch(ex) {};
	  try {fm.all('County').value= arrResult[0][15];} catch(ex) {};
	  try {fm.all('PostalProvince').value= rs1;} catch(ex) {};
	  try {fm.all('PostalCity').value= rs2;} catch(ex) {};
	  try {fm.all('PostalCounty').value= rs3;} catch(ex) {};
  }else{
	  var rs4 = easyExecSql("select code from ldcode1 where codetype='province1' and codename ='"+arrResult[0][13]+"'");
	  var rs5 = easyExecSql("select code from ldcode1 where codetype='city1' and codename ='"+arrResult[0][14]+"'");
	  var rs6 = easyExecSql("select code from ldcode1 where codetype='county1' and codename ='"+arrResult[0][15]+"'");
	  try {fm.all('Province').value= rs4;} catch(ex) {};
	  try {fm.all('City').value= rs5;} catch(ex) {};
	  try {fm.all('County').value= rs6;} catch(ex) {};
	  try {fm.all('PostalProvince').value= arrResult[0][13];} catch(ex) {};
	  try {fm.all('PostalCity').value= arrResult[0][14];} catch(ex) {};
	  try {fm.all('PostalCounty').value= arrResult[0][15];} catch(ex) {};
  }
  
  try {
    fm.all('PostalStreet').value= arrResult[0][16];
  } catch(ex) {}
  ;
  try {
    fm.all('PostalCommunity').value= arrResult[0][17];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeCode').value= arrResult[0][18];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeNumber').value= arrResult[0][19];
  } catch(ex) {}
  ;  
}

/*********************************************************************
 *  查询保险计划
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getContPlanCode(tProposalGrpContNo) {
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select ContPlanCode,ContPlanName from LCContPlan where ContPlanCode<>'00' and ContPlanCode<>'11' and ProposalGrpContNo='"+tProposalGrpContNo+"'";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
    divContPlan.style.display="";
  } else {
    //alert("保险计划没查到");
    divContPlan.style.display="none";
  }
  //alert ("tcodedata : " + tCodeData);
  return tCodeData;
}

/*********************************************************************
 *  查询处理机构
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getExecuteCom(tProposalGrpContNo) {
  //alert("1");
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select ExecuteCom,Name from LCGeneral a,LDCom b where a.GrpContNo='"+tProposalGrpContNo+"' and a.ExecuteCom=b.ComCode";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
    divExecuteCom.style.display="";
  } else {
    divExecuteCom.style.display="none";
  }
  //alert ("tcodedata : " + tCodeData);

  return tCodeData;
}

function emptyInsured() 
{
  //清空套餐信息、套餐责任信息、告知信息
  //initWrapGrid();
  initRiskWrapGrid("");
  queryWrapGrip();
  initDisDesbGrid();

  try {
    fm.all('InsuredNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('ExecuteCom').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('FamilyID').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToMainInsured').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToAppnt').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('EnglishName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('SexName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value= "0";
  } catch(ex) {}
  ;
  try {
    fm.all('IDTypeName').value= "身份证";
  } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlaceName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('BankCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('BankAccNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('AccName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Salary').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('WorkType').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('ContPlanCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('CompanyPhone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeFax').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpFax').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Fax').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('CheckPostalAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativeCity').value= "";
  } catch(ex) {}
  ;
  emptyAddress();
}

/*********************************************************************
 *  清空客户地址数据
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function emptyAddress() {
  try {
    fm.all('PostalAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= "";
  } catch(ex) {}
  ;
  //try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){};
  try{
  	fm.all('GrpName').value="";
  }catch(ex){}
  ;
  try {
    fm.all('GrpPhone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= "";
  } catch(ex) {}
  ;
}
/*********************************************************************
 *  根据身份证号取得出生日期和性别
 *  参数  ：  身份证号
 *  返回值：  无
 *********************************************************************
 */

function getBirthdaySexByIDNo(iIdNo) {
  if(fm.all('IDType').value=="0") {
    fm.all('Birthday').value=getBirthdatByIdNo(iIdNo);
    fm.all('Sex').value=getSexByIDNo(iIdNo);
  }
}

function getaddresscodedata() {
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  //strsql = "select AddressNo,PostalAddress from LCAddress where CustomerNo ='"+fm.InsuredNo.value+"'";
  strsql = "select max(int(AddressNo)) from LCAddress where CustomerNo ='"+fm.InsuredNo.value+"'";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData =turnPage.arrDataCacheSet[i][0];
    }
  }
  //alert ("tcodedata : " + tCodeData);
  //return tCodeData;
  //fm.all("AddressNo").CodeData=tCodeData;
  fm.all("AddressNo").value=tCodeData;
  afterCodeSelect("AddressNo","");
}

function checkidtype() {
  if(fm.IDType.value=="") {
    alert("请先选择证件类型！");
    fm.IDNo.value="";
  }
}
function getallinfo() {
  if(fm.Name.value!=""&&fm.IDType.value!=""&&fm.IDNo.value!="") {
    strSQL = "select a.CustomerNo, a.Name, a.Sex, a.Birthday, a.IDType, a.IDNo from LDPerson a where 1=1 "
             +"  and Name='"+fm.Name.value
             +"' and IDType='"+fm.IDType.value
             +"' and IDNo='"+fm.IDNo.value
             +"' order by a.CustomerNo";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (turnPage.strQueryResult != "") {
      mOperate = 2;
      //alert(mOperate);
      //window.open("../sys/LDPersonQueryAll.html?Name="+fm.Name.value+"&IDType="+fm.IDType.value+"&IDNo="+fm.IDNo.value,"newwindow","height=10,width=1090,top=180,left=180, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no,status=no");
    } else
      return;
  }
}
function DelRiskInfo() {
  if(fm.InsuredNo.value=="") {
    alert("请先选择被保人");
    return false;
  }
  var tSel =PolGrid.getSelNo();
  if( tSel == 0 || tSel == null ) {
    alert("该客户没有险种或者您忘记选择了？");
    return false;
  }
  if(!confirm("是否要删除该险种？删除请点击“确定”，否则点“取消”。"))
  {
  	return;
  }
  var tRow = PolGrid.getSelNo() - 1;
  var tpolno=PolGrid.getRowColData(tRow,1)
             fm.all('fmAction').value="DELETE||INSUREDRISK";
  fm.action="./DelIsuredRisk.jsp?polno="+tpolno;
  fm.submit(); //提交

}
function InsuredChk() {
  var tSel =InsuredGrid.getSelNo();
  if( tSel == 0 || tSel == null ) {
    alert("请先选择被保险人！");
    return false;
  }
  var tRow = InsuredGrid.getSelNo() - 1;
  var tInsuredNo=InsuredGrid.getRowColData(tRow,1);
  var tInsuredName=InsuredGrid.getRowColData(tRow,2);
  var tInsuredSex=InsuredGrid.getRowColData(tRow,3);
  var tBirthday=InsuredGrid.getRowColData(tRow,4);
  var sqlstr="select *from ldperson where Name='"+tInsuredName+"' and Sex='"+tInsuredSex+"' and Birthday='"+tBirthday+"' and CustomerNo<>'"+tInsuredNo+"'";
  arrResult = easyExecSql(sqlstr,1,0);
  if(arrResult==null) {
    alert("没有与该被保人相似的客户,无需校验");
    return false;
  }

  window.open("../uw/InsuredChkMain.jsp?ProposalNo1="+fm.ContNo.value+"&InsuredNo="+tInsuredNo+"&Flag=I","window1");
}
function FillPostalAddress() {
    var PostalAddressSQL="select PostalAddress from lcaddress where customerno in(select appntno from lccont where prtno ='"+fm.all( 'PrtNo' ).value+"')  and AddressNo =(select char(max(int(AddressNo))) from LCAddress where customerno in(select appntno from lccont where prtno='"+fm.all( 'PrtNo' ).value+"'))";
    var sqlstr=easyExecSql(PostalAddressSQL,1,0);
    if(sqlstr){
			//alert();
    	if(fm.CheckPostalAddress.value=="1") {
    		fm.all('PostalAddress').value=sqlstr;
    		fm.all('ZipCode').value="";
    		}else if(fm.CheckPostalAddress.value=="2"){
    		fm.all('PostalAddress').value="";
    		fm.all('ZipCode').value="";    		
    		}
  		}else{
    	fm.all('PostalAddress').value="";
    	fm.all('ZipCode').value="";    
    	} 
}
function checksex() {
  var malearray= new Array("02","04","06","08","09","14","16","19","22");
  var femalearray= new Array("01","05","07","10","11","15","17","20","23");
  if(fm.Sex.value=="0") {
    for(var relationmcount=0;relationmcount<femalearray.length-1;relationmcount++) {
      if(fm.RelationToAppnt.value==femalearray[relationmcount]) {
        alert("被保人性别与'与投保人关系'中的关系身份不符");
        return false;
      }
      if(fm.RelationToMainInsured.value==femalearray[relationmcount]) {
        alert("被保人性别与'与第一被保人关系'中的关系身份不符");
        return false;
      }
    }
  }
  if(fm.Sex.value=="1") {
    for(var relationfcount=0;relationfcount<malearray.length;relationfcount++) {
      if(fm.RelationToAppnt.value==malearray[relationfcount]) {
        alert("被保人性别与'与投保人关系'中的关系身份不符");
        return false;
      }
      if(fm.RelationToMainInsured.value==malearray[relationfcount]) {
        alert("被保人性别与'与第一被保人关系'中的关系身份不符");
        return false;
      }
    }
  }
  
  return true;
}
function getdetailaccount() {
  if(fm.AccountNo.value=="1") {
    if(mSwitch.getVar("AppntBankAccNo"))
      fm.all('BankAccNo').value=mSwitch.getVar("AppntBankAccNo");
    if(mSwitch.getVar("AppntBankCode"))
      fm.all('BankCode').value=mSwitch.getVar("AppntBankCode");
      fm.all('bankName').value = easyExecSql("select codename from ldcode where codetype='bank' and code='"+mSwitch.getVar("AppntBankCode")+"'");
    if(mSwitch.getVar("AppntAccName"))
      fm.all('AccName').value=mSwitch.getVar("AppntAccName");
  }
  if(fm.AccountNo.value=="2") {
    fm.all('BankAccNo').value="";
    fm.all('BankCode').value="";
    fm.all('AccName').value="";
  }

}
function AutoMoveForNext() {
  if(fm.AutoMovePerson.value=="定制第二被保险人") {
    //emptyFormElements();
    param="122";
    fm.pagename.value="122";
    fm.AutoMovePerson.value="定制第三被保险人";
    return false;
  }
  if(fm.AutoMovePerson.value=="定制第三被保险人") {
    //emptyFormElements();
    param="123";
    fm.pagename.value="123";
    fm.AutoMovePerson.value="定制第一被保险人";
    return false;
  }
  if(fm.AutoMovePerson.value=="定制第一被保险人") {
    //emptyFormElements();
    param="121";
    fm.pagename.value="121";
    fm.AutoMovePerson.value="定制第二被保险人";
    return false;
  }
}

function getdetail() {
  var strSql = "select BankCode,AccName from LCAccount where BankAccNo='" + fm.BankAccNo.value+"'";
  arrResult = easyExecSql(strSql);
  if (arrResult != null) {
    fm.BankCode.value = arrResult[0][0];
    fm.AccName.value = arrResult[0][1];
  }

}
//用于无名单时填充被保险人信息
function FillNoName() {
  fm.IDType.value='4';
  DivLCInsured.style.display='none';
  DivGrpNoname.style.display='';
}

//用于无名单校验
function checkifnoname() {
  var strSql = "select ContPlanCode from LCInsured where ContNo='" + oldContNo+"' and Name='无名单'";
  arrResult = easyExecSql(strSql);
  if(fm.ContPlanCode.value!=""&&fm.ContPlanCode.value!=arrResult[0][0]) {
    alert("所选计划与原无名单所选不一致！");
    return false;
  } else {
    return true;
  }
}
//用于无名单校验
function checkenough() {
  var strSql1 = " select * from lcpol where appflag='4' and MasterPolNo in (select  proposalNo from lcpol where Contno='" + oldContNo+"')";
  var arrResult1 = easyExecSql(strSql1);
  var strSql2 = " select * from lccont where appflag<>'1' and GrpContNo = (select  GrpContNo from lcCont where Contno='" + oldContNo+"')";
  var arrResult2 = easyExecSql(strSql2);
  if(arrResult1!=null||arrResult2!=null) {
    alert("尚有补名单未签单,请先签单再补其他名单!");
    return false;
  } else {
    return true;
  }
}
//险种复合
function showComplexRiskInfo()
{
	window.open("./ComplexRiskMain.jsp?ContNo="+ContNo+"&prtNo="+fm.PrtNo.value,"", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}
//校验是否重新生成新的合同
function checkCanModify(){
	var strSql = "select 1 from LCInsured where name='"+fm.Name.value+"' and EnglishName='"+fm.EnglishName.value+"' and OthIDNo='"+fm.OthIDNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		return false;
	}else{
		return true;
	}
}

//查询所有套餐信息
function queryWrapGrip()
{
  var sql = "select RiskWrapCode, WrapName "
            + "from LDWrap "
            + "where WrapType = '" + tWrapType + "' ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.pageLineNum = 100;
	turnPage2.queryModal(sql, WrapGrid);
	
  if("8" == tCardFlag)
  {
    controlOmnipotence();
  }
  if("TB05" == fm.SubType.value)
  {
    controlDisplay();
    fm.all("DivLCPol").style.display = "";
    fm.all("queryWrapDetailID").style.display = "";
  }
}

//查询套餐明细
function queryWrapDetail()
{
  var riskWrapCodes = "";  //已选择的套餐信息
  
  for(var i = 0; i < WrapGrid.mulLineCount; i++)
  {
    if(WrapGrid.getChkNo(i))
    {
      riskWrapCodes = riskWrapCodes 
        + "'" + WrapGrid.getRowColDataByName(i, "RiskWrapCode") + "',";
    }
  }
  
  if(riskWrapCodes == "")
  {
    alert("请选择套餐");
    return false;
  }
  riskWrapCodes = riskWrapCodes.substring(0, riskWrapCodes.length - 1);
  
  initRiskWrapGrid(riskWrapCodes);
  
  var sql = strSqlMain;
  
  sql = sql 
    + "  and a.RiskWrapCode in (" + riskWrapCodes + ") "
    + "order by a.RiskWrapCode, RiskCode, DutyCode ";
  
	turnPage3.pageDivName = "divPage3";
	turnPage3.pageLineNum = 100;
	turnPage3.queryModal(sql, RiskWrapGrid);
	
	//套餐责任信息
	var sql = "select distinct RiskWrapCode, RiskCode, DutyCode "
            + "from LCRiskDutyWrap "
            + "where PrtNo = '" + fm.PrtNo.value + "' "
            + "   and InsuredNo = '" + insuredNo + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    //选中被保人已录入的套餐责任信息
    for(var i = 0; i < rs.length; i++)
    {
      for(var j = 0; j < RiskWrapGrid.mulLineCount; j++)
      {
        var tGridRiskWrapCode = RiskWrapGrid.getRowColDataByName(j, "RiskWrapCode");
        var tGridRiskCode = RiskWrapGrid.getRowColDataByName(j, "RiskCode");
        var tGridDutyCode = RiskWrapGrid.getRowColDataByName(j, "DutyCode");
        if(rs[i][0] == tGridRiskWrapCode
            && (rs[i][1] == tGridRiskCode || "000000" == tGridRiskCode)
            && (rs[i][2] == tGridDutyCode || "000000" == tGridDutyCode))  //若不需要显示到险种和责任，那么只需要比较套餐即可
        {
          RiskWrapGrid.checkBoxSel(j+1);
        }
      }
    }
  }
  
  for(var i = 0 ; i<RiskWrapGrid.mulLineCount ; i++)
  {
		for(var m = 1 ; m<RiskWrapGrid.colCount ; m++)
		{
      //将"null"显示为空字符创""
			if(RiskWrapGrid.getRowColData(i,m)=="null" || RiskWrapGrid.getRowColData(i,m)=="0")
			{
				RiskWrapGrid.setRowColData(i,m,"");
			}
			
  		//少儿险WR0004的显示要特殊赋值
  		if(RiskWrapGrid.getRowColDataByName(i, "RiskWrapCode") == "WR0004")
  		{
  		  if(RiskWrapGrid.getRowColData(i, m) == "InsuYear")
  		  {
  		    RiskWrapGrid.setRowColData(i, m + 1, "1年");
  		  }
  		  if(RiskWrapGrid.getRowColData(i, m) == "Amnt")
  		  {
  		    RiskWrapGrid.setRowColData(i, m + 1, "220000");
  		  }
  		  if(RiskWrapGrid.getRowColData(i, m) == "Prem")
  		  {
  		    RiskWrapGrid.setRowColData(i, m + 1, "20000");
  		  }
  		}//少儿险的显示要特殊赋值
		}
	}
	
}

//查询受益人信息
function queryBnfGrid()
{
  var sql = "select distinct BnfType, Name, IDType, IDNo, Sex, RelationToInsured, BnfLot, BnfGrade "
          + "from LCBnf "
          + "where ContNo = '" + fm.ContNo.value + "' "
          + "   and InsuredNo = '" + fm.InsuredNo.value + "' ";
	turnPage5.pageDivName = "divPage5";
	turnPage5.pageLineNum = 100;
	turnPage5.queryModal(sql, BnfGrid);
}

function addInit(){
	var index = BnfGrid.mulLineCount;
	BnfGrid.setRowColData(index-1, 1, "1");
}

//控制万能险控件的显示
function controlOmnipotence()
{
  //不显示套餐信息，自动查询套餐责任信息，并自动选择套装
  fm.all("DivLCPol").style.display = "none";
  fm.all("queryWrapDetailID").style.display = "none";
  
  //只有第一被保人
  fm.SequenceNo.value = "1";
  fm.RelationToMainInsured.value = "00";
  
  for(var i = 1; i <= WrapGrid.mulLineCount; i++)
  {
    WrapGrid.checkBoxSel(i);
  }
  queryWrapDetail();
  
  //选择所有责任
  //for(var i = 1; i <= RiskWrapGrid.mulLineCount; i++)
  //{
    //RiskWrapGrid.checkBoxSel(i);
  //}
  
  controlDisplay();
  
  fm.RelationToMainInsured.value = "00";
}

function controlDisplay()
{
  fm.all("ICDDisDesbID").style.display = "none";
  fm.all("divAddDelButton").style.display = "none";
  fm.all("addInsuredButtonBottomID").style.display = "";
  fm.all("modifyInsuredButtonBottomID").style.display = "";
  fm.all("delInsuredButtonBottomID").style.display = "";
  fm.all("divPrtNo").style.display = "none";
  fm.all("divInsureGrid").style.display = "none";
  fm.all("SequenceNoTitleID").style.display = "none";
  fm.all("SequenceNoInputID").style.display = "none";
  fm.all("RelationToMainInsuredTitleID").style.display = "none";
  fm.all("RelationToMainInsuredInputID").style.display = "none";
  fm.all("EnglishNameText").style.display = "none";
  fm.all("EnglishNameClass").style.display = "none";
  //fm.all("NativePlaceTitleID").style.display = "none";
  //fm.all("NativePlaceInputID").style.display = "none";
  fm.all("ZhiweiApptitle").style.display = "";
  fm.all("ZhiweiApp").style.display = "";
  fm.all("LCBnfDivID").style.display = "";
}

//抵达国家
function getNationInfo()
{
	if("TB05" == fm.SubType.value)
	{
		fm.all("NationDiv").style.display = '';
		fm.all("NationGridDiv").style.display = '';
		sql = "select NationNo, ChineseName from lcnation where ContNo='" + ContNo + "'";
		turnPage.queryModal(sql, NationGrid);
	}
}

//外包反馈错误信息
function findIssue()
{
	window.open("../app/BPOIssueInput.jsp?prtNo=" + prtNo,"windowIssue");
}

function setSubType()
{
	var sql = "select SubType from ES_DOC_MAIN where DocCode = '" + prtNo + "'";
	var rs = easyExecSql(sql);
	if(rs)
	{
	  fm.SubType.value = rs[0][0];
	}
}

function initButton()
{
  if(LoadFlag == "6")
  {
    fm.all("addInsuredButtonBottomID").style.display = "none";
    fm.all("modifyInsuredButtonBottomID").style.display = "none";
    fm.all("delInsuredButtonBottomID").style.display = "none";
    fm.all("inputConfirmButtonID").style.display = "none";
    fm.all("approveConfirmButtonID").style.display = "none";
  }
  else if(LoadFlag == "5")
  {
	  PostalAddressID.style.display = "";
    var sql = "select * from lcinsured where contno = '"+fm.ContNo.value+"'";
    var rs = easyExecSql(sql);
    if(rs == null){
    	fm.all("addInsuredButtonBottomID").style.display = "";
    }else{
    	fm.all("addInsuredButtonBottomID").style.display = "none";
    }    
  }
}


function getImpartAll()
{
    getImpartInfo();
    getImpartDetailInfo();
}


/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartInfo()
{
    initImpartGrid();

    clearImpart();    //清空
    getImpartbox();   //保障状况告知※健康告知
}


/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartDetailInfo()
{
    initImpartDetailGrid();
    
    var InsuredNo=fm.all("InsuredNo").value;
    var ContNo=fm.all("ContNo").value;

    var sql = "select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where CustomerNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and CustomerNoType='I'";
    
    turnPage6.pageLineNum = 50;
    turnPage6.pageDivName = "divPage6";
    turnPage6.queryModal(sql, ImpartDetailGrid);
}
/*********************************************************************
 *  查询红利领取方式
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initBonusgetmode(){
	var tSql = "select bonusgetmode from lcpol where contno = '"+ContNo+"' and bonusgetmode != '' and bonusgetmode is not null  "
	//prompt('',tSql);
	var arrResult = easyExecSql(tSql);
	if(arrResult!=null) {
		fm.all("BonusGetMode").value = arrResult[0][0];
		var tNameSql = "select codename from ldcode where codetype = 'bonusgetmode' and code = '"+arrResult[0][0]+"'";
		var arrNameResult = easyExecSql(tNameSql);
		fm.all("BonusGetModeName").value = arrNameResult[0][0];
	}
}

/*********************************************************************
 *  校验险种 
 * 1、只能有一个被保人
 * 2、只能有一个主险（银保万能/分红，目前只能支持一个主险）
 * 3、附加险选择时，不允许主险为万能险产品
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkInsuredAndRisk(){
	var tSql = "select 1 from lcinsured where contno = '"+ContNo+"' "
	//prompt('',tSql);
	var arrResult = easyExecSql(tSql);
	if(arrResult==null || arrResult.length != 1) {
		alert("只允许有一名被保险人");
		return false;
	}
	tSql = "select 1 from lmriskapp where riskcode in (select riskcode from lcpol where contno = '"+ContNo+"') and subriskflag = 'M'";
	var mainRiskResult = easyExecSql(tSql);
	if(mainRiskResult==null) {
		alert("必须购买主险产品！");
		return false;
	}
	if(mainRiskResult.length != 1) {
		alert("只允许购买一个主险产品！");
		return false;
	}
	tSql = "select riskcode from lcpol where contno = '"+ContNo+"' ";
	var riskResult = easyExecSql(tSql);
	if(riskResult == null){
		alert("获取险种信息失败!");
		return false;
	}
	var fhcount = 0;
	for(var i=0;i<riskResult.length;i++){
		var tSRiskSql = "select risktype4,subriskflag from lmriskapp where riskcode = '"+riskResult[i][0]+"' ";
		var tSRiskResult = easyExecSql(tSRiskSql);
		if(tSRiskResult != null ){
			if(tSRiskResult[0][1] == 'S'){
				//var tMRiskSql = "select 1 from dual where (select code1 from ldcode1 where codetype = 'mainsubriskrela' and Code = '"+riskResult[i][0]+"') in (select riskcode from lcpol where contno = '"+ContNo+"')";
				var tMRiskSql = "select 1 from ldcode1 where codetype = 'mainsubriskrela' and Code = '"+riskResult[i][0]+"' and code1 in (select riskcode from lcpol where contno = '"+ContNo+"')";				
				var tMRiskResult = easyExecSql(tMRiskSql);
				if(tMRiskResult == null){
					alert(riskResult[i][0]+"附加险种必须购买主险!");
					return false;
				}
			}
			if(tSRiskResult[0][0] == '2'){//分红险
				fhcount++;
			}
		}
	}
	//没有分红险时，不可填写红利领取方式
	var tSql = "select 1 from lcpol where contno = '"+ContNo+"' and bonusgetmode != '' and bonusgetmode is not null  "
	//prompt('',tSql);
	var arrResult = easyExecSql(tSql);
	if(arrResult!=null && fhcount == 0) {
		alert("未购买分红险产品，不可选择红利领取方式!");
		return false;
	}
	return true;
}

function changRate(){
	window.open("./ChangeRateMain.jsp?prtno=" + prtNo);
}


function checkAppInsr(relation){
    if(relation=="03"){
       var appday = easyExecSql("select AppntBirthday + 18 year from lcappnt where prtno='"+fm.PrtNo.value+"'");
       var birthday = fm.Birthday.value;
       var fdate = new Date(Date.parse(appday[0][0].replace(/-/g,"/")));
       var cdate = new Date(Date.parse(birthday.replace(/-/g,"/")));
       if(Date.parse(cdate)-Date.parse(fdate) < 0){
           if(!confirm("被保人与投保人年龄差距小于18岁，是否继续？")){
               return false;
           }
       }
    }
    return true;
}

function chenkIdNo(idno,idtype){
        if(idtype == "" || idtype == null){
             alert("证件类型不能为空！");
             return false;
        }else{
             var arr = easyExecSql("select code1,codealias,othersign from ldcode1 where codetype='checkidtype' and code='"+idtype+"'");
             if(arr!=null){
                var othersign = arr[0][2];
                var first = arr[0][0];
                var two = arr[0][1];
                if("double"==othersign){
                   if(idno.length!=first&&idno.length!=two){
                       alert("录入的证件号码有误，请检查！");
                       return false;
                   }
                }
                if("min"==othersign){
                   if(idno.length<first){
                      alert("录入的证件号码有误，请检查！");
                       return false;
                   }
                }
                if("between"==othersign){
                   if(idno.length<first||idno.length>two){
                      alert("录入的证件号码有误，请检查！");
                      return false;
                   }
                }
                
             }else{
                alert("选择的证件类型有误，请检查！");
                return false;
             }
        }
        
        return true;
}

function checkMobile(mobile){
   if(mobile != "" && mobile != null){
      if(mobile.length!=11){
          alert("移动电话不符合规则，请核实！");
          return false;
      }else{
         if(mobile.substring(0,2)!=13&&mobile.substring(0,2)!=16&&mobile.substring(0,2)!=14&&mobile.substring(0,2)!=15&&mobile.substring(0,2)!=18&&mobile.substring(0,2)!=17&&mobile.substring(0,2)!=19){
            alert("移动电话不符合规则，请核实！");
            return false;
         }
      }
   }
   return true;
}

function BirthdaySexAppntIDNo()
{
	if(fm.all('IDType').value=="0")
	{
		if(fm.all('Birthday').value!=getBirthdatByIdNo(fm.all('IDNo').value) || fm.all('Sex').value!=getSexByIDNo(fm.all('IDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
	
	return true;
}

function checkAllInsu(){
    var mobile = fm.Mobile.value;
    var realtion= fm.RelationToAppnt.value;
    var idno = fm.IDNo.value;
    var idtype = fm.IDType.value; 
    var name = fm.Name.value;
    if(BirthdaySexAppntIDNo()==false)
    {
        return false;
    }       
	if(!chenkIdNo(idno,idtype)){
	  return false;
	}

	if(!checkAppInsr(realtion)){
	  return false;
	} 
	
	if(mobile != null && mobile !="") {
	    if(!isInteger(mobile) || mobile.length != 11){
		    alert("被保人移动电话需为11位数字，请核查！");
   	      	return false;
	    } 
	}

	if(!checkMobile(mobile)){
	  return false;
	}
	
	if(realtion!="00"){
	    if(name.length<2){
	       alert("被保人姓名需大于1个字符！");
	       return false;
	    }
	}
	if(!checkAddress()){
		return false;
	}
	
	return true;
}

function checkAddress(){
	   var PostalProvince = fm.PostalProvince.value;
	   var PostalCity = fm.PostalCity.value;
	   var PostalCounty = fm.PostalCounty.value;
	   var PostalStreet = fm.PostalStreet.value;
	   var PostalCommunity = fm.PostalCommunity.value;
	   //校验联系地址级联关系
	   var checkCityError = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province.value+"' "
			   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"' )");
	   if(checkCityError == null || checkCityError == ""){
		   alert("被保人省、市地址级联关系不正确，请检查！");
		   return false;
	   }
	   var checkCityError2 = easyExecSql("select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County.value+"')");
	//   var checkCityError2 = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province.value+"' "
//		   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County.value+"'))");
	   if(checkCityError2 == null || checkCityError2 == ""){
		   alert("被保人市、县地址级联关系不正确，请检查！");
		   return false;
	   }
	   //当省级单位下没有市县级单位或市级单位下没有县级单位的时候改变拼接完整地址的方式
	   if(fm.PostalCity.value == '空' && fm.PostalCounty.value == '空'){
		   fm.PostalAddress.value=PostalProvince+PostalStreet+PostalCommunity;
	   }else if(fm.PostalCity.value != '空' && fm.PostalCounty.value == '空') {
		   fm.PostalAddress.value=PostalProvince+PostalCity+PostalStreet+PostalCommunity;
	   }else{
		   fm.PostalAddress.value=PostalProvince+PostalCity+PostalCounty+PostalStreet+PostalCommunity;
	   }
	   return true;
	}

function controlNativeCity(displayFlag)
{
  if(fm.NativePlace.value=="OS"){
    fm.all("NativePlace1").style.display = displayFlag;
	fm.all("NativeCity1").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="国家";
  }else if(fm.NativePlace.value=="HK"){
    fm.all("NativePlace1").style.display = displayFlag;
	fm.all("NativeCity1").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="地区";
  }else{
    fm.all("NativePlace1").style.display = "none";
	fm.all("NativeCity1").style.display = "none";
  }
}

function checkNative(){
   if(fm.NativePlace.value=="OS"||fm.NativePlace.value=="HK"){
      if(fm.NativeCity.value==""||fm.NativeCity.value==null){
         alert("被保人所属国家/地区未录入！");
         return false;
      }
   }
   return true;
}

function getAudioAndVideo(){
  window.open("http://10.214.4.143:9001/prpcard/video/"+trim(fm.PrtNo.value)+".rar","window1");
  fm.VideoFlag.value="1"; 
}

//为代码赋汉字
function setCodeName(fieldName, codeType, code)
{
  var sql = "select CodeName('" + codeType + "', '" + code + "') from dual";
  var rs = easyExecSql(sql)
  if(rs)
  {
    fm.all(fieldName).value = rs[0][0];
  }
}  

function changePostalAddress(){
	if(fm.PostalCity.value == '空' ){
		   fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalStreet.value+fm.PostalCommunity.value;
	   }else if(fm.PostalCity.value != '空' && fm.PostalCounty.value == '空') {
		   fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalCity.value+fm.PostalStreet.value+fm.PostalCommunity.value;
	   }else{
		   fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalCity.value+fm.PostalCounty.value+fm.PostalStreet.value+fm.PostalCommunity.value;
	   }
//	fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalCity.value+fm.PostalCounty.value+fm.PostalStreet.value+fm.PostalCommunity.value;
}