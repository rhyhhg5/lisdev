<!--%@include file="../common/jsp/UsrCheck.jsp"%-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ContInputInit.jsp"%>
  <SCRIPT src="ContInput.js"></SCRIPT>
  <SCRIPT src="ContAutoMove.js"></SCRIPT>
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  <SCRIPT src="InputConfirm.js"></SCRIPT>
  <!--SCRIPT src="InitDatabaseToCont.js"></SCRIPT-->
  <script>
	var	agentComValue = "ManageCom";
	var agentComName;
    try
    {
        agentComName = fm.ManageCom.value;
    }
    catch(ex)
    {}
  </script>
</head>
<body  onload="initForm();initElementtype();setFocus();" >
  <form action="./ContSave.jsp" method=post name=fm target="fraSubmit">	 
    <!--table  class=common align=center style="display: none">
  		<tr align=right>
  			<td class=button >
  				&nbsp;&nbsp;
  			</td>
  			<td class=button width="10%" align=right>
  				<INPUT class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="return submitForm();">
  			</td>
  			<td class=button width="10%" align=right>
  				<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
  			</td>
  		</tr>
  	</table-->
  	
    <table id="table1">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ManageInfoDiv);">
	  			</td>
	  			<td class="titleImg">管理信息
	  			</td>
	  		</tr>
	  </table>
	  
	  <div id="ManageInfoDiv1" style="display: ''">
	  	<table class=common>
	  		<tr class=common>
	  			<td class=title>
	  				印刷号
	  			</td>
	  			<td class=input>
	  				<Input class= common8 name=PrtNo elementtype=nacessary TABINDEX="-1" readonly  >
	  			</td>
	  			<td class=title>
	  				核保审核号
	  			</td>
	  			<td class=input>
	  				<Input class= common8 name=UWConfirmNo TABINDEX="-1" verify="核保审核号|len<21">
	  			</td>
	  			<td class=title>
	  				管理机构
	  			</td>
	  			<td class=input>
	  				<Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull"  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
	  			</td>
	  			<TD  class= title8>
	          销售渠道
	        </TD> 
	        <TD  class= input8>
	        	<%--<Input class=codeNo name=SaleChnl verify="销售渠道|notnull"  ondblclick="return showCodeList(tSalChnlCode,[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey(tSalChnlCode,[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>--%>
	        	<Input class=codeNo name=SaleChnl verify="销售渠道|notnull"  ondblclick="return showCodeList('unitesalechnl',[this,SaleChnlName],[0,1],null,'1',null,1);" onkeyup="return showCodeListKey('unitesalechnl',[this,SaleChnlName],[0,1],null,'1',null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>
	        	<input type=hidden NAME="SaleChnlDetail" VALUE="01" MAXLENGTH="2"  CLASS="code" ondblclick="return showCodeList('SaleChnlDetail',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('SaleChnlDetail',[this],null,null,null,null,1);" >
	        </TD>
	  		</tr>
	  		<tr class=common id="DLXSInput1" style="display: none">
	  		  <TD  class= title8>
                 是否已进行销售过程全程记录
              </TD>
              <TD  class= input8 >
      		    <input NAME="XSFlag" CLASS=codeNo CodeData="0|^Y|是^N|否" ondblclick="return showCodeListEx('XSFlag',[this,XSFlagName],[0,1]);" onkeyup="return showCodeListKeyEx('XSFlag',[this,XSFlagName],[0,1]);"><input class=codename name=XSFlagName readonly=true >
              </TD>
              <TD  class= title8></TD>
              <TD  class= input8></TD>
              <TD  class= title8></TD>
              <TD  class= input8></TD>  
              <TD  class= title8></TD>
              <TD  class= input8></TD> 
	  		</tr>
	  	</table>
	  </div>
	  
	  <div id="ManageInfoDiv2" style="display: ''">
	  	<table class=common>
	  	
            <!--tr>
                <td colspan="6"><font color="red">对于非银行渠道的交叉销售业务，销售渠道选择“中介”，交叉销售渠道选择“财代健”或者“寿代健”；对于银行渠道的交叉销售，则填写“银行代理”渠道</font></td>
            </tr>
            <tr>
                <td colspan="6"><font color="red">以下“交叉销售渠道”，“交叉销售业务类型”，“对方机构代码”，“对方业务员代码”，“对方业务员姓名”信息，仅集团交叉销售业务需要填写。</font></td>
            </tr-->
            <tr>
            	<td colspan="6"><font color="black">如果是交叉销售,请选择</font><INPUT TYPE="checkbox" NAME="MixComFlag" onclick="isMixCom();"></td>
        	</tr>
        	<%@include file="../sys/MixedSalesAgent.jsp"%>
        	<!--  
	        <tr class="common8" id="GrpAgentComID" style="display: none">
	            <td class="title8">交叉销售渠道</td>
	            <td class="input8">
	                <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" elementtype=nacessary/>
	            </td>
	            <td class="title8">交叉销售业务类型</td>
	            <td class="input8">
	                <input class="codeNo" name="Crs_BussType" id="Crs_BussType" verify="交叉销售业务类型|code:crs_busstype" ondblclick="return showCodeList('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_BussTypeName" readonly="readonly" elementtype=nacessary/><input type =button class=cssButton value="业务说明" onclick="Crs_BussTypeHelp();"> 
	            </td>
	            <td class="title8">&nbsp;</td>
	            <td class="input8">&nbsp;</td>
	        </tr>	
		<tr class=common id="GrpAgentTitleID" style="display: 'none'">
	    		<td CLASS="title" >对方机构代码</td>
				<td CLASS="input" COLSPAN="1" >		
	    	      <Input class="code" name="GrpAgentCom" elementtype=nacessary  ondblclick="return queryGrpAgentCom();">
	    	    </td>
	    	    <td  class= title>对方机构名称</td>
		        <td  class= input>
		          <Input class="common" name="GrpAgentComName" elementtype=nacessary TABINDEX="-1" readonly >
		        </td>  
				<td CLASS="title">对方业务员代码</td>
	    	    <td CLASS="input" COLSPAN="1">
			    <input NAME="GrpAgentCode" VALUE MAXLENGTH="20" CLASS="code" elementtype=nacessary ondblclick="return queryGrpAgent();" >
    		    </td>
	        </tr>
	        <tr class=common id="GrpAgentTitleIDNo" style="display: 'none'">
	            <td  class="title" >对方业务员姓名</td>
	            <td  class="input" COLSPAN="1">
	               <Input  name=GrpAgentName CLASS="common" elementtype=nacessary>
	            </td>
		        <td CLASS="title">对方业务员身份证</td>
		        <td CLASS="input" COLSPAN="1">
			    <input NAME="GrpAgentIDNo" VALUE MAXLENGTH="18" CLASS="code" elementtype=nacessary ondblclick="return queryGrpAgent();" >
    	        </td>
            </tr>
            -->
            
	  		<%--<TR class=common id="DirectID" style="display:''">
		    	<TD  class= title8 id =AgentCode">
	          业务员代码
	        </TD>     
	        <TD  class= input8>
	      		<input NAME="GroupAgentCode" VALUE MAXLENGTH="0" CLASS="code" elementtype=nacessary ondblclick="return queryAgent();" onkeyup="return queryAgent2();" verify="代理人编码|notnull">
	      		<Input NAME=AgentCode VALUE="" MAXLENGTH=0 CLASS=code8 type='hidden'>
	        </TD>
	        <TD  class= title8 id=AgentNameID">
	          业务员名称
	        </TD>        
	        <TD  class= input8>
	      		<Input NAME=AgentName VALUE=""  CLASS=common >
	        </TD> 
	        <TD  class= title8 id= "BlankTD1" style="display: ''">
	        </TD>
	        <TD  class= input8 id= "BlankTD2" style="display: ''">
	        </TD>
          <!--TD  class= title8>
          业务员组别
        </TD>
        <TD  class= input8-->
            <Input class="readonly"  type=hidden readonly name=AgentGroup verify="业务员组别notnull&len<=12" >
        <!--/TD-->  
        </TR>
        --%>
        <TR class=common id="AgencyID" style="display: ''">
	  			<TD  class= title8 id=zhongjiecode style="display: 'none'">
	          <div id="AgencyIDCodeTitle">中介公司代码</div>
          </TD>
          <TD  class= input8 id=zhongjiename style="display:'none'">
            <Input class="codeNo" name=AgentCom ondblclick="return showCodeList(agentcomCode,[this,AgentComName],[0,1],null,agentComValue,agentComName,1);" onkeyup="return showCodeListKey(agentcomCode,[this,AgentComName],[0,1],null,agentComValue,agentComName,1);"><Input class="codeName" name=AgentComName readonly >
          </TD>
		    	<TD  class= title8 id =TXCode>
	          <div id="AgentCodeTitile">业务员代码</div>
	        </TD>	        
	        <TD  class= input8>
	            <input NAME="GroupAgentCode" VALUE MAXLENGTH="0" CLASS="code" elementtype=nacessary ondblclick="return queryAgent();" onkeyup="return queryAgent2();" verify="代理人编码|notnull">
	      		<Input NAME=AgentCode VALUE="" MAXLENGTH=0 CLASS=code8 type='hidden' >
	        </TD>
	        <TD  class= title8 id=TXName>
	          <div id="AgentNameTitile">业务员名称</div>
	        </TD>	        
	        <TD  class= input8>
	      		<Input NAME=AgentName VALUE="" readonly CLASS=common >
	        </TD> 
	        <TD  class= title8 id= "BlankTD1" style="display: ''">
	        </TD>
	        <TD  class= input8 id= "BlankTD2" style="display: ''">
	        </TD>
          <!--TD  class= title8>
          业务员组别
        </TD>
        <TD  class= input8-->
            <Input class="readonly"  type=hidden readonly name=AgentGroup verify="业务员组别notnull&len<=12" >
        <!--/TD-->  
        </TR>
        <tr class=common>
        	<TD  class= title8 id = InputDateText>
            投保单填写日期
          </TD>
          <TD  class= input8 id = InputDateClass >
            <Input class="coolDatePicker"  name=PolApplyDate elementtype=nacessary verify="投保单填写日期|notnull&&date" >
          </TD> 
          <TD  class= title8 id = ReceiveDateText>
            收单日期
          </TD>
          <TD  class= input8 id = ReceiveDateClass>
            <Input class="coolDatePicker"  name=ReceiveDate elementtype=nacessary verify="收单日期|notnull&&date" >
          </TD>   
          <TD  class= title8 id = CValiDateText>
            保单生效日
          </TD>
          <TD  class= input8 id = CValiDateClass>
            <Input class="coolDatePicker" elementtype=nacessary name=CValiDate  verify="生效日|notnull&&date" >
          </TD>     
            
          <TD  class= input8 id = FillControl style="display:none">
            <Input class="readonly" name=FillControlText>
          </TD>       	        
  
    			<td class=title id=Tempfeetitle style="display: 'none'">
    				缴费凭证号
    			</td>
          <TD  class= input  id=input1 style="display: 'none'">
          	<Input name=TempFeeNo VALUE=""  CLASS=common  >
          </TD>
        </tr>
		<!-- 新增代理销售业务员信息 20120611 by  zcx-->
        <TR class=common id="AgentSaleCodeID" style="display: 'none'">
	      	<TD  class= title8 >
            <div id="AgentSaleCode1Title">网点销售人员编码</div>
          </TD>
          <TD  class= input8>
      			<Input NAME=AgentSaleCode VALUE="" MAXLENGTH=10 CLASS=code8 ondblclick="return queryAgentSaleCode();"onblur="return queryAgentSaleCode2();">
         </TD>
          <TD  class= title8>
            <div id="AgentSaleCode2Title">网点销售人员名称</div>
          </TD>
          <TD  class= input8>
      			<Input NAME=AgentSaleName VALUE=""  readonly CLASS=common >
         </TD>         
          <TD  class= title8>
          </TD>
          <TD  class= input8>
          </TD>       
        </TR>
	    </table>
	  </div>	  
	  <table class=common>
      	<tr>
            <td colspan="6"><font color="black">综合开拓标志</font><INPUT TYPE="checkbox" NAME="ExtendFlag" onclick="isAssist();"></td>
        </tr>
		<tr class=common id="ExtendID" style="display: 'none'">
    		<td CLASS="title" >协助销售渠道</td>
			<td CLASS="input" COLSPAN="1" >
			  <Input class=codeNo name="AssistSaleChnl" ondblclick="return showCodeList('assistsalechnl_y',[this,AssistSalechnlName],[0,1]);" onkeyup="return showCodeListKey('assistsalechnl_y',[this,AssistSalechnlName],[0,1]);"><input class=codename name=AssistSalechnlName readonly=true elementtype=nacessary>
    	    </td>
    	    <td  class= title>协助销售人员代码</td>
	        <td  class= input>
	          <Input class="code8" name="AssistAgentCode" elementtype=nacessary ondblclick="return queryAssistAgent();">
	        </td>  
			<td CLASS="title">协助销售人员姓名</td>
			<td CLASS="input" COLSPAN="1">
				<input NAME="AssistAgentName" CLASS="common" TABINDEX="-1" readonly>
    		</td>
    	</tr>
    	</table>
        <br />
        
        <table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">投保人帐户信息：</td>
            </tr>
        </table>        
        <div id="divAppntAccInfo" style="display:''">
            <table class="common">
                <tr>
                    <td class="title">缴费模式</td>
                    <td class="input">
                        <input class="codeNo" name="PayMethod" ondblclick="return showCodeList('paymethod',[this,PayMethodName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('paymethod',[this,PayMethodName],[0,1],null,null,null,1);"><input class="codename" name="PayMethodName" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                <tr>
                    <td class="title">投保人帐户编号</td>
                    <td class="input">
                        <input class="readonly" name="CustNoAppAcc" readonly="readonly" />
                    </td>
                    <td class="title">投保人帐户户名</td>
                    <td class="input">
                        <input class="readonly" name="CustNameAppAcc" readonly="readonly" />
                    </td>
                    <td class="title">投保人帐户余额</td>
                    <td class="input">
                        <input class="readonly" name="BalanceAppAcc" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>
        
        <br />
	  
    <!--table id="table1">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,AppntInfoDiv);">
	  			</td>
	  			<td class="titleImg">投保人信息
	  			</td>
	  		</tr>
	  </table>
	  <div id="AppntInfoDiv" style="display: ''">
	  	<table class=common>
	  		<tr class=common style="display:''">
	  			<td class=title COLSPAN="1">
	  				投保人客户号
	  			</td>
	  			<td class=input COLSPAN="1">
	  				<Input class= common name=AppntNo  verify="投保人客户号|len<=60">
	  			</td>
	  			<td><Input type=button class= cssbutton VALUE="查询" OnClick="queryAppnt()"></td><td></td>
	  		</tr>
	  	</table>
	  </div-->
	  	 
	  <DIV id=DivLCAppntIndButton STYLE="display:''">
    <!-- 投保人信息部分 -->
      <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
          </td>
          <td class= titleImg>
           投保人资料(客户号)：<Input class= common  name=AppntNo>
            <INPUT id="butBack" VALUE="查  询" TYPE=button class= cssButton onclick="queryAppntNo();">
            首次投保客户无需填写客户号）
          </td>
          <!--td class= titleImg>
            投保人资料(客户号)：<Input class= common name=AppntNo >
          </td-->
        </tr>
      </table>
    </DIV>
 
	  	 
	  	<table class = common>
	  	  <div id="AppntInfoDiv1" style="display: ''">
	  	    <tr class=common id="AppntTypeTrID">
  	  			 <TD  class= title8>
              投保人类型
            </TD>
            <TD  class= input8>
              <Input class=codeNo name=AppntType value="1" readonly CodeData="0|^1|个人^2|团体" verify="投保人类型|notnull" ondblclick="return showCodeListEx('AppntType',[this,AppntTypeName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('AppntType',[this,AppntTypeName],[0,1],null,null,null,1);"><input class=codename name=AppntTypeName elementtype=nacessary value="个人" readonly=true >  
            </TD>	
  	  		</tr>
  	  		<tr class=common>
  	  			<td class=title COLSPAN="1" id="AppntNameTitleID">
  	  				<div id="AppntNameTitleValueID">姓名</div>
  	  			</td>
  	  			<td class=input COLSPAN="1" id="AppntNameInputID">
  	  				<Input class= common name=AppntName elementtype=nacessary verify="姓名|notnull&len<=60&len>1">
  	  			</td>
  	  			<td class=title COLSPAN="1" id = EnglishNameText>
  	  				拼音
  	  			</td>
  	  			<td class=input COLSPAN="1" id = EnglishNameClass>
  	  				<Input class=common name=EnglishName>
  	  			</td>
  	  			 <TD  class= title8 id="AppntIDTypeTitleID">
              证件类型
            </TD>
            <TD  class= input8 id="AppntIDTypeInputID">
              <Input class= codeno name=AppntIDType value="0" verify="证件类型|notnull&code:IDType" readonly ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=AppntIDTypeName value="身份证" elementtype=nacessary readonly=true>
            </TD>
            <TD  class= title COLSPAN="1" id="AppntIDNoTitleID">
              证件号码
            </TD>
            <TD  class= input COLSPAN="1" id="AppntIDNoInputID">
              <Input class= common name=AppntIDNo verify="证件号码|len<=30" onblur="getAppntBirthdaySexByIDNo(this.value)">
            </TD>
        <TD  class= title>
               授权使用客户信息
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntAuth" readonly=true  verify="授权使用客户信息|notnull&code:Auth" ondblclick="return false;return showCodeList('Auth',[this,AppntAuthName],[0,1]);"  onkeyup="return false;return showCodeListKey('Auth',[this,AppntAuthName],[0,1]); "><input class=codename name=AppntAuthName readonly=true elementtype=nacessary>    
          </TD>	  			
  	  		</tr>
  	  		<tr class=common id="Row3ID">
  	  		 <TD  class= title COLSPAN="1" id="AppntBirthdayTitleID">
  	          证件生效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" id="AppntIDStartDateInput">
  	        	<Input class= coolDatePicker name=IDStartDate  verify="证件生效日期|date">
  	        </TD>
  	        	 <TD  class= title COLSPAN="1" id="AppntBirthdayTitleID">
  	          证件失效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" id="AppntIDEndDateInput">
  	        	<Input class= coolDatePicker name=IDEndDate  verify="证件失效日期|date">
  	        </TD>
  	        <TD  class= title COLSPAN="1" id="AppntSexTitleID">
  	          性别
  	        </TD> 
  	        <TD  class= input COLSPAN="1" id="AppntSexInputID">
  	        	<Input class= codeno name=AppntSex verify="性别|notnull&code:sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1],null,null,null,1);"><input class=codename name=SexName readonly elementtype=nacessary>
  	        </TD>
  	         <TD  class= title COLSPAN="1">
	          客户类型
	        </TD>
	        <TD  class= input COLSPAN="1">
	        		<Input class= codeno name=appnt_CountyType CodeData="0|^1|城镇^2|农村" ondblclick="return showCodeListEx('appnt_CountyType',[this,appnt_CountyName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('appnt_CountyType',[this,appnt_CountyName],[0,1],null,null,null,1);"><input class=codename name=appnt_CountyName >
	        </TD>	 	    		       	       	       	       	  
  	        </tr>
  	  		<tr class=common id="Row2ID">            
  	        <TD  class= title COLSPAN="1" id="AppntBirthdayTitleID">
  	          出生日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" id="AppntBirthdayInputID">
  	        	<Input class= coolDatePicker name=AppntBirthday elementtype=nacessary verify="出生日期|notnull&date">
  	        </TD>
  	        <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntMarriage" style="width:50px" verify="投保人婚姻状况|notnull&code:Marriage" ondblclick="return showCodeList('Marriage',[this,AppntMarriageName],[0,1]);" onkeyup="return showCodeListKey('Marriage',[this,AppntMarriageName],[0,1]);"><input class=codename name=AppntMarriageName readonly=true>    
          </TD> 
          <TD  class= title id="NativePlaceTitleID">
              国籍
            </TD>
            <TD  class= input id="NativePlaceInputID">
              <Input class=codeno name=AppntNativePlace verify="国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,NativePlaceName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('NativePlace',[this,NativePlaceName],[0,1],null,null,null,1);" ><input class=codename name=NativePlaceName readonly=true >
            </TD>
          <TD  class= title id="NativePlace" style="display: none">
          <div id="NativeCityTitle">国家</div>
          </TD>
          <TD  class= input id="NativeCity" style="display: none">
          <input class=codeNo name="AppntNativeCity" ondblclick="return showCodeList('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);"><input class=codename name=AppntNativeCityName readonly=true >    
          </TD>
	        </tr>
  	       <TR>
  	       <TD  class= title id = OccupationCodeText>
  	          职业代码
  	        </TD> 
  	        <TD  class= input id = OccupationCodeClass>
  	        	<Input class= codeno name=OccupationCode style="width:50px" ondblclick="return showCodeList('OccupationCode',[this,OccupationName],[0,1],null,null,null,1,300);" onkeyup="return showCodeListKey('OccupationCode',[this,OccupationName],[0,1],null,null,null,1,300);"><input class=codename name=OccupationName readonly=true  style="width:110px">
  	        </TD>
  		      <TD  class= title>
      	     年收入(万元)
   	 			</TD>
    		<TD  class= input>
      	<Input class= 'common' name=AppntSalary>
    		</TD> 
    		<TD  class= title>
              家庭收入(万元)
            </TD>
            <TD  class= input>
              <Input class= common name=FamilySalary >
            </TD>
    		
  	       </TR>	      
  	       <tr class=common>
  	  			<td class=title id=ZhiweiApptitle style="display: 'none'">
  	  				岗位职务
  	  			</td>
  		       <TD  class= input  id=ZhiweiApp style="display: 'none'">
  		      		<Input name=Position VALUE="" CLASS=common>
  		       </TD>
<%--  	      	<td class=title8 COLSPAN="1" id="PostalAddressTitleID">--%>
<%--  	      		联系地址--%>
<%--  	      	</td>--%>
<%--  	      	<td class=input8 COLSPAN="3" id="PostalAddressID">--%>
<%--  		      	<input class= common3 elementtype=nacessary name=PostalAddress verify="联系地址|notnull" style="width:350">--%>
<%--  		      </td>--%>
  		      <TD  class= title8 id="ZipCodeTitleID">
              邮政编码
            </TD>
            <TD  class= input8 id="ZipCodeInputID">
              <Input class= common8 name=ZipCode verify="邮政编码|zipcode">
            </TD> 
            <td class=title COLSPAN="1" id = WorkNameText>
  	      		工作单位
  	      	</td>
  	      	<td class=input8 COLSPAN="3" id = WorkNameClass>
  		      	<input class= common3 name=GrpName verify="工作单位"> 
  		      </td>
  	      </tr>
  	      <tr class=common  id="PostalAddressID" style="display: none">
  	      <td class=title COLSPAN="1">
  	      		联系地址
  	      	</td>
  	      	<TD  class= input colspan=5  >
  		      	<input class= common3   name=PostalAddress  readonly=true>
  	      	</td>
  	      </tr>
  	      <tr class=common>
  	      <td class=title COLSPAN="1" id="PostalAddressTitleID">
  	      		联系地址
  	      	</td>
  	      	<TD  class= input colspan=9  >
			<Input class="codeno" name=Province verify="省（自治区直辖市）|notnull"  readonly=true ondblclick="return showCodeList('Province1',[this,PostalProvince],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province1',[this,PostalProvince],[0,1],null,'0','Code1',1);" onblur="changePostalAddress();"><input class=codename name=PostalProvince readonly=true elementtype=nacessary>省（自治区直辖市）
			<Input class="codeno" name=City verify="市|notnull" readonly=true ondblclick="return showCodeList('City1',[this,PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onkeyup="return showCodeListKey('City1',[this,PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onblur="changePostalAddress();"><input class=codename name=PostalCity readonly=true elementtype=nacessary>市
			<Input class="codeno" name=County verify="县（区）|notnull" readonly=true ondblclick="return showCodeList('County1',[this,PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onkeyup="return showCodeListKey('County1',[this,PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onblur="changePostalAddress();"><input class=codename name=PostalCounty readonly=true elementtype=nacessary>县（区）
			<input  class= common10 name=PostalStreet verify="乡镇（街道）|notnull"  onblur="changePostalAddress();"> 乡镇（街道）
	    	<input  class= common10 name=PostalCommunity verify="村（社区）|notnull" onblur="changePostalAddress();"> 村（社区）
	    </TD>
  	      	<!-- <td class= input colspan=5 id="PostalAddressID">
  		      	<input class= common3 type="hidden" name=PostalAddress >
  		      	<input  class= common10 name=PostalProvince verify="投保人联系地址|notnull"> 省（自治区直辖市）
		        <input  class= common10 name=PostalCity verify="投保人联系地址|notnull"> 市
		        <input  class= common10 name=PostalCounty verify="投保人联系地址|notnull"> 县（区）
		        <input  class= common10 name=PostalStreet verify="投保人联系地址|notnull"> 乡镇（街道）
		        <input  class= common10 name=PostalCommunity verify="投保人联系地址|notnull"> 村（社区）     
		        </td> -->
  	      </tr>  	      
  	      <tr class=common8 >
  	      	<TD  class= title8 COLSPAN="1" id="PhoneTitleID">
              联系电话
            </TD>
            <TD  class= input8 COLSPAN="1" id="PhoneInputID">
              <Input class= common8 name=Phone elementtype=nacessary verify="联系电话|len<=30&notnull">
            </TD>
            <TD  class= title8 id="MobileTitleID">
              移动电话
            </TD>
            <TD  class= input8 id="MobileInputID">
              <Input class= common8 name=Mobile   verify="移动电话|len<=30">
            </TD>
            <TD  class= title8 COLSPAN="1" id="CompanyPhoneTitleID" style="display: none">
              办公电话
            </TD>
            <TD  class= input8 COLSPAN="1" id="CompanyPhoneInputID" style="display: none">
              <Input class= common8 name=CompanyPhone verify="办公电话|len<=30">
            </TD>
            <TD  class= title8 id="EMailTitleID" COLSPAN="1" style="display: none">
              电子邮箱
            </TD>
            <TD class= input8 id="EMailInputID" COLSPAN="1" style="display: none">
              <Input class= common8 name=EMail verify="电子邮箱|len<=60&Email">
            </TD>
            <TD  class= title8 COLSPAN="1" id="FaxTitleID">
              传真
            </TD>
            <TD  class= input8 COLSPAN="1" id="FaxInputID">
              <Input class= common8 name=Fax verify="传真|len<=30">
            </TD>
  	      </tr>
      </table>
	  
	  <hr>
	  
	  <div id="PayInfoTitleDiv" style="display: ''">
			<table id="table5">
		  		<tr>
		  			<td>
		  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,PayInfoDiv);">
		  			</td>
		  			<td class="titleImg">缴费信息
		  			</td>
		  		</tr>
		  </table>
		</div>
	  <div id="PayInfoDiv" style="display: ''">
		  <table class="common">
		  	<tr class=common8 id="PayerTypeTRID">
			  	<TD  class= title8 COLSPAN="1">
			      支付者
			    </TD>
			    <TD  class= input8 COLSPAN="1">
			      <Input class=codeNo name=PayerType value="1" CodeData="0|^1|投保人^2|主被保人" verify="缴费频次|notnull" ondblclick="return showCodeListEx('PayerType',[this,PayerTypeName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('PayerType',[this,PayerTypeName],[0,1],null,null,null,1);"><input class=codename name=PayerTypeName elementtype=nacessary value="投保人" readonly=true >  
			    </TD>
			    <TD  class= title8 COLSPAN="1">
			    </TD>
			    <TD  class= input8 COLSPAN="1">
			    </TD>
				</tr>
		  	<tr class=common8>
			  	<TD  class= title8 COLSPAN="1">
			      缴费频次
			    </TD>
			    <TD  class= input8 COLSPAN="1">
			      <Input class=codeNo name=PayIntv value="0" CodeData="0|^0|趸缴^1|月缴 ^3|季缴 ^6|半年缴 ^12|年缴" verify="缴费频次|notnull" ondblclick="return showCodeListEx('grppayintv',[this,PayIntvName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('grppayintv',[this,PayIntvName],[0,1],null,null,null,1);"><input class=codename name=PayIntvName elementtype=nacessary value="趸缴" readonly=true >  
			    </TD>
			    <TD  class= title8 COLSPAN="1">
			      缴费方式
			    </TD>
			    <TD  class= input8 COLSPAN="1">
			      <Input class=codeNo name=PayMode verify="缴费方式|notnull"  ondblclick="return showCodeList('PayModeInd',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="showCodeListKey('PayModeInd',[this,PayModeName],[0,1],null,null,null,1);"><input class=codename name=PayModeName elementtype=nacessary readonly=true >
			    </TD>
				</tr>
	    </table>
	  </div>		    
	   <div id="BankCom" style="display: ''">	
	   	 <table class="common">
	   	   <tr>		    
			    <TD  class= title8 COLSPAN="1" >
	          开户银行
	        </TD>
	        <TD  class= input8 COLSPAN="1" >
	          <Input class=codeNo  name=BankCode  verify="开户银行|len<=24" ondblclick="return showCodeList('BankBranchCom',[this,BankName],[0,1],null,fm.ManageCom.value,'ComCode',1);" onkeyup="return showCodeListKey('BankBranchCom',[this,BankName],[0,1],null,fm.ManageCom.value,'ComCode',1);"><input class=codename name=BankName >
	        </TD>
			  </tr>
			  <tr class=common8>
			  	<TD  class= title8 >
	          账&nbsp;&nbsp;&nbsp;号
	        </TD>
	        <TD  class= input8 >
	          <Input class= common8 name=BankAccNo  verify="帐号|len<=40">
	        </TD>
	        <TD  class= title8 >
	          户&nbsp;&nbsp;&nbsp;名
	        </TD>
	        <TD  class= input8 >
	          <Input class= common8 name=AccName  verify="户名|len<=40">
	        </TD>   
			  </tr>
			  <tr class="common8">
			      <td class="title8">
			          是否需要续期缴费提醒
			      </td>
			      <td class="input8" colspan="3">
			          <input name="DueFeeMsgFlag" class="codeNo" ondblclick="return showCodeList('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);" verify="是否需要续期缴费提醒|notnull&code:duefeemsgflag"><input class="codename" name="DueFeeMsgFlagName" readonly=true elementtype=nacessary />
			      </td>
			  </tr>
	    </table>
	  </div>
	  
	  <div id="RemarkDiv" style="display ''">
  	  <table id="table6">
    		<tr>
    			<td>
    			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,RemarkDiv);">
    			</td>
    			<td class="titleImg">备注栏
    			</td>
    		</tr>
  	  </table>
	  	<table class=common >
			  <TR  class= common> 
		      <TD  class= title8> 特别约定 </TD>
		    </TR>
		    <TR  class= common>	
		      <TD  class= title8>
		        <textarea name="Remark" cols="110" rows="3" class="common3" width=100% value=""></textarea>
		      </TD>
		    </TR>
		  </table>
	  </div>
	  <Div  id= "divInputContButton" style= "display: ''" style="float: right">
	    <input TYPE=button name="findIssueButton" class=cssButton VALUE="查看外包错误" onclick="return findIssue();">
	    <INPUT class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="return submitForm();">
	    <INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
      <INPUT class=cssButton id="intoInsuredButtonID" VALUE="录入被保险人" TYPE=button onclick="intoInsured();">    
    	<INPUT class=cssButton id="inputConfirmButtonID" name=lrwb VALUE="录入完毕" TYPE=button onclick="inputConfirm(1);" style="display: none">
    	<INPUT class=cssButton id="approveConfirmButtonID" VALUE="复核完毕" TYPE=button onclick="inputConfirm(2);" style="display: none"> 
    </DIV>
    <Input type=hidden class=common name=GrpContNo>
	  <input type=hidden id="" name="InputDate">
	  <INPUT  type= "hidden" class= Common name= prtn1 value= "<%=request.getParameter("prtNo")%>"><!-- 印刷号-->
	  <input type=hidden id="" name="WorkFlowFlag">
	  <input type=hidden id="" name="MissionID">
	  <input type=hidden id="" name="SubMissionID">
	  <input type=hidden id="" name="fmAction">
	  <input type=hidden NAME="ProposalContNo" CLASS="common" readonly TABINDEX="-1" MAXLENGTH="20"  >
	  <input type=hidden id="" name="ContNo">
	  <input type=hidden id="" name="MakeDate">
	  <input type=hidden id="" name="MakeTime">
	  <input type=hidden id="" name="AppntMakeDate">
	  <input type=hidden id="" name="AppntMakeTime">
	  <input type=hidden id="" name="CardFlag">
	  <input type=hidden id="" name="OccupationType">
	  <input type=hidden id="" name="IntlFlag">
	  <input type=hidden id="" name="OldPayIntv">
	  <input type= "hidden" class= Common name=VideoFlag>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>