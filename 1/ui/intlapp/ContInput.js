//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var mAction = "";
var showInfo;
var turnPage = new turnPageClass();
var succFlag = false;
var ACTION_INSERT = "INSERT";
var ACTION_UPDATE = "UPDATE";

window.onfocus=myonfocus;
parent.fraMain.rows = "0,0,0,0,*";
var ConfirmFlag = false;

var tSalChnlCode = "salechnlall";

//**************************初始化区域***************************

//初始化按钮
function initButton()
{
  if(LoadFlag == "5")
  {
		PostalAddressID.style.display = "";
    //根据外测反馈,本页面不需要"录入完毕"和"复核完毕"按钮
    //fm.all("inputConfirmButtonID").style.display = "none";
    //fm.all("approveConfirmButtonID").style.display = "";
    fm.all("saveButton").style.display = "none";
    fm.intoInsuredButtonID.value = "复核被保险人";
    fm.VideoFlag.value=tVideoFlag;
    if(tXSFlag=="Y"){
       fm.XSFlag.value=tXSFlag;
       fm.XSFlagName.value="是";
    }else if(tXSFlag=="N"){
       fm.XSFlag.value=tXSFlag;
       fm.XSFlagName.value="否";
    }
    //在复核时禁止修改管理机构   by zhangyagn   2011-09-15
    //fm.ManageCom.ondblclick = "";
    //fm.ManageCom.onkeyup = "";
    fm.ManageCom.readOnly = true;
    //----------------------
  }
  else if(LoadFlag == "6")
  {
	PostalAddressID.style.display = "";
    fm.all("saveButton").style.display = "none";
    fm.all("modifyButton").style.display = "none";
    fm.all("inputConfirmButtonID").style.display = "none";
    fm.all("approveConfirmButtonID").style.display = "none";
    fm.intoInsuredButtonID.value = "查看被保人信息";
  }
}

/*********************************************************************
 *  查询显示保单信息
 *  参数  ： 
 *  返回值：  无
 *********************************************************************
 */
function queryContInfo()
{	
    queryManageMentInfo();
    queryAppntInfo();
    queryFeeInfo();
    
    qryAppAccInfo();
    initAgentSaleCode();
    initCountyType();
}

//得到管理信息
function queryManageMentInfo()
{
  var sql = "select a.ManageCom, a.SaleChnl, a.SaleChnlDetail, a.AgentCode, "
          + " (select Name from LAAgent where AgentCode = a.AgentCode), "
          + " a.AgentCom, a.AgentGroup, a.InputDate, a.ReceiveDate, "
          + " a.AppntNo, a.AppntName, a.UWConfirmNo, a.CValiDate, a.PolApplyDate, "
          + " a.MakeDate, a.MakeTime, a.GrpContNo, a.ContNo, a.ProposalContNo, a.TempFeeNo, "
          + " a.GrpAgentCom, a.GrpAgentCode, a.GrpAgentName, a.Crs_SaleChnl, a.Crs_BussType ,a.GrpAgentIDNo ,a.DueFeeMsgFlag,a.PayMethod,getUniteCode(a.agentcode) "
          + " from LCCont a "
          + " where PrtNo = '" + fm.PrtNo.value + "' "
          + " and ContType = '1' ";
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] != null && rs[0][0] != "" && rs[0][0] != "null")
  {
    try
    {
      fm.ManageCom.value = rs[0][0]; 
      var sql1 = "select Name from LDCom where ComCode = '" + fm.ManageCom.value + "' ";
	  var result1 = easyExecSql(sql1);
	  if(result1)
	  {
	    fm.ManageComName.value = result1[0][0];
	  }
      if(fm.all('ManageCom').value.length>=4&&LoadFlag == "5"){
      	if(fm.all('ManageCom').value.substring(0,4)=='8691'){
      	    fm.all("DLXSInput1").style.display="";
      	}
      }
      fm.SaleChnl.value = rs[0][1];
      setCodeName("SaleChnlName", "unitesalechnl", fm.SaleChnl.value);
      afterCodeSelect('unitesalechnl', '');
      fm.SaleChnlDetail.value = rs[0][2];
      fm.GroupAgentCode.value = rs[0][28];
      fm.AgentCode.value = rs[0][3];
      //fm.AgentCodeG.value = rs[0][28];
      fm.AgentName.value = rs[0][4];
      //fm.AgentNameG.value = rs[0][4];
      fm.AgentGroup.value = rs[0][6];
      //fm.AgentGroupG.value = rs[0][6];
      fm.AgentCom.value = rs[0][5];
      if(fm.SaleChnl.value == "04"||fm.SaleChnl.value == "15"||fm.SaleChnl.value == "03"||fm.SaleChnl.value == "10"||fm.SaleChnl.value == "20"){
      	  var sql2 = "select name from LACom where agentcom = '" + fm.AgentCom.value + "' ";
		  var result2 = easyExecSql(sql2);
		  if(result2)
		  {
		    fm.AgentComName.value = result2[0][0];
		  }
      }
      fm.InputDate.value = rs[0][7];
      fm.ReceiveDate.value = rs[0][8];
      fm.AppntNo.value = rs[0][9];
      fm.AppntName.value = rs[0][10];
      fm.UWConfirmNo.value = rs[0][11];
      fm.CValiDate.value = rs[0][12];
      //if(fm.PolApplyDate.value == "" || fm.PolApplyDate.value == "null")
      //{
        fm.PolApplyDate.value = rs[0][13];
      //}
      fm.MakeDate.value = rs[0][14];
      fm.MakeTime.value = rs[0][15];
      fm.GrpContNo.value = rs[0][16];
      fm.ContNo.value = rs[0][17];
      fm.ProposalContNo.value = rs[0][18];
      fm.TempFeeNo.value = rs[0][19];
      
      // 集团交叉业务要素
      fm.GrpAgentCom.value = rs[0][20];
      fm.GrpAgentCode.value = rs[0][21];
      fm.GrpAgentName.value = rs[0][22];
      fm.Crs_SaleChnl.value = rs[0][23];
      setCodeName("Crs_SaleChnlName", "crs_salechnl", fm.Crs_SaleChnl.value);
      fm.Crs_BussType.value = rs[0][24];
      setCodeName("Crs_BussTypeName", "crs_busstype", fm.Crs_BussType.value);
      fm.GrpAgentIDNo.value = rs[0][25];
      fm.DueFeeMsgFlag.value = rs[0][26];
      fm.PayMethod.value = rs[0][27];
      if(rs[0][20] != ""&&rs[0][21]!=""&&rs[0][22]!=""&&rs[0][23]!=""&&rs[0][24]!=""&&rs[0][25]!="")
      {
          fm.MixComFlag.checked = true;
          if(fm.MixComFlag.checked == true)
          {
              fm.all('GrpAgentComID').style.display = "";
	          fm.all('GrpAgentTitleID').style.display = "";
	          fm.all('GrpAgentTitleIDNo').style.display = "";
          }
          /*
          var strSql = "select org_lvl from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
		  var arrResult1 = easyExecSql(strSql);
		  if (arrResult1 != null) {
			  if(arrResult1[0][0] == "0")
			  {
			      fm.GrpAgentComName.value = "总公司";
			  }
			  if(arrResult1[0][0] == "1")
			  {
			      fm.GrpAgentComName.value = "省分公司";
			  }
			  if(arrResult1[0][0] == "2")
			  {
			      fm.GrpAgentComName.value = "地市分公司";
			  }
			  if(arrResult1[0][0] == "3")
			  {
			      fm.GrpAgentComName.value = "县支公司";
			  }
			  if(arrResult1[0][0] == "4")
			  {
			      fm.GrpAgentComName.value = "基层机构";
			  }
			}
			else{  
			     fm.GrpAgentComName.value = '';
		    }*/
		  var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
		  var arrResult1 = easyExecSql(strSql);
		  if (arrResult1 != null) {
		       fm.GrpAgentComName.value = arrResult1[0][0];
		  }
		  else{  
		       fm.GrpAgentComName.value = "";
		  }
      }
      // --------------------
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }
}

//得到投保人信息
function queryAppntInfo()
{
  var sql = "select a.AppntNo, a.AppntName, a.EnglishName, a.IDType, a.IDNo, "
          + "   a.AppntSex, a.AppntBirthday, "
          + "   a.OccupationCode, (select OccupationName from LDOccupation where OccupationCode = a.OccupationCode), "
          + "   (select GrpName from LDPerson where CustomerNo = a.AppntNo), "
          + "   a.Position, b.PostalAddress, b.ZipCode, a.NativePlace, b.Phone, b.Mobile, "
          + "   b.CompanyPhone, b.EMail, b.fax, a.MakeDate, a.MakeTime, a.Marriage, a.Salary, a.IDStartDate,a.IDEndDate "
          + "  ,b.PostalProvince,b.PostalCity,b.PostalCounty,b.PostalStreet,b.PostalCommunity,a.nativecity,(select Authorization from LDPerson where CustomerNo = a.AppntNo) "
          + "from LCAppnt a, LCAddress b "
          + "where a.AppntNo = b.CustomerNo "
          + "   and a.AddressNo = b.AddressNo "
          + "   and a.PrtNo = '" + fm.PrtNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] != null && rs[0][0] != "" && rs[0][0] != "null")
  {
    try
    {
      fm.AppntNo.value = rs[0][0];
      fm.AppntName.value = rs[0][1];
      fm.EnglishName.value = rs[0][2];
      fm.AppntIDType.value = rs[0][3];
      setCodeName("AppntIDTypeName", "idtype", fm.AppntIDType.value);
      fm.AppntIDNo.value = rs[0][4];
      fm.AppntSex.value = rs[0][5];
      setCodeName("SexName", "sex", fm.AppntSex.value);
      fm.AppntBirthday.value = rs[0][6];
      fm.OccupationCode.value = rs[0][7];
      fm.OccupationName.value = rs[0][8];
      fm.GrpName.value = rs[0][9];
      fm.Position.value = rs[0][10];
      fm.PostalAddress.value = rs[0][11];
      fm.ZipCode.value = rs[0][12];
      fm.AppntNativePlace.value = rs[0][13];
      fm.Phone.value = rs[0][14];
      fm.Mobile.value = rs[0][15];
      fm.CompanyPhone.value = rs[0][16];
      fm.EMail.value = rs[0][17];
      fm.Fax.value = rs[0][18];
      fm.AppntMakeDate.value = rs[0][19];
      fm.AppntMakeTime.value = rs[0][20];
      fm.AppntMarriage.value = rs[0][21];
      fm.AppntSalary.value = rs[0][22];
       fm.IDStartDate.value = rs[0][23];
        fm.IDEndDate.value = rs[0][24];
        var rs1 = easyExecSql("select codename from ldcode1 where codetype ='province1' and code='"+rs[0][25]+"' and code1= '0' ");
        var rs2 = easyExecSql("select codename from ldcode1 where codetype ='city1' and code='"+rs[0][26]+"' and code1= '"+rs[0][25]+"' ");
        var rs3 = easyExecSql("select codename from ldcode1 where codetype ='county1' and code='"+rs[0][27]+"' and code1= '"+rs[0][26]+"' ");
        if(rs1 != null ){
        	fm.Province.value= rs[0][25];
        	fm.City.value= rs[0][26];
        	fm.County.value= rs[0][27];
        	fm.PostalProvince.value= rs1;
        	fm.PostalCity.value= rs2;
        	fm.PostalCounty.value= rs3;
        }else{
        	var rs4 = easyExecSql("select code from ldcode1 where codetype='province1' and codename ='"+rs[0][25]+"'");
        	var rs5 = easyExecSql("select code from ldcode1 where codetype='city1' and codename ='"+rs[0][26]+"'");
        	var rs6 = easyExecSql("select code from ldcode1 where codetype='county1' and codename ='"+rs[0][27]+"'");
        	fm.Province.value= rs4;
        	fm.City.value= rs5;
        	fm.County.value= rs6;
        	fm.PostalProvince.value= rs[0][25];
        	fm.PostalCity.value= rs[0][26];
        	fm.PostalCounty.value= rs[0][27];
        }
      fm.PostalStreet.value= rs[0][28];
      fm.PostalCommunity.value= rs[0][29];  
      fm.AppntNativeCity.value= rs[0][30];
      //modify by zxs 
      fm.AppntAuth.value= rs[0][31];
      var s =  easyExecSql("select 1 from lwmission where missionprop1 = '"+fm.prtn1.value+"' and activityid='0000009001'");
  		if(s =='1'){
  				fm.all('AppntAuth').value = '1';
  		}
      
      setCodeName("AppntNativeCityName", "nativecity", fm.AppntNativeCity.value);
    }
    catch(ex)
    {
      alert(ex.message);
    }
    
    fm.AppntType.value = getAppntType();
    setCodeName("AppntTypeName", 'appnttype', fm.AppntType.value);
    controlNativeCity("");
    showAllCodeName();

    if(fm.AppntType.value == "1")
    {
      displayAppntElement("");
    }
    else
    {
      displayAppntElement("none");
      
    }
  }
}

//得到交费信息
function queryFeeInfo()
{
  var sql = "select a.PayIntv, a.PayMode, a.BankCode, a.BankAccNo, "
          + "   a.AccName, (select BankName from LDBank where BankCode = a.BankCode), "
          + "   a.PayerType "
          + "from LCCont a "
          + "where PrtNo = '" + fm.PrtNo.value + "' "
          + "   and ContType = '1' ";
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] != null && rs[0][0] != "" && rs[0][0] != "null")
  {
    try
    {
      fm.PayIntv.value = rs[0][0];
      fm.OldPayIntv.value = rs[0][0];
      setCodeName("PayIntvName", "payintv", fm.PayIntv.value);
      fm.PayMode.value = rs[0][1];
      setCodeName("PayModeName", "paymode", fm.PayMode.value);
      fm.BankCode.value = rs[0][2];
      fm.BankAccNo.value = rs[0][3];
      fm.AccName.value = rs[0][4];
      fm.BankName.value = rs[0][5];
      fm.PayerType.value = rs[0][6];
    }
    catch(ex)
    {
      alert(ex.message);
    }
    
    fm.AppntType.value = getAppntType();
  }
}

function setAppntType()
{
  var sql = "select CustomerType from LDPerson where CustomerNo = '" + fm.AppntNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.AppntType.value = rs[0][0];
  }
}

//为代码赋汉字
function setCodeName(fieldName, codeType, code)
{
  var sql = "select CodeName('" + codeType + "', '" + code + "') from dual";
  var rs = easyExecSql(sql)
  if(rs)
  {
    fm.all(fieldName).value = rs[0][0];
  }
}

//控制万能险控件的显示
function controlOmnipotence()
{
  //tSalChnlCode = "lcsalechnl";
  tSalChnlCode = "lcomnsalechnl";
    
  //生效日默认为当前日期，在签单时修改为签单次日
  fm.all("CValiDateText").style.display = "none";
  fm.all("CValiDateClass").style.display = "none";
  //fm.all("FillControl").style.display = "";
  fm.all("Tempfeetitle").style.display = "";
  fm.all("input1").style.display = "";  //缴费凭证号录入框
  //fm.CValiDate.value = tCurrentDate;
  fm.CValiDate.value = fm.PolApplyDate.value
  
  //投保人只能是个人
  fm.all("AppntTypeTrID").style.display = "none";
  fm.all("PayerTypeTRID").style.display = "none";
  
  fm.all("RemarkDiv").style.display = "none";
  fm.all("EnglishNameText").style.display = "none";
  fm.all("EnglishNameClass").style.display = "none";
  fm.all("ZhiweiApptitle").style.display = "";
  fm.all("ZhiweiApp").style.display = "";
  //fm.all("NativePlaceTitleID").style.display = "none";
  //fm.all("NativePlaceInputID").style.display = "none";
  fm.all("FaxTitleID").style.display = "none";
  fm.all("FaxInputID").style.display = "none";
  fm.all("EMailTitleID").style.display = "";
  fm.all("EMailInputID").style.display = "";
}

 
//**************************事件响应区域***************************


/*********************************************************************
 *  初始化部分录入控件信息
 *  参数  ： 
 *  返回值：  无
 *********************************************************************
 */
function initInputBox()
{
  if(tCardFlag == null || tCardFlag == "" || tCardFlag == "null")
  {
    var sql = "select CardFlag from LCCont where PrtNo = '" + prtNo + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      tCardFlag = rs[0][0];
    }
  }
  
  //链接传进来的日期:年月日 包括时分秒，只去年月日
  var temp = PolApplyDate;
  if(temp.indexOf(" ") > 0)
  {
    temp = temp.substring(0, temp.indexOf(" "));
  }
  fm.PrtNo.value = prtNo;
  fm.PolApplyDate.value = temp;
  fm.InputDate.value = tCurrentDate;
  fm.ReceiveDate.value =tCurrentDate;
  fm.ManageCom.value = ManageCom;
  fm.CardFlag.value = tCardFlag;
  fm.IntlFlag.value = tIntlFlag;
  
  var sql = "select Name from LDCom where ComCode = '" + fm.ManageCom.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.ManageComName.value = rs[0][0];
  }
  
  afterCodeSelect("AppntType", "");
  
  //控制万能险控件的显示
  //万能险保单，生效日默认为当前日期，在签单时修改为签单次日
  if("8" == tCardFlag || "6" == tCardFlag)
  {
    controlOmnipotence();
  }
  if("6" == tCardFlag)
  {
    fm.all("CValiDateText").style.display = "none";
    fm.all("CValiDateClass").style.display = "none";
    fm.all("ReceiveDate").verify = "";
  }
}

//控制投保人要素控件的显示, cDisplay="none"显示团体
function displayAppntElement(cDisplay)
{
  var colSpan = "3";
  var tAppntNameTitleValueID = "姓名";
  if(cDisplay == "none")
  {
    colSpan = "1";  //设为1才能保证两列的时候不错位
    tAppntNameTitleValueID = "团体名称";
    fm.AppntIDType.value = "";
    fm.AppntIDType.verify = "";
    fm.AppntSex.verify = "";
    fm.AppntBirthday.verify = "";
  }
  fm.all("AppntNameTitleValueID").innerHTML = tAppntNameTitleValueID;
  //fm.all("PostalAddressID").colSpan = colSpan;
  
  fm.all("AppntIDTypeTitleID").style.display = cDisplay;
  fm.all("AppntIDTypeInputID").style.display = cDisplay;
  fm.all("AppntIDNoTitleID").style.display = cDisplay;
  fm.all("AppntIDNoInputID").style.display = cDisplay;
  fm.all("Row2ID").style.display = cDisplay;
  //fm.all("NativePlaceTitleID").style.display = cDisplay;
  //fm.all("NativePlaceInputID").style.display = cDisplay;
  fm.all("MobileTitleID").style.display = cDisplay;
  fm.all("MobileInputID").style.display = cDisplay;
  //fm.all("CompanyPhoneTitleID").style.display = cDisplay;
  //fm.all("CompanyPhoneInputID").style.display = cDisplay;
  
  
  //控制万能险控件的显示
  //万能险保单，生效日默认为当前日期，在签单时修改为签单次日
  if("8" == tCardFlag || "6" == tCardFlag)
  {
    controlOmnipotence();
  }
  if("6" == tCardFlag)
  {
    fm.all("CValiDateText").style.display = "";
    fm.all("CValiDateClass").style.display = "";
  }
}

//清空投保人信息
function clearAppnt()
{
  fm.AppntName.value = "";
  fm.EnglishName.value = "";
  fm.AppntIDType.value = "0";
  fm.AppntIDTypeName.value = "身份证";
  fm.AppntIDNo.value = "";
  
  fm.AppntSex.value = "";
  fm.SexName.value = "";
  fm.AppntBirthday.value = "";
  fm.OccupationCode.value = "";
  fm.OccupationName.value = "";
  fm.GrpName.value = "";
  
  fm.Position.value = "";
  fm.PostalAddress.value = "";
  fm.ZipCode.value = "";
  fm.AppntNativePlace.value = "";
  fm.NativePlaceName.value = "";
  fm.AppntNativeCity.value = "";
  fm.AppntNativeCityName.value = "";
  
  fm.Phone.value = "";
  fm.Mobile.value = "";
  fm.CompanyPhone.value = "";
  fm.EMail.value = "";
  fm.Fax.value = "";
}

function afterCodeSelect( cCodeName, Field )
{
	//关闭社保渠道勾选交叉销售功能（社保渠道不得选择交叉销售业务）
	if(cCodeName=="unitesalechnl"){
		fm.all("MixComFlag").style.display="";
		fm.MixComFlag.checked = false;
		if(fm.SaleChnl.value=="18"||fm.SaleChnl.value=="20"){
			fm.all("MixComFlag").style.display="none";
			fm.MixComFlag.checked = false;
		}
		isMixCom();
	}
	
  if(cCodeName=="AppntType")
  {
    clearAppnt();
    
    if(fm.AppntType.value == "1")
    {
      displayAppntElement("");
    }
    else
    {
      displayAppntElement("none");
    }
    
    if(fm.AppntType.value == getAppntType())
    {
      queryAppntInfo();
    }
    
  }
  
	if(cCodeName=="OccupationCode")
  {
    //var t = Field.value;
    //var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+t+"'";
    //var arr = easyExecSql(strSql);
    //if( arr )
    //{
    //	fm.OccupationType.value = arr[0][0]; 
    //}
   } 
   
   //缴费方式
  if(cCodeName=="PayModeInd")
  {
    //if(fm.PayMode.value=="1" || fm.PayMode.value=="3") 
    //{
    //  fm.all('BankCode').value="";
    //  fm.all('BankCode').style.display="none";
    //  fm.all('BankName').value="";
    //  fm.all('BankName').style.display="none";
    //  fm.all('BankAccNo').value= "";
    //  fm.all('BankAccNo').style.display="none";
    //  fm.all('AccName').value= "";
    //  fm.all('AccName').style.display="none";
    //  fm.all("BankCom").style.display = "none";
    //}      
    //else if(fm.PayMode.value=="4") 
    //{
    //  fm.all('BankCode').style.display="";
    //  fm.all('BankName').style.display="";
    //  fm.all('AccName').style.display="";
    //  fm.all('BankAccNo').style.display="";
    //  fm.all("BankCom").style.display = "";
    //}
    //所有缴费方式，均显示缴费信息
    fm.all('BankCode').value="";
    fm.all('BankCode').style.display="";
    fm.all('BankName').value="";
    fm.all('BankName').style.display="";
    fm.all('BankAccNo').value= "";
    fm.all('BankAccNo').style.display="";
    fm.all('AccName').value= "";
    fm.all('AccName').style.display="";
    fm.all("BankCom").style.display = "";
  }
  
  //销售渠道
  if(cCodeName=="unitesalechnl")
  {
    if(fm.SaleChnl.value == "03")
    {
      agentcomCode = "agentcominput";
      
      //fm.all("AgencyID").style.display = "";
      //fm.all("DirectID").style.display = "none";
      fm.all("zhongjiecode").style.display = "";
      fm.all("zhongjiename").style.display = "";
      fm.AgentCom.value = "";
      //fm.AgentCode.value = "";
      //fm.AgentName.value = "";
      //fm.AgentCodeG.value = "";
      //fm.AgentNameG.value = "";
      fm.AgentSaleCode.value = "";
      fm.AgentSaleName.value = "";
      fm.all("AgentSaleCodeID").style.display = ""; 
      fm.all("AgencyIDCodeTitle").innerHTML = "中介公司代码";
      fm.all("AgentCodeTitile").innerHTML = "中介专员代码";		
      fm.all("AgentNameTitile").innerHTML = "中介专员名称";
      fm.all("AgentSaleCode1Title").innerHTML = "代理销售人员编码";
      fm.all("AgentSaleCode2Title").innerHTML = "代理销售人员名称";	
      var tBranchType="2";
      var tBranchType2="02";	
      agentComName = "branchtype";
	  agentComValue = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
    }
    else if(fm.SaleChnl.value == "04")
    {
      agentcomCode = "agentcombank";
      agentComName = "ManageCom";
      agentComValue = fm.ManageCom.value;
      
      //fm.all("AgencyID").style.display = "";
      fm.all("AgentSaleCodeID").style.display = "";   	
      fm.AgentCom.value = "";
      //fm.AgentCode.value = "";
      //fm.AgentName.value = "";
      //fm.AgentCodeG.value = "";
      //fm.AgentNameG.value = "";
      fm.AgentSaleCode.value = "";
      fm.AgentSaleName.value = "";
      
      //fm.all("DirectID").style.display = "none"; 
      fm.all("zhongjiecode").style.display = "";
      fm.all("zhongjiename").style.display = "";  
      fm.all("AgencyIDCodeTitle").innerHTML = "银行网点";
      fm.all("AgentCodeTitile").innerHTML = "银代专员";
      fm.all("AgentNameTitile").innerHTML = "银代专员名称";
      fm.all("AgentSaleCode1Title").innerHTML = "网点销售人员编码";
      fm.all("AgentSaleCode2Title").innerHTML = "网点销售人员名称";		
    }
    else if(fm.SaleChnl.value == "01" || fm.SaleChnl.value == "02" || fm.SaleChnl.value == "07" || fm.SaleChnl.value == "13" || fm.SaleChnl.value == "14")
    {
      //fm.all("DirectID").style.display = ""; 
      
      fm.AgentCom.value = "";
      //fm.AgentCodeG.value = "";
      //fm.AgentNameG.value = "";
      fm.all("AgentSaleCodeID").style.display = "none"; 
      
      //fm.all("AgencyID").style.display = "none";
      fm.all("zhongjiecode").style.display = "none";
      fm.all("zhongjiename").style.display = "none";
      fm.all("AgencyIDCodeTitle").innerHTML = "中介公司代码";
      fm.all("AgentCodeTitile").innerHTML = "业务员代码";
      fm.all("AgentNameTitile").innerHTML = "业务员名称";		
    }else if(fm.SaleChnl.value == "15")
    {
      agentcomCode = "agentcominput";
      //fm.all("DirectID").style.display = "none"; 
      
      fm.AgentCom.value = "";
      //fm.AgentCode.value = "";
      //fm.AgentName.value = "";
      //fm.AgentCodeG.value = "";
      //fm.AgentNameG.value = "";
      fm.all("AgentSaleCodeID").style.display = ""; 
      
      //fm.all("AgencyID").style.display = "";
      fm.all("zhongjiecode").style.display = "";
      fm.all("zhongjiename").style.display = "";
      fm.all("AgencyIDCodeTitle").innerHTML = "中介公司代码";
      fm.all("AgentCodeTitile").innerHTML = "中介专员代码";
      fm.all("AgentNameTitile").innerHTML = "中介专员名称";	
      fm.all("AgentSaleCode1Title").innerHTML = "代理销售人员编码";
      fm.all("AgentSaleCode2Title").innerHTML = "代理销售人员名称";
      var tBranchType="#5";
	  var tBranchType2="01";
	  agentComName = "branchtype";
	  agentComValue = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
    }else if(fm.SaleChnl.value == "20")
    {
      agentcomCode = "agentcominput";
      fm.AgentCom.value = "";
      fm.all("AgentSaleCodeID").style.display = ""; 
      
      fm.all("zhongjiecode").style.display = "";
      fm.all("zhongjiename").style.display = "";
      fm.all("AgencyIDCodeTitle").innerHTML = "中介公司代码";
      fm.all("AgentCodeTitile").innerHTML = "中介专员代码";
      fm.all("AgentNameTitile").innerHTML = "中介专员名称";	
      fm.all("AgentSaleCode1Title").innerHTML = "代理销售人员编码";
      fm.all("AgentSaleCode2Title").innerHTML = "代理销售人员名称";
      var tBranchType="6";
	  var tBranchType2="02";
	  agentComName = "branchtype";
	  agentComValue = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value ;		
    }else if(fm.SaleChnl.value == "11" || fm.SaleChnl.value == "12")
    {
      agentcomCode = "agentcominput";
      
      //fm.all("AgencyID").style.display = "";
      fm.all("AgentSaleCodeID").style.display = "none"; 
      
      fm.AgentCom.value = "";
      //fm.AgentCode.value = "";
      //fm.AgentName.value = "";
      //fm.AgentCodeG.value = "";
      //fm.AgentNameG.value = "";
      
      //fm.all("DirectID").style.display = "none";   
      fm.all("zhongjiecode").style.display = "";
      fm.all("zhongjiename").style.display = "";	
      fm.all("AgencyIDCodeTitle").innerHTML = "中介公司代码";
      fm.all("AgentCodeTitile").innerHTML = "中介专员代码";		
      fm.all("AgentNameTitile").innerHTML = "中介专员名称";	
      
      var tBranchType="2";
	  var tBranchType2="04";
      agentComName = "branchtype";
      agentComValue = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;	
    }else if(fm.SaleChnl.value == "10"){
      agentcomCode = "agentcominput";
      //fm.all("DirectID").style.display = "none"; 
      //fm.all("AgencyID").style.display = "";
      fm.all("zhongjiecode").style.display = "";
      fm.all("zhongjiename").style.display = "";
      fm.all("AgentSaleCodeID").style.display = ""; 
      fm.all("AgencyIDCodeTitle").innerHTML = "中介公司代码";
      fm.all("AgentCodeTitile").innerHTML = "中介专员代码";
      fm.all("AgentNameTitile").innerHTML = "中介专员名称";	
      fm.all("AgentSaleCode1Title").innerHTML = "代理销售人员编码";
      fm.all("AgentSaleCode2Title").innerHTML = "代理销售人员名称";
     
	  var tBranchType="#1";
	  var tBranchType2="02#,#04";
	  agentComName = "branchtype";
	  agentComValue = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";

    }else
    {
	  
      agentComName = "ManageCom";
      agentComValue = fm.ManageCom.value;
    }
    
      fm.AgentCom.value = "";
      fm.AgentComName.value = "";
      //fm.GroupAgentCode.value = "";
      //fm.AgentCode.value = "";
      //fm.AgentName.value = "";
      //fm.AgentCodeG.value = "";
      //fm.AgentNameG.value = "";
      fm.AgentSaleCode.value = "";
      fm.AgentSaleName.value = "";
      
    
  }
  
  //银行网点选中后自动带出银代专员信息   2008-3-13
  if (cCodeName == "agentcombank")
  {
    var agentCodeSql = "select a.AgentCode, b.Name, b.AgentGroup,b.groupagentcode from LAComToAgent a left join LAAgent b "
                    + "on a.AgentCode = b.AgentCode where AgentCom='" + fm.AgentCom.value + "' and RelaType='1' and b.groupagentcode is not null and  b.groupagentcode <>'' ";
    var arr = easyExecSql(agentCodeSql);
	if(arr){
      fm.AgentCode.value = arr[0][0];
      fm.GroupAgentCode.value = arr[0][3];
      //fm.AgentCodeG.value = arr[0][3];
      //fm.AgentNameG.value = arr[0][1];
      fm.AgentName.value = arr[0][1];
      fm.AgentGroup.value = arr[0][2];
	}
	//else
	//{
    //  fm.AgentCode.value = "";
    //  fm.GroupAgentCode.value = "";
    //  fm.AgentCodeG.value = "";
    //  fm.AgentNameG.value = "";
    //  fm.AgentName.value = "";
    //  fm.AgentGroup.value = "";
	//}
  }
  //双击交叉销售渠道时,清空对方机构代码和对方机构名称
  if(cCodeName=="crs_salechnl")
  {
	  fm.GrpAgentCom.value = "";
  }
  //if(cCodeName=="grpagentcom")
  //{
	  //GetGrpAgentName();
  //}
  
  if(cCodeName=="NativePlace"){
	   if(Field.value == "OS"){
	      fm.all("NativePlace").style.display = "";
	      fm.all("NativeCity").style.display = "";
	      fm.all("NativeCityTitle").innerHTML="国家";
	   }else if(Field.value == "HK"){
	      fm.all("NativePlace").style.display = "";
	      fm.all("NativeCity").style.display = "";
	      fm.all("NativeCityTitle").innerHTML="地区";
	   }else{
	      fm.all("NativePlace").style.display = "none";
	      fm.all("NativeCity").style.display = "none";
	   }
	    fm.all("AppntNativeCity").value = "";
	    fm.all("AppntNativeCityName").value = "";
	}
} 

//提交，保存按钮对应操作
function submitForm()
{
  mAction = ACTION_INSERT;
  fm.all( 'fmAction' ).value = mAction;
  
  var sql = "select 1 from LCCont where PrtNo = '" + prtNo + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    alert("保单已保存，请使用修改功能");
    return false;
  }
  
    var tAppntIDNo=fm.AppntIDNo.value;
	var tAppntIDType=fm.AppntIDType.value;
	var tAppntSex=fm.AppntSex.value;
	var tAppntBirthday=fm.AppntBirthday.value;
	if(tAppntIDNo!=null && tAppntIDNo!=""){
		var strChkIdNo=checkIdNo(tAppntIDType,tAppntIDNo,tAppntBirthday,tAppntSex);
		if(strChkIdNo !=""){
			alert(strChkIdNo);
			return false;
		}
	}
  
  // by gzh 20101118
	//选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
	
	if(!checkNative()){
	   return false;
	}
	
	//是否在计算保费后修改计算保费的要素，不让修改
	if(!checkPayintv()){
		return false;
	}
	if(!checkBeforeSubmit())
	{
	  return false;
	}
	
	
	if(!checkOccupationType())
  	{
    	return false;
  	}
  	
  	if(!checkBankCode())
  	{
  		return false;
  	}
  	
  	if(!checkChnlComCode()){
  		return false;
  	}
  	
  	if(!ExtendCheck()){
  		return false;
  	}
  	
  	if(!checkAllapp()){
  	    return false;
  	}
	
    //2017-02-08 赵庆涛
//  社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
    if(!checkCrs_SaleChnl()){
    	return false;
    }

       
	if (fm.all('ProposalContNo').value != "") 
	{
	  alert("保存结果只能进行修改操作！");
	} 
	else 
	{
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
	  //保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
	  tAction = fm.action;
	  ChangeDecodeStr();		  
	  fm.submit(); //提交
	  fm.action = tAction;
	}
}

//查询机构业务员
function queryAgent()
{
    if(fm.all('ManageCom').value==""){
        alert("请先录入管理机构信息！");
        return;
    }
    
    if(fm.all('GroupAgentCode').value == "")
    {
        var saleChnl = fm.SaleChnl.value;
        var branchType = "1";
        if(saleChnl == "02" || saleChnl == "11" || saleChnl == "12")
        {
            branchType = "2";
        }
        //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&AgentCom=" + fm.AgentCom.value + "&SaleChnl=" + saleChnl + "&branchtype=" + branchType,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    }
    if(fm.all('GroupAgentCode').value != "")	 {
        var cAgentCode = fm.GroupAgentCode.value;  //保单号码
        var strSql = "select GroupAgentCode,Name,AgentGroup,agentcode from LAAgent where GroupAgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null) {
            fm.AgentGroup.value = arrResult[0][2];
            fm.AgentCode.value= arrResult[0][3];
            alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value="";
            fm.AgentCode.value="";
            alert("代码为:["+fm.all('GroupAgentCode').value+"]的业务员不存在，请确认!");
        }
    }
}

//显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value = "";
        fm.all('Crs_SaleChnlName').value = "";
        fm.all('Crs_BussType').value = "";
        fm.all('Crs_BussTypeName').value = "";
        fm.all('GrpAgentCom').value = "";
        fm.all('GrpAgentComName').value = "";
        fm.all('GrpAgentCode').value = "";
        fm.all('GrpAgentName').value = "";
        fm.all('GrpAgentIDNo').value = "";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}

//代理人查询返回
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{
  if(arrResult!=null)
  {
    fm.AgentCode.value = arrResult[0][0];
    fm.AgentName.value = arrResult[0][5];
    //fm.AgentCodeG.value = arrResult[0][95];
    //fm.AgentNameG.value = arrResult[0][5];
    fm.AgentGroup.value = arrResult[0][1];
    fm.GroupAgentCode.value = arrResult[0][95];
  }
}

/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit(FlagStr, content)
{
	UnChangeDecodeStr();
	showInfo.close();
	window.focus();
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		//showDiv(operateButton, "true"); 
		//showDiv(inputButton, "false");
		
		if(mAction == ACTION_INSERT)
		{
		  initForm();
		}
		if("Confirm" == fm.all('fmAction').value)
		{
		  top.opener.focus();
		  top.opener.initForm();
		  top.close();
		}
	}
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
  mAction = ACTION_UPDATE;
  fm.all( 'fmAction' ).value = mAction;
  
  var sql = "select 1 from LCCont where PrtNo = '" + prtNo + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
  }
  else
  {
    alert("请先保存保单，再进行修改");
    return false;
  }
  
    var tAppntIDNo=fm.AppntIDNo.value;
    var tAppntIDType=fm.AppntIDType.value;
	var tAppntSex=fm.AppntSex.value;
	var tAppntBirthday=fm.AppntBirthday.value;
	if(tAppntIDNo!=null && tAppntIDNo!=""){
		var strChkIdNo=checkIdNo(tAppntIDType,tAppntIDNo,tAppntBirthday,tAppntSex);
		if(strChkIdNo !=""){
			alert(strChkIdNo);
			return false;
		}
	}
  
    // by gzh 20101118
	//选择交叉销售后，对交叉销售各不为空项校验。
  if(!MixComCheck())
  {
	  return false;
  }
  
  if(!checkNative()){
	   return false;
	}
	
  //是否在计算保费后修改计算保费的要素，不让修改
	if(!checkPayintv()){
		return false;
	}
  if(!checkBeforeSubmit())
  {
    return false;
  }
  
  if(!checkOccupationType())
  {
    return false;
  }
  
  if(!checkBankCode())
  {
  	return false;
  }
  
  if(!checkChnlComCode()){
  	return false;
  }
  
  if(!ExtendCheck()){
  	return false;
  }
  
  if(!checkAllapp()){
  	return false;
  }
  
  //2017-02-08 赵庆涛
//社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
  if(!checkCrs_SaleChnl()){
  	return false;
  }

  
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	ChangeDecodeStr();
	
	fm.action = "ContSave.jsp";
	fm.submit(); //提交
  
  return true;
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

/*********************************************************************
 *  当点击“进入个人信息”按钮时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoInsured()
{
	var tAppntNo = fm.AppntNo.value;
	var tAppntName = fm.AppntName.value;
	if( fm.ContNo.value == "" )
	{
		alert("您必须先录入合同信息才能进入被保险人信息部分。");
		return false
	}
	var cXSFlag = fm.XSFlag.value;
	if(fm.all('ManageCom').value.length>=4&&LoadFlag == "5"){
      if(fm.all('ManageCom').value.substring(0,4)=='8691'){
      	  if(cXSFlag!='Y'){
      	    alert("未进行销售过程全程记录，不能复核被保人！");
            return false;
      	  }
      }
    }
    
	//把集体信息放入内存
	mSwitch = parent.VD.gVSwitch;  //桢容错
	putCont();
	var cVideoFlag = fm.VideoFlag.value;
	
	//var strSql = "select 1 from lwmission where missionid='"+tMissionID+"' and submissionid='1' and activityid='0000008001'  and processid = '0000000008'";
	//var arr = easyExecSql(strSql);
	//if(!arr)
	//{
	//	alert("合同信息已保存！");
	//	return;
	//}
	

	
	try 
	{
	  parent.fraInterface.window.location = "./ContInsuredInput.jsp?LoadFlag=" + LoadFlag + "&type=" + type+ "&MissionID=" + tMissionID+ "&SubMissionID=" + tSubMissionID+ "&AppntNo=" + tAppntNo+ "&AppntName=" + tAppntName+"&checktype=1"+"&scantype="+scantype+"&ScanFlag="+ScanFlag+"&SaleChnl="+fm.SaleChnl.value+"&SaleChnlDetail="+fm.SaleChnlDetail.value+"&ContNo=" + fm.ContNo.value + "&PrtNo=" + fm.PrtNo.value+"&VideoFlag="+cVideoFlag;
	}
	catch (e) 
	{alert(e.message);
		parent.fraInterface.window.location = "./ContInsuredInput.jsp?LoadFlag=1&type=" + type+ "&MissionID=" +tMissionID+ "&SubMissionID=" + tSubMissionID+ "&AppntNo=" + tAppntNo+ "&AppntName=" + tAppntName +"&ContNo="+fm.ContNo.value + "&PrtNo=" + fm.PrtNo.value;
	}
} 

/*********************************************************************
 *  把合同信息放入内存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function putCont()
{
	delContVar();
	addIntoCont();
}

/*********************************************************************
 *  把合同信息放入加到变量中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addIntoCont()
{
	//try { mSwitch.addVar( "intoPolFlag", "", "GROUPPOL" ); } catch(ex) { };
	// body信息
	try { mSwitch.addVar( "BODY", "", window.document.body.innerHTML ); } catch(ex) { };
	// 集体信息
	//由"./AutoCreatLDGrpInit.jsp"自动生成
  try{mSwitch.addVar('ContNo','',fm.ContNo.value);}catch(ex){};
  try{mSwitch.addVar('ProposalContNo','',fm.ProposalContNo.value);}catch(ex){};
  try{mSwitch.addVar('PrtNo','',fm.PrtNo.value);}catch(ex){};
  try{mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);}catch(ex){};
  try{mSwitch.addVar('ContType','',fm.ContType.value);}catch(ex){};
  try{mSwitch.addVar('FamilyType','',fm.FamilyType.value);}catch(ex){};
  try{mSwitch.addVar('PolType','',fm.PolType.value);}catch(ex){};
  try{mSwitch.addVar('CardFlag','',fm.CardFlag.value);}catch(ex){};
  try{mSwitch.addVar('ManageCom','',fm.ManageCom.value);}catch(ex){};
  try{mSwitch.addVar('AgentCom','',fm.AgentCom.value);}catch(ex){};
  try{mSwitch.addVar('AgentCode','',fm.AgentCode.value);}catch(ex){};
  try{mSwitch.addVar('AgentGroup','',fm.AgentGroup.value);}catch(ex){};
  try{mSwitch.addVar('AgentCode1','',fm.AgentCode1.value);}catch(ex){};
  try{mSwitch.addVar('AgentType','',fm.AgentType.value);}catch(ex){};
  try{mSwitch.addVar('SaleChnl','',fm.SaleChnl.value);}catch(ex){};
  try{mSwitch.addVar('Handler','',fm.Handler.value);}catch(ex){};
  try{mSwitch.addVar('Password','',fm.Password.value);}catch(ex){};
  try{mSwitch.addVar('AppntNo','',fm.AppntNo.value);}catch(ex){};
  try{mSwitch.addVar('AppntName','',fm.AppntName.value);}catch(ex){};
  try{mSwitch.addVar('EnglishName','',fm.EnglishName.value);}catch(ex){};
  try{mSwitch.addVar('AppntSex','',fm.AppntSex.value);}catch(ex){};
  try{mSwitch.addVar('AppntBirthday','',fm.AppntBirthday.value);}catch(ex){};
  try{mSwitch.addVar('AppntIDType','',fm.AppntIDType.value);}catch(ex){};
  //modify by zxs
//  try {mSwitch.addVar('AppntAuth', '', fm.AppntAuth.value);} catch (ex) {};
  try{mSwitch.addVar('AppntIDNo','',fm.AppntIDNo.value);}catch(ex){};
  try{mSwitch.addVar('InsuredNo','',fm.InsuredNo.value);}catch(ex){};
  try{mSwitch.addVar('InsuredName','',fm.InsuredName.value);}catch(ex){};
  try{mSwitch.addVar('InsuredSex','',fm.InsuredSex.value);}catch(ex){};
  try{mSwitch.addVar('InsuredBirthday','',fm.InsuredBirthday.value);}catch(ex){};
  try{mSwitch.addVar('InsuredIDType','',fm.InsuredIDType.value);}catch(ex){};
  try{mSwitch.addVar('InsuredIDNo','',fm.InsuredIDNo.value);}catch(ex){};
  try{mSwitch.addVar('PayIntv','',fm.PayIntv.value);}catch(ex){};
  try{mSwitch.addVar('PayMode','',fm.PayMode.value);}catch(ex){};
  try{mSwitch.addVar('PayLocation','',fm.PayLocation.value);}catch(ex){};
  try{mSwitch.addVar('DisputedFlag','',fm.DisputedFlag.value);}catch(ex){};
  try{mSwitch.addVar('OutPayFlag','',fm.OutPayFlag.value);}catch(ex){};
  try{mSwitch.addVar('GetPolMode','',fm.GetPolMode.value);}catch(ex){};
  try{mSwitch.addVar('SignCom','',fm.SignCom.value);}catch(ex){};
  try{mSwitch.addVar('SignDate','',fm.SignDate.value);}catch(ex){};
  try{mSwitch.addVar('SignTime','',fm.SignTime.value);}catch(ex){};
  try{mSwitch.addVar('ConsignNo','',fm.ConsignNo.value);}catch(ex){};
  try{mSwitch.addVar('BankCode','',fm.BankCode.value);}catch(ex){};
  try{mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);}catch(ex){};
  try{mSwitch.addVar('AccName','',fm.AccName.value);}catch(ex){};
  try{mSwitch.addVar('PrintCount','',fm.PrintCount.value);}catch(ex){};
  try{mSwitch.addVar('LostTimes','',fm.LostTimes.value);}catch(ex){};
  try{mSwitch.addVar('Lang','',fm.Lang.value);}catch(ex){};
  try{mSwitch.addVar('Currency','',fm.Currency.value);}catch(ex){};
  try{mSwitch.addVar('Remark','',fm.Remark.value);}catch(ex){};
  try{mSwitch.addVar('Peoples','',fm.Peoples.value);}catch(ex){};
  try{mSwitch.addVar('Mult','',fm.Mult.value);}catch(ex){};
  try{mSwitch.addVar('Prem','',fm.Prem.value);}catch(ex){};
  try{mSwitch.addVar('Amnt','',fm.Amnt.value);}catch(ex){};
  try{mSwitch.addVar('SumPrem','',fm.SumPrem.value);}catch(ex){};
  try{mSwitch.addVar('Dif','',fm.Dif.value);}catch(ex){};
  try{mSwitch.addVar('PaytoDate','',fm.PaytoDate.value);}catch(ex){};
  try{mSwitch.addVar('FirstPayDate','',fm.FirstPayDate.value);}catch(ex){};
  try{mSwitch.addVar('CValiDate','',fm.CValiDate.value);}catch(ex){};
  try{mSwitch.addVar('InputOperator','',fm.InputOperator.value);}catch(ex){};
  try{mSwitch.addVar('InputDate','',fm.InputDate.value);}catch(ex){};
  try{mSwitch.addVar('InputTime','',fm.InputTime.value);}catch(ex){};
  try{mSwitch.addVar('ApproveFlag','',fm.ApproveFlag.value);}catch(ex){};
  try{mSwitch.addVar('ApproveCode','',fm.ApproveCode.value);}catch(ex){};
  try{mSwitch.addVar('ApproveDate','',fm.ApproveDate.value);}catch(ex){};
  try{mSwitch.addVar('ApproveTime','',fm.ApproveTime.value);}catch(ex){};
  try{mSwitch.addVar('UWFlag','',fm.UWFlag.value);}catch(ex){};
  try{mSwitch.addVar('UWOperator','',fm.UWOperator.value);}catch(ex){};
  try{mSwitch.addVar('UWDate','',fm.UWDate.value);}catch(ex){};
  try{mSwitch.addVar('UWTime','',fm.UWTime.value);}catch(ex){};
  try{mSwitch.addVar('AppFlag','',fm.AppFlag.value);}catch(ex){};
  try{mSwitch.addVar('PolApplyDate','',fm.PolApplyDate.value);}catch(ex){};
  try{mSwitch.addVar('GetPolDate','',fm.GetPolDate.value);}catch(ex){};
  try{mSwitch.addVar('GetPolTime','',fm.GetPolTime.value);}catch(ex){};
  try{mSwitch.addVar('CustomGetPolDate','',fm.CustomGetPolDate.value);}catch(ex){};
  try{mSwitch.addVar('State','',fm.State.value);}catch(ex){};
  try{mSwitch.addVar('Operator','',fm.Operator.value);}catch(ex){};
  try{mSwitch.addVar('MakeDate','',fm.MakeDate.value);}catch(ex){};
  try{mSwitch.addVar('MakeTime','',fm.MakeTime.value);}catch(ex){};
  try{mSwitch.addVar('ModifyDate','',fm.ModifyDate.value);}catch(ex){};
  try{mSwitch.addVar('ModifyTime','',fm.ModifyTime.value);}catch(ex){};
   
   //新的数据处理
  try { mSwitch.addVar('AppntNo','',fm.AppntNo.value); } catch(ex) { };
  try { mSwitch.addVar('AppntName','',fm.AppntName.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntSex','',fm.AppntSex.value); } catch(ex) { };				
  try { mSwitch.addVar('AppntBirthday','',fm.AppntBirthday.value); } catch(ex) { };		
  try { mSwitch.addVar('IDStartDate','',fm.IDStartDate.value); } catch(ex) { };	
  try { mSwitch.addVar('IDEndDate','',fm.IDEndDate.value); } catch(ex) { };	
  try { mSwitch.addVar('AppntIDType','',fm.AppntIDType.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntIDNo','',fm.AppntIDNo.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntPassword','',fm.AppntPassword.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntNativePlace','',fm.AppntNativePlace.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntNationality','',fm.AppntNationality.value); } catch(ex) { };
  try { mSwitch.addVar('AddressNo','',fm.AddressNo.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntRgtAddress','',fm.AppntRgtAddress.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntMarriage','',fm.AppntMarriage.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntMarriageDate','',fm.AppntMarriageDate.value); } catch(ex) { };	
  try { mSwitch.addVar('AppntHealth','',fm.AppntHealth.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntStature','',fm.AppntStature.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntAvoirdupois','',fm.AppntAvoirdupois.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntDegree','',fm.AppntDegree.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntCreditGrade','',fm.AppntCreditGrade.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntOthIDType','',fm.AppntOthIDType.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntOthIDNo','',fm.AppntOthIDNo.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntICNo','',fm.AppntICNo.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntGrpNo','',fm.AppntGrpNo.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntJoinCompanyDate','',fm.AppntJoinCompanyDate.value); } catch(ex) { };	
  try { mSwitch.addVar('AppntStartWorkDate','',fm.AppntStartWorkDate.value); } catch(ex) { };	
  try { mSwitch.addVar('Position','',fm.Position.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntSalary','',fm.AppntSalary.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntOccupationType','',fm.AppntOccupationType.value); } catch(ex) { };	
  try { mSwitch.addVar('AppntOccupationCode','',fm.OccupationCode.value); } catch(ex) { };  	
  try { mSwitch.addVar('AppntWorkType','',fm.AppntWorkType.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntPluralityType','',fm.AppntPluralityType.value); } catch(ex) { };	
  try { mSwitch.addVar('AppntDeathDate','',fm.AppntDeathDate.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntSmokeFlag','',fm.AppntSmokeFlag.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntBlacklistFlag','',fm.AppntBlacklistFlag.value); } catch(ex) { };	
  try { mSwitch.addVar('AppntProterty','',fm.AppntProterty.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntRemark','',fm.AppntRemark.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntState','',fm.AppntState.value); } catch(ex) { };			
  try { mSwitch.addVar('AppntOperator','',fm.AppntOperator.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntMakeDate','',fm.AppntMakeDate.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntMakeTime','',fm.AppntMakeTime.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntModifyDate','',fm.AppntModifyDate.value); } catch(ex) { };		
  try { mSwitch.addVar('AppntModifyTime','',fm.AppntModifyTime.value); } catch(ex) { };
  try { mSwitch.addVar('AppntHomeAddress','',fm.AppntHomeAddress.value); } catch(ex) { };
  try { mSwitch.addVar('AppntHomeZipCode','',fm.AppntHomeZipCode.value); } catch(ex) { };
  try { mSwitch.addVar('HomePhone','',fm.HomePhone.value); } catch(ex) { };
  try { mSwitch.addVar('CompanyPhone','',fm.CompanyPhone.value); } catch(ex) { };
  try { mSwitch.addVar('AppntHomeFax','',fm.AppntHomeFax.value); } catch(ex) { };		
  try { mSwitch.addVar('GrpName','',fm.GrpName.value); } catch(ex) { };
  try { mSwitch.addVar('AppntGrpPhone','',fm.AppntGrpPhone.value); } catch(ex) { };
  try { mSwitch.addVar('CompanyAddress','',fm.CompanyAddress.value); } catch(ex) { };
  try { mSwitch.addVar('AppntGrpZipCode','',fm.AppntGrpZipCode.value); } catch(ex) { };
  try { mSwitch.addVar('AppntGrpFax','',fm.AppntGrpFax.value); } catch(ex) { };
  try { mSwitch.addVar('PostalAddress','',fm.PostalAddress.value); } catch(ex) { };	
  try { mSwitch.addVar('ZipCode','',fm.ZipCode.value); } catch(ex) { };
  try { mSwitch.addVar('Phone','',fm.Phone.value); } catch(ex) { };
  try { mSwitch.addVar('Mobile','',fm.Mobile.value); } catch(ex) { }; 
  try { mSwitch.addVar('Fax','',fm.Fax.value); } catch(ex) { };
  try { mSwitch.addVar('EMail','',fm.EMail.value); } catch(ex) { };
  try { mSwitch.addVar('BankAccNo','',fm.all('BankAccNo').value); } catch(ex) { };    
  try { mSwitch.addVar('BankCode','',fm.all('BankCode').value); } catch(ex) { };    
  try { mSwitch.addVar('AccName','',fm.all('AccName').value); } catch(ex) { };   
  try { mSwitch.addVar('ActivityID' ,'',tActivityID); } catch(ex) {};
  try { mSwitch.addVar('MissionID' ,'',tMissionID); } catch(ex) {} ;
  try { mSwitch.addVar('SubMissionID' ,'',tSubMissionID); } catch(ex) {} ;
  try { mSwitch.addVar('LoadFlag' ,'',LoadFlag); } catch(ex) {} ;
  try { mSwitch.addVar('PostalCity' ,'',fm.PostalCity.value); } catch(ex) {} ;
  try { mSwitch.addVar('City' ,'',fm.City.value); } catch(ex) {} ;
  try { mSwitch.addVar('PostalCommunity' ,'',fm.PostalCommunity.value); } catch(ex) {} ;
  try { mSwitch.addVar('PostalCounty' ,'',fm.PostalCounty.value); } catch(ex) {} ;
  try { mSwitch.addVar('County' ,'',fm.County.value); } catch(ex) {} ;
  try { mSwitch.addVar('PostalProvince' ,'',fm.PostalProvince.value); } catch(ex) {} ;
  try { mSwitch.addVar('Province' ,'',fm.Province.value); } catch(ex) {} ;
  try { mSwitch.addVar('PostalStreet' ,'',fm.PostalStreet.value); } catch(ex) {} ;
  try { mSwitch.addVar('NativeCity' ,'',fm.AppntNativeCity.value); } catch(ex) {} ;
}  
   
/*********************************************************************
 *  把集体信息从变量中删除
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delContVar()
{  
	try { mSwitch.deleteVar( "intoPolFlag" ); } catch(ex) { };
	// body信息
	try { mSwitch.deleteVar( "BODY" ); } catch(ex) { };
	// 集体信息
  //由"./AutoCreatLDGrpInit.jsp"自动生成
  try { mSwitch.deleteVar('ContNo'); } catch(ex) { };
  try { mSwitch.deleteVar('ProposalContNo'); } catch(ex) { };
  try { mSwitch.deleteVar('PrtNo'); } catch(ex) { };
  try { mSwitch.deleteVar('GrpContNo'); } catch(ex) { };
  try { mSwitch.deleteVar('ContType'); } catch(ex) { };
  try { mSwitch.deleteVar('FamilyType'); } catch(ex) { };
  try { mSwitch.deleteVar('PolType'); } catch(ex) { };
  try { mSwitch.deleteVar('CardFlag'); } catch(ex) { };
  try { mSwitch.deleteVar('ManageCom'); } catch(ex) { };
  try { mSwitch.deleteVar('AgentCom'); } catch(ex) { };
  try { mSwitch.deleteVar('AgentCode'); } catch(ex) { };
  try { mSwitch.deleteVar('AgentGroup'); } catch(ex) { };
  try { mSwitch.deleteVar('AgentCode1'); } catch(ex) { };
  try { mSwitch.deleteVar('AgentType'); } catch(ex) { };
  try { mSwitch.deleteVar('SaleChnl'); } catch(ex) { };
  try { mSwitch.deleteVar('Handler'); } catch(ex) { };
  try { mSwitch.deleteVar('Password'); } catch(ex) { };
  try { mSwitch.deleteVar('AppntNo'); } catch(ex) { };
  try { mSwitch.deleteVar('AppntName'); } catch(ex) { };
  try { mSwitch.deleteVar('AppntSex'); } catch(ex) { };
  try { mSwitch.deleteVar('AppntBirthday'); } catch(ex) { };
  try { mSwitch.deleteVar('AppntIDType'); } catch(ex) { };
  try { mSwitch.deleteVar('AppntIDNo'); } catch(ex) { };
//modify by zxs
//  try {mSwitch.deleteVar('AppntAuth');} catch (ex) {};
  try { mSwitch.deleteVar('InsuredNo'); } catch(ex) { };
  try { mSwitch.deleteVar('InsuredNam'); } catch(ex) { };
  try { mSwitch.deleteVar('InsuredSex'); } catch(ex) { };
  try { mSwitch.deleteVar('InsuredBirthday'); } catch(ex) { };
  try { mSwitch.deleteVar('InsuredIDType'); } catch(ex) { };
  try { mSwitch.deleteVar('InsuredIDNo'); } catch(ex) { };
  try { mSwitch.deleteVar('PayIntv'); } catch(ex) { };
  try { mSwitch.deleteVar('PayMode'); } catch(ex) { };
  try { mSwitch.deleteVar('PayLocation'); } catch(ex) { };
  try { mSwitch.deleteVar('DisputedFlag'); } catch(ex) { };
  try { mSwitch.deleteVar('OutPayFlag'); } catch(ex) { };
  try { mSwitch.deleteVar('GetPolMode'); } catch(ex) { };
  try { mSwitch.deleteVar('SignCom'); } catch(ex) { };
  try { mSwitch.deleteVar('SignDate'); } catch(ex) { };
  try { mSwitch.deleteVar('SignTime'); } catch(ex) { };
  try { mSwitch.deleteVar('ConsignNo'); } catch(ex) { };
  try { mSwitch.deleteVar('BankCode'); } catch(ex) { };
  try { mSwitch.deleteVar('BankAccNo'); } catch(ex) { };
  try { mSwitch.deleteVar('AccName'); } catch(ex) { };
  try { mSwitch.deleteVar('PrintCount'); } catch(ex) { };
  try { mSwitch.deleteVar('LostTimes'); } catch(ex) { };
  try { mSwitch.deleteVar('Lang'); } catch(ex) { };
  try { mSwitch.deleteVar('Currency'); } catch(ex) { };
  try { mSwitch.deleteVar('Remark'); } catch(ex) { };
  try { mSwitch.deleteVar('Peoples'); } catch(ex) { };
  try { mSwitch.deleteVar('Mult'); } catch(ex) { };
  try { mSwitch.deleteVar('Prem'); } catch(ex) { };
  try { mSwitch.deleteVar('Amnt'); } catch(ex) { };
  try { mSwitch.deleteVar('SumPrem'); } catch(ex) { };
  try { mSwitch.deleteVar('Dif'); } catch(ex) { };
  try { mSwitch.deleteVar('PaytoDate'); } catch(ex) { };
  try { mSwitch.deleteVar('FirstPayDate'); } catch(ex) { };
  try { mSwitch.deleteVar('CValiDate'); } catch(ex) { };
  try { mSwitch.deleteVar('InputOperator'); } catch(ex) { };
  try { mSwitch.deleteVar('InputDate'); } catch(ex) { };
  try { mSwitch.deleteVar('InputTime'); } catch(ex) { };
  try { mSwitch.deleteVar('ApproveFlag'); } catch(ex) { };
  try { mSwitch.deleteVar('ApproveCode'); } catch(ex) { };
  try { mSwitch.deleteVar('ApproveDate'); } catch(ex) { };
  try { mSwitch.deleteVar('ApproveTime'); } catch(ex) { };
  try { mSwitch.deleteVar('UWFlag'); } catch(ex) { };
  try { mSwitch.deleteVar('UWOperator'); } catch(ex) { };
  try { mSwitch.deleteVar('UWDate'); } catch(ex) { };
  try { mSwitch.deleteVar('UWTime'); } catch(ex) { };
  try { mSwitch.deleteVar('AppFlag'); } catch(ex) { };
  try { mSwitch.deleteVar('PolApplyDate'); } catch(ex) { };
  try { mSwitch.deleteVar('GetPolDate'); } catch(ex) { };
  try { mSwitch.deleteVar('GetPolTime'); } catch(ex) { };
  try { mSwitch.deleteVar('CustomGetPolDate'); } catch(ex) { };
  try { mSwitch.deleteVar('State'); } catch(ex) { };
  try { mSwitch.deleteVar('Operator'); } catch(ex) { };
  try { mSwitch.deleteVar('MakeDate'); } catch(ex) { };
  try { mSwitch.deleteVar('MakeTime'); } catch(ex) { };
  try { mSwitch.deleteVar('ModifyDate'); } catch(ex) { };
  try { mSwitch.deleteVar('ModifyTime'); } catch(ex) { };

      //新的删除数据处理
  try { mSwitch.deleteVar  ('AppntNo'); } catch(ex) { };				
  try { mSwitch.deleteVar  ('AppntName'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntSex'); } catch(ex) { };				
  try { mSwitch.deleteVar  ('AppntBirthday'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntIDType'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntIDNo'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntPassword'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntNativePlace'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntNationality'); } catch(ex) { };	
  try {mSwitch.deleteVar('AddressNo');}catch(ex){};   	
  try { mSwitch.deleteVar  ('AppntRgtAddress'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntMarriage'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntMarriageDate'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('AppntHealth'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntStature'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntAvoirdupois'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntDegree'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntCreditGrade'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntOthIDType'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntOthIDNo'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntICNo'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntGrpNo'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntJoinCompanyDate'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('AppntStartWorkDate'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('Position') } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntSalary'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntOccupationType'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('AppntOccupationCode'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('AppntWorkType'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntPluralityType');} catch(ex) { };	
  try { mSwitch.deleteVar  ('AppntDeathDate'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntSmokeFlag'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntBlacklistFlag'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('AppntProterty'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntRemark'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntState'); } catch(ex) { };			
  try { mSwitch.deleteVar  ('AppntOperator'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntMakeDate'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntMakeTime'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntModifyDate'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntModifyTime'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('AppntHomeAddress'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntHomeZipCode'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntHomePhone'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntHomeFax'); } catch(ex) { };		
  try { mSwitch.deleteVar  ('AppntPostalAddress'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('AppntZipCode'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntPhone'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntFax'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('Fax'); } catch(ex) { };	
  try { mSwitch.deleteVar  ('AppntMobile'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntEMail'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntGrpName'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntGrpPhone'); } catch(ex) { };
  try { mSwitch.deleteVar  ('CompanyAddress'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntGrpZipCode'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntGrpFax'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntBankAccNo'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntBankCode'); } catch(ex) { };
  try { mSwitch.deleteVar  ('AppntAccName'); } catch(ex) { };
  try { mSwitch.deleteVar  ('ActivityID' ); } catch(ex) {};
  try { mSwitch.deleteVar  ('MissionID' ); } catch(ex) {} ;
  try { mSwitch.deleteVar  ('SubMissionID'); } catch(ex) {} ;
  try { mSwitch.deleteVar  ('LoadFlag'); } catch(ex) {} ;
  try { mSwitch.deleteVar  ('PostalCity'); } catch(ex) { };
  try { mSwitch.deleteVar  ('PostalCommunity' ); } catch(ex) {};
  try { mSwitch.deleteVar  ('PostalCounty' ); } catch(ex) {} ;
  try { mSwitch.deleteVar  ('PostalProvince'); } catch(ex) {} ;
  try { mSwitch.deleteVar  ('PostalStreet'); } catch(ex) {} ;
  try { mSwitch.deleteVar  ('AppntNativeCity'); } catch(ex) { };
}



//***************************校验区***************************************


/*********************************************************************
 *  根据身份证号取得出生日期和性别
 *  参数  ：  身份证号
 *  返回值：  无
 *********************************************************************
 */
function getAppntBirthdaySexByIDNo(iIdNo) 
{
  if(fm.all('AppntIDType').value=="0") 
  {  	
    fm.all('AppntBirthday').value=getBirthdatByIdNo(iIdNo);
    fm.all('AppntSex').value=getSexByIDNo(iIdNo);
  }
}

/*********************************************************************
 *  验证身份证号、生日＆性别
 *  参数  ：  
 *  返回值：  无
 *********************************************************************
 */
function BirthdaySexAppntIDNo()
{
	if(fm.all('AppntIDType').value=="0")
	{
		if(fm.all('AppntBirthday').value!=getBirthdatByIdNo(fm.all('AppntIDNo').value) || fm.all('AppntSex').value!=getSexByIDNo(fm.all('AppntIDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
	
	return true;
}

//提交前的校验
function checkBeforeSubmit()
{
    if("1" == tIntlFlag && fm.UWConfirmNo.value == "")
    {
        alert("请录入核保审核号");
        return false;
    }

    if(fm.all('PolApplyDate').value=="")
    {
        alert("请录入投保申请日期!");
        return false;
    }
    if(!chkDayAndMonth())
    {
        return false;
    }
    if(fm.all('PayMode').value=="4")
    {
        if(fm.all('BankCode').value==null||fm.all('BankCode').value=="")
        {
            alert("请录入开户银行！");
            fm.all('BankCode').focus;
            return false;
        }
        if(fm.all('BankAccNo').value==null||fm.all('BankAccNo').value=="")
        {
            alert("请录入帐号！");
            fm.all('BankAccNo').focus;
            return false;
        }
        if(fm.all('AccName').value==null||fm.all('AccName').value=="")
        {
            alert("请录入户名！");
            fm.all('AccName').focus;
            return false;
        }
    }

    if("6" != tCardFlag && fm.AppntType.value == "1")
    {
        if(fm.ZipCode.value == "")
        {
            alert("邮政编码不能为空");
            return false;
        }
    }
    
    // 集团交叉要素校验
    if(!checkCrsBussParams())
    {
        return false;
    }
    // --------------------

    /*如果是符合修改时使用，执行更新操作 20041125 wzw*/
    //if(LoadFlag=="3")
    //{
    //  updateClick();
    //  return;
    //}
    if (verifyInput2()== false)
    {
        return false;
    }

    if(BirthdaySexAppntIDNo()==false)
    {
        return false;
    }

    if(fm.AppntType.value == "1")
    {
        if(fm.AppntSex.value == "")
        {
            alert("性别不能为空");
            return false;
        }
        if(fm.AppntIDType.value == "")
        {
            alert("证件类型不能为空");
            return false;
        }
    }
    else
    {
        fm.AppntSex.value = "2";  //团体客户的性别默认2

        //if(fm.Fax.value == "")
        //{
        //  alert("传真不能为空");
        //  return false;
        //}
    }
    if(fm.AppntBirthday.value == "")
    {
        fm.AppntBirthday.value = "1900-01-01";
    }

    //if(fm.AppntIDType.value != "4" && fm.AppntIDNo.value == "")
    //{
    //  alert("证件号码必录");
    //  return false;
    //}

    if(fm.AppntNo.value != "")
    {
        var tAppntType = getAppntType();
        if((tAppntType == null || tAppntType == "" || tAppntType == "null" || tAppntType == "1")
        && fm.AppntType.value == "2"
        || tAppntType == 2 && fm.AppntType.value == "1")
        {
            alert("您录入的投保人类型与数据库中的不一致");
            return false;
        }
    }

    if(fm.AppntType.value == "2")
    {
        var sql = "select 1 from LCInsured a, LCAppnt b "
        + "where a.PrtNo = b.PrtNo "
        + "   and a.InsuredNo = b.AppntNo "
        + "   and a.PrtNo = '" + fm.PrtNo.value + "' ";
        var rs = easyExecSql(sql);
        if(rs)
        {
            alert("投保人也是被保人，不能修改为团体投保人");
            return false;
        }
    }
    
    //业务员离职校验
    var agentStateSql = "select 1 from LAAgent where AgentCode = '" + fm.AgentCode.value + "' and AgentState < '06'";
    var arr = easyExecSql(agentStateSql);
    if(!arr){
        alert("该业务员已离职！");
        return;
    }

    //销售渠道为银行代理的保单，银行网点和银代专员的校验   2008-3-13
    if(fm.SaleChnl.value  == "04")
    {
        if(fm.AgentCom.value == null || fm.AgentCom.value == "")
        {
            alert("银行网点代码不能为空！");
            return;
        }
        var agentCodeSql = "select 1 from LAComToAgent where AgentCom='" + fm.AgentCom.value
        + "' and RelaType='1' and AgentCode = '" + fm.AgentCode.value + "'";
        var arr = easyExecSql(agentCodeSql);
        if(!arr){
            alert("银行网点和银代专员不相符！");
            return;
        }
    }

    return true;
}

function checkOccupationType()
{
    if(fm.OccupationCode.value != "")
    {
        var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+fm.OccupationCode.value+"'";
	    var arr = easyExecSql(strSql);
	    if( arr ) {
	        fm.OccupationType.value = arr[0][0];
	    }
    }else
    {
        fm.OccupationType.value = '';
    }
    return true;    
}


//校验录入的投保人类型是否与库中一致
function getAppntType()
{
  var sql = "select CustomerType from LDPerson a, LCCont b "
          + "where a.CustomerNo = b.AppntNo "
          + "   and b.PrtNo = '" + fm.PrtNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs == null || rs[0][0] == null || rs[0][0] == "")
  {
    return "1";
  }
  return rs;
}

//投保人客户号查询按扭事件                                                   
function queryAppntNo() { 
	
  if (fm.all("AppntNo").value == "" && LoadFlag == "1") 
  {                    
    showAppnt1();                                                            
  } 
  else if (LoadFlag != "1" && LoadFlag != "2") 
  {                         
   alert("只能在投保单录入时进行操作！");                                 
  } 
  else 
  {                                                    
    arrResult = queryLDPerson(fm.AppntNo.value);
    if (arrResult == null) 
    {                                                 
      alert("未查到投保人信息");
      return true;
    } 
    else 
    {
      displayAppnt(arrResult[0]);
      //getaddresscodedata();                                            
    }                                                                  
  }                                                                          
}

//查询客户信息
function queryLDPerson(customerNo)
{
  var sql = "select a.* from LDPerson a where 1=1  and a.CustomerNo = '" + customerNo + "' ";
  return easyExecSql(sql);
}

//弹出查询客户窗口，该窗口调用afterQuery返回数据
function showAppnt1()                                                        
{                                                                            
	if( mOperate == 0 )                                                          
	{                                                                            
		mOperate = 2;                                                        
		showInfo = window.open( "../sys/LDPersonQueryNew.html" );               
	}                                                                            
}  

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
  if( arrQueryResult != null ) 
  {
    arrResult = queryLDPerson(arrQueryResult[0][0]);
    if(arrResult)
    {
      displayAppnt(arrResult[0]);
    }
  }
}

/*********************************************************************
 *  把查询返回的投保人数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt()
{
  clearAppnt();
  
  try{fm.all('AppntNo').value= arrResult[0][0]; }catch(ex){};              
  try{fm.all('AppntName').value= arrResult[0][1]; }catch(ex){};            
  try{fm.all('AppntSex').value= arrResult[0][2]; }catch(ex){};             
  try{fm.all('EnglishName').value= arrResult[0][42]; }catch(ex){};             
  try{fm.all('AppntBirthday').value= arrResult[0][3]; }catch(ex){};       
  try{fm.all('AppntIDType').value= arrResult[0][4]; }catch(ex){};          
  try{fm.all('AppntIDNo').value= arrResult[0][5]; }catch(ex){};            
  try{fm.all('AppntPassword').value= arrResult[0][6]; }catch(ex){};        
  try{fm.all('AppntNativePlace').value= arrResult[0][7]; }catch(ex){};     
  try{fm.all('AppntNationality').value= arrResult[0][8]; }catch(ex){};     
  try{fm.all('AppntRgtAddress').value= arrResult[0][9]; }catch(ex){};      
  try{fm.all('AppntMarriage').value= arrResult[0][10];}catch(ex){};        
  try{fm.all('AppntMarriageDate').value= arrResult[0][11];}catch(ex){};    
  try{fm.all('AppntHealth').value= arrResult[0][12];}catch(ex){};          
  try{fm.all('AppntStature').value= arrResult[0][13];}catch(ex){};         
  try{fm.all('AppntAvoirdupois').value= arrResult[0][14];}catch(ex){};     
  try{fm.all('AppntDegree').value= arrResult[0][15];}catch(ex){};          
  try{fm.all('AppntCreditGrade').value= arrResult[0][16];}catch(ex){};     
  try{fm.all('AppntOthIDType').value= arrResult[0][17];}catch(ex){};       
  try{fm.all('AppntOthIDNo').value= arrResult[0][18];}catch(ex){};         
  try{fm.all('AppntICNo').value= arrResult[0][19];}catch(ex){};   
           
  try{fm.all('AppntGrpNo').value= arrResult[0][20];}catch(ex){};           
  try{fm.all('AppntJoinCompanyDate').value= arrResult[0][21];}catch(ex){}; 
  try{fm.all('AppntStartWorkDate').value= arrResult[0][22];}catch(ex){};   
  try{fm.all('Position').value= arrResult[0][23];}catch(ex){};        
  try{fm.all('AppntSalary').value= arrResult[0][24];}catch(ex){};        
  try{fm.all('AppntOccupationType').value= arrResult[0][25];}catch(ex){};  
  try{fm.all('OccupationCode').value= arrResult[0][26];}catch(ex){};
  try {fm.all('AppntOccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][26]);} catch(ex) { };    
  try{fm.all('AppntWorkType').value= arrResult[0][27];}catch(ex){};        
  try{fm.all('AppntPluralityType').value= arrResult[0][28];}catch(ex){};   
  try{fm.all('AppntDeathDate').value= arrResult[0][29];}catch(ex){};       
  try{fm.all('AppntSmokeFlag').value= arrResult[0][30];}catch(ex){};       
  try{fm.all('AppntBlacklistFlag').value= arrResult[0][31];}catch(ex){};   
  try{fm.all('AppntProterty').value= arrResult[0][32];}catch(ex){};        
  try{fm.all('AppntRemark').value= arrResult[0][33];}catch(ex){};          
  try{fm.all('AppntState').value= arrResult[0][34];}catch(ex){};
  try{fm.all('VIPValue').value= arrResult[0][35];}catch(ex){};           
  try{fm.all('AppntOperator').value= arrResult[0][36];}catch(ex){};        
  try{fm.all('AppntMakeDate').value= arrResult[0][37];}catch(ex){};        
  try{fm.all('AppntMakeTime').value= arrResult[0][38];}catch(ex){};      
  try{fm.all('AppntModifyDate').value= arrResult[0][39];}catch(ex){};      
  try{fm.all('AppntModifyTime').value= arrResult[0][40];}catch(ex){};      
  try{fm.all('GrpName').value= arrResult[0][41];}catch(ex){}; 
  try{fm.all('AppntNativeCity').value= arrResult[0][47];}catch(ex){}; 
//modify by zxs
 // try {fm.all('AppntAuth').value = arrResult[0][48];} catch (ex) {};
  
  controlNativeCity("");
  showAllCodeName();
  
}


//显示提交操作时的提示框
function showSubmiInfo()
{
  var showStr="正在提交操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
}

//根据客户姓名查询客户信息
function getAppntByName()
{
  if(fm.AppntName.value != "")
  {
    var sql = "select 1 from LDPerson "
            + "where Name like '%%" + fm.AppntName.value + "' ";
    var rs = easyExecSql(sql);
    if(rs && rs.length > 0)
    {
      window.open("QueryCustomerMain.jsp?Name=" + fm.AppntName.value, "QCM");
    }
  }
}

function afterAppntQuery(appntNo)
{
  fm.AppntNo.value = appntNo;
  queryAppntNo();
}

//外包反馈错误信息
function findIssue()
{
	window.open("../app/BPOIssueInput.jsp?prtNo=" + prtNo,"windowIssue");
}


/**
 * 集团交叉要素校验
 */
function checkCrsBussParams()
{
    var tCrs_SaleChnl = trim(fm.Crs_SaleChnl.value);
    var tCrs_BussType = trim(fm.Crs_BussType.value);
    
    var tGrpAgentCom = trim(fm.GrpAgentCom.value);
    var tGrpAgentCode = trim(fm.GrpAgentCode.value);
    var tGrpAgentName = trim(fm.GrpAgentName.value);
    var tGrpAgentIDNo = trim(fm.GrpAgentIDNo.value);
    
    if(tCrs_SaleChnl != null && tCrs_SaleChnl != "")
    {
        if(tCrs_BussType == null || tCrs_BussType == "")
        {
            alert("选择集团交叉渠道时，集团交叉业务类型不能为空。");
            return false;
        }
        if(tGrpAgentCom == null || tGrpAgentCom == "")
        {
            alert("选择集团交叉渠道时，对方业务员机构不能为空。");
            return false;
        }
        if(tGrpAgentCode == null || tGrpAgentCode == "")
        {
            alert("选择集团交叉渠道时，对方业务员代码不能为空。");
            return false;
        }
        if(tGrpAgentName == null || tGrpAgentName == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名不能为空。");
            return false;
        }
        if(tGrpAgentIDNo == null || tGrpAgentIDNo == "")
        {
            alert("选择集团交叉渠道时，对方业务员身份证不能为空。");
            return false;
        }
    }
    else
    {
        if(tCrs_BussType != null && tCrs_BussType != "")
        {
            alert("未选择集团交叉渠道时，集团交叉业务类型不能填写。");
            return false;
        }
        if(tGrpAgentCom != null && tGrpAgentCom != "")
        {
            alert("未选择集团交叉渠道时，对方业务员机构不能填写。");
            return false;
        }
        if(tGrpAgentCode != null && tGrpAgentCode != "")
        {
            alert("未选择集团交叉渠道时，对方业务员代码不能填写。");
            return false;
        }
        if(tGrpAgentName != null && tGrpAgentName != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名不能填写。");
            return false;
        }
        if(tGrpAgentIDNo != null && tGrpAgentIDNo != "")
        {
            alert("未选择集团交叉渠道时，对方业务员身份证不能填写。");
            return false;
        }
    }
    
    return true;
}
//择对方机构代码时,显示对方机构名称.
function GetGrpAgentName()
{
	var strSql = "select org_lvl from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null) {
	      if(arrResult[0][0] == "0")
	      {
	          fm.GrpAgentComName.value = "总公司";
	      }
	      if(arrResult[0][0] == "1")
	      {
	          fm.GrpAgentComName.value = "省分公司";
	      }
	      if(arrResult[0][0] == "2")
	      {
	          fm.GrpAgentComName.value = "地市分公司";
	      }
	      if(arrResult[0][0] == "3")
	      {
	          fm.GrpAgentComName.value = "县支公司";
	      }
	      if(arrResult[0][0] == "4")
	      {
	          fm.GrpAgentComName.value = "基层机构";
	      }
	}
	else{  
	     fm.GrpAgentComName.value = '';
    }
}


//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryGrpAgent()
{
    if(fm.all('GrpAgentCode').value == "" && fm.all('GrpAgentIDNo').value == "" )
    {  
        var tGrpAgentCom = (fm.GrpAgentCom != null && fm.GrpAgentCom != "undefined") ? fm.GrpAgentCom.value : "";
        var tGrpAgentName = (fm.GrpAgentName != null && fm.GrpAgentName != "undefined") ? fm.GrpAgentName.value : "";
        var tGrpAgentIDNo = (fm.GrpAgentIDNo != null && fm.GrpAgentIDNo != "undefined") ? fm.GrpAgentIDNo.value : "";
        var strURL = "../sys/GrpAgentCommonQueryMain.jsp?GrpAgentCom=" + tGrpAgentCom +
                     "&GrpAgentName=" + tGrpAgentName + "&GrpAgentIDNo=" + tGrpAgentIDNo;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCode').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Sales_Cod='" + fm.all('GrpAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.GrpAgentCode.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }else if(fm.all('GrpAgentIDNo').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Id_No='" + fm.all('GrpAgentIDNo').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("身份证号码为:[" +  fm.GrpAgentIDNo.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery3(arrResult)
{  
  if(arrResult!=null)
    {  	
  	fm.GrpAgentCode.value = arrResult[0][0];
  	fm.GrpAgentName.value = arrResult[0][1];
    fm.GrpAgentIDNo.value = arrResult[0][2];
  }
}


//执行查询交叉销售对方机构代码 date 20101117 by gzh
function queryGrpAgentCom()
{
	if(fm.all('Crs_SaleChnl').value == "" || fm.all('Crs_SaleChnl').value == null)
	{
		alert("请先选择交叉销售渠道！！");
		return false;
	}
    if(fm.all('GrpAgentCom').value == "")
    {  
        var tCrs_SaleChnl =  fm.Crs_SaleChnl.value;
        var tCrs_SaleChnlName = fm.Crs_SaleChnlName.value;
        var strURL = "../sys/GrpAgentComQueryMain.jsp?Crs_SaleChnl="+tCrs_SaleChnl+"&Crs_SaleChnlName="+tCrs_SaleChnlName;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentComQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCom').value != "")
    {	
        var strGrpSql = "select grpagentcom,Under_Orgname  from lomixcom  where  grpagentcom ='"+fm.GrpAgentCom.value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery4(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("机构代码为:[" +  fm.GrpAgentCom.value + "]的机构不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery4(arrResult)
{  
  if(arrResult!=null)
    { 	
  	fm.GrpAgentCom.value = arrResult[0][0];
  	fm.GrpAgentComName.value = arrResult[0][1];
  }
}


//选择交叉销售后对交叉销售各不为空项的校验
    // by gzh 20101118
function MixComCheck()
{    
    if(fm.MixComFlag.checked == true)
    {
    	if(fm.Crs_SaleChnl.value == "" || fm.Crs_SaleChnl.value == null)
    	{
    		alert("选择交叉销售时，交叉销售渠道不能为空，请核查！");
    		fm.all('Crs_SaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.Crs_BussType.value == "" || fm.Crs_BussType.value == null)
    	{
    		alert("选择交叉销售时，交叉销售业务类型不能为空，请核查！");
    		fm.all('Crs_BussType').focus();
    		return false;
    	}
    	if(fm.GrpAgentCom.value == "" || fm.GrpAgentCom.value == null)
    	{
    		alert("选择交叉销售时，对方机构代码不能为空，请核查！");
    		fm.all('GrpAgentCom').focus();
    		return false;
    	}
    	/**
    	if(fm.GrpAgentComName.value == "" || fm.GrpAgentComName.value == null)
    	{
    		alert("选择交叉销售时，对方机构名称不能为空，请核查！");
    		fm.all('GrpAgentComName').focus();
    		return false;
    	}
    	*/
    	if(fm.GrpAgentCode.value == "" || fm.GrpAgentCode.value == null)
    	{
    		alert("选择交叉销售时，对方业务员代码不能为空，请核查！");
    		fm.all('GrpAgentCode').focus();
    		return false;
    	}
    	if(fm.GrpAgentName.value == "" || fm.GrpAgentName.value == null)
    	{
    		alert("选择交叉销售时，对方业务员姓名不能为空，请核查！");
    		fm.all('GrpAgentName').focus();
    		return false;
    	}
    	if(fm.GrpAgentIDNo.value == "" || fm.GrpAgentIDNo.value == null)
    	{
    		alert("选择交叉销售时，对方业务员身份证号码不能为空，请核查！");
    		fm.all('GrpAgentIDNo').focus();
    		return false;
    	}
      	return true;
    }else{
    	return true;
    }
}    
    // --------------------
    
function qryAppAccInfo()
{
    var tStrSql = ""
        + " select "
        + " distinct lcaa.CustomerNo, lcc.AppntName, lcaa.AccGetMoney "
        + " from LCCont lcc "
        + " inner join LCAppAcc lcaa on lcaa.CustomerNo = lcc.AppntNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.PrtNo = '" + prtNo + "' "
        + " and lcaa.State = '1' "
        ;
        
    var arrResult = easyExecSql(tStrSql);
    if(arrResult != null)
    {
        var tAppAccInfo = arrResult[0];
        
        var tAppAccNo = tAppAccInfo[0];
        fm.CustNoAppAcc.value = tAppAccNo;
        
        var tAppAccName = tAppAccInfo[1];
        fm.CustNameAppAcc.value = tAppAccName;
        
        var tAppAccBalance = tAppAccInfo[2];
        fm.BalanceAppAcc.value = tAppAccBalance;
    }
}
// by gzh 20110311
//当修改计算保费的要素Payintv时，若已存在被保险人即已计算过保费，则不允许修改Payintv
function checkPayintv(){
	if(fm.OldPayIntv.value != fm.PayIntv.value){
		var tSql = "select * from lcpol where contno = '"+fm.ContNo.value+"'";
		var arrResult = easyExecSql(tSql);
		if(arrResult != null){
			alert("该保单已对被保人计算保费，若需修改保费计算要素，请先删除险种及被保人信息！");
			return false;
		}
	}
	return true;	
}

function Crs_BussTypeHelp(){
	window.open("../sys/JtjxYwlyInfo.jsp");
}
//校验开户银行
function checkBankCode(){
	var tManageCom = fm.ManageCom.value;
	var tBankCode = fm.BankCode.value;
	if(tBankCode != "" && tBankCode != null){
		var tSql = "select 1 from LDBank where (BankUniteFlag is null or BankUniteFlag<>'1') and Comcode like '" + tManageCom.substring(0, 4) + "%' and BankCode = '"+tBankCode+"' ";
		var arrResult = easyExecSql(tSql);
		if(!arrResult){
			alert("填写的开户银行在系统中未定义，请核查！");
			return false;
		}
	}
	return true;
}


//综合开拓信息显示
function isAssist()
{
    if(fm.ExtendFlag.checked == true)
    {
        fm.all('ExtendID').style.display = "";
    }
    if(fm.ExtendFlag.checked == false)
    {
        fm.all('AssistSalechnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value="";
        fm.all('AssistSaleChnl').value="";
        fm.all('ExtendID').style.display = "none";
    }
}

//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryAssistAgent()
{
    if(fm.all('AssistSaleChnl').value == "" || fm.all('AssistSaleChnl').value == null)
    {
    	alert("请先选择协助销售渠道！");
    	return false;
    }
    if(fm.all('AssistAgentCode').value == "" )
    {  
        var tAssistSaleChnl = (fm.AssistSaleChnl != null && fm.AssistSaleChnl != "undefined") ? fm.AssistSaleChnl.value : "";
        var strURL = "../sys/AssistAgentCommonQueryMain.jsp?AssistSaleChnl=" + tAssistSaleChnl;        
        var newWindow = window.open(strURL, "AssistAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('AssistAgentCode').value != "")
    {	
        var strGrpSql = "select agentcode,name from LAAgent where agentcode ='" + fm.all('AssistAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery6(arrResult);
        }
        else
        {            
            alert("代码为:[" +  fm.AssistAgentCode.value + "]的业务员不存在，请确认!");
        }
    }
}
function afterQuery6(arrResult){
	if(arrResult!=null) {
		fm.AssistAgentCode.value = arrResult[0][0];
		fm.AssistAgentName.value = arrResult[0][1];
	}
}
function ExtendCheck()
{    
    if(fm.ExtendFlag.checked == true)
    {
    	if(fm.AssistSaleChnl.value == "" || fm.AssistSaleChnl.value == null)
    	{
    		alert("选择综合开拓时，协助销售渠道不能为空，请核查！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.AssistSaleChnl.value != "02" && fm.AssistSaleChnl.value != "01")
    	{
    		alert("协助销售渠道只能选择个险直销或团险直销！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.AssistAgentCode.value == "" || fm.AssistAgentCode.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员代码不能为空，请核查！");
    		fm.all('AssistAgentCode').focus();
    		return false;
    	}
    	if(fm.AssistAgentName.value == "" || fm.AssistAgentName.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员姓名不能为空，请核查！");
    		fm.all('AssistAgentName').focus();
    		return false;
    	}
    	var tSQL = "select code1,codename from ldcode1 where codetype = 'salechnl' and code = '"+fm.AssistSaleChnl.value+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("销售渠道与业务员代码不匹配！");
			return false;
		}
		var tSQL1 = "select 1 from laagent where agentcode = '"+fm.AssistAgentCode.value+"' and BranchType = '"+arrResult[0][0]+"' and BranchType2 = '"+arrResult[0][1]+"'";
		var arrResult1 = easyExecSql(tSQL1);
		if(!arrResult1){
			alert("销售渠道与业务员代码不匹配，请核查！");
			return false;
		}
      	return true;
    }else{
    	fm.AssistSaleChnl.value == "";
    	fm.AssistAgentCode.value == "";
    	fm.AssistAgentName.value == "";
    	return true;
    }
}
function initExtend(){
	
	var tSql = "select AssistSalechnl,AssistAgentCode from LCExtend where prtno='" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.ExtendFlag.checked = true;
			fm.all('ExtendID').style.display = "";
			fm.AssistSaleChnl.value = arrResult[0][0];
			fm.AssistAgentCode.value = arrResult[0][1];
			var  tSql1 = "select name from LAAgent where agentcode ='" + arrResult[0][1] + "'";
			var arrResult1 = easyExecSql(tSql1);
			fm.AssistAgentName.value = arrResult1[0][0];
		}
	}
}

function initCountyType(){
   var tSql = "select CountyType,FamilySalary from lccontsub where prtno='" + fm.PrtNo.value + "' ";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.appnt_CountyType.value = arrResult[0][0];
			var  tSql1 = "select codename from ldcode where codetype= 'countytype' and code ='" + arrResult[0][0] + "'";
			var arrResult1 = easyExecSql(tSql1);
			fm.appnt_CountyName.value = arrResult1[0][0];
		}
		fm.FamilySalary.value=arrResult[0][1];
	}
}

function chenkIdNo(){
        var idtype = fm.AppntIDType.value;
        var idno = fm.AppntIDNo.value;
        if(idtype == "" || idtype == null){
             alert("证件类型不能为空！");
             return false;
        }else{
             var arr = easyExecSql("select code1,codealias,othersign from ldcode1 where codetype='checkidtype' and code='"+idtype+"'");
             if(arr!=null){
                var othersign = arr[0][2];
                var first = arr[0][0];
                var two = arr[0][1];
                if("double"==othersign){
                   if(idno.length!=first&&idno.length!=two){
                       alert("录入的证件号码有误，请检查！");
                       return false;
                   }
                }
                if("min"==othersign){
                   if(idno.length<first){
                      alert("录入的证件号码有误，请检查！");
                       return false;
                   }
                }
                if("between"==othersign){
                   if(idno.length<first||idno.length>two){
                      alert("录入的证件号码有误，请检查！");
                      return false;
                   }
                }
                
             }else{
                alert("选择的证件类型有误，请检查！");
                return false;
             }
        }
        
        return true;
}

function checkAddress(){
   var PostalProvince = fm.PostalProvince.value;
   var PostalCity = fm.PostalCity.value;
   var PostalCounty = fm.PostalCounty.value;
   var PostalStreet = fm.PostalStreet.value;
   var PostalCommunity = fm.PostalCommunity.value;
   //校验联系地址级联关系
   var checkCityError = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province.value+"' "
		   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"' )");
   if(checkCityError == null || checkCityError == ""){
	   alert("投保人省、市地址级联关系不正确，请检查！");
	   return false;
   }
   var checkCityError2 = easyExecSql("select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County.value+"')");
//   var checkCityError2 = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province.value+"' "
//	   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County.value+"'))");
   if(checkCityError2 == null || checkCityError2 == ""){
	   alert("投保人市、县地址级联关系不正确，请检查！");
	   return false;
   }
 //当省级单位下没有市县级单位或市级单位下没有县级单位的时候改变拼接完整地址的方式
   if(fm.PostalCity.value == '空' && fm.PostalCounty.value == '空'){
	   fm.PostalAddress.value=PostalProvince+PostalStreet+PostalCommunity;
   }else if(fm.PostalCity.value != '空' && fm.PostalCounty.value == '空') {
	   fm.PostalAddress.value=PostalProvince+PostalCity+PostalStreet+PostalCommunity;
   }else{
	   fm.PostalAddress.value=PostalProvince+PostalCity+PostalCounty+PostalStreet+PostalCommunity;
   }
   return true;
}

function checkMobile(mobile){
   if(mobile != "" && mobile != null){
      if(mobile.length!=11){
          alert("移动电话不符合规则，请核实！");
          return false;
      }else{
         if(mobile.substring(0,2)!=13&&mobile.substring(0,2)!=14&&mobile.substring(0,2)!=16&&mobile.substring(0,2)!=15&&mobile.substring(0,2)!=18&&mobile.substring(0,2)!=17&&mobile.substring(0,2)!=19){
            alert("移动电话不符合规则，请核实！");
            return false;
         }
      }
   }else{
      if(!confirm("投保人移动电话为空，是否继续？")){
         return false;
	  }
   }
   return true;
}

function checkAllapp(){
   var mobile = fm.Mobile.value;
    if(!chenkIdNo()){
	  return false;
	}
	
	if(!checkAddress()){
	  return false;
	}
	if(mobile != null && mobile !="") {
	    if(!isInteger(mobile) || mobile.length != 11){
		    alert("投保人移动电话需为11位数字，请核查！");
   	      	return false;
	    } 
	}
	if(!checkMobile(mobile)){
	  return false;
	}
	
   return true;
}

function checkCrs_SaleChnl(){
	var tCrs_BussType=fm.Crs_BussType.value;
	var tSaleChnl=fm.SaleChnl.value;
	if((tCrs_BussType=="01"||tCrs_BussType=="02")&&(tSaleChnl=="16"||tSaleChnl=="18")){
		alert("社保直销及社保综拓渠道出单若选择交叉销售，交叉销售业务类型不能为相互代理或联合展业！");
		return false;
	}
	return true;
}

//销售渠道和业务员的校验
function checkChnlComCode()
{
    //业务员离职校验
    var agentStateSql = "select 1 from LAAgent where AgentCode = '" 
        + fm.AgentCode.value + "' and AgentState < '06'";
    var arr = easyExecSql(agentStateSql);
    if(!arr){
        alert("该业务员已离职！");
        return;
    }
    
    var tAgentCode = fm.AgentCode.value;
    var tSaleChnl = fm.SaleChnl.value;
    var tManageCom = fm.ManageCom.value;
    var tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
	var arrCode = easyExecSql(tSQLCode);
	if(!arrCode){
		alert("业务员与管理机构不匹配！");
		return false;
	}
    //业务员和销售渠道的校验
    var agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
	                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
                     + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
                     + "union all "
		             + "select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
		             + "and '10' = '" + tSaleChnl + "' and a.BranchType2 = '04'  "
		             + "union all "
		             + "select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
		             + "and '03' = '" + tSaleChnl + "' and a.BranchType2 = '04'  "
		             + "union all "
		             + "select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
		             + "and '02' = '" + tSaleChnl + "' and a.BranchType = '2' and a.BranchType2 = '02'  "
		             ;

    var arr = easyExecSql(agentCodeSql);
    if(!arr){
        alert("业务员和销售渠道不相符！");
        return;
    }
    
    if(fm.SaleChnl.value == "04" || fm.SaleChnl.value == "15"|| fm.SaleChnl.value == "10"|| fm.SaleChnl.value == "03"|| fm.SaleChnl.value == "20"){
    	var tSQL = "select 1 from lacomtoagent where agentcom = '"+fm.AgentCom.value+"' and agentcode = '"+fm.AgentCode.value+"' ";
    	var arrComCode = easyExecSql(tSQL);
    	if(!arrComCode){
    		alert("业务员和中介机构不相符！");
        	return;
    	}
    }
    return true;
}
//双击对方业务员框要显示的页面
function otherSalesInfo(){
	//alert("双击对方业务员代码");
	window.open("../sys/MixedSalesAgentMain.jsp?ManageCom="+fm.ManageCom.value);
}
//对方业务员页面查询完“返回”按钮，接收数据
function afterQueryMIX(arrResult){
	if(arrResult!=null){
		var arr = arrResult.split("#");
		fm.GrpAgentCom.value = arr[0]; //对方机构代码
		//fm.GrpAgentComName.value = arr[1]; //对方机构名称
		fm.GrpAgentCode.value = arr[2]; //对方业务员代码
		fm.GrpAgentName.value = arr[3]; //对方业务员姓名
		fm.GrpAgentIDNo.value = arr[4]; //对方业务员身份证
		
	}else{
		alert("返回过程出现异常，请重新操作");
	}	
}

function controlNativeCity(displayFlag)
{
  if(fm.AppntNativePlace.value=="OS"){
    fm.all("NativePlace").style.display = displayFlag;
	fm.all("NativeCity").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="国家";
  }else if(fm.AppntNativePlace.value=="HK"){
    fm.all("NativePlace").style.display = displayFlag;
	fm.all("NativeCity").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="地区";
  }else{
    fm.all("NativePlace").style.display = "none";
	fm.all("NativeCity").style.display = "none";
  }
}  

function checkNative(){
   if(fm.AppntNativePlace.value=="OS"||fm.AppntNativePlace.value=="HK"){
      if(fm.AppntNativeCity.value==""||fm.AppntNativeCity.value==null){
         alert("投保人所属国家/地区未录入！");
         return false;
      }
   }
   return true;
}

function changePostalAddress(){
	if(fm.PostalCity.value == '空' ){
		   fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalStreet.value+fm.PostalCommunity.value;
	   }else if(fm.PostalCity.value != '空' && fm.PostalCounty.value == '空') {
		   fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalCity.value+fm.PostalStreet.value+fm.PostalCommunity.value;
	   }else{
		   fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalCity.value+fm.PostalCounty.value+fm.PostalStreet.value+fm.PostalCommunity.value;
	   }
}
function rightKey(){
	if(event.button == 1){
		event.returnValue = false;
		alert("禁止修改");
	}
}

