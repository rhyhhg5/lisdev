<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DownloadCustCardSave.jsp
//程序功能：客户卡下载
//创建日期：2007-8-23 11:28
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.intltb.*"%>
<%
  String FlagStr = "";
  String Content = "";
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //获得mutline中的数据信息
  
  String tSuffix = request.getParameter("Suffix");
  
  String checkBox[] = request.getParameterValues("InpContGridChk");
  String tContNo[] = request.getParameterValues("ContGrid1");
  
  LCContSet tLCContSet = new LCContSet();
  for(int i = 0; i < checkBox.length; i++)
  {
    if("1".equals(checkBox[i]))
    {
      LCContSchema tLCContSchema = new LCContSchema();
      tLCContSchema.setContNo(tContNo[i]);
      tLCContSet.add(tLCContSchema);
    }
  }
  
  String fileName = "Card" + PubFun.getCurrentDate() + "-" + (System.currentTimeMillis() % 1000) + "." + tSuffix;
  if(tLCContSet.size() == 1)
  {
    fileName = "Card" + tLCContSet.get(1).getContNo() + "." + tSuffix;
  }
  
  String tFileFolder = "printdata/data";
  String tOppositePath = tFileFolder + "/" + fileName;
  String tOutXmlPath = application.getRealPath(tFileFolder) + "/" + fileName;
  
  System.out.println("\nOutXmlPath:" + tOutXmlPath);
  System.out.println("OppositePath:" + tOppositePath);
  System.out.println(tLCContSet.size());
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
  tTransferData.setNameAndValue("OppositePath",tOppositePath);
  tTransferData.setNameAndValue("Suffix",tSuffix);
  
	try
	{
    VData vData = new VData();
  	vData.add(tG);
  	vData.add(tLCContSet);
  	vData.add(tTransferData);
  	
    IntlDownloadCustCardUI tIntlDownloadCustCardUI = new IntlDownloadCustCardUI();
		if (!tIntlDownloadCustCardUI.submitData(vData, ""))
		{
			Content = "客户卡下载失败，原因是:" + tIntlDownloadCustCardUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
	  else
	  {
	 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
	 	  File file = new File(tOutXmlPath);
	 	  
	    response.reset();
      response.setContentType("application/octet-stream"); 
      response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
      response.setContentLength((int) file.length());
      
      byte[] buffer = new byte[4096];
      BufferedOutputStream output = null;
      BufferedInputStream input = null;    
      //写缓冲区
      try 
      {
        output = new BufferedOutputStream(response.getOutputStream());
        input = new BufferedInputStream(new FileInputStream(file));
        
        int len = 0;
        while((len = input.read(buffer)) >0)
        {
          output.write(buffer,0,len);
        }
        input.close();
        output.close();
      }
      catch (Exception e) 
      {
        e.printStackTrace();
      } // maybe user cancelled download
      finally 
      {
          if (input != null) input.close();
          if (output != null) output.close();
      }
	  }
	}
	catch(Exception ex)
	{
	  ex.printStackTrace();
	}
  
  if (!FlagStr.equals("Fail"))
  {
  	Content = "";
  	FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">
  //parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>
