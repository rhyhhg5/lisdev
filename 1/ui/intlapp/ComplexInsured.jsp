
<!-- 国际业务被保人基本信息部分 -->
<DIV id=DivLCInsuredButton STYLE="display:''">
<!-- 被保人信息部分 -->
  <table>
    <tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCInsured);">
      </td>
      <td class= titleImg>
        <Input class= mulinetitle value="第一被保险人资料"  name=InsuredSequencename readonly >（客户号：<Input class= common name=InsuredNo >
        <INPUT id="butBack" VALUE="查  询" class=cssButton TYPE=button onclick="queryInsuredNo();"> 首次投保客户无需填写客户号）
        <Div  id= "divSamePerson" style="display:'none'">
          <font color=red>
          如投保人为被保险人本人，可免填本栏，请选择
          <INPUT TYPE="checkbox" NAME="SamePersonFlag" onclick="isSamePerson();">
          </font>
        </div>
      </td>
    </tr>
  </table>
</DIV> 

<DIV id=DivRelation STYLE="display:''">
  <table  class= common>  
    <TR  class= common>
      <TD  class= title id="SequenceNoTitleID">
        客户内部号码</TD>             
      <TD  class= input id="SequenceNoInputID">
        <Input class="code" name="SequenceNo"  readonly= true elementtype=nacessary   CodeData="0|^1|第一被保险人 ^2|第二被保险人 ^3|第三被保险人 ^4|第四被保险人" ondblclick="return showCodeListEx('SequenceNo', [this]);" onkeyup="return showCodeListKeyEx('SequenceNo', [this]);">
      </TD>              
      <TD  class= title>与投保人关系</TD>
      <TD  class= input>           
        <Input class="code" name="RelationToAppnt" readonly= true elementtype=nacessary  ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);" >
      </TD> 
	    <TD  class= title id="RelationToMainInsuredTitleID">与第一被保险人关系</TD>             
      <TD  class= input id="RelationToMainInsuredInputID">
        <Input class="code" name="RelationToMainInsured" readonly= true   elementtype=nacessary  ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);">
      </TD>
      <TD  class= input id="FieldTD1">
      </TD>
      <TD  class= input id="FieldTD2">
      </TD>
    </TR> 
  </table> 
</DIV>  

<div id="DivLCInsured" style="display: ''">
  <table class = common>
		<tr class=common>
			<td class=title COLSPAN="1">
				姓名
			</td>
			<td class=input COLSPAN="1">
				<Input class= common name=Name elementtype=nacessary verify="姓名|notnull&len<=60">
			</td>
			<td class=title COLSPAN="1" id = EnglishNameText>
				拼音
			</td>
			<td class=input COLSPAN="1" id = EnglishNameClass>
				<Input class=common name=EnglishName  verify="投保人拼音" >
			</td>
			<TD  class= title8>
        	证件类型
      		</TD>
      		<TD  class= input8>
        		<Input class= codeno name=IDType value="0" verify="证件类型|code:IDType" readonly ondblclick="return showCodeList('IDType',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=IDTypeName elementtype=nacessary readonly=true verify="被保险人证件类型|code:IDType">
      		</TD>
      		<TD  class= title COLSPAN="1">
       	 	证件号码
      		</TD>
      		<TD  class= input COLSPAN="1">
        		<Input class= common name=IDNo elementtype=nacessary verify="证件号码|len<=30" onblur="getBirthdaySexByIDNo(this.value)">
      		</TD>	  			
			</tr>
			<tr class=common>	
			<tr class=common id="Row3ID">
  	  		 <TD  class= title COLSPAN="1" id="AppntBirthdayTitleID">
  	          证件生效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" id="AppntIDStartDateInput">
  	        	<Input class= coolDatePicker name=IDStartDate  verify="证件生效日期|date">
  	        </TD>
  	        	 <TD  class= title COLSPAN="1" id="AppntBirthdayTitleID">
  	          证件失效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" id="AppntIDEndDateInput">
  	        	<Input class= coolDatePicker name=IDEndDate  verify="证件失效日期|date">
  	        </TD>
  	       <TD  class= title COLSPAN="1">
        	性别
      		</TD> 
      		<TD  class= input COLSPAN="1">
      			<Input class= codeno name=Sex verify="性别|notnull" ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1],null,null,null,1);"><input class=codename name=SexName readonly elementtype=nacessary>
      		</TD>  
  	        </tr>                
      		<TD  class= title COLSPAN="1" >
        		出生日期
      		</TD> 
		    <TD  class= input COLSPAN="1">
      			<Input class= coolDatePicker name=Birthday elementtype=nacessary verify="出生日期|notnull&len<=20">
      	  	</TD>
      	  	<TD  class= title>
            	婚姻状况
          	</TD>
          	<TD  class= input>
            	<Input class=codeno name=Marriage  ondblclick="return showCodeList('Marriage',[this,MarriageName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Marriage',[this,MarriageName],[0,1],null,null,null,1);"  verify="婚姻状况|notnull&code:Marriage"><input class=codename name=MarriageName readonly=true>
          	</TD> 
          	<TD  class= title id="NativePlaceTitleID">
       	 	国籍
     		</TD>
      		<TD  class= input id="NativePlaceInputID">
        		<Input class=codeno name=NativePlace verify="国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,NativePlaceName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('NativePlace',[this,NativePlaceName],[0,1],null,null,null,1);" ><input class=codename name=NativePlaceName readonly=true >
      		</TD>
      		<TD  class= title id="NativePlace1" style="display: none">
            <div id="NativeCityTitle">国家</div>
          </TD>
          <TD  class= input id="NativeCity1" style="display: none">
          <input class=codeNo name="NativeCity" ondblclick="return showCodeList('NativeCity',[this,NativeCityName],[0,1],null,fm.NativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,NativeCityName],[0,1],null,fm.NativePlace.value,'ComCode',1);"><input class=codename name=NativeCityName readonly=true >    
          </TD>  
          	</tr> 
          	<TR>  
      		<TD  class= title id = OccupationCodeText>
        	职业代码
      		</TD> 
      		<TD  class= input id = OccupationCodeClass>
      			<Input class= codeno name=OccupationCode style="width:50px" verify="职业代码" ondblclick="return showCodeList('OccupationCode',[this,OccupationName],[0,1],null,null,null,1,300);" onkeyup="return showCodeListKey('OccupationCode',[this,OccupationName],[0,1],null,null,null,1,300);"><input class=codename name=OccupationName readonly=true  style="width:110px">
      		</TD>
      		<TD  class= title>
      	     年收入(万元)
   	 		</TD>
    		<TD  class= input>
      			<Input class= 'common' name=Salary>
    		</TD> 
    		<td class=title COLSPAN="1" id = WorkNameText>
    		工作单位
    		</td>
    		<td class=input8 COLSPAN="3" id = WorkNameClass>
      			<input class= common3 name=GrpName verify="工作单位"> 
      		</td>
      		</TR> 	 	    		       	       	       	       	      
     		<tr class=common id=PostalAddressID  style="display: 'none'">
     		<td class=title8 COLSPAN="1">
    		联系地址
    		</td>
    		<TD  class= input colspan=5>
      			<input class= common3 name=PostalAddress verify="联系地址" readonly=true>
     		</td>
      		</TR> 	 	    		       	       	       	       	      
     		<tr class=common>
     		<td class=title8 COLSPAN="1">
    		联系地址
    		</td>
    		<TD  class= input colspan=9>
					<Input class="codeno" name=Province verify="省（自治区直辖市）|notnull" ondblclick="return showCodeList('Province1',[this,PostalProvince],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province1',[this,PostalProvince],[0,1],null,'0','Code1',1);" onblur="return changePostalAddress();" readonly=true ><input class=codename name=PostalProvince readonly=true elementtype=nacessary>省（自治区直辖市）
					<Input class="codeno" name=City verify="市|notnull" ondblclick="return showCodeList('City1',[this,PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onkeyup="return showCodeListKey('City1',[this,PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onblur="return changePostalAddress();" readonly=true ><input class=codename name=PostalCity readonly=true elementtype=nacessary>市
					<Input class="codeno" name=County verify="县（区）|notnull" ondblclick="return showCodeList('County1',[this,PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onkeyup="return showCodeListKey('County1',[this,PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onblur="return changePostalAddress();" readonly=true ><input class=codename name=PostalCounty readonly=true elementtype=nacessary>县（区）
					<input  class= common10 name=PostalStreet verify="乡镇（街道）|notnull"  onblur="return changePostalAddress();"> 乡镇（街道）
			    	<input  class= common10 name=PostalCommunity verify="村（社区）|notnull" onblur="return changePostalAddress();"> 村（社区）
			    </TD>
			    </TR>
			    <tr class=common>
			<td class=title id=ZhiweiApptitle style="display: 'none'">
				岗位职务
			</td>
       		<TD  class= input  id=ZhiweiApp style="display: 'none'">
      			<Input name=Position VALUE=""  CLASS=common  >
       		</TD>
    		
    		<!-- <td class=input8 COLSPAN="3">
      			<input class= common3 name=PostalAddress verify="联系地址">
      			<input  class= common10 name=PostalProvince type="hidden">
		        <input  class= common10 name=PostalCity type="hidden">
		        <input  class= common10 name=PostalCounty type="hidden">
		        <input  class= common10 name=PostalStreet type="hidden">
		        <input  class= common10 name=PostalCommunity type="hidden">
     		</td> -->
      		<TD  class= title8>
        	邮政编码
      		</TD>
      		<TD  class= input8>
        		<Input class= common8 name=ZipCode    verify="邮政编码|zipcode">
      		</TD> 
      		 <TD  class= title>
            		授权使用客户信息
          </TD>
          <TD  class= input>
            <Input class=codeNo name="InsuredAuth" readonly=true  verify="授权使用客户信息|code:Auth" ondblclick="return false;return showCodeList('AUTH',[this,InsuredAuthName],[0,1]);" onkeyup="return false;return showCodeListKey('Auth',[this,InsuredAuthName],[0,1]);"><input class=codename name=InsuredAuthName readonly=true elementtype=nacessary>    
          </TD>  
    		</tr>
    		<tr class=common8 >
    		<TD  class= title8 COLSPAN="1">
        	联系电话
      		</TD>
      		<TD  class= input8 COLSPAN="1">
        		<Input class= common8 name=Phone  verify="联系电话|len<=30">
      		</TD>
      		<TD  class= title8>
        	移动电话
      		</TD>
      		<TD  class= input8>
        		<Input class= common8 name=Mobile   verify="移动电话|len<=30">
      		</TD>
     		<TD  class= title8 COLSPAN="1" id="CompanyPhoneTitleID" style="display: none">
       		 办公电话
      		</TD>
      		<TD  class= input8 COLSPAN="1" id="CompanyPhoneInputID" style="display: none">
        		<Input class= common8 name=CompanyPhone verify="办公电话|len<=30">
      		</TD>
      		<TD  class= title8 COLSPAN="1">
       		 电子邮箱
      		</TD>
      		<TD  class= input8 COLSPAN="1">
        		<Input class= common8 name=EMail verify="电子邮箱|len<=60&Email">
      		</TD>
    		</tr>
  		</table>
	</div>

<TABLE class= common>
    <TR class= common>
        <TD  class= title>
            <DIV id="divContPlan" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            保障计划
                    </TD>
                    <TD  class= input>
                        <Input class="code" name="ContPlanCode" verify="保障计划|code:ContPlanCode" ondblclick="showCodeListEx('ContPlanCode',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ContPlanCode',[this],[0],'', '', '', true);">
                    </TD>
                    </TR>
	            </TABLE>
            </DIV>
        </TD> 
        <TD  class= title>
            <DIV id="divExecuteCom" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            处理机构
                        </TD>
                        <TD  class= input>
                            <Input class="code" name="ExecuteCom" ondblclick="showCodeListEx('ExecuteCom',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ExecuteCom',[this],[0],'', '', '', true);">
                        </TD>
		            </TR>
	            </TABLE>
            </DIV>
        </TD>
        <TD  class= title>
        	<DIV id="divPolTypeFlag" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD CLASS=title>
					          保单类型标记
					        </TD>
					        <TD CLASS=input >
					          <Input class=codeno name=PolTypeFlag  verify="保单类型标记|code:PolTypeFlag" value="0" ondblclick="return showCodeList('PolTypeFlag',[this,PolTypeFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('PolTypeFlag',[this,PolTypeFlagName],[0,1],null,null,null,1);"><input class=codename name=PolTypeFlagName readonly=true >	
					        </TD> 
		            </TR>
	            </TABLE>
            </DIV>
        </TD>		
    </TR>
</TABLE>