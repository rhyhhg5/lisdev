//ChangExtractRate.js该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass(); // 使用翻页功能，必须建立为全局变量

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
	
}

function Submit(flag) {
	if(tPrtNo == null || tPrtNo == ""){
		alert("传入的保单信息为空，请关闭页面重新进入");
		return false;
	}
	if(flag != "DELETE"){
		//var sql = "select 1 from lcpol lcp where lcp.riskcode in ('332901','333001','334601') and lcp.prtno = '" + tPrtNo + "'";
		var sql = "select ld.code from lcriskdutywrap lc,ldcode1 ld where lc.prtno='" + tPrtNo + "' and ld.codetype='changerate' and lc.riskwrapcode=ld.code";
		var arrResult = easyExecSql(sql);
		var riskWrap = "";
		if(arrResult == null || arrResult.length == 0){
		    sql = "select distinct code from ldcode1 where codetype='changerate' ";
		    arrResult = easyExecSql(sql);
		    if(arrResult){
		      for(var index = 0; index < arrResult.length; index++){
		         if(index==0){
		            riskWrap += arrResult[index][0];
		         }else{
		            riskWrap +="、"+arrResult[index][0];
		         }
		      }
		    }
			alert("尚未添加"+riskWrap+"套餐，无需进行费率比例录入");
			return false;
		}
		riskWrap = arrResult[0][0];
		sql = "select code1,codealias,othersign,codename from ldcode1 ld where ld.codetype='changerate' and ld.code='"+riskWrap+"' order by int(code1)";
		arrResult = easyExecSql(sql);	
		for(var num = 0; num < ChangeRateGrid.mulLineCount; num++){
			if(ChangeRateGrid.getRowColData(num,2) == ""){
				alert("请录入第" + (num + 1) + "行费用比例");
				return false;
			}
			var codealias=arrResult[num][1];
			var othersign=arrResult[num][2];
			var codename=arrResult[num][3];
			if(othersign=="equal"&&ChangeRateGrid.getRowColData(num,2)!=codealias){
			   alert(codename);
			   return false;
			}
			if(othersign=="lessqual"&&ChangeRateGrid.getRowColData(num,2)>codealias){
			   alert(codename);
			   return false;
			}
		}
	}
    var i = 0;
    var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./ChangeRateSave.jsp?PrtNo=" + tPrtNo + "&Operate=" + flag;
    fm.submit(); //提交
}
