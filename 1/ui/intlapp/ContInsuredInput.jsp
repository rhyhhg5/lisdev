<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ContInsuredInput.jsp
//程序功能：国际业务被保人录入
//创建日期：2007-6-21 9:50
//创建人  ：Yangyalin
//更新记录：  更新人     更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<head >
<script>
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var prtNo = "<%=request.getParameter("PrtNo")%>";
	if (prtNo == null)
	    prtNo="";	
	var checktype = "<%=request.getParameter("checktype")%>";	    
	var scantype = "<%=request.getParameter("scantype")%>";
	var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
	if (ScanFlag == "null") ScanFlag = "1";
	var LoadFlag = "<%=request.getParameter("LoadFlag")%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
	if (LoadFlag == null)
	    Loadflag=1
        var ContType = "<%=request.getParameter("ContType")%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
	if (ContType == "null")
	    ContType=1
	var Auditing = "<%=request.getParameter("Auditing")%>";  //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
	if (Auditing == "null")
	    Auditing=0;
	var ProcessID = "";
	var ActivityID = "<%=request.getParameter("ActivityID")%>";
	var tMissionID = "<%=request.getParameter("MissionID")%>";
	var tSubMissionID = "<%=request.getParameter("SubMissionID")%>";
	var AppntNo = "<%=request.getParameter("AppntNo")%>";
	var AppntName = "<%=request.getParameter("AppntName")%>";
	var AppntAuth = "<%=request.getParameter("AppntAuth")%>";
	var BQFlag = "<%=request.getParameter("BQFlag")%>";	
	var EdorType = "<%=request.getParameter("EdorType")%>";
	var SaleChnl = "<%=request.getParameter("SaleChnl")%>";	
	var SaleChnlDetail = "<%=request.getParameter("SaleChnlDetail")%>";	
	var oldContNo = "<%=request.getParameter("oldContNo")%>";
	var GrpContNo = "<%=request.getParameter("polNo")%>";
	var Resource = "<%=request.getParameter("Resource")%>";
	var param="";
	var tVideoFlag = "<%=request.getParameter("VideoFlag")%>";	
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>	
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="ProposalAutoMove.js"></SCRIPT> 	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="ContInsuredLCImpart.js"></SCRIPT>
  
  <SCRIPT src="ContInsuredInput.js"></SCRIPT>
  <SCRIPT src="InputConfirm.js"></SCRIPT>
  <SCRIPT src="ContInsuredAutoMove.js"></SCRIPT>
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  <%@include file="ContInsuredInit.jsp"%>
  <%if(request.getParameter("scantype")!=null&&request.getParameter("scantype").equals("scan")){%>  
    <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
    <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  <%}%>   
</head>
<body  onload="initForm();initElementtype();setFocus();" >
  <Form action="./ContInsuredSave.jsp" method=post name=fm target="fraSubmit">    
    
    <br>
    
    <div id= "divAddDelButton" style= "display: ''" align=right>
      <input type =button name="addInsuredButton" class=cssButton value="添加被保险人" onclick="addRecord();" id="addInsuredButtonID"> </TD>
      <input type =button class=cssButton value="修改被保险人" onclick="modifyRecord();"></TD>
      <input type =button name="delInsuredButton" class=cssButton value="删除被保险人" onclick="deleteRecord();" name="delInsuredButtonID"> </TD>
    </DIV>
    
    <Div  id= "divFamilyType" style="display:'none'">
    <table class=common>
       <tr class=common>
          <TD  class= title>
            个人合同类型
          </TD>
          <TD  class= input>
            <Input class="code" name=FamilyType value="1" ondblclick="showCodeList('FamilyType',[this]);" onkeyup="return showCodeListKey('FamilyType',[this]);" onfocus="choosetype();">
          </TD>
       </tr>
    </table>
    </Div>
    
    <DIV id="divPrtNo" style="display:''">
    	<table class=common>
    		<TR>
    			<TD  class= title>
    			  印刷号码
    			</TD>
    			<TD  class= input>
    			  <Input class="common" readonly name=PrtNo verify="印刷号码|notnull" >
    			</TD>
     			<TD  class= title>
    			</TD>
    			<TD  class= input>
    			</TD>   			
    		</TR>
    	</table>
    </DIV>
    <Div  id= "divInsureGrid" style= "display: ''">
    	<Table  class= common>
    		<tr>
    			<td text-align: left colSpan=1>
	  				<span id="spanInsuredGrid" >
	  				</span> 
	  			</td>
    		</tr>
    	</Table>
    </div>

   <%@include file="ComplexInsured.jsp"%>
    
	  <hr>
	  
		<!-- 抵达国家 -->
		<div id="NationDiv" style="display: 'none'">
			<table class=common>
				<tr>
					<td COLSPAN="1" class="titleImg">
						<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,NationGridDiv);">抵达国家
					</td>
				</tr>
			</table>
		</div> 
		 
		<div id="NationGridDiv" style="display: 'none'">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanNationGrid" >
						</span>
					</td>
				</tr>
			</table>
		</div> 
    
    <hr/>
      
    <%@include file="ContInsuredLCImpart.jsp"%>
    
    <br />
    
    <br />
    
    <!--套餐信息-->
    <DIV id=DivLCPol STYLE="display:''">
      <table>
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
          </td>
          <td class= titleImg>
            套餐信息
          </td>
        </tr>
      </table>

      <div  id= "divLCPol1" style= "display: ''">
        <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
              <span id="spanWrapGrid" >
              </span>
            </td>
          </tr>
        </table>
      </div>
      <Div id= "divPage2" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
    	</Div>
    </DIV>
    
    <div id= "queryWrapDetailID" style= "display: ''">
      <input type =button name="queryWrapDetailButton" class=cssButton value="责任查询" onclick="queryWrapDetail();"> </TD>
    
      <hr>
    </DIV>
    
    <!--套餐责任信息-->
    <DIV id=RiskWrapID STYLE="display:''">
      <table>
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskWrap);">
          </td>
          <td class= titleImg>
            <input class="readonly" name="PolInfo" value = "责任信息" readOnly >
          </td>
        </tr>
      </table>
      <div><font color="red">目前只有套餐（WR0043）可以进行追加保费，套餐（WR0105）金利宝产品必须录入缴费期间和缴费期间标记。</font></div>
      
	  <div><font color="red">购买康利人生两全保险（分红型）时，必须填写红利领取方式。</font></div>
      <div>
      <table  class= common>
        <tr  class= common>
          <td>红利领取方式
      <Input id=BonusGetMode class=codeno name=BonusGetMode verify="红利领取方式|code:BonusGetMode" ondblclick="return showCodeList('BonusGetMode',[this,BonusGetModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('BonusGetMode',[this,BonusGetModeName],[0,1],null,null,null,1);"><input class=codename name=BonusGetModeName> 		     		
          </td>
          <td></td>
          <td></td>
        </tr>
        </table>
      </div>
      <div  id= "divRiskWrap" style= "display: ''">
        <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
              <span id="spanRiskWrapGrid" >
              </span>
            </td>
          </tr>
        </table>
      </div>
      <Div id= "divPage3" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage();"> 
    	</Div>
    </DIV>
    
    <!--健康告知整理-->
    <Div id="ICDDisDesbID">
      <%@include file="../uw/ICDDisDesbGrid.jsp"%>
    </Div>
    
    <Div id="LCBnfDivID" style="display:none">
      <table id="table9" style="display: ''">
  	    <tr>
  	  	  <td>
  	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
  	  		</td>
  	  		<td class="titleImg"><input class="readonly" name="BnfInfo" value = "受益人信息" readOnly >
  	  		</td>
  	  	</tr>
  	  </table>
  	  <table  class= common id="MulBnfGrid" style="display: ''">
  			<tr  class= common>
  				<td text-align: left colSpan=1>
  					<span id="spanBnfGrid" >
  					</span>
  				</td>
  			</tr>
  		</table>
      <Div id= "divPage5" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage5.firstPage();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage5.previousPage();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage5.nextPage();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage5.lastPage();"> 
    	</Div>
    </Div>
    
    <Div  id= "divGrpInputoverButton" style= "display: ''" style="float: right"> 
      <input TYPE=button name="findIssueButton" class=cssButton VALUE="查看外包错误" onclick="return findIssue();">
      <input type =button name="addInsuredButton" class=cssButton value="保  存" onclick="addRecord();" id="addInsuredButtonBottomID" style= "display: 'none'">
      <input type =button class=cssButton value="修  改" onclick="modifyRecord();" id="modifyInsuredButtonBottomID" style= "display: 'none'">
      <input type =button name="delInsuredButton" class=cssButton value="删  除" onclick="deleteRecord();" id="delInsuredButtonBottomID" style= "display: 'none'">
      
      <INPUT VALUE="费率比例录入" class=cssButton TYPE=button onclick="changRate();">
      <INPUT class=cssButton id="riskbutton1" VALUE="返回上一页" TYPE=button onclick="returnparent();">&nbsp;&nbsp;&nbsp;&nbsp;
      <INPUT class=cssButton id="inputConfirmButtonID" name=lrwb VALUE="录入完毕" TYPE=button onclick="inputConfirm(1);">
      <INPUT class=cssButton VALUE="录音录像调阅"  TYPE=button onclick="getAudioAndVideo();" id="AudioAndVideo" style="display: none">
	  <INPUT  type= "hidden" class= Common name=VideoFlag>
      <INPUT class=cssButton id="approveConfirmButtonID" VALUE="复核完毕" TYPE=button onclick="inputConfirm(2);" style="display: none">
    </Div>
    
    <input type=hidden id="" name="autoMoveFlag">
    <input type=hidden id="" name="autoMoveValue"> 
    <input type=hidden id="" name="pagename" value="">    
  	<input type=hidden id="WorkFlowFlag" name="WorkFlowFlag">
  	<INPUT type= "hidden" class= Common name= MissionID value= ""><!-- 工作流任务编码 -->
    <INPUT type= "hidden" class= Common name= SubMissionID value= "">
    <INPUT type= "hidden" class= Common name= ProposalGrpContNo value= "">
    <INPUT type= "hidden" class= Common name= AppntNo value= "">
    <INPUT type= "hidden" class= Common name= AppntName value= "">
    <INPUT  type= "hidden" class= Common name= prtn2 value= "<%=request.getParameter("PrtNo")%>">
    <INPUT type= "hidden" class= Common name= SelPolNo value= "">
    <INPUT type= "hidden" class= Common name= SaleChnl value= "">
    <INPUT type= "hidden" class= Common name= ManageCom value= "">
    <INPUT type= "hidden" class= Common name= AgentCode value= "">
    <INPUT type= "hidden" class= Common name= AgentGroup value= "">
    <INPUT type= "hidden" class= Common name= CValiDate value= "">
    <INPUT type= "hidden" class= Common name= AddressNo value= "">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="ContType" name="ContType">
    <input type=hidden id="GrpContNo" name="GrpContNo">
    <input type=hidden id="ContNo" name="ContNo">
    <Input class="common" readonly name=ProposalContNo  type="hidden">
    <input type=hidden name=BQFlag>
    <input type=hidden  name=Resource> 
    <INPUT type= "hidden" name=RiskWrapCol value= ""> <!--套餐责任总列数-->
    <INPUT type= "hidden" name=RiskWrapColFix value= ""> <!--套餐责任固定列总列数-->
    <INPUT type= "hidden" name="SubType" value=""> <!--套餐责任固定列总列数-->
    <INPUT type= "hidden" class= Common name= OccupationType>
  </Form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>