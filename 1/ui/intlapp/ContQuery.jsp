<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>	
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<SCRIPT src="ContQuery.js"></SCRIPT>
<%@include file="ContQueryInit.jsp"%>

<title>打印个人保单 </title>
</head>
<body onload="initForm();" >
	<form action="./ContQueryDownLoad.jsp" method=post name=fm target="fraSubmit">
	<!-- 个人保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr><td class= titleImg align= center>请输入保单查询条件：</td></tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>印刷号码</TD>
				<TD class= input> <Input class= common name=PrtNo verify="印刷号码|int"> </TD>
				<TD class= title>保险合同号</TD>
				<TD class= input> <Input class= common name=ContNo verify="保险合同号|int"> </TD>
				<TD class= title>管理机构</TD>
				<TD class= input><Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true > </TD>
			</TR>
			<TR class= common>
  			<TD class= title>销售渠道</TD>
				<TD class= input><Input class="codeNo"  name=SaleChnl  ondblclick="return showCodeList('salechnlall',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('salechnlall',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true > </TD>			
				<TD class= title>业务员</TD>
				<TD class= input><Input class="codeNo"  name=AgentCode  verify="业务员代码|int" ondblclick="return showCodeList('AgentCode',[this,AgentCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeName],[0,1],null,null,null,1);"><input class=codename name=AgentCodeName readonly=true > </TD>						
				<TD class= title>投保人号</TD>
				<TD class= input> <Input class= common name=AppntNo verify="投保人号|int"> </TD>
			</TR>
			<TR class= common>
  			<TD class= title>被保人号码</TD>
				<TD class= input> <Input class= common name=InsuredNo verify="投保人号|int"> </TD>
				<TD class= title>生效日期起</TD>
				<TD class= input><Input class="coolDatePicker" name=StartDate elementtype=nacessary verify="生效日期起|notnull&&date" style="width:130"></TD>
				<TD class= title>生效日期止</TD>
				<TD class= input><Input class="coolDatePicker" name=EndDate elementtype=nacessary verify="投保单填写日期|notnull&&date" style="width:130"></TD>
			</TR>
		</table>
		<INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="queryCont();">
		<INPUT VALUE="下  载" class="cssButton" TYPE=button onclick="downloadCont();">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<Div id= "divLCPol1" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanContGrid" ></span>
					</td>
				</tr>
			</table>
			<Div id= "divPage2" align="center" style= "display: 'none' ">
  			<INPUT VALUE="首 页" class="cssButton" TYPE=button onclick="turnPage2.firstPage();">
  			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="turnPage2.previousPage();">
  			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="turnPage2.nextPage();">
  			<INPUT VALUE="尾 页" class="cssButton" TYPE=button onclick="turnPage2.lastPage();">
  	  </Div>
		</div>
		<Input type=hidden name=Sql>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
