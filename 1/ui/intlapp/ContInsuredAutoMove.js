//程序名称：AutoMove.js
//程序功能：外包录入随动
//创建日期：2007-11-5
//创建人  ：xiaoxin
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var k = 0;
var turnPage = new turnPageClass();
var cflag = "";
var canReplyFlag = false;

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
 
/**
 * 为每个界面录入控件增加相应随动的事件，在ProposalInput.js中调用 
 **/
function setFocus() {
  for (var elementsNum=0; elementsNum<window.document.forms[0].elements.length; elementsNum++) {
  	window.document.forms[0].elements[elementsNum].onfocus = goToArea;        		
  } 
}


/** 
 * 隐藏红框
 **/
function hiddenPosition() {
  top.fraPic.spanPosition.style.display = "none";
}


/** 
 * 显示红框，将控件框起来 
 **/
function showPosition(l, t, w, h) {
  //alert(l + " " + t + " " + w + " " + h);
  top.fraPic.spanPosition.style.display = "";
  top.fraPic.spanPosition.style.left = l;
  top.fraPic.spanPosition.style.top = t;
  top.fraPic.Rect.width = w;
  top.fraPic.Rect.height = h;
}

/*** 随动 ***/
function goToArea() {
	var goToLock = true;
  var objName = this.name;
  var hx = 0;
  var hy = 0;
  try { hiddenPosition(); } catch(e) {}


	//被保人信息

	try { if (objName == "RelationToAppnt") { goToPic(0); top.fraPic.scrollTo(0, 389); showPosition(636+hx, 474+hy, 379, 35); } } catch(e) {} 
	try { if (objName == "Name") { goToPic(0); top.fraPic.scrollTo(0, 404); showPosition(325+hx, 501+hy, 260, 35); } } catch(e) {} 
	try { if (objName == "IDType") { goToPic(0); top.fraPic.scrollTo(0, 434); showPosition(324+hx, 527+hy, 264, 35); } } catch(e) {} 
	try { if (objName == "IDNo") { goToPic(0); top.fraPic.scrollTo(0, 449); showPosition(626+hx, 527+hy, 572, 36); } } catch(e) {} 
	try { if (objName == "Sex") { goToPic(0); top.fraPic.scrollTo(0, 255); showPosition(763+hx, 489+hy, 134, 28); } } catch(e) {} 
	try { if (objName == "Marriage") { goToPic(0); top.fraPic.scrollTo(0, 270); showPosition(892+hx, 485+hy, 269, 33); } } catch(e) {} 
    try { if (objName == "NativePlace") { goToPic(0); top.fraPic.scrollTo(0, 285); showPosition(933+hx, 516+hy, 230, 28); } } catch(e) {} 
    try { if (objName == "Salary") { goToPic(0); top.fraPic.scrollTo(0, 300); showPosition(1012+hx, 537+hy, 149, 32); } } catch(e) {}
	try { if (objName == "Birthday") { goToPic(0); top.fraPic.scrollTo(0, 419); showPosition(627+hx, 500+hy, 326, 36); } } catch(e) {} 
	try { if (objName == "OccupationCode") { goToPic(0); top.fraPic.scrollTo(0, 404); showPosition(860+hx, 553+hy, 191, 37); } } catch(e) {} 
	try { if (objName == "GrpName") { goToPic(0); top.fraPic.scrollTo(0, 419); showPosition(323+hx, 554+hy, 269, 35); } } catch(e) {} 
	try { if (objName == "Position") { goToPic(0); top.fraPic.scrollTo(0, 449); showPosition(323+hx, 579+hy, 676, 37); } } catch(e) {} 
	try { if (objName == "PostalAddress") { goToPic(0); top.fraPic.scrollTo(0, 413); showPosition(322+hx, 579+hy, 682, 38); } } catch(e) {} 
	try { if (objName == "ZipCode") { goToPic(0); top.fraPic.scrollTo(0, 464); showPosition(1009+hx, 579+hy, 184, 39); } } catch(e) {} 
	try { if (objName == "Phone") { goToPic(0); top.fraPic.scrollTo(0, 413); showPosition(322+hx, 606+hy, 259, 37); } } catch(e) {} 
	try { if (objName == "Mobile") { goToPic(0); top.fraPic.scrollTo(0, 413); showPosition(627+hx, 605+hy, 232, 38); } } catch(e) {} 
	try { if (objName == "EMail") { goToPic(0); top.fraPic.scrollTo(0, 413); showPosition(921+hx, 605+hy, 264, 39); } } catch(e) {} 

	
	try { if (objName == "BnfInfo") { goToPic(0); top.fraPic.scrollTo(0, 534); showPosition(251+hx, 656+hy, 956, 100); } } catch(e) {} 
	try { if (objName == "PolInfo") { goToPic(0); top.fraPic.scrollTo(0, 957); showPosition(247+hx, 1040+hy, 960, 205); } } catch(e) {} 
	
}



