<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContSave.jsp
//程序功能：
//创建日期：2007-6-18 13:51
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr="";
	String Content = "";
	String tAction = "";
	String tOperate = "";

	//输入参数
	VData tVData = new VData();
	LCContSchema tLCContSchema   = new LCContSchema();
  LCAppntSchema tLCAppntSchema = new LCAppntSchema();
	LCAddressSchema tLCAddressSchema = new LCAddressSchema();
  LCAccountSchema tLCAccountSchema = new LCAccountSchema();

	GlobalInput tG = (GlobalInput)session.getValue("GI");


	tAction = request.getParameter( "fmAction" );
	if( tAction.equals( "DELETE" ))
	{
    tLCContSchema.setContNo(request.getParameter("ContNo"));
    tLCContSchema.setManageCom(request.getParameter("ManageCom"));	    
	}
	else
  {        
  	LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
    tLDSysTraceSchema.setPolNo(request.getParameter("PrtNo"));
    tLDSysTraceSchema.setCreatePos("承保录单");
    tLDSysTraceSchema.setPolState("1002");
    LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
    inLDSysTraceSet.add(tLDSysTraceSchema);
    VData VData3 = new VData();
    VData3.add(tG);
    VData3.add(inLDSysTraceSet);
    
    LockTableUI LockTableUI1 = new LockTableUI();
    if (!LockTableUI1.submitData(VData3, "INSERT")) 
    {
      VData rVData = LockTableUI1.getResult();
      System.out.println("LockTable Failed! " + (String)rVData.get(0));
    }
    else 
    {
      System.out.println("LockTable Succed!");
    }
    
    //处理投保人
    tLCAppntSchema.setGrpContNo(request.getParameter("GrpContNo"));
    tLCAppntSchema.setContNo(request.getParameter("ContNo"));
    tLCAppntSchema.setPrtNo(request.getParameter("PrtNo"));
    tLCAppntSchema.setAppntNo(request.getParameter("AppntNo"));
    tLCAppntSchema.setAppntGrade(request.getParameter("AppntGrade"));
    tLCAppntSchema.setAddressNo(request.getParameter("AddressNo"));
    tLCAppntSchema.setNativePlace(request.getParameter("AppntNativePlace"));
    tLCAppntSchema.setNationality(request.getParameter("Nationality"));
    tLCAppntSchema.setRgtAddress(request.getParameter("RgtAddress"));
    tLCAppntSchema.setMarriage(request.getParameter("AppntMarriage"));
    tLCAppntSchema.setMarriageDate(request.getParameter("MarriageDate"));
    tLCAppntSchema.setHealth(request.getParameter("Health"));
    tLCAppntSchema.setStature(request.getParameter("Stature"));
    tLCAppntSchema.setAvoirdupois(request.getParameter("Avoirdupois"));
    tLCAppntSchema.setDegree(request.getParameter("Degree"));
    tLCAppntSchema.setCreditGrade(request.getParameter("CreditGrade"));
    tLCAppntSchema.setBankCode(request.getParameter("BankCode"));
    tLCAppntSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLCAppntSchema.setAccName(request.getParameter("AccName"));
    tLCAppntSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
    tLCAppntSchema.setStartWorkDate(request.getParameter("StartWorkDate"));
    tLCAppntSchema.setPosition(request.getParameter("Position"));
    tLCAppntSchema.setSalary(request.getParameter("AppntSalary"));
    tLCAppntSchema.setOccupationType(request.getParameter("OccupationType"));
    tLCAppntSchema.setOccupationCode(request.getParameter("OccupationCode"));
    tLCAppntSchema.setWorkType(request.getParameter("WorkType"));
    tLCAppntSchema.setPluralityType(request.getParameter("PluralityType"));
    tLCAppntSchema.setSmokeFlag(request.getParameter("SmokeFlag"));
    tLCAppntSchema.setOperator(request.getParameter("Operator"));
    tLCAppntSchema.setManageCom(request.getParameter("ManageCom"));
    tLCAppntSchema.setMakeDate(request.getParameter("AppntMakeDate"));
    tLCAppntSchema.setMakeTime(request.getParameter("AppntMakeTime"));
    tLCAppntSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLCAppntSchema.setModifyTime(request.getParameter("ModifyTime"));
    tLCAppntSchema.setOthIDType(request.getParameter("OthIDType"));
    tLCAppntSchema.setOthIDNo(request.getParameter("OthIDNo"));
    tLCAppntSchema.setEnglishName(request.getParameter("EnglishName"));
    tLCAppntSchema.setAppntName(request.getParameter("AppntName"));
    tLCAppntSchema.setAppntSex(request.getParameter("AppntSex"));
    tLCAppntSchema.setAppntBirthday(request.getParameter("AppntBirthday"));
    tLCAppntSchema.setIDStartDate(request.getParameter("IDStartDate"));
    tLCAppntSchema.setIDEndDate(request.getParameter("IDEndDate"));
    tLCAppntSchema.setIDType(request.getParameter("AppntIDType"));
    tLCAppntSchema.setIDNo(request.getParameter("AppntIDNo"));
    tLCAppntSchema.setNativeCity(request.getParameter("AppntNativeCity"));
    //modify by zxs
    tLCAppntSchema.setAuthorization(request.getParameter("AppntAuth"));
    System.out.println("AppntAuth="+request.getParameter("AppntAuth"));
   
    
    tLCContSchema.setGrpContNo(request.getParameter("GrpContNo"));
    tLCContSchema.setContNo(request.getParameter("ContNo"));
    tLCContSchema.setProposalContNo(request.getParameter("ProposalContNo"));
    tLCContSchema.setPrtNo(request.getParameter("PrtNo"));
    tLCContSchema.setContType("1");
    tLCContSchema.setFamilyType(request.getParameter("FamilyType"));
    tLCContSchema.setFamilyID(request.getParameter("FamilyID"));
    tLCContSchema.setPolType("0");
    tLCContSchema.setCardFlag(request.getParameter("CardFlag"));
    tLCContSchema.setManageCom(request.getParameter("ManageCom"));
    tLCContSchema.setExecuteCom(request.getParameter("ExecuteCom"));
    String agentCom=request.getParameter("AgentCom");
		if(StrTool.cTrim(agentCom).equals("")){
    tLCContSchema.setAgentCom(request.getParameter("AgentCom1"));
  	}else{
  	  tLCContSchema.setAgentCom(request.getParameter("AgentCom"));
  	}
  	if("null".equals(tLCContSchema.getAgentCom()))
  	{
  	  tLCContSchema.setAgentCom("");
  	}
    tLCContSchema.setAgentCode(request.getParameter("AgentCode"));
    if(tLCContSchema.getAgentCode() == "")
    {
      tLCContSchema.setAgentCode(request.getParameter("AgentCodeG"));
    }
    
    tLCContSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLCContSchema.setAgentCode1(request.getParameter("AgentCode1"));
    tLCContSchema.setAgentType(request.getParameter("AgentType"));
    tLCContSchema.setSaleChnl(request.getParameter("SaleChnl"));
    tLCContSchema.setHandler(request.getParameter("Handler"));
    tLCContSchema.setPassword(request.getParameter("Password"));
    tLCContSchema.setAppntNo(request.getParameter("AppntNo"));
    tLCContSchema.setAppntName(request.getParameter("AppntName"));
    tLCContSchema.setAppntSex(request.getParameter("AppntSex"));
    tLCContSchema.setAppntBirthday(request.getParameter("AppntBirthday"));
    tLCContSchema.setAppntIDType(request.getParameter("AppntIDType"));
    tLCContSchema.setAppntIDNo(request.getParameter("AppntIDNo"));
    tLCContSchema.setAppntNo(request.getParameter("InsuredNo"));
    tLCContSchema.setAppntName(request.getParameter("InsuredName"));
    tLCContSchema.setAppntSex(request.getParameter("InsuredSex"));
    tLCContSchema.setAppntBirthday(request.getParameter("InsuredBirthday"));
    tLCContSchema.setAppntIDType(request.getParameter("InsuredIDType"));
    tLCContSchema.setAppntNo(request.getParameter("InsuredIDNo"));
    tLCContSchema.setPayIntv(request.getParameter("PayIntv"));
    tLCContSchema.setPayMode(request.getParameter("PayMode"));
    tLCContSchema.setPayLocation(request.getParameter("PayLocation"));
    tLCContSchema.setDisputedFlag(request.getParameter("DisputedFlag"));
    tLCContSchema.setOutPayFlag(request.getParameter("OutPayFlag"));
    tLCContSchema.setGetPolMode(request.getParameter("GetPolMode"));
    tLCContSchema.setSignCom(request.getParameter("SignCom"));
    tLCContSchema.setSignDate(request.getParameter("SignDate"));
    tLCContSchema.setSignTime(request.getParameter("SignTime"));
    tLCContSchema.setConsignNo(request.getParameter("ConsignNo"));
    tLCContSchema.setBankCode(request.getParameter("BankCode"));
    tLCContSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLCContSchema.setAccName(request.getParameter("AccName"));
    tLCContSchema.setPrintCount(request.getParameter("PrintCount"));
    tLCContSchema.setLostTimes(request.getParameter("LostTimes"));
    tLCContSchema.setLang(request.getParameter("Lang"));
    tLCContSchema.setCurrency(request.getParameter("Currency"));
    tLCContSchema.setRemark(request.getParameter("Remark"));
    tLCContSchema.setPeoples(request.getParameter("Peoples"));
    tLCContSchema.setMult(request.getParameter("Mult"));
    tLCContSchema.setPrem(request.getParameter("Prem"));
    tLCContSchema.setAmnt(request.getParameter("Amnt"));
    tLCContSchema.setSumPrem(request.getParameter("SumPrem"));
    tLCContSchema.setDif(request.getParameter("Dif"));
    tLCContSchema.setPaytoDate(request.getParameter("PaytoDate"));
    tLCContSchema.setFirstPayDate(request.getParameter("FirstPayDate"));
    //tLCContSchema.setCValiDate(request.getParameter("CValiDate"));
    tLCContSchema.setCValiDate(request.getParameter("PolApplyDate"));
    //tLCContSchema.setInputOperator(request.getParameter("InputOperator"));
    //tLCContSchema.setInputDate(request.getParameter("InputDate"));
    //tLCContSchema.setInputTime(request.getParameter("InputTime"));
    tLCContSchema.setApproveFlag(request.getParameter("ApproveFlag"));
    tLCContSchema.setApproveCode(request.getParameter("ApproveCode"));
    tLCContSchema.setApproveDate(request.getParameter("ApproveDate"));
    tLCContSchema.setApproveTime(request.getParameter("ApproveTime"));
    tLCContSchema.setUWFlag(request.getParameter("UWFlag"));
    tLCContSchema.setUWOperator(request.getParameter("UWOperator"));
    tLCContSchema.setUWDate(request.getParameter("UWDate"));
    tLCContSchema.setUWTime(request.getParameter("UWTime"));
    tLCContSchema.setAppFlag(request.getParameter("AppFlag"));
    tLCContSchema.setPolApplyDate(request.getParameter("PolApplyDate"));
    tLCContSchema.setGetPolDate(request.getParameter("GetPolDate"));
    tLCContSchema.setGetPolTime(request.getParameter("GetPolTime"));
    tLCContSchema.setCustomGetPolDate(request.getParameter("CustomGetPolDate"));
    tLCContSchema.setState(request.getParameter("State"));
    tLCContSchema.setOperator(request.getParameter("Operator"));
    tLCContSchema.setMakeDate(request.getParameter("MakeDate"));
    tLCContSchema.setMakeTime(request.getParameter("MakeTime"));
    tLCContSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLCContSchema.setModifyTime(request.getParameter("ModifyTime"));
    tLCContSchema.setFirstTrialOperator(request.getParameter("FirstTrialOperator"));
    tLCContSchema.setFirstTrialDate(request.getParameter("FirstTrialDate"));
    tLCContSchema.setFirstTrialTime(request.getParameter("FirstTrialTime"));
    tLCContSchema.setReceiveOperator(request.getParameter("ReceiveOperator"));
    tLCContSchema.setReceiveDate(request.getParameter("ReceiveDate"));
    tLCContSchema.setReceiveTime(request.getParameter("ReceiveTime"));
    tLCContSchema.setTempFeeNo(request.getParameter("TempFeeNo"));
    tLCContSchema.setProposalType(request.getParameter("ProposalType"));
    tLCContSchema.setSaleChnlDetail(request.getParameter("SaleChnlDetail"));
    tLCContSchema.setContPrintLoFlag(request.getParameter("ContPrintLoFlag"));
    tLCContSchema.setContPremFeeNo(request.getParameter("ContPremFeeNo"));
    tLCContSchema.setCustomerReceiptNo(request.getParameter("CustomerReceiptNo"));
    tLCContSchema.setCInValiDate(request.getParameter("CInValiDate"));
    tLCContSchema.setCopys(request.getParameter("Copys"));
    tLCContSchema.setDegreeType(request.getParameter("DegreeType"));
    tLCContSchema.setIntlFlag(request.getParameter("IntlFlag"));
    tLCContSchema.setUWConfirmNo(request.getParameter("UWConfirmNo"));
    tLCContSchema.setPayerType(request.getParameter("PayerType"));
    tLCContSchema.setGrpAgentCom(request.getParameter("GrpAgentCom"));
    tLCContSchema.setGrpAgentCode(request.getParameter("GrpAgentCode"));
    tLCContSchema.setGrpAgentName(request.getParameter("GrpAgentName"));
    tLCContSchema.setGrpAgentIDNo(request.getParameter("GrpAgentIDNo"));
    
    tLCContSchema.setCrs_SaleChnl(request.getParameter("Crs_SaleChnl"));
    tLCContSchema.setCrs_BussType(request.getParameter("Crs_BussType"));
    tLCContSchema.setAgentSaleCode(request.getParameter("AgentSaleCode"));
    
    // 续保提醒标志
    tLCContSchema.setDueFeeMsgFlag(request.getParameter("DueFeeMsgFlag"));
    
    // 缴费模式
    tLCContSchema.setPayMethod(request.getParameter("PayMethod"));
    
    tLCAddressSchema.setCustomerNo(request.getParameter("AppntNo"));
    tLCAddressSchema.setAddressNo(request.getParameter("AddressNo"));
    tLCAddressSchema.setPostalAddress(request.getParameter("PostalAddress"));
    tLCAddressSchema.setZipCode(request.getParameter("ZipCode"));
    tLCAddressSchema.setPhone(request.getParameter("Phone"));
    tLCAddressSchema.setMobile(request.getParameter("Mobile"));
    tLCAddressSchema.setCompanyPhone(request.getParameter("CompanyPhone"));
    tLCAddressSchema.setEMail(request.getParameter("EMail"));
    tLCAddressSchema.setFax(request.getParameter("Fax"));  
    tLCAddressSchema.setPostalCity(request.getParameter("City"));
    tLCAddressSchema.setPostalCommunity(request.getParameter("PostalCommunity"));
    tLCAddressSchema.setPostalCounty(request.getParameter("County"));
    tLCAddressSchema.setPostalProvince(request.getParameter("Province"));
    tLCAddressSchema.setPostalStreet(request.getParameter("PostalStreet"));
    tLCAddressSchema.setGrpName(request.getParameter("GrpName"));
                                        
	} // end of else     
	
	String tAssistSaleChnl = request.getParameter("AssistSaleChnl");
  	String tAssistAgentCode = request.getParameter("AssistAgentCode");
	LCExtendSchema tLCExtendSchema = new LCExtendSchema();
	tLCExtendSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCExtendSchema.setAssistSalechnl(tAssistSaleChnl);
	tLCExtendSchema.setAssistAgentCode(tAssistAgentCode);
	tVData.add(tLCExtendSchema);
	
	//#1855 客户类型
	LCContSubSchema tLCContSubSchema = new LCContSubSchema();
	tLCContSubSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCContSubSchema.setCountyType(request.getParameter("appnt_CountyType"));
	tLCContSubSchema.setFamilySalary(request.getParameter("FamilySalary"));
	tVData.add(tLCContSubSchema);
	                                                              
  System.out.println("end setSchema:");                                             
	// 准备传输数据 VData                                                           
	tVData.add( tLCContSchema );  
	tVData.add( tLCAddressSchema );   
	tVData.add( tLCAppntSchema );   
	tVData.add( tLCAccountSchema );
	tVData.add( tG );                                                               
	                                                                                
	//传递非SCHEMA信息                                                              
  TransferData tTransferData = new TransferData();                                  
  String SavePolType="";                                                            
  String BQFlag=request.getParameter("BQFlag");                                     
  if(BQFlag==null) SavePolType="0";                                                 
  else if(BQFlag.equals("")) SavePolType="0";                                       
    else  SavePolType=BQFlag;                                                       
                                                                                    
                                                                                    
  tTransferData.setNameAndValue("SavePolType",SavePolType); //保全保存标记，默认为0，标识非保全
  tTransferData.setNameAndValue("GrpNo",request.getParameter("AppntGrpNo"));
  tTransferData.setNameAndValue("GrpName",request.getParameter("GrpName"));
  tTransferData.setNameAndValue("CustomerType",request.getParameter("AppntType"));
  System.out.println("SavePolType，BQ is 2，other is 0 : " + request.getParameter("BQFlag"));
  //modify by zxs
   tTransferData.setNameAndValue("Authorization",request.getParameter("AppntAuth"));
  tVData.addElement(tTransferData);                                                 
                                                                                    
	if( tAction.equals( "INSERT" )) tOperate = "INSERT||CONT";                  
	if( tAction.equals( "UPDATE" )) tOperate = "UPDATE||CONT";                  
	if( tAction.equals( "DELETE" )) tOperate = "DELETE||CONT";                  

  System.out.println("\n\n\n" + tOperate);
  
	ContUI tContUI = new ContUI();         
	System.out.println("before submit");                             
	if( tContUI.submitData( tVData, tOperate ) == false )                       
	{                                                                               
		Content = " 保存失败，原因是: " + tContUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";

		tVData.clear();
		tVData = tContUI.getResult();

		// 显示
		// 保单信息
		LCContSchema mLCContSchema = new LCContSchema(); 
    LCAddressSchema mLCAddressSchema = new LCAddressSchema();		
		mLCContSchema.setSchema(( LCContSchema )tVData.getObjectByObjectName( "LCContSchema", 0 ));
		mLCAddressSchema.setSchema(( LCAddressSchema )tVData.getObjectByObjectName( "LCAddressSchema", 0 ));		
	}

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

