<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="GBK"/>

<xsl:template match="/">

<xsl:for-each select="POLDATA/ROW">

  <!-- 所属省份 -->
  <xsl:value-of select="DEPT_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 个团标志 -->
  <xsl:value-of select="GP_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 保单号 -->  
  <xsl:value-of select="POLNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 分单号 -->
  <xsl:value-of select="CERTNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 险种序号 --> 
  <xsl:value-of select="BRNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 险种代码 --> 
  <xsl:value-of select="PLAN_CODE"/>
  <xsl:value-of select="'|'"/>
  <!-- 业务来源类别 --> 
  <xsl:value-of select="BUSI_SRC_TYPE"/> 
  <xsl:value-of select="'|'"/>
  <!-- 缴费人数 -->
  <xsl:value-of select="PREM_PSNS"/>
  <xsl:value-of select="'|'"/>
  <!-- 保险期限（月） -->
  <xsl:value-of select="PERIOD"/>
  <xsl:value-of select="'|'"/>
  <!-- 缴费期限（月） -->
  <xsl:value-of select="PREM_TERM"/>
  <xsl:value-of select="'|'"/>
  <!-- 份数 -->
  <xsl:value-of select="UNITS"/>
  <xsl:value-of select="'|'"/>
  <!-- 保额 -->
  <xsl:value-of select="SUM_INS"/>
  <xsl:value-of select="'|'"/>
  <!-- 保费币种 -->
  <xsl:value-of select="CURNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 实收保费合计折合人民币 -->
  <xsl:value-of select="TOT_PREM_CNVT"/>
  <xsl:value-of select="'|'"/>
  <!-- 期缴次数 -->
  <xsl:value-of select="PREM_TIMES"/>
  <xsl:value-of select="'|'"/>
  <!-- 保单年度 -->
  <xsl:value-of select="POL_YR"/>
  <xsl:value-of select="'|'"/>
  <!-- 缴别 -->
  <xsl:value-of select="PREM_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 中介机构代码 -->
  <xsl:value-of select="AGT_CODE"/>
  <xsl:value-of select="'|'"/>
  <!-- 经纪代理手续费比例 -->
  <xsl:value-of select="PROC_RATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 业务员代码 -->
  <xsl:value-of select="AGENTNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 直接佣金比例 -->
  <xsl:value-of select="COM_RATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 收付类型 -->
  <xsl:value-of select="AMT_TYPE"/>
  <xsl:value-of select="'|'"/>  
  <!-- 应缴日期 -->
  <xsl:value-of select="PAY_TO_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 交费日期 -->
  <xsl:value-of select="PAYMENT_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 到帐日期 -->
  <xsl:value-of select="GAINED_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 缴至日期 -->
  <xsl:value-of select="POL_PAY_TO_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 缴清日期 -->
  <xsl:value-of select="POL_PAID_UP_DATE"/>
  <xsl:value-of select="'|'"/>

  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>