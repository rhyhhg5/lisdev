<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="GBK"/>

<xsl:template match="/">

<xsl:for-each select="POLDATA/ROW">

  <!-- 所属省份-->  
  <xsl:value-of select="DEPT_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 个团标志 -->
  <xsl:value-of select="GP_TYPE"/>
  <xsl:value-of select="'|'"/>  
  <!-- 保单号 -->
  <xsl:value-of select="POLNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 分单号 -->
  <xsl:value-of select="CERTNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 清单号--> 
  <xsl:value-of select="LISTNO"/> 
  <xsl:value-of select="'|'"/>
  <!-- 险种序号 --> 
  <xsl:value-of select="BRNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 险种代码 --> 
  <xsl:value-of select="PLAN_CODE"/>
  <xsl:value-of select="'|'"/>
  <!-- 应付日期 --> 
  <xsl:value-of select="ACCRU_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 收付类型 --> 
  <xsl:value-of select="AMT_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 币种 --> 
  <xsl:value-of select="CURNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 应付金额折合人民币 --> 
  <xsl:value-of select="ACCRU_AMT"/>
  <xsl:value-of select="'|'"/>
  <!-- 到帐日期（实付日期） -->
  <xsl:value-of select="GAINED_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 领取人姓名 -->
  <xsl:value-of select="PAYEENAME"/>
  <xsl:value-of select="'|'"/>
 
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>