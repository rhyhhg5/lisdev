<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="GBK"/>

<xsl:template match="/">

<xsl:for-each select="POLDATA/ROW">

  <!-- 案件号 -->  
  <xsl:value-of select="CASENO"/>
  <xsl:value-of select="'|'"/> 
  <!-- 个团标志 -->
  <xsl:value-of select="GP_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 保单号 -->  
  <xsl:value-of select="POLNO"/>
  <xsl:value-of select="'|'"/> 
  <!-- 投保人名称 -->
  <xsl:value-of select="APP_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 申请人姓名 -->
  <xsl:value-of select="REQU_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 报案日期 -->
  <xsl:value-of select="DOCU_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 立案日期 -->
  <xsl:value-of select="CASE_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 结案日期 -->
  <xsl:value-of select="END_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 估损金额 -->
  <xsl:value-of select="RECKON_LOSS"/>
  <xsl:value-of select="'|'"/>  
  <!-- 核赔给付额 -->
  <xsl:value-of select="EXAM_AMT"/>
  <xsl:value-of select="'|'"/>  
  <!-- 查勘费用 --> 
  <xsl:value-of select="CHECK_FEE"/> 
  <xsl:value-of select="'|'"/>
  <!-- 拒赔金额 -->
  <xsl:value-of select="REJE_AMT"/>
  <xsl:value-of select="'|'"/>
  <!-- 通融给付原因 -->
  <xsl:value-of select="ACCOMMODATE_CAUSE"/>
  <xsl:value-of select="'|'"/>
 
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>