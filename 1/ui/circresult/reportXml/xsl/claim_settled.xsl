<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="GBK"/>

<xsl:template match="/">

<xsl:for-each select="POLDATA/ROW">

  <!-- 所属省份 -->
  <xsl:value-of select="DEPT_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 案件号 -->  
  <xsl:value-of select="CASENO"/>
  <xsl:value-of select="'|'"/> 
  <!-- 个团标志 -->
  <xsl:value-of select="GP_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 保单号 -->  
  <xsl:value-of select="POLNO"/>
  <xsl:value-of select="'|'"/> 
  <!-- 分单号 -->
  <xsl:value-of select="CERTNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 清单号--> 
  <xsl:value-of select="LISTNO"/> 
  <xsl:value-of select="'|'"/>
  <!-- 险种序号 --> 
  <xsl:value-of select="BRNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 险种代码 --> 
  <xsl:value-of select="PLAN_CODE"/>
  <xsl:value-of select="'|'"/>
  <!-- 业务来源类别 --> 
  <xsl:value-of select="BUSI_SRC_TYPE"/> 
  <xsl:value-of select="'|'"/>
  <!-- 保险期限（月） -->
  <xsl:value-of select="PERIOD"/>
  <xsl:value-of select="'|'"/>  
  <!-- 缴费期限（月） -->
  <xsl:value-of select="PREM_TERM"/>
  <xsl:value-of select="'|'"/>
  <!-- 份数 -->
  <xsl:value-of select="UNITS"/>
  <xsl:value-of select="'|'"/>
  <!-- 保单年度 -->
  <xsl:value-of select="POL_YR"/>
  <xsl:value-of select="'|'"/>
  <!-- 缴别 -->
  <xsl:value-of select="PREM_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 赔付次数 -->
  <xsl:value-of select="PAYSEQ"/>
  <xsl:value-of select="'|'"/>
  <!-- 被保人名称 -->
  <xsl:value-of select="INSNAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 领取人姓名 -->
  <xsl:value-of select="PAID_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 收付类型 -->
  <xsl:value-of select="AMT_TYPE"/>
  <xsl:value-of select="'|'"/>  
  <!-- 赔付金额折合人民币 -->
  <xsl:value-of select="PAID_AMT_CNVT"/>
  <xsl:value-of select="'|'"/>
  <!-- 查勘费用 --> 
  <xsl:value-of select="CHECK_FEE"/> 
  <xsl:value-of select="'|'"/>
  <!-- 币种 -->
  <xsl:value-of select="CURNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 结案日期 -->
  <xsl:value-of select="END_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 付日期（实付日期） -->
  <xsl:value-of select="GAINED_DATE"/>
  <xsl:value-of select="'|'"/>    
  <!-- 是否为豫赔 -->
  <xsl:value-of select="IS_PREPAY"/>
  <xsl:value-of select="'|'"/>  
  <!-- 摊回比例分保赔款 -->
  <xsl:value-of select="N_SHARE_SUM"/>
  <xsl:value-of select="'|'"/>    
  <!-- 摊回非比例分保赔款 -->
  <xsl:value-of select="N_SHARE_SUM_N"/>
  <xsl:value-of select="'|'"/>  
 
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>