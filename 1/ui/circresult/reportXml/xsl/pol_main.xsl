<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="GBK"/>

<xsl:template match="/">

<xsl:for-each select="POLDATA/ROW">

  <!-- 保单号 -->  
  <xsl:value-of select="POLNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 分单号 -->
  <xsl:value-of select="CERTNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 所属省份 -->
  <xsl:value-of select="DEPT_NAME"/>
  <xsl:value-of select="'|'"/>  
  <!-- 个团标志 -->
  <xsl:value-of select="GP_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 业务来源类 --> 
  <xsl:value-of select="BUSI_SRC_TYPE"/> 
  <xsl:value-of select="'|'"/>
  <!-- 投保人名称 -->
  <xsl:value-of select="APP_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 被保人名称 -->
  <xsl:value-of select="INSNAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 投保日期 -->
  <xsl:value-of select="APP_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 生效日期 -->
  <xsl:value-of select="EFF_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 满期日期 -->
  <xsl:value-of select="MATU_DATE"/>
  <xsl:value-of select="'|'"/>
  <!-- 缴别 -->
  <xsl:value-of select="PREM_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 缴费联系人姓名 -->
  <xsl:value-of select="CONTA_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 联系电话 -->
  <xsl:value-of select="CONTA_TEL"/>
  <xsl:value-of select="'|'"/>
  <!-- 中介机构代码 -->
  <xsl:value-of select="AGT_CODE"/>
  <xsl:value-of select="'|'"/>
  <!-- 业务员代码 -->
  <xsl:value-of select="AGENTNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 临分标志 -->
  <xsl:value-of select="FACUL_FLAG"/>
  <xsl:value-of select="'|'"/>
  <!-- 分保标志 -->
  <xsl:value-of select="CEDE_FLAG"/>
  <xsl:value-of select="'|'"/>

  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>