<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%> 
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<html>
<head>
<script>
	function initDate(){
   fm.SignDateS.value="<%=afterdate%>";
   fm.SignDateE.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
   fm.ConfirmDateS.value="<%=afterdate%>";
   fm.ConfirmDateE.value="<%=CurrentDate%>";
  }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="LCCustomerRCConfirm.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LCCustomerRCConfirmInit.jsp"%>
<title>客户洗钱风险等级</title>
</head>
<body  onload="initForm();initDate();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>客户洗钱风险等级
         </TD>
       </TR>
      </table>
    <table class="common" align="center" id="tbInfo" name="tbInfo">
      <tr class="common">
        <TD  class= title>管理机构</TD>
        <TD  class= input>
           <Input class="codeno" name=ManageCom readonly verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1,180);" ><input class=codename name=ComName></TD>
        <TD  class= title>客户类型</TD>
        <TD  class= input>
           <input class="codeno" CodeData="0|2^0|团体^1|个人" readonly name=CustomerType ondblclick="return showCodeListEx('CustomerType',[this,CustomerTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CustomerType',[this,CustomerTypeName],[0,1]);" ><input class=codename name=CustomerTypeName ></TD> 
        <TD  class= title>分类状态</TD>
        <TD  class= input>
           <input class="codeno" CodeData="0|3^0|未分类^1|初步分类^2|最终分类" readonly name=ClassState ondblclick="return showCodeListEx('ClassState',[this,ClassStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ClassState',[this,ClassStateName],[0,1]);"><input class=codename name=ClassStateName></TD> 
      </tr>
      <tr class="common">
        <TD  class= title>销售部门</TD>
        <TD  class= input>
           <input class="codeno" CodeData="0|3^0|个险^1|团险^2|银代" name=SaleChnl ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName></TD> 
        <td class="title">承保起期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="SignDateS"></td>
        <td class="title">承保止期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="SignDateE"></td>
      </tr>
      <tr class="common">
        <td class="title">客户号</td>
        <td class="input"><input class="common" name="CustomerNo"></td>
        <td class="title">客户名称</td>
        <td class="input"><input class="common" name="CustomerName"></td>
        <td class="title" id="divIDNot">证件号码</td>
        <td class="input" id="divIDNo"><input class="common" name="IDNo"></td>
      </tr>
      <tr class="common" id="divConfirm" style="display:'none'" >
        <td class="title">确认起期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="ConfirmDateS"></td>
        <td class="title">确认止期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="ConfirmDateE"></td>
        <TD  class= title>风险等级</TD>
        <TD  class= input>
           <input class="codeno" CodeData="0|3^0|低^1|中^2|高" name=CustomerGrade ondblclick="return showCodeListEx('CustomerGrade',[this,CustomerGradeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CustomerGrade',[this,CustomerGradeName],[0,1]);"><input class=codename name=CustomerGradeName></TD> 
      </tr>
      <tr class="common">
        <td class="title">数量合计</td>
        <td class="input" col=3>
              <input class="readonly" readonly name="SumCount">
        </td>
      </tr>
	</table>
	  <input value="查  询" type="button" class="cssButton" onclick="searchCustomer();">
    <br>
    
    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCustomerInfo);"></td>
    	<td class= titleImg>客户信息</td></tr>
    </table>

  	<div id="divCustomerInfo" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCustomerInfo"></span></td></tr>
      </table>

      <input VALUE="首  页" TYPE="button" class="cssButton" onclick="turnPage.firstPage();">
      <input VALUE="上一页" TYPE="button" class="cssButton" onclick="turnPage.previousPage();">
      <input VALUE="下一页" TYPE="button" class="cssButton" onclick="turnPage.nextPage();">
      <input VALUE="尾  页" TYPE="button" class="cssButton" onclick="turnPage.lastPage();">
  	</div>
  	<br>
  	<table>
      <tr>
        <td  id="divSubmitForm" >
          	<input value="确  认" type="button" class="cssButton" onclick="submitForm();">
        </td>
        <td>
            <input value="下载清单" type="button" class="cssButton" onclick="printList();">
        </td>
        <td>
            <input value="客户信息查询" type="button" class="cssButton" onclick="CustomerQuery();">
        </td>
      </tr>
    </table>
    <br>
    <div id="divRiskClassTrace" style="display: 'none'">
  	<table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCustomerTrace);"></td>
    	<td class= titleImg>客户等级确认轨迹</td></tr>
    </table>

  	<div id="divCustomerTrace" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCustomerTrace"></span></td></tr>
    </table>
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
    </div>
  	</div>
  	<br><font color = 'red'>提示：如需下载清单，请将统计时间限制在三个月内。</font></br>
  	<br>注：客户洗钱风险等级“分类依据”，请参照《中国人民健康保险股份有限公司客户洗钱风险等级分类管理办法（试行）》第二章第六条、第七条、第八条规定情形填写，仅填写款项序号，如某客户被划分为“中等风险”，分类依据填写为“（一）”，即表示下述“中风险等级客户”对应的第（一）款“公司向监管机关报送的大额交易客户，上述第六条第（四）款情形除外”。</br>
    <br><font size='3'>高风险等级客户：</font></br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（一）客户信息与已知恐怖组织或个人、洗钱、欺诈等组织或个人，外国政要、军事组织高级官员等名单特征相符或与之发生交易；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（二）我国相关司法机关、政府部门印发的黑名单中的客户;</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（三）存在化整为零，逃避大额交易监测的行为或者存在因知悉身份资料将被调查而中止交易的行为的客户；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（四）客户进行与其身份和职业不符且不能清楚说明来源的大额交易；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（五）涉及重大洗钱案件的客户，或者被中国人民银行或其他监管机关确定涉嫌洗钱活动，正在被调查的客户；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（六）利用虚假证件办理业务等故意隐瞒真实身份信息的客户；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（七）公司向监管机关报送的可疑交易的客户；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（八）属于赌场或其他赌博机构、彩票销售行业、与武器有关的行业、娱乐、珠宝古董、典当行等行业法定代表人的客户；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（九）符合《金融机构报告涉嫌恐怖融资的可疑交易管理办法》第八条和第九条等其他重大可疑交易情形的客户； </br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（十）其他应划分为高风险等级的客户。</br>
    <br><font size='3'>中风险等级客户：</font></br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（一）公司向监管机关报送的大额交易客户，上述第六条第（四）款情形除外；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（二）客户提交的身份证件或者身份证明文件已过有效期，没有在合理期限内更新且没有提出合理理由的，或者客户无正当理由拒绝更新客户基本信息的；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（三）来自于反洗钱、反恐怖融资监管薄弱国家（地区）的客户；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（四）购买保单与其经济状况明显不符的客户，且不能合理解释的；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（五）面临破产、解散等状况的企业客户；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（六）其他应划分为中风险等级的客户。</br>
    <br><font size='3'>低风险等级客户：</font></br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（一）各项身份资料和交易记录齐全的客户；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（二）未达到中国人民银行规定进行客户身份识别保险费起点金额或条件的客户；</br>
    <br>&nbsp;&nbsp;&nbsp;&nbsp;（三）其他应划分为低风险等级的客户。</br>

</br>
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
		<input type=hidden name="SearchSQL" value="">
		<input type=hidden name="Operate" value="">
		<input type=hidden name="LoadFlag" value="<%=LoadFlag%>">
		<Div id=DivFileDownload style="display:'none'">
      <A id=fileUrl href=""></A>
    </Div>
  </form>
</body>
</html>
