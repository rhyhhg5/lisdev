<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：LCCustomerRCConfirmInit.jsp
//程序功能：
//创建日期：2003-06-16
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  String LoadFlag = request.getParameter("LoadFlag");
  LoadFlag = LoadFlag==null?"":LoadFlag;
%>
<script language="JavaScript">
var LoadFlag="<%=LoadFlag%>";

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
  	fm.CustomerType.value="1";
  	fm.CustomerTypeName.value="个人";
  	fm.ClassState.value="0";
  	fm.ClassStateName.value="未评定";
  	if (LoadFlag=="C") {
  		divSubmitForm.style.display='none';
  	}
  }
  catch(ex)
  {
    alert("在LCCustomerRCConfirmInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LCCustomerRCConfirmInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
		initCustomerInfo();
		initCustomerTrace();
		//initCardListInfo();
  }
  catch(re)
  {
    alert("LCCustomerRCConfirmInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 个人客户信息列表的初始化
function initCustomerInfo()
{
	var iArray = new Array();
	var CustomerFlag;
  if (fm.CustomerType.value=="0") {
  	CustomerFlag = 3;
  } else {
  	CustomerFlag = 0;
  }
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="客户号";         		//列名
		iArray[1][1]="70px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="客户名称";          		//列名
		iArray[2][1]="120px";            		//列宽
		iArray[2][2]=85;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="国籍";        		  //列名
		iArray[3][1]="70px";            		//列宽
		iArray[3][2]=85;            			  //列最大值
		iArray[3][3]=CustomerFlag;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="性别";          		//列名
		iArray[4][1]="40px";            		//列宽
		iArray[4][2]=100;            		 	  //列最大值
		iArray[4][3]=CustomerFlag;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="出生日期";         		  //列名
		iArray[5][1]="80px";            		//列宽
		iArray[5][2]=100;            			  //列最大值
		iArray[5][3]=CustomerFlag;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[6]=new Array();
		iArray[6][0]="证件号";          		  //列名
		iArray[6][1]="130px";            		//列宽
		iArray[6][2]=60;            			  //列最大值
		iArray[6][3]=CustomerFlag;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="职业类型";          		  //列名
		iArray[7][1]="10px";            		//列宽
		iArray[7][2]=60;            			  //列最大值
		iArray[7][3]=3;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="职业编码";          		  //列名
		iArray[8][1]="30px";            		//列宽
		iArray[8][2]=60;            			  //列最大值
		iArray[8][3]=3;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[9]=new Array();
		iArray[9][0]="职业";          		  //列名
		iArray[9][1]="80px";            		//列宽
		iArray[9][2]=60;            			  //列最大值
		iArray[9][3]=CustomerFlag;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[10]=new Array();
		iArray[10][0]="承保日期";          		  //列名
		iArray[10][1]="80px";            		//列宽
		iArray[10][2]=60;            			  //列最大值
		iArray[10][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[11]=new Array();
		iArray[11][0]="保费";          		  //列名
		iArray[11][1]="80px";            		//列宽
		iArray[11][2]=60;            			  //列最大值
		iArray[11][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[12]=new Array();
		iArray[12][0]="缴费方式";          		  //列名
		iArray[12][1]="80px";            		//列宽
		iArray[12][2]=60;            			  //列最大值
		iArray[12][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[13]=new Array();
		iArray[13][0]="风险等级编码";         		  //列名
		iArray[13][1]="0px";            		//列宽
		iArray[13][2]=100;            			  //列最大值
		iArray[13][3]=3;              			  //是否允许输入,1表示允许，0表示不允许
		iArray[13][21]="customergrade";

		iArray[14]=new Array();
		iArray[14][0]="风险等级";       		  //列名
		iArray[14][1]="60px";            		//列宽
		iArray[14][2]=100;            			  //列最大值
		iArray[14][3]=2;              			  //是否允许输入,1表示允许，0表示不允许
		iArray[14][10]="CustomerGradeC";
		iArray[14][11]="0|^0|低|^1|中|^2|高";
		iArray[14][12]="14|13";
		iArray[14][13]="1|0";

		iArray[15]=new Array();
		iArray[15][0]="分类依据编码";       		  //列名
		iArray[15][1]="0px";            		//列宽
		iArray[15][2]=100;            			  //列最大值
		iArray[15][3]=3;              			  //是否允许输入,1表示允许，0表示不允许
		iArray[15][21]="classreason";
		
		iArray[16]=new Array();
		iArray[16][0]="分类依据";       		  //列名
		iArray[16][1]="60px";            		//列宽
		iArray[16][2]=120;            			  //列最大值
		iArray[16][3]=2;              			  //是否允许输入,1表示允许，0表示不允许
		iArray[16][4]="riskclassreason";
		iArray[16][5]="16|15";
		iArray[16][6]="1|0";
		iArray[16][15]="riskclass";
    iArray[16][17]="13";
    iArray[16][21]="classreasonname";
		
		iArray[17]=new Array();
		iArray[17][0]="分类日期";       		  //列名
		iArray[17][1]="80px";            		//列宽
		iArray[17][2]=100;            			  //列最大值
		iArray[17][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[18]=new Array();
		iArray[18][0]="备注";       		  //列名
		iArray[18][1]="120px";            		//列宽
		iArray[18][2]=100;            			  //列最大值
		iArray[18][3]=1;              			  //是否允许输入,1表示允许，0表示不允许

		CustomerInfo = new MulLineEnter("fm" , "CustomerInfo");
		// 这些属性必须在loadMulLine前
		CustomerInfo.mulLineCount = 10;
		CustomerInfo.displayTitle = 1;
		CustomerInfo.locked = 1;
		CustomerInfo.canSel = 1;
		CustomerInfo.canChk = 1;
    CustomerInfo.hiddenPlus = 1;
    CustomerInfo.hiddenSubtraction = 1;
		CustomerInfo.selBoxEventFuncName = "SelectCustomerInfo";
		CustomerInfo.loadMulLine(iArray);		
	}
	catch(ex)
	{
		alert(ex);
	}
}

function initCustomerTrace()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="客户号";         		//列名
		iArray[1][1]="70px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="客户名称";          		//列名
		iArray[2][1]="120px";            		//列宽
		iArray[2][2]=85;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="承保日期";          		  //列名
		iArray[3][1]="80px";            		//列宽
		iArray[3][2]=60;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="风险等级";       		  //列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            			  //列最大值
		iArray[4][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="分类依据";       		  //列名
		iArray[5][1]="60px";            		//列宽
		iArray[5][2]=100;            			  //列最大值
		iArray[5][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="分类状态";       		  //列名
		iArray[6][1]="80px";            		//列宽
		iArray[6][2]=100;            			  //列最大值
		iArray[6][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="分类人";       		  //列名
		iArray[7][1]="100px";            		//列宽
		iArray[7][2]=100;            			  //列最大值
		iArray[7][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="分类日期";       		  //列名
		iArray[8][1]="80px";            		//列宽
		iArray[8][2]=100;            			  //列最大值
		iArray[8][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[9]=new Array();
		iArray[9][0]="备注";       		  //列名
		iArray[9][1]="120px";            		//列宽
		iArray[9][2]=100;            			  //列最大值
		iArray[9][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
				
		CustomerTrace = new MulLineEnter("fm" , "CustomerTrace");
		// 这些属性必须在loadMulLine前
		CustomerTrace.mulLineCount = 2;
		CustomerTrace.displayTitle = 1;
		CustomerTrace.locked = 1;
    CustomerTrace.hiddenPlus = 1;
    CustomerTrace.hiddenSubtraction = 1;
		CustomerTrace.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>