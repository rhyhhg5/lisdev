//程序名称：filwdownLoad.js
//程序功能：文件下载
//创建日期：2007-11-23
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();
var flag1 = 0;
window.onfocus=myonfocus;

//下拉框选择后执行
function afterCodeSelect(control, Filed)
{	
  if(control == 'filetype')
  {
    fileType1 = fm.fileType.value;
		fm.all("fileDetailType").value = "";
		fm.all("fileDetailTypeName").value = "";
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(content)
{
  window.focus();
	alert(content);
}

//提交后操作,服务器数据返回后执行的操作
function query()
{
	// 初始化表格
	initFileGrid();	
	if(!verifyInput2())
	{
		return false;
	}
	
	// 书写SQL语句
	var strsql = "SELECT FileNo, FileName, FileCode, CodeName('filetype', FileType), "
						+ "(SELECT CodeName FROM LDCode1 WHERE CodeType = 'filetypedetail' and Code = FileType and Code1 = FileDetailType), "
						+ "OtherNo, OtherNoType, FilePath, Discription, ManageCom, Operator, CHAR(MakeDate)||CHAR(MakeTime) "
						+ "FROM LCFileManage WHERE 1 = 1"
						+ getWherePart('FileName','fileName')
						+ getWherePart('FileType','fileType')
						+ getWherePart('FileDetailType','fileDetailType');
	
	turnPage.queryModal(strsql,FileGrid);
}

/**
*文件下载
*/	


function downLoad()
{
  var row = FileGrid.getSelNo();
  if(row == null || row == 0)
  {
    alert("请选择要下载的文件.");
    return false;
  }
  
  var fileName = FileGrid.getRowColData(row - 1, 2);  
  if (fileName == null || fileName == "")
  {
    alert("没有附件,不能进行下载操作！")	
    return false;
  }
  //alert(fileName);

  fm.action = "./fileDownLoadSave.jsp";
  fm.submit();
}


function FileSelect()
{
	var tRow = FileGrid.getSelNo() - 1;
	if(tRow == null || tRow < 0)
	{
		alert("请选择要下载的文件.");
		return false;
	}
	
	var FilePath = ReInsureAuditGrid.getRowColData(row - 1, 7);  
    if (FilePath=="")
    {
      alert("没有附件,不能进行下载操作！")	
      return false;
    }
  
  
	var tResult=FileGrid.getRowData(tRow);
	alert("tResult[0]:" + tResult[0]);
	alert("tResult[5]:" + tResult[5]);
	
	//fm.all("FileCode").value = tResult[5];
	//fm.submit();	
	var filenameall = "tResult[7]";
	
    var tfilesList = document.getElementById("filesList");
    
    tfilesList.innerHTML = "";
    if(filenameall != null && filenameall != "undefined")
    {
    	tfilesList.innerHTML +=  "<br /> 下载 [<a href='../temp/upload/" + filenameall + "' >"+ filenameall +"</a>]";
        /**var filename = transact.split("|");       
        for(index in filename)
        {
            if(filename[index] != "")
            {
                var tSkip = filename[index].lastIndexOf("/")==-1?0:filename[index].lastIndexOf("/")+1;
                tfilesList.innerHTML +=  "<br /> 下载 [<a href='../" + filename[index] + "' >" + filename[index].substring(tSkip) + "</a>]";
            }
        }*/
    }       
}


 

 