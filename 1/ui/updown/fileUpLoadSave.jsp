<%
	//程序名称：doUpload.jsp
	//程序功能：用户手册,模板上传
	//创建日期：2007-11-23
	//创建人  ：shaoax
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
  <%@page import="java.io.*"%>
  <%@page import="org.apache.commons.fileupload.*"%>
<%
		GlobalInput globalInput = new GlobalInput(); 	 	 
		globalInput = (GlobalInput) session.getValue("GI");
		String FlagStr = "";
		String Content = "";

		//临时保存解析出的数据的保存路径
		String path = application.getRealPath("").replace('\\','/')+'/';
		String importPath = path + "temp/upload";
		System.out.println(importPath);

		DiskFileUpload fu = new DiskFileUpload();
		//设置请求消息实体内容的最大允许大小
		fu.setSizeMax(100 * 1024 * 1024);
		//设置是否使用临时文件保存解析出的数据的那个临界值
		fu.setSizeThreshold(1 * 1024 * 1024);
		//设置setSizeThreshold方法中提到的临时文件的存放目录
		fu.setRepositoryPath(importPath);
		//判断请求消息中的内容是否是“multipart/form-data”类型
		//fu.isMultipartContent(request);
		//设置转换时所使用的字符集编码
		//fu.setHeaderEncoding(encoding);
		
		List fileItems = null;
		try
		{
			//解析出FORM表单中的每个字段的数据，并将它们分别包装成独立的FileItem对象
			//然后将这些FileItem对象加入进一个List类型的集合对象中返回
			fileItems = fu.parseRequest(request);
		}
		catch(Exception ex)
		{
			fileItems = new ArrayList();
			ex.printStackTrace();
		}

			System.out.println(fileItems.size());
		//依次处理每个上传的文件
		Iterator iter = fileItems.iterator();

		String fileName = "";
		HashMap hashMap = new HashMap();
		while (iter.hasNext()) 
		{
			FileItem item = (FileItem) iter.next();
			
			//判断FileItem类对象封装的数据是否属于一个普通表单字段，还是属于一个文件表单字段
			if(item.isFormField())
			{
				//返回表单字段元素的name属性值、将FileItem对象中保存的主体内容作为一个字符串返回
				hashMap.put(item.getFieldName(), item.getString());
				//System.out.println(item.getFieldName() + " : " + item.getString());
			}
		}
		
		iter = fileItems.iterator();
		while (iter.hasNext()) 
		{
			FileItem item = (FileItem) iter.next();
			
			//判断FileItem类对象封装的数据是属于一个普通表单字段(true)，还是属于一个文件表单字段
			if(!item.isFormField())
			{
				//获得文件上传字段中的文件名
				String name = item.getName();
				System.out.println("name : " + name);
				long size = item.getSize();
				if((name==null||name.equals("")) && size==0)
				{
					continue;
				}

				fileName = name.substring(name.lastIndexOf("\\") + 1);
				System.out.println("file name : "+fileName + " size : " + size);
				//Random rand = new Random();
				//int tSelect = rand.nextInt(999);
				String filePath = (String) hashMap.get("filePath");
				System.out.println("filePath : " + path + filePath + "/" +  fileName);
				File file = new File(path + filePath);   
				if (!file.exists()) 
				{    
					file.mkdirs();
				}

				
				//保存上传的文件到指定的目录
				try 
				{
					item.write(new File(path + filePath + "/" +  fileName));

					LCFileManageSchema fileManage = new LCFileManageSchema();
					String fielNo = new SysMaxNoPicch().CreateMaxNo("FILENO", 10);
					fileManage.setFileNo(fielNo);
					fileManage.setFileName(fileName);
					fileManage.setFileCode("test");
					fileManage.setFileType((String) hashMap.get("fileType"));
					fileManage.setFileDetailType((String) hashMap.get("fileDetailType"));
					fileManage.setOtherNo((String) hashMap.get("otherNo"));
					fileManage.setOtherNoType((String) hashMap.get("otherNoType"));
					fileManage.setFilePath(filePath);
					fileManage.setDiscription((String) hashMap.get("discription"));
					fileManage.setManageCom(globalInput.ComCode);
					fileManage.setOperator(globalInput.Operator);
					fileManage.setMakeDate(PubFun.getCurrentDate());
					fileManage.setMakeTime(PubFun.getCurrentTime());
					fileManage.setModifyDate(PubFun.getCurrentDate());
					fileManage.setModifyTime(PubFun.getCurrentTime());
					
					
					MMap map = new MMap();
					map.put(fileManage, "INSERT");
					VData inputData = new VData();
					inputData.add(map);
					PubSubmit pubSubmit = new PubSubmit();
					if (pubSubmit.submitData(inputData, "")) {
						FlagStr = "succ";
						Content = "文件上载成功.";
					}
					else
					{
						FlagStr = "Fail";
						Content = fileName + "上载失败.";
						break;
					}
				} catch(Exception e) 
				{
					e.printStackTrace();
					System.out.println("upload file error ...");
					FlagStr = "Fail";
					Content = fileName + "上载失败.";
					break;
				}
			}
		}
		System.out.println("run succ...");
%>                      
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>




