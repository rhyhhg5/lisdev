<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：downLoad.jsp
	//程序功能：用户手册,模板下载界面
	//创建日期：2007-11-23
	//创建人  ：shaoax
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
	<%
		GlobalInput tGI = new GlobalInput();
		tGI = (GlobalInput) session.getValue("GI");
	%>
	<script>
		var operator = "<%=tGI.Operator%>";   //记录操作员
		var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
		var fileType1 = "";
	</script>

	<head>
		<meta http-equiv="Content-Type" content="text/html charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="./fileDownLoad.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="./fileDownLoadInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form method=post name=fm target="fraSubmit">
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输入查询条件：
					</td>
				</tr>
			</table>
			<table class=common align=center border="0" width="30%">
				<tr>
					<td class=title>
						文件名称:
					</td>
					<td class=input>
						<input class=common type="text" name="fileName">
					</td>
					<td class=title>
						文件类型:
					</td>
					<td class=input>
						<Input class="codeno" name="fileType"
							ondblclick="return showCodeList('filetype',[this,fileTypeName],[0,1],null,null,null,1);"
							onkeyup="return showCodeList('filetype',[this,fileTypeName],[0,1],null,null,null,1);"
							verify="类别|notnull" /><Input class="codename" readonly="readonly" name="fileTypeName" elementtype=nacessary/>
					</td>
					<td class=title>
						文件细类:
					</td>
					<td class=input>
						<Input class="codeno" name="fileDetailType"
							ondblclick="return showCodeList('filetypedetail',[this,fileDetailTypeName],[0,1],null,fileType1,'Code',1);"
							onkeyup="return showCodeList('filetypedetail',[this,fileDetailTypeName],[0,1],null,fileType1,'Code',1);"
							verify="文件细类|notnull" readonly="readonly" /><Input class="codename" readonly="readonly" name="fileDetailTypeName" elementtype=nacessary/>
					</td>
				</tr>
			</table>
			<input value="查  询" class=cssButton type=button onclick="query();">
			<input value="重  置" class="cssButton" type=button
				onclick="initForm();">
			<input class=common type=hidden name="FileCode">
			<!-- 查询结果 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divfile);">
					</td>
					<td class=titleImg>
						查询结果列表
					</td>
				</tr>
			</table>

			<!-- 信息（列表） -->
			<div id="divFileGrid" style="display:''">
				<span id="spanFileGrid"></span>
			</div>
			<div id="divPage" align="center" style="display:'none'">
				<input class="cssButton" value="首  页" title="The First Page" type="button" onclick="turnPage.firstPage();" />
				<input class="cssButton" value="上一页" title="Back" type="button" onclick="turnPage.previousPage();" />
				<input class="cssButton" value="下一页" title="Next" type="button" onclick="turnPage.nextPage();" />
				<input class="cssButton" value="尾  页" title="The Last Page" type="button" onclick="turnPage.lastPage();" />
			</div>
			<table class=common>
			<tr><td>
				<!--div id="filesList" style="margin-top:10px;"></div-->	
				<input value="下  载" class=cssButton type=button onclick="downLoad();">
			</tr></td>
			</table>
			<!--div><a href="../temp/upload/3_8.rar">3_8.rar</a></div-->			
		</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>

