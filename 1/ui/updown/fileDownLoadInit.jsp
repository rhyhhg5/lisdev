<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：BriefContDeleteInit.jsp
//程序功能：简易保单整单删除初始化
//创建日期：2007-11-22
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	String strManageCom = globalInput.ComCode;
	String strOperator = globalInput.Operator;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
    // 保单查询条件  
    fm.all('fileName').value = '';
    fm.all('fileType').value = '';  
    fm.all('fileTypeName').value = ''; 
    fm.all('fileDetailType').value = '';  
    fm.all('fileDetailTypeName').value = '';                              
  }
  catch(ex)
  {
    alert("在downLoadInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initFileGrid();
  }
  catch(ex)
  {
    alert("在downLoadInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }  
}


// 保单信息列表的初始化
function initFileGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=30;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="文件编号";         			//列名
		iArray[1][1]="100px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="文件名";         			//列名
		iArray[2][1]="100px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="文件代码";         			//列名
		iArray[3][1]="50px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="文件类型";         			//列名
		iArray[4][1]="70px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="文件细类";         			//列名
		iArray[5][1]="70px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="其它号码";         			//列名
		iArray[6][1]="50px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="其它号码类型";         			//列名
		iArray[7][1]="50px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="相对路径";         			//列名
		iArray[8][1]="50px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[9]=new Array();
		iArray[9][0]="文件描述";         			//列名
		iArray[9][1]="200px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[10]=new Array();
		iArray[10][0]="上传机构";         			//列名
		iArray[10][1]="70px";            		//列宽
		iArray[10][2]=100;            			//列最大值
		iArray[10][3]=0;  
		
		iArray[11]=new Array();
		iArray[11][0]="上传人";         		//列名
		iArray[11][1]="50px";            		//列宽
		iArray[11][2]=100;            			//列最大值
		iArray[11][3]=0;  
		
		iArray[12]=new Array();
		iArray[12][0]="上传时间";         		//列名
		iArray[12][1]="150px";            		//列宽
		iArray[12][2]=100;            			//列最大值
		iArray[12][3]=0;  
		
		FileGrid = new MulLineEnter( "fm" , "FileGrid" ); 
		//这些属性必须在loadMulLine前
		FileGrid.mulLineCount = 3;   
		FileGrid.displayTitle = 1;
		FileGrid.locked = 1;
		FileGrid.canSel = 1;
		FileGrid.hiddenPlus = 1;
		FileGrid.hiddenSubtraction = 1;
		FileGrid.loadMulLine(iArray);
		
		FileGrid.selBoxEventFuncName = "FileSelect"; 
	}
  catch(ex)
  {
    alert(ex);
  }
}


</script>