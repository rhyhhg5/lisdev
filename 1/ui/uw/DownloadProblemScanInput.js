//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();
var manageCom;

// 提交，保存按钮对应操作
function submitForm() {
	var i = 0;
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	initPolGrid();
	fm.submit(); // 提交
}

function getQueryResult() {
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if (tRow == 0 || tRow == null || arrDataSet == null)
		return arrSelected;

	arrSelected = new Array();
	// 设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow - 1];

	return arrSelected;
}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	// showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	easyQueryClick();
}

// 重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
	try {
		initForm();
	} catch (re) {
		alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

// 提交前的校验、计算
function beforeSubmit() {
	// 添加操作
}

// 查询按钮
function easyQueryClick() {
	// 初始化表格
	initPolGrid();
	if (!verifyInput2())
		return false;
	if (fm.PrtNo.value == "" && fm.ContNo.value == ""
			&& fm.startMakeDate.value == "") {
		alert("印刷号、保单号和下发起始日期至少录入一个！");
		return false;
	}
	if (fm.PrtNo.value != "" || fm.ContNo.value != "")
		fm.startMakeDate.value = "";

	var tSeltype = "";// 是否回销
	var tStateFlag = "";// 是否已打
	if (RePrintFlag == "1") {
		if (fm.all('SelType').value == "0") {
			tSeltype = "and a.PrtSeq not in (select doccode from es_doc_main where subtype='TB22' )";
		} else if (fm.all('SelType').value == "1") {
			tSeltype = "and a.PrtSeq in (select doccode from es_doc_main where subtype='TB22' )";
		}
		if (fm.all('StateFlag').value == "0") {
			tStateFlag = " and a.stateflag='0' ";
		} else if (fm.all('StateFlag').value == "1"
				|| fm.all('StateFlag').value == "2") {
			tStateFlag = " and a.stateflag<>'0' ";
		}
	} else {
		tStateFlag = " and a.StateFlag = '0' ";
	}

	// 2014-11-4 杨阳
	// 把新的业务员编码转成旧的
	var agentCodesql = "";
	if (fm.AgentCode.value != "" && fm.AgentCode.value != null) {
		agentCodesql = " and b.Agentcode = (select getAgentCode("
				+ fm.AgentCode.value + ") from dual) ";
	}

	// 书写SQL语句
	var strSQL = "";
	strSQL = "SELECT a.PrtSeq, b.PrtNo, b.PolApplyDate, b.CValiDate, b.AppntName, b.Prem, "
			+ "getUniteCode(b.Agentcode) as agentcode, (select Name from LAAgent aa where aa.AgentCode = b.AgentCode) AgentName, "
			+ "(select substr(BranchAttr, 1, 10) from LABranchGroup bg where bg.AgentGroup = b.AgentGroup) BranchAttr,"
			+ "(select Name from LABranchGroup bg where BranchLevel = '03' and bg.BranchAttr = (select substr(branchattr, 1, 10) from LABranchGroup bg where bg.AgentGroup = b.AgentGroup)) BranchName, "
			+ "a.MakeDate, "
			+ "(select MakeDate from es_doc_main where DocCode = a.PrtSeq and SubType = 'TB22') TakeBackDate, "
			+ "(case when a.StateFlag='0' then '未打' else '已打' end) StateFlag, "
			+ "PrintTimes, b.ManageCom, c.Operator, "
			+ "trim(c.QuestionObj)||'--'||trim(c.ErrFieldName)||'--'||trim(c.IssueCont) Issue "
			+ "FROM LOPrtManager a, LCCont b, LCIssuePol c "
			+ "WHERE a.OtherNo = b.ContNo and a.Code = '85' "
			+ "and b.signdate is not null and (b.printcount = '0' or b.printcount='-1') "
			+ "and c.PrtSeq = a.PrtSeq and c.ContNo = b.ContNo "
			+ "and c.BackObjType = '2' and c.NeedPrint = 'Y' "
			+ "and (b.UWFlag not in ('1','a','8') or b.UWFlag is null) "
			+ "and b.ManageCom like '"
			+ fm.ManageCom.value
			+ "%' "
			+ getWherePart('b.AgentGroup', 'agentGroup')
			+ getWherePart('b.PrtNo', 'PrtNo')
			+ getWherePart('b.ContNo', 'ContNo')
			+ getWherePart('b.AppntName', 'AppntName')
			+ getWherePart('a.MakeDate', 'startMakeDate', '>=')
			+ getWherePart('a.MakeDate', 'endMakeDate', '<=')
			+ getWherePart('b.CvaliDate', 'CvaliDate')
			+ getWherePart('b.PolApplyDate', 'PolApplyDate')
			+ tSeltype
			+ tStateFlag + agentCodesql + " order by BranchAttr, a.MakeDate";

	fm.querySql.value = strSQL;

	strSQL = "select distinct PrtSeq, PrtNo, PolApplyDate, CValiDate, AppntName, Prem, agentcode, "
			+ "AgentName, BranchAttr, BranchName, MakeDate, TakeBackDate, StateFlag, PrintTimes, "
			+ "ManageCom from (" + strSQL + ") as X";
	turnPage.strQueryResult = easyQueryVer3(strSQL);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有要打印的问题件通知书！");
		return false;
	}

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)

	// 设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = PolGrid;

	// 保存SQL语句
	turnPage.strQuerySql = strSQL;

	// 设置查询起始位置
	turnPage.pageIndex = 0;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,
			MAXSCREENLINES);
	// tArr = chooseArray(arrDataSet,[0,1,3,4])
	// 调用MULTILINE对象显示查询结果

	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	// displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function queryBranch() {
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

// 查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult) {

	if (arrResult != null) {
		fm.BranchGroup.value = arrResult[0][3];
		fm.agentGroup.value = arrResult[0][0];
	}
}

var turnPage2 = new turnPageClass();
var mPrtSeq;

// 下载投保单信息清单 2007-10-30
function printList() {
	if (fm.querySql.value != null && fm.querySql.value != ""
			&& PolGrid.mulLineCount > 0) {
		fm.action = "FillIssuePrint.jsp";
		fm.target = "_blank";
		fm.submit();
	} else {
		alert("请先执行查询操作，再打印！");
		return;
	}
}

// 下拉框选择后执行 zhangjianbao 2007-11-16
function afterCodeSelect(cName, Filed) {
	if (cName == 'statuskind') {
		saleChnl = fm.saleChannel.value;
	}
}

// 选择营销机构前执行 zhangjianbao 2007-11-16
function beforeCodeSelect() {
	if (saleChnl == "")
		alert("请先选择销售渠道");
}

// pdf提交，保存按钮对应操作
function printPolpdfNew() {
	var i = 0;

	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if (tSel == 0 || tSel == null) {
		alert("请先选择一条记录，再点击返回按钮。");
		return;
	}

	else {
		arrReturn = getQueryResult();
		PrtSeq = PolGrid.getRowColData(tSel - 1, 1);

		if (null == arrReturn) {
			alert("无效的数据");
			return;
		}
	}
	tPrtSeq = PolGrid.getRowColData(tSel - 1, 1);
	// tMissionID = PolGrid.getRowColData(tSel-1,9);
	// tSubMissionID = PolGrid.getRowColData(tSel-1,10);
	tMissionID = "";
	tSubMissionID = "";
	tPrtNo = PolGrid.getRowColData(tSel - 1, 2);
	tContNo = easyExecSql("select contno from lccont where prtno='" + tPrtNo
			+ "'");
	fmSave.PrtSeq.value = tPrtSeq;
	fmSave.MissionID.value = tMissionID;
	fmSave.SubMissionID.value = tSubMissionID;
	fmSave.PrtNo.value = tPrtNo;
	fmSave.ContNo.value = tContNo;
	fmSave.fmtransact.value = "PRINT";

	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fmSave.target = "fraSubmit";
	// fmSave.action="./PrintPDFSave.jsp?Code=085&RePrintFlag="+RePrintFlag;
	fmSave.action = "../uw/PDFProblemPrintSave.jsp?Code=85&OtherNo=" + tContNo
			+ "&OldPrtSeq=" + tPrtSeq;
	fmSave.submit();
	showInfo.close();

}

// 下载事件
function afterSubmit2(FlagStr, Content) {
	showInfo.close();
/*	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else if (FlagStr == "PrintError") {
		// window.parent.close();
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {*/
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	//}
}