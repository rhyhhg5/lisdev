<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
String GrpContNo = request.getParameter("GrpContNo");
String RiskFlag = request.getParameter("RiskFlag");
%>
<head>
<script>
GrpContNo="<%=GrpContNo%>";
RiskFlag="<%=RiskFlag%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="MakePrice.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="MakePriceInit.jsp"%>
<title>承包过程记录 </title>
</head>
<body onload="initForm();initButtons();">
	<form method=post name=fm target="fraSubmit" action="MakePriceSave.jsp">
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>合同信息</td>
			</tr>
		</table>
		<table  class= common align=center>
			<TR  class= common>
				<TD  class= title>集体合同号</TD>
				<TD  class= input>
					<Input class=readonly readonly name=GrpContNo value="<%=GrpContNo%>">
					<Input type=hidden name=ProposalGrpContNo>	
					<input type=hidden name=mOperate>
				</TD>
				<TD  class= title>管理机构</TD>
				<TD  class= input>
					<Input class=readonly readonly name=ManageCom>
					<Input type=hidden name='OperFlag' >
					<Input type=hidden name='DutyCode' >
					<Input type=hidden name='RiskCode' >
					<Input type=hidden name='ContPlanCode' >
				</TD>
				<TD  class= title>投保人客户号</TD>
				<TD  class= input>
					<Input class= readonly readonly name=AppntNo>
				</TD>
			</TR>
			<TR  class= common>
				
				<TD  class= title>投保单位名称</TD>
				<TD  class= input>
					<Input class= readonly readonly name=GrpName>
				</TD>
			</TR>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>保险计划信息</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanCodeGrid" ></span>
				</td>
			</tr>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>险种信息</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanRiskInfoGrid" ></span>
				</td>
			</tr>
		</table>
		
	<table>
		
	<Div  id= "divRiskFactorGrid" style= "display:''">
		<table class= common>
		    	<tr>
		
		    		<td class= titleImg>
		    			 核保险种定价
		    		</td>
		    		<td width="60">
		    		</td>
		        <td class= titleImg>
		    			 产品险种定价
		    		</td>
		
		    	</tr>

			<tr  class= common>
				
				<td text-align: left colSpan=1>
					<span id="spanRiskFactorGrid" ></span>
				</td>
				<td></td>
				<td text-align: left colSpan=1>
					<span id="spanMPRiskFactorGrid" ></span>
				</td>
			</tr>
	</div>
	<div id=divRiskFactorButton style="display:''">
			<tr>
				<td align=left class= titleImg>
					<INPUT id=UWRiskFactor name=UWRiskFactor VALUE="核保险种定价要素保存" class="cssButton" TYPE=button onclick="SaveRiskFactor();">
				</td>
				<td align=left>
					<INPUT VALUE="<" TYPE=button  onclick="moveRiskFactor()" class="cssButton">
				</td>
				<td align=left class= titleImg>
					<INPUT id=MPRiskFactor name=MPRiskFactor VALUE="产品险种定价要素保存" class="cssButton" TYPE=button onclick="SaveMPRiskFactor();">
					<input name=RiskFlag type=hidden>
				</td>
			</tr>
		</table>
	</div>
	
	<hr></hr>
		<table>
				<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"">
				</td>
				<td class= titleImg>险种责任信息</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanContPlanDutyGrid" ></span>
				</td>
			</tr>
		</table>
	<Div  id= "divDutyFactorGrid" style= "display:''">
		<table class=common>
			<tr>
		    		<td class= titleImg>
		    			 核保责任定价要素
		    		</td>
		    		<td width="60">
		    		</td>
		        <td class= titleImg>
		    			 产品责任定价要素
		    		</td>
		    		<td class=titleImg></td>
		    		
			</tr>

			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanDutyFactorGrid" ></span>
				</td>
				<td></td>
				<td text-align: left colSpan=1>
					<span id="spanMPDutyFactorGrid" ></span>
				</td>
			</tr>

	</div>

		<Div  id= "divDutyFactorButton" style= "display: ''" align= left > 
			<tr>
				<td align=left class= titleImg>		
					<INPUT id=UWDutyFactor name=UWDutyFactor VALUE="核保责任定价要素保存" class="cssButton"  TYPE=button onclick="SaveDutyFactor();">
				</td>
				<td align=center>
					<INPUT VALUE="<" TYPE=button  onclick="moveDutyFactor()" class="cssButton">
				</td>
				<td align=left class= titleImg>		
					<INPUT id=MPDutyFactor name=MPDutyFactor VALUE="产品责任定价要素保存" class="cssButton"  TYPE=button align=right onclick="SaveMPDutyFactor();">
					<input name=DutyFlag type=hidden>
				</td>
			</tr>		
		</Div>

<hr></hr>

	 <tr>
   	<TD  class= titleImg>
   	  核保意见
   	</TD>
   	<td width="100">
	 	</td>
   	<TD  class= titleImg>
   	  定价意见
   	</TD>
   </tr>
   <tr>
   	<TD  class= input> <textarea name="UWIdea" cols="50%" rows="5" witdh=50% class="common" ></textarea></TD>
    <td width="60">
	 	</td>
	 	<TD  class= input> <textarea name="MPIdea" cols="50%" rows="5" witdh=50% class="common"></textarea></TD>
   </tr> 
   <tr>
   	<TD  class= title>
   			<INPUT name=PlanCodeNew type=hidden>
   			<INPUT name=GrpContNoNew type=hidden>
   			<INPUT name=PlanTypeNew type=hidden >
				<INPUT id=UWSave name=UWSave VALUE="保  存" class="cssButton"  TYPE=button align=right onclick="UWSave1();">
   	</TD>
   	<td width="60">
	 	</td>
   	<TD  class= title>
   	 		<INPUT id=MPSave name=MPSave VALUE="保  存" class="cssButton"  TYPE=button align=right onclick="MPSave1();">
   	</TD>   	
   </tr>
</table>
		<p>
		<INPUT VALUE="返  回" class="cssButton"  TYPE=button onclick="returnparent();">
		</p>
		
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>